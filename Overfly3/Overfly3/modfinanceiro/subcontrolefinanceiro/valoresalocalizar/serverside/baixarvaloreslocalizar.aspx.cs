﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.valoresalocalizarEx.serverside
{
    public partial class baixarvaloreslocalizar : System.Web.UI.OverflyPage
    {

        private string Resultado;
        private string ResultadoVL;
        private string Financeiro;

        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private Integer FinanceiroID;

        public Integer nFinanceiroID  
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value; }
        }
        private Integer TipoValorID;

        public Integer nTipoValorID
        {
            get { return TipoValorID; }
            set { TipoValorID = value; }
        }
        private string EhEstorno;

        public string bEhEstorno
        {
            get { return EhEstorno; }
            set { EhEstorno = value; }
        }
        private string EhImporte;

        public string bEhImporte
        {
            get { return EhImporte; }
            set { EhImporte = value; }
        }
        private Integer HistoricoPadraoID;

        public Integer nHistoricoPadraoID
        {
            get { return HistoricoPadraoID; }
            set { HistoricoPadraoID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private string RelPesContaID;

        public string nRelPesContaID
        {
            get { return RelPesContaID; }
            set { RelPesContaID = value; }
        }
        private string SaldoTotal;

        public string nSaldoTotal
        {
            get { return SaldoTotal; }
            set { SaldoTotal = value; }
        }

        private string[] Saldo;

        public string[] nSaldo
        {
            get { return Saldo; }
            set { Saldo = value; }
        }
        private string[] ValorID;

        public string[] nValorID
        {
            get { return ValorID; }
            set { ValorID = value; }
        }

        protected void Gerador()
        {
            if (FinanceiroID.intValue() == 0)
            {
                ProcedureParameters[] procParams = new ProcedureParameters[20];
                procParams[0] = new ProcedureParameters(
                    "@EmpresaID",
                    System.Data.SqlDbType.Int,
                    EmpresaID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(EmpresaID.ToString()));

                procParams[1] = new ProcedureParameters(
                    "@TipoFinanceiroID",
                    System.Data.SqlDbType.Int,
                    TipoValorID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(TipoValorID.ToString()));

                procParams[2] = new ProcedureParameters(
                   "@Duplicata",
                   System.Data.SqlDbType.VarChar,
                   DBNull.Value);

                procParams[3] = new ProcedureParameters(
                   "@PessoaID",
                   System.Data.SqlDbType.Int,
                   EmpresaID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(EmpresaID.ToString()));

                procParams[4] = new ProcedureParameters(
                   "@dtEmissao",
                   System.Data.SqlDbType.DateTime,
                   DBNull.Value);

                procParams[5] = new ProcedureParameters(
                   "@FormaPagamentoID",
                   System.Data.SqlDbType.Int,
                   1037);

                procParams[6] = new ProcedureParameters(
                   "@PrazoPagamento",
                   System.Data.SqlDbType.Int,
                   0);

                procParams[7] = new ProcedureParameters(
                   "@dtVencimento",
                   System.Data.SqlDbType.DateTime,
                   DBNull.Value);

                procParams[8] = new ProcedureParameters(
                   "@MoedaID",
                   System.Data.SqlDbType.Int,
                   DBNull.Value);

                procParams[9] = new ProcedureParameters(
                   "@Valor",
                   System.Data.SqlDbType.Decimal,
                   SaldoTotal == "0" ? DBNull.Value : (Object)Convert.ToDouble(SaldoTotal));

                procParams[10] = new ProcedureParameters(
                   "@TaxaAtualizacao",
                   System.Data.SqlDbType.Int,
                   0);

                procParams[11] = new ProcedureParameters(
                   "@EhEstorno",
                   System.Data.SqlDbType.Bit,
                   bEhEstorno.ToString() == "0" ? false : true);

                procParams[12] = new ProcedureParameters(
                   "@EhImporte",
                   System.Data.SqlDbType.Bit,
                   bEhImporte.ToString() == "0" ? false : true);

                procParams[13] = new ProcedureParameters(
                   "@HistoricoPadraoID",
                   System.Data.SqlDbType.Int,
                   HistoricoPadraoID.intValue() == 0 ? DBNull.Value : (Object)HistoricoPadraoID.ToString());

                procParams[14] = new ProcedureParameters(
                   "@ImpostoID",
                   System.Data.SqlDbType.Int,
                  DBNull.Value);

                procParams[15] = new ProcedureParameters(
                   "@Observacao",
                   System.Data.SqlDbType.VarChar,
                   DBNull.Value);

                procParams[16] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                  UsuarioID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(UsuarioID.ToString()));                   

                procParams[17] = new ProcedureParameters(
                   "@RelPesContaID",
                   System.Data.SqlDbType.Int,
                   RelPesContaID.ToString() == "null" ? DBNull.Value : (Object)Convert.ToInt32(RelPesContaID));

                procParams[18] = new ProcedureParameters(
                   "@FinanceiroID",
                   System.Data.SqlDbType.Int,
                  DBNull.Value,
                  ParameterDirection.InputOutput);

                procParams[19] = new ProcedureParameters(
                   "@Resultado",
                   System.Data.SqlDbType.VarChar,
                  DBNull.Value,
                  ParameterDirection.InputOutput);
                procParams[19].Length = 8000;


                DataInterfaceObj.execNonQueryProcedure("sp_Financeiro_Gerador", procParams);

                if (procParams[18].Data != DBNull.Value)
                    Financeiro = (procParams[18].Data.ToString());

                if (procParams[19].Data != DBNull.Value)
                    Resultado = procParams[19].Data.ToString();
            }

            if (Financeiro.ToString() != "0")
            {
                for (int i = 0; i < ValorID.Length; i++)
                {
                    ProcedureParameters[] procParamsLocalizar = new ProcedureParameters[8];

                    procParamsLocalizar[0] = new ProcedureParameters(
                        "@ValorID",
                        System.Data.SqlDbType.Int,
                        ValorID.Length == 0 ? DBNull.Value : (Object)Convert.ToInt32(ValorID[i]));

                    procParamsLocalizar[1] = new ProcedureParameters(
                        "@FinanceiroID",
                        System.Data.SqlDbType.Int,
                        Financeiro.ToString() == "0" ? DBNull.Value : (Object)Convert.ToInt32(Financeiro.ToString()));

                    procParamsLocalizar[2] = new ProcedureParameters(
                       "@Valor",
                       System.Data.SqlDbType.VarChar,
                        Saldo.Length == 0 ? DBNull.Value : (Object)Convert.ToDouble(Saldo[i]));

                    procParamsLocalizar[3] = new ProcedureParameters(
                       "@OK",
                       System.Data.SqlDbType.Bit,
                       true);

                    procParamsLocalizar[4] = new ProcedureParameters(
                       "@Observacao",
                       System.Data.SqlDbType.VarChar,
                       DBNull.Value);

                    procParamsLocalizar[5] = new ProcedureParameters(
                       "@UsuarioID",
                       System.Data.SqlDbType.Int,
                       UsuarioID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(UsuarioID.ToString()));

                    procParamsLocalizar[6] = new ProcedureParameters(
                       "@Incluir",
                       System.Data.SqlDbType.Int,
                       true);

                    procParamsLocalizar[7] = new ProcedureParameters(
                       "@Resultado",
                       System.Data.SqlDbType.VarChar,
                      DBNull.Value,
                      ParameterDirection.InputOutput);
                    procParamsLocalizar[7].Length = 8000;


                    DataInterfaceObj.execNonQueryProcedure("sp_ValorLocalizar_AssociarFinanceiro", procParamsLocalizar);

                    if (procParamsLocalizar[7].Data != DBNull.Value)
                        ResultadoVL = procParamsLocalizar[7].Data.ToString();
                }

            }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            Gerador();
            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select '" + Resultado + "' as Resultado, " +
                                Financeiro + " as FinanceiroID"));
        }
    }
}