﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.valoresalocalizarEx.serverside
{
    public partial class fillcmbsmodal : System.Web.UI.OverflyPage
    {
        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string SQL = "";
            SQL = "Select 0 AS Indice, 0 as fldID, '' as fldName , '0' AS Ordem " +
                   "UNION ALL " +
                   "Select DISTINCT 0 AS Indice, a.ItemID as fldID, a.ItemAbreviado as fldName , LTRIM(RTRIM(STR(a.ordem))) AS Ordem " +
                   "From TiposAuxiliares_Itens a WITH(NOLOCK), Financeiro b WITH(NOLOCK) " +
                   "Where a.ItemID = b.FormaPagamentoID AND b.EmpresaID = " + EmpresaID + " " +
                   "UNION ALL " +
                   "Select 1 AS Indice, 0 as fldID, '' as fldName , '0' AS Ordem " +
                   "UNION ALL " +
                   "Select DISTINCT 1 AS Indice, a.PessoaID AS ItemID, a.Fantasia  as fldName, a.Fantasia AS Ordem " +
                   "FROM Pessoas a WITH(NOLOCK), Financeiro b WITH(NOLOCK) " +
                   "WHERE a.PessoaID=b.ProprietarioID AND b.EmpresaID = " + EmpresaID + " " +
                   "Order By Indice, a.Ordem ";

            WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
        }
    }
}