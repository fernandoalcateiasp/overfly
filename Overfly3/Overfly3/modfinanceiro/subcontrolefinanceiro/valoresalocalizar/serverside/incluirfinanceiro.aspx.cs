﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.valoresalocalizarEx.serverside
{
    public partial class incluirfinanceiro : System.Web.UI.OverflyPage
    {
        private java.lang.Integer Zero = new java.lang.Integer(0);
        private Integer ValorID;

        public Integer nValorID
        {
            get { return ValorID; }
            set { ValorID = value; }
        }
        private Integer FinanceiroID;

        public Integer nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = (value != null ? value : Zero); }
        }
        private string Valor;

        public string nValor
        {
            get { return Valor; }
            set { Valor = value; }
        }
        private Integer UserID;

        public Integer nUserID
        {
            get { return UserID; }
            set { UserID = value; }
        }
        private Integer OK;

        public Integer nOK
        {
            get { return OK; }
            set { OK = value; }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            string Resultado = "";
            ProcedureParameters[] procParams = new ProcedureParameters[9];

            procParams[0] = new ProcedureParameters(
            "@ValorID",
            System.Data.SqlDbType.Int,
            ValorID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(ValorID));

            procParams[1] = new ProcedureParameters(
            "@FinanceiroID",
            System.Data.SqlDbType.Int,
            FinanceiroID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(FinanceiroID));

            procParams[2] = new ProcedureParameters(
            "@Valor",
            System.Data.SqlDbType.Decimal,
            Valor == null ? DBNull.Value : (Object)Convert.ToDouble(Valor));

            procParams[3] = new ProcedureParameters(
            "@ValorEncargos",
            System.Data.SqlDbType.Decimal,
            DBNull.Value);

            procParams[4] = new ProcedureParameters(
            "@OK",
            System.Data.SqlDbType.Bit,
            OK.ToString() == "0" ? false : true);

            procParams[5] = new ProcedureParameters(
            "@Observacao",
            System.Data.SqlDbType.VarChar,
            DBNull.Value);

            procParams[6] = new ProcedureParameters(
            "@UsuarioID",
            System.Data.SqlDbType.Int,
            UserID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(UserID));

            procParams[7] = new ProcedureParameters(
            "@Incluir",
            System.Data.SqlDbType.Bit,
            true);

            procParams[8] = new ProcedureParameters(
            "@Resultado",
            System.Data.SqlDbType.VarChar,
            DBNull.Value,
            ParameterDirection.InputOutput);
            procParams[8].Length = 8000;


            DataInterfaceObj.execNonQueryProcedure("sp_ValorLocalizar_AssociarFinanceiro", procParams);

            if (procParams[8].Data != DBNull.Value)
                Resultado += procParams[8].Data.ToString();



            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT '" + Resultado + "' AS Mensagem"));
        }
    }
}