﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.valoresalocalizarEx.serverside
{
    public partial class verificacaovalor : System.Web.UI.OverflyPage
    {

        private Integer ValorID;

        public Integer nValorID
        {
            get { return ValorID; }
            set { ValorID = value; }
        }
        private Integer EstadoDeID;

        public Integer nEstadoDeID
        {
            get { return EstadoDeID; }
            set { EstadoDeID = value; }
        }
        private Integer EstadoParaID;

        public Integer nEstadoParaID
        {
            get { return EstadoParaID; }
            set { EstadoParaID = value; }
        }
        
        protected override void PageLoad(object sender, EventArgs e)
        {
            string sql = "";
            sql = "SELECT dbo.fn_ValorLocalizar_Verifica( " + ValorID+", " +
												EstadoDeID+", " +
												EstadoParaID+", GETDATE()) AS Verificacao";

            WriteResultXML(DataInterfaceObj.getRemoteData(sql));
        }
    }
}