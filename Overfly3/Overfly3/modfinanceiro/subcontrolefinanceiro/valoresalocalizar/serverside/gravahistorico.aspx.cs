﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.valoresalocalizarEx.serverside
{
    public partial class gravahistorico : System.Web.UI.OverflyPage
    {
        private Integer ValorID;

        public Integer nValorID
        {
            get { return ValorID; }
            set { ValorID = value; }
        }
        private string Historico;

        public string sHistorico
        {
            get { return Historico; }
            set { Historico = value; }
        }
        private Integer NumeroCheque;

        public Integer nNumeroCheque
        {
            get { return NumeroCheque; }
            set { NumeroCheque = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string sql = "";
            int fldresp = 0;

            sql = "UPDATE ValoresLocalizar SET Historico = '" + sHistorico + "' , " +
            "NumeroDocumento = '" + (NumeroCheque) + "'  " +
            "WHERE (ValorID = " + (ValorID) + ")";

            fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select '" + fldresp + "' as fldresp "));
        }
    }
}