﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.valoresalocalizarEx.serverside
{
    public partial class estornavalorlocalizar : System.Web.UI.OverflyPage
    {
        private string MensagemErro;

        private Integer ProcessoID;

        public Integer nProcessoID
        {
            get { return ProcessoID; }
            set { ProcessoID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private string Observacao;

        public string sObservacao
        {
            get { return Observacao; }
            set { Observacao = value; }
        }

        public void EstornarFechamento()
        {

            ProcedureParameters[] procParam = new ProcedureParameters[4];

            procParam[0] = new ProcedureParameters(
                "@ValorID",
                System.Data.SqlDbType.Int,
                ProcessoID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(ProcessoID.ToString()));

            procParam[1] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                UsuarioID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(UsuarioID.ToString()));

            procParam[2] = new ProcedureParameters(
                "@Observacoes",
                System.Data.SqlDbType.VarChar,
                Observacao == null ? DBNull.Value : (Object)Observacao.ToString());

            procParam[3] = new ProcedureParameters(
                "@MensagemErro",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParam[3].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure("sp_ValorLocalizar_EstornarFechamento", procParam);

            if (procParam[3].Data != DBNull.Value)
                MensagemErro = procParam[3].Data.ToString();
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            EstornarFechamento();
            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select '" + MensagemErro + "' as MensagemErro "));
        }
    }
}