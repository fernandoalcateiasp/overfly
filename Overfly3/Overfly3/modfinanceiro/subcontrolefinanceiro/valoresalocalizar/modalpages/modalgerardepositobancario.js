/********************************************************************
modalgerardepositobancario.js

Library javascript para o modalgerardepositobancario.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;

var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalgerardepositobancarioBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalgerardepositobancarioDblClick(grid, Row, Col)
js_modalgerardepositobancarioKeyPress(KeyAscii)
js_modalgerardepositobancario_ValidateEdit()
js_modalgerardepositobancario_BeforeEdit(grid, row, col)
js_modalgerardepositobancario_AfterEdit(Row, Col)
js_fg_AfterRowColmodalgerardepositobancario (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalgerardepositobancarioBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	glb_bFirstFill = true;
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText(glb_sTitle, 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK
    btnOK.disabled = true;
	btnFillGrid.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblDataInicio','txtDataInicio',10,1,-10,-10],
						  ['lblDataFim','txtDataFim',10,1, -4],
						  ['lblMotivoDevolucao','chkMotivoDevolucao',3,1],
						  ['btnFillGrid','btn',70,1, -2],
						  ['lblContaID','selContaID',25,1, -4],
						  ['lblTotalDeposito','txtTotalDeposito',11,1],
						  ['lblSaldoFinal','txtSaldoFinal',11,1]], null, null, true);

	txtDataInicio.onfocus = selFieldContent;
	txtDataInicio.maxLength = 10;
	txtDataInicio.onkeypress = txtDataFiltro_onKeyPress;
	txtDataInicio.value = glb_dInicio;

	txtDataFim.onfocus = selFieldContent;
	txtDataFim.maxLength = 10;
	txtDataFim.onkeypress = txtDataFiltro_onKeyPress;
	txtDataFim.value = glb_dFim;
	
	chkMotivoDevolucao.onclick = chkMotivoDevolucao_onclick;

	txtTotalDeposito.readOnly = true;
	txtSaldoFinal.readOnly = true;

	selContaID.onchange = setupBtnsFromGridState;
	// Se form de depositos bancarios
	selContaID.disabled = (glb_nFormID == 9150);

    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = txtDataFim.offsetLeft + txtDataFim.offsetWidth + ELEM_GAP;    
        height = txtDataFim.offsetTop + txtDataFim.offsetHeight;    
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + txtDataFim.offsetTop;
		style.width = 70;
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		value = 'Deposito';
		title = 'Gerar dep�sito banc�rio';
    }
    
    with (btnFillGrid)
    {         
//		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		//style.top = btnOK.currentStyle.top;
		//style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnFillGrid.currentStyle.width, 10) - 4;
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtDataFiltro_onKeyPress()
{
    if ( event.keyCode == 13 )
        btn_onclick(btnFillGrid);
}

function chkMotivoDevolucao_onclick()
{
    // zera o grid
    fg.Rows = 1;
	setupBtnsFromGridState();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnFillGrid')
    {
		listar();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    lockControlsInModalWin(true);
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var i;
    var nValor, nTotalDeposito;
    var bHasRowsInGrid = fg.Rows > 1;

    nValor = 0;
    nTotalDeposito = 0;
    
    // Se form de depositos bancarios
    if (glb_nFormID == 9150)
        nTotalDeposito = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Posicao' + '\'' + '].value');
    
    if ((isNaN(nTotalDeposito)) || (nTotalDeposito==null))
		nTotalDeposito = 0;

	btnOK.disabled = true;

	if (bHasRowsInGrid)
	{
		for (i=1; i<fg.Rows; i++)
		{
			if (getCellValueByColKey(fg, 'OK', i) != 0)
			{
				btnOK.disabled = false;

				nValor = parseFloat(replaceStr(getCellValueByColKey(fg, 'Valor*', i), ',', '.'));
				
				if (! isNaN(nValor))
					nTotalDeposito += nValor;
			}	
		}
	}
	
	var sSaldoInicial = '';
	if (selContaID.selectedIndex >= 0)
	{
		sSaldoInicial = selContaID.options.item(selContaID.selectedIndex).getAttribute('saldoConta', 1);
		lblSaldoFinal.title = 'Saldo inicial: ' + sSaldoInicial;
		txtSaldoFinal.title = lblSaldoFinal.title;
		//txtSaldo.value = selContaID.options.item(selContaID.selectedIndex).getAttribute('saldoConta', 1);
	}	
	else
	{
		sSaldoInicial = '';
		lblSaldoFinal.title = sSaldoInicial;
		txtSaldoFinal.title = lblSaldoFinal.title;
		//txtSaldo.value = '';
	}	
		
	txtTotalDeposito.value = roundNumber(nTotalDeposito,2);
	
	if ( !(isNaN(replaceStr(sSaldoInicial, ',', '.')) || isNaN(replaceStr(txtTotalDeposito.value, ',', '.'))) )
		txtSaldoFinal.value = roundNumber(parseFloat(replaceStr(sSaldoInicial, ',', '.')) +
			parseFloat(replaceStr(txtTotalDeposito.value, ',', '.')),2);
	else
		txtSaldoFinal.value = '0.00';
	
	btnFillGrid.disabled = false;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }
    
	var aGrid = null;
	var i = 0;
	var sDataInicio = '';
	var sDataFim = '';
	var sMotivoDevolucao = '';
	
	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;
    
    // zera o grid
    fg.Rows = 1;
    
	if (trimStr(txtDataInicio.value) != '')
		sDataInicio = '\'' + normalizeDate_DateTime(trimStr(txtDataInicio.value), 1) + '\'';
    else
		sDataInicio = 'dbo.fn_Data_Zero(GETDATE())';

	if (trimStr(txtDataFim.value) != '')
		sDataFim = '\'' + normalizeDate_DateTime(trimStr(txtDataFim.value), 1) + '\'';
    else
		sDataFim = 'dbo.fn_Data_Zero(GETDATE())';

	if (chkMotivoDevolucao.checked)
		sMotivoDevolucao = ' AND a.MotivoDevolucao IS NOT NULL ';
	else		
		sMotivoDevolucao = ' AND a.MotivoDevolucao IS NULL ';

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	dsoGrid.SQL = 'SELECT d.ItemAbreviado AS Forma, a.dtApropriacao, a.dtEmissao, b.Fantasia, a.Documento, a.Emitente, a.BancoAgencia, a.NumeroDocumento, a.MotivoDevolucao, a.ValorID, ' +
			'CONVERT(BIT, 0) AS OK, c.SimboloMoeda, a.Valor, a.Observacao ' +
		'FROM ValoresLocalizar a WITH(NOLOCK) ' +
            'LEFT OUTER JOIN Pessoas b WITH(NOLOCK) ON (a.PessoaID = b.PessoaID) ' +
            'INNER JOIN Conceitos c WITH(NOLOCK) ON (a.MoedaID = c.ConceitoID) ' +
            'INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (a.FormapagamentoID = d.ItemID) ' +
		'WHERE (a.EmpresaID = ' + glb_aEmpresaData[0] + ' AND a.TipoValorID = 1002 AND a.EstadoID IN (41,53,48) AND a.FormaPagamentoID IN (1031, 1032) AND ' +
			'(a.dtApropriacao BETWEEN ' + sDataInicio + ' AND ' + sDataFim + ') AND ' +
			'a.ValorID NOT IN (SELECT b.ValorID FROM DepositosBancarios a WITH(NOLOCK), DepositosBancarios_ValoresLocalizar b WITH(NOLOCK) WHERE (a.EstadoID NOT IN(5) AND a.DepositoID = b.DepositoID)) ' +
			sMotivoDevolucao + ') ' +
		'ORDER BY a.FormaPagamentoID, a.dtApropriacao, a.dtEmissao, b.Fantasia, a.Documento, a.Emitente, a.BancoAgencia, a.NumeroDocumento, a.ValorID ';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
    try
    {
		dsoGrid.Refresh();
	}	
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Filtro inv�lido.') == 0 )
            return null;

		lockControlsInModalWin(false);        
        window.focus();
		txtDataFim.focus();		
	}
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

	if (glb_bFirstFill)
	{
		glb_bFirstFill = false;
		showExtFrame(window, true);
		
		window.focus();
    
		if ( !txtDataInicio.disabled )
			txtDataInicio.focus();
	}

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

    headerGrid(fg,['Forma',
				   'Apropria��o',
				   'Emiss�o',
				   'Pessoa',
				   'Documento',
				   'Emitente',
				   'Banco/Ag�ncia',
				   'N�mero',
				   'Mot',
				   'ValorID',
				   'OK',
				   '$',
				   'Valor',
				   'Observa��o'], []);

    fillGridMask(fg,dsoGrid,['Forma*',
							 'dtApropriacao*',
							 'dtEmissao*',
							 'Fantasia*',
							 'Documento*',
							 'Emitente*',
							 'BancoAgencia*',
							 'NumeroDocumento*',
							 'MotivoDevolucao*',
							 'ValorID*',
							 'OK',
							 'SimboloMoeda*',
							 'Valor*',
							 'Observacao*'],
							 ['','99/99/9999', '99/99/9999', '', '', '', '', '', '', '9999999999', '', '', '99999999.99',''],
							 ['',dTFormat, dTFormat, '', '', '', '', '', '', '##########', '', '', '###,###,##0.00','']);

    fg.Redraw = 0;
    
    alignColsInGrid(fg,[9, 12]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
	fg.FrozenCols = 1;

	paintCellsSpecialyReadOnly();
	    
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
		fg.Editable = true;
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // Merge de Colunas
    fg.MergeCells = 4;
    //fg.MergeCol(-1) = true;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;
    fg.MergeCol(5) = true;
    fg.MergeCol(6) = true;
    fg.MergeCol(7) = true;
    fg.MergeCol(8) = true;
    fg.Redraw = 2;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nDepositoID = 0;
	
	// Se form de depositos bancarios
	if (glb_nFormID == 9150)
	    nDepositoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'DepositoID' + '\'' + '].value');
	
    lockControlsInModalWin(true);
    
    strPars += '?nUserID=' + escape(glb_nUserID);
	strPars += '&nEmpresaID=' + escape(glb_aEmpresaData[0]);
	strPars += '&nRelPesContaID=' + escape(selContaID.value);
	strPars += '&nMoedaID=' + escape(glb_nMoedaID);
	strPars += '&nDepositoID=' + escape(nDepositoID);
    
    for (i=1; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'OK', i) != 0)
		{
			nDataLen++;
			strPars += '&nValorID=' + escape(getCellValueByColKey(fg, 'ValorID*', i));
		}	
	}
	
	strPars += '&nDataLen=' + escape(nDataLen);

	dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/geradepositobancario.aspx' + strPars;
	dsoGrava.ondatasetcomplete = saveDataInGrid_DSC;
	dsoGrava.refresh();
}

function listar()
{
	if (!verificaData())
		return null;
		
	if (trimStr(txtDataInicio.value) == '')
	{
	    if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;

        window.focus();
		txtDataInicio.focus();

		return null;
	}

	if (trimStr(txtDataFim.value) == '')
	{
	    if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;

        window.focus();
		txtDataFim.focus();

		return null;
	}
	
	if ( daysBetween(trimStr(txtDataFim.value), trimStr(txtDataInicio.value)) > 0 )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;

        window.focus();
		txtDataInicio.focus();

		return null;
	}

	refreshParamsAndDataAndShowModalWin(false);
}

function verificaData()
{
	var sData = trimStr(txtDataInicio.value);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(txtDataInicio.value);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        window.focus();
        
        if ( sData != '' )    
			txtDataInicio.focus();
		
		return false;
	}

	sData = trimStr(txtDataFim.value);
	bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(sData);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        window.focus();
        
        if ( sData != '' )    
			txtDataFim.focus();
		
		return false;
	}

	return true;
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
	if (glb_nFormID != 9150)
	{
		if ( !((dsoGrava.recordset.BOF) && (dsoGrava.recordset.EOF)) )
		{
		    if ((dsoGrava.recordset['DepositoID'].value != null) &&
				(dsoGrava.recordset['DepositoID'].value != 0))
			{
		        var _retMsg = window.top.overflyGen.Confirm('Gerado o Dep�sito: ' + dsoGrava.recordset['DepositoID'].value + '\n' + 
															'Detalhar o Dep�sito?');
			    
				if ( _retMsg == 0 )
					return null;
				else if ( _retMsg == 1 )
				{
					sHTMLSupID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getHtmlId()');
					sendJSCarrier(sHTMLSupID, 'SHOWDEPOSITOBANCARIO', new Array(glb_aEmpresaData[0], dsoGrava.recordset['DepositoID'].value));
				}
			}	
		}
	}
	// Se form de depositos bancarios
	else
	{
		if ( !((dsoGrava.recordset.BOF) && (dsoGrava.recordset.EOF)) )
		{
		    if ((dsoGrava.recordset['Resultado'].value != null) &&
				(dsoGrava.recordset['Resultado'].value != ''))
			{
		        if (window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0)
					return null;
			}	
		}
	}

    lockControlsInModalWin(false);

	glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerardepositobancarioBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerardepositobancarioDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    // trap so colunas editaveis
    if ( getColIndexByColKey(grid, 'OK') != Col)
        return true;
        
    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);
    
    glb_PassedOneInDblClick = true;
    
    // limpa coluna se tem um check box checado
    for ( i=1; i<grid.Rows; i++ )
    {
        if ( grid.ValueMatrix(i, Col) != 0 )
        {
            bFill = false;
            break;
        }
    }
    
    for ( i=1; i<grid.Rows; i++ )
    {
        if ( bFill )
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerardepositobancarioKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerardepositobancario_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerardepositobancario_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerardepositobancario_AfterEdit(Row, Col)
{
	setupBtnsFromGridState();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalgerardepositobancario(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return true;
	//setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************
