
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modallocalizarfinanceiroHtml" name="modallocalizarfinanceiroHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modallocalizarfinanceiro.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
     
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen//commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modallocalizarfinanceiro.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, sCurrDateFormat, nValorID, nEmpresaID, nTipoValorID, nFormaPagamentoID 
Dim nPessoaID, sFantasia, sdtEmissao, sdtApropriacao, nValor, nOK, nProcessoID

sCaller = ""
nValorID = 0
nEmpresaID = 0
nTipoValorID = 0
nFormaPagamentoID = 0
nPessoaID = 0
sFantasia = ""
sdtEmissao = "" 
sdtApropriacao = ""
nValor = 0
nOK = 0
nProcessoID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nValorID").Count
    nValorID = Request.QueryString("nValorID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nTipoValorID").Count
    nTipoValorID = Request.QueryString("nTipoValorID")(i)
Next

For i = 1 To Request.QueryString("nFormaPagamentoID").Count
    nFormaPagamentoID = Request.QueryString("nFormaPagamentoID")(i)
Next

For i = 1 To Request.QueryString("nPessoaID").Count
    nPessoaID = Request.QueryString("nPessoaID")(i)
Next

For i = 1 To Request.QueryString("sFantasia").Count
    sFantasia = Request.QueryString("sFantasia")(i)
Next

For i = 1 To Request.QueryString("sdtEmissao").Count
    sdtEmissao = Request.QueryString("sdtEmissao")(i)
Next

For i = 1 To Request.QueryString("sdtApropriacao").Count
    sdtApropriacao = Request.QueryString("sdtApropriacao")(i)
Next

For i = 1 To Request.QueryString("nValor").Count
    nValor = Request.QueryString("nValor")(i)
Next

For i = 1 To Request.QueryString("nOK").Count
    nOK = Request.QueryString("nOK")(i)
Next

For i = 1 To Request.QueryString("nProcessoID").Count
    nProcessoID = Request.QueryString("nProcessoID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nValorID = " & CStr(nValorID) & ";"
Response.Write vbcrlf
Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"
Response.Write vbcrlf
Response.Write "var glb_nTipoValorID = " & CStr(nTipoValorID) & ";"
Response.Write vbcrlf
Response.Write "var glb_nFormaPagamentoID = " & CStr(nFormaPagamentoID) & ";"
Response.Write vbcrlf
Response.Write "var glb_nPessoaID = " & CStr(nPessoaID) & ";"
Response.Write vbcrlf
Response.Write "var glb_sFantasia = " & Chr(39) & CStr(sFantasia) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_sdtEmissao = " & Chr(39) & CStr(sdtEmissao) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_sdtApropriacao = " & Chr(39) & CStr(sdtApropriacao) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nValor = " & CStr(nValor) & ";"
Response.Write vbcrlf
Response.Write "var glb_nOK = " & CStr(nOK) & ";"
Response.Write vbcrlf
Response.Write "var glb_nProcessoID = " & CStr(nProcessoID) & ";"
Response.Write vbcrlf

'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modallocalizarfinanceiro_ValidateEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modallocalizarfinanceiro_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modallocalizarfinanceiroKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modallocalizarfinanceiro_BeforeEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modallocalizarfinanceiroDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modallocalizarfinanceiroBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Atualizar linha de mensagens
js_fg_AfterRowColmodallocalizarfinanceiro (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

</head>

<body id="modallocalizarfinanceiroBody" name="modallocalizarfinanceiroBody" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <!-- Div Controles -->
    <div id="divControls" name="divControls" class="divGeneral">

        <p id="lblValorID" name="lblValorID" class="lblGeneral">ID</p>
        <input type="text" id="txtValorID" name="txtValorID" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lblEstado" name="lblEstado" class="lblGeneral">Est</p>
        <input type="text" id="txtEstado" name="txtEstado" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lblFormaPagamento" name="lblFormaPagamento" class="lblGeneral">Forma</p>
        <input type="text" id="txtFormaPagamento" name="txtFormaPagamento" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lbldtEmissao" name="lbldtEmissao" class="lblGeneral">Emiss�o</p>
        <input type="text" id="txtdtEmissao" name="txtdtEmissao" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lbldtApropriacao" name="lbldtApropriacao" class="lblGeneral">Apropria��o</p>
        <input type="text" id="txtdtApropriacao" name="txtdtApropriacao" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lblBancoAgencia" name="lblBancoAgencia" class="lblGeneral">Banco/Ag�ncia</p>
        <input type="text" id="txtBancoAgencia" name="txtBancoAgencia" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lblNumeroDocumento" name="lblNumeroDocumento" class="lblGeneral">Documento</p>
        <input type="text" id="txtNumeroDocumento" name="txtNumeroDocumento" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lblMoeda" name="lblMoeda" class="lblGeneral">$</p>
        <input type="text" id="txtMoeda" name="txtMoeda" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lblVLValor" name="lblVLValor" class="lblGeneral">Valor</p>
        <input type="text" id="txtVLValor" name="txtVLValor" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lblVLSaldoFinanceiro" name="lblVLSaldoFinanceiro" class="lblGeneral">Saldo Financeiro</p>
        <input type="text" id="txtVLSaldoFinanceiro" name="txtVLSaldoFinanceiro" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lblSaldo" name="lblSaldo" class="lblGeneral">Saldo</p>
        <input type="checkbox" id="chkSaldo" name="chkSaldo" class="fldGeneral" title="considera o saldo devedor atualizado ou o saldo devedor?">
        <p id="lblVariacaoValor" name="lblVariacaoValor" class="lblGeneral">Var Valor</p>
        <input type="text" id="txtVariacaoValor" name="txtVariacaoValor" class="fldGeneral" LANGUAGE="javascript" title="Varia��o em Valor. (toler�ncia para cima ou para baixo)"></input>
        <p id="lblVariacaoDias" name="lblVariacaoDias" class="lblGeneral">Var Dias</p>
        <input type="text" id="txtVariacaoDias" name="txtVariacaoDias" class="fldGeneral" LANGUAGE="javascript" title="Varia��o em Dias. (toler�ncia para cima ou para baixo)"></input>
        <p id="lblFormaPagamentoID" name="lblFormaPagamentoID" class="lblGeneral">Forma</p>
        <select id="selFormaPagamentoID" name="selFormaPagamentoID" class="fldGeneral">
<%
    Dim rsData, strSQL

    Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "Select 0 as fldID, '' as fldName , '0' AS Ordem " & _
             "UNION ALL " & _
             "Select DISTINCT a.ItemID as fldID, a.ItemAbreviado as fldName , LTRIM(RTRIM(STR(a.ordem))) AS Ordem " & _
             "From TiposAuxiliares_Itens a WITH(NOLOCK), Financeiro b WITH(NOLOCK) " & _
             "Where a.ItemID = b.FormaPagamentoID AND b.EmpresaID = " & CStr(nEmpresaID) & " AND " & _
             "b.EstadoID NOT IN (3,5,48) " & _
             "Order By Ordem "

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData.EOF)
	    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34) & " "

		'If (CLng(rsData.Fields("fldID").Value) = CLng(nFormaPagamentoID)) Then
			'Response.Write "selected"
		'End If

		Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
	    rsData.MoveNext
	Wend
	
	rsData.Close
	Set rsData = Nothing
	
%>
        </select>
        <p id="lblColaboradorID" name="lblColaboradorID" class="lblGeneral">Colaborador</p>
        <select id="selColaboradorID" name="selColaboradorID" class="fldGeneral">
<%
    Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "Select 0 as fldID, '' as fldName , '0' AS Ordem " & _
             "UNION ALL " & _
             "Select DISTINCT a.PessoaID AS ItemID, a.Fantasia  as fldName, a.Fantasia AS Ordem " & _
             "FROM Pessoas a WITH(NOLOCK), Financeiro b WITH(NOLOCK) " & _
             "WHERE a.PessoaID=b.ProprietarioID AND b.EmpresaID = " & CStr(nEmpresaID) & " AND " & _
             "a.EstadoID = 2 " & _
             "Order By Ordem "

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData.EOF)
	    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
		Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
	    rsData.MoveNext
	Wend
	
	rsData.Close
	Set rsData = Nothing
	
%>
        </select>
        <p id="lblPedidoID" name="lblPedidoID" class="lblGeneral">Pedido</p>
        <input type="text" id="txtPedidoID" name="txtPedidoID" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lblFinanceiroID" name="lblFinanceiroID" class="lblGeneral">Financeiro</p>
        <input type="text" id="txtFinanceiroID" name="txtFinanceiroID" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lblDuplicata" name="lblDuplicata" class="lblGeneral">Duplicata</p>
        <input type="text" id="txtDuplicata" name="txtDuplicata" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lblValor" name="lblValor" class="lblGeneral">Valor</p>
        <input type="text" id="txtValor" name="txtValor" class="fldGeneral" LANGUAGE="javascript" title="Valor a ser aplicado/baixado no financeiro."></input>
        <p id="lblHistoricoPadraoID" name="lblHistoricoPadraoID" class="lblGeneral">Hist�rico Padr�o</p>
        <select id="selHistoricoPadraoID" name="selHistoricoPadraoID" class="fldGeneral">
<%
    Set rsData = Server.CreateObject("ADODB.Recordset")

	strSQL = "SELECT 0 AS Indice, 0 as fldID, SPACE(0) as fldName " & _
			"UNION ALL SELECT DISTINCT 1 AS Indice, b.HistoricoPadraoID as fldID, b.HistoricoPadrao as fldName " & _
			"FROM Financeiro a WITH(NOLOCK), HistoricosPadrao b WITH(NOLOCK) " & _
			"WHERE (a.EmpresaID = " & nEmpresaID & " AND a.TipoFinanceiroID = " & nTipoValorID & " AND " & _
				"a.HistoricoPadraoID = b.HistoricoPadraoID) " & _
			"ORDER BY Indice, fldName"

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData.EOF)
	    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34) & " "
		Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
	    rsData.MoveNext
	Wend
	
	rsData.Close
	Set rsData = Nothing
	
%>
        </select>
        <p id="lblParceiro" name="lblParceiro" class="lblGeneral">Parc</p>
        <input type="checkbox" id="chkParceiro" name="chkParceiro" class="fldGeneral" title="pesquisa pelo parceiro ou pela pessoa?">
        <p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">Pessoa</p>
        <select id="selPessoaID" name="selPessoaID" class="fldGeneral">
<%
	Response.Write "<option value =" & Chr(34) & "0" & Chr(34)
	Response.Write ">" & "&nbsp" & "</option>" & vbCrlf

	If (CLng(nPessoaID) <> 0) Then
		Response.Write "<option value =" & Chr(34) & CStr(nPessoaID) & Chr(34)
		Response.Write " selected>" & CStr(sFantasia) & "</option>" & vbCrlf
	End If
%>
        </select>
        <p id="lblCidade" name="lblCidade" class="lblGeneral">Cidade</p>
        <input type="text" id="txtCidade" name="txtCidade" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lblBairro" name="lblBairro" class="lblGeneral">Bairro</p>
        <input type="text" id="txtBairro" name="txtBairro" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lblFantasia" name="lblFantasia" class="lblGeneral">Fantasia</p>
        <input type="text" id="txtFantasia" name="txtFantasia" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lbldtInicio" name="lbldtInicio" class="lblGeneral">Data in�cio</p>
        <input type="text" id="txtdtInicio" name="txtdtInicio" class="fldGeneral" LANGUAGE="javascript"></input>
        <p id="lbldtFim" name="lbldtFim" class="lblGeneral">Data fim</p>
        <input type="text" id="txtdtFim" name="txtdtFim" class="fldGeneral" LANGUAGE="javascript"></input>        
    </div>    

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
            
	<input type="button" id="btnFillGrid" name="btnFillGrid" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
</body>

</html>
