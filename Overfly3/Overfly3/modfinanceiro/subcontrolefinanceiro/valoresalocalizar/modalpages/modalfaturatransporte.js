/********************************************************************
incluidocumentotransporte.js

Library javascript para o modalfaturatransporte.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport("dsoPesq");
//var dsoIncluiDocumentoTransporte = new CDatatransport("dsoIncluiDocumentoTransporte");
var glb_sMensagem = '';
var dsoPesq = new CDatatransport('dsoPesq');
var dsoIncluiDocumentoTransporte = new CDatatransport('dsoIncluiDocumentoTransporte');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    var elem = document.getElementById('modalfaturatransporteHtmlBody');

    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }


    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no campo apropriado
    if (document.getElementById('txtNumeroFatura').disabled == false)
        txtNumeroFatura.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Fatura de Transporte', 1);

    // ajusta elementos da janela
    var elem;
    var temp;

    // ajusta o divDocumentoTransporte
    elem = window.document.getElementById('divDocumentoTransporte');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;

    }

    // txtNumeroFatura
    elem = window.document.getElementById('txtNumeroFatura');
    elem.maxLength = 15;
    with (elem.style) {
        left = 0;
        top = 16;
        width = (elem.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;

        txtNumeroFatura.onkeypress = verifyNumericEnterNotLinked;
        txtNumeroFatura.setAttribute('thePrecision', 10, 1);
        txtNumeroFatura.setAttribute('theScale', 0, 1);
        txtNumeroFatura.setAttribute('verifyNumPaste', 1);
        txtNumeroFatura.setAttribute('minMax', new Array(1, 9999999999), 1);
        txtNumeroFatura.value = '';
    }

    // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');
    // btnFindPesquisa
    btnFindPesquisa.disabled = true;

    with (elem.style) {
        top = parseInt(document.getElementById('txtNumeroFatura').style.top);
        left = parseInt(document.getElementById('txtNumeroFatura').style.left) + parseInt(document.getElementById('txtNumeroFatura').style.width) + 2;
        width = 80;
        height = 24;
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divDocumentoTransporte').style.top) + parseInt(document.getElementById('divDocumentoTransporte').style.height) + ELEM_GAP;
        width = temp + 249;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 30;
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }

    startGridInterface(fg);



    headerGrid(fg, ['ID', 'Fatura', 'Estado', 'ID', 'Transportadora', 'Data', 'Valor'], []);

    //fg.Redraw = 0;
    //fg.ColWidth(getColIndexByColKey(fg, 'NotaFiscalID')) = 1430;
    fg.Redraw = 2;

}

function txtNumeroFatura_ondigit(ctl) {
    // changeBtnState(ctl.value);
    if (trimStr(txtNumeroFatura.value) != '')
        changeBtnState(txtNumeroFatura.value);

    if (event.keyCode == 13)
        btnFindPesquisa_onclick(btnFindPesquisa);
}

function btnFindPesquisa_onclick(ctl) {
    txtNumeroFatura.value = trimStr(txtNumeroFatura.value);

    changeBtnState(txtNumeroFatura.value);

    if (btnFindPesquisa.disabled)
        return;

    startPesq(txtNumeroFatura.value);
}

//------------------------------------------------------------------
function incluiDocumentoTransporte() {

    var nExisteDocumentoTransporteINF;
    var strPars = new String();
    var nFaturaID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FaturaID'));
    var nValorID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ValorID' + '\'' + '].value');

    strPars = '?nValorID=' + escape(nValorID);
    strPars += '&nFaturaID=' + escape(nFaturaID);

    setConnection(dsoIncluiDocumentoTransporte);

    dsoIncluiDocumentoTransporte.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/incluirfinanceirosfaturatransporte.aspx' + strPars;
    dsoIncluiDocumentoTransporte.ondatasetcomplete = dsoIncluiDocumentoTransporte_DSC;
    dsoIncluiDocumentoTransporte.refresh();

    if (glb_sMensagem != '') {
        if (window.top.overflyGen.Alert(glb_sMensagem) == 0)
            return null;

        glb_sMensagem = '';

        lockControlsInModalWin(false);
    }
}

function dsoIncluiDocumentoTransporte_DSC() {
    // NULL - Verificacao OK
    if (!(dsoIncluiDocumentoTransporte.recordset.BOF && dsoIncluiDocumentoTransporte.recordset.EOF)) {
        dsoIncluiDocumentoTransporte.recordset.MoveFirst();
        if ((dsoIncluiDocumentoTransporte.recordset['Resultado'].value != null) && (dsoIncluiDocumentoTransporte.recordset['Resultado'].value != '')) {
            window.top.overflyGen.Alert(dsoIncluiDocumentoTransporte.recordset['Resultado'].value);
            lockControlsInModalWin(false);
            return null;
        }
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
    }
}

//------------------------------------------------------------------

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        incluiDocumentoTransporte();
        // sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'fg.Row = fg.Rows - 1');

    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);

}

function startPesq(strPesquisa) {
    lockControlsInModalWin(true);

    setConnection(dsoPesq);

    dsoPesq.SQL = 'SELECT FaturaID, ' +
		                    'Numero, ' +
		                    'b.RecursoAbreviado AS Estado, ' +
		                    'a.TransportadoraID, ' +
		                    'c.Fantasia AS Transportadora, ' +
		                    'CONVERT(VARCHAR,a.dtEmissao,103) AS Data, ' +
		                    'a.ValorTotal ' +
                    'FROM FaturasTransporte a WITH(NOLOCK) ' +
	                    'INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID) ' +
	                    'INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.TransportadoraID) ' +
                    'WHERE a.Numero = \'' + txtNumeroFatura.value + '\' ' +
                    'ORDER BY a.Numero';

    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC() {

    startGridInterface(fg);

    fg.FrozenCols = 0;

    headerGrid(fg, ['ID', 'Fatura', 'Est', 'ID', 'Transportadora', 'Data', 'Valor'], []);

    fillGridMask(fg, dsoPesq, ['FaturaID', 'Numero', 'Estado',
                               'TransportadoraID', 'Transportadora', 'Data', 'ValorTotal'],
                              ['', '', '', '', '', '', '']);

    alignColsInGrid(fg, [0, 2, 3, 4, 5, 6]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    lockControlsInModalWin(false);

    window.focus();

    // destrava botao Incluir se tem linhas no grid
    if (fg.Rows > 1) {
        btnOK.disabled = false;
        fg.focus();
    }
    else {
        btnOK.disabled = true;
        txtNumeroFatura.focus();
    }
}
