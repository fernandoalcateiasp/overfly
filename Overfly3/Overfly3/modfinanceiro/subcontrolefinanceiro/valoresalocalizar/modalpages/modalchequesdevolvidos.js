/********************************************************************
modalchequesdevolvidos.js

Library javascript para o modalchequesdevolvidos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveChequeDevolvido()
listar()
paintCellsSpecialyReadOnly()

js_fg_modalchequesdevolvidosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalchequesdevolvidosDblClick(grid, Row, Col)
js_modalchequesdevolvidosKeyPress(KeyAscii)
js_modalchequesdevolvidos_ValidateEdit()
js_modalchequesdevolvidos_BeforeEdit(grid, row, col)
js_modalchequesdevolvidos_AfterEdit(Row, Col)
js_fg_AfterRowColmodalchequesdevolvidos (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalchequesdevolvidosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	glb_bFirstFill = true;
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	btnOK.title = 'Confirmar Financeiros';
	btnOK.value = 'Confirmar';
	secText('Cheques devolvidos - Associar ao original', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK
    btnOK.disabled = true;
	btnFillGrid.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    adjustElementsInForm([['lblValorID','txtValorID',10,1,-10,-10],
						  ['lbldtInicio','txtdtInicio',10,1],
						  ['lbldtFim','txtdtFim',10,1],
						  ['lblFormaPagamentoID','selFormaPagamentoID',7,1],
                          ['lblEmitente','txtEmitente',18,1],
                          ['lblBancoAgencia','txtBancoAgencia',9,1],
                          ['lblNumeroDocumento','txtNumeroDocumento',7,1],
                          ['lblValor','txtValor',11,2,-10]], null, null, true);

    txtValorID.maxLength = 10;
    txtValorID.setAttribute('thePrecision', 10, 1);
    txtValorID.setAttribute('theScale', 0, 1);
    txtValorID.setAttribute('verifyNumPaste', 1);
    txtValorID.onkeypress = txtFields_onKeyPress;
    txtValorID.onkeyup = txtFields_onkeyup;
    txtValorID.setAttribute('minMax', new Array(0, 999999999.99), 1);
    txtValorID.onfocus = selFieldContent;

    txtdtInicio.maxLength = 10;
    txtdtInicio.setAttribute('verifyNumPaste', 1);
    txtdtInicio.onkeypress = txtFields_onKeyPress;
    txtdtInicio.onfocus = selFieldContent;

    txtdtFim.maxLength = 10;
    txtdtFim.setAttribute('verifyNumPaste', 1);
    txtdtFim.onkeypress = txtFields_onKeyPress;
    txtdtFim.onfocus = selFieldContent;

	txtEmitente.onfocus = selFieldContent;
	txtEmitente.maxLength = 20;
	txtEmitente.onkeypress = txtFields_onKeyPress;
	txtEmitente.value = glb_sEmitente;

	txtBancoAgencia.onfocus = selFieldContent;
	txtBancoAgencia.maxLength = 9;
	txtBancoAgencia.onkeypress = txtFields_onKeyPress;
	txtBancoAgencia.value = glb_sBancoAgencia;

	txtNumeroDocumento.onfocus = selFieldContent;
	txtNumeroDocumento.maxLength = 6;
	txtNumeroDocumento.onkeypress = txtFields_onKeyPress;
	txtNumeroDocumento.value = glb_sNumeroDocumento;

    txtValor.setAttribute('thePrecision', 11, 1);
    txtValor.setAttribute('theScale', 2, 1);
    txtValor.setAttribute('verifyNumPaste', 1);
    txtValor.onkeypress = txtFields_onKeyPress;
    txtValor.setAttribute('minMax', new Array(0, 999999999.99), 1);
    txtValor.onfocus = selFieldContent;
    txtValor.maxLength = 11;
	txtValor.value = glb_nValor;

    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
			  btnFillGrid.offsetTop + btnFillGrid.offsetHeight + 2;
        width = txtValor.offsetLeft + txtValor.offsetWidth + ELEM_GAP;    
        height = txtValor.offsetTop + txtValor.offsetHeight;    
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + txtValor.offsetTop;
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		value = 'Associar';
		title = 'Associar Valor';
    }
    
    with (btnFillGrid)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		style.top = btnOK.offsetTop - btnFillGrid.offsetHeight - 5;
		style.left = btnOK.currentStyle.left;
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtFields_onKeyPress()
{
    if ( event.keyCode == 13 )
	{
		refreshParamsAndDataAndShowModalWin(false);
	}
}

function txtFields_onkeyup()
{
	setupBtnsFromGridState();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveChequeDevolvido();
    }
    else if (controlID == 'btnFillGrid')
    {
		refreshParamsAndDataAndShowModalWin(false);
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    lockControlsInModalWin(true);
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    var sValorID = trimStr(txtValorID.value);
    var sVisibility = 'hidden';
    
	if (sValorID == '')	
		sVisibility = 'inherit';	

	lbldtInicio.style.visibility = sVisibility;
	txtdtInicio.style.visibility = sVisibility;
	lbldtFim.style.visibility = sVisibility;
	txtdtFim.style.visibility = sVisibility;
	lblFormaPagamentoID.style.visibility = sVisibility;
	selFormaPagamentoID.style.visibility = sVisibility;
	lblEmitente.style.visibility = sVisibility;
	txtEmitente.style.visibility = sVisibility;
	lblBancoAgencia.style.visibility = sVisibility;
	txtBancoAgencia.style.visibility = sVisibility;
	lblNumeroDocumento.style.visibility = sVisibility;
	txtNumeroDocumento.style.visibility = sVisibility;
	lblValor.style.visibility = sVisibility;
	txtValor.style.visibility = sVisibility;

	btnOK.disabled = true;

	if (bHasRowsInGrid)
	{
		for (i=1; i<fg.Rows; i++)
		{
			if (getCellValueByColKey(fg, 'OK', i) != 0)
			{
				btnOK.disabled = false;
				break;
			}	
		}
	}
	
	btnFillGrid.disabled = false;
}

function verificaData(fldData)
{
	var sData = trimStr(fldData.value);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(fldData.value);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
		
		lockControlsInModalWin(false);        
        window.focus();
		fldData.focus();
		
		return false;
	}
	
	return true;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }
    
	var aGrid = null;
	var i = 0;
	var sFiltro = '';
	var sFiltroData = '';
    var nValorID = trimStr(txtValorID.value);
	var sDataInicio = trimStr(txtdtInicio.value);
	var sDataFim = trimStr(txtdtFim.value);
	var nFormaPagamentoID = selFormaPagamentoID.value;
	var sEmitente = trimStr(txtEmitente.value);
	var sBancoAgencia = trimStr(txtBancoAgencia.value);
	var sNumeroDocumento = trimStr(txtNumeroDocumento.value);
	var nValor = trimStr(txtValor.value);

	if  (!verificaData(sDataInicio))
	{
		return null;
	}
	else if  (!verificaData(sDataFim))
	{
		return null;
	}
	
	if (nValorID == '')
	{
		if (sDataInicio != '')
		{
			if (nFormaPagamentoID == 1032)
				sFiltro += ' AND a.dtApropriacao >= ' + '\'' + normalizeDate_DateTime(sDataInicio, 1) + '\'' + ' ';
			else
				sFiltro += ' AND a.dtEmissao >= ' + '\'' + normalizeDate_DateTime(sDataInicio, 1) + '\'' + ' ';
		}
		if (sDataFim != '')
		{
			if (nFormaPagamentoID == 1032)
				sFiltro += ' AND a.dtApropriacao <= ' + '\'' + normalizeDate_DateTime(sDataFim, 1) + '\'' + ' ';
			else
				sFiltro += ' AND a.dtEmissao <= ' + '\'' + normalizeDate_DateTime(sDataFim, 1) + '\'' + ' ';
		}

		sFiltro += ' AND a.FormaPagamentoID = ' + nFormaPagamentoID + ' ';

		if (nFormaPagamentoID == 1032)	
			sFiltro += ' AND a.Valor = m.Valor ';
		else			
			sFiltro += ' AND a.Valor >= m.Valor ';

		if (sEmitente != '')
			sFiltro += ' AND a.Emitente LIKE ' + '\'' + '%' + sEmitente + '%' + '\'' + ' ';

		if (sBancoAgencia != '')
			sFiltro += ' AND a.BancoAgencia LIKE ' + '\'' + '%' + sBancoAgencia + '%' + '\'' + ' ';

		if (sNumeroDocumento != '')
			sFiltro += ' AND a.NumeroDocumento LIKE ' + '\'' + '%' + sNumeroDocumento + '%' + '\'' + ' ';

		if (nValor != '')
			sFiltro += ' AND a.Valor = ' + parseFloat(nValor) + ' ';
	}
	else
		sFiltro += ' AND a.ValorID = ' + parseInt(nValorID, 10) + ' ';
	
	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;
    
    // zera o grid
    fg.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	dsoGrid.SQL = 'SELECT TOP 100 a.ValorID AS fldID, j.RecursoAbreviado AS Estado, c.ItemAbreviado AS FormaPagamento, ' +
			'l.Fantasia AS Pessoa, a.Documento, a.Emitente, a.dtEmissao AS dtEmissao, a.dtApropriacao AS dtApropriacao, ' +
			'a.BancoAgencia AS BancoAgencia, a.Conta, a.NumeroDocumento AS NumeroDocumento, d.SimboloMoeda AS Moeda, ' +
			'a.Valor AS Valor, dbo.fn_ValorLocalizar_Posicao(a.ValorID, 2) AS SaldoOcorrencia ' +
		'FROM ValoresLocalizar a WITH(NOLOCK) ' +
            'LEFT OUTER JOIN Pessoas l WITH(NOLOCK) ON (a.PessoaID = l.PessoaID) ' +
            'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (a.FormaPagamentoID = c.ItemID) ' +
            'INNER JOIN Conceitos d WITH(NOLOCK) ON (a.MoedaID = d.ConceitoID) ' +
            'INNER JOIN Recursos j WITH(NOLOCK) ON (a.EstadoID = j.RecursoID), ' +
            'ValoresLocalizar m WITH(NOLOCK) ' +
		'WHERE (a.EstadoID IN (41,48) AND a.EmpresaID = ' + glb_aEmpresaData[0] + ' AND ' +
			'm.ValorID = ' + glb_nValorID + ' AND ' +
			'a.ValorID <> ' + glb_nValorID + ' AND ' +
			'((a.FormaPagamentoID = 1033) OR (a.ValorID NOT IN ((SELECT ISNULL(ValorReferenciaID, 0) FROM ValoresLocalizar WITH(NOLOCK) WHERE (EstadoID <> 5))))) ' +
			sFiltro + ') ' +
		'ORDER BY a.ValorID';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
	dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

	if (glb_bFirstFill)
	{
		glb_bFirstFill = false;
		showExtFrame(window, true);
		
		window.focus();
    
		if ( !txtValorID.disabled )
			txtValorID.focus();
	}

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	var aHoldCols = new Array();

	aHoldCols = [];

    headerGrid(fg,['ValorID',
				   'Est',
				   'Forma',
				   'Pessoa',
				   'Documento',
				   'Emitente',
				   'Emiss�o',
				   'Apropria��o',
				   'Banco/Ag�ncia',
				   'Conta',
				   'N�mero',
				   '$',
				   'Valor',
				   'Saldo'], aHoldCols);

    fillGridMask(fg,dsoGrid,['fldID',
							 'Estado',
							 'FormaPagamento',
							 'Pessoa',
							 'Documento',
							 'Emitente',
							 'dtEmissao',
							 'dtApropriacao',
							 'BancoAgencia',
							 'Conta',
							 'NumeroDocumento',
							 'Moeda',
							 'Valor',
							 'SaldoOcorrencia'],
							 ['','','','','','','','','','','','','',''],
							 ['##########','','','','','',dTFormat,dTFormat,'','','','','###,###,##0.00','###,###,##0.00']);
    fg.Redraw = 0;
    
    alignColsInGrid(fg,[0, 12, 13]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
	fg.FrozenCols = 1;

	paintCellsSpecialyReadOnly();
	    
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
		fg.Editable = false;
        window.focus();
        fg.focus();
    }                
    else
    {
		window.focus();
    
		if ( !txtValorID.disabled )
			txtValorID.focus();
    }            

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;
    fg.Redraw = 2;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveChequeDevolvido()
{
	var param2 = new Array();
	param2[0] = getCellValueByColKey(fg, 'fldID', fg.Row);
	
	if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'SaldoOcorrencia')) > 0)
	{
        if ( window.top.overflyGen.Alert('Este VL tem saldo de ocorr�ncia.') == 0 )
            return null;
		
		lockControlsInModalWin(false);        
        window.focus();
        return null;
	}

	sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, dataToReturn());
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalchequesdevolvidosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalchequesdevolvidosDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    glb_PassedOneInDblClick = true;
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    saveChequeDevolvido();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalchequesdevolvidosKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalchequesdevolvidos_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalchequesdevolvidos_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalchequesdevolvidos_AfterEdit(Row, Col)
{
	setupBtnsFromGridState();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalchequesdevolvidos(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return true;

	setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************

/********************************************************************
Monta e retorna array de dados a serem enviados para o sub form que chamou
a modal
********************************************************************/
function dataToReturn() {
    var aData = new Array();

    // id do combo
    aData[0] = 'selValorReferenciaID';
    // id do VL
    aData[1] = getCellValueByColKey(fg, 'fldID', fg.Row); 
    // id do VL
    aData[2] = getCellValueByColKey(fg, 'fldID', fg.Row);

    return aData;
}
