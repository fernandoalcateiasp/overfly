
/********************************************************************
modallocalizarfinanceiro.js

Library javascript para o modallocalizarfinanceiro.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_ValLocTimerInt = null;
var glb_nSaldoValFinanceiro = 0;
var glb_nTaxaVL = 0;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bDevolucao = false;
var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
incluir()
listar()
incluir_DSC()
paintCellsSpecialyReadOnly()

js_fg_modallocalizarfinanceiroBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modallocalizarfinanceiroDblClick(grid, Row, Col)
js_modallocalizarfinanceiroKeyPress(KeyAscii)
js_modallocalizarfinanceiro_ValidateEdit()
js_modallocalizarfinanceiro_BeforeEdit(grid, row, col)
js_modallocalizarfinanceiro_AfterEdit(Row, Col)
js_fg_AfterRowColmodallocalizarfinanceiro (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modallocalizarfinanceiroBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    showExtFrame(window, true);
   
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
    
    window.focus();
    txtVariacaoDias.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Localizar Financeiro', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;
	btnFillGrid.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

adjustElementsInForm([['lblValorID','txtValorID',10,1,-10,-10],
						  ['lblEstado','txtEstado',2,1],
						  ['lblFormaPagamento','txtFormaPagamento',5,1],
						  ['lbldtEmissao','txtdtEmissao',10,1],
						  ['lbldtApropriacao','txtdtApropriacao',10,1],
						  ['lblBancoAgencia','txtBancoAgencia',9,1],
						  ['lblNumeroDocumento','txtNumeroDocumento',9,1],
						  ['lblMoeda','txtMoeda',5,1],
						  ['lblVLValor','txtVLValor',11,1],
						  ['lblVLSaldoFinanceiro','txtVLSaldoFinanceiro',11,1],
						  ['lblSaldo','chkSaldo',3,2,-9],
						  ['lblVariacaoValor','txtVariacaoValor',6,2,-8],
						  ['lblVariacaoDias','txtVariacaoDias',5,2,-8],
						  ['lbldtInicio','txtdtInicio',10,2,-10],
						  ['lbldtFim','txtdtFim',10,2,-2],
						  ['lblFormaPagamentoID','selFormaPagamentoID',7,2,-2],
						  ['lblColaboradorID','selColaboradorID',16,2,-2],
						  ['lblPedidoID','txtPedidoID',10,2,-2],
						  ['lblFinanceiroID','txtFinanceiroID',10,2,-2],
						  ['lblDuplicata','txtDuplicata',8,2,-2],						  
						  ['lblHistoricoPadraoID','selHistoricoPadraoID',25,3,-7],
						  ['lblParceiro','chkParceiro',3,3,-3],
						  ['lblPessoaID','selPessoaID',20,3,-3],						  
						  ['lblCidade','txtCidade',10,3],
						  ['lblBairro','txtBairro',10,3],
						  ['lblFantasia','txtFantasia',12,3],
						  ['lblValor','txtValor',10,4,-10]],null,null,true);
	/*var nSaldo = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,'glb_nSaldo');

	if (nSaldo == '0')
		chkSaldo.checked = false;
	else		
		chkSaldo.checked = true;
	*/
	chkSaldo.checked = false;
	chkSaldo.onclick = setupBtnsFromGridState;
	lblValor.style.color = 'blue';
	txtValorID.disabled = true;
	txtEstado.disabled = true;
	txtFormaPagamento.disabled = true;
	txtdtEmissao.disabled = true;
	txtdtApropriacao.disabled = true;
	txtBancoAgencia.disabled = true;
	txtNumeroDocumento.disabled = true;
	txtMoeda.disabled = true;
	txtVLValor.disabled = true;
	txtVLSaldoFinanceiro.disabled = true;

    chkParceiro.checked = true;
    
    with (txtVariacaoValor)
    {
		onfocus = selFieldContent;
		onkeypress = verifyNumericEnterNotLinked;
		onkeydown = txtFields_onKeyDown;
        setAttribute('thePrecision', 11, 1);
        setAttribute('theScale', 2, 1);
        //setAttribute('minMax', new Array(0, glb_nValor));
        setAttribute('verifyNumPaste', 1);
    }
	txtVariacaoValor.value = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,'glb_nVariacaoValor');

    with (txtVariacaoDias)
	{
		onfocus = selFieldContent;
		onkeypress = verifyNumericEnterNotLinked;
		onkeydown = txtFields_onKeyDown;
        setAttribute('thePrecision', 2, 1);
        setAttribute('theScale', 0, 1);
        setAttribute('minMax', new Array(0, 30));
        setAttribute('verifyNumPaste', 1);
	}
	txtVariacaoDias.value = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,'glb_nVariacaoDias');

	selFormaPagamentoID.onchange = adjustLabelsCombos;
	selColaboradorID.onchange = adjustLabelsCombos;

    with (txtPedidoID)
	{
		onfocus = selFieldContent;
		onkeypress = verifyNumericEnterNotLinked;
		onkeydown = txtFields_onKeyDown;
        setAttribute('thePrecision', 10, 1);
        setAttribute('theScale', 0, 1);
        setAttribute('minMax', new Array(0, 9999999999));
        setAttribute('verifyNumPaste', 1);
	}

    with (txtFinanceiroID)
	{
		onfocus = selFieldContent;
		onkeypress = verifyNumericEnterNotLinked;
		onkeydown = txtFields_onKeyDown;
        setAttribute('thePrecision', 10, 1);
        setAttribute('theScale', 0, 1);
        setAttribute('minMax', new Array(0, 9999999999));
        setAttribute('verifyNumPaste', 1);
	}

    with (txtDuplicata)
	{
		onfocus = selFieldContent;
		onkeydown = txtFields_onKeyDown;
	}

    with (txtValor)
	{
		onfocus = selFieldContent;
		onkeypress = verifyNumericEnterNotLinked;
		onkeydown = txtFields_onKeyDown;
        setAttribute('thePrecision', 11, 1);
        setAttribute('theScale', 2, 1);
        setAttribute('minMax', new Array(0, 999999999.99));
        setAttribute('verifyNumPaste', 1);
	}

	selHistoricoPadraoID.onchange = selHistoricoPadraoID_onchange;
	selPessoaID.onchange = selPessoaID_onchange;

    with (txtCidade)
	{
		onfocus = selFieldContent;
		maxLength = 30;
		onkeydown = txtFields_onKeyDown;
	}

    with (txtBairro)
	{
		onfocus = selFieldContent;
		maxLength = 20;
		onkeydown = txtFields_onKeyDown;
	}

    with (txtFantasia)
	{
		onfocus = selFieldContent;
		maxLength = 20;
		onkeydown = txtFields_onKeyDown;
	}
	
	with (txtdtInicio)
	{
	    maxLength = 10;
	    onkeyup = txtFieldsDatas_onkeyup;	    
	}
	
	with (txtdtFim)
	{
	    maxLength = 10;
	    onkeyup = txtFieldsDatas_onkeyup;	    
	}
	
    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;			 
        width = txtFantasia.offsetLeft + txtFantasia.offsetWidth + ELEM_GAP;    
        height = txtValor.offsetTop + txtValor.offsetHeight;    
        
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // Reposiciona botao OK

    with (btnOK)
    {
		style.width = btnOK.offsetWidth - ELEM_GAP;
		style.top = divControls.offsetTop + txtValor.offsetTop;
		style.left = parseInt(txtValor.currentStyle.left,10) + parseInt(txtValor.currentStyle.width,10) + (2 * ELEM_GAP);
		style.color = 'blue';
		value = 'Incluir';
    }    
    
    with (btnFillGrid)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		style.top = divControls.offsetTop + txtValor.offsetTop;
		style.left = parseInt(btnOK.currentStyle.left,10) + parseInt(btnOK.currentStyle.width,10) + ELEM_GAP;
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    var sMotivoDevolucao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[\'MotivoDevolucao\'].value ');
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'EstadoID\'].value ');

	if (((nEstadoID == 1) && (sMotivoDevolucao != null) && (sMotivoDevolucao != '')) || (glb_nProcessoID == 1423))
		glb_bDevolucao = true;

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
    
    selPessoaID_onchange();
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtFields_onKeyDown()
{
    if ( event.keyCode == 13 )
        btn_onclick(btnFillGrid);
}

/********************************************************************
Considera campos data ou campo variacao data
********************************************************************/
function txtFieldsDatas_onkeyup()
{
    if ((txtdtInicio.value != '') || (txtdtFim.value != ''))
        txtVariacaoDias.value = '';
    else     
        txtVariacaoDias.value = '0';
}
/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        incluir();
    }
    else if (controlID == 'btnFillGrid')
    {
		listar();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
		sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,'glb_nSaldo = ' + '\'' + (chkSaldo.checked ? '1' : '0') + '\'' + ';');
		sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,'glb_nVariacaoValor = ' + '\'' + trimStr(txtVariacaoValor.value) + '\'' + ';');
		sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,'glb_nVariacaoDias = ' + '\'' + trimStr(txtVariacaoDias.value) + '\'' + ';');

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false );
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    //lockControlsInModalWin(true);
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // preenche o grid da janela modal
    fillGridData(fromServer);
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var bHasRowsInGrid = fg.Rows > 1;
	var nSaldo = 0;
	var nValor = 0;
    
    btnOK.disabled = !(bHasRowsInGrid);
	txtValor.disabled = !(bHasRowsInGrid);

	if (bHasRowsInGrid)
	{
		if (!glb_bDevolucao)
		{
			if (chkSaldo.checked)
				nSaldo = parseFloat(replaceStr(getCellValueByColKey(fg, 'SaldoAtualizado', fg.Row), ',', '.'));
			else
				nSaldo = parseFloat(replaceStr(getCellValueByColKey(fg, 'SaldoDevedor', fg.Row), ',', '.'));
		}
		else
			nSaldo = parseFloat(replaceStr(getCellValueByColKey(fg, 'TotalAmortizacao', fg.Row), ',', '.'));
		
		if (nSaldo > glb_nSaldoValFinanceiro)
			nValor = glb_nSaldoValFinanceiro;
		else		
			nValor = nSaldo;
	}
	
	if (nValor > 0)
		txtValor.value = nValor;
	else		
		txtValor.value = '';

   // txtValor.setAttribute('minMax', new Array(0, nValor));

	btnFillGrid.disabled = false;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData(bFirtLoad)
{
    if (!verificaData(txtdtInicio.value))
    {
		setupBtnsFromGridState();
		lockControlsInModalWin(false);        
        window.focus();
		return null;
	}
	
	if (!verificaData(txtdtFim.value))
	{
		setupBtnsFromGridState();
		lockControlsInModalWin(false);        
        window.focus();
		return null;
    }
	
	if (glb_ValLocTimerInt != null)
    {
        window.clearInterval(glb_ValLocTimerInt);
        glb_ValLocTimerInt = null;
    }
    
    var nTop = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');
    var sSQL = '';
	var aGrid = null;
	var i = 0;
	var sdtBalancete = '';
	var sSaldo = '';
	var sdtInicio = dateFormatToSearch(txtdtInicio.value);
	var sdtFim = dateFormatToSearch(txtdtFim.value);
	var sFiltrodtVencimento = '';
	var sFiltroVariacaoValor = '';
	var sFiltroVariacaoDias = '';
	var nFormaPagamentoID = 0;
	var nColaboradorID = 0;
	var nHistoricoPadraoID = 0;
	var sParceiro = '';
	var nPessoaID = 0;
	var sFiltro = '';
	var bVerifica = false;
	var sFiltroDevolucao = '';
	var sFiltroCheque = ''; 
	var sFiltroWhere = '';
	var sSaldoSelect = '';
	var sMensagemErro = '';
	
	if (parseInt(glb_nFormaPagamentoID, 10) == 1032)
		sdtBalancete = glb_sdtApropriacao;
	else
		sdtBalancete = glb_sdtEmissao;

    nFormaPagamentoID = parseInt(selFormaPagamentoID.value, 10);
	if (nFormaPagamentoID > 0)
		sFiltro += ' AND a.FormaPagamentoID = ' + nFormaPagamentoID + ' ';

	nColaboradorID = parseInt(selColaboradorID.value, 10);
	if (nColaboradorID > 0)
		sFiltro += ' AND a.ProprietarioID = ' + nColaboradorID + ' ';

	txtPedidoID.value = trimStr(txtPedidoID.value);
	if (txtPedidoID.value != '')
		sFiltro += ' AND a.PedidoID = ' + parseInt(txtPedidoID.value, 10) + ' ';

	txtFinanceiroID.value = trimStr(txtFinanceiroID.value);
	if (txtFinanceiroID.value != '')
		sFiltro += ' AND a.FinanceiroID = ' + parseInt(txtFinanceiroID.value, 10) + ' ';

	txtDuplicata.value = trimStr(txtDuplicata.value);
	if (txtDuplicata.value != '')
		sFiltro += ' AND a.Duplicata LIKE ' + '\'' + '%' + txtDuplicata.value + '%' + '\'' + ' ';

	nHistoricoPadraoID = parseInt(selHistoricoPadraoID.value, 10);
	if (nHistoricoPadraoID > 0)
		sFiltro += ' AND a.HistoricoPadraoID = ' + nHistoricoPadraoID + ' ';

	if (chkParceiro.checked)
		sParceiro = 'a.ParceiroID';
	else
		sParceiro = 'a.PessoaID';

	nPessoaID = parseInt(selPessoaID.value, 10);
	if (nPessoaID > 0)
		sFiltro += ' AND c.PessoaID = ' + nPessoaID + ' ';
	else
	{
		txtCidade.value = trimStr(txtCidade.value);
		if (txtCidade.value != '')
			sFiltro += ' AND g.Localidade LIKE ' + '\'' + '%' + txtCidade.value + '%' + '\'' + ' ';

		txtBairro.value = trimStr(txtBairro.value);
		if (txtBairro.value != '')
			sFiltro += ' AND d.Bairro LIKE ' + '\'' + '%' + txtBairro.value + '%' + '\'' + ' ';

		txtFantasia.value = trimStr(txtFantasia.value);
		if (txtFantasia.value != '')
			sFiltro += ' AND c.Fantasia LIKE ' + '\'' + '%' + txtFantasia.value + '%' + '\'' + ' ';
	}
	
	if ((sdtInicio != '') && (sdtFim != ''))
	    sFiltrodtVencimento = ' AND (a.dtVencimento BETWEEN \'' + sdtInicio + '\' AND \'' + sdtFim+ '\') ';
	else if ((sdtInicio != '') && (sdtFim == ''))
	    sFiltrodtVencimento = ' AND (a.dtVencimento >= \'' + sdtInicio + '\') ';    
    else if ((sdtInicio == '') && (sdtFim != ''))
	    sFiltrodtVencimento = ' AND (a.dtVencimento <= \'' + sdtFim + '\') ';    	    
	
    txtVariacaoValor.value = trimStr(txtVariacaoValor.value);
    
	if (txtVariacaoValor.value != '')
	{
	    if (chkSaldo.checked)
		{
	        sFiltroVariacaoValor = ' AND (dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, ' + '\'' + sdtBalancete + '\'' + ', NULL, NULL)  BETWEEN ' +
			    ' ( dbo.fn_ValorLocalizar_Posicao(' + glb_nValorID + ', 4) - ' + txtVariacaoValor.value + ') AND ' +
			    ' ( dbo.fn_ValorLocalizar_Posicao(' + glb_nValorID + ', 4) + ' + txtVariacaoValor.value + ')) ';
	    }
	    else
	    {
            sFiltroVariacaoValor = ' AND (a.SaldoDevedor  BETWEEN ' +
			    ' ( dbo.fn_ValorLocalizar_Posicao(' + glb_nValorID + ', 4) - ' + txtVariacaoValor.value + ') AND ' +
			    ' ( dbo.fn_ValorLocalizar_Posicao(' + glb_nValorID + ', 4) + ' + txtVariacaoValor.value + ')) ';
	    }
	}
	else
		bVerifica = true;
      
	txtVariacaoDias.value = trimStr(txtVariacaoDias.value);
	if (txtVariacaoDias.value != '')
		sFiltroVariacaoDias = ' AND (a.dtVencimento BETWEEN ' +
						'DATEADD (dd, - ' + txtVariacaoDias.value + ', ' + '\'' + sdtBalancete + '\'' + ') AND ' +
						'DATEADD (dd,   ' + txtVariacaoDias.value + ', ' + '\'' + sdtBalancete + '\'' + ')) ';
	else if (bVerifica)
	{
		if ( (txtPedidoID.value == '') && (txtFinanceiroID.value == '') && (txtDuplicata.value == '') && 
			 (nPessoaID == 0) && (txtFantasia.value == '') )
		{
			if ( window.top.overflyGen.Alert('Especificar ao menos Pedido, Financeiro, Duplicata, Pessoa ou Fantasia') == 0 )
			    return null;

			return null;					
		}
	}

	if (glb_bDevolucao)
		sFiltroDevolucao = ' AND dbo.fn_Financeiro_Totais(a.FinanceiroID, GETDATE(), 22) > 0 ';

    //sFiltroCheque = ' AND a.TipoFinanceiroID = 1001 AND a.EstadoID <> 48 ';

    if (glb_nProcessoID == 1423 && glb_nTipoValorID == 1001)
        sFiltroCheque = ' AND a.TipoFinanceiroID = 1002 AND a.EstadoID = 48 ';
    else
        sFiltroCheque = ' AND a.TipoFinanceiroID = ' + glb_nTipoValorID + ' AND a.EstadoID <> 48 ';        

    if (glb_nProcessoID == 1423 && glb_nTipoValorID == 1001 && txtFinanceiroID.value != '')
        sFiltroWhere = ' a.FinanceiroID = ' + parseInt(txtFinanceiroID.value, 10) + ' ';
    else
    {
        sFiltroWhere = '(a.EstadoID NOT IN (3, 5) AND ' +
			        'dbo.fn_ValorLocalizar_Posicao(' + glb_nValorID + ', 4) > 0 AND ' +
			        '(a.FinanceiroID NOT IN ((SELECT FinanceiroID FROM ValoresLocalizar_Financeiros WITH(NOLOCK) ' + 
			                                    'WHERE (ValorID = ' + glb_nValorID + ' AND dbo.fn_ValorLocalizarFinanceiro_Posicao(ValFinanceiroID, 2) > 0)))) ' +
					sFiltroVariacaoValor + sFiltroVariacaoDias + sFiltroDevolucao + sFiltro + sFiltrodtVencimento + sFiltroCheque + ') ';
    }
	
	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;
    
    // zera o grid
    fg.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    sSQL = 'SELECT 0 AS Indice, a.ValorID AS VLValorID, b.RecursoAbreviado AS VLEstado, ' +
				'c.ItemAbreviado AS VLFormaPagamento, CONVERT(VARCHAR(10), a.dtEmissao, ' + DATE_SQL_PARAM + ') AS VLdtEmissao, ' +
				'CONVERT(VARCHAR(10), a.dtApropriacao, ' + DATE_SQL_PARAM + ') AS VLdtApropriacao, a.BancoAgencia AS VLBancoAgencia, a.NumeroDocumento AS VLNumeroDocumento, ' +
				'd.SimboloMoeda AS VLMoeda, a.Valor AS VLValor, dbo.fn_ValorLocalizar_Posicao(a.ValorID, 4) AS VLSaldoFinanceiro, ' +
				'NULL AS Ordem, NULL AS FormaPagamentoID, NULL AS FormaPagamento, NULL AS Moeda, NULL AS Valor, ' +
				'NULL AS SaldoDevedor, ' +
				'NULL AS SaldoAtualizado, ' + 
				'NULL AS TotalAmortizacao, ' +
				'NULL AS FinanceiroID, NULL AS Estado, NULL AS PrazoPagamento, ' +
				'NULL AS dtEmissao, NULL AS dtVencimento, NULL AS Duplicata, NULL AS PedidoID, NULL AS Fantasia, ' +
				'NULL AS Bairro, NULL AS Cidade, NULL AS UF, NULL AS Pais, ' +
				'NULL AS Colaborador ' +
				'FROM ValoresLocalizar a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), Conceitos d WITH(NOLOCK) ' +
				'WHERE (a.ValorID = ' + glb_nValorID + ' AND a.EstadoID = b.RecursoID AND a.FormaPagamentoID = c.ItemID AND a.MoedaID = d.ConceitoID) ';

    if (!bFirtLoad)
    {
        sSaldoSelect = 'dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, ' + '\'' + sdtBalancete + '\'' + ', NULL, NULL) AS SaldoDevedor, ' +
                       'dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, ' + '\'' + sdtBalancete + '\'' + ', NULL, NULL) AS SaldoAtualizado, ';

        if (glb_nProcessoID == 1427)
        {
            glb_nTaxaVL = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TaxaMoedaImportacao\'].value ');

            if ((glb_nTaxaVL == 0) || (glb_nTaxaVL == null))
            {
                if (window.top.overflyGen.Alert('VL de cambio precisa ter a Taxa Moeda Importa��o preenchida.') == 0)
                    return null;

                lockControlsInModalWin(false);

                return null;
            }
            else
            {
                sSaldoSelect = /*'CONVERT(NUMERIC(11, 2), (((dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, ' + '\'' + sdtBalancete + '\'' + ', NULL, NULL) / ' +
                                    'ISNULL(ISNULL(n.TaxaMoeda, a.TaxaMoeda), 1)) + ' +
                                        'ISNULL((SELECT ((ISNULL(SUM(aa.ValorOcorrencia), 0) / ISNULL(ISNULL(n.TaxaMoeda, a.TaxaMoeda), 1)) - ' +
                                                '(SUM(ISNULL(aa.ValorOcorrencia, 0) / ISNULL(cc.TaxaMoedaImportacao, ' + glb_nTaxaVL + '))))' +
                                            'FROM Financeiro_Ocorrencias aa WITH(NOLOCK) ' +
                                                'LEFT JOIN ValoresLocalizar_Financeiros bb WITH(NOLOCK) ON (bb.ValFinanceiroID = aa.ValFinanceiroID) ' +
                                                'LEFT JOIN ValoresLocalizar cc WITH(NOLOCK) ON (cc.ValorID = bb.ValorID) ' +
                                            'WHERE ((aa.FinanceiroID = a.FinanceiroID) AND (aa.TipoOcorrenciaID = 1055) AND ' + 
                                                '((SELECT COUNT(1) FROM Financeiro_Ocorrencias bb WITH(NOLOCK) WHERE ' +
                                                    '((bb.FinanceiroID = aa.FinanceiroID) AND (bb.TipoOcorrenciaID = 1064))) = 0))), 0)) * ' + glb_nTaxaVL + ')) AS SaldoDevedor, ' +*/
                               'dbo.fn_FinanceiroCambio_VL(' + parseInt(txtFinanceiroID.value, 10) + ', ' + glb_nValorID + ', ' + glb_nTaxaVL + ', ' + '\'' + sdtBalancete + '\'' + '), ' +
                               'CONVERT(NUMERIC(11, 2), (((dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, ' + '\'' + sdtBalancete + '\'' + ', NULL, NULL) / ' +
                                    'ISNULL(ISNULL(n.TaxaMoeda, a.TaxaMoeda), 1)) + ' +
                                        'ISNULL((SELECT ((ISNULL(SUM(aa.ValorOcorrencia), 0) / ISNULL(ISNULL(n.TaxaMoeda, a.TaxaMoeda), 1)) - ' +
                                                '(SUM(ISNULL(aa.ValorOcorrencia, 0) / ISNULL(cc.TaxaMoedaImportacao, ' + glb_nTaxaVL + '))))' +
                                            'FROM Financeiro_Ocorrencias aa WITH(NOLOCK) ' +
                                                'LEFT JOIN ValoresLocalizar_Financeiros bb WITH(NOLOCK) ON (bb.ValFinanceiroID = aa.ValFinanceiroID) ' +
                                                'LEFT JOIN ValoresLocalizar cc WITH(NOLOCK) ON (cc.ValorID = bb.ValorID) ' +
                                            'WHERE ((aa.FinanceiroID = a.FinanceiroID) AND (aa.TipoOcorrenciaID = 1055) AND ' +
                                                '((SELECT COUNT(1) FROM Financeiro_Ocorrencias bb WITH(NOLOCK) ' +
                                                    'WHERE ((bb.FinanceiroID = aa.FinanceiroID) AND (bb.TipoOcorrenciaID = 1064))) = 0))), 0)) * ' + glb_nTaxaVL + ')) AS SaldoAtualizado, ';
            }
        }

        sSQL += 'UNION ALL SELECT TOP ' + nTop + ' 1 AS Indice, NULL AS VLValorID, NULL AS VLEstado, ' +
				'NULL AS VLFormaPagamento, NULL AS VLdtEmissao, ' +
				'NULL AS VLdtApropriacao, NULL AS VLBancoAgencia, NULL AS VLNumeroDocumento, ' +
				'NULL AS VLMoeda, NULL AS VLValor, NULL AS VLSaldoFinanceiro, ' +
				'h.Ordem AS Ordem, h.ItemID AS FormaPagamentoID, h.ItemAbreviado AS FormaPagamento, i.SimboloMoeda AS Moeda, a.Valor AS Valor, ' +
                sSaldoSelect +
				'dbo.fn_Financeiro_Totais(a.FinanceiroID, GETDATE(), 22) AS TotalAmortizacao, ' +
				'a.FinanceiroID AS FinanceiroID, b.RecursoAbreviado AS Estado, a.PrazoPagamento AS PrazoPagamento, ' +
				'a.dtEmissao AS dtEmissao, a.dtVencimento AS dtVencimento, a.Duplicata AS Duplicata, a.PedidoID AS PedidoID, c.Fantasia AS Fantasia, ' +
				'd.Bairro AS Bairro, g.Localidade AS Cidade, f.CodigoLocalidade2 AS UF, e.CodigoLocalidade2 AS Pais, ' +
				'j.Fantasia AS Colaborador ' +
			'FROM Financeiro a WITH(NOLOCK) ' +
			        'INNER JOIN Recursos b WITH(NOLOCK) ON a.EstadoID = b.RecursoID ' +
			        'INNER JOIN Pessoas c WITH(NOLOCK) ON ' + sParceiro + ' = c.PessoaID  ' +
			        'LEFT JOIN Pessoas_Enderecos d WITH(NOLOCK) ON (c.PessoaID = d.PessoaID AND d.Ordem=1) ' +
			        'LEFT JOIN Localidades e WITH(NOLOCK) ON d.PaisID = e.LocalidadeID ' + 
			        'LEFT JOIN Localidades f WITH(NOLOCK) ON d.UFID = f.LocalidadeID ' + 
			        'LEFT JOIN Localidades g WITH(NOLOCK) ON d.CidadeID = g.LocalidadeID ' +
			        'INNER JOIN TiposAuxiliares_Itens h WITH(NOLOCK) ON a.FormaPagamentoID = h.ItemID ' +
			        'INNER JOIN Conceitos i WITH(NOLOCK) ON a.MoedaID = i.ConceitoID ' +
			        'INNER JOIN Pessoas j WITH(NOLOCK) ON a.ProprietarioID = j.PessoaID ' +
			        'INNER JOIN fn_Matriz_Filial_tbl(' + glb_nEmpresaID + ') k ON a.EmpresaID = k.EmpresaID ' +
					'LEFT OUTER JOIN Pedidos l WITH(NOLOCK) ON (l.PedidoID = a.PedidoID) ' +
                    'LEFT OUTER JOIN Importacao_Pedidos m WITH(NOLOCK) ON (m.PedidoID = l.PedidoID) ' +
                    'LEFT OUTER JOIN Importacao n WITH(NOLOCK) ON (n.ImportacaoID = m.ImportacaoID) ' +
			'WHERE ' + sFiltroWhere +
			' ORDER BY Indice, Ordem, Moeda, Valor, SaldoDevedor, SaldoAtualizado, FinanceiroID';
    }			 
    
    dsoGrid.SQL = sSQL;

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
    try
    {
		dsoGrid.Refresh();
	}	
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Filtro inv�lido.') == 0 )
            return null;

		setupBtnsFromGridState();
		lockControlsInModalWin(false);        
        window.focus();
	}
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

	txtValorID.value = '';
	txtEstado.value = '';
	txtFormaPagamento.value = '';
	txtdtEmissao.value = '';
	txtdtApropriacao.value = '';
	txtBancoAgencia.value = '';
	txtNumeroDocumento.value = '';
	txtMoeda.value = '';
	txtVLValor.value = '';
	txtVLSaldoFinanceiro.value = '';
	glb_nSaldoValFinanceiro = 0;

	if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF))
	{
		dsoGrid.recordset.setFilter('Indice = 0');
		dsoGrid.recordset.MoveFirst();

		if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF))
		{
		    if (dsoGrid.recordset['VLValorID'].value  != null)
		        txtValorID.value = dsoGrid.recordset['VLValorID'].value ;
		    if (dsoGrid.recordset['VLEstado'].value  != null)
		        txtEstado.value =  dsoGrid.recordset['VLEstado'].value ;
		    if (dsoGrid.recordset['VLFormaPagamento'].value  != null)
		        txtFormaPagamento.value =  dsoGrid.recordset['VLFormaPagamento'].value ;
		    if (dsoGrid.recordset['VLdtEmissao'].value  != null)
		        txtdtEmissao.value =  dsoGrid.recordset['VLdtEmissao'].value ;
		    if (dsoGrid.recordset['VLdtApropriacao'].value  != null)
		        txtdtApropriacao.value =  dsoGrid.recordset['VLdtApropriacao'].value ;
		    if (dsoGrid.recordset['VLBancoAgencia'].value  != null)
		        txtBancoAgencia.value =  dsoGrid.recordset['VLBancoAgencia'].value ;
		    if (dsoGrid.recordset['VLNumeroDocumento'].value  != null)
		        txtNumeroDocumento.value =  dsoGrid.recordset['VLNumeroDocumento'].value ;
		    if (dsoGrid.recordset['VLMoeda'].value  != null)
		        txtMoeda.value =  dsoGrid.recordset['VLMoeda'].value ;
		    if (dsoGrid.recordset['VLValor'].value  != null)
		        txtVLValor.value =  dsoGrid.recordset['VLValor'].value ;
		    if (dsoGrid.recordset['VLSaldoFinanceiro'].value  != null)
			{
		        txtVLSaldoFinanceiro.value =  dsoGrid.recordset['VLSaldoFinanceiro'].value ;
		        glb_nSaldoValFinanceiro =  dsoGrid.recordset['VLSaldoFinanceiro'].value ;
			}					
		}

		dsoGrid.recordset.setFilter('Indice = 1');
		 
		if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF))
			dsoGrid.recordset.MoveFirst();
	}

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
    var bTempValue;
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	var aHoldCols = null;

	if (!glb_bDevolucao)
		aHoldCols = [5];
    else
		aHoldCols = [];

    headerGrid(fg,['Forma',
                   '$',
                   'Valor',
                   'Saldo',
                   'Saldo Atual',
                   'Total Amort',
                   'Financ',
                   'E',
                   'Prazo',
                   'Emiss�o',
                   'Vencimento',
                   'Duplicata',
                   'Pedido',
                   'Fantasia',
                   'Bairro',
                   'Cidade',
                   'UF',
				   'Pa�s',
                   'Colaborador'], aHoldCols);

    fillGridMask(fg,dsoGrid,['FormaPagamento',
                             'Moeda',
                             'Valor',
                             'SaldoDevedor',
							 'SaldoAtualizado',
							 'TotalAmortizacao',
                             'FinanceiroID',
                             'Estado',
                             'PrazoPagamento',
                             'dtEmissao',
                             'dtVencimento',
                             'Duplicata',
                             'PedidoID',
                             'Fantasia',
                             'Bairro',
                             'Cidade',
                             'UF',
                             'Pais',
                             'Colaborador'],
                             ['','','','','','','','','','','','','','','','','','',''],
                             ['','','###,###,###,###.00','###,###,###,###.00','###,###,###,###.00','###,###,###,###.00','','','',dTFormat,dTFormat,'','','','','','','','']);

	if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF))
	{
	    dsoGrid.recordset.setFilter('');

		// move o cursor do dso para o primeiro registro
		dsoGrid.recordset.MoveFirst();
	}

    alignColsInGrid(fg,[2, 3, 4, 5, 6, 8, 12]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
	fg.FrozenCols = 3;

	paintCellsSpecialyReadOnly();
	    
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;
    fg.Redraw = 2;
    
    window.focus();
    txtVariacaoValor.focus();

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Inclui
********************************************************************/
function incluir()
{
	var strPars = new String();

	var nValor = 0;
    
    txtValor.value = trimStr(txtValor.value);

	if (txtValor.value != '')
		nValor = parseFloat(replaceStr(txtValor.value, ',', '.'));
	
	if (nValor == 0)
	{
        if ( window.top.overflyGen.Alert('Valor inv�lido.') == 0 )
			return null;
	}		
	else
	{
		lockControlsInModalWin(true);
    
		strPars = '?nValorID=' + escape(glb_nValorID);
		strPars += '&nFinanceiroID=' + escape(getCellValueByColKey(fg, 'FinanceiroID', fg.Row));
		strPars += '&nValor=' + escape(nValor);
		strPars += '&nUserID=' + escape(glb_nUserID);
		strPars += '&nOK=' + escape(glb_nOK);
    
		dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/incluirfinanceiro.aspx' + strPars;
		dsoGrava.ondatasetcomplete = incluir_DSC;
		dsoGrava.refresh();
	}
}

function listar()
{    
	refreshParamsAndDataAndShowModalWin(false);
}

function adjustLabelsCombos()
{
    setLabelOfControl(lblFormaPagamentoID, selFormaPagamentoID.value);
    setLabelOfControl(lblColaboradorID, selColaboradorID.value);
    setLabelOfControl(lblHistoricoPadraoID, selHistoricoPadraoID.value);
    setLabelOfControl(lblPessoaID, selPessoaID.value);
}

function selPessoaID_onchange()
{

	var sShow = 'inherit';
	
	if (parseInt(selPessoaID.value, 10) != 0)
		sShow = 'hidden';				
	
	lblCidade.style.visibility = sShow;
	txtCidade.style.visibility = sShow;
	lblBairro.style.visibility = sShow;
	txtBairro.style.visibility = sShow;
	lblFantasia.style.visibility = sShow;
	txtFantasia.style.visibility = sShow;
	
	adjustLabelsCombos();
}

function selHistoricoPadraoID_onchange()
{
	adjustLabelsCombos();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function incluir_DSC()
{
    lockControlsInModalWin(false);

	if ( !(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF))
	{
		dsoGrava.recordset.MoveFirst();
		if ((dsoGrava.recordset['Mensagem'].value != null) &&
			 (dsoGrava.recordset['Mensagem'].value != ''))
		{
		    if (window.top.overflyGen.Alert(dsoGrava.recordset['Mensagem'].value) == 0)
				return null;
		}
	}

	glb_ValLocTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

/********************************************************************
Verifica��o da data
********************************************************************/
function verificaData(sData)
{
	var sData = trimStr(sData);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(sData);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
		return false;
	}
	return true;
}


// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modallocalizarfinanceiroBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modallocalizarfinanceiroDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    glb_PassedOneInDblClick = true;

	if (fg.Rows > 1)
		incluir();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallocalizarfinanceiroKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallocalizarfinanceiro_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallocalizarfinanceiro_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallocalizarfinanceiro_AfterEdit(Row, Col)
{
	setupBtnsFromGridState();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodallocalizarfinanceiro(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return true;

	setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************
