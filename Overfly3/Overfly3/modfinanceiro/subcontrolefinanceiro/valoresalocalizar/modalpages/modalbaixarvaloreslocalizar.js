/********************************************************************
modalbaixarvaloreslocalizar.js

Library javascript para o modalbaixarvaloreslocalizar.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bWindowLoading = true;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_bFill = true;
var glb_nFinanceiroGeradoID = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
var dsoGrid = new CDatatransport('dsoGrid');
var dsoCmbPessoas = new CDatatransport('dsoCmbPessoas');
var dsoCmbHPData = new CDatatransport('dsoCmbHPData');
var dsoCmbGrid01 = new CDatatransport('dsoCmbGrid01');
var dsoCurrData = new CDatatransport('dsoCurrData');
var dsoTaxaAtualizacao = new CDatatransport('dsoTaxaAtualizacao');
var dsoGrava = new CDatatransport('dsoGrava');
/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalbaixarvaloreslocalizarBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalbaixarvaloreslocalizarDblClick(grid, Row, Col)
js_modalbaixarvaloreslocalizarKeyPress(KeyAscii)
js_modalbaixarvaloreslocalizar_ValidateEdit()
js_modalbaixarvaloreslocalizar_BeforeEdit(grid, row, col)
js_modalbaixarvaloreslocalizar_AfterEdit(Row, Col)
js_fg_AfterRowColmodalbaixarvaloreslocalizar (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalbaixarvaloreslocalizarBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // Clonar financeiro
    if (glb_nFinanceiroID != 0)
		clonarFinanceiro();
	else	
		loadDsoCmbHPData();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Baixar Valores a Localizar', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK
    btnOK.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    
    adjustElementsInForm([['lblFormaPagamentoID','selFormaPagamentoID',7,1,-10,-10],
						  ['lblValorID','txtValorID',10,1],
						  ['lbldtInicio','txtdtInicio',10,1],
						  ['lbldtFim','txtdtFim',10,1],
						  ['lblPessoaID','selPessoaID',18,1],
						  ['lblSaldoMaximo','txtSaldoMaximo',11,1],
						  ['lblEhEstorno','chkEhEstorno',3,2, -10],
						  ['lblEhImporte','chkEhImporte',3,2],
						  ['lblHistoricoPadraoID','selHistoricoPadraoID',25,2]], null, null, true);

    txtValorID.onkeydown = execPesq_OnKey;
    txtValorID.maxLength = 20;
    
	selPessoaID.onchange = selCmbs_onchange;
	selPessoaID.disabled = true;

    txtdtInicio.onkeydown = execPesq_OnKey;
    txtdtInicio.maxLength = 10;
	
	selFormaPagamentoID.selectedIndex = 0;
	selFormaPagamentoID.onchange = selCmbs_onchange;
	
    txtdtFim.maxLength = 10;

    txtSaldoMaximo.onkeypress = verifyNumericEnterNotLinked;
    txtSaldoMaximo.onkeydown = execPesq_OnKey;
    txtSaldoMaximo.setAttribute('thePrecision', 11, 1);
    txtSaldoMaximo.setAttribute('theScale', 2, 1);
    txtSaldoMaximo.setAttribute('verifyNumPaste', 1);
    txtSaldoMaximo.setAttribute('minMax', new Array(1, 111111111.11), 1);

	chkEhEstorno.onclick = chkEhEstorno_onclick;
	chkEhImporte.onclick = chkEhImporte_onclick;
	
	selHistoricoPadraoID.selectedIndex = -1;
	selHistoricoPadraoID.onchange = selHistoricoPadraoID_onchange;

	adjustLabelsCombos();
    
    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = txtSaldoMaximo.offsetLeft + txtSaldoMaximo.offsetWidth + ELEM_GAP;    
        height = selHistoricoPadraoID.offsetTop + selHistoricoPadraoID.offsetHeight;    
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    with (btnFillGrid)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		style.top = divControls.offsetTop + txtSaldoMaximo.offsetTop - 4;
		style.left = divControls.offsetLeft + txtSaldoMaximo.offsetLeft + txtSaldoMaximo.offsetWidth + ELEM_GAP;
    }
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + selHistoricoPadraoID.offsetTop - 4;
		style.left = divControls.offsetLeft + selHistoricoPadraoID.offsetLeft + selHistoricoPadraoID.offsetWidth + ELEM_GAP;
		value = 'Financeiros';
		title = 'Gerar financeiros?';
    }

	if (glb_nFinanceiroID != 0)
	{
		btnOK.value = 'Incluir';
		btnOK.title = 'Incluir Valores a Localizar?';
	}

    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function execPesq_OnKey()
{
    if ( event.keyCode == 13 )
	{
		fg.Rows = 1;
		// ajusta estado dos botoes
		setupBtnsFromGridState();
		
		listar();
	}
	else
	{
		fg.Rows = 1;
		setupBtnsFromGridState();
	}	
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		var sMsg = '';
		
		if (glb_nFinanceiroID == 0)
			sMsg = 'Gerar financeiro?';
		else			
			sMsg = 'Incluir Valores a Localizar?';

		var _retMsg = window.top.overflyGen.Confirm(sMsg);

		if ( _retMsg == 0 )
		    return null;
		else if ( _retMsg == 1 )
		{
			saveDataInGrid();
		}             
		else
		    return null;
    }
    else if (controlID == 'btnFillGrid')
    {
		listar();
    }
    // codigo privado desta janela
    else
    {        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    lockControlsInModalWin(true);
    
    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    
	btnOK.disabled = true;

	if (glb_nFinanceiroID == 0)
	{
		chkEhEstorno.disabled = false;
		chkEhImporte.disabled = false;
		selHistoricoPadraoID.disabled = false;
	}
	else
	{
		chkEhEstorno.disabled = true;
		chkEhImporte.disabled = true;
		selHistoricoPadraoID.disabled = true;
	}

	if (selHistoricoPadraoID.selectedIndex >= 0)
	{
		if (bHasRowsInGrid)
		{
			for (i=1; i<fg.Rows; i++)
			{
				if (getCellValueByColKey(fg, 'OK', i) != 0)
				{
					btnOK.disabled = false;
					break;
				}	
			}
		}
	}
}

function loadDsoCmbHPData()
{
	lockControlsInModalWin(true);
	glb_nDSOs = 4;
    // parametrizacao do dso dsoGrid
    
    var sFiltro = '';
    var sFiltroUsoCotidiano = '';
    var nTipoLancamentoID;
    var bEhEstorno = chkEhEstorno.checked;

	if (chkEhImporte.checked)
		nTipoLancamentoID = 1573;
	else
	{
		if (((glb_nTipoValorID==1001) && (!bEhEstorno)) || ((glb_nTipoValorID==1002) && (bEhEstorno)))
			nTipoLancamentoID = 1571;
		else
			nTipoLancamentoID = 1572;
	}
	
	sFiltro = ' AND b.TipoLancamentoID = ' + nTipoLancamentoID + ' ';

	if (((glb_nTipoValorID==1001) && (!bEhEstorno)) || ((glb_nTipoValorID==1002) && (bEhEstorno)))
		sFiltro += ' AND a.TipoLancamentoID = 1571 ';
	else
		sFiltro += ' AND a.TipoLancamentoID = 1572 ';

	setConnection(dsoCmbHPData);
	dsoCmbHPData.SQL = 'SELECT DISTINCT a.HistoricoPadraoID as fldID, a.HistoricoPadrao as fldName, ' +
					'dbo.fn_HistoricoPadrao_TipoDetalhamento(a.HistoricoPadraoID, ' + nTipoLancamentoID + ', 1543) AS Imposto ' +
                    'FROM HistoricosPadrao a WITH(NOLOCK), HistoricosPadrao_Contas b WITH(NOLOCK) ' +
                    'WHERE (a.EstadoID = 2 AND a.HistoricoPadraoID = b.HistoricoPadraoID AND (b.Pedido = 1 OR b.Financeiro = 1) ' + sFiltro + sFiltroUsoCotidiano + ') ' +
                    'ORDER BY fldName';

    dsoCmbHPData.ondatasetcomplete = loadDsoCmbHPData_DSC;
    
	dsoCmbHPData.Refresh();
	
	setConnection(dsoCmbGrid01);
	dsoCmbGrid01.SQL = 'SELECT ItemID AS fldID, ItemAbreviado AS fldName ' +
			 'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
			 'WHERE (EstadoID = 2 AND TipoID=804) ' +
			 'ORDER BY Ordem';

    dsoCmbGrid01.ondatasetcomplete = loadDsoCmbHPData_DSC;
	dsoCmbGrid01.Refresh();
	
	dsoCurrData.URL = SYS_PAGESURLROOT + '/serversidegenEx/currdate.aspx?currDateFormat='+escape(DATE_FORMAT);
	dsoCurrData.ondatasetcomplete = loadDsoCmbHPData_DSC;
	dsoCurrData.Refresh();
	
	setConnection(dsoTaxaAtualizacao);
	dsoTaxaAtualizacao.SQL = 'SELECT Taxas.TaxaAtualizacao ' +
		'FROM RelacoesPesRec Empresas WITH(NOLOCK), RelacoesPesRec_Financ Taxas WITH(NOLOCK) ' +
		'WHERE (Empresas.SujeitoID = ' + glb_nEmpresaID + ' AND Empresas.ObjetoID = 999 AND  ' +
			'Empresas.TipoRelacaoID = 12 AND Empresas.RelacaoID = Taxas.RelacaoID AND ' +
			'Taxas.MoedaID = 647)';

	dsoTaxaAtualizacao.ondatasetcomplete = loadDsoCmbHPData_DSC;
	dsoTaxaAtualizacao.Refresh();
}

function loadDsoCmbHPData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs==0)
	{
		clearComboEx(['selHistoricoPadraoID']);

		if (! ((dsoCmbHPData.recordset.BOF)&&(dsoCmbHPData.recordset.EOF)) )
		{
			dsoCmbHPData.recordset.MoveFirst();
			while (! dsoCmbHPData.recordset.EOF )
			{
			    optionStr = dsoCmbHPData.recordset['fldName'].value;
			    optionValue = dsoCmbHPData.recordset['fldID'].value;
				var oOption = document.createElement("OPTION");
				oOption.text = optionStr;
				oOption.value = optionValue;
				oOption.bShowImposto = dsoCmbHPData.recordset['Imposto'].value;
				selHistoricoPadraoID.add(oOption);
				dsoCmbHPData.recordset.MoveNext();
			}
		}
		
		if (! (dsoCurrData.recordset.BOF && dsoCurrData.recordset.EOF) )
		{
			if (trimStr(txtdtInicio.value)=='')
			    txtdtInicio.value = dsoCurrData.recordset['currDate'].value;
			
			if (trimStr(txtdtFim.value)=='')
			    txtdtFim.value = dsoCurrData.recordset['currDate'].value;
		}
		
		selHistoricoPadraoID.selectedIndex = -1;
		
		// Ajusta o Historico Padrao
		if (glb_nFinanceiroID !=0)
		{
			nHistoricoPadraoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				'dsoSup01.recordset['+ '\'' + 'HistoricoPadraoID' + '\'' + '].value');

			selOptByValueInSelect(getHtmlId(), 'selHistoricoPadraoID', nHistoricoPadraoID);
			
		}
		
		selHistoricoPadraoID.disabled = (selHistoricoPadraoID.options.length == 0);
		adjustLabelsCombos();
		setupBtnsFromGridState();
		
		dsoTaxaAtualizacao_DSC();

		lockControlsInModalWin(false);

		if (glb_bWindowLoading)
		{
			// lockBtnLupa(btnFindPessoa, false);
			glb_bWindowLoading = false;
			showExtFrame(window, true);
			window.focus();

			if ( !txtValorID.disabled )
				txtValorID.focus();
				
			if (glb_nFinanceiroID != 0)
				listar();
		}
	}
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

	var aGrid = null;
	var i = 0;
	var nValor = trimStr(txtSaldoMaximo.value);
	var sValor = '';
	var sFormaPagamentoID = '';
	var sData = '';
	var sValorID = '';
	var sSaldo = '';

	if (trimStr(txtdtInicio.value) != '')
	{
		if (selFormaPagamentoID.value==0)
			sData += ' AND (CASE WHEN a.FormaPagamentoID = 1032 THEN a.dtApropriacao ELSE a.dtEmissao END) >= ' + '\'' + putDateInMMDDYYYY2(txtdtInicio.value) + '\'' + ' ';
		else if (selFormaPagamentoID.value==1032)
			sData += ' AND a.dtApropriacao >= ' + '\'' + putDateInMMDDYYYY2(txtdtInicio.value) + '\'' + ' ';
		else
			sData += ' AND a.dtEmissao >= ' + '\'' + putDateInMMDDYYYY2(txtdtInicio.value) + '\'' + ' ';
	}

	if (trimStr(txtdtFim.value) != '')
	{
		if (selFormaPagamentoID.value==0)
			sData += ' AND (CASE WHEN a.FormaPagamentoID = 1032 THEN a.dtApropriacao ELSE a.dtEmissao END) <= ' + '\'' + putDateInMMDDYYYY2(txtdtFim.value) + '\'' + ' ';
		else if (selFormaPagamentoID.value==1032)
			sData += ' AND a.dtApropriacao <= ' + '\'' + putDateInMMDDYYYY2(txtdtFim.value) + '\'' + ' ';
		else
			sData += ' AND a.dtEmissao <= ' + '\'' + putDateInMMDDYYYY2(txtdtFim.value) + '\'' + ' ';
	}
	
	if (trimStr(txtValorID.value) != '')
		sValorID = ' AND a.ValorID = ' + txtValorID.value;

	if (trimStr(txtSaldoMaximo.value) != '')
		if (!isNaN(trimStr(txtSaldoMaximo.value)))
			sSaldo = ' AND dbo.fn_ValorLocalizar_Posicao(a.ValorID, 4) <= ' + parseFloat(txtSaldoMaximo.value);

	if (selFormaPagamentoID.value != 0)
		sFormaPagamentoID = ' AND a.FormaPagamentoID = ' + selFormaPagamentoID.value + ' ';

	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;
    
    // zera o grid
    fg.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	dsoGrid.SQL = 'SELECT b.ItemAbreviado, c.SimboloMoeda, a.Valor, dbo.fn_ValorLocalizar_Posicao(a.ValorID, 4) AS Saldo,' +
		'a.ValorID, d.RecursoAbreviado, CONVERT(BIT, 0) AS OK, a.dtEmissao, a.dtApropriacao, a.Emitente, e.Fantasia ' +
	'FROM ValoresLocalizar a WITH(NOLOCK) ' +
        'LEFT OUTER JOIN Pessoas e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID) ' +
        'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (a.FormaPagamentoID = b.ItemID) ' +
        'INNER JOIN Conceitos c WITH(NOLOCK) ON (a.MoedaID = c.ConceitoID) ' +
        'INNER JOIN Recursos d WITH(NOLOCK) ON (a.EstadoID = d.RecursoID) ' +
	'WHERE (a.EmpresaID = ' + glb_nEmpresaID + ' AND a.TipoValorID = ' + glb_nTipoValorID + ' AND ' +
		'a.EstadoID = 41 AND dbo.fn_ValorLocalizar_Posicao(a.ValorID, 4) > 0) ' + 
		 sValor + sFormaPagamentoID + sData + sValorID + sSaldo + ' ' +
	'ORDER BY b.ItemAbreviado, dbo.fn_ValorLocalizar_Posicao(a.ValorID, 4)';
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
	dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
    
    headerGrid(fg,['Forma',
				   '$',
				   'Valor',
				   'Saldo',
				   'Valor ID',
				   'E',
				   'OK',
				   'Emiss�o',
				   'Apropria��o',
				   'Emitente',
				   'Pessoa'], []);

    fillGridMask(fg,dsoGrid,['ItemAbreviado*',
							 'SimboloMoeda*',
							 'Valor*',
							 'Saldo*',
							 'ValorID*',
							 'RecursoAbreviado*',
							 'OK',
							 'dtEmissao*',
							 'dtApropriacao*',
							 'Emitente*',
							 'Fantasia*'],
							 ['','','999999999.99','999999999.99','9999999999','','','99/99/9999','99/99/9999','',''],
							 ['','','###,###,##0.00','###,###,##0.00','##########','','',dTFormat,dTFormat,'','']);
	fg.Redraw = 0;
    gridHasTotalLine(fg, 'Total', 0XC0C0C0, null, true, [[getColIndexByColKey(fg, 'ValorID*'),'####0','C'],
		[getColIndexByColKey(fg, 'Valor*'),'###,###,##0.00','S'],
		[getColIndexByColKey(fg, 'Saldo*'),'###,###,##0.00','S']]);
		
	paintCellsSpecialyReadOnly();
	
	if (fg.Rows >1)
	{
		fg.TextMatrix(1, getColIndexByColKey(fg, 'ItemAbreviado*')) = 'Totais';
		fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorID*')) = 0;
		fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor*')) = 0;
		fg.TextMatrix(1, getColIndexByColKey(fg, 'Saldo*')) = 0;
	}

    fg.Redraw = 0;
    
    alignColsInGrid(fg,[2,3,4]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
  	fg.MergeCells = 4;
	fg.MergeCol(-1) = true;

	fg.ExplorerBar = 5;
	
	fg.Redraw = 2;
	
	if ((fg.Rows > 1) && (fg.Cols > 1))
		fg.Col = getColIndexByColKey(fg, 'OK');
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
		fg.Editable = true;
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    fg.Redraw = 2;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

function gridTotais()
{
	var nQtd = 0;
	var nValor = 0;
	var nSaldo = 0;
	var aRetVal = [0,0,0];
	var i;
	
	for (i=2; i<fg.Rows; i++)
	{
		
		if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0)
		{
			nQtd++;
			nValor += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor*'));
			nSaldo += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Saldo*'));
		}
	}
	
	aRetVal = [nQtd,nValor,nSaldo];
	
	return aRetVal;
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var sEventos = '';
	var bGeraFinanceiro = false;
	var nBytesAcum = 0;
	glb_aSendDataToServer = [];
	var nRelPesContaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset.Fields['RelPesContaID'].value");

    lockControlsInModalWin(true);
    strPars = '';
    for (i=2; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'OK', i) == 0)
			continue;
			
		nBytesAcum=strPars.length;
		bGeraFinanceiro = true;
		
		if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
		{
			nBytesAcum = 0;
			if (strPars != '')
			{
				glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
				strPars = '';
			}
			else
			{
				glb_nFinanceiroGeradoID = glb_nFinanceiroID;
			}
			
			//strPars += '?nFinanceiroID=' + escape(glb_nFinanceiroGeradoID);
			strPars += '&nTipoValorID=' + escape(glb_nTipoValorID);
			strPars += '&nEmpresaID=' + escape(glb_nEmpresaID);
			strPars += '&nHistoricoPadraoID=' + escape(selHistoricoPadraoID.value);
			strPars += '&nUsuarioID=' + escape(glb_nUserID);
			strPars += '&bEhEstorno=' + escape((chkEhEstorno.checked ? 1 : 0));
			strPars += '&bEhImporte=' + escape((chkEhImporte.checked ? 1 : 0));
			strPars += '&nSaldoTotal=' + escape(gridTotais()[2]);
			strPars += '&nRelPesContaID=' + escape(nRelPesContaID);
		}

		strPars += '&nValorID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorID*')));
		strPars += '&nSaldo=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Saldo*')));
	}
	
	if (bGeraFinanceiro)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

	if (!bGeraFinanceiro)
	{
		if ( window.top.overflyGen.Alert('Preencha algum valor!') == 0 )
			return null;

		lockControlsInModalWin(false);
		return null;
	}
	else
	{
		glb_nPointToaSendDataToServer = 0;
		sendDataToServer();
	}
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/baixarvaloreslocalizar.aspx' + 
				'?nFinanceiroID=' + escape(glb_nFinanceiroGeradoID) + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
			saveDataInGrid_DSC();
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
	}
}

function sendDataToServer_DSC()
{
	if (!((dsoGrava.recordset.BOF)&&(dsoGrava.recordset.EOF)))
	{
	    if ((dsoGrava.recordset['FinanceiroID'].value != null) && (dsoGrava.recordset['FinanceiroID'].value != 0))
	        glb_nFinanceiroGeradoID = dsoGrava.recordset['FinanceiroID'].value;
	}

	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

function clonarFinanceiro()
{
	lockControlsInModalWin(true);
	
	var nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'dsoSup01.recordset['+ '\'' + 'PessoaID' + '\'' + '].value');
	var sFantasia = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'selPessoaID.options[selPessoaID.selectedIndex].innerText');
	var bEhEstorno = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'dsoSup01.recordset['+ '\'' + 'EhEstorno' + '\'' + '].value');
	var bEhImporte = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'dsoSup01.recordset['+ '\'' + 'EhImporte' + '\'' + '].value');

	clearComboEx(['selPessoaID']);

	// Ajusta o combo de Pessoa
	var oOption = document.createElement("OPTION");
	oOption.text = sFantasia;
	oOption.value = nPessoaID;
	selPessoaID.add(oOption);
	selPessoaID.selectedIndex = 0;
	
	// Ajusta o EhEstorno
	chkEhEstorno.checked = (bEhEstorno == true);

	// Ajusta o EhImporte
	chkEhImporte.checked = (bEhImporte == true);
	
	chkEhImporte_onclick();
}

function chkEhEstorno_onclick()
{
	setupBtnsFromGridState();	
	lockControlsInModalWin(true);
	loadDsoCmbHPData();
}

function chkEhImporte_onclick()
{
	setupBtnsFromGridState();	
	lockControlsInModalWin(true);
	loadDsoCmbHPData();
}

function selCmbs_onchange()
{
	var bShow = false;
	
	fg.Rows = 1;
	
	setupBtnsFromGridState();
	adjustLabelsCombos();
	return null;
}

function selHistoricoPadraoID_onchange()
{
	setupBtnsFromGridState();
	adjustLabelsCombos();
	return null;
}

function loadTaxaAtualizacao()
{
	lockControlsInModalWin(true);
	setConnection(dsoTaxaAtualizacao);
	dsoTaxaAtualizacao.SQL = 'SELECT Taxas.TaxaAtualizacao ' +
		'FROM RelacoesPesRec Empresas WITH(NOLOCK), RelacoesPesRec_Financ Taxas WITH(NOLOCK)  ' +
		'WHERE (Empresas.SujeitoID = ' + glb_nEmpresaID + ' AND Empresas.ObjetoID = 999 AND  ' +
			'Empresas.TipoRelacaoID = 12 AND Empresas.RelacaoID = Taxas.RelacaoID AND ' +
			'Taxas.MoedaID = 647)';

	dsoTaxaAtualizacao.ondatasetcomplete = dsoTaxaAtualizacao_DSC;
	dsoTaxaAtualizacao.Refresh();
}

function dsoTaxaAtualizacao_DSC()
{
	lockControlsInModalWin(false);
}

function adjustLabelsCombos()
{
    setLabelOfControl(lblPessoaID, selPessoaID.value);
    setLabelOfControl(lblHistoricoPadraoID, selHistoricoPadraoID.value);
    setLabelOfControl(lblFormaPagamentoID, selFormaPagamentoID.value);
}

function listar()
{
	var sMsg = '';
	var oControl = null;
	
	txtdtInicio.value = trimStr(txtdtInicio.value);
	
	if (chkDataEx(txtdtInicio.value) != true)
	{
		oControl = txtdtInicio;
		sMsg += 'Data de in�cio\n';
	}

	if (chkDataEx(txtdtFim.value) != true)
	{
		oControl = txtdtFim;
		sMsg += 'Data de fim\n';
	}
	
	if (sMsg=='')
	{
		if (putDateInYYYYMMDD(txtdtFim.value) < putDateInYYYYMMDD(txtdtInicio.value))
		{
			oControl = txtdtFim;
			sMsg = 'A data de vencimento deve ser maior ou igual a data de emiss�o.';
		}
	}
	else
		sMsg = 'Preencha corretamente os campos:\n\n' + sMsg;
		
	if (sMsg != '')
	{
		if ( window.top.overflyGen.Alert(sMsg) == 0 )
            return null;
            
		lockControlsInModalWin(false);
		window.focus();
		if (oControl.disabled == false)
			oControl.focus();
			
		return null;			
	}
	
	refreshParamsAndDataAndShowModalWin(false);
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    var _retMsg = false;
    lockControlsInModalWin(false);
	
	if (glb_sCaller	 == 'PL')
	{
	    if ((dsoGrava.recordset['FinanceiroID'].value != null) && (dsoGrava.recordset['FinanceiroID'].value != 0))
		{
	        _retMsg = window.top.overflyGen.Confirm('Gerado o Financeiro: ' + dsoGrava.recordset.Fields["FinanceiroID"].value + '\n' +
														'Detalhar Financeiro?');
		    if ( _retMsg == 0 )
		        return null;
		    else if ( _retMsg == 1 )
		    {
		        sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(glb_nEmpresaID, dsoGrava.recordset.Fields["FinanceiroID"].value));
		    }             
		
		}
	    else if ( window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0 )
			return null;			
	}
	else
	{
	    if ((dsoGrava.recordset['Resultado'].value != null) && (dsoGrava.recordset['Resultado'].value != ''))
		{
	        if ( window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0 )
				return null;			
		}
		else
		{
			var sMsg = '';
			sMsg = 'Financeiro gerado.';
			
			if ( window.top.overflyGen.Alert(sMsg) == 0 )
				return null;
		}
	}
	
	fg.Rows = 1;
	
	setupBtnsFromGridState();
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalbaixarvaloreslocalizarBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalbaixarvaloreslocalizarDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    // trap so colunas editaveis
    if ( getColIndexByColKey(grid, 'OK') != Col)
        return true;
        
    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);
    
    glb_PassedOneInDblClick = true;
    
    for ( i=1; i<grid.Rows; i++ )
    {
        if (glb_bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    
    glb_bFill = !glb_bFill;
    
    var aTotais = gridTotais();
	fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorID*')) = aTotais[0];
	fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor*')) = aTotais[1];
	fg.TextMatrix(1, getColIndexByColKey(fg, 'Saldo*')) = aTotais[2];

    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalbaixarvaloreslocalizarKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalbaixarvaloreslocalizar_ValidateEdit()
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalbaixarvaloreslocalizar_BeforeEdit(grid, row, col)
{
	if (fg.Row == 1)
		fg.Row = 2;

    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalbaixarvaloreslocalizar_AfterEdit(Row, Col)
{
	var nColValor = getColIndexByColKey(fg, 'OK');
	
    if (Col == nColValor)
    {
        fg.TextMatrix(Row, nColValor) = treatNumericCell(fg.TextMatrix(Row, nColValor));
        var aTotais = gridTotais();
		fg.TextMatrix(1, getColIndexByColKey(fg, 'ItemAbreviado*')) = 'Totais';
		fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorID*')) = aTotais[0];
		fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor*')) = aTotais[1];
		fg.TextMatrix(1, getColIndexByColKey(fg, 'Saldo*')) = aTotais[2];
    }
	
	setupBtnsFromGridState();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalbaixarvaloreslocalizar(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return true;

	setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************
