/********************************************************************
modalestornavl.js

Library javascript para o modalestornavl.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bWindowLoading = true;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_bFill = true;
var glb_nFinanceiroGeradoID = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoEstorna = new CDatatransport('dsoEstorna');
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalestornavlBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    if (glb_bWindowLoading) {
        glb_bWindowLoading = false;
        showExtFrame(window, true);
        window.focus();
    }
    
    window.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Estornar Valor a Localizar', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // desabilita o botao OK
    btnOK.disabled = false;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls

    adjustElementsInForm([['lblMotivo', 'txtMotivo', 41, 1, -10, -10],
                          ['btnOK', 'btn', 30, 2]], null, null, true);

    // Ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP + 20;
        width = txtMotivo.offsetLeft + txtMotivo.offsetWidth + ELEM_GAP;
        height = 100;
    }

    // Reposiciona botao OK
    with (btnOK) {
        style.top = divControls.offsetTop + txtMotivo.offsetTop + 40;
        //style.left = divControls.offsetLeft + txtMotivo.offsetLeft + txtMotivo.offsetWidth - 80;        
        value = 'Estornar';
        title = 'Estornar Valor a Localizar';
        style.width = 80;
        style.height = 24;
        style.left = (modWidth / 2) - (parseInt(btnOK.currentStyle.width, 10) / 2);
    }

    // Esconde botao cancel
    with (btnCanc) {
        style.visibility = 'hidden';
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        var sMsg = '';
        var sTexto = '';
        sTexto = txtMotivo.value;

        if (sTexto == null || sTexto == '')
            sMsg = 'Digite um Motivo para o Estorno';

        if (sMsg == '')
            estornaVL();
        else {
            if (window.top.overflyGen.Alert(sMsg) == 0)
                return null;

            lockControlsInModalWin(false);
            window.focus();
            return null;
        }
        window.focus();
    }
    // codigo privado desta janela
    else {
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
- null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    // @@ atualizar interface aqui
    lockControlsInModalWin(true);
}

/********************************************************************
Funcao do programador - chama o asp para estornar o vl
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function estornaVL() {
    lockControlsInModalWin(true);

    var strPars = new String();
    var nCounter = 0;
    var sErro_ = '';
    var sErro__ = '';

    strPars = '';

    var nUsuarioID = 0;
    var nProcessoID = 0;
    var sObservacao = '';

    nUsuarioID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    sObservacao = txtMotivo.value;

    strPars += '?nProcessoID=' + escape(glb_nRegistroID);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);
    strPars += '&sObservacao=' + escape(sObservacao);

    dsoEstorna.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/estornavalorlocalizar.aspx' + strPars;
    dsoEstorna.ondatasetcomplete = estornaVL_DSC;
    dsoEstorna.refresh();
}

/********************************************************************
Funcao do programador
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function estornaVL_DSC() {
    lockControlsInModalWin(false);
    var nValorID, sResultado;

    if (!(dsoEstorna.recordset.BOF && dsoEstorna.recordset.EOF)) {
        sResultado = dsoEstorna.recordset['MensagemErro'].value;

        if ((sResultado != null) && (sResultado != '')) {
            if (window.top.overflyGen.Alert(sResultado) == 0)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Valor a localizar estornado') == 0);
                sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
        }
    }
}