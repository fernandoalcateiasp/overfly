/********************************************************************
modalfindpessoa.js

Library javascript para o modalfindpessoa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var modPesqrelacoes_Timer = null;

var dsoPesq = new CDatatransport('dsoPesq');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES

NOTA: Ver tambem o arquivo commonrelacoes.js

FUNCOES GERAIS:
window_onload()
setupPage()
txtPesquisa_onKeyPress()
txtPesquisa_ondigit(ctl)
btnFindPesquisa_onclick(ctl)
btn_onclick(ctl)
startPesq(strPesquisa)
dataToReturn()

Eventos de grid particulares desta janela:
fg_ModPesqrelacoesDblClick()
fg_ModPesqrelacoesKeyPress(KeyAscii)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalfindpessoaBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtPesquisa').disabled == false )
        txtPesquisa.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    var sTemp = glb_sLabel;
        
    secText('Selecionar ' + sTemp, 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    
    // ajusta o divPesquisa
    with (divPesquisa.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        height = 40;
    }
    
    with (lblChavePesquisa.style)
    {
        left = 0;
        top = 0;
        width = 13 * FONT_WIDTH;
    }
    
    with (selChavePesquisa.style)
    {
        left = 0;
        top = 16;
        width = 13 * FONT_WIDTH;
    }
    
	// lblPesquisa
    with (lblPesquisa.style)
    {
        left = selChavePesquisa.offsetWidth + ELEM_GAP;
        top = 0;
        width = (txtPesquisa.maxLength + 8) * FONT_WIDTH - 4;
    }

    // txtPesquisa
    txtPesquisa.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (txtPesquisa.style)
    {
        left = selChavePesquisa.offsetWidth + ELEM_GAP;
        top = 16;
        width = (txtPesquisa.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }
    
    txtPesquisa.onkeypress = txtPesquisa_onKeyPress;
    
    // btnFindPesquisa
    btnFindPesquisa.disabled = true;
    with (btnFindPesquisa.style)
    {
        top = parseInt(txtPesquisa.style.top, 10);
        left = parseInt(txtPesquisa.style.left, 10) + parseInt(txtPesquisa.style.width, 10) + 2;
        width = 80;
        height = 24;
    }
    
    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }

    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.style.top, 10) + parseInt(divPesquisa.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.style.width, 10) * 18;
    fg.Redraw = 2;
    
    lblPesquisa.innerText = sTemp;
}

/********************************************************************
Usuario pressionou tecla no campo txtPesquisa
********************************************************************/
function txtPesquisa_onKeyPress()
{
    if ( event.keyCode == 13 )
    {
        lockControlsInModalWin(true);
        modPesqrelacoes_Timer = window.setInterval('btnFindPesquisa_onclick()', 30, 'JavaScript');
    }
}

/********************************************************************
Caracter digitado no campo txtPesquisa
********************************************************************/
function txtPesquisa_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

/********************************************************************
Usuario clicou o botao btnFindPesquisa
********************************************************************/
function btnFindPesquisa_onclick(ctl)
{
    if ( modPesqrelacoes_Timer != null )
    {
        window.clearInterval(modPesqrelacoes_Timer);
        modPesqrelacoes_Timer = null;
    }
    
    txtPesquisa.value = trimStr(txtPesquisa.value);
    
    changeBtnState(txtPesquisa.value);

    if (btnFindPesquisa.disabled)
    {
        lockControlsInModalWin(false);
        return;
    }    
    
    if (selChavePesquisa.value == 1)
    {
		if (! window.top.chkTypeData( 'N', txtPesquisa.value ))
		{
            if ( window.top.overflyGen.Alert('O campo cont�m tipo de dado incompativel.') == 0 )
                return null;

			return null;
		}
	}

    startPesq("'%" + txtPesquisa.value + "%'");
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqrelacoesDblClick()
{
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqrelacoesKeyPress(KeyAscii)
{
    if ( fg.Row < 1 )
        return true;
 
    if ( (KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, dataToReturn() );    
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
}

/********************************************************************
Chamada de pagina asp no servidor para obter dados para preencher grid
********************************************************************/
function startPesq(strPesquisa)
{
    lockControlsInModalWin(true);
    
    var strPars = new String();
    strPars = '?';
    strPars += 'nFormID='  + escape(glb_nFormID);
    strPars += '&sComboID='  + escape(glb_sComboID);
    strPars += '&nEmpresaID='  + escape(glb_nEmpresaID);
    strPars += '&strToFind='+ escape(strPesquisa);
    strPars += '&nKey='+ escape(selChavePesquisa.value);
    
    dsoPesq.URL = SYS_ASPURLROOT + '/serversidegenEx/pesqrelacoes.aspx' + strPars;
	
	dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

/********************************************************************
Retorno de pagina asp no servidor com dados para preencher grid
********************************************************************/
function dsopesq_DSC()
{
    startGridInterface(fg);
    fg.FrozenCols = 0;
    
	headerGrid(fg,['Nome',
				   'ID',
				   'Fantasia',
				   'Documento',
				   'Cidade',
				   'UF',
				   'Pa�s'], []);

	fillGridMask(fg,dsoPesq,['fldCompleteName',
							 'fldID',
							 'fldName',
							 'Documento',
							 'Cidade',
							 'UF',
							 'Pais'],
							 ['','','','','','','']);

    alignColsInGrid(fg,[1]);
                             
    fg.FrozenCols = 1;
    
    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
        btnOK.disabled = false;
    }    
    else
        btnOK.disabled = true;    
}

/********************************************************************
Monta e retorna array de dados a serem enviados para o sub form que chamou
a modal
********************************************************************/
function dataToReturn()
{
    var aData = new Array();

	// id do combo
    aData[0] = glb_sComboID;
    // id da pessoa
    aData[1] = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'fldID'));
    // fantasia da pessoa
    aData[2] = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'fldName'));
    // documento da pessoa
    aData[3] = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Documento'));

    return aData;
}
