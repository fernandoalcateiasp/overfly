/********************************************************************
modalconfirmarfinanceiros.js

Library javascript para o modalconfirmarfinanceiros.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;

var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalconfirmarfinanceirosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalconfirmarfinanceirosDblClick(grid, Row, Col)
js_modalconfirmarfinanceirosKeyPress(KeyAscii)
js_modalconfirmarfinanceiros_ValidateEdit()
js_modalconfirmarfinanceiros_BeforeEdit(grid, row, col)
js_modalconfirmarfinanceiros_AfterEdit(Row, Col)
js_fg_AfterRowColmodalconfirmarfinanceiros (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalconfirmarfinanceirosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	glb_bFirstFill = true;
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	chkConfirmar_onClick(true);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK
    btnOK.disabled = true;
	btnFillGrid.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblConfirmar','chkConfirmar',3,1,-10,-10],
						  ['lblData','txtData',10,1,-10],
						  ['lblFormaPagamentoID','selFormaPagamentoID',7,1],
						  ['lblFiltro','txtFiltro',46,1,5]], null, null, true);

	txtData.onfocus = selFieldContent;
	txtData.maxLength = 10;
	txtData.onkeypress = txtDataFiltro_onKeyPress;
	txtData.value = glb_dCurrDate;

	chkConfirmar.onclick = chkConfirmar_onClick;
	selFormaPagamentoID.onchange = selFormaPagamentoID_onchange;
	adjustLabelsCombos();

	txtFiltro.onfocus = selFieldContent;
	txtFiltro.maxLength = 1000;
	txtFiltro.onkeypress = txtDataFiltro_onKeyPress;
	
    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
			  btnFillGrid.offsetTop + btnFillGrid.offsetHeight + 2;
        width = txtFiltro.offsetLeft + txtFiltro.offsetWidth + ELEM_GAP;    
        height = txtFiltro.offsetTop + txtFiltro.offsetHeight;    
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + txtFiltro.offsetTop;
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		value = 'Confirmar';
		title = 'Confirmar Financeiros';
    }
    
    with (btnFillGrid)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		style.top = btnOK.currentStyle.top;
		style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnFillGrid.currentStyle.width, 10) - 4;
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtDataFiltro_onKeyPress()
{
    if ( event.keyCode == 13 )
	{
		fg.Rows = 1;
		// ajusta estado dos botoes
		setupBtnsFromGridState();
	}
}

function chkConfirmar_onClick(bCarregamento)
{
	if (chkConfirmar.checked)
	{
		btnOK.title = 'Confirmar Financeiros';
		btnOK.value = 'Confirmar';
		secText('Confirmar Financeiros', 1);
	}
	else		
	{
		btnOK.title = 'Ativar Valores a Localizar';
		btnOK.value = 'Ativar';
		secText('Ativar Valores a Localizar', 1);
	}
	if (bCarregamento == null)
	{
		fg.Rows = 1;
		// ajusta estado dos botoes
		setupBtnsFromGridState();
	}
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnFillGrid')
    {
		listar();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    lockControlsInModalWin(true);
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    
	btnOK.disabled = true;

	if (bHasRowsInGrid)
	{
		for (i=1; i<fg.Rows; i++)
		{
			if (getCellValueByColKey(fg, 'OK', i) != 0)
			{
				btnOK.disabled = false;
				break;
			}	
		}
	}
	
	btnFillGrid.disabled = false;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }
    
	var aGrid = null;
	var i = 0;
	var sFiltroValorID = '';
	var sFiltro = trimStr(txtFiltro.value);
	var sData = trimStr(txtData.value);
	var sFiltroData = '';
	var sFiltroFormaPagamento = '';
	
	sFiltro = (sFiltro == '' ? '' : ' AND ' + sFiltro);

	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;
    
	if (glb_sCaller == 'S')
		sFiltroValorID = ' AND a.ValorID=' + 
			sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'ValorID' + '\'' + '].value') +
			' ';

    // zera o grid
    fg.Rows = 1;
    
	if (sData != '')
	{
		sData = normalizeDate_DateTime(sData, 1);

		sFiltroData = ' AND ((CASE a.FormaPagamentoID WHEN 1032 THEN a.dtApropriacao ELSE a.dtEmissao END) <= ' + '\'' + sData + '\'' + ' ) '; 
	}
    else
		sFiltroData = '';

	if (selFormaPagamentoID.value > 0)
		sFiltroFormaPagamento = ' AND a.FormaPagamentoID = ' + selFormaPagamentoID.value + ' ';
	
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	if (chkConfirmar.checked)
	{
	    dsoGrid.SQL = 'SELECT dbo.fn_Pessoa_Fantasia(a.EmpresaID, 0) AS Empresa, a.ValorID AS ValorID, j.RecursoAbreviado AS Estado, c.ItemAbreviado AS FormaPagamento, l.Fantasia AS Pessoa1, a.Documento, a.Emitente, ' +
				'a.dtEmissao AS dtEmissao, a.dtApropriacao AS dtApropriacao, a.BancoAgencia AS BancoAgencia, a.Conta, ' +
				'a.NumeroDocumento AS NumeroDocumento, d.SimboloMoeda AS MoedaValLocalizar, ' +
				'a.Valor AS ValorVL, b.Valor AS Valor, CONVERT(BIT, 0) AS OK, e.FinanceiroID AS FinanceiroID, ' +
				'e.PedidoID, e.Duplicata, h.Fantasia AS Pessoa, f.ItemAbreviado AS FormaPagamentoFinanceiro, ' +
				'e.PrazoPagamento, e.dtVencimento AS dtVencimento, g.SimboloMoeda AS MoedaFinanceiro, ' +
				'e.Valor AS ValorFinanceiro, ' +
				'dbo.fn_Financeiro_Posicao(e.FinanceiroID, 9, NULL, (CASE a.FormaPagamentoID WHEN 1032 THEN a.dtApropriacao ELSE a.dtEmissao END), NULL, NULL) AS Saldo, ' +
				'dbo.fn_Financeiro_Posicao(e.FinanceiroID, 8, NULL, (CASE a.FormaPagamentoID WHEN 1032 THEN a.dtApropriacao ELSE a.dtEmissao END), NULL, NULL) AS SaldoAtual, ' +
				'a.Historico, i.Fantasia AS Colaborador, ' + 
				'b.ValFinanceiroID ' +
			'FROM ValoresLocalizar a WITH(NOLOCK) ' +
                'LEFT OUTER JOIN Pessoas l WITH(NOLOCK) ON (a.PessoaID = l.PessoaID) ' +
                'INNER JOIN ValoresLocalizar_Financeiros b WITH(NOLOCK) ON (a.ValorID = b.ValorID) ' +
                'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (a.FormaPagamentoID = c.ItemID) ' +
                'INNER JOIN Conceitos d WITH(NOLOCK) ON (a.MoedaID = d.ConceitoID) ' +
                'INNER JOIN Financeiro e WITH(NOLOCK) ON (b.FinanceiroID = e.FinanceiroID) ' +
                'INNER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (e.FormaPagamentoID = f.ItemID) ' +
                'INNER JOIN Conceitos g WITH(NOLOCK) ON (e.MoedaID = g.ConceitoID) ' +
                'INNER JOIN Pessoas h WITH(NOLOCK) ON (e.PessoaID=h.PessoaID) ' +
                'INNER JOIN Pessoas i WITH(NOLOCK) ON (e.ProprietarioID = i.PessoaID) ' +
                'INNER JOIN Recursos j WITH(NOLOCK) ON (a.EstadoID = j.RecursoID) ' +
               'WHERE (a.EstadoID IN (51,41,53,56) AND (dbo.fn_Empresa_Matriz(a.EmpresaID, NULL, 1) = ' + glb_aEmpresaData[0] + ') AND b.OK = 0 AND ' +		
				'a.TipoValorID = ' + glb_nTipoValorID + ' AND dbo.fn_ValorLocalizarFinanceiro_Posicao(b.ValFinanceiroID, 2) > 0 ' +
				sFiltroData + sFiltro + sFiltroValorID + sFiltroFormaPagamento + ') ' +
			'ORDER BY a.ValorID';
	}
	else
	{
	    dsoGrid.SQL = 'SELECT dbo.fn_Pessoa_Fantasia(a.EmpresaID, 0) AS Empresa, a.ValorID AS ValorID, j.RecursoAbreviado AS Estado, c.ItemAbreviado AS FormaPagamento, l.Fantasia AS Pessoa1, a.Documento, a.Emitente, ' +
				'a.dtEmissao AS dtEmissao, a.dtApropriacao AS dtApropriacao, a.BancoAgencia AS BancoAgencia, a.Conta, ' +
				'a.NumeroDocumento AS NumeroDocumento, d.SimboloMoeda AS MoedaValLocalizar, ' +
				'a.Valor AS ValorVL, NULL AS Valor, CONVERT(BIT, 0) AS OK, NULL AS FinanceiroID, ' +
				'NULL AS PedidoID, NULL AS Duplicata, NULL AS Pessoa, NULL AS FormaPagamentoFinanceiro, ' +
				'NULL AS PrazoPagamento, NULL AS dtVencimento, NULL AS MoedaFinanceiro, ' +
				'NULL AS ValorFinanceiro, ' +
				'NULL AS Saldo, ' +
				'NULL AS SaldoAtual, ' +
				'NULL AS Colaborador, ' + 
				'a.Historico AS Historico, ' +
				'NULL AS ValFinanceiroID ' +
			'FROM ValoresLocalizar a WITH(NOLOCK) ' +
                'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (a.FormaPagamentoID = c.ItemID) ' +
                'INNER JOIN Conceitos d WITH(NOLOCK) ON (a.MoedaID = d.ConceitoID) ' +
                'INNER JOIN Recursos j WITH(NOLOCK) ON (a.EstadoID = j.RecursoID) ' +
                'LEFT OUTER JOIN Pessoas l WITH(NOLOCK) ON (a.PessoaID = l.PessoaID) ' +
                'INNER JOIN Pessoas m WITH(NOLOCK) ON (m.PessoaID = a.EmpresaID) ' +
			'WHERE (a.EstadoID IN (1) AND (dbo.fn_Empresa_Matriz(a.EmpresaID, NULL, 1) = ' + glb_aEmpresaData[0] + ') AND ' +		
				'a.TipoValorID = ' + glb_nTipoValorID + ' ' +
				sFiltroData + sFiltro + sFiltroValorID + sFiltroFormaPagamento + ') ' +
			'ORDER BY a.ValorID';
	}

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
    try
    {
		dsoGrid.Refresh();
	}	
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Filtro inv�lido.') == 0 )
            return null;

		lockControlsInModalWin(false);        
        window.focus();
		txtFiltro.focus();		
	}
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

	if (glb_bFirstFill)
	{
		glb_bFirstFill = false;
		showExtFrame(window, true);
		
		window.focus();
    
		if ( !txtData.disabled )
			txtData.focus();
	}

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	var aHoldCols = new Array();

	if (chkConfirmar.checked)
		aHoldCols = [11, 26];
	else
		aHoldCols = [15,16,17,18,19,20,21,22,23,24,25,26];

   headerGrid(fg, ['Empresa',
                   'ValorID',
   				   'OK',
				   'E',
				   'For',
				   'Pessoa',
				   'Emitente',
				   'Emiss�o',
				   'Apr',
				   'Bco/Ag',
				   'Conta',
				   'N�m',
   				   'Hist�rico',
				   '$',
				   'Valor VL',
				   'Valor',
				   'Financ',
				   'Pedido',
				   'Duplicata',
				   'Pessoa',
				   'For',
				   'Prazo',
				   'Vencimento',
				   '$',
				   'Valor',
				   'Colaborador',
				   'ValFinanceiroID'], aHoldCols);

   fillGridMask(fg, dsoGrid,['Empresa',
                             'ValorID*',
   							 'OK',
							 'Estado*',
							 'FormaPagamento*',
							 'Pessoa1*',
							 'Emitente*',
							 'dtEmissao*',
							 'dtApropriacao*',
							 'BancoAgencia*',
							 'Conta*',
							 'NumeroDocumento*',
   							 'Historico*',
							 'MoedaValLocalizar*',
							 'ValorVL*',
							 'Valor*',
							 'FinanceiroID*',
							 'PedidoID*',
							 'Duplicata*',
							 'Pessoa*',
							 'FormaPagamentoFinanceiro*',
							 'PrazoPagamento*',
							 'dtVencimento*',
							 'MoedaFinanceiro*',
							 'ValorFinanceiro*',
						     'Colaborador*',
							 'ValFinanceiroID*'],
							 ['','','','','','','','','','','','','','','','','','','','','','','','','',''],
							 ['', '##########', '', '', '', '', '', dTFormat, dTFormat, '', '', '', '', '', '###,###,##0.00', '###,###,##0.00', '##########', '##########', '', '',
							'','#####',dTFormat,'','###,###,##0.00','','']);
    fg.Redraw = 0;
    
    alignColsInGrid(fg,[0, 13, 14, 15, 16, 20, 23]);

	var sColuna = 'Valor*';
	
	if (!chkConfirmar.checked)
		sColuna = 'ValorVL*';

	gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, sColuna),'###,###,###.00','S']]);

	if (fg.Rows > 1)
	{
		fg.TextMatrix(1, getColIndexByColKey(fg, 'FormaPagamento*')) = 0;
		fg.TextMatrix(1, getColIndexByColKey(fg, sColuna)) = 0;
	}

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
	fg.FrozenCols = 2;

	paintCellsSpecialyReadOnly();
	    
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
		fg.Editable = true;
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.Redraw = 2;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nConfirmar = 0;
	var nBytesAcum = 0;

	if (chkConfirmar.checked)
		nConfirmar = 1;
	else
		nConfirmar = 0;

    lockControlsInModalWin(true);
    
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;

    for (i=1; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'OK', i) != 0)
		{
			nBytesAcum=strPars.length;
			
			if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
			{
				nBytesAcum = 0;
				if (strPars != '')
				{
					glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
					strPars = '';
					nDataLen = 0;
				}
				
				strPars += '?nUserID=' + escape(glb_nUserID);
				strPars += '&nConfirmar=' + escape(nConfirmar);
			}

			if (nConfirmar == 1)
				strPars += '&nValFinanceiroID=' + escape(getCellValueByColKey(fg, 'ValFinanceiroID*', i));
			else
				strPars += '&nValorID=' + escape(getCellValueByColKey(fg, 'ValorID*', i));
			
			nDataLen++;				
		}	
	}
	
	if (nDataLen>0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);

	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/confirmarfinanceiros.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
	}
}

function sendDataToServer_DSC()
{
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

function selFormaPagamentoID_onchange()
{
	adjustLabelsCombos();
	fg.Rows = 1;
	// ajusta estado dos botoes
	setupBtnsFromGridState();
}

function adjustLabelsCombos()
{
    setLabelOfControl(lblFormaPagamentoID, selFormaPagamentoID.value);
}
function listar()
{
	if (!verificaData())
		return null;
		
	refreshParamsAndDataAndShowModalWin(false);
}

function verificaData()
{
	var sData = trimStr(txtData.value);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(txtData.value);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        window.focus();
        
        if ( sData != '' )    
			txtData.focus();
		
		return false;
	}
	return true;
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);

	glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalconfirmarfinanceirosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalconfirmarfinanceirosDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    // trap so colunas editaveis
    if ( getColIndexByColKey(grid, 'OK') != Col)
        return true;
        
    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);
    
    glb_PassedOneInDblClick = true;
    
    // limpa coluna se tem um check box checado
    for ( i=1; i<grid.Rows; i++ )
    {
        if ( grid.ValueMatrix(i, Col) != 0 )
        {
            bFill = false;
            break;
        }
    }
    
    for ( i=1; i<grid.Rows; i++ )
    {
        if ( bFill )
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    setTotalColuns();
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalconfirmarfinanceirosKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalconfirmarfinanceiros_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalconfirmarfinanceiros_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalconfirmarfinanceiros_AfterEdit(Row, Col)
{
	setupBtnsFromGridState();
	setTotalColuns();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalconfirmarfinanceiros(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return true;

	setupBtnsFromGridState();
}

function setTotalColuns()
{
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    var nValue = 0;
    var nCounter = 0;

	var sColuna = 'Valor*';
	
	if (!chkConfirmar.checked)
		sColuna = 'ValorVL*';
    
	if (bHasRowsInGrid)
	{
		for (i=2; i<fg.Rows; i++)
		{
			if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) != 0)
			{
				nValue += fg.ValueMatrix(i, getColIndexByColKey(fg, sColuna));
				nCounter++;
			}				
		}
	}

	fg.TextMatrix(1, getColIndexByColKey(fg, sColuna)) = nValue;
	fg.TextMatrix(1, getColIndexByColKey(fg, 'FormaPagamento*')) = nCounter;
}

// FINAL DE EVENTOS DE GRID *****************************************
