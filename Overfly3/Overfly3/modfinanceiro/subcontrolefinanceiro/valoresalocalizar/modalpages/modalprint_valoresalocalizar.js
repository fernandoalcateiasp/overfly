/********************************************************************
modalprint_valoreslocalizar.js

Library javascript para o modalprint.asp
Valores a Localizar
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoEMail = new CDatatransport('dsoEMail');
var dsoLayoutCheque = new CDatatransport('dsoLayoutCheque');
var dsoCheque = new CDatatransport('dsoCheque');
var dsoGravaHistorico = new CDatatransport('dsoGravaHistorico');
var dsoSummaryHeader = new CDatatransport('dsoSummaryHeader');
var dsoSummary = new CDatatransport('dsoSummary');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    linksDivAndReports()
    invertChkBox()
    chkBox_Clicked()
    selReports_Change()
    btnOK_Status()
    btnOK_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    noRights()
    
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

var glb_nDSOs = 0;
var glb_aDataCheque = new Array();
var glb_nNominalIndexInArray = -1;

function _Cheque() {
    this.nTipoCampoID = 0;
    this.nLinha = 0;
    this.nColuna = 0;
    this.nTamanho = 0;
    this.vPrintValue = '';
    this.bNegrito = false;
    this.bCaixaAlta = false;
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    glb_bUseProgressBar = true;
    asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalprintBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // Somente se veio do sup
    if (glb_sCaller == 'S') {
        getDataToCheque();
        return null;
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    window.focus();

    if (selReports.disabled == false)
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();
}


function getDataToCheque() {
    glb_nDSOs = 4;

    var nValorID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ValorID' + '\'' + '].value');

    setConnection(dsoLayoutCheque);
    dsoLayoutCheque.SQL = 'SELECT e.TipoCampoID, e.Linha, e.Coluna, e.Tamanho, e.TamanhoFonte, e.Negrito, e.CaixaAlta ' +
						  'FROM ValoresLocalizar a WITH(NOLOCK), Pessoas b WITH(NOLOCK), Pessoas_Enderecos c WITH(NOLOCK), Bancos d WITH(NOLOCK), Bancos_Cheques e WITH(NOLOCK) ' +
						  'WHERE (a.ValorID = ' + nValorID + ' AND a.EmpresaID = b.PessoaID AND b.PessoaID = c.PessoaID AND c.Ordem = 1 AND ' +
							'c.EndFaturamento = 1 AND LEFT(a.BancoAgencia, 3) = d.Codigo AND c.PaisID = d.PaisID AND d.EstadoID = 2 AND d.BancoID = e.BancoID) ' +
							'ORDER BY e.Linha, e.Coluna';

    dsoLayoutCheque.ondatasetcomplete = showModal_DSC;
    dsoLayoutCheque.Refresh();

    setConnection(dsoCheque);
    dsoCheque.SQL = 'SELECT TOP 1 a.BancoAgencia, ' +
			'ISNULL(a.Conta, SPACE(0)) AS Conta, ' +
			'ISNULL(a.NumeroDocumento, SPACE(0)) AS NumeroCheque, ABS(a.Valor) AS Valor, ' +
			'ISNULL(dbo.fn_Caracter_RemoveEspecial(b.Nome),\'\') AS Pessoa, ISNULL(dbo.fn_Caracter_RemoveEspecial(a.Emitente),\'\') AS Emitente, c.Nome AS Empresa, ' +
			'dbo.fn_Caracter_RemoveEspecial(e.Localidade) AS Localidade, DATEPART(dd, ISNULL(a.dtEmissao, GETDATE())) AS Dia, ' +
			'dbo.fn_Caracter_RemoveEspecial(dbo.fn_Tradutor(DATENAME(mm, ISNULL(a.dtEmissao, GETDATE())), 245, -a.EmpresaID, NULL)) AS Mes, ' +
			'DATEPART(yyyy, ISNULL(a.dtEmissao, GETDATE())) AS Ano, ' +
			'CONVERT(VARCHAR, a.dtApropriacao, ' + DATE_SQL_PARAM + ') AS dtApropriacao,' +
			'ISNULL(dbo.fn_Caracter_RemoveEspecial(f.Banco), SPACE(0)) AS NomeBanco, a.ValorID, ISNULL(dbo.fn_Caracter_RemoveEspecial(a.Historico), SPACE(0)) AS Historico, ' +
			'(CASE WHEN a.dtApropriacao > a.dtEmissao THEN 1 ELSE 0 END) AS BomPara ' +
		'FROM ValoresLocalizar a WITH(NOLOCK) ' +
            'LEFT OUTER JOIN Pessoas b WITH(NOLOCK) ON (a.PessoaID = b.PessoaID) ' +
            'INNER JOIN Pessoas c WITH(NOLOCK) ON (a.EmpresaID = c.PessoaID) ' +
            'INNER JOIN Pessoas_Enderecos d WITH(NOLOCK) ON (c.PessoaID = d.PessoaID) ' +
            'LEFT OUTER JOIN Bancos f WITH(NOLOCK) ON (LEFT(a.BancoAgencia, 3) = f.Codigo AND d.PaisID = f.PaisID AND f.EstadoID = 2) ' +
            'INNER JOIN Localidades e WITH(NOLOCK) ON (d.CidadeID = e.LocalidadeID) ' +
		'WHERE (a.ValorID = ' + nValorID + ' AND d.Ordem = 1 AND d.EndFaturamento = 1)';

    dsoCheque.ondatasetcomplete = showModal_DSC;
    dsoCheque.Refresh();

    setConnection(dsoSummaryHeader);
    dsoSummaryHeader.SQL = 'SELECT dbo.fn_Caracter_RemoveEspecial(b.Nome) AS Pessoa, a.ValorID, ISNULL(CONVERT(VARCHAR(10), a.dtEmissao, ' + DATE_SQL_PARAM + '), SPACE(0)) AS dtEmissao, ' +
			'ISNULL(dbo.fn_Caracter_RemoveEspecial(dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 5)), SPACE(0)) AS Banco, ' +
			'ISNULL(dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4), SPACE(0)) AS Conta, ' +
			'ISNULL(a.NumeroDocumento, SPACE(0)) AS Cheque, ' +
			'ISNULL(dbo.fn_Numero_Formata(a.Valor, 2, 1, 101), SPACE(0)) AS Valor ' +
		'FROM ValoresLocalizar a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
		'WHERE a.ValorID=' + nValorID + ' AND a.PessoaID=b.PessoaID';

    dsoSummaryHeader.ondatasetcomplete = showModal_DSC;
    dsoSummaryHeader.Refresh();

    setConnection(dsoSummary);
    dsoSummary.SQL = 'SELECT ISNULL(a.FinanceiroID,0) AS FinanceiroID, ISNULL(b.PedidoID,0) AS PedidoID,' +
			'ISNULL(b.Duplicata, SPACE(0)) AS Duplicata, ISNULL(CONVERT(VARCHAR(10),b.dtVencimento, ' + DATE_SQL_PARAM + '), SPACE(0)) AS dtVencimento, ' +
			'ISNULL(dbo.fn_Numero_Formata(b.Valor,2,1,' + DATE_SQL_PARAM + '), SPACE(0)) AS OriginalAmmount, ' +
			'ISNULL(dbo.fn_Numero_Formata(dbo.fn_Financeiro_Posicao(b.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL),2,1, ' + DATE_SQL_PARAM + '), SPACE(0)) AS BalanceDue,  ' +
			'ISNULL(dbo.fn_Numero_Formata(a.Valor,2,1,' + DATE_SQL_PARAM + '), SPACE(0)) AS Payment ' +
		'FROM ValoresLocalizar_Financeiros a WITH(NOLOCK), Financeiro b WITH(NOLOCK) ' +
		'WHERE a.ValorID = ' + nValorID + ' AND a.FinanceiroID=b.FinanceiroID ' +
		'ORDER BY a.FinanceiroID';

    dsoSummary.ondatasetcomplete = showModal_DSC;
    dsoSummary.Refresh();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    window.focus();
    if (ctl.id == btnOK.id)
        btnOK.focus();
    else if (ctl.id == btnCanc.id)
        btnCanc.focus();

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        pb_StopProgressBar(true);
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}


/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Relat�rios', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;

    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    with (lblReports.style) {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // ajusta combo de relatorios
    with (selReports.style) {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // ajusta o divRelatorioValoresLocalizar
    with (divRelatorioValoresLocalizar.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divCheque
    with (divCheque.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // O estado do botao btnOK
    btnOK_Status();

    // O usuario tem direito ou nao
    noRights();
}


/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;

    selReports.onchange = selReports_Change;

    // carrega a lista de relatorios
    loadReportsInList();

    // linka os divs nos relatorios
    linkDivsAndReports();

    // ajusta os divs

    // ajusta o divRelatorioValoresLocalizar
    adjustDivRelatorioValoresLocalizar();

    // ajusta o divCheque
    adjustDivCheque();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList() {
    var i;

    if (glb_arrayReports != null) {
        for (i = 0; i < glb_arrayReports.length; i++) {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);

            if (glb_arrayReports[i][2])
                selReports.selectedIndex = i;
        }

        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports() {
    divRelatorioValoresLocalizar.setAttribute('report', 40191, 1);
    divCheque.setAttribute('report', 40192, 1);
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox() {
    var ctl = this.nextSibling;

    if (ctl.disabled == true)
        return;

    ctl.checked = !ctl.checked;

    // O estado do botao btnOK
    btnOK_Status();

    window.focus();
    if (ctl.disabled == false)
        ctl.focus();

    return true;
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change() {
    // mostra o div correspondente ao relatorio
    showDivByReportID();

    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if (this.disabled == false)
        this.focus();

    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status() {
    var btnOKStatus = true;
    var i, elem;

    if (selReports.selectedIndex != -1) {
        // Relatorio de Valores a Localizar
        if (selReports.value == 40191)
            btnOKStatus = false;
            // Impressao de cheque
        else if (selReports.value == 40192)
            btnOKStatus = (glb_nFormaPagamentoID != 1032);
    }

    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked() {
    // Relatorio de Valores a Localizar
    if ((selReports.value == 40191) && (glb_sCaller == 'PL'))
        relatorioValoresLocalizar();
    else if ((selReports.value == 40192) && (glb_sCaller == 'S'))
        imprimeCheque();
    else
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
}

/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID() {
    var i, coll, attr, currRep;

    attr = null;
    currRep = 0;

    currRep = selReports.value;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (attr == currRep)
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr() {
    var i, coll, attr, retVal;

    attr = null;
    retVal = 0;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (coll[i].style.visibility == 'visible') {
                retVal = attr;
                break;
            }
        }
    }
    return retVal;
}


/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights() {
    if (selReports.options.length != 0)
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;

    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else
            coll[i].style.visibility = 'hidden';
    }

    // desabilita o combo de relatorios
    selReports.disabled = true;

    // desabilita o botao OK
    btnOK.disabled = true;

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];

    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);

    elem = document.getElementById('selReports');

    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);

    // a altura livre    
    modHeight -= topFree;

    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt';
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';

    // acrescenta o elemento
    window.document.body.appendChild(elem);

    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);

    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;

    return null;
}

function adjustDivRelatorioValoresLocalizar() {
    var i;
    txtDataInicial.maxLength = 10;
    txtDataFinal.maxLength = 10;

    txtDataInicial.value = glb_dCurrDate;
    txtDataFinal.value = glb_dCurrDate;

    adjustElementsInForm([['lblDataInicial', 'txtDataInicial', 10, 1, -10, -10],
                          ['lblDataFinal', 'txtDataFinal', 10, 1],
                          ['lblOcorrencias', 'chkOcorrencias', 3, 1],
						  ['lblApropriacao', 'chkApropriacao', 3, 1],
						  ['lblFormaPagamentoID', 'selFormaPagamentoID', 8, 1],
						  ['lblEstadoCadastrado', 'chkEstadoCadastrado', 3, 2, -10],
						  ['lblEstadoConsultado', 'chkEstadoConsultado', 3, 2],
						  ['lblEstadoGarantia', 'chkEstadoGarantia', 3, 2],
						  ['lblEstadoAberto', 'chkEstadoAberto', 3, 2],
						  ['lblEstadoDepositado', 'chkEstadoDepositado', 3, 2],
						  ['lblEstadoFechado', 'chkEstadoFechado', 3, 2],
						  ['lblEstadoDevolvido', 'chkEstadoDevolvido', 3, 2],
						  ['lblEstadoEnvAdvogado', 'chkEstadoEnvAdvogado', 3, 2],
						  ['lblEstadoIncobravel', 'chkEstadoIncobravel', 3, 2],
						  ['lblEstadoDevolvidoCliente', 'chkEstadoDevolvidoCliente', 3, 2],
                          ['lblFiltro', 'txtFiltro', 46, 3, -10]], null, null, true);

    txtDataInicial.onfocus = selFieldContent;
    txtDataFinal.onfocus = selFieldContent;

    // Direitos
    var dirA1;
    var dirA2;

    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, 40191, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, 40191, 5);

    //DireitoEspecifico
    //Valores a Localizar->Recebimento/Pagamento->Modal Print
    //Relatorio: 40191 -> Valores a Localizar
    //A1=0 || A2 = 0-> Desabilita os campos: chkApropriacao,chkEstadoCadastrado,chkEstadoConsultado,chkEstadoGarantia,chkEstadoAberto,chkEstadoDepositado,chkEstadoFechado
    //chkEstadoDevolvido,chkEstadoEnvAdvogado,chkEstadoIncobravel,chkEstadoDevolvidoCliente,selFormaPagamentoID    

    if (!((dirA1) && (dirA2))) {
        selFormaPagamentoID.selectedIndex = 0;
        chkEstadoCadastrado.checked = true;
        chkEstadoAberto.checked = false;
        chkEstadoFechado.checked = false;
        chkEstadoConsultado.checked = false;
        chkEstadoGarantia.checked = false;
        chkEstadoDepositado.checked = false;
        chkEstadoDevolvido.checked = false;
        chkEstadoEnvAdvogado.checked = false;
        chkEstadoIncobravel.checked = false;
        chkEstadoDevolvidoCliente.checked = false;
    }

    chkApropriacao.disabled = (!((dirA1) && (dirA2)));
    chkEstadoCadastrado.disabled = (!((dirA1) && (dirA2)));
    chkEstadoConsultado.disabled = (!((dirA1) && (dirA2)));
    chkEstadoGarantia.disabled = (!((dirA1) && (dirA2)));
    chkEstadoAberto.disabled = (!((dirA1) && (dirA2)));
    chkEstadoDepositado.disabled = (!((dirA1) && (dirA2)));
    chkEstadoFechado.disabled = (!((dirA1) && (dirA2)));
    chkEstadoDevolvido.disabled = (!((dirA1) && (dirA2)));
    chkEstadoEnvAdvogado.disabled = (!((dirA1) && (dirA2)));
    chkEstadoIncobravel.disabled = (!((dirA1) && (dirA2)));
    chkEstadoDevolvidoCliente.disabled = (!((dirA1) && (dirA2)));
    selFormaPagamentoID.disabled = (!((dirA1) && (dirA2)));

    if (!((dirA1) && (dirA2))) {
        selFormaPagamentoID.selectedIndex = 0;
        chkEstadoCadastrado.checked = true;
        chkEstadoAberto.checked = false;
        chkEstadoFechado.checked = false;
        chkEstadoConsultado.checked = false;
        chkEstadoGarantia.checked = false;
        chkEstadoDepositado.checked = false;
        chkEstadoDevolvido.checked = false;
        chkEstadoEnvAdvogado.checked = false;
        chkEstadoIncobravel.checked = false;
        chkEstadoDevolvidoCliente.checked = false;
    }
}

function adjustDivCheque() {
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    adjustElementsInForm([['lblCheckSummary', 'chkCheckSummary', 3, 1, -10, -10],
						  ['lblBancoAgencia', 'txtBancoAgencia', 12, 1, -4],
						  ['lblConta', 'txtConta', 20, 1, 2],
						  ['lblNumero', 'txtNumero', 10, 1],
						  ['lblValor', 'txtValor', 16, 1],
						  ['lblExtenso', 'txtExtenso', 66, 2, -10]], null, null, true);

    adjustElementsInForm([['lblNominal', 'txtNominal', 66, 1, -10, 105],
						  ['lblLocal', 'txtLocal', 32, 2, -10],
						  ['lblData', 'txtData', 32, 2, 4],
						  ['lblBanco', 'txtBanco', 32, 3, -10],
						  ['lblEmitente', 'txtEmitente', 32, 3, 4],
						  ['lblValorID', 'txtValorID', 10, 4, -10],
						  ['lblHistorico', 'txtHistorico', 43, 4, 0],
						  ['lblDataApropriacao', 'txtDataApropriacao', 10, 4, 0],
						  ['lblTamanhoFonte', 'txtTamanhoFonte', 15, 5, -10]], null, null, true);

    txtExtenso.style.height = parseInt(txtNominal.offsetHeight, 10) * 2;
    txtHistorico.maxLength = 30;
    txtHistorico.onfocus = selFieldContent;

    txtNumero.maxLength = 6;
    txtNumero.onkeypress = verifyNumericEnterNotLinked;
    txtNumero.setAttribute('verifyNumPaste', 1);
    txtNumero.setAttribute('thePrecision', 6, 1);
    txtNumero.setAttribute('theScale', 0, 1);
    txtNumero.setAttribute('minMax', new Array(0, 999999), 1);
    txtNumero.onfocus = selFieldContent;

    // Checado se idioma ingles
    chkCheckSummary.checked = (aEmpresaData[8] == 245);

    //Tamanho M�ximo do campo TamanhoFonte
    txtTamanhoFonte.maxLength = 2;
}


function relatorioValoresLocalizar() {
    var sDataEmissaoIni = '';
    var sDataEmissaoFim = '';
    var sFieldData;
    // pega o value do contexto atual
    var nContextoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCmbCurrDataInControlBar(' + '\'' + 'sup' + '\'' + ', 1)[1]');
    var nFiltroID = 0;
    var strSQL = '';
    //var strSQL2 = '';
    var sEstados = '';
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var formato = 2;
    // var formato = selFormato.value;
    var sFiltro;

    if ((txtDataInicial.value.length > 0) || (txtDataFinal.value.length > 0)) {
        if ((txtDataInicial.value.length > 0) && ((!chkDataEx(txtDataInicial.value)) || (txtDataInicial.value.length < 1))) {
            if (window.top.overflyGen.Alert('Data inicial de emiss�o inv�lida.') == 0)
                return null;
            restoreModalPrint();
            window.focus();
            txtDataInicial.focus();
            return null;
        }
        else
            sDataEmissaoIni = txtDataInicial.value;

        if ((txtDataFinal.value.length > 0) && ((!chkDataEx(txtDataFinal.value)) || (!chkDataEx(txtDataFinal.value)))) {
            if (window.top.overflyGen.Alert('Data final de emiss�o inv�lida.') == 0)
                return null;
            restoreModalPrint();
            window.focus();
            txtDataFinal.focus();
            return null;
        }
        else
            sDataEmissaoFim = txtDataFinal.value;
    }

    if (trimStr(sDataEmissaoIni) != '') {
        sDataEmissaoIni = putDateInMMDDYYYY2(sDataEmissaoIni);
        sDataEmissaoIni = '\'' + sDataEmissaoIni + '\'';
    }
    else
        sDataEmissaoIni = '';

    if (trimStr(sDataEmissaoFim) != '') {
        sDataEmissaoFim = putDateInMMDDYYYY2(sDataEmissaoFim);
        sDataEmissaoFim = '\'' + sDataEmissaoFim + '\'';
    }
    else
        sDataEmissaoFim = '';

    if (txtFiltro.value == '')
        sFiltro = "NULL";
    else
        sFiltro = txtFiltro.value;

    var aEstados = new Array();
    var aEstadosLabel = new Array();
    var sEstados = "";

    if (chkEstadoCadastrado.checked) {
        aEstados[aEstados.length] = 1;
        aEstadosLabel[aEstadosLabel.length] = 'C';
    }

    if (chkEstadoAberto.checked) {
        aEstados[aEstados.length] = 41;
        aEstadosLabel[aEstadosLabel.length] = 'A';
    }

    if (chkEstadoFechado.checked) {
        aEstados[aEstados.length] = 48;
        aEstadosLabel[aEstadosLabel.length] = 'F';
    }

    if (chkEstadoConsultado.checked) {
        aEstados[aEstados.length] = 51;
        aEstadosLabel[aEstadosLabel.length] = 'O';
    }

    if (chkEstadoGarantia.checked) {
        aEstados[aEstados.length] = 52;
        aEstadosLabel[aEstadosLabel.length] = 'G';
    }

    if (chkEstadoDepositado.checked) {
        aEstados[aEstados.length] = 53;
        aEstadosLabel[aEstadosLabel.length] = 'E';
    }

    if (chkEstadoDevolvido.checked) {
        aEstados[aEstados.length] = 54;
        aEstadosLabel[aEstadosLabel.length] = 'D';
    }

    if (chkEstadoEnvAdvogado.checked) {
        aEstados[aEstados.length] = 45;
        aEstadosLabel[aEstadosLabel.length] = 'V';
    }

    if (chkEstadoIncobravel.checked) {
        aEstados[aEstados.length] = 49;
        aEstadosLabel[aEstadosLabel.length] = 'I';
    }

    if (chkEstadoDevolvidoCliente.checked) {
        aEstados[aEstados.length] = 55;
        aEstadosLabel[aEstadosLabel.length] = 'L';
    }

    for (i = 0; i < aEstados.length; i++) {
        if (i == 0)
            sEstados = ' AND (';

        sEstados = sEstados + '(ValoresLocalizar.EstadoID=' + aEstados[i] + ') ';

        if (i == aEstados.length - 1)
            sEstados = sEstados + ')';
        else
            sEstados = sEstados + ' OR ';
    }

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato + "&nContextoID=" + nContextoID +
                        "&sDataEmissaoIni=" + sDataEmissaoIni + "&sDataEmissaoFim=" + sDataEmissaoFim + "&chkApropriacao=" + (chkApropriacao.checked ? 1 : 0) +
                        "&selFormaPagamentoID=" + selFormaPagamentoID.value + "&txtFiltro=" + sFiltro + "&chkOcorrencias=" + (chkOcorrencias.checked ? 1 : 0) + "&sEstados=" + sEstados;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/Reports_valoresalocalizar.aspx?' + strParameters;


}

function showModal_DSC() {
    glb_nDSOs--;

    if (glb_nDSOs == 0) {
        adjustFieldsValues();

        var bBuild = checkDataAndLayout();

        // mostra a janela modal com o arquivo carregado
        showExtFrame(window, true);

        window.focus();

        // coloca foco no campo Pesquisa
        //if (!txtNominal.disabled)
        //txtNominal.focus();

        // Layout de cheques nao definido
        if (bBuild == 0) {
            if (window.top.overflyGen.Alert('Layout de Impress�o de Cheques n�o est� definido.') == 0)
                return null;

            btnOK.disabled = true;

            return null;
        }
            // Nao retornou dados na query do cheque
        else if (bBuild == -1) {
            if (window.top.overflyGen.Alert('N�o foi poss�vel retornar dados do cheque.') == 0)
                return null;

            btnOK.disabled = true;

            return null;
        }
            // Forma de pagamento diferente de cheque
        else if (bBuild == -2) {
            if (window.top.overflyGen.Alert('Relat�rio dispon�vel apenas para cheque.') == 0)
                return null;

            btnOK.disabled = true;

            return null;
        }

    }
}

function adjustFieldsValues() {
    if ((dsoCheque.recordset.BOF) && (dsoCheque.recordset.EOF))
        return null;

    var i;

    // pega o value do contexto atual
    var nContextoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCmbCurrDataInControlBar(' + '\'' + 'sup' + '\'' + ', 1)[1]');
    var sNominal = '';
    var sEmitente = '';

    //Pagamentos
    if (nContextoID == 9131) {
        sNominal = dsoCheque.recordset['Pessoa'].value;
        sEmitente = dsoCheque.recordset['Empresa'].value;
    }
        //Recebimentos	
    else if (nContextoID == 9132) {
        sNominal = dsoCheque.recordset['Empresa'].value;

        if (dsoCheque.recordset['Emitente'].value != null)
            sEmitente = dsoCheque.recordset['Emitente'].value;
        else if (dsoCheque.recordset['Pessoa'].value != null)
            sEmitente = dsoCheque.recordset['Pessoa'].value;
    }

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var nValorEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
    // ValorExtenso1 = replicate('*',63);

    var nParamIdioma = 0;
    var sMoedaSingular = '';
    var sMoedaPlural = '';

    // Ingles
    if (aEmpresaData[8] == 245) {
        nParamIdioma = 1;
        sMoedaSingular = 'Dollar';
        sMoedaPlural = 'Dollars';
    }
        // Portugues
    else if (aEmpresaData[8] == 246) {
        nParamIdioma = 0;
        sMoedaSingular = 'Real';
        sMoedaPlural = 'Reais';
    }
        // Espanhol
    else if (aEmpresaData[8] == 247) {
        nParamIdioma = 2;
        sMoedaSingular = 'Euro';
        sMoedaPlural = 'Euros';
    }

    var sValorExtenso = valorExtenso(dsoCheque.recordset['Valor'].value, sMoedaSingular, sMoedaPlural, nParamIdioma);
    var nSizeExtenso1 = 0;
    var nSizeExtenso2 = 0;
    sValorExtenso = sValorExtenso + ' ' + replicate('* ', 63);
    sValorExtenso = sValorExtenso.substr(0, 63 * 2);

    var ValorExtenso1 = sValorExtenso.substr(0, 63);
    var ValorExtenso2 = sValorExtenso.substr(63, 63);

    txtBancoAgencia.readOnly = true;
    txtConta.readOnly = true;
    txtNumero.readOnly = (nValorEstadoID != 1);
    txtValor.readOnly = true;
    txtExtenso.readOnly = true;
    txtNominal.readOnly = false;
    txtLocal.readOnly = true;
    txtData.readOnly = true;
    txtBanco.readOnly = true;
    txtEmitente.readOnly = true;
    txtValorID.readOnly = true;
    txtDataApropriacao.readOnly = true;
    txtTamanhoFonte.readOnly = false;

    if (dsoCheque.recordset['BancoAgencia'].value != null)
        img_LogBanco_RecSac.src = glb_pagesURLRoot + '/serversidegenEx/imageblob.aspx?nFormID=9220&' +
				'nSubFormID=28230&nRegistroID=' + (dsoCheque.recordset['BancoAgencia'].value).substr(0, 3);
    else
        img_onerror(null);

    txtBancoAgencia.value = dsoCheque.recordset['BancoAgencia'].value;
    txtConta.value = dsoCheque.recordset['Conta'].value;
    txtNumero.value = dsoCheque.recordset['NumeroCheque'].value;
    txtValor.value = dsoCheque.recordset['Valor'].value;
    txtExtenso.value = ValorExtenso1 + String.fromCharCode(13, 10) + ValorExtenso2;
    txtExtenso.value = removeDiatricsOnJava(txtExtenso.value);
    txtNominal.value = sNominal;
    txtLocal.value = dsoCheque.recordset['Localidade'].value;
    txtData.value = dsoCheque.recordset['Dia'].value + ' de ' + dsoCheque.recordset['Mes'].value + ' de ' +
		dsoCheque.recordset['Ano'].value;
    txtData.value = txtData.value;
    txtBanco.value = dsoCheque.recordset['NomeBanco'].value;
    txtEmitente.value = sEmitente;
    txtValorID.value = dsoCheque.recordset['ValorID'].value;
    txtHistorico.value = dsoCheque.recordset['Historico'].value;
    txtDataApropriacao.value = dsoCheque.recordset['dtApropriacao'].value;
    txtTamanhoFonte.value = '10';

    //btnOK.disabled = dsoCheque.recordset['Controle'].value;
}

function checkDataAndLayout() {
    if ((dsoLayoutCheque.recordset.BOF) && (dsoLayoutCheque.recordset.EOF))
        return 0;
    else if ((dsoCheque.recordset.BOF) && (dsoCheque.recordset.EOF))
        return -1;
    else if (glb_nFormaPagamentoID != 1032)
        return -2;
    else
        return 1;
}

function buildDataAndLayout() {
    var i = 0;
    var sValorExtenso, ValorExtenso1, ValorExtenso2;

    dsoLayoutCheque.recordset.moveFirst();
    dsoCheque.recordset.moveFirst();

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    // ValorExtenso1 = replicate('*',63);

    var nParamIdioma = 0;
    var sMoedaSingular = '';
    var sMoedaPlural = '';

    // Ingles
    if (aEmpresaData[8] == 245) {
        nParamIdioma = 1;
        sMoedaSingular = 'Dollar';
        sMoedaPlural = 'Dollars';
    }
        // Portugues
    else if (aEmpresaData[8] == 246) {
        nParamIdioma = 0;
        sMoedaSingular = 'Real';
        sMoedaPlural = 'Reais';
    }
        // Espanhol
    else if (aEmpresaData[8] == 247) {
        nParamIdioma = 2;
        sMoedaSingular = 'Euro';
        sMoedaPlural = 'Euros';
    }

    sValorExtenso = valorExtenso(dsoCheque.recordset['Valor'].value, sMoedaSingular, sMoedaPlural, nParamIdioma);
    sValorExtenso = sValorExtenso + ' ' + replicate('* ', 80);
    sValorExtenso = sValorExtenso.substr(0, 160);

    while (!dsoLayoutCheque.recordset.EOF) {
        glb_aDataCheque[i] = new _Cheque();

        glb_aDataCheque[i].nTipoCampoID = dsoLayoutCheque.recordset['TipoCampoID'].value;
        glb_aDataCheque[i].nLinha = dsoLayoutCheque.recordset['Linha'].value;
        glb_aDataCheque[i].nColuna = dsoLayoutCheque.recordset['Coluna'].value;
        glb_aDataCheque[i].nTamanho = dsoLayoutCheque.recordset['Tamanho'].value;
        glb_aDataCheque[i].nFonte = dsoLayoutCheque.recordset['TamanhoFonte'].value;
        glb_aDataCheque[i].bNegrito = dsoLayoutCheque.recordset['Negrito'].value;
        glb_aDataCheque[i].bCaixaAlta = dsoLayoutCheque.recordset['CaixaAlta'].value;

        switch (dsoLayoutCheque.recordset['TipoCampoID'].value) {
            // Valor
            case 1081:
                {
                    glb_aDataCheque[i].vPrintValue = dsoCheque.recordset['Valor'].value;
                }
                break;

                // Extenso 1	
            case 1082:
                {
                    ValorExtenso1 = sValorExtenso.substr(0, glb_aDataCheque[i].nTamanho);
                    glb_aDataCheque[i].vPrintValue = ValorExtenso1;
                }
                break;

                // Extenso 2
            case 1083:
                {
                    ValorExtenso2 = sValorExtenso.substr(glb_aDataCheque[i - 1].nTamanho, glb_aDataCheque[i].nTamanho);

                    if ((nParamIdioma != 0) && (trimStr(ValorExtenso2.substr(0, 2)) == '*'))
                        glb_aDataCheque[i].vPrintValue = '';
                    else
                        glb_aDataCheque[i].vPrintValue = ValorExtenso2;
                }
                break;

                // Nominal
            case 1084:
                {
                    glb_aDataCheque[i].vPrintValue = txtNominal.value;
                    glb_nNominalIndexInArray = i;
                }
                break;

                // Cidade
            case 1085:
                {
                    glb_aDataCheque[i].vPrintValue = dsoCheque.recordset['Localidade'].value;
                }
                break;

                // Dia
            case 1086:
                {
                    glb_aDataCheque[i].vPrintValue = dsoCheque.recordset['Dia'].value;
                }
                break;

                // Mes
            case 1087:
                {
                    glb_aDataCheque[i].vPrintValue = dsoCheque.recordset['Mes'].value;
                }
                break;

                // Ano
            case 1088:
                {
                    glb_aDataCheque[i].vPrintValue = dsoCheque.recordset['Ano'].value;
                }
                break;

                // Historico
            case 1089:
                {
                    glb_aDataCheque[i].vPrintValue = dsoCheque.recordset['ValorID'].value + '   ' + txtHistorico.value;

                    //if (dsoCheque.recordset['BomPara'].value == 1)
                    //{
                    //	glb_aDataCheque[i].vPrintValue += '   Bom para ' + dsoCheque.recordset['dtApropriacao'].value;
                    //}
                }
                break;

            default:
                break;
        }

        i++;
        dsoLayoutCheque.recordset.moveNext();
    }
}


/********************************************************************
Pressiona botao OK se pressionado Enter neste campo
********************************************************************/
function txtNominal_onkeypress() {
    if (event.keyCode == 13)
        btnOK_Clicked();
}

function imprimeCheque() {
    var sTime = '';
    var d = new Date();
    var nTop = 0;
    var nLeft = 0;
    var nFontSize = 11;
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var sHeader1 = '';
    var i = 0;
    var nCount = 0;
    var nContextoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCmbCurrDataInControlBar(' + '\'' + 'sup' + '\'' + ', 1)[1]');
    var sPessoa = '';
    var nFonteSummary = 0;
    var bNegrito = false;
    var sCampo = '';
    var nValorID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ValorID' + '\'' + '].value');
    var sLinguaLogada = getDicCurrLang();

    var strParameters = "RelatorioID=" + selReports.value + "&nEmpresaLogada=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&nEmpresaLogadaIdioma=" + aEmpresaData[8];


    //Verifica se o conte�do do campo � numero e v�lido
    if ((!isNaN(txtTamanhoFonte.value)) && (txtTamanhoFonte.value != null) && (txtTamanhoFonte.value != '0') && (txtTamanhoFonte.value != ''))
        nFonteSummary = txtTamanhoFonte.value;
    else {
        lockControlsInModalWin(false);

        if (window.top.overflyGen.Alert('Preencha o tamanho da fonte corretamente') == 0)
            return null;

        window.focus();
        txtTamanhoFonte.focus();
        return null;
    }

    if (trimStr(txtNumero.value) == '') {
        lockControlsInModalWin(false);

        if (window.top.overflyGen.Alert('Preencha o n�mero do cheque') == 0)
            return null;

        window.focus();
        txtNumero.focus();
        return null;
    }

    sTime += d.getHours() + ':';
    sTime += padL((d.getMinutes()).toString(), 2, '0') + ':';
    sTime += padL((d.getSeconds()).toString(), 2, '0');

    buildDataAndLayout();
    txtNominal.value = trimStr(txtNominal.value);


    glb_aDataCheque[glb_nNominalIndexInArray].vPrintValue = trimStr(txtNominal.value);


    for (i = 0; i < glb_aDataCheque.length; i++) {
        nTop = glb_aDataCheque[i].nLinha;
        var sFontName = 'Fixedsys';

        /*
		    Tamanho da fonte din�mico - FSM 17/09/2012
		*/

        // Se tem valor no campo fonte
        if ((glb_aDataCheque[i].nFonte != 0) && (glb_aDataCheque[i].nFonte != null))
            nFontSize = glb_aDataCheque[i].nFonte;
        else {
            // Se campo = Valor
            if (glb_aDataCheque[i].nTipoCampoID == 1081)
                nFontSize = 14;
                // Se campo = extenso1 ou extenso2 ou historico...
            else if ((glb_aDataCheque[i].nTipoCampoID == 1082) || (glb_aDataCheque[i].nTipoCampoID == 1083) ||
		    (glb_aDataCheque[i].nTipoCampoID == 1089))
                nFontSize = 10;
            else
                nFontSize = 11;
        }

        //Trata a propriedade Negrito
        if (glb_aDataCheque[i].bNegrito == null)
            bNegrito = false;
        else
            bNegrito = glb_aDataCheque[i].bNegrito;

        //Trata a propriedade Caixa Alta
        if ((glb_aDataCheque[i].bCaixaAlta) && (glb_aDataCheque[i].vPrintValue != null)) {
            sCampo = glb_aDataCheque[i].vPrintValue;
            glb_aDataCheque[i].vPrintValue = sCampo.toString().toUpperCase();
        }

        if (glb_aDataCheque[i].nTipoCampoID == 1084 && glb_aDataCheque[i].vPrintValue.indexOf('&') >= 0)  
            strParameters += "&" + glb_aDataCheque[i].nTipoCampoID + "=" + glb_aDataCheque[i].vPrintValue.replace('\&', '\%26');
        else
            strParameters += "&" + glb_aDataCheque[i].nTipoCampoID + "=" + glb_aDataCheque[i].vPrintValue;
    }


    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);



    strParameters += "&chkCheckSummary=" + (chkCheckSummary.checked ? true : false) + "&nValorID=" + nValorID + "&nContextoID=" + nContextoID + "&nFonteSummary=" + nFonteSummary + "&txtNumero=" + txtNumero.value +
                     "&nEmpresaNome=" + aEmpresaData[6] + "&sLinguaLogada=" + sLinguaLogada;

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechangeCheque;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/Reports_valoresalocalizar.aspx?" + strParameters;
}

function gravarHistorico() {
    if ((txtHistorico.value != dsoCheque.recordset['Historico'].value) ||
	    (txtNumero.value != dsoCheque.recordset['NumeroCheque'].value)) {
        var strPars = '?nValorID=' + escape(dsoCheque.recordset['ValorID'].value);
        strPars += '&sHistorico=' + escape(txtHistorico.value);
        strPars += '&nNumeroCheque=' + escape(txtNumero.value);

        dsoGravaHistorico.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/gravahistorico.aspx' + strPars;
        dsoGravaHistorico.ondatasetcomplete = dsoGravaHistorico_DSC;
        dsoGravaHistorico.Refresh();
    }
    else {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S', new Array(0, 0));
    }
}

function reports_onreadystatechangeCheque() {
    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
    (frameReport.contentWindow.document.readyState == 'interactive') ||
    (frameReport.contentWindow.document.readyState == 'complete')) {
        gravarHistorico();
    }
}

function dsoGravaHistorico_DSC() {
    // 1. O usuario clicou o botao OK
    // sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S' , new Array(glb_nLanContaID, txtNominal.value));
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S', new Array(0, 0));
}

function img_onload(ctl) {
    img_LogBanco_RecSac.style.left = lblBanco.offsetLeft;
    img_LogBanco_RecSac.style.top = lblBanco.offsetTop;
    img_LogBanco_RecSac.style.width = 39;
    img_LogBanco_RecSac.style.height = 39;
    lblBanco.style.left = img_LogBanco_RecSac.offsetLeft + img_LogBanco_RecSac.offsetWidth + ELEM_GAP;
    img_LogBanco_RecSac.style.visibility = 'inherit';
    txtBanco.style.left = lblBanco.offsetLeft;
    txtBanco.style.width = txtBanco.offsetWidth - (img_LogBanco_RecSac.offsetWidth + ELEM_GAP);
}

function img_onerror(ctl) {
    img_LogBanco_RecSac.style.visibility = 'hidden';
    img_LogBanco_RecSac.style.left = 0;
    img_LogBanco_RecSac.style.top = 0;
    img_LogBanco_RecSac.style.width = 0;
    img_LogBanco_RecSac.style.height = 0;
}
