
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalprintHtml" name="modalprintHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalprint.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
        
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen//commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_progressbar.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen//commonmodalprint.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalprint_valoresalocalizar.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Dim i, sCaller, nEmpresaID, sEmpresaFantasia, nContextoID, sContexto, nUserID
Dim relatorioID, relatorio, EhDefault, relatorioFantasia, dirA1, dirA2, dirA1A2True
Dim currDateFormat, nRelPesContaID, nFormaPagamentoID

sCaller = ""
nEmpresaID = 0
sEmpresaFantasia = ""
nContextoID = 0
sContexto = ""
nUserID = 0
nRelPesContaID = 0
nFormaPagamentoID = 0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

'nEmpresaID - ID da empresa logada
For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

'sEmpresaFantasia - fantasia da empresa logada
For i = 1 To Request.QueryString("sEmpresaFantasia").Count    
    sEmpresaFantasia = Request.QueryString("sEmpresaFantasia")(i)
Next

'nContextoID - ID do contexto
For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

'sContexto
For i = 1 To Request.QueryString("sContexto").Count    
    sContexto = Request.QueryString("sContexto")(i)
Next

'nUserID - ID do usuario logado
For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

'Formato corrente de data
For i = 1 To Request.QueryString("currDateFormat").Count
    currDateFormat = Request.QueryString("currDateFormat")(i)
Next

For i = 1 To Request.QueryString("nRelPesContaID").Count
    nRelPesContaID = Request.QueryString("nRelPesContaID")(i)
Next

For i = 1 To Request.QueryString("nFormaPagamentoID").Count
    nFormaPagamentoID = Request.QueryString("nFormaPagamentoID")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & nEmpresaID & ";"
Response.Write vbcrlf

Response.Write "var glb_sEmpresaFantasia = " & Chr(39) & sEmpresaFantasia & Chr(39) & ";"
Response.Write vbcrlf

If (currDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';"
ElseIf (currDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';"
ElseIf (currDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';"
Else
    Response.Write "var glb_dCurrDate = '';"
End If
Response.Write vbcrlf

Response.Write "var glb_nRelPesContaID = " & CStr(nRelPesContaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nFormaPagamentoID = " & CStr(nFormaPagamentoID) & ";"
Response.Write vbcrlf

If (Mid(pagesURLRoot, 1, 10) <> "http://www") Then
	Response.Write "var glb_pagesURLRoot = 'http://localhost/overfly3';"
	Response.Write vbcrlf
Else
	Response.Write "var glb_pagesURLRoot = '" & pagesURLRoot & "';"
	Response.Write vbcrlf
End If

Response.Write "var glb_sContexto = '" & sContexto & "';"
Response.Write vbcrlf

'Monta array dos relatorios
Response.Write "// Array dos relatorios, com seus ids, nomes"
Response.Write vbcrlf

Dim rsData, strSQL
Dim rsData1

If ( (sCaller) = "PL" ) Then
    i = 0
Else
    i = 1
End If        

strSQL = "SELECT DISTINCT d.RecursoID AS RelatorioID, d.Recurso AS Relatorio, " & _
         "c.EhDefault, d.RecursoFantasia AS RelatorioFantasia, c.Ordem, " & _
         "g.Alterar1 AS A1, g.Alterar2 AS A2 " & _
         "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), " & _
         "RelacoesPesRec e WITH(NOLOCK), RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK) " & _
         "WHERE a.ObjetoID= " & CStr(nContextoID) & " AND a.TipoRelacaoID=1 AND a.EstadoID=2 " & _
         "AND a.SujeitoID=b.RecursoID AND b.EstadoID=2 AND b.TipoRecursoID=4 " & _ 
         "AND b.Principal=1 AND b.RecursoID=c.ObjetoID AND c.EstadoID=2 " & _
         "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID " & _
         "AND d.EstadoID=2 AND d.TipoRecursoID=5 AND d.ClassificacaoID=14 AND d.EhDetalhe= " & CStr(i) & " " & _
         "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) WHERE a.RecursoID= " & CStr(nContextoID) & " )) " & _
         "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " & _
         "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " & _
         "AND e.SujeitoID IN ((SELECT " & CStr(nUserID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(nUserID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
         "AND e.RelacaoID=f.RelacaoID " & _
         "AND f.EmpresaID= " & CStr(nEmpresaID) & " AND g.ContextoID= " & CStr(nContextoID) & " " & _
         "AND c.ObjetoID=g.RecursoMaeID AND d.RecursoID=g.RecursoID " & _
         "AND (g.Consultar1=1 OR g.Consultar2=1) " & _
         "AND f.PerfilID=g.PerfilID " & _
         "ORDER BY c.Ordem"

Set rsData = Server.CreateObject("ADODB.Recordset")         

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write vbcrlf
Response.Write "var glb_arrayReports = new Array();"
Response.Write vbcrlf
Response.Write vbcrlf
'------------------
i = 0
Do While (Not rsData.EOF)
        
    relatorioID = rsData.Fields("RelatorioID").Value
    relatorio = rsData.Fields("Relatorio").Value
    EhDefault = rsData.Fields("EhDefault").Value
    relatorioFantasia = rsData.Fields("RelatorioFantasia").Value
    dirA1 = rsData.Fields("A1").Value
    dirA2 = rsData.Fields("A2").Value
    
    If ( dirA1 AND dirA2 ) Then
        dirA1A2True = TRUE        
    Else
        dirA1A2True = FALSE    
    End If
        
    rsData.MoveNext
        
    Do While (Not rsData.EOF)
        If ( CLng(relatorioID) = CLng(rsData.Fields("RelatorioID").Value) ) Then
            'dirX1 e dirX2 e o anterior sempre
            If ( (rsData.Fields("A1").Value) AND (rsData.Fields("A2").Value) ) Then
                dirA1A2True = TRUE        
            End If

            dirA1 = dirA1 OR rsData.Fields("A1").Value
            dirA2 = dirA2 OR rsData.Fields("A2").Value
            rsData.MoveNext
        Else
            Exit Do
        End If    
    Loop    
        
    If ( (dirA1) AND (dirA2) ) Then
        If ( NOT (dirA1A2True)  ) Then
            dirA1 = FALSE
            dirA2 = TRUE
        End If
    End If    

    Response.Write "glb_arrayReports[" & CStr(i) & "] = new Array("
    Response.Write Chr(39)
    Response.Write CStr(relatorio)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write CStr(relatorioID)
    Response.Write ","
    Response.Write LCase(CStr(EhDefault))
    Response.Write ","
    Response.Write Chr(39)
    Response.Write CStr(relatorioFantasia)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write LCase(CStr(dirA1))
    Response.Write ","
    Response.Write LCase(CStr(dirA2))
    Response.Write ");"
    i = i + 1

Loop

rsData.Close
Set rsData = Nothing

Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "</script>"

Response.Write vbcrlf

'--- Interface dos relatorios do form Relacoes entre Pessoas 
    
'strSQL = ""

'Set rsData1 = Server.CreateObject("ADODB.Recordset")         

'rsData1.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

'--- Final de Interface do Relatorios do form Relacoes entre Pessoas  

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>


</head>

<body id="modalprintBody" name="modalprintBody" LANGUAGE="javascript" onload="return window_onload()">

    <div id="divRelatorioValoresLocalizar" name="divRelatorioValoresLocalizar" class="divGeneral">
        <p id="lblDataInicial" name="lblDataInicial" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicial" name="txtDataInicial" class="fldGeneral"></input>
        <p id="lblDataFinal" name="lblDataFinal" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFinal" name="txtDataFinal" class="fldGeneral"></input>
        <p id="lblOcorrencias" name="lblOcorrencias" class="lblGeneral">Oc</p>
        <input type="checkbox" id="chkOcorrencias" name="chkOcorrencias" class="fldGeneral" title="Detalhar Ocorr�ncias?">
        <p id="lblApropriacao" name="lblApropriacao" class="lblGeneral">Apr</p>
        <input type="checkbox" id="chkApropriacao" name="chkApropriacao" class="fldGeneral" title="Usar data de apropria��o?">

        <p id="lblEstadoCadastrado" name="lblEstadoCadastrado" class="lblGeneral">C</p>
        <input type="checkbox" id="chkEstadoCadastrado" name="chkEstadoCadastrado" class="fldGeneral" title="Cadastrado">
        <p id="lblEstadoConsultado" name="lblEstadoConsultado" class="lblGeneral">O</p>
        <input type="checkbox" id="chkEstadoConsultado" name="chkEstadoConsultado" class="fldGeneral" title="Consultado">
        <p id="lblEstadoGarantia" name="lblEstadoGarantia" class="lblGeneral">G</p>
        <input type="checkbox" id="chkEstadoGarantia" name="chkEstadoGarantia" class="fldGeneral" title="Garantia">
        <p id="lblEstadoAberto" name="lblEstadoAberto" class="lblGeneral">A</p>
        <input type="checkbox" id="chkEstadoAberto" name="chkEstadoAberto" class="fldGeneral" title="Aberto">
        <p id="lblEstadoDepositado" name="lblEstadoDepositado" class="lblGeneral">E</p>
        <input type="checkbox" id="chkEstadoDepositado" name="chkEstadoDepositado" class="fldGeneral" title="Depositado">
        <p id="lblEstadoFechado" name="lblEstadoFechado" class="lblGeneral">F</p>
        <input type="checkbox" id="chkEstadoFechado" name="chkEstadoFechado" class="fldGeneral" title="Fechado">
        <p id="lblEstadoDevolvido" name="lblEstadoDevolvido" class="lblGeneral">D</p>
        <input type="checkbox" id="chkEstadoDevolvido" name="chkEstadoDevolvido" class="fldGeneral" title="Devolvido">
        <p id="lblEstadoEnvAdvogado" name="lblEstadoEnvAdvogado" class="lblGeneral">V</p>
        <input type="checkbox" id="chkEstadoEnvAdvogado" name="chkEstadoEnvAdvogado" class="fldGeneral" title="Enviado p/ advogado">
        <p id="lblEstadoIncobravel" name="lblEstadoIncobravel" class="lblGeneral">I</p>
        <input type="checkbox" id="chkEstadoIncobravel" name="chkEstadoIncobravel" class="fldGeneral" title="Incobr�vel">
        <p id="lblEstadoDevolvidoCliente" name="lblEstadoDevolvidoCliente" class="lblGeneral">L</p>
        <input type="checkbox" id="chkEstadoDevolvidoCliente" name="chkEstadoDevolvidoCliente" class="fldGeneral" title="Devolvido para cliente">
        <p id="lblFormaPagamentoID" name="lblFormaPagamentoID" class="lblGeneral">Forma</p>
        <select id="selFormaPagamentoID" name="selFormaPagamentoID" class="fldGeneral">        
<%
'Preenche os options do combo de FormaPagamentoID
Dim rsDataFormas, strSQLFormas
Set rsDataFormas = Server.CreateObject("ADODB.Recordset")         

strSQLFormas = "SELECT 0 AS Indice, 0 AS FldID, SPACE(0) AS FldName, 0 AS Ordem " & _
	"UNION ALL " & _
	"SELECT 1 AS Indice, ItemID AS FldID, ItemAbreviado AS FldName, Ordem " & _
	"FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
	"WHERE TipoID=804 AND EstadoID=2 AND Filtro LIKE '%(9130)%' " & _
	"ORDER BY Indice, Ordem"
	
rsDataFormas.Open strSQLFormas, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

While Not rsDataFormas.EOF

    Response.Write "<option value =" & rsDataFormas.Fields("FldID").Value & _
                   ">" & _
                   rsDataFormas.Fields("FldName").Value & _
                   "</option>" & chr(13) & chr(10)        
    rsDataFormas.MoveNext
Wend

rsDataFormas.Close
Set rsDataFormas = Nothing
%>            
        </select>
        <p id="lblFiltro" name="lblFiltro" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro" name="txtFiltro" class="fldGeneral"></input>
    </div>

    <div id="divCheque" name="divCheque" class="divGeneral">
        <p id="lblCheckSummary" name="lblCheckSummary" class="lblGeneral">CS</p>
        <input type="checkbox" id="chkCheckSummary" name="chkCheckSummary" class="fldGeneral" title="Print Check Summary?">
		<p id="lblBancoAgencia" name="lblBancoAgencia" class="lblGeneral">Banco/Ag�ncia</p>
		<input type="text" id="txtBancoAgencia" class="fldGeneral" NAME="txtBancoAgencia">
		<p id="lblConta" name="lblConta" class="lblGeneral">Conta</p>
		<input type="text" id="txtConta" class="fldGeneral" NAME="txtConta">
		<p id="lblNumero" name="lblNumero" class="lblGeneral">N�mero</p>
		<input type="text" id="txtNumero" class="fldGeneral" NAME="txtNumero">
		<p id="lblValor" name="lblValor" class="lblGeneral">Valor</p>
		<input type="text" id="txtValor" class="fldGeneral" NAME="txtValor">
		<p id="lblExtenso" name="lblExtenso" class="lblGeneral">Extenso</p>
		<textarea id="txtExtenso" name="txtExtenso" class="fldGeneral"></textarea>
		<p id="lblNominal" name="lblNominal" class="lblGeneral">Nominal</p>
		<input type="text" id="txtNominal" class="fldGeneral" NAME="txtNominal">
		<p id="lblLocal" name="lblLocal" class="lblGeneral">Local</p>
		<input type="text" id="txtLocal" class="fldGeneral" NAME="txtLocal">
		<p id="lblData" name="lblData" class="lblGeneral">Data</p>
		<input type="text" id="txtData" class="fldGeneral" NAME="txtData">
		<img ID="img_LogBanco_RecSac" name="img_LogBanco_RecSac" class="lblGeneral" Language=javascript onload="return img_onload(this)" onerror="return img_onerror(this)"></IMG>
		<p id="lblBanco" name="lblBanco" class="lblGeneral">Banco</p>
		<input type="text" id="txtBanco" class="fldGeneral" NAME="txtBanco">
		<p id="lblEmitente" name="lblEmitente" class="lblGeneral">Emitente</p>
		<input type="text" id="txtEmitente" class="fldGeneral" NAME="txtEmitente">
		<p id="lblValorID" name="lblValorID" class="lblGeneral">ValorID</p>
		<input type="text" id="txtValorID" class="fldGeneral" NAME="txtValorID">
		<p id="lblHistorico" name="lblHistorico" class="lblGeneral">Hist�rico</p>
		<input type="text" id="txtHistorico" class="fldGeneral" NAME="txtHistorico">
		<p id="lblDataApropriacao" name="lblDataApropriacao" class="lblGeneral">Bom para</p>
		<input type="text" id="txtDataApropriacao" class="fldGeneral" NAME="txtDataApropriacao">		
        <p id="lblTamanhoFonte" name="lblTamanhoFonte" class="lblGeneral">Font Check Summary</p>
        <input type="text" id="txtTamanhoFonte" name="txtTamanhoFonte" class="fldGeneral"></input>
	</div>		

    <p id="lblReports" name="lblReports" class="paraNormal"></p>
    <select id="selReports" name="selReports" class="fldGeneral"></select>


    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>

</body>

</html>
