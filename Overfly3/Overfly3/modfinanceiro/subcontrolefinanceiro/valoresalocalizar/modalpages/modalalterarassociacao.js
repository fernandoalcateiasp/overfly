/********************************************************************
modalalterarassociacao.js

Library javascript para o modalalterarassociacao.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_ValLocTimerInt = null;
var glb_nSaldoValFinanceiro = 0;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bDevolucao = false;
var glb_nTimerSendDataToServer = null;
var glb_ListarTimerInt = null;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nMaxStringSize = 1024 * 1;
var glb_sMensagem = '';
var glb_aDeletedAssociacao = new Array();
var dsoGrid = new CDatatransport('dsoGrid');
var dsoGravaAssociacao = new CDatatransport('dsoGravaAssociacao');
var dsoValidaAssociacao = new CDatatransport('dsoValidaAssociacao');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
gravar()
listar()
gravar_DSC()
paintCellsSpecialyReadOnly()

js_fg_modallocalizarfinanceiroBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modallocalizarfinanceiroDblClick(grid, Row, Col)
js_modallocalizarfinanceiroKeyPress(KeyAscii)
js_modallocalizarfinanceiro_ValidateEdit()
js_modallocalizarfinanceiro_BeforeEdit(grid, row, col)
js_modallocalizarfinanceiro_AfterEdit(Row, Col)
js_fg_AfterRowColmodallocalizarfinanceiro (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalalterarassociacaoBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    showExtFrame(window, true);

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);

    window.focus();
    //txtVariacaoDias.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Alterar Associa��es', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    // desabilita o botao OK
    btnOK.disabled = true;
    btnFillGrid.disabled = false;
    btnExcluir.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    adjustElementsInForm([['lblOk', 'chkOk', 3, 1, 0, 35]], null, null, true);

    // Ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - parseInt(top, 10) - ((ELEM_GAP + 6) + 60);
    }

    with (fg.style) {
        left = 0;
        top = 50;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // Reposiciona botao OK

    with (btnOK) {
        style.top = divControls.offsetTop + 52;  //+ txtFiltro.offsetTop;
        style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
        value = 'Gravar';
    }

    with (btnFillGrid) {
        style.width = btnOK.currentStyle.width;
        style.height = btnOK.currentStyle.height;
        style.top = btnOK.currentStyle.top;
        style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnFillGrid.currentStyle.width, 10) - 4;
    }

    with (btnExcluir) {
        style.width = btnOK.currentStyle.width;
        style.height = btnOK.currentStyle.height;
        style.top = btnOK.currentStyle.top;
        style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnExcluir.currentStyle.width, 10) - 88;
    }

    // Esconde botao cancel
    with (btnCanc) {
        style.visibility = 'hidden';
    }

    var sMotivoDevolucao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'MotivoDevolucao\'].value');
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'EstadoID\'].value');

    if (((nEstadoID == 1) && (sMotivoDevolucao != null) && (sMotivoDevolucao != '')) || (glb_nProcessoID == 1423))
        glb_bDevolucao = true;

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;

}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        gravar();
    }
    else if (controlID == 'btnFillGrid') {
        listar();
        //btnExcluir.disabled = false;
    }
    else if (controlID == 'btnExcluir') {
        validaExclusao();
    }
        // codigo privado desta janela
    else {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtDataFiltro_onKeyPress() {
    if (event.keyCode == 13)
        btn_onclick(btnFillGrid);
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    //lockControlsInModalWin(true);

    // @@ atualizar interface aqui
    setupBtnsFromGridState();

    // preenche o grid da janela modal
    //    fillGridData(fromServer);
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState() {
    var bHasRowsInGrid = fg.Rows > 1;
    var nSaldo = 0;
    var nValor = 0;

    btnOK.disabled = !(bHasRowsInGrid);

    /*if (fg.Rows >= 1)
	    btnExcluir.disabled = true;
*/
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData(bFirtLoad) {
    btnExcluir.disabled = false;
    if (glb_ListarTimerInt != null) {
        window.clearInterval(glb_ListarTimerInt);
        glb_ListarTimerInt = null;
    }

    var nRegistroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ValorID\'].value');
    var sFiltro;
    var bOK;
    var nSaldoOcorrencia = '';

    if (chkOk.checked)
        sFiltro = 'AND a.OK = 1';
    else
        sFiltro = '';

    var sSQL = 'SELECT a.FinanceiroID, a.Valor AS ValorFin, ' +
	               'dbo.fn_ValorLocalizarFinanceiro_Posicao(a.ValFinanceiroID, 2) AS SaldoOcorrencia, ' +
	               'ISNULL(a.ok,0) AS OK, CONVERT(BIT, (CASE WHEN dbo.fn_ValorLocalizarFinanceiro_Posicao(a.ValFinanceiroID, 2) = 0 THEN 1 ELSE 0 END)) AS Aplicado, ' +
	               'a.observacao, h.Fantasia AS Usuario, d.RecursoAbreviado, b.PedidoID, b.Duplicata, e.Fantasia AS Pessoa, f.ItemAbreviado, ' +
                   'b.PrazoPagamento, b.dtEmissao, b.dtVencimento, c.SimboloMoeda, b.Valor, ' +
                   'dbo.fn_Financeiro_Posicao(b.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, ' +
                   'dbo.fn_Financeiro_Posicao(b.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, ' +
                   'g.Fantasia AS Colaborador, a.ValFinanceiroID ' +
                'FROM ValoresLocalizar_Financeiros a WITH(NOLOCK) ' +
	                'LEFT OUTER JOIN Pessoas h WITH(NOLOCK) ON (a.UsuarioID=h.PessoaID) ' +
	                'INNER JOIN Financeiro b WITH(NOLOCK) ON (a.FinanceiroID = b.FinanceiroID) ' +
	                'INNER JOIN Conceitos c WITH(NOLOCK) ON (b.MoedaID = c.ConceitoID) ' +
	                'INNER JOIN Recursos d WITH(NOLOCK) ON (b.EstadoID = d.RecursoID) ' +
	                'INNER JOIN Pessoas e WITH(NOLOCK) ON (b.PessoaID = e.PessoaID) ' +
	                'INNER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (b.FormaPagamentoID = f.ItemID) ' +
	                'INNER JOIN Pessoas g WITH(NOLOCK) ON (b.ProprietarioID = g.PessoaID) ' +
                'WHERE (a.ValorID = ' + nRegistroID + ') ' + nSaldoOcorrencia + sFiltro +
                'ORDER BY a.Valor DESC';


    setConnection(dsoGrid);
    dsoGrid.SQL = sSQL;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.FrozenCols = 0;

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '8';
    fg.FrozenCols = 0;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;
    glb_aDeletedAssociacao = new Array();
    //glb_GridIsBuilding = true;  

    headerGrid(fg, ['Financeiro',
                       'Valor',
                       'OK',
                       'A',
                       'Observa��o',
                       'Usu�rio',
                       'Est',
                       'Pedido',
                       'Duplicata',
                       'Pessoa',
                       'Forma',
                       'Prazo',
                       'Emiss�o',
                       'Vencimento',
                       '$',
                       'Valor',
                       'Saldo',
                       'Saldo Atual',
                       'Colaborador',
                       'ValFinanceiroID'], [19]);

    fillGridMask(fg, dsoGrid, ['FinanceiroID*',
                                   'ValorFin',
                                   'OK',
                                   'Aplicado*',
                                   'observacao',
                                   'Usuario*',
                                   'RecursoAbreviado*',
                                   'PedidoID*',
                                   'Duplicata*',
                                   'Pessoa*',
                                   'ItemAbreviado*',
                                   'PrazoPagamento*',
                                   'dtEmissao*',
                                   'dtVencimento*',
                                   'SimboloMoeda*',
                                   'Valor*',
                                   'SaldoDevedor*',
                                   'SaldoAtualizado*',
                                   'Colaborador*',
                                   'ValFinanceiroID'],
                             ['', '999999999.99', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                             ['', '###,###,##0.00', '', '', '', '', '', '', '', '', '', '', dTFormat, dTFormat, '', '###,###,###.00', '###,###,###.00', '###,###,###.00', '', '']);

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1, '###,###,###,###.00', 'S'],
	                                                      //[2, '###,###,###,###.00', 'S'],
														  [8, '########', 'C'],
														  [16, '###,###,###,###.00', 'S'],
														  [17, '###,###,###,###.00', 'S'],
														  [18, '###,###,###,###.00', 'S']]);

    alignColsInGrid(fg, [0, 1, 8, 12, 16, 17, 18]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.FrozenCols = 1;

    // Controla se o grid esta em construcao
    fg.ExplorerBar = 5;
    glb_GridIsBuilding = false;

    if (fg.Rows > 2)
        fg.Row = 2;

    if (fg.Rows > 2)
        fg.Editable = true;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
    }


    fg.Redraw = 2;
    window.focus();
    lockControlsInModalWin(false);
}

function listar() {
    fillGridData();
    dsoGrid.ondatasetcomplete = fillGridData_DSC;

}

function validaExclusao() {

    var nValFinanceiroID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValFinanceiroID'));
    var nRegistroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ValorID\'].value');

    var sSQL = 'SELECT COUNT(c.FinanceiroID) AS Quantidade ' +
	            'FROM ValoresLocalizar a WITH(NOLOCK) ' +
		            'INNER JOIN ValoresLocalizar_Financeiros b WITH(NOLOCK) ON (b.ValorID = a.ValorID) ' +
		            'INNER JOIN Financeiro_Ocorrencias c WITH(NOLOCK) ON (b.ValFinanceiroID = c.ValFinanceiroID) ' +
	            'WHERE a.ValorID = ' + nRegistroID + '  AND b.ValFinanceiroID = ' + nValFinanceiroID;

    setConnection(dsoValidaAssociacao);
    dsoValidaAssociacao.SQL = sSQL;
    dsoValidaAssociacao.ondatasetcomplete = validaExclusao_DSC;
    dsoValidaAssociacao.Refresh();

}

function validaExclusao_DSC() {
    if (!(dsoValidaAssociacao.recordset.BOF && dsoValidaAssociacao.recordset.EOF)) {
        if (dsoValidaAssociacao.recordset['Quantidade'].value > 0) {
            window.top.overflyGen.alert('Imposs�vel excluir financeiro com ocorr�ncias');
            return null;
        }
    }

    excluir();
}

function adjustLabelsCombos() {
    /*setLabelOfControl(lblFormaPagamentoID, selFormaPagamentoID.value);
    setLabelOfControl(lblColaboradorID, selColaboradorID.value);
    setLabelOfControl(lblHistoricoPadraoID, selHistoricoPadraoID.value);
    setLabelOfControl(lblPessoaID, selPessoaID.value);*/
}

function selHistoricoPadraoID_onchange() {
    adjustLabelsCombos();
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly() {
    paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalalterarassociacaoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
/*function js_fg_modalalterarassociacaoDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    glb_PassedOneInDblClick = true;

    if (fg.Rows > 1)
		gravar();
}*/

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalalterarassociacaoKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalalterarassociacao_ValidateEdit() {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalalterarassociacao_BeforeEdit(grid, row, col) {
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if (fldDet == null)   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ((fldDet[0] == 200) || (fldDet[0] == 129))  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalalterarassociacao_AfterEdit(Row, Col) {
    setupBtnsFromGridState();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalalterarassociacao(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return true;

    setupBtnsFromGridState();
}

function js_fg_modalalterarassociacaoDblClick(grid, Row, Col) {

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if (getColIndexByColKey(grid, 'OK') != Col)
        return true;

    var i;
    var bFill = true;

    lockControlsInModalWin(true);

    // limpa coluna se tem um check box checado
    for (i = 2; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 2; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    lockControlsInModalWin(false);
}

// FINAL DE EVENTOS DE GRID *****************************************

/********************************************************************
        BOT�O GRAVAR
********************************************************************/

function gravar() {
    lockControlsInModalWin(true);

    var sMensagem = '';
    var strPars = new String();
    var nBytesAcum = 0;
    var nDataLen = 0;
    var nValor;
    var bOk;
    var sObservacao;
    var nValFinanceiroID;

    strPars = '';

    //zera os arrays
    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_sResultado = '';

    for (var i = 2; i < fg.Rows; i++) {
        nBytesAcum = strPars.length;
        bOK = fg.textMatrix(i, getColIndexByColKey(fg, 'OK'));
        nValFinanceiroID = fg.TextMatrix(i, getColIndexByColKey(fg, 'ValFinanceiroID'));
        nValor = fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorFin'));
        sObservacao = (fg.TextMatrix(i, getColIndexByColKey(fg, 'Observacao') == '')) ? ' ' : (fg.TextMatrix(i, getColIndexByColKey(fg, 'Observacao')));

        if (nBytesAcum >= glb_nMaxStringSize) {
            nBytesAcum = 0;

            if (strPars != '') {
                glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                strPars = '';
                nDataLen = 0;
            }
        }

        if (strPars != '')
            strPars += '&nValFinanceiroID=' + escape(nValFinanceiroID);
        else
            strPars = '?nValFinanceiroID=' + escape(nValFinanceiroID);

        strPars += '&bOK=' + escape(bOK);
        strPars += '&sObservacao=' + escape(sObservacao);
        strPars += '&nValor=' + escape(nValor);
        strPars += '&nDelete=' + escape(0);

        nDataLen++;

    }

    if ((nDataLen > 0) || (glb_aSendDataToServer.length > 0)) {
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
        sendDataToServer();
    }

    else
        lockControlsInModalWin(false);
}
/*******************************************************
    BOT�O EXCLUIR
********************************************************/
function excluir() {

    var sMensagem = '';
    var strPars = new String();
    var nBytesAcum = 0;
    var nDataLen = 0;
    var nValFinanceiroID = 0;
    var bOk;

    strPars = '';

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_sResultado = '';

    for (var i = 2; i < fg.Rows; i++) {
        nBytesAcum = strPars.length;
        nValFinanceiroID = fg.TextMatrix(i, getColIndexByColKey(fg, 'ValFinanceiroID'));
        bOK = fg.textMatrix(i, getColIndexByColKey(fg, 'OK'));

        if (nBytesAcum >= glb_nMaxStringSize) {
            nBytesAcum = 0;

            if (strPars != '') {
                glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                strPars = '';
                nDataLen = 0;
            }
        }
        if (strPars == '')
            strPars += '?nDelete=' + escape(1);

        if (bOK != 0) {
            strPars += '&nValFinanceiroID=' + escape(nValFinanceiroID);

            nDataLen++;
        }
    }

    if ((nDataLen > 0) || (glb_aSendDataToServer.length > 0)) {
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
        sendDataToServer();
        dsoGrid.ondatasetcomplete = fillGridData_DSC;
    }
}

/*****Chamando a p�gina ASP*****/

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGravaAssociacao.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/gravaocorrencia.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGravaAssociacao.ondatasetcomplete = sendDataToServer_DSC;
            dsoGravaAssociacao.Refresh();
        }
        else {
            if (glb_sMensagem != '') {
                if (window.top.overflyGen.Alert(glb_sMensagem) == 0)
                    return null;

                glb_sMensagem = '';

                lockControlsInModalWin(false);
            }
            else {
                lockControlsInModalWin(false);
                glb_ListarTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
            }
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_ListarTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
    }

}

function sendDataToServer_DSC() {
    if (!(dsoGravaAssociacao.recordset.BOF && dsoGravaAssociacao.recordset.EOF)) {
        if (dsoGravaAssociacao.recordset['Mensagem'].value != null && dsoGravaAssociacao.recordset['Mensagem'].value.length > 0)
            glb_sMensagem = dsoGravaAssociacao.recordset['Mensagem'].value;
        else {
            glb_nPointToaSendDataToServer++;
            glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
        }
    }
}

function js_modalalterarassociacao_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {
    var firstLine = 1;

    if (glb_totalCols__ != false)
        firstLine = 2;

    if (NewRow >= firstLine)
        fg.Row = NewRow;
    else if ((NewRow < firstLine) && (fg.Rows > firstLine))
        fg.Row = firstLine;
}