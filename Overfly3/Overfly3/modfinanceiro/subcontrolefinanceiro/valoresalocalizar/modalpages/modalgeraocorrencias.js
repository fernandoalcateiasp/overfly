/********************************************************************
modalgeraocorrencias.js

Library javascript para o modalgeraocorrencias.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalgeraocorrenciasBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalgeraocorrenciasDblClick(grid, Row, Col)
js_modalgeraocorrenciasKeyPress(KeyAscii)
js_modalgeraocorrencias_ValidateEdit()
js_modalgeraocorrencias_BeforeEdit(grid, row, col)
js_modalgeraocorrencias_AfterEdit(Row, Col)
js_fg_AfterRowColmodalgeraocorrencias (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalgeraocorrenciasBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    glb_bFirstFill = true;
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Gerar Ocorr�ncias (Baixar Financeiro)', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    // desabilita o botao OK
    btnOK.disabled = true;
    btnFillGrid.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblData', 'txtData', 10, 1, -10, -10],
						  ['lblMotivo', 'chkMotivo', 3, 1],
						  ['lblFiltro', 'txtFiltro', 57, 1, -5]], null, null, true);

    txtData.onfocus = selFieldContent;
    txtData.maxLength = 10;
    txtData.onkeypress = txtDataFiltro_onKeyPress;
    txtData.value = glb_dCurrDate;

    chkMotivo.onclick = chkMotivo_onclick;

    txtFiltro.onfocus = selFieldContent;
    txtFiltro.maxLength = 1000;
    txtFiltro.onkeypress = txtDataFiltro_onKeyPress;

    // Ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        btnFillGrid.offsetTop + btnFillGrid.offsetHeight + 2;
        width = txtFiltro.offsetLeft + txtFiltro.offsetWidth + ELEM_GAP;
        height = txtFiltro.offsetTop + txtFiltro.offsetHeight;
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // Reposiciona botao OK
    with (btnOK) {
        style.top = divControls.offsetTop + txtFiltro.offsetTop;
        style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
        value = 'Ocorr�ncias';
        title = 'Gerar ocorr�ncias';
    }

    with (btnFillGrid) {
        style.width = btnOK.currentStyle.width;
        style.height = btnOK.currentStyle.height;
        style.top = btnOK.currentStyle.top;
        style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnFillGrid.currentStyle.width, 10) - 4;
    }

    // Esconde botao cancel
    with (btnCanc) {
        style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;
}

function chkMotivo_onclick() {
    fg.Rows = 1;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtDataFiltro_onKeyPress() {
    if (event.keyCode == 13)
        btn_onclick(btnFillGrid);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        saveDataInGrid();
    }
    else if (controlID == 'btnFillGrid') {
        listar();
    }
        // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    lockControlsInModalWin(true);

    // @@ atualizar interface aqui
    setupBtnsFromGridState();

    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState() {
    var i;
    var bHasRowsInGrid = fg.Rows > 1;

    btnOK.disabled = true;

    if (bHasRowsInGrid) {
        for (i = 2; i < fg.Rows; i++) {
            if (getCellValueByColKey(fg, 'OK', i) != 0) {
                btnOK.disabled = false;
                break;
            }
        }
    }

    btnFillGrid.disabled = false;
}

function setTotalColumns() {
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    var nValue = 0;
    var nCounter = 0;

    if (bHasRowsInGrid) {
        for (i = 2; i < fg.Rows; i++) {
            if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
                nValue += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor*'));
                nCounter++;
            }
        }
    }

    fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor*')) = nValue;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'FormaPagamento*')) = nCounter;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    var aGrid = null;
    var i = 0;
    var sFiltroValorID = '';
    var sFiltro = trimStr(txtFiltro.value);
    var sData = trimStr(txtData.value);
    var sFiltroData = '';
    var sMotivo = '';

    sFiltro = (sFiltro == '' ? '' : ' AND ' + sFiltro);

    lockControlsInModalWin(true);

    glb_nDSOs = 1;

    if (glb_sCaller == 'S')
        sFiltroValorID = ' AND a.ValorID=' +
			sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ValorID' + '\'' + '].value') +
			' ';

    // zera o grid
    fg.Rows = 1;

    if (sData != '') {
        sData = normalizeDate_DateTime(sData, 1);

        sFiltroData = '\'' + sData + '\'';
    }
    else
        sFiltroData = 'dbo.fn_Data_Zero(GETDATE())';

    if (chkMotivo.checked)
        sMotivo = ' AND a.MotivoDevolucao IS NOT NULL ';
    else
        sMotivo = ' AND a.MotivoDevolucao IS NULL ';

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT dbo.fn_Pessoa_Fantasia(a.EmpresaID, 0) AS Empresa, a.ValorID AS ValorID, l.RecursoAbreviado AS Estado, c.ItemAbreviado AS FormaPagamento, a.MotivoDevolucao, ' +
			'a.dtEmissao AS dtEmissao, a.dtApropriacao AS dtApropriacao, a.BancoAgencia AS BancoAgencia, ' +
			'a.NumeroDocumento AS NumeroDocumento, a.Historico, d.SimboloMoeda AS MoedaValLocalizar, a.Valor AS ValorVL,' +
			'b.Valor AS Valor, CONVERT(BIT, 0) AS OK, e.FinanceiroID AS FinanceiroID, ' +
			'e.PedidoID, e.Duplicata, h.Fantasia AS Pessoa, f.ItemAbreviado AS FormaPagamentoFinanceiro, ' +
			'e.PrazoPagamento, e.dtVencimento AS dtVencimento, g.SimboloMoeda AS MoedaFinanceiro, ' +
			'e.Valor AS ValorFinanceiro, ' +
			'dbo.fn_Financeiro_Posicao(e.FinanceiroID, 9, NULL, a.dtEmissao, NULL, NULL) AS Saldo, ' +
			'dbo.fn_Financeiro_Posicao(e.FinanceiroID, 8, NULL, a.dtEmissao, NULL, NULL) AS SaldoAtual, ' +
			'i.Fantasia AS Colaborador, ' +
			'j.HistoricoPadrao, ' +
			'b.ValFinanceiroID ' +
		'FROM ValoresLocalizar a WITH(NOLOCK) ' +
            'INNER JOIN ValoresLocalizar_Financeiros b WITH(NOLOCK) ON (a.ValorID = b.ValorID) ' +
            'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (a.FormaPagamentoID = c.ItemID) ' +
            'INNER JOIN Conceitos d WITH(NOLOCK) ON (a.MoedaID = d.ConceitoID) ' +
            'INNER JOIN Financeiro e WITH(NOLOCK) ON (b.FinanceiroID = e.FinanceiroID) ' +
            'LEFT OUTER JOIN HistoricosPadrao j WITH(NOLOCK) ON (e.HistoricoPadraoID = j.HistoricoPadraoID) ' +
            'INNER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (e.FormaPagamentoID = f.ItemID) ' +
            'INNER JOIN Conceitos g WITH(NOLOCK) ON (e.MoedaID = g.ConceitoID) ' +
            'INNER JOIN Pessoas h WITH(NOLOCK) ON (e.PessoaID = h.PessoaID) ' +
            'INNER JOIN Pessoas i WITH(NOLOCK) ON (e.ProprietarioID = i.PessoaID) ' +
            'INNER JOIN Recursos l WITH(NOLOCK) ON (a.EstadoID = l.RecursoID) ' +
		'WHERE (a.EstadoID IN (41,53,56) AND (dbo.fn_Empresa_Matriz(a.EmpresaID, NULL, 1) = ' + glb_aEmpresaData[0] + ') AND b.OK = 1 AND ' +
			'a.TipoValorID = ' + glb_nTipoValorID + ' AND a.FormaPagamentoID <> 1032 AND ' +
			'dbo.fn_ValorLocalizarFinanceiro_Posicao(b.ValFinanceiroID, 2) > 0 AND ' +
			'e.EstadoID <> 1 AND ' +
			'a.dtEmissao <= ' + sFiltroData + sFiltro + sFiltroValorID + sMotivo + ') ' +
	'UNION ALL SELECT dbo.fn_Pessoa_Fantasia(a.EmpresaID, 0) AS Empresa, a.ValorID AS ValorID, l.RecursoAbreviado AS Estado, c.ItemAbreviado AS FormaPagamento, a.MotivoDevolucao, ' +
			'a.dtEmissao AS dtEmissao, a.dtApropriacao AS dtApropriacao, a.BancoAgencia AS BancoAgencia, ' +
			'a.NumeroDocumento AS NumeroDocumento, a.Historico,d.SimboloMoeda AS MoedaValLocalizar, a.Valor AS ValorVL,' +
			'b.Valor AS Valor, CONVERT(BIT, 0) AS OK, e.FinanceiroID AS FinanceiroID, ' +
			'e.PedidoID, e.Duplicata, h.Fantasia AS Pessoa, f.ItemAbreviado AS FormaPagamentoFinanceiro, ' +
			'e.PrazoPagamento, e.dtVencimento AS dtVencimento, g.SimboloMoeda AS MoedaFinanceiro, ' +
			'e.Valor AS ValorFinanceiro, ' +
			'dbo.fn_Financeiro_Posicao(e.FinanceiroID, 9, NULL, a.dtApropriacao, NULL, NULL) AS Saldo, ' +
			'dbo.fn_Financeiro_Posicao(e.FinanceiroID, 8, NULL, a.dtApropriacao, NULL, NULL) AS SaldoAtual, ' +
			'i.Fantasia AS Colaborador, ' +
			'j.HistoricoPadrao, ' +
			'b.ValFinanceiroID ' +
		'FROM ValoresLocalizar a WITH(NOLOCK) ' +
            'INNER JOIN ValoresLocalizar_Financeiros b WITH(NOLOCK) ON (a.ValorID = b.ValorID) ' +
            'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (a.FormaPagamentoID = c.ItemID) ' +
            'INNER JOIN Conceitos d WITH(NOLOCK) ON (a.MoedaID = d.ConceitoID) ' +
            'INNER JOIN Financeiro e WITH(NOLOCK) ON (b.FinanceiroID = e.FinanceiroID) ' +
            'LEFT OUTER JOIN HistoricosPadrao j WITH(NOLOCK) ON (e.HistoricoPadraoID = j.HistoricoPadraoID) ' +
            'INNER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (e.FormaPagamentoID = f.ItemID) ' +
            'INNER JOIN Conceitos g WITH(NOLOCK) ON (e.MoedaID = g.ConceitoID) ' +
            'INNER JOIN Pessoas h WITH(NOLOCK) ON (e.PessoaID = h.PessoaID) ' +
            'INNER JOIN Pessoas i WITH(NOLOCK) ON (e.ProprietarioID = i.PessoaID) ' +
            'INNER JOIN Recursos l WITH(NOLOCK) ON (a.EstadoID = l.RecursoID) ' +
		'WHERE (a.EstadoID IN (41,53,56) AND (dbo.fn_Empresa_Matriz(a.EmpresaID, NULL, 1) = ' + glb_aEmpresaData[0] + ') AND b.OK = 1 AND ' +
			'a.TipoValorID = ' + glb_nTipoValorID + ' AND a.FormaPagamentoID = 1032 AND ' +
			'dbo.fn_ValorLocalizarFinanceiro_Posicao(b.ValFinanceiroID, 2) > 0 AND ' +
			'e.EstadoID <> 1 AND ' +
			'a.dtApropriacao <= ' + sFiltroData + sFiltro + sFiltroValorID + sMotivo + ') ' +
		'ORDER BY Empresa, ValorID';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;

    try {
        dsoGrid.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
        txtFiltro.focus();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;

    if (glb_nDSOs > 0)
        return null;

    if (glb_bFirstFill) {
        glb_bFirstFill = false;
        showExtFrame(window, true);

        window.focus();

        if (!txtData.disabled)
            txtData.focus();
    }

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
    var bTempValue;

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg, ['Empresa',
                   'ValorID',
				   'OK',
				   'E',
				   'For',
				   'Mot',
				   'Emiss�o',
				   'Apropria��o',
				   'Bco/Ag',
				   'N�m',
				   'Hist�rico',
				   '$',
				   'Valor VL',
				   'Valor',
				   'Financ',
				   'Pedido',
				   'Duplicata',
				   'Pessoa',
				   'For',
				   'Prazo',
				   'Vencimento',
				   '$',
				   'Valor',
				   'Saldo',
				   'Saldo Atual',
				   'Colaborador',
				   'Hist�rico',
				   'ValFinanceiroID'], [26]);

    fillGridMask(fg, dsoGrid, ['Empresa',
                                'ValorID*',
							 'OK',
							 'Estado*',
							 'FormaPagamento*',
							 'MotivoDevolucao*',
							 'dtEmissao*',
							 'dtApropriacao*',
							 'BancoAgencia*',
							 'NumeroDocumento*',
							 'Historico*',
							 'MoedaValLocalizar*',
							 'ValorVL*',
							 'Valor*',
							 'FinanceiroID*',
							 'PedidoID*',
							 'Duplicata*',
							 'Pessoa*',
							 'FormaPagamentoFinanceiro*',
							 'PrazoPagamento*',
							 'dtVencimento*',
							 'MoedaFinanceiro*',
							 'ValorFinanceiro*',
							 'Saldo*',
							 'SaldoAtual*',
							 'Colaborador*',
							 'HistoricoPadrao*',
							 'ValFinanceiroID*'],
							 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
							 ['##########', '', '', '', '', '', dTFormat, dTFormat, '', '', '', '', '###,###,##0.00', '###,###,##0.00',
								'##########', '##########', '', '', '', '#####', dTFormat, '',
								'###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '', '', '']);
    fg.Redraw = 0;

    alignColsInGrid(fg, [1, 12, 13, 14, 15, 19, 22, 23, 24]);

    // Linha de Totais
    gridHasTotalLine(fg, 'Total', 0xC0C0C0, null, true, [[11, '###,###,###,###.00', 'S']]);

    if (fg.Rows > 1) {
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor*')) = 0;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'FormaPagamento*')) = 0;
    }

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.FrozenCols = 2;

    paintCellsSpecialyReadOnly();

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        fg.Editable = true;
        window.focus();
        fg.focus();
        fg.Col = 1;
    }
    else {
        ;
    }

    // Merge de Colunas
    fg.MergeCells = 4;
    //fg.MergeCol(-1) = true;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;
    fg.MergeCol(5) = true;
    fg.MergeCol(6) = true;
    fg.MergeCol(7) = true;
    fg.MergeCol(8) = true;
    fg.MergeCol(9) = true;
    fg.MergeCol(10) = true;

    fg.Redraw = 2;

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;
    var nValorID = 0;

    lockControlsInModalWin(true);
    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    for (i = 2; i < fg.Rows; i++) {
        if ((getCellValueByColKey(fg, 'OK', i) != 0) && (nValorID != getCellValueByColKey(fg, 'ValorID*', i))) {
            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                    strPars = '';
                    nDataLen = 0;
                }

                strPars += '?nUserID=' + escape(glb_nUserID);
            }

            nDataLen++;
            //strPars += '&nValFinanceiroID=' + escape(getCellValueByColKey(fg, 'ValFinanceiroID*', i));
            strPars += '&nValorID=' + escape(getCellValueByColKey(fg, 'ValorID*', i));
            nValorID = getCellValueByColKey(fg, 'ValorID*', i);
        }
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);

    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/gerarocorrencia.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            lockControlsInModalWin(false);
            glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        dsoGrava.recordset.MoveFirst();
        if ((dsoGrava.recordset['Resultado'].value != null) &&
			 (dsoGrava.recordset['Resultado'].value != '')) {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0)
                return null;

            glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
            return null;
        }
    }

    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

function listar() {
    if (!verificaData())
        return null;

    if (trimStr(txtData.value) == '') {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        window.focus();
        txtData.focus();

        return null;
    }

    if (daysBetween(glb_dCurrDate, trimStr(txtData.value)) > 0) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        window.focus();
        txtData.focus();

        return null;
    }

    refreshParamsAndDataAndShowModalWin(false);
}

function verificaData() {
    var sData = trimStr(txtData.value);
    var bDataIsValid = true;

    if (sData != '')
        bDataIsValid = chkDataEx(txtData.value);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        window.focus();

        if (sData != '')
            txtData.focus();

        return false;
    }
    return true;
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    lockControlsInModalWin(false);

    glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly() {
    paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgeraocorrenciasBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgeraocorrenciasDblClick(grid, Row, Col) {
    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if (getColIndexByColKey(grid, 'OK') != Col)
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);

    glb_PassedOneInDblClick = true;

    // limpa coluna se tem um check box checado
    for (i = 2; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 2; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    setTotalColumns();

    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgeraocorrenciasKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgeraocorrencias_ValidateEdit() {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgeraocorrencias_BeforeEdit(grid, row, col) {
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if (fldDet == null)   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ((fldDet[0] == 200) || (fldDet[0] == 129))  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgeraocorrencias_AfterEdit(Row, Col) {
    setupBtnsFromGridState();
    setTotalColumns();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalgeraocorrencias(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return false;

    setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************
