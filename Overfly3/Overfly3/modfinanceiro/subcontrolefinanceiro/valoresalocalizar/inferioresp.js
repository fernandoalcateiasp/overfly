/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT * FROM ValoresLocalizar a WHERE '+
        sFiltro + 'a.ValorID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28041) // Financeiros
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM ValoresLocalizar_Financeiros a WITH(NOLOCK) ' +
                  'WHERE '+ sFiltro + 'a.ValorID = ' + idToFind + ' ' +
                  'ORDER BY a.FinanceiroID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28042) // Ocorrencias
    {
        dso.SQL = 'SELECT c.FinanceiroID,c.PedidoID,d.Fantasia,b.FinOcorrenciaID,e.ItemMasculino as TipoOcorrencia,' +
				'b.dtOcorrencia, f.SimboloMoeda as Moeda, b.ValorConvertido, ' +
				'dbo.fn_Financeiro_Posicao(c.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, ' +
				'dbo.fn_Financeiro_Posicao(c.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, ' +
				'g.Fantasia AS Colaborador ' +
			'FROM ValoresLocalizar_Financeiros a WITH(NOLOCK), Financeiro_Ocorrencias b WITH(NOLOCK), Financeiro c WITH(NOLOCK), Pessoas d WITH(NOLOCK), TiposAuxiliares_Itens e WITH(NOLOCK), ' +
				'Conceitos f WITH(NOLOCK), Pessoas g WITH(NOLOCK) ' +
			'WHERE (' + sFiltro + ' a.ValorID = ' + idToFind + ' AND ' +
				'a.ValFinanceiroID = b.ValFinanceiroID AND b.FinanceiroID = c.FinanceiroID AND ' +
				'c.PessoaID=d.PessoaID AND b.TipoOcorrenciaID = e.ItemID AND c.MoedaID = f.ConceitoID) AND ' +
				'b.ColaboradorID = g.PessoaID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28043) // Depositos Bancarios
    {
        dso.SQL = 'SELECT (SELECT TOP 1 aa.RecursoFantasia FROM Recursos aa WITH(NOLOCK) WHERE (aa.RecursoID = 9150)) AS Form, ' +
                            '(SELECT TOP 1 aa.RecursoFantasia FROM Recursos aa WITH(NOLOCK) WHERE (aa.RecursoID = 28080)) AS SubForm, ' +
                            'NULL AS Tipo, ' +
                            'dbo.fn_DepositoBancario_Tipo(b.DepositoID) AS SubTipo, ' +
                            'b.DepositoID AS RegistroID, c.RecursoAbreviado AS Estado, ' +
				            'b.dtData AS Data, dbo.fn_ContaBancaria_Nome(b.RelPescontaID, 4) AS ContaBancaria, ' +
				            'dbo.fn_DepositoBancario_Posicao(b.DepositoID, 3) AS Quantidade, d.SimboloMoeda AS Moeda, ' +
				            'dbo.fn_DepositoBancario_Posicao(b.DepositoID, 8) AS Total ' +
			            'FROM DepositosBancarios_ValoresLocalizar a WITH(NOLOCK) ' +
			                    'INNER JOIN DepositosBancarios b WITH(NOLOCK) ON (a.DepositoID = b.DepositoID) ' +
			                    'INNER JOIN Recursos c WITH(NOLOCK) ON (b.EstadoID = c.RecursoID) ' +
			                    'INNER JOIN Conceitos d WITH(NOLOCK) ON (b.MoedaID = d.ConceitoID) ' +
			            'WHERE (' + sFiltro + ' a.ValorID = ' + idToFind + ') ' +
                    ' UNION ALL ' +
                    'SELECT (SELECT TOP 1 aa.RecursoFantasia FROM Recursos aa WITH(NOLOCK) WHERE (aa.RecursoID = 9110)) AS Form, ' +
                            '(SELECT TOP 1 aa.RecursoFantasia FROM Recursos aa WITH(NOLOCK) WHERE (aa.RecursoID = 28000)) AS SubForm, ' +
                            'dbo.fn_TipoAuxiliar_Item(b.TipoFinanceiroID, 0) AS Tipo, ' +
                            'dbo.fn_Financeiro_DepositoBancario_Tipo(b.FinanceiroID) AS SubTipo, ' +
                            'b.FinanceiroID AS RegistroID, c.RecursoAbreviado AS Estado, ' +
                            'b.dtEmissao AS Data, dbo.fn_ContaBancaria_Nome(b.RelPescontaID, 4) AS ContaBancaria, ' +
                            'dbo.fn_DepositoBancario_Posicao(b.FinanceiroID, 3) AS Quantidade, d.SimboloMoeda AS Moeda, ' +
                            'dbo.fn_DepositoBancario_Posicao(b.FinanceiroID, 8) AS Total ' +
                        'FROM Financeiro_ValoresDepositados a WITH(NOLOCK) ' +
                                'INNER JOIN Financeiro b WITH(NOLOCK) ON (a.FinanceiroID = b.FinanceiroID) ' +
                                'INNER JOIN Recursos c WITH(NOLOCK) ON (b.EstadoID = c.RecursoID) ' +
                                'INNER JOIN Conceitos d WITH(NOLOCK) ON (b.MoedaID = d.ConceitoID) ' +
                        'WHERE (' + sFiltro + ' a.ValorID = ' + idToFind + ') ';
			        
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28044) // Lancamentos contabeis
    {
		dso.SQL = 'EXEC sp_EventosContabeis_Lancamentos ' + glb_nFormID + ', NULL, ' + idToFind;
		return 'dso01Grid_DSC';
    }
    else if (folderID == 28048) // Valores Filhos
    {
        dso.SQL = 'SELECT b.ItemAbreviado, a.ValorID, c.RecursoAbreviado, a.MotivoDevolucao, a.Documento, a.Emitente, a.dtEmissao, ' +
	                'a.dtApropriacao, a.BancoAgencia, a.NumeroDocumento, d.SimboloMoeda, a.Valor ' +
	                'FROM ValoresLocalizar a WITH(NOLOCK) ' +
	                'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.FormaPagamentoID = b.ItemID ' +
	                'INNER JOIN Recursos c WITH(NOLOCK) ON a.EstadoID = c.RecursoID ' +
	                'INNER JOIN Conceitos d WITH(NOLOCK) ON a.MoedaID = d.ConceitoID ' +
	                'WHERE a.ValorReferenciaID = ' + idToFind + ' ' +
	                'ORDER BY a.ValorID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28049) // Lancamentos Bancos
    {
        dso.SQL = 'SELECT a.dtEmissao, a.ExtratoID, a.Concilia, c.RecursoAbreviado, d.Fantasia, b.dtLancamento, b.Historico, b.Documento, b.Valor, b.Identificador, ' + 
                    'b.TipoLancamento, b.NaoConcilia ' +
	                    'FROM ExtratosBancarios a WITH(NOLOCK) ' +
	                    'INNER JOIN ExtratosBancarios_Lancamentos b WITH(NOLOCK) ON a.ExtratoID = b.ExtratoID ' +
	                    'INNER JOIN Recursos c WITH(NOLOCK) ON (a.EstadoID = c.RecursoID) ' +
	                    'INNER JOIN Pessoas d WITH(NOLOCK) ON (a.EmpresaID = d.PessoaID) ' +
	                    'WHERE b.ValorID = ' + idToFind + ' ' +
	                    'ORDER BY a.dtEmissao';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Financeiros
    else if ( pastaID == 28041 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT a.ValFinanceiroID, c.SimboloMoeda, ' +
						'CONVERT(BIT, (CASE WHEN dbo.fn_ValorLocalizarFinanceiro_Posicao(a.ValFinanceiroID, 2) = 0 THEN 1 ELSE 0 END)) AS Aplicado, ' +
						'd.RecursoAbreviado, b.PedidoID, b.Duplicata, e.Fantasia AS Pessoa, ' +
						'f.ItemAbreviado, b.PrazoPagamento, b.dtEmissao, b.dtVencimento, b.Valor, ' +
						'dbo.fn_Financeiro_Posicao(b.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, ' +
						'dbo.fn_Financeiro_Posicao(b.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, ' +
						'g.Fantasia AS Colaborador, h.Fantasia AS Usuario, ' +
						'dbo.fn_ValorLocalizarFinanceiro_Posicao(a.ValFinanceiroID,2) AS SaldoOcorrencia ' +
                      'FROM ValoresLocalizar_Financeiros a WITH(NOLOCK) ' +
                        'LEFT OUTER JOIN Pessoas h WITH(NOLOCK) ON (a.UsuarioID=h.PessoaID) ' +
                        'INNER JOIN Financeiro b WITH(NOLOCK) ON (a.FinanceiroID = b.FinanceiroID) ' +
                        'INNER JOIN Conceitos c WITH(NOLOCK) ON (b.MoedaID = c.ConceitoID) ' +
                        'INNER JOIN Recursos d WITH(NOLOCK) ON (b.EstadoID = d.RecursoID) ' +
                        'INNER JOIN Pessoas e WITH(NOLOCK) ON (b.PessoaID = e.PessoaID) ' +
                        'INNER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (b.FormaPagamentoID = f.ItemID) ' +
                        'INNER JOIN Pessoas g WITH(NOLOCK) ON (b.ProprietarioID = g.PessoaID) ' +
                      'WHERE (a.ValorID = ' + nRegistroID + ')';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
	var nFormaPagamentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,'dsoSup01.recordset[\'FormaPagamentoID\'].value');

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(28041); 
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 28041); // Financeiros

    vPastaName = window.top.getPastaName(28042); 
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 28042); // Ocorrencias

	if ((nFormaPagamentoID == 1031) || (nFormaPagamentoID == 1032))
	{
		vPastaName = window.top.getPastaName(28043);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 28043); // Depositos Bancarios
	}

    vPastaName = window.top.getPastaName(28044);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28044); // Lancamentos Contabeis

    vPastaName = window.top.getPastaName(28048);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28048); // Valores Filhos

    vPastaName = window.top.getPastaName(28049);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28049); // Lancamentos Bancos

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 28041) //Financeiro
            currLine = criticLineGridAndFillDsoEx(fg,currDSO,keepCurrLineInGrid(),[0],['ValorID',registroID],[]);

        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 28041) // Financeiros
    {
        headerGrid(fg,['Financeiro',
                       'Valor',
                       'Valor Encargos',
                       //'Saldo Ocorr',
                       'OK',
                       'A',
                       'Observa��o',
                       'Usu�rio',
                       'Est',
                       'Pedido',
                       'Duplicata',
                       'Pessoa',
                       'Forma',
                       'Prazo',
                       'Emiss�o',
                       'Vencimento',
                       '$',
                       'Valor',
                       'Saldo',
                       'Saldo Atual',
                       'Colaborador',
                       'ValFinanceiroID'], [20]);

        glb_aCelHint = [[0,5,'Aplicado? (Baixado no Financeiro)']];

        fillGridMask(fg,dso01Grid,['FinanceiroID*',
                                   'Valor',
                                   'ValorEncargos',
                                   //'^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^SaldoOcorrencia*',
                                   'OK',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^Aplicado*',
                                   'Observacao',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^Usuario*',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^RecursoAbreviado*',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^PedidoID*',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^Duplicata*',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^Pessoa*',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^ItemAbreviado*',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^PrazoPagamento*',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^dtEmissao*',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^dtVencimento*',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^SimboloMoeda*',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^Valor*',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^SaldoDevedor*',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^SaldoAtualizado*',
                                   '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^Colaborador*',
                                   'ValFinanceiroID'],
									['', '999999999.99', '999999999.99', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
									['', '###,###,###.00', '###,###,###.00', '', '', '', '', '', '', '', '', '', '', dTFormat, dTFormat, '', '###,###,###.00', '###,###,###.00', '###,###,###.00', '', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1, '###,###,###,###.00', 'S'],
                                                              [2, '###,###,###,###.00', 'S'],
		                                                      //[3, '###,###,###,###.00', 'S'],
															  [9,'########','C'], 
															  [17,'###,###,###,###.00','S'],
															  [18,'###,###,###,###.00','S'],
															  [19,'###,###,###,###.00','S']]);


        // colunas a serem alinhadas a direita
        alignColsInGrid(fg,[0,1,2,9,13,17,18,19]);
        
        fg.FrozenCols = 1;
    }
    else if (folderID == 28042) // Ocorrencias
    {
        headerGrid(fg,['Financeiro',
                       'Pedido',
                       'Pessoa',
                       'Tipo',
                       'Data',
                       'Moeda',
                       'Valor',
                       'Saldo Devedor',
                       'Saldo Atual',
                       'Colaborador'], []);
                       
        fillGridMask(fg,currDSO,['FinanceiroID',
                                 'PedidoID',
                                 'Fantasia',
                                 'TipoOcorrencia',
                                 'dtOcorrencia',
                                 'Moeda',
                                 'ValorConvertido',
								 'SaldoDevedor',
								 'SaldoAtualizado',
								 'Colaborador'],
                                 ['','','','','','','999999999.99','999999999.99','',''],
                                 ['','','','','','','###,###,###,###.00','###,###,###,###.00','###,###,###,###.00','']);

        // colunas a serem alinhadas a direita
        alignColsInGrid(fg,[0,1,6,7,8]);

        fg.FrozenCols = 1;

        // Pinta celulas de vermelho caso negativo
        var nValorCelula;
        var i, j;

        for (i = 1; i < fg.Rows; i++) {
            for (j = 6; j <= 7; j++) {
                nValorCelula = fg.ValueMatrix(i, j);
                if ((!(isNaN(nValorCelula))) && (nValorCelula < 0)) {
                    fg.Select(i, j, i, j);
                    fg.FillStyle = 1;
                    fg.CellForeColor = 0X0000FF;
                }
            }
        }
    }
    else if (folderID == 28043) // Depositos Bancarios
    {
        headerGrid(fg, ['Form',
                        'SubForm',
                        'Tipo',
                        'SubTipo',                                                
                        'Registro',
                        'Est',
                        'Data',
                        'Conta',
                        'Quant',
                        'Moeda',
                        'Total'], []);

        fillGridMask(fg, currDSO, ['Form',
								     'SubForm',
								     'Tipo',
								     'SubTipo',
								     'RegistroID',
                                     'Estado',
                                     'Data',
                                     'ContaBancaria',
                                     'Quantidade',
                                     'Moeda',
                                     'Total'],
                                     ['','','','','','','','','','',''],
                                     ['','','','','','',dTFormat,'','','','###,###,###,###.00']);

        // colunas a serem alinhadas a direita
        alignColsInGrid(fg,[0,4,6]);

        fg.FrozenCols = 1;
    }
    else if (folderID == 28044) // Lancamentos contabeis
    {
        headerGrid(fg,['Lan�amento',
                        'Est',
                        'Data',
                        'Balancete',
                        'Est',
                        'Tipo',
                        'Quant',
                        'Hist�rico Padr�o',
                        'Hist�rico Complementar',
                        'Detalhe',
                        'D',
                        'C',
                        'Moeda',
                        'Total Lan�amento',
                        'Taxa',
                        'Total Convertido',
                        'Observa��o',
                        'EventoID'], [17]);
                       
        glb_aCelHint = [[0,4,'� estorno?'],
						[0,6,'Quantidade de detalhes'],
						[0,10,'N�mero de D�bitos'],
						[0,11,'N�mero de Cr�ditos'],
						[0,11,'Taxa de convers�o para moeda comum']];

        fillGridMask(fg,currDSO,['LancamentoID',
                                 'Estado',
                                 'dtLancamento',
                                 'dtBalancete',
                                 'EhEstorno',
                                 'TipoLancamento',
                                 'Quantidade',
                                 'HistoricoPadrao',
                                 'HistoricoComplementar',
                                 'Detalhe',
                                 'NumeroDebitos',
                                 'NumeroCreditos',
                                 'Moeda',
                                 'ValorTotalLancamento',
                                 'TaxaMoeda',
                                 'ValorTotalConvertido',
                                 'Observacao',
                                 'EventoID'],
                                 ['9999999999','','99/99/9999','99/99/9999','','','999999','','','','99','99','',
									'999999999.99','9999999999999.99','999999999.99','',''],
								 ['##########','',dTFormat,dTFormat,'','','######','','','','##','##','',
									'###,###,##0.00','#,###,###,###,##0.0000000','###,###,##0.00','','']);

        alignColsInGrid(fg,[0,6,10,11,13,14,15]);
        fg.FrozenCols = 2;
    }
    else if (folderID == 28048) // Valores Filhos
    {
        headerGrid(fg, ['Forma',
					   'ValorID',
					   'Est',
					   'Mot',
					   'Documento',
					   'Emitente',
					   'Emiss�o',
					   'Apropria��o',
					   'Banco',
					   'N�mero',
					   '$',
					   'Valor'], [9]);

        with (fg) {
            ColDataType(0) = 8;
            ColDataType(1) = 3;
            ColDataType(2) = 8;
            ColDataType(3) = 8;
            ColDataType(4) = 8;
            ColDataType(5) = 8;
            ColDataType(8) = 8;
            ColDataType(9) = 8;
            ColDataType(10) = 8;
        }

        fillGridMask(fg, currDSO, ['ItemAbreviado*',
								 'ValorID*',
								 'RecursoAbreviado*',
								 'MotivoDevolucao*',
								 'Documento*',
								 'Emitente*',
								 'dtEmissao*',
								 'dtApropriacao*',
								 'BancoAgencia*',
								 'NumeroDocumento*',
								 'SimboloMoeda*',
								 'Valor*'],
								 ['', '9999999999', '', '', '', '', '99/99/9999', '99/99/9999', '', '', '', '999999999.99'],
                                 ['', '##########', '', '', '', '', dTFormat, dTFormat, '', '', '', '(###,###,##0.00)']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1, '######', 'C'], [11, '###,###,###.00', 'S']]);

        alignColsInGrid(fg, [1, 11]);
        fg.FrozenCols = 2;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 28049) // Lancamentos Bancos
    {
        headerGrid(fg, ['Emiss�o',
                        'Extrato',
                        'Concilia',
                        'Est',
                        'Empresa',
                        'Data',
                        'Hist�rico',
                        'Documento',
                        'Valor',
                        'Identificador',
                        'Tipo',
                        'NC'], []);

        fillGridMask(fg, currDSO, ['dtEmissao',
                                     'ExtratoID',
                                     'Concilia',
                                     'RecursoAbreviado',
                                     'Fantasia',
                                     'dtLancamento',
                                     'Historico',
                                     'Documento',
                                     'Valor',
                                     'Identificador',
                                     'TipoLancamento',
                                     'NaoConcilia'],
                                     ['99/99/9999', '', '', '', '', '99/99/9999', '', '', '', '', '', ''],
								     [dTFormat, '', '', '', '', dTFormat, '', '', '(###,###,##0.00)', '', '', '']);

        for (i = 1; i < fg.Rows; i++) 
        {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')) < 0)
            {
                fg.Select(i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor'));
                fg.FillStyle = 1;
                fg.CellForeColor = 0X0000FF;
            }
        }

        alignColsInGrid(fg, [1, 7]);
        fg.FrozenCols = 2;
    }
}
