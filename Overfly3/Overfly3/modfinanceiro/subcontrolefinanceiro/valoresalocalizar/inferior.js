/********************************************************************
inferior.js

Library javascript para o recursosinf01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var __winTimerHandler = null;
var glb_bForceRefrInf = false;
var glb_nSaldo = '1';
var glb_nVariacaoValor = '0';
var glb_nVariacaoDias = '0';

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_localTimerRefresh = null;
var glb_nMaxStringSize = 1024 * 1;
var dso01JoinSup = new CDatatransport('dso01JoinSup');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoDynamicCmbs = new CDatatransport('dsoDynamicCmbs');
var dsoGeraOcorrencia = new CDatatransport('dsoGeraOcorrencia');
var dsoStateMachineLkp = new CDatatransport('dsoStateMachineLkp');
var dsoFiltroInf = new CDatatransport('dsoFiltroInf');
var dso01Grid = new CDatatransport('dso01Grid');
var dsoCmb01Grid_01 = new CDatatransport('dsoCmb01Grid_01');
var dsoCmb01Grid_02 = new CDatatransport('dsoCmb01Grid_02');
var dsoCmb01Grid_03 = new CDatatransport('dsoCmb01Grid_03');
var dsoCmb01Grid_04 = new CDatatransport('dsoCmb01Grid_04');
var dso01GridLkp = new CDatatransport('dso01GridLkp');
var dso02GridLkp = new CDatatransport('dso02GridLkp');
var dso03GridLkp = new CDatatransport('dso03GridLkp');
var dso04GridLkp = new CDatatransport('dso04GridLkp');
var dsoCmbsPropAlt = new CDatatransport('dsoCmbsPropAlt');
var dsoGravar = new CDatatransport('dsoGravar');
var dsoEstornar = new CDatatransport('dsoEstornar');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    adjustElementsInDivsNonSTD()
    putSpecialAttributesInControls()
    optChangedInCmb(cmb)
    prgServerInf(btnClicked)
    prgInterfaceInf(btnClicked, pastaID, dso)
    finalOfInfCascade(btnClicked)
    treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, currDSO)
    btnLupaClicked(btnClicked)
    addedLineInGrid(folderID, grid, nLineInGrid, dso)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    btnAltPressedInGrid( folderID )
    btnAltPressedInNotGrid( folderID )
    fillCmbFiltsInfAutomatic(folderID)
    selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
    frameIsAboutToApplyRights(folderID)


FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG
    fg_AfterRowColChange_Prg()
    fg_DblClick_Prg()
    fg_ChangeEdit_Prg()
    fg_ValidateEdit_Prg()
    fg_BeforeRowColChange_Prg()
    fg_EnterCell_Prg()
    fg_MouseUp_Prg()
    fg_MouseDown_Prg()
    fg_BeforeEdit_Prg()

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function window_onload() {
    // Ze em 17/03/08
    dealWithGrid_Load();

    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array dos ids dos dsos que tenham campos linkados
    // a controles HTMLs.
    // Estes controles HTMls estao em divs e sao linkados a campos do dso
    // que sejam: obrigatorios, numericos ou de datas, nos dsos.
    // Se nao aplicavel ao form este array deve ser null
    glb_aDsosToCloneFieldsEtc = ['dso01JoinSup'];

    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;

    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 
    glb_aGridStaticCombos = [[20010, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', ''],
                                       [0, 'dso04GridLkp', '', '']]],
							  [28041, [[0, 'dso01GridLkp', '', '']]]];

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03'],
                               [20008, 20009, [20010, 28041, 28042, 28043, 28044, 28048, 28049]]);

    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();

}

function setupPage() {
    //@@ Ajustar os divs
    // Nada a codificar
}

function fg_AfterRowColChange_Prg() {

}

function fg_DblClick_Prg() {
    var aEmpresa = getCurrEmpresaData();
    if ((keepCurrFolder() == 28042) && (fg.Rows > 1))// Ocorrencias
    {
        // Manda o id do financeiro a detalhar
        sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(aEmpresa[0], fg.TextMatrix(fg.Row, 0)));
    }
    else if ((keepCurrFolder() == 28043) && (!fg.Editable)) {
        if (getCellValueByColKey(fg, 'SubForm', fg.Row) == 'Dep�sitos Banc�rios')
            sendJSCarrier(getHtmlId(), 'SHOWDEPOSITOBANCARIO', new Array(aEmpresa[0], fg.TextMatrix(fg.Row, 0)));

        else if (getCellValueByColKey(fg, 'SubForm', fg.Row) == 'Financeiro')
            sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(aEmpresa[0], fg.TextMatrix(fg.Row, 0)));
    }
    else if ((keepCurrFolder() == 28044) && (!fg.Editable))
        sendJSCarrier(getHtmlId(), 'SHOWLANCAMENTO', new Array(aEmpresa[0], fg.TextMatrix(fg.Row, 0)));
    else if ((keepCurrFolder() == 28049) && (!fg.Editable))
        sendJSCarrier(getHtmlId(), 'SHOWEXTRATOBANCARIO', new Array(aEmpresa[0], fg.TextMatrix(fg.Row, 0)));
}

function fg_ChangeEdit_Prg() {

}
function fg_ValidateEdit_Prg() {

}
function fg_BeforeRowColChange_Prg() {

}

function fg_EnterCell_Prg() {

}

function fg_MouseUp_Prg() {

}

function fg_MouseDown_Prg() {

}

function fg_BeforeEdit_Prg() {

}

// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD() {
    //@@
    // Este form nao tem este caso    
}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@
    // troca a pasta default para financeiros
    glb_pastaDefault = 28041;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario


    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div

    // Mover esta funcao para os finais retornos de operacoes no
    // banco de dados
    finalOfInfCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    showBtnsEspecControlBar('inf', false, [1, 1, 1, 1]);
    tipsBtnsEspecControlBar('inf', ['', '', '', '']);

    if (pastaID == 28041) // Financeiros
    {
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 1, 1, 1]);
        tipsBtnsEspecControlBar('inf', ['Incluir Financeiro', 'Detalhar Financeiro', 'Detalhar Pedido', 'Alterar Associa��es', 'Fatura de Transporte', 'Estornar Financeiro']);
    }
    else if (pastaID == 28042) // ocorrencias
    {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Financeiro', '', '', '']);
    }
    else if (pastaID == 28043) // Depositos Bancarios
    {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Dep�sito Banc�rio', '', '', '']);
    }
    else if (pastaID == 28044) // Eventos Contabeis
    {
        showBtnsEspecControlBar('inf', true, [1, 1, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Lan�amento', 'Detalhar Contas', '', '']);
    }
    else if (pastaID == 28048) // Valores Filhos
    {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Valor', '', '', '']);
    }
    else if (pastaID == 28049) // Lancamentos Bancos
    {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Extrato', '', '', '']);
    }


    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked) {
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ((btnClicked.toUpperCase()).indexOf('SUP') >= 0)
        // usario clicou botao no control bar sup
        sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
    else
        // usario clicou botao no control bar inf
        sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================

}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID) {
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'EstadoID\'].value');
    var sMaeVisibility = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selValorReferenciaID.currentStyle.visibility');
    var sBotaoDetMae = 'D';
    var sHaveLineZero = 'D';
    var sBotaoEstornar = 'D';

    if (sMaeVisibility != 'hidden')
        sBotaoDetMae = 'H';
    else
        sBotaoDetMae = 'D';

    if ((nEstadoID == 48) || (nEstadoID == 41) || (nEstadoID == 56))
        sBotaoEstornar = 'H';
    else
        sBotaoEstornar = 'D';

    if ((nEstadoID == 41) || (nEstadoID == 56))
        setupEspecBtnsControlBar('sup', 'HHHHHDD' + sBotaoDetMae + sBotaoEstornar);
    else
        setupEspecBtnsControlBar('sup', 'HHHDDDD' + sBotaoDetMae + sBotaoEstornar);

    if ((folderID == 28041) && (fg.Rows >= 1)) // Financeiros
    {
        if (nEstadoID == 1)
            setupEspecBtnsControlBar('inf', 'HHHHH' + sBotaoEstornar);
        else if ((nEstadoID == 41) || (nEstadoID == 56))
            setupEspecBtnsControlBar('inf', 'HHHHD' + sBotaoEstornar);
        else
            setupEspecBtnsControlBar('inf', 'DHHDD' + sBotaoEstornar);

        if (fg.Cols > 1)
            fg.ColWidth(getColIndexByColKey(fg, '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^Aplicado*')) = fg.ColWidth(getColIndexByColKey(fg, 'OK'));

            // So habilita se estiver em estado Aberto(41) ou Cadastrado(1)

        else if ((nEstadoID == 1) || (nEstadoID == 51) || (nEstadoID == 41) || (nEstadoID == 53))
            setupEspecBtnsControlBar('inf', 'HDDDD' + sHaveLineZero + sBotaoEstornar);
        else
            setupEspecBtnsControlBar('inf', 'DDDDD' + sHaveLineZero + sBotaoEstornar);
    }
        // Ocorrencias
    else if ((folderID == 28042) && (fg.Rows > 1))
        setupEspecBtnsControlBar('inf', 'HDDD');
    else if ((folderID == 28043) && (fg.Rows > 1))
        setupEspecBtnsControlBar('inf', 'HDDD');
    else if ((folderID == 28044) && (fg.Rows > 1))
        setupEspecBtnsControlBar('inf', 'HHDD');
    else if ((folderID == 28048) && (fg.Rows > 1))
        setupEspecBtnsControlBar('inf', 'HDDD');
    else if ((folderID == 28049) && (fg.Rows > 1))
        setupEspecBtnsControlBar('inf', 'HDDD');
    else
        setupEspecBtnsControlBar('inf', 'DDDD');

    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR')
          || (btnClicked == 'SUPEST')
          || (btnClicked == 'SUPOK') || (btnClicked == 'INFREFR')
          || (btnClicked == 'INFOK') || (btnClicked == 'INFCANC')) {
        if (glb_bForceRefrInf == true) {
            glb_bForceRefrInf = false;
            __btn_REFR('inf');
        }
    }

    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'showHideData(dsoSup01.recordset[\'FormaPagamentoID\'].value, true)');
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso) {
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    var nFolder = keepCurrFolder();
    var aEmpresa = getCurrEmpresaData();

    if (nFolder == null)
        return null;

    // Grid de Financeiros botao 1 (chama a modal de pesquisa)
    if ((nFolder == '28041') && (controlBar.toUpperCase() == 'INF') && (btnClicked == 1)) {
        var nPessoaID, sFantasia, sdtEmissao, sdtApropriacao, htmlPath, nProcessoID;

        var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		                            ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));

        var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		                            ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));

        var nOK = 0;

        //DireitoEspecifico
        //Financeiro->Recebimento/Pagamentos->Financeiro-> Modal localizar Financeiro(B1)
        //28041 SFI-Grid Financeiro -> A1&&A2 -> Ao inserir o financeiro, marca a coluna ok igual a 1. Caso a pessoa do vl for nula, atualiza com o parceiro do financeiro.
        if ((nA1 == 1) && (nA2 == 1))
            nOK = 1;

        nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'PessoaID\'].value');
        sFantasia = '';

        sdtEmissao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtdtEmissao.value');
        nProcessoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selProcessoID.value');

        if (sdtEmissao != '')
            sdtEmissao = normalizeDate_DateTime(sdtEmissao, true);

        sdtApropriacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtdtApropriacao.value');
        if (sdtApropriacao != '')
            sdtApropriacao = normalizeDate_DateTime(sdtApropriacao, true);

        if (nPessoaID != null)
            sFantasia = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selPessoaID.options[selPessoaID.selectedIndex].innerText');
        else
            nPessoaID = 0;

        // Passar parametro se veio do inf ou do sup
        var strPars = new String();
        strPars = '?sCaller=' + escape('INF');
        strPars += '&nValorID=' + escape(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ValorID\'].value'));
        strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
        strPars += '&nTipoValorID=' + escape(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TipoValorID\'].value'));
        strPars += '&nFormaPagamentoID=' + escape(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'FormaPagamentoID\'].value'));
        strPars += '&nPessoaID=' + escape(nPessoaID);
        strPars += '&sFantasia=' + escape(sFantasia);
        strPars += '&sdtEmissao=' + escape(sdtEmissao);
        strPars += '&sdtApropriacao=' + escape(sdtApropriacao);
        strPars += '&nValor=' + escape(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'Valor\'].value'));
        strPars += '&nOK=' + escape(nOK);
        strPars += '&nProcessoID=' + escape(nProcessoID);

        htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modallocalizarfinanceiro.asp' + strPars;
        showModalWin(htmlPath + strPars, new Array(775, 460));
    }
        // Grid de Financeiros botao 2 (detalhar financeiro)
    else if ((nFolder == '28041') && (controlBar.toUpperCase() == 'INF') && (btnClicked == 2))
        // Manda o id do financeiro a detalhar
        sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(aEmpresa[0], getCellValueByColKey(fg, 'FinanceiroID*', fg.Row)));

        // Grid de Financeiros botao 3 (detalhar pedido)
    else if ((nFolder == '28041') && (controlBar.toUpperCase() == 'INF') && (btnClicked == 3))
        // Manda o id do pedido a detalhar
        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(aEmpresa[0], getCellValueByColKey(fg, '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^PedidoID*', fg.Row)));

        // Grid de Financeiros botao 4 (Alterar Associa��o)
    else if ((nFolder == '28041') && (controlBar.toUpperCase() == 'INF') && (btnClicked == 4)) {
        // Chama funcao para deletar so financeiros com a coluna A desmarcada
        var nPessoaID, sFantasia, sdtEmissao, sdtApropriacao, htmlPath, nProcessoID;

        var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		                            ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));

        var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		                            ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));

        var nOK = 0;

        if ((nA1 == 1) && (nA2 == 1))
            nOK = 1;

        nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'PessoaID\'].value');
        sFantasia = '';

        sdtEmissao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtdtEmissao.value');
        nProcessoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selProcessoID.value');

        if (sdtEmissao != '')
            sdtEmissao = normalizeDate_DateTime(sdtEmissao, true);

        sdtApropriacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtdtApropriacao.value');
        if (sdtApropriacao != '')
            sdtApropriacao = normalizeDate_DateTime(sdtApropriacao, true);

        if (nPessoaID != null)
            sFantasia = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selPessoaID.options[selPessoaID.selectedIndex].innerText');
        else
            nPessoaID = 0;

        // Passar parametro se veio do inf ou do sup
        var strPars = new String();
        strPars = '?sCaller=' + escape('INF');
        strPars += '&nValorID=' + escape(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ValorID\'].value'));
        strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
        strPars += '&nTipoValorID=' + escape(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TipoValorID\'].value'));
        strPars += '&nFormaPagamentoID=' + escape(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'FormaPagamentoID\'].value'));
        strPars += '&nPessoaID=' + escape(nPessoaID);
        strPars += '&sFantasia=' + escape(sFantasia);
        strPars += '&sdtEmissao=' + escape(sdtEmissao);
        strPars += '&sdtApropriacao=' + escape(sdtApropriacao);
        strPars += '&nValor=' + escape(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'Valor\'].value'));
        strPars += '&nOK=' + escape(nOK);
        strPars += '&nProcessoID=' + escape(nProcessoID);

        htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalalterarassociacao.asp' + strPars;
        showModalWin(htmlPath + strPars, new Array(780, 470));
    }
        // Grid de Financeiros botao 5 (Fatura de Transporte)
    else if ((nFolder == '28041') && (controlBar.toUpperCase() == 'INF') && (btnClicked == 5)) {
        openModalFaturaTransporte();
    }
        // Grid de Financeiros botao 6 (ESTORNO DE FINANCEIRO)
    else if ((nFolder == '28041') && (controlBar.toUpperCase() == 'INF') && (btnClicked == 6)) {

        var empresa = getCurrEmpresaData();
        var FinanceiroID = getCellValueByColKey(fg, 'FinanceiroID*', fg.Row);

        var strPars = new String();
        strPars = '?nFinanceiroID=' + escape(FinanceiroID);
        strPars += '&nValorID=' + escape(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ValorID\'].value'));
        strPars += '&nUsuarioID=' + escape(getCurrUserID());

        dsoEstornar.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/EstornarFinanceiro.aspx' + strPars;
        dsoEstornar.ondatasetcomplete = estornafinanceiroDSC;
        dsoEstornar.refresh();

    }

        // Grid de Ocorrencias, botao 1 (Detalha financeiro)
    else if ((nFolder == '28042') && (controlBar.toUpperCase() == 'INF') && (btnClicked == 1)) {
        // Manda o id do financeiro a detalhar
        sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(aEmpresa[0], fg.TextMatrix(fg.Row, 0)));
    }
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 28043)) {
        var teste = getCellValueByColKey(fg, 'SubForm', fg.Row);

        if (getCellValueByColKey(fg, 'SubForm', fg.Row) == 'Dep�sito Banc�rio')
            sendJSCarrier(getHtmlId(), 'SHOWDEPOSITOBANCARIO', new Array(aEmpresa[0], getCellValueByColKey(fg, 'RegistroID', fg.Row)));

        else if (getCellValueByColKey(fg, 'SubForm', fg.Row) == 'Financeiro')
            sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(aEmpresa[0], getCellValueByColKey(fg, 'RegistroID', fg.Row)));
    }
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 28044)) {
        var empresa = getCurrEmpresaData();
        sendJSCarrier(getHtmlId(), 'SHOWLANCAMENTO', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));
    }
    else if ((controlBar == 'INF') && (btnClicked == 2) && (keepCurrFolder() == 28044)) {
        openModalDetalharContas();
    }
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 28049)) {
        var empresa = getCurrEmpresaData();
        sendJSCarrier(getHtmlId(), 'SHOWEXTRATOBANCARIO', new Array(empresa[0], getCellValueByColKey(fg, 'ExtratoID', fg.Row)));
    }
}


function trocaVirgulaPonto(sString) {
    var i;
    var sRet = '';
    for (i = 0; i < sString.length; i++) {
        sRet += (sString.substr(i, 1) == ',' ? '.' : sString.substr(i, 1));
    }
    return sRet;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    if ((keepCurrFolder() == 28041) && (idElement.toUpperCase() == 'MODALLOCALIZARFINANCEIROHTML')) {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            __winTimerHandler = window.setInterval('refreshSupAfterModalClose(true)', 10, 'JavaScript');
            return 0;
        }
    }
        // Modal Eventos Contabeis
    else if (idElement.toUpperCase() == 'MODALEVENTOSCONTABEIS_CONTASHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALALTERARASSOCIACAOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALFATURATRANSPORTEHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid(folderID) {

}

/**********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid(folderID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID) {

    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder) {

    //@@ retornar null para prosseguir a automacao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra inferior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID) {
    //@@
    // Pasta de LOG Read Only
    if ((folderID == 20010) || (folderID == 28043) || (folderID == 28044)) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else if (folderID == 28041) // Financeiro
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    else if (folderID == 28042) // Ocorrencias
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else if (folderID == 28048) // Valores Filhos
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    else if (folderID == 28049) // Lancamentos Bancos
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    else {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Executa um Refresh no SUP e/ou INF
           
Parametros:     
bRefreshInf - false- executa Refresh no SUP
            - true - executa refresh no SUP e no INF  

Retorno:
nenhum
********************************************************************/
function refreshSupAfterModalClose(bRefreshInf) {
    if (__winTimerHandler != null) {
        window.clearInterval(__winTimerHandler);
        __winTimerHandler = null;
    }

    if (bRefreshInf) {
        glb_bForceRefrInf = true;
    }

    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                          '__btn_REFR(\'sup\')');
}

function openModalDetalharContas() {
    if (fg.Row <= 0)
        return null;

    var htmlPath;
    var strPars = new String();
    var nEventoID = getCellValueByColKey(fg, 'EventoID', fg.Row);

    // mandar os parametros para o servidor
    strPars = '?nEventoID=' + escape(nEventoID);

    // carregar modal - nao faz operacao de banco no carregamento           
    htmlPath = SYS_PAGESURLROOT + '/modalgen/modaleventoscontabeis_contas.asp' + strPars;
    showModalWin(htmlPath, new Array(765, 460));
}

function deletarFinanceiros() {
    var i;
    var _retConf;

    _retConf = window.top.overflyGen.Confirm('Deseja deletar todos os financeiros \n onde a coluna A esteja desmarcada?');

    if (_retConf == 1)
        saveDataInGrid();
    else
        return;
}

function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;
    var iValFinanceiroID;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    for (i = 2; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, 4) == 0) {
            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) && (strPars != '')) {
                nBytesAcum = 0;
                glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                strPars = '';
                nDataLen = 0;
            }

            nDataLen++;

            iValFinanceiroID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValFinanceiroID'));
            strPars += (strPars == '' ? '?' : '&');
            strPars += 'iValFinanceiroID=' + escape(iValFinanceiroID);
        }
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGravar.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/deletafinanceiro.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGravar.ondatasetcomplete = sendDataToServer_DSC;
            dsoGravar.refresh();
        }
        else {
            glb_localTimerRefresh = window.setInterval('saveDataInGrid_DSC()', 30, 'JavaScript');
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Erro. Tente novamente.') == 0)
            return null;

        glb_localTimerRefresh = window.setInterval('saveDataInGrid_DSC()', 30, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    if (glb_localTimerRefresh != null) {
        window.clearInterval(glb_localTimerRefresh);
        glb_localTimerRefresh = null;
    }

    // Forca um refresh no inf
    __btn_REFR('inf');
}

function estornafinanceiroDSC() {

    if (!(dsoEstornar.recordset.BOF && dsoEstornar.recordset.EOF)) {
        sResultado = dsoEstornar.recordset['MensagemErro'].value;

        if ((sResultado != null) && (sResultado != '')) {
            if (window.top.overflyGen.Alert(sResultado) == 0)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Financeiro estornado') == 0);
            refreshSupAfterModalClose(true);
        }
    }
}
function openModalFaturaTransporte() {

    //var nEstadoID;
    var htmlPath;
    var strPars = "";
    var nCurrEmpresa;
    var nTransportadoraID;

    //var aEmpresa = getCurrEmpresaData();

    //nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");

    nCurrEmpresa = getCurrEmpresaData();
    nValorID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ValorID' + '\'' + '].value');

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa[0]);
    strPars += '&sCaller=' + escape('I');
    strPars += '&nValorID=' + escape(nValorID);

    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalfaturatransporte.asp' + strPars;
    showModalWin(htmlPath, new Array(600, 300));
}