<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="valoresalocsup01Html" name="valoresalocsup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf        
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="valoresalocsup01Body" name="valoresalocsup01Body" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->

    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" name="txtEstadoID" class="fldGeneral">
        <p id="lblProcessoID" name="lblProcessoID" class="lblGeneral">Processo</p>
        <select id="selProcessoID" name="selProcessoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ProcessoID"></select>
        <p id="lblFormaPagamentoID" name="lblFormaPagamentoID" class="lblGeneral">Forma</p>
        <select id="selFormaPagamentoID" name="selFormaPagamentoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FormaPagamentoID"></select>
        <p id="lblMotivoDevolucao" name="lblMotivoDevolucao" class="lblGeneral" title="Motivo da devolu��o">Mot</p>
        <input type="text" id="txtMotivoDevolucao" name="txtMotivoDevolucao" DATASRC="#dsoSup01" DATAFLD="MotivoDevolucao" class="fldGeneral" title="Motivo da devolu��o">
        <p id="lblValorReferenciaID" name="lblValorReferenciaID" class="lblGeneral">Valor Refer�ncia</p>
        <select id="selValorReferenciaID" name="selValorReferenciaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ValorReferenciaID"></select>
		<input type="image" id="btnFindValor" name="btnFindValor" class="fldGeneral" title="Preencher Combo" WIDTH="24" HEIGHT="23">
        <p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">Pessoa</p>
        <select id="selPessoaID" name="selPessoaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PessoaID"></select>
		<input type="image" id="btnFindPessoa" name="btnFindPessoa" class="fldGeneral" title="Preencher Combo" WIDTH="24" HEIGHT="23">
        <p id="lblDocumento" name="lblDocumento" class="lblGeneral">Documento</p>
        <input type="text" id="txtDocumento" name="txtDocumento" DATASRC="#dsoSup01" DATAFLD="Documento" class="fldGeneral">
        <p id="lblEmitente" name="lblEmitente" class="lblGeneral">Emitente</p>
        <input type="text" id="txtEmitente" name="txtEmitente" DATASRC="#dsoSup01" DATAFLD="Emitente" class="fldGeneral">
        <p id="lbldtEmissao" name="lbldtEmissao" class="lblGeneral">Emiss�o</p>
        <input type="text" id="txtdtEmissao" name="txtdtEmissao" DATASRC="#dsoSup01" DATAFLD="V_dtEmissao" class="fldGeneral">
        <p id="lbldtApropriacao" name="lbldtApropriacao" class="lblGeneral">Apropria��o</p>
        <input type="text" id="txtdtApropriacao" name="txtdtApropriacao" DATASRC="#dsoSup01" DATAFLD="V_dtApropriacao" class="fldGeneral" title="Data prevista p/ dep�sito do cheque ou da localiza��o do cr�dito">
        <p id="lblBancoAgencia" name="lblBancoAgencia" class="lblGeneral">Banco/Ag�ncia</p>
        <input type="text" id="txtBancoAgencia" name="txtBancoAgencia" DATASRC="#dsoSup01" DATAFLD="BancoAgencia" class="fldGeneral">
        <p id="lblConta" name="lblConta" class="lblGeneral">Conta</p>
        <input type="text" id="txtConta" name="txtConta" DATASRC="#dsoSup01" DATAFLD="Conta" class="fldGeneral">
        <p id="lblNumeroDocumento" name="lblNumeroDocumento" class="lblGeneral">N�mero</p>
        <input type="text" id="txtNumeroDocumento" name="txtNumeroDocumento" DATASRC="#dsoSup01" DATAFLD="NumeroDocumento" class="fldGeneral">
        <p id="lblMoedaID" name="lblMoedaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaID" name="selMoedaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="MoedaID"></select>
        <p id="lblValor" name="lblValor" class="lblGeneral">Valor</p>
        <input type="text" id="txtValor" name="txtValor" DATASRC="#dsoSup01" DATAFLD="Valor" class="fldGeneral">
        <p id="lblIdentificador" name="lblIdentificador" class="lblGeneral">Identificador</p>
        <input type="text" id="txtIdentificador" name="txtIdentificador" DATASRC="#dsoSup01" DATAFLD="Identificador" class="fldGeneral">
        <p id="lblRelPesContaID" name="lblRelPesContaID" class="lblGeneral">Conta Banc�ria</p>
        <select id="selRelPesContaID" name="selRelPesContaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="RelPesContaID"></select>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral">
        <p id="lblSaldoFinanceiro" name="lblSaldoFinanceiro" class="lblGeneral">Saldo Financeiro</p>
        <input type="text" id="txtSaldoFinanceiro" name="txtSaldoFinanceiro" DATASRC="#dsoSup01" DATAFLD="SaldoFinanceiro" class="fldGeneral">
        <p id="lblSaldoOcorrencia" name="lblSaldoOcorrencia" class="lblGeneral">Saldo Ocorr�ncia</p>
        <input type="text" id="txtSaldoOcorrencia" name="txtSaldoOcorrencia" DATASRC="#dsoSup01" DATAFLD="SaldoOcorrencia" class="fldGeneral">
        <p id="lblSaldoConciliacao" name="lblSaldoConciliacao" class="lblGeneral">Saldo Concilia��o</p>
        <input type="text" id="txtSaldoConciliacao" name="txtSaldoConciliacao" DATASRC="#dsoSup01" DATAFLD="SaldoConciliacao" class="fldGeneral">
        <p id="lblHistorico" name="lblHistorico" class="lblGeneral">Hist�rico</p>
        <input type="text" id="txtHistorico" name="txtHistorico" DATASRC="#dsoSup01" DATAFLD="Historico" class="fldGeneral">
        <p id="lblBancoCliente" name="lblBancoCliente" class="lblGeneral">Cliente: Banco / Ag�ncia / Conta</p>
        <input type="text" id="txtBancoCliente" name="txtBancoCliente" DATASRC="#dsoSup01" DATAFLD="ClienteBanco" class="fldGeneral" maxlength="3">
        <p id="lblAgenciaCliente" name="lblAgenciaCliente" class="lblGeneral"></p>
        <input type="text" id="txtAgenciaCliente" name="txtAgenciaCliente" DATASRC="#dsoSup01" DATAFLD="ClienteAgencia" class="fldGeneral" maxlength="8">
        <p id="lblContaCliente" name="lblContaCliente" class="lblGeneral"></p>
        <input type="text" id="txtContaCliente" name="txtContaCliente" DATASRC="#dsoSup01" DATAFLD="ClienteConta" class="fldGeneral" maxlength="17">
        
        <p id="lblContrato" name="lblContrato" class="lblGeneral">Contrato</p>
        <input type="text" id="txtContrato" name="txtContrato" DATASRC="#dsoSup01" DATAFLD="Contrato" class="fldGeneral">
        
        <p id="lblTaxaMoedaImportacao" name="lblTaxaMoedaImportacao" class="lblGeneral">Taxa Moeda Importa��o</p>
        <input type="text" id="txtTaxaMoedaImportacao" name="txtTaxaMoedaImportacao" DATASRC="#dsoSup01" DATAFLD="TaxaMoedaImportacao" class="fldGeneral">
        
    </div>
</body>

</html>
