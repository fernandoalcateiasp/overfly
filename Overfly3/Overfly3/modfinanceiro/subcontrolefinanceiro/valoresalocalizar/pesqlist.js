/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form valoreslocalizar
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoListData01 = new CDatatransport('dsoListData01');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoPropsPL = new CDatatransport('dsoPropsPL');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload() {
    windowOnLoad_1stPart();

    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'E', 'Processo', 'For', 'Mot', 'Pessoa', 'Emitente', 'Emiss�o', 'Apropria��o', 'Bco/Ag', 'Conta', 'N�mero', '$', 'Valor',
        'Saldo Financ', 'Saldo Ocorr', 'Saldo Conc', 'Identificador', 'Conta Banc�ria', 'Observa��o', 'Hist�rico');

    //@@ Formatacao das colunas do grid de pesquisa, definicao opcional
    // padrao do framework = null se variavel nao for definida aqui

    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', '', dTFormat, dTFormat, '', '', '', '', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', '', '', '', '');
    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked() {
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Confirmar Financeiros', 'Gerar Ocorr�ncias', 'Gerar Dep�sito Banc�rio', 'Baixar Valor a Localizar', 'Detalhar Valor Refer�ncia', 'Estornar Valor a Localizar']);

    setupEspecBtnsControlBar('sup', 'HHHHHHHD');

    if (fg.Rows > 1) {
        alignColsInGrid(fg, [0, 12, 13, 14, 15, 16]);
    }

    fg.FontSize = '8';
    fg.Redraw = 1;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    var contexto = 0;
    var sAnchor;
    contexto = getCmbCurrDataInControlBar('sup', 1);

    if (controlBar == 'SUP') {
        // Usuario clicou botao documentos
        if (btnClicked == 1) {
            if (fg.Rows > 1) {
                __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
            }
            else {
                window.top.overflyGen.Alert('Selecione um registro.');
            }
        } // usuario clicou botao imprimir
        else if (btnClicked == 2)
            openModalPrint();
            // Procedimento
        else if (btnClicked == 3) {
            if (contexto[1] == 9131)
                sAnchor = '2112';
            else
                sAnchor = '2122';

            window.top.openModalControleDocumento('PL', '', 910, null, sAnchor, 'T');
        }
        else if (btnClicked == 4)
            openModalConfirmarFinanceiros();
        else if (btnClicked == 5)
            openModalGerarOcorrencias();
        else if (btnClicked == 6)
            openModalDepositoBancario();
        else if (btnClicked == 7)
            openModalBaixarValoresLocalizar();
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Modal de impressao
    if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCONFIRMARFINANCEIROSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

    else if (idElement.toUpperCase() == 'MODALGERAOCORRENCIASHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

    else if (idElement.toUpperCase() == 'MODALGERARDEPOSITOBANCARIOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

    else if (idElement.toUpperCase() == 'MODALBAIXARVALORESLOCALIZARHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
        // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
        // Modal de estorno
    else if (idElement.toUpperCase() == 'MODALESTORNAVLHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch() {

    //@@ da automacao -> retorno padrao
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    // por questoes esteticas, coloca foco no grid de pesquisa
    if (fg.disabled == false)
        fg.focus();

    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&sContexto=' + escape(contexto[2]);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&currDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(395, 270));
}

/********************************************************************
Criado pelo programador
Abre a JM de Gerar Ocorrencias

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalConfirmarFinanceiros() {
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = 0;

    if (((typeof (aContextoID)).toUpperCase() == 'OBJECT') && (aContextoID.length >= 2))
        nContextoID = aContextoID[1];

    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nContextoID=' + escape(nContextoID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalconfirmarfinanceiros.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}

/********************************************************************
Criado pelo programador
Abre a JM de Gerar Ocorrencias

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalGerarOcorrencias() {
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = 0;

    if (((typeof (aContextoID)).toUpperCase() == 'OBJECT') && (aContextoID.length >= 2))
        nContextoID = aContextoID[1];

    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nContextoID=' + escape(nContextoID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalgeraocorrencias.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}

/********************************************************************
Criado pelo programador
Abre a JM de Gerar Ocorrencias

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalDepositoBancario() {
    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sDATE_SQL_PARAM=' + escape(DATE_SQL_PARAM);
    strPars += '&nEmpresaID=' + escape(getCurrEmpresaData()[0]);
    strPars += '&nFormID=' + escape(window.top.formID);
    strPars += '&sTitle=' + escape('Gerar dep�sito banc�rio');

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalgerardepositobancario.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}

/********************************************************************
Criado pelo programador

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalBaixarValoresLocalizar() {
    var htmlPath;
    var empresa = getCurrEmpresaData();
    var strPars = new String();
    var nTipoValorID = 0;
    var aItemSelected;
    var nContextoID;

    aItemSelected = getCmbCurrDataInControlBar('sup', 1);
    nContextoID = aItemSelected[1];

    if (nContextoID == 9131)
        nTipoValorID = 1001;
    else if (nContextoID == 9132)
        nTipoValorID = 1002;

    strPars = '?sCaller=' + escape('PL');
    strPars += '&nFinanceiroID=' + escape(0);
    strPars += '&nTipoValorID=' + escape(nTipoValorID);
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(empresa[0]);
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalbaixarvaloreslocalizar.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}
