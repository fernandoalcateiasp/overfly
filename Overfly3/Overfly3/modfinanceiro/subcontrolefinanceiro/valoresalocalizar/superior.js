/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// controla o retorno do servidor dos dados de combos dinamicos
// (selSujeitoID e selObjetoID)
var glb_CounterCmbsDynamics = 0;
// guarda o botao clicado enviado pelo frameword
var glb_BtnFromFramWork;
var glb_bLupa = false;
var glb_bIncl = false;
var dsoSup01 = new CDatatransport('dsoSup01');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoCmbDynamic02 = new CDatatransport('dsoCmbDynamic02');
var dsoFindEmitente = new CDatatransport('dsoFindEmitente');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoVerificacao = new CDatatransport('dsoVerificacao');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
putSpecialAttributesInControls()
prgServerSup(btnClicked)
prgInterfaceSup(btnClicked)
optChangedInCmb(cmb)
btnLupaClicked(btnClicked)
finalOfSupCascade(btnClicked)
btnBarClicked(controlBar, btnClicked)
btnBarNotEspecClicked(controlBar, btnClicked)
modalInformForm(idElement, param1, param2)
supInitEditMode()
formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
stateMachOpened( currEstadoID )
stateMachBtnOK( currEstadoID, newEstadoID )
stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
carrierArrived(idElement, idBrowser, param1, param2)
carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload() {
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);

    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selFormaPagamentoID', '1'],
                          ['selMoedaID', '2'],
                          ['selRelPesContaID', '3'],
                          ['selProcessoID', '4']]);

    windowOnLoad_1stPart();

    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/inferior.asp',
                              SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/pesquisa.asp');

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);

    windowOnLoad_2ndPart();

    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'ValorID';

    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

function setupPage() {

    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup', [[1, 'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
                          ['lblEstadoID', 'txtEstadoID', 2, 1, -4],
                          ['lblProcessoID', 'selProcessoID', 18, 1, -4],
                          ['lblFormaPagamentoID', 'selFormaPagamentoID', 7, 1, -4],
                          ['lblMotivoDevolucao', 'txtMotivoDevolucao', 3, 1, -4],
                          ['lblValorReferenciaID', 'selValorReferenciaID', 12, 1, -4],
                          ['btnFindValor', 'btn', 21, 1],
                          ['lblPessoaID', 'selPessoaID', 18, 1],
                          ['btnFindPessoa', 'btn', 21, 1],
                          ['lblDocumento', 'txtDocumento', 14, 1, -4],
                          ['lblEmitente', 'txtEmitente', 18, 1],
                          ['lbldtEmissao', 'txtdtEmissao', 10, 2],
                          ['lbldtApropriacao', 'txtdtApropriacao', 10, 2],
                          ['lblBancoAgencia', 'txtBancoAgencia', 19, 2, 2],
                          ['lblConta', 'txtConta', 15, 2, 2],
                          ['lblNumeroDocumento', 'txtNumeroDocumento', 9, 2],
                          ['lblMoedaID', 'selMoedaID', 7, 2],
                          ['lblValor', 'txtValor', 11, 2],
                          ['lblIdentificador', 'txtIdentificador', 20, 3],
                          ['lblRelPesContaID', 'selRelPesContaID', 24, 3, -2],
                          ['lblObservacao', 'txtObservacao', 26, 3, -2],
                          ['lblSaldoFinanceiro', 'txtSaldoFinanceiro', 11, 3, -2],
                          ['lblSaldoOcorrencia', 'txtSaldoOcorrencia', 11, 3, -10],
                          ['lblSaldoConciliacao', 'txtSaldoConciliacao', 11, 3, -10],
                          ['lblHistorico', 'txtHistorico', 30, 4],
                          ['lblBancoCliente', 'txtBancoCliente', 4, 4],
                          ['lblAgenciaCliente', 'txtAgenciaCliente', 8, 4, -135],
                          ['lblContaCliente', 'txtContaCliente', 10, 4, -5],
                          ['lblContrato', 'txtContrato', 15, 4],
                          ['lblTaxaMoedaImportacao', 'txtTaxaMoedaImportacao', 15, 4]], null, null, true);
}



// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2) {
    if (param1 == 'SHOWVALORID') {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if (__currFormState() != 0)
            return null;

        // param2 - traz array:
        // o id da empresa do form que mandou
        // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();

        if (param2[0] != empresa[0])
            return null;

        window.top.focus();

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);

        return new Array(getHtmlId(), null, null);
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2) {

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@
    //txtMotivoDevolucao.onkeypress = showHideData;
    txtMotivoDevolucao.onkeyup = showHideData;

    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.

}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked) {
    //@@
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;

    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR'))
        startDynamicCmbs();
    else
        finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked) {
    //@@
    if (btnClicked == 'SUPINCL')
        showHideData(0, true);

    // campos read-only
    setReadOnlyFields(btnClicked == 'SUPINCL');

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    clearAndLockCmbsDynamics(btnClicked);

    // direciona o evento onchange para funcao findEmitente_onChange
    var txtDocumentoClone = dsoSup01.recordset['Documento'].value;

    txtDocumento.onchange = findEmitente_onChange;

    if (txtDocumentoClone != null)
        txtDocumentoClone.onchange = findEmitente_onChange;

    showHideData(dsoSup01.recordset['FormaPagamentoID'].value, true);

    // campos read-only
    setReadOnlyFields(btnClicked == 'SUPINCL');

    if ((btnClicked == 'SUPINCL') && (selMoedaID.options.length > 0)) {
        dsoSup01.recordset['MoedaID'].value = selMoedaID.options[0].value;
        selOptByValueInSelect(getHtmlId(), 'selMoedaID', selMoedaID.options[0].value);
    }

    if (btnClicked == 'SUPINCL')
        refillCmbHistoricoPadrao(true);
    else
        refillCmbHistoricoPadrao(false);

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    //@@ Os if e os else if abaixo sao particulares de cada form

    if (cmb.id.toUpperCase() == 'SELFORMAPAGAMENTOID') {
        showHideData(cmb.value, false);
    }

    adjustLabelsCombos(false);

    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selTipoRegistroID')
        adjustSupInterface();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK(currEstadoID, newEstadoID) {
    verifyValorInServer(currEstadoID, newEstadoID);
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID, bSavedID) {
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {
    if (btnClicked.id == 'btnFindPessoa') {
        openModalFindPessoas();
        return null;
    }
    else if (btnClicked.id == 'btnFindValor') {
        openModalFindValor();
        return null;
    }
}

function openModalFindPessoas() {
    var empresaData = getCurrEmpresaData();

    strPars = '?nFormID=';
    strPars += escape(window.top.formID);
    strPars += '&sCaller=';
    strPars += escape('S');
    strPars += '&nEmpresaID=';
    strPars += escape(empresaData[0]);
    strPars += '&sComboID=';
    strPars += escape('selPessoaID');
    strPars += '&sLabel=';
    strPars += escape(getLabelNumStriped(lblPessoaID.innerText));

    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalfindpessoa.asp' + strPars;
    showModalWin(htmlPath, new Array(571, 284));
}

function openModalFindValor() {
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = 0;
    var nValorID = 0;
    var sEmitente = '';
    var sBancoAgencia = '';
    var sNumeroDocumento = '';
    var nValor = '';

    if (((typeof (aContextoID)).toUpperCase() == 'OBJECT') && (aContextoID.length >= 2))
        nContextoID = aContextoID[1];

    nValorID = dsoSup01.recordset['ValorID'].value;

    if (dsoSup01.recordset['Emitente'].value != null)
        sEmitente = dsoSup01.recordset['Emitente'].value;

    if (dsoSup01.recordset['BancoAgencia'].value != null)
        sBancoAgencia = dsoSup01.recordset['BancoAgencia'].value;

    if (dsoSup01.recordset['NumeroDocumento'].value != null)
        sNumeroDocumento = dsoSup01.recordset['NumeroDocumento'].value;

    nValor = dsoSup01.recordset['Valor'].value;

    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('S');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nContextoID=' + escape(nContextoID);
    strPars += '&nValorID=' + escape(nValorID);
    strPars += '&sEmitente=' + escape(sEmitente);
    strPars += '&sBancoAgencia=' + escape(sBancoAgencia);
    strPars += '&sNumeroDocumento=' + escape(sNumeroDocumento);
    strPars += '&nValor=' + escape(nValor);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalchequesdevolvidos.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}


/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    var contexto = 0;
    var sAnchor;
    var empresa = getCurrEmpresaData();
    contexto = getCmbCurrDataInControlBar('sup', 1);

    if (controlBar == 'SUP') {
        // Documentos
        if ((controlBar == 'SUP') && (btnClicked == 1)) {
            __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
        }
            // usuario clicou botao imprimir
        else if (btnClicked == 2)
            openModalPrint();
            // Procedimento
        else if (btnClicked == 3) {
            if (contexto[1] == 9131)
                sAnchor = '2112';
            else
                sAnchor = '2122';

            window.top.openModalControleDocumento('S', '', 910, null, sAnchor, 'T');
        }
        else if (btnClicked == 4)
            openModalConfirmarFinanceiros();
        else if (btnClicked == 5)
            openModalGerarOcorrencias();
        else if (btnClicked == 8)
            sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL',
                      [dsoSup01.recordset['ValorReferenciaID'].value, null, null]);
        else if (btnClicked == 9)
            openModalEstornaVL();
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
- Avancar, retroagir, incluir, alterar, estado, 
excluir, OK, cancelar refresh, anterior e proximo. 
(Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    var nValor = parseFloat(txtValor.value);

    // Para prosseguir a automacao retornar null
    if ((controlBar.toUpperCase() == 'SUP') && (btnClicked.toUpperCase() == 'SUPOK')) {
        // se e um registro novo
        if (dsoSup01.recordset['ValorID'].value == null) {
            // pega o value do contexto atual
            var aItemSelected = getCmbCurrDataInControlBar('sup', 1);
            var nContextoID = aItemSelected[1];
            var aEmpresa = getCurrEmpresaData();

            // 9131->Pagamentos, 9132 Recebimentos
            if (nContextoID == 9131)
                dsoSup01.recordset['TipoValorID'].value = 1001;
            else if (nContextoID == 9132)
                dsoSup01.recordset['TipoValorID'].value = 1002;

            dsoSup01.recordset['EmpresaID'].value = aEmpresa[0];
        }
            // se o valor nao for valido
        else if ((nValor <= 0) || (nValor == null)) {
            if (window.top.overflyGen.Alert('Preencher valor') == 0)
                return null;

            txtValor.focus();
            return false;
        }
    }

    if ((controlBar.toUpperCase() == 'SUP') && (btnClicked.toUpperCase() == 'SUPALT')) {
        glb_bLupa = true;
    }
    else
        glb_bLupa = false;

    if (btnClicked.toUpperCase() == 'SUPINCL') {
        glb_bIncl = true;
    }
    else
        glb_bIncl = false;

    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Modal de impressao
    if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            __btn_REFR('sup');
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCONFIRMARFINANCEIROSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

    else if (idElement.toUpperCase() == 'MODALGERAOCORRENCIASHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

        // Modal de relacoes
    else if (idElement.toUpperCase() == 'MODALFINDPESSOAHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            fillFieldsByRelationModal(param2);
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            // nao mexer
            return 0;
        }
    }

        // Modal de cheques devolvidos
    else if (idElement.toUpperCase() == 'MODALCHEQUESDEVOLVIDOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            //            fillFieldsByRelationModal(param2);               
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            fillCmbValorMaeByModal(param2[1]);
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            // nao mexer
            return 0;
        }
    }
        // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
        // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALESTORNAVLHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

function fillCmbValorMaeByModal(nValorID) {
    /*dsoSup01.recordset['ValorReferenciaID'].value = nValorID;
    clearComboEx(['selValorReferenciaID']);

    var oOption = document.createElement("OPTION");
    oOption.text = nValorID;
    oOption.value = nValorID;
    selValorReferenciaID.add(oOption);
    selValorReferenciaID.text = nValorID;
    selValorReferenciaID.value = nValorID ;

    __btn_OK('SUP');*/

    clearComboEx(['selValorReferenciaID']);
    var oldDataSrc = selValorReferenciaID.dataSrc;
    var oldDataFld = selValorReferenciaID.dataFld;
    selValorReferenciaID.dataSrc = '';
    selValorReferenciaID.dataFld = '';
    var oOption = document.createElement("OPTION");
    oOption.text = nValorID;
    oOption.value = nValorID;
    selValorReferenciaID.add(oOption);
    selValorReferenciaID.dataSrc = oldDataSrc;
    selValorReferenciaID.dataFld = oldDataFld;
    if (selValorReferenciaID.options.length != 0)
        setValueInControlLinked(selValorReferenciaID, dsoSup01);
    restoreInterfaceFromModal();
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode() {
    setReadOnlyFields(true);
    selPessoaID.disabled = (selPessoaID.options.length == 0);

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad() {
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Confirmar Financeiros', 'Gerar Ocorr�ncias', 'Gerar Dep�sito Banc�rio', 'Baixar Valor a Localizar', 'Detalhar Valor Refer�ncia', 'Estornar Valor a Localizar']);
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de parceiro 

Parametro:
aParceiro   Array: item 0->Nome
item 1->ID

Retorno:
nenhum
********************************************************************/
function fillComboPessoa(aPessoa) {
    clearComboEx(['selPessoaID']);
    var oldDataSrc = selPessoaID.dataSrc;
    var oldDataFld = selPessoaID.dataFld;
    selPessoaID.dataSrc = '';
    selPessoaID.dataFld = '';
    var oOption = document.createElement("OPTION");
    oOption.text = aPessoa[0];
    oOption.value = aPessoa[1];
    selPessoaID.add(oOption);
    selPessoaID.dataSrc = oldDataSrc;
    selPessoaID.dataFld = oldDataFld;
    if (selPessoaID.options.length != 0)
        setValueInControlLinked(selPessoaID, dsoSup01);
    restoreInterfaceFromModal();
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs() {
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selPessoaID,selCFOPID,selNotaFiscalID)
    var nEmpresaID = getCurrEmpresaData();
    glb_CounterCmbsDynamics = 2;

    // parametrizacao do dso dsoCmbDynamic01 (designado para selPessoaID)
    setConnection(dsoCmbDynamic01);

    dsoCmbDynamic01.SQL = 'SELECT 0 AS Indice, 0 as fldID,SPACE(0) as fldName ' +
						  'UNION ALL ' +
						  'SELECT 1 AS Indice, PessoaID as fldID,Fantasia as fldName ' +
                          'FROM Pessoas WITH(NOLOCK) ' +
                          'WHERE PessoaID = ' + dsoSup01.recordset['PessoaID'].value + ' ' +
                          'ORDER BY Indice';
    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();

    // ver com marcio
    setConnection(dsoCmbDynamic02);
    dsoCmbDynamic02.SQL = 'SELECT 0 AS Indice, ValorReferenciaID as fldID, ISNULL(CONVERT(VARCHAR(10), ValorReferenciaID), SPACE(0)) as fldName ' +
                          'FROM ValoresLocalizar WITH(NOLOCK) ' +
                          'WHERE (ValorID = ' + dsoSup01.recordset['ValorID'].value + ') ' +
                          'ORDER BY Indice';
    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic02.Refresh();
}

/********************************************************************
Funcao disparada pelo programado.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    var optionStr, optionValue;
    var aCmbsDynamics = [selPessoaID, selValorReferenciaID];
    var aDSOsDynamics = [dsoCmbDynamic01, dsoCmbDynamic02];
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var i;
    var lFirstRecord;

    // Inicia o carregamento de combos dinamicos (selPessoaID )
    clearComboEx(['selPessoaID', 'selValorReferenciaID']);

    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics == 0) {
        for (i = 0; i < aCmbsDynamics.length; i++) {
            oldDataSrc = aCmbsDynamics[i].dataSrc;
            oldDataFld = aCmbsDynamics[i].dataFld;
            aCmbsDynamics[i].dataSrc = '';
            aCmbsDynamics[i].dataFld = '';
            lFirstRecord = true;
            while (!aDSOsDynamics[i].recordset.EOF) {
                optionStr = aDSOsDynamics[i].recordset['fldName'].value;
                optionValue = aDSOsDynamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[i].add(oOption);
                aDSOsDynamics[i].recordset.MoveNext();
                lFirstRecord = false;
            }
            aCmbsDynamics[i].dataSrc = oldDataSrc;
            aCmbsDynamics[i].dataFld = oldDataFld;
        }

        adjustLabelsCombos(true);

        // volta para a automacao
        finalOfSupCascade(glb_BtnFromFramWork);
    }
    return null;
}

/********************************************************************
Funcao do programador
Pesquisa de emitente, esta pesquisa e feita durante
a inclusao/alteracao informando o numero do CGC/CPF do emitente
           
Parametros: 
Nenhum

Retorno:
Nenhum
********************************************************************/
function findEmitente_onChange() {
    sDoc = trimStr(this.value);
    txtEmitente.value = '';

    if (sDoc != '') {
        lockInterface(true);
        setConnection(dsoFindEmitente);

        dsoFindEmitente.SQL = 'SELECT a.Fantasia,b.Numero ' +
                              'FROM Pessoas a WITH(NOLOCK), Pessoas_Documentos b WITH(NOLOCK) ' +
                              'WHERE a.PessoaID=b.PessoaID AND ' +
                              'b.Numero = ' + '\'' + sDoc + '\'';

        dsoFindEmitente.ondatasetcomplete = dsoFindEmitente_DSC;
        dsoFindEmitente.Refresh();
    }
}

/********************************************************************
Funcao do programador
Retorno do servidor para pesquisa de emitente
           
Parametros: 
Nenhum

Retorno:
Nenhum
********************************************************************/
function dsoFindEmitente_DSC() {
    if (!((dsoFindEmitente.recordset.BOF) && (dsoFindEmitente.recordset.EOF)))
        txtEmitente.value = dsoFindEmitente.recordset['Fantasia'].value;

    lockInterface(false);
    txtEmitente.focus();
}

/********************************************************************
Funcao do programador
Mostra ou esconde os campos txtBanco, txtNumeroDocumento e seus respectivos
labels de acordo com a forma selecionada (Cheque/Credito em conta->mostra, DIN->esconde)
           
Parametros: 
Nenhum

Retorno:
Nenhum
********************************************************************/
function showHideData(nFormaPagamentoID, bPaging) {
    var sMostraBanco = 'hidden';
    var sMostraDevolucao = 'hidden';
    var sMotivoDevolucao = '';
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);

    /*if (glb_bIncl)
        selProcessoID.value = '1422';*/

    if (nFormaPagamentoID == null) {
        if (bPaging == true)
            nFormaPagamentoID = dsoSup01.recordset['FormaPagamentoID'].value;
        else
            nFormaPagamentoID = selFormaPagamentoID.value;
    }

    if ((nFormaPagamentoID != null) && (nFormaPagamentoID != 0)) {
        if (nFormaPagamentoID != 1031)
            sMostraBanco = 'inherit';

        if ((nFormaPagamentoID == 1032))
            sMostraDevolucao = 'inherit';

        if ((nFormaPagamentoID == 1033) && (dsoSup01.recordset['ProcessoID'].value == 1423) && (!glb_bIncl))
            sMostraDevolucao = 'inherit';
    }

    // Esconde campos de dados bancarios do cliente caso o contexto n�o seja pagamentos e a forma n�o seja DIN - DMC 01/09/2011
    if (aContextoID[1] != 9131) {
        lblBancoCliente.style.visibility = 'hidden';
        txtBancoCliente.style.visibility = 'hidden';

        lblAgenciaCliente.style.visibility = 'hidden';
        txtAgenciaCliente.style.visibility = 'hidden';

        lblContaCliente.style.visibility = 'hidden';
        txtContaCliente.style.visibility = 'hidden';
    }
    else {
        if (nFormaPagamentoID != 1031) {
            lblBancoCliente.style.visibility = 'inherit';
            txtBancoCliente.style.visibility = 'inherit';

            lblAgenciaCliente.style.visibility = 'inherit';
            txtAgenciaCliente.style.visibility = 'inherit';

            lblContaCliente.style.visibility = 'inherit';
            txtContaCliente.style.visibility = 'inherit';

            if (nFormaPagamentoID == 1033) {
                lblContrato.style.visibility = 'inherit';
                txtContrato.style.visibility = 'inherit';
                lblTaxaMoedaImportacao.style.visibility = 'inherit';
                txtTaxaMoedaImportacao.style.visibility = 'inherit';
            }
            else {
                lblContrato.style.visibility = 'hidden';
                txtContrato.style.visibility = 'hidden';
                lblTaxaMoedaImportacao.style.visibility = 'hidden';
                txtTaxaMoedaImportacao.style.visibility = 'hidden';
            }

        }
        else {
            lblBancoCliente.style.visibility = 'hidden';
            txtBancoCliente.style.visibility = 'hidden';

            lblAgenciaCliente.style.visibility = 'hidden';
            txtAgenciaCliente.style.visibility = 'hidden';

            lblContaCliente.style.visibility = 'hidden';
            txtContaCliente.style.visibility = 'hidden';
        }


    }

    lblBancoAgencia.style.visibility = sMostraBanco;
    txtBancoAgencia.style.visibility = sMostraBanco;
    lblNumeroDocumento.style.visibility = sMostraBanco;
    txtNumeroDocumento.style.visibility = sMostraBanco;
    lblMotivoDevolucao.style.visibility = sMostraDevolucao;
    txtMotivoDevolucao.style.visibility = sMostraDevolucao;

    lblValorReferenciaID.style.visibility = sMostraDevolucao;
    selValorReferenciaID.style.visibility = sMostraDevolucao;
    btnFindValor.style.visibility = sMostraDevolucao;

    /*lblValorReferenciaID.style.visibility = 'hidden';
    selValorReferenciaID.style.visibility = 'hidden';
    btnFindValor.style.visibility = 'hidden';*/

    if (sMostraDevolucao == 'inherit') {
        lockBtnLupa(btnFindValor, false);
        if (bPaging == true) {
            sMotivoDevolucao = dsoSup01.recordset['MotivoDevolucao'].value;
        }
        else {
            sMotivoDevolucao = trimStr(txtMotivoDevolucao.value);
        }

        if (((sMotivoDevolucao != null) && (sMotivoDevolucao != '')) ||
		    ((nFormaPagamentoID == 1033) && (dsoSup01.recordset['ProcessoID'].value == 1423))) {
            lblValorReferenciaID.style.visibility = 'inherit';
            selValorReferenciaID.style.visibility = 'inherit';
            btnFindValor.style.visibility = 'inherit';
        }
    }

    lblConta.style.visibility = sMostraBanco;
    txtConta.style.visibility = sMostraBanco;

    if ((selFormaPagamentoID.value == 1032) && (glb_bIncl == true))
        selProcessoID.disabled = false;
    else {
        if (glb_bIncl) {
            selProcessoID.disabled = true;
            selProcessoID.value = '1422';
        }
    }

    if ((sMostraDevolucao == 'inherit') && (glb_bIncl == true))
        lockBtnLupa(btnFindValor, false);
    else {
        lockBtnLupa(btnFindValor, true);
        if (glb_bLupa == true) {
            lockBtnLupa(btnFindValor, false);
            selValorReferenciaID.disabled = false;
            glb_bLupa = false;
        }
        else {
            lockBtnLupa(btnFindValor, true);
            selValorReferenciaID.disabled = true;
        }
    }

    //S� pode pendurar valor refer�ncia em A
    if (dsoSup01.recordset['EstadoID'].value != 41)
        lockBtnLupa(btnFindValor, true);

}

/********************************************************************
Funcao do programador
Seta os campos read-only do sup
           
Parametros: 
Nenhum

Retorno:
Nenhum
********************************************************************/
function setReadOnlyFields(bInitEditMode) {
    var bLockRelPesConta = true;
    var bReadOnlyFields = true;
    var nEstadoID = dsoSup01.recordset['EstadoID'].value;
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = 0;

    if (((typeof (aContextoID)).toUpperCase() == 'OBJECT') && (aContextoID.length >= 2))
        nContextoID = aContextoID[1];

    txtIdentificador.readOnly = true;
    if (((dsoSup01.recordset['EstadoID'].value == 1) ||
        //		 ((typeof (dsoSup01.recordset['EstadoID'].value)).toUpperCase() == 'UNDEFINED')) &&
        (nEstadoID == null)) &&
		(nContextoID == 9131) && (bInitEditMode))
        bLockRelPesConta = false;

    selRelPesContaID.disabled = bLockRelPesConta;

    txtSaldoFinanceiro.readOnly = true;
    txtSaldoOcorrencia.readOnly = true;

    if (dsoSup01.recordset['EstadoID'].value == 1) {
        txtBancoCliente.readOnly = false;
        txtAgenciaCliente.readOnly = false;
        txtContaCliente.readOnly = false;
    }
    else {
        txtBancoCliente.readOnly = true;
        txtAgenciaCliente.readOnly = true;
        txtContaCliente.readOnly = true;
    }

    //if ((((typeof (nEstadoID)).toUpperCase() == 'UNDEFINED') ||
    if (((nEstadoID == null) ||
		 (nEstadoID == 1)) && (bInitEditMode))
        bReadOnlyFields = false;
    else
        bReadOnlyFields = true;

    selFormaPagamentoID.disabled = bReadOnlyFields;
    selProcessoID.disabled = bReadOnlyFields;
    txtMotivoDevolucao.readOnly = bReadOnlyFields;

    if (((nEstadoID == null) ||
		 (nEstadoID == 1)) && (!bReadOnlyFields))
        lockBtnLupa(btnFindValor, false);
    else
        lockBtnLupa(btnFindValor, true);

    txtDocumento.readOnly = bReadOnlyFields;
    txtEmitente.readOnly = bReadOnlyFields;
    txtdtEmissao.readOnly = bReadOnlyFields;
    //txtdtApropriacao.readOnly = bReadOnlyFields;
    //if (getCloneFromOriginal('txtdtApropriacao') != null)
    //getCloneFromOriginal('txtdtApropriacao').readOnly = bReadOnlyFields;

    txtBancoAgencia.readOnly = bReadOnlyFields;
    txtConta.readOnly = bReadOnlyFields;
    txtBancoCliente.readOnly = bReadOnlyFields;
    txtAgenciaCliente.readOnly = bReadOnlyFields;
    txtContaCliente.readOnly = bReadOnlyFields;
    txtNumeroDocumento.readOnly = bReadOnlyFields;
    selMoedaID.readOnly = bReadOnlyFields;
    txtValor.readOnly = bReadOnlyFields;
    selValorReferenciaID.disabled = true;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&currDateFormat=' + escape(DATE_FORMAT);

    if ((isNaN(dsoSup01.recordset['RelPesContaID'].value)) || (dsoSup01.recordset['RelPesContaID'].value == null))
        strPars += '&nRelPesContaID=' + escape(0);
    else
        strPars += '&nRelPesContaID=' + escape(dsoSup01.recordset['RelPesContaID'].value);

    if ((isNaN(dsoSup01.recordset['FormaPagamentoID'].value)) || (dsoSup01.recordset['FormaPagamentoID'].value == null))
        strPars += '&nFormaPagamentoID=' + escape(0);
    else
        strPars += '&nFormaPagamentoID=' + escape(dsoSup01.recordset['FormaPagamentoID'].value);


    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(555, 465));
}

/********************************************************************
Criado pelo programador
Abre a JM de Gerar Ocorrencias

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalConfirmarFinanceiros() {
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = 0;

    if (((typeof (aContextoID)).toUpperCase() == 'OBJECT') && (aContextoID.length >= 2))
        nContextoID = aContextoID[1];

    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('S');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nContextoID=' + escape(nContextoID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalconfirmarfinanceiros.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}


/********************************************************************
Criado pelo programador
Abre a JM de Gerar Ocorrencias

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalGerarOcorrencias() {
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = 0;

    if (((typeof (aContextoID)).toUpperCase() == 'OBJECT') && (aContextoID.length >= 2))
        nContextoID = aContextoID[1];

    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('S');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nContextoID=' + escape(nContextoID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalgeraocorrencias.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}

/********************************************************************
Funcao criada pelo programador.
Preenche os campos do subform corrente, com os dados vindos da janela
modal de relacoes

Parametro:
aData   -   array de dados
aData[0] - ID do combo
aData[1] - ID da pessoa
aData[2] - Nome da Pessoa

os demais elementos sao opcionais

Retorno:
nenhum
********************************************************************/
function fillFieldsByRelationModal(aData) {
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var elem;
    var addSexOrServ = null;

    elem = window.document.getElementById(aData[0]);

    if (elem == null)
        return;

    clearComboEx([elem.id]);

    oldDataSrc = elem.dataSrc;
    oldDataFld = elem.dataFld;
    elem.dataSrc = '';
    elem.dataFld = '';

    // Preencher o combo selSujeitoID
    dsoSup01.recordset.Fields['PessoaID'].value = aData[1];

    oOption = document.createElement("OPTION");
    oOption.text = '';
    oOption.value = 0;
    elem.add(oOption);

    oOption = document.createElement("OPTION");
    oOption.text = aData[2];
    oOption.value = aData[1];
    elem.add(oOption);

    elem.selectedIndex = 1;

    elem.dataSrc = oldDataSrc;
    elem.dataFld = oldDataFld;

    elem.disabled = false;
}

/********************************************************************
Funcao criada pelo programador.
Limpa e trava os combos dinamicos quando o usuario clicar em inclusao

Parametro:
btnClicked  -> Ultimo botao clicado da barra superior

Retorno:
nenhum
********************************************************************/
function clearAndLockCmbsDynamics(btnClicked) {
    if (btnClicked == 'SUPINCL') {
        clearComboEx(['selPessoaID']);
        selPessoaID.disabled = true;

        // trava o botao de lupa do sujeito
        lockBtnLupa(btnFindPessoa, false);
    }
}

function adjustLabelsCombos(bPaging) {
    if (bPaging) {
        setLabelOfControl(lblFormaPagamentoID, dsoSup01.recordset['FormaPagamentoID'].value);
        setLabelOfControl(lblPessoaID, dsoSup01.recordset['PessoaID'].value);
        setLabelOfControl(lblRelPesContaID, dsoSup01.recordset['RelPesContaID'].value);
        setLabelOfControl(lblProcessoID, dsoSup01.recordset['ProcessoID'].value);
        setLabelOfControl(lblValorReferenciaID, dsoSup01.recordset['ValorReferenciaID'].value);
    }
    else {
        setLabelOfControl(lblFormaPagamentoID, selFormaPagamentoID.value);
        setLabelOfControl(lblPessoaID, selPessoaID.value);
        setLabelOfControl(lblRelPesContaID, selRelPesContaID.value);
        setLabelOfControl(lblProcessoID, selProcessoID.value);
        setLabelOfControl(lblValorReferenciaID, selValorReferenciaID.value);
    }
}

/********************************************************************
Verificacoes do Valor
********************************************************************/
function verifyValorInServer(currEstadoID, newEstadoID) {
    var nValorID = dsoSup01.recordset['ValorID'].value;
    var strPars = new String();

    strPars = '?nValorID=' + escape(nValorID);
    strPars += '&nEstadoDeID=' + escape(currEstadoID);
    strPars += '&nEstadoParaID=' + escape(newEstadoID);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/serverside/verificacaovalor.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyValorInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes do Valor
********************************************************************/
function verifyValorInServer_DSC() {
    if (dsoVerificacao.recordset.Fields['Verificacao'].value == null) {
        stateMachSupExec('OK');
    }
    else {
        if (window.top.overflyGen.Alert(dsoVerificacao.recordset.Fields['Verificacao'].value) == 0)
            return null;
        stateMachSupExec('CANC');
    }
}

/********************************************************************
Fun��o para alterar o(s) combo(s) est�tico(s)
********************************************************************/
function refillCmbHistoricoPadrao(inclusao) {
    var oldDataSrc = selProcessoID.dataSrc;
    var oldDataFld = selProcessoID.dataFld;

    var optionStr, optionValue;

    // pega o value do contexto atual
    var aItemSelected = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = aItemSelected[1];

    clearComboEx(['selProcessoID']);

    if (dsoEstaticCmbs.recordset.Fields.Count == 0)
        return null;

    if (inclusao)
        dsoEstaticCmbs.recordset.setFilter("Indice=4 AND Inc=1");
    else
        dsoEstaticCmbs.recordset.setFilter("Indice = 4 ");

    if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {
        dsoEstaticCmbs.recordset.MoveFirst();

        oldDataSrc = dsoEstaticCmbs.dataSrc;
        oldDataFld = dsoEstaticCmbs.dataFld;
        dsoEstaticCmbs.dataSrc = '';
        dsoEstaticCmbs.dataFld = '';

        while (!dsoEstaticCmbs.recordset.EOF) {
            optionStr = dsoEstaticCmbs.recordset['fldName'].value;
            optionValue = dsoEstaticCmbs.recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            oOption.Indice = dsoEstaticCmbs.recordset['Indice'].value;
            oOption.Ordem = dsoEstaticCmbs.recordset['Ordem'].value;
            oOption.Ordem2 = dsoEstaticCmbs.recordset['Ordem2'].value;
            oOption.Filtro = dsoEstaticCmbs.recordset['Filtro'].value;

            selProcessoID.add(oOption);
            dsoEstaticCmbs.recordset.MoveNext();
        }

        dsoEstaticCmbs.dataSrc = oldDataSrc;
        dsoEstaticCmbs.dataFld = oldDataFld;
    }

    dsoEstaticCmbs.recordset.setFilter('');
}


/********************************************************************
Criado pelo programador

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalEstornaVL() {
    var htmlPath;
    var empresa = getCurrEmpresaData();
    var strPars = new String();
    var nTipoValorID = 0;
    var aItemSelected;
    var nContextoID;
    var nValorID = dsoSup01.recordset['ValorID'].value;

    aItemSelected = getCmbCurrDataInControlBar('sup', 1);

    strPars = '?sCaller=' + escape('S');
    strPars += '&nRegistroID=' + escape(nValorID);
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(empresa[0]);
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalestornavl.asp' + strPars;
    showModalWin(htmlPath, new Array(350, 120));
}