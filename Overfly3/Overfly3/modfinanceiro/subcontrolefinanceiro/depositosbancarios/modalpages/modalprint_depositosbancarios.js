/********************************************************************
modalprint_depositosbancarios.js

Library javascript para o modalprint.asp
Valores a Localizar
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// dso usado para envia e-mails
var dsoEMail = new CDatatransport("dsoEMail");
var dsoTicket1 = new CDatatransport("dsoTicket1");
var dsoTicket2 = new CDatatransport("dsoTicket2");
var dsoLayoutCheque = new CDatatransport("dsoLayoutCheque");
var dsoCheque = new CDatatransport("dsoCheque");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    linksDivAndReports()
    invertChkBox()
    chkBox_Clicked()
    selReports_Change()
    btnOK_Status()
    btnOK_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    noRights()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';

var glb_nTocketsDSOs = 0;
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	glb_bUseProgressBar = true;
	asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    with (modalprintBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
    
    if ( selReports.disabled == false )
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();
}


/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {
		pb_StopProgressBar(true);
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
	}        
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Relat�rios', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;    
    
    // largura livre
    widthFree = modWidth - frameBorder;
    
    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    with (lblReports.style)
    {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta combo de relatorios
    with (selReports.style)
    {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta o divDepositoBancario
    with (divDepositoBancario.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }
    
    // ajusta o divDepositos
    with (divDepositos.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // O estado do botao btnOK
    btnOK_Status();
    
    // O usuario tem direito ou nao
    noRights();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;
    
    selReports.onchange = selReports_Change;
    
    // carrega a lista de relatorios
    loadReportsInList();
    
    // linka os divs nos relatorios
    linkDivsAndReports();
    
    // ajusta os divs

    // Desabilitado no Projeto Overfly 2.0. Verificado com Ivanilde e Camilo que essa modal n�o � utilizada. MTH 06/12/2017
    // ajusta o divDepositoBancario
    //adjustdivDepositoBancario();

    // ajusta o divDepositos
    adjustdivDepositos();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList()
{
    var i;
    
    /*Desabilitado relat�rio 40195 na modal de Dep�sitos Banc�rios (mantido na modal de financeiro) no Projeto Overfly 2.0. 
    Verificado com Ivanilde e Camilo que essa modal n�o � utilizada. MTH 06/12/2017*/
    if (glb_arrayReports != null && glb_arrayReports[0][1] != '40195') 
    {
        for ( i=0; i<glb_arrayReports.length; i++)    
        {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);
            
            if ( glb_arrayReports[i][2] )
                selReports.selectedIndex = i;
        }
        
        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports()
{
    divDepositoBancario.setAttribute('report', 40195 , 1);
    divDepositos.setAttribute('report', 40196 , 1);
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    // O estado do botao btnOK
    btnOK_Status();
    
    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();
    
    return true;
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change()
{
    // mostra o div correspondente ao relatorio
    showDivByReportID();
    
    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if ( this.disabled == false )
        this.focus();
    
    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status()
{
    var btnOKStatus = true;
    var i, elem;
    
    if ( selReports.selectedIndex != -1 )
    {
        // Relatorio de Valores a Localizar
        if (selReports.value == 40195)
            btnOKStatus = false;
        // Depositos
        else if (selReports.value == 40196)
            btnOKStatus = false;
    }
    
    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    // Relatorio de Valores a Localizar
    if ( (selReports.value == 40195) && (glb_sCaller == 'S') )
        relatorioDepositosBancarios();
    else if ( (selReports.value == 40196) && (glb_sCaller == 'PL') )
        Depositos();
    else
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null);
}

/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID()
{
    var i, coll, attr, currRep;
    
    attr = null;
    currRep = 0;
    
    currRep = selReports.value;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( attr == currRep )
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';    
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr()
{
    var i, coll, attr, retVal;
    
    attr = null;
    retVal = 0;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( coll[i].style.visibility == 'visible' )
            {
                retVal = attr;
                break;
            }    
        }
    }
    return retVal;    
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights()
{
    if ( selReports.options.length != 0 )
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;
    
    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
            coll[i].style.visibility = 'hidden';    
    }
    
    // desabilita o combo de relatorios
    selReports.disabled = true;
    
    // desabilita o botao OK
    btnOK.disabled = true;
        
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];
    
    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);
       
    elem = document.getElementById('selReports');
    
    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
        
    // a altura livre    
    modHeight -= topFree;
        
    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt'; 
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';
    
    // acrescenta o elemento
    window.document.body.appendChild(elem);
        
    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);
        
    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;
        
    return null;
}

function adjustdivDepositoBancario()
{
	adjustElementsInForm([['lblPadrao','selPadrao',8,1,-10,-10]],null,null,true);
	
    var empresaID = getCurrEmpresaData();
    
    // Se idioma ingles
    if (empresaID[8] == 245)
		selPadrao.selectedIndex = 1;
    else
		selPadrao.selectedIndex = 0;
	
	
    return null;
}

function adjustdivDepositos()
{
	adjustElementsInForm([['lblContas','selContas',25,1,-10,-10],
		['lblDinheiro','chkDinheiro',3,1],
		['lblCheque','chkCheque',3,1],
		['lblDtInicio','txtDtInicio',10,2,-10],
		['lblDtFim', 'txtDtFim', 10, 2],
	    ['lblFormatoSolicitacao', 'selFormatoSolicitacao', 10, 2]], null, null, true);

	txtDtInicio.value = glb_dCurrDate;
	txtDtFim.value = glb_dCurrDate;

	chkDinheiro.checked = true;
	chkCheque.checked = true;
	chkDinheiro.onclick = chkDin_onclick;
	chkCheque.onclick = chkChk_onclick;

    return null;
}

function chkDin_onclick()
{
	if (!chkDinheiro.checked)
		chkCheque.checked = true;
}

function chkChk_onclick()
{
	if (!chkCheque.checked)
		chkDinheiro.checked = true;
}

function Depositos()
{

    var nEmpresaLogada = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	var strDataInicio = '';
	var strDataFim = '';
    var sFiltroConta = '';
	
	if (!chkData(txtDtInicio))
		return null;

	if (!chkData(txtDtFim))
		return null;
		
	strDataInicio = putDateInMMDDYYYY2(txtDtInicio.value);
	strDataFim = putDateInMMDDYYYY2(txtDtFim.value);
	
	if (strDataInicio != '')
		strDataInicio = ' AND a.dtData >= ' + '\'' + strDataInicio + '\'' + ' ';

	if (strDataFim != '')
		strDataFim = ' AND a.dtData <= ' + '\'' + strDataFim + '\'' + ' ';
			
    if (selContas.selectedIndex > 0)
		sFiltroConta = ' AND a.RelPesContaID=' + selContas.value + ' ';
	
	var strParams = '';
	
	if (selContas.selectedIndex > 0)
		strParams += 'Conta: ' + selContas.options[selContas.selectedIndex].innerText + '   ';
		
	strParams += (chkDinheiro.checked ? 'Dinheiro   ' : '');
	strParams += (chkCheque.checked ? 'Cheque   ' : '');
	
	if (trimStr(txtDtInicio.value) != '')
		strParams += 'In�cio: ' + txtDtInicio.value + '   ';
		
	if (trimStr(txtDtFim.value) != '')
		strParams += 'Fim: ' + txtDtFim.value + '   ';
		
    //Envio dos par�metros pro lado servidor	
	var strParameters = "RelatorioID=" + selReports.value + "&nEmpresaLogada=" + nEmpresaLogada[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + selFormatoSolicitacao.value +
                        "&chkDinheiro=" + (chkDinheiro.checked ? '1' : '0') + "&chkCheque=" + (chkCheque.checked ? '1' : '0') + "&strDataInicio=" + strDataInicio +
	                    "&strDataFim=" + strDataFim + "&sFiltroConta=" + sFiltroConta + "&strParams=" + strParams;

	pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
	lockControlsInModalWin(true);

	var frameReport = document.getElementById("frmReport");
	frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
	frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modfinanceiro/subcontrolefinanceiro/depositosbancarios/serverside/Reports_depositosbancarios.aspx?" + strParameters;

	pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
}

function chkData(objData)
{
	if (objData.value == null)
		return true;
		
	objData.value = trimStr(objData.value);
	if (objData.value == '')
		return true;
		
	if (chkDataEx(objData.value) != true)
	{
        if ( window.top.overflyGen.Alert('Data inv�lida') == 0 )
            return null;
            
		lockControlsInModalWin(false);
        window.focus();
        objData.focus();

		return false;
	}
	
	return true;
}

function relatorioDepositosBancarios()
{
	// Padrao Brasil
	if (selPadrao.value == 0)
		relatorioDepositosBancarios_BRA();
	// Padrao USA
	else if (selPadrao.value == 1)
		relatorioDepositosBancarios_USA();
}

function relatorioDepositosBancarios_USA()
{
	glb_nTocketsDSOs = 2;
	
	var nDepositoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['DepositoID'].value");

    setConnection(dsoTicket1);
    dsoTicket1.SQL = 'SELECT 0 AS Indice, dbo.fn_Numero_Formata(dbo.fn_DepositoBancario_Posicao(a.DepositoID, 6), 2, 1, ' + DATE_SQL_PARAM + ') AS Valor, 1 AS Dinheiro ' +
		'FROM DepositosBancarios a WITH(NOLOCK) ' +
		'WHERE a.DepositoID = ' + nDepositoID + ' AND ISNULL(dbo.fn_DepositoBancario_Posicao(a.DepositoID, 6), 0) > 0 ' +
		'UNION ALL ' +
		'SELECT 1 AS Indice, dbo.fn_Numero_Formata(b.Valor, 2, 1, ' + DATE_SQL_PARAM + ') AS Valor, 0 AS Dinheiro ' +
		'FROM DepositosBancarios_ValoresLocalizar a WITH(NOLOCK), ValoresLocalizar b WITH(NOLOCK) ' +
		'WHERE (a.DepositoID = ' + nDepositoID + ' AND a.ValorID = b.ValorID AND b.FormaPagamentoID=1032) ';
    dsoTicket1.ondatasetcomplete = relatorioDepositosBancarios_USA_DSC;
    dsoTicket1.Refresh();

	setConnection(dsoTicket2);
	dsoTicket2.SQL = 'SELECT CONVERT(VARCHAR(10), a.dtData, 101) AS Data, ' +
		'dbo.fn_Numero_Formata(dbo.fn_DepositoBancario_Posicao(a.DepositoID, 8), 2, 1, ' + DATE_SQL_PARAM + ') AS ValorTotal ' +
		'FROM DepositosBancarios a WITH(NOLOCK) ' +
		'WHERE (a.DepositoID = ' + nDepositoID + ')';
	dsoTicket2.ondatasetcomplete = relatorioDepositosBancarios_USA_DSC;		
    dsoTicket2.Refresh();
}

function relatorioDepositosBancarios_USA_DSC()
{
   //Relat�rio tirado pelo form de financeiro - MTH 08/12/2017
}

function relatorioDepositosBancarios_BRA()
{
   //Relat�rio tirado pelo form de financeiro - MTH 08/12/2017
}
