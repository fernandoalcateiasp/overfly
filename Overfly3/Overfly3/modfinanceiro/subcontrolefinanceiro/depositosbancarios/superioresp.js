/********************************************************************
superioresp.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 *, ' +
               'CONVERT(VARCHAR, dtData, '+DATE_SQL_PARAM+') AS V_dtData, ' +
               'CONVERT(NUMERIC(4), dbo.fn_DepositoBancario_Posicao(DepositosBancarios.DepositoID, 3)) AS Quantidade, ' +
               'dbo.fn_DepositoBancario_Posicao(DepositosBancarios.DepositoID, 7) AS ValorCheques, ' +
               'dbo.fn_DepositoBancario_Posicao(DepositosBancarios.DepositoID, 5) AS DinheiroVL, ' +
               'dbo.fn_DepositoBancario_Posicao(DepositosBancarios.DepositoID, 8) AS ValorTotal, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=DepositosBancarios.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'ISNULL(dbo.fn_ContaBancaria_Nome(RelPesContaID, 5), SPACE(0)) AS NomeBanco, ' +
               '(CASE WHEN dbo.fn_Data_Zero(dtData) < dbo.fn_Data_Zero(GETDATE()) THEN 1 ELSE 0 END) AS DepositoDataMenor ' +
               'FROM DepositosBancarios WITH(NOLOCK)  ' +
               'WHERE ProprietarioID = '+ nID + 'ORDER BY DepositoID DESC';
    else
        sSQL = 'SELECT *, ' + 
               'CONVERT(VARCHAR, dtData, '+DATE_SQL_PARAM+') AS V_dtData, ' +
               'CONVERT(NUMERIC(4), dbo.fn_DepositoBancario_Posicao(DepositosBancarios.DepositoID, 3)) AS Quantidade, ' +
               'dbo.fn_DepositoBancario_Posicao(DepositosBancarios.DepositoID, 7) AS ValorCheques, ' +
               'dbo.fn_DepositoBancario_Posicao(DepositosBancarios.DepositoID, 5) AS DinheiroVL, ' +
               'dbo.fn_DepositoBancario_Posicao(DepositosBancarios.DepositoID, 8) AS ValorTotal, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=DepositosBancarios.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'ISNULL(dbo.fn_ContaBancaria_Nome(RelPesContaID, 5), SPACE(0)) AS NomeBanco, ' +
               '(CASE WHEN dbo.fn_Data_Zero(dtData) < dbo.fn_Data_Zero(GETDATE()) THEN 1 ELSE 0 END) AS DepositoDataMenor ' +
               'FROM DepositosBancarios WITH(NOLOCK) WHERE DepositoID = ' + nID + ' ORDER BY DepositoID DESC';

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);

    var sql;

	sql = 'SELECT *, ' +
          'CONVERT(VARCHAR, dtData, ' + DATE_SQL_PARAM + ') AS V_dtData, ' +
          'CONVERT(NUMERIC(4), dbo.fn_DepositoBancario_Posicao(DepositosBancarios.DepositoID, 3)) AS Quantidade, ' +
               'dbo.fn_DepositoBancario_Posicao(DepositosBancarios.DepositoID, 7) AS ValorCheques, ' +
               'dbo.fn_DepositoBancario_Posicao(DepositosBancarios.DepositoID, 5) AS DinheiroVL, ' +
               'dbo.fn_DepositoBancario_Posicao(DepositosBancarios.DepositoID, 8) AS ValorTotal, ' +
          '0 AS Prop1, 0 AS Prop2, ' +
          'ISNULL(dbo.fn_ContaBancaria_Nome(RelPesContaID, 5), SPACE(0)) AS NomeBanco, ' +
          '(CASE WHEN dbo.fn_Data_Zero(dtData) < dbo.fn_Data_Zero(GETDATE()) THEN 1 ELSE 0 END) AS DepositoDataMenor ' +
          'FROM DepositosBancarios WHERE DepositoID = 0';

    dso.SQL = sql;
}
