/********************************************************************
inferioresp.js

Library javascript para o inferior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:	
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
	setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
	fillCmbPastasInCtlBarInf(nTipoRegistroID)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao filtroComplete_DSC(emptyFilter) do js_detailinf.js

Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e no retorno chama a funcao de
ondatasetcomplete de acordo com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.DepositoID, a.ProprietarioID,a.AlternativoID,a.Observacoes FROM DepositosBancarios a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.DepositoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // Valores a Localizar
    else if (folderID == 28081)
    {
        dso.SQL = 'SELECT a.* FROM DepositosBancarios_ValoresLocalizar a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.DepositoID = ' + idToFind+ ' ' +
        'ORDER BY (SELECT ItemID From TiposAuxiliares_Itens b WITH(NOLOCK), ValoresLocalizar c WITH(NOLOCK) ' +
			'WHERE b.ItemID=c.FormaPagamentoID AND c.ValorID=a.ValorID), a.ValorID';
        return 'dso01Grid_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28082) // Lancamentos contabeis
    {
		dso.SQL = 'EXEC sp_EventosContabeis_Lancamentos ' + glb_nFormID + ', NULL, ' + idToFind;
		return 'dso01Grid_DSC';
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao dso01Grid_DSC() do js_detailinf.js

Monta string de select para combo de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp')
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Valores Localizar
    else if (pastaID == 28081)
    {
        if (dso.id == 'dso01GridLkp')
        {
            dso.SQL = 'SELECT a.ValorID, c.RecursoAbreviado AS Est, d.ItemAbreviado AS Forma, b.MotivoDevolucao, ' +
						'b.Documento, b.Emitente, b.dtEmissao, b.dtApropriacao, b.BancoAgencia, b.NumeroDocumento, ' +
						'f.SimboloMoeda AS Moeda, b.Valor, b.Identificador ' +
					'FROM DepositosBancarios_ValoresLocalizar a WITH(NOLOCK) ' +
					    'INNER JOIN ValoresLocalizar b WITH(NOLOCK) ON (a.ValorID = b.ValorID) ' +
					    'LEFT OUTER JOIN Pessoas e WITH(NOLOCK) ON (b.PessoaID=e.PessoaID) ' +
					    'INNER JOIN Recursos c WITH(NOLOCK) ON (b.EstadoID=c.RecursoID) ' +
					    'INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (b.FormaPagamentoID=d.ItemID) ' +
					    'INNER JOIN Conceitos f WITH(NOLOCK) ON (b.MoedaID=f.ConceitoID) ' +
					'WHERE (a.DepositoID = ' + nRegistroID + ')';
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(28081);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 28081); //Valores a Localizar

    vPastaName = window.top.getPastaName(28082);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 28082); // Lancamentos Contabeis

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
		if (folderID == 28081) // Valores Localizar
			currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['DepositoID', registroID]);

        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao buildGridFromFolder(folderID) do js_detailinf.js

Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Valores a Localizar
    else if (folderID == 28081)
    {
        headerGrid(fg,['Forma',
					   'ID',
					   'Est',
					   'Mot',
					   'Documento',
					   'Emitente',
					   'Emiss�o',
					   'Apropria��o',
					   'Banco',
					   'N�mero',
					   '$',
					   'Valor',
					   'Identificador',
                       'DepValorID'], [13]);

        fillGridMask(fg,currDSO,['^ValorID^dso01GridLkp^ValorID^Forma*',
								'ValorID',	
								'^ValorID^dso01GridLkp^ValorID^Est*',
								'^ValorID^dso01GridLkp^ValorID^MotivoDevolucao*',
								'^ValorID^dso01GridLkp^ValorID^Documento*',
								'^ValorID^dso01GridLkp^ValorID^Emitente*',
								'^ValorID^dso01GridLkp^ValorID^dtEmissao*',
								'^ValorID^dso01GridLkp^ValorID^dtApropriacao*',
								'^ValorID^dso01GridLkp^ValorID^BancoAgencia*',
								'^ValorID^dso01GridLkp^ValorID^NumeroDocumento*',
								'^ValorID^dso01GridLkp^ValorID^Moeda*',
								'^ValorID^dso01GridLkp^ValorID^Valor*',
								'^ValorID^dso01GridLkp^ValorID^Identificador*',
								'DepValorID'],
								 ['', '9999999999', '', '', '', '', dTFormat, dTFormat, '', '', '', '999999999.99','',''],
                                 ['', '##########','', '', '', '', dTFormat, dTFormat, '', '', '', '###,###,##0.00','','']);

		alignColsInGrid(fg,[1, 11]);
		fg.FrozenCols = 2;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 28082) // Lancamentos contabeis
    {
        headerGrid(fg,['Lan�amento',
                        'Est',
                        'Data',
                        'Balancete',
                        'Est',
                        'Tipo',
                        'Quant',
                        'Hist�rico Padr�o',
                        'Hist�rico Complementar',
                        'Detalhe',
                        'D',
                        'C',
                        'Moeda',
                        'Total Lan�amento',
                        'Taxa',
                        'Total Convertido',
                        'Observa��o',
                        'EventoID'], [17]);
                       
        glb_aCelHint = [[0,4,'� estorno?'],
						[0,6,'Quantidade de detalhes'],
						[0,10,'N�mero de D�bitos'],
						[0,11,'N�mero de Cr�ditos'],
						[0,11,'Taxa de convers�o para moeda comum']];

        fillGridMask(fg,currDSO,['LancamentoID',
                                 'Estado',
                                 'dtLancamento',
                                 'dtBalancete',
                                 'EhEstorno',
                                 'TipoLancamento',
                                 'Quantidade',
                                 'HistoricoPadrao',
                                 'HistoricoComplementar',
                                 'Detalhe',
                                 'NumeroDebitos',
                                 'NumeroCreditos',
                                 'Moeda',
                                 'ValorTotalLancamento',
                                 'TaxaMoeda',
                                 'ValorTotalConvertido',
                                 'Observacao',
                                 'EventoID'],
                                 ['9999999999','','99/99/9999','99/99/9999','','','999999','','','','99','99','',
									'999999999.99','9999999999999.99','999999999.99','',''],
								 ['##########','',dTFormat,dTFormat,'','','######','','','','##','##','',
									'###,###,##0.00','#,###,###,###,##0.0000000','###,###,##0.00','','']);

        alignColsInGrid(fg,[0,6,10,11,13,14,15]);
        fg.FrozenCols = 2;
    }
    
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************
