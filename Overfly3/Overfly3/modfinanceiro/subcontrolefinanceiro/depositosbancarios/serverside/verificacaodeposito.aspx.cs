using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.depositosbancarios.serverside
{
	public partial class verificacaodeposito : System.Web.UI.OverflyPage
	{
		private Integer depositoID = Constants.INT_ZERO;
		protected Integer nDepositoID
		{
			get { return depositoID; }
			set { depositoID = value; }
		}

		private Integer estadoDeID = Constants.INT_ZERO;
		protected Integer nEstadoDeID
		{
			get { return estadoDeID; }
			set { estadoDeID = value; }
		}

		private Integer estadoParaID = Constants.INT_ZERO;
		protected Integer nEstadoParaID
		{
			get { return estadoParaID; }
			set { estadoParaID = value; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"SELECT dbo.fn_DepositoBancario_Verifica( " + nDepositoID + ", " +
					nEstadoDeID + ", " +
					nEstadoParaID + ", GETDATE()) AS Verificacao"
			));
		}
	}
}
