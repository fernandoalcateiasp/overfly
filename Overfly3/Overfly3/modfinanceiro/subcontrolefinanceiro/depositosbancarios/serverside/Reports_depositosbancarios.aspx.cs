﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using OVERFLYSVRCFGLib;
using WSData;
using System.Drawing.Printing;
using System.Drawing;

namespace Overfly3.PrintJet
{
    public partial class Reports_depositosbancarios : System.Web.UI.Page
    {
        string sEmpresaLogada = HttpContext.Current.Request.Params["nEmpresaLogada"];
        string glb_sEmpresaFantasia = HttpContext.Current.Request.Params["glb_sEmpresaFantasia"];
        int formato = Convert.ToInt32(HttpContext.Current.Request.Params["formato"]);
        string chkDinheiro = HttpContext.Current.Request.Params["chkDinheiro"];
        string chkCheque = HttpContext.Current.Request.Params["chkCheque"];
        string strDataInicio = HttpContext.Current.Request.Params["strDataInicio"];
        string strDataFim = HttpContext.Current.Request.Params["strDataFim"];
        string sFiltroConta = HttpContext.Current.Request.Params["sFiltroConta"];
        string strParams = HttpContext.Current.Request.Params["strParams"];

        string SQLDetail1 = string.Empty;
        string SQLDetail2 = string.Empty;
        string SQLDetail3 = string.Empty;


        int Datateste = 0;
        bool nulo = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    
                   //Depósito Bancário
                    case "40196":
                        Depositos();
                        break;
                }
            }
        }


        private void Depositos()
        {
            string Title = "Depósitos";
            int iQLinhas = 0;

            if (chkDinheiro == "1")
            {
                //Resumido
                SQLDetail1 = "SELECT CONVERT(VARCHAR(10),a.dtData,103) AS Data, a.DepositoID AS DepositoID, a.DepositoID AS DepositoID2, dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) AS Conta, " +
                                    "b.ItemID AS TipoID, dbo.fn_Tradutor(b.ItemAbreviado, NULL, -a.EmpresaID, NULL) AS Forma,  " +
                                    "(dbo.fn_DepositoBancario_Posicao(a.DepositoID, 4) + dbo.fn_DepositoBancario_Posicao(a.DepositoID, 5)) AS ValorDeposito, " +
                                    "NULL AS ValorID, ' ' AS ValorAplicado, NULL AS FinanceiroID, ' ' AS Duplicata, ' ' AS Pessoa " +
                                "FROM DepositosBancarios a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) " +
                                "WHERE (a.EmpresaID = " + sEmpresaLogada + " AND a.EstadoID = 53 " +
                                    strDataInicio + strDataFim + sFiltroConta + " AND " +
                                    "(dbo.fn_DepositoBancario_Posicao(a.DepositoID, 4) > 0 OR dbo.fn_DepositoBancario_Posicao(a.DepositoID, 5) > 0) AND " +
                                    "b.ItemID = 1031) ";

                //Detalhe
                SQLDetail2 = "SELECT ' ' AS Data, a.DepositoID , NULL AS DepositoID2, ' ' AS Conta, b.ItemID AS TipoID, NULL AS Forma, NULL AS ValorDeposito,  " +
                                        "NULL AS ValorID, dbo.fn_Numero_Formata(dbo.fn_DepositoBancario_Posicao(a.DepositoID, 4),2,1,103) AS ValorAplicado, NULL AS FinanceiroID, NULL AS Duplicata, NULL AS Pessoa " +
                                    "FROM DepositosBancarios a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) " +
                                    "WHERE (a.EmpresaID = " + sEmpresaLogada + " AND a.EstadoID = 53 " +
                                        strDataInicio + strDataFim + sFiltroConta + " AND " +
                                        "dbo.fn_DepositoBancario_Posicao(a.DepositoID, 4) > 0 AND " +
                                        "b.ItemID = 1031) " +
                                "UNION ALL SELECT ' ' AS Data, a.DepositoID, NULL AS DepositoID2, ' ' AS Conta, g.ItemID AS TipoID, NULL AS Forma, NULL AS ValorDeposito,  " +
                                        "c.ValorID AS ValorID, dbo.fn_Numero_Formata(d.Valor,2,1,103) AS ValorAplicado, e.FinanceiroID AS FinanceiroID, e.Duplicata AS Duplicata, f.Fantasia AS Pessoa " +
                                    "FROM DepositosBancarios a WITH(NOLOCK), DepositosBancarios_ValoresLocalizar b WITH(NOLOCK),  " +
                                        "ValoresLocalizar c WITH(NOLOCK), ValoresLocalizar_Financeiros d WITH(NOLOCK), Financeiro e WITH(NOLOCK), Pessoas f WITH(NOLOCK), TiposAuxiliares_Itens g WITH(NOLOCK) " +
                                    "WHERE (a.EmpresaID = " + sEmpresaLogada + " AND a.EstadoID = 53 " +
                                        strDataInicio + strDataFim + sFiltroConta + " AND " +
                                        "dbo.fn_DepositoBancario_Posicao(a.DepositoID, 5) > 0 AND a.DepositoID = b.DepositoID AND b.ValorID = c.ValorID AND  " +
                                        "c.FormaPagamentoID = 1031 AND c.ValorID = d.ValorID AND d.OK = 1 AND d.FinanceiroID = e.FinanceiroID AND " +
                                        "e.PessoaID = f.PessoaID AND	c.FormaPagamentoID = g.ItemID) ";
            }

            if (chkCheque == "1")
            {
                SQLDetail1 += (SQLDetail1 == "" ? "" : "UNION ALL ");

                //Resumido
                SQLDetail1 += "SELECT CONVERT(VARCHAR(10),a.dtData,103) AS Data, a.DepositoID AS DepositoID, a.DepositoID AS DepositoID2, dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) AS Conta, " +
                                    "b.ItemID AS TipoID, dbo.fn_Tradutor(b.ItemAbreviado, NULL, -a.EmpresaID, NULL) AS Forma, " +
                                    "dbo.fn_DepositoBancario_Posicao(a.DepositoID, 7) AS ValorDeposito, " +
                                    "NULL AS ValorID, ' ' AS ValorAplicado, NULL AS FinanceiroID, ' ' AS Duplicata, ' ' AS Pessoa " +
                                "FROM DepositosBancarios a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) " +
                                "WHERE (a.EmpresaID = " + sEmpresaLogada + " AND a.EstadoID = 53 " +
                                    strDataInicio + strDataFim + sFiltroConta + " AND " +
                                    "dbo.fn_DepositoBancario_Posicao(a.DepositoID, 7) > 0 AND " +
                                    "b.ItemID = 1032) ";

                SQLDetail2 += (SQLDetail2 == "" ? "" : " UNION ALL ");

                //Detalhe
                SQLDetail2 += "SELECT ' ' AS Data, a.DepositoID, NULL AS DepositoID2, ' ' AS Conta, g.ItemID AS TipoID, NULL AS Forma, NULL AS ValorDeposito, " +
                                    "c.ValorID AS ValorID, dbo.fn_Numero_Formata(d.Valor,2,1,103) AS ValorAplicado, e.FinanceiroID AS FinanceiroID, e.Duplicata AS Duplicata, f.Fantasia AS Pessoa " +
                                "FROM DepositosBancarios a WITH(NOLOCK), DepositosBancarios_ValoresLocalizar b WITH(NOLOCK), " +
                                    "ValoresLocalizar c WITH(NOLOCK), ValoresLocalizar_Financeiros d WITH(NOLOCK), Financeiro e WITH(NOLOCK), Pessoas f WITH(NOLOCK), TiposAuxiliares_Itens g WITH(NOLOCK) " +
                                "WHERE (a.EmpresaID = " + sEmpresaLogada + " AND a.EstadoID = 53 " +
                                    strDataInicio + strDataFim + sFiltroConta + " AND " +
                                    "dbo.fn_DepositoBancario_Posicao(a.DepositoID, 7) > 0 AND a.DepositoID = b.DepositoID AND b.ValorID = c.ValorID AND " +
                                    "c.FormaPagamentoID = 1032 AND c.ValorID = d.ValorID AND d.OK = 1 AND d.FinanceiroID = e.FinanceiroID AND " +
                                    "e.PessoaID = f.PessoaID AND	c.FormaPagamentoID = g.ItemID) ";
            }

            SQLDetail1 += "ORDER BY Data, DepositoID, Conta, TipoID";
            SQLDetail2 += "ORDER BY DepositoID, TipoID, ValorAplicado, FinanceiroID";

            var Header_Body = (formato == 1 ? "Header" : "Body");

            ClsReport Relatorio = new ClsReport();

            string[,] arrQuerys;
            string tabelaContar = "SQLDetail1";


            arrQuerys = new string[,] { { "SQLDetail1", SQLDetail1 },
                                    { "SQLDetail2", SQLDetail2 }};


            string[,] arrIndexKey = new string[,] { { "SQLDetail2", "DepositoID", "DepositoID" } };

            if (formato == 1)
            {
                Relatorio.CriarRelatório(Title, arrQuerys, "BRA", "Landscape", "1.8", "Default", "Default", true);
            }
            else
            {
                Relatorio.CriarRelatório(Title, arrQuerys, "BRA", "Excel");
            }



            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            iQLinhas = dsFin.Tables[tabelaContar].Rows.Count;


            if (iQLinhas == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {
                double headerX = 0.2;
                double headerY = 0.4;
                double tabelaTitulo = 1.40;

                double tabelaX = 0.2;
                double tabelaY = 0;

                if (formato == 1)
                {

                    //Labels Cabeçalho
                    Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "12", "black", "B", "Header", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 12.5).ToString(), headerY.ToString(), "12", "#0113a0", "B", "Header", "5", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 23.4).ToString(), headerY.ToString(), "10", "black", "B", "Header", "3.72187", "Horas");

                    Relatorio.CriarNumeroPagina((headerX + 27).ToString(), (headerY).ToString(), "10", "Black", "B", false);

                    //Parametros(Filtros) selecionados
                    Relatorio.CriarObjLabel("Fixo", "", strParams, (headerX).ToString(), (headerY + 0.5).ToString(), "9", "black", "B", "Header", "29", "");

                    //Linha
                    Relatorio.CriarObjLinha((headerX).ToString(), (headerY + 0.9).ToString(), "29", "", "Header");

                                    
                    //Cabeçalho
                    Relatorio.CriarObjLabel("Fixo", "", "Data", (headerX).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "2", "");
                    Relatorio.CriarObjLabel("Fixo", "", "ID", (headerX += 2).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "1.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Conta", (headerX += 1.5).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "4", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Forma", (headerX += 4.2).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "1.5", "Center");
                    Relatorio.CriarObjLabel("Fixo", "", "Valor", (headerX += 1).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "2.2", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "ID", (headerX += 2.8).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "1.5", "Center");
                    Relatorio.CriarObjLabel("Fixo", "", "Valor", (headerX += 0.7).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "2.2", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "Financeiro", (headerX += 2.7).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "2", "Center");
                    Relatorio.CriarObjLabel("Fixo", "", "Duplicata", (headerX += 2.1).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "2", "Center");
                    Relatorio.CriarObjLabel("Fixo", "", "Pessoa", (headerX += 1.8).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "4", "");

                    //Tabela
                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), arrIndexKey, false, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "Data", false, "2", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup", "Null", "Null", 0, true, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Total Geral", "TotalGroup", "", "DetailGroup", 4, true, "10");

                    Relatorio.CriarObjColunaNaTabela(" ", "DepositoID2", false, "1.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "DepositoID2", "DetailGroup", "Null", "Null", 0, true, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela(" ", "Conta", false, "4", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Conta", "DetailGroup", "Null", "Null", 0, true, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela(" ", "Forma", false, "1.5", "Center", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Forma", "DetailGroup", "Null", "Null", 0, true, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela(" ", "ValorDeposito", false, "2.2", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorDeposito", "DetailGroup", "Null", "Null", 0, true, "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorDeposito", "TotalGroup", "", "DetailGroup", 0, true, "10", "<Format>#,0.00</Format>");

                    Relatorio.CriarObjColunaNaTabela(" ", "ValorID", false, "1.5", "Center", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorID", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela(" ", "ValorAplicado", false, "2.2", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorAplicado", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela(" ", "FinanceiroID", false, "2", "Center", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "FinanceiroID", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela(" ", "Duplicata", false, "2", "Center", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Duplicata", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela(" ", "Pessoa", false, "4", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Pessoa", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.TabelaEnd();

                }

                else if (formato == 2)
                {
                    Relatorio.CriarObjTabela("0", "0", arrIndexKey, false, true);

                    Relatorio.CriarObjColunaNaTabela("Data", "Data", false, "2", "Default", "10", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup", "Null", "Null", 0, true, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Total Geral", "TotalGroup", "", "DetailGroup", 4, true, "10");

                    Relatorio.CriarObjColunaNaTabela("ID", "DepositoID2", false, "1.5", "Default", "10", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "DepositoID2", "DetailGroup", "Null", "Null", 0, true, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela("Conta", "Conta", false, "4", "Default", "10", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Conta", "DetailGroup", "Null", "Null", 0, true, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela("Forma", "Forma", false, "1.5", "Center", "10", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Forma", "DetailGroup", "Null", "Null", 0, true, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela("Valor", "ValorDeposito", false, "2.2", "Right", "10", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorDeposito", "DetailGroup", "Null", "Null", 0, true, "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorDeposito", "TotalGroup", "", "DetailGroup", 0, true, "10", "<Format>#,0.00</Format>");

                    Relatorio.CriarObjColunaNaTabela("ID", "ValorID", false, "1.5", "Center", "10", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorID", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela("Valor", "ValorAplicado", false, "2.2", "Right", "10", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorAplicado", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela("Financeiro", "FinanceiroID", false, "2", "Center", "10", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "FinanceiroID", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela("Duplicata", "Duplicata", false, "2", "Center", "10", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Duplicata", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.CriarObjColunaNaTabela("Pessoa", "Pessoa", false, "4", "Default", "10", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Pessoa", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "10");

                    Relatorio.TabelaEnd();

                }

                Relatorio.CriarPDF_Excel(Title, formato);
            }
        }

        protected dataInterface DataInterfaceObj = new dataInterface(
        System.Configuration.ConfigurationManager.AppSettings["application"]);

        private DataTable RequestDatatable(string SQLQuery, string NomeQuery)
        {
            string strConn = "";

            Object oOverflySvrCfg = null;

            DataTable Dt = new DataTable();

            DataSet DsContainer = new DataSet();

            oOverflySvrCfg = comCreateObject("OverflySvrCfg.OverflyMTS");
            strConn = ((OVERFLYSVRCFGLib.OverflyMTS)oOverflySvrCfg).DotNetDataBaseStrConn(DataInterfaceObj.ApplicationName);

            SqlCommand cmd = new SqlCommand(SQLQuery);
            using (SqlConnection con = new SqlConnection(strConn))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    cmd.CommandTimeout = 0;
                    sda.SelectCommand = cmd;

                    sda.Fill(DsContainer, NomeQuery);
                }
            }

            Dt = DsContainer.Tables[NomeQuery];

            return Dt;
        }

        private static Object comCreateObject(string sProgID)
        {
            Type oType = Type.GetTypeFromProgID(sProgID);
            if (oType != null)
            {
                return Activator.CreateInstance(oType);
            }
            return null;
        }
    }
}