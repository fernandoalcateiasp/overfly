/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// controla o retorno do servidor dos dados de combos dinamicos
// (selSujeitoID e selObjetoID)
var glb_CounterCmbsDynamics = 0;
// guarda o botao clicado enviado pelo frameword
var glb_BtnFromFramWork;
// guarda o dias base da empresa logada
var glb_nDiasBase = null;
// variaveis de timer
var glb_FinanceiroSupTimer = null;
var glb_BloquetoSupTimer = null;


var dsoSup01 = new CDatatransport('dsoSup01');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoCmbDynamic02 = new CDatatransport('dsoCmbDynamic02');
var dsoCmbDynamic03 = new CDatatransport('dsoCmbDynamic03');
var dsoCmbDynamic04 = new CDatatransport('dsoCmbDynamic04');
var dsoCmbDynamic05 = new CDatatransport('dsoCmbDynamic05');
var dsoCmbComando = new CDatatransport('dsoCmbComando');
var dsoCurrData = new CDatatransport('dsoCurrData');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoParceiroData = new CDatatransport('dsoParceiroData');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoVerificacao = new CDatatransport('dsoVerificacao');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload() {
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);

    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selFormaPagamentoID', '1'],
                          ['selHistoricoPadraoID', '2'],
                          ['selMoedaID', '3'],
                          ['selMoedaOcorrenciaID', '4'],
                          ['selRelPesContaID', '5'],
                          ['selImpostoID', '7'],
                          ['selGuiaRecolhimento', '8']
    ]);

    windowOnLoad_1stPart();

    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/inferior.asp',
                              SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/pesquisa.asp');

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);

    windowOnLoad_2ndPart();

    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'FinanceiroID';

    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

function setupPage() {
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup', [[1, 'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
                          ['lblEstadoID', 'txtEstadoID', 2, 1],
                          ['lblSituacaoCobrancaID', 'selSituacaoCobrancaID', 9, 1],
                          ['lblPedido', 'txtPedido', 10, 1],
                          ['lblDuplicata', 'txtDuplicata', 15, 1],
                          ['lblPessoaID', 'selPessoaID', 20, 1],
                          ['btnFindPessoa', 'btn', 21, 1],
                          ['lblParceiroID', 'selParceiroID', 20, 1],
                          ['lbldtEmissao', 'txtdtEmissao', 10, 2],
                          ['lblFormaPagamentoID', 'selFormaPagamentoID', 7, 2],
                          ['lblPrazoPagamento', 'txtPrazoPagamento', 4, 2],
                          ['lbldtVencimento', 'txtdtVencimento', 10, 2],
                          ['lblDiasCompensacao', 'txtDiasCompensacao', 3, 2],
                          ['lbldtFluxoCaixa', 'txtdtFluxoCaixa', 10, 2],
                          ['lblEhEstorno', 'chkEhEstorno', 3, 2, -4],
                          ['lblEhImporte', 'chkEhImporte', 3, 2, -4],
                          ['lblHistoricoPadraoID', 'selHistoricoPadraoID', 24, 2, -4],
                          ['lblImpostoID', 'selImpostoID', 9, 2, -4],
                          ['lblGuiaRecolhimento', 'selGuiaRecolhimento', 10, 2, -4],
                          ['lblCodigoArrecadacao', 'txtCodigoArrecadacao', 10, 2],
                          ['lblMoedaID', 'selMoedaID', 7, 3, -2],
                          ['lblValor', 'txtValor', 11, 3, -2],
                          ['lblTaxaMoeda', 'txtTaxaMoeda', 11, 3, -4],
                          ['lblValorConvertido', 'txtValorConvertido', 11, 3, -4],
                          ['lblMoedaOcorrenciaID', 'selMoedaOcorrenciaID', 7, 3, -10],
                          ['lblTaxaAtualizacao', 'txtTaxaAtualizacao', 6, 3, -2],
                          ['lblObservacao', 'txtObservacao', 30, 3, -12],
                          ['lblMarketing', 'chkMarketing', 3, 3],
                          ['lblNossoNumero', 'txtNossoNumero', 17, 4],
                          ['lblRelPesContaID', 'selRelPesContaID', 12, 4],
                          ['lblCarteira', 'txtCarteira', 4, 4],
                          ['lblModalidadeID', 'selModalidadeID', 7, 4, -8],
                          ['lblComandoID', 'selComandoID', 5, 4],
                          ['lblInstrucao1', 'txtInstrucao1', 3, 4, -3],
                          ['lblInstrucao2', 'txtInstrucao2', 3, 4, -3],
                          ['lblCodigoBarra', 'txtCodigoBarra', 39, 4],
                          ['lblTotalPrincipal', 'txtTotalPrincipal', 11, 5, 0, -4],
                          ['lblTotalEncargos', 'txtTotalEncargos', 11, 5],
                          ['lblTotalDespesas', 'txtTotalDespesas', 11, 5],
                          ['lblTotalAbatimentos', 'txtTotalAbatimentos', 11, 5, -7],
                          ['lblTotalAmortizacoes', 'txtTotalAmortizacoes', 11, 5, -13],
                          ['lblSaldoDevedor', 'txtSaldoDevedor', 11, 5, -18],
                          ['lblSaldoAtualizado', 'txtSaldoAtualizado', 11, 5],
                          ['lblPercAmortizacao', 'txtPercAmortizacao', 7, 5]], null, null, true);

    chkEhImporte.onclick = chkEhImporte_onclick;
    chkEhEstorno.onclick = chkEhEstorno_onclick;

    refillCmbHistoricoPadrao(chkEhEstorno.checked, chkEhImporte.checked);
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2) {
    if (param1 == 'SHOWFINANCEIRO') {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if (__currFormState() != 0)
            return null;

        // param2 - traz array:
        // o id da empresa do form que mandou
        // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();

        if (param2[0] != empresa[0])
            return null;

        window.top.focus();

        if (param2[2] != null) {
            sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bRefreshInf = true');
        }

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);

        return new Array(getHtmlId(), null, null);
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2) {

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@

    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.

    /*Marco pediu para retirar. S� era utilizado em cobranca COB. DCS 22/02/2008 15:19*/
    /*
    txtNossoNumero.onchange = chkNossoNumero;
    */
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked) {
    //@@
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;

    if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.Find('fldID', '0');
        if (!dsoEstaticCmbs.recordset.EOF)
            glb_nDiasBase = dsoEstaticCmbs.recordset['fldName'].value;
    }


    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR')) {
        if (dsoSup01.recordset['CorObservacao'] != null && dsoSup01.recordset['CorObservacao'].value != null && dsoSup01.recordset['CorObservacao'].value != '') {
            txtObservacao.style.backgroundColor =
				eval(gridToInternationalColor(dsoSup01.recordset['CorObservacao'].value));
        }

        paintObservacoes();
        startDynamicCmbs();
    }
    else {
        finalOfSupCascade(glb_BtnFromFramWork);
    }
}


function paintObservacoes() {
    txtObservacao.style.backgroundColor = 'white';

    if ((dsoSup01.recordset['CorObservacao'] != null) &&
		(dsoSup01.recordset['CorObservacao'].value != null) &&
    (dsoSup01.recordset['CorObservacao'].value != '')) {
        txtObservacao.style.backgroundColor = eval(gridToInternationalColor(dsoSup01.recordset['CorObservacao'].value));
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked) {
    //@@

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Parceiro', 'Gerar Financeiros',
		'Ativar Financeiros', 'Gerar Valor a Localizar', 'Fluxo de caixa', 'Ocorr�ncias', 'Guias', 'Cobran�a', 'Demonstrativo de reten��es']);

    if (btnClicked == 'SUPINCL') {
        selMoedaID.disabled = false;
        setReadOnlyFields();

        if (!((dsoCurrData.recordset.BOF) && (dsoCurrData.recordset.EOF))) {
            txtdtEmissao.value = dsoCurrData.recordset['currDate'].value;
        }

        txtPrazoPagamento.value = 0;

        setCalcDateFields(txtPrazoPagamento);
        setValueInControlLinked(selMoedaID, dsoSup01);

        dsoSup01.recordset['EhEstorno'].value = false;
        dsoSup01.recordset['EhImporte'].value = true;

        selSituacaoCobrancaID.value = 1011;
        dsoSup01.recordset['SituacaoCobrancaID'].value = 1011;
        adjustLabelsCombos(false);
        refillCmbHistoricoPadrao(dsoSup01.recordset['EhEstorno'].value, dsoSup01.recordset['EhImporte'].value);
    }

    adjustCobranca(false);

    txtdtEmissao.onblur = setCalcDateFields;
    /*if (getCloneFromOriginal('txtdtEmissao') != null)
        getCloneFromOriginal('txtdtEmissao').onblur = setCalcDateFields;
        */
    txtPrazoPagamento.onblur = setCalcDateFields;
    //if (getCloneFromOriginal('txtPrazoPagamento') != null)
    //    getCloneFromOriginal('txtPrazoPagamento').onblur = setCalcDateFields;

    txtdtVencimento.onblur = setCalcDateFields;
    //if (getCloneFromOriginal('txtdtVencimento') != null)
    //    getCloneFromOriginal('txtdtVencimento').onblur = setCalcDateFields;

    txtDiasCompensacao.onblur = setCalcDateFields;
    //if (getCloneFromOriginal('txtDiasCompensacao') != null)
    //    getCloneFromOriginal('txtDiasCompensacao').onblur = setCalcDateFields;

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    if (cmbID == 'selFormaPagamentoID')
        adjustCobranca(true);
    else if (cmbID == 'selRelPesContaID') {
        clearComboEx(['selComandoID']);
        selComandoID.disabled = true;
        preencheComboComando();
    }
    else if (cmbID == 'selParceiroID')
        setLabelParceiro(selParceiroID.options[selParceiroID.selectedIndex].value);
    else if (cmbID == 'selHistoricoPadraoID')
    {
        setReadOnlyFields();
        showselImpostoID();
    }
        

    adjustLabelsCombos(false);

    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selTipoRegistroID')
        adjustSupInterface();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK(currEstadoID, newEstadoID) {
    verifyFinanceiroInServer(currEstadoID, newEstadoID);
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID, bSavedID) {
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {
    if (btnClicked.id == 'btnFindPessoa')
        openModalPessoa();
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    var empresa = getCurrEmpresaData();
    var contexto = 0;
    var sAnchor;
    contexto = getCmbCurrDataInControlBar('sup', 1);

    if (controlBar == 'SUP') {
        // Documentos
        if (btnClicked == 1) {
            __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
        }

            // Imprimir
        else if (btnClicked == 2)
            openModalPrint();

            // Procedimento
        else if (btnClicked == 3) {
            if (contexto[1] == 9111)
                sAnchor = '211';
            else
                sAnchor = '212';

            window.top.openModalControleDocumento('S', '', 910, null, sAnchor, 'T');
        }

            // Detalhar Parceiro
        else if ((btnClicked == 4) && (selParceiroID.value != null)) {
            // Manda o id do conceito a detalhar 
            sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0],
                          dsoSup01.recordset['ParceiroID'].value));
        }
            /*
                    // Detalhar Pedido
                    else if ( btnClicked == 4 )
                    {
                        // Manda o id do conceito a detalhar 
                        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], 
                                      dsoSup01.recordset['PedidoID'].value));
                    }
            */
            // Gerar Financeiro
        else if (btnClicked == 5) {
            openModalGerarFinanceiros();
        }

            // Gerar valor a localizar
        else if (btnClicked == 7) {
            openModalGerarValoresLocalizar();
        }

            // Gerar Demonstrativo de reten��es
        else if (btnClicked == 12) {
            openModalDemonstrativoRetencoes();
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    // Para prosseguir a automacao retornar null
    if ((controlBar.toUpperCase() == 'SUP') && (btnClicked.toUpperCase() == 'SUPOK')) {
        // validacao do campo Emissao
        var ddtEmissao;
        ddtEmissao = txtdtEmissao;

        if (chkDataEx(ddtEmissao.value) != true) {
            if (window.top.overflyGen.Alert('Data inv�lida') == 0)
                return null;
            ddtEmissao.focus();
            return false;
        }

        // validacao do campo Vencimento
        var ddtVencimento;
        ddtVencimento = txtdtVencimento;

        if (chkDataEx(ddtVencimento.value) != true) {
            if (window.top.overflyGen.Alert('Data inv�lida') == 0)
                return null;

            ddtVencimento.focus();
            return false;
        }

        // se a data de emissao for maior q o vencimento
        if (window.top.daysBetween(ddtEmissao.value, ddtVencimento.value) < 0) {
            if (window.top.overflyGen.Alert('A data de vencimento deve ser maior ou igual a data de emiss�o.') == 0)
                return null;

            ddtVencimento.focus();
            return false;
        }

        // se e um registro novo
        if (dsoSup01.recordset[glb_sFldIDName].value == null) {
            var aEmpresa = getCurrEmpresaData();
            var aContexto = getCmbCurrDataInControlBar('sup', 1);
            dsoSup01.recordset['EmpresaID'].value = aEmpresa[0];

            // Pagamento
            if (aContexto[1] == 9111)
                dsoSup01.recordset['TipoFinanceiroID'].value = 1001;
                // Recebimento    
            else if (aContexto[1] == 9112)
                dsoSup01.recordset['TipoFinanceiroID'].value = 1002;

        }

        // Nao permite selecionar comandos diferentes de 01 se nao tiver
        // nosso numero
        if ((selComandoID.selectedIndex > 1) && (trimStr(txtNossoNumero.value) == '')) {
            if (window.top.overflyGen.Alert('N�o � poss�vel selecionar este comando sem ter o Nosso N�mero.') == 0)
                return null;

            selComandoID.focus();
            return false;
        }

        // Valida nosso numero
        /*Marco pediu para retirar. S� era utilizado em cobranca COB. DCS 22/02/2008 15:19*/
        /*
        if (chkNossoNumero(txtNossoNumero) == false)
			return false;
		*/
    }
    else if (((controlBar.toUpperCase() == 'SUP') || (controlBar.toUpperCase() == 'PESQLIST')) &&
		(btnClicked.toUpperCase() == 'SUPINCL')) {
        setLabelPessoa();
        setLabelParceiro();
        refillCmbHistoricoPadrao(false, true);

        formatCalcFields(true);

        // Seta a Data
        dsoCurrData.URL = SYS_ASPURLROOT + '/serversidegenEx/currdate.aspx?currDateFormat=' + escape(DATE_FORMAT);
        dsoCurrData.ondatasetcomplete = dsoCurrDataComplete_DSC;
        dsoCurrData.refresh();
        // para a automacao, para prosseguir na funcao
        return true;
    }

    return null;
}

function dsoCurrDataComplete_DSC() {
    // Prossegue a automacao interrompida no botao de inclusao
    lockAndOrSvrSup_SYS();
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALPESSOAHTML') {
        if (param1 == 'OK') {
            // Preenche o combo de parceiro
            fillComboPessoa(param2);
            // esta funcao que fecha a janela modal e destrava a interface
            // restoreInterfaceFromModal() foi movida para final das funcoes
            // chamadas pela funcao fillComboPessoa;    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
        // Modal de impressao
    else if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            if (param2 == 'BLOQUETO') {
                glb_BloquetoSupTimer = window.setInterval('openModalPrintBloqueto_Timer()', 100, 'JavaScript');
            }
            else {
                // esta funcao fecha a janela modal e destrava a interface
                restoreInterfaceFromModal();
                // escreve na barra de status
                writeInStatusBar('child', 'cellMode', 'Detalhe');
            }

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            if (param2 == true)
                glb_FinanceiroSupTimer = window.setInterval('refreshSup()', 10, 'JavaScript');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALGERAVALORLOCALIZARHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALDEMONSTRATIVORETENCOESHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALGERARFINANCEIROSHTML') {
        if (param1 == 'OK') {
            alert("aqui2");
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
        // Modal bloqueto de cobranca
    else if (idElement.toUpperCase() == 'MODALBLOQUETOCOBRANCAHTML') {

        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode() {
    setReadOnlyFields();
    direitoCampos();

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad() {
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    // mostra quatro botoes especificos 
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Parceiro', 'Gerar Financeiros',
		'Ativar Financeiros', 'Gerar Valor a Localizar', 'Fluxo de caixa', 'Ocorr�ncias', 'Guias', 'Cobran�a', 'Demonstrativo de reten��es']);
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de parceiro 

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPessoa() {
    var htmlPath;
    var strPars = new Array();
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];
    var nContextoID = getCmbCurrDataInControlBar('sup', 1);

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa);
    strPars += '&nContextoID=' + escape(nContextoID[1]);

    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalpessoa.asp' + strPars;
    showModalWin(htmlPath, new Array(571, 284));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de parceiro 

Parametro:
aParceiro   Array: item 0->Nome
                   item 1->ID

Retorno:
nenhum
********************************************************************/
function fillComboPessoa(aPessoa) {
    clearComboEx(['selPessoaID', 'selParceiroID']);
    var oldDataSrc = selPessoaID.dataSrc;
    var oldDataFld = selPessoaID.dataFld;
    selPessoaID.dataSrc = '';
    selPessoaID.dataFld = '';
    var oOption = document.createElement("OPTION");
    oOption.text = aPessoa[0];
    oOption.value = aPessoa[1];
    selPessoaID.add(oOption);
    selPessoaID.dataSrc = oldDataSrc;
    selPessoaID.dataFld = oldDataFld;
    if (selPessoaID.options.length != 0) {
        setValueInControlLinked(selPessoaID, dsoSup01);
        setLabelPessoa(dsoSup01.recordset['PessoaID'].value);
        fillComboParceiro(aPessoa);
    }
    else
        restoreInterfaceFromModal();
}

/********************************************************************
Funcao criada pelo programador.
Preenche o combo de parceiro 

Parametro:
aPessoa     Array: item 0->Nome
                   item 1->ID
                   item 2->0-Fornecedor
                           1-Cliente
                   item 3->0-Usuario
                           1-Cliente
                           2-Usuario/Cliente
Retorno:
nenhum
********************************************************************/
function fillComboParceiro(aPessoa) {
    var nValue, sText;

    // Fornecedor ou Cliente Direto ou Pessoa
    if ((aPessoa[2] == 0) || ((aPessoa[2] == 1) && (aPessoa[3] == 1)) || (aPessoa[3] == 3)) {
        nValue = aPessoa[1];
        sText = aPessoa[0];
        var oldDataSrc = selParceiroID.dataSrc;
        var oldDataFld = selParceiroID.dataFld;
        selParceiroID.dataSrc = '';
        selParceiroID.dataFld = '';
        var oOption = document.createElement("OPTION");
        oOption.text = sText;
        oOption.value = nValue;
        selParceiroID.add(oOption);
        selParceiroID.dataSrc = oldDataSrc;
        selParceiroID.dataFld = oldDataFld;
        restoreInterfaceFromModal();
        if (selParceiroID.options.length != 0) {
            setValueInControlLinked(selParceiroID, dsoSup01);
            setLabelParceiro(dsoSup01.recordset['ParceiroID'].value);
            selParceiroID.disabled = false;
        }

    }
        // Cliente Indireto
    else {
        var nEmpresaID = getCurrEmpresaData();

        strSQL = 'SELECT c.PessoaID, c.Fantasia ' +
                 'FROM RelacoesPessoas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
                 'WHERE a.SujeitoID= ' + aPessoa[1] + ' AND a.TipoRelacaoID=21 AND a.EstadoID=2 ' +
                 'AND a.ObjetoID=b.SujeitoID AND b.TipoRelacaoID=21 AND b.EstadoID=2 ' +
                 'AND b.ObjetoID=' + nEmpresaID[0] + ' ' +
                 'AND b.SujeitoID=c.PessoaID AND c.EstadoID=2 ' +
                 'UNION ALL ' +
                 'SELECT b.PessoaID, b.Fantasia ' +
                 'FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                 'WHERE a.SujeitoID= ' + aPessoa[1] + ' AND a.TipoRelacaoID=21 AND a.EstadoID=2 ' +
                 'AND a.ObjetoID= ' + nEmpresaID[0] + ' AND b.PessoaID=a.SujeitoID ' +
                 'ORDER BY Fantasia';

        setConnection(dsoParceiroData);
        dsoParceiroData.SQL = strSQL;
        dsoParceiroData.ondatasetcomplete = Parceiro_DSC;
        dsoParceiroData.Refresh();
    }
}

function Parceiro_DSC() {
    var bDisableParceiroID = true;

    if (!((dsoParceiroData.recordset.BOF) && (dsoParceiroData.recordset.EOF))) {
        var oldDataSrc = selParceiroID.dataSrc;
        var oldDataFld = selParceiroID.dataFld;
        selParceiroID.dataSrc = '';
        selParceiroID.dataFld = '';
        while (!dsoParceiroData.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoParceiroData.recordset['Fantasia'].value;
            oOption.value = dsoParceiroData.recordset['PessoaID'].value;
            selParceiroID.add(oOption);
            dsoParceiroData.recordset.MoveNext();
            bDisableParceiroID = false;
        }
        dsoSup01.recordset['ParceiroID'].value = 0;
        selParceiroID.selectedIndex = -1;
        selParceiroID.dataSrc = oldDataSrc;
        selParceiroID.dataFld = oldDataFld;
    }

    if (selParceiroID.options.length == 1) {
        selParceiroID.selectedIndex = 0;
        dsoSup01.recordset['ParceiroID'].value = selParceiroID.options(0).value;
        setLabelParceiro(dsoSup01.recordset['ParceiroID'].value);
    }

    restoreInterfaceFromModal();

    selParceiroID.disabled = bDisableParceiroID;

    return null;
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs() {
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selPessoaID,selComandoID)
    var nEmpresaID = getCurrEmpresaData();
    var bExecParceiro = false;
    glb_CounterCmbsDynamics = 4;

    if ((dsoSup01.recordset['ParceiroID'].value != null) &&
         (dsoSup01.recordset['ParceiroID'].value != 0)) {
        bExecParceiro = true;
        glb_CounterCmbsDynamics++;
    }

    // parametrizacao do dso dsoCmbDynamic01 (designado para selPessoaID)
    setConnection(dsoCmbDynamic01);

    dsoCmbDynamic01.SQL = 'SELECT PessoaID as fldID,Fantasia as fldName ' +
                          'FROM Pessoas WITH(NOLOCK) ' +
                          'WHERE PessoaID = ' + dsoSup01.recordset['PessoaID'].value;
    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();

    // parametrizacao do dso dsoCmbDynamic02 (designado para selComandoID)
    setConnection(dsoCmbDynamic02);

    dsoCmbDynamic02.SQL = 'SELECT DISTINCT d.ComandoID AS fldID, d.Comando AS fldName, e.Ordem AS Ordem ' +
                          'FROM RelacoesPessoas_Contas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Bancos_Comandos d WITH(NOLOCK), TiposAuxiliares_Itens e WITH(NOLOCK) ' +
                          'WHERE a.RelPesContaID=' + dsoSup01.recordset['RelPesContaID'].value + ' AND a.RelacaoID = b.RelacaoID ' +
                          'AND b.ObjetoID = c.PessoaID AND c.BancoID = d.BancoID AND d.ComandoID = e.ItemID AND e.TipoID=810 AND e.EstadoID=2 AND e.Filtro LIKE ' + '\'' + '%(1091)%' + '\'' + ' ' +
                          'UNION ALL SELECT 0 AS fldID,' + '\'' + ' ' + '\'' + ' AS fldName,  0 AS Ordem ' +
                          'ORDER BY Ordem';

    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic02.Refresh();

    if (bExecParceiro) {
        // parametrizacao do dso dsoCmbDynamic03 (designado para selParceiroID)
        setConnection(dsoCmbDynamic03);

        dsoCmbDynamic03.SQL = 'SELECT PessoaID AS fldID, Fantasia AS fldName ' +
                              'FROM Pessoas WITH(NOLOCK) ' +
                              'WHERE PessoaID = ' + dsoSup01.recordset['ParceiroID'].value;

        dsoCmbDynamic03.ondatasetcomplete = dsoCmbDynamic_DSC;
        dsoCmbDynamic03.Refresh();
    }

    // parametrizacao do dso dsoCmbDynamic02 (designado para selComandoID)
    setConnection(dsoCmbDynamic04);

    dsoCmbDynamic04.SQL = 'SELECT a.ItemID as fldID, a.ItemAbreviado as fldName, a.ItemMasculino AS Hint ' +
		'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
		'WHERE (a.EstadoID = 2 AND a.TipoID=802) ' +
		'ORDER BY a.Ordem';

    dsoCmbDynamic04.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic04.Refresh();

    // parametrizacao do dso dsoCmbDynamic05 (designado para selModalidadeID)
    setConnection(dsoCmbDynamic05);

    dsoCmbDynamic05.SQL = 'SELECT 0 AS fldID,' + '\' \'' + ' AS fldName UNION ALL ' +
                            'SELECT BanModal.ModalidadeID as fldID, BanModal.Modalidade as fldName ' +
                            'FROM RelacoesPessoas_Contas RelPesConta WITH(NOLOCK) ' +
		                            'INNER JOIN RelacoesPessoas RelPessoas WITH(NOLOCK)  ON RelPesConta.RelacaoID = RelPessoas.RelacaoID ' +
		                            'INNER JOIN Pessoas WITH(NOLOCK)  ON RelPessoas.ObjetoID = Pessoas.PessoaID ' +
		                            'INNER JOIN Bancos_Modalidades BanModal WITH(NOLOCK)  ON Pessoas.bancoID = BanModal.BancoID ' +
                            'WHERE RelPesContaID = ' + dsoSup01.recordset['RelPesContaID'].value + ' ' +
		                            'AND RelPessoas.SujeitoID = ' + dsoSup01.recordset['EmpresaID'].value + ' ';

    dsoCmbDynamic05.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic05.Refresh();
}

/********************************************************************
Funcao disparada pelo programado.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    var optionStr, optionValue;
    var aCmbsDynamics = [selPessoaID, selComandoID, selParceiroID, selSituacaoCobrancaID, selModalidadeID];
    var aDSOsDynamics = [dsoCmbDynamic01, dsoCmbDynamic02, dsoCmbDynamic03, dsoCmbDynamic04, dsoCmbDynamic05];
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var i;
    var lFirstRecord;
    var nQtdCmbs = 4;
    var aEmpresa = getCurrEmpresaData();

    if ((dsoSup01.recordset['ParceiroID'].value != null) &&
         (dsoSup01.recordset['ParceiroID'].value != 0))
        nQtdCmbs++;

    // Inicia o carregamento de combos dinamicos (selPessoaID, selObjetoID )
    //
    clearComboEx(['selPessoaID', 'selComandoID', 'selParceiroID', 'selSituacaoCobrancaID', 'selModalidadeID']);

    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics == 0) {
        for (i = 0; i < nQtdCmbs; i++) {
            oldDataSrc = aCmbsDynamics[i].dataSrc;
            oldDataFld = aCmbsDynamics[i].dataFld;
            aCmbsDynamics[i].dataSrc = '';
            aCmbsDynamics[i].dataFld = '';
            lFirstRecord = true;
            while (!aDSOsDynamics[i].recordset.EOF) {
                optionStr = aDSOsDynamics[i].recordset['fldName'].value;
                optionValue = aDSOsDynamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;

                if (aCmbsDynamics[i].id.toUpperCase() == 'SELSITUACAOCOBRANCAID')
                    oOption.hint = aDSOsDynamics[i].recordset['Hint'].value;

                aCmbsDynamics[i].add(oOption);
                aDSOsDynamics[i].recordset.MoveNext();
                lFirstRecord = false;
            }
            aCmbsDynamics[i].dataSrc = oldDataSrc;
            aCmbsDynamics[i].dataFld = oldDataFld;
        }

        setLabelPessoa(dsoSup01.recordset['PessoaID'].value);
        setLabelParceiro(dsoSup01.recordset['ParceiroID'].value);
        adjustLabelsCombos(true);
        refillCmbHistoricoPadrao(dsoSup01.recordset['EhEstorno'].value, dsoSup01.recordset['EhImporte'].value);

        lblImpostoID.style.visibility = (dsoSup01.recordset['bShowImposto'].value == true ? 'inherit' : 'hidden');
        selImpostoID.style.visibility = lblImpostoID.style.visibility;

        lblGuiaRecolhimento.style.visibility = ((dsoSup01.recordset['bShowImposto'].value && aEmpresa[1] == 130) == true ? 'inherit' : 'hidden');
        selGuiaRecolhimento.style.visibility = lblGuiaRecolhimento.style.visibility;

        lblCodigoArrecadacao.style.visibility = ((dsoSup01.recordset['bShowImposto'].value && aEmpresa[1] == 130) ? 'inherit' : 'hidden');
        txtCodigoArrecadacao.style.visibility = lblCodigoArrecadacao.style.visibility;

        formatCalcFields(false);

        // volta para a automacao
        finalOfSupCascade(glb_BtnFromFramWork);
    }
    return null;
}

function formatCalcFields(bClear) {

    txtTotalPrincipal.value = (bClear ? '' : padNumReturningStr(roundNumber(dsoSup01.recordset['TotalPrincipal'].value, 2), 2));
    txtTotalEncargos.value = (bClear ? '' : padNumReturningStr(roundNumber(dsoSup01.recordset['TotalEncargos'].value, 2), 2));
    txtTotalDespesas.value = (bClear ? '' : padNumReturningStr(roundNumber(dsoSup01.recordset['TotalDespesas'].value, 2), 2));
    txtTotalAbatimentos.value = (bClear ? '' : padNumReturningStr(roundNumber(dsoSup01.recordset['TotalAbatimentos'].value, 2), 2));
    txtTotalAmortizacoes.value = (bClear ? '' : padNumReturningStr(roundNumber(dsoSup01.recordset['TotalAmortizacoes'].value, 2), 2));
    txtSaldoDevedor.value = (bClear ? '' : padNumReturningStr(roundNumber(dsoSup01.recordset['SaldoDevedor'].value, 2), 2));
    txtSaldoAtualizado.value = (bClear ? '' : padNumReturningStr(roundNumber(dsoSup01.recordset['SaldoAtualizado'].value, 2), 2));
    txtPercAmortizacao.value = (bClear ? '' : padNumReturningStr(roundNumber(dsoSup01.recordset['PercAmortizacao'].value, 2), 2));
}

/********************************************************************
Funcao do programador
    Seta os campos read-only
           
Parametros: 
Nenhum

Retorno:
Nenhum
********************************************************************/
function setReadOnlyFields() {
    var nEstadoID = dsoSup01.recordset['EstadoID'].value;
    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));

    if (!((nA1) && (nA2)))
        chkMarketing.disabled = true;

    // campos read-only
    selPessoaID.disabled = true;
    selParceiroID.disabled = true;
    selGuiaRecolhimento.readOnly = true;
    txtCodigoArrecadacao.readOnly = true;

    txtdtFluxoCaixa.readOnly = true;
    txtTaxaMoeda.readOnly = true;

    try
    {
        if ((selHistoricoPadraoID.value == 1185) || (selHistoricoPadraoID.value == 1128) || (selHistoricoPadraoID.value == 1186) ||
                (selHistoricoPadraoID.value == 3053) || (selHistoricoPadraoID.value == 3200) || (selHistoricoPadraoID.value == 3331) ||
                (selHistoricoPadraoID.value == 22020) || (selHistoricoPadraoID.value == 22010) || (selHistoricoPadraoID.value == 1211) ||
                (selHistoricoPadraoID.value == 1212) || (selHistoricoPadraoID.value == 3051) || (selHistoricoPadraoID.value == 3604) ||
                (selHistoricoPadraoID.value == 21020) || (selHistoricoPadraoID.value == 33010))
        {
            txtTaxaMoeda.readOnly = false;
        }
    }
    catch(e)
    {
        ;
    }

    txtPedido.readOnly = true;
    selModalidadeID.disabled = true;

    var nPedidoID = txtPedido.value;

    if ((nEstadoID == 1) || ((typeof (nEstadoID)).toUpperCase() == 'UNDEFINED') || (nEstadoID == null)) {
        if (nPedidoID != '') {
            txtDuplicata.readOnly = true;
            chkEhEstorno.disabled = true;
            chkEhImporte.disabled = true;
            selHistoricoPadraoID.disabled = true;
            selImpostoID.disabled = true;
            selGuiaRecolhimento.disabled = true;
        }
        else {
            txtDuplicata.readOnly = false;
            chkEhEstorno.disabled = false;
            chkEhImporte.disabled = false;
            selHistoricoPadraoID.disabled = false;
            selImpostoID.disabled = false;
            selGuiaRecolhimento.disabled = false;

            txtValor.readOnly = false;

        }
    }
    else {
        txtDuplicata.readOnly = true;

        chkEhEstorno.disabled = true;
        chkEhImporte.disabled = true;
        selHistoricoPadraoID.disabled = true;
        selImpostoID.disabled = true;
        selGuiaRecolhimento.disabled = true;

        txtValor.readOnly = true;

    }

    if (nPedidoID == '') {
        txtDuplicata.readOnly = false;
        txtCodigoArrecadacao.readOnly = false;
    }

    if (((selHistoricoPadraoID.value == 2132) && (selImpostoID.value == 9)) && ((nEstadoID == 1) || ((nEstadoID == 41)))) {
        selGuiaRecolhimento.disabled = false;
    }

    txtValorConvertido.readOnly = true;
    txtNossoNumero.readOnly = true;
    selRelPesContaID.disabled = true;
    txtCarteira.readOnly = true;
    txtTotalPrincipal.readOnly = true;
    txtTotalEncargos.readOnly = true;
    txtTotalDespesas.readOnly = true;
    txtTotalAbatimentos.readOnly = true;
    txtTotalAmortizacoes.readOnly = true;
    txtSaldoDevedor.readOnly = true;
    txtSaldoAtualizado.readOnly = true;
    txtPercAmortizacao.readOnly = true;

}

/********************************************************************
Funcao do programador
    Seta os campos calculados de data, durante a digitacao (Inclusao/Alteracao)
           
Parametros: 
Nenhum

Retorno:
Nenhum
********************************************************************/
function setCalcDateFields(callerID) {
    if (callerID != null)
        controlID = callerID.id;
    else
        controlID = this.id;

    // Se o campo alterado for txtPrazoPagamento ou txtdtEmissao
    if (((controlID.toUpperCase()).indexOf('TXTPRAZOPAGAMENTO') >= 0) ||
         ((controlID.toUpperCase()).indexOf('TXTDTEMISSAO') >= 0)) {
        calcVencimento(this);
        calcPrazoPagamento(this);
        calcFluxoCaixa(this);
    }
    // Se o campo alterado for txtdtVencimento
    if ((controlID.toUpperCase()).indexOf('TXTDTVENCIMENTO') >= 0) {
        calcPrazoPagamento(this);
        calcVencimento(this);
        calcFluxoCaixa(this);
    }
    // Se o campo alterado for txtDiasCompensacao
    if ((controlID.toUpperCase()).indexOf('TXTDIASCOMPENSACAO') >= 0) {
        calcFluxoCaixa(this);
        calcVencimento(this);
        calcPrazoPagamento(this);
    }
}

/********************************************************************
Funcao do programador
    Funcao de campo calculado - calcula a data de vencimento
    Vencimento = dtEmissao + PrazoPagamento
           
Parametros: 
oField      : Objeto que disparou o evendo onchange que chamou esta funcao

Retorno:
Nenhum
********************************************************************/
function calcVencimento(oField) {
    // Prazo pagamento
    var nPrazoPagamento;
    /*if (getCloneFromOriginal('txtPrazoPagamento') != null)
        nPrazoPagamento = parseInt(getCloneFromOriginal('txtPrazoPagamento'].value,10);
    else    */
    nPrazoPagamento = parseInt(txtPrazoPagamento.value);

    if (!isNaN(nPrazoPagamento)) {
        var ddtEmissao;
        /*if (getCloneFromOriginal('txtdtEmissao') != null)
            ddtEmissao = getCloneFromOriginal('txtdtEmissao'].value;
        else    */
        ddtEmissao = txtdtEmissao.value;

        if ((chkDataEx(ddtEmissao) == false) || (chkDataEx(ddtEmissao) == null))
            return null;

        /*if (getCloneFromOriginal('txtdtVencimento') != null)
            getCloneFromOriginal('txtdtVencimento'].value = window.top.DateToStr(window.top.daysSUM(ddtEmissao, nPrazoPagamento), DATE_FORMAT);
        else    */
        txtdtVencimento.value = window.top.DateToStr(window.top.daysSUM(ddtEmissao, nPrazoPagamento), DATE_FORMAT);
    }
}

/********************************************************************
Funcao do programador
    Funcao de campo calculado - calcula o prazo de pagamento
    PrazoPagamento = dtVencimento + dtEmissao
           
Parametros: 
oField      : Objeto que disparou o evendo onchange que chamou esta funcao

Retorno:
Nenhum
********************************************************************/
function calcPrazoPagamento(oField) {
    // Vencimento
    var ddtVencimento;
    /*if (getCloneFromOriginal('txtdtVencimento') != null)
        ddtVencimento = getCloneFromOriginal('txtdtVencimento'].value;
    else    */
    ddtVencimento = txtdtVencimento.value;

    var ddtEmissao;
    /*if (getCloneFromOriginal('txtdtEmissao') != null)
        ddtEmissao = getCloneFromOriginal('txtdtEmissao'].value;
    else    */
    ddtEmissao = txtdtEmissao.value;

    if ((chkDataEx(ddtVencimento) == true) && (chkDataEx(ddtEmissao) == true)) {

        /*if (getCloneFromOriginal('txtPrazoPagamento') != null)
            getCloneFromOriginal('txtPrazoPagamento'].value = window.top.daysBetween(ddtEmissao, ddtVencimento);
        else    */
        txtPrazoPagamento.value = window.top.daysBetween(ddtEmissao, ddtVencimento);
    }
}

/********************************************************************
Funcao do programador
    Funcao de campo calculado - calcula a data de fluxo de caixa
    FluxoCaixa = dtVencimento + DiasCompensasao
           
Parametros: 
oField      : Objeto que disparou o evendo onchange que chamou esta funcao

Retorno:
Nenhum
********************************************************************/
function calcFluxoCaixa(oField) {
    // Dias compensacao
    var nDiasCompensasao;
    /*if (getCloneFromOriginal('txtDiasCompensacao') != null)
        nDiasCompensasao = parseInt(getCloneFromOriginal('txtDiasCompensacao'].value,10);
    else    */
    nDiasCompensasao = parseInt(txtDiasCompensacao.value, 10);

    var ddtVencimento;
    /* (getCloneFromOriginal('txtdtVencimento') != null)
        ddtVencimento = getCloneFromOriginal('txtdtVencimento'].value;
    else    */
    ddtVencimento = txtdtVencimento.value;
    if ((!isNaN(nDiasCompensasao) == true) && (chkDataEx(ddtVencimento) == true)) {

        //if (getCloneFromOriginal('txtdtFluxoCaixa') != null)
        //    getCloneFromOriginal('txtdtFluxoCaixa'].value = window.top.DateToStr(window.top.daysSUM(ddtVencimento,nDiasCompensasao), DATE_FORMAT);
        //else     
        txtdtFluxoCaixa.value = window.top.DateToStr(window.top.daysSUM(ddtVencimento, nDiasCompensasao), DATE_FORMAT);

    }
}
/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    var nFinanceiroID = dsoSup01.recordset['FinanceiroID'].value;
    var nFormaPagamentoID = dsoSup01.recordset['FormaPagamentoID'].value;
    var nRelPesContaID = dsoSup01.recordset['RelPesContaID'].value;

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&nFinanceiroID=' + escape(nFinanceiroID);
    strPars += '&nFormaPagamentoID=' + escape(nFormaPagamentoID);
    strPars += '&nRelPesContaID=' + escape(nRelPesContaID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(473, 465));
}
/********************************************************************
Funcao criada pelo programador.
Controla a visibilidade dos campos Nosso Numero e Enviado P/ Cobran�a

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function adjustCobranca(bEditing) {
    var nSelFormaPgto;
    if (bEditing)
        nSelFormaPgto = selFormaPagamentoID.value;
    else
        nSelFormaPgto = dsoSup01.recordset['FormaPagamentoID'].value;

    if ((nSelFormaPgto == 1035) || (nSelFormaPgto == 1036)) {
        lblCodigoBarra.style.visibility = 'inherit';
        txtCodigoBarra.style.visibility = 'inherit';
        lblNossoNumero.style.visibility = 'inherit';
        txtNossoNumero.style.visibility = 'inherit';
        lblRelPesContaID.style.visibility = 'inherit';
        selRelPesContaID.style.visibility = 'inherit';
        lblCarteira.style.visibility = 'inherit';
        txtCarteira.style.visibility = 'inherit';
        lblModalidadeID.style.visibility = 'inherit';
        selModalidadeID.style.visibility = 'inherit';
        lblComandoID.style.visibility = 'inherit';
        selComandoID.style.visibility = 'inherit';
        lblInstrucao1.style.visibility = 'inherit';
        txtInstrucao1.style.visibility = 'inherit';
        lblInstrucao2.style.visibility = 'inherit';
        txtInstrucao2.style.visibility = 'inherit';
    }
    else {
        /*Marco pediu para mostrar esses Campos*/
        //lblCodigoBarra.style.visibility = 'hidden';
        //txtCodigoBarra.style.visibility = 'hidden';   
        lblNossoNumero.style.visibility = 'hidden';
        txtNossoNumero.style.visibility = 'hidden';
        lblRelPesContaID.style.visibility = 'hidden';
        selRelPesContaID.style.visibility = 'hidden';
        lblCarteira.style.visibility = 'hidden';
        txtCarteira.style.visibility = 'hidden';
        lblModalidadeID.style.visibility = 'hidden';
        selModalidadeID.style.visibility = 'hidden';
        lblComandoID.style.visibility = 'hidden';
        selComandoID.style.visibility = 'hidden';
        lblInstrucao1.style.visibility = 'hidden';
        txtInstrucao1.style.visibility = 'hidden';
        lblInstrucao2.style.visibility = 'hidden';
        txtInstrucao2.style.visibility = 'hidden';
    }
}
/********************************************************************
Funcao criada pelo programador.
Recolhe dados no servidor e preenche combo de Comandos.

Parametro:
nenhum
Retorno:
nenhum
********************************************************************/
function preencheComboComando() {
    var nParam1 = 0;

    nParam1 = selRelPesContaID.value;

    lockInterface(true);

    var strPars = new String();
    strPars = '?';
    strPars += 'nRelPesContaID=';
    strPars += escape(nParam1);
    dsoCmbComando.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/combocomando.aspx' + strPars;
    dsoCmbComando.ondatasetcomplete = preencheComboComando_DSC;
    dsoCmbComando.refresh();

}

/********************************************************************
Funcao criada pelo programador.
Recebe dados no servidor e preenche combo de Comandos.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function preencheComboComando_DSC() {
    var cmbRef;

    cmbRef = document.getElementById('selComandoID');

    clearComboEx(['selComandoID']);

    var oldSrc = cmbRef.dataSrc;
    var oldDtf = cmbRef.dataFld;
    cmbRef.dataSrc = '';
    cmbRef.dataFld = '';

    var optionStr, optionValue;

    while (!dsoCmbComando.recordset.EOF) {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoCmbComando.recordset.Fields['fldName'].value;
        oOption.value = dsoCmbComando.recordset.Fields['fldID'].value;
        cmbRef.add(oOption);
        dsoCmbComando.recordset.MoveNext();
    }

    cmbRef.dataSrc = oldSrc;
    cmbRef.dataFld = oldDtf;

    // destrava a interface
    lockInterface(false);

    cmbRef.disabled = true;

    if (cmbRef.options.length != 0) {
        // seleciona opcao no combo
        setValueInControlLinked(cmbRef, dsoSup01);

        // destrava e coloca foco no combo se tem options
        cmbRef.disabled = false;
        cmbRef.focus();
    }
}

/********************************************************************
Funcao criada pelo programador.
Da refresh no sup

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function refreshSup() {
    if (glb_FinanceiroSupTimer != null) {
        window.clearInterval(glb_FinanceiroSupTimer);
        glb_FinanceiroSupTimer = null;
    }

    __btn_REFR('sup');
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblPessoaID, de acordo com o ID da Pessoa selecionada

Parametro:
nPessoaID   ID da Pessoa

Retorno:
nenhum
********************************************************************/
function setLabelPessoa(nPessoaID) {
    if (nPessoaID == null)
        nPessoaID = '';

    var sLabelPessoa = translateTerm('Pessoa', null) + ' ' + nPessoaID;
    lblPessoaID.style.width = sLabelPessoa.length * FONT_WIDTH;
    lblPessoaID.innerText = sLabelPessoa;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblParceiroID, de acordo com o ID do Parceiro selecionado

Parametro:
nParceiroID   ID do Parceiro

Retorno:
nenhum
********************************************************************/
function setLabelParceiro(nParceiroID) {
    if (nParceiroID == null)
        nParceiroID = '';

    var sLabelParceiro = translateTerm('Parceiro', null) + ' ' + nParceiroID;
    lblParceiroID.style.width = sLabelParceiro.length * FONT_WIDTH;
    lblParceiroID.innerText = sLabelParceiro;
}

/********************************************************************
Funcao criada pelo programador.
Controla a habilitacao dos campos com direitos

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function direitoCampos() {
    var nE1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E1' + '\'' + ')'));

    var nE2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E2' + '\'' + ')'));

    var nEstadoID = dsoSup01.recordset['EstadoID'].value;

    var nUserID = getCurrUserID();

    var nEmpresaID = getCurrEmpresaData();

    var bReadOnly;

    var bReadOnly2;

    //DireitoEspecifico
    //Financeiro->Recebimento/Pagamento->SUP
    //28000 SFS-Grupo Financeiro-> 
    //E1&&E2 -> Permite edi��o dos campos: 'txtValor','txtCarteira'
    //E1&&E2 || Empresa!=10-> Permite edi��o do campo: 'txtNossoNumero'

    if ((nE1 == 1) && (nE2 == 1))
        bReadOnly = false;
    else
        bReadOnly = true;

    if ((nEmpresaID[0] != 10) || (!bReadOnly))
        bReadOnly2 = false;
    else
        bReadOnly2 = true;

    selMoedaID.disabled = bReadOnly;

    var nPedidoID = txtPedido.value;

    //if (getCloneFromOriginal('txtPedido') != null)
    //    nPedidoID = getCloneFromOriginal('txtPedido'].value;

    if ((nEstadoID == 1) || ((typeof (nEstadoID)).toUpperCase() == 'UNDEFINED')) {
        if (nPedidoID != '') {
            txtValor.readOnly = bReadOnly;
        }
        else {
            txtValor.readOnly = false;
        }
    }
    else {
        txtValor.readOnly = true;
    }
}

function adjustLabelsCombos(bPaging) {
    var sHistPadraoConta = dsoSup01.recordset['HistPadraoConta'].value;
    var sHisPadrao = dsoSup01.recordset['HistoricoPadraoID'].value + ' - ' + sHistPadraoConta;
    if (bPaging) {
        setLabelOfControl(lblFormaPagamentoID, dsoSup01.recordset['FormaPagamentoID'].value);

        if (sHistPadraoConta != '')
            setLabelOfControl_Prg(lblHistoricoPadraoID, sHisPadrao);
        else
            setLabelOfControl(lblHistoricoPadraoID, dsoSup01.recordset['HistoricoPadraoID'].value);
        setLabelOfControl(lblImpostoID, dsoSup01.recordset['ImpostoID'].value);
        setLabelOfControl(lblRelPesContaID, dsoSup01.recordset['RelPesContaID'].value);
        setLabelOfControl(lblModalidadeID, dsoSup01.recordset['ModalidadeID'].value);
        setLabelOfControl(lblSituacaoCobrancaID, dsoSup01.recordset['SituacaoCobrancaID'].value);
    }
    else {
        setLabelOfControl(lblFormaPagamentoID, selFormaPagamentoID.value);
        setLabelOfControl(lblHistoricoPadraoID, selHistoricoPadraoID.value);
        setLabelOfControl(lblImpostoID, selImpostoID.value);
        setLabelOfControl(lblGuiaRecolhimento, selGuiaRecolhimento.value);
        setLabelOfControl(lblRelPesContaID, selRelPesContaID.value);
        setLabelOfControl(lblModalidadeID, selModalidadeID.value);
        setLabelOfControl(lblSituacaoCobrancaID, selSituacaoCobrancaID.value);
    }

    if (selSituacaoCobrancaID.selectedIndex >= 0)
        lblSituacaoCobrancaID.title = selSituacaoCobrancaID.options(selSituacaoCobrancaID.selectedIndex).hint;
    else
        lblSituacaoCobrancaID.title = '';
}

function openModalPrintBloqueto_Timer() {
    if (glb_BloquetoSupTimer != null) {
        window.clearInterval(glb_BloquetoSupTimer);
        glb_BloquetoSupTimer = null;
    }

    // esta funcao fecha a janela modal e destrava a interface
    restoreInterfaceFromModal();
    // escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Detalhe');

    openModalPrintBloqueto();
}

function openModalPrintBloqueto() {
    var strPars = new String();

    var htmlPath;

    strPars = '?nRegistroID=' + escape(txtRegistroID.value);
    strPars += '&sTitulo=' + escape('Bloqueto de cobran�a');
    strPars += '&sCaller=' + escape('S');

    // carregar modal de documentos
    htmlPath = SYS_PAGESURLROOT + '/modalgen/modalbloquetocobranca.asp' + strPars;

    showModalWin(htmlPath, new Array(771, 462));
}

/********************************************************************
Criado pelo programador
Abre a JM de Gerar Financeiros

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalGerarFinanceiros() {
    var htmlPath;
    var aEmpresa = getCurrEmpresaData();
    var strPars = new String();

    strPars = '?sCaller=' + escape('S');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
    strPars += '&nEmpresaPaisID=' + escape(aEmpresa[1]);
    strPars += '&nFinanceiroID=' + escape(dsoSup01.recordset['FinanceiroID'].value);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalgerarfinanceiros.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}

/********************************************************************
Criado pelo programador
Abre a JM de Gerar Valores a Localizar

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalGerarValoresLocalizar() {
    var htmlPath;
    var strPars = new String();

    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'EmpresaID' + '\'' + '].value');

    var nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'FinanceiroID' + '\'' + '].value');

    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

    var nTipoFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'TipoFinanceiroID' + '\'' + '].value');

    strPars = '?sCaller=' + escape('S');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nFinanceiroID=' + escape(nFinanceiroID);
    strPars += '&nEstadoID=' + escape(nEstadoID);
    strPars += '&nTipoValorID=' + escape(nTipoFinanceiroID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalgeravalorlocalizar.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}

/********************************************************************
Criado pelo programador
Abre a JM que gera Demonstrativo de reten��es

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalDemonstrativoRetencoes() {
    var htmlPath;
    var aEmpresa = getCurrEmpresaData();
    var strPars = new String();

    strPars = '?sCaller=' + escape('S');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
    strPars += '&nEmpresaPaisID=' + escape(aEmpresa[1]);
    strPars += '&nFinanceiroID=' + escape(dsoSup01.recordset['FinanceiroID'].value);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modaldemonstrativoretencoes.asp' + strPars;
    showModalWin(htmlPath, new Array(996, 477));
}

function showvalorlocalizar(nValorID) {
    var empresa = getCurrEmpresaData();

    // Manda o id da Nota Fiscal a detalhar 
    sendJSCarrier(getHtmlId(), 'SHOWVALORID', new Array(empresa[0], nValorID));
}

/********************************************************************
Criado pelo programador
Abre a JM Servicos Bancarios

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalServicosBancarios() {
    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = 0;

    if (((typeof (aContextoID)).toUpperCase() == 'OBJECT') && (aContextoID.length >= 2))
        nContextoID = aContextoID[1];

    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('S');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nContextoID=' + escape(nContextoID);
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nFinanceiroID=' + escape(dsoSup01.recordset['FinanceiroID'].value);

    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalservicosbancarios.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 550));
}

/********************************************************************
Verificacoes do Financeiro
********************************************************************/
function verifyFinanceiroInServer(currEstadoID, newEstadoID) {
    var nFinanceiroID = dsoSup01.recordset['FinanceiroID'].value;
    var strPars = new String();

    strPars = '?nFinanceiroID=' + escape(nFinanceiroID);
    strPars += '&nEstadoDeID=' + escape(currEstadoID);
    strPars += '&nEstadoParaID=' + escape(newEstadoID);
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/verificacaofinanceiro.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyFinanceiroInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes do Financeiro
********************************************************************/
function verifyFinanceiroInServer_DSC() {
    var sMensagem = (dsoVerificacao.recordset.Fields['Mensagem'].value == null ? '' : dsoVerificacao.recordset.Fields['Mensagem'].value);
    var nResultado = (dsoVerificacao.recordset.Fields['Resultado'].value == null ? 0 : dsoVerificacao.recordset.Fields['Resultado'].value);

    if (nResultado == 1) {
        var _retConf = window.top.overflyGen.Confirm(sMensagem);

        if (_retConf == 0)
            return null;
        else if (_retConf == 1)
            stateMachSupExec('OK');
        else
            stateMachSupExec('CANC');
    }
    else if (nResultado == 2) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        stateMachSupExec('OK');
    }
    else {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        stateMachSupExec('CANC');
    }
}

function chkEhEstorno_onclick() {
    dsoSup01.recordset['HistoricoPadraoID'].value = null;
    selHistoricoPadraoID.selectedIndex = -1;
    adjustLabelsCombos(false);
    refillCmbHistoricoPadrao(chkEhEstorno.checked, chkEhImporte.checked);
}

function chkEhImporte_onclick() {
    dsoSup01.recordset['HistoricoPadraoID'].value = null;
    selHistoricoPadraoID.selectedIndex = -1;
    adjustLabelsCombos(false);
    refillCmbHistoricoPadrao(chkEhEstorno.checked, chkEhImporte.checked);
}

function refillCmbHistoricoPadrao(bEhEstorno, bEhImporte) {

    // pega o value do contexto atual
    var aItemSelected = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = aItemSelected == null ? 9112 : aItemSelected[1];
    var nHistoricoPadraoID = 0;
    var nEstadoID = 0;

    if (!(dsoSup01.recordset.BOF || dsoSup01.recordset.EOF)) {
        nHistoricoPadraoID = dsoSup01.recordset['HistoricoPadraoID'].value;
        nEstadoID = dsoSup01.recordset['EstadoID'].value;
    }

    clearComboEx(['selHistoricoPadraoID']);

    if (dsoEstaticCmbs.recordset.Fields.Count == 0)
        return null;

    if ((nEstadoID != 1) && (nEstadoID != null)){
        dsoEstaticCmbs.recordset.setFilter('Indice=2');
    }

    if ((nEstadoID == 1) || (nEstadoID == null)) {
        if (nHistoricoPadraoID == 4) {
            dsoEstaticCmbs.recordset.setFilter('Indice=2 AND Importe=1 AND TipoLancamentoID=1572');

        }

        if (nHistoricoPadraoID != 4) {
            if (bEhImporte) {
                chkEhImporte.checked = true;
                if ((nContextoID == 9111 && bEhEstorno == false) || (nContextoID == 9112 && bEhEstorno == true))
                    dsoEstaticCmbs.recordset.setFilter('Indice=2 AND Importe=1 AND TipoLancamentoID=1571');
                else if ((nContextoID == 9112 && bEhEstorno == false) || (nContextoID == 9111 && bEhEstorno == true))
                    dsoEstaticCmbs.recordset.setFilter('Indice=2 AND Importe=1 AND TipoLancamentoID=1572');
            }
            else {
                if ((nContextoID == 9111 && bEhEstorno == false) || (nContextoID == 9112 && bEhEstorno == true))
                    dsoEstaticCmbs.recordset.setFilter('Indice=2 AND Pagamento=1 AND TipoLancamentoID=1571');
                else if ((nContextoID == 9112 && bEhEstorno == false) || (nContextoID == 9111 && bEhEstorno == true))
                    dsoEstaticCmbs.recordset.setFilter('Indice=2 AND Recebimento=1 AND TipoLancamentoID=1572');
            }
        }
    }
    
    /*    if (dsoSup01.recordset[glb_sFldIDName].value != null) {
            if ((dsoSup01.recordset['HistoricoPadraoID'].value == 2) || (dsoSup01.recordset['HistoricoPadraoID'].value == 4)) {
                dsoEstaticCmbs.recordset.setFilter('');
    
                dsoEstaticCmbs.recordset.setFilter('Indice=2');
            }
        }*/


    if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {
        dsoEstaticCmbs.recordset.moveFirst();

        oldDataSrc = dsoEstaticCmbs.dataSrc;
        oldDataFld = dsoEstaticCmbs.dataFld;
        dsoEstaticCmbs.dataSrc = '';
        dsoEstaticCmbs.dataFld = '';

        while (!dsoEstaticCmbs.recordset.EOF) {
            optionStr = dsoEstaticCmbs.recordset['fldName'].value;
            optionValue = dsoEstaticCmbs.recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            oOption.ImpostoPagamento = dsoEstaticCmbs.recordset['ImpostoPagamento'].value;
            oOption.ImpostoRecebimento = dsoEstaticCmbs.recordset['ImpostoRecebimento'].value;
            oOption.ImpostoImporte = dsoEstaticCmbs.recordset['ImpostoImporte'].value;
            oOption.Marketing = dsoEstaticCmbs.recordset['Marketing'].value;
            selHistoricoPadraoID.add(oOption);
            dsoEstaticCmbs.recordset.MoveNext();

            if (dsoSup01.recordset[glb_sFldIDName] != null) {
                if (oOption.value == dsoSup01.recordset['HistoricoPadraoID'].value)
                    oOption.selected = true;
            }
        }

        dsoEstaticCmbs.dataSrc = oldDataSrc;
        dsoEstaticCmbs.dataFld = oldDataFld;
    }

    dsoEstaticCmbs.recordset.setFilter('');
}
function showselImpostoID() {
    // pega o value do contexto atual
    var aItemSelected = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = aItemSelected[1];
    var bshow = false;

    if (chkEhImporte.checked)
        bshow = selHistoricoPadraoID.options[selHistoricoPadraoID.selectedIndex].getAttribute('ImpostoImporte', 1);
    else {
        if ((nContextoID == 9111 && chkEhEstorno.checked == false) || (nContextoID == 9112 && chkEhEstorno.checked == true))
            bshow = selHistoricoPadraoID.options[selHistoricoPadraoID.selectedIndex].getAttribute('ImpostoPagamento', 1);
        else if ((nContextoID == 9112 && chkEhEstorno.checked == false) || (nContextoID == 9111 && chkEhEstorno.checked == true))
            bshow = selHistoricoPadraoID.options[selHistoricoPadraoID.selectedIndex].getAttribute('ImpostoRecebimento', 1);
    }

    if (bshow) {
        lblImpostoID.style.visibility = 'inherit';
        selImpostoID.style.visibility = 'inherit';
    }
    else {
        lblImpostoID.style.visibility = 'hidden';
        selImpostoID.style.visibility = 'hidden';
    }

    bshow = selHistoricoPadraoID.options[selHistoricoPadraoID.selectedIndex].getAttribute('Marketing', 1);
    chkMarketing.checked = bshow;
}

function setLabelOfControl_Prg(sLabel, sConcat) {
    if (sLabel.id == 'lblHistoricoPadraoID') {
        sLabel.innerText = 'HP (' + sConcat + ')';

        sLabel.style.width = sLabel.innerText.length * FONT_WIDTH;
    }
    else
        setLabelOfControl(sLabel, sConcat);
}