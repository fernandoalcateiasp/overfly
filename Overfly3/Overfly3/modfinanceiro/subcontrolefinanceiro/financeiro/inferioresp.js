/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID) {
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    //@@
    if (folderID == 20008) //Observacoes
    {
        dso.SQL = 'SELECT a.FinanceiroID, a.Observacoes FROM Financeiro a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.FinanceiroID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20009) //Prop/Altern
    {
        dso.SQL = 'SELECT a.FinanceiroID, a.ProprietarioID,a.AlternativoID FROM Financeiro a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.FinanceiroID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.RegistroID = ' + idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28001) // Ocorrencias
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Financeiro_Ocorrencias a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.FinanceiroID = ' + idToFind + ' ' +
                  'ORDER BY a.dtBalancete, a.TipoOcorrenciaID, a.dtOcorrencia';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28002) // Cobranca
    {
        dso.SQL =   'SELECT a.*, ' +
                        'b.dtgravacao       AS Data, ' +
                        'b.cobrancaid       AS CobrancaID,  ' +
                        'c.recursoabreviado AS Estado,  ' +
                        'g.codigo           AS Banco, ' +
                        'h.itemmasculino    AS TipoCobranca, ' +
                        'a.comando          AS CodigoComando, ' +
                        'i.itemmasculino    AS Comando ' +
                    'FROM   Cobranca_Detalhes a WITH(NOLOCK)  ' +
                        'INNER JOIN Cobranca b WITH(NOLOCK) ON (a.CobrancaID = b.CobrancaID) ' +
                        'INNER JOIN Recursos c WITH(NOLOCK) ON (b.EstadoID = c.RecursoID) ' +
                        'INNER JOIN RelacoesPessoas_Contas d WITH(NOLOCK) ON (b.RelPesContaID = d.RelPesContaID) ' +
                        'INNER JOIN RelacoesPessoas e WITH(NOLOCK) ON (d.RelacaoID = e.RelacaoID) ' +
                        'INNER JOIN Pessoas f WITH(NOLOCK) ON (e.ObjetoID = f.PessoaID) ' +
                        'INNER JOIN Bancos g WITH(NOLOCK) ON (f.BancoID = g.BancoID) ' +
                        'INNER JOIN TiposAuxiliares_Itens h WITH(NOLOCK) ON (b.TipoCobrancaID = h.ItemID) ' +
                        'INNER JOIN TiposAuxiliares_Itens i WITH(NOLOCK) ON (a.ComandoID = i.ItemID) ' +
                    'WHERE  a.FinanceiroID = ' + idToFind + '  ' +
                        'AND a.Processado = 1  ' +
                    'UNION ALL ' +
                    // Este select foi adicionado para atender os financeiros de adiantamento de clientes, gerados automaticamente, 
                    // que contenham a diferen�a do valor recebido na cobran�a. FRG - 16/05/2016. 
                    'SELECT a.*, ' +
                        'b.dtgravacao       AS Data, ' +
                        'b.cobrancaid       AS CobrancaID, ' +
                        'c.recursoabreviado AS Estado, ' +
                        'g.codigo           AS Banco, ' +
                        'h.itemmasculino    AS TipoCobranca, ' +
                        'a.comando          AS CodigoComando, ' +
                        'i.itemmasculino    AS Comando ' + 
                    'FROM   Financeiro_Ocorrencias aa WITH(NOLOCK) ' +
                        'INNER JOIN Cobranca_Detalhes a WITH(NOLOCK) ON(a.CobDetalheID = aa.CobDetalheID) AND (a.FinanceiroID <> aa.FinanceiroID) ' +
                        'INNER JOIN Cobranca b WITH(NOLOCK) ON (a.CobrancaID = b.CobrancaID) ' +
                        'INNER JOIN Recursos c WITH(NOLOCK) ON (b.EstadoID = c.RecursoID) ' +
                        'INNER JOIN RelacoesPessoas_Contas d WITH(NOLOCK) ON (b.RelPesContaID = d.RelPesContaID) ' +
                        'INNER JOIN RelacoesPessoas e WITH(NOLOCK) ON (d.RelacaoID = e.RelacaoID) ' +
                        'INNER JOIN Pessoas f WITH(NOLOCK) ON (e.ObjetoID = f.PessoaID) ' +
                        'INNER JOIN Bancos g WITH(NOLOCK) ON (f.BancoID = g.BancoID) ' +
                        'INNER JOIN TiposAuxiliares_Itens h WITH(NOLOCK) ON (b.TipoCobrancaID = h.ItemID) ' +
                        'INNER JOIN TiposAuxiliares_Itens i WITH(NOLOCK) ON (a.ComandoID = i.ItemID) ' + 
                    'WHERE  aa.FinanceiroID = ' + idToFind + ' ' +
                        'AND a.Processado = 1 ' +
                    'ORDER BY  b.cobrancaid ,b.dtgravacao';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 28003) // Lancamentos
    {
        dso.SQL = 'EXEC sp_EventosContabeis_Lancamentos ' + glb_nFormID + ', NULL, ' + idToFind;
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28004) // Registros Relacionados
    {
        dso.SQL = 'SELECT 1 AS Indice, d.dtVencimento AS Data, e.Fantasia AS Empresa, ' + '\'' + 'Fin' + '\'' + ' AS Doc, d.FinanceiroID AS RegistroID, ' +
                    //'d.Duplicata AS NotaFiscal, f.Fantasia AS Parceiro, NULL AS TipoCampanha, NULL AS Codigo, NULL AS ProdutoID, NULL AS Conceito, ' +
                    'd.Duplicata AS NotaFiscal, f.Fantasia AS Parceiro, dbo.fn_TipoAuxiliar_Item(d.TipoFinanceiroID, 0) AS TipoCampanha, NULL AS Codigo, NULL AS ProdutoID, NULL AS Conceito, ' +
					'NULL AS Modelo, g.SimboloMoeda AS SimboloMoeda, ' +
					'CONVERT(NUMERIC(11,2), d.Valor * (CASE WHEN a.TipoFinanceiroID = d.TipoFinanceiroID THEN -1 ELSE 1 END)) AS Valor, NULL AS RelacaoID, ' +
					'h.Fantasia AS Usuario ' +
				'FROM Financeiro a WITH(NOLOCK), Financeiro_Ocorrencias b WITH(NOLOCK), Financeiro_Ocorrencias c WITH(NOLOCK), Financeiro d WITH(NOLOCK), Pessoas e WITH(NOLOCK), ' +
				    'Pessoas f WITH(NOLOCK), Conceitos g WITH(NOLOCK), Pessoas h WITH(NOLOCK) ' +
				'WHERE (a.FinanceiroID = ' + idToFind + ' AND a.FinanceiroID = b.FinanceiroID AND ' +
					'b.TipoOcorrenciaID = 1051 AND b.FinOcorrenciaID = c.FinOcorrenciaReferenciaID AND ' +
					'c.TipoOcorrenciaID = 1051 AND c.FinanceiroID = d.FinanceiroID AND d.EstadoID NOT IN (5) AND d.EmpresaID = e.PessoaID AND ' +
					'd.ParceiroID = f.PessoaID AND d.MoedaID = g.ConceitoID) AND (h.PessoaID = b.ColaboradorID) ' +
				'UNION ALL SELECT 2 AS Indice, dbo.fn_Data_Zero(c.dtMovimento) AS Data, l.Fantasia AS Empresa, ' + '\'' + 'Ped' + '\'' + ' AS Doc, d.PedidoID AS RegistroID, ' +
                    'CONVERT(VARCHAR(8), e.NotaFiscal) AS NotaFiscal, f.Fantasia AS Parceiro, j.ItemMasculino AS TipoCampanha, ' +
                    'n.Codigo AS Codigo, ' +
                    'g.ConceitoID AS ProdutoID, ' +
                    'g.Conceito AS Conceito, g.Modelo AS Modelo, i.SimboloMoeda AS SimboloMoeda, a.Valor, ' +
                    '(SELECT aa.RelacaoID FROM RelacoesPesCon aa WHERE (aa.ObjetoID = g.ConceitoID AND aa.SujeitoID = l.PessoaID AND aa.TipoRelacaoID = 61)) AS RelacaoID, ' +
                    'o.Fantasia AS Usuario ' +
                    'FROM Financeiro_PedidosItensCampanhas a WITH(NOLOCK) ' +
                        'INNER JOIN Pedidos_Itens_Campanhas b WITH(NOLOCK) ON (a.PedIteCampanhaID = b.PedIteCampanhaID) ' +
                        'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (b.PedItemID = c.PedItemID) ' +
                        'INNER JOIN Pedidos d WITH(NOLOCK) ON (c.PedidoID = d.PedidoID) ' +
                        'LEFT OUTER JOIN NotasFiscais e WITH(NOLOCK) ON (d.NotaFiscalID = e.NotaFiscalID AND e.EstadoID=67) ' +
                        'INNER JOIN Pessoas f WITH(NOLOCK) ON (d.ParceiroID = f.PessoaID) ' +
                        'INNER JOIN Conceitos g WITH(NOLOCK) ON (c.ProdutoID = g.ConceitoID) ' +
                        'INNER JOIN Financeiro h WITH(NOLOCK) ON (a.FinanceiroID = h.FinanceiroID) ' +
                        'INNER JOIN Conceitos i WITH(NOLOCK) ON (h.MoedaID = i.ConceitoID) ' +
                        'INNER JOIN Pessoas l WITH(NOLOCK) ON (d.EmpresaID = l.PessoaID) ' +
                        'INNER JOIN Campanhas_Produtos m WITH(NOLOCK) ON (b.CamProdutoID = m.CamProdutoID) ' +
                        'INNER JOIN Campanhas n WITH(NOLOCK) ON (m.CampanhaID = n.CampanhaID) ' +
                        'INNER JOIN TiposAuxiliares_Itens j WITH(NOLOCK) ON (n.TipoCampanhaID = j.ItemID) ' +
                        'LEFT OUTER JOIN Pessoas o WITH(NOLOCK) ON (o.PessoaID = a.UsuarioID)' +
                    'WHERE (a.FinanceiroID = ' + idToFind + ') ' +
					'ORDER BY Indice, Data, Empresa, Doc, RegistroID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28005) // Valores a Localizar
    {
        dso.SQL = 'SELECT a.* ' +
				'FROM ValoresLocalizar_Financeiros a ' +
				'WHERE (a.FinanceiroID = ' + idToFind + ')';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28045) // Acoes Marketing
    {
        dso.SQL = 'SELECT a.AcaoID, b.Acao, c.Conceito, b.dtInicio, b.dtFim, dbo.fn_AcaoMarketing_Datas(a.AcaoID,1) AS dtVencimento, a.Valor, a.Observacao, dbo.fn_Pessoa_Fantasia(a.UsuarioID, 0) AS Usuario ' +
					'FROM dbo.Financeiro_AcoesMarketingPatrocinios a WITH(NOLOCK) ' +
							'INNER JOIN dbo.AcoesMarketing b WITH(NOLOCK) ON a.AcaoID = b.AcaoID ' +
							'LEFT JOIN dbo.Conceitos c WITH(NOLOCK) ON a.MarcaID = c.ConceitoID ' +
					'WHERE (a.FinanceiroID = ' + idToFind + ') ';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28007) // Comiss�es
    {
        dso.SQL = 'SELECT a.FinComissaoID, a.FinanceiroID, d.Fantasia AS Cliente, ' +
	                        'e.Fantasia AS Parceiro, a.Valor, h.Fantasia, c.PedidoID, f.NotaFiscal AS NF, f.dtNotaFiscal AS DataNF, ' +
	                        'g.Fantasia AS Usuario, a.dtAssociacao AS DataAssociacao ' +
                    'FROM Financeiro_Comissoes a WITH(NOLOCK) ' +
	                    'INNER JOIN Pedidos_Parcelas b WITH(NOLOCK) ON(a.PedParcelaID = b.PedParcelaID) ' +
	                    'LEFT OUTER JOIN Pedidos c WITH(NOLOCK) ON(c.PedidoID = b.PedidoID) ' +
	                    'LEFT OUTER JOIN Pessoas d WITH(NOLOCK) ON(d.PessoaID = c.PessoaID) ' +
                        'LEFT OUTER JOIN Pessoas e WITH(NOLOCK) ON(e.PessoaID = c.ParceiroID) ' +
	                    'LEFT OUTER JOIN NotasFiscais f WITH(NOLOCK) ON(c.PedidoID = f.PedidoID) AND (c.NotaFiscalID = f.NotaFiscalID) ' +
	                    'LEFT OUTER JOIN Pessoas g WITH(NOLOCK) ON(g.PessoaID = a.UsuarioID) ' +
	                    'LEFT OUTER JOIN Pessoas h WITH(NOLOCK) ON(c.EmpresaID = h.PessoaID) ' +
					'WHERE (a.FinanceiroID = ' + idToFind + ') ';
        return 'dso01Grid_DSC';
    }

    else if (folderID == 28046) // Despesas
    {
        dso.SQL = 'SELECT b.ItemMasculino AS Tipo, c.Conceito AS Familia, d.Conceito AS Marca, a.Valor ' +
			'FROM Pedidos_Itens_Despesas a WITH(NOLOCK) INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.TipoDespesaID=b.ItemID ' +
				'LEFT OUTER JOIN Conceitos c WITH(NOLOCK) ON a.FamiliaID=c.ConceitoID ' +
				'LEFT OUTER JOIN Conceitos d WITH(NOLOCK) ON a.MarcaID=d.ConceitoID ' +
			'WHERE a.FinanceiroID = ' + idToFind + ' ' +
			'ORDER BY Tipo, Familia, Marca, Valor DESC';

        return 'dso01Grid_DSC';
    }

    else if (folderID == 28006) // Retencoes
    {
        dso.SQL = 'SELECT a.* ' +
				    'FROM Financeiro_Retencoes_2 a ' +
				    'WHERE (a.FinanceiroID = ' + idToFind + ')';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28047) // Valores Depositados
    {
        dso.SQL = 'SELECT a.* ' +
                    'FROM Financeiro_ValoresDepositados a WITH(NOLOCK) ' +
                    'WHERE a.FinanceiroID = ' + idToFind + ' ' +
                    'ORDER BY (SELECT ItemID From TiposAuxiliares_Itens b WITH(NOLOCK), ValoresLocalizar c WITH(NOLOCK) ' +
		            'WHERE b.ItemID=c.FormaPagamentoID AND c.ValorID=a.ValorID), a.ValorID';

        return 'dso01Grid_DSC';
    }

}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID) {
    setConnection(dso);

    var aEmpresa = getCurrEmpresaData();

    // LOG
    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
        // Ocorrencias
    else if (pastaID == 28001) {
        if (dso.id == 'dsoCmb01Grid_01') // Tipo
        {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=805 ' +
                      'ORDER BY Ordem';
        }
        else if (dso.id == 'dsoCmb01Grid_04') // Tipo Ref
        {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=805 ' +
                      'ORDER BY Ordem';
        }
        else if (dso.id == 'dsoCmb01Grid_02') // Forma
        {
            dso.SQL = 'SELECT ItemID, ItemAbreviado ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE (EstadoID=2 AND TipoID=804) ' +
                      'ORDER BY Ordem';
        }
        else if (dso.id == 'dsoCmb01Grid_03') // Moeda
        {
            dso.SQL = 'SELECT c.ConceitoID , c.SimboloMoeda ' +
                      'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK) ' +
                      'WHERE (a.SujeitoID = ' + aEmpresa[0] + ' AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID ' +
                      'AND b.MoedaID = c.ConceitoID ' +
                      'AND d.RecursoID=999) ';
        }
        else if (dso.id == 'dso01GridLkp') // Totais
        {
            dso.SQL = 'SELECT DISTINCT a.FinOcorrenciaID, ' +
                      'dbo.fn_Financeiro_Posicao(a.FinanceiroID, -9, a.FinOcorrenciaID, GETDATE(), NULL, NULL) AS SaldoDevedor, ' +
                      'dbo.fn_Financeiro_Posicao(a.FinanceiroID, -6, a.FinOcorrenciaID, GETDATE(), NULL, NULL) AS Atualizacao, ' +
                      'dbo.fn_Financeiro_Posicao(a.FinanceiroID, -8, a.FinOcorrenciaID, GETDATE(), NULL, NULL) AS SaldoAtualizado, ' +
                      'dbo.fn_Financeiro_Posicao(a.FinanceiroID, -3, a.FinOcorrenciaID, GETDATE(), NULL, NULL) AS Dias ' +
                      'FROM Financeiro_Ocorrencias a WITH(NOLOCK) ' +
                      'WHERE a.FinanceiroID = ' + nRegistroID;
        }
        else if (dso.id == 'dso02GridLkp') // Valor a Localizar
        {
            dso.SQL = 'SELECT DISTINCT a.FinOcorrenciaID, c.ValorID ' +
                      'FROM Financeiro_Ocorrencias a WITH(NOLOCK), ValoresLocalizar_Financeiros b WITH(NOLOCK), ValoresLocalizar c WITH(NOLOCK) ' +
                      'WHERE a.FinanceiroID = ' + nRegistroID + ' AND a.ValFinanceiroID = b.ValFinanceiroID AND ' +
						'b.ValorID=c.ValorID';
        }
        else if (dso.id == 'dso03GridLkp') // Colaborador
        {
            dso.SQL = 'SELECT Pessoas.PessoaID, Pessoas.Fantasia ' +
                      'FROM Pessoas WITH(NOLOCK), Financeiro_Ocorrencias WITH(NOLOCK) ' +
                      'WHERE Pessoas.PessoaID=Financeiro_Ocorrencias.ColaboradorID AND  ' +
                        'Financeiro_Ocorrencias.FinanceiroID=' + nRegistroID + ' ' +
                      'ORDER BY Pessoas.Fantasia';
        }
        else if (dso.id == 'dso04GridLkp') // FinOcorrenciaID / Reten��o
        {
            dso.SQL = 'SELECT a.FinOcorrenciaID, c.Imposto ' +
                      'FROM Financeiro_Ocorrencias a WITH(NOLOCK) ' +
                        'LEFT OUTER JOIN Financeiro_Retencoes_2 b WITH(NOLOCK) ON ((b.FinRetencaoID = a.FinRetencaoID) AND (a.TipoOcorrenciaID = 1058)) ' +
                        'LEFT OUTER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ImpostoID) ' +
                      'WHERE a.FinanceiroID = ' + nRegistroID;
        }
        else if (dso.id == 'dso05GridLkp') // Financeiro Referencia
        {
            dso.SQL = 'SELECT DISTINCT a.FinOcorrenciaID, b.FinanceiroID ' +
                      'FROM Financeiro_Ocorrencias a WITH(NOLOCK), Financeiro_Ocorrencias b WITH(NOLOCK) ' +
                      'WHERE a.FinanceiroID = ' + nRegistroID + ' AND a.FinOcorrenciaReferenciaID = b.FinOcorrenciaID ';
        }
    }
        // Registros Relacionados
    else if (pastaID == 28004) {
        if (dso.id == 'dso01GridLkp') // Totais
        {
            dso.SQL = 'SELECT dbo.fn_Financeiro_Totais(' + nRegistroID + ', GETDATE(), 54) AS Saldo ';
        }
    }
        // Valores a Localizar
    else if (pastaID == 28005) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.ValFinanceiroID, e.ItemAbreviado AS FormaPagamento, b.ValorID, c.RecursoAbreviado, b.MotivoDevolucao, b.Documento, ' +
					'b.Emitente, b.dtEmissao, b.dtApropriacao, b.BancoAgencia, b.NumeroDocumento, d.SimboloMoeda, b.Valor AS ValorVL, ' +
					'CONVERT(BIT, (CASE WHEN dbo.fn_ValorLocalizarFinanceiro_Posicao(a.ValFinanceiroID, 2) = 0 THEN 1 ELSE 0 END)) AS Aplicado ' +
					'FROM ValoresLocalizar_Financeiros a WITH(NOLOCK), ValoresLocalizar b WITH(NOLOCK), Recursos c WITH(NOLOCK), Conceitos d WITH(NOLOCK), TiposAuxiliares_Itens e WITH(NOLOCK) ' +
					'WHERE (a.FinanceiroID = ' + nRegistroID + ' AND a.ValorID = b.ValorID AND b.EstadoID = c.RecursoID AND ' +
						'b.MoedaID = d.ConceitoID AND b.FormaPagamentoID = e.ItemID)';
        }
    }

        // Acoes de Marketing
    else if (pastaID == 28045) {
        if (dso.id == 'dso01GridLkp') // Totais
        {
            dso.SQL = 'SELECT (dbo.fn_Financeiro_Totais(' + nRegistroID + ', GETDATE(), 22) + ' +
                              'dbo.fn_Financeiro_Totais(' + nRegistroID + ', GETDATE(), 62)) AS Saldo ';
        }
    }
    else if (pastaID == 28006) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.FinRetencaoID, a.Imposto AS Imposto ' +
					    'FROM Conceitos a WITH(NOLOCK) ' +
					        'INNER JOIN Financeiro_Retencoes_2 b WITH(NOLOCK) ON (b.ImpostoID = a.ConceitoID) ' +
					    'WHERE (b.FinanceiroID = ' + nRegistroID + ')';
        }
    }

        // Valores Depositados
    else if (pastaID == 28047) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.ValorID, c.RecursoAbreviado AS Est, d.ItemAbreviado AS Forma, b.MotivoDevolucao, ' +
						    'b.Documento, b.Emitente, b.dtEmissao, b.dtApropriacao, b.BancoAgencia, b.NumeroDocumento, ' +
						    'f.SimboloMoeda AS Moeda, b.Valor, b.Identificador ' +
					    'FROM Financeiro_ValoresDepositados a WITH(NOLOCK) ' +
					        'INNER JOIN ValoresLocalizar b WITH(NOLOCK) ON (a.ValorID = b.ValorID) ' +
					        'LEFT OUTER JOIN Pessoas e WITH(NOLOCK) ON (b.PessoaID=e.PessoaID) ' +
					        'INNER JOIN Recursos c WITH(NOLOCK) ON (b.EstadoID=c.RecursoID) ' +
					        'INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (b.FormaPagamentoID=d.ItemID) ' +
					        'INNER JOIN Conceitos f WITH(NOLOCK) ON (b.MoedaID=f.ConceitoID) ' +
					    'WHERE (a.FinanceiroID = ' + nRegistroID + ')';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID) {
    var i = 0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
    var bCreditoFornecedores = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["CreditoFornecedores"].value');

    cleanupSelInControlBar('inf', 1);

    vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(28001);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28001); // Ocorrencias

    vPastaName = window.top.getPastaName(28005);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28005); // Valores Localizar

    vPastaName = window.top.getPastaName(28006);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28006); // Retencoes

    vPastaName = window.top.getPastaName(28002);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28002); // Cobranca

    vPastaName = window.top.getPastaName(28004);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28004); // Registros Relacionados

    vPastaName = window.top.getPastaName(28045);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28045); // Acoes Marketing

    vPastaName = window.top.getPastaName(28007);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28007); // Comiss�es

    vPastaName = window.top.getPastaName(28046);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28046); // Despesas

    vPastaName = window.top.getPastaName(28003);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28003); // Lancamento

    vPastaName = window.top.getPastaName(28047);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28047); // Valores Depositados



    // Nao mexer - Inicio de automacao ============================== 	
    // Tenta selecionar novamente a pasta anterior selecionada no combo
    // Se nao mais existe a pasta anteriormente selecionada no combo
    // seleciona a pasta default

    stripFoldersByRight(currSelection);

    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consist�ncia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID) {
    //@@ return true se tudo ok, caso contrario return false;

    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 28001) //Ocorrencias
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [2, 3, 6, 7, 8], ['FinanceiroID', registroID], [19]);

        if (folderID == 28005) //Valores Localizar
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [12], ['FinanceiroID', registroID]);

        if (folderID == 28006) //Demonstrativo de Retenc�es
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [12], ['FinanceiroID', registroID]);

        if (folderID == 28047) //Valores Depositados
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [12], ['FinanceiroID', registroID]);

        if (currLine < 0)
            return false;
    }

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO) {
    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.FrozenCols = 0;

    if (folderID == 20010) // LOG
    {
        headerGrid(fg, ['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);

        fillGridMask(fg, currDSO, ['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['', '99/99/9999', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm:ss', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 28001) // Ocorrencias
    {
        headerGrid(fg, ['Data',
                       'Balancete',
                       'Tipo',
                       'Tipo Ref',
                       'Imposto',
                       'Est',
                       'Form',
                       '$',
                       'Valor',
                       'Convertido',
                       'Saldo',
                       'Taxa',
                       'Dias',
                       'Atualiz',
                       'Saldo Atual',
                       
                       'Taxa',
                       'ValorID',
                       'Fin Ref',
                       'Colaborador',
                       'ColaboradorID',
                       'Aprov Com',
                       'Pgto Com',
                       'Ocorr�ncia',
                       'Observa��es',
                       'FinOcorrenciaID'], [19, 24]);

        fillGridMask(fg, currDSO, ['dtOcorrencia*',
                                 'dtBalancete',
                                 'TipoOcorrenciaID',
                                 'TipoOcorrenciaReferenciaID',
                                 '^FinOcorrenciaID^dso04GridLkp^FinOcorrenciaID^Imposto*',
                                 'Estorno',
                                 'FormaPagamentoID',
                                 'MoedaID',
                                 'ValorOcorrencia',
                                 'ValorConvertido*',
                                 '^FinOcorrenciaID^dso01GridLkp^FinOcorrenciaID^SaldoDevedor*',
                                 'TaxaAtualizacao*',
                                 '^FinOcorrenciaID^dso01GridLkp^FinOcorrenciaID^Dias*',
                                 '^FinOcorrenciaID^dso01GridLkp^FinOcorrenciaID^Atualizacao*',
                                 '^FinOcorrenciaID^dso01GridLkp^FinOcorrenciaID^SaldoAtualizado*',
                                 
                                 'TaxaMoeda*',
                                 '^FinOcorrenciaID^dso02GridLkp^FinOcorrenciaID^ValorID*',
                                 '^FinOcorrenciaID^dso05GridLkp^FinOcorrenciaID^FinanceiroID*',
                                 '^ColaboradorID^dso03GridLkp^PessoaID^Fantasia*',
                                 'ColaboradorID',
								 'dtAprovacaoComissao*',
								 'dtPagamentoComissao*',
								 '^FinOcorrenciaID^dso04GridLkp^FinOcorrenciaID^FinOcorrenciaID*',
                                 'Observacoes*',
                                 'FinOcorrenciaID'],
                                 ['99/99/9999', '99/99/9999', '', '', '', '', '', '', '999999999.99', '', '', '', '', '', '', '', '', '', '',
                                 '99/99/9999', '99/99/9999', '', '', ''],
                                 [dTFormat, dTFormat, '', '', '', '', '', '', '###,###,###.00', '###,###,###.00', '###,###,###.00', '##,###,###.00',
                                 '###,###,###.00', '###,###,###.00', '', '##,###,###.0000000', '', '',
                                 '', '', dTFormat, dTFormat, '', '', '']);

        // colunas a serem alinhadas a direita
        alignColsInGrid(fg, [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 22]);

        fg.FrozenCols = 3;

        // Pinta celulas de vermelho caso negativo
        var nValorCelula;
        var i, j;

        for (i = 1; i < fg.Rows; i++) {
            for (j = 8; j <= 9; j++) {
                nValorCelula = fg.ValueMatrix(i, j);
                if ((!(isNaN(nValorCelula))) && (nValorCelula < 0)) {
                    fg.Select(i, j, i, j);
                    fg.FillStyle = 1;
                    fg.CellForeColor = 0X0000FF;
                }
            }
        }
    }
    else if (folderID == 28005) // Valores a Localizar
    {
        headerGrid(fg, ['Forma',
					   'ID',
					   'Est',
					   'Mot',
					   'Documento',
					   'Emitente',
					   'Emiss�o',
					   'Apropria��o',
					   'Banco',
					   'N�mero',
					   '$',
					   'Valor',
					   'Aplicado',
					   'OK',
					   'A',
					   'ValFinanceiroID'], [15]);

        fillGridMask(fg, currDSO, ['^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^FormaPagamento*',
								 'ValorID*',
								 '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^RecursoAbreviado*',
								 '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^MotivoDevolucao*',
								 '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^Documento*',
								 '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^Emitente*',
								 '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^dtEmissao*',
								 '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^dtApropriacao*',
								 '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^BancoAgencia*',
								 '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^NumeroDocumento*',
								 '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^SimboloMoeda*',
								 '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^ValorVL*',
								 'Valor',
								 'OK',
								 '^ValFinanceiroID^dso01GridLkp^ValFinanceiroID^Aplicado*',
								 'ValFinanceiroID'],
								 ['', '9999999999', '', '', '', '', dTFormat, dTFormat, '', '', '', '999999999.99', '999999999.99', '', '', ''],
                                 ['', '##########', '', '', '', '', dTFormat, dTFormat, '', '', '', '###,###,##0.00', '###,###,##0.00', '', '', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1, '######', 'C'], [11, '###,###,###.00', 'S'], [12, '###,###,###.00', 'S']]);

        glb_aCelHint = [[0, 14, 'Aplicado? (Baixado no Financeiro)']];

        alignColsInGrid(fg, [1, 11, 12]);
        fg.FrozenCols = 2;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 28002) // Cobranca
    {
        headerGrid(fg, ['Data',
                       'Cobran�a',
                       'Est',
                       'Banco',
                       'Tipo',
                       'Seql',
                       'C�d Com',
                       'Comando',
                       'Inst1',
                       'Inst2',
                       'Motivo',
                       'Ocor',
                       'OK',
                       'Valor Tarifa',
                       'Outras Desp',
                       'Juros Desc',
                       'IOF Desc',
                       'Valor Abat',
                       'Desc Conc',
                       'Valor Rec',
                       'Juros Mora',
                       'Outros Rec',
                       'Abat n Aprov',
                       'Valor Lan�'], []);

        fillGridMask(fg, currDSO, ['Data',
                                 'CobrancaID',
                                 'Estado',
                                 'Banco',
                                 'TipoCobranca',
                                 'SequencialRegistro',
                                 'CodigoComando',
                                 'Comando',
                                 'Instrucao1',
                                 'Instrucao2',
                                 'Motivo',
                                 'OcorrenciaID',
                                 'Processado',
                                 'ValorTarifa',
                                 'OutrasDespesas',
                                 'JurosDesconto',
                                 'IOFDesconto',
                                 'ValorAbatimento',
                                 'DescontoConcedido',
                                 'ValorRecebido',
                                 'JurosMora',
                                 'OutrosRecebimentos',
                                 'AbatimentoNaoAproveitado',
                                 'ValorLancamento'],
                                 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                                 [dTFormat, '', '', '', '', '', '', '', '', '', '', '', '',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00']);

        alignColsInGrid(fg, [1, 3, 5, 6, 8, 9, 10, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]);
        fg.FrozenCols = 2;
    }
    else if (folderID == 28004) // Registros Relacionados
    {
        headerGrid(fg, ['Data',
                       'Empresa',
                       'Doc',
                       'Registro',
                       'NF',
                       'Parceiro',
                       'Tipo',
                       'C�digo',
                       'ID',
                       'Produto',
                       'Modelo',
                       '$',
                       'Valor',
                       'Usuario',
                       'RelacaoID'], [14]);

        fillGridMask(fg, currDSO, ['Data',
                                 'Empresa',
                                 'Doc',
                                 'RegistroID',
                                 'NotaFiscal',
                                 'Parceiro',
								 'TipoCampanha',
                                 'Codigo',
                                 'ProdutoID',
                                 'Conceito',
                                 'Modelo',
                                 'SimboloMoeda',
                                 'Valor',
                                 'Usuario',
                                 'RelacaoID'],
                                 ['', '', '', '', '', '', '', '', '', '', '', '', '999999999.99', '', ''],
                                 [dTFormat, '', '', '', '', '', '', '', '', '', '', '', '###,###,###.00', '', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[3, '######', 'C'], [12, '###,###,###.00', 'S']]);

        // colunas a serem alinhadas a direita
        alignColsInGrid(fg, [3, 8, 12]);

        fg.FrozenCols = 4;

        if (fg.Rows > 1) {
            if (!(dso01GridLkp.recordset.BOF && dso01GridLkp.recordset.EOF)) {
                var saldo = padNumReturningStr(roundNumber(dso01GridLkp.recordset['Saldo'].value, 2), 2);
                fg.TextMatrix(1, getColIndexByColKey(fg, 'Modelo')) = 'Saldo ' + saldo;
            }
        }
    }
    else if (folderID == 28003) // Lancamentos contabeis
    {
        headerGrid(fg, ['Lan�amento',
                        'Est',
                        'Data',
                        'Balancete',
                        'Est',
                        'Tipo',
                        'Quant',
                        'Hist�rico Padr�o',
                        'Hist�rico Complementar',
                        'Detalhe',
                        'D',
                        'C',
                        'Moeda',
                        'Total Lan�amento',
                        'Taxa',
                        'Total Convertido',
                        'Observa��o',
                        'EventoID'], [17]);

        glb_aCelHint = [[0, 4, '� estorno?'],
						[0, 6, 'Quantidade de detalhes'],
						[0, 10, 'N�mero de D�bitos'],
						[0, 11, 'N�mero de Cr�ditos'],
						[0, 11, 'Taxa de  convers�o para moeda comum']];

        fillGridMask(fg, currDSO, ['LancamentoID',
                                 'Estado',
                                 'dtLancamento',
                                 'dtBalancete',
                                 'EhEstorno',
                                 'TipoLancamento',
                                 'Quantidade',
                                 'HistoricoPadrao',
                                 'HistoricoComplementar',
                                 'Detalhe',
                                 'NumeroDebitos',
                                 'NumeroCreditos',
                                 'Moeda',
                                 'ValorTotalLancamento',
                                 'TaxaMoeda',
                                 'ValorTotalConvertido',
                                 'Observacao',
                                 'EventoID'],
                                 ['9999999999', '', '99/99/9999', '99/99/9999', '', '', '999999', '', '', '', '99', '99', '',
									'999999999.99', '9999999999999.99', '999999999.99', '', ''],
								 ['##########', '', dTFormat, dTFormat, '', '', '######', '', '', '', '##', '##', '',
									'###,###,##0.00', '#,###,###,###,##0.0000000', '###,###,##0.00', '', '']);

        alignColsInGrid(fg, [0, 6, 10, 11, 13, 14, 15]);
        fg.FrozenCols = 2;
    }
    else if (folderID == 28045) // Acoes Marketing
    {
        headerGrid(fg, ['ID',
                        'A��o',
                        'Marca',
                        'In�cio',
                        'Fim',
                        'Vencimento',
                        'Valor',
                        'Observa��o',
                        'Usu�rio'], []);

        fillGridMask(fg, currDSO, ['AcaoID',
                                 'Acao',
                                 'Conceito',
                                 'dtInicio',
                                 'dtFim',
                                 'dtVencimento',
                                 'Valor',
                                 'Observacao',
                                 'Usuario'],
                                 ['', '', '', '99/99/9999', '99/99/9999', '99/99/9999', '99999999999.99', '', ''],
								 ['', '', '', dTFormat, dTFormat, dTFormat, '###,###,##0.00', '', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'Valor'), '###,###,###.00', 'S']]);

        // colunas a serem alinhadas a direita
        alignColsInGrid(fg, [0, 6]);

        if (fg.Rows > 1) {
            if (!(dso01GridLkp.recordset.BOF && dso01GridLkp.recordset.EOF)) {
                var saldo = padNumReturningStr(roundNumber(dso01GridLkp.recordset['Saldo'].value, 2), 2);
                fg.TextMatrix(1, getColIndexByColKey(fg, 'dtVencimento')) = 'Saldo ' + saldo;
            }
        }
    }
    else if (folderID == 28046) // Despesas
    {
        headerGrid(fg, ['Tipo',
                        'Fam�lia',
                        'Marca',
                        'Valor'], []);

        fillGridMask(fg, currDSO, ['Tipo',
								 'Familia',
								 'Marca',
								 'Valor'],
								 ['', '', '', '99999999999.99'],
								 ['', '', '', '###,###,##0.00']);

        alignColsInGrid(fg, [3]);
    }
    else if (folderID == 28007) // Comiss�es
    {
        headerGrid(fg, ['Cliente',
                        'FinanceiroID',
                        'Parceiro',
                        'Valor',
                        'Empresa',
                        'Pedido',
                        'Nota Fiscal',
                        'Data NF',
                        'Usu�rio',
                        'Associa��o',
                        'FinComissaoID'], [1, 10]);

        fillGridMask(fg, currDSO, ['Cliente',
								  'FinanceiroID',
								  'Parceiro',
								  'Valor',
								  'Fantasia',
								  'PedidoID',
								  'NF',
								  'DataNF',
								  'Usuario',
								  'DataAssociacao',
								  'FinComissaoID'],
								  ['', '', '', '999999999.99', '', '', '', '99/99/9999', '', '99/99/9999', ''],
								  ['', '', '', '###,###,###.00', '', '', '', '', '', dTFormat + ' hh:mm:ss', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[3, '###,###,###.00', 'S']]);

        alignColsInGrid(fg, [3, 5, 6]);
    }
    else if (folderID == 28006) // Retencoes
    {
        headerGrid(fg, ['Imposto',
					   'Base',
					   'Aliquota',
					   'Valor',
					   'Vencimento',
					   'ImpostoID',
					   'FinRetencaoID'], [5, 6]);

        fillGridMask(fg, currDSO, ['^FinRetencaoID^dso01GridLkp^FinRetencaoID^Imposto*',
								 'BaseCalculo*',
								 'Aliquota*',
								 'ValorRetencao*',
								 'dtVencimento*',
								 'ImpostoID',
								 'FinRetencaoID'],
								 ['', '999999999.99', '999999999.99', '999999999.99', '', '', ''],
                                 ['', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '', '', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[3, '###,###,###.00', 'S']]);

        alignColsInGrid(fg, [1, 2, 3]);
        fg.FrozenCols = 2;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 28047) // Valores Depositados
    {
        {
            headerGrid(fg, ['Forma',
					   'ID',
					   'Est',
					   'Mot',
					   'Documento',
					   'Emitente',
					   'Emiss�o',
					   'Apropria��o',
					   'Banco',
					   'N�mero',
					   '$',
					   'Valor',
					   'Identificador',
                       'FinValorDepositadoID'], [13]);

            fillGridMask(fg, currDSO, ['^ValorID^dso01GridLkp^ValorID^Forma*',
								'ValorID',
								'^ValorID^dso01GridLkp^ValorID^Est*',
								'^ValorID^dso01GridLkp^ValorID^MotivoDevolucao*',
								'^ValorID^dso01GridLkp^ValorID^Documento*',
								'^ValorID^dso01GridLkp^ValorID^Emitente*',
								'^ValorID^dso01GridLkp^ValorID^dtEmissao*',
								'^ValorID^dso01GridLkp^ValorID^dtApropriacao*',
								'^ValorID^dso01GridLkp^ValorID^BancoAgencia*',
								'^ValorID^dso01GridLkp^ValorID^NumeroDocumento*',
								'^ValorID^dso01GridLkp^ValorID^Moeda*',
								'^ValorID^dso01GridLkp^ValorID^Valor*',
								'^ValorID^dso01GridLkp^ValorID^Identificador*',
								'FinValorDepositadoID'],
								 ['', '9999999999', '', '', '', '', dTFormat, dTFormat, '', '', '', '999999999.99', '', ''],
                                 ['', '##########', '', '', '', '', dTFormat, dTFormat, '', '', '', '###,###,##0.00', '', '']);

            alignColsInGrid(fg, [1, 11]);
            fg.FrozenCols = 2;

            // Merge de Colunas
            fg.MergeCells = 4;
            fg.MergeCol(-1) = true;
        }
    }
}
