/********************************************************************
inferior.js

Library javascript para o recursosinf01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// guarda ultimo botao clicado
var glb_sLastBtnClicked = '';

// acumula saldo devedor
var glb_nSaldoDevedor = 0;

// guarda o valor da ocorrencia antes da alteracao
var glb_nValorOcorrenciaInicial = 0;

// controla o Refresh inferior quando vem do Valores a Localizar
var glb_bRefreshInf = false;

// Form ID
var glb_nFormID = window.top.formID;

// SubFormID
var glb_nSubFormID = window.top.subFormID;
var dso01JoinSup = new CDatatransport('dso01JoinSup');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoDynamicCmbs = new CDatatransport('dsoDynamicCmbs');
var dsoCurrData = new CDatatransport('dsoCurrData');
var dsoComissaoDesassociar = new CDatatransport('dsoComissaoDesassociar');
var dsoStateMachineLkp = new CDatatransport('dsoStateMachineLkp');
var dsoFiltroInf = new CDatatransport('dsoFiltroInf');
var dso01Grid = new CDatatransport('dso01Grid');
var dsoCmb01Grid_01 = new CDatatransport('dsoCmb01Grid_01');
var dsoCmb01Grid_02 = new CDatatransport('dsoCmb01Grid_02');
var dsoCmb01Grid_03 = new CDatatransport('dsoCmb01Grid_03');
var dsoCmb01Grid_04 = new CDatatransport('dsoCmb01Grid_04');
var dso01GridLkp = new CDatatransport('dso01GridLkp');
var dso02GridLkp = new CDatatransport('dso02GridLkp');
var dso03GridLkp = new CDatatransport('dso03GridLkp');
var dso04GridLkp = new CDatatransport('dso04GridLkp');
var dso05GridLkp = new CDatatransport('dso05GridLkp');
var dsoFinLancamento = new CDatatransport('dsoFinLancamento');
var dsoCmbsPropAlt = new CDatatransport('dsoCmbsPropAlt');
var dsoIncluiRetencoes = new CDatatransport('dsoIncluiRetencoes');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    adjustElementsInDivsNonSTD()
    putSpecialAttributesInControls()
    optChangedInCmb(cmb)
    prgServerInf(btnClicked)
    prgInterfaceInf(btnClicked, pastaID, dso)
    finalOfInfCascade(btnClicked)
    treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, currDSO)
    btnLupaClicked(btnClicked)
    addedLineInGrid(folderID, grid, nLineInGrid, dso)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    btnAltPressedInGrid( folderID )
    btnAltPressedInNotGrid( folderID )
    fillCmbFiltsInfAutomatic(folderID)
    selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
    frameIsAboutToApplyRights(folderID)


FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG
    fg_AfterRowColChange_Prg()
    fg_DblClick_Prg()
    fg_ChangeEdit_Prg()
    fg_ValidateEdit_Prg()
    fg_BeforeRowColChange_Prg()
    fg_EnterCell_Prg()
    fg_MouseUp_Prg()
    fg_MouseDown_Prg()
    fg_BeforeEdit_Prg()

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload() {
    // Ze em 17/03/08
    dealWithGrid_Load();

    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array dos ids dos dsos que tenham campos linkados
    // a controles HTMLs.
    // Estes controles HTMls estao em divs e sao linkados a campos do dso
    // que sejam: obrigatorios, numericos ou de datas, nos dsos.
    // Se nao aplicavel ao form este array deve ser null
    glb_aDsosToCloneFieldsEtc = ['dso01JoinSup'];

    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;

    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 
    glb_aGridStaticCombos = [[20010, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', ''],
                                       [0, 'dso04GridLkp', '', '']]],
                              [28001, [[2, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID'],
                                       [3, 'dsoCmb01Grid_04', 'ItemMasculino', 'ItemID'],
                                       [6, 'dsoCmb01Grid_02', 'ItemAbreviado', 'ItemID'],
                                       [7, 'dsoCmb01Grid_03', 'SimboloMoeda', 'ConceitoID'],
                                       [0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', ''],
                                       [0, 'dso04GridLkp', '', ''],
                                       [0, 'dso05GridLkp', '', '']]],
                              [28004, [[0, 'dso01GridLkp', '', '']]],
                              [28005, [[0, 'dso01GridLkp', '', '']]],
                              [28006, [[0, 'dso01GridLkp', '', '']]],
                              [28045, [[0, 'dso01GridLkp', '', '']]],
                              [28047, [[0, 'dso01GridLkp', '', '']]]];
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03'],
                               [20008, 20009, [20010, 28001, 28002, 28003, 28004, 28005, 28006, 28045, 28007, 28046, 28047]]); //Adicionada pasta retencoes.

    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();

}

function fg_KeyPressEdit() {
    return false;
}

function setupPage() {
    //@@ Ajustar os divs
    // Nada a codificar
}

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg() {
    if (keepCurrLineInGrid() == -1)
        return null;
}

function fg_CellChanged() {
    return null;
}

function fg_DblClick_Prg() {
    var empresa = getCurrEmpresaData();

    if ((keepCurrFolder() == 28002) && (!fg.Editable))
        sendJSCarrier(getHtmlId(), 'SHOWCOBRANCA', new Array(empresa[0], fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CobrancaID'))));
    else if ((keepCurrFolder() == 28003) && (!fg.Editable))
        sendJSCarrier(getHtmlId(), 'SHOWLANCAMENTO', new Array(empresa[0], fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'LancamentoID'))));
    else if ((keepCurrFolder() == 28004) && (!fg.Editable))
        detalharRegistro();
    else if ((keepCurrFolder() == 28005) && (!fg.Editable))
        sendJSCarrier(getHtmlId(), 'SHOWVALORID', new Array(empresa[0], parseFloat(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorID*')))));
    else if ((keepCurrFolder() == 28045) && (!fg.Editable))
        sendJSCarrier(getHtmlId(), 'SHOWACOESMARKETING', new Array(empresa[0], parseFloat(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'AcaoID')))));
}

function fg_ChangeEdit_Prg() {

}

function fg_ValidateEdit_Prg() {
}

function fg_BeforeRowColChange_Prg() {

}

function fg_EnterCell_Prg() {

}

function fg_MouseUp_Prg() {

}

function fg_MouseDown_Prg() {

}

function fg_BeforeEdit_Prg() {
}

// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD() {
    //@@
    // Este form nao tem este caso    
}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@
    // troca a pasta default para ocorrencias
    glb_pastaDefault = 28001;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario


    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div

    glb_sLastBtnClicked = btnClicked;
    // busca data corrente do servidor de paginas
    dsoCurrData.URL = SYS_PAGESURLROOT + '/serversidegenEx/currdate.aspx?currDateFormat=' + escape(DATE_FORMAT);
    dsoCurrData.ondatasetcomplete = dsoCurrDataComplete_DSC;
    dsoCurrData.Refresh();
}

function dsoCurrDataComplete_DSC() {
    // Mover esta funcao para os finais retornos de operacoes no
    // banco de dados
    finalOfInfCascade(glb_sLastBtnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    showBtnsEspecControlBar('inf', false, [1, 1, 1, 1]);
    tipsBtnsEspecControlBar('inf', ['', '', '', '']);

    if (pastaID == 28001) // ocorrencias
    {
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 1]);
        tipsBtnsEspecControlBar('inf', ['Observa��es', 'Detalhar Valor a Localizar', 'Detalhar Financeiro Ref', 'Gerar Ocorr�ncias Cruzadas(CAR e MAT)']);
    }
    else if (pastaID == 28002) // Cobranca
    {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Cobran�a', '', '', '']);
    }
    else if (pastaID == 28003) // Lancamentos
    {
        showBtnsEspecControlBar('inf', true, [1, 1, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Lan�amento', 'Detalhar Contas', '', '']);
    }
    else if (pastaID == 28004) // Registros Relacionados
    {
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 1, 1]);
        tipsBtnsEspecControlBar('inf', ['Incluir �tens', 'Estornar Financeiro', 'Ratear lan�amento entre empresas', 'Detalhar registro', 'Detalhar produto']);
    }
    else if (pastaID == 28005) // Valores a Localizar
    {
        showBtnsEspecControlBar('inf', true, [1, 1, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Incluir Valor a Localizar', 'Detalhar Valor a Localizar', '', '']);
    }
    else if (pastaID == 28006) // Retencoes
    {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Calcular Reten��o', '', '', '']);
    }
    else if (pastaID == 28045) // Acoes Marketing
    {
        showBtnsEspecControlBar('inf', true, [1, 1, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar A��es Marketing', 'Associar A��es de Marketing', '', '']);
    }
    else if (pastaID == 28007) // Comiss�es
    {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Desassociar comiss�es', '', '', '']);
    }
    else if (pastaID == 28047) // Valores Depositados
    {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Valor a Localizar', '', '', '']);
    }


    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked) {
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ((btnClicked.toUpperCase()).indexOf('SUP') >= 0)
        // usario clicou botao no control bar sup
        sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
    else
        // usario clicou botao no control bar inf
        sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================

}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID) {
    //@@
    var sBtn1_State = 'D';
    var sBtn2_State = 'D';
    var sBtn3_State = 'D';
    var sBtn4_State = 'D';

    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR')
          || (btnClicked == 'SUPOK') || (btnClicked == 'INFREFR')
          || (btnClicked == 'INFOK') || (btnClicked == 'INFCANC')) {
        if (btnClicked == 'SUPOK')
            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'formatCalcFields(false)');

        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'paintObservacoes()');

        setupEspecBtnsControlBar('sup', 'HHHHHDHDDDDH');

        if (glb_bRefreshInf == true) {
            glb_bRefreshInf = false;
            __btn_REFR('inf');
        }
    }
    else
        setupEspecBtnsControlBar('sup', 'DDDDDDDDDDDH');

    // Ocorrencias
    if ((folderID == 28001) && (fg.Rows > 1)) {
        if (fg.Editable)
            setupEspecBtnsControlBar('inf', 'HDDD');
        else {
            var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["EstadoID"].value');
            var nTipoFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["TipoFinanceiroID"].value');
            var nFormaPagamentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selFormaPagamentoID.value');
            var nSaldoAtualizado = parseFloat(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtSaldoAtualizado.value'));
            var nPermiteOcorrenciasCruzadas = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["PermiteOcorrenciasCruzadas"].value');
            var nOcorrenciasCruzadas = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["OcorrenciasCruzadas"].value');
            var bEhEstorno = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["EhEstorno"].value');

            sBtn2_State = 'D';
            sBtn3_State = 'H';
            sBtn4_State = 'D';

            if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '^FinOcorrenciaID^dso02GridLkp^FinOcorrenciaID^ValorID*')) != '')
                sBtn2_State = 'H';

            if ((nEstadoID != 1) &&
				 ((nFormaPagamentoID == 1037) || (nFormaPagamentoID == 1038)) &&
				 ((nPermiteOcorrenciasCruzadas > 0) || (bEhEstorno)) &&
				 ((nSaldoAtualizado > 0) || (nOcorrenciasCruzadas > 0)))
                sBtn4_State = 'H';

            setupEspecBtnsControlBar('inf', 'H' + sBtn2_State + sBtn3_State + sBtn4_State);
        }
    }
    else if ((folderID == 28002) && (fg.Rows > 1)) {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    else if ((folderID == 28003) && (fg.Rows > 1)) {
        setupEspecBtnsControlBar('inf', 'HHDD');
    }
    else if (folderID == 28004) {
        var bCreditoFornecedores = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["CreditoFornecedores"].value');

        sBtn1_State = (bCreditoFornecedores ? 'H' : 'D');
        sBtn2_State = 'H';
        sBtn3_State = ((bCreditoFornecedores && (fg.Rows > 1)) ? 'H' : 'D');

        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', sBtn1_State + sBtn2_State + sBtn3_State + 'HH');
        else
            setupEspecBtnsControlBar('inf', sBtn1_State + sBtn2_State + sBtn3_State + 'DD');
    }
    else if (folderID == 28005) {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HHDD');
        else
            setupEspecBtnsControlBar('inf', 'HDDD');
    }
    else if (folderID == 28006) {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    else if ((folderID == 28045)) {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HHDD');
        else
            setupEspecBtnsControlBar('inf', 'DHDD');
    }
    else if ((folderID == 28007)) {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }
    else if ((folderID == 28047)) {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }

    else
        setupEspecBtnsControlBar('inf', 'DDDDD');

}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso) {
    // habilita o primeiro botao especifico para grid de ocorrencias
    if (folderID == 28001)
        setupEspecBtnsControlBar('inf', 'HDDD');

    // Ocorrencias
    if (folderID == '28001') {
        // O Tipo default e amortizacao

        fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'TipoOcorrenciaID')) = 1055;

        // A moeda default e moeda da ocorrencia, no sup
        var nMoedaOcorrenciaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'MoedaOcorrenciaID' + '\'' + '].value');
        if (!(isNaN(parseInt(nMoedaOcorrenciaID, 10))))
            fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'MoedaID')) = parseInt(nMoedaOcorrenciaID, 10);

        // Grava o id do colaborador
        fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'ColaboradorID')) = getCurrUserID();
        fg.LeftCol = 0;
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    var empresa = getCurrEmpresaData();
    var nImpostoID;

    // ocorrencias - Observacoes
    if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 28001)) {
        // funcao do frame work
        // abre janela modal para Observacoes
        startObsWindow_inf(fg, dso01Grid, 'Observacoes', getColIndexByColKey(fg, 'Observacoes*'));
    }
    else if ((controlBar == 'INF') && (btnClicked == 2) && (keepCurrFolder() == 28001)) {
        // Manda o id do valorid a detalhar 
        sendJSCarrier(getHtmlId(), 'SHOWVALORID', new Array(empresa[0], parseFloat(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '^FinOcorrenciaID^dso02GridLkp^FinOcorrenciaID^ValorID*')))));
    }
        // Detalhar Financeiro
    else if ((controlBar == 'INF') && (btnClicked == 3) && (keepCurrFolder() == 28001)) {
        var strval = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '^FinOcorrenciaID^dso05GridLkp^FinOcorrenciaID^FinanceiroID*'));

        if (strval == null || strval == "") return;

        // Manda o id do valorid a detalhar 
        sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(empresa[0], parseFloat(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '^FinOcorrenciaID^dso05GridLkp^FinOcorrenciaID^FinanceiroID*')))));
    }
        // ocorrencias - Modal gerar ocorrencias
    else if ((controlBar == 'INF') && (btnClicked == 4) && (keepCurrFolder() == 28001)) {
        openModalGerarOcorrencias();
    }
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 28002)) {
        sendJSCarrier(getHtmlId(), 'SHOWCOBRANCA', new Array(empresa[0], fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CobrancaID'))));
    }
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 28003)) {
        sendJSCarrier(getHtmlId(), 'SHOWLANCAMENTO', new Array(empresa[0], fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'LancamentoID'))));
    }
    else if ((controlBar == 'INF') && (btnClicked == 2) && (keepCurrFolder() == 28003)) {
        openModalDetalharContas();
    }
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 28004)) {
        openModalCampanhas();
    }
    else if ((controlBar == 'INF') && (btnClicked == 2) && (keepCurrFolder() == 28004)) {
        openModalEstornarFinanceiro();
    }
    else if ((controlBar == 'INF') && (btnClicked == 3) && (keepCurrFolder() == 28004)) {
        openModalGerarLancamentos();
    }
    else if ((controlBar == 'INF') && (btnClicked == 4) && (keepCurrFolder() == 28004)) {
        detalharRegistro();
    }
    else if ((controlBar == 'INF') && (btnClicked == 5) && (keepCurrFolder() == 28004)) {
        sendJSCarrier(getHtmlId(), 'SHOWRELPESCON', new Array(empresa[0], fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RelacaoID'))));
    }
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 28005)) {
        openModalBaixarValoresLocalizar();
    }
    else if ((controlBar == 'INF') && (btnClicked == 2) && (keepCurrFolder() == 28005)) {
        sendJSCarrier(getHtmlId(), 'SHOWVALORID', new Array(empresa[0], parseFloat(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorID*')))));
    }
        // Grid Reten��es. BJBN 03/11/2014
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 28006)) {
        nImpostoID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ImpostoID'));

        calcularRetencao(nImpostoID);
    }
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 28045)) {
        sendJSCarrier(getHtmlId(), 'SHOWACOESMARKETING', new Array(empresa[0], parseFloat(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'AcaoID')))));
    }
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 28007)) {
        comissoesDesassociar();
    }
    else if ((controlBar == 'INF') && (btnClicked == 2) && (keepCurrFolder() == 28045)) {
        openModalAssociarAcoes();
    }
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 28047)) {
        // Manda o id do valorid a detalhar
        sendJSCarrier(getHtmlId(), 'SHOWVALORID', new Array(empresa[0], parseFloat(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorID')))));
    }

}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // ocorrencias - observacoes
    if (keepCurrFolder() == 28001 && (idElement.toUpperCase() == 'MODALOBSERVACAOHTML')) {
        if (param1 == 'OK') {
            // escreve observacoes no grid
            fg.Redraw = 0;
            // Escreve no campo
            fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'Observacoes*')) = param2;
            fg.Redraw = 2;
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if ((keepCurrFolder() == 28001) && (idElement.toUpperCase() == 'MODALGERAROCORRENCIASHTML')) {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCAMPANHASHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALESTORNARFINANCEIROHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALGERARLANCAMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALBAIXARVALORESLOCALIZARHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
        // Modal Eventos Contabeis
    else if (idElement.toUpperCase() == 'MODALEVENTOSCONTABEIS_CONTASHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALASSOCIARACOESHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid(folderID) {
    //@@
    // Ocorrencias
    if ((folderID == 28001) && (fg.Rows > 1)) {
        setupEspecBtnsControlBar('inf', 'HDDD');
        fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'ColaboradorID')) = getCurrUserID();
        fg.LeftCol = 0;
    }
    else
        setupEspecBtnsControlBar('inf', 'DDDD');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid(folderID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID) {

    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder) {

    //@@ retornar null para prosseguir a automacao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra infeior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID) {
    // Pasta de LOG e Cobranca Read Only
    if ((folderID == 20010) || (folderID == 28002) || (folderID == 28003) || (folderID == 28004) || (folderID == 28045) || (folderID == 28046) || (folderID == 28047)) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else if (folderID == 28005) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    else if (folderID == 28047) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }

    else {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }

    return null;
}
/********************************************************************
Funcao do programador.
Abre modal associar acoes com financeiros
Parametros: 
Nenhum

Retorno:
Nenhum
********************************************************************/
function openModalAssociarAcoes() {
    var htmlPath;
    var strPars = new String();
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];
    var nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["FinanceiroID"].value');
    var nTipoFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["TipoFinanceiroID"].value');
    var nPageSize = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');

    // mandar os parametros para o servidor
    strPars = '?sCaller=' + escape('I');
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nFinanceiroID=' + escape(nFinanceiroID);
    strPars += '&nTipoFinanceiroID=' + escape(nTipoFinanceiroID);
    strPars += '&nPageSize=' + escape(nPageSize);

    htmlPath = SYS_PAGESURLROOT + '/modmarketing/subcampanhas/acoesmarketing/modalpages/modalassociaracoes.asp' + strPars;
    showModalWin(htmlPath, new Array(771, 462));

    // Prossegue a automacao
    return null;
}
function converteFloatMoeda(valor) {
    var inteiro = null, decimal = null, c = null, j = null;
    var aux = new Array();
    valor = "" + valor;
    c = valor.indexOf(".", 0);
    //encontrou o ponto na string
    if (c > 0) {
        //separa as partes em inteiro e decimal
        inteiro = valor.substring(0, c);
        decimal = valor.substring(c + 1, valor.length);
    } else {
        inteiro = valor;
    }

    //pega a parte inteiro de 3 em 3 partes
    for (j = inteiro.length, c = 0; j > 0; j -= 3, c++) {
        aux[c] = inteiro.substring(j - 3, j);
    }

    //percorre a string acrescentando os pontos
    inteiro = "";
    for (c = aux.length - 1; c >= 0; c--) {
        inteiro += aux[c] + '.';
    }
    //retirando o ultimo ponto e finalizando a parte inteiro

    inteiro = inteiro.substring(0, inteiro.length - 1);

    decimal = parseInt(decimal);
    if (isNaN(decimal)) {
        decimal = "00";
    } else {
        decimal = "" + decimal;
        if (decimal.length === 1) {
            decimal = decimal + "0";
        }
    }


    valor = "R$ " + inteiro + "," + decimal;


    return valor;
}

/********************************************************************
Funcao do programador.
Realiza cancelamento das comiss�es internas.
Parametros: 
Nenhum

Retorno:
Nenhum
********************************************************************/
function comissoesDesassociar() {
    var _retMsg;
    var nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["FinanceiroID"].value');
    var valor = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Valor'));
    var finComissaoID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FinComissaoID'));
    var strPars = '';

    if (valor.indexOf(",") == -1) {
        valor = valor + ",00";
    }

    valor = (valor.replace(",", "."));

    _retMsg = window.top.overflyGen.Confirm('Deseja desassociar a Comiss�o Interna(CI) de R$ ' + valor + ' ?');

    if (_retMsg == 1) {
        setConnection(dsoComissaoDesassociar);

        strPars = '?nFinanceiroID=' + escape(nFinanceiroID);
        strPars += '&nValor=' + escape(valor);
        strPars += '&nFinComissaoID=' + escape(finComissaoID);
        strPars += '&nUsuarioID=' + escape(getCurrUserID());
        strPars += '&nFormID=' + escape(glb_nFormID);
        strPars += '&nSubFormID=' + escape(glb_nSubFormID);

        dsoComissaoDesassociar.URL = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/financeirocomissoes_desassociar.aspx' + strPars;
        dsoComissaoDesassociar.ondatasetcomplete = dsoComissaoDesassociarComplete_DSC;
        dsoComissaoDesassociar.Refresh();
    }
    else {
        return null;
    }
}

function dsoComissaoDesassociarComplete_DSC() {
    if (window.top.overflyGen.Alert('Comiss�o desassociada com sucesso!') == 0)
        return null;

    __btn_REFR('inf');
}
/********************************************************************
Funcao do programador.
Abre modal de gerar ocorrencias.
Parametros: 
Nenhum

Retorno:
Nenhum
********************************************************************/
function openModalGerarOcorrencias() {
    var htmlPath;
    var nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["FinanceiroID"].value');
    var nOcorrenciasCruzadas = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["OcorrenciasCruzadas"].value');
    var nSaldoAtualizado = parseFloat(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtSaldoAtualizado.value'));
    var empresa = getCurrEmpresaData();
    var nEhEstorno = 0;
    var nPaisID = empresa[1];
    var _retMsg;
    /*
    Normalmente, quando existem ocorr�ncias para serem estornadas, a modal abre perguntando se deseja abrir no modo estorno, nas empresas do Brasil essa op��o n�o deve mais aparecer. - BJBN
    */
    if ((nOcorrenciasCruzadas > 0) && (nPaisID != 130)) {

        _retMsg = window.top.overflyGen.Confirm('Deseja fazer estorno de ocorr�ncias?');

        if (_retMsg == 0)
            return null;
        else if (_retMsg == 1)
            nEhEstorno = 1;
        else if ((_retMsg == 2) && (nSaldoAtualizado <= 0))
            return null;
    }

    var strPars = new String();

    strPars = '?sCaller=' + escape('INF');
    strPars += '&nFinanceiroID=' + escape(nFinanceiroID);
    strPars += '&nEhEstorno=' + escape(nEhEstorno);
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(empresa[0]);
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalgerarocorrencias.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}

function openModalCampanhas() {
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];
    var nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["FinanceiroID"].value');
    var nUserID = getCurrUserID();

    strPars = '?sCaller=' + escape('INF');
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nFinanceiroID=' + escape(nFinanceiroID);
    strPars += '&nUserID=' + escape(nUserID);
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalcampanhas.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}
function openModalGerarLancamentos() {
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];
    var nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["FinanceiroID"].value');
    var nUserID = getCurrUserID();

    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
    var nDireito = 0;

    //DireitoEspecifico
    //Financeiro->Pagamento/Recebimento->Registros Relacionados-> Modal Gerar Lancamento (B3)
    //28004 SFI-Grid Registros relacionados-> A1&&A2 -> Habilita bot�o OK se tem celulas da coluna Ratear igual a 1.
    if ((nA1 == 1) && (nA2 == 1))
        nDireito = 1;
    else
        nDireito = 0;

    strPars = '?sCaller=' + escape('INF');
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nFinanceiroID=' + escape(nFinanceiroID);
    strPars += '&nUserID=' + escape(nUserID);
    strPars += '&nDireito=' + escape(nDireito);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalgerarlancamentos.asp' + strPars;
    showModalWin(htmlPath, new Array(450, 230));
}

/********************************************************************
Criado pelo programador
Abre a JM de Gerar Financeiros

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalEstornarFinanceiro() {
    var htmlPath;
    var aEmpresa = getCurrEmpresaData();
    var nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["FinanceiroID"].value');
    var nTipoFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["TipoFinanceiroID"].value');
    var bEhEstorno = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["EhEstorno"].value');
    var bEhImporte = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["EhImporte"].value');
    var nHistoricoPadraoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["HistoricoPadraoID"].value');
    var strPars = new String();

    var nEhEstorno = (bEhEstorno ? 0 : 1);
    var nEhImporte = (bEhImporte ? 1 : 0);

    strPars = '?sCaller=' + escape('INF');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
    strPars += '&nFinanceiroID=' + escape(nFinanceiroID);
    strPars += '&nTipoFinanceiroID=' + escape(nTipoFinanceiroID);
    strPars += '&nEhEstorno=' + escape(nEhEstorno);
    strPars += '&nEhImporte=' + escape(nEhImporte);
    strPars += '&nHistoricoPadraoID=' + escape(nHistoricoPadraoID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalestornarfinanceiro.asp' + strPars;
    showModalWin(htmlPath, new Array(725, 180));
}
function detalharRegistro() {
    var empresa = getCurrEmpresaData();
    var sDoc = getCellValueByColKey(fg, 'Doc', fg.Row);
    var sShow = '';

    if (sDoc == 'Ped')
        sShow = 'SHOWPEDIDO';
    else
        sShow = 'SHOWFINANCEIRO';

    sendJSCarrier(getHtmlId(), sShow, new Array(empresa[0], fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RegistroID'))));
}
/********************************************************************
Criado pelo programador

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalBaixarValoresLocalizar() {
    var htmlPath;
    var empresa = getCurrEmpresaData();
    var nFinanceiroID = 0;
    var nTipoValorID = 0;
    var strPars = new String();

    nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["FinanceiroID"].value');
    nTipoValorID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["TipoFinanceiroID"].value');

    strPars = '?sCaller=' + escape('INF');
    strPars += '&nFinanceiroID=' + escape(nFinanceiroID);
    strPars += '&nTipoValorID=' + escape(nTipoValorID);
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(empresa[0]);
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalbaixarvaloreslocalizar.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}

function openModalDetalharContas() {
    if (fg.Row <= 0)
        return null;

    var htmlPath;
    var strPars = new String();
    var nEventoID = getCellValueByColKey(fg, 'EventoID', fg.Row);

    // mandar os parametros para o servidor
    strPars = '?nEventoID=' + escape(nEventoID);

    // carregar modal - nao faz operacao de banco no carregamento           
    htmlPath = SYS_PAGESURLROOT + '/modalgen/modaleventoscontabeis_contas.asp' + strPars;
    showModalWin(htmlPath, new Array(765, 460));
}

function calcularRetencao(nImpostoID) {
    lockControls(true);

    var nFinanceiroID = 0;
    var strPars;

    nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["FinanceiroID"].value');

    strPars = '?nFinanceiroID=' + escape(nFinanceiroID);
    strPars += '&nValor=' + escape(0);
    strPars += '&nValorID=' + escape(0);
    strPars += '&nImpostoID=' + escape(nImpostoID);
    strPars += '&sDtFaturamento=' + escape(0);

    setConnection(dsoIncluiRetencoes);
    dsoIncluiRetencoes.URL = SYS_ASPURLROOT + '/serversidegen/calcularetencoes.asp' + strPars;
    dsoIncluiRetencoes.ondatasetcomplete = incluiRetencoes_DSC;
    dsoIncluiRetencoes.refresh();
}

function incluiRetencoes_DSC() {
    __btn_REFR('inf');

    lockControls(false);
}