<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalprintHtml" name="modalprintHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalprint.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
       
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_progressbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodalprint.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalprint_financeiro.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
Function TransformPath (origPath)

    Dim i, transfPath
    
    transfPath = ""
    
    For i=1 To Len(origPath)
        transfPath = transfPath & Mid(origPath, i, 1)
        If (Mid(origPath, i, 1) = "\") Then
            transfPath = transfPath & Mid(origPath, i, 1)
        End If
    Next
    
    If (Right(transfPath, 1) = "\") Then
        transfPath = transfPath & "\"
    Else    
        transfPath = transfPath & "\" & "\"
    End If    
    
    TransformPath = transfPath

End Function
%>

<%
'Script de variaveis globais

Dim i, sCaller, nEmpresaID, sEmpresaFantasia, nContextoID, nUserID
Dim relatorioID, relatorio, EhDefault, relatorioFantasia, dirA1, dirA2, dirA1A2True
Dim nFinanceiroID, nFormaPagamentoID, nRelPesContaID
Dim currDateFormat

sCaller = ""
nEmpresaID = 0
sEmpresaFantasia = ""
nContextoID = 0
nUserID = 0
nFinanceiroID = 0
nFormaPagamentoID = 0
nRelPesContaID = 0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "var glb_USERID = 0;"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

'nEmpresaID - ID da empresa logada
For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

'sEmpresaFantasia - fantasia da empresa logada
For i = 1 To Request.QueryString("sEmpresaFantasia").Count    
    sEmpresaFantasia = Request.QueryString("sEmpresaFantasia")(i)
Next

'nContextoID - ID do contexto
For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

'nUserID - ID do usuario logado
For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

'nFinanceiroID - ID Financeiro
For i = 1 To Request.QueryString("nFinanceiroID").Count    
    nFinanceiroID = Request.QueryString("nFinanceiroID")(i)
Next

'nFormaPagamentoID - ID da forma de pagamento
For i = 1 To Request.QueryString("nFormaPagamentoID").Count    
    nFormaPagamentoID = Request.QueryString("nFormaPagamentoID")(i)
Next

'nRelPesContaID - ID da forma de pagamento
For i = 1 To Request.QueryString("nRelPesContaID").Count    
    nRelPesContaID = Request.QueryString("nRelPesContaID")(i)
Next

'Formato corrente de data
For i = 1 To Request.QueryString("currDateFormat").Count
    currDateFormat = Request.QueryString("currDateFormat")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

If (currDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';"
ElseIf (currDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';"
ElseIf (currDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';"
Else
    Response.Write "var glb_dCurrDate = '';"
End If

Response.Write vbcrlf

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & nEmpresaID & ";"
Response.Write vbcrlf

Response.Write "var glb_sEmpresaFantasia = " & Chr(39) & sEmpresaFantasia & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nFinanceiroID = " & nFinanceiroID & ";"
Response.Write vbcrlf

Response.Write "var glb_nFormaPagamentoID = " & nFormaPagamentoID & ";"
Response.Write vbcrlf


'Monta array dos relatorios
Response.Write "// Array dos relatorios, com seus ids, nomes"
Response.Write vbcrlf

Dim rsData, strSQL
Dim rsData1

strSQL = "SELECT TOP 1 CarteiraCOD, CarteiraCOB " & _
	"FROM RelacoesPesRec_Cobranca a WITH(NOLOCK) INNER JOIN RelacoesPesRec b WITH(NOLOCK) ON a.RelacaoID=b.RelacaoID " & _
	"WHERE b.SujeitoID=" & CStr(nEmpresaID)& " AND b.ObjetoID=999 AND b.TipoRelacaoID=12 AND a.RelPesContaID=" & CStr(nRelPesContaID) & " "

Set rsData = Server.CreateObject("ADODB.Recordset")         
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write "var glb_nCarteiraCOD = '-1';"
Response.Write vbcrlf

Response.Write "var glb_nCarteiraCOB = '-1';"
Response.Write vbcrlf

If (Not (rsData.BOF OR rsData.EOF)) Then
    If (NOT IsNull(rsData.Fields("CarteiraCOD").Value)) Then
        Response.Write "var glb_nCarteiraCOD = '" & CStr(rsData.Fields("CarteiraCOD").Value) & "';"
        Response.Write vbcrlf
    End If

    If (NOT IsNull(rsData.Fields("CarteiraCOB").Value)) Then
        Response.Write "var glb_nCarteiraCOB = '" & CStr(rsData.Fields("CarteiraCOB").Value) & "';"
        Response.Write vbcrlf
    End If
End If

If ( (sCaller) = "PL" ) Then
    i = 0
Else
    i = 1
End If        

strSQL = "SELECT DISTINCT d.RecursoID AS RelatorioID, d.Recurso AS Relatorio, " & _
         "c.EhDefault, d.RecursoFantasia AS RelatorioFantasia, c.Ordem, " & _
         "g.Alterar1 AS A1, g.Alterar2 AS A2 " & _
         "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), " & _
         "RelacoesPesRec e WITH(NOLOCK), RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " & _
         "WHERE a.ObjetoID= " & CStr(nContextoID) & " AND a.TipoRelacaoID=1 AND a.EstadoID=2 " & _
         "AND a.SujeitoID=b.RecursoID AND b.EstadoID=2 AND b.TipoRecursoID=4 " & _ 
         "AND b.Principal=1 AND b.RecursoID=c.ObjetoID AND c.EstadoID=2 " & _
         "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID " & _
         "AND d.EstadoID=2 AND d.TipoRecursoID=5 AND d.ClassificacaoID=14 AND d.EhDetalhe= " & CStr(i) & " " & _
         "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) WHERE a.RecursoID= " & CStr(nContextoID) & " )) " & _
         "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " & _
         "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " & _
         "AND e.SujeitoID IN ((SELECT " & CStr(nUserID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(nUserID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
         "AND e.RelacaoID=f.RelacaoID " & _
         "AND f.EmpresaID= " & CStr(nEmpresaID) & " AND g.ContextoID= " & CStr(nContextoID) & " " & _
         "AND c.ObjetoID=g.RecursoMaeID AND d.RecursoID=g.RecursoID " & _
         "AND (g.Consultar1=1 OR g.Consultar2=1) " & _
         "AND f.PerfilID=g.PerfilID AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " & _
         "ORDER BY c.Ordem"

Set rsData = Server.CreateObject("ADODB.Recordset")         

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write vbcrlf
Response.Write "var glb_arrayReports = new Array();"
Response.Write vbcrlf
Response.Write vbcrlf
'------------------
i = 0
Do While (Not rsData.EOF)
        
    relatorioID = rsData.Fields("RelatorioID").Value
    relatorio = rsData.Fields("Relatorio").Value
    EhDefault = rsData.Fields("EhDefault").Value
    relatorioFantasia = rsData.Fields("RelatorioFantasia").Value
    dirA1 = rsData.Fields("A1").Value
    dirA2 = rsData.Fields("A2").Value
    
    If ( dirA1 AND dirA2 ) Then
        dirA1A2True = TRUE        
    Else
        dirA1A2True = FALSE    
    End If
        
    rsData.MoveNext
        
    Do While (Not rsData.EOF)
        If ( CLng(relatorioID) = CLng(rsData.Fields("RelatorioID").Value) ) Then
            'dirX1 e dirX2 e o anterior sempre
            If ( (rsData.Fields("A1").Value) AND (rsData.Fields("A2").Value) ) Then
                dirA1A2True = TRUE        
            End If

            dirA1 = dirA1 OR rsData.Fields("A1").Value
            dirA2 = dirA2 OR rsData.Fields("A2").Value
            rsData.MoveNext
        Else
            Exit Do
        End If    
    Loop    
        
    If ( (dirA1) AND (dirA2) ) Then
        If ( NOT (dirA1A2True)  ) Then
            dirA1 = FALSE
            dirA2 = TRUE
        End If
    End If    

    Response.Write "glb_arrayReports[" & CStr(i) & "] = new Array("
    Response.Write Chr(39)
    Response.Write CStr(relatorio)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write CStr(relatorioID)
    Response.Write ","
    Response.Write LCase(CStr(EhDefault))
    Response.Write ","
    Response.Write Chr(39)
    Response.Write CStr(relatorioFantasia)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write LCase(CStr(dirA1))
    Response.Write ","
    Response.Write LCase(CStr(dirA2))
    Response.Write ");"
    i = i + 1

Loop

rsData.Close
Set rsData = Nothing

Response.Write vbcrlf

Set rsData = Server.CreateObject("ADODB.Recordset")         

strSQL = "SELECT SerasaCaminho, SerasaArquivo " & _
         "FROM RelacoesPesRec WITH(NOLOCK) " & _
         "WHERE (SujeitoID = " & CStr(nEmpresaID) & " " &_
         "AND ObjetoID = 999 AND TipoRelacaoID = 12)"

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write vbcrlf
If ( NOT IsNull(rsData.Fields("SerasaCaminho").Value) ) Then
    Response.Write "var glb_SerasaCaminho = " & Chr(39) & TransformPath(rsData.Fields("SerasaCaminho").Value) & Chr(39) & ";"
Else
    Response.Write "var glb_SerasaCaminho = " & Chr(39) & Chr(39) & ";"
End If    
Response.Write vbcrlf

Response.Write vbcrlf
If ( NOT IsNull(rsData.Fields("SerasaArquivo").Value) ) Then
    Response.Write "var glb_SerasaArquivo = " & Chr(39) & rsData.Fields("SerasaArquivo").Value & Chr(39) & ";"
Else
    Response.Write "var glb_SerasaArquivo = " & Chr(39) & Chr(39) & ";"
End If    
Response.Write vbcrlf

rsData.Close

strSQL = "SELECT a.MoedaID " & _
		 "FROM Recursos a WITH(NOLOCK) " & _
		 "WHERE (a.RecursoID = 999)"

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write "var glb_nMoedaSistemaID = 0;"
Response.Write vbcrlf

If (Not (rsData.BOF AND rsData.EOF)) Then
	Response.Write "glb_nMoedaSistemaID = " & CStr(rsData.Fields("MoedaID").value) & ";"
	Response.Write vbcrlf
End If

rsData.Close
Set rsData = Nothing

Response.Write "</script>"

Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=oPrinter EVENT=OnPrintFinish>
<!--
 oPrinter_OnPrintFinish();
//-->
</SCRIPT>

</head>

<body id="modalprintBody" name="modalprintBody" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Controle de impressao matricial -->
    <object classid="clsid:1DF93CFF-D5C8-4A6D-983F-19DBAAC698D9" id = "oPrinter" name = "oPrinter" width=0 height=0 VIEWASTEXT></object>
    
    <p id="lblReports" name="lblReports" class="paraNormal"></p>
    <select id="selReports" name="selReports" class="fldGeneral"></select>

    <div id="divFluxoCaixa" name="divFluxoCaixa" class="divGeneral">
        <p id="lblEstado" name="lblEstado" class="lblGeneral">Estado</p>
        <select id="selEstado" name="selEstado" class="fldGeneral" multiple></select>
        <p id="lblSituacao" name="lblSituacao" class="lblGeneral">Situa��o</p>
        <select id="selSituacao" name="selSituacao" class="fldGeneral" multiple></select>
        <p id="lblForma" name="lblForma" class="lblGeneral">Forma</p>
        <select id="selForma" name="selForma" class="fldGeneral" multiple></select>
        <p id="lblVendedor" name="lblVendedor" class="lblGeneral">Vendedor</p>
        <select id="selVendedor" name="selVendedor" class="fldGeneral" multiple></select>
        <p id="lblContas" name="lblContas" class="lblGeneral">Contas</p>
        <select id="selContas" name="selContas" class="fldGeneral"></select>
        
        <p id="lblEmpresaFluxoCaixa" name="lblEmpresaFluxoCaixa" class="lblGeneral">Empresas</p>
        <select id="selEmpresaFluxoCaixa" name="selEmpresaFluxoCaixa" class="fldGeneral" style=" left: 448px; top: 23px; width: 106px; height: 150px;" multiple></select>
        
        <p id="lblOrdem" name="lblOrdem" class="lblGeneral">Ordem</p>
        <select id="selOrdem" name="selOrdem" class="fldGeneral">
            <option value="0">Vencimento</option>
            <option value="1">Alfab�tica</option>
        </select>
        <p id="lblMoeda" name="lblMoeda" class="lblGeneral">Moeda</p>
        <select id="selMoeda" name="selMoeda" class="fldGeneral"></select>
        <p id="lblEmpresa" name="lblEmpresa" class="lblGeneral">Emp</p>
        <input type="checkbox" id="chkEmpresa" name="chkEmpresa" class="fldGeneral" title="N�o considera financeiro de nossas empresas">
        <p id="lblInadimplencia" name="lblInadimplencia" class="lblGeneral">Inad</p>
        <input type="checkbox" id="chkInadimplencia" name="chkInadimplencia" class="fldGeneral" title="Considerar somente financeiro para inadimpl�ncia?">
        <p id="lblTaxaAtualizacao" name="lblTaxaAtualizacao" class="lblGeneral">Taxa</p>
        <input type="checkbox" id="chkTaxaAtualizacao" name="chkTaxaAtualizacao" class="fldGeneral" title="S� financeiro com taxa de atualiza��o">
        <p id="lblDadosSeguradora" name="lblDadosSeguradora" class="lblGeneral">Seg</p>
        <input type="checkbox" id="chkDadosSeguradora" name="chkDadosSeguradora" class="fldGeneral" title="Dados da seguradora">
        <p id="lblInicio" name="lblInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtInicio" name="txtInicio" class="fldGeneral" title="Data de in�cio dos vencimentos dos financeiros"></input>
        <p id="lblFim" name="lblFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtFim" name="txtFim" class="fldGeneral" title="Data de fim dos vencimentos dos financeiros"></input>
        <p id="lbldtPosicao" name="lbldtPosicao" class="lblGeneral">Posi��o</p>
        <input type="text" id="txtdtPosicao" name="txtdtPosicao" class="fldGeneral" title="Data da posi��o do fluxo de caixa"></input>

        <p id="lblFormatoSolicitacao" name="lblFormatoSolicitacao" class="lblGeneral">Formato</p>
        <select id="selFormatoSolicitacao" name="selFormatoSolicitacao" class="fldGeneral">
            <option value="1">PDF</option>
            <option value="2">Excel</option>		
        </select>

<%
Response.Write vbcrlf

Response.Write "<p id=" & Chr(34) & "lblFiltro" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblFiltro" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Filtro" & "</p>"

Response.Write vbcrlf

Response.Write "<input type=" & Chr(34) & "text" & Chr(34) & " " & _
               "id=" & Chr(34) & "txtFiltro" & Chr(34) & " " & _
               "name=" & Chr(34) & "txtFiltro" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"        

Response.Write vbcrlf
%>    
    
    </div>    

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>
    
    <div id="divBoletoCobranca" name="divBoletoCobranca" class="divGeneral">
        <p id="lblNossoNumero" name="lblNossoNumero" class="lblGeneral">Nosso N�mero</p>
        <input type="text" id="txtNossoNumero" name="txtNossoNumero" class="fldGeneral">
    </div>

    <div id="divVendasRecebidas" name="divVendasRecebidas" class="divGeneral">
        <p id="lblTipo" name="lblTipo" class="lblGeneral">Tipo</p>
        <select id="selTipo" name="selTipo" class="fldGeneral"></select>
        <p id="lblDataInicio3" name="lblDataInicio3" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio3" name="txtDataInicio3" class="fldGeneral">

        <p id="lblDataFim3" name="lblDataFim3" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim3" name="txtDataFim3" class="fldGeneral">

        <p id="lblFiltro2" name="lblFiltro2" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro2" name="txtFiltro2" class="fldGeneral">
    </div>

    <div id="divSerasa" name="divSerasa" class="divGeneral">
        <p id="lblDataInicio" name="lblDataInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio" name="txtDataInicio" class="fldGeneral">
        <p id="lblDataFim" name="lblDataFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim" name="txtDataFim" class="fldGeneral">
        <p id="lblRelatoCorrecao" name="lblRelatoCorrecao" class="lblGeneral">Cor</p>
        <input type="checkbox" id="chkRelatoCorrecao" name="chkRelatoCorrecao" class="fldGeneral" title="Relato de corre��o?">
    </div>

    <div id="divCampanhas" name="divCampanhas" class="divGeneral">
        <p id="lblCampanhas" name="lblCampanhas" class="lblGeneral">Tipo</p>
        <select id="selCampanhas" name="selCampanhas" class="fldGeneral">
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT 'Todos' AS fldName, 0 AS fldID, 0 AS Ordem " & _
	"UNION ALL " & _
	"SELECT a.ItemMasculino AS fldName, a.ItemID AS fldID, a.Ordem " & _
	"FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _
	"WHERE (a.TipoID = 812 AND a.Aplicar=1) " & _
	"ORDER BY Ordem"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>

		<p id="lblPendentes" name="lblPendentes" class="lblGeneral">Pend</p>
        <input type="checkbox" id="chkPendentes" name="chkPendentes" class="fldGeneral" title="DPA pendentes?">

        <p id="lblMoedaSistema" name="lblMoedaSistema" class="lblGeneral">MS</p>
        <input type="checkbox" id="chkMoedaSistema" name="chkMoedaSistema" class="fldGeneral" title="Na moeda do sistema?">
        
        <p id="lblResumido" name="lblResumido" class="lblGeneral">Res</p>
        <input type="checkbox" id="chkResumido" name="chkResumido" class="fldGeneral" title="S� resumo?">
               
        <p id="lblDataInicio2" name="lblDataInicio2" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio2" name="txtDataInicio2" class="fldGeneral">
        <p id="lblDataFim2" name="lblDataFim2" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim2" name="txtDataFim2" class="fldGeneral">
		<p id="lblEmpresaCampanha" name="lblEmpresaCampanha" class="lblGeneral">Empresa</p>
		<select id="selEmpresaCampanha" name="selEmpresaCampanha" class="fldGeneral"></select>
		<p id="lblFabricante" name="lblFabricante" class="lblGeneral">Fabricante</p>
		<select id="selFabricante" name="selFabricante" class="fldGeneral"></select>

        <p id="lblResponsavelID" name="lblResponsavelID" class="lblGeneral">Respons�vel</p>
        <select id="selResponsavelID" name="selResponsavelID" class="fldGeneral"></select>

        <p id="lblFiltro3" name="lblFiltro3" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro3" name="txtFiltro3" class="fldGeneral">

        <p id="lblFormatoSolicitacao2" name="lblFormatoSolicitacao2" class="lblGeneral">Formato</p>
        <select id="selFormatoSolicitacao2" name="selFormatoSolicitacao2" class="fldGeneral">
            <option value="1">PDF</option>		
            <option value="2">Excel</option>	
        </select>
    </div>
    
    <div id="divNotaDebito" name="divNotaDebito" class="divGeneral">
        <p id="lblBanco" name="lblBanco" class="lblGeneral">Bancos</p>
		<select id="selBancos" name="selBancos" class="fldGeneral"></select>
    </div>

    <div id="divCartaAnuencia" name="divCartaAnuencia" class="divGeneral">
        <p id="lblNomeResponsavel" name="lblNomeResponsavel" class="lblGeneral">Respons�vel</p>
        <select id="selNomeResponsavel" name="selNomeResponsavel" class="fldGeneral">
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT DISTINCT c.PessoaID AS fldID, c.Fantasia AS fldName, c.Nome, " & _
	    "ISNULL(LEFT(dbo.fn_Pessoa_Documento(c.PessoaID, NULL, NULL, 101, 101, 0), 9) + '-' +  " & _
	    "RIGHT(dbo.fn_Pessoa_Documento(c.PessoaID, NULL, NULL, 101, 101, 0), 2), SPACE(0)) AS CPF " & _
    "FROM RelacoesPesRec_Perfis a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK), Pessoas c WITH(NOLOCK)  " & _
    "WHERE (a.EmpresaID=" & CStr(nEmpresaID) & " AND a.RelacaoID = b.RelacaoID AND  " & _
    "b.EstadoID = 2 AND b.SujeitoID = c.PessoaID AND c.EstadoID = 2)  " & _
    "ORDER BY c.Fantasia"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34) & " "

    Response.Write "CPF=" & Chr(34) & rsData.Fields("CPF").Value & Chr(34) & " "
    Response.Write "Nome=" & Chr(34) & rsData.Fields("Nome").Value & Chr(34) & " "

    If (CLng(nEmpresaID) <> 10) Then
        'Eduardo Rogrigues
        If (CLng(rsData.Fields("fldID").Value) = 1004) Then
            Response.Write " SELECTED "
        End If
    Else
        'Renato Vilella
        If (CLng(rsData.Fields("fldID").Value) = 1255) Then
            Response.Write " SELECTED "
        End If
    End If
    
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
    </div>
    
    <div id="divDepositoBancario" name="divDepositoBancario" class="divGeneral">
        <!--POSSUI SELECT (selFormatoDepositoBancario) PARA FORMATO (excel ou pdf), QUE EST� SENDO INCLU�DO NA 'modalprint_financeiro.js'-->
    </div>
    
    <iframe id="frmReport" name="frmReport" style="display:none"  frameborder="no"></iframe>

</body>

</html>
