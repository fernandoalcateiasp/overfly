/********************************************************************
modalcampanhas.js

Library javascript para o modalcampanhas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_nCmbs = 0;
var glb_sResultado = '';
var glb_sEmpresas = '0';
var glb_bEnableDblClick = true;
var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');
var dsoCmbEmpresa = new CDatatransport('dsoCmbEmpresa');
var dsoCmbFabricante = new CDatatransport('dsoCmbFabricante');
var dsoSaldoFinanceiro = new CDatatransport('dsoSaldoFinanceiro');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalcampanhasBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalcampanhasDblClick(grid, Row, Col)
js_modalcampanhasKeyPress(KeyAscii)
js_modalcampanhas_ValidateEdit()
js_modalcampanhas_BeforeEdit(grid, row, col)
js_modalcampanhas_AfterEdit(Row, Col)
js_fg_AfterRowColmodalcampanhas (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalcampanhasBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    glb_bFirstFill = true;
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Controle de Campanhas', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // desabilita o botao OK
    btnOK.disabled = true;
    btnFillGrid.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls 
    adjustElementsInForm([['lblTipoCampanha', 'selTipoCampanha', 8, 1, -10, -10],
						  ['lblAD', 'chkAD', 3, 1],
						  ['lblFinanceiroID', 'txtFinanceiroID', 9, 1, -5],
						  ['lblParceiro', 'txtParceiro', 19, 1],
						  ['lblVencimento', 'txtVencimento', 10, 1],
						  ['lblMoeda', 'txtMoeda', 4, 1],
						  ['lblValor', 'txtValor', 11, 1],
						  ['lblSaldo', 'txtSaldo', 11, 1],
						  ['lblEmpresaID', 'selEmpresaID', 18, 2, -10],
						  ['lblFabricanteID', 'selFabricanteID', 18, 2],
						  ['lblCodigo', 'txtCodigo', 10, 2],
						  ['lblProduto', 'txtProduto', 10, 2],
						  ['lblDataInicio', 'txtDataInicio', 10, 2],
						  ['lblDataFim', 'txtDataFim', 10, 2]], null, null, true);

    selTipoCampanha.onchange = fillCmbs;
    chkAD.checked = true;
    chkAD.onclick = fillCmbs;

    txtFinanceiroID.value = glb_nFinanceiroID;
    txtParceiro.value = glb_sParceiro;
    txtVencimento.value = glb_sVencimento;
    txtMoeda.value = glb_sMoeda;
    txtValor.value = (padNumReturningStr(roundNumber(glb_nValorFinanceiro, 2), 2));
    txtSaldo.value = (padNumReturningStr(roundNumber(glb_nSaldoFinanceiro, 2), 2));

    selEmpresaID.onchange = selEmpresaID_onchange;
    selEmpresaID.disabled = true;
    selFabricanteID.onchange = selFabricanteID_onchange;
    selFabricanteID.disabled = true;

    txtFinanceiroID.readOnly = true;
    txtParceiro.readOnly = true;
    txtVencimento.readOnly = true;
    txtMoeda.readOnly = true;
    txtValor.readOnly = true;
    txtSaldo.readOnly = true;

    txtCodigo.onkeydown = txtFields_onKeyDown;
    txtProduto.onkeydown = txtFields_onKeyDown;
    txtDataInicio.onkeydown = txtFields_onKeyDown;
    txtDataFim.onkeydown = txtFields_onKeyDown;

    txtDataInicio.maxLength = 10;
    txtDataFim.maxLength = 10;

    adjustLabelsCombos();

    // Ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = txtSaldo.offsetLeft + txtSaldo.offsetWidth + ELEM_GAP;
        height = txtDataFim.offsetTop + txtDataFim.offsetHeight;
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // Reposiciona botao OK
    with (btnOK) {
        style.top = txtSaldo.offsetTop;
        style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 14;
        value = 'Associar';
        title = 'Associar Pedidos';
    }

    with (btnFillGrid) {
        style.width = btnOK.currentStyle.width;
        style.height = btnOK.currentStyle.height;
        style.top = txtDataFim.currentStyle.top;
        style.left = parseInt(btnOK.currentStyle.left, 10);
    }

    // Esconde botao cancel
    with (btnCanc) {
        style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;
}

function fillCmbs() {
    adjustLabelsCombos();
    fg.Rows = 1;
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    lockControlsInModalWin(true);

    var sFiltroMoeda = '';
    var nTipoCampanhaID = selTipoCampanha.value;
    var sFiltrocampanhas = '';

    //Diminuir lentid�o no carregamento dos combos. DCS 20/03/2008
    /*
	// Associar
	if (chkAD.checked)
		sFiltrocampanhas = ' AND dbo.fn_PedidoItem_CampanhaTotais(a.PedItemID, ' + nTipoCampanhaID + ', ' + glb_nMoedaSistema + ', 3) <> 0 ';
	// Dissociar
	else
		sFiltrocampanhas = ' AND dbo.fn_PedidoItem_CampanhaTotais(a.PedItemID, ' + nTipoCampanhaID + ', ' + glb_nMoedaSistema + ', 2) <> 0 ';
	*/

    //Altera��o Daniel -- Descomentar depois
    /*
	if (glb_nMoedaSistemaID != glb_nMoedaFinanceiroID)
		sFiltroMoeda = ' AND h.MoedaID = ' + glb_nMoedaFinanceiroID + ' ';
	*/
    //Altera��o Daniel

    glb_nCmbs = 2;

    setConnection(dsoCmbEmpresa);
    /*dsoCmbEmpresa.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
		'UNION ALL SELECT DISTINCT f.PessoaID AS fldID, f.Fantasia AS fldName ' +
		'FROM Pedidos_Itens_Campanhas a WITH(NOLOCK) ' +
		        'INNER JOIN Campanhas_Produtos b WITH(NOLOCK) ON a.CamProdutoID = b.CamProdutoID ' +
                'INNER JOIN Campanhas c WITH(NOLOCK) ON b.CampanhaID = c.CampanhaID  ' +
			    'INNER JOIN Pedidos_Itens d WITH(NOLOCK) ON a.PedItemID = d.PedItemID ' +
                'INNER JOIN Pedidos e WITH(NOLOCK) ON d.PedidoID = e.PedidoID ' +
                'INNER JOIN Pessoas f WITH(NOLOCK) ON e.EmpresaID = f.PessoaID ' +
                'INNER JOIN RelacoesPesRec g WITH(NOLOCK) ON f.PessoaID = g.SujeitoID ' +
                'INNER JOIN RelacoesPesRec_Moedas h WITH(NOLOCK) ON g.RelacaoID = h.RelacaoID ' +
		'WHERE (c.TipoCampanhaID = ' + nTipoCampanhaID + ' AND e.EstadoID >= 29 AND g.TipoRelacaoID = 12 AND g.ObjetoID = 999 AND ' + 
			'h.Faturamento = 1 ' + sFiltroMoeda + sFiltrocampanhas + ') ' +
		'ORDER BY fldName ';*/

    var nEmpresaData = getCurrEmpresaData();

    dsoCmbEmpresa.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
                        'UNION ALL ' +
                            'SELECT DISTINCT a.SujeitoID AS fldID, dbo.fn_Pessoa_Fantasia(a.SujeitoID, 0) AS fldName ' +
			                    'FROM RelacoesPesRec a WITH (NOLOCK) ' +
				                    'INNER JOIN Campanhas_Empresas b WITH(NOLOCK) ON ( a.SujeitoID = b.EmpresaID ) ' +
                                    'INNER JOIN Pessoas_Enderecos c WITH(NOLOCK) ON (c.PessoaID = a.SujeitoID) ' +
			                    'WHERE	a.TipoRelacaoID = 12 AND ' +
					            'a.EstadoID = 2 AND ' +
					            'a.ObjetoID = 999 AND ' +
                                'c.PaisID = ' + nEmpresaData[1] + ' ' +
					            'ORDER BY fldName ';

    dsoCmbEmpresa.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbEmpresa.Refresh();

    setConnection(dsoCmbFabricante);
    dsoCmbFabricante.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
		'UNION ALL SELECT DISTINCT g.PessoaID AS fldID, g.Fantasia AS fldName ' +
		'FROM Pedidos_Itens_Campanhas a WITH(NOLOCK), Campanhas_Produtos b WITH(NOLOCK), Campanhas c WITH(NOLOCK), ' +
			'Pedidos_Itens d WITH(NOLOCK), Pedidos e WITH(NOLOCK), Conceitos f WITH(NOLOCK), Pessoas g WITH(NOLOCK) ' +
		'WHERE (a.CamProdutoID = b.CamProdutoID AND b.CampanhaID = c.CampanhaID AND c.TipoCampanhaID = ' + nTipoCampanhaID + ' AND ' +
			'a.PedItemID = d.PedItemID AND d.PedidoID = e.PedidoID AND e.EstadoID >= 29 AND ' +
			'd.ProdutoID = f.ConceitoID AND f.FabricanteID = g.PessoaID ' + sFiltrocampanhas + ') ' +
		'ORDER BY fldName ';

    dsoCmbFabricante.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbFabricante.Refresh();

}

function dsoCmbDynamic_DSC() {
    glb_nCmbs--;

    if (glb_nCmbs > 0)
        return null;

    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selEmpresaID, selFabricanteID];
    var aDSOsDunamics = [dsoCmbEmpresa, dsoCmbFabricante];
    var nLastSelected = 0;

    glb_sEmpresas = '0';

    for (i = 0; i <= 1; i++) {
        nLastSelected = aCmbsDynamics[i].value;
        clearComboEx([aCmbsDynamics[i].id]);
        while (!aDSOsDunamics[i].recordset.EOF) {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);

            // Empresa
            if (i == 0)
                glb_sEmpresas += ', ' + aDSOsDunamics[i].recordset['fldID'].value;

            aDSOsDunamics[i].recordset.MoveNext();
        }

        aCmbsDynamics[i].selectedIndex = -1;

        if (nLastSelected > 0)
            selOptByValueInSelect(getHtmlId(), aCmbsDynamics[i].id, nLastSelected);
        else if (i == 1) {
            var nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selPessoaID.value');
            if (nPessoaID > 0)
                selOptByValueInSelect(getHtmlId(), aCmbsDynamics[i].id, nPessoaID);
        }

    }

    lockControlsInModalWin(false);

    selEmpresaID.disabled = (selEmpresaID.options.length == 0);
    selFabricanteID.disabled = (selFabricanteID.options.length == 0);

    if (glb_bFirstFill) {
        glb_bFirstFill = false;
        showExtFrame(window, true);

        window.focus();
        //chkcampanhas.focus();
        selTipoCampanha.focus();
    }

    adjustLabelsCombos();
    // ajusta estado dos botoes
    setupBtnsFromGridState(fg.Row);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        saveDataInGrid();
    }
    else if (controlID == 'btnFillGrid') {
        listar();
    }
        // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    lockControlsInModalWin(true);

    // @@ atualizar interface aqui
    setupBtnsFromGridState(fg.Row);

    // preenche o grid da janela modal
    fillCmbs();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState(nRow) {
    var nSaldo = 0;
    var nValorChecked = 0;
    var nValorAplicarChecked = 0;
    var nQuantidade = 0;
    var nCounter = 0;

    btnFillGrid.disabled = false;
    btnOK.disabled = true;

    nSaldo = parseFloat(glb_nSaldoFinanceiro);

    if (fg.Rows > 1) {
        if (fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'OK')) != 0) {
            if (fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'ValorAplicar')) == 0)
                fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ValorAplicar')) = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'Saldo*'));
        }
        else
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ValorAplicar')) = 0;
    }

    var aTotais = getTotalInGrid();

    nCounter = aTotais[0];
    nQuantidade = aTotais[1];
    nValorChecked = aTotais[2];
    nValorAplicarChecked = aTotais[3];

    txtSaldo.value = (padNumReturningStr(roundNumber(nSaldo - nValorAplicarChecked, 2), 2));

    if (fg.Rows > 1) {
        fg.TextMatrix(1, getColIndexByColKey(fg, 'PedidoID*')) = nCounter;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Quantidade*')) = nQuantidade;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorTotal*')) = nValorChecked;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorAplicar')) = nValorAplicarChecked;
    }

    if (chkAD.checked) {
        btnOK.value = 'Associar';
        btnOK.title = 'Associar Pedidos';

        if (((nSaldo - nValorAplicarChecked) >= 0) && (nValorAplicarChecked != 0))
            btnOK.disabled = false;
    }
    else {
        btnOK.value = 'Dissociar';
        btnOK.title = 'Dissociar Pedidos';

        if (nValorChecked != 0)
            btnOK.disabled = false;
    }
}

/********************************************************************
Totaliza valores checados no grid
array de retorno 0->Quantidade de registros
			     1->Quantidade
			     2->Valor Total
			     3->Valor Aplicar
********************************************************************/
function getTotalInGrid() {
    var i;
    var nCounter = 0;
    var nValorChecked = 0;
    var nValorAplicarChecked = 0;
    var nQuantidade = 0;
    var retVal = 0;

    for (i = 2; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0) {
            nCounter++;
            nValorChecked += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorTotal*'));
            nValorAplicarChecked += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorAplicar'));
            nQuantidade += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade*'));
        }
    }

    /*	if (nTipoTotal == 0)
            retVal = nCounter;
        else if (nTipoTotal == 1)
            retVal = nQuantidade;
        else if (nTipoTotal == 2)
            retVal = nValorChecked;
        else if (nTipoTotal == 3)
            retVal = nValorAplicarChecked;*/

    return (new Array(nCounter, nQuantidade, nValorChecked, nValorAplicarChecked));
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    var aGrid = null;
    var i = 0;
    var sDataInicio = txtDataInicio.value;
    var sDataFim = txtDataFim.value;
    var sFiltroEmpresa = '';
    var sFiltroFabricante = '';
    var sFiltroData = '';
    var sFiltrocampanhas = '';
    var sFiltroProduto = '';
    var sSelectDissociar = 'NULL AS FinPedIteCampanhaID';
    var sFromDissociar = '';
    var sFiltroDissociar = '';
    var sFiltroCodigo = '';

    lockControlsInModalWin(true);

    glb_nDSOs = 1;

    // zera o grid
    fg.Rows = 1;

    // Associar
    if (chkAD.checked)
        sFiltrocampanhas = ' dbo.fn_PedidoItem_CampanhaTotais(a.PedItemID, ' + selTipoCampanha.value + ', ' + glb_nMoedaSistema + ', 3) ';
        // Dissociar
    else {
        sFiltrocampanhas = ' dbo.fn_PedidoItem_CampanhaTotais(a.PedItemID, ' + selTipoCampanha.value + ', ' + glb_nMoedaSistema + ', 2) ';

        sSelectDissociar = 'aa.FinPedIteCampanhaID ';
        sFromDissociar = ' INNER JOIN Financeiro_PedidosItensCampanhas aa WITH(NOLOCK) ON (a.PedIteCampanhaID = aa.PedIteCampanhaID) ';
        sFiltroDissociar = ' AND aa.FinanceiroID = ' + glb_nFinanceiroID + ' ';
    }

    if ((selEmpresaID.selectedIndex > 0) && (selEmpresaID.value > 0))
        sFiltroEmpresa = ' AND e.EmpresaID = ' + selEmpresaID.value + ' ';

    if ((selFabricanteID.selectedIndex > 0) && (selFabricanteID.value > 0))
        sFiltroFabricante = ' AND g.FabricanteID = ' + selFabricanteID.value + ' ';

    if (trimStr(txtProduto.value) != '') {
        sFiltroProduto = ' AND (ISNULL(CONVERT(VARCHAR(10), g.ConceitoID), SPACE(0)) + CHAR(95) + ' +
							   'ISNULL(g.Conceito, SPACE(0)) + CHAR(95) + ' +
							   'ISNULL(g.Modelo, SPACE(0)) + CHAR(95) + ' +
							   'ISNULL(g.Descricao, SPACE(0)) + CHAR(95) + ' +
							   'ISNULL(g.PartNumber, SPACE(0))) LIKE ' + '\'' + '%' + trimStr(txtProduto.value) + '%' + '\'' + ' ';
    }

    if (trimStr(txtCodigo.value) != '') {
        sFiltroCodigo = ' AND c.Codigo LIKE ' + '\'' + '%' + trimStr(txtCodigo.value) + '%' + '\'' + ' ';
    }

    if (sDataInicio != '') {
        sDataInicio = normalizeDate_DateTime(sDataInicio, 1);

        sFiltroData += ' AND dbo.fn_Data_Zero(d.dtMovimento) >= ' + '\'' + sDataInicio + '\'' + ' ';
    }

    if (sDataFim != '') {
        sDataFim = normalizeDate_DateTime(sDataFim, 1);

        sFiltroData += ' AND dbo.fn_Data_Zero(d.dtMovimento) <= ' + '\'' + sDataFim + '\'' + ' ';
    }

    if (glb_nMoedaSistema == 1)
        glb_TaxaFinanceiro = '1';

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL = 'DECLARE @Campanhas TABLE (FinPedIteCampanhaID INT, Empresa VARCHAR(20), PedIteCampanhaID INT, Data DATETIME, PedidoID INT, NotaFiscal BIGINT, Parceiro VARCHAR(20), ' +
								                                        'Codigo VARCHAR(20), ConceitoID INT, Conceito VARCHAR(25), Modelo VARCHAR(18), Quantidade INT, ValorUnitarioMoedaSistema NUMERIC(11,2), ' +
                                                                        'SimboloMoeda VARCHAR(6), ValorUnitario NUMERIC(11,2), ValorTotal NUMERIC(11,2), Saldo NUMERIC(11,2), Ok BIT, ValorAplicar NUMERIC(11,2), ' +
                                                                        'ValorFiltroCampanhas NUMERIC(11,2)) ' +
        'INSERT INTO @Campanhas ' +
            'SELECT ' + sSelectDissociar + ', j.Fantasia AS Empresa, a.PedIteCampanhaID, dbo.fn_Data_Zero(d.dtMovimento) AS Data, d.PedidoID, i.NotaFiscal, f.Fantasia AS Parceiro, ' +
		    'c.Codigo AS Codigo, ' +
		    'g.ConceitoID, g.Conceito, g.Modelo, (d.Quantidade * POWER(-1, e.TipoPedidoID) * POWER(-1, ' + glb_nTipoFinanceiroID + ')) AS Quantidade, ' +
		    'CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(dbo.fn_PedidoItem_CampanhaTotais(a.PedItemID, ' + selTipoCampanha.value + ', 1, 1), d.Quantidade) * POWER(-1, ' + glb_nTipoFinanceiroID + ')) AS ValorUnitarioMoedaSistema, ' +
		    'h.SimboloMoeda, ' +

		    //Altera��o Daniel
		    'CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(dbo.fn_PedidoItem_CampanhaTotais(a.PedItemID, ' + selTipoCampanha.value + ', ' + glb_nMoedaSistema + ', 1) * ' +
			    'POWER(-1, 1002), d.Quantidade) * (CASE WHEN ((' + glb_nMoedaSistema + ' = 0) AND (' + glb_nMoedaID + ' <> e.MoedaID)) THEN ' + glb_TaxaFinanceiro +
			    ' ELSE 1 END)) AS ValorUnitario, ' +
			    // dbo.fn_Preco_Cotacao(CASE WHEN ' + glb_nMoedaSistema + ' = 0 THEN e.MoedaID ELSE 541 END, ' + glb_nMoedaID + ', NULL, ' + glb_dtEmissao + /*d.dtMovimento*/')) AS ValorUnitario, ' +
            'CONVERT(NUMERIC(11,2), (dbo.fn_PedidoItem_CampanhaTotais(a.PedItemID, ' + selTipoCampanha.value + ', ' + glb_nMoedaSistema + ', 1) * ' +
			    'POWER(-1, 1002) * (CASE WHEN ((' + glb_nMoedaSistema + ' = 0) AND (' + glb_nMoedaID + ' <> e.MoedaID)) THEN ' + glb_TaxaFinanceiro +
			    ' ELSE 1 END))) AS ValorTotal, ' +
			    //dbo.fn_Preco_Cotacao(CASE WHEN ' + glb_nMoedaSistema + ' = 0 THEN e.MoedaID ELSE 541 END, ' + glb_nMoedaID + ', NULL, ' + glb_dtEmissao + /*d.dtMovimento*/'))) AS ValorTotal, ' +
		    'CONVERT(NUMERIC(11,2), (dbo.fn_PedidoItem_CampanhaTotais(a.PedItemID, ' + selTipoCampanha.value + ', ' + glb_nMoedaSistema + ', 3) * ' +
			    'POWER(-1, 1002) * (CASE WHEN ((' + glb_nMoedaSistema + ' = 0) AND (' + glb_nMoedaID + ' <> e.MoedaID)) THEN ' + glb_TaxaFinanceiro +
			    ' ELSE 1 END))) AS Saldo, ' +
			    //dbo.fn_Preco_Cotacao(CASE WHEN ' + glb_nMoedaSistema + ' = 0 THEN e.MoedaID ELSE 541 END, ' + glb_nMoedaID + ', NULL, ' + glb_dtEmissao + /*d.dtMovimento*/'))) AS Saldo, ' +
		    //Altera��o Daniel
		    /*
		    'CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(dbo.fn_PedidoItem_CampanhaTotais(a.PedItemID, ' + selTipoCampanha.value + ', ' + glb_nMoedaSistema + ', 1) * POWER(-1, ' + glb_nTipoFinanceiroID + '), d.Quantidade)) AS ValorUnitario, ' +
		    '(dbo.fn_PedidoItem_CampanhaTotais(a.PedItemID, ' + selTipoCampanha.value + ', ' + glb_nMoedaSistema + ', 1)  * POWER(-1, ' + glb_nTipoFinanceiroID + ')) AS ValorTotal, ' +
		    '(dbo.fn_PedidoItem_CampanhaTotais(a.PedItemID, ' + selTipoCampanha.value + ', ' + glb_nMoedaSistema + ', 3)  * POWER(-1, ' + glb_nTipoFinanceiroID + ')) AS Saldo, ' +
		    */
		    ' 0 AS Ok, 0 AS ValorAplicar, ' + sFiltrocampanhas +
	        'FROM Pedidos_Itens_Campanhas a WITH(NOLOCK) ' + sFromDissociar + ' ' +
	            'INNER JOIN Campanhas_Produtos b WITH(NOLOCK) ON (a.CamProdutoID = b.CamProdutoID) ' +
	            'INNER JOIN Campanhas c WITH(NOLOCK) ON (b.CampanhaID = c.CampanhaID) ' +
	            'INNER JOIN Pedidos_Itens d WITH(NOLOCK) ON (a.PedItemID = d.PedItemID) ' +
	            'INNER JOIN Pedidos e WITH(NOLOCK) ON (d.PedidoID = e.PedidoID) ' +
	            'LEFT OUTER JOIN NotasFiscais i WITH(NOLOCK) ON (e.NotaFiscalID = i.NotaFiscalID AND i.EstadoID=67) ' +
	            'INNER JOIN Pessoas f WITH(NOLOCK) ON (e.ParceiroID = f.PessoaID) ' +
	            'INNER JOIN Conceitos g WITH(NOLOCK) ON (d.ProdutoID = g.ConceitoID) ' +
	            'INNER JOIN Pessoas j WITH(NOLOCK) ON (e.EmpresaID = j.PessoaID), ' +
    	        'Conceitos h ' +
	    'WHERE (c.TipoCampanhaID = ' + selTipoCampanha.value + ' AND ' +
		    'e.EstadoID >= 29 AND ' +
		    'h.ConceitoID = ' + glb_nMoedaID + ' AND ' +
		    'e.EmpresaID IN (' + glb_sEmpresas + ') ' + sFiltroDissociar + sFiltroEmpresa + sFiltroFabricante + sFiltroProduto + sFiltroCodigo + sFiltroData + ') ' +

        'SELECT FinPedIteCampanhaID, Empresa, PedIteCampanhaID, Data, PedidoID, NotaFiscal, Parceiro, Codigo, ConceitoID, Conceito, Modelo, Quantidade, ValorUnitarioMoedaSistema, SimboloMoeda, ' +
                'ValorUnitario, ValorTotal, Saldo, Ok, ValorAplicar ' +
            'FROM @Campanhas ' +
            'WHERE  ValorFiltroCampanhas <> 0 ' +
            'ORDER BY Data, Parceiro, PedidoID ';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;

    try {
        dsoGrid.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;

    if (glb_nDSOs > 0)
        return null;

    if (glb_bFirstFill) {
        glb_bFirstFill = false;
        showExtFrame(window, true);

        window.focus();
    }

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg, ['Data',
				   'Empresa',
				   'Pedido',
				   'NF',
				   'Parceiro',
				   'Codigo',
				   'ID',
				   'Produto',
				   'Modelo',
				   'Valor Unit(US$)',
				   'Quant',
				   '$',
				   'Valor Unit',
				   'Valor Total',
				   'Saldo',
				   'Ok',
				   'Aplicar',
				   'PedIteCampanhaID',
				   'FinPedIteCampanhaID'], [17, 18]);

    fillGridMask(fg, dsoGrid, ['Data*',
							 'Empresa*',
							 'PedidoID*',
							 'NotaFiscal*',
							 'Parceiro*',
							 'Codigo*',
							 'ConceitoID*',
							 'Conceito*',
							 'Modelo*',
							 'ValorUnitarioMoedaSistema*',
							 'Quantidade*',
							 'SimboloMoeda*',
							 'ValorUnitario*',
							 'ValorTotal*',
							 'Saldo*',
							 'Ok',
							 'ValorAplicar',
							 'PedIteCampanhaID',
							 'FinPedIteCampanhaID'],
							 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '#99999999.99', '', ''],
							 [dTFormat, '', '', '', '', '', '', '', '', '###,###,##0.00', '', '', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '', '###,###,##0.00', '', '']);

    // linha de Totais
    gridHasTotalLine(fg, 'Total', 0xC0C0C0, null, true, [[2, '##########', 'S'], [10, '######', 'S'], [13, '###,###,##0.00', 'S'], [16, '###,###,##0.00', 'S']]);

    if (fg.Rows > 1) {
        fg.TextMatrix(1, getColIndexByColKey(fg, 'PedidoID*')) = '0';
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Quantidade*')) = '0';
        fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorTotal*')) = padNumReturningStr(roundNumber(0, 2), 2);
        fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorAplicar')) = padNumReturningStr(roundNumber(0, 2), 2);
    }

    fg.Redraw = 0;

    alignColsInGrid(fg, [2, 3, 6, 9, 10, 12, 13, 14, 16]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.FrozenCols = 3;

    paintCellsSpecialyReadOnly();

    fg.ExplorerBar = 5;

    fg.ColWidth(getColIndexByColKey(fg, 'ValorAplicar')) = 150 * FONT_WIDTH;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        fg.Editable = true;
        window.focus();
        fg.focus();
    }
    else {
        ;
    }

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;
    fg.MergeCol(5) = true;
    fg.MergeCol(6) = true;
    fg.MergeCol(7) = true;
    fg.Redraw = 2;

    // ajusta estado dos botoes
    setupBtnsFromGridState(fg.Row);
}

function setSaldoFinanceiro() {
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    lockControlsInModalWin(true);

    setConnection(dsoSaldoFinanceiro);

    dsoSaldoFinanceiro.SQL = ' SELECT dbo.fn_Financeiro_Totais(a.FinanceiroID, GETDATE(), 54) AS SaldoFinanceiro ' +
		'FROM Financeiro a WITH(NOLOCK) ' +
		'WHERE (a.FinanceiroID = ' + glb_nFinanceiroID + ')';

    dsoSaldoFinanceiro.ondatasetcomplete = setSaldoFinanceiro_DSC;
    dsoSaldoFinanceiro.Refresh();
}

function setSaldoFinanceiro_DSC() {
    while (!dsoSaldoFinanceiro.recordset.EOF) {
        glb_nSaldoFinanceiro = dsoSaldoFinanceiro.recordset['SaldoFinanceiro'].value;
        dsoSaldoFinanceiro.recordset.MoveNext();
    }

    lockControlsInModalWin(false);

    // ajusta estado dos botoes
    setupBtnsFromGridState(fg.Row);
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;
    var nUserID = glb_USERID;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    lockControlsInModalWin(true);

    for (i = 2; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0) {
            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                    strPars = '';
                    nDataLen = 0;
                }

                strPars += '?bAD=' + escape(chkAD.checked ? 1 : 0);
            }

            nDataLen++;

            if (chkAD.checked)
                strPars += '&nFinanceiroID=' + escape(glb_nFinanceiroID);
            else
                strPars += '&nFinanceiroID=' + escape('-' + getCellValueByColKey(fg, 'FinPedIteCampanhaID', i));

            strPars += '&nPedIteCampanhaID=' + escape(getCellValueByColKey(fg, 'PedIteCampanhaID', i));
            strPars += '&nValor=' + escape(parseFloat(replaceStr(getCellValueByColKey(fg, 'ValorAplicar', i), ',', '.')));
            strPars += '&nUserID=' + escape(nUserID);
        }
    }

    if (nDataLen > 0) {
        strPars += '&nUserID=' + escape(nUserID);
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
    }

    glb_sResultado = '';
    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/gravarpedidos.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            lockControlsInModalWin(false);

            if (glb_sResultado != '')
                if (window.top.overflyGen.Alert(glb_sResultado) == 0)
                    return null;

            fg.Rows = 1;
            glb_OcorrenciasTimerInt = window.setInterval('setSaldoFinanceiro()', 10, 'JavaScript');
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        if ((dsoGrava.recordset['Resultado'].value != null) && (dsoGrava.recordset['Resultado'].value != ''))
            glb_sResultado += dsoGrava.recordset['Resultado'].value;
    }

    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

function selFabricanteID_onchange() {
    fg.Rows = 1;

    adjustLabelsCombos();
    // ajusta estado dos botoes
    setupBtnsFromGridState(fg.Row);
}

function selEmpresaID_onchange() {
    fg.Rows = 1;

    adjustLabelsCombos();
    // ajusta estado dos botoes
    setupBtnsFromGridState(fg.Row);
}

function adjustLabelsCombos() {
    setLabelOfControl(lblTipoCampanha, selTipoCampanha.value);
    setLabelOfControl(lblEmpresaID, selEmpresaID.value);
    setLabelOfControl(lblFabricanteID, selFabricanteID.value);
}

function listar() {
    if (!verificaData(txtDataInicio))
        return null;

    if (!verificaData(txtDataFim))
        return null;

    fillGridData();
}

function verificaData(oData) {
    var sData = trimStr(oData.value);
    var bDataIsValid = true;

    if (sData != '')
        bDataIsValid = chkDataEx(oData.value);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        window.focus();

        if (sData != '')
            oData.focus();

        return false;
    }
    return true;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtFields_onKeyDown() {
    if (event.keyCode == 13)
        listar();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    lockControlsInModalWin(false);

    glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly() {
    paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalcampanhasBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalcampanhasDblClick(grid, Row, Col) {
    if (Row != 1)
        return null;

    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if (getColIndexByColKey(grid, 'OK') != Col)
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    //lockControlsInModalWin(true);

    glb_PassedOneInDblClick = true;

    for (i = 1; i < grid.Rows; i++) {
        grid.TextMatrix(i, Col) = (glb_bEnableDblClick ? 1 : 0);
        setupBtnsFromGridState(i);
    }

    glb_bEnableDblClick = !glb_bEnableDblClick;

    //lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    //setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcampanhasKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcampanhas_ValidateEdit(grid, nRow, nCol) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcampanhas_BeforeEdit(grid, row, col) {
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if (fldDet == null)   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ((fldDet[0] == 200) || (fldDet[0] == 129))  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcampanhas_AfterEdit(Row, Col) {
    if ((Col == getColIndexByColKey(fg, 'ValorAplicar')) && (Col > 1) && (Row > 1))
        fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

    setupBtnsFromGridState(Row);
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalcampanhas(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return true;

    //setupBtnsFromGridState(NewRow);
}

// FINAL DE EVENTOS DE GRID *****************************************
