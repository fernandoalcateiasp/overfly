
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalgerarocorrenciasHtml" name="modalgerarocorrenciasHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalgerarocorrencias.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
       
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalgerarocorrencias.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, sCurrDateFormat, nFinanceiroID, nEhEstorno, nTipoFinanceiroID, nEmpresaID, nUsuarioID

sCaller = ""
nFinanceiroID = 0
nEhEstorno = 0
nTipoFinanceiroID = 0
nEmpresaID = 0
nUsuarioID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nFinanceiroID").Count    
    nFinanceiroID = Request.QueryString("nFinanceiroID")(i)
Next

For i = 1 To Request.QueryString("nEhEstorno").Count    
    nEhEstorno = Request.QueryString("nEhEstorno")(i)
Next

For i = 1 To Request.QueryString("nTipoFinanceiroID").Count    
    nTipoFinanceiroID = Request.QueryString("nTipoFinanceiroID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nUsuarioID").Count    
    nUsuarioID = Request.QueryString("nUsuarioID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nFinanceiroID = " & CStr(nFinanceiroID) & ";"
Response.Write vbcrlf
Response.Write "var glb_bEhEstorno = false;"
Response.Write vbcrlf

If (CLng(nEhEstorno) = 1) Then
	Response.Write "glb_bEhEstorno = true;"
End If

Response.Write vbcrlf

'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

Dim SQLDataFormat

SQLDataFormat = 103

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
	SQLDataFormat = 103
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
	SQLDataFormat = 101
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If

Dim rsData, strSQL, nParceiroID, sParceiro

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT a.FinanceiroID, b.RecursoAbreviado AS Estado, ISNULL(a.PedidoID, 0) AS PedidoID, " & _
				"ISNULL((SELECT d.OperacaoID FROM Operacoes d WITH(NOLOCK) WHERE (c.TransacaoID = d.OperacaoID)), SPACE(0)) AS CFOP, " & _
				"ISNULL((SELECT d.Operacao FROM Operacoes d WITH(NOLOCK) WHERE (c.TransacaoID = d.OperacaoID)), SPACE(0)) AS Operacao, " & _
				"a.Duplicata, a.PessoaID AS PessoaID, a.ParceiroID AS ParceiroID, e.Fantasia AS Pessoa, f.Fantasia AS Parceiro, g.ItemAbreviado AS FormaPagamento, " & _
				"a.PrazoPagamento, CONVERT(VARCHAR(10), a.dtVencimento, " & CStr(SQLDataFormat) & ") AS dtVencimento, " & _
				"h.SimboloMoeda AS Moeda, a.Valor, " & _
                "dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, " & _
	            "dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, " & _
	            "a.TipoFinanceiroID " & _
            "FROM Financeiro a WITH(NOLOCK) " & _
                "LEFT OUTER JOIN Pedidos c WITH(NOLOCK) ON (a.PedidoID = c.PedidoID) " & _
                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " & _
                "INNER JOIN Pessoas e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID) " & _
                "INNER JOIN Pessoas f WITH(NOLOCK) ON (a.ParceiroID = f.PessoaID) " & _
                "INNER JOIN TiposAuxiliares_Itens g WITH(NOLOCK) ON (a.FormaPagamentoID = g.ItemID) " & _
                "INNER JOIN Conceitos h WITH(NOLOCK) ON (a.MoedaID = h.ConceitoID) " & _
            "WHERE (a.FinanceiroID = " & CStr(nFinanceiroID) & " ) "

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

While (Not rsData.EOF)
    Response.Write "var glb_sEstado = " & Chr(34) & rsData.Fields("Estado").Value & Chr(34) & ";"
	Response.Write vbcrlf
    Response.Write "var glb_nPedidoID = " & rsData.Fields("PedidoID").Value & ";"
	Response.Write vbcrlf
    Response.Write "var glb_sCFOP = " & Chr(34) & rsData.Fields("CFOP").Value & Chr(34) & ";"
	Response.Write vbcrlf
    Response.Write "var glb_sOperacao= " & Chr(34) & rsData.Fields("Operacao").Value & Chr(34) & ";"
	Response.Write vbcrlf
    Response.Write "var glb_sDuplicata = " & Chr(34) & rsData.Fields("Duplicata").Value & Chr(34) & ";"
	Response.Write vbcrlf
    Response.Write "var glb_nPessoaID = " & rsData.Fields("PessoaID").Value & ";"
	Response.Write vbcrlf
    Response.Write "var glb_sPessoa = " & Chr(34) & rsData.Fields("Pessoa").Value & Chr(34) & ";"
	Response.Write vbcrlf
    Response.Write "var glb_nParceiroID = " & rsData.Fields("ParceiroID").Value & ";"
	Response.Write vbcrlf
    Response.Write "var glb_sParceiro = " & Chr(34) & rsData.Fields("Parceiro").Value & Chr(34) & ";"
	Response.Write vbcrlf
    Response.Write "var glb_sFormaPagamento = " & Chr(34) & rsData.Fields("FormaPagamento").Value & Chr(34) & ";"
	Response.Write vbcrlf
    Response.Write "var glb_nPrazoPagamento= " & rsData.Fields("PrazoPagamento").Value & ";"
	Response.Write vbcrlf
    Response.Write "var glb_sdtVencimento = " & Chr(34) & rsData.Fields("dtVencimento").Value & Chr(34) & ";"
	Response.Write vbcrlf
    Response.Write "var glb_sMoeda = " & Chr(34) & rsData.Fields("Moeda").Value & Chr(34) & ";"
	Response.Write vbcrlf
    Response.Write "var glb_nValor= " & Chr(34) & rsData.Fields("Valor").Value & Chr(34) & ";"
	Response.Write vbcrlf
    Response.Write "var glb_nSaldoDevedor= " & Chr(34) & rsData.Fields("SaldoDevedor").Value & Chr(34) & ";"
	Response.Write vbcrlf
    Response.Write "var glb_nSaldoAtualizado= " & Chr(34) & rsData.Fields("SaldoAtualizado").Value & Chr(34) & ";"
	Response.Write vbcrlf
	nParceiroID = rsData.Fields("ParceiroID").Value
	Response.Write vbcrlf
	sParceiro = rsData.Fields("Parceiro").Value
	rsData.MoveNext
Wend

rsData.Close

strSQL = "SELECT Aliquota AS AliquotaPCC " & _
	        "FROM Conceitos_ImpostosRetencoes WITH(NOLOCK) " & _
	        "WHERE ConceitoID = 18"

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write "var glb_nAliquotaPCC = " & Chr(34) & rsData.Fields("AliquotaPCC").Value & Chr(34) & ";"
Response.Write vbcrlf

rsData.Close
	
Response.Write "</script>"
Response.Write vbcrlf
%>
<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalgerarocorrencias_ValidateEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalgerarocorrencias_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalgerarocorrenciasKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalgerarocorrencias_BeforeEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modalgerarocorrenciasDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalgerarocorrenciasBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Atualizar linha de mensagens
js_fg_AfterRowColmodalgerarocorrencias (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<script ID="vbFunctions" LANGUAGE="vbscript">
<!--

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

//-->
</script>

</head>

<body id="modalgerarocorrenciasBody" name="modalgerarocorrenciasBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <!-- Div Controles -->
    <div id="divControls" name="divControls" class="divGeneral">
        <p class="lblGeneral" id="lblFinanceiroID">ID</p>
        <input type="text" id="txtFinanceiroID" name="txtFinanceiroID" class="fldGeneral">
        <p id="lblEstado" name="lblEstado" class="lblGeneral">Est</p>
        <input type="text" id="txtEstado" name="txtEstado" class="fldGeneral">
        <p id="lblPedidoID" name="lblPedidoID" class="lblGeneral">Pedido</p>
        <input type="text" id="txtPedidoID" name="txtPedidoID" class="fldGeneral">
        <p id="lblCFOP" name="lblCFOP" class="lblGeneral">CFOP</p>
        <input type="text" id="txtCFOP" name="txtCFOP" class="fldGeneral">
        <p id="lblDuplicata" name="lblDuplicata" class="lblGeneral">Duplicata</p>
        <input type="text" id="txtDuplicata" name="txtDuplicata" class="fldGeneral">
        <p id="lblPessoa" name="lblPessoa" class="lblGeneral">Pessoa</p>
        <input type="text" id="txtPessoa" name="txtPessoa" class="fldGeneral">
        <p id="lblParceiro" name="lblParceiro" class="lblGeneral">Parceiro</p>
        <input type="text" id="txtParceiro" name="txtParceiro" class="fldGeneral">
        <p id="lblFormaPagamento" name="lblFormaPagamento" class="lblGeneral">Forma</p>
        <input type="text" id="txtFormaPagamento" name="txtFormaPagamento" class="fldGeneral">
        <p id="lblPrazoPagamento" name="lblPrazoPagamento" class="lblGeneral">Prazo</p>
        <input type="text" id="txtPrazoPagamento" name="txtPrazoPagamento" class="fldGeneral">
        <p id="lbldtVencimento" name="lbldtVencimento" class="lblGeneral">Vencimento</p>
        <input type="text" id="txtdtVencimento" name="txtdtVencimento" class="fldGeneral">
        <p id="lblMoeda" name="lblMoeda" class="lblGeneral">$</p>
        <input type="text" id="txtMoeda" name="txtMoeda" class="fldGeneral">
        <p id="lblValor" name="lblValor" class="lblGeneral">Valor</p>
        <input type="text" id="txtValor" name="txtValor" class="fldGeneral">
        <p id="lblSaldoDevedor" name="lblSaldoDevedor" class="lblGeneral">Saldo Devedor</p>
        <input type="text" id="txtSaldoDevedor" name="txtSaldoDevedor" class="fldGeneral">
        <p id="lblSaldoAtualizado" name="lblSaldoAtualizado" class="lblGeneral">Saldo Atualizado</p>
        <input type="text" id="txtSaldoAtualizado" name="txtSaldoAtualizado" class="fldGeneral">
        <p id="lblSaldo" name="lblSaldo" class="lblGeneral">Saldo</p>
        <input type="text" id="txtSaldo" name="txtSaldo" class="fldGeneral">
        <p id="lblPedidoPesquisaID" name="lblPedidoPesquisaID" class="lblGeneral">Pedido</p>
        <input type="text" id="txtPedidoPesquisaID" name="txtPedidoPesquisaID" class="fldGeneral">
        <p id="lblFinanceiroPesquisaID" name="lblFinanceiroPesquisaID" class="lblGeneral">Financeiro</p>
        <input type="text" id="txtFinanceiroPesquisaID" name="txtFinanceiroPesquisaID" class="fldGeneral">
        <p id="lblArgumento" name="lblArgumento" class="lblGeneral">Argumento</p>
        <input type="text" id="txtArgumento" name="txtArgumento" class="fldGeneral">
        <input type="image" id="btnFindPessoa" name="btnFindPessoa" class="fldGeneral" title="Preencher combo" onclick="javascript:return btnLupaClicked()" WIDTH="24" HEIGHT="23">
        <p id="lblParceiroID" name="lblParceiroID" class="lblGeneral">Parceiro</p>
        <select id="selParceiroID" name="selParceiroID" class="fldGeneral">
			<option value=0></option>
            <option value=<% Response.Write nParceiroID %>
            selected>
            <% Response.Write sParceiro %></option>
        </select>
        <p id="lblSaldo2" name="lblSaldo2" class="lblGeneral">Saldo</p>
        <input type="checkbox" id="chkSaldo" name="chkSaldo" class="fldGeneral" title="considera o saldo devedor atualizado ou o saldo devedor?">
        
        <p id="lblEmpresaID" name="lblEmpresaID" class="lblGeneral">Empresa</p>
		<select id="selEmpresaID" name="selEmpresaID" class="fldGeneral">
<%
	Dim nAtributo1ID, nAtributo2ID
	
	IF (CLng(nTipoFinanceiroID) = 1001) Then
		nAtributo1ID = 44
		nAtributo2ID = 45
	Else
		nAtributo1ID = 45
		nAtributo2ID = 44
	End If	
	
	strSQL = "SELECT b.PessoaID, b.Fantasia " & _
		"FROM RelacoesPesRec a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " & _
		"WHERE (a.TipoRelacaoID = 12 AND a.EstadoID = 2 AND a.ObjetoID = 999 AND a.EstaEmOperacao = 1 AND " & _
			"a.SujeitoID = b.PessoaID AND b.EstadoID = 2 AND " & _
			"dbo.fn_Direitos_Verifica(" & CStr(nUsuarioID) & ", " & CStr(nEmpresaID) & ", NULL, " & CStr(nAtributo1ID) & ", 1, GETDATE()) = 1 AND " & _
			"dbo.fn_Direitos_Verifica(" & CStr(nUsuarioID) & ", b.PessoaID, NULL, " & CStr(nAtributo2ID) & ", 1, GETDATE()) = 1) " & _
		"ORDER BY b.Fantasia "

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	While (Not rsData.EOF)
		Response.Write "<option value =" & rsData.Fields("PessoaID").Value & " "
		
		If (CLng(rsData.Fields("PessoaID").Value) = CLng(nEmpresaID)) Then
			Response.Write " SELECTED "
		End If
		
		Response.Write ">" & rsData.Fields("Fantasia").Value & "</option>" & chr(13) & chr(10)        
	
		rsData.MoveNext
	WEnd

	rsData.Close
	Set rsData = Nothing
%>		
		</select>
        
        <p id="lblDataInicio" name="lblDataInicio" class="lblGeneral">Data Inicio Ciclo</p>
        <input type="text" id="txtDataInicio" name="txtDataInicio" class="fldGeneral">

        <p id="lblDataFim" name="lblDataFim" class="lblGeneral">Data Fim Ciclo</p>
        <input type="text" id="txtDataFim" name="txtDataFim" class="fldGeneral">

    </div>    

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
            
	<input type="button" id="btnFillGrid" name="btnFillGrid" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnOK" name="btnOK" value="Ocorrências" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
</body>

</html>
