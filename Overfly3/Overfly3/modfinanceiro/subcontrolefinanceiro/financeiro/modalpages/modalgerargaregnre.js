/********************************************************************
modalgerargaregnre.js

Library javascript para o modalgerargaregnre.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoListarFinGareGnre = new CDatatransport("dsoListarFinGareGnre");
var dsoCaminhoRede = new CDatatransport("dsoCaminhoRede");
var dsoTipoGuia = new CDatatransport("dsoTipoGuia");
var dsoGeraGareGnre = new CDatatransport("dsoGeraGareGnre");
var dsoNomeArquivo = new CDatatransport("dsoNomeArquivo");
var dsoVerificaValor = new CDatatransport("dsoVerificaValor");

var glbPedidoID;
var glbRow;
var glbVerifica;
var glbMensagemVerificaValor;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES

NOTA: Ver tambem o arquivo commongerargaregnre.js

FUNCOES GERAIS:
window_onload()
setupPage()
txtPesquisa_onKeyPress()
txtPesquisa_ondigit(ctl)
btnFindPesquisa_onclick(ctl)
btn_onclick(ctl)
startPesq(strPesquisa)
dataToReturn()

Eventos de grid particulares desta janela:
fg_modalgerargaregnreDblClick()
fg_modalgerargaregnreKeyPress(KeyAscii)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // ajusta o body do html

    with (modalgerargaregnreBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Gerar GARE GNRE ', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;


    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
    }

    adjustElementsInForm([['lblUF', 'selUF', 12, 1, 0, 10],
                          ['lblTipoGuia', 'selTipoGuia', 10, 1, 0],
                          ['lblDifal', 'chkDifal', 3, 1, 0],
                          ['lblVias', 'selvias', 4, 1, 0],
						  ['lblFinanceiro', 'txtFinanceiro', 10, 1, -2],
						  ['lblCaminhorede', 'txtCaminhoRede', 30, 1, 154]], null, null, true);

    with (divFG.style) {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = (ELEM_GAP * 2) - 1;
        top = (ELEM_GAP * 3) + 70;
        width = modWidth - 50;
        height = MAX_FRAMEHEIGHT_OLD - 250;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // Posiciona botao Listar
    with (btnListar) {
        style.top = divControls.offsetTop + txtFinanceiro.offsetTop;
        style.width = 65;
        style.left = modWidth - ELEM_GAP - 415;
    }

    // Posiciona botao Listar
    with (btnGerarArquivo) {
        style.top = divControls.offsetTop + txtFinanceiro.offsetTop;
        style.width = 80;
        style.left = modWidth - ELEM_GAP - 345;
    }

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.style.width, 10) * 18;
    fg.Redraw = 2;

    btnOK.style.visibility = 'hidden';
    btnCanc.value = 'Cancelar';
    btnCanc.style.visibility = 'hidden';
    btnGerarArquivo.disabled = true;
    btnListar.disabled = false;
    btnListar.onclick = btnListar_onclick;
    btnGerarArquivo.onclick = btnGerarArquivo_onclick;
    selUF.onchange = selUF_onchange;
    selTipoGuia.onchange = selTipoGuia_onchange;
    txtFinanceiro.onkeypress = Financeiro_onkeypress;
    chkDifal.disabled = true;
    preencheFiltroTipoGuia();
    PreencheCaminhoRede();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_modalgerargaregnreDblClick() {
    var nPreencheOk = 0;

    if ((fg.Row == 1) && (fg.Col == 7)) {
        for (i = 2; i < fg.Rows; i++) {
            if (i == 2) {
                for (y = 2; y < fg.Rows; y++) {
                    if (fg.TextMatrix(y, getColIndexByColKey(fg, 'OK')) == 0)
                        nPreencheOk = nPreencheOk + 1;
                }
            }

            if (nPreencheOk != 0) {
                fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = -1;
                glbVerifica = true;
            }
            else {
                fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = 0;
                glbVerifica = false;
            }
        }
    }

    totalLine();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('OK_CALLFORM_PL'), null);
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_PL'), null);
    }
}


/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_modalgerargaregnreKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerargaregnre_AfterEdit(Row, Col) {
    glbPedidoID = fg.TextMatrix(Row, getColIndexByColKey(fg, 'PedidoID*'));
    glbRow = Row;

    if (!glbVerifica)
        verificaFinanceiroValor();

    totalLine();
}

function verificaFinanceiroValor() {
    setConnection(dsoVerificaValor);

    if (!glbVerifica) {
        dsoVerificaValor.SQL = 'SELECT SUM(dbo.fn_PedidoItem_DemonstrativoCalculoICMSST(a.PedItemID, 2)) AS ICMSST ' +
                                    'FROM Pedidos_Itens a WITH(NOLOCK) ' +
                                    'WHERE a.PedidoID = ' + glbPedidoID + ' AND' +
                                    '(dbo.fn_PedidoItem_STGuiaRecolhimento(a.PedItemID) = (CASE WHEN ' + selTipoGuia.value + ' = 1401 THEN 2 ELSE 1 END))';

    }
    else {
        dsoVerificaValor.SQL = 'SELECT a.PedidoID, SUM(dbo.fn_PedidoItem_DemonstrativoCalculoICMSST(a.PedItemID, 2)) AS ICMSST ' +
                                    'FROM Pedidos_Itens a WITH(NOLOCK) ' +
                                    'WHERE a.PedidoID IN (' + glbPedidoID + ')' + ' AND' +
                                    '(dbo.fn_PedidoItem_STGuiaRecolhimento(a.PedItemID) = (CASE WHEN ' + selTipoGuia.value + ' = 1401 THEN 2 ELSE 1 END))' +
                                    'GROUP BY a.PedidoID';
    }

    dsoVerificaValor.ondatasetcomplete = verificaFinanceiroValor_DSC;
    dsoVerificaValor.Refresh();
}

function verificaFinanceiroValor_DSC() {
    var nValorICMSST;
    var nValorFinanceiro;
    var nFinanceiroID;
    var nPedidoID;
    var bOK;
    var i = 2;

    if ((!(dsoVerificaValor.recordset.EOF) || (dsoVerificaValor.recordset.BOF)) && (!chkDifal.checked)) {
        if (!glbVerifica) {
            nValorICMSST = dsoVerificaValor.recordset['ICMSST'].value;
            nValorFinanceiro = fg.ValueMatrix(glbRow, getColIndexByColKey(fg, 'Valor*'));
            nFinanceiroID = fg.ValueMatrix(glbRow, getColIndexByColKey(fg, 'Financeiro*'));
            bOK = fg.TextMatrix(glbRow, getColIndexByColKey(fg, 'OK'));

            if ((nValorICMSST != nValorFinanceiro) && (bOK)) {
                fg.TextMatrix(glbRow, getColIndexByColKey(fg, 'OK')) = !bOK;
                window.top.window.top.overflyGen.Alert('Financeiro ' + nFinanceiroID + ' diferente do demonstrativo.    Valor: R$ ' + nValorFinanceiro + '    Demonstrativo: R$ ' + nValorICMSST);
            }
        }
        else {
            while (i < fg.Rows) {
                if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK'))) {
                    nPedidoID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'PedidoID*'));
                    nFinanceiroID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Financeiro*'));
                    nValorFinanceiro = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor*'));
                    dsoVerificaValor.recordset.moveFirst();
                    dsoVerificaValor.recordset.Find('PedidoID', nPedidoID);

                    if (!(dsoVerificaValor.recordset.EOF) || (dsoVerificaValor.recordset.BOF)) {
                        nValorICMSST = dsoVerificaValor.recordset['ICMSST'].value;

                        if (nValorICMSST != nValorFinanceiro) {
                            if ((glbMensagemVerificaValor == null) && (glbMensagemVerificaValor == undefined))
                                glbMensagemVerificaValor = 'Financeiro ' + nFinanceiroID + ' diferente do demonstrativo.    Valor: R$ ' + nValorFinanceiro +
                                                                    '    Demonstrativo: R$ ' + nValorICMSST + '\n';
                            else
                                glbMensagemVerificaValor += 'Financeiro ' + nFinanceiroID + ' diferente do demonstrativo.    Valor: R$ ' + nValorFinanceiro +
                                                                    '    Demonstrativo: R$ ' + nValorICMSST + '\n';
                        }
                    }
                }

                i++;
            }

            if ((glbMensagemVerificaValor != null) && (glbMensagemVerificaValor != undefined)) {
                window.top.window.top.overflyGen.Alert(glbMensagemVerificaValor);
                lockControlsInModalWin(false);
                return null;
            }
            else
                GeraGareGnre();
        }
    }
    else
        return null;
}

function selUF_onchange() {
    if (fg.Rows > 1) {
        fg.Rows = 1;
        btnGerarArquivo.disabled = true;
    }

    if (selUF.value == 1)
        selVias.disabled = false;
    else
        selVias.disabled = true;
}

function selTipoGuia_onchange() {
    if (fg.Rows > 1) {
        fg.Rows = 1;
        btnGerarArquivo.disabled = true;
    }
    if (selTipoGuia.value == 1401) {
        chkDifal.disabled = false;

    }
    else {
        chkDifal.disabled = true;
    }

}

function PreencheCaminhoRede() {
    var empresa = getCurrEmpresaData();
    var nEmpresaID = empresa[0];

    setConnection(dsoCaminhoRede);
    dsoCaminhoRede.SQL = 'SELECT ISNULL(CaminhoGuiaRecolhimento, SPACE(0)) AS CaminhoGuiaRecolhimento ' +
                            'FROM RelacoesPesRec a WITH (NOLOCK) ' +
                            'WHERE a.TipoRelacaoID = 12 AND a.RelacaoID = ' + nEmpresaID;
    dsoCaminhoRede.ondatasetcomplete = CaminhoRede_DSC;
    dsoCaminhoRede.Refresh();
}

function CaminhoRede_DSC() {
    txtCaminhoRede.value = dsoCaminhoRede.recordset['CaminhoGuiaRecolhimento'].value;
    txtCaminhoRede.disabled = true;
}

function preencheFiltroTipoGuia() {
    setConnection(dsoTipoGuia);
    dsoTipoGuia.SQL = 'SELECT a.ItemID, a.ItemMasculino FROM TiposAuxiliares_Itens a WITH (NOLOCK) WHERE a.TipoID = 814';
    dsoTipoGuia.ondatasetcomplete = tipoGuia_DSC;
    dsoTipoGuia.Refresh();
}

function tipoGuia_DSC() {
    clearComboEx(['selTipoGuia']);

    var oOption = document.createElement("OPTION");
    //oOption.text = '';
    //oOption.value = '';
    //selTipoGuia.add(oOption);

    dsoTipoGuia.recordset.moveFirst();

    while (!dsoTipoGuia.recordset.EOF) {
        optionStr = dsoTipoGuia.recordset['ItemMasculino'].value;
        optionValue = dsoTipoGuia.recordset['ItemID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        //oOption.setAttribute('Filtro', dsoTipoGuia.recordset('Filtro').Value, 1);
        selTipoGuia.add(oOption);
        dsoTipoGuia.recordset.MoveNext();
    }
}

function btnListar_onclick() {
    listaFinanceiros();
}

function Financeiro_onkeypress() {
    if (event.keyCode == 13)
        listaFinanceiros();
}

/*******************************************************************
Select que preenche o grid
*******************************************************************/
function listaFinanceiros() {
    var empresa = getCurrEmpresaData();
    var nEmpresa = empresa[0];
    var nAplicacao = selUF.value;
    var nFinanceiro = txtFinanceiro.value;
    var nTipoGuia = selTipoGuia.value;
    var bDifal = (chkDifal.checked ? 1 : 0);

    glbVerifica = false;

    lockControlsInModalWin(true);

    var strPars = '?nEmpresa=' + escape(nEmpresa);
    strPars += '&nAplicacao=' + escape(nAplicacao);
    strPars += '&nTipoGuia=' + escape(nTipoGuia);
    strPars += '&nFinanceiro=' + escape(nFinanceiro);
    strPars += '&bDifal=' + escape(bDifal);

    setConnection(dsoListarFinGareGnre);
    dsoListarFinGareGnre.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/garegnre.aspx' + strPars;
    dsoListarFinGareGnre.ondatasetcomplete = listaFinanceiros_DSC;
    dsoListarFinGareGnre.Refresh();
}

function listaFinanceiros_DSC() {
    if (!(dsoListarFinGareGnre.recordset.EOF) || (dsoListarFinGareGnre.recordset.BOF)) {
        startGridInterface(fg);
        fg.FontSize = '8';
        fg.FrozenCols = 0;

        headerGrid(fg, ['Financeiro',
                       'Observa��o',
                       'PessoaID',
                       'Pessoa',
                       'UF',
                       'Pedido',
                       'Valor',
                       'OK'], []);

        fillGridMask(fg, dsoListarFinGareGnre, ['Financeiro*',
                       'Observacao*',
                       'PessoaID*',
                       'Pessoa*',
                       'UF*',
                       'PedidoID*',
                       'Valor*',
                       'OK'],
                       ['', '', '', '', '', '', '', ''],
				       ['', '', '', '', '', '', '###,###,###,###.00', '']);

        fg.ColDataType(getColIndexByColKey(fg, 'OK')) = 11; // format boolean (checkbox)

        if (fg.Rows > 1) {
            gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C']]);
            totalLine();
        }

        alignColsInGrid(fg, [0, 2, 5, 6]);

        //fg.FrozenCols = 1;
        fg.Redraw = 2;
        fg.Editable = true;

        lockControlsInModalWin(false);

        // destrava botao Incluir se tem linhas no grid
        if (fg.Rows > 1) {
            window.focus();
            fg.focus();
            btnGerarArquivo.disabled = false;
        }
        else
            btnGerarArquivo.disabled = true;
    }
}

function totalLine() {
    var nValor = 0;
    var nFinanceiros = 0;

    for (i = 2; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
            nValor = nValor + fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor*'));
            nFinanceiros += 1;
        }
    }

    fg.TextMatrix(1, getColIndexByColKey(fg, 'Financeiro*')) = nFinanceiros;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor*')) = nValor;
}

function btnGerarArquivo_onclick() {
    lockControlsInModalWin(true);

    var i = 2;

    while (i < fg.rows) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
            if ((glbPedidoID == null) || (glbPedidoID == undefined))
                glbPedidoID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'PedidoID*'));
            else
                glbPedidoID += ', ' + fg.ValueMatrix(i, getColIndexByColKey(fg, 'PedidoID*'));
        }

        i++;
    }

    if (glbVerifica)
        verificaFinanceiroValor();
    else
        GeraGareGnre();
}

function GeraGareGnre() {
    var nAplicacao = selUF.value;
    var sCaminho = txtCaminhoRede.value;
    var sPedidos;
    var sFinanceiro;
    var sTipoGAREGNRE = selTipoGuia[selTipoGuia.selectedIndex].text;
    var nVias = selVias.value;
    var i = 0;
    var j = 0;
    var strPars;
    var aFinanceiro = new Array();

    lockControlsInModalWin(true);

    for (i = 2; i < fg.rows; i++) {

        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
            if (sPedidos != null)
                sPedidos += '/' + fg.ValueMatrix(i, getColIndexByColKey(fg, 'PedidoID*'));
            else
                sPedidos = '/' + fg.ValueMatrix(i, getColIndexByColKey(fg, 'PedidoID*'));
        }
    }

    if (sCaminho != "") {

        if (sPedidos != null) {
            sPedidos += '/';

            strPars = '?sPedidosID=' + escape(sPedidos);
            strPars += '&nAplicacaoID=' + escape(nAplicacao);
            strPars += '&sCaminhoRede=' + escape(sCaminho);
            strPars += '&sTipoGAREGNRE=' + escape(sTipoGAREGNRE);
            strPars += '&nVias=' + escape(nVias);

            i = 2;
            j = 0;

            while (i < fg.rows) {
                if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
                    aFinanceiro[j] = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Financeiro*'));
                    j++;
                }

                i++;
            }

            for (i = 0; i < aFinanceiro.length; i++) {
                if (aFinanceiro.length == 1)
                    strPars += '&aFinanceiro=' + aFinanceiro[i];
                else if (i == 0)
                    strPars += '&aFinanceiro=' + aFinanceiro[i] + ',';
                else if ((i + 1) < aFinanceiro.length)
                    strPars += aFinanceiro[i] + ',';
                else
                    strPars += aFinanceiro[i];
            }

            setConnection(dsoGeraGareGnre);
            dsoGeraGareGnre.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/geragaregnre.aspx' + strPars;
            dsoGeraGareGnre.ondatasetcomplete = GeraGareGnre_DSC;
            dsoGeraGareGnre.Refresh();
        }
        else {

            window.top.window.top.overflyGen.Alert('Selecione um financeiro da lista.');
            lockControlsInModalWin(false);
        }
    }
    else {
        window.top.window.top.overflyGen.Alert('Caminho inv�lido.');
        lockControlsInModalWin(false);
    }
}

function GeraGareGnre_DSC() {
    var sMensagem = dsoGeraGareGnre.recordset['Mensagem'].value;
    var sNomeArquivo = dsoGeraGareGnre.recordset['NomeArquivo'].value;

    if (sMensagem != 'ok')
        window.top.overflyGen.Alert('Erro ao gerar o arquivo ' + sNomeArquivo);
    else {
        window.top.overflyGen.Alert('Arquivo ' + sNomeArquivo + ' Gerado com sucesso');
        listaFinanceiros();
    }
}