/********************************************************************
modalprint_financeiro.js

Library javascript para o modalprint.asp
Financeiro
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_BoletoPrinted = false;
var glb_nDSOsCampanhas = 0;

var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
var dsoCmbsCampanhas = new CDatatransport("dsoCmbsCampanhas");
var dsoCmbsCampanhas2 = new CDatatransport("dsoCmbsCampanhas2");
var dsoBancos = new CDatatransport("dsoBancos");
var dsoPrint01 = new CDatatransport("dsoPrint01");
var dsoPrint02 = new CDatatransport("dsoPrint02");
var dsoUpdateNossoNumero = new CDatatransport("dsoUpdateNossoNumero");
var dsoEMail = new CDatatransport("dsoEMail");
var dsoTicket1 = new CDatatransport("dsoTicket1");
var dsoTicket2 = new CDatatransport("dsoTicket2");
var dsoTicket3 = new CDatatransport("dsoTicket3");
var dsoLayoutCheque = new CDatatransport("dsoLayoutCheque");
var dsoCheque = new CDatatransport("dsoCheque");
/*var dsoTicket1 = new CDatatransport("dsoTicket1");
var dsoTicket2 = new CDatatransport("dsoTicket2");
var dsoLayoutCheque = new CDatatransport("dsoLayoutCheque");
var dsoCheque = new CDatatransport("dsoCheque");*/
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    linksDivAndReports()
    selReports_Change()
    btnOK_Status(
    btnOK_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    adjustDivFluxoCaixa()
    adjustDivBoletoCobranca()
    adjustDivNotaDebito()
    adjustdivCartaAnuencia()
    adjustDivVendasRecebidas()
    adjustDivSerasa()
    adjustCampanhas()
    setMaskAndLengthToInputs()
    checkDataCoerency()
    noRights()
    relatorioDepositosBancarios_BRA()
    relatorioDepositosBancarios_USA()
    relatorioDepositosBancarios_USA_DSC()
    DepositoBancario()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    glb_bUseProgressBar = true;
    asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalprintBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // inicia o carregamento de combos desta modal
    startLocalCmbs();


}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    window.focus();
    if (ctl.id == btnOK.id)
        btnOK.focus();
    else if (ctl.id == btnCanc.id)
        btnCanc.focus();

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        pb_StopProgressBar(true);

        if (glb_BoletoPrinted)
            sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, true);
        else
            sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Relat�rios', 1);

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;

    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    with (lblReports.style) {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // ajusta combo de relatorios
    with (selReports.style) {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // ajusta o Fluxo de Caixa
    with (divFluxoCaixa.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP) + 500;
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP + 300;
    }

    // ajusta o BoletoCobranca
    with (divBoletoCobranca.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    //ajusta o divNotaDebito
    with (divNotaDebito.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    //ajusta o divCartaAnuencia
    with (divCartaAnuencia.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divVendasRecebidas
    with (divVendasRecebidas.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divSerasa
    with (divSerasa.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o Campanhas
    with (divCampanhas.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o Campanhas
    with (divDepositoBancario.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // O estado do botao btnOK
    btnOK_Status();

    // O usuario tem direito ou nao
    noRights();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;

    selReports.onchange = selReports_Change;

    // carrega a lista de relatorios
    loadReportsInList();

    // linka os divs nos relatorios
    linkDivsAndReports();

    // ajusta o divFluxoCaixa
    adjustDivFluxoCaixa();

    // ajusta o divBoletoCobranca
    adjustDivBoletoCobranca();

    //ajusta o divNotaDebito
    adjustDivNotaDebito();

    adjustdivCartaAnuencia();

    // ajusta o divVendasRecebidas
    adjustDivVendasRecebidas();

    // ajusta o divSerasa
    adjustDivSerasa();

    // ajusta o divCampanhas
    adjustCampanhas();

    // ajusta o divDepositoBancario
    adjustdivDepositoBancario();

    // restringe digitacao em campos numericos
    setMaskAndLengthToInputs();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList() {
    var i;

    if (glb_arrayReports != null) {
        for (i = 0; i < glb_arrayReports.length; i++) {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);

            if (glb_arrayReports[i][2])
                selReports.selectedIndex = i;
        }

        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports() {
    divCartaAnuencia.setAttribute('report', 40164, 1);
    divNotaDebito.setAttribute('report', 40162, 1);
    divFluxoCaixa.setAttribute('report', 40183, 1);
    divBoletoCobranca.setAttribute('report', 40182, 1);
    divVendasRecebidas.setAttribute('report', 40184, 1);
    divSerasa.setAttribute('report', 40185, 1);
    divCampanhas.setAttribute('report', 40186, 1);
    divDepositoBancario.setAttribute('report', 40195, 1);
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change() {
    // mostra o div correspondente ao relatorio
    showDivByReportID();

    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if (this.disabled == false)
        this.focus();

    if ((parseInt(this.value, 10) == 40182) && (txtNossoNumero.disabled == false))
        txtNossoNumero.focus();

    if ((parseInt(this.value, 10) == 40186))
        loadCmbsCampanhas();

    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status() {
    var btnOKStatus = true;
    var i, elem;
    var nFormaPagamento;
    var sDuplicata = '';
    var nHistoricoPadrao = 0;
    var nCarteira = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
	    'dsoSup01.recordset[' + '\'' + 'Carteira' + '\'' + '].value');

    if (selReports.selectedIndex != -1) {
        //Nota Debito
        if (selReports.value == 40162) {
            nHistoricoPadrao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'HistoricoPadraoID' + '\'' + ').value');

            if (((nHistoricoPadrao != 3001) && (nHistoricoPadrao != 3005) && (nHistoricoPadrao != 3021)) || (nHistoricoPadrao == null) || (nHistoricoPadrao == ''))
                btnOKStatus = false;
        }

            // Carta de Anuencia 
        else if (selReports.value == 40164) {
            btnOKStatus = false;
        }

            // Duplicatas 
        else if (selReports.value == 40181) {
            sDuplicata = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Duplicata' + '\'' + '].value');
            if ((sDuplicata != '') && (sDuplicata != null))
                btnOKStatus = false;
        }
            // Boletos    
        else if (selReports.value == 40182) {
            var nFormaPagamentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
            'dsoSup01.recordset[' + '\'' + 'FormaPagamentoID' + '\'' + '].value');

            var sNossoNumero = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
            'dsoSup01.recordset[' + '\'' + 'NossoNumero' + '\'' + '].value');

            var sCarteira = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
            'dsoSup01.recordset[' + '\'' + 'Carteira' + '\'' + '].value');

            //if ( (sCarteira == '17') && (nFormaPagamentoID == 1035) && (sNossoNumero != '') && (sNossoNumero != null) )
            if ((sCarteira == glb_nCarteiraCOD) && (nFormaPagamentoID == 1035) && (sNossoNumero != '') && (sNossoNumero != null))
                btnOKStatus = false;
                //else if ( (sCarteira == '18') && (nFormaPagamentoID == 1035 ) )
            else if ((sCarteira == glb_nCarteiraCOB) && (nFormaPagamentoID == 1035))
                btnOKStatus = false;
        }
            // Fluxo de Caixa 
        else if (selReports.value == 40183)
            btnOKStatus = false;
            // Vendas Recebidas    
        else if (selReports.value == 40184)
            btnOKStatus = false;
            // Arquivo Serasa    
        else if (selReports.value == 40185)
            btnOKStatus = false;
            // Campanhas
        else if (selReports.value == 40186)
            btnOKStatus = false;
            // DepositoBancario
        else if (selReports.value == 40195)
            btnOKStatus = false;
    }

    btnOK.disabled = btnOKStatus;

    if (selReports.value == 40182) {
        if (nCarteira != glb_nCarteiraCOD)
            chkNossoNumero();
    }

}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked() {

    // Carta de Anuencia
    if ((selReports.value == 40164) && (glb_sCaller == 'S'))
        cartaAnuencia();

        //Nota Debito
    else if ((selReports.value == 40162) && (glb_sCaller == 'S'))
        notaDebito();

        // Fluxo de Caixa    
    else if ((selReports.value == 40183) && (glb_sCaller == 'PL')) {
        if (checkDataCoerency())
            fluxoCaixa();
        else
            return true;
    }
        // Duplicatas    
    else if ((selReports.value == 40181) && (glb_sCaller == 'S'))
        duplicata();
        // Boleto de cobranca
    else if ((selReports.value == 40182) && (glb_sCaller == 'S'))
        boleto();
        // Vendas Recebidas = Descontos Concedidos
    else if ((selReports.value == 40184) && (glb_sCaller == 'PL'))
        vendasRecebidas();
        // Campanhas
    else if ((selReports.value == 40186) && (glb_sCaller == 'PL'))
        Campanhas();
        // DepositoBancario
    else if ((selReports.value == 40195) && (glb_sCaller == 'S'))
        DepositoBancario();
    else
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);

    return true;
}

/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID() {
    var i, coll, attr, currRep;

    attr = null;
    currRep = 0;

    currRep = selReports.value;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (attr == currRep)
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr() {
    var i, coll, attr, retVal;

    attr = null;
    retVal = 0;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (coll[i].style.visibility == 'visible') {
                retVal = attr;
                break;
            }
        }
    }
    return retVal;
}

/********************************************************************
Ajusta os elementos labels e checkbox do divFluxoCaixa
********************************************************************/
function adjustDivFluxoCaixa() {
    adjustElementsInForm([['lblEstado', 'selEstado', 13, 1, -10],
						  ['lblSituacao', 'selSituacao', 12, 1],
						  ['lblForma', 'selForma', 7, 1],
						  ['lblVendedor', 'selVendedor', 20, 1],
						  ['lblEmpresaFluxoCaixa', 'selEmpresaFluxoCaixa', 13, 1],
						  ['lblContas', 'selContas', 13, 2, -10, 126],
						  ['lblOrdem', 'selOrdem', 12, 2],
                          ['lblMoeda', 'selMoeda', 8, 2],
                          ['lblEmpresa', 'chkEmpresa', 3, 2],
                          ['lblInadimplencia', 'chkInadimplencia', 3, 2],
                          ['lblTaxaAtualizacao', 'chkTaxaAtualizacao', 3, 2],
                          ['lblDadosSeguradora', 'chkDadosSeguradora', 3, 2],
                          ['lblInicio', 'txtInicio', 10, 3, -10],
                          ['lblFim', 'txtFim', 10, 3],
                          ['lbldtPosicao', 'txtdtPosicao', 10, 3],
                          ['lblFormatoSolicitacao', 'selFormatoSolicitacao', 10, 3],
                          ['lblFiltro', 'txtFiltro', 56, 4, -10, 3]], null, null, true);

    var lstHeight = 150;
    var twidth = 106;
    var tleft = 449;
    var twidth2 = 557;
    //var lstHeight = 166;
    selEstado.style.height = lstHeight;
    selSituacao.style.height = lstHeight;
    selForma.style.height = lstHeight;
    selVendedor.style.height = lstHeight;
    //selPadrao.style.height = lstHeight;

    selEmpresaFluxoCaixa.style.width = twidth;
    selEmpresaFluxoCaixa.style.left = tleft;

    txtdtPosicao.value = glb_dCurrDate;

    txtFiltro.style.width = twidth2;
}

function adjustDivBoletoCobranca() {
    var nCarteira = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
	    'dsoSup01.recordset[' + '\'' + 'Carteira' + '\'' + '].value');

    var nNossoNumero = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
        'dsoSup01.recordset[' + '\'' + 'NossoNumero' + '\'' + '].value');

    if ((nNossoNumero != '') && (nNossoNumero != null) && (!(isNaN(nNossoNumero))))
        txtNossoNumero.value = nNossoNumero;

    adjustElementsInForm([['lblNossoNumero', 'txtNossoNumero', 12, 1, -10, -10]],
                          null, null, true);

    if (nCarteira != 18) {
        lblNossoNumero.style.visibility = 'hidden';
        txtNossoNumero.style.visibility = 'hidden';
        txtNossoNumero.disabled = true;
    }
}

function adjustDivNotaDebito() {
    var nFormaPagamento = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
	    'dsoSup01.recordset[' + '\'' + 'FormaPagamentoID' + '\'' + '].value');


    adjustElementsInForm([['lblBanco', 'selBancos', 30, 1, -10, -10]],
                          null, null, true);

    if (nFormaPagamento != 1033) {
        lblBanco.style.visibility = 'hidden';
        selBancos.style.visibility = 'hidden';
        selBancos.disabled = true;
    }
    else {
        setConnection(dsoBancos);

        dsoBancos.SQL = 'SELECT dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) AS Bancos, b.RelPesContaID AS RelPesContaID, c.Nome, ' +
						'c.Agencia + (CASE WHEN c.AgenciaDV IS NOT NULL THEN \'-\' + c.AgenciaDV ELSE \'\' END) AS Agencia, ' +
						'b.Conta +	(CASE WHEN b.ContaDV IS NOT NULL THEN \'-\' + b.ContaDV ELSE SPACE(0) END) AS ContaCorrente ' +
						'FROM RelacoesPessoas a WITH(NOLOCK) ' +
							'INNER JOIN RelacoesPessoas_Contas b WITH(NOLOCK) ON a.RelacaoID = b.RelacaoID ' +
							'INNER JOIN Pessoas c WITH(NOLOCK) ON a.ObjetoID = c.PessoaID ' +
						'WHERE a.TipoRelacaoID = 24 AND a.SujeitoID = ' + glb_nEmpresaID + ' ' +
						'ORDER BY Bancos';

        dsoBancos.ondatasetcomplete = adjustDivNotaDebito_DSC;
        dsoBancos.refresh();
    }
}

function adjustDivNotaDebito_DSC() {
    clearComboEx(['selBancos']);

    while (!dsoBancos.recordset.EOF) {
        optionStr = dsoBancos.recordset['Bancos'].value;
        optionValue = dsoBancos.recordset['RelPesContaID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selBancos.add(oOption);
        dsoBancos.recordset.MoveNext();
    }
}

function adjustdivCartaAnuencia() {
    var nEmpresaLogada = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
	    'dsoSup01.recordset[' + '\'' + 'EmpresaID' + '\'' + '].value');

    adjustElementsInForm([['lblNomeResponsavel', 'selNomeResponsavel', 30, 1, -10]], null, null, true);
}

function adjustDivVendasRecebidas() {
    adjustElementsInForm([['lblTipo', 'selTipo', 15, 1, -10],
                          ['lblDataInicio3', 'txtDataInicio3', 10, 1],
                          ['lblDataFim3', 'txtDataFim3', 10, 1],
                          ['lblFiltro2', 'txtFiltro2', 47, 2, -10]], null, null, true);


    txtFiltro2.style.width = 557;
    selTipo.onchange = selTipo_onchange;
    txtDataInicio3.maxLength = 10;
    txtDataFim3.maxLength = 10;


}

function selTipo_onchange() {
    setLabelOfControl(lblTipo, (selTipo.value == 0 ? '' : selTipo.value));
}

function adjustDivSerasa() {
    adjustElementsInForm([['lblDataInicio', 'txtDataInicio', 10, 1, -10],
                          ['lblDataFim', 'txtDataFim', 10, 1],
                          ['lblRelatoCorrecao', 'chkRelatoCorrecao', 3, 1]], null, null, true);

    txtDataInicio.maxLength = 10;
    txtDataFim.maxLength = 10;

    txtDataInicio.value = glb_dCurrDate;
    txtDataFim.value = glb_dCurrDate;
}

function adjustCampanhas() {
    adjustElementsInForm([['lblCampanhas', 'selCampanhas', 8, 1, -10],
						  ['lblPendentes', 'chkPendentes', 3, 1],
						  ['lblMoedaSistema', 'chkMoedaSistema', 3, 1],
						  ['lblResumido', 'chkResumido', 3, 1],
						  ['lblDataInicio2', 'txtDataInicio2', 10, 1],
                          ['lblDataFim2', 'txtDataFim2', 10, 1],
						  ['lblEmpresaCampanha', 'selEmpresaCampanha', 18, 2, -10],
						  ['lblFabricante', 'selFabricante', 18, 2],
						  ['lblResponsavelID', 'selResponsavelID', 18, 2],
						  ['lblFiltro3', 'txtFiltro3', 47, 3, -10],
                          ['lblFormatoSolicitacao2', 'selFormatoSolicitacao2', 10, 4, -10]], null, null, true);

    chkPendentes.checked = true;
    chkMoedaSistema.checked = true;


    chkPendentes.onclick = loadCmbsCampanhas;
    selCampanhas.onchange = loadCmbsCampanhas;

    txtFiltro3.style.width = 557;

    txtDataInicio2.maxLength = 10;
    txtDataFim2.maxLength = 10;

    txtDataInicio2.value = '';
    txtDataFim2.value = '';
}

function adjustdivDepositoBancario() {
    var empresaID = getCurrEmpresaData();

    // Se pa�s EUA
    if (empresaID[1] != 167) {

        //O IF INCLUI NO MODALPRINT.ASP UM SELECT PARA ESCOLHER O FORMATO
        if (empresaID[1] != 167) {
            var divDepositoBancario = document.getElementById("divDepositoBancario").innerHTML;

            document.getElementById("divDepositoBancario").innerHTML = divDepositoBancario +
                '<p id="lblFormatoDepositoBancario"name="lblFormatoDepositoBancario" class="lblGeneral">Formato</p> ' +
                '<select id="selFormatoDepositoBancario"name="selFormatoDepositoBancario"class="fldGeneral"> ' +
                    ' <option value=1>PDF</option> ' +
                    ' <option value=2>Excel</option> ' +
                ' </select> ';
        }

        adjustElementsInForm([['lblFormatoDepositoBancario', 'selFormatoDepositoBancario', 8, 1, -6, -2]],
                              null, null, true);
    }
}

/********************************************************************
Restringe quantidade de caracteres digitados, coloca
mascara para digitacao em campos numericos,
seleciona conteudo do campo quando em foco etc
********************************************************************/
function setMaskAndLengthToInputs() {
    txtInicio.maxLength = 10;
    txtFim.maxLength = 10;
    txtdtPosicao.maxLength = 10;

    txtInicio.onfocus = selFieldContent;
    txtFim.onfocus = selFieldContent;
    txtdtPosicao.onfocus = selFieldContent;

    txtNossoNumero.setAttribute('thePrecision', 12, 1);
    txtNossoNumero.setAttribute('theScale', 0, 1);
    txtNossoNumero.setAttribute('verifyNumPaste', 1);
    //txtNossoNumero.onkeypress = verifyNumericEnterNotLinked;
    txtNossoNumero.onkeyup = chkNossoNumero;
    //txtNossoNumero.setAttribute('minMax', new Array(0, 999999999999), 1);
    txtNossoNumero.maxLength = 12;
    txtNossoNumero.onfocus = selFieldContent;
}

/********************************************************************
Trima o conteudo dos controles input type text e
verifica coerencia dos seus dados.

Parametros - nenhum

Retorno - true se todos os campos atendem, false se pelo menos
          um nao atende
********************************************************************/
function checkDataCoerency() {
    // campo txtInicio e txtFim devem ser trimados
    txtInicio.value = trimStr(txtInicio.value);
    txtFim.value = trimStr(txtFim.value);
    txtdtPosicao.value = trimStr(txtdtPosicao.value);

    // verifica os campos datas
    if (txtInicio.value != '') {
        if (!chkDataEx(txtInicio.value)) {
            if (window.top.overflyGen.Alert('Data inicial inv�lida') == 0)
                return null;

            lockControlsInModalWin(false);
            txtInicio.focus();
            return false;
        }
    }
    if (txtFim.value != '') {
        if (!chkDataEx(txtFim.value)) {
            if (window.top.overflyGen.Alert('Data final inv�lida') == 0)
                return null;
            lockControlsInModalWin(false);
            txtFim.focus();
            return false;
        }
    }

    if ((txtdtPosicao.value == '') || (!chkDataEx(txtdtPosicao.value))) {
        if (window.top.overflyGen.Alert('Data posi��o inv�lida') == 0)
            return null;
        lockControlsInModalWin(false);
        txtdtPosicao.focus();
        return false;
    }

    return true;
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights() {
    if (selReports.options.length != 0)
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;

    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else
            coll[i].style.visibility = 'hidden';
    }

    // desabilita o combo de relatorios
    selReports.disabled = true;

    // desabilita o botao OK
    btnOK.disabled = true;

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];

    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);

    elem = document.getElementById('selReports');

    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);

    // a altura livre    
    modHeight -= topFree;

    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt';
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';

    // acrescenta o elemento
    window.document.body.appendChild(elem);

    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);

    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;

    return null;
}

/********************************************************************
Funcao de carregamento de combos estaticos startLocalCmbs()
********************************************************************/
function startLocalCmbs() {
    var strPas = '?nEmpresaID=' + escape(glb_nEmpresaID);

    dsoEstaticCmbs.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/fillcmbsmodal.aspx' + strPas;
    dsoEstaticCmbs.ondatasetcomplete = dsoEstaticCmbs_DSC;
    dsoEstaticCmbs.Refresh();
}

/********************************************************************
retorno da Funcao de carregamento de combos estaticos startLocalCmbs()
********************************************************************/
function dsoEstaticCmbs_DSC() {
    var optionStr, optionValue;
    var aCmbsDynamics = [selContas, selMoeda, selForma, selVendedor, selTipo, selEstado, selSituacao, selEmpresaFluxoCaixa];
    var nIndice = 0;

    clearComboEx(['selContas', 'selMoeda', 'selForma', 'selVendedor', 'selTipo', 'selEstado', 'selSituacao', 'selEmpresaFluxoCaixa']);

    if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF)))
        dsoEstaticCmbs.recordset.MoveFirst();
    else {
        // mostra a janela modal com o arquivo carregado
        showExtFrame(window, true);

        window.focus();

        if (selReports.disabled == false)
            selReports.focus();
        else if (btnCanc.disabled == false)
            btnCanc.focus();

        return null;
    }

    var nContextoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
	    'getCmbCurrDataInControlBar(' + '\'' + 'sup' + '\'' + ', 1)[1]');

    // Pagar
    if (nContextoID == 9112)
        nContextoID = 1002;
        // Receber
    else if (nContextoID == 9111)
        nContextoID = 1001;

    while (!dsoEstaticCmbs.recordset.EOF) {
        nIndice = dsoEstaticCmbs.recordset['Indice'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = dsoEstaticCmbs.recordset['fldName'].value;
        oOption.value = dsoEstaticCmbs.recordset['fldID'].value;

        // Combo de Contas
        if (nIndice == 0) {
            if (dsoEstaticCmbs.recordset['fldID'].value == nContextoID)
                oOption.selected = true;
        }
            // Combo de Forma
        else if (nIndice == 2) {
            oOption.abreviacao = dsoEstaticCmbs.recordset['Abreviacao'].value;
        }
            // Combo de Estado
        else if (nIndice == 5) {
            // Aberto
            if (dsoEstaticCmbs.recordset['fldID'].value == 41)
                oOption.selected = true;

            oOption.abreviacao = dsoEstaticCmbs.recordset['Abreviacao'].value;
        }
            // Combo de Situacao
        else if (nIndice == 6) {
            // Normal
            if (dsoEstaticCmbs.recordset['fldID'].value == 1011)
                oOption.selected = true;

            oOption.abreviacao = dsoEstaticCmbs.recordset['Abreviacao'].value;
        }

        else if (nIndice == 7) {
            if (glb_nEmpresaID == dsoEstaticCmbs.recordset['fldID'].value)
                oOption.selected = true;

            oOption.abreviacao = dsoEstaticCmbs.recordset['Abreviacao'].value;
        }

        // Combo Empresas
        /*else if (nIndice == 7)
        {
	        oOption.abreviacao = dsoEstaticCmbs.recordset['Abreviacao'].value;				
		}*/

        aCmbsDynamics[nIndice].add(oOption);
        dsoEstaticCmbs.recordset.MoveNext();
    }

    if (selForma.options.length > 0)
        selForma.selectedIndex = 0;

    if (selVendedor.options.length > 0)
        selVendedor.selectedIndex = 0;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    window.focus();

    if (selReports.disabled == false)
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();

    return null;
}

function qtdSelectedInCmb(cmb) {
    var i = 0;
    var nRetVal = 0;

    for (i = 0; i < cmb.options.length; i++)
        if (cmb.options(i).selected)
            nRetVal++;

    return nRetVal;
}

/********************************************************************
Impressao do relatorio de Fluxo de Caixa (ID=40183)
Financeiro->Recebimento/Pagamentos->Modal Print
Relatorio: 40183 -> Contas a Pagar/Receber
********************************************************************/
function fluxoCaixa() {
    var nUserID;
    var sEstados = '';
    var sSituacao = '';
    var sFormaPagamento = '';
    var sVendedor = '';
    var sdtInicio = '';
    var sdtFim = '';
    var sEmpresas = '';
    var sDataPosicao = '';
    var formato = selFormatoSolicitacao.value; //Excel
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    nUserID = window.top.userID;

    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);

    sEstados = buildSQLStringByList(selEstado, 'a.EstadoID');
    sSituacao = buildSQLStringByList(selSituacao, 'a.SituacaoCobrancaID');
    sFormaPagamento = buildSQLStringByList(selForma, 'a.FormaPagamentoID');
    sVendedor = buildSQLStringByList(selVendedor, 'a.ProprietarioID');

    if (trimStr(txtInicio.value) != '')
        sdtInicio = '\'' + putDateInMMDDYYYY2(trimStr(txtInicio.value)) + '\'';
    else
        sdtInicio = 'null';

    if (trimStr(txtFim.value) != '')
        sdtFim = '\'' + putDateInMMDDYYYY2(trimStr(txtFim.value)) + '\'';
    else
        sdtFim = 'null';

    sDataPosicao = '\'' + putDateInMMDDYYYY2(trimStr(txtdtPosicao.value)) + '\'';

    // codigo Combo empresa multiple
    sEmpresas = EmpresasIn();


    // Monta string de parametros
    var strParams = '';

    for (i = 0; i < divFluxoCaixa.children.length; i++) {
        elem = divFluxoCaixa.children[i];

        if (elem.id != "selFormatoSolicitacao") {
            // Campos Textos
            if (((elem.tagName).toUpperCase() == 'INPUT') && ((elem.type).toUpperCase() == 'TEXT') &&
                 ((elem.id).toUpperCase() != 'TXTFILTRO')) {
                if ((elem.value != null) && (trimStr(elem.value) != '')) {
                    strParams += (elem.previousSibling).innerText;
                    strParams += ':';
                    strParams += trimStr(elem.value);
                    strParams += '   ';
                }
            }
            else if ((elem.tagName).toUpperCase() == 'SELECT') {
                if (elem.id.toUpperCase() == 'SELCONTAS')
                    continue;

                if (elem.selectedIndex != -1) {
                    if ((elem.multiple == false) ||
                        ((elem.id.toUpperCase() == 'SELVENDEDOR') && (qtdSelectedInCmb(elem) == 1))) {
                        if ((elem.options(elem.selectedIndex).innerText != null) &&
                             (trimStr(elem.options(elem.selectedIndex).innerText) != '')) {
                            strParams += (elem.previousSibling).innerText;
                            strParams += ':';
                            strParams += trimStr(elem.options(elem.selectedIndex).innerText);
                            strParams += '   ';
                        }
                    }
                    else if (qtdSelectedInCmb(elem) >= 1) {
                        bPrimeiro = true;
                        for (j = 0; j < elem.options.length; j++) {
                            if (elem.options(j).selected) {
                                if ((elem.options(j).innerText != null) &&
                                    (trimStr(elem.options(j).innerText) != '')) {
                                    if (bPrimeiro) {
                                        bPrimeiro = false;
                                        strParams += (elem.previousSibling).innerText;
                                        strParams += ':';
                                        strParams += trimStr(elem.options(j).abreviacao);
                                    }
                                    else
                                        strParams += ' ' + trimStr(elem.options(j).abreviacao);
                                }
                            }
                        }

                        if (!bPrimeiro)
                            strParams += '   ';
                    }
                }
            }
        }
    }

    // Trima a string de parametros
    if (strParams != '')
        strParams = trimStr(strParams);

    var sLinguaLogada = getDicCurrLang();

    var strParameters = "RelatorioID=" + selReports.value + "&nEmpresaLogada=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&formato=" + formato + "&nEmpresaLogadaIdioma=" + aEmpresaData[8] +
                        "&nUserID=" + nUserID + "&dirA1=" + dirA1 + "&dirA2=" + dirA2 + "&selContasValue=" + selContas.value + "&selContasSelectedIndex=" + selContas.selectedIndex +
                        "&selOrdem=" + selOrdem.value + "&selMoeda=" + selMoeda.value + "&sEstados=" + sEstados + "&sSituacao=" + sSituacao + "&sFormaPagamento=" + sFormaPagamento +
                        "&sVendedor=" + sVendedor + "&sdtInicio=" + sdtInicio + "&sdtFim=" + sdtFim + "&txtFiltro=" + txtFiltro.value + "&chkEmpresa=" + chkEmpresa.checked +
                        "&chkTaxaAtualizacao=" + chkTaxaAtualizacao.checked + "&chkInadimplencia=" + chkInadimplencia.checked + "&sDataPosicao=" + sDataPosicao + "&sEmpresas=" + sEmpresas +
                        "&DATE_SQL_PARAM=" + DATE_SQL_PARAM + "&chkDadosSeguradora=" + chkDadosSeguradora.checked + "&strParams=" + strParams + "&sLinguaLogada=" + sLinguaLogada;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/Reports_financeiro.aspx?" + strParameters;

}


function buildSQLStringByList(cmbID, fldName) {
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    var retVal = '';
    var i = 0;

    for (i = 0; i < cmbID.options.length; i++) {
        if (cmbID.options(i).selected) {
            if (cmbID.options(i).value != 0) {
                if (cmbID.options(i).value == 1019 && aEmpresaData[0] == 7) { //TRATATIVA PARA N�O COLOCAR PEFIN NO SELECT
                    retVal += (retVal == '' ? '1019' : '');
                }
                else {
                    retVal += (retVal == '' ? '' : ',');
                    retVal += cmbID.options(i).value;
                }
            }
        }
    }

    if (retVal != '')
        retVal = ' AND ' + fldName + ' IN(' + retVal + ') ';

    return retVal;
}

/********************************************************************
Criado pelo programador
Impressao de duplicatas

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function duplicata() {
    var nEmpresaLogada = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var nFinanceiroID = glb_nFinanceiroID;
    var formato = 1;

    var strParameters = "RelatorioID=" + selReports.value + "&nEmpresaLogada=" + nEmpresaLogada[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia +
                        "&nFinanceiroID=" + nFinanceiroID + "&nEmpresaLogadaIdioma=" + nEmpresaLogada[8];

    //var frameReport = document.getElementById("frmReport");

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/Reports_financeiro.aspx?" + strParameters + "&via2=false";


    glbContRel = 1;
}

function oPrinter_OnPrintFinish() {
    winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
}

/********************************************************************
Criado pelo programador
Impressao de boleto

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function boleto() {
    var nCarteira = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
        'dsoSup01.recordset[' + '\'' + 'Carteira' + '\'' + '].value');

    if (nCarteira == glb_nCarteiraCOD)
        printBoletoCOD();
    else if (nCarteira == glb_nCarteiraCOB)
        printBoletoCOB();
    else
        return null;
}

function printBoletoCOD() {
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, 'BLOQUETO');

    return false;
}

function printBoletoCOB() {
    var bNossoNumeroValido = true;
    // Checa se o nosso numero � valido
    if (txtNossoNumero.value == '') {
        if (window.top.overflyGen.Alert('Digite o Nosso N�mero.') == 0)
            return false;
        bNossoNumeroValido = false;
    }
    else if ((txtNossoNumero.value).length < 12) {
        if (window.top.overflyGen.Alert('N�mero insuficiente de d�gitos.') == 0)
            return false;
        bNossoNumeroValido = false;
    }
    else {
        txtNossoNumero.value = (txtNossoNumero.value).toUpperCase();
        if (!verificaDigito(txtNossoNumero.value, 8, 1)) {
            if (window.top.overflyGen.Alert('D�gito Inv�lido.') == 0)
                return false;
            bNossoNumeroValido = false;
        }
    }

    if (!bNossoNumeroValido) {
        restoreModalPrint();
        if (txtNossoNumero.disabled == false) {
            window.focus();
            txtNossoNumero.focus();
        }
        return null;
    }

    var nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
        'dsoSup01.recordset[' + '\'' + 'FinanceiroID' + '\'' + '].value');

    var strPas = '?';
    strPas += 'nFinanceiroID=' + escape(nFinanceiroID);
    strPas += '&sNossoNumero=' + escape(txtNossoNumero.value);

    dsoUpdateNossoNumero.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/updatenossonumero.aspx' + strPas;
    dsoUpdateNossoNumero.ondatasetcomplete = dsoUpdateNossoNumero_DSC;
    dsoUpdateNossoNumero.Refresh();
}

function dsoUpdateNossoNumero_DSC() {
    setConnection(dsoPrint01);

    dsoPrint01.SQL = 'SELECT STR(a.FinanceiroID,10) as FinanceiroID, a.dtEmissao, STR(a.Valor,14,2) as Valor, a.Duplicata, a.dtVencimento, d.Nome, d.PessoaID, ' +
                        'RTRIM(ISNULL(e.Endereco,SPACE(0))) + ' + '\'' + ' ' + '\'' + ' + RTRIM(ISNULL(e.Numero,SPACE(0))) + ' + '\'' + ' ' + '\'' + ' + RTRIM(ISNULL(e.Complemento,SPACE(0))) + ' + '\'' + ' ' + '\'' + ' + RTRIM(ISNULL(e.Bairro,SPACE(0)))  as Endereco, ' +
                        'e.CEP, f.Localidade as Cidade, g.CodigoLocalidade2 as UF, ISNULL(dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 101, 111, 0),' + '\'' + ' ' + '\'' + ') as CNPJCPF, ' +
                        'i.DiasBase as DiasBase, j.TaxaMultaAtraso as TaxaMultaAtraso ' +
                     'FROM Financeiro a WITH(NOLOCK), Pessoas d WITH(NOLOCK), Pessoas_Enderecos e WITH(NOLOCK), Localidades f WITH(NOLOCK), Localidades g WITH(NOLOCK), ' +
                         'RelacoesPesRec i WITH(NOLOCK), RelacoesPesRec_Financ j WITH(NOLOCK) ' +
                     'WHERE a.financeiroid = ' + glb_nFinanceiroID + ' AND a.PessoaID=d.PessoaID and ' +
                        'a.PessoaID=e.PessoaID AND e.Ordem=1 AND e.CidadeID=f.LocalidadeID AND e.UFID=g.LocalidadeID AND ' +
                        'i.ObjetoID=999 AND i.TipoRelacaoID=12 AND i.SujeitoID=' + glb_nEmpresaID + ' AND i.RelacaoID=j.RelacaoID ' +
                        'AND a.MoedaID=j.MoedaID';

    dsoPrint01.ondatasetcomplete = boleto_DSC;
    dsoPrint01.Refresh();
}

function boleto_DSC() {
    // seta os parametros de inicializacao da impressora
    oPrinter.DefaultPrinterNFParams(8, 32, 10, 'N', false);
    oPrinter.ParallelPort = 1;
    oPrinter.ResetPrinter();
    oPrinter.ResetTextForPrint();

    oPrinter.MoveToNewLine(2);
    oPrinter.SendTextForPrint(113, padL(dsoPrint01.recordset['FinanceiroID'].value, 10, ' '), 'N', false);
    oPrinter.SendTextForPrint(156, dsoPrint01.recordset['dtVencimento'].value, 'N', false);
    oPrinter.MoveToNewLine(4);
    oPrinter.SendTextForPrint(12, dsoPrint01.recordset['dtEmissao'].value, 'N', false);
    oPrinter.SendTextForPrint(42, dsoPrint01.recordset['Duplicata'].value, 'N', false);
    oPrinter.SendTextForPrint(114, dsoPrint01.recordset['dtEmissao'].value, 'N', false);
    oPrinter.MoveToNewLine(2);
    oPrinter.SendTextForPrint(145, padL(dsoPrint01.recordset['Valor'].value, 14, ' '), 'N', false);
    oPrinter.MoveToNewLine(3);

    var nDiasBase = dsoPrint01.recordset['DiasBase'].value;
    var nPercentualAtraso = dsoPrint01.recordset['TaxaMultaAtraso'].value;
    var nJuros = padNumReturningStr(roundNumber((parseFloat(dsoPrint01.recordset['Valor'].value) / nDiasBase) * nPercentualAtraso / 100, 2), 2);

    oPrinter.SendTextForPrint(15, 'Juros por dia de atraso R$ ' + nJuros, 'N', false);
    oPrinter.MoveToNewLine(2);
    oPrinter.SendTextForPrint(15, 'Protesto no 5o dia �til ap�s vencimento', 'N', false);
    oPrinter.MoveToNewLine(7);
    oPrinter.SendTextForPrint(15, (dsoPrint01.recordset['Nome'].value).toUpperCase(), 'N', false);
    oPrinter.SendTextForPrint(125, dsoPrint01.recordset['PessoaID'].value, 'N', false);
    oPrinter.MoveToNewLine(1);
    oPrinter.SendTextForPrint(15, dsoPrint01.recordset['Endereco'].value, 'N', false);
    oPrinter.MoveToNewLine(1);
    oPrinter.SendTextForPrint(15, dsoPrint01.recordset['CEP'].value + ' ' + dsoPrint01.recordset['Cidade'].value + ' ' + dsoPrint01.recordset['UF'].value, 'N', false);
    oPrinter.SendTextForPrint(125, dsoPrint01.recordset['CNPJCPF'].value, 'N', false);

    oPrinter.FormFeed();
    oPrinter.PrintText();

    glb_BoletoPrinted = true;
}

function chkNossoNumero() {
    var bHabilita = true;
    btnOK.disabled = true;

    if (glb_nFormaPagamentoID != 1035)
        bHabilita = false;
    else if ((txtNossoNumero.value == null) || (txtNossoNumero.value == ''))
        bHabilita = false;
    else if (txtNossoNumero.value.length < 12)
        bHabilita = false;
    else {
        txtNossoNumero.value = (txtNossoNumero.value).toUpperCase();
        if (!verificaDigito(txtNossoNumero.value, 8, 1))
            bHabilita = false;
    }

    btnOK.disabled = !bHabilita;

    return true;
}


/****************************************************************************
Impressao do relatorio de Vendas Recebidas = Descontos Concedidos (ID=40184)
*****************************************************************************/
function vendasRecebidas() {
    var dtInicio = null;
    var dtFim = null;
    var nEmpresaLogada = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var formato = 2; //EXCEL

    // Analiza o campo txtDataInicio3
    txtDataInicio3.value = trimStr(txtDataInicio3.value);

    if (!((txtDataInicio3.value == null) || (txtDataInicio3.value == ''))) {
        if (chkDataEx(txtDataInicio3.value)) {
            dtInicio = normalizeDate_DateTime(txtDataInicio3.value, true);

            if (dtInicio == null)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
                return null;

            winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
            return null;
        }
    }

    // Analiza o campo txtDataInicio3
    txtDataFim3.value = trimStr(txtDataFim3.value);

    if (!((txtDataFim3.value == null) || (txtDataFim3.value == ''))) {
        if (chkDataEx(txtDataFim3.value)) {
            dtFim = normalizeDate_DateTime(txtDataFim3.value, true);

            if (dtFim == null)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
                return null;

            winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
            return null;
        }
    }

    var strParameters = "RelatorioID=" + selReports.value + "&nEmpresaLogada=" + nEmpresaLogada[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&formato=" + formato +
                        "&dtInicio=" + dtInicio + "&dtFim=" + dtFim + "&selTipoValue=" + selTipo.value + "&txtFiltro2=" + txtFiltro2.value + "&selTipoSelectedIndex=" + selTipo.selectedIndex +
                        "&selTipoInnerText=" + selTipo[selTipo.selectedIndex].innerText + "&txtDataInicio3=" + txtDataInicio3.value + "&txtDataFim3=" + txtDataFim3.value;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/Reports_financeiro.aspx?" + strParameters;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
}

/********************************************************************
Funcao de carregamento de combos estaticos startLocalCmbs()
********************************************************************/
function loadCmbsCampanhas() {
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    lockControlsInModalWin(true);
    glb_nDSOsCampanhas = 2;

    dsoCmbsCampanhas.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/CombosFabEmp.aspx';
    dsoCmbsCampanhas.ondatasetcomplete = cmbsCampanhas_DSC;
    dsoCmbsCampanhas.Refresh();

    dsoCmbsCampanhas2.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/CombosResp.aspx';
    dsoCmbsCampanhas2.ondatasetcomplete = cmbsCampanhas_DSC;
    dsoCmbsCampanhas2.Refresh();
}

/********************************************************************
retorno da Funcao de carregamento de combos estaticos startLocalCmbs()
********************************************************************/
function cmbsCampanhas_DSC() {
    glb_nDSOsCampanhas--;

    if (glb_nDSOsCampanhas > 0)
        return null;

    var optionStr, optionValue;
    var aDSOsDunamics = [dsoCmbsCampanhas, dsoCmbsCampanhas, dsoCmbsCampanhas2];
    var aCmbsDynamics = [selEmpresaCampanha, selFabricante, selResponsavelID];
    var nIndice = 0;

    clearComboEx(['selEmpresaCampanha', 'selFabricante', 'selResponsavelID']);

    if (!((dsoCmbsCampanhas.recordset.BOF) && (dsoCmbsCampanhas.recordset.EOF)))
        dsoCmbsCampanhas.recordset.MoveFirst();
    else {
        lockControlsInModalWin(false);
        // mostra a janela modal com o arquivo carregado
        showExtFrame(window, true);

        window.focus();

        if (selReports.disabled == false)
            selReports.focus();
        else if (btnCanc.disabled == false)
            btnCanc.focus();

        return null;
    }

    for (var icount = 0; icount <= 2; icount++) {
        if (!(aDSOsDunamics[icount].recordset.BOF && aDSOsDunamics[icount].recordset.EOF))
            aDSOsDunamics[icount].recordset.moveFirst();

        while (!aDSOsDunamics[icount].recordset.EOF) {
            if (icount == 0) {
                if (aDSOsDunamics[icount].recordset['Indice'].value != 1) {
                    aDSOsDunamics[icount].recordset.MoveNext();
                    continue;
                }
            }
            else if (icount == 1) {
                if (aDSOsDunamics[icount].recordset['Indice'].value != 2) {
                    aDSOsDunamics[icount].recordset.MoveNext();
                    continue;
                }
            }

            optionStr = aDSOsDunamics[icount].recordset['Fantasia'].value;
            optionValue = aDSOsDunamics[icount].recordset['PessoaID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[icount].add(oOption);
            aDSOsDunamics[icount].recordset.MoveNext();
        }
    }

    // mostra a janela modal com o arquivo carregado
    lockControlsInModalWin(false);

    window.focus();

    if (selReports.disabled == false)
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();

    return null;
}


function Campanhas() {
    var nEmpresaLogada = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var strParams = '';
    var _txtDataInicio2 = '';
    var _txtDataFim2 = '';
    var _txtFiltro3 = '';
    var sLinguaLogada = getDicCurrLang();

    nUserID = window.top.userID;

    strParams = '';

    if (selCampanhas.value == 0)
        strParams += 'Campanhas   ';
    else
        strParams += selCampanhas.options[selCampanhas.selectedIndex].innerText + '   ';

    if (chkPendentes.checked)
        strParams += 'Pendentes';

    if (chkMoedaSistema.checked)
        strParams += '    US$';
    else
        strParams += '    R$';

    if (selEmpresaCampanha.selectedIndex > 0)
        strParams += '    Empresa: ' + selEmpresaCampanha.options[selEmpresaCampanha.selectedIndex].innerText + ' ';

    if (selFabricante.selectedIndex > 0)
        strParams += '    Fabricante: ' + selFabricante.options[selFabricante.selectedIndex].innerText + ' ';

    if (trimStr(txtDataInicio2.value) != '')
        strParams += '    In�cio: ' + txtDataInicio2.value;

    if (trimStr(txtDataFim2.value) != '')
        strParams += ' Fim: ' + txtDataFim2.value;

    if (trimStr(txtDataInicio2.value) != '')
        _txtDataInicio2 = '\'' + putDateInMMDDYYYY2(trimStr(txtDataInicio2.value)) + '\'';
    else
        _txtDataInicio2 = 'NULL';

    if (trimStr(txtDataFim2.value) != '')
        _txtDataFim2 = '\'' + putDateInMMDDYYYY2(trimStr(txtDataFim2.value)) + '\'';
    else
        _txtDataFim2 = 'NULL';

    if (trimStr(txtFiltro3.value) != '')
        _txtFiltro3 = '\'' + txtFiltro3.value + '\'';
    else
        _txtFiltro3 = 'NULL';

    var strParameters = "RelatorioID=" + selReports.value + "&nEmpresaLogada=" + nEmpresaLogada[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&formato=" + selFormatoSolicitacao2.value +
                    "&selEmpresaCampanha=" + (selEmpresaCampanha.value == 0 ? 'NULL' : selEmpresaCampanha.value) + "&selFabricante=" + (selFabricante.value == 0 ? 'NULL' : selFabricante.value) +
                    "&selCampanhas=" + (selCampanhas.value == 0 ? 'NULL' : selCampanhas.value) + "&selResponsavelID=" + (selResponsavelID.value == 0 ? 'NULL' : selResponsavelID.value) +
                    "&chkMoedaSistema=" + (chkMoedaSistema.checked ? 1 : 0) + "&chkPendentes=" + (chkPendentes.checked ? 1 : 0) + "&txtDataInicio2=" + _txtDataInicio2 + "&txtDataFim2=" + _txtDataFim2 +
                    "&txtFiltro3=" + _txtFiltro3 + "&strParams=" + strParams + "&chkResumido=" + chkResumido.checked + "&sLinguaLogada=" + sLinguaLogada;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/Reports_financeiro.aspx?" + strParameters;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);



}

function notaDebito() {
    var sBanco = '';
    var sAgencia = '';
    var sContaCorrente = '';
    var nDsoRelPesContaID;
    var nSelRelPesContaID;

    var nEmpresaLogada = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
    'dsoSup01.recordset[' + '\'' + 'EmpresaID' + '\'' + '].value');

    var nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
	    'dsoSup01.recordset[' + '\'' + 'FinanceiroID' + '\'' + '].value');


    strParameters = "DATE_SQL_PARAM=" + DATE_SQL_PARAM + "&nFinanceiroID=" + nFinanceiroID + "&nEmpresaLogada=" + nEmpresaLogada +
                    "&RelatorioID=" + selReports.value + "&nSelRelPesContaID=" + selBancos.value;


    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/Reports_financeiro.aspx?" + strParameters + "&via2=false";



    glbContRel = 1;
}

function cartaAnuencia() {

    var nEmpresaLogada = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
    'dsoSup01.recordset[' + '\'' + 'EmpresaID' + '\'' + '].value');

    var nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
	    'dsoSup01.recordset[' + '\'' + 'FinanceiroID' + '\'' + '].value');


    strParameters = "DATE_SQL_PARAM=" + DATE_SQL_PARAM + "&nFinanceiroID=" + nFinanceiroID + "&nEmpresaLogada=" + nEmpresaLogada +
                    "&sNomeResponsavel=" + selNomeResponsavel.options[selNomeResponsavel.selectedIndex].getAttribute('Nome', 1) +
                    "&sCPF=" + selNomeResponsavel.options[selNomeResponsavel.selectedIndex].getAttribute('CPF', 1) + "&RelatorioID=" + selReports.value;


    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    // window.document.onreadystatechange = reports_onreadystatechange;
    // window.document.location = SYS_PAGESURLROOT + "/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/Reports_financeiro.aspx?" + strParameters + "&via2=false";

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/Reports_financeiro.aspx?" + strParameters + "&via2=false";

    glbContRel = 1;
}

/******************************
Funcao criada para quebrar linha de acordo com os parametros.
strToParse -> String a ser analizada para quebrar linha.
lenghtSnack -> Limite da linha
sArray -> Array onde � armazeda as linhas
maxLines -> Maximo de linhas
******************************/
function breakLine(strToParse, lenghtSnack, sArray, maxLines) {
    var sStr = '';
    var sStr2 = '';
    var nStart = 0;
    var nEnd = 0;
    var a = 0, b = 0, i = 0;

    if ((strToParse == '') || (strToParse == null))
        return null;

    if ((lenghtSnack <= 0) || (lenghtSnack == '') || (lenghtSnack == null))
        return null;

    sStr = strToParse;
    sStr2 = sStr;

    while (sStr2.length > 0) {
        sStr2 = sStr.substring(nStart, nStart + lenghtSnack);

        for (i = 0; i < sStr2.length; i++) {
            if ((sStr2.charCodeAt(i) == 13) && (sStr2.charCodeAt(i + 1) == 10)) {
                sArray[sArray.length] = sStr2.substring(0, i/*+1*/);

                if (sArray.length >= maxLines)
                    return;

                nStart += i + 2;
                a = 1;

                break;
            }
        }

        if (a == 0) {
            nEnd = lenghtSnack + 1;
            while ((sStr2.substring(nEnd, nEnd + 1) != ' ') && (sStr2.length >= lenghtSnack)) {
                nEnd--;
                if (nEnd == 0)
                    break;

            }
            if ((sStr2.length < lenghtSnack) && (sStr2.charCodeAt(0) == 32))
                b = 1;

            sArray[sArray.length] = sStr2.substring(0 + b, nEnd + 1);

            if (sArray.length >= maxLines)
                return;

            nStart += nEnd + 1;
            b = 0;
        }
        a = 0;
        sStr2 = sStr.substring(nStart, nStart + lenghtSnack);

    }
    return;
}

function DepositoBancario() {
    var nEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtRegistroID.value');
    var sLinguaLogada = getDicCurrLang();
    var Title = translateTerm(selReports.options[selReports.selectedIndex].innerText, null);
    var sEstado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtEstadoID.value');
    var dtData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtdtVencimento.value');
    var sMoeda = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selMoedaID.options(selMoedaID.selectedIndex).innerText');
    var nTotal = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtTotalPrincipal.value');

    if (nEmpresaData[1] == 167) {
        var formato = 1;
        var selPadrao = 1;
    }
    else {
        var formato = selFormatoDepositoBancario.value;
        var selPadrao = 0;
    }

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + nEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&nFinanceiroID=" + nFinanceiroID + "&sLinguaLogada=" + sLinguaLogada + "&sTitle=" + Title + "&nEmpresaData7=" + nEmpresaData[7] + "&nEmpresaData8=" + nEmpresaData[8] +
                        "&sSelPadrao=" + selPadrao + "&sEstado=" + sEstado + "&dtData=" + dtData + "&sMoeda=" + sMoeda + "&nTotal=" + nTotal;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/Reports_financeiro.aspx?' + strParameters;
    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);


    if (nEmpresaData[1] == 167) {
        //ESTA FUN��O FECHA A MODAL
        restoreInterfaceFromModal();
    }

}

function EmpresasIn() {
    var i;
    var empresasID;

    // Come�a a montar a lista de empresas.
    empresasID = "'(";

    // Monta a lista de empresas.
    for (i = 0; i < selEmpresaFluxoCaixa.options.length; i++) {
        // Se a empresa estiver selecionada, coloca ela na lista.
        if (selEmpresaFluxoCaixa.options[i].selected == true) {
            if (empresasID != "'(") empresasID += ",";


            empresasID += selEmpresaFluxoCaixa.options[i].value;

        }

    }

    // Termina de montar a lista das empresas.
    empresasID += ")'";

    return empresasID;
}