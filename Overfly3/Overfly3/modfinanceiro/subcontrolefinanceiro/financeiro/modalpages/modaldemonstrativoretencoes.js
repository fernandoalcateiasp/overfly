/********************************************************************
modaldemonstrativoretencoes.js

Library javascript para o modaldemonstrativoretencoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
//var dsoGrid = new CDatatransport("dsoGrid");

// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_nTipoFinanceiroID = 0;
var glb_bWindowLoading = true;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// Recupera valor do SUP
var nFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FinanceiroID' + '\'' + '].value');
var ntxtdtVencimento = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtdtVencimento.value');
var nImpostoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selImpostoID.value');

var nValorFinanceiro = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtValor.value');

var dsoGrid = new CDatatransport('dsoGrid');
var dsoAbreModal = new CDatatransport('dsoAbreModal');
/********************************************************************
RELACAO DAS FUNCOES

NOTA: Ver tambem o arquivo commongerargaregnre.js

FUNCOES GERAIS:
window_onload()
setupPage()
txtPesquisa_onKeyPress()
txtPesquisa_ondigit(ctl)
btnFindPesquisa_onclick(ctl)
btn_onclick(ctl)
startPesq(strPesquisa)
dataToReturn()

Eventos de grid particulares desta janela:
fg_modaldemonstrativoretencoesDblClick()
fg_modaldemonstrativoretencoesKeyPress(KeyAscii)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html

    with (modaldemonstrativoretencoesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 13;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }
    
    // configuracao inicial do html
    setupPage();
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fillGridData();
    //fg.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Demonstrativos de Reten��es', 1);
    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    
    
    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }

    adjustElementsInForm([['lblFinanceiro', 'txtFinanceiro', 12, 1, -1],
						  ['lbldtVencimento', 'txtdtVencimento', 12, 1, -1],
						  ['lblValorFinanceiro', 'txtValorFinanceiro', 12, 1, -1],
						  ['lblRetencoes', 'txtRetencoes', 12, 1, -1],
						  ['lblValorLiquido', 'txtValorLiquido', 12, 1, -1],
						  ['btnCanc', 'btnCanc', 50, 1, 275]], null, null, true);
    
    with (divFG.style)
    {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = (ELEM_GAP * 2) - 1;
        top = (ELEM_GAP * 3) + 60;
        width = modWidth - 42;
        height = MAX_FRAMEHEIGHT_OLD - 350;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    with (divFG2.style) 
    {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = (ELEM_GAP * 2) - 1;
        top = (ELEM_GAP * 3) + 250;
        width = modWidth - 42;
        height = MAX_FRAMEHEIGHT_OLD - 350;
    }

    with (fg2.style) 
    {
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10);
        height = parseInt(divFG2.style.height, 10);
    }

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG2.style.width, 10) * 18;
    fg.Redraw = 2;

    startGridInterface(fg2);
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.style.width, 10) * 18;
    fg2.Redraw = 2;

    txtFinanceiro.value = nFinanceiroID;
    txtdtVencimento.value = ntxtdtVencimento;

    txtFinanceiro.readOnly = true;
    txtdtVencimento.readOnly = true;
    txtRetencoes.readOnly = true;
    txtValorLiquido.readOnly = true;
    txtValorFinanceiro.readOnly = true;

    txtRetencoes.value = padNumReturningStr(roundNumber(txtRetencoes.value, 2), 2);
    txtValorLiquido.value = padNumReturningStr(roundNumber(txtValorLiquido.value, 2), 2);
    txtValorFinanceiro.value = padNumReturningStr(roundNumber(txtValorFinanceiro.value, 2), 2);

    fg2.Rows = 0;

    btnOK.style.visibility = 'hidden';    
    
    btnCanc.value = 'Cancelar';
    btnCanc.style.visibility = 'hidden';
    btnCanc.onClick = btn_onclick();
    //txtFinanceiro.onkeypress = Financeiro_onkeypress;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() 
{
    var sDadosAvaliacao = '';
    var aGrid = null;
    var i = 0;
    var sFiltro = '';

    lockControlsInModalWin(true);
    
    glb_nDSOs = 1;

    // zera o grid
    fg.Rows = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    // Select para testar a funcionalidade dos Grids
    dsoGrid.SQL = 'DECLARE @Financeiros VARCHAR(8000) = SPACE(0) ' +

                    'SELECT @Financeiros += ISNULL((\'[\' + CONVERT(VARCHAR(20), b.FinanceiroID) + \',\' + CONVERT(VARCHAR(20), c.Valor) + \']\'), \'\') ' +
                        'FROM ValoresLocalizar_Financeiros a WITH(NOLOCK) ' + 
                            'INNER JOIN ValoresLocalizar_Financeiros b WITH(NOLOCK) ON (b.ValorID = a.ValorID) ' + 
                            'INNER JOIN Financeiro c WITH(NOLOCK) ON (c.FinanceiroID = b.FinanceiroID) ' + 
                        'WHERE (a.FinanceiroID = ' + nFinanceiroID + ') ' +
                        'ORDER BY b.ValFinanceiroID ' +
    
                    'IF (LEN(@Financeiros) = 0) ' +
                        'SET @Financeiros = NULL ' +

                    'SELECT DISTINCT b.ImpostoID, b.FinRetencaoID, d.Imposto AS Imposto, ' +
    		            '(SELECT TOP 1 COUNT(1) ' +
    			            'FROM fn_Financeiro_Retencoes_tbl(b.FinanceiroID, b.FinRetencaoID, b.ImpostoID, 0, @Financeiros, NULL) aa) AS Financeiros, ' +
    		            'b.Valor AS ValorBase, ' +
    		            '(SELECT TOP 1 aa.RetencoesAnteriores ' +
    			            'FROM fn_Financeiro_Retencoes_tbl(b.FinanceiroID, b.FinRetencaoID, b.ImpostoID, 0, @Financeiros, NULL) aa ' +
    			            'WHERE (aa.FinRetencaoID = b.FinRetencaoID)) AS RetencoesAnteriores, ' +
    		            'b.BaseCalculo, b.Aliquota, ' +
    		            '(SELECT TOP 1 aa.RetencaoPreliminar ' +
    			            'FROM fn_Financeiro_Retencoes_tbl(b.FinanceiroID, b.FinRetencaoID, b.ImpostoID, 0, @Financeiros, NULL) aa ' +
    			            'WHERE (aa.FinRetencaoID = b.FinRetencaoID)) AS RetencaoPreliminar, ' +
    		            '(CASE e.TipoMinimo ' +
    			            'WHEN 1 THEN \'Lancamento\' ' +
    			            'WHEN 2 THEN \'Vencimento\' ' +
    			            'ELSE \'Pagamento\' END) AS TipoLimite, ' +
    		            '(SELECT TOP 1 aa.ValorMinimo ' +
    			            'FROM fn_Financeiro_Retencoes_tbl(b.FinanceiroID, b.FinRetencaoID, b.ImpostoID, 0, @Financeiros, NULL) aa ' +
    			            'WHERE (aa.FinRetencaoID = b.FinRetencaoID)) AS Limite, ' +
    		            'b.ValorRetencao AS Retencao, a.Valor AS ValorFinanceiro  ' +
    	            'FROM Financeiro a WITH(NOLOCK) ' +
    		            'INNER JOIN Financeiro_Retencoes_2 b WITH(NOLOCK) ON (b.FinanceiroID = a.FinanceiroID) ' +
    		            'INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = b.ImpostoID) ' +
    		            'INNER JOIN Conceitos_ImpostosRetencoes e WITH(NOLOCK) ON (e.ConceitoID = b.ImpostoID) ' +
    																            'AND ((e.LocalidadeID IS NULL) OR ' +
    																	            '(e.LocalidadeID = ' +
    																	                'dbo.fn_Pessoa_Localidade(a.EmpresaID, dbo.fn_Localidade_Tipo(e.LocalidadeID), NULL, NULL))) ' +
    				'WHERE (a.FinanceiroID = ' + nFinanceiroID + ')';
    
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

function fillGridData_DSC() 
{
    if (!(dsoGrid.recordset.EOF)) 
    {
        SomaValorRetencoes();
        dsoGrid.recordset.MoveFirst();
    }
    
    if (!(dsoGrid.recordset.EOF) || (dsoGrid.recordset.BOF))
    {
        startGridInterface(fg);
        fg.FontSize = '8';
        fg.FrozenCols = 0;

        headerGrid(fg, ['ImpostoID',
                        'FinRetencaoID',
                        'Imposto',
                        'Financeiros',
                        'Valor Base',
                        'RA',
                        'Base C�lculo',
                        'Al�quota',
                        'Reten��o Preliminar',
                        'Tipo Limite',
                        'Limite',
                        'Reten��o'], [0, 1]);
        fillGridMask(fg, dsoGrid, ['ImpostoID',
                                   'FinRetencaoID',
                                   'Imposto',
                                   'Financeiros',
                                   'ValorBase',
                                   'RetencoesAnteriores',
                                   'BaseCalculo',
                                   'Aliquota',
                                   'RetencaoPreliminar',
                                   'TipoLimite',
                                   'Limite',
                                   'Retencao'],
                                   ['', '', '', '', '999999999999.99', '', '999999999999.99', '999999999999.99', '999999999999.99', '', '999999999999.99', '999999999999.99'],
							       ['', '', '', '', '###,###,###,###.00', '', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', '', '###,###,###,###.00', '###,###,###,###.00']);


        glb_aCelHint = [[0, 5, 'Houve reten��es anteriores?']];
        
        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);

        fg.Redraw = 2;
        fg.Editable = false;

        pintaGridDivergencias();

        lockControlsInModalWin(false);
    }
}

function pintaGridDivergencias()
{
    var nValorRetencao = 0;
    var nRetencaoPreliminar = 0;
    var nDivergencia;
    
    for (i = 1; i < fg.Rows; i++)
    {
        nValorRetencao = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Retencao'));
        nRetencaoPreliminar = fg.ValueMatrix(i, getColIndexByColKey(fg, 'RetencaoPreliminar'));
        nValorBase = fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorBase'));
        nBaseCalculo = fg.ValueMatrix(i, getColIndexByColKey(fg, 'BaseCalculo'));
        nImpostoID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'ImpostoID'));

        if ((nValorRetencao != nRetencaoPreliminar) && (nValorRetencao > 0))
        {
            // Verifica se BC do PCC n�o bate com valor da reten��o do PCC (para casos em que a BC � formada por mais de 1 financeiro)
            if (((nValorRetencao != parseFloat(nBaseCalculo * 0.0465).toFixed(2)) && (nImpostoID == 18)) || (nImpostoID != 18))
            {
                fg.Cell(6, i, 0, i, (fg.Cols - 1)) = 0x00FFFF;
                nDivergencia = true;
            }
        }
    }
    
    if (nDivergencia)
        window.top.overflyGen.Alert('As linhas assinaladas em amarelo est�o com diverg�ncia de c�lculo. Favor contatar o CPD');
}

function SomaValorRetencoes() 
{
    var nValorAtual = 0;
    var nValorAnterior = 0;

    dsoGrid.recordset.MoveFirst();

    while (!(dsoGrid.recordset.EOF) || (dsoGrid.recordset.BOF)) 
    {
        nValorFinanceiro = dsoGrid.recordset['ValorFinanceiro'].value;

        nValorAtual = dsoGrid.recordset['Retencao'].value;
        nValorAnterior = nValorAnterior + nValorAtual;

        dsoGrid.recordset.MoveNext();
    }
    // Valor do campo Reten��es
    txtRetencoes.value = nValorAnterior;
    txtRetencoes.value = padNumReturningStr(roundNumber(txtRetencoes.value, 2), 2);

    // Valor do campo Valor Financeiro
    txtValorFinanceiro.value = nValorFinanceiro;
    txtValorFinanceiro.value = padNumReturningStr(roundNumber(txtValorFinanceiro.value, 2), 2);
    
    // Valor do campo Valor L�quido
    txtValorLiquido.value = (nValorFinanceiro - nValorAnterior);
    txtValorLiquido.value = padNumReturningStr(roundNumber(txtValorLiquido.value, 2), 2);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_modaldemonstrativoretencoesDblClick() 
{
    if (fg.Row >= 1)
    {
        var nValor = parseFloat(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ImpostoID')));
        var nFinRetencaoID = parseFloat(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'FinRetencaoID')));

        var sSQL = 'SELECT b.FinanceiroID, e.NotaFiscal, b.Valor AS ValorBase ' +
                        'FROM fn_Financeiro_Retencoes_tbl(' + nFinanceiroID + ', ' + nFinRetencaoID + ', NULL, 0, NULL, NULL) a ' +
		                    'INNER JOIN Financeiro_Retencoes_2 b WITH(NOLOCK) ON (b.FinRetencaoID = a.FinRetencaoID) ' +
		                    'INNER JOIN Financeiro c WITH(NOLOCK) ON (c.FinanceiroID = b.FinanceiroID) ' +
		                    'INNER JOIN Pedidos d WITH(NOLOCK) ON (d.PedidoID = c.PedidoID) ' + 
		                    'INNER JOIN NotasFiscais e WITH(NOLOCK) ON (e.NotaFiscalID = d.NotaFiscalID)';
        
        divFG2.style.visibility = 'inherit';
        fg2.style.visibility = 'inherit';

        lockControlsInModalWin(true);
        
        setConnection(dsoAbreModal);
        dsoAbreModal.SQL = sSQL;
        dsoAbreModal.ondatasetcomplete = Financeiros_DSC;
        dsoAbreModal.Refresh();
    }
}
/*
function fg_modaldemonstrativoretencoesDblClick()
{
    if (fg.Row >= 1) 
    {
        var nValor = parseFloat(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ImpostoID')));

        divFG2.style.visibility = 'inherit';
        fg2.style.visibility = 'inherit';

        lockControlsInModalWin(true);

        var strPas = '?';
        strPas += 'nFinanceiroID=' + escape(nFinanceiroID);
        strPas += '&nValor=' + escape(nValor);

        //dsoAbreModal.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/pesqdemonstrativoretencoes.aspx' + strPas;
        dsoAbreModal.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/pesqdemonstrativoretencoes.aspx' + strPas;
        dsoAbreModal.ondatasetcomplete = AbreModal_DSC;
        dsoAbreModal.refresh();
    }
}
*/

function Financeiros_DSC()
{
    if (!(dsoAbreModal.recordset.EOF) || (dsoAbreModal.recordset.BOF))
    {
        startGridInterface(fg2);
        fg2.FontSize = '8';
        fg2.FrozenCols = 0;

        headerGrid(fg2, ['FinanceiroID',
                         'Nota Fiscal',
                         'Valor Base'], []);
        fillGridMask(fg2, dsoAbreModal, ['FinanceiroID',
                                         'NotaFiscal',
                                         'ValorBase'],
                                        ['', '', '999999999999.99'],
                                        ['', '', '###,###,###,###.00']);

        
        
        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);

        fg.Redraw = 2;
        fg.Editable = false;
    }

    lockControlsInModalWin(false);
}

/*
function AbreModal_DSC() 
{
    startGridInterface(fg2);
    fg2.FontSize = '8';
    fg2.FrozenCols = 0;

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var nValor = parseFloat(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Imposto')));

    if (nValor == 18) 
    {
        headerGrid(fg2, ['Imposto',
                         'Situac�o',
                         'Lan�amento',
                         'Vencimento',
                         'Pagamento',
                         'Ac?',
                         'Financeiro',
                         'Aliquota',
                         'Vl Financeiro',
                         'Vl Base',
                         'Vl Acumulado',
                         'Vl M�nimo',
                         'Calc Intermedi�rio',
                         'Reten��es Anteriores',
                         'Reten��o',
                         'Venc Reten��o'], []);

        fillGridMask(fg2, dsoAbreModal, ['Imposto',
                                         'Situacao',
                                         'Lancamento',
                                         'Vencimento',
                                         'Pagamento',
                                         'AC?',
                                         'Financeiro',
                                         'Aliquota',
                                         'ValorFinanceiro',
                                         'ValorBase',
                                         'ValorAcumulado',
                                         'ValorMinimo',
                                         'RetencaoPreliminar',
                                         'RetencoesAnteriores',
                                         'Retencao',
                                         'VencimentoRetencao'],
                                         ['', '', '', '', '', '', '', '999999999999.99', '999999999999.99', '999999999999.99', '999999999999.99', '###,###,###,###.00', '999999999999.99', '999999999999.99', '99/99/9999'],
                                         ['', '', '', '', '', '', '', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', dTFormat]);
    }
    else 
    {
        headerGrid(fg2, ['Imposto',
                         'Situa��o',
                         'Lan�amento',
                         'Ac?',
                         'Financeiro',
                         'Aliquota',
                         'Vl Base',
                         'Vl Acumulado',
                         'Vl M�nimo',
                         'Calc Intermedi�rio',
                         'Reten��es Anteriores',
                         'Reten��o',
                         'Venc Reten��o'], []);

        fillGridMask(fg2, dsoAbreModal, ['Imposto',
                                         'Situacao',
                                         'Lancamento',
                                         'AC?',
                                         'Financeiro',
                                         'Aliquota',
                                         'ValorBase',
                                         'ValorAcumulado',
                                         'ValorMinimo',
                                         'RetencaoPreliminar',
                                         'RetencoesAnteriores',
                                         'Retencao',
                                         'VencimentoRetencao'],
                                         ['', '', '', '', '', '999999999999.99', '999999999999.99', '999999999999.99', '999999999999.99', '999999999999.99', '999999999999.99', '999999999999.99', '99/99/9999'],
                                         ['', '', '', '', '', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', dTFormat]);

    }
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.Redraw = 2;
    fg.Editable = false;
    
    lockControlsInModalWin(false);
}
*/

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick() 
{
    //restoreInterfaceFromModal();
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
}


/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_modaldemonstrativoretencoesKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaldemonstrativoretencoes_AfterEdit(Row, Col)
{
    ;
}

/*******************************************************************
Select que preenche o grid
*******************************************************************/

//// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modaldemonstrativoretencoesBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaldemonstrativoretencoesKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaldemonstrativoretencoes_ValidateEdit()
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaldemonstrativoretencoes_BeforeEdit(grid, row, col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaldemonstrativoretencoes_AfterEdit(Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodaldemonstrativoretencoes(grid, OldRow, OldCol, NewRow, NewCol)
{
    fg2.Rows = 0;
}

// FINAL DE EVENTOS DE GRID *****************************************