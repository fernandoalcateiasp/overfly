
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalgerarfinanceirosHtml" name="modalgerarfinanceirosHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalgerarfinanceiros.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
       
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

  
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalgerarfinanceiros.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nEmpresaID, nEmpresaPaisID, sCurrDateFormat, nFinanceiroID

sCaller = ""
nEmpresaID = 0
nEmpresaPaisID = 0
nFinanceiroID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaPaisID").Count    
    nEmpresaPaisID = Request.QueryString("nEmpresaPaisID")(i)
Next

For i = 1 To Request.QueryString("nFinanceiroID").Count    
    nFinanceiroID = Request.QueryString("nFinanceiroID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"

Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) &  ";"

'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If

Response.Write "var glb_nFinanceiroID = " & CStr(nFinanceiroID) & ";"

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalgerarfinanceiros_ValidateEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalgerarfinanceiros_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalgerarfinanceirosKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalgerarfinanceiros_BeforeEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modalgerarfinanceirosDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalgerarfinanceirosBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Atualizar linha de mensagens
js_fg_AfterRowColmodalgerarfinanceiros (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<script ID="vbFunctions" LANGUAGE="vbscript">
<!--

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

//-->
</script>

</head>

<body id="modalgerarfinanceirosBody" name="modalgerarfinanceirosBody" LANGUAGE="javascript" onload="return window_onload()">
    
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <!-- Div Controles -->
    <div id="divControls" name="divControls" class="divGeneral">
        <p  id="lblFuncionario" name="lblFuncionario" class="lblGeneral">Func</p>
        <input type="checkbox" id="chkFuncionario" name="chkFuncionario" class="fldGeneral" title="Funcion�rios?"></input>
        <p id="lblArgumento" name="lblArgumento" class="lblGeneral">Argumento</p>
        <input type="text" id="txtArgumento" name="txtArgumento" class="fldGeneral">
        <input type="image" id="btnFindPessoa" name="btnFindPessoa" class="fldGeneral" title="Preencher combo" onclick="javascript:return btnLupaClicked()" WIDTH="24" HEIGHT="23">
        <p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">Pessoa</p>
        <select id="selPessoaID" name="selPessoaID" class="fldGeneral"></select>
        <p id="lblDuplicata" name="lblDuplicata" class="lblGeneral">Duplicata</p>
        <input type="text" id="txtDuplicata" name="txtDuplicata" class="fldGeneral">
        <p id="lblFormaPagamentoID" name="lblFormaPagamentoID" class="lblGeneral">Forma</p>
        <select id="selFormaPagamentoID" name="selFormaPagamentoID" class="fldGeneral">
<%
	Dim strSQL, rsData

	Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT ItemID AS fldID, ItemAbreviado AS fldName " & _
			 "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
			 "WHERE (EstadoID = 2 AND TipoID=804) " & _
			 "ORDER BY Ordem"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd
	End If
	rsData.Close
	Set rsData = Nothing
%>		
        </select>
        <p id="lbldtEmissao" name="lbldtEmissao" class="lblGeneral">Emiss�o</p>
        <input type="text" id="txtdtEmissao" name="txtdtEmissao" class="fldGeneral">
        <p id="lbldtVencimento1" name="lbldtVencimento1" class="lblGeneral">Vencimento1</p>
        <input type="text" id="txtdtVencimento1" name="txtdtVencimento1" class="fldGeneral" title="Prazo da primeira parcela">
        <p id="lblParcelas" name="lblParcelas" class="lblGeneral">Parc</p>
        <input type="text" id="txtParcelas" name="txtParcelas" class="fldGeneral" title="N�mero de parcelas">
        <p id="lblIncremento" name="lblIncremento" class="lblGeneral">Incr</p>
        <input type="text" id="txtIncremento" name="txtIncremento" class="fldGeneral" title="Incremento das parcelas seguintes">
        <p  id="lblMesmoDia" name="lblMesmoDia" class="lblGeneral">MD</p>
        <input type="checkbox" id="chkMesmoDia" name="chkMesmoDia" class="fldGeneral" title="Manter o mesmo dia?"></input>
        <p id="lblMoedaID" name="lblMoedaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaID" name="selMoedaID" class="fldGeneral">
<%
	Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT c.ConceitoID as fldID, c.SimboloMoeda as fldName, b.Ordem " & _
             "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK) " & _
             "WHERE (a.SujeitoID = " & CStr(nEmpresaID) & " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " & _
                "AND b.MoedaID = c.ConceitoID AND b.Faturamento = 1 " & _
                "AND d.RecursoID=999)"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
	Set rsData = Nothing
%>
        </select>
        <p id="lblValor" name="lblValor" class="lblGeneral">Valor</p>
        <input type="text" id="txtValor" name="txtValor" class="fldGeneral">
        <p id="lblAtualizacao" name="lblAtualizacao" class="lblGeneral">Atualiza��o</p>
        <input type="text" id="txtAtualizacao" name="txtAtualizacao" class="fldGeneral" title="Taxa de atualiza��o">
        <p  id="lblEhImporte" name="lblEhImporte" class="lblGeneral">Imp</p>
        <input type="checkbox" id="chkEhImporte" name="chkEhImporte" class="fldGeneral" title="� importe?"></input>
        <p  id="lblUsoCotidiano" name="lblUsoCotidiano" class="lblGeneral">UC</p>
        <input type="checkbox" id="chkUsoCotidiano" name="chkUsoCotidiano" class="fldGeneral" title="Somente HP de uso cotidiano ou todos dispon�veis?"></input>
        <p id="lblHistoricoPadraoID" name="lblHistoricoPadraoID" class="lblGeneral">Hist�rico Padr�o</p>
        <select id="selHistoricoPadraoID" name="selHistoricoPadraoID" class="fldGeneral"></select>
        <p id="lblImpostoID" name="lblImpostoID" class="lblGeneral">Imposto</p>
        <select id="selImpostoID" name="selImpostoID" class="fldGeneral">
<%
	Set rsData = Server.CreateObject("ADODB.Recordset")

	strSQL = "SELECT 0 as fldID, SPACE(0) as fldName, SPACE(0) as Ordem2 " & _
		"UNION ALL " & _
		"SELECT ConceitoID as fldID, Imposto as fldName, Imposto as Ordem2 " & _
		"FROM Conceitos WITH(NOLOCK) " & _
		"WHERE (TipoConceitoID = 306 AND EstadoID = 2 AND PaisID = " & CStr(nEmpresaPaisID) & ") " & _
		"ORDER BY Ordem2, fldID"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
	Set rsData = Nothing
%>
        </select>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" class="fldGeneral">
    </div>    

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
            
	<input type="button" id="btnFillGrid" name="btnFillGrid" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
</body>

</html>
