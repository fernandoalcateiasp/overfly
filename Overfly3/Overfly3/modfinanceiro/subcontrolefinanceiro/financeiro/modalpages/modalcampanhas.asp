
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalcampanhasHtml" name="modalcampanhasHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalcampanhas.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
       
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

  
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalcampanhas.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, sCurrDateFormat, nEmpresaID, nFinanceiroID, rsData, sqlParam, strSQL

sCaller = ""
nEmpresaID = 0
nFinanceiroID = 0
sqlParam = 101

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"

Response.Write vbcrlf

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"

Response.Write vbcrlf

For i = 1 To Request.QueryString("nFinanceiroID").Count    
    nFinanceiroID = Request.QueryString("nFinanceiroID")(i)
Next

Response.Write "var glb_nFinanceiroID = " & CStr(nFinanceiroID) & ";"

Response.Write vbcrlf

'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
	sqlParam = 103
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
	sqlParam = 101
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT a.TipoFinanceiroID, b.Fantasia AS Parceiro, CONVERT(VARCHAR(10), a.dtVencimento, " & CStr(sqlParam) & ") AS Vencimento, " & _
		 "c.ConceitoID AS MoedaFinanceiroID, c.SimboloMoeda AS MoedaFinanceiro, a.Valor, " & _
		 "dbo.fn_Financeiro_Totais(a.FinanceiroID, GETDATE(), 54) AS SaldoFinanceiro, " & _
		 "e.ConceitoID AS MoedaSistemaID, e.SimboloMoeda AS MoedaSistema, a.TaxaMoeda " & _
		 "FROM Financeiro a WITH(NOLOCK) " & _
		        "INNER JOIN Pessoas b WITH(NOLOCK) ON  a.ParceiroID = b.PessoaID " & _
		        "INNER JOIN Conceitos c WITH(NOLOCK) ON  a.MoedaID = c.ConceitoID, " & _
		        "Recursos d WITH(NOLOCK) " & _
		        "INNER JOIN Conceitos e WITH(NOLOCK) ON d.MoedaID = e.ConceitoID " & _
		 "WHERE (a.FinanceiroID = " & CStr(nFinanceiroID) & " " & _
		 " AND d.RecursoID = 999) "

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write "var glb_nTipoFinanceiroID = 0;"
Response.Write vbcrlf
Response.Write "var glb_sParceiro = '';"
Response.Write vbcrlf
Response.Write "var glb_sVencimento = '';"
Response.Write vbcrlf
Response.Write "var glb_nMoedaFinanceiroID = 0;"
Response.Write vbcrlf
Response.Write "var glb_sMoedaFinanceiro = '';"
Response.Write vbcrlf
Response.Write "var glb_nValorFinanceiro = 0;"
Response.Write vbcrlf
Response.Write "var glb_nSaldoFinanceiro = 0;"
Response.Write vbcrlf
Response.Write "var glb_nMoedaSistemaID = 0;"
Response.Write vbcrlf
Response.Write "var glb_sMoedaSistema = '';"
Response.Write vbcrlf
Response.Write "var glb_nMoedaSistema = 0;"
Response.Write vbcrlf
Response.Write "var glb_nMoedaID = 0;"
Response.Write vbcrlf
Response.Write "var glb_sMoeda = '';"
Response.Write vbcrlf
Response.Write "var glb_TaxaFinanceiro = '';"
Response.Write vbcrlf

While Not rsData.EOF
        
	Response.Write "glb_nTipoFinanceiroID = " & CStr(rsData.Fields("TipoFinanceiroID").Value) & ";"
	Response.Write vbcrlf
	Response.Write "glb_sParceiro = " & Chr(39) & CStr(rsData.Fields("Parceiro").Value) & Chr(39) & ";"
	Response.Write vbcrlf
	Response.Write "glb_sVencimento = " & Chr(39) & CStr(rsData.Fields("Vencimento").Value) & Chr(39) & ";"
	Response.Write vbcrlf
	Response.Write "glb_nMoedaFinanceiroID = " & CStr(rsData.Fields("MoedaFinanceiroID").Value) & ";"
	Response.Write vbcrlf
	Response.Write "glb_sMoedaFinanceiro = " & Chr(39) & CStr(rsData.Fields("MoedaFinanceiro").Value) & Chr(39) & ";"
	Response.Write vbcrlf
	Response.Write "glb_nValorFinanceiro = " & CStr(rsData.Fields("Valor").Value) & ";"
	Response.Write vbcrlf
	Response.Write "glb_nSaldoFinanceiro = " & CStr(rsData.Fields("SaldoFinanceiro").Value) & ";"
	Response.Write vbcrlf
	Response.Write "glb_nMoedaSistemaID = " & CStr(rsData.Fields("MoedaSistemaID").Value) & ";"
	Response.Write vbcrlf
	Response.Write "glb_sMoedaSistema = " & Chr(39) & CStr(rsData.Fields("MoedaSistema").Value) & Chr(39) & ";"
	Response.Write vbcrlf
	Response.Write "glb_TaxaFinanceiro = " & Chr(39) & CStr(rsData.Fields("TaxaMoeda").Value) & Chr(39) & ";"
	Response.Write vbcrlf
	
	If (CLng(rsData.Fields("MoedaSistemaID").Value) = CLng(rsData.Fields("MoedaFinanceiroID").Value)) Then
		Response.Write "glb_nMoedaSistema = 1;"
		Response.Write vbcrlf
		Response.Write "glb_nMoedaID = " & CStr(rsData.Fields("MoedaSistemaID").Value) & ";"
		Response.Write vbcrlf
		Response.Write "glb_sMoeda = " & Chr(39) & CStr(rsData.Fields("MoedaSistema").Value) & Chr(39) & ";"
		Response.Write vbcrlf
	Else
		Response.Write "glb_nMoedaSistema = 0;"
		Response.Write vbcrlf
		Response.Write "glb_nMoedaID = " & CStr(rsData.Fields("MoedaFinanceiroID").Value) & ";"
		Response.Write vbcrlf
		Response.Write "glb_sMoeda = " & Chr(39) & CStr(rsData.Fields("MoedaFinanceiro").Value) & Chr(39) & ";"
		Response.Write vbcrlf
	End If

	
    rsData.MoveNext
Wend

Set rsData = Nothing

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalcampanhas_ValidateEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalcampanhas_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalcampanhasKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalcampanhas_BeforeEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modalcampanhasDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalcampanhasBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Atualizar linha de mensagens
js_fg_AfterRowColmodalcampanhas (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<script ID="vbFunctions" LANGUAGE="vbscript">
<!--

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

//-->
</script>

</head>

<body id="modalcampanhasBody" name="modalcampanhasBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <!-- Div Controles -->
    <div id="divControls" name="divControls" class="divGeneral">
        <p id="lblTipoCampanha" name="lblTipoCampanha" class="lblGeneral">Tipo</p>
        <select id="selTipoCampanha" name="selTipoCampanha" class="fldGeneral">
<%
	Set rsData = Server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT Ordem, ItemID AS fldID, ItemMasculino AS fldName " & _
		"FROM tiposauxiliares_itens WITH(NOLOCK) " & _
		"WHERE Tipoid=812 and Aplicar=1 and Estadoid=2 " & _
		"ORDER BY Ordem"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	While (Not rsData.EOF)
		Response.Write "<option value =" & rsData.Fields("fldID").Value 
		Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & chr(13) & chr(10)

		rsData.MoveNext
	WEnd

	rsData.Close
	Set rsData = Nothing
%>		
	</select>
    
        <p id="lblAD" name="lblAD" class="lblGeneral">A/D</p>
        <input type="checkbox" id="chkAD" name="chkAD" class="fldGeneral" title="Associar ou Dissociar?">
		<p id="lblFinanceiroID" name="lblFinanceiroID" class="lblGeneral">Financeiro</p>
        <input type="text" id="txtFinanceiroID" name="txtFinanceiroID" class="fldGeneral">
		<p id="lblParceiro" name="lblParceiro" class="lblGeneral">Parceiro</p>
        <input type="text" id="txtParceiro" name="txtParceiro" class="fldGeneral">
        <p id="lblVencimento" name="lblVencimento" class="lblGeneral">Vencimento</p>
        <input type="text" id="txtVencimento" name="txtVencimento" class="fldGeneral">
        <p id="lblMoeda" name="lblMoeda" class="lblGeneral">$</p>
        <input type="text" id="txtMoeda" name="txtMoeda" class="fldGeneral">
		<p id="lblValor" name="lblValor" class="lblGeneral">Valor</p>
        <input type="text" id="txtValor" name="txtValor" class="fldGeneral">
        <p id="lblSaldo" name="lblSaldo" class="lblGeneral">Saldo</p>
        <input type="text" id="txtSaldo" name="txtSaldo" class="fldGeneral">
        <p id="lblEmpresaID" name="lblEmpresaID" class="lblGeneral">Empresa</p>
        <select id="selEmpresaID" name="selEmpresaID" class="fldGeneral"></select>
        <p id="lblFabricanteID" name="lblFabricanteID" class="lblGeneral">Fabricante</p>
        <select id="selFabricanteID" name="selFabricanteID" class="fldGeneral"></select>
        <p id="lblCodigo" name="lblCodigo" class="lblGeneral">C�digo</p>
        <input type="text" id="txtCodigo" name="txtCodigo" class="fldGeneral">
        <p id="lblProduto" name="lblProduto" class="lblGeneral">Produto</p>
        <input type="text" id="txtProduto" name="txtProduto" class="fldGeneral">
        <p id="lblDataInicio" name="lblDataInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio" name="txtDataInicio" class="fldGeneral">
        <p id="lblDataFim" name="lblDataFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim" name="txtDataFim" class="fldGeneral">
		<input type="button" id="btnFillGrid" name="btnFillGrid" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    </div>    

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
            
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
</body>

</html>
