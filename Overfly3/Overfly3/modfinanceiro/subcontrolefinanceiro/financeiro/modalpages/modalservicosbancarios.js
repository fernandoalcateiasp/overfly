/********************************************************************
modalservicosbancarios.js

Library javascript para o modalservicosbancarios.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_sResultado = '';
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_nOK = 0, glb_nOKfg2 = 0;
var glb_nIncluir = new Array();
// Contantes de servi�os banc�rios.
var DEPOSITO_BANCARIO = 1;
var TRANSFERENCIA_BANCARIA = 2;
var campos = [];
var campos2 = [];


var dsoGrid = new CDatatransport("dsoGrid");
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");
var dsoCmbDynamic02 = new CDatatransport("dsoCmbDynamic02");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoCombo = new CDatatransport("dsoCombo");
var dsoDeposito = new CDatatransport("dsoDeposito");
var dsoTransf = new CDatatransport("dsoTransf");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalservicosbancariosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalservicosbancariosDblClick(grid, Row, Col)
js_modalservicosbancariosKeyPress(KeyAscii)
js_modalservicosbancarios_ValidateEdit()
js_modalservicosbancarios_BeforeEdit(grid, row, col)
js_modalservicosbancarios_AfterEdit(Row, Col)
js_fg_AfterRowColmodalservicosbancarios (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalservicosbancariosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    glb_bFirstFill = true;

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {

    secText(translateTerm('Servi�os Banc�rios', null), 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    var btnHeight = parseInt(btnOK.currentStyle.height, 10);

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));

    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // desabilita o botao OK
    btnOK.disabled = true;
    btnFillGrid.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([
                            ['lbldtInicio', 'txtdtInicio', 10, 2, -10],
                            ['lbldtFim', 'txtdtFim', 10, 2],
                            ['lblRelPesContaID', 'selRelPesContaID', 20, 2],
                            ['lblMotivo', 'chkMotivo', 3, 2, -5],
                            ['btnFillGrid', 'btn', btnWidth, 2, ELEM_GAP - 4]],
                            null, null, true);

    btnFillGrid.style.height = btnHeight;
    btnFillGrid.style.left = modWidth - parseInt(btnFillGrid.currentStyle.width, 10) - 25;
    btnOK.style.height = btnHeight;
    btnGerar.style.height = btnHeight;
    btnRemover.disabled = true;
    btnOK.style.left = modWidth - parseInt(btnOK.currentStyle.width, 10) - parseInt(btnRemover.currentStyle.width, 10) - 35;
    btnGerar.style.left = modWidth - parseInt(btnGerar.currentStyle.width, 10) - 25;
    btnRemover.style.left = modWidth - parseInt(btnRemover.currentStyle.width, 10) - 25;
    btnGerar.disabled = true;

    txtdtInicio.onfocus = selFieldContent;
    txtdtInicio.maxLength = 10;
    txtdtInicio.onkeypress = txtFields_onKeyPress;
    txtdtInicio.value = glb_dCurrDate;

    txtdtFim.onfocus = selFieldContent;
    txtdtFim.maxLength = 10;
    txtdtFim.onkeypress = txtFields_onKeyPress;
    txtdtFim.value = glb_dCurrDate;

    selRelPesContaID.onchange = selRelPesContaID_onchange;
    selServicoBancarioID.onchange = selServicoBancarioID_onchange;
    selOrigem.onchange = selOrigem_onchange;
    selDestino.onchange = selDestino_onchange;
    adjustLabelsCombos();

    txtValor.onkeypress = verifyNumericEnterNotLinked;
    txtValor.setAttribute('thePrecision', 11, 1);
    txtValor.setAttribute('theScale', 2, 1);
    txtValor.setAttribute('verifyNumPaste', 1);
    txtValor.setAttribute('minMax', new Array(1, 111111111.99), 1);

    // Ajusta o divServico
    with (divServico.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = selRelPesContaID.offsetLeft + selRelPesContaID.offsetWidth + ELEM_GAP;
        height = selRelPesContaID.offsetTop + selRelPesContaID.offsetHeight;
    }

    // Ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        btnFillGrid.offsetTop + btnFillGrid.offsetHeight + 2;
        width = selRelPesContaID.offsetLeft + selRelPesContaID.offsetWidth + ELEM_GAP;
        height = selRelPesContaID.offsetTop + selRelPesContaID.offsetHeight;
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP - 2;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6) - 210;
    }

    with (divFG2.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = (parseInt(divFG.currentStyle.top, 10) + parseInt(divFG.currentStyle.height, 10) + ELEM_GAP) + 18;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = 160;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    with (fg2.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10);
        height = parseInt(divFG2.style.height, 10);
    }

    // Altera botao OK
    with (btnOK) {
        value = 'Incluir';
        title = 'Incluir VL no Dep�sito';
    }

    // Ajusta o divTransferencia
    with (divTransferencia.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.height, 10) - 11;
        btnFillGrid.offsetTop + btnFillGrid.offsetHeight + 2;
        width = selRelPesContaID.offsetLeft + selRelPesContaID.offsetWidth + ELEM_GAP;
        height = selRelPesContaID.offsetTop + selRelPesContaID.offsetHeight;
    }

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblOrigem', 'selOrigem', 20, 1, -10],
                          ['lblDestino', 'selDestino', 20, 1],
                          ['lblValor', 'txtValor', 15, 1],
                          ['btnGerarTransf', 'btn', btnWidth, 1, ELEM_GAP - 4]],
                        null, null, true);

    // Esconde botao Cancel
    btnCanc.style.visibility = 'hidden';
    btnOK.style.top = 263;
    btnRemover.style.top = 263;
    btnGerar.style.top = 453;
    btnGerar.disabled = true;
    btnGerarTransf.style.left = modWidth - parseInt(btnGerarTransf.currentStyle.width, 10) - 25;
    btnGerarTransf.style.height = btnHeight;

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;

    startGridInterface(fg2);
    fg2.Cols = 0;
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;
    fg2.Redraw = 2;

    //window.resizeTo(820, 150);

    adjustdivs();
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtFields_onKeyPress() {
    if (event.keyCode == 13) {
        fg.Rows = 1;
    }
}

/********************************************************************
Clique botao
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado
    var controlID = ctl.id;

    if (controlID == 'btnOK') {
        incluir();
    }
    else if (controlID == 'btnFillGrid') {
        listar();
    }
    else if (controlID == 'btnRemover') {
        remover();
    }
    else if (controlID == 'btnGerar') {
        gerardeposito();
    }
    else if (controlID == 'btnGerarTransf') {
        gerartransf();
    }
        // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    showExtFrame(window, true);
    window.focus();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    if (glb_sResultado != '') {
        if (window.top.overflyGen.Alert(glb_sResultado) == 0)
            return null;

        glb_sResultado = '';
    }

    var aGrid = null;
    var i = 0;
    var sDataInicio = trimStr(txtdtInicio.value);
    var sDataFim = trimStr(txtdtFim.value);
    var sFiltroDataInicio = '';
    var sFiltroDataFim = '';
    var sFiltroContaBancaria = '';
    var nTop = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');

    lockControlsInModalWin(true);

    glb_nDSOs = 1;

    // zera o grid
    fg.Rows = 1;

    if (sDataInicio != '') {
        sDataInicio = normalizeDate_DateTime(sDataInicio, 1);

        sFiltroDataInicio = ' a.dtApropriacao >= ' + '\'' + sDataInicio + '\'' + ' ';
    }
    else
        sFiltroDataInicio = '';

    if (sDataFim != '') {
        sDataFim = normalizeDate_DateTime(sDataFim, 1);

        sFiltroDataFim = ' AND a.dtApropriacao <= ' + '\'' + sDataFim + '\'' + ' ';
    }
    else
        sFiltroDataFim = '';

    if (chkMotivo.checked == true)
        sFiltroContaBancaria = ' AND a.MotivoDevolucao IS NOT NULL ';


    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT b.ItemAbreviado AS Forma, dtApropriacao AS Apropriacao, dtEmissao AS Emissao, Fantasia AS Pessoa, NULL AS OK, ' +
	               'Documento AS Documento, Emitente AS Emitente, BancoAgencia AS [Banco/Agencia],a.NumeroDocumento AS Numero, MotivoDevolucao AS Motivo, ' +
	               'ValorID AS ValorID, d.SimboloMoeda AS Moeda, Valor AS Valor, a.Observacao AS Observacao ' +
	            'FROM ValoresLocalizar a WITH(NOLOCK) ' +
	            'LEFT OUTER JOIN Pessoas c WITH(NOLOCK) ON a.PessoaID = c.PessoaID ' +
	            'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.FormaPagamentoID = b.ItemID ' +
	            'INNER JOIN Conceitos d WITH(NOLOCK) ON a.MoedaID = d.ConceitoID ' +
	            'WHERE ' + sFiltroDataInicio + sFiltroDataFim + sFiltroContaBancaria +
	                ' AND a.EmpresaID = ' + glb_aEmpresaData[0] + ' AND a.TipoValorID = 1002 AND a.EstadoID IN (41,53,48) AND a.FormaPagamentoID IN (1031, 1032) AND ' +
			        'a.ValorID NOT IN (SELECT f.ValorID FROM Financeiro_ValoresDepositados f WITH(NOLOCK) INNER JOIN Financeiro g WITH(NOLOCK) ON (f.FinanceiroID = g.FinanceiroID) WHERE (f.ValorID = a.ValorID) AND (g.EstadoID NOT IN (5))) AND ' +
	                'a.ValorID NOT IN (SELECT b.ValorID FROM DepositosBancarios a WITH(NOLOCK), DepositosBancarios_ValoresLocalizar b WITH(NOLOCK) WHERE (a.EstadoID NOT IN (5) AND a.DepositoID = b.DepositoID)) AND ' +
                    'a.ProcessoID IN (1422,1423) ' +
	                //'a.ProcessoID IN (1422,1423) AND ' +
	                //'(dbo.fn_ValorLocalizar_Posicao(a.ValorID, 4) = 0) ' +
	            'ORDER BY b.ItemAbreviado, dtApropriacao, dtEmissao, ValorID';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;

    try {
        dsoGrid.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;
    if (glb_nDSOs > 0)
        return null;

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '7';
    fg.FrozenCols = 5;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    var aHoldCols = new Array();

    aHoldCols = [];

    headerGrid(fg, ['Forma',
				   'Apropria��o',
				   'Emiss�o',
				   'Pessoa',
				   'OK',
				   'Documento',
				   'Emitente',
				   'Banco/Ag�ncia',
				   'Numero',
				   'Motivo',
				   'ValorID',
				   '$',
				   'Valor',
				   'Observa��o'], []);

    fillGridMask(fg, dsoGrid, ['Forma*',
				               'Apropriacao*',
				               'Emissao*',
				               'Pessoa*',
				               'OK',
				               'Documento*',
				               'Emitente*',
				               'Banco/Agencia*',
				               'Numero*',
				               'Motivo*',
				               'ValorID*',
				               'Moeda*',
				               'Valor*',
				               'Observacao*'],
							 ['', '', '', '', '', '', '', '', '', '', '', '', '', ''],
							 ['', dTFormat, dTFormat, '', '', '', '', '', '', '', '', '', '###,###,##0.00', '']);

    with (fg) {
        ColDataType(4) = 11;
    }

    fg.Redraw = 0;

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    paintCellsSpecialyReadOnly();

    lockControlsInModalWin(false);
    fg.ExplorerBar = 5;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;

    fg.Redraw = 2;
    fg.LeftCol = 1;
    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        fg.Editable = true;
        window.focus();
        fg.focus();
    }
    else {
        ;
    }
    controlButtonsState();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData2() {
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    if (glb_sResultado != '') {
        if (window.top.overflyGen.Alert(glb_sResultado) == 0)
            return null;

        glb_sResultado = '';
    }

    var aGrid = null;
    var i = 0, nFirst = true;
    var i2 = 0;
    var sFiltroValorID = '';
    var nTop = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');

    lockControlsInModalWin(true);

    glb_nDSOs = 1;

    // zera o grid
    fg2.Rows = 1;

    for (i2 = 0; i2 < glb_nIncluir.length; i2++) {
        if ((glb_nIncluir[i2] != null) && (glb_nIncluir[i2] != 0)) {
            if (nFirst == true) {
                sFiltroValorID = ' a.ValorID IN (' + glb_nIncluir[i2];
                nFirst = false;
            }
            else
                sFiltroValorID = sFiltroValorID + ', ' + glb_nIncluir[i2];
        }
    }


    if (sFiltroValorID == '')
        sFiltroValorID = ' a.ValorID IN (0) ';
    else
        sFiltroValorID = sFiltroValorID + ') ';

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT b.ItemAbreviado AS Forma, dtApropriacao AS Apropriacao, dtEmissao AS Emissao, Fantasia AS Pessoa, ' +
	               'Documento AS Documento, Emitente AS Emitente, BancoAgencia AS [Banco/Agencia],a.NumeroDocumento AS Numero, MotivoDevolucao AS Motivo, ' +
	               'ValorID AS ValorID, d.SimboloMoeda AS Moeda, Valor AS Valor, a.Observacao AS Observacao ' +
	            'FROM ValoresLocalizar a WITH(NOLOCK) ' +
	            'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.FormaPagamentoID = b.ItemID ' +
	            'INNER JOIN Pessoas c WITH(NOLOCK) ON a.PessoaID = c.PessoaID ' +
	            'INNER JOIN Conceitos d WITH(NOLOCK) ON a.MoedaID = d.ConceitoID ' +
	            'WHERE ' + sFiltroValorID +
	            'ORDER BY b.ItemAbreviado, dtApropriacao, dtEmissao, ValorID';

    dsoGrid.ondatasetcomplete = fillGridData_DSC2;

    try {
        dsoGrid.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC2() {
    glb_nDSOs--;
    if (glb_nDSOs > 0)
        return null;

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();

    fg2.Redraw = 0;
    fg2.Editable = false;
    startGridInterface(fg2, 1, null);

    fg2.FontSize = '7';
    fg2.FrozenCols = 5;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    var aHoldCols = new Array();

    aHoldCols = [];

    headerGrid(fg2, ['Forma',
				   'Apropria��o',
				   'Emiss�o',
				   'Pessoa',
				   'Documento',
				   'Emitente',
				   'Banco/Ag�ncia',
				   'Numero',
				   'Motivo',
				   'ValorID',
				   '$',
				   'Valor',
				   'Observa��o'], []);

    fillGridMask(fg2, dsoGrid, ['Forma',
				               'Apropriacao',
				               'Emissao',
				               'Pessoa',
				               'Documento',
				               'Emitente',
				               'Banco/Agencia',
				               'Numero',
				               'Motivo',
				               'ValorID',
				               'Moeda',
				               'Valor',
				               'Observacao'],
							 ['', '', '', '', '', '', '', '', '', '', '', '', ''],
							 ['', dTFormat, dTFormat, '', '', '', '', '', '', '', '', '###,###,##0.00', '']);

    paintReadOnlyCols(fg2);
    fg2.Redraw = 0;

    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0, fg2.Cols - 1);

    gridHasTotalLine(fg2, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg2, 'Valor'), '###,###,###.00', 'S'],
    [getColIndexByColKey(fg2, 'ValorID'), '', 'C']]);

    lockControlsInModalWin(false);
    fg2.ExplorerBar = 5;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // Merge de Colunas
    fg2.MergeCells = 4;
    fg2.MergeCol(0) = true;
    fg2.MergeCol(1) = true;
    fg2.MergeCol(2) = true;
    fg2.MergeCol(3) = true;

    fg2.Redraw = 2;

    fg2.LeftCol = 1;

    if (fg2.Rows >= 2)
        fg2.Row = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg2.Rows > 1) {
        fg2.Editable = false;
        window.focus();
        fg2.focus();
    }
    else {
        ;
    }
    controlButtonsState();
}

function selRelPesContaID_onchange() {
    adjustLabelsCombos();
    fg.Rows = 1;
}

function selOrigem_onchange() {
    adjustLabelsCombos();
    habilitaGerarTransf();
    ComboOrigem();
    for (var loop = 0; loop <= campos2.length; loop++) {
        if (campos2[loop] == selOrigem.options.item(selOrigem.selectedIndex).value) {
            lblOrigem.innerText = 'Conta Origem ' + campos2[loop + 1];
            loop = campos2.length + 1;
        }
    }

    startDynamicCmbs();
}

function selDestino_onchange() {
    adjustLabelsCombos();
    habilitaGerarTransf();

    for (var loop = 0; loop <= campos.length; loop++) {
        if (campos[loop] == selDestino.options.item(selDestino.selectedIndex).value) {
            lblDestino.innerText = 'Conta Destino ' + campos[loop + 1];
            loop = campos.length + 1;
        }
    }
}

function selServicoBancarioID_onchange() {
    adjustLabelsCombos();

    adjustdivs();

    if (selServicoBancarioID.value == 1412)
        habilitaGerarTransf();

}

function adjustLabelsCombos() {
    setLabelOfControl(lblRelPesContaID, selRelPesContaID.value);
    setLabelOfControl(lblServicoBancarioID, selServicoBancarioID.value);
    controlButtonsState();
}

function listar() {
    if (!verificaData())
        return null;

    fillGridData();
}

function verificaData() {
    var sDataInicio = trimStr(txtdtInicio.value);
    var sDataFim = trimStr(txtdtFim.value);

    var bDataIsValid = true;

    if (sDataInicio != '')
        bDataIsValid = chkDataEx(sDataInicio);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data in�cio inv�lida.') == 0)
            return null;

        window.focus();

        if (sDataInicio != '')
            txtdtInicio.focus();

        return false;
    }

    bDataIsValid = true;

    if (sDataFim != '')
        bDataIsValid = chkDataEx(sDataFim);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data fim inv�lida.') == 0)
            return null;

        window.focus();

        if (sDataFim != '')
            txtdtFim.focus();

        return false;
    }

    return true;
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    lockControlsInModalWin(false);

    glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly() {
    paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalservicosbancariosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalservicosbancariosDblClick(grid, Row, Col) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalservicosbancariosKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalservicosbancarios_ValidateEdit() {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalservicosbancarios_BeforeEdit(grid, row, col) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalservicosbancarios_AfterEdit(Row, Col) {
    controlButtonsState();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalservicosbancarios(grid, OldRow, OldCol, NewRow, NewCol) {
    if (grid.id == 'fg') {
        if (glb_GridIsBuilding)
            return true;
    }
    else if (grid.id == 'fg2') {
            ;
    }
}

function imprimeGrid() {
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var sMsg = aEmpresaData[6] + '     ' + translateTerm('Servi�os Banc�rios', null) + '     ' + getCurrDate();
    var gridLine = fg.gridLines;
    fg.gridLines = 2;
    fg.PrintGrid(sMsg, false, 2, 0, 450);
    fg.gridLines = gridLine;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 1;
}

/********************************************************************
Controla o estado dos bot�es
********************************************************************/
function controlButtonsState() {
    glb_nOK = 0;
    for (i = 1; i < fg.Rows; i++) {
        if ((getCellValueByColKey(fg, 'OK', i) != null) && (getCellValueByColKey(fg, 'OK', i) != 0))
            glb_nOK++;
    }

    btnFillGrid.disabled = false;
    if ((selServicoBancarioID.value == '1411') && (selRelPesContaID.value != '0') && (fg2.Rows > 1))
        btnGerar.disabled = false;
    else
        btnGerar.disabled = true;

    if (glb_nOK >= 1)
        btnOK.disabled = false;
    else
        btnOK.disabled = true;

    if (fg2.Row > 1)
        btnRemover.disabled = false;
    else
        btnRemover.disabled = true;
}

/********************************************************************
Inclui registros do fg para o fg2
********************************************************************/
function incluir() {
    var bFind = false;
    for (i = 0; i < fg.Rows; i++) {
        bFind = false;
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {

            for (var i3 = 0; i3 < glb_nIncluir.length; i3++) {
                if (glb_nIncluir[i3] == fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorID*')))
                    bFind = true;
            }
            if (bFind == false)
                glb_nIncluir[glb_nIncluir.length] = fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorID*'));
        }
        if (i >= 1)
            fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = 0;
    }
    controlButtonsState();
    fillGridData2();
}

/********************************************************************
Remover registros do fg2
********************************************************************/
function remover() {
    if (fg2.Row > 1) {
        for (var i3 = 0; i3 < glb_nIncluir.length; i3++) {
            if (glb_nIncluir[i3] == fg2.ValueMatrix(fg2.Row, getColIndexByColKey(fg2, 'ValorID')))
                glb_nIncluir[i3] = null;
        }
    }

    controlButtonsState();
    fillGridData2();
}

/********************************************************************
Funcao do programador
Gerar Dep�sito
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function gerardeposito() {
    lockControlsInModalWin(true);

    var strPars = new String();
    var nCounter = 0;

    strPars = '';

    var nRelPesContaID = 0;
    var sValoresID = '';
    var nUsuarioID = 0;

    nUsuarioID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    nRelPesContaID = escape(selRelPesContaID.value);
    strPars += '?nRelPesContaID=' + escape(nRelPesContaID);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);

    for (i = 2; i < fg2.Rows; i++) {
        if (nCounter == 0)
            strPars += '&nValorID=/';
        else
            strPars += '/';

        strPars += escape(getCellValueByColKey(fg2, 'ValorID', i));
        nCounter++;
    }
    strPars += '/';


    if (nCounter == 0) {
        if (window.top.overflyGen.Alert('Nenhum Valor a Localizar selecionado') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    dsoDeposito.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/gerardeposito.aspx' + strPars;
    dsoDeposito.ondatasetcomplete = gerardeposito_DSC;
    dsoDeposito.refresh();
}

/********************************************************************
Funcao do programador
Retorno do Dep�sito
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function gerardeposito_DSC() {
    var _retMsg = 0;
    lockControlsInModalWin(false);

    if (!(dsoDeposito.recordset.BOF && dsoDeposito.recordset.EOF)) {
        if ((dsoDeposito.recordset['Resultado'].value != null) && (dsoDeposito.recordset['Resultado'].value != '')) {
            if (window.top.overflyGen.Alert(dsoDeposito.recordset['Resultado'].value) == 0)
                return null;
        }
        else
            _retMsg = window.top.overflyGen.Confirm('Detalhar FinanceiroID ' + dsoDeposito.recordset['FinanceiroID'].value + '?');

        var detalhar = dsoDeposito.recordset['FinanceiroID'].value;

        limpagrids();
        controlButtonsState();

        if (_retMsg != 1) {
            return null;
        }
        else if (_retMsg == 1) {
            //sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(glb_nEmpresaID, dsoTransf.recordset['FinanceiroDeID'].value));
            sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, detalhar);
            return true;
        }
    }
}

/********************************************************************
Funcao do programador
Gerar Transfer�ncia
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function gerartransf() {

    lockControlsInModalWin(true);

    if ((txtValor.value == '0') || (txtValor.value == null) || (txtValor.value == '')) {
        if (window.top.overflyGen.Alert('Valor incorreto ou nulo') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    var strPars = new String();
    var nCounter = 0;

    strPars = '';

    var nRelPesContaDeID = 0;
    var nRelPesContaParaID = 0;
    var nValor = 0;
    var nUsuarioID = 0;

    nUsuarioID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    nRelPesContaDeID = escape(selOrigem.value);
    nRelPesContaParaID = escape(selDestino.value);
    nValor = escape(txtValor.value);

    strPars += '?nRelPesContaDeID=' + escape(nRelPesContaDeID);
    strPars += '&nRelPesContaParaID=' + escape(nRelPesContaParaID);
    strPars += '&nValor=' + escape(nValor);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);

    dsoTransf.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/gerartransf.aspx' + strPars;
    dsoTransf.ondatasetcomplete = gerartransf_DSC;
    dsoTransf.refresh();
}

/********************************************************************
Funcao do programador
Retorno da Transfer�ncia
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function gerartransf_DSC() {
    var _retMsg = '';
    lockControlsInModalWin(false);

    if (!(dsoTransf.recordset.BOF && dsoTransf.recordset.EOF)) {
        if ((dsoTransf.recordset['Resultado'].value != null) && (dsoTransf.recordset['Resultado'].value != '')) {
            if (window.top.overflyGen.Alert(dsoTransf.recordset['Resultado'].value) == 0) {
                return null;
            }
        }
        else {
            _retMsg = window.top.overflyGen.Confirm('Transfer�ncia Banc�ria criada com sucesso: ' +
                      '\n  Financeiro Origem: ' + dsoTransf.recordset['FinanceiroDeID'].value +
                      '\n  Financeiro Destino: ' + dsoTransf.recordset['FinanceiroParaID'].value +
                      '\n\nDetalhar Financeiro Origem?');
            var detalhar = dsoTransf.recordset['FinanceiroDeID'].value;

            if (_retMsg != 1) {
                limparTransf();
                return null;
            }
            else if (_retMsg == 1) {
                //sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(glb_nEmpresaID, dsoTransf.recordset['FinanceiroDeID'].value));
                sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, detalhar);
                return true;
            }

        }
    }
}

/********************************************************************
Funcao do programador
Limpa os grids de dep�sito
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function limpagrids() {
    fg.Rows = 1;

    glb_nIncluir.length = 0;
    fg2.Rows = 1;
}

/********************************************************************
Funcao do programador
Ajusta os divs da modal
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustdivs() {

    if (selServicoBancarioID.value == 1412) {
        //window.resizeTo(820, 150);
        document.getElementById('divTransferencia').style.display = "block";
        document.getElementById('divControls').style.display = "none";
        document.getElementById('divFG').style.display = "none";
        document.getElementById('divFG2').style.display = "none";
    }
    else if (selServicoBancarioID.value == 1411) {
        //window.resizeTo(820, 520);
        document.getElementById('divTransferencia').style.display = "none";
        document.getElementById('divControls').style.display = "block";
        document.getElementById('divFG').style.display = "block";
        document.getElementById('divFG2').style.display = "block";
    }
    else {
        //window.resizeTo(820, 150);
        document.getElementById('divTransferencia').style.display = "none";
        document.getElementById('divControls').style.display = "none";
        document.getElementById('divFG').style.display = "none";
        document.getElementById('divFG2').style.display = "none";
    }
}

/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos combos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs() {
    var nContadeID = selOrigem.value;

    var sFiltro = ' (dbo.fn_Empresa_Matriz(dbo.fn_ContaBancaria_Empresa(' + nContadeID + '), 1, NULL) = ' +
                  '  dbo.fn_Empresa_Matriz(dbo.fn_ContaBancaria_Empresa(b.RelPesContaID), 1, NULL)) AND ' +
                  ' RelPesContaID <> ' + nContadeID + ' ';
    var dsoCmbDynamic01 = dsoDeposito;

    setConnection(dsoCmbDynamic01);

    dsoCmbDynamic01.SQL = 'SELECT DISTINCT 0 as fldIndex, 0 as fldID, SPACE(0) as fldName, SPACE(0) AS Ordem, SPACE(0) AS fldText ' +
                          'UNION ALL ' +
                          'SELECT DISTINCT 1 as fldIndex, b.RelPesContaID as fldID, ' +
                          'dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) fldName, ' +
	                      'LTRIM(RTRIM(STR(b.RelPesContaID))) AS Ordem, CHAR(45) + SPACE(1) + c.Fantasia as fldText ' +
	                      'FROM RelacoesPessoas a WITH(NOLOCK) ' +
                          'INNER JOIN RelacoesPessoas_Contas b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) ' +
	                      'INNER JOIN Pessoas c WITH(NOLOCK) ON a.SujeitoID = c.PessoaID ' +
	                      'WHERE (a.TipoRelacaoID = 24 AND a.EstadoID = 2 AND ' +
                          'b.TipoContaID = 1506 AND b.EstadoID = 2 AND ' +
                          sFiltro +
                          ') ORDER BY fldName';

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();

    var loop = 0;
    while (dsoCmbDynamic01.recordset.EOF == false) {
        campos[loop] = dsoCmbDynamic01.recordset["fldID"].value;
        campos[loop + 1] = dsoCmbDynamic01.recordset["fldText"].value;
        dsoCmbDynamic01.recordset.MoveNext();
        loop = loop + 2;
    }

}

/********************************************************************
Funcao disparada pelo programador
Preenchimento dos combos
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    var optionStr, optionValue;
    var aCmbsDynamics = [selDestino];
    var aDSOsDynamics = [dsoDeposito];
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var i;
    var lFirstRecord;
    var nQtdCmbs = 1;

    // Inicia o carregamento de combos dinamicos (selDestino)
    clearComboEx(['selDestino']);

    for (i = 0; i < nQtdCmbs; i++) {
        oldDataSrc = aCmbsDynamics[i].dataSrc;
        oldDataFld = aCmbsDynamics[i].dataFld;
        aCmbsDynamics[i].dataSrc = '';
        aCmbsDynamics[i].dataFld = '';
        lFirstRecord = true;
        while (!aDSOsDynamics[i].recordset.EOF) {
            optionStr = aDSOsDynamics[i].recordset['fldName'].value;
            optionValue = aDSOsDynamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDynamics[i].recordset.MoveNext();
            lFirstRecord = false;
        }
        aCmbsDynamics[i].dataSrc = oldDataSrc;
        aCmbsDynamics[i].dataFld = oldDataFld;
    }
    lockControlsInModalWin(false);
    adjustLabelsCombos();

    controlButtonsState();
    return null;
}

function ComboOrigem() {
    var dsoCmbDynamic02 = dsoDeposito;

    setConnection(dsoCmbDynamic02);
    dsoCmbDynamic02.SQL = 'Select 0 as fldID, SPACE(0) as fldName , 0 AS Ordem, SPACE(0) AS fldText ' +
                             'UNION ALL ' +
                             'SELECT b.RelPesContaID as fldID, dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) as fldName, ' +
                             'LTRIM(RTRIM(STR(b.RelPesContaID))) AS Ordem, CHAR(45) + SPACE(1) + c.Fantasia as fldText ' +
                                'FROM RelacoesPessoas a WITH(NOLOCK) ' +
                                        'INNER JOIN RelacoesPessoas_Contas b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) ' +
                                        'INNER JOIN Pessoas c WITH(NOLOCK) ON a.SujeitoID = c.PessoaID ' +
                                'WHERE (a.TipoRelacaoID = 24 AND a.SujeitoID = ' + glb_aEmpresaData[0] + ' AND a.EstadoID = 2 AND ' +
	                                    'b.TipoContaID = 1506 AND b.EstadoID = 2) ' +
                                'ORDER BY fldName';
    dsoCmbDynamic02.Refresh();

    var loop = 0;
    while (dsoCmbDynamic02.recordset.EOF == false) {
        campos2[loop] = dsoCmbDynamic02.recordset["fldID"].value;
        campos2[loop + 1] = dsoCmbDynamic02.recordset["fldText"].value;
        dsoCmbDynamic02.recordset.MoveNext();
        loop = loop + 2;
    }

}

/********************************************************************
Funcao disparada pelo programador
Verifica��o de campos para saber se o bot�o 'Gerar' pode ser travado/destravado
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function habilitaGerarTransf() {
    var habilita = 0;

    if (selOrigem.value != 0 && selOrigem.value != null) {
        if (selDestino.value != 0 && selDestino.value != null) {
            habilita = 1;
        }
    }

    if (habilita == 1)
        btnGerarTransf.disabled = false;
    else
        btnGerarTransf.disabled = true;
}

/********************************************************************
Funcao disparada pelo programador
limpa campos da transferencia           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function limparTransf() {
    selOrigem.value = 0;
    selDestino.value = 0;
    txtValor.value = '';
    selDestino.options.item(0).text = '';
    selDestino.options.item(0).value = '';
    selDestino.selectedIndex = 0;
    selOrigem.options.item(0).text = '';
    selOrigem.options.item(0).value = '';
    selOrigem.selectedIndex = 0;

    lblOrigem.innerText = 'Conta Origem';
    lblDestino.innerText = 'Conta Destino';
    habilitaGerarTransf();
}
