
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalativarfinanceirosHtml" name="modalativarfinanceirosHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalativarfinanceiros.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
       
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

  
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalativarfinanceiros.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, sCurrDateFormat, nContextoID

sCaller = ""
nContextoID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"

Response.Write vbcrlf

If (nContextoID = 9111) Then
	Response.Write "var glb_nTipoFinanceiroID = 1001;"
Else
	Response.Write "var glb_nTipoFinanceiroID = 1002;"
End If

'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalativarfinanceiros_ValidateEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalativarfinanceiros_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalativarfinanceirosKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalativarfinanceiros_BeforeEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modalativarfinanceirosDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalativarfinanceirosBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Atualizar linha de mensagens
js_fg_AfterRowColmodalativarfinanceiros (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<script ID="vbFunctions" LANGUAGE="vbscript">
<!--

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

//-->
</script>

</head>

<body id="modalativarfinanceirosBody" name="modalativarfinanceirosBody" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <!-- Div Controles -->
    <div id="divControls" name="divControls" class="divGeneral">
        <p id="lbldtInicio" name="lbldtInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtdtInicio" name="txtdtInicio" class="fldGeneral">
        <p id="lbldtFim" name="lbldtFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtdtFim" name="txtdtFim" class="fldGeneral">
        <p id="lblSaldoMaximo" name="lblSaldoMaximo" class="lblGeneral">Saldo M�ximo</p>
        <input type="text" id="txtSaldoMaximo" name="txtSaldoMaximo" class="fldGeneral">
        <p id="lblFormaPagamentoID" name="lblFormaPagamentoID" class="lblGeneral">Forma</p>
        <select id="selFormaPagamentoID" name="selFormaPagamentoID" class="fldGeneral">
<%
    Dim rsData, strSQL

    Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "Select 0 as fldID, '' as fldName , '0' AS Ordem " & _
             "UNION ALL " & _
             "Select DISTINCT a.ItemID as fldID, a.ItemAbreviado as fldName , LTRIM(RTRIM(STR(a.ordem))) AS Ordem " & _
             "From TiposAuxiliares_Itens a WITH(NOLOCK) " & _
             "Where (a.TipoID = 804 AND a.EstadoID = 2 AND a.Filtro LIKE '%(9130)%') " & _
             "Order By Ordem "

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData.EOF)
	    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34) & " "
		Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
	    rsData.MoveNext
	Wend
	
	rsData.Close
	Set rsData = Nothing
	
%>
        </select>
        <p id="lblEstadoDeID" name="lblEstadoDeID" class="lblGeneral">Est De</p>
        <select id="selEstadoDeID" name="selEstadoDeID" class="fldGeneral">
<%
    Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT b.RecursoID AS fldID, b.RecursoAbreviado AS fldName " & _
		"FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK) " & _
		"WHERE (a.ObjetoID = 331 AND a.TipoRelacaoID = 3 AND " & _
			"a.SujeitoID = b.RecursoID) " & _
		"ORDER BY a.Ordem"

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData.EOF)
	    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34) & " "
		Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
	    rsData.MoveNext
	Wend
	
	rsData.Close
	Set rsData = Nothing
	
%>
        </select>
        <p id="lblSituacaoCobrancaDeID" name="lblSituacaoCobrancaDeID" class="lblGeneral">Situa��o De</p>
        <select id="selSituacaoCobrancaDeID" name="selSituacaoCobrancaDeID" class="fldGeneral">
<%
    Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT 1 AS Indice, 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem " & _
		"UNION ALL " & _
		"SELECT 2 AS Indice, a.ItemID AS fldID, a.ItemFeminino AS fldName, a.Ordem AS Ordem " & _
		"FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _
		"WHERE (a.EstadoID = 2 AND a.TipoID = 802) " & _
		"ORDER BY Indice, Ordem"

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData.EOF)
	    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34) & " "
		Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
	    rsData.MoveNext
	Wend
	
	rsData.Close
	Set rsData = Nothing
	
%>
        </select>
        <p id="lblEstadoParaID" name="lblEstadoParaID" class="lblGeneral">Est Para</p>
        <select id="selEstadoParaID" name="selEstadoParaID" class="fldGeneral">
        </select>
        <p id="lblSituacaoCobrancaParaID" name="lblSituacaoCobrancaParaID" class="lblGeneral">Situa��o Para</p>
        <select id="selSituacaoCobrancaParaID" name="selSituacaoCobrancaParaID" class="fldGeneral">
<%
    Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT 1 AS Indice, 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem " & _
		"UNION ALL " & _
		"SELECT 2 AS Indice, a.ItemID AS fldID, a.ItemFeminino AS fldName, a.Ordem AS Ordem " & _
		"FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _
		"WHERE (a.EstadoID = 2 AND a.TipoID = 802) " & _
		"ORDER BY Indice, Ordem"

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData.EOF)
	    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34) & " "
		Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
	    rsData.MoveNext
	Wend
	
	rsData.Close
	Set rsData = Nothing
	
%>
        </select>
        <p class="lblGeneral" id="lblFiltro">Filtro</p>
        <input type="text" id="txtFiltro" name="txtFiltro" class="fldGeneral">
    </div>    

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
            
	<input type="button" id="btnFillGrid" name="btnFillGrid" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
	<input type="button" id="btnImprimir" name="btnImprimir" value="Imprimir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
</body>

</html>
