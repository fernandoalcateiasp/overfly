/********************************************************************
modalgerarfinanceiros.js

Library javascript para o modalgerarfinanceiros.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_nTipoFinanceiroID = 0;
var glb_bWindowLoading = true;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
var dsoGrid = new CDatatransport('dsoGrid');
var dsoCmbPessoas = new CDatatransport('dsoCmbPessoas');
var dsoCmbHPData = new CDatatransport('dsoCmbHPData');
var dsoCmbGrid01 = new CDatatransport('dsoCmbGrid01');
var dsoCurrData = new CDatatransport('dsoCurrData');
var dsoTaxaAtualizacao = new CDatatransport('dsoTaxaAtualizacao');
var dsoGrava = new CDatatransport('dsoGrava');
/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalgerarfinanceirosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalgerarfinanceirosDblClick(grid, Row, Col)
js_modalgerarfinanceirosKeyPress(KeyAscii)
js_modalgerarfinanceiros_ValidateEdit()
js_modalgerarfinanceiros_BeforeEdit(grid, row, col)
js_modalgerarfinanceiros_AfterEdit(Row, Col)
js_fg_AfterRowColmodalgerarfinanceiros (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalgerarfinanceirosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // Clonar financeiro
    if (glb_nFinanceiroID != 0)
        clonarFinanceiro();
    else
        loadDsoCmbHPData();
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {
    if (btnFindPessoa.src == glb_LUPA_IMAGES[1].src)
        return;

    loadCmbPessoa();
}

function loadCmbPessoa() {
    lockControlsInModalWin(true);
    // parametrizacao do dso dsoGrid
    setConnection(dsoCmbPessoas);
    var sFiltro = '';

    if (txtArgumento.value != '')
        sFiltro = ' AND Fantasia LIKE ' + '\'' + '%' + txtArgumento.value + '%' + '\'' + ' ';

    dsoCmbPessoas.SQL = 'SELECT TOP 50 PessoaID AS fldID, Fantasia AS fldName ' +
		'FROM Pessoas WITH(NOLOCK) ' +
		'WHERE EstadoID=2 AND TipoPessoaID IN (51, 52) ' + sFiltro +
		'ORDER BY fldName';

    dsoCmbPessoas.ondatasetcomplete = loadCmbPessoa_DSC;

    dsoCmbPessoas.Refresh();
}

function loadCmbPessoa_DSC() {
    fg.Rows = 1;
    clearComboEx(['selPessoaID']);

    if (!((dsoCmbPessoas.recordset.BOF) && (dsoCmbPessoas.recordset.EOF))) {
        dsoCmbPessoas.recordset.MoveFirst();
        while (!dsoCmbPessoas.recordset.EOF) {
            optionStr = dsoCmbPessoas.recordset['fldName'].value;
            optionValue = dsoCmbPessoas.recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            selPessoaID.add(oOption);
            dsoCmbPessoas.recordset.MoveNext();
        }
    }

    lockControlsInModalWin(false);
    if (selPessoaID.options.length == 1) {
        selPessoaID.selectedIndex = 0;
        adjustLabelsCombos();
    }
    else
        selPessoaID.selectedIndex = -1;

    selPessoaID.disabled = (selPessoaID.options.length == 0);
    adjustLabelsCombos();
    setupBtnsFromGridState();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Gerar Financeiros', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls

    adjustElementsInForm([['lblFuncionario', 'chkFuncionario', 3, 1, -10, -10],
						  ['lblArgumento', 'txtArgumento', 13, 1, -1],
					      ['btnFindPessoa', 'btn', 24, 1],
						  ['lblPessoaID', 'selPessoaID', 18, 1, -1],
						  ['lblDuplicata', 'txtDuplicata', 11, 1, -1],
						  ['lblFormaPagamentoID', 'selFormaPagamentoID', 7, 1, -1],
						  ['lbldtEmissao', 'txtdtEmissao', 10, 1, -1],
						  ['lbldtVencimento1', 'txtdtVencimento1', 10, 1, -1],
						  ['lblParcelas', 'txtParcelas', 3, 1, -1],
						  ['lblIncremento', 'txtIncremento', 3, 1, -1],
						  ['lblMesmoDia', 'chkMesmoDia', 3, 1, -1],
						  ['lblMoedaID', 'selMoedaID', 8, 2, -10],
						  ['lblValor', 'txtValor', 10, 2],
						  ['lblAtualizacao', 'txtAtualizacao', 6, 2],
						  ['lblEhImporte', 'chkEhImporte', 3, 2, -10],
						  ['lblUsoCotidiano', 'chkUsoCotidiano', 3, 2],
						  ['lblHistoricoPadraoID', 'selHistoricoPadraoID', 25, 2],
						  ['lblImpostoID', 'selImpostoID', 10, 2],
						  ['lblObservacao', 'txtObservacao', 19, 2]], null, null, true);

    txtArgumento.onkeydown = execPesq_OnKey;
    txtArgumento.maxLength = 20;

    selPessoaID.onchange = selCmbs_onchange;
    selPessoaID.disabled = true;

    txtDuplicata.onkeydown = execPesq_OnKey;
    txtDuplicata.maxLength = 15;

    txtdtEmissao.onkeydown = execPesq_OnKey;
    txtdtEmissao.onblur = txtdtEmissao_onblur;
    txtdtEmissao.maxLength = 10;

    selFormaPagamentoID.selectedIndex = -1;
    selFormaPagamentoID.onchange = selCmbs_onchange;

    txtParcelas.value = '1';
    txtParcelas.onkeypress = verifyNumericEnterNotLinked;
    txtParcelas.onkeydown = execPesq_OnKey;
    txtParcelas.onblur = txtParcelas_onblur;
    txtParcelas.setAttribute('thePrecision', 2, 1);
    txtParcelas.setAttribute('theScale', 0, 1);
    txtParcelas.setAttribute('verifyNumPaste', 1);
    txtParcelas.setAttribute('minMax', new Array(1, 99), 1);

    txtdtVencimento1.onkeydown = execPesq_OnKey;
    txtdtVencimento1.maxLength = 10;

    lblIncremento.style.visibility = 'hidden';
    txtIncremento.style.visibility = 'hidden';
    txtIncremento.onkeypress = verifyNumericEnterNotLinked;
    txtIncremento.onkeydown = execPesq_OnKey;
    txtIncremento.onblur = txtIncremento_onblur;
    txtIncremento.setAttribute('thePrecision', 2, 1);
    txtIncremento.setAttribute('theScale', 0, 1);
    txtIncremento.setAttribute('verifyNumPaste', 1);
    txtIncremento.setAttribute('minMax', new Array(0, 99), 1);

    lblMesmoDia.style.visibility = 'hidden';
    chkMesmoDia.style.visibility = 'hidden';
    chkMesmoDia.onclick = chkMesmoDia_onclick;

    if (selMoedaID.options.length == 1) {
        selMoedaID.selectedIndex = 0;
        adjustLabelsCombos();
    }
    else
        selMoedaID.selectedIndex = -1;

    selMoedaID.onchange = selCmbs_onchange;

    txtValor.onkeypress = verifyNumericEnterNotLinked;
    txtValor.onkeydown = execPesq_OnKey;
    txtValor.setAttribute('thePrecision', 11, 1);
    txtValor.setAttribute('theScale', 2, 1);
    txtValor.setAttribute('verifyNumPaste', 1);
    txtValor.setAttribute('minMax', new Array(1, 111111111.11), 1);

    txtAtualizacao.onkeypress = verifyNumericEnterNotLinked;
    txtAtualizacao.onkeydown = execPesq_OnKey;
    txtAtualizacao.setAttribute('thePrecision', 5, 1);
    txtAtualizacao.setAttribute('theScale', 2, 1);
    txtAtualizacao.setAttribute('verifyNumPaste', 1);
    txtAtualizacao.setAttribute('minMax', new Array(0, 100), 1);

    chkFuncionario.onclick = chkFuncionario_onclick;
    chkEhImporte.onclick = chkEhImporte_onclick;
    chkUsoCotidiano.onclick = chkEhImporte_onclick;

    selHistoricoPadraoID.selectedIndex = -1;
    selHistoricoPadraoID.onchange = selCmbs_onchange;

    selImpostoID.selectedIndex = -1;
    selImpostoID.onchange = selCmbs_onchange;
    lblImpostoID.style.visibility = 'hidden';
    selImpostoID.style.visibility = 'hidden';

    txtObservacao.maxLength = 30;

    adjustLabelsCombos();

    // Ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = selImpostoID.offsetLeft + selImpostoID.offsetWidth + ELEM_GAP;
        height = selImpostoID.offsetTop + selImpostoID.offsetHeight;
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + parseInt(btnFillGrid.currentStyle.height, 10) + ELEM_GAP + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // Reposiciona botao OK
    with (btnOK) {
        style.top = divControls.offsetTop + selImpostoID.offsetTop + parseInt(btnFillGrid.currentStyle.height, 10) + ELEM_GAP;
        style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
        value = 'Financeiros';
        title = 'Gerar financeiros?';
    }

    with (btnFillGrid) {
        style.width = btnOK.currentStyle.width;
        style.height = btnOK.currentStyle.height;
        style.top = btnOK.currentStyle.top;
        style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnFillGrid.currentStyle.width, 10) - 4;
    }

    // Esconde botao cancel
    with (btnCanc) {
        style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;

    var aContexto = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCmbCurrDataInControlBar(' + '\'' + 'sup' + '\'' + ', 1);');

    // Pagamento
    if (aContexto[1] == 9111)
        glb_nTipoFinanceiroID = 1001;
        // Recebimento    
    else if (aContexto[1] == 9112)
        glb_nTipoFinanceiroID = 1002;
}

function txtParcelas_onblur() {
    var sVisibility = 'hidden';

    txtIncremento.value = '';
    if (!isNaN(txtParcelas.value)) {
        if (parseInt(txtParcelas.value) >= 2) {
            sVisibility = 'visible';
            txtIncremento.value = '30';
        }
    }

    lblIncremento.style.visibility = sVisibility;
    txtIncremento.style.visibility = sVisibility;

    txtIncremento_onblur();
}

function txtdtEmissao_onblur() {
    txtdtVencimento1.value = txtdtEmissao.value;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function execPesq_OnKey() {
    if (event.keyCode == 13) {
        fg.Rows = 1;
        // ajusta estado dos botoes
        setupBtnsFromGridState();

        if (this.id == 'txtArgumento')
            btnLupaClicked(btnFindPessoa);
        else
            listar();
    }
    else {
        fg.Rows = 1;
        setupBtnsFromGridState();
    }
}

function txtIncremento_onblur() {
    var sVisibility = 'hidden';

    if (trimStr(txtIncremento.value) == '30') {
        lblMesmoDia.style.visibility = 'inherit';
        chkMesmoDia.style.visibility = 'inherit';
        window.focus();
        chkMesmoDia.focus();
    }
    else {
        lblMesmoDia.style.visibility = sVisibility;
        chkMesmoDia.style.visibility = sVisibility;
        chkMesmoDia.checked = false;
    }
}

function chkMesmoDia_onclick() {
    fg.Rows = 1;
    setupBtnsFromGridState();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        saveDataInGrid();
    }
    else if (controlID == 'btnFillGrid') {
        listar();
    }
        // codigo privado desta janela
    else {
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    // @@ atualizar interface aqui
    setupBtnsFromGridState();

    lockControlsInModalWin(true);

    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState() {
    var i;
    var bHasRowsInGrid = fg.Rows > 1;

    btnOK.disabled = true;

    if (bHasRowsInGrid) {
        for (i = 1; i < fg.Rows; i++) {
            if (getCellValueByColKey(fg, 'OK', i) != 0) {
                btnOK.disabled = false;
                break;
            }
        }
    }
}

function loadDsoCmbHPData() {
    lockControlsInModalWin(true);
    glb_nDSOs = 4;
    // parametrizacao do dso dsoGrid

    var sFiltro = '';
    var sFiltroUsoCotidiano = '';
    var nTipoLancamentoID;

    if (chkEhImporte.checked)
        nTipoLancamentoID = 1573;
    else {
        if (glb_nTipoFinanceiroID == 1001)
            nTipoLancamentoID = 1571;
        else if (glb_nTipoFinanceiroID == 1002)
            nTipoLancamentoID = 1572;
    }

    sFiltro = ' AND b.TipoLancamentoID = ' + nTipoLancamentoID + ' ';

    if (glb_nTipoFinanceiroID == 1001)
        sFiltro += ' AND a.TipoLancamentoID = 1571 ';
    else if (glb_nTipoFinanceiroID == 1002)
        sFiltro += ' AND a.TipoLancamentoID = 1572 ';

    if (chkUsoCotidiano.checked)
        sFiltroUsoCotidiano = ' AND a.UsoCotidiano = 1 ';

    setConnection(dsoCmbHPData);
    dsoCmbHPData.SQL = 'SELECT DISTINCT a.HistoricoPadraoID as fldID, a.HistoricoPadrao as fldName, ' +
					'dbo.fn_HistoricoPadrao_TipoDetalhamento(a.HistoricoPadraoID, ' + nTipoLancamentoID + ', 1543) AS Imposto ' +
                    'FROM HistoricosPadrao a WITH(NOLOCK), HistoricosPadrao_Contas b WITH(NOLOCK) ' +
                    'WHERE (a.EstadoID = 2 AND a.HistoricoPadraoID = b.HistoricoPadraoID AND (b.Pedido = 1 OR b.Financeiro = 1) ' + sFiltro + sFiltroUsoCotidiano + ') ' +
                    'ORDER BY fldName';

    dsoCmbHPData.ondatasetcomplete = loadDsoCmbHPData_DSC;

    dsoCmbHPData.Refresh();

    setConnection(dsoCmbGrid01);
    dsoCmbGrid01.SQL = 'SELECT ItemID AS fldID, ItemAbreviado AS fldName ' +
			 'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
			 'WHERE (EstadoID = 2 AND TipoID=804) ' +
			 'ORDER BY Ordem';

    dsoCmbGrid01.ondatasetcomplete = loadDsoCmbHPData_DSC;
    dsoCmbGrid01.Refresh();

    dsoCurrData.URL = SYS_PAGESURLROOT + '/serversidegenEx/currdate.aspx?currDateFormat=' + escape(DATE_FORMAT);
    dsoCurrData.ondatasetcomplete = loadDsoCmbHPData_DSC;
    dsoCurrData.Refresh();

    setConnection(dsoTaxaAtualizacao);
    dsoTaxaAtualizacao.SQL = 'SELECT Taxas.TaxaAtualizacao ' +
		'FROM RelacoesPesRec Empresas WITH(NOLOCK), RelacoesPesRec_Financ Taxas WITH(NOLOCK)  ' +
		'WHERE (Empresas.SujeitoID = ' + glb_nEmpresaID + ' AND Empresas.ObjetoID = 999 AND  ' +
			'Empresas.TipoRelacaoID = 12 AND Empresas.RelacaoID = Taxas.RelacaoID AND ' +
			'Taxas.MoedaID = ' + selMoedaID.value + ')';

    dsoTaxaAtualizacao.ondatasetcomplete = loadDsoCmbHPData_DSC;
    dsoTaxaAtualizacao.Refresh();
}

function loadDsoCmbHPData_DSC() {
    glb_nDSOs--;

    if (glb_nDSOs == 0) {
        fg.Rows = 1;
        clearComboEx(['selHistoricoPadraoID']);

        if (!((dsoCmbHPData.recordset.BOF) && (dsoCmbHPData.recordset.EOF))) {
            dsoCmbHPData.recordset.MoveFirst();
            while (!dsoCmbHPData.recordset.EOF) {
                optionStr = dsoCmbHPData.recordset['fldName'].value;
                optionValue = dsoCmbHPData.recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                oOption.bShowImposto = dsoCmbHPData.recordset['Imposto'].value;
                selHistoricoPadraoID.add(oOption);
                dsoCmbHPData.recordset.MoveNext();
            }
        }

        if (!(dsoCurrData.recordset.BOF && dsoCurrData.recordset.EOF)) {
            if (trimStr(txtdtEmissao.value) == '')
                txtdtEmissao.value = dsoCurrData.recordset['currDate'].value;

            if (trimStr(txtdtVencimento1.value) == '')
                txtdtVencimento1.value = dsoCurrData.recordset['currDate'].value;
        }

        selHistoricoPadraoID.selectedIndex = -1;

        // Ajusta o Historico Pardrao
        if (glb_nFinanceiroID != 0) {
            nHistoricoPadraoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				'dsoSup01.recordset[' + '\'' + 'HistoricoPadraoID' + '\'' + '].value');

            selOptByValueInSelect(getHtmlId(), 'selHistoricoPadraoID', nHistoricoPadraoID);

            // Ajusta o Vencimento
            if (chkDataEx(txtdtEmissao.value) == true)
                txtdtVencimento1.value = window.top.DateToStr(window.top.daysSUM(txtdtEmissao.value, nPrazoPagamento), DATE_FORMAT);
        }

        selHistoricoPadraoID.disabled = (selHistoricoPadraoID.options.length == 0);
        adjustLabelsCombos();
        setupBtnsFromGridState();

        if (glb_nTipoFinanceiroID == 1002)
            dsoTaxaAtualizacao_DSC();
        else
            txtAtualizacao.value = '0';

        lockControlsInModalWin(false);

        if (glb_bWindowLoading) {
            // lockBtnLupa(btnFindPessoa, false);
            glb_bWindowLoading = false;
            showExtFrame(window, true);
            window.focus();

            if (!txtArgumento.disabled)
                txtArgumento.focus();

            if (glb_nFinanceiroID != 0)
                listar();
        }
    }
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    var aGrid = null;
    var i = 0;
    var bGeraDuplicata = false;
    var sDuplicata = trimStr(txtDuplicata.value);
    var nValor = trimStr(txtValor.value);
    var sValor = '';

    if (nValor == '')
        sValor = 'NULL ';
    else
        sValor = 'CONVERT(NUMERIC(11,2), ' + nValor + ') ';

    lockControlsInModalWin(true);

    glb_nDSOs = 1;

    // zera o grid
    fg.Rows = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    if (!chkFuncionario.checked) {
        if ((parseInt(txtIncremento.value, 10) == 0) && !(isNaN(parseInt(txtDuplicata.value, 10))))
            bGeraDuplicata = true;

        dsoGrid.SQL = 'SELECT 1 AS Indice, ' + selPessoaID.value + ' AS PessoaID,' +
			'\'' + selPessoaID.options[selPessoaID.selectedIndex].innerText + '\'' + ' AS Pessoa, ' +
			 '\'' + sDuplicata + '\'' + ' AS Duplicata, ' +
			selFormaPagamentoID.value + ' AS FormaPagamentoID, CONVERT(DATETIME,CONVERT(VARCHAR(10),' + '\'' + putDateInMMDDYYYY2(txtdtVencimento1.value) + '\'' +
				',' + DATE_SQL_PARAM + '),101) AS Vencimento, ' +
				sValor + ' AS Valor, ' + '\'' + txtObservacao.value + '\'' + ' AS Observacao ';

        for (i = 2; i <= parseInt(txtParcelas.value, 10) ; i++) {
            if (bGeraDuplicata)
                sDuplicata = parseInt(sDuplicata, 10) + 1;

            dsoGrid.SQL += 'UNION ALL SELECT ' + i + ' AS Indice, ' + selPessoaID.value + ' AS PessoaID,' +
			'\'' + selPessoaID.options[selPessoaID.selectedIndex].innerText + '\'' + ' AS Pessoa, ' +
			'\'' + sDuplicata + '\'' + ' AS Duplicata, ' +
				selFormaPagamentoID.value + ' AS FormaPagamentoID, ';

            if (chkMesmoDia.checked)
                dsoGrid.SQL += 'CONVERT(DATETIME, DATEADD(mm, (' + i + ' - 1), ' + '\'' +
					putDateInMMDDYYYY2(txtdtVencimento1.value) + '\'' + '),101) AS Vencimento, ';
            else
                dsoGrid.SQL += 'CONVERT(DATETIME, DATEADD(dd, (' + txtIncremento.value + ' * (' + i + ' - 1)), ' + '\'' +
					putDateInMMDDYYYY2(txtdtVencimento1.value) + '\'' + '),101) AS Vencimento, ';

            dsoGrid.SQL += sValor + ' AS Valor, ' + '\'' + txtObservacao.value + '\'' + ' AS Observacao ';
        }
    }
    else {
        dsoGrid.SQL = 'SELECT 1 AS Indice, a.PessoaID AS PessoaID, a.Fantasia AS Pessoa, ' +
			 '\'' + trimStr(txtDuplicata.value) + '\'' + ' AS Duplicata, ' +
			selFormaPagamentoID.value + ' AS FormaPagamentoID, CONVERT(DATETIME,CONVERT(VARCHAR(10),' + '\'' + putDateInMMDDYYYY2(txtdtVencimento1.value) + '\'' +
				',' + DATE_SQL_PARAM + '),101) AS Vencimento, ' +
				sValor + ' AS Valor, ' + '\'' + txtObservacao.value + '\'' + ' AS Observacao ' +
			'FROM Pessoas a WITH(NOLOCK) ' +
			'WHERE (a.EstadoID=2 AND dbo.fn_Pessoa_Tipo(a.PessoaID, ' + glb_aEmpresaData[0] + ', 2, 1536)=1) ' +
			'ORDER BY a.Fantasia';
    }

    dsoGrid.ondatasetcomplete = fillGridData_DSC;

    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;

    if (glb_nDSOs > 0)
        return null;

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '10';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    var aHoldCols = null;

    if (chkFuncionario.checked)
        aHoldCols = [0];
    else
        aHoldCols = [0, 1];

    headerGrid(fg, ['PessoaID',
				   'Pessoa',
				   'Duplicata',
				   'Forma',
				   'Vencimento',
				   'Valor',
				   'Observacao'], aHoldCols);

    fillGridMask(fg, dsoGrid, ['PessoaID',
							 'Pessoa*',
							 'Duplicata',
							 'FormaPagamentoID',
							 'Vencimento',
							 'Valor',
							 'Observacao'],
							 ['', '', '', '', '99/99/9999', '999999999.99', ''],
							 ['', '', '', '', dTFormat, '###,###,##0.00', '']);

    gridHasTotalLine(fg, 'Total', 0XC0C0C0, null, true, [[getColIndexByColKey(fg, 'FormaPagamentoID'), '####0', 'C'],
		[getColIndexByColKey(fg, 'Valor'), '###,###,##0.00', 'S']]);

    paintCellsSpecialyReadOnly();

    if (fg.Rows > 1) {
        fg.TextMatrix(1, getColIndexByColKey(fg, 'PessoaID')) = 'Total';
        fg.TextMatrix(1, getColIndexByColKey(fg, 'FormaPagamentoID')) = gridTotais(1);
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor')) = gridTotais(2);
    }

    insertcomboData(fg, getColIndexByColKey(fg, 'FormaPagamentoID'), dsoCmbGrid01, 'fldName', 'fldID');

    fg.Redraw = 0;

    alignColsInGrid(fg, [0, 5]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.FrozenCols = 1;

    fg.ExplorerBar = 5;

    if ((fg.Rows > 1) && (fg.Cols > 1))
        fg.Col = getColIndexByColKey(fg, 'Valor');

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        fg.Editable = true;
        window.focus();
        fg.focus();
    }
    else {
        ;
    }

    fg.Redraw = 2;

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

function gridTotais(nTipoTotalizacao) {
    var retVal = 0;
    var i = 0;
    var nValor = 0;

    for (i = 2; i < fg.Rows; i++) {
        nValor = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor'));

        if (nValor > 0) (nTipoTotalizacao == 1 ? retVal++ : retVal += nValor);
    }

    return retVal;
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var sEventos = '';
    var bGeraFinanceiro = false;
    var nBytesAcum = 0;
    glb_aSendDataToServer = [];

    var nRelPesContaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'dsoSup01.recordset[' + '\'' + 'RelPesContaID' + '\'' + '].value');

    lockControlsInModalWin(true);
    strPars = '';
    for (i = 2; i < fg.Rows; i++) {
        if ((getCellValueByColKey(fg, 'Valor', i) == '') ||
			 (getCellValueByColKey(fg, 'Valor', i) <= 0))
            continue;

        nBytesAcum = strPars.length;
        bGeraFinanceiro = true;

        if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
            nBytesAcum = 0;
            if (strPars != '') {
                glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                strPars = '';
            }

            strPars += '?nEmpresaID=' + escape(glb_nEmpresaID);
            strPars += '&nTipoFinanceiroID=' + escape(glb_nTipoFinanceiroID);
            strPars += '&dtEmissao=' + escape(putDateInMMDDYYYY2(txtdtEmissao.value));
            strPars += '&nMoedaID=' + escape(selMoedaID.value);
            strPars += '&nTaxaAtualizacao=' + escape(txtAtualizacao.value);
            strPars += '&bEhImporte=' + escape((chkEhImporte.checked ? 1 : 0));
            strPars += '&nHistoricoPadraoID=' + escape(selHistoricoPadraoID.value);
            strPars += '&nImpostoID=' + escape(selImpostoID.value);
            strPars += '&nUsuarioID=' + escape(glb_nUserID);
            strPars += '&nRelPesContaID=' + escape(nRelPesContaID);
        }

        strPars += '&nPessoaID=' + escape(getCellValueByColKey(fg, 'PessoaID', i));
        strPars += '&nDuplicata=' + escape(getCellValueByColKey(fg, 'Duplicata', i));
        strPars += '&nFormaPagamentoID=' + escape(getCellValueByColKey(fg, 'FormaPagamentoID', i));
        strPars += '&dtVencimento=' + escape(putDateInMMDDYYYY2(getCellValueByColKey(fg, 'Vencimento', i)));
        strPars += '&nValor=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')));
        strPars += '&sObservacao=' + escape(getCellValueByColKey(fg, 'Observacao', i));
    }

    if (bGeraFinanceiro)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

    if (!bGeraFinanceiro) {
        if (window.top.overflyGen.Alert('Preencha algum valor!') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
    else {
        glb_nPointToaSendDataToServer = 0;
        sendDataToServer();
    }
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/gerarfinanceiros.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else
            saveDataInGrid_DSC();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

function clonarFinanceiro() {
    lockControlsInModalWin(true);

    nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');
    sFantasia = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'selPessoaID.options[selPessoaID.selectedIndex].innerText');
    nFormaPagamentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'dsoSup01.recordset[' + '\'' + 'FormaPagamentoID' + '\'' + '].value');
    nPrazoPagamento = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'dsoSup01.recordset[' + '\'' + 'PrazoPagamento' + '\'' + '].value');
    nMoedaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'dsoSup01.recordset[' + '\'' + 'MoedaID' + '\'' + '].value');
    nValor = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'dsoSup01.recordset[' + '\'' + 'Valor' + '\'' + '].value');
    nAtualizacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'dsoSup01.recordset[' + '\'' + 'TaxaAtualizacao' + '\'' + '].value');
    bEhImporte = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'dsoSup01.recordset[' + '\'' + 'EhImporte' + '\'' + '].value');

    clearComboEx(['selPessoaID']);

    // Ajusta o combo de Pessoa
    var oOption = document.createElement("OPTION");
    oOption.text = sFantasia;
    oOption.value = nPessoaID;
    selPessoaID.add(oOption);
    selPessoaID.selectedIndex = 0;

    // Ajusta FormaPagamentoID
    selOptByValueInSelect(getHtmlId(), 'selFormaPagamentoID', nFormaPagamentoID);

    // Ajusta MoedaID
    selOptByValueInSelect(getHtmlId(), 'selMoedaID', nMoedaID);

    // Ajusta o valor
    txtValor.value = nValor;

    // Ajusta a atualizacao
    txtAtualizacao.value = nAtualizacao;

    // Ajusta o EhImporte
    chkEhImporte.checked = (bEhImporte == true);

    chkEhImporte_onclick();
}

function chkFuncionario_onclick() {
    var sVisibility = (chkFuncionario.checked ? 'hidden' : 'inherit');
    fg.Rows = 1;

    txtParcelas_onblur();
    lblArgumento.style.visibility = sVisibility;
    txtArgumento.style.visibility = sVisibility;

    lblPessoaID.style.visibility = sVisibility;
    selPessoaID.style.visibility = sVisibility;
    btnFindPessoa.style.visibility = sVisibility;
    lblParcelas.style.visibility = sVisibility;
    txtParcelas.style.visibility = sVisibility;

    if ((sVisibility == 'hidden') ||
		 ((sVisibility == 'inherit') && (lblIncremento.style.visibility != 'hidden'))) {
        lblIncremento.style.visibility = sVisibility;
        txtIncremento.style.visibility = sVisibility;
        lblMesmoDia.style.visibility = sVisibility;
        chkMesmoDia.style.visibility = sVisibility;
    }
}

function chkEhImporte_onclick() {
    fg.Rows = 1;
    setupBtnsFromGridState();
    lockControlsInModalWin(true);
    loadDsoCmbHPData();
}

function selCmbs_onchange() {
    var bShow = false;

    if (this.id == 'selHistoricoPadraoID') {
        bShow = selHistoricoPadraoID.options[selHistoricoPadraoID.selectedIndex].getAttribute('bShowImposto', 1);
        selImpostoID.selectedIndex = -1;
        lblImpostoID.style.visibility = (bShow ? 'inherit' : 'hidden');
        selImpostoID.style.visibility = (bShow ? 'inherit' : 'hidden');
    }
    else if (this.id == 'selMoedaID') {
        if (glb_nTipoFinanceiroID == 1002)
            loadTaxaAtualizacao();
        else
            txtAtualizacao.value = '0';
    }

    fg.Rows = 1;

    setupBtnsFromGridState();
    adjustLabelsCombos();
    return null;
}

function loadTaxaAtualizacao() {
    lockControlsInModalWin(true);
    setConnection(dsoTaxaAtualizacao);
    dsoTaxaAtualizacao.SQL = 'SELECT Taxas.TaxaAtualizacao ' +
		'FROM RelacoesPesRec Empresas WITH(NOLOCK), RelacoesPesRec_Financ Taxas WITH(NOLOCK)  ' +
		'WHERE (Empresas.SujeitoID = ' + glb_nEmpresaID + ' AND Empresas.ObjetoID = 999 AND  ' +
			'Empresas.TipoRelacaoID = 12 AND Empresas.RelacaoID = Taxas.RelacaoID AND ' +
			'Taxas.MoedaID = ' + selMoedaID.value + ')';

    dsoTaxaAtualizacao.ondatasetcomplete = dsoTaxaAtualizacao_DSC;
    dsoTaxaAtualizacao.Refresh();
}

function dsoTaxaAtualizacao_DSC() {
    if (!((dsoTaxaAtualizacao.recordset.BOF) && (dsoTaxaAtualizacao.recordset.EOF)))
        txtAtualizacao.value = dsoTaxaAtualizacao.recordset['TaxaAtualizacao'].value;
    else
        txtAtualizacao.value = '0';

    lockControlsInModalWin(false);
}

function adjustLabelsCombos() {
    setLabelOfControl(lblPessoaID, selPessoaID.value);
    setLabelOfControl(lblHistoricoPadraoID, selHistoricoPadraoID.value);
    setLabelOfControl(lblFormaPagamentoID, selFormaPagamentoID.value);
    setLabelOfControl(lblMoedaID, selMoedaID.value);
    setLabelOfControl(lblImpostoID, selImpostoID.value);
}

function listar() {
    var sMsg = '';
    var oControl = null;

    txtdtEmissao.value = trimStr(txtdtEmissao.value);

    if ((!chkFuncionario.checked) && (selPessoaID.selectedIndex == -1)) {
        oControl = txtArgumento;
        sMsg = 'Pessoa\n';
    }

    if (chkDataEx(txtdtEmissao.value) != true) {
        oControl = txtdtEmissao;
        sMsg += 'Data de emiss�o\n';
    }

    if (selFormaPagamentoID.selectedIndex == -1) {
        oControl = selFormaPagamentoID;
        sMsg += 'Forma de pagamento\n';
    }

    if ((!chkFuncionario.checked) && (trimStr(txtParcelas.value) == '')) {
        oControl = txtParcelas;
        sMsg += 'N�mero de parcelas\n';
    }

    if (chkDataEx(txtdtVencimento1.value) != true) {
        oControl = txtdtVencimento1;
        sMsg += 'Data de vencimento\n';
    }

    if ((!chkFuncionario.checked) && (txtIncremento.style.visibility != 'hidden') && (trimStr(txtIncremento.value) == '')) {
        oControl = txtIncremento;
        sMsg += 'Incremento\n';
    }

    if (selMoedaID.selectedIndex == -1) {
        oControl = selMoedaID;
        sMsg += 'Moeda\n';
    }
    /*	
        if ((!chkFuncionario.checked)&&(trimStr(txtValor.value) == ''))
        {
            oControl = txtValor;
            sMsg += 'Valor\n';
        }
    */
    if (trimStr(txtAtualizacao.value) == '') {
        oControl = txtAtualizacao;
        sMsg += 'Atualiza��o\n';
    }

    if (selHistoricoPadraoID.selectedIndex == -1) {
        oControl = selHistoricoPadraoID;
        sMsg += 'Hist�rico Padr�o\n';
    }

    if ((selImpostoID.style.visibility == 'inherit') && (selImpostoID.selectedIndex == -1)) {
        oControl = selImpostoID;
        sMsg += 'Imposto\n';
    }

    if (sMsg == '') {
        if (putDateInYYYYMMDD(txtdtVencimento1.value) < putDateInYYYYMMDD(txtdtEmissao.value)) {
            oControl = txtdtVencimento1;
            sMsg = 'A data de vencimento deve ser maior ou igual a data de emiss�o.';
        }
    }
    else
        sMsg = 'Preencha corretamente os campos:\n\n' + sMsg;

    if (sMsg != '') {
        if (window.top.overflyGen.Alert(sMsg) == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
        if (oControl.disabled == false)
            oControl.focus();

        return null;
    }

    refreshParamsAndDataAndShowModalWin(false);
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    lockControlsInModalWin(false);

    if ((dsoGrava.recordset['Resultado'].value != null) && (dsoGrava.recordset['Resultado'].value != '')) {
        if (window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0)
            return null;
    }
    else {
        var sMsg = '';
        sMsg = 'Financeiro' + (fg.Rows > 1 ? 's' : '') + ' gerado' + (fg.Rows > 1 ? 's' : '');
        fg.Rows = 1;
        if (window.top.overflyGen.Alert(sMsg) == 0)
            return null;
    }

    setupBtnsFromGridState();
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly() {
    paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerarfinanceirosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerarfinanceirosDblClick(grid, Row, Col) {
    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if (getColIndexByColKey(grid, 'OK') != Col)
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);

    glb_PassedOneInDblClick = true;

    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 1; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarfinanceirosKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarfinanceiros_ValidateEdit() {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarfinanceiros_BeforeEdit(grid, row, col) {
    if (fg.Row == 1)
        fg.Row = 2;

    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if (fldDet == null)   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ((fldDet[0] == 200) || (fldDet[0] == 129))  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarfinanceiros_AfterEdit(Row, Col) {
    var nColValor = getColIndexByColKey(fg, 'Valor');

    if (Col == nColValor) {
        fg.TextMatrix(Row, nColValor) = treatNumericCell(fg.TextMatrix(Row, nColValor));

        fg.TextMatrix(1, getColIndexByColKey(fg, 'PessoaID')) = 'Total';
        fg.TextMatrix(1, getColIndexByColKey(fg, 'FormaPagamentoID')) = gridTotais(1);
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor')) = gridTotais(2);
    }

    setupBtnsFromGridState();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalgerarfinanceiros(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return true;

    setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************