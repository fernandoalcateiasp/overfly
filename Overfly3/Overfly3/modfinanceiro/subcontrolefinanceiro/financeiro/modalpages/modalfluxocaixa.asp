<%@  language="VBSCRIPT" enablesessionstate="False" %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalfluxocaixaHtml" name="modalfluxocaixaHtml">

<head>

    <title></title>

    <%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalfluxocaixa.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
       
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

  
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalfluxocaixa.js" & Chr(34) & "></script>" & vbCrLf
    %>

    <%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nEmpresaID, nUserID, sCaller, sCurrDateFormat
Dim rsData, strSQL

nUserID = 0
nEmpresaID = 0
sCaller = ""
sCurrDateFormat = ""

For i = 1 To Request.QueryString("nEmpresaID").Count
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT DISTINCT b.EmpresaID, d.MoedaID, e.SimboloMoeda " & _
		 "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), RelacoesPesRec c WITH(NOLOCK), RelacoesPesRec_Moedas d WITH(NOLOCK), Conceitos e WITH(NOLOCK) " & _
		 "WHERE ((a.SujeitoID = " & CStr(nUserID) & ") AND (a.TipoRelacaoID = 11) AND (a.ObjetoID = 999) AND (a.RelacaoID = b.RelacaoID) AND " & _
			"(b.EmpresaID = c.SujeitoID) AND (c.TipoRelacaoID = 12) AND (c.ObjetoID = 999) AND (c.RelacaoID = d.RelacaoID) AND " & _
			"(d.MoedaID = e.ConceitoID)) " & _
		 "ORDER BY b.EmpresaID, e.SimboloMoeda"

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write "var glb_aMoedasEmpresas = new Array();"
Response.Write vbcrlf

i = 0
While Not rsData.EOF
        
    'glb_arrayEstado[i] = new Array(12, 'Bahia', 'BA');
        
    Response.Write "glb_aMoedasEmpresas[" & CStr(i) & "] = new Array("
    Response.Write CStr(rsData.Fields("EmpresaID").Value)
    Response.Write ","
    Response.Write CStr(rsData.Fields("MoedaID").Value)
    Response.Write ","
    Response.Write Chr(34)
    Response.Write CStr(rsData.Fields("SimboloMoeda").Value)
    Response.Write Chr(34)
    Response.Write ");"
    Response.Write vbcrlf
    
    rsData.MoveNext
    i = i + 1
Wend

Set rsData = Nothing

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
    %>

    <script id="wndJSProc" language="javascript">
<!--

    //-->
    </script>

    <!-- //@@ Eventos de grid -->
    <!-- fg -->

    <script language="javascript" for="fg" event="AfterRowColChange">
<!--
    fg_AfterRowColChange();
    //-->
    </script>
    <script language="javascript" for="fg" event="BeforeRowColChange">
<!--
    js_fg_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
    //-->
    </script>

    <script language="javascript" for="fg" event="DblClick">
<!--
    js_fg_DblClick();
    //-->
    </script>

</head>

<body id="modalfluxocaixaBody" name="modalfluxocaixaBody" language="javascript" onload="return window_onload()">

    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>

    <!-- Div dos controles -->
    <div id="divControles" name="divControles" class="divGeneral">
        <p id="lblEmpresas" name="lblEmpresas" class="lblGeneral">Empresas</p>
        <select id="selEmpresas" name="selEmpresas" class="fldGeneral" multiple>
            <%
	Set rsData = Server.CreateObject("ADODB.Recordset")

	strSQL = "SELECT DISTINCT b.EmpresaID, c.Fantasia " & _
			"FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " & _
			"WHERE ((a.SujeitoID = " & CStr(nUserID) & ") AND (a.TipoRelacaoID = 11) AND (a.ObjetoID = 999) AND " & _ 
				"(a.RelacaoID = b.RelacaoID) AND (b.EmpresaID = c.PessoaID)) " & _
			"ORDER BY c.Fantasia"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	While (Not rsData.EOF)
		Response.Write "<option value =" & rsData.Fields("EmpresaID").Value 
		
		If (CLng(nEmpresaID) = CLng(rsData.Fields("EmpresaID").Value)) Then
			Response.Write " SELECTED "
		End If
		
		Response.Write ">" & rsData.Fields("Fantasia").Value & "</option>" & _
			chr(13) & chr(10)

		rsData.MoveNext
	WEnd

	rsData.Close
	Set rsData = Nothing
            %>
        </select>   
        <p id="lblMoeda" name="lblMoeda" class="lblGeneral">Moeda</p>
        <select id="selMoeda" name="selMoeda" class="fldGeneral"></select>
        <p id="lblData" name="lblData" class="lblGeneral">Data</p>
        <input type="text" id="txtData" name="txtData" class="fldGeneral"></input>
        <p id="lblEstadoC" name="lblEstadoC" class="lblGeneral">C</p>
        <input type="checkbox" id="chkEstadoC" name="chkEstadoC" class="fldGeneral" title="Incluir os financeiros em C tamb�m?">
        <input type="button" id="btnFillGrid" name="btnFillGrid" value="Listar" language="javascript" onclick="return btn_onclick(this)" class="btns"></input>
    </div>

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" viewastext>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>

    <input type="button" id="btnOK" name="btnOK" value="Gravar" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" language="javascript" onclick="return btn_onclick(this)" class="btns">
</body>

</html>
