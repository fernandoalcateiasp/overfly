/********************************************************************
modalestornarfinanceiro.js

Library javascript para o modalestornarfinanceiro.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_nTipoFinanceiroID = 0;
var glb_bWindowLoading = true;
var glb_nSaldoFinanceiro = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';

var dsoData = new CDatatransport('dsoData');
var dsoCmbPessoas = new CDatatransport('dsoCmbPessoas');
var dsoCurrData = new CDatatransport('dsoCurrData');
var dsoGrava = new CDatatransport('dsoGrava');  
/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsState()
saveData()
saveData_DSC()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalestornarfinanceiroBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	loadDsoData();
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
    if (btnFindPessoa.src == glb_LUPA_IMAGES[1].src)
        return;
	
	loadCmbPessoa();
}

function loadCmbPessoa()
{
	lockControlsInModalWin(true);
    // parametrizacao do dso dsoGrid
    setConnection(dsoCmbPessoas);
    var sFiltro = '';
    
    if (txtArgumento.value != '')
		sFiltro = ' AND Fantasia LIKE ' + '\'' + '%' + txtArgumento.value + '%' + '\'' + ' ';

	dsoCmbPessoas.SQL = 'SELECT TOP 50 PessoaID AS fldID, Fantasia AS fldName ' +
		'FROM Pessoas WITH(NOLOCK) ' +
		'WHERE EstadoID=2 AND TipoPessoaID IN (51, 52) ' + sFiltro +
		'ORDER BY fldName';

    dsoCmbPessoas.ondatasetcomplete = loadCmbPessoa_DSC;
    
	dsoCmbPessoas.Refresh();
}

function loadCmbPessoa_DSC()
{
	clearComboEx(['selPessoaID']);

	if (! ((dsoCmbPessoas.recordset.BOF)&&(dsoCmbPessoas.recordset.EOF)) )
	{
		dsoCmbPessoas.recordset.MoveFirst();
		while (! dsoCmbPessoas.recordset.EOF )
		{
		    optionStr = dsoCmbPessoas.recordset['fldName'].value;
		    optionValue = dsoCmbPessoas.recordset['fldID'].value;
			var oOption = document.createElement("OPTION");
			oOption.text = optionStr;
			oOption.value = optionValue;
			selPessoaID.add(oOption);
			dsoCmbPessoas.recordset.MoveNext();
		}
	}

	lockControlsInModalWin(false);
	if (selPessoaID.options.length == 1)
	{
		selPessoaID.selectedIndex = 0;
		adjustLabelsCombos();
	}	
	else	
		selPessoaID.selectedIndex = -1;
		
	selPessoaID.disabled = (selPessoaID.options.length == 0);
	adjustLabelsCombos();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Estornar Financeiro', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK
    btnOK.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    
    adjustElementsInForm([['lblFinanceiroID','txtFinanceiroID',10,1,-10,-10],
						  ['lblEstado','txtEstado',2,1],
						  ['lblDuplicataFinanceiro','txtDuplicataFinanceiro',11,1],
						  ['lblParceiro','txtParceiro',20,1],
						  ['lblMoeda','txtMoeda',5,1],
						  ['lblValorFinanceiro','txtValorFinanceiro',16,1],
						  ['lblSaldoFinanceiro','txtSaldoFinanceiro',16,1],
						  ['lblArgumento','txtArgumento',13,2,-10],
					      ['btnFindPessoa','btn',24,2],
						  ['lblPessoaID','selPessoaID',18,2],
						  ['lblDuplicata','txtDuplicata',11,2],
						  ['lblFormaPagamentoID','selFormaPagamentoID',7,2],
						  ['lbldtVencimento','txtdtVencimento',10,2],
						  ['lblMoedaID','selMoedaID',8,2],
						  ['lblValor','txtValor',10,2],
						  ['lblObservacao','txtObservacao',20,3,-10],
						  ['lblHistoricoPadraoID','selHistoricoPadraoID',25,3]], null, null, true);

	txtFinanceiroID.readOnly = true;
	txtEstado.readOnly = true;
	txtDuplicataFinanceiro.readOnly = true;
	txtParceiro.readOnly = true;
	txtMoeda.readOnly = true;
	txtValorFinanceiro.readOnly = true;
	txtSaldoFinanceiro.readOnly = true;

    txtArgumento.onkeydown = execPesq_OnKey;
    txtArgumento.maxLength = 20;
    
	selPessoaID.onchange = selCmbs_onchange;
	selPessoaID.disabled = (selPessoaID.options.length == 0);
	selHistoricoPadraoID.onchange = selCmbs_onchange;

    txtDuplicata.maxLength = 15;

	selFormaPagamentoID.onchange = selCmbs_onchange;
	
    txtdtVencimento.maxLength = 10;

	if (selMoedaID.options.length == 1)
	{
		selMoedaID.selectedIndex = 0;
		adjustLabelsCombos();
	}	
	else
		selMoedaID.selectedIndex = -1;
		
	selMoedaID.onchange = selCmbs_onchange;

    txtValor.onkeypress = verifyNumericEnterNotLinked;
    txtValor.onkeydown = execPesq_OnKey;
    txtValor.setAttribute('thePrecision', 11, 1);
    txtValor.setAttribute('theScale', 2, 1);
    txtValor.setAttribute('verifyNumPaste', 1);
    txtValor.setAttribute('minMax', new Array(1, 111111111.11), 1);
    
    txtObservacao.maxLength = 20;

	adjustLabelsCombos();

    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
			  txtValor.offsetTop + txtValor.offsetHeight + 2;
        width = txtValor.offsetLeft + txtValor.offsetWidth + ELEM_GAP;    
        height = txtValor.offsetTop + txtValor.offsetHeight;    
    }
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + selHistoricoPadraoID.offsetTop;
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		value = 'Financeiro';
		title = 'Gerar financeiro?';
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

	var aContexto = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'getCmbCurrDataInControlBar(' + '\'' + 'sup' + '\'' + ', 1);');
	
    // Pagamento
    if (aContexto[1] == 9111)
        glb_nTipoFinanceiroID = 1001;
    // Recebimento    
    else if (aContexto[1] == 9112)    
        glb_nTipoFinanceiroID = 1002;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function execPesq_OnKey()
{
    if ( event.keyCode == 13 )
	{
		if (this.id == 'txtArgumento')
			btnLupaClicked(btnFindPessoa);
	}
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveData();
    }
    // codigo privado desta janela
    else
    {        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    lockControlsInModalWin(true);
}

function loadDsoData()
{
	lockControlsInModalWin(true);
    
	var nTipoLancamentoID = (glb_nTipoFinanceiroID == 1001 ? 1572 : 1571);	

	setConnection(dsoData);

	dsoData.SQL = 'SELECT a.FinanceiroID, b.RecursoAbreviado, ISNULL(a.Duplicata, SPACE(0)) AS Duplicata, ' +
					'c.PessoaID, c.Fantasia, ' +
					'd.SimboloMoeda, a.Valor, ' +
					'dbo.fn_Financeiro_Totais(a.FinanceiroID, GETDATE(), 54) AS SaldoFinanceiro, ' +
					'CONVERT(VARCHAR(10), a.dtVencimento, ' + DATE_SQL_PARAM + ') AS Vencimento, ISNULL(a.Observacao, SPACE(0)) AS Observacao ' +
					'FROM Financeiro a WITH(NOLOCK), Recursos b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Conceitos d WITH(NOLOCK) ' +
					'WHERE (a.FinanceiroID = ' + glb_nFinanceiroID + ' AND a.EstadoID = b.RecursoID AND ' +
					'a.PessoaID = c.PessoaID AND a.MoedaID = d.ConceitoID)';

    dsoData.ondatasetcomplete = loadDsoData_DSC;
    
	dsoData.Refresh();
}

function loadDsoData_DSC()
{
	if (! ((dsoData.recordset.BOF)&&(dsoData.recordset.EOF)) )
	{
		dsoData.recordset.MoveFirst();
		txtFinanceiroID.value = dsoData.recordset['FinanceiroID'].value;
		txtEstado.value = dsoData.recordset['RecursoAbreviado'].value;
		txtDuplicataFinanceiro.value = dsoData.recordset['Duplicata'].value;
		txtParceiro.value = dsoData.recordset['Fantasia'].value;
		txtMoeda.value = dsoData.recordset['SimboloMoeda'].value;
		txtValorFinanceiro.value = (padNumReturningStr(roundNumber(dsoData.recordset['Valor'].value, 2),2));
		txtSaldoFinanceiro.value = (padNumReturningStr(roundNumber(dsoData.recordset['SaldoFinanceiro'].value, 2),2));
		txtdtVencimento.value = dsoData.recordset['Vencimento'].value;
		glb_nSaldoFinanceiro = dsoData.recordset['SaldoFinanceiro'].value;
		txtValor.value = (padNumReturningStr(roundNumber(glb_nSaldoFinanceiro, 2),2));
		txtObservacao.value = dsoData.recordset['Observacao'].value;

		clearComboEx(['selPessoaID']);

		var optionStr = dsoData.recordset['Fantasia'].value;
		var optionValue = dsoData.recordset['PessoaID'].value;
		var oOption = document.createElement("OPTION");
		oOption.text = optionStr;
		oOption.value = optionValue;
		selPessoaID.add(oOption);
	}

	lockControlsInModalWin(false);

	selPessoaID.disabled = (selPessoaID.options.length == 0);

	if (glb_bWindowLoading)
	{
		// lockBtnLupa(btnFindPessoa, false);
		glb_bWindowLoading = false;
		showExtFrame(window, true);
		window.focus();

		if ( !txtArgumento.disabled )
			txtArgumento.focus();
			
	}
	
	setupBtnsState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveData()
{
	var nTipoFinanceiroID = 0;
	var nValor = 0;
	var strPars = '';
	var nRelPesContaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset.Fields["RelPesContaID"].value');

	if (glb_nTipoFinanceiroID == 1002)
		nTipoFinanceiroID = 1001;
	else		
		nTipoFinanceiroID = 1002;

	nValor = trimStr(txtValor.value);
	
	if (chkDataEx(txtdtVencimento.value) != true)
	{
		if ( window.top.overflyGen.Alert('Data de vencimento inv�lida.') == 0 )
            return null;
	}
	else if (nValor == '')
	{
		if ( window.top.overflyGen.Alert('Valor inv�lido.') == 0 )
            return null;
	}
	else if (glb_nSaldoFinanceiro < nValor)
	{
		if ( window.top.overflyGen.Alert('Valor inv�lido.') == 0 )
            return null;
	}
	else if (glb_nHistoricoPadraoID	== 0)
	{
		if ( window.top.overflyGen.Alert('Hist�rico Padr�o inv�lido.') == 0 )
            return null;
	}
	else
	{
		strPars += '?nEmpresaID=' + escape(glb_nEmpresaID);
		strPars += '&nFinanceiroID=' + escape(glb_nFinanceiroID);
		strPars += '&nTipoFinanceiroID=' + escape(nTipoFinanceiroID);
		strPars += '&dtVencimento=' + escape(putDateInMMDDYYYY2(txtdtVencimento.value));
		strPars += '&nMoedaID=' + escape(selMoedaID.value);
		strPars += '&nEhEstorno=' + escape(glb_nEhEstorno);
		strPars += '&nEhImporte=' + escape(glb_nEhImporte);
		strPars += '&nHistoricoPadraoID=' + escape(glb_nHistoricoPadraoID);
		strPars += '&nUsuarioID=' + escape(getCurrUserID());
		strPars += '&nPessoaID=' + escape(selPessoaID.value);
		strPars += '&sDuplicata=' + escape(txtDuplicata.value);
		strPars += '&nFormaPagamentoID=' + escape(selFormaPagamentoID.value);
		strPars += '&nValor=' + escape(txtValor.value);
		strPars += '&sObservacao=' + escape(trimStr(txtObservacao.value));
		strPars += '&nHistoricoPadrao2ID=' + escape(selHistoricoPadraoID.value);
		strPars += '&nRelPesContaID=' + escape(nRelPesContaID);

		lockControlsInModalWin(true);

		dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/estornarfinanceiro.aspx' + strPars;
		dsoGrava.ondatasetcomplete = saveData_DSC;
		dsoGrava.refresh();
	}
}
/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveData_DSC()
{
    var nFinanceiroID = 0;
    var nFinanceiro2ID = 0;
    var sMsg = '';

    lockControlsInModalWin(false);
	
    if ((dsoGrava.recordset['Resultado'].value != null) && (dsoGrava.recordset['Resultado'].value != ''))
	{
        if ( window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0 )
			return null;			
	}
	else
	{
        nFinanceiroID = dsoGrava.recordset['FinanceiroID'].value;
		nFinanceiro2ID = dsoGrava.recordset['Financeiro2ID'].value;

		sMsg = 'Gerado o Financeiro(s): ' + (nFinanceiroID == 0 ? '' : nFinanceiroID);
		
		if ((nFinanceiro2ID != '') && (nFinanceiro2ID != 0))
			sMsg += ', ' + nFinanceiro2ID;
		
		sMsg += '\n' + 'Detalhar Financeiro?';

		var _retConf = window.top.overflyGen.Confirm(sMsg);

		if ( _retConf == 0 )
		{
			return null;
		}
		else if ( _retConf == 1 )
		{
			sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(glb_nEmpresaID, nFinanceiroID));
		}

		loadDsoData();
	}
}

function selCmbs_onchange()
{
	setupBtnsState();
	adjustLabelsCombos();
	return null;
}

function adjustLabelsCombos()
{
    setLabelOfControl(lblPessoaID, selPessoaID.value);
    setLabelOfControl(lblFormaPagamentoID, selFormaPagamentoID.value);
    setLabelOfControl(lblMoedaID, selMoedaID.value);
    setLabelOfControl(lblHistoricoPadraoID, selHistoricoPadraoID.value);
}
function setupBtnsState()
{
	var nPessoaID = 0;
	
	btnOK.disabled = true;

	if ((selPessoaID.options.length > 0) && (selPessoaID.value > 0))
	{
		if ((selFormaPagamentoID.options.length > 0) && (selFormaPagamentoID.value > 0))
		{
			if (parseFloat(txtValor.value) > 0)
				btnOK.disabled = false;
		}
	}
}