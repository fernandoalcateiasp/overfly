/********************************************************************
modalocorrencias.js

Library javascript para o modalocorrencias.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_timerUnloadWin = null;
var glb_aNiveis = new Array();
var glb_aOrdens = new Array();
var glb_nLastNivelSelected = 0;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_OcorrenciasTimerInt = null;
var glb_aDatas = new Array();
var glb_bOKDblClick = true;
var glb_bOcorrencias = false;
var glb_nOldRow = -1;
var glb_nOldCol = -1;
var glb_nDsosCall = null;
var glb_bListaExcel = 0;

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_excel.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_excel_dis.gif';


var dsoGrid = new CDatatransport("dsoGrid");
var dsoGridNivelX = new CDatatransport("dsoGridNivelX");
var dsoDetalhamento = new CDatatransport("dsoDetalhamento");
var dsoDatasDefault = new CDatatransport("dsoDatasDefault");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoDateTime = new CDatatransport("dsoDateTime");
var dsoLogReport = new CDatatransport("dsoLogReport");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

function _objOcorrencias() {
    this.optionID = -1;
    this.fldID = '';
    this.fldName = '';
    this.fldOrderby = '';
    this.sDisplay = '';
    this.fldFiltro = '';
    this.bDatas = false;
    this.bProdutos = false;
    this.bLocalidades = false;
    this.bGalho = false;
    this.bFolha = false;
    this.aExceptions = new Array();
    this.aPreRequisitos = new Array();
    this.nItemDefault = -1;
    this.bExcessao = false;
}

function _objDatasDefault() {
    this.Indice = -1;
    this.DataInicio = '';
    this.DataFim = '';
    this.bPodeNulo = false;
}


/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
setupBtns()
cmb_onchange()
btn_onclick(ctl)
getCurrEmpresaData()
fillGridData(oDSO)
fillGridData_DSC()
findRowByContaMae(nContaMaeID)

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalocorrencias.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

js_fg_DblClick()

FINAL DE DEFINIDAS NO ARQUIVO modalocorrencias.ASP
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();

    // ajusta o body do html
    with (modalocorrenciasBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    var sEmpresas = '\'' + parseStrLists(selEmpresa, false, null) + '\'';
    var sVendedores;

    // ajusta parametros fixos dos dsos da pagina
    dsoFixedParams();

    if (glb_bDireitoA2)
        sVendedores = 'NULL';
    else
        sVendedores = '\'' + parseStrLists(selVendedor, false, 2) + '\'';

    setConnection(dsoDatasDefault);

    dsoDatasDefault.SQL = 'DECLARE @dtMaxPagComissao DATETIME ' +
			'SELECT @dtMaxPagComissao = dbo.fn_Data_Zero(dbo.fn_Empresa_FinOcorrenciasDatas(' + sEmpresas + ', ' + sVendedores + ', 1)) ' +
			'SELECT 0 AS Indice, ' +
			'CONVERT(VARCHAR(10), dbo.fn_Data_Zero(dbo.fn_Data_Periodo(GETDATE(), 1, 2)), ' + DATE_SQL_PARAM + ') AS dtInicio, ' +
			'CONVERT(VARCHAR(10), dbo.fn_Data_Zero(dbo.fn_Data_Periodo(GETDATE(), 2, 2)), ' + DATE_SQL_PARAM + ') AS dtFim, ' +
			'CONVERT(BIT, 0) AS PodeNulo ' +
		'UNION ALL ' +
			'SELECT 1 AS Indice, ' +
			'ISNULL(CONVERT(VARCHAR(10), @dtMaxPagComissao, ' + DATE_SQL_PARAM + '), SPACE(0)) AS dtInicio, ' +
			'ISNULL(CONVERT(VARCHAR(10), @dtMaxPagComissao, ' + DATE_SQL_PARAM + '), SPACE(0)) AS dtFim, ' +
			'CONVERT(BIT, 0) AS PodeNulo ' +
		'UNION ALL ' +
			'SELECT 2 AS Indice, SPACE(0) AS dtInicio, SPACE(0) AS dtFim, CONVERT(BIT, 1) AS PodeNulo ' +
		'UNION ALL ' +
			'SELECT 3 AS Indice, SPACE(0) AS dtInicio, ' +
			'CONVERT(VARCHAR(10), dbo.fn_Data_Periodo(@dtMaxPagComissao, 2, 2), ' + DATE_SQL_PARAM + ') AS dtFim, CONVERT(BIT, 1) AS PodeNulo ' +
		'UNION ALL ' +
			'SELECT 4 AS Indice, ' +
			'CONVERT(VARCHAR(10), dbo.fn_Data_Zero(dbo.fn_Data_Periodo(GETDATE(), 1, 2)), ' + DATE_SQL_PARAM + ') AS dtInicio, ' +
			'CONVERT(VARCHAR(10), dbo.fn_Data_Zero(dbo.fn_Data_Periodo(GETDATE(), 2, 2)), ' + DATE_SQL_PARAM + ') AS dtFim, ' +
			'CONVERT(BIT, 0) AS PodeNulo';

    dsoDatasDefault.ondatasetcomplete = window_onload_DSC;
    dsoDatasDefault.Refresh();
}

function window_onload_DSC() {
    if (!(dsoDatasDefault.recordset.BOF && dsoDatasDefault.recordset.EOF)) {
        while (!dsoDatasDefault.recordset.EOF) {
            glb_aDatas[glb_aDatas.length] = new _objDatasDefault();
            glb_aDatas[glb_aDatas.length - 1].Indice = dsoDatasDefault.recordset['Indice'].value;
            glb_aDatas[glb_aDatas.length - 1].DataInicio = dsoDatasDefault.recordset['dtInicio'].value;
            glb_aDatas[glb_aDatas.length - 1].DataFim = dsoDatasDefault.recordset['dtFim'].value;
            glb_aDatas[glb_aDatas.length - 1].bPodeNulo = dsoDatasDefault.recordset['PodeNulo'].value;
            dsoDatasDefault.recordset.moveNext();
        }
    }

    // configuracao inicial do html
    setupPage();

    with (btnCanc.style) {
        left = (parseInt(divFG.style.width, 10) / 2) - (parseInt(btnCanc.style.width, 10) / 2);
        visibility = 'hidden';
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Ocorr�ncias', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    btnExcel.src = glb_LUPA_IMAGES[0].src;

    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';

    for (i = 0; i <= 23; i++)
        glb_aNiveis[i] = new _objOcorrencias();

    glb_aNiveis[0].optionID = 0;
    glb_aNiveis[0].fldID = '';
    glb_aNiveis[0].fldName = '';
    glb_aNiveis[0].sDisplay = '';
    glb_aNiveis[0].bGalho = true;
    glb_aNiveis[0].bFolha = false;
    glb_aNiveis[0].aExceptions = [];
    glb_aNiveis[0].aPreRequisitos = [];
    glb_aNiveis[0].nItemDefault = 6;

    glb_aNiveis[1].optionID = 1;
    glb_aNiveis[1].fldID = 'EmpresaID';
    glb_aNiveis[1].fldName = 'Empresa';
    glb_aNiveis[1].sDisplay = 'Empresa';
    glb_aNiveis[1].fldFiltro = 'Financeiro.EmpresaID';
    glb_aNiveis[1].bGalho = true;
    glb_aNiveis[1].bFolha = true;
    glb_aNiveis[1].aExceptions = [1];
    glb_aNiveis[1].aPreRequisitos = [];
    glb_aNiveis[1].nItemDefault = 2;

    glb_aNiveis[2].optionID = 2;
    glb_aNiveis[2].fldID = 'EquipeID';
    glb_aNiveis[2].fldName = 'Equipe';
    glb_aNiveis[2].sDisplay = 'Equipe';
    glb_aNiveis[2].fldFiltro = 'Pedidos.AlternativoID';
    glb_aNiveis[2].bGalho = true;
    glb_aNiveis[2].bFolha = true;
    glb_aNiveis[2].aExceptions = [2];
    glb_aNiveis[2].aPreRequisitos = [];
    glb_aNiveis[2].nItemDefault = 3;

    glb_aNiveis[3].optionID = 3;
    glb_aNiveis[3].fldID = 'VendedorID';
    glb_aNiveis[3].fldName = 'Vendedor';
    glb_aNiveis[3].sDisplay = 'Vendedor';
    glb_aNiveis[3].fldFiltro = 'Pedidos.ProprietarioID';
    glb_aNiveis[3].bGalho = true;
    glb_aNiveis[3].bFolha = true;
    glb_aNiveis[3].aExceptions = [3];
    glb_aNiveis[3].aPreRequisitos = [];
    glb_aNiveis[3].nItemDefault = 4;

    glb_aNiveis[4].optionID = 4;
    glb_aNiveis[4].fldID = 'TransacaoID';
    glb_aNiveis[4].fldName = 'Transacao';
    glb_aNiveis[4].sDisplay = 'Transa��o';
    glb_aNiveis[4].fldFiltro = 'Transacoes.OperacaoID';
    glb_aNiveis[4].bGalho = true;
    glb_aNiveis[4].bFolha = true;
    glb_aNiveis[4].aExceptions = [4];
    glb_aNiveis[4].aPreRequisitos = [];
    glb_aNiveis[4].nItemDefault = 5;

    glb_aNiveis[5].optionID = 5;
    glb_aNiveis[5].fldID = 'FamiliaID';
    glb_aNiveis[5].fldName = 'Familia';
    glb_aNiveis[5].sDisplay = 'Fam�lia';
    glb_aNiveis[5].fldFiltro = 'Familias.ConceitoID';
    glb_aNiveis[5].bProdutos = true;
    glb_aNiveis[5].bGalho = true;
    glb_aNiveis[5].bFolha = true;
    glb_aNiveis[5].aExceptions = [5];
    glb_aNiveis[5].aPreRequisitos = [];
    glb_aNiveis[5].nItemDefault = 6;

    glb_aNiveis[6].optionID = 6;
    glb_aNiveis[6].fldID = 'MarcaID';
    glb_aNiveis[6].fldName = 'Marca';
    glb_aNiveis[6].sDisplay = 'Marca';
    glb_aNiveis[6].fldFiltro = 'Marcas.ConceitoID';
    glb_aNiveis[6].bProdutos = true;
    glb_aNiveis[6].bGalho = true;
    glb_aNiveis[6].bFolha = true;
    glb_aNiveis[6].aExceptions = [6];
    glb_aNiveis[6].aPreRequisitos = [];
    glb_aNiveis[6].nItemDefault = 7;

    glb_aNiveis[7].optionID = 7;
    glb_aNiveis[7].fldID = 'ProdutoID';
    glb_aNiveis[7].fldName = 'Produto';
    glb_aNiveis[7].sDisplay = 'Produto';
    glb_aNiveis[7].fldFiltro = 'Produtos.ConceitoID';
    glb_aNiveis[7].bProdutos = true;
    glb_aNiveis[7].bGalho = false;
    glb_aNiveis[7].bFolha = true;
    glb_aNiveis[7].aExceptions = [5, 6, 7];
    glb_aNiveis[7].aPreRequisitos = [5, 6];
    glb_aNiveis[7].nItemDefault = 19;

    glb_aNiveis[8].optionID = 8;
    glb_aNiveis[8].fldID = 'Ano';
    glb_aNiveis[8].fldName = 'Ano';
    glb_aNiveis[8].sDisplay = 'Ano';
    glb_aNiveis[8].fldFiltro = '';
    glb_aNiveis[8].bDatas = true;
    glb_aNiveis[8].bGalho = true;
    glb_aNiveis[8].bFolha = true;
    glb_aNiveis[8].aExceptions = [8];
    glb_aNiveis[8].aPreRequisitos = [];
    glb_aNiveis[8].nItemDefault = 9;

    glb_aNiveis[9].optionID = 9;
    glb_aNiveis[9].fldID = 'sQuarter';
    glb_aNiveis[9].fldName = 'sQuarter';
    glb_aNiveis[9].fldOrderby = 'dtInicio';
    glb_aNiveis[9].sDisplay = 'Quarter';
    glb_aNiveis[9].fldFiltro = '';
    glb_aNiveis[9].bDatas = true;
    glb_aNiveis[9].bGalho = true;
    glb_aNiveis[9].bFolha = true;
    glb_aNiveis[9].aExceptions = [8, 9];
    glb_aNiveis[9].aPreRequisitos = [];
    glb_aNiveis[9].nItemDefault = 10;

    glb_aNiveis[10].optionID = 10;
    glb_aNiveis[10].fldID = 'Mes';
    glb_aNiveis[10].fldName = 'Mes';
    glb_aNiveis[10].fldOrderby = 'dtInicio';
    glb_aNiveis[10].sDisplay = 'M�s';
    glb_aNiveis[10].fldFiltro = '';
    glb_aNiveis[10].bDatas = true;
    glb_aNiveis[10].bGalho = true;
    glb_aNiveis[10].bFolha = true;
    glb_aNiveis[10].aExceptions = [8, 9, 10];
    glb_aNiveis[10].aPreRequisitos = [];
    glb_aNiveis[10].nItemDefault = 11;

    glb_aNiveis[11].optionID = 11;
    glb_aNiveis[11].fldID = 'Semana';
    glb_aNiveis[11].fldName = 'Semana';
    glb_aNiveis[11].fldOrderby = 'dtInicio';
    glb_aNiveis[11].sDisplay = 'Semana';
    glb_aNiveis[11].fldFiltro = '';
    glb_aNiveis[11].bDatas = true;
    glb_aNiveis[11].bGalho = true;
    glb_aNiveis[11].bFolha = true;
    glb_aNiveis[11].aExceptions = [8, 9, 10, 11];
    glb_aNiveis[11].aPreRequisitos = [];
    glb_aNiveis[11].nItemDefault = 12;

    glb_aNiveis[12].optionID = 12;
    glb_aNiveis[12].fldID = 'dtOcorrencia';
    glb_aNiveis[12].fldName = 'dtOcorrencia';
    glb_aNiveis[12].sDisplay = 'Dia';
    glb_aNiveis[12].fldFiltro = '';
    glb_aNiveis[12].bDatas = true;
    glb_aNiveis[12].bGalho = false;
    glb_aNiveis[12].bFolha = true;
    glb_aNiveis[12].aExceptions = [8, 9, 10, 11, 12];
    glb_aNiveis[12].aPreRequisitos = [10, 11];
    glb_aNiveis[12].nItemDefault = 21;

    glb_aNiveis[13].optionID = 13;
    glb_aNiveis[13].fldID = 'PaisID';
    glb_aNiveis[13].fldName = 'Pais';
    glb_aNiveis[13].sDisplay = 'Pa�s';
    glb_aNiveis[13].fldFiltro = 'Enderecos.PaisID';
    glb_aNiveis[13].bLocalidades = true;
    glb_aNiveis[13].bGalho = true;
    glb_aNiveis[13].bFolha = true;
    glb_aNiveis[13].aExceptions = [13];
    glb_aNiveis[13].aPreRequisitos = [];
    glb_aNiveis[13].nItemDefault = 14;

    glb_aNiveis[14].optionID = 14;
    glb_aNiveis[14].fldID = 'RegiaoID';
    glb_aNiveis[14].fldName = 'Regiao';
    glb_aNiveis[14].sDisplay = 'Regi�o';
    glb_aNiveis[14].fldFiltro = 'UFs.TipoRegiaoID';
    glb_aNiveis[14].bLocalidades = true;
    glb_aNiveis[14].bGalho = false;
    glb_aNiveis[14].bFolha = true;
    glb_aNiveis[14].aExceptions = [13, 14];
    glb_aNiveis[14].aPreRequisitos = [13];
    glb_aNiveis[14].nItemDefault = 15;

    glb_aNiveis[15].optionID = 15;
    glb_aNiveis[15].fldID = 'UFID';
    glb_aNiveis[15].fldName = 'UF';
    glb_aNiveis[15].sDisplay = 'Estado';
    glb_aNiveis[15].fldFiltro = 'Enderecos.UFID';
    glb_aNiveis[15].bLocalidades = true;
    glb_aNiveis[15].bGalho = false;
    glb_aNiveis[15].bFolha = true;
    glb_aNiveis[15].aExceptions = [13, 14, 15];
    glb_aNiveis[15].aPreRequisitos = [13, 14];
    glb_aNiveis[15].nItemDefault = 16;

    glb_aNiveis[16].optionID = 16;
    glb_aNiveis[16].fldID = 'CidadeID';
    glb_aNiveis[16].fldName = 'Cidade';
    glb_aNiveis[16].sDisplay = 'Cidade';
    glb_aNiveis[16].fldFiltro = 'Enderecos.CidadeID';
    glb_aNiveis[16].bLocalidades = true;
    glb_aNiveis[16].bGalho = false;
    glb_aNiveis[16].bFolha = true;
    glb_aNiveis[16].aExceptions = [13, 14, 15, 16];
    glb_aNiveis[16].aPreRequisitos = [15];
    glb_aNiveis[16].nItemDefault = 17;

    glb_aNiveis[17].optionID = 17;
    glb_aNiveis[17].fldID = 'Bairro';
    glb_aNiveis[17].fldName = 'Bairro';
    glb_aNiveis[17].sDisplay = 'Bairro';
    glb_aNiveis[17].fldFiltro = 'Enderecos.Bairro';
    glb_aNiveis[17].bLocalidades = true;
    glb_aNiveis[17].bGalho = false;
    glb_aNiveis[17].bFolha = true;
    glb_aNiveis[17].aExceptions = [13, 14, 15, 16, 17];
    glb_aNiveis[17].aPreRequisitos = [16];
    glb_aNiveis[17].nItemDefault = 18;

    glb_aNiveis[18].optionID = 18;
    glb_aNiveis[18].fldID = 'ParceiroID';
    glb_aNiveis[18].fldName = 'Parceiro';
    glb_aNiveis[18].sDisplay = 'Cliente';
    glb_aNiveis[18].fldFiltro = 'Parceiros.PessoaID';
    glb_aNiveis[18].bGalho = false;
    glb_aNiveis[18].bFolha = true;
    glb_aNiveis[18].aExceptions = [13, 14, 15, 16, 17, 18];
    glb_aNiveis[18].aPreRequisitos = [];
    glb_aNiveis[18].nItemDefault = 20;

    glb_aNiveis[19].optionID = 19;
    glb_aNiveis[19].fldID = 'PessoaID';
    glb_aNiveis[19].fldName = 'Pessoa';
    glb_aNiveis[19].sDisplay = 'Pessoa';
    glb_aNiveis[19].fldFiltro = 'Pessoas.PessoaID';
    glb_aNiveis[19].bGalho = false;
    glb_aNiveis[19].bFolha = true;
    glb_aNiveis[19].aExceptions = [13, 14, 15, 16, 17, 19];
    glb_aNiveis[19].aPreRequisitos = [];
    glb_aNiveis[19].nItemDefault = 20;

    glb_aNiveis[20].optionID = 20;
    glb_aNiveis[20].fldID = 'PedidoID';
    glb_aNiveis[20].fldName = 'Pedido';
    glb_aNiveis[20].sDisplay = 'Pedido';
    glb_aNiveis[20].fldFiltro = 'Pedidos.PedidoID';
    glb_aNiveis[20].bGalho = false;
    glb_aNiveis[20].bFolha = true;
    glb_aNiveis[20].aExceptions = [20];
    glb_aNiveis[20].aPreRequisitos = [];
    glb_aNiveis[20].nItemDefault = 21;

    glb_aNiveis[21].optionID = 21;
    glb_aNiveis[21].fldID = 'FinanceiroID';
    glb_aNiveis[21].fldName = 'Financeiro';
    glb_aNiveis[21].sDisplay = 'Financeiro';
    glb_aNiveis[21].fldFiltro = 'Financeiro.FinanceiroID';
    glb_aNiveis[21].bGalho = false;
    glb_aNiveis[21].bFolha = true;
    glb_aNiveis[21].aExceptions = [20, 21];
    glb_aNiveis[21].aPreRequisitos = [];
    glb_aNiveis[21].nItemDefault = 22;

    glb_aNiveis[22].optionID = 22;
    glb_aNiveis[22].fldID = 'GPID';
    glb_aNiveis[22].fldName = 'GP';
    glb_aNiveis[22].sDisplay = 'GP';
    glb_aNiveis[22].fldFiltro = 'GPs.PessoaID';
    glb_aNiveis[22].bProdutos = true;
    glb_aNiveis[22].bGalho = true;
    glb_aNiveis[22].bFolha = true;
    glb_aNiveis[22].aExceptions = [22];
    glb_aNiveis[22].aPreRequisitos = [];
    glb_aNiveis[22].nItemDefault = 4;

    glb_aNiveis[23].optionID = 23;
    glb_aNiveis[23].fldID = 'FinOcorrenciaID';
    glb_aNiveis[23].fldName = 'FinOcorrencia';
    glb_aNiveis[23].sDisplay = 'Ocorr�ncia';
    glb_aNiveis[23].fldFiltro = 'Ocorrencias.FinOcorrenciaID';
    glb_aNiveis[23].bGalho = true;
    glb_aNiveis[23].bFolha = true;
    glb_aNiveis[23].aExceptions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
    glb_aNiveis[23].aPreRequisitos = [];
    glb_aNiveis[23].nItemDefault = -1;
    glb_aNiveis[23].bExcessao = true;

    for (i = 0; i < glb_aNiveis.length; i++)
        glb_aNiveis[0].aExceptions[i] = glb_aNiveis[i].optionID;

    glb_aOrdens[0] = 'Alfab�tica';
    glb_aOrdens[1] = 'Ocorr�ncias';
    glb_aOrdens[2] = 'Valor';
    glb_aOrdens[3] = 'Faturamento';
    glb_aOrdens[4] = 'Contribui��o';

    for (i = 0; i < glb_aOrdens.length; i++) {
        var oOption1 = document.createElement("OPTION");
        oOption1.text = glb_aOrdens[i];
        oOption1.value = i;
        selOrdem1.add(oOption1);

        var oOption2 = document.createElement("OPTION");
        oOption2.text = glb_aOrdens[i];
        oOption2.value = i;
        selOrdem2.add(oOption2);

        var oOption3 = document.createElement("OPTION");
        oOption3.text = glb_aOrdens[i];
        oOption3.value = i;
        selOrdem3.add(oOption3);

        var oOption4 = document.createElement("OPTION");
        oOption4.text = glb_aOrdens[i];
        oOption4.value = i;
        selOrdem4.add(oOption4);

        var oOptionX = document.createElement("OPTION");
        oOptionX.text = glb_aOrdens[i];
        oOptionX.value = i;
        selOrdemX.add(oOptionX);
    }

    selOrdem1.selectedIndex = 4;
    selOrdem2.selectedIndex = 4;
    selOrdem3.selectedIndex = 4;
    selOrdem4.selectedIndex = 4;
    selOrdemX.selectedIndex = 4;

    // ajusta os controles do divControles
    adjustElementsInForm([['lblAgruparPorX', 'selAgruparPorX', 12, 1, -10, -10],
						  ['lblOrdemX', 'selOrdemX', 10, 1, -2],
						  ['lblPesquisa', 'chkPesquisa', 3, 1, -2],
						  ['lblRecebido', 'chkRecebido', 3, 1, -3],
						  ['lblComissao', 'selComissao', 11, 1, -5],
						  ['lblDataInicio', 'txtDataInicio', 9, 1, -1],
						  ['lblDataFim', 'txtDataFim', 9, 1, -1],
						  ['btnFillGrid', 'btn', 39, 1, 2, -4],
						  ['btnDetalhar', 'btn', 39, 1, 1],
						  ['btnFinanceiro', 'btn', 39, 1, 1],
						  ['btnExcel', 'btn', 21, 1, 2],
						  ['btnPagar', 'btn', 39, 1, 1],
						  ['btnImprimir', 'btn', 39, 1, 1],
						  ['lblAgruparPor1', 'selAgruparPor1', 12, 2, -10],
						  ['lblOrdem1', 'selOrdem1', 10, 2, -2],
						  ['lblVendedor', 'selVendedor', 16, 2],
						  ['lblEmpresa', 'selEmpresa', 17, 2, -2],
						  ['lblFamilia', 'selFamilia', 16, 2],
						  ['lblMarca', 'selMarca', 16, 2],
						  ['lblAgruparPor2', 'selAgruparPor2', 12, 3, -10],
						  ['lblOrdem2', 'selOrdem2', 10, 3],
						  ['lblAgruparPor3', 'selAgruparPor3', 12, 4, -10],
						  ['lblOrdem3', 'selOrdem3', 10, 4],
						  ['lblAgruparPor4', 'selAgruparPor4', 12, 5, -10],
						  ['lblOrdem4', 'selOrdem4', 10, 5],
						  ['lblMoedaID', 'selMoedaID', 8, 5],
						  ['lblFiltro', 'txtFiltro', 60, 5]], null, null, true);

    chkRecebido.checked = true;
    chkRecebido.onclick = chkRecebido_onclick;
    selAgruparPor1.onchange = selAgruparPor_onchange;
    selAgruparPor1.setAttribute('cmbIndex', 0, 1);
    selOrdem1.setAttribute('cmbIndex', 0, 1);
    selOrdem1.onchange = selOrdem_onchange;
    selComissao.onchange = selComissao_onchange;

    selAgruparPor2.onchange = selAgruparPor_onchange;
    selAgruparPor2.setAttribute('cmbIndex', 1, 1);
    selAgruparPor2.disabled = true;
    selOrdem2.onchange = selOrdem_onchange;
    selOrdem2.setAttribute('cmbIndex', 1, 1);
    selOrdem2.disabled = true;

    selAgruparPor3.onchange = selAgruparPor_onchange;
    selAgruparPor3.setAttribute('cmbIndex', 2, 1);
    selAgruparPor3.disabled = true;
    selOrdem3.setAttribute('cmbIndex', 2, 1);
    selOrdem3.onchange = selOrdem_onchange;
    selOrdem3.disabled = true;

    selAgruparPor4.onchange = selAgruparPor_onchange;
    selAgruparPor4.setAttribute('cmbIndex', 3, 1);
    selAgruparPor4.disabled = true;
    selOrdem4.setAttribute('cmbIndex', 3, 1);
    selOrdem4.onchange = selOrdem_onchange;
    selOrdem4.disabled = true;

    selAgruparPorX.disabled = true;
    selOrdemX.disabled = true;

    var i, nIndexToSelect;

    fillCmbs(selAgruparPor1);

    selOrdem3.disabled = true;
    selOrdem4.disabled = true;

    btnFillGrid.style.height = btnOK.offsetHeight;
    btnDetalhar.style.height = btnOK.offsetHeight;
    btnFinanceiro.style.height = btnOK.offsetHeight;

    btnExcel.style.height = 21;
    btnPagar.style.height = btnOK.offsetHeight;
    btnImprimir.style.height = btnOK.offsetHeight;

    if ((glb_bDireitoA1) && (glb_bDireitoA2))
        btnExcel.style.visibility = 'inherit';
    else
        btnExcel.style.visibility = 'hidden';

    var nCmbsHeight = 118;
    selEmpresa.style.height = nCmbsHeight;
    selEmpresa.onchange = selEmpresa_onchange;
    selFamilia.style.height = nCmbsHeight;
    selVendedor.style.height = nCmbsHeight;

    if (!glb_bDireitoA2)
        selVendedor.disabled = true;

    selMarca.style.height = nCmbsHeight;

    chkPesquisa.onclick = chkPesquisa_onclick;

    txtDataInicio.maxLength = 10;
    txtDataInicio.onfocus = selFieldContent;
    txtDataInicio.onkeypress = txtData_onKeyPress;

    txtDataFim.maxLength = 10;
    txtDataFim.onfocus = selFieldContent;
    txtDataFim.onkeypress = txtData_onKeyPress;

    // ajusta o divControles
    with (divControles.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = parseInt(txtFiltro.currentStyle.top, 10) + parseInt(txtFiltro.currentStyle.height, 10);
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControles.currentStyle.top, 10) +
			  parseInt(divControles.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) -
				 parseInt(top, 10);
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // reajusta dimensoes e reposiciona a janela
    (getFrameInHtmlTop(getExtFrameID(window))).style.top =
		(getFrameInHtmlTop('frameSup01')).offsetTop;

    redimAndReposicionModalWin(modWidth, modHeight, false);

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;

    with (fg) {
        FontName = 'Tahoma, Arial, MS SansSerif';
        FontSize = '8';
        Editable = true;
        AllowUserResizing = 1;
        VirtualData = true;
        Ellipsis = 1;
        SelectionMode = 1;
        AllowSelection = false;
        Rows = 1;
        Cols = 2;
        FixedRows = 1;
        FixedCols = 1;
        ScrollBars = 3;
        OutLineBar = 5;
        // TreeColor = 0X000000;
        GridLines = 0;
        FormatString = 'Detalhe' + '\t' + 'Ocorr' + '\t' + '$' + '\t' + 'Valor' + '\t' + 'Faturamento' + '\t' + 'Contribui��o' + '\t' +
			'Filtro' + '\t' + 'CmbOptionID' + '\t' + 'FinanceiroID' + '\t' + 'OK' + '\t' + 'FinOcorrenciaID';
        OutLineBar = 1;
        // cores
        // TreeColor = 0X000000;
        //BackColorFixed = 0XFFFFFF;
        // linhas
        // Coloca botoes na barra
        GridLinesFixed = 13;
        GridLines = 1;
        GridColor = 0X000000;
        ColKey(0) = 'Detalhe';
        ColKey(1) = 'Ocorrencias';
        ColKey(2) = 'Moeda';
        ColKey(3) = 'ValorOcorrencia';
        ColKey(4) = 'ValorFaturamento';
        ColKey(5) = 'ValorContribuicao';
        ColKey(6) = 'Filtro';
        ColKey(7) = 'CmbOptionID';
        ColKey(8) = 'FinanceiroID';
        ColKey(9) = 'OK';
        ColKey(10) = 'FinOcorrenciaID';
        ColHidden(6) = true;
        ColHidden(7) = true;
        ColHidden(8) = true;
        ColHidden(9) = true;
        ColHidden(10) = true;
    }
    alignColsInGrid(fg, [1, 3, 4, 5]);

    fg.Redraw = 2;
    setDefault();
    showExtFrame(window, true);

    chkPesquisa_onclick();
    setupBtns();
    selEmpresa_onchange();
    window.focus();
    txtDataInicio.focus();
}

function chkRecebido_onclick() {
    fg.Rows = 1;
    clearComboEx([selAgruparPor1.id, selAgruparPor2.id, selAgruparPor3.id, selAgruparPor4.id, selAgruparPorX.id]);

    selAgruparPor1.disabled = true;
    selOrdem1.disabled = true;
    selAgruparPor2.disabled = true;
    selOrdem2.disabled = true;
    selAgruparPor3.disabled = true;
    selOrdem3.disabled = true;
    selAgruparPor4.disabled = true;
    selOrdem4.disabled = true;
    selAgruparPorX.disabled = true;
    selOrdemX.disabled = true;

    fillCmbs(selAgruparPor1);
    selAgruparPor1.selectedIndex = -1;

    selAgruparPor1.disabled = (selAgruparPor1.options.length == 0);
    selOrdem1.disabled = (selAgruparPor1.options.length == 0);

    setDefault();

    if (chkRecebido.checked) {
        lblComissao.style.visibility = 'inherit';
        selComissao.style.visibility = 'inherit';
    }
    else {
        lblComissao.style.visibility = 'hidden';
        selComissao.style.visibility = 'hidden';
    }
}

function chkPesquisa_onclick() {
    var nGap = (ELEM_GAP * 18);

    if (chkPesquisa.checked) {
        divFG.style.height = divFG.offsetHeight - nGap;
        divFG.style.top = parseInt(divControles.currentStyle.top, 10) +
			  parseInt(divControles.currentStyle.height, 10) + ELEM_GAP;
    }
    else {
        divFG.style.top = (ELEM_GAP * 7) + 9;
        divFG.style.height = divFG.offsetHeight + nGap;
    }

    setFieldsVisibility();
    fg.style.height = divFG.offsetHeight;
    drawBordersAroundTheGrid(fg);
}

/********************************************************************
Enter no campo data
********************************************************************/
function txtData_onKeyPress() {
    if (event.keyCode == 13)
        btn_onclick(btnFillGrid);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        ;
    }
    else if (controlID == 'btnCanc') {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
    else if (controlID == 'btnFillGrid')
        btnFillGrid_onclick();
    else if (controlID == 'btnDetalhar')
        agruparX();
    else if (controlID == 'btnFinanceiro')
        detalharFinanceiro();
    else if (controlID == 'btnExcel') {
        glb_bListaExcel = 1;
        fillGridData(dsoGrid, false);
    }
    else if (controlID == 'btnPagar')
        pagar();
    else if (controlID == 'btnImprimir')
        imprimirGrid();
}

function btnFillGrid_onclick() {
    if (!verificaData())
        return null;

    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    glb_nDsosCall = 1;

    fg.Rows = 1;

    dsoDateTime.URL = SYS_PAGESURLROOT + '/serversidegenEx/datetime.aspx';
    dsoDateTime.ondatasetcomplete = dsoDateTime_DSC;
    dsoDateTime.refresh();

    //    var strPas = '?ReportID=' + escape(0) +
    //        '&EmpresaID=' + escape(aEmpresaData[0]) +
    //        '&UserID=' + escape(nUserID);

    //    dsoLogReport.URL = SYS_ASPURLROOT + '/serversidegenEx/reportslog.aspx'+strPas;
    //    dsoLogReport.ondatasetcomplete = dsoDateTime_DSC;
    //    dsoLogReport.Refresh();
    //}
}
function dsoDateTime_DSC() {
    glb_nDsosCall--;

    if (glb_nDsosCall > 0)
        return null;

    var hora = padL(dsoDateTime.recordset.Fields['Hora'].value.toString(), 2, '0');
    var minuto = padL(dsoDateTime.recordset.Fields['Minuto'].value.toString(), 2, '0');
    var horAtual = trimStr(hora.toString()) + trimStr(minuto.toString());
    
	if (((!glb_bDireitoA1) || (!glb_bDireitoA2)) && (SYS_NAME != 'Overfly Relat�rios'))
	{
		if ((horAtual < 0800) || (horAtual > 1800))
		{
			if ( window.top.overflyGen.Alert ('Este relat�rio s� est� liberado no per�odo das 12:00 hs �s 14:00 hs.\n') == 0 )
				return null;    

			return null;	
		}
	}
    
    fillGridData(dsoGrid, false);
}

function verificaData() {
    var sDataInicio = trimStr(txtDataInicio.value);
    var sDataFim = trimStr(txtDataFim.value);
    var bDataIsValid = true;
    var numeroDias = 0;
    var bPodeNulo = false;

    if (!chkRecebido.checked)
        bPodeNulo = glb_aDatas[0].bPodeNulo;
    else
        bPodeNulo = glb_aDatas[selComissao.value].bPodeNulo;

    if (sDataInicio != '')
        bDataIsValid = chkDataEx(sDataInicio);

    if (!bDataIsValid || (!bPodeNulo && (sDataInicio == ''))) {
        if (window.top.overflyGen.Alert('Data de in�cio inv�lida.') == 0)
            return null;

        window.focus();

        if (sDataInicio != '')
            txtDataInicio.focus();

        return false;
    }

    if (sDataFim != '')
        bDataIsValid = chkDataEx(sDataFim);

    if (!bDataIsValid || (!bPodeNulo && (sDataFim == ''))) {
        if (window.top.overflyGen.Alert('Data de fim inv�lida.') == 0)
            return null;

        window.focus();

        if (sDataFim != '')
            txtDataFim.focus();

        return false;
    }

    if (!bPodeNulo) {
        numeroDias = window.top.daysBetween(txtDataInicio.value, txtDataFim.value);

        if (numeroDias > 365) {
            if (window.top.overflyGen.Alert('Excedido per�odo m�ximo 1 ano.') == 0)
                return null;

            window.focus();

            if (txtDataFim.value != '')
                txtDataFim.focus();

            return false;
        }
        else if ((selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('bExcessao', 1)) &&
			(numeroDias > 30)) {
            if (window.top.overflyGen.Alert('Excedido per�odo m�ximo 30 dias para ocorr�ncias.') == 0)
                return null;

            window.focus();

            if (txtDataFim.value != '')
                txtDataFim.focus();

            return false;
        }

        if (window.top.daysBetween(txtDataInicio.value, txtDataFim.value) < 0) {
            if (window.top.overflyGen.Alert('A data final deve ser maior ou igual a data inicial.') == 0)
                return null;

            window.focus();
            txtDataFim.focus();

            return false;
        }
    }

    return true;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData(oDSO, bNivelX) {
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    lockControlsInModalWin(true);

    fg.Editable = false;
    fg.ColHidden(getColIndexByColKey(fg, 'OK')) = true;

    if ((selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('bExcessao', 1)) &&
		(chkRecebido.checked) && (selComissao.value == 3)) {
        fg.Editable = true;
        fg.ColHidden(getColIndexByColKey(fg, 'OK')) = false;
    }
    else if ((selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('bExcessao', 1)) &&
	    (chkRecebido.checked) && (selComissao.value == 2)) {
        fg.Editable = true;
        fg.ColHidden(getColIndexByColKey(fg, 'OK')) = false;
    }

    var strSQL = '';
    var bRecebido = (chkRecebido.checked ? 1 : 0);
    var nTipoComissao = (chkRecebido.checked ? selComissao.value : 'NULL');
    var bMoedaSistema = moedaSistema();

    var sDataInicio = trimStr(txtDataInicio.value);
    var sDataFim = trimStr(txtDataFim.value);
    var sEmpresas = parseStrLists(selEmpresa, true, null);
    var sEquipes = parseStrLists(selVendedor, true, 1);
    var sVendedores = parseStrLists(selVendedor, true, 2);
    var sFamilias = parseStrLists(selFamilia, true, null);
    var sMarcas = parseStrLists(selMarca, true, null);
    var sGroupByID1, sGroupBy1, sOrderBy1;
    var sGroupByID2, sGroupBy2, sOrderBy2;
    var sGroupByID3, sGroupBy3, sOrderBy3;
    var sGroupByID4, sGroupBy4, sOrderBy4;

    var bDatas = '0';
    var bProdutos = '0';
    var bLocalidades = '0';

    sGroupByID1 = 'NULL';
    sGroupBy1 = 'NULL';
    sOrderBy1 = 'NULL';
    sGroupByID2 = 'NULL';
    sGroupBy2 = 'NULL';
    sOrderBy2 = 'NULL';
    sGroupByID3 = 'NULL';
    sGroupBy3 = 'NULL';
    sOrderBy3 = 'NULL';
    sGroupByID4 = 'NULL';
    sGroupBy4 = 'NULL';
    sOrderBy4 = 'NULL';

    var sFiltro = trimStr(txtFiltro.value);
    sFiltro = sFiltro.replace(/\'/g, '\'' + ' + CHAR(39) + ' + '\'');

    if (sFiltro != '')
        sFiltro = ' AND ' + sFiltro;

    if (glb_nEmpresaID != 7) {
        if (glb_nTipoPerfilComercialID == 263)//Gerente de produto
        {
            sFiltro += ' AND ProdutosEmpresa.ProprietarioID = ' + glb_nUsuarioID + ' ';
        }
        else if (glb_nTipoPerfilComercialID == 264)//Assistente de Produto
        {
            sFiltro += ' AND ProdutosEmpresa.AlternativoID = ' + glb_nUsuarioID + ' ';
        }
        else if (glb_nTipoPerfilComercialID == 265)//Vendedor e Televendas
        {
            sFiltro += ' AND Vendedores.PessoaID = ' + glb_nUsuarioID + ' ';
        }
    }

    bDatas = parametrosAdicionais('bDatas', bNivelX);

    if ((sFamilias != '') || (sMarcas != ''))
        bProdutos = '1';
    else
        bProdutos = parametrosAdicionais('bProdutos', bNivelX);

    bLocalidades = parametrosAdicionais('bLocalidades', bNivelX);

    sEmpresas = (sEmpresas == '' ? 'NULL' : '\'' + sEmpresas + '\'');
    sEquipes = (sEquipes == '' ? 'NULL' : '\'' + sEquipes + '\'');
    sVendedores = (sVendedores == '' ? 'NULL' : '\'' + sVendedores + '\'');
    sFamilias = (sFamilias == '' ? 'NULL' : '\'' + sFamilias + '\'');
    sMarcas = (sMarcas == '' ? 'NULL' : '\'' + sMarcas + '\'');

    if (!bNivelX) {
        if (selAgruparPor1.value != 0) {
            sGroupByID1 = selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('fldID', 1);
            sGroupBy1 = selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('fldName', 1);
            sOrderBy1 = selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('fldOrderby', 1);
        }

        if (selAgruparPor2.value != 0) {
            sGroupByID2 = selAgruparPor2.options.item(selAgruparPor2.selectedIndex).getAttribute('fldID', 1);
            sGroupBy2 = selAgruparPor2.options.item(selAgruparPor2.selectedIndex).getAttribute('fldName', 1);
            sOrderBy2 = selAgruparPor2.options.item(selAgruparPor2.selectedIndex).getAttribute('fldOrderby', 1);
        }

        if (selAgruparPor3.value != 0) {
            sGroupByID3 = selAgruparPor3.options.item(selAgruparPor3.selectedIndex).getAttribute('fldID', 1);
            sGroupBy3 = selAgruparPor3.options.item(selAgruparPor3.selectedIndex).getAttribute('fldName', 1);
            sOrderBy3 = selAgruparPor3.options.item(selAgruparPor3.selectedIndex).getAttribute('fldOrderby', 1);
        }

        if (selAgruparPor4.value != 0) {
            sGroupByID4 = selAgruparPor4.options.item(selAgruparPor4.selectedIndex).getAttribute('fldID', 1);
            sGroupBy4 = selAgruparPor4.options.item(selAgruparPor4.selectedIndex).getAttribute('fldName', 1);
            sOrderBy4 = selAgruparPor4.options.item(selAgruparPor4.selectedIndex).getAttribute('fldOrderby', 1);
        }
    }
    else {
        sGroupByID1 = selAgruparPorX.options.item(selAgruparPorX.selectedIndex).getAttribute('fldID', 1);
        sGroupBy1 = selAgruparPorX.options.item(selAgruparPorX.selectedIndex).getAttribute('fldName', 1);
        sOrderBy1 = selAgruparPorX.options.item(selAgruparPorX.selectedIndex).getAttribute('fldOrderby', 1);

        sFiltro += fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Filtro'));
    }

    sFiltro = (sFiltro == '' ? 'NULL' : '\'' + sFiltro + '\'');

    if (sDataInicio != '') {
        sDataInicio = normalizeDate_DateTime(sDataInicio, 1);

        sDataInicio = '\'' + sDataInicio + '\'';
    }
    else
        sDataInicio = 'NULL';

    if (sDataFim != '') {
        sDataFim = normalizeDate_DateTime(sDataFim, 1);

        sDataFim = '\'' + sDataFim + '\'';
    }
    else
        sDataFim = 'NULL';

    var geraExcelID = 1;
    var formato = 2; //Excel
    var sLinguaLogada = getDicCurrLang();

    var strParameters = "RelatorioID=" + geraExcelID + "&Formato=" + formato + "&lista_excel=" + glb_bListaExcel + "&sLinguaLogada=" + sLinguaLogada + "&glb_sEmpresaFantasia=" + glb_aEmpresaData[3] +
                        "&sFiltro=" + sFiltro + "&sDataInicio=" + sDataInicio + "&sDataFim=" + sDataFim + "&bRecebido=" + bRecebido + "&nTipoComissao=" + nTipoComissao +
                        "&bMoedaSistema=" + bMoedaSistema + "&bDatas=" + bDatas + "&bProdutos=" + bProdutos + "&bLocalidades=" + bLocalidades + "&sEmpresas=" + sEmpresas +
                        "&sEquipes=" + sEquipes + "&sVendedores=" + sVendedores + "&sFamilias=" + sFamilias + "&sMarcas=" + sMarcas +
                        "&sGroupByID1=" + sGroupByID1 + "&sGroupBy1=" + sGroupBy1 + "&sOrderBy1=" + sOrderBy1 +
                        "&sGroupByID2=" + sGroupByID2 + "&sGroupBy2=" + sGroupBy2 + "&sOrderBy2=" + sOrderBy2 +
                        "&sGroupByID3=" + sGroupByID3 + "&sGroupBy3=" + sGroupBy3 + "&sOrderBy3=" + sOrderBy3 +
                        "&sGroupByID4=" + sGroupByID4 + "&sGroupBy4=" + sGroupBy4 + "&sOrderBy4=" + sOrderBy4 +
                        "&selAgruparPor1=" + selAgruparPor1.value + "&selOrdem1=" + selOrdem1.value +
                        "&selAgruparPor2=" + selAgruparPor2.value + "&selOrdem2=" + selOrdem2.value +
                        "&selAgruparPor3=" + selAgruparPor3.value + "&selOrdem3=" + selOrdem3.value +
                        "&selAgruparPor4=" + selAgruparPor4.value + "&selOrdem4=" + selOrdem4.value +
                        "&bNivelX=" + bNivelX + "&selOrdemX=" + selOrdemX.value;


    if (glb_bListaExcel == 0) {
        oDSO.URL = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/ReportsGrid_ocorrencias.aspx?' + strParameters;
        if (!bNivelX)
            oDSO.ondatasetcomplete = fillGridData_DSC;
        else
            oDSO.ondatasetcomplete = agruparX_DSC;

        oDSO.Refresh();
    }
    else if (glb_bListaExcel == 1) {
        var frameReport = document.getElementById("frmReport");
        frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/ReportsGrid_ocorrencias.aspx?' + strParameters;
        lockControlsInModalWin(false);
        glb_bListaExcel = 0;
    }
}
/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_bOcorrencias = false;

    fg.Redraw = 0;

    if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)) {
        glb_GridIsBuilding = true;

        if (dsoGrid.recordset.RecordCount() > 100) {
            if ((selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('bExcessao', 1)))
                glb_bOcorrencias = true;
        }

        if (glb_bOcorrencias) {
            var _retMsg = window.top.overflyGen.Confirm('Este detalhamento possui ' + dsoGrid.recordset.RecordCount() +
				' ocorr�ncias.\nO detalhamento pode demorar um pouco.\n' +
				'Deseja detalhar assim mesmo?');

            if (_retMsg == 0)
                return null;
            else if (_retMsg == 1)
                glb_bOcorrencias = false;
        }

        buildGrid(glb_bOcorrencias);
    }

    paintGrid();

    glb_GridIsBuilding = false;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if (fg.Rows > 1)
        fg.Row = 1;

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;

    lockControlsInModalWin(false);

    setupBtns();
    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
    }
    else {
        ;
    }
}

function buildGrid(bOcorrencias) {
    var sfld1ID, sfld2ID, sfld3ID, sfld4ID;
    var sfldName1, sfldName2, sfldName3, sfldName4;
    var sfldFiltro1, sfldFiltro2, sfldFiltro3, sfldFiltro4;
    var nfld1ID = 0;
    var nfld2ID = 0;
    var nfld3ID = 0;
    var nfld4ID = 0;
    var nQtdOcorrencia2 = 0;
    var nQtdOcorrencia3 = 0;
    var nQtdOcorrencia4 = 0;
    var nTotalOcorrencia2 = 0;
    var nTotalOcorrencia3 = 0;
    var nTotalOcorrencia4 = 0;
    var nTotalFaturamento2 = 0;
    var nTotalFaturamento3 = 0;
    var nTotalFaturamento4 = 0;
    var nTotalContribuicao2 = 0;
    var nTotalContribuicao3 = 0;
    var nTotalContribuicao4 = 0;
    var nFgOldRow1 = 0;
    var nFgOldRow2 = 0;
    var nFgOldRow3 = 0;
    var nFgOldRow4 = 0;
    var sFiltro1 = '';
    var sFiltro2 = '';
    var sFiltro3 = '';
    var sFiltro4 = '';
    var nCmbOptionID1 = 0;
    var nCmbOptionID2 = 0;
    var nCmbOptionID3 = 0;
    var nCmbOptionID4 = 0;
    var nTotalGeral1 = 0;
    var nTotalGeral2 = 0;
    var nTotalGeral3 = 0;
    var nTotalGeral4 = 0;
    var sData = '';

    if (chkRecebido.checked) {
        if (selComissao.value == 1)
            sData = 'Ocorrencias.dtPagamentoComissao';
        else if (selComissao.value == 2)
            sData = 'Ocorrencias.dtAprovacaoComissao';
        else
            sData = 'Ocorrencias.dtBalancete';
    }
    else
        sData = 'Financeiro.dtVencimento';

    if (!bOcorrencias) {
        while ((!dsoGrid.recordset.EOF) && (selAgruparPor1.value != 0)) {
            // Nivel 1
            while (!dsoGrid.recordset.EOF) {
                sfld1ID = selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('fldID', 1);
                sfldName1 = selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('fldName', 1);
                sfldFiltro1 = selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('fldFiltro', 1);
                nfld1ID = dsoGrid.recordset[sfld1ID].value;
                nCmbOptionID1 = selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('optionID', 1);

                if (sfldFiltro1 != '')
                    sFiltro1 = ' AND ' + sfldFiltro1 + ' = ' + isDateToStr(nfld1ID, true);
                else
                    sFiltro1 = '';

                if (selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('bDatas', 1)) {
                    sFiltro1 += ' AND dbo.fn_Data_Zero(' + sData + ') BETWEEN ' + isDateToStr(dsoGrid.recordset['dtInicio'].value, true) + ' AND ';
                    sFiltro1 += isDateToStr(dsoGrid.recordset['dtFim'].value, true);
                }

                nNivel = 0;
                addRowinGrid(nNivel, sfldName1, fg.Rows - 1, sFiltro1, nCmbOptionID1, '', dsoGrid);
                nFgOldRow1 = fg.Row;
                nQtdOcorrencia2 = 0;
                nTotalOcorrencia2 = 0;
                nTotalFaturamento2 = 0;
                nTotalContribuicao2 = 0;

                // Nivel 2
                while ((!dsoGrid.recordset.EOF) && (selAgruparPor2.value != 0) &&
						(compare(nfld1ID, dsoGrid.recordset[sfld1ID].value))) {
                    sfld2ID = selAgruparPor2.options.item(selAgruparPor2.selectedIndex).getAttribute('fldID', 1);
                    sfldName2 = selAgruparPor2.options.item(selAgruparPor2.selectedIndex).getAttribute('fldName', 1);
                    sfldFiltro2 = selAgruparPor2.options.item(selAgruparPor2.selectedIndex).getAttribute('fldFiltro', 1);
                    nfld2ID = dsoGrid.recordset[sfld2ID].value;
                    nCmbOptionID2 = selAgruparPor2.options.item(selAgruparPor2.selectedIndex).getAttribute('optionID', 1);

                    sFiltro2 = sFiltro1;

                    if (sfldFiltro2 != '')
                        sFiltro2 += ' AND ' + sfldFiltro2 + ' = ' + isDateToStr(nfld2ID, true);

                    if (selAgruparPor2.options.item(selAgruparPor2.selectedIndex).getAttribute('bDatas', 1)) {
                        sFiltro2 += ' AND dbo.fn_Data_Zero(' + sData + ') BETWEEN ' + isDateToStr(dsoGrid.recordset['dtInicio'].value, true) + ' AND ';
                        sFiltro2 += isDateToStr(dsoGrid.recordset['dtFim'].value, true);
                    }

                    nNivel = 1;
                    addRowinGrid(nNivel, sfldName2, fg.Rows - 1, sFiltro2, nCmbOptionID2, '', dsoGrid);
                    nFgOldRow2 = fg.Row;
                    nQtdOcorrencia3 = 0;
                    nTotalOcorrencia3 = 0;
                    nTotalFaturamento3 = 0;
                    nTotalContribuicao3 = 0;

                    // Nivel 3
                    while ((!dsoGrid.recordset.EOF) && (selAgruparPor3.value != 0) &&
						(compare(nfld1ID, dsoGrid.recordset[sfld1ID].value)) &&
						(compare(nfld2ID, dsoGrid.recordset[sfld2ID].value))) {
                        sfld3ID = selAgruparPor3.options.item(selAgruparPor3.selectedIndex).getAttribute('fldID', 1);
                        sfldName3 = selAgruparPor3.options.item(selAgruparPor3.selectedIndex).getAttribute('fldName', 1);
                        sfldFiltro3 = selAgruparPor3.options.item(selAgruparPor3.selectedIndex).getAttribute('fldFiltro', 1);
                        nfld3ID = dsoGrid.recordset[sfld3ID].value;
                        nCmbOptionID3 = selAgruparPor3.options.item(selAgruparPor3.selectedIndex).getAttribute('optionID', 1);

                        sFiltro3 = sFiltro2;

                        if (sfldFiltro3 != '')
                            sFiltro3 += ' AND ' + sfldFiltro3 + ' = ' + isDateToStr(nfld3ID, true);

                        if (selAgruparPor3.options.item(selAgruparPor3.selectedIndex).getAttribute('bDatas', 1)) {
                            sFiltro3 += ' AND dbo.fn_Data_Zero(' + sData + ') BETWEEN ' + isDateToStr(dsoGrid.recordset['dtInicio'].value, true) + ' AND ';
                            sFiltro3 += isDateToStr(dsoGrid.recordset['dtFim'].value, true);
                        }

                        nNivel = 2;
                        addRowinGrid(nNivel, sfldName3, fg.Rows - 1, sFiltro3, nCmbOptionID3, '', dsoGrid);
                        nFgOldRow3 = fg.Row;
                        nQtdOcorrencia4 = 0;
                        nTotalOcorrencia4 = 0;
                        nTotalFaturamento4 = 0;
                        nTotalContribuicao4 = 0;

                        // Nivel 4
                        while ((!dsoGrid.recordset.EOF) && (selAgruparPor4.value != 0) &&
							(compare(nfld1ID, dsoGrid.recordset[sfld1ID].value)) &&
							(compare(nfld2ID, dsoGrid.recordset[sfld2ID].value)) &&
						(compare(nfld3ID, dsoGrid.recordset[sfld3ID].value))) {
                            sfld4ID = selAgruparPor4.options.item(selAgruparPor4.selectedIndex).getAttribute('fldID', 1);
                            sfldName4 = selAgruparPor4.options.item(selAgruparPor4.selectedIndex).getAttribute('fldName', 1);
                            sfldFiltro4 = selAgruparPor4.options.item(selAgruparPor4.selectedIndex).getAttribute('fldFiltro', 1);
                            nfld4ID = dsoGrid.recordset[sfld4ID].value;
                            nCmbOptionID4 = selAgruparPor4.options.item(selAgruparPor4.selectedIndex).getAttribute('optionID', 1);

                            sFiltro4 = sFiltro3;

                            if (sfldFiltro4 != '')
                                sFiltro4 += ' AND ' + sfldFiltro4 + ' = ' + isDateToStr(nfld4ID, true);

                            if (selAgruparPor4.options.item(selAgruparPor4.selectedIndex).getAttribute('bDatas', 1)) {
                                sFiltro4 += ' AND dbo.fn_Data_Zero(' + sData + ') BETWEEN ' + isDateToStr(dsoGrid.recordset['dtInicio'].value, true) + ' AND ';
                                sFiltro4 += isDateToStr(dsoGrid.recordset['dtFim'].value, true);
                            }

                            nNivel = 3;
                            addRowinGrid(nNivel, sfldName4, fg.Rows - 1, sFiltro4, nCmbOptionID4, '', dsoGrid);
                            nQtdOcorrencia4 += dsoGrid.recordset['Ocorrencias'].value;
                            nTotalOcorrencia4 += dsoGrid.recordset['ValorOcorrencia'].value;
                            nTotalFaturamento4 += dsoGrid.recordset['FaturamentoRecebido'].value;
                            nTotalContribuicao4 += dsoGrid.recordset['ContribuicaoRecebida'].value;

                            if (!dsoGrid.recordset.EOF)
                                dsoGrid.recordset.MoveNext();
                        }

                        if (selAgruparPor4.value > 0) {
                            nQtdOcorrencia3 += nQtdOcorrencia4;
                            nTotalOcorrencia3 += nTotalOcorrencia4;
                            nTotalFaturamento3 += nTotalFaturamento4;
                            nTotalContribuicao3 += nTotalContribuicao4;
                        }
                        else {
                            nQtdOcorrencia3 += dsoGrid.recordset['Ocorrencias'].value;
                            nTotalOcorrencia3 += dsoGrid.recordset['ValorOcorrencia'].value;
                            nTotalFaturamento3 += dsoGrid.recordset['FaturamentoRecebido'].value;
                            nTotalContribuicao3 += dsoGrid.recordset['ContribuicaoRecebida'].value;
                        }

                        if (nNivel > 2) {
                            fg.TextMatrix(nFgOldRow3, getColIndexByColKey(fg, 'Ocorrencias')) = nQtdOcorrencia4;
                            fg.TextMatrix(nFgOldRow3, getColIndexByColKey(fg, 'ValorOcorrencia')) = nTotalOcorrencia4;
                            fg.TextMatrix(nFgOldRow3, getColIndexByColKey(fg, 'ValorFaturamento')) = nTotalFaturamento4;
                            fg.TextMatrix(nFgOldRow3, getColIndexByColKey(fg, 'ValorContribuicao')) = nTotalContribuicao4;
                        }

                        if ((!dsoGrid.recordset.EOF) && (selAgruparPor4.value == 0))
                            dsoGrid.recordset.MoveNext();
                    }

                    if (selAgruparPor3.value > 0) {
                        nQtdOcorrencia2 += nQtdOcorrencia3;
                        nTotalOcorrencia2 += nTotalOcorrencia3;
                        nTotalFaturamento2 += nTotalFaturamento3;
                        nTotalContribuicao2 += nTotalContribuicao3;
                    }
                    else {
                        nQtdOcorrencia2 += dsoGrid.recordset['Ocorrencias'].value;
                        nTotalOcorrencia2 += dsoGrid.recordset['ValorOcorrencia'].value;
                        nTotalFaturamento2 += dsoGrid.recordset['FaturamentoRecebido'].value;
                        nTotalContribuicao2 += dsoGrid.recordset['ContribuicaoRecebida'].value;
                    }

                    if (nNivel > 1) {
                        fg.TextMatrix(nFgOldRow2, getColIndexByColKey(fg, 'Ocorrencias')) = nQtdOcorrencia3;
                        fg.TextMatrix(nFgOldRow2, getColIndexByColKey(fg, 'ValorOcorrencia')) = nTotalOcorrencia3;
                        fg.TextMatrix(nFgOldRow2, getColIndexByColKey(fg, 'ValorFaturamento')) = nTotalFaturamento3;
                        fg.TextMatrix(nFgOldRow2, getColIndexByColKey(fg, 'ValorContribuicao')) = nTotalContribuicao3;
                    }

                    if ((!dsoGrid.recordset.EOF) && (selAgruparPor3.value == 0))
                        dsoGrid.recordset.MoveNext();
                }

                if (selAgruparPor2.value == 0) {
                    nQtdOcorrencia2 = dsoGrid.recordset['Ocorrencias'].value;
                    nTotalOcorrencia2 = dsoGrid.recordset['ValorOcorrencia'].value;
                    nTotalFaturamento2 = dsoGrid.recordset['FaturamentoRecebido'].value;
                    nTotalContribuicao2 = dsoGrid.recordset['ContribuicaoRecebida'].value;
                }

                fg.TextMatrix(nFgOldRow1, getColIndexByColKey(fg, 'Ocorrencias')) = nQtdOcorrencia2;
                fg.TextMatrix(nFgOldRow1, getColIndexByColKey(fg, 'ValorOcorrencia')) = nTotalOcorrencia2;
                fg.TextMatrix(nFgOldRow1, getColIndexByColKey(fg, 'ValorFaturamento')) = nTotalFaturamento2;
                fg.TextMatrix(nFgOldRow1, getColIndexByColKey(fg, 'ValorContribuicao')) = nTotalContribuicao2;

                nTotalGeral1 += nQtdOcorrencia2;
                nTotalGeral2 += nTotalOcorrencia2;
                nTotalGeral3 += nTotalFaturamento2;
                nTotalGeral4 += nTotalContribuicao2;

                if ((!dsoGrid.recordset.EOF) && (selAgruparPor2.value == 0))
                    dsoGrid.recordset.MoveNext();
            }
            if (!dsoGrid.recordset.EOF)
                dsoGrid.recordset.MoveNext();
        }
    }
    else {
        while ((!dsoGrid.recordset.EOF) && (selAgruparPor1.value != 0)) {
            nTotalGeral2 += dsoGrid.recordset['ValorOcorrencia'].value;
            nTotalGeral3 += dsoGrid.recordset['FaturamentoRecebido'].value;
            nTotalGeral4 += dsoGrid.recordset['ContribuicaoRecebida'].value;
            dsoGrid.recordset.MoveNext();
        }
        nTotalGeral1 = dsoGrid.recordset.RecordCount();
    }

    addTotalRowinGrid(nTotalGeral1, nTotalGeral2, nTotalGeral3, nTotalGeral4);
}

function compare(vValue1, vValue2) {
    vValue1 = isDateToStr(vValue1, false);
    vValue2 = isDateToStr(vValue2, false);

    return (vValue1 == vValue2);
}

function isDateToStr(vValue, bChar) {
    if ((typeof (vValue)).toUpperCase() == 'DATE') {
        vValue = window.top.DateToStr(vValue, 'MM/DD/YYYY');

        if (bChar)
            vValue = '\'' + ' + CHAR(39) + ' + '\'' + vValue + '\'' + ' + CHAR(39) + ' + '\'' + ' ';
    }
    else if ((bChar) && ((typeof (vValue)).toUpperCase() == 'STRING'))
        vValue = '\'' + ' + CHAR(39) + ' + '\'' + vValue + '\'' + ' + CHAR(39) + ' + '\'' + ' ';

    return vValue;
}

function addRowinGrid(nNivel, sCampo, nLine, sFiltro, nCmbOptionID, nFinanceiroID, odso) {
    fg.Row = nLine;

    fg.AddItem('', fg.Row + 1);

    if (fg.Row < (fg.Rows - 1))
        fg.Row++;

    fg.ColDataType(getColIndexByColKey(fg, 'Detalhe')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Detalhe')) = odso.recordset[sCampo].value;

    fg.ColDataType(getColIndexByColKey(fg, 'Ocorrencias')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Ocorrencias')) = odso.recordset['Ocorrencias'].value;

    fg.ColDataType(getColIndexByColKey(fg, 'Moeda')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Moeda')) = odso.recordset['SimboloMoeda'].value;

    fg.ColDataType(getColIndexByColKey(fg, 'ValorOcorrencia')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorOcorrencia')) = odso.recordset['ValorOcorrencia'].value;

    fg.ColDataType(getColIndexByColKey(fg, 'ValorFaturamento')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorFaturamento')) = odso.recordset['FaturamentoRecebido'].value;

    fg.ColDataType(getColIndexByColKey(fg, 'ValorContribuicao')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorContribuicao')) = odso.recordset['ContribuicaoRecebida'].value;

    fg.ColDataType(getColIndexByColKey(fg, 'Filtro')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Filtro')) = sFiltro;

    fg.ColDataType(getColIndexByColKey(fg, 'CmbOptionID')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CmbOptionID')) = nCmbOptionID;

    fg.ColDataType(getColIndexByColKey(fg, 'FinanceiroID')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FinanceiroID')) = nFinanceiroID;

    fg.ColDataType(getColIndexByColKey(fg, 'OK')) = 11;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'OK')) = 0;

    if ((selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('bExcessao', 1)) &&
	    (glb_bDireitoA1) && (glb_bDireitoA2) &&
		(chkRecebido.checked) && (selComissao.value == 3)) {
        fg.ColDataType(getColIndexByColKey(fg, 'FinOcorrenciaID')) = 12;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FinOcorrenciaID')) = odso.recordset['FinOcorrenciaID'].value;
    }
    else if ((selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('bExcessao', 1)) &&
	    (glb_bDireitoFinanceiro) && (glb_bDireitoA1) && (glb_bDireitoA2) &&
	    (chkRecebido.checked) && (selComissao.value == 2)) {
        fg.ColDataType(getColIndexByColKey(fg, 'FinOcorrenciaID')) = 12;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FinOcorrenciaID')) = odso.recordset['FinOcorrenciaID'].value;
    }

    fg.IsSubTotal(fg.Row) = true;
    fg.RowOutlineLevel(fg.Row) = nNivel;
}

function addTotalRowinGrid(nTotalGeral1, nTotalGeral2, nTotalGeral3, nTotalGeral4) {
    fg.Row = 0;
    fg.AddItem('', fg.Row + 1);
    fg.Row = 1;

    fg.ColDataType(getColIndexByColKey(fg, 'Detalhe')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Detalhe')) = 'Totais Gerais';

    fg.ColDataType(getColIndexByColKey(fg, 'Moeda')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Moeda')) = selMoedaID.options.item(selMoedaID.selectedIndex).innerText;

    fg.ColDataType(getColIndexByColKey(fg, 'Ocorrencias')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Ocorrencias')) = nTotalGeral1;

    fg.ColDataType(getColIndexByColKey(fg, 'ValorOcorrencia')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorOcorrencia')) = nTotalGeral2;

    fg.ColDataType(getColIndexByColKey(fg, 'ValorFaturamento')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorFaturamento')) = nTotalGeral3;

    fg.ColDataType(getColIndexByColKey(fg, 'ValorContribuicao')) = 12;
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorContribuicao')) = nTotalGeral4;

    if (glb_bOcorrencias) {
        fg.ColDataType(getColIndexByColKey(fg, 'OK')) = 11;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'OK')) = 0;
    }

    fg.IsSubTotal(fg.Row) = true;
    fg.RowOutlineLevel(fg.Row) = -1;
}

function paintGrid() {
    putMasksInGrid(fg, ['', '999999', '', '999999999.99', '999999999.99', '999999999.99', '', '', ''],
                       ['', '######', '', '(###,###,##0.00)', '(###,###,##0.00)', '(###,###,##0.00)', '', '', '']);

    alignColsInGrid(fg, [1, 3, 4, 5]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);

    for (i = 1; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorOcorrencia')) < 0) {
            fg.Select(i, getColIndexByColKey(fg, 'ValorOcorrencia'), i, getColIndexByColKey(fg, 'ValorOcorrencia'));
            fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }

        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorFaturamento')) < 0) {
            fg.Select(i, getColIndexByColKey(fg, 'ValorFaturamento'), i, getColIndexByColKey(fg, 'ValorFaturamento'));
            fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }

        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorContribuicao')) < 0) {
            fg.Select(i, getColIndexByColKey(fg, 'ValorContribuicao'), i, getColIndexByColKey(fg, 'ValorContribuicao'));
            fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }
    }
}

function parseStrLists(oCmb, bOnlySelected, nIndice) {
    var retVal = '';
    var i = 0;

    for (i = 0; i < oCmb.length; i++) {
        if (nIndice != null) {
            if (oCmb.options[i].Indice != nIndice)
                continue;
        }

        if (bOnlySelected) {
            if (oCmb.options[i].selected)
                retVal += oCmb.options[i].value + '/';
        }
        else
            retVal += oCmb.options[i].value + '/';
    }

    if (retVal != '')
        retVal = '/' + retVal;

    return retVal;
}

/********************************************************************
Fecha a janela se mostra-la
********************************************************************/
function unloadThisWin() {
    if (glb_timerUnloadWin != null) {
        window.clearInterval(glb_timerUnloadWin);
        glb_timerUnloadWin = null;
    }

    if (window.top.overflyGen.Alert('Nenhum registro dispon�vel.') == 0)
        return null;

    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, 'NOREG');
}

/********************************************************************
Localiza a linha do grid por ContaMaeID
********************************************************************/
function findRowByContaMae(nContaMaeID) {
    var i = 0;

    for (i = 0; i < fg.Rows; i++) {
        if (parseInt(fg.TextMatrix(i, getColIndexByColKey(fg, 'ContaID')), 10) == parseInt(nContaMaeID))
            return i;
    }

    return fg.Rows - 1;
}

function js_fg_DblClick() {
    var i;
    var bFill = true;
    var oldEditable = fg.Editable;

    if (fg.ColHidden(getColIndexByColKey(fg, 'OK')))
        agruparX();
    else {
        glb_GridIsBuilding = true;
        fg.Editable = false;
        fg.Redraw = 0;
        lockControlsInModalWin(true);

        for (i = 1; i < fg.Rows; i++)
            fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = glb_bOKDblClick;

        glb_bOKDblClick = !glb_bOKDblClick;
        lockControlsInModalWin(false);
        fg.Editable = oldEditable;
        fg.Redraw = 2;
        window.focus();
        fg.focus();
        setupBtns();
        glb_GridIsBuilding = false;
    }
}

function fg_modalocorrencias_BeforeRowColChange(OldRow, OldCol) {
    if (glb_GridIsBuilding)
        return true;

    glb_nOldRow = OldRow;
    glb_nOldCol = OldCol;
    return false;
}

function fg_modalocorrencias_AfterRowColChange() {
    if (glb_GridIsBuilding)
        return true;

    if (glb_nOldRow != fg.Row) {
        var nDefalut = -1;
        nCmbOptionID = parseInt(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CmbOptionID')), 10);
        objNiveis = findaNiveis(nCmbOptionID);

        if (objNiveis != null)
            nDefalut = objNiveis.nItemDefault;

        fillCmbEx(nDefalut);
        setupBtns();
    }
}

function js_modalocorrencias_BeforeEdit(grid, Row, Col) {
    if ((Col != getColIndexByColKey(grid, 'OK')) && (grid.Editable == true)) {
        grid.Col = getColIndexByColKey(grid, 'OK');
    }
    else if ((grid.Row == 1) && (grid.Editable == true) && (!glb_bOcorrencias))
        grid.Row = 2;

    setupBtns();
}

function js_modalocorrencias_AfterEdit(Row, Col) {
    if (Col != getColIndexByColKey(fg, 'OK'))
        fg.EditText = fg.TextMatrix(Row, Col);

    setupBtns();
}

function agruparX() {
    if ((fg.Row < 1) || (selAgruparPorX.value == 0))
        return true;

    var nNivel = fg.RowOutlineLevel(fg.Row);

    if (nNivel < glb_nLastNivelSelected) {
        var node = fg.GetNode();

        try {
            node.Expanded = !node.Expanded;
        }
        catch (e) {
            ;
        }
    }
    else if (nNivel >= glb_nLastNivelSelected) {

        var nOcorrencias = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Ocorrencias'));

        if ((!isNaN(nOcorrencias)) && (nOcorrencias > 1000)) {
            var _retMsg = window.top.overflyGen.Confirm('Este detalhamento possui ' + nOcorrencias.toString() +
				' ocorr�ncias.\nO detalhamento pode demorar um pouco.\n' +
				'Deseja detalhar assim mesmo?');

            if (_retMsg != 1) {
                lockControlsInModalWin(false);
                return null;
            }
        }

        deletaContasFilhas();
        fillGridData(dsoGridNivelX, true);
    }
}

function agruparX_DSC() {
    var nNivel = fg.RowOutlineLevel(fg.Row);
    var sfldID = selAgruparPorX.options.item(selAgruparPorX.selectedIndex).getAttribute('fldID', 1);
    var sfldName = selAgruparPorX.options.item(selAgruparPorX.selectedIndex).getAttribute('fldName', 1);
    var sfldFiltro = selAgruparPorX.options.item(selAgruparPorX.selectedIndex).getAttribute('fldFiltro', 1);
    var nCmbOptionID = selAgruparPorX.options.item(selAgruparPorX.selectedIndex).getAttribute('optionID', 1);
    var nfldID;
    var sFiltroMae = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Filtro'));
    var nFinanceiroID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FinanceiroID'));
    var sFiltro = '';
    var currentRow = fg.Row;
    var sData = '';

    if (chkRecebido.checked) {
        if (selComissao.value == 1)
            sData = 'Ocorrencias.dtPagamentoComissao';
        else if (selComissao.value == 2)
            sData = 'Ocorrencias.dtAprovacaoComissao';
        else
            sData = 'Ocorrencias.dtBalancete';
    }
    else
        sData = 'Financeiro.dtVencimento';

    if (!(dsoGridNivelX.recordset.BOF && dsoGridNivelX.recordset.EOF)) {
        glb_GridIsBuilding = true;
        fg.Redraw = 0;

        while (!dsoGridNivelX.recordset.EOF) {
            // Ocorreu erro no servidor
            if (dsoGridNivelX.recordset.fields[0].name == 'Msg') {
                if (window.top.overflyGen.Alert(dsoGridNivelX.recordset['Msg'].value) == 0)
                    return null;

                break;
            }

            nfldID = dsoGridNivelX.recordset[sfldID].value;

            if ((nFinanceiroID == '') && (sfldID == 'FinanceiroID'))
                nFinanceiroID = nfldID;

            if (sfldFiltro != '')
                sFiltro = ' AND ' + sfldFiltro + ' = ' + isDateToStr(nfldID, true);
            else
                sFiltro = '';

            if (selAgruparPorX.options.item(selAgruparPorX.selectedIndex).getAttribute('bDatas', 1)) {
                sFiltro += ' AND dbo.fn_Data_Zero(' + sData + ') BETWEEN ' + isDateToStr(dsoGridNivelX.recordset['dtInicio'].value, true) + ' AND ';
                sFiltro += isDateToStr(dsoGridNivelX.recordset['dtFim'].value, true);
            }

            sFiltro += ' ' + sFiltroMae;

            addRowinGrid(nNivel + 1, sfldName, fg.Row, sFiltro, nCmbOptionID, nFinanceiroID, dsoGridNivelX);

            dsoGridNivelX.recordset.MoveNext();
        }

        fg.Redraw = 2;
    }

    paintGrid();

    glb_GridIsBuilding = false;

    fg.Row = currentRow;
    fg.TopRow = fg.Row;
    fg_modalocorrencias_AfterRowColChange();

    lockControlsInModalWin(false);

    setupBtns();
}

function setupBtns() {
    var nNivel;
    var i = 0;

    btnFillGrid.disabled = true;
    btnDetalhar.disabled = true;
    btnFinanceiro.disabled = true;
    btnExcel.src = glb_LUPA_IMAGES[1].src;
    btnPagar.style.visibility = 'hidden';

    if ((selAgruparPor1.value > 0) && (parseStrLists(selEmpresa, true, null) != '') &&
		 ((glb_bDireitoA2) || (parseStrLists(selVendedor, true, null) != '')))
        btnFillGrid.disabled = false;

    if ((fg.Rows >= 2) && (selAgruparPorX.value > 0)) {
        nNivel = fg.RowOutlineLevel(fg.Row);

        if (nNivel >= glb_nLastNivelSelected)
            btnDetalhar.disabled = false;
    }

    if (fg.Rows >= 2) {
        if ((fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FinanceiroID')) != null) &&
			(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FinanceiroID')) != ''))
            btnFinanceiro.disabled = false;
    }

    if (fg.Rows > 1)
        btnExcel.src = glb_LUPA_IMAGES[0].src;

    if (fg.Rows > 1) {
        if ((selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('bExcessao', 1)) &&
			(glb_bDireitoA1) && (glb_bDireitoA2) &&
			(chkRecebido.checked) && (selComissao.value == 3))
            btnPagar.style.visibility = 'inherit';
        else if ((selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('bExcessao', 1)) &&
			(glb_bDireitoFinanceiro) && (glb_bDireitoA1) && (glb_bDireitoA2) &&
			(chkRecebido.checked) && (selComissao.value == 2))
            btnPagar.style.visibility = 'inherit';
    }
}

function deletaContasFilhas() {
    var nRow;
    var nNivel = fg.RowOutlineLevel(fg.Row);

    nRow = fg.Row;

    if (nRow <= fg.Rows - 2) {
        nRow++;

        while ((nRow <= fg.Rows - 1) && (nNivel < fg.RowOutlineLevel(nRow)))
            fg.RemoveItem(nRow);
    }
}

function selEmpresa_onchange() {
    var nMoedaSistemaID = selMoedaID.getAttribute('MoedaID', 1);
    var sMoedaSistema = selMoedaID.getAttribute('SimboloMoeda', 1);
    var nMoedaID = 0;
    var sMoedaID = '';
    var nMoeda2ID = 0;
    var i = 0;
    var j = 0;
    var bInsere = false;

    clearComboEx([selMoedaID.id]);

    if (nMoedaSistemaID != null) {
        var oOption = document.createElement("OPTION");
        oOption.text = sMoedaSistema;
        oOption.value = nMoedaSistemaID;
        selMoedaID.add(oOption);
    }

    for (i = 0; i < selEmpresa.options.length; i++) {
        bInsere = true;

        if (selEmpresa.options.item(i).selected) {
            nMoedaID = selEmpresa.options.item(i).getAttribute('MoedaID', 1);
            sMoedaID = selEmpresa.options.item(i).getAttribute('SimboloMoeda', 1);

            if ((lookUpValueInCmb(selMoedaID, nMoedaID) == -1) &&
				 (nMoedaID != nMoedaSistemaID)) {
                for (j = 0; j < selEmpresa.options.length; j++) {
                    if (selEmpresa.options.item(j).selected) {
                        nMoeda2ID = selEmpresa.options.item(j).getAttribute('MoedaID', 1);

                        if (nMoeda2ID != nMoedaID)
                            bInsere = false;
                    }
                }

                if (bInsere) {
                    var oOption = document.createElement("OPTION");
                    oOption.text = sMoedaID;
                    oOption.value = nMoedaID;
                    oOption.selected = true;
                    selMoedaID.add(oOption);
                }
            }
        }
    }

    setupBtns();
}

function selOrdem_onchange() {
    var aCmbs = new Array(selOrdem1, selOrdem2, selOrdem3, selOrdem4, selOrdemX);
    var iChanged = this.getAttribute('cmbIndex');

    fg.Rows = 1;

    if (iChanged < 4) {
        for (i = iChanged + 1; i < aCmbs.length; i++)
            aCmbs[i].selectedIndex = aCmbs[i - 1].selectedIndex;
    }
}

function selComissao_onchange() {
    fg.Rows = 1;
    setDefault();
}

function selAgruparPor_onchange(ocmb) {
    if (ocmb == null)
        ocmb = this;

    var aCmbs = new Array(selAgruparPor1, selAgruparPor2, selAgruparPor3, selAgruparPor4);
    var aCmbs2 = new Array(selOrdem1, selOrdem2, selOrdem3, selOrdem4);
    var iChanged = ocmb.getAttribute('cmbIndex');

    fg.Rows = 1;

    if (iChanged < 3) {
        for (i = iChanged + 1; i < aCmbs.length; i++) {
            clearComboEx([aCmbs[i].id]);
            aCmbs[i].disabled = true;
            aCmbs2[i].selectedIndex = aCmbs2[i - 1].selectedIndex;
            aCmbs2[i].disabled = true;
        }
        fillCmbs(aCmbs[iChanged + 1]);

        if (aCmbs[iChanged + 1].options.length > 1) {
            aCmbs[iChanged + 1].disabled = false;
            aCmbs2[iChanged + 1].disabled = false;
        }
    }

    if (iChanged == 0)
        glb_nLastNivelSelected = 0;
    else if ((iChanged > 0) && (ocmb.selectedIndex > 0))
        glb_nLastNivelSelected = iChanged;
    else
        glb_nLastNivelSelected = iChanged - 1;

    fillCmbEx(ocmb.options.item(ocmb.selectedIndex).getAttribute('nItemDefault', 1));

    setupBtns();
}

function setFieldsVisibility() {
    var sVisible = (chkPesquisa.checked ? 'inherit' : 'hidden');

    lblAgruparPor2.style.visibility = sVisible;
    selAgruparPor2.style.visibility = sVisible;
    lblAgruparPor1.style.visibility = sVisible;
    selAgruparPor1.style.visibility = sVisible;
    lblOrdem1.style.visibility = sVisible;
    selOrdem1.style.visibility = sVisible;
    lblOrdem2.style.visibility = sVisible;
    selOrdem2.style.visibility = sVisible;
    lblEmpresa.style.visibility = sVisible;
    selEmpresa.style.visibility = sVisible;
    lblVendedor.style.visibility = sVisible;
    selVendedor.style.visibility = sVisible;
    lblFamilia.style.visibility = sVisible;
    selFamilia.style.visibility = sVisible;
    lblMarca.style.visibility = sVisible;
    selMarca.style.visibility = sVisible;
    lblAgruparPor3.style.visibility = sVisible;
    selAgruparPor3.style.visibility = sVisible;
    lblOrdem3.style.visibility = sVisible;
    selOrdem3.style.visibility = sVisible;
    lblAgruparPor4.style.visibility = sVisible;
    selAgruparPor4.style.visibility = sVisible;
    lblOrdem4.style.visibility = sVisible;
    selOrdem4.style.visibility = sVisible;
    lblFiltro.style.visibility = sVisible;
    txtFiltro.style.visibility = sVisible;
}

function fillCmbs(oCmb) {
    var iCmb = oCmb.getAttribute('cmbIndex');
    var aExceptions = getCmbExceptions(iCmb);

    for (i = 0; i < glb_aNiveis.length; i++) {
        if ((glb_aNiveis[i].optionID == 0) && (iCmb == 0))
            continue;

        if (glb_aNiveis[i].bGalho) {
            if ((glb_aNiveis[i].bExcessao) && (!chkRecebido.checked))
                continue;

            if ((glb_aNiveis[i].bExcessao) && (iCmb > 0))
                continue;

            if (ascan(aExceptions, glb_aNiveis[i].optionID, false) == -1) {
                var oOption = document.createElement("OPTION");
                oOption.text = glb_aNiveis[i].sDisplay;
                oOption.value = i;
                oOption.setAttribute('optionID', glb_aNiveis[i].optionID, 1);
                oOption.setAttribute('fldID', glb_aNiveis[i].fldID, 1);
                oOption.setAttribute('fldName', glb_aNiveis[i].fldName, 1);
                oOption.setAttribute('fldOrderby', glb_aNiveis[i].fldOrderby, 1);
                oOption.setAttribute('fldFiltro', glb_aNiveis[i].fldFiltro, 1);
                oOption.setAttribute('bDatas', glb_aNiveis[i].bDatas, 1);
                oOption.setAttribute('bProdutos', glb_aNiveis[i].bProdutos, 1);
                oOption.setAttribute('bLocalidades', glb_aNiveis[i].bLocalidades, 1);
                oOption.setAttribute('aExceptions', glb_aNiveis[i].aExceptions, 1);
                oOption.setAttribute('aPreRequisitos', glb_aNiveis[i].aPreRequisitos, 1);
                oOption.setAttribute('nItemDefault', glb_aNiveis[i].nItemDefault, 1);
                oOption.setAttribute('bExcessao', glb_aNiveis[i].bExcessao, 1);
                oCmb.add(oOption);
            }
        }
    }
}

function fillCmbEx(nDefalutItemToSelect) {
    var aExceptions = getCmbExceptions(glb_nLastNivelSelected + 1);
    var aSelecionados = getCmbPreRequisitos();
    clearComboEx(['selAgruparPorX']);

    var i;
    var objNiveis = null;
    var nRow;
    var nLastNivel;
    var nNivel;
    var nCmbOptionID = 0;
    var nIndexToSelect = -1;

    if (fg.Rows > 1) {
        nRow = fg.Row;
        nLastNivel = fg.RowOutlineLevel(nRow) + 1;
        nNivel = fg.RowOutlineLevel(nRow);

        while ((nRow >= 2) && (nNivel >= 0)) {
            nNivel = fg.RowOutlineLevel(nRow);

            if (nNivel < nLastNivel) {
                nCmbOptionID = parseInt(fg.TextMatrix(nRow, getColIndexByColKey(fg, 'CmbOptionID')), 10);

                objNiveis = findaNiveis(nCmbOptionID);

                if (objNiveis != null) {

                    for (i = 0; i < objNiveis.aExceptions.length; i++) {
                        aExceptions[aExceptions.length] = objNiveis.aExceptions[i];
                    }
                    aSelecionados[aSelecionados.length] = nCmbOptionID;
                }
                nLastNivel = nNivel;
            }
            nRow--;
        }
    }

    for (i = 1; i < glb_aNiveis.length; i++) {
        if (glb_aNiveis[i].bFolha) {
            if ((glb_aNiveis[i].bExcessao) && (!chkRecebido.checked))
                continue;

            if (ascan(aExceptions, glb_aNiveis[i].optionID, false) == -1) {

                if ((glb_aNiveis[i].aPreRequisitos.length == 0) ||
					(temPreRequisito(aSelecionados, glb_aNiveis[i].aPreRequisitos))) {
                    var oOption = document.createElement("OPTION");
                    oOption.text = glb_aNiveis[i].sDisplay;
                    oOption.value = i;
                    oOption.setAttribute('optionID', glb_aNiveis[i].optionID, 1);
                    oOption.setAttribute('fldID', glb_aNiveis[i].fldID, 1);
                    oOption.setAttribute('fldName', glb_aNiveis[i].fldName, 1);
                    oOption.setAttribute('fldFiltro', glb_aNiveis[i].fldFiltro, 1);
                    oOption.setAttribute('bDatas', glb_aNiveis[i].bDatas, 1);
                    oOption.setAttribute('bProdutos', glb_aNiveis[i].bProdutos, 1);
                    oOption.setAttribute('bLocalidades', glb_aNiveis[i].bLocalidades, 1);
                    oOption.setAttribute('aExceptions', glb_aNiveis[i].aExceptions, 1);
                    oOption.setAttribute('aPreRequisitos', glb_aNiveis[i].aPreRequisitos, 1);
                    oOption.setAttribute('bExcessao', glb_aNiveis[i].bExcessao, 1);
                    selAgruparPorX.add(oOption);
                }
            }
        }
    }

    selAgruparPorX.disabled = (selAgruparPorX.options.length == 0);
    selOrdemX.disabled = (selAgruparPorX.options.length == 0);

    if (nDefalutItemToSelect >= 0) {
        nIndexToSelect = findCmbIndexByOptionID(selAgruparPorX, nDefalutItemToSelect);

        if (nIndexToSelect >= 0)
            selAgruparPorX.selectedIndex = nIndexToSelect;
    }
}

function temPreRequisito(aSelecionados, aPreRequisitos) {
    var retVal = false;
    var i = 0;

    for (i = 0; i < aPreRequisitos.length; i++) {
        if (ascan(aSelecionados, aPreRequisitos[i], false) >= 0) {
            retVal = true;
            break;
        }
    }

    return retVal;
}

function getCmbPreRequisitos() {
    var aCmbs = new Array(selAgruparPor1, selAgruparPor2, selAgruparPor3, selAgruparPor4);
    var retVal = new Array();

    for (i = 0; i < aCmbs.length; i++) {
        if (aCmbs[i].value > 0)
            retVal[retVal.length] = aCmbs[i].options.item(aCmbs[i].selectedIndex).getAttribute('optionID', 1);
    }

    return retVal;
}

function getCmbExceptions(iCmb) {
    var aCmbs = new Array(selAgruparPor1, selAgruparPor2, selAgruparPor3, selAgruparPor4);
    var retVal = new Array();
    var nCounter = 0;

    if (iCmb == 0)
        return retVal;

    for (i = 0; i < iCmb; i++) {
        for (j = 0; j < aCmbs[i].options.item(aCmbs[i].selectedIndex).getAttribute('aExceptions', 1).length; j++) {
            retVal[nCounter] = aCmbs[i].options.item(aCmbs[i].selectedIndex).getAttribute('aExceptions', 1)[j];
            nCounter++;
        }
    }

    return retVal;
}

function parametrosAdicionais(sTipo, bNivelX) {
    var sParametro = '0';
    var bParametro = false;
    var nCmbOptionID = 0;
    var nRow;
    var nNivel = 0;
    var nLastNivel = 0;
    var objNiveis = null;

    if (selAgruparPor1.value > 0) {
        if (selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute(sTipo, 1))
            sParametro = '1';
    }
    if (selAgruparPor2.value > 0) {
        if (selAgruparPor2.options.item(selAgruparPor2.selectedIndex).getAttribute(sTipo, 1))
            sParametro = '1';
    }
    if (selAgruparPor3.value > 0) {
        if (selAgruparPor3.options.item(selAgruparPor3.selectedIndex).getAttribute(sTipo, 1))
            sParametro = '1';
    }
    if (selAgruparPor4.value > 0) {
        if (selAgruparPor4.options.item(selAgruparPor4.selectedIndex).getAttribute(sTipo, 1))
            sParametro = '1';
    }

    if ((bNivelX) && (sParametro == '0')) {
        if ((selAgruparPorX.value > 0) && (selAgruparPorX.options.item(selAgruparPorX.selectedIndex).getAttribute(sTipo, 1)))
            sParametro = '1';
        else {
            nRow = fg.Row;

            nLastNivel = fg.RowOutlineLevel(nRow) + 1;

            while ((nRow >= 2) && (nNivel >= 0)) {
                nNivel = fg.RowOutlineLevel(nRow);

                if (nNivel < nLastNivel) {
                    nCmbOptionID = parseInt(fg.TextMatrix(nRow, getColIndexByColKey(fg, 'CmbOptionID')), 10);

                    objNiveis = findaNiveis(nCmbOptionID);

                    if (objNiveis != null) {
                        bParametro = eval('objNiveis.' + sTipo);

                        if (bParametro) {
                            sParametro = '1';
                            break;
                        }
                    }
                    nLastNivel = nNivel;
                }

                nRow--;
            }
        }
    }

    return sParametro;
}

function findaNiveis(nOptionID) {
    var retVal = null;
    var i = 0;

    for (i = 0; i < glb_aNiveis.length; i++) {
        if (glb_aNiveis[i].optionID == nOptionID) {
            retVal = glb_aNiveis[i];
            break;
        }
    }

    return retVal;
}

function findCmbIndexByOptionID(oCmb, nOptionID) {
    var retVal = -1;
    var i = 0;

    for (i = 0; i < oCmb.options.length; i++) {
        if (oCmb.options.item(i).getAttribute('optionID', 1) == nOptionID) {
            retVal = i;
            break;
        }
    }

    return retVal;
}

function detalharFinanceiro() {
    var nFinanceiroID = '';

    if (fg.Rows > 1)
        nFinanceiroID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FinanceiroID'));

    if ((nFinanceiroID != null) && (nFinanceiroID != ''))
        sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(glb_aEmpresaData[0], nFinanceiroID));
}

function setDefault() {
    btnPagar.value = 'Aprovar';

    if (selComissao.value == 2)
        btnPagar.value = 'Pagar';

    clearComboEx([selAgruparPor1.id, selAgruparPor2.id, selAgruparPor3.id, selAgruparPor4.id, selAgruparPorX.id]);

    if (!chkRecebido.checked) {
        fillSelectCmb(selAgruparPor1, 3);
        fillSelectCmb(selAgruparPor2, 10);
        fillSelectCmb(selAgruparPor3, 1);

        txtDataInicio.value = glb_aDatas[0].DataInicio;
        txtDataFim.value = glb_aDatas[0].DataFim;
    }
    else {
        if (selComissao.value == 4) {
            fillSelectCmb(selAgruparPor1, 1);
            fillSelectCmb(selAgruparPor2, 4);
            fillSelectCmb(selAgruparPor3, 3);
        }
        else {
            fillSelectCmb(selAgruparPor1, 3);
            fillSelectCmb(selAgruparPor2, 4);
            fillSelectCmb(selAgruparPor3, 1);
        }

        txtDataInicio.value = glb_aDatas[selComissao.value].DataInicio;
        txtDataFim.value = glb_aDatas[selComissao.value].DataFim;

    }
}

function fillSelectCmb(oCmb, nOptionID) {
    var nIndexToSelect = 0;
    clearComboEx([oCmb.id]);
    fillCmbs(oCmb);
    nIndexToSelect = findCmbIndexByOptionID(oCmb, nOptionID);

    if (nIndexToSelect >= 0) {
        oCmb.selectedIndex = nIndexToSelect;
        selAgruparPor_onchange(oCmb);
    }
}

function pagar() {
    var strPars = new String();
    var i = 0;
    var sEventos = '';
    var bGrava = false;
    var nBytesAcum = 0;
    var nTipo = 0;
    var bHasOK = false;
    var iInicial = 1;

    if (!glb_bOcorrencias)
        iInicial = 2;

    if ((selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('bExcessao', 1)) &&
	    (glb_bDireitoA1) && (glb_bDireitoA2) &&
		(chkRecebido.checked) && (selComissao.value == 3)) {
        for (i = iInicial; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
                bHasOK = true;
                break;
            }
        }
    }
    else if ((selAgruparPor1.options.item(selAgruparPor1.selectedIndex).getAttribute('bExcessao', 1)) &&
	    (glb_bDireitoFinanceiro) && (glb_bDireitoA1) && (glb_bDireitoA2) &&
	    (chkRecebido.checked) && (selComissao.value == 2)) {
        for (i = iInicial; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
                bHasOK = true;
                break;
            }
        }
    }

    if (!bHasOK) {
        if (window.top.overflyGen.Alert('Selecione uma ocorr�ncia.') == 0)
            return null;

        return null;
    }

    nTipo = (selComissao.value == 3 ? 1 : 2);
    glb_aSendDataToServer = [];

    lockControlsInModalWin(true);

    strPars = '';

    if (!glb_bOcorrencias) {
        for (i = 2; i < fg.Rows; i++) {
            if ((getCellValueByColKey(fg, 'OK', i) == 0) || (getCellValueByColKey(fg, 'FinOcorrenciaID', i) == ''))
                continue;

            nBytesAcum += strPars.length;
            bGrava = true;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                    strPars = '';
                }

                strPars += '?nTipo=' + escape(nTipo);
            }

            strPars += '&nFinOcorrenciaID=' + escape(getCellValueByColKey(fg, 'FinOcorrenciaID', i));
        }
    }
    else {
        if (!((dsoGrid.recordset.BOF) && (dsoGrid.recordset.EOF)))
            dsoGrid.recordset.moveFirst();

        while (!dsoGrid.recordset.EOF) {
            nBytesAcum += strPars.length;
            bGrava = true;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                    strPars = '';
                }

                strPars += '?nTipo=' + escape(nTipo);
            }

            strPars += '&nFinOcorrenciaID=' + escape(dsoGrid.recordset['FinOcorrenciaID'].value);
            dsoGrid.recordset.moveNext();
        }
    }

    if (bGrava)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

    if (!bGrava) {
        if (window.top.overflyGen.Alert('Selecione alguma ocorrencia!') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
    else {
        glb_nPointToaSendDataToServer = 0;
        sendDataToServer();
    }
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/gravarocorrencias.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            fg.Rows = 1;
            glb_OcorrenciasTimerInt = window.setInterval('fillGridData(dsoGrid, false)', 10, 'JavaScript');
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        fg.Rows = 1;
        lockControlsInModalWin(false);
        glb_OcorrenciasTimerInt = window.setInterval('fillGridData(dsoGrid, false)', 10, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        dsoGrava.recordset.MoveFirst();
        if ((dsoGrava.recordset['Erros'].value != null) &&
			 (dsoGrava.recordset['Erros'].value != '')) {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['Erros'].value) == 0)
                return null;
        }
    }



    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

function moedaSistema() {
    var nMoedaSistemaID = selMoedaID.getAttribute('MoedaID', 1);
    var nMoedaID = selMoedaID.value;
    var retVal = '0';

    if (nMoedaSistemaID == nMoedaID) {
        for (i = 0; i < selEmpresa.options.length; i++) {
            if (selEmpresa.options.item(i).selected) {
                nMoedaID = selEmpresa.options.item(i).getAttribute('MoedaID', 1);

                if (nMoedaID != nMoedaSistemaID) {
                    retVal = '1';
                    break;
                }
            }
        }
    }

    return retVal;
}

function imprimirGrid() {
    fg.ColWidth(getColIndexByColKey(fg, 'ValorOcorrencia')) = 10 * 145;
    fg.ColWidth(getColIndexByColKey(fg, 'ValorFaturamento')) = 10 * 145;
    fg.ColWidth(getColIndexByColKey(fg, 'ValorContribuicao')) = 10 * 145;
    var gridLine = fg.gridLines;
    fg.gridLines = 2;

    fg.PrintGrid('Ocorr�ncias', false, 1, 0, 450);

    fg.gridLines = gridLine;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 1;
}