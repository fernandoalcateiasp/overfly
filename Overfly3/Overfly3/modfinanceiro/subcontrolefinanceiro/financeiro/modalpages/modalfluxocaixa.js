/********************************************************************
modalfluxocaixa.js

Library javascript para o modalfluxocaixa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_aMoedas = new Array();
var glb_nLastLineToExpand = -1;
var dsoGrid = new CDatatransport('dsoGrid');
var dsoDetalhamento = new CDatatransport('dsoDetalhamento');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
getCurrEmpresaData()
fillGridData()
fillGridData_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalfluxocaixa.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

js_fg_DblClick()

FINAL DE DEFINIDAS NO ARQUIVO modalfluxocaixa.ASP
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    
	fillGlbArrays();
	
	fillCmbMoeda();

    // ajusta o body do html
    with (modalfluxocaixaBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
    
    with (btnCanc.style)
    {
		left = (parseInt(divFG.style.width,10) / 2) - (parseInt(btnCanc.style.width,10) / 2);
		visibility = 'hidden';
	}	
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Fluxo de caixa', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
    
	// ajusta os controles do divControles
	
	elem = lblEmpresas;
	with (elem.style)
	{
		left = 0;
		top = 0;
		width = FONT_WIDTH * 18;
    }
    
    elem = selEmpresas;
    elem1 = lblEmpresas;
    with (elem.style)
    {
		left = 0;
		top = elem1.offsetTop + elem1.offsetHeight;
		width = elem1.offsetWidth;
		height = 38;
    }

    adjustElementsInForm([['lblMoeda','selMoeda',10,1, (lblEmpresas.offsetLeft + lblEmpresas.offsetWidth) + 2, -7],
						  ['lblData','txtData',10,1],
						  ['lblEstadoC','chkEstadoC',3,1],
						  ['btnFillGrid','btn',btnOK.offsetWidth,1, 10]],
					      null, null, true);

	btnFillGrid.style.height = btnOK.offsetHeight;
	selEmpresas.onchange = selEmpresas_onchange;
	selMoeda.onchange  = selMoeda_onchange;

	txtData.maxLength = 10;
	txtData.onfocus = selFieldContent;
	txtData.onkeypress = txtData_onKeyPress;
	txtData.value = glb_dCurrDate;
	txtData.readOnly = true;
	
	// ajusta o divControles
	with (divControles.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(selEmpresas.currentStyle.top, 10) + parseInt(selEmpresas.currentStyle.height, 10);
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControles.currentStyle.top, 10) +
			  parseInt(divControles.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = 300;
        height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) -
				 parseInt(top, 10);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // reajusta dimensoes e reposiciona a janela
    (getFrameInHtmlTop( getExtFrameID(window))).style.top = 
		(getFrameInHtmlTop( 'frameSup01')).offsetTop;
		
	redimAndReposicionModalWin(modWidth, modHeight, false);
    
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    
    with (fg)
    {
        FontName = 'Tahoma, Arial, MS SansSerif';
        FontSize = '10';
   		Editable = false;
		AllowUserResizing = 1;
		VirtualData = true;
		Ellipsis = 1;
		SelectionMode = 1;
		AllowSelection = false;
		Rows = 1;
		Cols = 2;
		FixedRows = 1;
		FixedCols = 1;
		ScrollBars = 3;
		OutLineBar = 5;
		// TreeColor = 0X000000;
		GridLines = 0;
		FormatString = 'Descri��o' + '\t' + 'Quant' + '\t' + 'Valor' + '\t' + 
			'Saldo' + '\t' + 'TipoFinanceiroID' + '\t' + 'ParceiroID' + '\t' + 'FinanceiroID' +
			'\t' + 'FluxoID' + '\t' + 'Nivel' + '\t' + 'dtFluxoCaixa' + '\t' + 'Cor' + '\t' + 'Expandido';
			
		OutLineBar = 1;
		// cores
		// Coloca botoes na barra
        GridLinesFixed = 13;
		GridLines = 1;
		GridColor = 0X000000;
		
		ColHidden(4) = true;
		ColHidden(5) = true;
		ColHidden(6) = true;
		ColHidden(7) = true;
		ColHidden(8) = true;
		ColHidden(9) = true;
		ColHidden(10) = true;
		ColHidden(11) = true;
    }
    
    fg.Redraw = 2;

	showExtFrame(window, true);
	
	window.focus();
	selEmpresas.focus();
}

/********************************************************************
Enter no campo data
********************************************************************/
function txtData_onKeyPress()
{
	fg.Rows = 1;
    if ( event.keyCode == 13 )
        btn_onclick(btnFillGrid);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		;
    }
    else if (controlID == 'btnCanc')
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }    
    else if (controlID == 'btnFillGrid')
    {
		btnFillGrid_onclick();
    }
        
}

function btnFillGrid_onclick()
{
	if (!verificaData())
		return null;
	
	fg.Rows = 1;

    fillGridData(0);
}

function verificaData()
{
	var sData = trimStr(txtData.value);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(txtData.value);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        window.focus();
        
        if ( sData != '' )    
			txtData.focus();
		
		return false;
	}
	return true;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData(nNivel)
{
	if (nNivel > 3)
		return null;
		
    glb_dataGridWasChanged = false;
    var i=0;
    var sEmpresas = '/';
    var strPars = new String();
    
    
    for (i=0; i<selEmpresas.length; i++)
    {
		if (selEmpresas.options[i].selected == true )
			sEmpresas += selEmpresas.options[i].value + '/';
    }
    
    if (sEmpresas == '/')
    {
        if ( window.top.overflyGen.Alert('Selecione pelo menos uma empresa.') == 0 )
            return null;
            
		return null;
    }
    
    if (selMoeda.selectedIndex < 0)
    {
        if ( window.top.overflyGen.Alert('Selecione a moeda.') == 0 )
            return null;
            
		return null;
    }
    
	lockControlsInModalWin(true);
	
    strPars =  '?sEmpresas=' + escape(sEmpresas);
    strPars += '&nMoedaID=' + escape(selMoeda.value);
    strPars += '&dt_Data=' + escape(putDateInMMDDYYYY2(trimStr(txtData.value)));
    
    if (nNivel >= 1)
		strPars += '&dt_FluxoCaixa=' + escape(putDateInMMDDYYYY2(fg.TextMatrix(fg.Row, 9)));
    
    if (nNivel >= 2)
		strPars += '&nTipoFinanceiroID=' + escape(fg.TextMatrix(fg.Row, 4));

    if (nNivel >= 3)
		strPars += '&nParceiroID=' + escape(fg.TextMatrix(fg.Row, 5));

	strPars += '&bEstadoC=' + (chkEstadoC.checked ? '1' : '0');

	dsoGrid.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/getfluxodata.aspx' + strPars;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	if (dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)
	{
		lockControlsInModalWin(false);
		return null;
	}
	

    var i, j;
    var nFirstNivel = 0;
	var nRowToInsInGrid = fg.Row + 1;
	var nOutLineLevel = 0;
	
	if (fg.Row > 0)
		nOutLineLevel = fg.RowOutlineLevel(fg.Row) + 1;
		
	if (nRowToInsInGrid < 1)
		nRowToInsInGrid = 1;

    fg.Redraw = 0;
    
    dsoGrid.recordset.MoveFirst();
    while (! dsoGrid.recordset.EOF)
    {
        if (dsoGrid.recordset['Nivel'].value <= 1)
			fg.Row = fg.Rows - 1;

        fg.AddItem('' + String.fromCharCode(9) +
			'' + String.fromCharCode(9) +
			'' + String.fromCharCode(9) +
			'' + String.fromCharCode(9) +
			'' + String.fromCharCode(9) +
			'' + String.fromCharCode(9) +
			'' + String.fromCharCode(9) +
			'' + String.fromCharCode(9) +
			'' + String.fromCharCode(9) +
			/*'' + String.fromCharCode(9) +*/
			'' + String.fromCharCode(9), nRowToInsInGrid);

		if (fg.Row < (fg.Rows - 1))
			fg.Row++;
		
		fg.ColDataType(0) = 12;
		fg.TextMatrix(nRowToInsInGrid, 0) = (dsoGrid.recordset['Descricao'].value == null ? '' : dsoGrid.recordset['Descricao'].value);
			
		fg.ColDataType(1) = 12;
		fg.TextMatrix(nRowToInsInGrid, 1) = (dsoGrid.recordset['Quant'].value == null ? 0 : dsoGrid.recordset['Quant'].value);

		fg.ColDataType(2) = 12;
		fg.TextMatrix(nRowToInsInGrid, 2) = (dsoGrid.recordset['Valor'].value == null ? 0 : dsoGrid.recordset['Valor'].value);

		fg.ColDataType(3) = 12;
		fg.TextMatrix(nRowToInsInGrid, 3) = (dsoGrid.recordset['Saldo'].value == null ? 0 : dsoGrid.recordset['Saldo'].value);

		fg.ColDataType(4) = 12;
		fg.TextMatrix(nRowToInsInGrid, 4) = (dsoGrid.recordset['TipoFinanceiroID'].value == null ? 0 : dsoGrid.recordset['TipoFinanceiroID'].value);

		fg.ColDataType(5) = 12;
		fg.TextMatrix(nRowToInsInGrid, 5) = (dsoGrid.recordset['ParceiroID'].value == null ? 0 : dsoGrid.recordset['ParceiroID'].value);

		fg.ColDataType(6) = 12;
		fg.TextMatrix(nRowToInsInGrid, 6) = (dsoGrid.recordset['FinanceiroID'].value == null ? 0 : dsoGrid.recordset['FinanceiroID'].value);

		fg.ColDataType(7) = 12;
		fg.TextMatrix(nRowToInsInGrid, 7) = (dsoGrid.recordset['FluxoID'].value == null ? 0 : dsoGrid.recordset['FluxoID'].value);

		fg.ColDataType(8) = 12;
		fg.TextMatrix(nRowToInsInGrid, 8) = (dsoGrid.recordset['Nivel'].value == null ? 0 : dsoGrid.recordset['Nivel'].value);
		
		fg.ColDataType(9) = 12;
		fg.TextMatrix(nRowToInsInGrid, 9) = (dsoGrid.recordset['dtFluxoCaixa'].value == null ? '' : dsoGrid.recordset['dtFluxoCaixa'].value);

		fg.ColDataType(10) = 12;
		fg.TextMatrix(nRowToInsInGrid, 10) = (dsoGrid.recordset['Cor'].value == null ? '' : dsoGrid.recordset['Cor'].value);

		fg.ColDataType(11) = 12;
		fg.TextMatrix(nRowToInsInGrid, 11) = 0;

		fg.IsSubTotal(nRowToInsInGrid) = true;
		fg.RowOutlineLevel(nRowToInsInGrid) = nOutLineLevel;
			
		nRowToInsInGrid++;
		
		dsoGrid.recordset.MoveNext();
    }
    
	paintGrid();
            
    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( fg.Rows > 1 )
        fg.Row = 1;
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;
    
    lockControlsInModalWin(false);

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
		if ( glb_nLastLineToExpand > 0 )
		{
			fg.TopRow = glb_nLastLineToExpand;
			fg.Row = glb_nLastLineToExpand;
			glb_nLastLineToExpand = -1;
		}	
		
        window.focus();
        fg.focus();
    }
    else
    {
        ;
    }            
}

function paintGrid()
{
    var dTFormat = '';
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

	// Atualmente 10 colunas
	
    putMasksInGrid(fg, ['','99999','999999999.99','999999999.99','','','','','','',''], 
                       ['','#####','(###,###,##0.00)','(###,###,##0.00)','','','','','','','']);

    for ( i=1; i<fg.Rows; i++ )
    {
		if ( fg.ValueMatrix(i, 2) <0 )
		{
			fg.Select(i, 2, i, 2);
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }

		if ( fg.ValueMatrix(i, 3) <0 )
		{
			fg.Select(i, 3, i, 3);
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }

		if ( fg.TextMatrix(i, 10) == 'True' )
			fg.Cell(6, i, 0, i, 3) = 0X8CE6F0;
    }
}

function js_fg_DblClick()
{
	glb_nLastLineToExpand = -1;

	// Nao executa expansao da linha
	// se a mesma ja esta expandida
	if (fg.ValueMatrix(fg.Row, 11) == 1)
	{
		if ( fg.Row < 1 )
			return true;
			
		var node = fg.GetNode();
    
		try
		{
			node.Expanded = !node.Expanded;
		}	
		catch(e)
		{
			;	
		}	
		
		var nFinanceiroID = fg.TextMatrix(fg.Row, 6);
		
		if ((nFinanceiroID != null)&&(nFinanceiroID != ''))
			sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(glb_aEmpresaData[0], nFinanceiroID));
		
		return true;
	}	
		
	fg.TextMatrix(fg.Row, 11) = 1;
	
	glb_nLastLineToExpand = fg.Row;

	fillGridData(fg.TextMatrix(fg.Row, 8));
}

function fillGlbArrays()
{
	var i=0;
	var nCounter = 0;

	for (i=0; i<glb_aMoedasEmpresas.length; i++)
	{
		if ( ascan(glb_aMoedas, glb_aMoedasEmpresas[i][1], false) < 0 )
		{
			glb_aMoedas[nCounter] = glb_aMoedasEmpresas[i][1];
			nCounter++;
		}
	}
}

function fillCmbMoeda()
{
	selMoeda.disabled = true;
	selEmpresas_onchange();
}

function selMoeda_onchange()
{
	fg.Rows = 1;
}

function selEmpresas_onchange()
{
	var bAddMoeda = true;
	var i=0;
	var j=0;
	var nFind=0;
	var nMoedaSelected = null;
	var nCount = 0;
	
	fg.Rows = 1;
	
	if (selMoeda.selectedIndex >= 0)
		nMoedaSelected = selMoeda.value;
		
	clearComboEx(['selMoeda']);		
	
	if (selEmpresas.selectedIndex < 0)
	{
		selMoeda.disabled = true;
		btnFillGrid.disabled = true;
		return true;
	}
	else
	{
		selMoeda.disabled = false;
		btnFillGrid.disabled = false;
	}
	
	for (i=0; i<glb_aMoedas.length; i++)
	{
		bAddMoeda = true;
		for (j=0; j<selEmpresas.length; j++)
		{
			if (selEmpresas.options[j].selected == true )
			{
				bAddMoeda = (bAddMoeda && (bFindMoedaEmpresa(glb_aMoedas[i],selEmpresas.options[j].value) >= 0));
			}
		}
		
		if (bAddMoeda)
		{
			nFind = bFindMoedaEmpresa(glb_aMoedas[i]);
			var optionStr = glb_aMoedasEmpresas[nFind][2];
			var optionValue = glb_aMoedasEmpresas[nFind][1];
			var oOption = document.createElement("OPTION");
			oOption.text = optionStr;
			oOption.value = optionValue;
			selMoeda.add(oOption);
			
			if (optionValue == nMoedaSelected)
				nMoedaSelected = nCount;
				
			nCount++;
		}
	}
	
	selMoeda.disabled = (selMoeda.options.length == 0);

	if (selMoeda.options.length == 1)
		selMoeda.selectedIndex = 0;
	else if (nMoedaSelected != null)
		selMoeda.selectedIndex = nMoedaSelected;
}

function bFindMoedaEmpresa(nMoedaID, nEmpresaID)
{
	var i=0;
	var nRetVal = -1;
	
	for (i=0; i<glb_aMoedasEmpresas.length; i++)
	{
		if (nEmpresaID == null)
		{
			if (glb_aMoedasEmpresas[i][1] == nMoedaID)
			{
				nRetVal = i;
				break;
			}
		}
		else if ( (glb_aMoedasEmpresas[i][0] == nEmpresaID) && (glb_aMoedasEmpresas[i][1] == nMoedaID) )
		{
			nRetVal = i;
			break;
		}
		
	}
	
	return nRetVal;
}
