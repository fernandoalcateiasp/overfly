/********************************************************************
modalcobrancaduplicatas.js

Library javascript para o modalcobrancaduplicatas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoFiltroParceiros = new CDatatransport("dsoFiltroParceiros");
var dsoListaParceiros = new CDatatransport("dsoListaParceiros");
var dsoListaClientes = new CDatatransport("dsoListaClientes");
var dsoVisualizaObservacoes = new CDatatransport("dsoVisualizaObservacoes");
var dsoGravaObservacoes = new CDatatransport("dsoGravaObservacoes");
var dsoGeraXLS = new CDatatransport("dsoGeraXLS");
var glbRow;
var glbVerifica;
var gblFinanceiroID;
var gblFinanceiros;
var gblEmpresa = getCurrEmpresaData();
var gblUsuarioID = getCurrUserID();

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES

FUNCOES GERAIS:
window_onload()
setupPage()

chkEmpresa_onclick()
btnExcel_onclick()
btnL_onclick()
btnListar_onclick()
btnObservacoes_onclick()
btnCancelar_onclick()
btnGravar_onclick()
btn_onclick(ctl)

cancelaObservacao()
gavaObservacao()
GravaObservacoes_DSC()
preencheFiltroParceiros()
FiltroParceiros_DSC()
preencheGridParceiros()
ListaParceiros_DSC()
preencheGridClientes()
ListaClientes_DSC()
VisualizarObservacoes()
dsoVisualizaObservacoes_DSC()

Eventos de grid particulares desta janela:
fg_modalcobrancaduplicatasDblClick()
fg2_modalcobrancaduplicatasDblClick()


********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html

    with (modalcobrancaduplicatasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 61;
    //modalFrame.style.left = 0;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no grid
    fg.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Cobran�a de duplicatas ', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    
    
    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }

    adjustElementsInForm([['lblEmpresa', 'chkEmpresa', 3, 1, -10, 10],
                          ['lblDataInicial', 'txtDataInicial', 10, 1, -7],
                          ['lblDataFinal', 'txtDataFinal', 10, 1, 0],
                          ['lblNome', 'txtNome', 25, 1, 0, 0],
                          ['lblParceiro', 'selParceiro', 28, 1, 17, 0]], null, null, true);
                          
    // Posiciona divFG
    with (divFG.style)
    {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = (ELEM_GAP * 8);
        width = modWidth - 25;    
        height = 215;
    }
    
    // Posiciona divFG2
    with (divFG2.style) {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = (ELEM_GAP * 25) + 55;
        width = modWidth - 25;
        height = 215;
    }

    // Posiciona Grid fg
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // Posiciona Grid fg2
    with (fg2.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10);
        height = parseInt(divFG2.style.height, 10);
    }


    // Posiciona botao L
    with (btnL) {
        style.top = txtDataInicial.offsetTop;
        style.width = 18;
        style.heigth = 18;
        style.left = 407;
    }
    
    // Posiciona botao Listar
    with (btnListar) {
        style.top = txtDataInicial.offsetTop;
        style.width = 75;
        style.left = modWidth - ELEM_GAP - 297;
    }

    // Posiciona botao Excel
    with (btnExcel) {
        style.top = txtDataInicial.offsetTop;
        style.width = 75;
        style.left = modWidth - ELEM_GAP - 215;
    }
    
    // Posiciona botao Observacoes
    with (btnObservacoes) {
        style.top = divControls.offsetTop + 33;
        style.width = 117;
        style.left = modWidth - ELEM_GAP - 122;
    }

    //Posiciona Botao Gravar
    with (btnGravar) {
        style.top = -258;
        style.width = 55;
        style.left = modWidth - ELEM_GAP - 133;
    }

    //Posiciona Botao Cancelar
    with (btnCancelar) {
        style.top = -258;
        style.width = 55;
        style.left = modWidth - ELEM_GAP - 71;
    }
    // Posiciona DIV Obs
    with (divObs) {
        style.border = 'solid';
        style.borderWidth = 1;
        style.backgroundColor = 'transparent';
        style.left = ELEM_GAP;
        style.top = (ELEM_GAP * 25) + 55;
        style.width = modWidth - 25;
        style.height = 150;
        style.visibility = 'hidden';
    }

    // Posiciona txtObs
    with (txtObs) {
        style.top = 20;
        style.width = 970;
        style.height = 196;
    }

    //Define Grid fg
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.style.width, 10) * 18;
    fg.Redraw = 2;
    
    //Define Grid fg2
    startGridInterface(fg2);
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.style.width, 10) * 18;
    fg2.Redraw = 2;
    
    //Define estado dos botoes
    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';
    selParceiro.disabled = true;
    btnObservacoes.disabled = true;

    //Aplica acoes aos botoes
    btnListar.onclick = btnListar_onclick;
    btnL.onclick = btnL_onclick;
    btnExcel.onclick = btnExcel_onclick;
    btnObservacoes.onclick = btnObservacoes_onclick;
    btnCancelar.onclick = btnCancelar_onclick;
    btnGravar.onclick = btnGravar_onclick;
    chkEmpresa.onclick = chkEmpresa_onclick;
    
}

/********************************************************************
Acoes dos botoes
********************************************************************/
function btnExcel_onclick() {
    lockControlsInModalWin(true);
    fg2.rows = 0;
    cancelaObservacao();
    sendToExcel();
}
function chkEmpresa_onclick() {
    fg.rows = 0;
    fg2.rows = 0;
    cancelaObservacao();
}

function btnL_onclick() {
    preencheFiltroParceiros();
}

function btnListar_onclick() {

    
    fg2.rows = 0;
    cancelaObservacao();
    preencheGridParceiros();
}

function btnObservacoes_onclick() {

    var i = 0;
    var y = 0;
    while (i < fg2.rows) {
        if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'OK')) != 0) {
            y = y + 1;
        }

        i++;
    }

    if (y > 0) {
        divFG2.style.visibility = 'hidden';
        divObs.style.visibility = 'inherit';
        btnObservacoes.style.visibility = 'hidden';
        txtObs.value = '';
    }
    else {
        window.top.overflyGen.Alert('N�o existe item selecionado');
    }
}

function btnCancelar_onclick() {
    cancelaObservacao();
}

function btnGravar_onclick() {

    if ((txtObs.value != null) && (txtObs.value != '')) {
        lockControlsInModalWin(true);
        var i = 0;

        gblFinanceiros = null;

        while (i < fg2.rows) {
            if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'OK')) != 0) {
                if ((gblFinanceiros == null) || (gblFinanceiros == undefined))
                    gblFinanceiros = '/' + fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'Financeiro*')) + '/';
                else
                    gblFinanceiros += fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'Financeiro*')) + '/';
            }

            i++;
        }

        if (gblFinanceiros != null) {
            gavaObservacao();
        }
    }
    else {
        window.top.overflyGen.Alert('Preencha o campo Observa��es!');
    }

}

/********************************************************************
Funcoes particulares desta modal
********************************************************************/
function dateFormatToSearch(sDate) {
    // parece que e data, se nao for devolve como esta
    if (chkDataEx(sDate) != true)
        return sDate;

    // e data, se o sistema esta no formato MMDDYYYY devolve
    if (DATE_FORMAT == "MM/DD/YYYY")
        return sDate;

    // e data e o sistema esta no formato DDMMYYYY, inverte e devolve
    var rExp;
    var aString;
    var nDay, nMonth, nYear;

    rExp = /\D/g;
    aString = sDate.split(rExp);

    if ((typeof (aString)).toUpperCase() != 'OBJECT')
        return sDate;

    if (aString.length != 3)
        return sDate;

    nDay = parseInt(aString[0], 10);
    nMonth = parseInt(aString[1], 10);
    nYear = parseInt(aString[2], 10);

    return (nMonth.toString() + '/' + nDay.toString() + '/' + nYear.toString());
}

function cancelaObservacao() {

    txtObs.vaule = '';
    divFG2.style.visibility = 'inherit';
    divObs.style.visibility = 'hidden';
    btnObservacoes.style.visibility = 'inherit';
    btnGravar.style.visibility = 'inherit';

}

function gavaObservacao() {

    var nTipo = 4;
    var sParceiroID = '';
    var sParceiroNome = '';
    var sDataInicial = '';
    var sDataFinal = '';
    var sFinanceiros = gblFinanceiros;
    var sObservacao = txtObs.value;
    var sEmpresa = gblEmpresa[0];
    var nEmpLogada;
    var sUsuario = gblUsuarioID;
    
    if (chkEmpresa.checked == true) {
        nEmpLogada = 1;
    }
    else {
        nEmpLogada = 0;
    }
    
    var strPars = '?nTipo=' + escape(nTipo);
    strPars += '&sParceiroID=' + escape(sParceiroID);
    strPars += '&sParceiroNome=' + escape(sParceiroNome);
    strPars += '&sDataInicial=' + escape(sDataInicial);
    strPars += '&sDataFinal=' + escape(sDataFinal);
    strPars += '&sFinanceiros=' + escape(sFinanceiros);
    strPars += '&sObservacao=' + escape(sObservacao);
    strPars += '&sEmpresa=' + escape(sEmpresa);
    strPars += '&sUsuario=' + escape(sUsuario);
    
    setConnection(dsoGravaObservacoes);
    dsoGravaObservacoes.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/cobrancaduplicatas.aspx' + strPars;
    dsoGravaObservacoes.ondatasetcomplete = GravaObservacoes_DSC;
    dsoGravaObservacoes.Refresh();

}

function GravaObservacoes_DSC() 
{
    lockControlsInModalWin(false);
    window.top.overflyGen.Alert(dsoGravaObservacoes.recordset['Mensagem'].value);
    fg2.rows = 0;
    preencheGridClientes();
    cancelaObservacao();
    
}

function preencheFiltroParceiros() {

    var nTipo = 1;
    var sParceiroID = selParceiro.value;
    var sParceiroNome = txtNome.value;
    var sDataInicial = '';
    var sDataFinal = '';
    var sFinanceiros = '';
    var sObservacao = '';
    var sEmpresa = gblEmpresa[0];
    var nEmpLogada = '';
    var sUsuario = gblUsuarioID;
    
    lockControlsInModalWin(true);

    if (chkEmpresa.checked == true) {
        nEmpLogada = 1;
    }
    else {
        nEmpLogada = 0;
    }

    var strPars = '?nTipo=' + escape(nTipo);
    strPars += '&sParceiroID=' + escape(sParceiroID);
    strPars += '&sParceiroNome=' + escape(sParceiroNome);
    strPars += '&sDataInicial=' + escape(sDataInicial);
    strPars += '&sDataFinal=' + escape(sDataFinal);
    strPars += '&sFinanceiros=' + escape(sFinanceiros);
    strPars += '&sObservacao=' + escape(sObservacao);
    strPars += '&sEmpresa=' + escape(sEmpresa);
    strPars += '&nEmpLogada=' + escape(nEmpLogada);
    strPars += '&sUsuario=' + escape(sUsuario);
    
    setConnection(dsoFiltroParceiros);
    dsoFiltroParceiros.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/cobrancaduplicatas.aspx' + strPars;
    dsoFiltroParceiros.ondatasetcomplete = FiltroParceiros_DSC;
    dsoFiltroParceiros.Refresh();

}

function FiltroParceiros_DSC() 
{
    clearComboEx(['selParceiro']);

    var oOption = document.createElement("OPTION");
    oOption.text = '';
    oOption.value = '';
    selParceiro.add(oOption);

    dsoFiltroParceiros.recordset.moveFirst();
    
    while (!dsoFiltroParceiros.recordset.EOF) {
        optionStr = dsoFiltroParceiros.recordset['Nome'].value;
        optionValue = dsoFiltroParceiros.recordset['ID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selParceiro.add(oOption);
        dsoFiltroParceiros.recordset.MoveNext();
    }

    lockControlsInModalWin(false);
    selParceiro.disabled = false;
}

function preencheGridParceiros() 
{
    var nTipo = 2;
    var sParceiroID = selParceiro.value;
    var sParceiroNome = '';
    var sDataInicial = txtDataInicial.value;
    var sDataFinal = txtDataFinal.value;
    var sFinanceiros = '';
    var sObservacao = '';
    var sEmpresa = gblEmpresa[0];
    var nEmpLogada = '';
    var sUsuario = gblUsuarioID;
    var sUsuario = gblUsuarioID;
    
    lockControlsInModalWin(true);

    if (chkEmpresa.checked == true) {
        nEmpLogada = 1;
    }
    else {
        nEmpLogada = 0;
    }
        
    if (txtDataInicial.value != '') {
        if (!chkDataEx(txtDataInicial.value)) {
            if (window.top.overflyGen.Alert('Data inicial inv�lida') == 0)
                return null;

            lockControlsInModalWin(false);
            txtDataInicial.focus();
            return false;
        }
    }

    if (txtDataFinal.value != '') {
        if (!chkDataEx(txtDataFinal.value)) {
            if (window.top.overflyGen.Alert('Data final inv�lida') == 0)
                return null;
            lockControlsInModalWin(false);
            txtDataFinal.focus();
            return false;
        }
    }

    var strPars = '?nTipo=' + escape(nTipo);
    strPars += '&sParceiroID=' + escape(sParceiroID);
    strPars += '&sParceiroNome=' + escape(sParceiroNome);
    strPars += '&sDataInicial=' + escape(dateFormatToSearch(sDataInicial));
    strPars += '&sDataFinal=' + escape(dateFormatToSearch(sDataFinal));
    strPars += '&sFinanceiros=' + escape(sFinanceiros);
    strPars += '&sObservacao=' + escape(sObservacao);
    strPars += '&sEmpresa=' + escape(sEmpresa);
    strPars += '&nEmpLogada=' + escape(nEmpLogada);
    strPars += '&sUsuario=' + escape(sUsuario);
    
    setConnection(dsoListaParceiros);
    dsoListaParceiros.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/cobrancaduplicatas.aspx' + strPars;
    dsoListaParceiros.ondatasetcomplete = ListaParceiros_DSC;
    dsoListaParceiros.Refresh();
}

function ListaParceiros_DSC() 
{
    if (!(dsoListaParceiros.recordset.EOF) || (dsoListaParceiros.recordset.BOF)) {
        startGridInterface(fg);
        fg.FontSize = '8';
        fg.FrozenCols = 0;

        headerGrid(fg, ['Nome',
                       'ID',
                       'Financeiros',
                       'Valor',
                       'Contato',
                       'Telefone',
                       'Email'], []);

        fillGridMask(fg, dsoListaParceiros, ['Nome*',
                       'ID*',
                       'QtdFinanceiro*',
                       'Valor*',
                       'Contato*',
                       'Telefone*',
                       'Email*'],
                       ['', '', '', '', '', '', ''],
				       ['', '', '', '###,###,###,###.00', '', '', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1, '###,###,###,###', 'C'],
                                                                [2, '###,###,###,###', 'S'],
                                                                [3, '###,###,###,###.00', 'S']], null);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(0) = true;
        fg.MergeCol(1) = true;
        fg.MergeCol(2) = true;
        fg.MergeCol(3) = true;

        //alignColsInGrid(fg);

        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols -1);

        // Permite ordena��o pelo header do grid
        fg.ExplorerBar = 5;

        fg.Redraw = 2;
        fg.Editable = false;

        lockControlsInModalWin(false);
    }

}

function preencheGridClientes() {

    var nTipo = 3;
    var sParceiroID = glbRow;
    var sParceiroNome = '';
    var sDataInicial = txtDataInicial.value;
    var sDataFinal = txtDataFinal.value;
    var sFinanceiros = '';
    var sObservacao = '';
    var sEmpresa = gblEmpresa[0];
    var nEmpLogada = '';
    var sUsuario = gblUsuarioID;
    
    lockControlsInModalWin(true);

    if (chkEmpresa.checked == true) {
        nEmpLogada = 1;
    }
    else {
        nEmpLogada = 0;
    }
    
    // verifica os campos datas
    if (txtDataInicial.value != '') {
        if (!chkDataEx(txtDataInicial.value)) {
            if (window.top.overflyGen.Alert('Data inicial inv�lida') == 0)
                return null;

            lockControlsInModalWin(false);
            txtDataInicial.focus();
            return false;
        }
    }

    if (txtDataFinal.value != '') {
        if (!chkDataEx(txtDataFinal.value)) {
            if (window.top.overflyGen.Alert('Data final inv�lida') == 0)
                return null;
            lockControlsInModalWin(false);
            txtDataFinal.focus();
            return false;
        }
    }


    var strPars = '?nTipo=' + escape(nTipo);
    strPars += '&sParceiroID=' + escape(sParceiroID);
    strPars += '&sParceiroNome=' + escape(sParceiroNome);
    strPars += '&sDataInicial=' + escape(dateFormatToSearch(sDataInicial));
    strPars += '&sDataFinal=' + escape(dateFormatToSearch(sDataFinal));
    strPars += '&sFinanceiros=' + escape(sFinanceiros);
    strPars += '&sObservacao=' + escape(sObservacao);
    strPars += '&sEmpresa=' + escape(sEmpresa);
    strPars += '&nEmpLogada=' + escape(nEmpLogada);
    strPars += '&sUsuario=' + escape(sUsuario);
    
    setConnection(dsoListaClientes);
    dsoListaClientes.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/cobrancaduplicatas.aspx' + strPars;
    dsoListaClientes.ondatasetcomplete = ListaClientes_DSC;
    dsoListaClientes.Refresh();
}

function ListaClientes_DSC() 
{

    if (!(dsoListaClientes.recordset.EOF) || (dsoListaClientes.recordset.BOF)) 
    {
        startGridInterface(fg2);
        fg2.FontSize = '8';

        headerGrid(fg2, ['Nome',
                       'ID',
                       'Telefone',
                       'Empresa',
                       'Vencimento',
                       'Financeiro',
                       'Pedido',
                       'Duplicata',
                       'Valor',
                       'Saldo Devedor',
                       'Banco',
                       'Devolucao',
                       'Vendedor',
                       'Cobrado',
                       'OK',
                       'Vencida',
                       'EmpresaID'], [11, 15, 16]);

        fillGridMask(fg2, dsoListaClientes, ['Nome*',
                       'ID*',
                       'Telefone*',
                       'Empresa*',
                       'Vencimento*',
                       'Financeiro*',
                       'Pedido*',
                       'Duplicata*',
                       'Valor*',
                       'SaldoDevedor*',
                       'Banco*',
                       'Devolucao*',
                       'Vendedor*',
                       'Cobrado*',
                       'OK',
                       'Vencida*',
                       'EmpresaID*'],
                       ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
				       ['', '', '', '', '', '', '', '###,###,###,###.00', '###,###,###,###.00', '', '', '', '', '', '', '']);

        gridHasTotalLine(fg2, 'Totais', 0xC0C0C0, null, true, [[5, '######', 'C'],
                                                                [8, '###,###,###,###.00', 'S']], null);
        //Congela Coluna
        fg2.FrozenCols = 2;

        // Merge de Colunas
        fg2.MergeCells = 4;
        fg2.MergeCol(0) = true;
        fg2.MergeCol(1) = true;
        fg2.MergeCol(2) = true;
        fg2.MergeCol(3) = true;
        fg2.MergeCol(4) = true;

        //alignColsInGrid(fg2);


        fg2.ColDataType(getColIndexByColKey(fg2, 'OK')) = 11; // format boolean (checkbox)

        fg2.AutoSizeMode = 0;
        fg2.AutoSize(0, fg2.Cols - 1);

        // Permite ordena��o pelo header do grid
        fg2.ExplorerBar = 5;

        fg2.Editable = true;
        fg2.Redraw = 2;

        if (fg2.Rows > 1)
            pintaGrid();
    }

    lockControlsInModalWin(false);
    if (fg2.rows > 1) {
        btnObservacoes.disabled = false;
    }
}

function VisualizarObservacoes() 
{
    setConnection(dsoVisualizaObservacoes);
    dsoVisualizaObservacoes.SQL = 'SELECT Observacoes FROM Financeiro WHERE FinanceiroID = ' + gblFinanceiroID;
    dsoVisualizaObservacoes.ondatasetcomplete = dsoVisualizaObservacoes_DSC;
    dsoVisualizaObservacoes.Refresh();
}
 
function dsoVisualizaObservacoes_DSC() 
{

    if (dsoVisualizaObservacoes.recordset['Observacoes'].value != null) 
    {
        txtObs.value = dsoVisualizaObservacoes.recordset['Observacoes'].value;
    }
    else 
    {
        txtObs.value = '';
    }
    
    divFG2.style.visibility = 'hidden';
    divObs.style.visibility = 'inherit';
    btnGravar.style.visibility = 'hidden';
    btnObservacoes.style.visibility = 'hidden';
    divFG.enabled = false;
}

/********************************************************************
Funcoes dos grids fg e fg2
********************************************************************/

function fg_modalcobrancaduplicatasKeyPress(KeyAscii) {

    glbRow = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ID*'));
    preencheGridClientes();
    cancelaObservacao();

}

function fg_modalcobrancaduplicatasDblClick() {

    glbRow = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ID*'));
    preencheGridClientes();
    cancelaObservacao();

}

function js_modalitensKeyPress(KeyAscii)
{
    gblFinanceiroID = fg2.TextMatrix(fg2.Row, getColIndexByColKey(fg2, 'Financeiro*'));
    VisualizarObservacoes();
}

function fg2_modalcobrancaduplicatasDblClick()
{
    if (fg2.Row > 1)
    {
        var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

        sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(getCellValueByColKey(fg2, 'EmpresaID*', fg2.Row), getCellValueByColKey(fg2, 'Financeiro*', fg2.Row)));
    }
}

function js_modalcobrancaduplicatas_AfterEdit(Row, Col) {

    if (Col == getColIndexByColKey(fg2, 'Cobrado*'))
    {
        if (fg2.ValueMatrix(Row, getColIndexByColKey(fg2, 'Cobrado*')) == 0)
            fg2.TextMatrix(Row, getColIndexByColKey(fg2, 'Cobrado*')) = -1;
        else
            fg2.TextMatrix(Row, getColIndexByColKey(fg2, 'Cobrado*')) = 0;
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('OK_CALLFORM_PL'), null );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_PL'), null );
    }
}

/********************************************************************
Funcoes para gerar Excel
********************************************************************/
function sendToExcel() {
    var nLinguaLogada = getDicCurrLang();
    var nTipo = 5;
    var sParceiroID = ((selParceiro.value != '') ? selParceiro.value : 'NULL');
    var sParceiroNome = '';
    var sDataInicial = ((txtDataInicial.value != '') ? '\'' + dateFormatToSearch(txtDataInicial.value) + '\'' : 'NULL');
    var sDataFinal = ((txtDataFinal.value != '') ? '\'' + dateFormatToSearch(txtDataFinal.value) + '\'' : 'NULL');
    var sFinanceiros = '';
    var sObservacao = '';
    var nEmpLogada = '';
    var sUsuario = gblUsuarioID;

    if (chkEmpresa.checked == true) {
        nEmpLogada = 1;
    }
    else {
        nEmpLogada = 0;
    }

    if (txtDataInicial.value != '') {
        if (!chkDataEx(txtDataInicial.value)) {
            if (window.top.overflyGen.Alert('Data inicial inv�lida') == 0)
                return null;

            lockControlsInModalWin(false);
            txtDataInicial.focus();
            return false;
        }
    }

    if (txtDataFinal.value != '') {
        if (!chkDataEx(txtDataFinal.value)) {
            if (window.top.overflyGen.Alert('Data final inv�lida') == 0)
                return null;
            lockControlsInModalWin(false);
            txtDataFinal.focus();
            return false;
        }
    }

    strPars = '?nEmpresaID=' + gblEmpresa[0];
    strPars += '&controle=' + 'Excel';
    strPars += '&sEmpresaFantasia=' + gblEmpresa[3];
    strPars += '&nLinguaLogada=' + nLinguaLogada;
    strPars += '&nTipo=' + nTipo;
    strPars += '&sParceiroID=' + sParceiroID;
    strPars += '&sDataInicial=' + sDataInicial;
    strPars += '&sDataFinal=' + sDataFinal;
    strPars += '&nEmpLogada=' + nEmpLogada;
    strPars += '&sUsuario=' + sUsuario;

    lockControlsInModalWin(true);
    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/ReportsGrid_CobrancaDuplicatas.aspx' + strPars;
}

function pintaGrid() {
    var bgColorRed = 0X0000FF;

    lockInterface(true);
    for (i = 2; i < fg2.Rows; i++) {
        if (fg2.TextMatrix(i, getColIndexByColKey(fg2, 'Vencida*')) == 1)
        {
            //Pinta c�lula
            fg2.Cell(7, i, getColIndexByColKey(fg2, 'Vencimento*'), i, getColIndexByColKey(fg2, 'Vencimento*')) = bgColorRed;
            fg2.Cell(7, i, getColIndexByColKey(fg2, 'Financeiro*'), i, getColIndexByColKey(fg2, 'Financeiro*')) = bgColorRed;
            fg2.Cell(7, i, getColIndexByColKey(fg2, 'Pedido*'), i, getColIndexByColKey(fg2, 'Pedido*')) = bgColorRed;
            fg2.Cell(7, i, getColIndexByColKey(fg2, 'Duplicata*'), i, getColIndexByColKey(fg2, 'Duplicata*')) = bgColorRed;
            fg2.Cell(7, i, getColIndexByColKey(fg2, 'Valor*'), i, getColIndexByColKey(fg2, 'Valor*')) = bgColorRed;
            fg2.Cell(7, i, getColIndexByColKey(fg2, 'SaldoDevedor*'), i, getColIndexByColKey(fg2, 'SaldoDevedor*')) = bgColorRed;
            fg2.Cell(7, i, getColIndexByColKey(fg2, 'Banco*'), i, getColIndexByColKey(fg2, 'Banco*')) = bgColorRed;
            fg2.Cell(7, i, getColIndexByColKey(fg2, 'Devolucao*'), i, getColIndexByColKey(fg2, 'Devolucao*')) = bgColorRed;
            fg2.Cell(7, i, getColIndexByColKey(fg2, 'Vendedor*'), i, getColIndexByColKey(fg2, 'Vendedor*')) = bgColorRed;


        }
    }
    lockInterface(false);
}
function reports_onreadystatechange() {

    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
        (frameReport.contentWindow.document.readyState == 'interactive') ||
        (frameReport.contentWindow.document.readyState == 'complete')) {

        lockControlsInModalWin(false);
    }
}