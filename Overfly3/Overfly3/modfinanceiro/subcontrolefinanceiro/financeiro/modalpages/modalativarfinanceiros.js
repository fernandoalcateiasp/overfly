/********************************************************************
modalativarfinanceiros.js

Library javascript para o modalativarfinanceiros.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_sResultado = '';
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');
var dsoCombo = new CDatatransport('dsoCombo');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalativarfinanceirosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalativarfinanceirosDblClick(grid, Row, Col)
js_modalativarfinanceirosKeyPress(KeyAscii)
js_modalativarfinanceiros_ValidateEdit()
js_modalativarfinanceiros_BeforeEdit(grid, row, col)
js_modalativarfinanceiros_AfterEdit(Row, Col)
js_fg_AfterRowColmodalativarfinanceiros (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalativarfinanceirosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	glb_bFirstFill = true;

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText(translateTerm('Ativar Financeiros', null), 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK
    btnOK.disabled = true;
	btnFillGrid.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lbldtInicio','txtdtInicio',10,1,-10,-10],
						  ['lbldtFim','txtdtFim',10,1],
						  ['lblSaldoMaximo','txtSaldoMaximo',11,1],
						  ['lblFormaPagamentoID','selFormaPagamentoID',7,1],
						  ['lblEstadoDeID','selEstadoDeID',7,1],
						  ['lblSituacaoCobrancaDeID','selSituacaoCobrancaDeID',12,1],
						  ['lblEstadoParaID','selEstadoParaID',7,1],
						  ['lblSituacaoCobrancaParaID','selSituacaoCobrancaParaID',12,1],
						  ['lblFiltro','txtFiltro',75,2,-10]], null, null, true);

	txtdtInicio.onfocus = selFieldContent;
	txtdtInicio.maxLength = 10;
	txtdtInicio.onkeypress = txtFields_onKeyPress;
	txtdtInicio.value = glb_dCurrDate;

	txtdtFim.onfocus = selFieldContent;
	txtdtFim.maxLength = 10;
	txtdtFim.onkeypress = txtFields_onKeyPress;
	txtdtFim.value = glb_dCurrDate;

	txtSaldoMaximo.onkeypress = verifyNumericEnterNotLinked;
    txtSaldoMaximo.setAttribute('thePrecision', 11, 1);
    txtSaldoMaximo.setAttribute('theScale', 2, 1);
    txtSaldoMaximo.setAttribute('minMax', new Array(0, 999999999.99));
    txtSaldoMaximo.setAttribute('verifyNumPaste', 1);

	selFormaPagamentoID.onchange = selFormaPagamentoID_onchange;
	selEstadoDeID.onchange = selEstadoDeID_onchange;
	selSituacaoCobrancaDeID.onchange = selSituacaoCobrancaDeID_onchange;
	selEstadoParaID.onchange = selEstadoParaID_onchange;
	selSituacaoCobrancaParaID.onchange = selSituacaoCobrancaParaID_onchange;
	
	adjustLabelsCombos();

	txtFiltro.onfocus = selFieldContent;
	txtFiltro.maxLength = 1000;
	txtFiltro.onkeypress = txtFields_onKeyPress;
	
    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
			  btnFillGrid.offsetTop + btnFillGrid.offsetHeight + 2;
        width = txtFiltro.offsetLeft + txtFiltro.offsetWidth + ELEM_GAP;    
        height = txtFiltro.offsetTop + txtFiltro.offsetHeight;    
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + txtFiltro.offsetTop;
		style.width = 63;
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		value = 'Aplicar';
		title = 'Aplicar';
    }
    
    with (btnImprimir)
    {         
		style.top = divControls.offsetTop + txtFiltro.offsetTop;
		style.width = 63;
		style.left = btnOK.offsetLeft - ELEM_GAP - btnImprimir.offsetWidth;
    }
    
    with (btnFillGrid)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		style.top = divControls.offsetTop + 10;
		style.left = modWidth - ELEM_GAP - parseInt(btnFillGrid.currentStyle.width, 10) - 4;
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtFields_onKeyPress()
{
    if ( event.keyCode == 13 )
	{
		fg.Rows = 1;
		// ajusta estado dos botoes
		setupBtnsFromGridState();
	}
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnFillGrid')
    {
		listar();
    }
    else if (controlID == 'btnImprimir')
    {
		imprimeGrid();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
	fillComboEstadoParaID();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    
	btnOK.disabled = true;

	if ((bHasRowsInGrid) && ((selEstadoParaID.value > 0) || (selSituacaoCobrancaParaID.value > 0)))
	{
		for (i=2; i<fg.Rows; i++)
		{
			if (getCellValueByColKey(fg, 'OK', i) != 0)
			{
				btnOK.disabled = false;
				break;
			}	
		}
	}
	
	btnFillGrid.disabled = false;
	btnImprimir.disabled = (fg.Rows <= 2);
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }
    
    if (glb_sResultado != '')
    {
		if ( window.top.overflyGen.Alert(glb_sResultado) == 0 )
			return null;
			
		glb_sResultado = '';
    }
    
	var aGrid = null;
	var i = 0;
	var sFiltro = trimStr(txtFiltro.value);
	var sDataInicio = trimStr(txtdtInicio.value);
	var sDataFim = trimStr(txtdtFim.value);
	var sFiltroDataInicio = '';
	var sFiltroDataFim = '';
	var nSaldoMaximo = trimStr(txtSaldoMaximo.value);
	var sFiltroSaldoMaximo = '';
	var sFiltroFormaPagamento = '';
	var sFiltroEstadoDe = '';
	var sFiltroSituacaoCobranca = '';
    var nTop = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');
	
	sFiltro = (sFiltro == '' ? '' : ' AND ' + sFiltro);

	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;
    
    // zera o grid
    fg.Rows = 1;
    
	if (sDataInicio != '')
	{
		sDataInicio = normalizeDate_DateTime(sDataInicio, 1);

		sFiltroDataInicio = ' AND a.dtVencimento >= ' + '\'' + sDataInicio + '\'' + ' '; 
	}
    else
		sFiltroDataInicio = '';

	if (sDataFim != '')
	{
		sDataFim = normalizeDate_DateTime(sDataFim, 1);

		sFiltroDataFim = ' AND a.dtVencimento <= ' + '\'' + sDataFim + '\'' + ' '; 
	}
    else
		sFiltroDataFim = '';

	if (nSaldoMaximo != '')
	{
		sFiltroSaldoMaximo = ' AND dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) <= ' + nSaldoMaximo + ' '; 
	}
    else
		sFiltroSaldoMaximo = '';

	if (selFormaPagamentoID.value > 0)
		sFiltroFormaPagamento = ' AND a.FormaPagamentoID = ' + selFormaPagamentoID.value + ' ';
	
	if (selEstadoDeID.value > 0)
	{
		sFiltroEstadoDe = ' AND a.EstadoID = ' + selEstadoDeID.value + ' ';

		if (selEstadoDeID.value == 1)
			sFiltroEstadoDe += ' AND a.PedidoID IS NULL ';
	}

	if (selSituacaoCobrancaDeID.value > 0)
		sFiltroSituacaoCobranca = ' AND a.SituacaoCobrancaID = ' + selSituacaoCobrancaDeID.value + ' ';

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	dsoGrid.SQL = 'SELECT TOP ' + nTop + ' a.FinanceiroID AS FinanceiroID, f.RecursoAbreviado AS Estado, CONVERT(BIT, 0) AS OK, ' +
			'a.PedidoID, a.Duplicata, d.Fantasia AS Pessoa, b.ItemAbreviado AS FormaPagamento, ' +
			'a.dtEmissao AS dtEmissao, a.PrazoPagamento, a.dtVencimento AS dtVencimento, c.SimboloMoeda AS Moeda, ' +
			'a.Valor AS Valor, ' +
			'dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, ' +
			'dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, ' +
			'e.Fantasia AS Colaborador, g.ItemAbreviado AS Sit ' +
		'FROM Financeiro a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Pessoas e WITH(NOLOCK), Recursos f WITH(NOLOCK), TiposAuxiliares_Itens g WITH(NOLOCK) ' +
		'WHERE (a.EmpresaID = ' + glb_aEmpresaData[0] + ' AND a.TipoFinanceiroID = ' + glb_nTipoFinanceiroID + ' AND ' +
			'a.FormaPagamentoID = b.ItemID AND a.MoedaID = c.ConceitoID AND ' +
			'a.PessoaID=d.PessoaID AND a.ProprietarioID = e.PessoaID AND ' +
			'a.EstadoID = f.RecursoID AND a.SituacaoCobrancaID=g.ItemID ' +
			sFiltroDataInicio + sFiltroDataFim + sFiltroSaldoMaximo + sFiltroFormaPagamento + sFiltro + sFiltroEstadoDe + sFiltroSituacaoCobranca + ') ' +
		'ORDER BY a.FinanceiroID';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
    try
    {
		dsoGrid.Refresh();
	}	
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Filtro inv�lido.') == 0 )
            return null;

		lockControlsInModalWin(false);        
        window.focus();
		txtFiltro.focus();		
	}
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	var aHoldCols = new Array();

	aHoldCols = [];

    headerGrid(fg,['Financeiro',
				   'Est',
				   'Sit',
				   'OK',
				   'Pedido',
				   'Duplicata',
				   'Pessoa',
				   'Forma',
				   'Emiss�o',
				   'Prazo',
				   'Vencimento',
				   '$',
				   'Valor',
				   'Saldo Devedor',
				   'Saldo Atual',
				   'Colaborador'], aHoldCols);

    fillGridMask(fg,dsoGrid,['FinanceiroID*',
							 'Estado*',
							 'Sit*',
							 'OK',
							 'PedidoID*',
							 'Duplicata*',
							 'Pessoa*',
							 'FormaPagamento*',
							 'dtEmissao*',
							 'PrazoPagamento*',
							 'dtVencimento*',
							 'Moeda*',
							 'Valor*',
							 'SaldoDevedor*',
							 'SaldoAtualizado*',
						     'Colaborador*'],
							 ['','','','','','','','','','','','','','',''],
							 ['','','','','','','','',dTFormat,'',dTFormat,'','###,###,##0.00','###,###,##0.00','###,###,##0.00','']);
    fg.Redraw = 0;
    alignColsInGrid(fg,[0, 4, 9, 12, 13, 14]);
    
    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'Valor*'),'###,###,###.00','S'],
														  [getColIndexByColKey(fg, 'SaldoDevedor*'),'###,###,###.00','S'],
														  [getColIndexByColKey(fg, 'SaldoAtualizado*'),'###,###,###.00','S']]);

    if (fg.Rows > 1)
    {
		fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor*')) = 0;
		fg.TextMatrix(1, getColIndexByColKey(fg, 'SaldoDevedor*')) = 0;
		fg.TextMatrix(1, getColIndexByColKey(fg, 'SaldoAtualizado*')) = 0;
		fg.TextMatrix(1, getColIndexByColKey(fg, 'FormaPagamento*')) = 0;
	}
	
	fg.FrozenCols = 4;

	paintCellsSpecialyReadOnly();
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;
    fg.MergeCol(5) = true;
    fg.MergeCol(6) = true;
    fg.MergeCol(7) = true;
    fg.MergeCol(8) = true;
    fg.Redraw = 2;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
    
	fg.Redraw = 2;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
		fg.Editable = true;
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;

	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
	glb_sResultado = '';

    lockControlsInModalWin(true);
    
    for (i=2; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'OK', i) != 0)
		{
			nBytesAcum=strPars.length;
			
			if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
			{
				nBytesAcum = 0;
				if (strPars != '')
				{
					glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
					strPars = '';
					nDataLen = 0;
				}
				
				strPars += '?nUserID=' + escape(glb_nUserID);

				if (selEstadoParaID.value > 0)
				{
					strPars += '&nTipo=' + escape(1);
					strPars += '&nEstadoDeID=' + escape(selEstadoDeID.value);
					strPars += '&nEstadoParaID=' + escape(selEstadoParaID.value);
				}
				else
				{
					strPars += '&nTipo=' + escape(2);
					strPars += '&nSituacaoCobrancaParaID=' + escape(selSituacaoCobrancaParaID.value);
				}					
			}
		
			nDataLen++;
			strPars += '&nFinanceiroID=' + escape(getCellValueByColKey(fg, 'FinanceiroID*', i));
		}	
	}
	
	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
		
	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/ativarfinanceiros.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
	}
}

function sendDataToServer_DSC()
{
	if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF))
	{
	    if (dsoGrava.recordset['Mensagem'].value != null)
	        glb_sResultado = glb_sResultado + dsoGrava.recordset['Mensagem'].value;
	}
	
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

function selFormaPagamentoID_onchange()
{
	adjustLabelsCombos();
	fg.Rows = 1;
	// ajusta estado dos botoes
	setupBtnsFromGridState();
}

function selEstadoDeID_onchange()
{
	adjustLabelsCombos();
	fg.Rows = 1;

	fillComboEstadoParaID();		
}

function selSituacaoCobrancaDeID_onchange()
{
	adjustLabelsCombos();
	fg.Rows = 1;

	// ajusta estado dos botoes
	setupBtnsFromGridState();
}

function selEstadoParaID_onchange()
{
	selSituacaoCobrancaParaID.selectedIndex = 0;
	adjustLabelsCombos();

	// ajusta estado dos botoes
	setupBtnsFromGridState();
}

function selSituacaoCobrancaParaID_onchange()
{
	selEstadoParaID.selectedIndex = 0;
	adjustLabelsCombos();

	// ajusta estado dos botoes
	setupBtnsFromGridState();
}

function adjustLabelsCombos()
{
    setLabelOfControl(lblFormaPagamentoID, selFormaPagamentoID.value);
    setLabelOfControl(lblEstadoDeID, selEstadoDeID.value);
    setLabelOfControl(lblSituacaoCobrancaDeID, selSituacaoCobrancaDeID.value);
    setLabelOfControl(lblEstadoParaID, selEstadoParaID.value);
    setLabelOfControl(lblSituacaoCobrancaParaID, selSituacaoCobrancaParaID.value);
}
function listar()
{
	if (!verificaData())
		return null;
		
	fillGridData();
}

function verificaData()
{
	var sDataInicio = trimStr(txtdtInicio.value);
	var sDataFim = trimStr(txtdtFim.value);

	var bDataIsValid = true;

	if (sDataInicio != '')
		bDataIsValid = chkDataEx(sDataInicio);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data in�cio inv�lida.') == 0 )
            return null;
        
        window.focus();
        
        if ( sDataInicio != '' )    
			txtdtInicio.focus();
		
		return false;
	}

	bDataIsValid = true;

	if (sDataFim != '')
		bDataIsValid = chkDataEx(sDataFim);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data fim inv�lida.') == 0 )
            return null;
        
        window.focus();
        
        if ( sDataFim != '' )    
			txtdtFim.focus();
		
		return false;
	}

	return true;
}

function fillComboEstadoParaID()
{
	var aItemSelected, nContextoID;

	aItemSelected = getCmbCurrDataInControlBar('sup', 1);
	nContextoID = aItemSelected[1];

    lockControlsInModalWin(true);

    // parametrizacao do dso dsoCombo
    setConnection(dsoCombo);

	dsoCombo.SQL = 'SELECT DISTINCT 1 AS Indice, 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem ' +
	'UNION ALL ' +
	'SELECT DISTINCT 2 AS Indice, i.RecursoID AS fldID, i.RecursoAbreviado AS fldName, e.Ordem AS Ordem ' +
         'FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), RelacoesRecursos_Estados d WITH(NOLOCK), ' +
			'RelacoesRecursos e WITH(NOLOCK), RelacoesPesRec f WITH(NOLOCK), RelacoesPesRec_Perfis g WITH(NOLOCK), Recursos_Direitos h WITH(NOLOCK), Recursos i WITH(NOLOCK), Recursos j WITH(NOLOCK) ' +
         'WHERE (a.EstadoID = 2 AND a.TipoRelacaoID = 1 AND a.ObjetoID = ' + nContextoID + ' AND ' +
         	'a.SujeitoID = b.RecursoID AND b.Principal = 1 AND a.MaquinaEstadoID = c.ObjetoID AND ' +
			'c.EstadoID = 2 AND c.TipoRelacaoID = 3 AND c.SujeitoID = ' + selEstadoDeID.value + ' AND c.RelacaoID = d.RelacaoID AND ' + 
			'ISNULL(d.UsoSistema, 0) = 0 AND e.SujeitoID = d.RecursoID AND e.ObjetoID = a.MaquinaEstadoID AND ' +
			'e.TipoRelacaoID = 3 AND e.EstadoID = 2 AND f.SujeitoID IN ((SELECT ' + glb_nUserID + ' UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = ' + glb_nUserID + ' AND GETDATE() BETWEEN dtInicio AND dtFim))) AND ' +
         	'f.ObjetoID = 999 AND f.TipoRelacaoID = 11 AND f.EstadoID = 2 AND f.RelacaoID = g.RelacaoID AND ' +
         	'g.EmpresaID = ' + glb_aEmpresaData[0] + ' AND h.RecursoID = ' + selEstadoDeID.value + ' AND h.EstadoParaID = d.RecursoID AND ' + 
			'h.RecursoMaeID = b.RecursoID AND h.ContextoID = ' + nContextoID + ' AND h.PerfilID = g.PerfilID AND ' +
			'(h.Alterar1 = 1 AND h.Alterar2 = 1) AND ' +
			'h.EstadoParaID = i.RecursoID AND g.PerfilID = j.RecursoID AND j.EstadoID = 2) ' +
         'ORDER BY Indice, Ordem';

    dsoCombo.ondatasetcomplete = fillComboEstadoParaID_DSC;
	dsoCombo.Refresh();
}

function fillComboEstadoParaID_DSC()
{
    var optionStr, optionValue;
    var oOption;
    var i;
	
	clearComboEx(['selEstadoParaID']);

    while (! dsoCombo.recordset.EOF )
    {
        optionStr = dsoCombo.recordset['fldName'].value;
        optionValue = dsoCombo.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        
        if ((glb_bFirstFill) && (dsoCombo.recordset['fldID'].value == 41))
			oOption.selected = true;
		
        selEstadoParaID.add(oOption);
        dsoCombo.recordset.MoveNext();
    }
    
    lockControlsInModalWin(false);

    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    adjustLabelsCombos();
    
	if (glb_bFirstFill)
	{
		glb_bFirstFill = false;
		showExtFrame(window, true);
		
		window.focus();
    
		if ( !txtdtInicio.disabled )
			txtdtInicio.focus();
	}
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);

	glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalativarfinanceirosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalativarfinanceirosDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    // trap so colunas editaveis
    if ( getColIndexByColKey(grid, 'OK') != Col)
        return true;
        
    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);
    
    glb_PassedOneInDblClick = true;
    
    // limpa coluna se tem um check box checado
    for ( i=1; i<grid.Rows; i++ )
    {
        if ( grid.ValueMatrix(i, Col) != 0 )
        {
            bFill = false;
            break;
        }
    }
    
    for ( i=1; i<grid.Rows; i++ )
    {
        if ( bFill )
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    setTotalColuns();
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalativarfinanceirosKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalativarfinanceiros_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalativarfinanceiros_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalativarfinanceiros_AfterEdit(Row, Col)
{
	setupBtnsFromGridState();
	setTotalColuns();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalativarfinanceiros(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return true;

	setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************

function setTotalColuns()
{
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    var nValorTotal = 0;
    var nSaldoDevedor = 0;
    var nSaldoAtualizado = 0;
    var nCounter = 0;
    
	if (bHasRowsInGrid)
	{
		for (i=2; i<fg.Rows; i++)
		{
			if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) != 0)
			{
				nValorTotal += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor*'));
				nSaldoDevedor += fg.ValueMatrix(i, getColIndexByColKey(fg, 'SaldoDevedor*'));
				nSaldoAtualizado += fg.ValueMatrix(i, getColIndexByColKey(fg, 'SaldoAtualizado*'));
				nCounter++;
			}				
		}
	}

	fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor*')) = nValorTotal;
	fg.TextMatrix(1, getColIndexByColKey(fg, 'SaldoDevedor*')) = nSaldoDevedor;
	fg.TextMatrix(1, getColIndexByColKey(fg, 'SaldoAtualizado*')) = nSaldoAtualizado;
	fg.TextMatrix(1, getColIndexByColKey(fg, 'FormaPagamento*')) = nCounter;
}

function getCurrDate()
{
   var d, s = "";
   d = new Date();
   
   if (DATE_SQL_PARAM == 103)
   {
		s += padL(d.getDate().toString(),2,'0') + "/";
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
   }
   else
   {
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
		s += padL(d.getDate().toString(),2,'0') + "/";
   }
   
   s += d.getYear() + " ";
   s += padL(d.getHours().toString(),2,'0') + ":";
   s += padL(d.getMinutes().toString(),2,'0');

   return(s);
}

function imprimeGrid()
{
	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	var sMsg = aEmpresaData[6] + '     ' + translateTerm('Ativar Financeiros', null) + '     ' + getCurrDate();
	var gridLine = fg.gridLines;
	fg.gridLines = 2;

	//fg.ColWidth(i+6) = 1600;
	
	fg.PrintGrid(sMsg, false, 2, 0, 450);
	fg.gridLines = gridLine;
	fg.AutoSizeMode = 0;
	fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 1;
}
