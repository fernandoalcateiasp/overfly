
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalocorrenciasHtml" name="modalocorrenciasHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalocorrencias.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
     
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalocorrencias.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nModalType, sCurrDateFormat, nEmpresaID, nUsuarioID, nDireitoFinanceiro, nDireitoA1, nDireitoA2

sCaller = ""
nModalType = 0
nEmpresaID = 0
nUsuarioID = 0
nDireitoFinanceiro = 0
nDireitoA1 = 0
nDireitoA2 = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nModalType").Count
    nModalType = Request.QueryString("nModalType")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nUserID").Count
    nUsuarioID = Request.QueryString("nUserID")(i)
Next

For i = 1 To Request.QueryString("nDireitoFinanceiro").Count
    nDireitoFinanceiro = Request.QueryString("nDireitoFinanceiro")(i)
Next

For i = 1 To Request.QueryString("nDireitoA1").Count
    nDireitoA1 = Request.QueryString("nDireitoA1")(i)
Next

For i = 1 To Request.QueryString("nDireitoA2").Count
    nDireitoA2 = Request.QueryString("nDireitoA2")(i)
Next

'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If


If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dFirstDayMonth = '01/" & Month(DateAdd("m", -3, Date))& "/" & Year(DateAdd("m", -3, Date)) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dFirstDayMonth = '" & Month(DateAdd("m", -3, Date)) & "/01/" & Year(DateAdd("m", -3, Date)) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dFirstDayMonth = '" & Year(DateAdd("m", -3, Date)) & "/" & Month(DateAdd("m", -3, Date)) & "/01';" & vbcrlf
Else
    Response.Write "var glb_dFirstDayMonth = '';" & vbcrlf
End If

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nModalType = " & CStr(nModalType) & ";"
Response.Write vbcrlf
Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"
Response.Write vbcrlf
Response.Write "var glb_nUsuarioID = " & CStr(nUsuarioID) & ";"
Response.Write vbcrlf

If (CInt(nDireitoFinanceiro) = 1) Then
	Response.Write "var glb_bDireitoFinanceiro = true;"
Else
	Response.Write "var glb_bDireitoFinanceiro = false;"
End If	

Response.Write vbcrlf

If (CInt(nDireitoA1) = 1) Then
	Response.Write "var glb_bDireitoA1 = true;"
Else
	Response.Write "var glb_bDireitoA1 = false;"
End If	

Response.Write vbcrlf

If (CInt(nDireitoA2) = 1) Then
	Response.Write "var glb_bDireitoA2 = true;"
Else
	Response.Write "var glb_bDireitoA2 = false;"
End If	

Response.Write vbcrlf

strSQL = "SELECT TOP 1 ISNULL(b.TipoPerfilComercialID, 0) AS TipoPerfilComercialID " & _
         "FROM RelacoesPessoas a WITH(NOLOCK) " & _
            "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.CargoID) " & _
         "WHERE	a.TipoRelacaoID = 31 AND a.EstadoID = 2 AND a.SujeitoID = " & nUsuarioID & _
         " ORDER BY a.FuncionarioDireto DESC"
         
Set rsData = Server.CreateObject("ADODB.Recordset")         
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText         

Response.Write "var glb_nTipoPerfilComercialID = 0;"
Response.Write vbcrlf

If (Not ((rsData.BOF) AND (rsData.EOF))) Then
    Response.Write "glb_nTipoPerfilComercialID = " & rsData.Fields("TipoPerfilComercialID").Value & ";"
    Response.Write vbcrlf
End If
rsData.Close

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalocorrencias_BeforeEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalocorrencias_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
 fg_AfterRowColChange();
 fg_modalocorrencias_AfterRowColChange();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 fg_modalocorrencias_BeforeRowColChange(arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
	js_fg_DblClick();
//-->
</SCRIPT>


</head>

<body id="modalocorrenciasBody" name="modalocorrenciasBody" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <!-- Div dos controles -->
    <div id="divControles" name="divControles" class="divGeneral">
		<p id="lblAgruparPorX" name="lblAgruparPorX" class="lblGeneral">Agrupar por&nbsp(n)</p>
        <select id="selAgruparPorX" name="selAgruparPorX" class="fldGeneral"></select>
		<p id="lblOrdemX" name="lblOrdemX" class="lblGeneral">Ordem&nbsp(n)</p>
        <select id="selOrdemX" name="selOrdemX" class="fldGeneral"></select>

		<p id="lblAgruparPor1" name="lblAgruparPor1" class="lblGeneral">Agrupar por&nbsp(1)</p>
        <select id="selAgruparPor1" name="selAgruparPor1" class="fldGeneral"></select>
		<p id="lblOrdem1" name="lblOrdem1" class="lblGeneral">Ordem&nbsp(1)</p>
        <select id="selOrdem1" name="selOrdem1" class="fldGeneral"></select>

		<p id="lblAgruparPor2" name="lblAgruparPor2" class="lblGeneral">Agrupar por&nbsp(2)</p>
        <select id="selAgruparPor2" name="selAgruparPor2" class="fldGeneral"></select>
		<p id="lblOrdem2" name="lblOrdem2" class="lblGeneral">Ordem&nbsp(2)</p>
        <select id="selOrdem2" name="selOrdem2" class="fldGeneral"></select>

		<p id="lblAgruparPor3" name="lblAgruparPor3" class="lblGeneral">Agrupar por&nbsp(3)</p>
        <select id="selAgruparPor3" name="selAgruparPor3" class="fldGeneral"></select>
		<p id="lblOrdem3" name="lblOrdem3" class="lblGeneral">Ordem&nbsp(3)</p>
        <select id="selOrdem3" name="selOrdem3" class="fldGeneral"></select>

		<p id="lblAgruparPor4" name="lblAgruparPor4" class="lblGeneral">Agrupar por&nbsp(4)</p>
        <select id="selAgruparPor4" name="selAgruparPor4" class="fldGeneral"></select>
		<p id="lblOrdem4" name="lblOrdem4" class="lblGeneral">Ordem&nbsp(4)</p>
        <select id="selOrdem4" name="selOrdem4" class="fldGeneral"></select>
        
        <p id="lblPesquisa" name="lblPesquisa" class="lblGeneral" title="Configurar pesquisa?">Conf</p>
        <input type="checkbox" id="chkPesquisa" name="chkPesquisa" class="fldGeneral" title="Configurar pesquisa?"></input>

        <p id="lblRecebido" name="lblRecebido" class="lblGeneral" title="Financeiros recebidos?">Rec</p>
        <input type="checkbox" id="chkRecebido" name="chkRecebido" class="fldGeneral" title="Financeiros recebidos?"></input>

		<p id="lblComissao" name="lblComissao" class="lblGeneral">Comiss�es</p>
        <select id="selComissao" name="selComissao" class="fldGeneral">
			<option value="1">Pagas</option>
			<option value="2">Aprovadas</option>
			<option value="3" SELECTED>Pendentes</option>
			<option value="4">Todas</option>
        </select>

        <p id="lblDataInicio" name="lblDataInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio" name="txtDataInicio" class="fldGeneral" LANGUAGE="javascript" onfocus="return __selFieldContent(this)">
        <p id="lblDataFim" name="lblDataFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim" name="txtDataFim" class="fldGeneral" LANGUAGE="javascript" onfocus="return __selFieldContent(this)">

        <p id="lblVendedor" name="lblVendedor" class="lblGeneral">Vendedores</p>
        <select id="selVendedor" name="selVendedor" class="fldGeneral" MULTIPLE>
<%
Dim rsData, strSQL

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT 1 AS Indice, e.PessoaID AS fldID, e.Fantasia AS fldName " & _
     "FROM RelacoesPesRec a WITH(NOLOCK) " & _
         " INNER JOIN RelacoesPesRec_Perfis b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) " & _
         " INNER JOIN Recursos_Direitos c WITH(NOLOCK) ON (b.PerfilID = c.PerfilID) " & _
         " INNER JOIN Financeiro d WITH(NOLOCK) ON (d.EmpresaID = b.EmpresaID) " & _
         " INNER JOIN Financeiro_Ocorrencias f WITH(NOLOCK) ON (f.FinanceiroID=d.FinanceiroID) AND (f.TipoOcorrenciaID = 1055) " & _
         " INNER JOIN Pessoas e WITH(NOLOCK) ON (d.AlternativoID = e.PessoaID) " & _
    " WHERE (a.SujeitoID = " & CStr(nUsuarioID) & "  AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999   " & _
      " AND c.RecursoID = 40009 AND c.RecursoMaeID = 28000 AND c.ContextoID IN (9111,9112) AND (c.Alterar1 = 1 OR c.Alterar2 = 1) " & _
      " AND f.dtPagamentoComissao IS NULL AND DATEDIFF(dd, GETDATE(), f.dtOcorrencia) <= 730 " & _
      " AND  d.PedidoID IS NOT NULL AND  e.TipoPessoaID = 53 " 
      
If (CInt(nDireitoA2) = 0) Then
	strSQL = strSQL & " AND e.PessoaID = " & CStr(nUsuarioID) & " "
End If

strSQL = strSQL & ") GROUP BY e.PessoaID, e.Fantasia "

strSQL = strSQL & " UNION ALL SELECT 2 AS Indice, e.PessoaID AS fldID, e.Fantasia AS fldName " & _
	  "FROM RelacoesPesRec a WITH(NOLOCK) " & _
         " INNER JOIN RelacoesPesRec_Perfis b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) " & _
         " INNER JOIN Recursos_Direitos c WITH(NOLOCK) ON (b.PerfilID = c.PerfilID) " & _
         " INNER JOIN Financeiro d WITH(NOLOCK) ON (d.EmpresaID = b.EmpresaID) " & _
         " INNER JOIN Financeiro_Ocorrencias f WITH(NOLOCK) ON (f.FinanceiroID=d.FinanceiroID) AND (f.TipoOcorrenciaID = 1055) " & _
         " INNER JOIN Pessoas e WITH(NOLOCK) ON (d.ProprietarioID = e.PessoaID) " & _
    " WHERE (a.SujeitoID = " & CStr(nUsuarioID) & "  AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999   " & _
      " AND c.RecursoID = 40009 AND c.RecursoMaeID = 28000 AND c.ContextoID IN (9111,9112) AND (c.Alterar1 = 1 OR c.Alterar2 = 1) " & _
      " AND f.dtPagamentoComissao IS NULL AND DATEDIFF(dd, GETDATE(), f.dtOcorrencia) <= 365 " & _
	  " AND d.PedidoID IS NOT NULL "

If (CInt(nDireitoA2) = 0) Then
	strSQL = strSQL & " AND e.PessoaID = " & CStr(nUsuarioID) & " "
End If

strSQL = strSQL & ") GROUP BY e.PessoaID, e.Fantasia "

strSQL = strSQL & "ORDER BY Indice, fldName"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
   
	Response.Write " Indice = " & CStr(rsData.Fields("Indice").Value) & " "
    
    If ((CInt(nDireitoA2) = 0) AND (CStr(nUsuarioID) = CStr(rsData.Fields("fldID").Value))) Then
		Response.Write " SELECTED "
    End If
    
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>

        <p id="lblEmpresa" name="lblEmpresa" class="lblGeneral">Empresas</p>
        <select id="selEmpresa" name="selEmpresa" class="fldGeneral" MULTIPLE>
<%

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT d.PessoaID AS fldID, d.Fantasia AS fldName, g.ConceitoID AS MoedaID, g.SimboloMoeda AS SimboloMoeda " & _
	"FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), Recursos_Direitos c WITH(NOLOCK), Pessoas d WITH(NOLOCK), RelacoesPesRec e WITH(NOLOCK), RelacoesPesRec_Moedas f WITH(NOLOCK), Conceitos g WITH(NOLOCK) " & _
	"WHERE (a.SujeitoID = " & CStr(nUsuarioID) & " AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 AND " & _
		"a.RelacaoID = b.RelacaoID AND b.PerfilID = c.PerfilID AND c.RecursoID = 40009 AND " & _
		"c.RecursoMaeID = 28000 AND c.ContextoID IN (9111,9112) AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND b.EmpresaID = d.PessoaID AND " & _
		"b.EmpresaID = e.SujeitoID AND e.TipoRelacaoID = 12 AND e.ObjetoID = 999 AND e.RelacaoID = f.RelacaoID AND " & _
		"f.Faturamento = 1 AND f.MoedaID = g.ConceitoID) " & _
		"GROUP BY d.PessoaID, d.Fantasia, g.ConceitoID, g.SimboloMoeda " & _
		"ORDER BY fldName"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
    
    If (CStr(nEmpresaID) = CStr(rsData.Fields("fldID").Value)) Then
		Response.Write " SELECTED "
    End If

    Response.Write "MoedaID = " & Chr(34) & rsData.Fields("MoedaID").Value & Chr(34) & " " 
    Response.Write "SimboloMoeda = " & Chr(34) & rsData.Fields("SimboloMoeda").Value & Chr(34) & " " 
    
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>

        <p id="lblFamilia" name="lblFamilia" class="lblGeneral">Fam�lias</p>
        <select id="selFamilia" name="selFamilia" class="fldGeneral" MULTIPLE>
<%

Set rsData = Server.CreateObject("ADODB.Recordset")

'strSQL = "SELECT g.ConceitoID AS fldID, g.Conceito AS fldName " & _
'	"FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), Recursos_Direitos c WITH(NOLOCK), Financeiro d WITH(NOLOCK), " & _
'	"RelacoesPesCon e WITH(NOLOCK), Conceitos f WITH(NOLOCK), Conceitos g WITH(NOLOCK) " & _
'	"WHERE (a.SujeitoID = " & CStr(nUsuarioID) & " AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 AND " & _
'		"a.RelacaoID = b.RelacaoID AND b.PerfilID = c.PerfilID AND c.RecursoID = 40008 AND " & _
'		"c.RecursoMaeID = 28000 AND c.ContextoID IN (9111,9112) AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND " & _
'		"b.EmpresaID = d.EmpresaID AND d.PedidoID IS NOT NULL AND b.EmpresaID = e.SujeitoID AND e.TipoRelacaoID = 61 AND " & _
'		"e.ObjetoID = f.ConceitoID AND f.ProdutoID = g.ConceitoID) " & _
'		"GROUP BY g.ConceitoID, g.Conceito " & _
'		"ORDER BY fldName"

strSQL = "SELECT g.ConceitoID AS fldID, g.Conceito AS fldName " & _
	"FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), Recursos_Direitos c WITH(NOLOCK), " & _
	"RelacoesPesCon e WITH(NOLOCK), Conceitos f WITH(NOLOCK), Conceitos g WITH(NOLOCK) " & _
	"WHERE (a.SujeitoID = " & CStr(nUsuarioID) & " AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 AND " & _
		"a.RelacaoID = b.RelacaoID AND b.PerfilID = c.PerfilID AND c.RecursoID = 40009 AND " & _
		"c.RecursoMaeID = 28000 AND c.ContextoID IN (9111,9112) AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND " & _
		"b.EmpresaID = e.SujeitoID AND e.TipoRelacaoID = 61 AND " & _
		"e.ObjetoID = f.ConceitoID AND f.ProdutoID = g.ConceitoID) " & _
		"GROUP BY g.ConceitoID, g.Conceito " & _
		"ORDER BY fldName"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>

        <p id="lblMarca" name="lblMarca" class="lblGeneral">Marcas</p>
        <select id="selMarca" name="selMarca" class="fldGeneral" MULTIPLE>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

'strSQL = "SELECT g.ConceitoID AS fldID, g.Conceito AS fldName " & _
'	"FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), Recursos_Direitos c WITH(NOLOCK), Financeiro d WITH(NOLOCK), " & _
'	"RelacoesPesCon e WITH(NOLOCK), Conceitos f WITH(NOLOCK), Conceitos g WITH(NOLOCK) " & _
'	"WHERE (a.SujeitoID = " & CStr(nUsuarioID) & " AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 AND " & _
'		"a.RelacaoID = b.RelacaoID AND b.PerfilID = c.PerfilID AND c.RecursoID = 40009 AND " & _
'		"c.RecursoMaeID = 28000 AND c.ContextoID IN (9111,9112) AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND " & _
'		"b.EmpresaID = d.EmpresaID AND d.PedidoID IS NOT NULL AND b.EmpresaID = e.SujeitoID AND e.TipoRelacaoID = 61 AND " & _
'		"e.ObjetoID = f.ConceitoID AND f.MarcaID = g.ConceitoID) " & _
'		"GROUP BY g.ConceitoID, g.Conceito " & _
'		"ORDER BY fldName"

strSQL = "SELECT g.ConceitoID AS fldID, g.Conceito AS fldName " & _
	"FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), Recursos_Direitos c WITH(NOLOCK), " & _
	"RelacoesPesCon e WITH(NOLOCK), Conceitos f WITH(NOLOCK), Conceitos g WITH(NOLOCK) " & _
	"WHERE (a.SujeitoID = " & CStr(nUsuarioID) & " AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 AND " & _
		"a.RelacaoID = b.RelacaoID AND b.PerfilID = c.PerfilID AND c.RecursoID = 40009 AND " & _
		"c.RecursoMaeID = 28000 AND c.ContextoID IN (9111,9112) AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND " & _
		"b.EmpresaID = e.SujeitoID AND e.TipoRelacaoID = 61 AND " & _
		"e.ObjetoID = f.ConceitoID AND f.MarcaID = g.ConceitoID) " & _
		"GROUP BY g.ConceitoID, g.Conceito " & _
		"ORDER BY fldName"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>

        <p id="lblMoedaID" name="lblMoedaID" class="lblGeneral">Moeda</p>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT TOP 1 b.ConceitoID AS fldID, b.SimboloMoeda AS fldName " & _
	"FROM Recursos a WITH(NOLOCK), Conceitos b WITH(NOLOCK) " & _
	"WHERE (a.TipoRecursoID = 1 AND a.EstadoID = 2 AND a.MoedaID = b.ConceitoID) "

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

Response.Write "<select id='selMoedaID' name='selMoedaID' class='fldGeneral' "

If (Not rsData.EOF) Then
	Response.Write "MoedaID = '" & rsData.Fields("fldID").Value & "' " & _
		"SimboloMoeda = '" & rsData.Fields("fldName").Value & "'>" & vbCrlf
Else
	Response.Write ">" & vbCrlf
End If

Response.Write "</select>"

rsData.Close
Set rsData = Nothing
%>
        <p id="lblFiltro" name="lblFiltro" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro" name="txtFiltro" class="fldGeneral" LANGUAGE="javascript" onfocus="return __selFieldContent(this)">

        <input type="button" id="btnFillGrid" name="btnFillGrid" value="List" LANGUAGE="javascript" onclick="return btn_onclick(this)" title="Listar" class="btns"></input>
        <input type="button" id="btnDetalhar" name="btnDetalhar" value="Det" LANGUAGE="javascript" onclick="return btn_onclick(this)" title="Detalhar" class="btns"></input>
        <input type="button" id="btnFinanceiro" name="btnFinanceiro" value="Fin" LANGUAGE="javascript" onclick="return btn_onclick(this)" title="Detalhar Financeiro" class="btns"></input>
        <input type="image" id="btnExcel" name="btnExcel" class="fldGeneral" title="Exportar para Excel" LANGUAGE="javascript" onclick="return btn_onclick(this)" WIDTH="24" HEIGHT="23">
		<input type="button" id="btnPagar" name="btnPagar" value="Paga" LANGUAGE="javascript" onclick="return btn_onclick(this)" title="Pagar comiss�es" class="btns"></input>
		<input type="button" id="btnImprimir" name="btnImprimir" value="Imp" LANGUAGE="javascript" onclick="return btn_onclick(this)" title="Imprimir" class="btns"></input>
    </div>    
    
    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    

    <input type="button" id="btnOK" name="btnOK" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>
</body>

</html>
