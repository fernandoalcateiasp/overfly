/********************************************************************
modalgerarocorrencias.js

Library javascript para o modalgerarocorrencias.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var glb_bListar = false;
var glb_nIncluirRetencoes = 0;
var glb_nRow = null;
var glb_RetencoesPCC = false;
var glb_RetencoesGerais = 0;
var glb_TemPCC = false;
var glb_TemPCCFinGrid = false;
var glb_ProdutoServico = false;
var glb_bParceiroMarketplace = false;
var glb_nTipoFinanceiroID = null;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';

var dsoGrid = new CDatatransport("dsoGrid");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoCmbPessoas = new CDatatransport("dsoCmbPessoas");
var dsoRetencoes = new CDatatransport("dsoRetencoes");
var dsoProdutoServico = new CDatatransport("dsoProdutoServico");
var dsoIncluiRetencoes = new CDatatransport("dsoIncluiRetencoes");
var dsoParceiro = new CDatatransport("dsoParceiro");
/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalgerarocorrenciasBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalgerarocorrenciasDblClick(grid, Row, Col)
js_modalgerarocorrenciasKeyPress(KeyAscii)
js_modalgerarocorrencias_ValidateEdit()
js_modalgerarocorrencias_BeforeEdit(grid, row, col)
js_modalgerarocorrencias_AfterEdit(Row, Col)
js_fg_AfterRowColmodalgerarocorrencias (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalgerarocorrenciasBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    glb_bFirstFill = true;

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    if (!glb_bEhEstorno)
        secText('Gerar Ocorr�ncias (Baixa Cruzada)', 1);
    else
        secText('Gerar Ocorr�ncias (Baixa Cruzada Estorno)', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // desabilita o botao OK
    btnOK.disabled = true;
    btnFillGrid.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblFinanceiroID', 'txtFinanceiroID', 10, 1, -10, -10],
						  ['lblEstado', 'txtEstado', 2, 1, -2],
						  ['lblPedidoID', 'txtPedidoID', 10, 1, -2],
						  ['lblCFOP', 'txtCFOP', 5, 1, -2],
                          ['lblDuplicata', 'txtDuplicata', 8, 1, -2],
                          ['lblPessoa', 'txtPessoa', 16, 1, -2],
                          ['lblParceiro', 'txtParceiro', 16, 1, -2],
                          ['lblFormaPagamento', 'txtFormaPagamento', 5, 1, -2],
                          ['lblPrazoPagamento', 'txtPrazoPagamento', 4, 1, -2],
                          ['lbldtVencimento', 'txtdtVencimento', 9, 1, -2],
                          ['lblMoeda', 'txtMoeda', 4, 2, -10],
                          ['lblValor', 'txtValor', 10, 2, -2],
                          ['lblSaldoDevedor', 'txtSaldoDevedor', 11, 2, -2],
                          ['lblSaldoAtualizado', 'txtSaldoAtualizado', 11, 2, -2],
                          ['lblSaldo', 'txtSaldo', 11, 2, -10],
                          ['lblPedidoPesquisaID', 'txtPedidoPesquisaID', 10, 2, -2],
						  ['lblFinanceiroPesquisaID', 'txtFinanceiroPesquisaID', 10, 2, -2],
  						  ['lblArgumento', 'txtArgumento', 13, 3, -10],
					      ['btnFindPessoa', 'btn', 24, 3],
						  ['lblParceiroID', 'selParceiroID', 18, 3],
						  ['lblSaldo2', 'chkSaldo', 3, 3],
						  ['lblEmpresaID', 'selEmpresaID', 20, 3],
                          ['lblDataInicio', 'txtDataInicio', 10, 3],
                          ['lblDataFim', 'txtDataFim', 10, 3, -22]], null, null, true);

    // Ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        btnFillGrid.offsetTop + btnFillGrid.offsetHeight + 2;
        width = txtSaldoAtualizado.offsetLeft + txtSaldoAtualizado.offsetWidth + ELEM_GAP;
        height = selParceiroID.offsetTop + selParceiroID.offsetHeight;
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // Reposiciona botao OK
    with (btnOK) {
        style.top = divControls.offsetTop + txtSaldoAtualizado.offsetTop;
        style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
        value = 'Ocorr�ncias';
        title = 'Gerar Ocorr�ncias';
    }

    with (btnFillGrid) {
        style.width = btnOK.currentStyle.width;
        style.height = btnOK.currentStyle.height;
        style.top = btnOK.currentStyle.top;
        style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnFillGrid.currentStyle.width, 10) - 4;
    }

    // Esconde botao cancel
    with (btnCanc) {
        style.visibility = 'hidden';
    }

    txtFinanceiroID.readOnly = true;
    txtEstado.readOnly = true;
    txtPedidoID.readOnly = true;
    txtCFOP.readOnly = true;
    txtDuplicata.readOnly = true;
    txtPessoa.readOnly = true;
    txtParceiro.readOnly = true;
    txtFormaPagamento.readOnly = true;
    txtPrazoPagamento.readOnly = true;
    txtdtVencimento.readOnly = true;
    txtMoeda.readOnly = true;
    txtValor.readOnly = true;
    txtSaldoDevedor.readOnly = true;
    txtSaldoAtualizado.readOnly = true;
    txtSaldo.readOnly = true;

    txtFinanceiroID.value = glb_nFinanceiroID;
    txtEstado.value = glb_sEstado;

    if (glb_nPedidoID > 0)
        txtPedidoID.value = glb_nPedidoID;

    txtCFOP.value = glb_sCFOP;
    txtCFOP.title = glb_sOperacao;
    txtDuplicata.value = glb_sDuplicata;
    lblPessoa.innerText = lblPessoa.innerText + ' ' + glb_nPessoaID;
    lblPessoa.style.width = (lblPessoa.innerText.length * FONT_WIDTH) + ELEM_GAP;
    txtPessoa.value = glb_sPessoa;
    lblParceiro.innerText = lblParceiro.innerText + ' ' + glb_nParceiroID;
    lblParceiro.style.width = (lblParceiro.innerText.length * FONT_WIDTH) + ELEM_GAP;
    txtParceiro.value = glb_sParceiro;
    txtFormaPagamento.value = glb_sFormaPagamento;
    txtPrazoPagamento.value = glb_nPrazoPagamento;
    txtdtVencimento.value = glb_sdtVencimento;
    txtMoeda.value = glb_sMoeda;
    txtValor.value = padNumReturningStr(roundNumber(glb_nValor, 2), 2);
    txtSaldoDevedor.value = padNumReturningStr(roundNumber(glb_nSaldoDevedor, 2), 2);
    txtSaldoAtualizado.value = padNumReturningStr(roundNumber(glb_nSaldoAtualizado, 2), 2);

    txtPedidoPesquisaID.setAttribute('thePrecision', 10, 1);
    txtPedidoPesquisaID.setAttribute('theScale', 0, 1);
    txtPedidoPesquisaID.setAttribute('verifyNumPaste', 1);
    txtPedidoPesquisaID.onkeypress = verifyNumericEnterNotLinked;
    txtPedidoPesquisaID.onkeyup = pesquisaKeyUp;
    txtPedidoPesquisaID.setAttribute('minMax', new Array(0, 9999999999), 1);
    txtPedidoPesquisaID.maxLength = 10;
    txtPedidoPesquisaID.onfocus = selFieldContent;

    txtFinanceiroPesquisaID.setAttribute('thePrecision', 10, 1);
    txtFinanceiroPesquisaID.setAttribute('theScale', 0, 1);
    txtFinanceiroPesquisaID.setAttribute('verifyNumPaste', 1);
    txtFinanceiroPesquisaID.onkeypress = verifyNumericEnterNotLinked;
    txtFinanceiroPesquisaID.onkeyup = pesquisaKeyUp;
    txtFinanceiroPesquisaID.setAttribute('minMax', new Array(0, 9999999999), 1);
    txtFinanceiroPesquisaID.maxLength = 10;
    txtFinanceiroPesquisaID.onfocus = selFieldContent;

    txtArgumento.onkeyup = pesquisaPessoaKeyUp;
    selParceiroID.onchange = selCmbs_onchange;
    selEmpresaID.onchange = selCmbs_onchange;
    chkSaldo.onclick = selCmbs_onchange;

    txtDataInicio.maxLength = 10;
    txtDataInicio.onkeypress = verifyDateTimeNotLinked;
    txtDataFim.maxLength = 10;
    txtDataFim.onkeypress = verifyDateTimeNotLinked;

    selCmbs_onchange(selParceiroID.id);

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;
    adjustLabelsCombos();
}

function pesquisaKeyUp() {
    if (event.keyCode == 13) {
        listar();
    }
}

function pesquisaPessoaKeyUp() {
    if (event.keyCode == 13) {
        loadCmbPessoa();
    }
}

function btnLupaClicked(btnClicked) {
    if (btnFindPessoa.src == glb_LUPA_IMAGES[1].src)
        return;

    loadCmbPessoa();
}

function loadCmbPessoa() {
    lockControlsInModalWin(true);
    // parametrizacao do dso dsoGrid
    setConnection(dsoCmbPessoas);
    var sFiltro = '';

    if (txtArgumento.value != '')
        sFiltro = ' AND Fantasia LIKE ' + '\'' + '%' + txtArgumento.value + '%' + '\'' + ' ';

    dsoCmbPessoas.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
		'UNION ALL ' +
		'SELECT TOP 50 PessoaID AS fldID, Fantasia AS fldName ' +
		'FROM Pessoas WITH(NOLOCK) ' +
		'WHERE EstadoID=2 AND TipoPessoaID IN (51, 52) ' + sFiltro +
		'ORDER BY fldName';

    dsoCmbPessoas.ondatasetcomplete = loadCmbPessoa_DSC;

    dsoCmbPessoas.Refresh();
}

function loadCmbPessoa_DSC() {
    fg.Rows = 1;
    clearComboEx(['selParceiroID']);

    if (!((dsoCmbPessoas.recordset.BOF) && (dsoCmbPessoas.recordset.EOF))) {
        dsoCmbPessoas.recordset.MoveFirst();
        while (!dsoCmbPessoas.recordset.EOF) {
            optionStr = dsoCmbPessoas.recordset['fldName'].value;
            optionValue = dsoCmbPessoas.recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            selParceiroID.add(oOption);
            dsoCmbPessoas.recordset.MoveNext();
        }
    }

    lockControlsInModalWin(false);
    if (selParceiroID.options.length == 2) {
        selParceiroID.selectedIndex = 1;
        adjustLabelsCombos();
    }
    else
        selParceiroID.selectedIndex = 0;

    selParceiroID.disabled = (selParceiroID.options.length == 0);
    adjustLabelsCombos();
    setupBtnsFromGridState();
}

function selCmbs_onchange(component) {
    if ((this.id == 'selParceiroID') || (this.id == 'selEmpresaID') || (this.id == 'chkSaldo')) {
        fg.Rows = 1;
        adjustLabelsCombos();
        setupBtnsFromGridState();
    }

    if ((this.id == 'selParceiroID') || (component == 'selParceiroID')) {
        setConnection(dsoParceiro);
        dsoParceiro.SQL = 'SELECT dbo.fn_Pessoa_Marketplace(' + selParceiroID.value + ', 3) AS ParceiroID';
        dsoParceiro.ondatasetcomplete = dsoParceiro_DSC;
        dsoParceiro.Refresh();
    }
}

function adjustLabelsCombos() {
    setLabelOfControl(lblParceiroID, selParceiroID.value);
    setLabelOfControl(lblEmpresaID, selEmpresaID.value);
}

function dsoParceiro_DSC() {

    var nParceiroID = 0;

    if ((!dsoParceiro.recordset.EOF) && (!dsoParceiro.recordset.BOF)) {
        nParceiroID = dsoParceiro.recordset['ParceiroID'].value;

        if (nParceiroID > 0) {
            glb_bParceiroMarketplace = true;
            lblDataInicio.style.visibility = 'inherit';
            txtDataInicio.style.visibility = 'inherit';
            lblDataFim.style.visibility = 'inherit';
            txtDataFim.style.visibility = 'inherit';
        }
        else {
            glb_bParceiroMarketplace = false;
            lblDataInicio.style.visibility = 'hidden';
            txtDataInicio.style.visibility = 'hidden';
            lblDataFim.style.visibility = 'hidden';
            txtDataFim.style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        saveDataInGrid();
    }
    else if (controlID == 'btnFillGrid') {
        listar();
    }
        // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    lockControlsInModalWin(true);

    // @@ atualizar interface aqui
    setupBtnsFromGridState();

    // preenche o grid da janela modal
    if (!glb_bFirstFill)
        fillGridData();

    glb_bFirstFill = false;
    // provisorio
    showExtFrame(window, true);
    lockControlsInModalWin(false);
    setupBtnsFromGridState();
    window.focus();
    txtPedidoPesquisaID.focus();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState() {
    var i;
    var bHasRowsInGrid = fg.Rows > 1;

    btnOK.disabled = true;
    btnFillGrid.disabled = true;

    if (bHasRowsInGrid) {
        for (i = 1; i < fg.Rows; i++) {
            if (getCellValueByColKey(fg, 'OK', i) != 0) {
                btnOK.disabled = false;
                break;
            }
        }
    }

    btnFillGrid.disabled = false;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    glb_nTipoFinanceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoFinanceiroID' + '\'' + '].value');

    lockControlsInModalWin(true);

    var strSQL = '';
    var sFiltroPedido = '';
    var sFiltroFinanceiro = '';
    var sFiltroEmpresa = '';
    var sPedidosOuterJoin = 'LEFT OUTER JOIN';
    var sFiltroParceiro = '';

    if (selParceiroID.value > 0)
        //sFiltroParceiro = '((b.PessoaID = ' + selParceiroID.value + ') OR (b.ParceiroID = ' + selParceiroID.value + ')) AND ';
        /*remover isso*/
        sFiltroParceiro = '((b.PessoaID = ' + (selParceiroID.value == 67788 ? 238356 : selParceiroID.value) + ') OR (b.ParceiroID = ' + (selParceiroID.value == 67788 ? 238356 : selParceiroID.value )+ ')) AND '; 
    if (trimStr(txtPedidoPesquisaID.value) != '') {
        sFiltroPedido = ' AND d.PedidoID = ' + txtPedidoPesquisaID.value + ' ';
        sPedidosOuterJoin = 'INNER JOIN';
    }

    if (trimStr(txtFinanceiroPesquisaID.value) != '')
        sFiltroFinanceiro = ' AND b.FinanceiroID = ' + txtFinanceiroPesquisaID.value + ' ';

    sFiltroEmpresa = ' AND b.EmpresaID=' + selEmpresaID.value + ' ';

    var nTipoSaldo = (chkSaldo.checked ? '8' : '9');

    if ((selParceiroID.value == 0) && (sFiltroPedido == '') && (sFiltroFinanceiro == '')) {
        if (window.top.overflyGen.Alert('Selecione pelo menos um parceiro, pedido ou financeiro.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
        return null;
    }

    var sAplicar = '';
    var sFiltroMarketplace = '';

    if (glb_bEhEstorno) {
        sAplicar = 'j.ValorOcorrencia * dbo.fn_Financeiro_TaxaMoedaReferencia(a.FinanceiroID, b.FinanceiroID)';
    }
    else {
        if (glb_bParceiroMarketplace){
            sAplicar = 'q.Valor';
            sFiltroMarketplace = ' AND (p.EstadoID = 8) AND (d.OrigemPedidoID = 607) ' + filtroDataCiclo();
        }
        else
            sAplicar = '0';
    }

    glb_nDSOs = 1;

    // zera o grid
    fg.Rows = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    strSQL = 'SELECT 1 AS Indice, NULL AS FinanceiroID, b.RecursoAbreviado AS Estado, NULL AS OK, ' +
			'NULL AS PedidoID, NULL AS Duplicata, NULL AS Pessoa, NULL AS Parceiro, NULL AS FormaPagamento, ' +
			'NULL AS PrazoPagamento, NULL AS dtVencimento, NULL AS Moeda, NULL AS Valor, NULL AS Moeda2, ' +
            'dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, a.dtVencimento, a.MoedaID, NULL) AS SaldoDevedor, ' +
	        'dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, a.dtVencimento, a.MoedaID, NULL) AS SaldoAtualizado, NULL AS Aplicar, NULL AS FinOcorrenciaID, ' +
	        '0 AS Retencoes, 0 AS Aplicado, ' +
	        'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + glb_nFinanceiroID + ', NULL, 18, NULL, NULL, NULL, ' + glb_nFinanceiroID + ', 2), 0) AS PCC, ' +
	        'a.HistoricoPadraoID AS HistoricoPadraoID, ' +
	        'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + glb_nFinanceiroID + ', NULL, NULL, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesGerais, ' + //Reten��es gerais do financeiro
			'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + glb_nFinanceiroID + ', NULL, 18, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesPCC, ' +
			'0 AS ValorFinanceiroPrincipal, ' +
			'(SELECT TOP 1 aa.ValorBase FROM dbo.fn_Financeiro_Retencoes_tbl(' + glb_nFinanceiroID + ', NULL, 18, 1, NULL, NULL) aa ' +
			    'WHERE ((aa.FinanceiroID = ' + glb_nFinanceiroID + ') AND (aa.Acumula = 1) AND (aa.Retencao IS NULL)) ORDER BY aa.Data DESC) AS ValorAcumulado, ' +
            '(SELECT COUNT(1) FROM  Financeiro  a WITH(NOLOCK) ' +
                    'INNER JOIN Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) ' +
                    'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) ' +
                    'INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = c.ProdutoID) ' +
                    ' WHERE a.FinanceiroID = ' + glb_nFinanceiroID + ' AND (a.EmpresaID <> 7) AND (d.TipoConceitoID = 303) AND (d.ClassificacaoID = 315)) AS ProdutoServico, ' +
            //'(SELECT COUNT(1) FROM Financeiro_Ocorrencias aa WITH(NOLOCK) WHERE aa.FinanceiroID = ' + glb_nFinanceiroID + ' AND aa.TipoOcorrenciaID = 1055) AS Pagamentos ' + //Se J� existe linha de pagamento.
            '0 AS SaldoDevedorPag ' +
			'FROM Financeiro a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
			'WHERE (a.FinanceiroID = ' + glb_nFinanceiroID + ' AND a.EstadoID = b.RecursoID) ' +
			'UNION ALL ' +
			'SELECT 2 AS Indice, b.FinanceiroID AS FinanceiroID, c.RecursoAbreviado AS Estado, CONVERT(BIT, 0) AS OK, ' +
			'b.PedidoID, b.Duplicata, f.Fantasia AS Pessoa, g.Fantasia AS Parceiro, h.ItemAbreviado AS FormaPagamento, ' +
			'b.PrazoPagamento, b.dtVencimento AS dtVencimento, i.SimboloMoeda AS Moeda, b.Valor AS Valor, ' +
			'o.SimboloMoeda AS Moeda2, ' +
            'dbo.fn_Financeiro_Posicao(b.FinanceiroID, 9, NULL, a.dtVencimento, a.MoedaID, NULL) * ' +
				'dbo.fn_Financeiro_TaxaMoedaReferencia(a.FinanceiroID, b.FinanceiroID) AS SaldoDevedor, ' +
	        'dbo.fn_Financeiro_Posicao(b.FinanceiroID, 8, NULL, a.dtVencimento, a.MoedaID, NULL) * ' +
				'dbo.fn_Financeiro_TaxaMoedaReferencia(a.FinanceiroID, b.FinanceiroID) AS SaldoAtualizado, ' +
	        sAplicar + ' AS Aplicar, ' +
			(glb_bEhEstorno ? 'l.FinOcorrenciaID' : '0') + ' AS FinOcorrenciaID, ' +
			'0 AS Retencoes, 0 AS Aplicado, ' +
			'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(b.FinanceiroID, NULL, 18, NULL, NULL, NULL, b.FinanceiroID, 2), 0) AS PCC, ' +
			'b.HistoricoPadraoID, ' +
			'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(b.FinanceiroID, NULL, NULL, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesGerais, ' + //Reten��es gerais do financeiro
			'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(b.FinanceiroID, NULL, 18, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesPCC, ' +
			'0 AS ValorFinanceiroPrincipal, ' +
            '(SELECT TOP 1 aa.ValorBase FROM dbo.fn_Financeiro_Retencoes_tbl(b.FinanceiroID, NULL, 18, 1, NULL, NULL) aa ' +
                'WHERE ((aa.FinanceiroID = b.FinanceiroID) AND (aa.Acumula = 1) AND (aa.Retencao IS NULL)) ORDER BY aa.Data DESC) AS ValorAcumulado, ' +
			//'(SELECT COUNT(1) FROM Financeiro_Ocorrencias aa WITH(NOLOCK) WHERE aa.FinanceiroID = b.FinanceiroID AND aa.TipoOcorrenciaID = 1055) AS Pagamentos ' + //Se J� existe linha de pagamento.
            '(SELECT COUNT(1) FROM  Financeiro  a WITH(NOLOCK) ' +
                    'INNER JOIN Pedidos bb WITH(NOLOCK) ON (bb.PedidoID = a.PedidoID) ' +
                    'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.PedidoID = bb.PedidoID) ' +
                    'INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = c.ProdutoID) ' +
                    ' WHERE a.FinanceiroID = b.FinanceiroID AND (a.EmpresaID <> 7) AND (d.TipoConceitoID = 303) AND (d.ClassificacaoID = 315)) AS ProdutoServico, ' +
            'dbo.fn_Financeiro_VariacaoCambial((CASE WHEN (a.TipoFinanceiroID = 1001) THEN a.FinanceiroID ELSE b.FinanceiroID END), (CASE WHEN (a.TipoFinanceiroID = 1001) THEN b.FinanceiroID ELSE a.FinanceiroID END), 2) AS SaldoDevedorPag ' +

		'FROM Financeiro a WITH(NOLOCK) ' +
		    'INNER JOIN Financeiro b WITH(NOLOCK) ON ((b.TipoFinanceiroID = (CASE a.TipoFinanceiroID WHEN 1001 THEN 1002 ELSE 1001 END))) ' +
            'INNER JOIN Recursos c WITH(NOLOCK) ON (b.EstadoID = c.RecursoID) ' +
            sPedidosOuterJoin + ' Pedidos d ON (b.PedidoID = d.PedidoID) ' +
            'INNER JOIN Pessoas f WITH(NOLOCK) ON (b.PessoaID = f.PessoaID) ' +
            'INNER JOIN Pessoas g WITH(NOLOCK) ON (b.ParceiroID = g.PessoaID) ' +
            'INNER JOIN TiposAuxiliares_Itens h WITH(NOLOCK) ON (b.FormaPagamentoID = h.ItemID) ' +
            'INNER JOIN Conceitos i WITH(NOLOCK) ON (b.MoedaID = i.ConceitoID) ' +
            'INNER JOIN HistoricosPadrao m WITH(NOLOCK) ON (a.HistoricoPadraoID = m.HistoricoPadraoID) ' +
            'INNER JOIN HistoricosPadrao n WITH(NOLOCK) ON (b.HistoricoPadraoID = n.HistoricoPadraoID) ' +
            'INNER JOIN Conceitos o WITH(NOLOCK) ON (a.MoedaID = o.ConceitoID) ';

    if (glb_bParceiroMarketplace)
        strSQL += "INNER JOIN CobrancaDiversas p WITH(NOLOCK) ON ((p.PrestadorID = d.ProgramaMarketingID) AND (p.EmpresaID = d.EmpresaID)) " +
                  "INNER JOIN CobrancaDiversas_Detalhes q WITH(NOLOCK) " +
                      "ON ((q.CobrancaDiversaID = p.CobrancaDiversaID) AND (d.SeuPedido LIKE '%' + q.PedidoPrestador + '%') AND (q.TipoOcorrenciaID = 1441) " +
                        "AND ((q.Estorno = 1 AND d.TipoPedidoID = 601) OR (q.Estorno = 0 AND d.TipoPedidoID = 602))) ";

    if (!glb_bEhEstorno)
        strSQL += 'WHERE (a.FinanceiroID = ' + glb_nFinanceiroID + ' AND ' +
				'dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, a.dtVencimento, a.MoedaID, NULL) > 0 AND ' +
				sFiltroParceiro +
				'a.EstadoID NOT IN (1,5,48) AND b.EstadoID NOT IN(1,5,48) AND ' +
				'((ISNULL(m.PesoContabilizacao, 0) <> 0) OR (a.EhEstorno = 1) OR (b.EhEstorno = 1) ' +
                    'OR (a.HistoricoPadraoID IN (1185, 1186, 1127, 1213, 1214, 1128, 2141, 2142)) OR (a.HistoricoPadraoID IN (1185, 1186, 1127, 1213, 1214, 1128, 2141, 2142))) AND ' +
				'((ISNULL(n.PesoContabilizacao, 0) <> 0) OR (a.EhEstorno = 1) OR (b.EhEstorno = 1) ' +
                    'OR (a.HistoricoPadraoID IN (1185, 1186, 1127, 1213, 1214, 1128, 2141, 2142)) OR (a.HistoricoPadraoID IN (1185, 1186, 1127, 1213, 1214, 1128, 2141, 2142))) AND ' +
				'dbo.fn_Financeiro_Posicao(b.FinanceiroID, ' + nTipoSaldo + ', NULL, a.dtVencimento, a.MoedaID, NULL) > 0) ' +
				sFiltroPedido + sFiltroFinanceiro + sFiltroEmpresa + sFiltroMarketplace +
			'ORDER BY Indice, FinanceiroID';
    else
        strSQL += ' INNER JOIN Financeiro_Ocorrencias j WITH(NOLOCK) ON (a.FinanceiroID = j.FinanceiroID) ' +
		        ' INNER JOIN Financeiro_Ocorrencias l WITH(NOLOCK) ON (j.FinOcorrenciaReferenciaID = l.FinOcorrenciaID AND b.FinanceiroID = l.FinanceiroID) ' +
				'WHERE (a.FinanceiroID = ' + glb_nFinanceiroID + ' AND j.Estorno = 0 AND ' +
				'a.EstadoID NOT IN (1,5) AND b.EstadoID NOT IN(1,5) AND ' +
				'((ISNULL(m.PesoContabilizacao, 0) <> 0) OR (a.EhEstorno = 1) OR (b.EhEstorno = 1) ' +
                    'OR (a.HistoricoPadraoID IN (1185, 1186, 1127, 1213, 1214, 1128, 2141, 2142)) OR (a.HistoricoPadraoID IN (1185, 1186, 1127, 1213, 1214, 1128, 2141, 2142))) AND ' +
				'((ISNULL(n.PesoContabilizacao, 0) <> 0) OR (a.EhEstorno = 1) OR (b.EhEstorno = 1) ' +
                    'OR (a.HistoricoPadraoID IN (1185, 1186, 1127, 1213, 1214, 1128, 2141, 2142)) OR (a.HistoricoPadraoID IN (1185, 1186, 1127, 1213, 1214, 1128, 2141, 2142))) AND ' +
				'l.Estorno = 0 AND ' +
				'(SELECT COUNT(*) FROM Financeiro_Ocorrencias WITH(NOLOCK) WHERE (FinanceiroID = j.FinanceiroID AND Estorno = 1 AND dtBalancete = j.dtBalancete AND ' +
				'ValorOcorrencia = j.ValorOcorrencia AND FinOcorrenciaReferenciaID IS NOT NULL)) = 0) ' +
				sFiltroPedido + sFiltroFinanceiro + sFiltroEmpresa + sFiltroMarketplace +
			'ORDER BY Indice, FinanceiroID';
    
    dsoGrid.SQL = strSQL;

    dsoGrid.ondatasetcomplete = fillGridData_DSC;

    try {
        dsoGrid.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;

    var nValorPCC = 0;
    var nValorPCCGrid = 0;
    var nAplicar;

    if (glb_nDSOs > 0)
        return null;

    var nSaldoAtualizado;
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    var aHoldCols = new Array();

    if (glb_bEhEstorno)
        aHoldCols = [15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25];
    else
        aHoldCols = [18, 19, 20, 21, 22, 23, 24, 25];

    dsoGrid.recordset.setFilter('Indice = 1');

    if (!((dsoGrid.recordset.BOF) && (dsoGrid.recordset.EOF))) {
        dsoGrid.recordset.MoveFirst();
        glb_sEstado = dsoGrid.recordset['Estado'].value;

        if ((dsoGrid.recordset['RetencoesPCC'].value <= 0) && (dsoGrid.recordset['HistoricoPadraoID'].value == 3270)) {
            glb_RetencoesPCC = true;
            glb_RetencoesGerais = dsoGrid.recordset['RetencoesGerais'].value;

            glb_nSaldoDevedor = dsoGrid.recordset['SaldoDevedor'].value + glb_RetencoesGerais;
            glb_nSaldoAtualizado = dsoGrid.recordset['SaldoAtualizado'].value + glb_RetencoesGerais;
        }
        else {
            glb_nSaldoDevedor = dsoGrid.recordset['SaldoDevedor'].value;
            glb_nSaldoAtualizado = dsoGrid.recordset['SaldoAtualizado'].value;
        }

        nValorPCC = dsoGrid.recordset['PCC'].value;

        if (nValorPCC > 0)
            glb_TemPCC = true;


        txtEstado.value = glb_sEstado;
        txtSaldoDevedor.value = padNumReturningStr(roundNumber(glb_nSaldoDevedor, 2), 2);
        txtSaldoAtualizado.value = padNumReturningStr(roundNumber(glb_nSaldoAtualizado, 2), 2);
    }
    dsoGrid.recordset.setFilter('');
    dsoGrid.recordset.MoveFirst();


    dsoGrid.recordset.setFilter('Indice = 2');

    if (!((dsoGrid.recordset.BOF) && (dsoGrid.recordset.EOF)))
        dsoGrid.recordset.MoveFirst();

    headerGrid(fg, ['Financeiro',
				   'Est',
				   'OK',
				   'Pedido',
				   'Duplicata',
				   'Pessoa',
				   'Parceiro',
				   'Forma',
				   'Prazo',
				   'Vencimento',
				   '$',
				   'Valor',
				   '$',
				   'Saldo Dev',
				   'Saldo Atualizado',
				   'Aplicar',
				   'Reten��es',
				   'Aplicado',
				   'RetencoesGerais',
				   'HistoricoPadraoID',
				   'PCC',
				   'RetencoesPCC',
				   'ValorFinanceiroPrincipal',
				   'FinOcorrenciaID',
                   'ProdutoServico',
                   'SaldoDevedorPag'], aHoldCols);

    fillGridMask(fg, dsoGrid, ['FinanceiroID*',
							 'Estado*',
							 'OK',
							 'PedidoID*',
							 'Duplicata*',
							 'Pessoa*',
							 'Parceiro*',
							 'FormaPagamento*',
							 'PrazoPagamento*',
							 'dtVencimento*',
							 'Moeda*',
							 'Valor*',
							 'Moeda2*',
							 'SaldoDevedor*',
							 'SaldoAtualizado*',
							 'Aplicar',
							 'PCC*',
							 'Aplicado*',
							 'Retencoes*',
							 'HistoricoPadraoID*',
							 'RetencoesGerais*',
				             'RetencoesPCC*',
				             'ValorFinanceiroPrincipal',
							 'FinOcorrenciaID',
                             'ProdutoServico*',
                             'SaldoDevedorPag'],
							 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '', '', '', '', '', '', ''],
							 ['', '', '', '', '', '', '', '', '', dTFormat, '', '###,###,##0.00', '', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '', '', '', '', '', '', '']);

    dsoGrid.recordset.setFilter('');

    fg.Redraw = 0;

    alignColsInGrid(fg, [0, 3, 8, 11, 13, 14, 15, 16, 17, 18, 20]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.FrozenCols = 5;

    if (!glb_bEhEstorno) {
        for (i = 1; i < fg.Rows; i++) {
            if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'RetencoesPCC*')) <= 0) &&
                    (((fg.TextMatrix(i, getColIndexByColKey(fg, 'HistoricoPadraoID*')) == 1163) && (fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoServico*')) == 1)) ||
                    (fg.TextMatrix(i, getColIndexByColKey(fg, 'HistoricoPadraoID*')) == 3141) || (fg.TextMatrix(i, getColIndexByColKey(fg, 'HistoricoPadraoID*')) == 3270))) {
                fg.TextMatrix(i, getColIndexByColKey(fg, 'SaldoAtualizado*')) = fg.ValueMatrix(i, getColIndexByColKey(fg, 'SaldoAtualizado*')) +
                                                                                    fg.ValueMatrix(i, getColIndexByColKey(fg, 'RetencoesGerais*'));

                fg.TextMatrix(i, getColIndexByColKey(fg, 'SaldoDevedor*')) = fg.ValueMatrix(i, getColIndexByColKey(fg, 'SaldoDevedor*')) +
                                                                                    fg.ValueMatrix(i, getColIndexByColKey(fg, 'RetencoesGerais*'));
            }

            if (chkSaldo.checked)
                nSaldoAtualizado = fg.ValueMatrix(i, getColIndexByColKey(fg, 'SaldoAtualizado*'));
            else
                nSaldoAtualizado = fg.ValueMatrix(i, getColIndexByColKey(fg, 'SaldoDevedor*'));

            nValorPCCGrid = fg.ValueMatrix(i, getColIndexByColKey(fg, 'PCC*'));

            if (nValorPCCGrid > 0)
                glb_TemPCCFinGrid = true;

			if (!glb_bParceiroMarketplace) {
				//Aqui tratar
				if ((nSaldoAtualizado + ((glb_RetencoesPCC) ? glb_RetencoesGerais : 0)) > glb_nSaldoAtualizado)
				{
					if (glb_nTipoFinanceiroID == 1001)
					{
						if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'SaldoDevedorPag')) > 0)
							glb_nSaldoAtualizado = fg.ValueMatrix(i, getColIndexByColKey(fg, 'SaldoDevedorPag'));

						txtSaldoAtualizado.value = padNumReturningStr(roundNumber(glb_nSaldoAtualizado, 2), 2);
					}

					nAplicar = roundNumber(glb_nSaldoAtualizado * ((nValorPCC > 0) ? (1 - (glb_nAliquotaPCC / 100)) : 1), 2);
					fg.TextMatrix(i, getColIndexByColKey(fg, 'ValorFinanceiroPrincipal')) = 1;
				}
				else
				{
					if (glb_nTipoFinanceiroID == 1001)
						nAplicar = roundNumber(nSaldoAtualizado * ((nValorPCCGrid > 0) ? (1 - (glb_nAliquotaPCC / 100)) : 1), 2);
					else
					{
						if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'SaldoDevedorPag')) > 0)
							nSaldoAtualizado = fg.ValueMatrix(i, getColIndexByColKey(fg, 'SaldoDevedorPag'));

						nAplicar = roundNumber(nSaldoAtualizado * ((nValorPCCGrid > 0) ? (1 - (glb_nAliquotaPCC / 100)) : 1), 2);
					}
				}

				fg.TextMatrix(i, getColIndexByColKey(fg, 'Aplicar')) = nAplicar;
			}
        }

        glb_bListar = true;

        //Efetua o calculo das reten��e realizadas no pagamento. BJBN
        calculaRetencoes();
    }

    paintCellsSpecialyReadOnly();

    fg.ExplorerBar = 5;

    fg.ColWidth(getColIndexByColKey(fg, 'Aplicar')) = 150 * FONT_WIDTH;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // Tenta selecionar o maximo de pedidos possiveis para baixar - (esse processo deve passar pelo evento AfterEdit)
    var nColOK = getColIndexByColKey(fg, 'OK');

    if (glb_bParceiroMarketplace) {
        for (i = 1; i < fg.Rows; i++) {
            fg.TextMatrix(i, nColOK) = 1;
            js_modalgerarocorrencias_AfterEdit(i, nColOK);
        }
    }

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        fg.Editable = true;
        window.focus();
        fg.focus();
    }
    else {
        ;
    }

    fg.Redraw = 2;

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nEhEstorno = (glb_bEhEstorno ? 1 : 0);
    var nAplicar = 0;

    lockControlsInModalWin(true);

    strPars += '?nUsuarioID=' + escape(glb_nUserID);
    strPars += '&nFinanceiroMATID=' + escape(glb_nFinanceiroID);
    strPars += '&nEhEstorno=' + escape(nEhEstorno);

    for (i = 1; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0) {
            if (!glb_bEhEstorno)
                strPars += '&nFinanceiroID=' + escape(getCellValueByColKey(fg, 'FinanceiroID*', i));
            else
                strPars += '&nFinanceiroID=' + escape('-' + getCellValueByColKey(fg, 'FinOcorrenciaID', i));

            strPars += '&nValor=' + escape(parseFloat(replaceStr(getCellValueByColKey(fg, 'Aplicar', i), ',', '.')) -
                                                        parseFloat(replaceStr(getCellValueByColKey(fg, 'PCC*', i), ',', '.')));

            strPars += '&nValorRetencao=' + escape(parseFloat(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PCC*'))));

            //Tratar aqui. Bacil
            if ((glb_RetencoesPCC) && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'PCC*')) > 0))
                nAplicar = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aplicar')) + glb_RetencoesGerais;
            else if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'RetencoesPCC*')) <= 0) && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'PCC*')) > 0))
                nAplicar = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aplicar')) + fg.ValueMatrix(i, getColIndexByColKey(fg, 'RetencoesGerais*'));
            else
                nAplicar = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aplicar'));

            strPars += '&nValorBase=' + escape(nAplicar);

            nDataLen++;
        }
    }

    strPars += '&nDataLen=' + escape(nDataLen);

    try {
        dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/gerarocorrencias.aspx' + strPars;
        dsoGrava.ondatasetcomplete = saveDataInGrid_DSC;
        dsoGrava.refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
    }
}

function listar() {
    refreshParamsAndDataAndShowModalWin(false);
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    lockControlsInModalWin(false);

    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        dsoGrava.recordset.MoveFirst();
        if ((dsoGrava.recordset['Resultado'].value != null) &&
			 (dsoGrava.recordset['Resultado'].value != '')) {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0)
                return null;
        }
    }

    glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

/********************************************************************
Gera filtro de data caso seja parceiro marketplace
********************************************************************/
function filtroDataCiclo() {
    var dtInicio = putDateInMMDDYYYY(txtDataInicio.value);
    var dtFim = putDateInMMDDYYYY(txtDataFim.value);

    if ((glb_bParceiroMarketplace) && (dtInicio != "") && (dtFim != "")) {
        dtInicio = "'" + dtInicio + "'";
        dtFim = "'" + dtFim + " 23:59:59'";

        return ' AND (q.dtCiclo BETWEEN ' + dtInicio + ' AND ' + dtFim + ')';
    }
    else {
        return '';
    }   
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly() {
    paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerarocorrenciasBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerarocorrenciasDblClick(grid, Row, Col) {
    return true;

    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if (getColIndexByColKey(grid, 'OK') != Col)
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);

    glb_PassedOneInDblClick = true;

    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 1; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarocorrenciasKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarocorrencias_ValidateEdit() {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarocorrencias_BeforeEdit(grid, row, col) {
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if (fldDet == null)   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ((fldDet[0] == 200) || (fldDet[0] == 129))  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarocorrencias_AfterEdit(Row, Col) {
    var nSaldoAtualizado = parseFloat(txtSaldoAtualizado.value);
    var nTotalSaldoSelecionado = somaSaldoSelecionados();
    var nAplicar;
    var nAplicarRetencao;
    var nHistoricoPadraoID;

    if (!glb_bEhEstorno) {
        if (Col == getColIndexByColKey(fg, 'OK')) {
            nHistoricoPadraoID = fg.TextMatrix(Row, getColIndexByColKey(fg, 'HistoricoPadraoID*'));

            if (isNaN(nSaldoAtualizado) || (nSaldoAtualizado == null) || (nSaldoAtualizado == 0))
                fg.TextMatrix(Row, Col) = 0;

            else if (nTotalSaldoSelecionado > nSaldoAtualizado)
                fg.TextMatrix(Row, Col) = 0;

            else if (((glb_ProdutoServico) && (nHistoricoPadraoID == 1163)) || (nHistoricoPadraoID == 3141) || (nHistoricoPadraoID == 3270)) {
                if (verificaOkRetencao())
                    fg.TextMatrix(Row, Col) = 0;
                else
                    txtSaldo.value = padNumReturningStr(roundNumber(nSaldoAtualizado - nTotalSaldoSelecionado, 2), 2);
            }

            else
                txtSaldo.value = padNumReturningStr(roundNumber(nSaldoAtualizado - nTotalSaldoSelecionado, 2), 2);
        }
        else if (Col == getColIndexByColKey(fg, 'Aplicar')) {
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

            //Efetua o calculo das reten��es.
            calculaRetencoes(Row);

            //Altera��o para saber qual saldo utilizar. BJBN 31/08/2011
            if ((!glb_RetencoesPCC) || (!fg.ValueMatrix(Row, getColIndexByColKey(fg, 'PCC*')) > 0)) {
                nSaldoAtualizado = glb_nSaldoAtualizado - glb_RetencoesGerais;
            }
            //Altera��o para saber qual saldo utilizar. BJBN

            //Recalcula soma das linhas selecionadas para levar em conta o novo calculo da reten��o.
            nTotalSaldoSelecionado = somaSaldoSelecionados();

            if (fg.ColHidden(getColIndexByColKey(fg, 'Aplicado*'))) {
                nAplicar = parseFloat(fg.ValueMatrix(Row, Col));
                nAplicarRetencao = nAplicar;
            }
            else {
                nAplicarRetencao = parseFloat(fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Aplicado*'))) + parseFloat(fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Retencoes*')));
                nAplicar = (nAplicarRetencao - parseFloat(fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Retencoes*'))));
            }

            var nSaldoAtualizadoGrid = parseFloat(fg.ValueMatrix(Row, getColIndexByColKey(fg, 'SaldoAtualizado*')));
            if (isNaN(nSaldoAtualizadoGrid))
                nSaldoAtualizadoGrid = 0;

            if ((!isNaN(nAplicar)) && (nAplicar != null) && (nAplicar != 0)) {
                nHistoricoPadraoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'HistoricoPadraoID' + '\'' + '].value');

                //HPs de toma��o de servico.
                if (((glb_ProdutoServico) && (nHistoricoPadraoID == 1163)) || (nHistoricoPadraoID == 3141) || (nHistoricoPadraoID == 3270)) {
                    if ((nAplicarRetencao > nSaldoAtualizado) || (roundNumber(nAplicar, 2) > nSaldoAtualizadoGrid) ||
					    (nTotalSaldoSelecionado > nSaldoAtualizado)) {
                        fg.TextMatrix(Row, Col) = 0;
                        fg.TextMatrix(Row, getColIndexByColKey(fg, 'Aplicado*')) = 0;
                        fg.TextMatrix(Row, getColIndexByColKey(fg, 'Retencoes*')) = 0;
                        fg.TextMatrix(Row, getColIndexByColKey(fg, 'PCC*')) = 0;
                    }
                }
                else {
                    if ((nAplicar > nSaldoAtualizado) || (nAplicarRetencao > nSaldoAtualizadoGrid) ||
					    (nTotalSaldoSelecionado > nSaldoAtualizado)) {
                        fg.TextMatrix(Row, Col) = 0;
                        fg.TextMatrix(Row, getColIndexByColKey(fg, 'Aplicado*')) = 0;
                        fg.TextMatrix(Row, getColIndexByColKey(fg, 'Retencoes*')) = 0;
                        fg.TextMatrix(Row, getColIndexByColKey(fg, 'PCC*')) = 0;
                    }
                }
            }

            txtSaldo.value = padNumReturningStr(roundNumber(nSaldoAtualizado - somaSaldoSelecionados(), 2), 2);
        }
    }

    setupBtnsFromGridState();
}

function somaSaldoSelecionados() {
    var retVal = 0;
    var i;
    var nValor = 0;
    var nValorAcumulado = 0;

    for (i = 1; i < fg.Rows; i++) {
        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) != '0') {
            if (fg.ColHidden(getColIndexByColKey(fg, 'Aplicado*')))
                nValor = parseFloat(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aplicar')));
            else
                nValor = parseFloat(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aplicado*'))); // + parseFloat(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PCC*')));

            if ((!isNaN(nValor)) && (nValor != null) && (nValor > 0))
                nValorAcumulado += nValor;
        }
    }

    return nValorAcumulado;
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalgerarocorrencias(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return true;

    setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************

function calculaRetencoes(Row) {
    var nFinanceiroID = txtFinanceiroID.value;
    var sSQL = '';
    var sFinanceiros = '';
    var nFinanceiroGrid = 0;
    var nValorAplicarGrid = 0;
    var nHistoricoPadraoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'HistoricoPadraoID' + '\'' + '].value');

    //HPs de toma��o de servico.
    //Precisa tratar este IF quando for o HP 1163 (Sugest�o: entrar no IF quando houver reten��o de PCC no financeiro da modal).
    if (((glb_ProdutoServico) && (nHistoricoPadraoID == 1163)) || (nHistoricoPadraoID == 3141) || (nHistoricoPadraoID == 3270) || (glb_TemPCC == true)) {
        for (iRet = 1; iRet < fg.Rows; iRet++) {
            if (iRet > 1)
                sSQL += ' UNION ALL ';

            if (Row == undefined) {
                nFinanceiroGrid = fg.TextMatrix(iRet, getColIndexByColKey(fg, 'FinanceiroID*'));

                dsoGrid.recordset.setFilter('Indice = 1');

                if ((!dsoGrid.recordset.EOF) && (!dsoGrid.recordset.BOF)) {
                    if ((dsoGrid.recordset['RetencoesPCC'].value <= 0) && (fg.ValueMatrix(iRet, getColIndexByColKey(fg, 'ValorFinanceiroPrincipal')) <= 0))
                        nValorAplicarGrid = roundNumber(((parseFloat(fg.ValueMatrix(iRet, getColIndexByColKey(fg, 'Aplicar'))) + dsoGrid.recordset['RetencoesGerais'].value) / (1 - (glb_nAliquotaPCC / 100))), 2);
                    else
                        nValorAplicarGrid = roundNumber((parseFloat(fg.ValueMatrix(iRet, getColIndexByColKey(fg, 'Aplicar'))) / (1 - (glb_nAliquotaPCC / 100))), 2);
                }
                else
                    nValorAplicarGrid = roundNumber((parseFloat(fg.ValueMatrix(iRet, getColIndexByColKey(fg, 'Aplicar'))) / (1 - (glb_nAliquotaPCC / 100))), 2);

                fg.TextMatrix(iRet, getColIndexByColKey(fg, 'Aplicar')) = nValorAplicarGrid;
                dsoGrid.recordset.setFilter('');

                sFinanceiros = '\'[' + nFinanceiroID + ', ' + nValorAplicarGrid + ']\'';

                sSQL += 'SELECT ' + nFinanceiroGrid + ' AS FinanceiroID, ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroID + ', NULL, NULL, NULL, NULL, NULL, NULL, 3), 0) + ' +
                                    'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroID + ', NULL, 18, 1, ' + sFinanceiros + ', NULL, ' + nFinanceiroID + ', 2), 0) AS Retencao, ' +
                               'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroID + ', NULL, 18, 1, ' + sFinanceiros + ', NULL, ' + nFinanceiroID + ', 2), 0) AS PCC, ' +
                               'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroID + ', NULL, NULL, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesGerais, ' +
                               'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroID + ', NULL, 18, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesPCC, ' +
                               '(SELECT COUNT(1) FROM  Financeiro  a WITH(NOLOCK) ' +
                                        'INNER JOIN Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) ' +
                                        'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) ' +
                                        'INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = c.ProdutoID) ' +
                                        ' WHERE a.FinanceiroID = ' + nFinanceiroID + ' AND (a.EmpresaID <> 7) AND (d.TipoConceitoID = 303) AND (d.ClassificacaoID = 315)) AS ProdutoServico,' +
                               '(SELECT COUNT(1) FROM Financeiro_Ocorrencias WITH(NOLOCK) WHERE FinanceiroID = ' + nFinanceiroID + ' AND TipoOcorrenciaID = 1055) AS Pagamentos';
            }
            else {
                nFinanceiroGrid = fg.TextMatrix(Row, getColIndexByColKey(fg, 'FinanceiroID*'));

                dsoGrid.recordset.setFilter('Indice = 1');

                if ((!dsoGrid.recordset.EOF) && (!dsoGrid.recordset.BOF)) {
                    if (dsoGrid.recordset['RetencoesPCC'].value <= 0) {
                        nValorAplicarGrid = roundNumber(((parseFloat(fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Aplicar'))) + dsoGrid.recordset['RetencoesGerais'].value +
                            dsoGrid.recordset['ValorAcumulado'].value) / (1 - (glb_nAliquotaPCC / 100))), 2);

                        nValorAplicarGrid = (nValorAplicarGrid - dsoGrid.recordset['ValorAcumulado'].value);
                    }
                    else
                        nValorAplicarGrid = roundNumber(((parseFloat(fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Aplicar'))) +
                            dsoGrid.recordset['ValorAcumulado'].value) / (1 - (glb_nAliquotaPCC / 100))), 2);
                }
                else
                    nValorAplicarGrid = roundNumber((parseFloat(fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Aplicar'))) / (1 - (glb_nAliquotaPCC / 100))), 2);

                fg.TextMatrix(Row, getColIndexByColKey(fg, 'Aplicar')) = nValorAplicarGrid;
                dsoGrid.recordset.setFilter('');

                sFinanceiros = '\'[' + nFinanceiroID + ', ' + nValorAplicarGrid + ']\'';

                sSQL = 'SELECT ' + nFinanceiroGrid + ' AS FinanceiroID, ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroID + ', NULL, NULL, NULL, NULL, NULL, NULL, 3), 0) + ' +
                                    'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroID + ', NULL, 18, 1, ' + sFinanceiros + ', NULL, ' + nFinanceiroID + ', 2), 0) AS Retencao, ' +
                               'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroID + ', NULL, 18, 1, ' + sFinanceiros + ', NULL, ' + nFinanceiroID + ', 2), 0) AS PCC, ' +
                               'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroID + ', NULL, NULL, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesGerais, ' +
                               'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroID + ', NULL, 18, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesPCC, ' +
                               '(SELECT COUNT(1) FROM  Financeiro  a WITH(NOLOCK) ' +
                                        'INNER JOIN Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) ' +
                                        'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) ' +
                                        'INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = c.ProdutoID) ' +
                                        ' WHERE a.FinanceiroID = ' + nFinanceiroID + ' AND (a.EmpresaID <> 7) AND (d.TipoConceitoID = 303) AND (d.ClassificacaoID = 315)) AS ProdutoServico,' +
                               '(SELECT COUNT(1) FROM Financeiro_Ocorrencias WITH(NOLOCK) WHERE FinanceiroID = ' + nFinanceiroID + ' AND TipoOcorrenciaID = 1055) AS Pagamentos';

                glb_nRow = Row;
                break;
            }
        }

        lockControlsInModalWin(true);

        if (sSQL != '') {
            setConnection(dsoRetencoes);
            dsoRetencoes.SQL = sSQL;
            dsoRetencoes.ondatasetcomplete = calculaRetencoes_DSC;
            dsoRetencoes.Refresh();
        }
        else
            lockControlsInModalWin(false);
    }
    else {
        for (var ix = 1; ix < fg.Rows; ix++) {
            if (Row == undefined) {
                nFinanceiroID = fg.ValueMatrix(ix, getColIndexByColKey(fg, 'FinanceiroID*'));

                dsoGrid.recordset.setFilter('Indice = 2 AND FinanceiroID = ' + nFinanceiroID);

                glb_ProdutoServico = (dsoGrid.recordset['ProdutoServico'].value == '1' ? true : false);

                if ((!dsoGrid.recordset.EOF) && (!dsoGrid.recordset.BOF)) {
                    dsoGrid.recordset.MoveFirst();
                    //Precisa tratar este IF quando for o HP 1163 (Sugest�o: entrar no IF quando houver reten��o de PCC no financeiro do grid).
                    if (((glb_ProdutoServico) && (dsoGrid.recordset['HistoricoPadraoID'].value == 1163)) ||
                        (dsoGrid.recordset['HistoricoPadraoID'].value == 3141) || (dsoGrid.recordset['HistoricoPadraoID'].value == 3270) || (glb_TemPCCFinGrid == true)) {
                        nValorAplicarGrid = roundNumber((fg.ValueMatrix(ix, getColIndexByColKey(fg, 'Aplicar')) / (1 - (glb_nAliquotaPCC / 100))), 2);

                        fg.TextMatrix(ix, getColIndexByColKey(fg, 'Aplicar')) = nValorAplicarGrid;

                        nFinanceiroGrid = fg.TextMatrix(ix, getColIndexByColKey(fg, 'FinanceiroID*'));
                        sFinanceiros = '\'[' + nFinanceiroID + ', ' + nValorAplicarGrid + ']\'';

                        if (sSQL != '')
                            sSQL += ' UNION ALL ';

                        sSQL += 'SELECT ' + nFinanceiroGrid + ' AS FinanceiroID, ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroGrid + ', NULL, NULL, NULL, NULL, NULL, NULL, 3), 0) + ' +
                                        'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroGrid + ', NULL, 18, 1, ' + sFinanceiros + ', NULL, ' + nFinanceiroGrid + ', 2), 0) AS Retencao, ' +
                                   'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroGrid + ', NULL, 18, 1, ' + sFinanceiros + ', NULL, ' + nFinanceiroGrid + ', 2), 0) AS PCC, ' +
                                   'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroGrid + ', NULL, NULL, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesGerais, ' +
                                   'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroGrid + ', NULL, 18, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesPCC, ' +
                                   '(SELECT COUNT(1) FROM  Financeiro  a WITH(NOLOCK) ' +
                                        'INNER JOIN Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) ' +
                                        'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) ' +
                                        'INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = c.ProdutoID) ' +
                                        'WHERE a.FinanceiroID = ' + nFinanceiroGrid + ' AND (a.EmpresaID <> 7) AND (d.TipoConceitoID = 303) AND (d.ClassificacaoID = 315)) AS ProdutoServico,' +
                                   '(SELECT COUNT(1) FROM Financeiro_Ocorrencias WITH(NOLOCK) WHERE FinanceiroID = ' + nFinanceiroGrid + ' AND TipoOcorrenciaID = 1055) AS Pagamentos';
                    }
                    else
                        fg.TextMatrix(ix, getColIndexByColKey(fg, 'Aplicado*')) = parseFloat(fg.ValueMatrix(ix, getColIndexByColKey(fg, 'Aplicar')));
                }
            }
            else {
                nFinanceiroID = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'FinanceiroID*'));

                dsoGrid.recordset.setFilter('Indice = 2 AND FinanceiroID = ' + nFinanceiroID);

                glb_ProdutoServico = (dsoGrid.recordset['ProdutoServico'].value == '1' ? true : false);

                if ((!dsoGrid.recordset.EOF) && (!dsoGrid.recordset.BOF)) {
                    if (((glb_ProdutoServico) && (dsoGrid.recordset['HistoricoPadraoID'].value == 1163)) ||
                        (dsoGrid.recordset['HistoricoPadraoID'].value == 3141) || (dsoGrid.recordset['HistoricoPadraoID'].value == 3270)) {
                        if (fg.ValueMatrix(Row, getColIndexByColKey(fg, 'RetencoesPCC*')) <= 0) {
                            nValorAplicarGrid = roundNumber(((fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Aplicar')) +
                                fg.ValueMatrix(Row, getColIndexByColKey(fg, 'RetencoesGerais*')) + dsoGrid.recordset['ValorAcumulado'].value) / (1 - (glb_nAliquotaPCC / 100))), 2);

                            nValorAplicarGrid = (nValorAplicarGrid - dsoGrid.recordset['ValorAcumulado'].value);
                        }
                        else
                            nValorAplicarGrid = roundNumber((fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Aplicar')) / (1 - (glb_nAliquotaPCC / 100))), 2);

                        fg.TextMatrix(Row, getColIndexByColKey(fg, 'Aplicar')) = nValorAplicarGrid;

                        nFinanceiroGrid = fg.TextMatrix(Row, getColIndexByColKey(fg, 'FinanceiroID*'));
                        sFinanceiros = '\'[' + nFinanceiroID + ', ' + nValorAplicarGrid + ']\'';

                        if (sSQL != '')
                            sSQL += ' UNION ALL ';

                        sSQL += 'SELECT ' + nFinanceiroGrid + ' AS FinanceiroID, ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroGrid + ', NULL, NULL, NULL, NULL, NULL, NULL, 3), 0) + ' +
                                        'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroGrid + ', NULL, 18, 1, ' + sFinanceiros + ', NULL, ' + nFinanceiroGrid + ', 2), 0) AS Retencao, ' +
                                   'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroGrid + ', NULL, 18, 1, ' + sFinanceiros + ', NULL, ' + nFinanceiroGrid + ', 2), 0) AS PCC, ' +
                                   'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroGrid + ', NULL, NULL, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesGerais, ' +
                                   'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroGrid + ', NULL, 18, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesPCC, ' +
                                   '(SELECT COUNT(1) FROM  Financeiro  a WITH(NOLOCK) ' +
                                        'INNER JOIN Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) ' +
                                        'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) ' +
                                        'INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = c.ProdutoID) ' +
                                        'WHERE a.FinanceiroID = ' + nFinanceiroGrid + ' AND (a.EmpresaID <> 7) AND (d.TipoConceitoID = 303) AND (d.ClassificacaoID = 315)) AS ProdutoServico, ' +
                                   '(SELECT COUNT(1) FROM Financeiro_Ocorrencias WITH(NOLOCK) WHERE FinanceiroID = ' + nFinanceiroGrid + ' AND TipoOcorrenciaID = 1055) AS Pagamentos';
                    }
                }

                glb_nRow = Row;
                break;
            }
        }

        if (sSQL != '') {
            lockControlsInModalWin(true);

            setConnection(dsoRetencoes);
            dsoRetencoes.SQL = sSQL;
            dsoRetencoes.ondatasetcomplete = calculaRetencoesGrid_DSC;
            dsoRetencoes.Refresh();
        }
        else //if (glb_bListar)
        {
            if (Row != undefined)
                fg.TextMatrix(Row, getColIndexByColKey(fg, 'Aplicado*')) = parseFloat(fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Aplicar')));
        }
    }
}

function calculaRetencoes_DSC() {
    var nFinanceiroID = 0;
    var bAtualizaCalculo = false;
    var bAtualizaSaldo = true;
    var nValorAplicar;
    var nValorAcumuladoRetencoes = 0;

    dsoGrid.recordset.setFilter('Indice = 1');

    if ((!dsoGrid.recordset.EOF) && (!dsoGrid.recordset.BOF))
        nValorAcumuladoRetencoes = dsoGrid.recordset['ValorAcumulado'].value;

    for (iRetC = 1; iRetC < fg.Rows; iRetC++) {
        if (glb_nRow != null)
            iRetC = glb_nRow;

        nFinanceiroID = fg.ValueMatrix(iRetC, getColIndexByColKey(fg, 'FinanceiroID*'));

        dsoRetencoes.recordset.setFilter('FinanceiroID = ' + nFinanceiroID);

        if ((!dsoRetencoes.recordset.EOF) && (!dsoRetencoes.recordset.BOF)) {

            glb_ProdutoServico = (dsoRetencoes.recordset['ProdutoServico'].value == '1' ? true : false);

            if (!bAtualizaCalculo) {
                if (dsoRetencoes.recordset['RetencoesPCC'].value <= 0) {
                    if (dsoRetencoes.recordset['PCC'].value > 0) {
                        fg.TextMatrix(iRetC, getColIndexByColKey(fg, 'Retencoes*')) = dsoRetencoes.recordset['Retencao'].value;
                        fg.TextMatrix(iRetC, getColIndexByColKey(fg, 'Aplicado*')) = (parseFloat(fg.ValueMatrix(iRetC, getColIndexByColKey(fg, 'Aplicar'))) - dsoRetencoes.recordset['Retencao'].value);
                        fg.TextMatrix(iRetC, getColIndexByColKey(fg, 'PCC*')) = dsoRetencoes.recordset['PCC'].value;
                        fg.TextMatrix(iRetC, getColIndexByColKey(fg, 'Aplicar')) = fg.ValueMatrix(iRetC, getColIndexByColKey(fg, 'Aplicado*')) + fg.ValueMatrix(iRetC, getColIndexByColKey(fg, 'PCC*'));
                    }
                    else {
                        if (glb_bListar) {
                            nValorAplicar = roundNumber((fg.ValueMatrix(iRetC, getColIndexByColKey(fg, 'Aplicar')) * (1 - (glb_nAliquotaPCC / 100))) - dsoRetencoes.recordset['Retencao'].value, 2);
                            fg.TextMatrix(iRetC, getColIndexByColKey(fg, 'Aplicar')) = nValorAplicar;
                            fg.TextMatrix(iRetC, getColIndexByColKey(fg, 'Aplicado*')) = fg.ValueMatrix(iRetC, getColIndexByColKey(fg, 'Aplicar'));
                        }
                        else {
                            nValorAplicar = roundNumber((fg.ValueMatrix(iRetC, getColIndexByColKey(fg, 'Aplicar')) * (1 - (glb_nAliquotaPCC / 100))) - dsoRetencoes.recordset['Retencao'].value, 2);
                            fg.TextMatrix(iRetC, getColIndexByColKey(fg, 'Aplicar')) = nValorAplicar;
                            fg.TextMatrix(iRetC, getColIndexByColKey(fg, 'PCC*')) = 0;
                            fg.TextMatrix(iRetC, getColIndexByColKey(fg, 'Aplicado*')) = fg.ValueMatrix(iRetC, getColIndexByColKey(fg, 'Aplicar'));
                        }
                    }
                }
                else {
                    fg.TextMatrix(iRetC, getColIndexByColKey(fg, 'Retencoes*')) = dsoRetencoes.recordset['PCC'].value;
                    fg.TextMatrix(iRetC, getColIndexByColKey(fg, 'Aplicado*')) = (parseFloat(fg.ValueMatrix(iRetC, getColIndexByColKey(fg, 'Aplicar'))) - dsoRetencoes.recordset['PCC'].value);
                    fg.TextMatrix(iRetC, getColIndexByColKey(fg, 'PCC*')) = dsoRetencoes.recordset['PCC'].value;
                    fg.TextMatrix(iRetC, getColIndexByColKey(fg, 'Aplicar')) = fg.ValueMatrix(iRetC, getColIndexByColKey(fg, 'Aplicado*')) + fg.ValueMatrix(iRetC, getColIndexByColKey(fg, 'PCC*'));
                }
            }
        }

        dsoRetencoes.recordset.setFilter('');

        if (glb_nRow != null)
            break;
    }

    glb_bListar = false;

    if (bAtualizaCalculo)
        calculaRetencoes();

    lockControlsInModalWin(false);

    glb_nRow = null;
}

function calculaRetencoesGrid_DSC() {
    var nSaldoAtualizado;
    var nValorAplicar;

    for (iCount = 1; iCount < fg.Rows; iCount++) {
        if (glb_nRow != null)
            iCount = glb_nRow;

        nFinanceiroID = fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'FinanceiroID*'));

        dsoRetencoes.recordset.setFilter('FinanceiroID = ' + nFinanceiroID);

        if ((!dsoRetencoes.recordset.EOF) && (!dsoRetencoes.recordset.BOF)) {

            glb_ProdutoServico = (dsoRetencoes.recordset['ProdutoServico'].value == '1' ? true : false);

            if (dsoRetencoes.recordset['RetencoesPCC'].value <= 0) {
                if (dsoRetencoes.recordset['PCC'].value > 0) {
                    fg.TextMatrix(iCount, getColIndexByColKey(fg, 'Retencoes*')) = dsoRetencoes.recordset['Retencao'].value;
                    fg.TextMatrix(iCount, getColIndexByColKey(fg, 'Aplicado*')) = (parseFloat(fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'Aplicar'))) - dsoRetencoes.recordset['Retencao'].value);
                    fg.TextMatrix(iCount, getColIndexByColKey(fg, 'PCC*')) = dsoRetencoes.recordset['PCC'].value;
                    fg.TextMatrix(iCount, getColIndexByColKey(fg, 'Aplicar')) = fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'Aplicado*')) + fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'PCC*'));
                }
                else {
                    if (glb_bListar) {
                        dsoGrid.recordset.setFilter('Indice = 2 AND FinanceiroID = ' + nFinanceiroID);

                        if ((!dsoGrid.recordset.EOF) && (!dsoGrid.recordset.BOF)) {
                            fg.TextMatrix(iCount, getColIndexByColKey(fg, 'SaldoAtualizado*')) = dsoGrid.recordset['SaldoAtualizado'].value;

                            fg.TextMatrix(iCount, getColIndexByColKey(fg, 'SaldoDevedor*')) = dsoGrid.recordset['SaldoDevedor'].value;

                            if (chkSaldo.checked)
                                nSaldoAtualizado = fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'SaldoAtualizado*'));
                            else
                                nSaldoAtualizado = fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'SaldoDevedor*'));

                            if (nSaldoAtualizado > glb_nSaldoAtualizado)
                                fg.TextMatrix(iCount, getColIndexByColKey(fg, 'Aplicar')) = glb_nSaldoAtualizado;
                            else
                                fg.TextMatrix(iCount, getColIndexByColKey(fg, 'Aplicar')) = nSaldoAtualizado;

                            fg.TextMatrix(iCount, getColIndexByColKey(fg, 'Aplicado*')) = parseFloat(fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'Aplicar')));
                        }
                    }
                    else {
                        nValorAplicar = roundNumber((parseFloat(fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'Aplicar'))) * (1 - (glb_nAliquotaPCC / 100))) - fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'RetencoesGerais*')), 2);
                        fg.TextMatrix(iCount, getColIndexByColKey(fg, 'Aplicar')) = nValorAplicar;
                        fg.TextMatrix(iCount, getColIndexByColKey(fg, 'Aplicado*')) = parseFloat(fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'Aplicar')));
                    }

                    dsoGrid.recordset.setFilter('');
                }
            }
            else {
                if (dsoRetencoes.recordset['PCC'].value > 0) {
                    fg.TextMatrix(iCount, getColIndexByColKey(fg, 'Retencoes*')) = dsoRetencoes.recordset['PCC'].value;
                    fg.TextMatrix(iCount, getColIndexByColKey(fg, 'Aplicado*')) = (parseFloat(fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'Aplicar'))) - dsoRetencoes.recordset['PCC'].value);
                    fg.TextMatrix(iCount, getColIndexByColKey(fg, 'PCC*')) = dsoRetencoes.recordset['PCC'].value;
                }
                else {
                    fg.TextMatrix(iCount, getColIndexByColKey(fg, 'Aplicar')) = roundNumber((parseFloat(fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'Aplicar'))) * (1 - (glb_nAliquotaPCC / 100))), 2);
                    fg.TextMatrix(iCount, getColIndexByColKey(fg, 'Aplicado*')) = parseFloat(fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'Aplicar')));
                }
            }
        }
        else {
            fg.TextMatrix(iCount, getColIndexByColKey(fg, 'Aplicado*')) = parseFloat(fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'Aplicar')));
        }

        if (glb_nRow != null)
            break;
    }

    lockControlsInModalWin(false);

    glb_bListar = false;

    glb_nRow = null;
}

function verificaOkRetencao() {
    var bOkRetencao = false;
    var nHistoricoPadraoID = 0;
    var nCont = 0;

    for (i = 1; i < fg.Rows; i++) {
        nHistoricoPadraoID = fg.TextMatrix(i, getColIndexByColKey(fg, 'HistoricoPadraoID*'));

        if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) != '0') && (((glb_ProdutoServico) && (nHistoricoPadraoID == 1163)) || (nHistoricoPadraoID == 3141) || (nHistoricoPadraoID == 3270))) {
            nCont++;

            if (nCont > 1) {
                bOkRetencao = true;
                break;
            }
        }
    }

    return bOkRetencao;
}


function VerificaProdutoServico() {
    //txtFinanceiroPesquisaID.value


    var sql = '';

    sql = " SELECT COUNT(d.ConceitoID) as ProdutoServico " +
            "FROM  Financeiro  a WITH(NOLOCK)  " +
            "INNER JOIN Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) " +
            "INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) " +
            "INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = c.ProdutoID) " +
            "WHERE a.FinanceiroID = " + glb_nFinanceiroID + " AND (a.EmpresaID <> 7) AND (d.TipoConceitoID = 303) AND (d.ClassificacaoID = 315) ";

    setConnection(dsoProdutoServico);
    dsoProdutoServico.SQL = sql;
    dsoProdutoServico.ondatasetcomplete = VerificaProdutoServico_DSC;
    dsoProdutoServico.Refresh();

}
function VerificaProdutoServico_DSC() {


    if ((!dsoProdutoServico.recordset.EOF) && (!dsoProdutoServico.recordset.BOF)) {
        if (dsoProdutoServico.recordset['ProdutoServico'].value > 0) {
            glb_ProdutoServico = true;
        }
    }
}