/********************************************************************
modalgerarlancamentos.js

Library javascript para o modalgerarlancamentos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var glb_nCmbs = 0;
var glb_sResultado = '';
var glb_nTipoCreditoID = 0;
var glb_sTipoCredito = '';
var glb_sEmpresas = '0';
var glb_bEnableDblClick = true;

var dsoGrid = new CDatatransport("dsoGrid");
var dsoGrava = new CDatatransport("dsoGrava");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalgerarlancamentosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalgerarlancamentosDblClick(grid, Row, Col)
js_modalgerarlancamentosKeyPress(KeyAscii)
js_modalgerarlancamentos_ValidateEdit()
js_modalgerarlancamentos_BeforeEdit(grid, row, col)
js_modalgerarlancamentos_AfterEdit(Row, Col)
js_fg_AfterRowColmodalgerarlancamentos (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalgerarlancamentosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	glb_bFirstFill = true;
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Ratear Lan�amento', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.value = 'Ratear';
    
    btnOK.style.left = (modWidth/2) - (btnOK.offsetWidth/2);
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
    btnCanc.style.visibility = 'hidden';

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP; 
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - btnOK.offsetHeight - (ELEM_GAP * 3);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;

	fillGridData();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        gerarLancamentos();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    lockControlsInModalWin(true);
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState(fg.Row);
    
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState(nRow)
{
	var i;
	btnOK.disabled = true;
	
	if (glb_nDireito == 1)
	{
		for (i=2; i<fg.Rows; i++)
		{
			if (getCellValueByColKey(fg, 'Rateado', i) == 0)
				btnOK.disabled = false;
		}
	}
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	var nUsuarioID = getCurrUserID();
	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;
    
    // zera o grid
    fg.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	dsoGrid.SQL = 'EXEC sp_Financeiro_LancamentoRatear ' + glb_nFinanceiroID + ', ' + nUsuarioID + ', 0, NULL, NULL';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
    try
    {
		dsoGrid.Refresh();
	}	
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Filtro inv�lido.') == 0 )
            return null;

		lockControlsInModalWin(false);        
        window.focus();
	}
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

	if (glb_bFirstFill)
	{
		glb_bFirstFill = false;
		showExtFrame(window, true);
		
		window.focus();
	}
	
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
	if ( !((dsoGrid.recordset.BOF) && (dsoGrid.recordset.EOF)) )
	{
	    if ((dsoGrid.recordset['Resultado'].value != null) && (dsoGrid.recordset['Resultado'].value  != ''))
		{
	        if ( window.top.overflyGen.Alert(dsoGrid.recordset['Resultado'].value ) == 0 )
                return null;
		}
	}
	
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '10';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

    headerGrid(fg,['Empresa',
				   '$',
				   'Valor',
				   'Rateado',
				   '$',
				   'Valor'], []);

    fillGridMask(fg,dsoGrid,['EmpresaColigada',
							 'MoedaEmpresa',
							 'ValorEmpresa',
							 'Rateado',
							 'MoedaColigada',
							 'ValorColigada'],
							 ['','','','','',''],
							 ['','','###,###,##0.00','','','###,###,##0.00']);

    // linha de Totais
    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[2,'###,###,##0.00','S']]);

    fg.Redraw = 0;

    alignColsInGrid(fg,[2,5]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
	fg.FrozenCols = 3;

	paintCellsSpecialyReadOnly();
	    
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    fg.Redraw = 2;
    
	window.focus();

	// ajusta estado dos botoes
	setupBtnsFromGridState(fg.Row);
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtFields_onKeyDown()
{
	;
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

function gerarLancamentos()
{
    var nUsuarioID = getCurrUserID();

	lockInterface(true);

	var strPars = '?nFinanceiroID=' + escape(glb_nFinanceiroID);
	strPars += '&nUsuarioID=' + escape(nUsuarioID);

	dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/gerarlancamentos.aspx'+strPars;
	dsoGrava.ondatasetcomplete = gerarLancamentos_DSC;
	dsoGrava.Refresh();
}	

function gerarLancamentos_DSC()
{
    var empresa = getCurrEmpresaData();
    var nLancamentoID = 0;
    //destrava a Interface
	lockInterface(false);

	if ( !((dsoGrava.recordset.BOF) && (dsoGrava.recordset.EOF)) )
	{
	    if ((dsoGrava.recordset['Resultado'].value  != null) && (dsoGrava.recordset['Resultado'].value  != ''))
		{
	        if ( window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value ) == 0 )
                return null;
		}
	    else if ( (dsoGrava.recordset['LancamentoID'].value  != null) &&
			 (dsoGrava.recordset['LancamentoID'].value  != '') )
		{
		    nLancamentoID = dsoGrava.recordset['LancamentoID'].value ;
			var _retConf = window.top.overflyGen.Confirm('Deseja detalhar o lan�amento ' + nLancamentoID + '\ngerado para a Empresa logada?', 1);
			    
			if ( _retConf == 1 )
			{
				sendJSCarrier(getHtmlId(), 'SHOWLANCAMENTO', new Array(empresa[0], nLancamentoID));
				sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
			}
			else
			{
				sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
				return null;
			}
		}
	}
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerarlancamentosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerarlancamentosDblClick(grid, Row, Col)
{
	if (Row != 1)
		return null;
		
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    // trap so colunas editaveis
    if ( getColIndexByColKey(grid, 'OK') != Col)
        return true;
        
    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);
    
    glb_PassedOneInDblClick = true;
    
    for ( i=1; i<grid.Rows; i++ )
    {
		grid.TextMatrix(i, Col) = (glb_bEnableDblClick ? 1 : 0);
		setupBtnsFromGridState(i);
    }
    
    glb_bEnableDblClick = !glb_bEnableDblClick;
    
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    //setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarlancamentosKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarlancamentos_ValidateEdit(grid, nRow, nCol)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarlancamentos_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarlancamentos_AfterEdit(Row, Col)
{
	setupBtnsFromGridState(Row);
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalgerarlancamentos(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return true;

	//setupBtnsFromGridState(NewRow);
}

// FINAL DE EVENTOS DE GRID *****************************************
