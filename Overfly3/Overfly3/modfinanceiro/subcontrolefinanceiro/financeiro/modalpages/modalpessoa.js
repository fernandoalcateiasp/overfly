/********************************************************************
modalpessoa.js

Library javascript para o modalpessoa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES
var dsoPesq = new CDatatransport("dsoPesq");
/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modalpessoaBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtPessoa').disabled == false )
        txtPessoa.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Pessoa', 1);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    
    // ajusta o divPessoa
    elem = window.document.getElementById('divPessoa');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;
        
    }

    adjustElementsInForm([['lblEhCliente', 'selEhCliente', 12, 1, -10, -10],
                          ['lblParceiro', 'chkParceiro', 3, 1],
                          ['lblChavePesquisa', 'selChavePesquisa', 12, 1, -10],
                          ['lblPessoa', 'txtPessoa', 25, 1, -3]],
                          null,null,true);

    // btnFindPesquisa
    btnFindPesquisa.disabled = true;
    with (btnFindPesquisa.style)
    {
        top = parseInt(txtPessoa.style.top, 10);
        left = parseInt(txtPessoa.style.left, 10) + parseInt(txtPessoa.style.width, 10) + 2;
        width = 80;
        height = 24;
    }

    selEhCliente.onchange = selEhCliente_onChange;    
    
    var aOptions = [[2,'Pessoa'],
					[1,'Cliente'],
                    [0,'Fornecedor']];
    var i;
    
    for (i=0; i<=2; i++)
    {
        var oOption = document.createElement("OPTION");
        oOption.value = aOptions[i][0];
        oOption.text = aOptions[i][1];
        selEhCliente.add(oOption);
    }

    var aOptions2 = [[0,'Nome'],
                     [1,'Fantasia'],
                     [2,'Documento']];

    for (i=0; i<=2; i++)
    {
        var oOption = document.createElement("OPTION");
        oOption.value = aOptions2[i][0];
        oOption.text = aOptions2[i][1];
        selChavePesquisa.add(oOption);
    }

    selEhCliente.selectedIndex = 0;
    chkParceiro.style.visibility = 'inherit';
    lblParceiro.style.visibility = 'inherit';
        
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divPessoa').style.top) + parseInt(document.getElementById('divPessoa').style.height) + ELEM_GAP;
        width = temp + 221;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);

    headerGrid(fg,['Nome',
                   'ID',
                   'Fantasia',
                   'Documento',
                   'Cidade',
                   'UF',
                   'Pa�s',
                   'Parceiro'], [2, 7]);
    
    fg.Redraw = 2;
    
    txtPessoa.onkeypress = txtPessoa_onKeyPress;
}

function txtPessoa_onKeyPress()
{
    if ( event.keyCode == 13 )
        btnFindPesquisa_onclick();
}

function txtPessoa_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    txtPessoa.value = trimStr(txtPessoa.value);
    
    changeBtnState(txtPessoa.value);

    if (btnFindPesquisa.disabled)
        return;
    
    startPesq(txtPessoa.value);
}

function fg_DblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S' , new Array(
                      fg.TextMatrix(fg.Row, 2), fg.TextMatrix(fg.Row, 1), selEhCliente.value, 
                      fg.TextMatrix(fg.Row, 7)));
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null );    

}

function startPesq(strPesquisa)
{
    lockControlsInModalWin(true);
    var nEhCliente = selEhCliente.value;
    var strSQL = '';
    var strSQL2 = '';
    var strSQLSelect = '';
    var strSQLFrom = '';
    var strSQLWhere = '';
    var strSQLSelect2 = '';
    var strSQLFrom2 = '';
    var strSQLWhere2 = '';
    var strSQLOrder = '';

	var sChavePesquisa = 'Pessoas.Nome';
	var sPesquisa = ' AND Pessoas.Nome';

	if (selChavePesquisa.value == 1)
	{
		sChavePesquisa = 'Pessoas.Fantasia';
		sPesquisa = ' AND Pessoas.Fantasia';
	}
	else if (selChavePesquisa.value == 2)
	{
		sPesquisa = 'AND Documentos.Numero';
	}
    
    // Pessoa
    if ( nEhCliente == 2 )
	{
        strSQLSelect = 'SELECT TOP 100 ' + sChavePesquisa + ' AS fldName, Pessoas.PessoaID AS fldID, Pessoas.Fantasia AS Fantasia, Documentos.Numero AS Numero, ' +
	        'Cidades.Localidade AS Cidade, UFs.CodigoLocalidade2 AS UF, Paises.CodigoLocalidade2 AS Pais, ' +
            '3 AS Parceiro ';

        strSQLFrom = 'FROM Pessoas WITH (NOLOCK) ' +
            'INNER JOIN Pessoas_Documentos Documentos WITH (NOLOCK) ON (Pessoas.PessoaID = Documentos.PessoaID) ' +
            'INNER JOIN Pessoas_Enderecos Enderecos WITH (NOLOCK) ON (Pessoas.PessoaID = Enderecos.PessoaID) ' +
            'LEFT OUTER JOIN Localidades Cidades WITH (NOLOCK) ON (Enderecos.CidadeID = Cidades.LocalidadeID) ' +
            'LEFT OUTER JOIN Localidades UFs WITH (NOLOCK) ON (Enderecos.UFID = UFs.LocalidadeID) ' +
            'LEFT OUTER JOIN Localidades Paises WITH (NOLOCK) ON (Enderecos.PaisID = Paises.LocalidadeID) ';
	            
        strSQLWhere = 'WHERE (Pessoas.EstadoID = 2 AND ' +
	        '((Documentos.TipoDocumentoID = 101 OR Documentos.TipoDocumentoID = 111)) AND ' +
	        '(Enderecos.Ordem = 1)) ' +
	        sPesquisa + ' >= ' + '\'' + strPesquisa + '\'' + ' ';
	}
    // Clientes
    else if ( nEhCliente == 1 )
    {                
        strSQLSelect = 'SELECT TOP 100 ' + sChavePesquisa + ' AS fldName, Pessoas.PessoaID AS fldID, Pessoas.Fantasia AS Fantasia, Documentos.Numero AS Numero, ' +
	        'Cidades.Localidade AS Cidade, UFs.CodigoLocalidade2 AS UF, Paises.CodigoLocalidade2 AS Pais, ' +
            '(CASE WHEN (SELECT COUNT (Pessoas1.PessoaID) ' +
                'FROM RelacoesPessoas RelPessoas1 WITH (NOLOCK), RelacoesPessoas RelPessoas2 WITH (NOLOCK), Pessoas Pessoas1 WITH (NOLOCK) ' +
                'WHERE (Pessoas.PessoaID = RelPessoas1.SujeitoID AND RelPessoas1.TipoRelacaoID = 21 AND ' +
			        'RelPessoas1.EstadoID = 2 AND RelPessoas1.ObjetoID = RelPessoas2.SujeitoID AND ' +
			        'RelPessoas2.TipoRelacaoID = 21 AND RelPessoas2.EstadoID = 2 AND RelPessoas2.ObjetoID = ' + glb_nEmpresaID + ' AND ' +
			        'RelPessoas2.SujeitoID = Pessoas1.PessoaID AND Pessoas1.EstadoID = 2) ) >0 THEN 2 ELSE 1 END ) AS Parceiro ';

        strSQLFrom = 'FROM RelacoesPessoas RelPessoas WITH (NOLOCK) ' +
            'INNER JOIN Pessoas WITH (NOLOCK) ON (RelPessoas.SujeitoID = Pessoas.PessoaID) ' +
            'INNER JOIN Pessoas_Documentos Documentos WITH (NOLOCK) ON (Pessoas.PessoaID = Documentos.PessoaID) ' +
            'INNER JOIN Pessoas_Enderecos Enderecos WITH (NOLOCK) ON (Pessoas.PessoaID = Enderecos.PessoaID) ' +
            'LEFT OUTER JOIN Localidades Cidades WITH (NOLOCK) ON (Enderecos.CidadeID = Cidades.LocalidadeID) ' +
            'LEFT OUTER JOIN Localidades UFs WITH (NOLOCK) ON (Enderecos.UFID = UFs.LocalidadeID) ' +
            'LEFT OUTER JOIN Localidades Paises WITH (NOLOCK) ON (Enderecos.PaisID = Paises.LocalidadeID) ';
	            
        strSQLWhere = 'WHERE (RelPessoas.ObjetoID = ' + glb_nEmpresaID + ' AND RelPessoas.TipoRelacaoID = 21 AND RelPessoas.EstadoID = 2 AND ' +
	        'Pessoas.EstadoID = 2 AND ' +
	        '((Documentos.TipoDocumentoID = 101 OR Documentos.TipoDocumentoID = 111)) AND (Enderecos.Ordem = 1)) ' +
	        sPesquisa + ' >= ' + '\'' + strPesquisa + '\'' + ' ';

        // Parceiros e Usuarios
        if ( !chkParceiro.checked )
        {
            strSQLSelect2 = 'UNION ALL SELECT DISTINCT TOP 100 ' + sChavePesquisa + ' AS fldName, Pessoas.PessoaID AS fldID, Pessoas.Fantasia AS Fantasia, Documentos.Numero AS Numero, ' +
	            'Cidades.Localidade AS Cidade, UFs.CodigoLocalidade2 AS UF, Paises.CodigoLocalidade2 AS Pais, 0 AS Parceiro ';

            strSQLFrom2 = 'FROM RelacoesPessoas RelParceiros WITH (NOLOCK) ' +
                'INNER JOIN Pessoas Parceiros WITH (NOLOCK) ON (RelParceiros.SujeitoID = Parceiros.PessoaID) ' +
                'INNER JOIN RelacoesPessoas RelPessoas WITH (NOLOCK) ON (Parceiros.PessoaID = RelPessoas.ObjetoID) ' +
                'INNER JOIN Pessoas WITH (NOLOCK) ON (RelPessoas.SujeitoID = Pessoas.PessoaID) ' +
                'INNER JOIN Pessoas_Documentos Documentos WITH (NOLOCK) ON (Pessoas.PessoaID = Documentos.PessoaID) ' +
                'INNER JOIN Pessoas_Enderecos Enderecos WITH (NOLOCK) ON (Pessoas.PessoaID = Enderecos.PessoaID) ' +
                'LEFT OUTER JOIN Localidades Cidades WITH (NOLOCK) ON (Enderecos.CidadeID = Cidades.LocalidadeID) ' +
                'LEFT OUTER JOIN Localidades UFs WITH (NOLOCK) ON (Enderecos.UFID = UFs.LocalidadeID) ' +
                'LEFT OUTER JOIN Localidades Paises WITH (NOLOCK) ON (Enderecos.PaisID = Paises.LocalidadeID) ';
	                    
            strSQLWhere2 = 'WHERE (RelParceiros.ObjetoID = ' + glb_nEmpresaID + ' AND RelParceiros.TipoRelacaoID = 21 AND ' +
                'RelParceiros.EstadoID = 2 AND Parceiros.EstadoID = 2 AND ' +
                'RelPessoas.TipoRelacaoID = 21 AND RelPessoas.EstadoID = 2 AND ' +
	            'Pessoas.EstadoID = 2 AND ' +
	            '((Documentos.TipoDocumentoID = 101 OR Documentos.TipoDocumentoID = 111)) AND (Enderecos.Ordem = 1)) AND ' +
                'Pessoas.PessoaID NOT IN (SELECT SujeitoID FROM RelacoesPessoas WHERE ObjetoID= ' + glb_nEmpresaID + ' AND TipoRelacaoID=21 AND EstadoID=2) ' +
	            sPesquisa + ' >= ' + '\'' + strPesquisa + '\'' + ' ';

         }             
    }
    // Fornecedores
    else if ( nEhCliente == 0 )
    {
        strSQLSelect = 'SELECT TOP 100 ' + sChavePesquisa + ' AS fldName, Pessoas.PessoaID AS fldID, Pessoas.Fantasia AS Fantasia, Documentos.Numero AS Numero, ' +
	        'Cidades.Localidade AS Cidade, UFs.CodigoLocalidade2 AS UF, Paises.CodigoLocalidade2 AS Pais, ' +
            '1 AS Parceiro ' ;

        strSQLFrom = 'FROM RelacoesPessoas RelPessoas WITH (NOLOCK) ' +
            'INNER JOIN Pessoas WITH (NOLOCK) ON (RelPessoas.ObjetoID = Pessoas.PessoaID) ' +
            'INNER JOIN Pessoas_Documentos Documentos WITH (NOLOCK) ON (Pessoas.PessoaID = Documentos.PessoaID) ' +
            'INNER JOIN Pessoas_Enderecos Enderecos WITH (NOLOCK) ON (Pessoas.PessoaID = Enderecos.PessoaID) ' +
            'LEFT OUTER JOIN Localidades Cidades WITH (NOLOCK) ON (Enderecos.CidadeID = Cidades.LocalidadeID) ' +
            'LEFT OUTER JOIN Localidades UFs WITH (NOLOCK) ON (Enderecos.UFID = UFs.LocalidadeID) ' +
            'LEFT OUTER JOIN Localidades Paises WITH (NOLOCK) ON (Enderecos.PaisID = Paises.LocalidadeID) ';
	            
        strSQLWhere = 'WHERE (RelPessoas.SujeitoID = ' + glb_nEmpresaID + ' AND (RelPessoas.TipoRelacaoID BETWEEN 21 AND 22) AND RelPessoas.EstadoID = 2 AND ' +
	        'Pessoas.EstadoID = 2 AND ' +
	        '((Documentos.TipoDocumentoID = 101 OR Documentos.TipoDocumentoID = 111)) AND (Enderecos.Ordem = 1)) ' +
	        sPesquisa + ' >= ' + '\'' + strPesquisa + '\'' + ' ';
    }

    strSQLOrder =  'ORDER BY fldName';

    strSQL = strSQLSelect + strSQLFrom + strSQLWhere;

    strSQL2 = strSQLSelect2 + strSQLFrom2 + strSQLWhere2;

    strSQL += strSQL2 + strSQLOrder;
    
    setConnection(dsoPesq);
    dsoPesq.SQL = strSQL;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC()
{
	var sChavePesquisa = 'Nome';
	if (selChavePesquisa.value == 1)
		sChavePesquisa = 'Fantasia';

    startGridInterface(fg);
    fg.FrozenCols = 0;

    headerGrid(fg,[sChavePesquisa,
                   'ID',
                   'Fantasia',
                   'Documento',
                   'Cidade',
                   'UF',
                   'Pa�s',
                   'Parceiro'], [2, 7]);

    fillGridMask(fg,dsoPesq,['fldName',
                             'fldID',
                             'Fantasia',
                             'Numero',
                             'Cidade',
                             'UF',
                             'Pais',
                             'Parceiro'],['','','','','','','','']);
    fg.FrozenCols = 1;

    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
    }    
    else
    {
        btnOK.disabled = true;    
        txtPessoa.focus();
    }    
}

function fg_KeyPress(KeyAscii)
{
    // Enter
    if ( (KeyAscii == 13) && (fg.Row > 0) )
    {
        btn_onclick(btnOK);
    }
}

// Funcao disparada no on change do combo selEhCliente
function selEhCliente_onChange ()
{
    if (this.value == 0)
    {
        chkParceiro.style.visibility = 'hidden';
        lblParceiro.style.visibility = 'hidden';
    }
    else
    {
        chkParceiro.style.visibility = 'inherit';
        lblParceiro.style.visibility = 'inherit';
    }
    fg.Rows = 1;
    btnOK.disabled = true;
}
