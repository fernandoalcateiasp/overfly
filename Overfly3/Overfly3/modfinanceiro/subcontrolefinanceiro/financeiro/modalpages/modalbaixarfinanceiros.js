/********************************************************************
modalbaixarfinanceiros.js

Library javascript para o modalbaixarfinanceiros.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;

var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalbaixarfinanceirosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalbaixarfinanceirosDblClick(grid, Row, Col)
js_modalbaixarfinanceirosKeyPress(KeyAscii)
js_modalbaixarfinanceiros_ValidateEdit()
js_modalbaixarfinanceiros_BeforeEdit(grid, row, col)
js_modalbaixarfinanceiros_AfterEdit(Row, Col)
js_fg_AfterRowColmodalbaixarfinanceiros (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalbaixarfinanceirosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    glb_bFirstFill = true;
    // mostra a modal
    //refreshParamsAndDataAndShowModalWin(true);

    showExtFrame(window, true);
    window.focus();

    if (!txtValor.disabled)
        txtValor.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Baixar Financeiros', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblDesconto', 'chkDesconto', 3, 1, -10, -10],
						  ['lblGravaValor', 'chkGravaValor', 3, 1],
					      ['lblValor', 'txtValor', 10, 1],
						  ['lblDataInicio', 'txtDataInicio', 10, 1],
						  ['lblDataFim', 'txtDataFim', 10, 1],
						  ['lblFiltro', 'txtFiltro', 31, 1]], null, null, true);

    txtValor.onkeypress = verifyNumericEnterNotLinked;
    txtValor.setAttribute('verifyNumPaste', 1);
    txtValor.setAttribute('thePrecision', 11, 1);
    txtValor.setAttribute('theScale', 2, 1);
    txtValor.setAttribute('minMax', new Array(0, 9999999999), 1);
    txtValor.onfocus = selFieldContent;
    txtValor.maxLength = 10;

    txtDataInicio.onfocus = selFieldContent;
    txtDataInicio.maxLength = 10;
    txtDataInicio.onkeypress = txtDataInicioFiltro_onKeyPress;
    txtDataInicio.value = glb_dCurrDate;

    txtDataFim.onfocus = selFieldContent;
    txtDataFim.maxLength = 10;
    txtDataFim.onkeypress = txtDataInicioFiltro_onKeyPress;
    txtDataFim.value = glb_dCurrDate;

    txtFiltro.onfocus = selFieldContent;
    txtFiltro.maxLength = 1024;
    txtFiltro.onkeypress = txtDataInicioFiltro_onKeyPress;

    adjustLabelsCombos();

    // Ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        btnFillGrid.offsetTop + btnFillGrid.offsetHeight + 2;
        width = txtDataFim.offsetLeft + txtDataFim.offsetWidth + ELEM_GAP;
        height = txtDataFim.offsetTop + txtDataFim.offsetHeight;
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // Reposiciona botao OK
    with (btnOK) {
        style.top = divControls.offsetTop + txtDataFim.offsetTop;
        style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
        value = 'Baixar';
        title = 'Baixar Financeiros';
    }

    with (btnFillGrid) {
        style.width = btnOK.currentStyle.width;
        style.height = btnOK.currentStyle.height;
        style.top = btnOK.currentStyle.top;
        style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnFillGrid.currentStyle.width, 10) - 4;
    }

    // Esconde botao cancel
    with (btnCanc) {
        style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtDataInicioFiltro_onKeyPress() {
    if (event.keyCode == 13) {
        fg.Rows = 1;
        // ajusta estado dos botoes
        setupBtnsFromGridState();
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        saveDataInGrid();
    }
    else if (controlID == 'btnFillGrid') {
        listar();
    }
        // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    lockControlsInModalWin(true);

    // @@ atualizar interface aqui
    setupBtnsFromGridState();

    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState() {
    var i;
    var bHasRowsInGrid = fg.Rows > 1;

    btnOK.disabled = true;

    if (bHasRowsInGrid) {
        for (i = 1; i < fg.Rows; i++) {
            if (getCellValueByColKey(fg, 'OK', i) != 0) {
                btnOK.disabled = false;
                break;
            }
        }
    }
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    var aGrid = null;
    var i = 0;
    var sDataInicio = trimStr(txtDataInicio.value);
    var sDataFim = trimStr(txtDataFim.value);
    var sFiltro = '';
    var sFiltroData = '';
    var sFiltroValor = '';
    var nValor = txtValor.value;

    lockControlsInModalWin(true);

    glb_nDSOs = 1;

    // zera o grid
    fg.Rows = 1;

    sDataInicio = normalizeDate_DateTime(sDataInicio, 1);
    sDataFim = normalizeDate_DateTime(sDataFim, 1);

    if (trimStr(txtFiltro.value) != '')
        sFiltro = ' AND ' + trimStr(txtFiltro.value) + ' ';

    sFiltroData = ' AND a.dtVencimento BETWEEN ' + '\'' + sDataInicio + '\'' + ' AND ' + '\'' + sDataFim + '\'' + ' ';

    if (chkDesconto.checked) {
        sFiltroValor = ' AND dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) > 0 AND dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) <= ' + nValor + ' ';
    }
    else
        sFiltroValor = ' AND dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) = 0 AND ' +
			'dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) <= ' + nValor + ' ';

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT a.FinanceiroID AS FinanceiroID, f.RecursoAbreviado AS Estado, CONVERT(BIT, 0) AS OK, ' +
			'a.PedidoID, a.Duplicata, d.Fantasia AS Pessoa, b.ItemAbreviado AS FormaPagamento, ' +
			'a.dtEmissao AS dtEmissao, a.PrazoPagamento, a.dtVencimento AS dtVencimento, c.SimboloMoeda AS Moeda, ' +
			'a.Valor AS Valor, dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, ' +
			'dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, a.TaxaAtualizacao, ' +
			(chkDesconto.checked ? 'NULL' : 'dbo.fn_Financeiro_TaxaAtualizacao(a.FinanceiroID, 3, GETDATE())') + ' AS TaxaCalculada, ' +
			'e.Fantasia AS Colaborador ' +
		'FROM Financeiro a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Pessoas e WITH(NOLOCK), Recursos f WITH(NOLOCK) ' +
		'WHERE (a.EstadoID IN (41,47) AND a.EmpresaID = ' + glb_aEmpresaData[0] + ' AND a.TipoFinanceiroID = ' + glb_nTipoFinanceiroID + ' AND ' +
			'a.FormaPagamentoID = b.ItemID AND a.MoedaID = c.ConceitoID AND ' +
			'a.PessoaID = d.PessoaID AND a.ProprietarioID = e.PessoaID AND a.EstadoID = f.RecursoID ' +
			sFiltroData + sFiltroValor + sFiltro + ') ' +
		'ORDER BY a.FinanceiroID';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;

    try {
        dsoGrid.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
        txtDataFim.focus();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;

    if (glb_nDSOs > 0)
        return null;

    if (glb_bFirstFill) {
        glb_bFirstFill = false;
    }

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    var aHoldCols = new Array();

    if (chkDesconto.checked)
        aHoldCols = [15];
    else
        aHoldCols = [];

    headerGrid(fg, ['Financeiro',
				   'Est',
				   'OK',
				   'Pedido',
				   'Duplicata',
				   'Pessoa',
				   'Forma',
				   'Emiss�o',
				   'Prazo',
				   'Vencimento',
				   '$',
				   'Valor',
				   'Saldo Devedor',
				   'Saldo Atual',
				   'Taxa Atual',
				   'Taxa Calc',
				   'Colaborador'], aHoldCols);

    fillGridMask(fg, dsoGrid, ['FinanceiroID*',
							 'Estado*',
							 'OK',
							 'PedidoID*',
							 'Duplicata*',
							 'Pessoa*',
							 'FormaPagamento*',
							 'dtEmissao*',
							 'PrazoPagamento*',
							 'dtVencimento*',
							 'Moeda*',
							 'Valor*',
							 'SaldoDevedor*',
							 'SaldoAtualizado*',
							 'TaxaAtualizacao*',
							 'TaxaCalculada*',
						     'Colaborador*'],
							 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
							 ['', '', '', '', '', '', '', dTFormat, '', dTFormat, '', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '']);
    fg.Redraw = 0;

    alignColsInGrid(fg, [0, 3, 8, 11, 12, 13, 14, 15]);

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[11, '###,###,###,###.00', 'S'],
                                                          [12, '###,###,###,###.00', 'S'],
                                                          [13, '###,###,###,###.00', 'S']]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    paintCellsSpecialyReadOnly();

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        js_modalbaixarfinanceiros_AfterEdit(2, getColIndexByColKey(fg, 'OK'));
        fg.Editable = true;
        window.focus();
        fg.focus();
    }
    else {
        ;
    }

    fg.Redraw = 2;
    fg.FrozenCols = 5;

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    lockControlsInModalWin(true);

    for (i = 2; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0) {
            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                    strPars = '';
                    nDataLen = 0;
                }

                strPars += '?nUsuarioID=' + escape(glb_nUserID);
                strPars += '&bDesconto=' + escape(chkDesconto.checked ? 1 : 0);
                strPars += '&bGravaValor=' + escape(chkGravaValor.checked ? 1 : 0);
            }

            nDataLen++;
            strPars += '&nFinanceiroID=' + escape(getCellValueByColKey(fg, 'FinanceiroID*', i));
        }
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/baixarfinanceiros.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            lockControlsInModalWin(false);
            glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

function adjustLabelsCombos() {
    ;
}
function listar() {
    if (!verificaData())
        return null;

    if ((trimStr(txtValor.value) == '') || (isNaN(trimStr(txtValor.value)))) {
        if (window.top.overflyGen.Alert('Valor inv�lido.') == 0)
            return null;

        window.focus();
        txtValor.focus();

        return null;
    }

    refreshParamsAndDataAndShowModalWin(false);
}

function verificaData() {
    var sData = trimStr(txtDataInicio.value);
    var bDataIsValid = true;
    var sData2 = trimStr(txtDataFim.value);
    var bDataIsValid2 = true;

    if (sData != '')
        bDataIsValid = chkDataEx(txtDataInicio.value);

    if (sData2 != '')
        bDataIsValid2 = chkDataEx(txtDataFim.value);

    if (bDataIsValid != true) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        window.focus();
        txtDataInicio.focus();

        return false;
    }

    if (bDataIsValid2 != true) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        window.focus();
        txtDataFim.focus();

        return false;
    }

    return true;
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    lockControlsInModalWin(false);

    glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly() {
    paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalbaixarfinanceirosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalbaixarfinanceirosDblClick(grid, Row, Col) {
    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if (getColIndexByColKey(grid, 'OK') != Col)
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);

    glb_PassedOneInDblClick = true;

    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 1; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    js_modalbaixarfinanceiros_AfterEdit(fg.Row, fg.Col);

    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalbaixarfinanceirosKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalbaixarfinanceiros_ValidateEdit() {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalbaixarfinanceiros_BeforeEdit(grid, row, col) {
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if (fldDet == null)   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ((fldDet[0] == 200) || (fldDet[0] == 129))  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalbaixarfinanceiros_AfterEdit(Row, Col) {
    var nColValor = getColIndexByColKey(fg, 'OK');

    if (Col == nColValor) {
        var nTotalRows = gridTotais('OK');
        fg.TextMatrix(1, getColIndexByColKey(fg, 'FinanceiroID*')) = 'Totais';
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Estado*')) = nTotalRows;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor*')) = gridTotais('Valor*');
        fg.TextMatrix(1, getColIndexByColKey(fg, 'SaldoDevedor*')) = gridTotais('SaldoDevedor*');
        fg.TextMatrix(1, getColIndexByColKey(fg, 'SaldoAtualizado*')) = gridTotais('SaldoAtualizado*');

        btnOK.disabled = (nTotalRows == 0);
    }

    setupBtnsFromGridState();
}

function gridTotais(sColKeyTotal) {
    var retVal = 0;
    var i = 0;

    if (fg.Rows < 2)
        return 0;

    for (i = 2; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
            if (sColKeyTotal == 'OK')
                retVal++;
            else
                retVal += fg.ValueMatrix(i, getColIndexByColKey(fg, sColKeyTotal));
        }
    }

    return retVal;
}


/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalbaixarfinanceiros(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return true;

    setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************
