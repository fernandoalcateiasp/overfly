<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="financeirosup01Html" name="financeirosup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/financeiro/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="financeirosup01Body" name="financeirosup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->

    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" name="txtEstadoID" class="fldGeneral">        
        <p id="lblSituacaoCobrancaID" name="lblSituacaoCobrancaID" class="lblGeneral">Sit</p>
        <select id="selSituacaoCobrancaID" name="selSituacaoCobrancaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="SituacaoCobrancaID"></select>
        <p id="lblPedido" name="lblPedido" class="lblGeneral">Pedido</p>
        <input type="text" id="txtPedido" name="txtPedido" DATASRC="#dsoSup01" DATAFLD="PedidoID" class="fldGeneral">
        <p id="lblDuplicata" name="lblDuplicata" class="lblGeneral">Duplicata</p>
        <input type="text" id="txtDuplicata" name="txtDuplicata" DATASRC="#dsoSup01" DATAFLD="Duplicata" class="fldGeneral">
        <p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">Pessoa</p>
        <select id="selPessoaID" name="selPessoaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PessoaID"></select>
        <input type="image" id="btnFindPessoa" name="btnFindPessoa" class="fldGeneral" title="Preencher Combo">
        <p id="lblParceiroID" name="lblParceiroID" class="lblGeneral">Parceiro</p>
        <select id="selParceiroID" name="selParceiroID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ParceiroID"></select>
        <p id="lbldtEmissao" name="lbldtEmissao" class="lblGeneral">Emiss�o</p>
        <input type="text" id="txtdtEmissao" name="txtdtEmissao" DATASRC="#dsoSup01" DATAFLD="V_dtEmissao" class="fldGeneral">
        <p id="lblFormaPagamentoID" name="lblFormaPagamentoID" class="lblGeneral">Forma</p>
        <select id="selFormaPagamentoID" name="selFormaPagamentoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FormaPagamentoID"></select>
        <p id="lblPrazoPagamento" name="lblPrazoPagamento" class="lblGeneral">Prazo</p>
        <input type="text" id="txtPrazoPagamento" name="txtPrazoPagamento" DATASRC="#dsoSup01" DATAFLD="PrazoPagamento" class="fldGeneral">
        <p id="lbldtVencimento" name="lbldtVencimento" class="lblGeneral">Vencimento</p>
        <input type="text" id="txtdtVencimento" name="txtdtVencimento" DATASRC="#dsoSup01" DATAFLD="V_dtVencimento" class="fldGeneral">
        <p id="lblDiasCompensacao" name="lblDiasCompensacao" class="lblGeneral">D+n</p>
        <input type="text" id="txtDiasCompensacao" name="txtDiasCompensacao" DATASRC="#dsoSup01" DATAFLD="DiasCompensacao" class="fldGeneral">
        <p id="lbldtFluxoCaixa" name="lbldtFluxoCaixa" class="lblGeneral">Fluxo de Caixa</p>
        <input type="text" id="txtdtFluxoCaixa" name="txtdtFluxoCaixa" DATASRC="#dsoSup01" DATAFLD="V_dtFluxoCaixa" class="fldGeneral">
        <p id="lblEhEstorno" name="lblEhEstorno" class="lblGeneral">Est</p>
        <input type="checkbox" id="chkEhEstorno" name="chkEhEstorno" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EhEstorno" title="� Estorno?">
        <p id="lblEhImporte" name="lblEhImporte" class="lblGeneral">Imp</p>
        <input type="checkbox" id="chkEhImporte" name="chkEhImporte" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EhImporte" title="� Importe?">
        <p id="lblHistoricoPadraoID" name="lblHistoricoPadraoID" class="lblGeneral">Hist�rico Padr�o</p>
        <select id="selHistoricoPadraoID" name="selHistoricoPadraoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="HistoricoPadraoID"></select>
        <p id="lblImpostoID" name="lblImpostoID" class="lblGeneral">Imposto</p>
        <select id="selImpostoID" name="selImpostoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ImpostoID"></select>
        <p id="lblMarketing" name="lblMarketing" class="lblGeneral">Mkt</p>
        <input type="checkbox" id="chkMarketing" name="chkMarketing" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Marketing" title="Baixa a��o de marketing?">
        <p id="lblMoedaID" name="lblMoedaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaID" name="selMoedaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="MoedaID"></select>
        <p id="lblValor" name="lblValor" class="lblGeneral">Valor</p>
        <input type="text" id="txtValor" name="txtValor" DATASRC="#dsoSup01" DATAFLD="Valor" class="fldGeneral">
        <p id="lblTaxaMoeda" name="lblTaxaMoeda" class="lblGeneral">Taxa</p>
        <input type="text" id="txtTaxaMoeda" name="txtTaxaMoeda" DATASRC="#dsoSup01" DATAFLD="TaxaMoeda" class="fldGeneral">
        <p id="lblValorConvertido" name="lblValorConvertido" class="lblGeneral">Valor Convertido</p>
        <input type="text" id="txtValorConvertido" name="txtValorConvertido" DATASRC="#dsoSup01" DATAFLD="ValorConvertido" class="fldGeneral">
        <p id="lblMoedaOcorrenciaID" name="lblMoedaOcorrenciaID" class="lblGeneral">Moeda Oc</p>
        <select id="selMoedaOcorrenciaID" name="selMoedaOcorrenciaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="MoedaOcorrenciaID" title="Moeda das Ocorr�ncias"></select>
        <p id="lblTaxaAtualizacao" name="lblTaxaAtualizacao" class="lblGeneral">Atualiza��o</p>
        <input type="text" id="txtTaxaAtualizacao" name="txtTaxaAtualizacao" DATASRC="#dsoSup01" DATAFLD="TaxaAtualizacao" class="fldGeneral" title="Taxa de Atualiza��o (%)">
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <p id="lblGuiaRecolhimento" name="lblGuiaRecolhimento" class="lblGeneral">Guia Rec</p>
        <!--<input type="text" id="txtGuiaRecolhimento" name="txtGuiaRecolhimento" DATASRC="#dsoSup01" DATAFLD="GuiaRec" class="fldGeneral" title="Guia de Recolhimento">-->
        <select id="selGuiaRecolhimento" name="selGuiaRecolhimento" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="GuiaRecolhimentoID"></select>
        <p id="lblCodigoArrecadacao" name="lblCodigoArrecadacao" class="lblGeneral">C�digo Arrec</p>
        <input type="text" id="txtCodigoArrecadacao" name="txtCodigoArrecadacao" DATASRC="#dsoSup01" DATAFLD="CodigoArrecadacao" class="fldGeneral" title="C�digo de Arrecada��o">
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral">
        <p id="lblNossoNumero" name="lblNossoNumero" class="lblGeneral">Nosso N�mero</p>
        <input type="text" id="txtNossoNumero" name="txtNossoNumero" DATASRC="#dsoSup01" DATAFLD="NossoNumero" class="fldGeneral">
        <p id="lblRelPesContaID" name="lblRelPesContaID" class="lblGeneral">Banco</p>
        <select id="selRelPesContaID" name="selRelPesContaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="RelPesContaID"></select>
        <p id="lblCarteira" name="lblCarteira" class="lblGeneral">Cart</p>
        <input type="text" id="txtCarteira" name="txtCarteira" DATASRC="#dsoSup01" DATAFLD="Carteira" class="fldGeneral" title="Carteira de cobran�a">
        <p id="lblModalidadeID" name="lblModalidadeID" class="lblGeneral">Mod</p>
        <select id="selModalidadeID" name="selModalidadeID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ModalidadeID" title="Modalidade de cobran�a"></select>
        <p id="lblComandoID" name="lblComandoID" class="lblGeneral">Com</p>
        <select id="selComandoID" name="selComandoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ComandoID" title="Comando de cobran�a"></select>
        <p id="lblInstrucao1" name="lblInstrucao1" class="lblGeneral">Inst1</p>
        <input type="text" id="txtInstrucao1" name="txtInstrucao1" DATASRC="#dsoSup01" DATAFLD="Instrucao1" class="fldGeneral" title="Instru��o 1">
        <p id="lblInstrucao2" name="lblInstrucao2" class="lblGeneral">Inst2</p>
        <input type="text" id="txtInstrucao2" name="txtInstrucao2" DATASRC="#dsoSup01" DATAFLD="Instrucao2" class="fldGeneral" title="Instru��o 2">
        <p id="lblCodigoBarra" name="lblCodigoBarra" class="lblGeneral">C�digo de Barra</p>
        <input type="text" id="txtCodigoBarra" name="txtCodigoBarra" DATASRC="#dsoSup01" DATAFLD="CodigoBarra" class="fldGeneral">
        <p id="lblTotalPrincipal" name="lblTotalPrincipal" class="lblGeneral">Total Principal</p>
        <input type="text" id="txtTotalPrincipal" name="txtTotalPrincipal" class="fldGeneral" title="Total do Principal">
        <p id="lblTotalEncargos" name="lblTotalEncargos" class="lblGeneral">Total Encargos</p>
        <input type="text" id="txtTotalEncargos" name="txtTotalEncargos" class="fldGeneral" title="Total dos Encargos Financeiros">
        <p id="lblTotalDespesas" name="lblTotalDespesas" class="lblGeneral">Total Despesas</p>
        <input type="text" id="txtTotalDespesas" name="txtTotalDespesas" class="fldGeneral" title="Total das Despesas Financeiras">
        <p id="lblTotalAbatimentos" name="lblTotalAbatimentos" class="lblGeneral">Total Abatimentos</p>
        <input type="text" id="txtTotalAbatimentos" name="txtTotalAbatimentos" class="fldGeneral" title="Total dos Abatimentos">
        <p id="lblTotalAmortizacoes" name="lblTotalAmortizacoes" class="lblGeneral">Total Amortiza��es</p>
        <input type="text" id="txtTotalAmortizacoes" name="txtTotalAmortizacoes" class="fldGeneral" title="Total das Amortiza��es e Liquida��es">
        <p id="lblSaldoDevedor" name="lblSaldoDevedor" class="lblGeneral">Saldo Devedor</p>
        <input type="text" id="txtSaldoDevedor" name="txtSaldoDevedor" class="fldGeneral" title="Valor do Saldo Devedor">
        <p id="lblSaldoAtualizado" name="lblSaldoAtualizado" class="lblGeneral">Saldo Atualizado</p>
        <input type="text" id="txtSaldoAtualizado" name="txtSaldoAtualizado" class="fldGeneral" title="Valor do Saldo Devedor Atualizado">
        <p id="lblPercAmortizacao" name="lblPercAmortizacao" class="lblGeneral">Perc</p>
        <input type="text" id="txtPercAmortizacao" name="txtPercAmortizacao" class="fldGeneral" title="Percentual de Amortiza��o">

    </div>
    
</body>

</html>
