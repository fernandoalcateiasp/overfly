/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form financeiro
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoListData01 = new CDatatransport('dsoListData01');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoPropsPL = new CDatatransport('dsoPropsPL');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Sit', 'Pedido', 'Duplicata', 'Pessoa', 'Forma',
                                 'Prazo', 'Vencimento', '$', 'Valor', 'Saldo Devedor', 'Saldo Atualizado', 'Hist�rico Padr�o', 'Observa��o', 'CorObservacao');
                                 
    //@@ Formatacao das colunas do grid de pesquisa, definicao opcional
    // padrao do framework = null se variavel nao for definida aqui
    var dTFormat = '';
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '','', '', dTFormat, '','###,###,###.00','###,###,###.00','###,###,###.00', '', '', '');
                                                                    
    windowOnLoad_2ndPart();
    //AjusteFiltros();
}
/*
function AjusteFiltros() {

    adjustElementsInForm([['lblRefrInf', 'chkRefrInf', 3, 1],
                              ['lblOrdem', 'chkOrdem', 3, 1, -9],
                              ['lblProprietariosPL', 'selProprietariosPL', 15, 1, -9],
                              ['btnGetProps', 'btn', 20, 1],
                              ['lblRegistros', 'selRegistros', 6, 1, -3],
                              ['lblMetodoPesq', 'selMetodoPesq', 9, 1, -9],
                              ['lblPesquisa', 'selPesquisa', 14, 1, -4],
                              ['lblArgumento', 'txtArgumento', 12, 1, -4],
                              ['lblFiltro', 'txtFiltro', 15, 1, -6]]);

    with (btnGuias) {
        style.top = divSup01_01.offsetTop + txtArgumento.offsetTop;
        style.width = 40;
        style.left = 680;
    }

    with (btnCobranca) {
        style.top = divSup01_01.offsetTop + txtArgumento.offsetTop;
        style.width = 60;
        style.left = 720;
    }
}
*/
/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
	/*showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1,1,1]);
	tipsBtnsEspecControlBar('sup', ['Relat�rios', 'Procedimento', 'Liquidar Financeiros', 'Gerar Financeiros', 
		'Ativar Financeiros', 'Gerar Valor a Localizar', 'Fluxo de caixa', 'Ocorr�ncias']);
    */

	showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1,1,1,1,1,1,1,1]);
	tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Liquidar Financeiros', 'Gerar Financeiros',
		'Ativar Financeiros', 'Gerar Valor a Localizar', 'Fluxo de caixa', 'Ocorr�ncias', 'Guias', 'Cobran�a', 'Demonstrativo de reten��es', 'Servi�os Banc�rios']);

	fg.ColHidden(fg.Cols-1) = true;
	
	glb_GridIsBuilding = true;
    
    lastRow = fg.Row;
    
    FillStyle = 1;
	for ( i=1; i<fg.Rows; i++  )
	{
		// Limite Observa��o
		if (fg.TextMatrix(i, fg.Cols-1) != '')
			fg.Cell(6, i, getColIndexByColKey(fg, 'Observacao') , i, getColIndexByColKey(fg, 'Observacao') ) = eval(fg.TextMatrix(i, fg.Cols-1));
	}
    FillStyle = 0;

    setupEspecBtnsControlBar('sup', 'HHHHHHHHHHHHH');
    alignColsInGrid(fg,[0,3,7,10,11,12]);
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	var contexto = 0;
	var sAnchor;
	contexto = getCmbCurrDataInControlBar('sup', 1);

    if ( controlBar == 'SUP' )
    {
        // usuario clicou botao documentos
        if (btnClicked == 1) {
            if (fg.Rows > 1) {
                __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
            }
            else {
                window.top.overflyGen.Alert('Selecione um registro.');
            }
        }

        // usuario clicou botao imprimir
        else if (btnClicked == 2)
            openModalPrint();

        // Procedimento
        else if (btnClicked == 3) {
            if (contexto[1] == 9111)
                sAnchor = '211';
            else
                sAnchor = '212';

            window.top.openModalControleDocumento('PL', '', 910, null, sAnchor, 'T');
        }

        // Baixar Financeiros
        else if (btnClicked == 4)
            openModalBaixarFinanceiro();

        // Gerar Financeiros
        else if (btnClicked == 5)
            openModalGerarFinanceiros();

        // Ativar Financeiro
        else if (btnClicked == 6)
            openModalAtivarFinanceiro();

        // Gerar Valores a Localizar  
        else if (btnClicked == 7)
            openModalGerarValoresLocalizar();

        // Fluxo Caixa    
        else if (btnClicked == 8)
            openModalFluxoCaixa();

        // Ocorrencias
        else if (btnClicked == 9)
            openModalOcorrencias();

        // Guias
        else if (btnClicked == 10)
            openModalGuias();

        // Cobranca
        else if (btnClicked == 11)
            openModalCobranca();

        // Servicos Bancarios
        else if (btnClicked == 13)
            openModalServicosBancarios();
    }    
}

function openModalGuias() {
	htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalgerargaregnre.asp';
	showModalWin(htmlPath, new Array(800, 490));
}

function openModalCobranca() {
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalcobrancaduplicatas.asp';
    showModalWin(htmlPath, new Array(995, 535));
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Modal de impressao
    if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALGERAVALORLOCALIZARHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    // Modal de fluxo de caixa
    else if ( idElement.toUpperCase() == 'MODALFLUXOCAIXAHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    // Modal Baixar Financeiros
    else if ( idElement.toUpperCase() == 'MODALBAIXARFINANCEIROSHTML' )
    {
        if ( param1 == 'OK' )                
        {
    		// esta funcao fecha a janela modal e destrava a interface
			restoreInterfaceFromModal();    
			// escreve na barra de status
			writeInStatusBar('child', 'cellMode', 'Detalhe');
            
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            
            // nao mexer
            return 0;
        }    
    }

    // Modal Ativar Financeiros
    else if ( idElement.toUpperCase() == 'MODALATIVARFINANCEIROSHTML' )
    {
        if ( param1 == 'OK' )                
        {
    		// esta funcao fecha a janela modal e destrava a interface
			restoreInterfaceFromModal();    
			// escreve na barra de status
			writeInStatusBar('child', 'cellMode', 'Detalhe');
            
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            
            // nao mexer
            return 0;
        }    
    }
    else if ( idElement.toUpperCase() == 'MODALGERARFINANCEIROSHTML' )
    {
        if ( param1 == 'OK' )                
        {
    		// esta funcao fecha a janela modal e destrava a interface
			restoreInterfaceFromModal();    
			// escreve na barra de status
			writeInStatusBar('child', 'cellMode', 'Detalhe');
            
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            
            // nao mexer
            return 0;
        }    
    }
    else if ( idElement.toUpperCase() == 'MODALOCORRENCIASHTML' )
    {
        if ( param1 == 'OK' )                
        {
    		// esta funcao fecha a janela modal e destrava a interface
			restoreInterfaceFromModal();    
			// escreve na barra de status
			writeInStatusBar('child', 'cellMode', 'Detalhe');
            
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            
            // nao mexer
            return 0;
        }    
    }
    // Modal gerar GARE GNRE
    else if (idElement.toUpperCase() == 'MODALGERARGAREGNREHTML') {
    	if (param1 == 'OK') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    	else if (param1 == 'CANCEL') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    }
    // Modal cobranca de duplicatas
    else if (idElement.toUpperCase() == 'MODALCOBRANCADUPLICATASHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal Servicos Bancarios
    else if (idElement.toUpperCase() == 'MODALSERVICOSBANCARIOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            if (param2 != null)
                detalharFinanceiro(param2);
            
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    // por questoes esteticas, coloca foco no grid de pesquisa
    if ( fg.disabled == false )
        fg.focus();
    
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // Lib a linkar ao asp: modalprint_financeiro.js
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);
    strPars += '&currDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalprint.asp'+strPars;
    
    showModalWin(htmlPath, new Array(583,444));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de ocorrencias

Parametro:
btnClicked

Retorno:
nenhum
********************************************************************/
function openModalOcorrencias(btnClicked)
{
    var htmlPath;
	var aEmpresa = getCurrEmpresaData();
    var strPars = new String();
    var nWidth = 772;
    var nHeight = 462;
	var nDireitoFinanceiro = 0;

	var nDireitoFinanceiroA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
			    ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')') );

	var nDireitoFinanceiroA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
			    ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')') );

	if ( (nDireitoFinanceiroA1 == 1) && (nDireitoFinanceiroA2 == 1) )
		nDireitoFinanceiro = 1;

	var nDireitoA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
			    ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B9A1' + '\'' + ')') );

	var nDireitoA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
			    ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B9A2' + '\'' + ')') );

    //DireitoEspecificoDetalhar
    //Financeiro->Recebimento/Pagamento->Modal Ocorrencias (B8)
    //28000 SFS-Grupo Financeiro-> A1&&A2 -> Permite edi��o dos campos: 'txtValor','txtCarteira'
    //B8A2 = 1 -> Carrena no combo Vendedores todos os vendedores e equipes de todas as empresas dos perfis do usuario logado caso o perfil tenha direito A1 ou A2 no B8.
    //                No contrario, carrega apenas o usuario logado (com a op��o select, selecionado) caso o perfil seja A1 ou A2 no B8.
    //B8A1&&B8A2 -> habilita excel
    //B8A2=0 -> Desabilita Combo Vendedor
    //(B8A1 = 0 || B8A2 = 0) && hora entre 12:00&&14:00 -> N�o preenche o grid
    //selAgruparPor1=Ocorrencia && B8A1 && B8A2 && (chkRecebido.checked) && (selComissao.value == 3) -> Habilita edi��o do grid e mostra coluna OK
    //selAgruparPor1=Ocorrencia && (A1&&A2) && (B8A1) && (B8A2) && (chkRecebido.checked) && (selComissao.value == 2)) -> Habilita edi��o do grid e mostra coluna OK
    //selAgruparPor1=Ocorrencia && B8A1 && B8A2 && (chkRecebido.checked) && (selComissao.value == 3) -> Preenche a coluna FinOcorrenciaID do grid
    //selAgruparPor1=Ocorrencia && (A1&&A2) && (B8A1) && (B8A2) && (chkRecebido.checked) && (selComissao.value == 2)) -> Preenche a coluna FinOcorrenciaID do grid
    //(selAgruparPor1.value > 0) && selEmpresa>0 && ((B8A2) || selVendedor>0) -> habilita botao List
    //selAgruparPor1=Ocorrencia && B8A1 && B8A2 && (chkRecebido.checked) && (selComissao.value == 3) -> Mostra bot�o Pagar
    //selAgruparPor1=Ocorrencia && (A1&&A2) && B8A1 && B8A2 && (chkRecebido.checked) && (selComissao.value == 2) -> Mostra bot�o Pagar
    //selAgruparPor1=Ocorrencia && B8A1 && B8A2 && (chkRecebido.checked) && (selComissao.value == 3) -> quando preciona o botao Pagar, verifica se tem algum campo da coluna ok igual 1
    //selAgruparPor1=Ocorrencia && (A1&&A2) && B8A1 && B8A2 && (chkRecebido.checked) && (selComissao.value == 2) -> quando preciona o botao Pagar, verifica se tem algum campo da coluna ok igual 1
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
    strPars += '&nUserID=' + escape(getCurrUserID());
    strPars += '&nDireitoFinanceiro=' + escape(nDireitoFinanceiro);
    strPars += '&nDireitoA1=' + escape(nDireitoA1);
    strPars += '&nDireitoA2=' + escape(nDireitoA2);
    strPars += '&nModalType=' + escape(btnClicked);
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalocorrencias.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth,nHeight));
}


function openModalFluxoCaixa()
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 772;
    var nHeight = 462;
	var aEmpresa = getCurrEmpresaData();
    
    strPars = '?nUserID=' + escape(getCurrUserID());
    strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars += '&sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalfluxocaixa.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth,nHeight));
}


function openModalGerarFinanceiros()
{
    var htmlPath;
	var aEmpresa = getCurrEmpresaData();
    var strPars = new String();

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
	strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
	strPars += '&nEmpresaPaisID=' + escape(aEmpresa[1]);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalgerarfinanceiros.asp' + strPars;
    showModalWin(htmlPath, new Array(775,460));
}

function openModalBaixarFinanceiro()
{
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = 0;
    var nA1 = getCurrRightValue('SUP','A1');
    var nA2 = getCurrRightValue('SUP','A2');
    
    //DireitoEspecifico
    //Financeiro->Recebimentos/Pagamentos->Modal Baixar Financeiros
    //28000 SFS-Grupo Financeiro-> A1&&A2 -> Permite abrir modal.
    if ((nA1 == 1) && (nA2 == 1))
    {    
		if ( ((typeof(aContextoID)).toUpperCase() == 'OBJECT') && (aContextoID.length >= 2) )
			nContextoID = aContextoID[1];

		var htmlPath;
		var strPars = new String();

		strPars = '?sCaller=' + escape('PL');
		strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
		strPars += '&nContextoID=' + escape(nContextoID);

		// carregar modal - faz operacao de banco no carregamento
		htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalbaixarfinanceiros.asp' + strPars;
		showModalWin(htmlPath, new Array(775,460));
    }
}

function openModalAtivarFinanceiro()
{
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = 0;
    
    if ( ((typeof(aContextoID)).toUpperCase() == 'OBJECT') && (aContextoID.length >= 2) )
        nContextoID = aContextoID[1];

    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nContextoID=' + escape(nContextoID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalativarfinanceiros.asp' + strPars;
    showModalWin(htmlPath, new Array(775,460));
}

/********************************************************************
Criado pelo programador
Abre a JM de Gerar Valores a Localizar

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalGerarValoresLocalizar()
{
    var htmlPath;
    var strPars = new String();

    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = contexto[1];       

	// Pagamentos
	if (nContextoID == 9111)
		nTipoFinanceiroID = 1001;
	else
		nTipoFinanceiroID = 1002;

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nTipoValorID=' + escape(nTipoFinanceiroID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalgeravalorlocalizar.asp'+strPars;
    showModalWin(htmlPath, new Array(775,460));
}

/********************************************************************
Criado pelo programador
Abre a JM Servicos Bancarios

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalServicosBancarios()
{
    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = 0;

    if (((typeof (aContextoID)).toUpperCase() == 'OBJECT') && (aContextoID.length >= 2))
        nContextoID = aContextoID[1];

    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nContextoID=' + escape(nContextoID);
    strPars += '&nEmpresaID=' + escape(nEmpresaID);

    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/modalpages/modalservicosbancarios.asp'+strPars;
    showModalWin(htmlPath, new Array(820, 520));
}

/********************************************************************
Funcao do programador, detalha um financeiro

Parametro:
ID do financeiro a detalhar

Retorno:
a string ou null se nao tem
********************************************************************/
function detalharFinanceiro(financeiroID) {
    lockInterface(true);

    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWDETAIL',
                  [financeiroID, null, null]);
}