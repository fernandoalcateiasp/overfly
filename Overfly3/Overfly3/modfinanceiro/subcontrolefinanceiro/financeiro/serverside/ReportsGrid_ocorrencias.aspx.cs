﻿using System;
using System.Web;
using System.Data;

namespace Overfly3.PrintJet
{
    public partial class ReportsGrid_ocorrencias : System.Web.UI.OverflyPage
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;

        private int RelatorioID = Convert.ToInt32(HttpContext.Current.Request.Params["RelatorioID"]);
        private int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["formato"]);
        private int lista_excel = Convert.ToInt32(HttpContext.Current.Request.Params["lista_excel"]);
        private int sLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["sLinguaLogada"]);
        private string glb_sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);

        int Datateste = 0;
        bool nulo = false;

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "1":
                        relatorioOcorrencias();
                        break;
                }
            }
        }

        public void relatorioOcorrencias()
        {
            //Parametros da procedure
            string sFiltro = Convert.ToString(HttpContext.Current.Request.Params["sFiltro"]);
            string sDataInicio = Convert.ToString(HttpContext.Current.Request.Params["sDataInicio"]);
            string sDataFim = Convert.ToString(HttpContext.Current.Request.Params["sDataFim"]);
            string bRecebido = Convert.ToString(HttpContext.Current.Request.Params["bRecebido"]);
            string nTipoComissao = Convert.ToString(HttpContext.Current.Request.Params["nTipoComissao"]);
            string bMoedaSistema = Convert.ToString(HttpContext.Current.Request.Params["bMoedaSistema"]);
            string bDatas = Convert.ToString(HttpContext.Current.Request.Params["bDatas"]);
            string bProdutos = Convert.ToString(HttpContext.Current.Request.Params["bProdutos"]);
            string bLocalidades = Convert.ToString(HttpContext.Current.Request.Params["bLocalidades"]);
            string sEmpresas = Convert.ToString(HttpContext.Current.Request.Params["sEmpresas"]);
            string sEquipes = Convert.ToString(HttpContext.Current.Request.Params["sEquipes"]);
            string sVendedores = Convert.ToString(HttpContext.Current.Request.Params["sVendedores"]);
            string sFamilias = Convert.ToString(HttpContext.Current.Request.Params["sFamilias"]);
            string sMarcas = Convert.ToString(HttpContext.Current.Request.Params["sMarcas"]);
            string sGroupByID1 = Convert.ToString(HttpContext.Current.Request.Params["sGroupByID1"]);
            string sGroupBy1 = Convert.ToString(HttpContext.Current.Request.Params["sGroupBy1"]);
            string sOrderBy1 = Convert.ToString(HttpContext.Current.Request.Params["sOrderBy1"]);
            string sGroupByID2 = Convert.ToString(HttpContext.Current.Request.Params["sGroupByID2"]);
            string sGroupBy2 = Convert.ToString(HttpContext.Current.Request.Params["sGroupBy2"]);
            string sOrderBy2 = Convert.ToString(HttpContext.Current.Request.Params["sOrderBy2"]);
            string sGroupByID3 = Convert.ToString(HttpContext.Current.Request.Params["sGroupByID3"]);
            string sGroupBy3 = Convert.ToString(HttpContext.Current.Request.Params["sGroupBy3"]);
            string sOrderBy3 = Convert.ToString(HttpContext.Current.Request.Params["sOrderBy3"]);
            string sGroupByID4 = Convert.ToString(HttpContext.Current.Request.Params["sGroupByID4"]);
            string sGroupBy4 = Convert.ToString(HttpContext.Current.Request.Params["sGroupBy4"]);
            string sOrderBy4 = Convert.ToString(HttpContext.Current.Request.Params["sOrderBy4"]);
            string selAgruparPor1 = Convert.ToString(HttpContext.Current.Request.Params["selAgruparPor1"]);
            string selOrdem1 = Convert.ToString(HttpContext.Current.Request.Params["selOrdem1"]);
            string selAgruparPor2 = Convert.ToString(HttpContext.Current.Request.Params["selAgruparPor2"]);
            string selOrdem2 = Convert.ToString(HttpContext.Current.Request.Params["selOrdem2"]);
            string selAgruparPor3 = Convert.ToString(HttpContext.Current.Request.Params["selAgruparPor3"]);
            string selOrdem3 = Convert.ToString(HttpContext.Current.Request.Params["selOrdem3"]);
            string selAgruparPor4 = Convert.ToString(HttpContext.Current.Request.Params["selAgruparPor4"]);
            string selOrdem4 = Convert.ToString(HttpContext.Current.Request.Params["selOrdem4"]);
            bool bNivelX = Convert.ToBoolean(HttpContext.Current.Request.Params["bNivelX"]);
            string selOrdemX = Convert.ToString(HttpContext.Current.Request.Params["selOrdemX"]);


            if (!bNivelX)
            {
                if (selAgruparPor1 != "0" && selAgruparPor1 != "")
                {
                    if ((selOrdem1 == "0") && (sOrderBy1 == ""))
                    {
                        sOrderBy1 = "NULL";
                        // Ocorrencia
                        if (selAgruparPor1 == "21")
                            sOrderBy1 = "FinOcorrenciaID";
                    }
                    else if (selOrdem1 == "1")
                        sOrderBy1 = " '(SELECT COUNT(DISTINCT aa.FinOcorrenciaID) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + ")) DESC' ";
                    else if (selOrdem1 == "2")
                        sOrderBy1 = " '(SELECT SUM(aa.ValorOcorrencia) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + ")) DESC' ";
                    else if (selOrdem1 == "3")
                        sOrderBy1 = " '(SELECT SUM(aa.FaturamentoRecebido) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + ")) DESC' ";
                    else if (selOrdem1 == "4")
                        sOrderBy1 = " '(SELECT SUM(aa.ContribuicaoRecebida) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + ")) DESC' ";
                }

                if (selAgruparPor2 != "0" && selAgruparPor2 != "")
                {
                    if ((selOrdem2 == "0") && (sOrderBy2 == ""))
                        sOrderBy2 = "NULL";
                    else if (selOrdem2 == "1")
                        sOrderBy2 = " '(SELECT COUNT(DISTINCT aa.FinOcorrenciaID) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + " AND aa." + sGroupByID2 + " = #TempTable." + sGroupByID2 + ")) DESC' ";
                    else if (selOrdem2 == "2")
                        sOrderBy2 = " '(SELECT SUM(aa.ValorOcorrencia) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + " AND aa." + sGroupByID2 + " = #TempTable." + sGroupByID2 + ")) DESC' ";
                    else if (selOrdem2 == "3")
                        sOrderBy2 = " '(SELECT SUM(aa.FaturamentoRecebido) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + " AND aa." + sGroupByID2 + " = #TempTable." + sGroupByID2 + ")) DESC' ";
                    else if (selOrdem2 == "4")
                        sOrderBy2 = " '(SELECT SUM(aa.ContribuicaoRecebida) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + " AND aa." + sGroupByID2 + " = #TempTable." + sGroupByID2 + ")) DESC' ";
                }

                if (selAgruparPor3 != "0" && selAgruparPor3 != "")
                {
                    if ((selOrdem3 == "0") && (sOrderBy3 == ""))
                        sOrderBy3 = "NULL";
                    else if (selOrdem3 == "1")
                        sOrderBy3 = " '(SELECT COUNT(DISTINCT aa.FinOcorrenciaID) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + " AND aa." + sGroupByID2 + " = #TempTable." + sGroupByID2 + " AND aa." + sGroupByID3 + " = #TempTable." + sGroupByID3 + ")) DESC' ";
                    else if (selOrdem3 == "2")
                        sOrderBy3 = " '(SELECT SUM(aa.ValorOcorrencia) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + " AND aa." + sGroupByID2 + " = #TempTable." + sGroupByID2 + " AND aa." + sGroupByID3 + " = #TempTable." + sGroupByID3 + ")) DESC' ";
                    else if (selOrdem3 == "3")
                        sOrderBy3 = " '(SELECT SUM(aa.FaturamentoRecebido) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + " AND aa." + sGroupByID2 + " = #TempTable." + sGroupByID2 + " AND aa." + sGroupByID3 + " = #TempTable." + sGroupByID3 + ")) DESC' ";
                    else if (selOrdem3 == "4")
                        sOrderBy3 = " '(SELECT SUM(aa.ContribuicaoRecebida) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + " AND aa." + sGroupByID2 + " = #TempTable." + sGroupByID2 + " AND aa." + sGroupByID3 + " = #TempTable." + sGroupByID3 + ")) DESC' ";
                }

                if (selAgruparPor4 != "0" && selAgruparPor4 != "")
                {
                    if ((selOrdem4 == "0") && (sOrderBy4 == ""))
                        sOrderBy4 = "NULL";
                    else if (selOrdem4 == "1")
                        sOrderBy4 = " '(SELECT COUNT(DISTINCT aa.FinOcorrenciaID) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + " AND aa." + sGroupByID2 + " = #TempTable." + sGroupByID2 + " AND aa." + sGroupByID3 + " = #TempTable." + sGroupByID3 + " AND aa." + sGroupByID4 + " = #TempTable." + sGroupByID4 + ")) DESC' ";
                    else if (selOrdem4 == "2")
                        sOrderBy4 = " '(SELECT SUM(aa.ValorOcorrencia) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + " AND aa." + sGroupByID2 + " = #TempTable." + sGroupByID2 + " AND aa." + sGroupByID3 + " = #TempTable." + sGroupByID3 + " AND aa." + sGroupByID4 + " = #TempTable." + sGroupByID4 + ")) DESC' ";
                    else if (selOrdem4 == "3")
                        sOrderBy4 = " '(SELECT SUM(aa.FaturamentoRecebido) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + " AND aa." + sGroupByID2 + " = #TempTable." + sGroupByID2 + " AND aa." + sGroupByID3 + " = #TempTable." + sGroupByID3 + " AND aa." + sGroupByID4 + " = #TempTable." + sGroupByID4 + ")) DESC' ";
                    else if (selOrdem4 == "4")
                        sOrderBy4 = " '(SELECT SUM(aa.ContribuicaoRecebida) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + " AND aa." + sGroupByID2 + " = #TempTable." + sGroupByID2 + " AND aa." + sGroupByID3 + " = #TempTable." + sGroupByID3 + " AND aa." + sGroupByID4 + " = #TempTable." + sGroupByID4 + ")) DESC' ";
                }

                if (selAgruparPor1 != "0" && selAgruparPor1 != "")
                {
                    sGroupByID1 = "'" + sGroupByID1 + "'";
                    sGroupBy1 = "'" + sGroupBy1 + "'";
                }
                if (selAgruparPor2 != "0" && selAgruparPor2 != "")
                {
                    sGroupByID2 = "'" + sGroupByID2 + "'";
                    sGroupBy2 = "'" + sGroupBy2 + "'";
                }
                if (selAgruparPor3 != "0" && selAgruparPor3 != "")
                {
                    sGroupByID3 = "'" + sGroupByID3 + "'";
                    sGroupBy3 = "'" + sGroupBy3 + "'";
                }
                if (selAgruparPor4 != "0" && selAgruparPor4 != "")
                {
                    sGroupByID4 = "'" + sGroupByID4 + "'";
                    sGroupBy4 = "'" + sGroupBy4 + "'";
                }
            }
            else
            {
                if ((selOrdemX == "0") && (sOrderBy1 == ""))
                    sOrderBy1 = "NULL";
                else if (selOrdemX == "1")
                    sOrderBy1 = " '(SELECT COUNT(DISTINCT aa.FinOcorrenciaID) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + ")) DESC' ";
                else if (selOrdemX == "2")
                    sOrderBy1 = " '(SELECT SUM(aa.ValorOcorrencia) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + ")) DESC' ";
                else if (selOrdemX == "3")
                    sOrderBy1 = " '(SELECT SUM(aa.FaturamentoRecebido) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + ")) DESC' ";
                else if (selOrdemX == "4")
                    sOrderBy1 = " '(SELECT SUM(aa.ContribuicaoRecebida) FROM #TempTable aa WHERE (aa." + sGroupByID1 + " = #TempTable." + sGroupByID1 + ")) DESC' ";

                sGroupByID1 = "'" + sGroupByID1 + "'";
                sGroupBy1 = "'" + sGroupBy1 + "'";
            }

            if (sGroupByID1 == "'FinOcorrenciaID'")
            {
                sGroupByID1 = "NULL";
                sGroupBy1 = "NULL";
            }
            else if (sGroupByID2 == "'FinOcorrenciaID'")
            {
                sGroupByID2 = "NULL";
                sGroupBy2 = "NULL";
            }
            else if (sGroupByID3 == "'FinOcorrenciaID'")
            {
                sGroupByID3 = "NULL";
                sGroupBy3 = "NULL";
            }
            else if (sGroupByID4 == "'FinOcorrenciaID'")
            {
                sGroupByID4 = "NULL";
                sGroupBy4 = "NULL";
            }

            //Define formato de data de acordo com a lingua logada
            string param = "dd/MM/yyy";
            if (sLinguaLogada != 246)
                param = "MM/dd/yyy";

            //Recebe data atual para colocar no titulo do Excel
            DateTime DataHoje = DateTime.Now;
            string _data = DataHoje.ToString(param);
            
            string Title = "Ocorrências_" + glb_sEmpresaFantasia + "_" + _data;

            string strSQL = " DECLARE @Filtro VARCHAR(8000) " +
                            " SELECT @Filtro = " + sFiltro + " " +
                            " EXEC sp_Financeiro_Ocorrencias " + sDataInicio + ", " + sDataFim + ", " + bRecebido + ", " + nTipoComissao + ", " + bMoedaSistema + ", " +
                            bDatas + ", " + bProdutos + ", " + bLocalidades + ", " + sEmpresas + ", " + sEquipes + ", " + sVendedores + ", " + sFamilias + ", " + sMarcas + ", " +
                            sGroupByID1 + ", " + sGroupBy1 + ", " + sOrderBy1 + ", " +
                            sGroupByID2 + ", " + sGroupBy2 + ", " + sOrderBy2 + ", " +
                            sGroupByID3 + ", " + sGroupBy3 + ", " + sOrderBy3 + ", " +
                            sGroupByID4 + ", " + sGroupBy4 + ", " + sOrderBy4 + ", @Filtro";
            
            //Se Lista btnListar
            if (lista_excel == 0)
            {
                //Cria DataSet com retorno da procedure
                DataSet dsLista = DataInterfaceObj.getRemoteData(strSQL);

                //Escreve XML com o DataSet
                WriteResultXML(dsLista);
            }

            //SE EXCEL btnExcel
            else if (lista_excel == 1)
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");


                DataSet dsFin = new DataSet();
                dsFin = (DataSet)HttpContext.Current.Session["Query"];
                Datateste = dsFin.Tables.Count;

                //Verifica se a Query está vazia e retorna mensagem de erro
                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    return;
                }
                else
                {
                    if (Formato == 1) //Se formato PDF
                    {
                        ;
                    }

                    else if (Formato == 2) //Se formato Excel
                    {
                        Relatorio.CriarObjTabela("0.00", "0.00", "Query1", false, true, true);
                        Relatorio.TabelaEnd();
                    }
                    Relatorio.CriarPDF_Excel(Title, Formato);
                }
            }
        }
    }
}