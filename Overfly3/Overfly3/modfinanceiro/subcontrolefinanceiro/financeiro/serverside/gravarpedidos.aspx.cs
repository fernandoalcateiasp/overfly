﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class gravarpedidos : System.Web.UI.OverflyPage
    {
        string Mensagem = "";
        private Integer DataLen;

        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }
        private Integer[] UserID;

        public Integer[] nUserID
        {
            get { return UserID; }
            set { UserID = value; }
        }
        private Integer[] FinanceiroID;

        public Integer[] nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value; }
        }
        private string AD;

        public string bAD
        {
            get { return AD; }
            set { AD = value; }
        }
        private Integer[] PedIteCampanhaID;

        public Integer[] nPedIteCampanhaID
        {
            get { return PedIteCampanhaID; }
            set { PedIteCampanhaID = value; }
        }
        private string[] Valor;

        public string[] nValor
        {
            get { return Valor; }
            set { Valor = value; }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {

            for (int i = 0; i < DataLen.intValue(); i++)
            {


                ProcedureParameters[] procParams = new ProcedureParameters[6];

                procParams[0] = new ProcedureParameters(
                "@FinanceiroID",
                System.Data.SqlDbType.Int,
                FinanceiroID.Length == 0 ? DBNull.Value : (Object)Convert.ToInt32(FinanceiroID[i]));

                procParams[1] = new ProcedureParameters(
                "@PedIteCampanhaID",
                System.Data.SqlDbType.Int,
                PedIteCampanhaID.Length == 0 ? DBNull.Value : (Object)Convert.ToInt32(PedIteCampanhaID[i]));

                procParams[2] = new ProcedureParameters(
                "@Valor",
                System.Data.SqlDbType.Money,
                Valor[i] == null ? DBNull.Value : (Object)Convert.ToDouble(Valor[i].ToString()));

                procParams[3] = new ProcedureParameters(
                "@Dissociar",
                System.Data.SqlDbType.Int,
                AD.ToString() == "0" ? true : false);

                procParams[4] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                UserID.Length == 0 ? DBNull.Value : (Object)Convert.ToInt32(UserID[i]));

                procParams[5] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
                procParams[5].Length = 8000;


                DataInterfaceObj.execNonQueryProcedure("sp_Financeiro_PedidoItemCampanha", procParams);

                if (procParams[5].Data != DBNull.Value)
                    Mensagem += procParams[5].Data.ToString();
            }

            WriteResultXML(DataInterfaceObj.getRemoteData(
                  "select '" + Mensagem + "' as Resultado "));

        }
    }
}
