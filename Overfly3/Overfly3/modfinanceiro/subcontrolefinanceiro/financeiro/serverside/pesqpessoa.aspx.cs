﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class pesqpessoa : System.Web.UI.OverflyPage
    {
        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private string StrToFind;

        public string strToFind
        {
            get { return StrToFind; }
            set { StrToFind = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            string SQL = " ";
            SQL = "SELECT TOP 100 b.Nome AS fldName,b.PessoaID AS fldID,b.Fantasia AS Fantasia, " +
             "dbo.fn_Pessoa_Documento(b.PessoaID, NULL, NULL, 101, 111, 0) AS Documento, f.Localidade " +
             "AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " +
             "FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " +
             "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (b.PessoaID = e.PessoaID AND (e.Ordem=1)) " +
             "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " +
             "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " +
             "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " +
             "WHERE a.ObjetoID = " + EmpresaID + " AND a.TipoRelacaoID = 21 " +
             "AND a.EstadoID = 2 AND a.SujeitoID = b.PessoaID " +
             "AND b.EstadoID = 2 " +
             "AND b.Nome >= '" + StrToFind + "' " +
             "ORDER BY fldName ";

            WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
        }
    }
}