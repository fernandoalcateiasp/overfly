﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class updatenossonumero : System.Web.UI.OverflyPage
    {

        private Integer FinanceiroID;

        public Integer nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value; }
        }
        private string NossoNumero;

        public string sNossoNumero
        {
            get { return NossoNumero; }
            set { NossoNumero = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            int RecsAffected = 0;
            string sql = "";

            sql = "UPDATE Financeiro SET NossoNumero = '" + NossoNumero + "' " +
                    "WHERE FinanceiroID = " + (FinanceiroID);

            RecsAffected = DataInterfaceObj.ExecuteSQLCommand(sql);

            WriteResultXML(DataInterfaceObj.getRemoteData(
              "select '" + RecsAffected + "' as fldresp "));
        }
    }
}