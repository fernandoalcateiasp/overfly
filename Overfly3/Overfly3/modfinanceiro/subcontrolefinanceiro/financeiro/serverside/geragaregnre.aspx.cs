using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
	public partial class geragaregnre: System.Web.UI.OverflyPage
	{
        /*
         * Financeiro.FinanceiroID AS Financeiro,
         * Financeiro.Observacao,
         * Financeiro.PessoaID,
         * Pessoa.Fantasia AS Pessoa,
         * Localidade.CodigoLocalidade2 AS UF, 
         * Financeiro.PedidoID,
         * Financeiro.Valor, 
         * SPACE(0) AS OK
         */
        String sMessage;
        String sNomeArquivo;
        String SQL;
        int nTipoResultado;
        bool bVerifica;
        /*
        private string PedidosID;
		protected string sPedidosID
		{
			get { return PedidosID; }
			set { PedidosID = value; }
		}
        */
        /*
        private string Financeiro;
        protected string sFinanceiro
        {
            get { return Financeiro; }
            set { sFinanceiro = value; }
        }
        */

        private Integer AplicacaoID;
        protected Integer nAplicacaoID
        {
            get { return AplicacaoID; }
            set { AplicacaoID = value; }
        }

        private string CaminhoRede;
        protected string sCaminhoRede
        {
            get { return CaminhoRede; }
            set { CaminhoRede = value; }
        }

        private string TipoGAREGNRE;
        protected string sTipoGAREGNRE
        {
            get { return TipoGAREGNRE; }
            set { TipoGAREGNRE = value; }
        }

        private Integer Vias;
        protected Integer nVias
        {
            get { return Vias; }
            set { Vias = value; }
        }

        private Integer[] Financeiro;
        protected Integer[] aFinanceiro
        {
            get { return Financeiro; }
            set { Financeiro = value; }
        }

        protected void GerarGNRE()
        {

            try
            {

                ProcedureParameters[] param = new ProcedureParameters[] 
                {
				new ProcedureParameters("@SQL ", SqlDbType.VarChar, DBNull.Value),
                new ProcedureParameters("@Path ", SqlDbType.VarChar, DBNull.Value),
                new ProcedureParameters("@FileOK ", SqlDbType.Bit, DBNull.Value, ParameterDirection.InputOutput)
	            };

                //Corre��o para digito 0
                //Inicio
                string dia = "";
                string mes = "";
                string ano = "";
                string hora = "";
                string minuto = "";
                string segundo = "";
                string sFinanceiro = "";
                int estado = 0; //Define se � de SP ou das outras UFs

                for (int i = 0; i < aFinanceiro.Length; i++)
                    sFinanceiro += "/" + aFinanceiro[i].ToString() + "/";

                //dia
                if (DateTime.Now.Day < 10)
                {
                    dia = "0" + DateTime.Now.Day.ToString();
                }
                else
                {
                    dia = DateTime.Now.Day.ToString();
                }
                //mes
                if (DateTime.Now.Month < 10)
                {
                    mes = "0" + DateTime.Now.Month.ToString();
                }
                else
                {
                    mes = DateTime.Now.Month.ToString();
                }
                //ano
                if (DateTime.Now.Year.ToString().Length < 4)
                {
                    ano = "20" + DateTime.Now.Year.ToString();
                }
                else
                {
                    ano = DateTime.Now.Year.ToString();
                }
                //hora
                if (DateTime.Now.Hour < 10)
                {
                    hora = "0" + DateTime.Now.Hour.ToString();
                }
                else
                {
                    hora = DateTime.Now.Hour.ToString();
                }
                //minuto
                if (DateTime.Now.Minute < 10)
                {
                    minuto = "0" + DateTime.Now.Minute.ToString();
                }
                else
                {
                    minuto = DateTime.Now.Minute.ToString();
                }
                //segundo
                if (DateTime.Now.Second < 10)
                {
                    segundo = "0" + DateTime.Now.Second.ToString();
                }
                else
                {
                    segundo = DateTime.Now.Second.ToString();
                }
                //fim

                if (nAplicacaoID.intValue() == 1)
                    estado = 1;
                else
                    estado = 2;

                sNomeArquivo = sTipoGAREGNRE + ano + mes + dia + hora + minuto + segundo + ".xml";

                    if (sTipoGAREGNRE.ToString() == "GNRE")
                        nTipoResultado = 1401;
                    else
                        nTipoResultado = 1400;

                    param[0].Data = "sp_Pedido_ArquivoXML_GARE_GNRE '" + sFinanceiro + "', " + nVias.ToString() + ", " + nTipoResultado.ToString()+ ", " + estado.ToString();
                    param[1].Data = sCaminhoRede + sNomeArquivo;
                //}
                /*else
                {
                    sNomeArquivo = sTipoGAREGNRE + ano + mes + dia + hora + minuto + segundo + ".txt";

                    if (sTipoGAREGNRE.ToString() == "GNRE")
                        nTipoResultado = 1401;
                    else
                        nTipoResultado = 1400;

                    param[0].Data = "sp_Pedido_ArquivoTexto_GARE_GNRE '" + sFinanceiro + "', " + nTipoResultado.ToString();
                    param[1].Data = sCaminhoRede + sNomeArquivo;
                }*/

                DataInterfaceObj.execNonQueryProcedure("sp_EDI_ArquivoTexto_Gerador", param);

                bVerifica = bool.Parse(param[2].Data.ToString());

                if (bVerifica != false)
                {
                    sMessage = "ok";

                    for (int i = 0; i < aFinanceiro.Length; i++)
                    {
                        SQL = "UPDATE Financeiro SET ArquivoGuiaRecolhimento = '" + sNomeArquivo + "' WHERE FinanceiroID = " + aFinanceiro[i];
                        DataInterfaceObj.ExecuteSQLCommand(SQL);
                    }
                }
                else
                {
                    sMessage = "erro";
                }

            }
            catch (SqlException ex)
            {
                sMessage = ex.Message.ToString();
            }
        }

		protected override void PageLoad(object sender, EventArgs e)
		{
            GerarGNRE();

			sMessage = sMessage.Replace("'", "\"");

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select '" + sMessage + "' as Mensagem, '" + sNomeArquivo + "' as NomeArquivo" 
            ));
		}
	}
}
