﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class loadcmbsCampanhas : System.Web.UI.OverflyPage
    {
        private Integer TipoCampanhaID;

        public Integer nTipoCampanhaID
        {
            get { return TipoCampanhaID; }
            set { TipoCampanhaID = value; }
        }
        private Integer tipoResultado;

        public Integer TipoResultado
        {
            get { return tipoResultado; }
            set { tipoResultado = value; }
        }
        private string Pendente;

        public string bPendente
        {
            get { return Pendente; }
            set { Pendente = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            ProcedureParameters[] procParams = new ProcedureParameters[10];

            procParams[0] = new ProcedureParameters(
            "@EmpresaID",
            System.Data.SqlDbType.Int,
            DBNull.Value);

            procParams[1] = new ProcedureParameters(
            "@FabricanteID",
            System.Data.SqlDbType.Int,
            DBNull.Value);

            procParams[2] = new ProcedureParameters(
            "@TipoCampanhaID",
            System.Data.SqlDbType.Int,
            TipoCampanhaID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(TipoCampanhaID.ToString()));

            procParams[3] = new ProcedureParameters(
            "@ProprietarioID",
            System.Data.SqlDbType.Int,
            DBNull.Value);
            
            procParams[4] = new ProcedureParameters(
            "@MoedaSistema",
            System.Data.SqlDbType.Int,
            DBNull.Value);
            
            procParams[5] = new ProcedureParameters(
            "@Pendentes",
            System.Data.SqlDbType.Bit,
            Pendente.ToString() == "0" ? true : false);

            procParams[6] = new ProcedureParameters(
            "@dtInicio",
            System.Data.SqlDbType.Date,
            DBNull.Value);

            procParams[7] = new ProcedureParameters(
            "@dtFim",
            System.Data.SqlDbType.Date,
            DBNull.Value);

            procParams[8] = new ProcedureParameters(
            "@Filtro",
            System.Data.SqlDbType.VarChar,
            DBNull.Value);

            procParams[9] = new ProcedureParameters(
            "@TipoResultado",
            System.Data.SqlDbType.Int,
            tipoResultado.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(tipoResultado));

            DataInterfaceObj.execNonQueryProcedure("sp_Financeiro_Campanhas", procParams);

            WriteResultXML(DataInterfaceObj.getRemoteData("select '' as Msg"));
        }
    }
}