﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class verificacaofinanceiro : System.Web.UI.OverflyPage
    {

        private Integer FinanceiroID;

        public Integer nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value; }
        }
        private Integer EstadoDeID;

        public Integer nEstadoDeID
        {
            get { return EstadoDeID; }
            set { EstadoDeID = value; }
        }
        private Integer EstadoParaID;

        public Integer nEstadoParaID
        {
            get { return EstadoParaID; }
            set { EstadoParaID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }


        protected override void PageLoad(object sender, EventArgs e)
        {
            string sql = "";
            string Resultado = "";
            string Mensagem = "";

            ProcedureParameters[] procParams = new ProcedureParameters[6];

            procParams[0] = new ProcedureParameters(
                "@FinanceiroID",
                System.Data.SqlDbType.Int,
            FinanceiroID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(FinanceiroID));

            procParams[1] = new ProcedureParameters(
                "@EstadoDeID",
                System.Data.SqlDbType.Int,
            EstadoDeID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(EstadoDeID));

            procParams[2] = new ProcedureParameters(
                "@EstadoParaID",
                System.Data.SqlDbType.Int,
            EstadoParaID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(EstadoParaID));

            procParams[3] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
            UsuarioID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(UsuarioID));

            procParams[4] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);

            procParams[5] = new ProcedureParameters(
                "@Mensagem",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[5].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure("sp_Financeiro_Verifica", procParams);

            if (procParams[4].Data != DBNull.Value)
                Resultado += procParams[4].Data.ToString();

            if (procParams[5].Data != DBNull.Value)
                Mensagem += procParams[5].Data.ToString();

            sql += (sql == "" ? "SELECT" : "UNION SELECT") + " " +
                (Resultado != null ? ("'" + Resultado + "' as Resultado, ") : " ") +
                (Mensagem.ToString() != null ? ("'" + Mensagem + "' as Mensagem ") : " ");
            //(ValorId.ToString() != null ? ("' " + ValorId + "' as ValorID ") : " ");


            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    sql));


        }
    }
}