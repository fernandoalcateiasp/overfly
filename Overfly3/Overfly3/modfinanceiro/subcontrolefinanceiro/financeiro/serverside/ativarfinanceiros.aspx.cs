﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class ativarfinanceiros : System.Web.UI.OverflyPage
    {

        private Integer UserID;

        public Integer nUserID
        {
            get { return UserID; }
            set { UserID = value; }
        }
        private Integer Tipo;

        public Integer nTipo
        {
            get { return Tipo; }
            set { Tipo = value; }
        }
        private Integer EstadoDeID;

        public Integer nEstadoDeID
        {
            get { return EstadoDeID; }
            set { EstadoDeID = value; }
        }
        private Integer EstadoParaID;

        public Integer nEstadoParaID
        {
            get { return EstadoParaID; }
            set { EstadoParaID = value; }
        }
        private Integer SituacaoCobrancaParaID;

        public Integer nSituacaoCobrancaParaID
        {
            get { return SituacaoCobrancaParaID; }
            set { SituacaoCobrancaParaID = value; }
        }
        private Integer DataLen;

        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }
        private Integer[] FinanceiroID;

        public Integer[] nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value; }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            string Resultado = "";
            string Mensagem = "";
            string strSQL = "";

            for (int i = 0; i < DataLen.intValue(); i++)
            {

                if (Tipo.intValue() == 1)
                {
                    ProcedureParameters[] procParams = new ProcedureParameters[6];

                    procParams[0] = new ProcedureParameters(
                    "@FinanceiroID",
                    System.Data.SqlDbType.Int,
                    FinanceiroID.Length == 0 ? DBNull.Value : (Object)Convert.ToInt32(FinanceiroID[i]));

                    procParams[1] = new ProcedureParameters(
                    "@EstadoDeID",
                    System.Data.SqlDbType.Int,
                    EstadoDeID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(EstadoDeID.ToString()));

                    procParams[2] = new ProcedureParameters(
                    "@EstadoParaID",
                    System.Data.SqlDbType.Int,
                    EstadoParaID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(EstadoParaID.ToString()));

                    procParams[3] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                    UserID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(UserID.ToString()));

                    procParams[4] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.Int,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                    procParams[4].Length = 8000;

                    procParams[5] = new ProcedureParameters(
                    "@Mensagem",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                    procParams[5].Length = 8000;


                    DataInterfaceObj.execNonQueryProcedure("sp_Financeiro_AlteraEstado", procParams);

                    Resultado = procParams[4].Data.ToString();

                    if (procParams[5].Data != DBNull.Value)
                        Mensagem += procParams[5].Data.ToString();
                }

                else
                {
                    strSQL = "UPDATE Financeiro SET SituacaoCobrancaID = " + (SituacaoCobrancaParaID) + ", UsuarioID = " + (UserID) + " " +
                                "WHERE (FinanceiroID = " + (FinanceiroID[i]) + ") ";

                    DataInterfaceObj.ExecuteSQLCommand(strSQL);

                }
            }
            WriteResultXML(DataInterfaceObj.getRemoteData(
                 "select '" + Resultado + "' as Resultado, " +
                          "'" + Mensagem + "' as Mensagem "));
        }
    }
}