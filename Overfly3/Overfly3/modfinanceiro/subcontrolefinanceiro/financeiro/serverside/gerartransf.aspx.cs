﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class gerartransf : System.Web.UI.OverflyPage
    {
        private int FinanceiroDeID;
        private int FinanceiroParaID;
        private int ValorDeID;
        private int ValorParaID;
        private string Resultado;

        private Integer RelPesContaDeID;

        public Integer nRelPesContaDeID
        {
            get { return RelPesContaDeID; }
            set { RelPesContaDeID = value; }
        }
        private Integer RelPesContaParaID;

        public Integer nRelPesContaParaID
        {
            get { return RelPesContaParaID; }
            set { RelPesContaParaID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private string Valor;

        public string nValor
        {
            get { return Valor; }
            set { Valor = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            ProcedureParameters[] procParams = new ProcedureParameters[9];

            procParams[0] = new ProcedureParameters(
                "@RelPesContaDeID",
                System.Data.SqlDbType.Int,
            RelPesContaDeID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(RelPesContaDeID));

            procParams[1] = new ProcedureParameters(
                "@RelPesContaParaID",
                System.Data.SqlDbType.Int,
            RelPesContaParaID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(RelPesContaParaID.ToString()));

            procParams[2] = new ProcedureParameters(
             "@ValorTransferencia",
             System.Data.SqlDbType.Money,
             Valor.ToString() == "0" ? DBNull.Value : (Object)Valor.ToString());

            procParams[3] = new ProcedureParameters(
             "@UsuarioID",
             System.Data.SqlDbType.Int,
             UsuarioID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(UsuarioID.ToString()));

            procParams[4] = new ProcedureParameters(
                "@FinanceiroDeID",
                System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[4].Length = 8000;

            procParams[5] = new ProcedureParameters(
                 "@FinanceiroParaID",
                 System.Data.SqlDbType.Int,
                 DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[5].Length = 8000;

            procParams[6] = new ProcedureParameters(
                "@ValorDeID",
                System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[6].Length = 8000;

            procParams[7] = new ProcedureParameters(
                "@ValorParaID",
                System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[7].Length = 8000;

            procParams[8] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[8].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure("sp_ServicoBancario_Transferencia", procParams);

            if (procParams[4].Data != DBNull.Value)
                FinanceiroDeID = Convert.ToInt32(procParams[4].Data.ToString());

            if (procParams[5].Data != DBNull.Value)
                FinanceiroParaID = Convert.ToInt32(procParams[5].Data.ToString());

            if (procParams[6].Data != DBNull.Value)
                ValorDeID += Convert.ToInt32(procParams[6].Data.ToString());

            if (procParams[7].Data != DBNull.Value)
                ValorParaID += Convert.ToInt32(procParams[7].Data.ToString());

            if (procParams[8].Data != DBNull.Value)
                Resultado += procParams[8].Data.ToString();


            WriteResultXML(DataInterfaceObj.getRemoteData(
                 "select '" + Resultado + "' as Resultado, " +
                          "'" + FinanceiroDeID + "' as FinanceiroDeID, " +
                          "'" + FinanceiroParaID + "' as FinanceiroParaID, " +
                          "'" + ValorDeID + "' as ValorDeID, " +
                          "'" + ValorParaID + "' as ValorParaID "));
        }
    }
}