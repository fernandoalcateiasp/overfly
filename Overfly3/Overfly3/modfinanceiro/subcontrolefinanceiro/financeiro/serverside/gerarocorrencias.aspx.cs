﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class gerarocorrencias : System.Web.UI.OverflyPage
    {

        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private Integer DataLen;

        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }
        private Integer FinanceiroMATID;

        public Integer nFinanceiroMATID
        {
            get { return FinanceiroMATID; }
            set { FinanceiroMATID = value; }
        }
        private string EhEstorno;

        public string nEhEstorno
        {
            get { return EhEstorno; }
            set { EhEstorno = value; }
        }
        private Integer[] FinanceiroID;

        public Integer[] nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value; }
        }
        private string[] Valor;

        public string[] nValor
        {
            get { return Valor; }
            set { Valor = value; }
        }
        private string[] ValorRetencao;

        public string[] nValorRetencao
        {
            get { return ValorRetencao; }
            set { ValorRetencao = value; }
        }
        private string[] ValorBase;

        public string[] nValorBase
        {
            get { return ValorBase; }
            set { ValorBase = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string Mensagem = "";
            try
            {
                for (int i = 0; i < DataLen.intValue(); i++)
                {
                    ProcedureParameters[] procParams = new ProcedureParameters[8];

                    procParams[0] = new ProcedureParameters(
                        "@FinanceiroID",
                        System.Data.SqlDbType.Int,
                    FinanceiroID.Length == 0 ? DBNull.Value : (Object)Convert.ToInt32(FinanceiroID[i].ToString()));

                    procParams[1] = new ProcedureParameters(
                        "@FinanceiroMATID",
                        System.Data.SqlDbType.Int,
                    FinanceiroMATID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(FinanceiroMATID.ToString()));

                    procParams[2] = new ProcedureParameters(
                        "@EhEstorno",
                        System.Data.SqlDbType.Int,
                    EhEstorno.ToString() == "0" ? false : true);

                    procParams[3] = new ProcedureParameters(
                        "@Valor",
                        System.Data.SqlDbType.Money,
                     Valor.Length == 0 ? DBNull.Value : (Object)Convert.ToDouble(Valor[i].ToString()));

                    procParams[4] = new ProcedureParameters(
                         "@ValorRetencao",
                         System.Data.SqlDbType.Money,
                     ValorRetencao.Length == 0 ? DBNull.Value : (Object)Convert.ToDouble(ValorRetencao[i].ToString()));

                    procParams[5] = new ProcedureParameters(
                         "@ValorBase",
                         System.Data.SqlDbType.Money,
                     ValorBase.Length == 0 ? DBNull.Value : (Object)Convert.ToDouble(ValorBase[i].ToString()));

                    procParams[6] = new ProcedureParameters(
                        "@UsuarioID",
                        System.Data.SqlDbType.Int,
                    UsuarioID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(UsuarioID.ToString()));

                    procParams[7] = new ProcedureParameters(
                        "@Resultado",
                        System.Data.SqlDbType.VarChar,
                        DBNull.Value,
                    ParameterDirection.InputOutput);
                    procParams[7].Length = 8000;

                    DataInterfaceObj.execNonQueryProcedure("sp_Financeiro_OcorrenciasCruzadas", procParams);

                    if (procParams[7].Data != DBNull.Value)
                        Mensagem += procParams[7].Data.ToString();

                }//fim do for
            }
            catch (System.Exception exception)
            {
                //erros += "#ERROR#OverflyPage (" + this.GetType().Name + "):" + exception.Message + " \r\n ";
                Mensagem += exception.Message + " Erro ! \r\n ";
            }
            finally
            {
                WriteResultXML(DataInterfaceObj.getRemoteData(
                "select '" + Mensagem + "' as Resultado "));
            }
        }
    }
}