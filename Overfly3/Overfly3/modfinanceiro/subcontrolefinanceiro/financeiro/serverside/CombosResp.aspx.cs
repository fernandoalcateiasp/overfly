﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class CombosResp : System.Web.UI.OverflyPage
    {
        protected override void PageLoad(object sender, EventArgs e)
        {
            string SQL = "";
            SQL = "SELECT '1' AS [Indice],0 AS [PessoaID], '' AS [Fantasia] " +
                    "UNION ALL " +
                "SELECT DISTINCT '2', ProprietarioID, dbo.fn_Pessoa_Fantasia(ProprietarioID, 0)[Responsavel] " +
                    "FROM Campanhas WITH (NOLOCK) " +
                    "WHERE ProprietarioID IS NOT NULL " +
                    "ORDER BY Indice, Fantasia";

            WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
        }
    }
}