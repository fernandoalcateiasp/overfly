using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
	public partial class garegnre: System.Web.UI.OverflyPage
	{
        /*
         * Financeiro.FinanceiroID AS Financeiro,
         * Financeiro.Observacao,
         * Financeiro.PessoaID,
         * Pessoa.Fantasia AS Pessoa,
         * Localidade.CodigoLocalidade2 AS UF, 
         * Financeiro.PedidoID,
         * Financeiro.Valor, 
         * SPACE(0) AS OK
         */

        private Integer Financeiro = Constants.INT_ZERO;
		protected Integer nFinanceiro
		{
			get { return Financeiro; }
			set { Financeiro = ((value != null) ? value : Constants.INT_ZERO); }
		}

        private Integer Empresa = Constants.INT_ZERO;
		protected Integer nEmpresa
		{
			get { return Empresa; }
			set { Empresa = value; }
		}

        private Integer TipoGuia;
        protected Integer nTipoGuia
        {
            get { return TipoGuia; }
            set { TipoGuia = value; }
        }

		private Integer Aplicacao = Constants.INT_ZERO;
        protected Integer nAplicacao
		{
            get { return Aplicacao; }
            set { Aplicacao = value; }
		}
        private string Difal = string.Empty;
        protected string bDifal
        {
            get { return Difal; }
            set { Difal = (value == null ? "0" : value); }
        }

        protected DataSet ListarFinanceiros()
        {
            ProcedureParameters[] param = new ProcedureParameters[] 
            {
				new ProcedureParameters("@EmpresaID", SqlDbType.Int, DBNull.Value),
				new ProcedureParameters("@AplicacaoID", SqlDbType.Int, DBNull.Value),
                new ProcedureParameters("@GuiaRecolhimentoID", SqlDbType.Int, DBNull.Value),
				new ProcedureParameters("@FinanceiroID", SqlDbType.Int, DBNull.Value),
                new ProcedureParameters("@DA", SqlDbType.Bit, DBNull.Value)
	        };

            param[0].Data = nEmpresa.intValue();
            param[1].Data = nAplicacao.intValue();
            param[2].Data = nTipoGuia.intValue();
            
            if (nFinanceiro.intValue() > 0)
                param[3].Data = nFinanceiro.intValue();

            param[4].Data = Difal.Length > 0 ? (Object)int.Parse(bDifal) : DBNull.Value;

            return DataInterfaceObj.execQueryProcedure("sp_Financeiro_ListarGareGnre", param);
        }

		protected override void PageLoad(object sender, EventArgs e)
		{

            WriteResultXML(ListarFinanceiros());
		}
	}
}
