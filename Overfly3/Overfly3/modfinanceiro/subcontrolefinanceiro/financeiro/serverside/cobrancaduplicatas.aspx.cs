using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class cobrancaduplicatas: System.Web.UI.OverflyPage
	{
        string sMensagem;

        private Integer Tipo = Constants.INT_ZERO;
        protected Integer nTipo
        {
            get { return Tipo; }
            set { Tipo = ((value != null) ? value : Constants.INT_ZERO); }
        }

        private String ParceiroID;
        protected String sParceiroID 
		{
            get { return ParceiroID; }
            set { ParceiroID = value; }
		}

        private String ParceiroNome;
        protected String sParceiroNome
        {
            get { return ParceiroNome; }
            set { ParceiroNome = value; }
        }

        private String DataInicial;
        protected String sDataInicial
		{
            get { return DataInicial; }
            set { DataInicial = value; }
		}

        private String DataFinal;
        protected String sDataFinal
        {
            get { return DataFinal; }
            set { DataFinal = value; }
        }

        private String Financeiros;
        protected String sFinanceiros
        {
            get { return Financeiros; }
            set { Financeiros = value; }
        }

        private String Observacao;
        protected String sObservacao
        {
            get { return Observacao; }
            set { Observacao = value; }
        }

        private String Empresa;
        protected String sEmpresa
        {
            get { return Empresa; }
            set { Empresa = value; }
        }

        private Integer EmpLogada = Constants.INT_ZERO;
        protected Integer nEmpLogada
        {
            get { return EmpLogada; }
            set { EmpLogada = ((value != null) ? value : Constants.INT_ZERO); }
        }

        private String Usuario;
        protected String sUsuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }
        
        protected DataSet Consulta()
        {
            ProcedureParameters[] param = new ProcedureParameters[] 
            {
				new ProcedureParameters("@Tipo", SqlDbType.Int, DBNull.Value),
				new ProcedureParameters("@ParceiroID", SqlDbType.VarChar, DBNull.Value),
                new ProcedureParameters("@ParceiroNome", SqlDbType.VarChar, DBNull.Value),
                new ProcedureParameters("@DataInicial", SqlDbType.VarChar, DBNull.Value),
				new ProcedureParameters("@DataFinal", SqlDbType.VarChar, DBNull.Value),
                new ProcedureParameters("@Financeiros", SqlDbType.VarChar, DBNull.Value),
                new ProcedureParameters("@Observacao", SqlDbType.VarChar, DBNull.Value),
                new ProcedureParameters("@EmpresaID", SqlDbType.VarChar, DBNull.Value),
                new ProcedureParameters("@EmpLogada", SqlDbType.Int, DBNull.Value),
                new ProcedureParameters("@UsuarioID", SqlDbType.VarChar, DBNull.Value),
                new ProcedureParameters("@Mensagem", SqlDbType.VarChar, DBNull.Value, ParameterDirection.InputOutput, 8000)
	        };

            param[0].Data = nTipo.intValue();

            if (sParceiroID != "")
                param[1].Data = sParceiroID;
            if (sParceiroNome != "")
                param[2].Data = sParceiroNome;
            if (sDataInicial != "")
                param[3].Data = sDataInicial;
            if (sDataFinal != "")
                param[4].Data = sDataFinal;
            if (sFinanceiros != "")
                param[5].Data = sFinanceiros;
            if (sObservacao != "")
                param[6].Data = sObservacao;

            param[7].Data = sEmpresa;
            param[8].Data = nEmpLogada.intValue();
            param[9].Data = sUsuario;

            if (nTipo.intValue() == 4)
            {
                DataInterfaceObj.execNonQueryProcedure("sp_Cobranca_Duplicatas", param);
                sMensagem = param[10].Data.ToString();
                return null;
            }
            else
            {
                return DataInterfaceObj.execQueryProcedure("sp_Cobranca_Duplicatas", param);
            }

        }

		protected override void PageLoad(object sender, EventArgs e)
		{
            //Chama a funcao para realizar leitura no banco ou para realizar um update caso o valor seja 4.
            if (nTipo.intValue() == 4)
            {
                Consulta();
                WriteResultXML(DataInterfaceObj.getRemoteData("select '" + sMensagem + "' AS Mensagem"));
            }
            else
            {
                WriteResultXML(Consulta());
            }
		}
	}
}
