﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class baixarfinanceiros : System.Web.UI.OverflyPage
    {
        private Integer nDataLen;

        public Integer DataLen;
        private string sResultado;
        private string strSQL;


        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private Integer[] FinanceiroID;

        public Integer[] nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value; }
        }
        private string Desconto;

        public string bDesconto
        {
            get { return Desconto; }
            set { Desconto = value; }
        }
        private string GravaValor;

        public string bGravaValor
        {
            get { return GravaValor; }
            set { GravaValor = value; }
        }


        protected override void PageLoad(object sender, EventArgs e)
        {

            for (int i = 0; i < FinanceiroID.Length; i++)
            {
                ProcedureParameters[] procParams = new ProcedureParameters[5];

                procParams[0] = new ProcedureParameters(
                "@FinanceiroID",
                System.Data.SqlDbType.Int,
                FinanceiroID.Length == 0 ? DBNull.Value : (Object)Convert.ToInt32(FinanceiroID[i]));

                procParams[1] = new ProcedureParameters(
                "@Desconto",
                System.Data.SqlDbType.Bit,
                Desconto.ToString() == "0" ? false : true);

                procParams[2] = new ProcedureParameters(
                "@GravaValor",
                System.Data.SqlDbType.Bit,
                GravaValor.ToString() == "0" ? false : true);

                procParams[3] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                UsuarioID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(UsuarioID.ToString()));

                procParams[4] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
                procParams[4].Length = 8000;


                DataInterfaceObj.execNonQueryProcedure("sp_Financeiro_Liquidacao", procParams);

                if (procParams[4].Data != DBNull.Value)
                    sResultado += procParams[4].Data.ToString() + "\r \n";
            }


            WriteResultXML(DataInterfaceObj.getRemoteData(
                 "select '" + sResultado + "' as Resultado"));
        }
    }
}