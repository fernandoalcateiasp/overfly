﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class gerarlancamentos : System.Web.UI.OverflyPage
    {
        private Integer FinanceiroID;

        public Integer nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string Resultado = "";
            string LancamentoID = "";

            ProcedureParameters[] procParams = new ProcedureParameters[6];

            procParams[0] = new ProcedureParameters(
                "@FinanceiroID",
                System.Data.SqlDbType.Int,
            FinanceiroID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(FinanceiroID));

            procParams[1] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
            UsuarioID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(UsuarioID.ToString()));

            procParams[2] = new ProcedureParameters(
             "@GeraLancamento",
             System.Data.SqlDbType.Bit,
             true);

            procParams[3] = new ProcedureParameters(
                "@LancamentoCompensacaoID",
                System.Data.SqlDbType.Int,
                DBNull.Value,
            ParameterDirection.InputOutput);
            procParams[3].Length = 8000;

            procParams[4] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
            ParameterDirection.InputOutput);
            procParams[4].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure("sp_Financeiro_LancamentoRatear", procParams);

            if (procParams[4].Data != DBNull.Value)
                LancamentoID = procParams[4].Data.ToString();

            if (procParams[5].Data != DBNull.Value)
                Resultado += procParams[5].Data.ToString();


            WriteResultXML(DataInterfaceObj.getRemoteData(
                 "select '" + Resultado + "' as Resultado, " +
                          "'" + LancamentoID + "' as LancamentoID "));
        }
    }
}