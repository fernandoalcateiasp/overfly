﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class estornarfinanceiro : System.Web.UI.OverflyPage
    {
        protected static Integer Zero = new Integer(0);
        private int nVezes;
        private bool bEhEstorno;
        private int Financeiro2ID;
        private int FinanceiroId;
        private string Resultado;
        private string sql = "";

        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private Integer FinanceiroID;

        public Integer nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value;
                      
            }
        }
        private string TipoFinanceiroID;

        public string nTipoFinanceiroID
        {
            get { return TipoFinanceiroID; }
            set { TipoFinanceiroID = value; }
        }
        private Integer PessoaID;

        public Integer nPessoaID
        {
            get { return PessoaID; }
            set { PessoaID = value; }
        }
        private Integer RelPesContaID;

        public Integer nRelPesContaID
        {
            get { return RelPesContaID; }
            set 
            { 
                RelPesContaID = (value == null ? Zero : value); 
            }
        }
        private Integer MoedaID;

        public Integer nMoedaID
        {
            get { return MoedaID; }
            set { MoedaID = value; }
        }
        private Integer EhEstorno;

        public Integer nEhEstorno
        {
            get { return EhEstorno; }
            set { EhEstorno = value; }
        }
        private Integer EhImporte;

        public Integer nEhImporte
        {
            get { return EhImporte; }
            set { EhImporte = value; }
        }
        private Integer HistoricoPadraoID;

        public Integer nHistoricoPadraoID
        {
            get { return HistoricoPadraoID; }
            set { HistoricoPadraoID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private string Duplicata;

        public string sDuplicata
        {
            get { return Duplicata; }
            set { Duplicata = value; }
        }
        private Integer FormaPagamentoID;

        public Integer nFormaPagamentoID
        {
            get { return FormaPagamentoID; }
            set { FormaPagamentoID = value; }
        }
        private string DtVencimento;

        public string dtVencimento
        {
            get { return DtVencimento; }
            set { DtVencimento = value; }
        }
        private string Valor;

        public string nValor
        {
            get { return Valor; }
            set { Valor = value; }
        }
        private string Observacao;

        public string sObservacao
        {
            get { return Observacao; }
            set { Observacao = value; }
        }
        private Integer HistoricoPadrao2ID;

        public Integer nHistoricoPadrao2ID
        {
            get { return HistoricoPadrao2ID; }
            set { HistoricoPadrao2ID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            nVezes = 1;
            Financeiro2ID = 0;
            bEhEstorno = false;

            if (EhEstorno.intValue() == 1)
            {
                bEhEstorno = true;

            }

            if (HistoricoPadrao2ID.intValue() > 2)
            {
                nVezes = 2;
            }

            for (int i = 0; i < nVezes; i++)
            {
                bEhEstorno = !bEhEstorno;

                if (i == 2)
                {
                    if (TipoFinanceiroID.ToString() == "1001")
                    {

                        TipoFinanceiroID = "1002";
                    }
                    else
                    {
                        nTipoFinanceiroID = "1001";
                    }

                    nHistoricoPadraoID = nHistoricoPadrao2ID;
                }

                ProcedureParameters[] procParams = new ProcedureParameters[20];

                procParams[0] = new ProcedureParameters(
                    "@EmpresaID",
                    System.Data.SqlDbType.Int,
                EmpresaID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(EmpresaID));

                procParams[1] = new ProcedureParameters(
                    "@TipoFinanceiroID",
                    System.Data.SqlDbType.Int,
                TipoFinanceiroID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(TipoFinanceiroID.ToString()));

                procParams[2] = new ProcedureParameters(
                    "@Duplicata",
                    System.Data.SqlDbType.VarChar,
                Duplicata.ToString() == null ? DBNull.Value : (Object)Duplicata.ToString());

                procParams[3] = new ProcedureParameters(
                    "@PessoaID",
                    System.Data.SqlDbType.Int,
                PessoaID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(PessoaID.ToString()));

                procParams[4] = new ProcedureParameters(
                    "@dtEmissao",
                    System.Data.SqlDbType.DateTime,
                DBNull.Value);

                procParams[5] = new ProcedureParameters(
                    "@FormaPagamentoID",
                    System.Data.SqlDbType.Int,
                FormaPagamentoID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(FormaPagamentoID.ToString()));

                procParams[6] = new ProcedureParameters(
                    "@PrazoPagamento",
                    System.Data.SqlDbType.VarChar,
                DBNull.Value);

                procParams[7] = new ProcedureParameters(
                   "@dtVencimento",
                   System.Data.SqlDbType.DateTime,
                dtVencimento.ToString() == null ? DBNull.Value : (Object)Convert.ToDateTime(dtVencimento.ToString()));
                
                procParams[8] = new ProcedureParameters(
                    "@MoedaID",
                    System.Data.SqlDbType.Int,
                MoedaID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(MoedaID.ToString()));

                procParams[9] = new ProcedureParameters(
                    "@Valor",
                    System.Data.SqlDbType.Money,
                Valor.ToString() == "0" ? DBNull.Value : (Object)Valor.ToString());

                procParams[10] = new ProcedureParameters(
                    "@TaxaAtualizacao",
                    System.Data.SqlDbType.Int,
                    0);

                procParams[11] = new ProcedureParameters(
                    "@EhEstorno",
                    System.Data.SqlDbType.Bit,
                EhEstorno.ToString() == "0" ? false : true);

                procParams[12] = new ProcedureParameters(
                    "@EhImporte",
                    System.Data.SqlDbType.Bit,
                EhImporte.ToString() == "0" ? false : true);

                procParams[13] = new ProcedureParameters(
                    "@HistoricoPadraoID",
                    System.Data.SqlDbType.Int,
                HistoricoPadraoID.ToString() == "0" ? DBNull.Value : (Object)Convert.ToInt32(HistoricoPadraoID.ToString()));

                procParams[14] = new ProcedureParameters(
                    "@ImpostoID",
                    System.Data.SqlDbType.Int,
                DBNull.Value);

                procParams[15] = new ProcedureParameters(
                    "@Observacao",
                    System.Data.SqlDbType.VarChar,
                Observacao.ToString() == null ? DBNull.Value : (Object)Observacao.ToString());

                procParams[16] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                UsuarioID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(UsuarioID.ToString()));

                procParams[17] = new ProcedureParameters(
                    "@RelPesContaID",
                    System.Data.SqlDbType.Int,
                RelPesContaID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(RelPesContaID.ToString()));

                procParams[18] = new ProcedureParameters(
                    "@FinanceiroID",
                    System.Data.SqlDbType.Int,
                    FinanceiroID,
                    ParameterDirection.InputOutput);                

                procParams[19] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[19].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure("sp_Financeiro_Gerador", procParams);
                
                if (procParams[18].Data != DBNull.Value)
                {
                    if (i == 1)
                    {
                        FinanceiroId = Convert.ToInt32(procParams[18].Data.ToString());
                    }
                    else
                    {
                        Financeiro2ID = Convert.ToInt32(procParams[18].Data.ToString());
                    }
                }

                if (procParams[19].Data != DBNull.Value)
                    Resultado += procParams[19].Data.ToString();

            }

            sql += (sql == "" ? "SELECT" : "UNION SELECT") + " " +
                ("'" + (Resultado!= null ? Resultado: "")  + "' as Resultado, ") +
                       (FinanceiroId != null ? ("' " + FinanceiroId + "' as FinanceiroID, ") : " ") +
                       (Financeiro2ID != null ? ("' " + Financeiro2ID + "' as Financeiro2ID ") : " ");

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    sql));

        }
    }
}