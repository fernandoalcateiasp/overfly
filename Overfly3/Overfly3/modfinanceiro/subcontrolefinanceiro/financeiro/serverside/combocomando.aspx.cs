﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class combocomando : System.Web.UI.OverflyPage
    {
        private string strSQL;
        private Integer RelPesContaID;

        public Integer nRelPesContaID
        {
            get { return RelPesContaID; }
            set { RelPesContaID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            strSQL = "SELECT DISTINCT d.ComandoID AS fldID, d.Comando AS fldName, e.Ordem AS Ordem " +
                     "FROM RelacoesPessoas_Contas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Bancos_Comandos d WITH(NOLOCK), TiposAuxiliares_Itens e WITH(NOLOCK) " +
                     "WHERE a.RelPesContaID=" + RelPesContaID + " AND a.RelacaoID = b.RelacaoID " +
                     "AND b.ObjetoID = c.PessoaID AND c.BancoID = d.BancoID AND d.ComandoID = e.ItemID AND e.TipoID=810 AND e.EstadoID=2 AND e.Filtro LIKE '%(1091)%' " +
                     "UNION ALL SELECT 0 AS fldID, ' ' AS fldName, 0 AS Ordem " +
                     "ORDER BY Ordem ";

            WriteResultXML(DataInterfaceObj.getRemoteData(strSQL));
        }
    }
}