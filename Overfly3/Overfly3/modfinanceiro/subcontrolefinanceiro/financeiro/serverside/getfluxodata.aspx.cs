﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class getfluxodata : System.Web.UI.OverflyPage
    {
        private string Empresas;

        public string sEmpresas
        {
            get { return Empresas; }
            set { Empresas = value; }
        }
        private Integer MoedaID;

        public Integer nMoedaID
        {
            get { return MoedaID; }
            set { MoedaID = value; }
        }
        private string Dt_Data;

        public string dt_Data
        {
            get { return Dt_Data; }
            set { Dt_Data = value; }
        }
        private string Dt_FluxoCaixa;

        public string dt_FluxoCaixa
        {
            get { return Dt_FluxoCaixa; }
            set { Dt_FluxoCaixa = value; }
        }
        private Integer TipoFinanceiroID;

        public Integer nTipoFinanceiroID
        {
            get { return TipoFinanceiroID; }
            set { TipoFinanceiroID = value; }
        }
        private Integer ParceiroID;

        public Integer nParceiroID
        {
            get { return ParceiroID; }
            set { ParceiroID = value; }
        }
        private string EstadoC;

        public string bEstadoC
        {
            get { return EstadoC; }
            set { EstadoC = value; }
        }
        protected DataSet Fluxodata()
        {

            ProcedureParameters[] ProcParams = new ProcedureParameters[7];

            ProcParams[0] = new ProcedureParameters(
                "@Empresas",
                System.Data.SqlDbType.VarChar,
                Empresas == null || Empresas.Length == 0 ? System.DBNull.Value : (Object)Empresas.ToString());

            ProcParams[1] = new ProcedureParameters(
                "@MoedaID",
                System.Data.SqlDbType.Int,
                MoedaID == null ? System.DBNull.Value : (Object)Convert.ToInt32(MoedaID));

            ProcParams[2] = new ProcedureParameters(
               "@dtData",
               System.Data.SqlDbType.Date,
               dt_Data == null ? System.DBNull.Value : (Object)(dt_Data.ToString()));

            ProcParams[3] = new ProcedureParameters(
               "@dtFluxoCaixa",
               System.Data.SqlDbType.Date,
              dt_FluxoCaixa == null ? System.DBNull.Value : (Object)(dt_FluxoCaixa.ToString()));

            ProcParams[4] = new ProcedureParameters(
               "@TipoFinanceiroID",
               System.Data.SqlDbType.Int,
               TipoFinanceiroID == null ? System.DBNull.Value : (Object)Convert.ToInt32(TipoFinanceiroID));

            ProcParams[5] = new ProcedureParameters(
               "@ParceiroID",
               System.Data.SqlDbType.Int,
               ParceiroID == null ? System.DBNull.Value : (Object)Convert.ToInt32(ParceiroID));

            ProcParams[6] = new ProcedureParameters(
               "@EstadoC",
               System.Data.SqlDbType.Bit,
               EstadoC.ToString() == "0" ? false : true);

            return DataInterfaceObj.execQueryProcedure(
                "sp_Financeiro_FluxoCaixa",
                ProcParams);
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(Fluxodata());           

        }
    }
}