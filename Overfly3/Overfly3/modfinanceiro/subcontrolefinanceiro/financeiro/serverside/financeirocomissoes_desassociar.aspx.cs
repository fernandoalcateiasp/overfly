﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class financeirocomissoes_desassociar : System.Web.UI.OverflyPage
    {
        private Integer FinanceiroID;

        public Integer nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value; }
        }
        private string Valor;

        public string nValor
        {
            get { return Valor; }
            set { Valor = value; }
        }
        private Integer FinComissaoID;

        public Integer nFinComissaoID
        {
            get { return FinComissaoID; }
            set { FinComissaoID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private Integer FormID;

        public Integer nFormID
        {
            get { return FormID; }
            set { FormID = value; }
        }
        private Integer SubFormID;

        public Integer nSubFormID
        {
            get { return SubFormID; }
            set { SubFormID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            ProcedureParameters[] procParams = new ProcedureParameters[6];

            procParams[0] = new ProcedureParameters(
            "@FormID",
            System.Data.SqlDbType.Int,
            FormID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(FormID));

            procParams[1] = new ProcedureParameters(
            "@SubFormID",
            System.Data.SqlDbType.Int,
            SubFormID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(SubFormID));

            procParams[2] = new ProcedureParameters(
            "@RegistroID",
            System.Data.SqlDbType.Int,
            FinanceiroID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(FinanceiroID));

            procParams[3] = new ProcedureParameters(
            "@FinComissaoID",
            System.Data.SqlDbType.Int,
            FinComissaoID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(FinComissaoID.ToString()));

            procParams[4] = new ProcedureParameters(
            "@UsuarioID",
            System.Data.SqlDbType.Int,
            UsuarioID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(UsuarioID.ToString()));

            procParams[5] = new ProcedureParameters(
            "@Valor",
            System.Data.SqlDbType.VarChar,
            Valor.ToString() == null ? DBNull.Value : (Object)Valor.ToString());

            DataInterfaceObj.execNonQueryProcedure("sp_FinanceiroComissoes_Desassociar", procParams);

            WriteResultXML(DataInterfaceObj.getRemoteData(
                 "select '' as fldresp"));

        }
    }
}