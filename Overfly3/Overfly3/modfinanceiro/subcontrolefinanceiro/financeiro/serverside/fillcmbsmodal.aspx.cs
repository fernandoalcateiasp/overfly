﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class fillcmbsmodal : System.Web.UI.OverflyPage
    {
        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            string SQL = "";
            SQL = "SELECT '0' AS Indice, 0 AS fldID, ' ' AS fldName, ' ' AS Ordem, SPACE(0) AS Abreviacao  " +
             "UNION ALL " +
             "SELECT '0' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, CONVERT(varchar,Ordem) AS Ordem, SPACE(0) AS Abreviacao " +
             "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
             "WHERE TipoID=801 AND EstadoID=2 " +
             "UNION ALL " +
             "SELECT '1' AS Indice, c.ConceitoID AS fldID, c.SimboloMoeda AS fldName, CONVERT(varchar,1) AS Ordem, SPACE(0) AS Abreviacao " +
             "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " +
             "WHERE a.ObjetoID=999 AND a.SujeitoID= " + EmpresaID + " AND a.TipoRelacaoID=12 AND a.RelacaoID=b.RelacaoID " +
             "AND b.Faturamento=1 AND b.MoedaID=c.ConceitoID " +
             "UNION ALL " +
             "SELECT '1' AS Indice, b.ConceitoID AS fldID, b.SimboloMoeda AS fldName, CONVERT(varchar,2) AS Ordem, SPACE(0) AS Abreviacao " +
             "FROM Recursos a WITH(NOLOCK), Conceitos b WITH(NOLOCK) " +
             "WHERE a.RecursoID=999 AND a.MoedaID=b.ConceitoID " +
             "UNION ALL " +
             "SELECT '2' AS Indice, 0 AS fldID, ' ' AS fldName, ' ' AS Ordem, SPACE(0) AS Abreviacao  " +
             "UNION ALL " +
             "SELECT '2' AS Indice, a.ItemID AS fldID, a.ItemAbreviado AS fldName, CONVERT(varchar,a.Ordem) AS Ordem, a.ItemAbreviado AS Abreviacao " +
             "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " +
             "WHERE (a.TipoID=804 AND a.EstadoID=2) " +
             "UNION ALL " +
             "SELECT '3' AS Indice, 0 AS fldID, ' ' AS fldName, ' ' AS Ordem, SPACE(0) AS Abreviacao  " +
             "UNION ALL " +
             "SELECT DISTINCT '3' AS Indice, a.PessoaID AS fldID, a.Fantasia AS fldName, a.Fantasia AS Ordem, SPACE(0) AS Abreviacao " +
             "FROM Pessoas a WITH(NOLOCK), Financeiro b WITH(NOLOCK) " +
             "WHERE a.PessoaID=b.ProprietarioID AND b.EmpresaID = " + EmpresaID + " " +
             "UNION ALL " +
             "SELECT '4' AS Indice, 0 AS fldID, SPACE(0) AS fldName, '0' AS Ordem, SPACE(0) AS Abreviacao " +
             "UNION ALL " +
             "SELECT '4' AS Indice, ItemID, ItemMasculino, convert(varchar,Ordem) AS Ordem, SPACE(0) AS Abreviacao " +
             "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
             "WHERE (TipoID = 805 AND EstadoID = 2 AND Aplicar = 1) " +
             "UNION ALL " +
             "SELECT '5' AS Indice, b.RecursoID, b.RecursoFantasia, convert(varchar,RecursoID) AS Ordem, b.RecursoAbreviado AS Abreviacao " +
             "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK) " +
             "WHERE (a.ObjetoID = 331 AND a.TipoRelacaoID = 3 AND a.SujeitoID = b.RecursoID AND b.RecursoID NOT IN (5)) " +
             "UNION ALL " +
             "SELECT '6' AS Indice,a.ItemID, a.ItemFeminino, convert(varchar,a.ItemID) AS Ordem, a.ItemAbreviado AS Abreviacao " +
             "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " +
             "WHERE (a.EstadoID = 2 AND a.TipoID = 802) " +
             "UNION ALL " +
             "SELECT DISTINCT '7' AS Indice, a.PessoaID AS fldID, a.Fantasia AS fldName, a.Fantasia AS Ordem, a.Fantasia AS Abreviacao " +
             "FROM Pessoas a WITH(NOLOCK) " +
             "WHERE a.PessoaID in " +
            "(SELECT " + EmpresaID + " " +
            " UNION ALL SELECT EmpresaAlternativaID FROM FopagEmpresas WITH(NOLOCK) WHERE (EmpresaID = " + EmpresaID + " AND " +
            "FuncionarioID IS NULL) AND (EmpresaAlternativaID = dbo.fn_Empresa_Matriz(EmpresaAlternativaID, 1, 1))) " +
             "ORDER BY Indice,Ordem ";

            WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
        }
    }
}