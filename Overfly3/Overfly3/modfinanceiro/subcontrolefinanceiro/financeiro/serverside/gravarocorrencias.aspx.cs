﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class gravarocorrencias : System.Web.UI.OverflyPage
    {
        private Integer Tipo;

        public Integer nTipo
        {
            get { return Tipo; }
            set { Tipo = value; }
        }
        private Integer[] FinOcorrenciaID;

        public Integer[] nFinOcorrenciaID
        {
            get { return FinOcorrenciaID; }
            set { FinOcorrenciaID = value; }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            string strSQL = "";
            string sData = "";
            string erros = "";
            int RecsAffected = 0;


            if (Tipo.intValue() == 1)
            {
                sData = "dtAprovacaoComissao";
            }
            else
            {
                sData = "dtPagamentoComissao";
            }

            for (int i = 0; i < FinOcorrenciaID.Length; i++)
            {

                strSQL += "UPDATE Financeiro_Ocorrencias SET " + sData + "= GETDATE() " +
                                "WHERE (FinOcorrenciaID = " + FinOcorrenciaID[i] + " AND " + sData + " IS NULL) ";
            }
            try
            {
                RecsAffected = DataInterfaceObj.ExecuteSQLCommand(strSQL);
            }
            catch (System.Exception exception)
            {

                //erros += "#ERROR#OverflyPage (" + this.GetType().Name + "):" + exception.Message + " \r\n ";
                erros += exception.Message + "  \r\n ";
            }

            WriteResultXML(DataInterfaceObj.getRemoteData(
              "select '" + RecsAffected + "' as fldresp, '" + erros + "'  as Erros "));
        }
    }
}