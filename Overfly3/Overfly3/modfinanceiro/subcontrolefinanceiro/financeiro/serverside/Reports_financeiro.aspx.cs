﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using OVERFLYSVRCFGLib;
using WSData;
using System.Drawing.Printing;
using System.Drawing;

namespace Overfly3.PrintJet
{
    public partial class Reports_financeiro : System.Web.UI.Page
    {
        string DATE_SQL_PARAM = HttpContext.Current.Request.Params["DATE_SQL_PARAM"];
        string nEmpresaLogada = HttpContext.Current.Request.Params["nEmpresaLogada"];
        string nEmpresaLogadaIdioma = HttpContext.Current.Request.Params["nEmpresaLogadaIdioma"];
        string glb_sEmpresaFantasia = HttpContext.Current.Request.Params["glb_sEmpresaFantasia"];
        int formato = Convert.ToInt32(HttpContext.Current.Request.Params["formato"]);
        string nFinanceiroID = HttpContext.Current.Request.Params["nFinanceiroID"];
        string sNomeResponsavel = HttpContext.Current.Request.Params["sNomeResponsavel"];
        string sSelRelPesContaID = HttpContext.Current.Request.Params["nSelRelPesContaID"];
        string sCPF = HttpContext.Current.Request.Params["sCPF"];
        string sLinguaLogada = HttpContext.Current.Request.Params["sLinguaLogada"];
        string selEmpresaCampanha = HttpContext.Current.Request.Params["selEmpresaCampanha"];
        string selFabricante = HttpContext.Current.Request.Params["selFabricante"];
        string selCampanhas = HttpContext.Current.Request.Params["selCampanhas"];
        string selResponsavelID = HttpContext.Current.Request.Params["selResponsavelID"];
        string chkMoedaSistema = HttpContext.Current.Request.Params["chkMoedaSistema"];
        string chkPendentes = HttpContext.Current.Request.Params["chkPendentes"];
        string txtDataInicio2 = HttpContext.Current.Request.Params["txtDataInicio2"];
        string txtDataFim2 = HttpContext.Current.Request.Params["txtDataFim2"];
        string txtFiltro3 = HttpContext.Current.Request.Params["txtFiltro3"];
        string chkResumido = HttpContext.Current.Request.Params["chkResumido"];
        string strParams = HttpContext.Current.Request.Params["strParams"];
        int selPadrao = Convert.ToInt32(HttpContext.Current.Request.Params["selPadrao"]);

        string SQLImagem = string.Empty;
        string SQLDetail1 = string.Empty;
        string SQLDetail2 = string.Empty;
        string SQLDetail3 = string.Empty;


        int Datateste = 0;
        bool nulo = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    //Nota de Débito
                    case "40162":
                        NotaDebito();
                        break;
                    
                    //Carta de Anuência
                    case "40164":
                        CartaAnuencia();
                        break;
                    
                    //Duplicata
                    case "40181":
                        duplicata();
                        break;

                    //Descontos Concedidos
                    case "40184":
                        vendasRecebidas();
                        break;

                    //Contas a Pagar/Receber
                    case "40183":
                        fluxoCaixa();
                        break;
                    
                   //Campanhas
                    case "40186":
                        campanhas();
                        break;

                    //DepositoBancario
                    case "40195":
                        DepositoBancario();
                        break;
                        
                }
            }
        }

        private void NotaDebito()
        {
            if (sSelRelPesContaID == "")
            {
                sSelRelPesContaID = "0";
            }

            SQLImagem = "SELECT TOP 1 ImagensEmpresa.RegistroID AS EmpresaID, ImagensEmpresa.Arquivo AS FotoEmpresa " +
                            "FROM [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] ImagensEmpresa WITH(NOLOCK) " +
                            "WHERE ImagensEmpresa.FormID=1210 AND ImagensEmpresa.SubFormID=20100 AND ImagensEmpresa.TipoArquivoID = 1451 AND ImagensEmpresa.RegistroID =" + nEmpresaLogada;


            SQLDetail1 = "SELECT Financeiro.GuiaRecolhimentoID AS GuiaRecolhimentoID, Financeiro.FinanceiroID AS FinanceiroID,  CONVERT(VARCHAR(10), Financeiro.dtEmissao, " + DATE_SQL_PARAM + ") AS dtEmissao, " +
                                    "CONVERT(VARCHAR(10), Financeiro.dtVencimento, " + DATE_SQL_PARAM + ") AS dtVencimento, PedidoID, Duplicata, " +
                                    "ISNULL(dbo.fn_TrimTextLines(Financeiro.Observacoes),'\') AS Observacoes, g.Localidade AS Cidade, CONVERT(VARCHAR, GETDATE(), " + DATE_SQL_PARAM + ") AS Data, UPPER(a.Nome) AS Nome,  " +
                                    "dbo.fn_Numero_Formata(Financeiro.Valor, 2, 1, " + DATE_SQL_PARAM + ") AS sValor, Financeiro.Valor AS nValor," +
                                    "(CASE WHEN b.Endereco IS NULL THEN '\' ELSE b.Endereco END) + " +
                                    "(CASE WHEN b.Numero IS NULL THEN '\' ELSE  ' \' + b.Numero END) AS Endereco, " +
                                    "(CASE WHEN b.CEP IS NULL THEN '\' ELSE b.CEP END) + " +
                                    "(CASE WHEN c.Localidade IS NULL THEN '\' ELSE ' \' + c.Localidade END) +  " +
                                    "(CASE WHEN d.CodigoLocalidade2 IS NULL THEN '\' ELSE ' \' + d.CodigoLocalidade2 END) AS Endereco2, " +
                                    "(CASE WHEN dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, NULL, 0) IS NULL THEN '\' ELSE 'CNPJ \'+ dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, NULL, 0) END) AS CNPJ, " +
                                    "(CASE WHEN dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 112, NULL, 0) IS NULL THEN '\' ELSE 'IE \' + dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 112, NULL, 0) END) AS IE, " +
                                    "UPPER(e.Nome) AS Empresa_Nome, e.Nome AS Empresa_Nome2, " +
                                    "(CASE WHEN f.Endereco IS NULL THEN '\' ELSE f.Endereco END) + " +
                                    "(CASE WHEN f.Numero IS NULL THEN '\' ELSE  ' \' + f.Numero END) AS Empresa_Endereco, " +
                                    "(CASE WHEN f.CEP IS NULL THEN '\' ELSE f.CEP END) + " +
                                    "(CASE WHEN g.Localidade IS NULL THEN '\' ELSE ' \' + g.Localidade END) +  " +
                                    "(CASE WHEN h.CodigoLocalidade2 IS NULL THEN \'\' ELSE \' \' + h.CodigoLocalidade2 END) + " +
                                    "(CASE WHEN i.Localidade IS NULL THEN '\' ELSE ' \' + i.Localidade END) AS Empresa_Endereco2, " +
                                    "(CASE WHEN dbo.fn_Pessoa_Documento(e.PessoaID, NULL, NULL, 111, NULL, 0) IS NULL THEN '\' ELSE 'CNPJ \' + dbo.fn_Pessoa_Documento(e.PessoaID, NULL, NULL, 111, NULL, 0) END) AS Empresa_CNPJ, " +
                                    "(CASE WHEN dbo.fn_Pessoa_Documento(e.PessoaID, NULL, NULL, 112, NULL, 0) IS NULL THEN '\' ELSE 'IE \' + dbo.fn_Pessoa_Documento(e.PessoaID, NULL, NULL, 112, NULL, 0) END) AS Empresa_IE, " +
                                    "'Fone \' + dbo.fn_Pessoa_Telefone(e.PessoaID, 119, 119,1,0,NULL) +  \'    Fax \' + dbo.fn_Pessoa_Telefone(e.PessoaID, 122, 122,1,0,NULL)AS Empresa_Telefone, " +
                                    "dbo.fn_ValorExtenso(Financeiro.Valor,0) AS sValorExtenso " +
                                "FROM Financeiro WITH(NOLOCK) " +
                                    "INNER JOIN Pessoas a WITH(NOLOCK) ON Financeiro.PessoaID = a.PessoaID " +
                                    "INNER JOIN Pessoas_Enderecos b WITH(NOLOCK) ON a.PessoaID = b.PessoaID " +
                                    "LEFT JOIN Localidades c WITH(NOLOCK) ON b.CidadeID = c.LocalidadeID " +
                                    "LEFT JOIN Localidades d WITH(NOLOCK) ON b.UFID = d.LocalidadeID " +
                                    "INNER JOIN Pessoas e WITH(NOLOCK) ON Financeiro.EmpresaID = e.PessoaID " +
                                    "INNER JOIN Pessoas_Enderecos f WITH(NOLOCK) ON e.PessoaID = f.PessoaID " +
                                    "LEFT JOIN Localidades g WITH(NOLOCK) ON f.CidadeID = g.LocalidadeID " +
                                    "LEFT JOIN Localidades h WITH(NOLOCK) ON f.UFID = h.LocalidadeID " +
                                    "LEFT JOIN Localidades i WITH(NOLOCK) ON f.PaisID = i.LocalidadeID " +
                                "WHERE b.Ordem = 1 AND b.EndFaturamento = 1 AND f.Ordem = 1 AND f.EndFaturamento = 1 AND Financeiro.FinanceiroID = " + nFinanceiroID;

            SQLDetail2 = "SELECT dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) AS Bancos, b.RelPesContaID AS RelPesContaID, c.Nome," +
                                "c.Agencia + (CASE WHEN c.AgenciaDV IS NOT NULL THEN '-' + c.AgenciaDV ELSE '' END) AS Agencia," +
                                "b.Conta + (CASE WHEN b.ContaDV IS NOT NULL THEN '-' + b.ContaDV ELSE SPACE(0) END) AS ContaCorrente " +
                            "FROM RelacoesPessoas a WITH(NOLOCK) " +
                                "INNER JOIN RelacoesPessoas_Contas b WITH(NOLOCK) ON a.RelacaoID = b.RelacaoID " +
                                "INNER JOIN Pessoas c WITH(NOLOCK) ON a.ObjetoID = c.PessoaID " +
                            "WHERE a.TipoRelacaoID = 24 AND b.RelPesContaID = " + sSelRelPesContaID;

            printNotaDebito();

        }

        private void printNotaDebito()
        {

            ClsReport Relatorio = new ClsReport();

            DataSet Ds = new DataSet();

            string[,] arrQuerys;

            arrQuerys = new string[,] {
                                        {"sqlImagem", SQLImagem},
                                        {"sqlDetail1", SQLDetail1},
                                        {"sqlDetail2", SQLDetail2}
                                      };


            string Left = "1.4";
            double espLinha = 0.5;

            string Bloco1_Left = "5";
            double Bloco1_Top = 5.1;
            double Bloco2_Top = 7.1;
            double Bloco3_Top = 9.1;
            double Bloco4_Top = 13;
            double Bloco5_Top = 14.2;

            Relatorio.CriarRelatório("Nota de Débito", arrQuerys, "BRA", "Portrait");

            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;


            if (dsFin.Tables["SQLDetail1"].Rows.Count == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Pessoa sem endereço ordem 1.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else {

                string sEmpresa_Nome2 = dsFin.Tables["SQLDetail1"].Rows[0]["Empresa_Nome2"].ToString();
                string sValorExtenso = dsFin.Tables["SQLDetail1"].Rows[0]["sValorExtenso"].ToString();
                string sValor = dsFin.Tables["SQLDetail1"].Rows[0]["sValor"].ToString();
                string sObservacoes = dsFin.Tables["SQLDetail1"].Rows[0]["Observacoes"].ToString();
                string sGuiaRecolhimentoID = dsFin.Tables["SQLDetail1"].Rows[0]["GuiaRecolhimentoID"].ToString();


                Relatorio.CriarObjImage("Query", "sqlImagem", "FotoEmpresa", "8", "0.5", "Body", "3.89104", "1.74215");

                Relatorio.CriarObjLabel("Fixo", "", "NOTA DE DÉBITO " + nFinanceiroID, "14", "3.5", "12", "Black", "B", "Body", "6", "");

                Relatorio.CriarObjLabel("Fixo", "", "Data de emissão:", Left, Bloco1_Top.ToString(), "10", "Black", "L", "Body", "6", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "dtEmissao", Bloco1_Left, Bloco1_Top.ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Fixo", "", "Data de vencimento:", Left, (Bloco1_Top += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "dtVencimento", Bloco1_Left, Bloco1_Top.ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Fixo", "", "Valor do documento:", Left, (Bloco1_Top += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                Relatorio.CriarObjLabel("Fixo", "", "R$ " + sValor, Bloco1_Left, Bloco1_Top.ToString(), "10", "black", "L", "Body", "10", "");

                Relatorio.CriarObjLabel("Fixo", "", "Valor por extenso:", Left, (Bloco2_Top).ToString(), "10", "Black", "L", "Body", "6", "");
                Relatorio.CriarObjLabel("Fixo", "", "(" + sValorExtenso + ")", Left, (Bloco2_Top += espLinha).ToString(), "10", "black", "L", "Body", "17", "");

                Relatorio.CriarObjLabel("Fixo", "", "Dados do sacado:", Left, (Bloco3_Top).ToString(), "10", "Black", "L", "Body", "6", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Nome", Left, (Bloco3_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Endereco", Left, (Bloco3_Top += espLinha).ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Endereco2", Left, (Bloco3_Top += espLinha).ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "CNPJ", Left, (Bloco3_Top += espLinha).ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "IE", Left, (Bloco3_Top += espLinha).ToString(), "10", "black", "L", "Body", "10", "");

                Relatorio.CriarObjLabel("Fixo", "", "O sacado deve à " + sEmpresa_Nome2 + " a importância desta Nota de Débito de prestação de serviços, referente a:", Left, Bloco4_Top.ToString(), "10", "Black", "L", "Body", "17", "");

                if (sObservacoes != "")
                {
                    Relatorio.CriarObjLabel("Fixo", "", sObservacoes, "2.4", Bloco5_Top.ToString(), "10", "Black", "L", "Body", "17", "");
                    Bloco5_Top += (Math.Ceiling(Convert.ToDouble(sObservacoes.Length) / 100) * 0.5) + 0.2;
                }

                if (sGuiaRecolhimentoID == "1400")
                {
                    string sPedidoID = dsFin.Tables["SQLDetail1"].Rows[0]["PedidoID"].ToString();
                    string sDuplicata = dsFin.Tables["SQLDetail1"].Rows[0]["Duplicata"].ToString();

                    Relatorio.CriarObjLabel("Fixo", "", "Reembolso referente ao PE " + sPedidoID + " NF: " + sDuplicata, "2.4", Bloco5_Top.ToString(), "10", "Black", "B", "Body", "17", "");
                    Bloco5_Top += 1.5;
                }

                Relatorio.CriarObjLabel("Fixo", "", "Condições de pagamento:", Left, Bloco5_Top.ToString(), "10", "Black", "B", "Body", "15", "");

                if (Convert.ToInt16(sSelRelPesContaID) > 0)
                {
                    string sBanco = dsFin.Tables["SQLDetail2"].Rows[0]["Nome"].ToString();
                    string sAgencia = dsFin.Tables["SQLDetail2"].Rows[0]["Agencia"].ToString();
                    string sContaCorrente = dsFin.Tables["SQLDetail2"].Rows[0]["ContaCorrente"].ToString();

                    Relatorio.CriarObjLabel("Fixo", "", "Crédito em conta corrente junto ao " + sBanco + ", agencia " + sAgencia + ", conta corrente" + sContaCorrente + " em nome de " + sEmpresa_Nome2 + ".", Left, (Bloco5_Top + espLinha).ToString(), "10", "Black", "L", "Body", "18", "");
                }
                else
                {
                    Relatorio.CriarObjLabel("Fixo", "", "Valor a ser descontado de suas duplicatas a serem definidas.", Left, (Bloco5_Top += espLinha).ToString(), "10", "Black", "L", "Body", "18", "");
                }


                Relatorio.CriarObjLabel("Fixo", "", "Atenciosamente", Left, (Bloco5_Top += 3).ToString(), "10", "Black", "L", "Body", "15", "");

                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_Nome", Left, (Bloco5_Top += 1.5).ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_Endereco", Left, (Bloco5_Top += espLinha).ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_Endereco2", Left, (Bloco5_Top += espLinha).ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_CNPJ", Left, (Bloco5_Top += espLinha).ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_IE", Left, (Bloco5_Top += espLinha).ToString(), "10", "black", "L", "Body", "10", "");


                Relatorio.CriarPDF_Excel("Nota de Débito", 1);
            }
        }

        private void CartaAnuencia()
        {

            SQLImagem = "SELECT TOP 1 ImagensEmpresa.RegistroID AS EmpresaID, ImagensEmpresa.Arquivo AS FotoEmpresa " +
                            "FROM [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] ImagensEmpresa WITH(NOLOCK) " +
                            "WHERE ImagensEmpresa.FormID=1210 AND ImagensEmpresa.SubFormID=20100 AND ImagensEmpresa.TipoArquivoID = 1451 AND ImagensEmpresa.RegistroID =" + nEmpresaLogada;


            SQLDetail1 = "SELECT Financeiro.FinanceiroID AS FinanceiroID, CONVERT(VARCHAR(10), Financeiro.dtEmissao, " + DATE_SQL_PARAM + ") AS dtEmissao, CONVERT(VARCHAR(10), Financeiro.dtVencimento, " + DATE_SQL_PARAM + ") AS dtVencimento, ISNULL(Financeiro.Duplicata, SPACE(0)) AS Duplicata, " +
                                    "ISNULL(Financeiro.Observacoes,'\') AS Observacoes, g.Localidade AS Cidade, CONVERT(VARCHAR, GETDATE(), " + DATE_SQL_PARAM + ") AS Data, UPPER(a.Nome) AS Nome,  " +
                                    "dbo.fn_Numero_Formata(Financeiro.Valor, 2, 1, " + DATE_SQL_PARAM + ") AS sValor, Financeiro.Valor AS nValor," +
                                    "(CASE WHEN b.Endereco IS NULL THEN '\' ELSE b.Endereco END) + " +
                                    "(CASE WHEN b.Numero IS NULL THEN '\' ELSE  ' \' + b.Numero END) AS Endereco, " +
                                    "(CASE WHEN b.CEP IS NULL THEN '\' ELSE b.CEP END) + " +
                                    "(CASE WHEN c.Localidade IS NULL THEN '\' ELSE ' \' + c.Localidade END) +  " +
                                    "(CASE WHEN d.CodigoLocalidade2 IS NULL THEN '\' ELSE ' \' + d.CodigoLocalidade2 END) AS Endereco2, " +
                                    "(CASE WHEN dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, NULL, 0) IS NULL THEN '\' ELSE 'CNPJ \'+ dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, NULL, 0) END) AS CNPJ, " +
                                    "(CASE WHEN dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 112, NULL, 0) IS NULL THEN '\' ELSE 'IE \' + dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 112, NULL, 0) END) AS IE, " +
                                    "UPPER(e.Nome) AS Empresa_Nome, e.Nome AS Empresa_Nome2, " +
                                    "(CASE WHEN f.Endereco IS NULL THEN '\' ELSE f.Endereco END) + " +
                                    "(CASE WHEN f.Numero IS NULL THEN '\' ELSE  ' \' + f.Numero END) AS Empresa_Endereco, " +
                                    "(CASE WHEN f.CEP IS NULL THEN '\' ELSE f.CEP END) + " +
                                    "(CASE WHEN g.Localidade IS NULL THEN '\' ELSE ' \' + g.Localidade END) +  " +
                                    "(CASE WHEN h.CodigoLocalidade2 IS NULL THEN '\' ELSE ' \' + h.CodigoLocalidade2 END) + " +
                                    "(CASE WHEN i.Localidade IS NULL THEN '\' ELSE ' \' + i.Localidade END) AS Empresa_Endereco2, " +
                                    "(CASE WHEN dbo.fn_Pessoa_Documento(e.PessoaID, NULL, NULL, 111, NULL, 0) IS NULL THEN '\' ELSE 'CNPJ \' + dbo.fn_Pessoa_Documento(e.PessoaID, NULL, NULL, 111, NULL, 0) END) AS Empresa_CNPJ, " +
                                    "(CASE WHEN dbo.fn_Pessoa_Documento(e.PessoaID, NULL, NULL, 112, NULL, 0) IS NULL THEN '\' ELSE 'IE \' + dbo.fn_Pessoa_Documento(e.PessoaID, NULL, NULL, 112, NULL, 0) END) AS Empresa_IE, " +
                                    "'Fone \' + dbo.fn_Pessoa_Telefone(e.PessoaID, 119, 119,1,0,NULL) +  '    Fax \' + ISNULL(dbo.fn_Pessoa_Telefone(e.PessoaID, 122, 122,1,0,NULL), '\')AS Empresa_Telefone, j.SimboloMoeda, " +
                                    "dbo.fn_ValorExtenso(Financeiro.Valor,0) AS sValorExtenso, " +
                                    "g.Localidade + ', ' + DBO.fn_Data_Extenso(GETDATE(),1) + ' de ' + DBO.fn_Data_Extenso(GETDATE(),2) + ' de ' + DBO.fn_Data_Extenso(GETDATE(),3) as dtExtenso " +
                                "FROM Financeiro WITH(NOLOCK) " +
                                    "INNER JOIN Pessoas a WITH(NOLOCK) ON Financeiro.PessoaID = a.PessoaID " +
                                    "INNER JOIN Pessoas_Enderecos b WITH(NOLOCK) ON a.PessoaID = b.PessoaID " +
                                    "LEFT JOIN Localidades c WITH(NOLOCK) ON b.CidadeID = c.LocalidadeID " +
                                    "LEFT JOIN Localidades d WITH(NOLOCK) ON b.UFID = d.LocalidadeID " +
                                    "INNER JOIN Pessoas e WITH(NOLOCK) ON Financeiro.EmpresaID = e.PessoaID " +
                                    "INNER JOIN Pessoas_Enderecos f WITH(NOLOCK) ON e.PessoaID = f.PessoaID " +
                                    "LEFT JOIN Localidades g WITH(NOLOCK) ON f.CidadeID = g.LocalidadeID " +
                                    "LEFT JOIN Localidades h WITH(NOLOCK) ON f.UFID = h.LocalidadeID " +
                                    "LEFT JOIN Localidades i WITH(NOLOCK) ON f.PaisID = i.LocalidadeID " +
                                    "INNER JOIN Conceitos j WITH(NOLOCK) ON Financeiro.MoedaID = j.ConceitoID " +
                                "WHERE b.Ordem = 1 AND Financeiro.FinanceiroID = " + nFinanceiroID;

            printCartaAnuencia_DSC();

        }

        private void printCartaAnuencia_DSC()
        {

            ClsReport Relatorio = new ClsReport();

            DataSet Ds = new DataSet();

            string[,] arrQuerys;

            arrQuerys = new string[,] {
                                        {"sqlImagem", SQLImagem},
                                        {"sqlDetail1", SQLDetail1}
                                      };


            string Left = "1.4";
            double espLinha = 0.5;

            string Bloco1_Left = "4.5";
            double Bloco1_Top = 3.5;

            string Bloco2_Left = "5.8";
            double Bloco2_Top = 7.8;
            double Bloco2_TopC = 7.8;

            string Bloco3_Left = "4.5";
            double Bloco3_Top = 10.3;

            string Bloco4_Left = "4.5";
            double Bloco4_Top = 13.3;

            string Bloco5_Left = "1.5";
            double Bloco5_Top = 21.9;

            Relatorio.CriarRelatório("Carta de Anuência", arrQuerys, "BRA", "Portrait");

            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;


            if (dsFin.Tables["SQLDetail1"].Rows.Count == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Pessoa sem endereço ordem 1.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");

                HttpContext.Current.Response.End();
                return;
            }
            else
            {
                string nValor = dsFin.Tables["SQLDetail1"].Rows[0]["sValor"].ToString();
                string sValorExtenso = dsFin.Tables["SQLDetail1"].Rows[0]["sValorExtenso"].ToString();
                string sDtExtenso = dsFin.Tables["SQLDetail1"].Rows[0]["dtExtenso"].ToString();


                Relatorio.CriarObjImage("Query", "sqlImagem", "FotoEmpresa", "8", "0.5", "Body", "3.89104", "1.74215");

                Relatorio.CriarObjLabel("Fixo", "", "RECEBEMOS DE: ", Left, Bloco1_Top.ToString(), "10", "Black", "L", "Body", "4.51332", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Nome", Bloco1_Left, Bloco1_Top.ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Endereco", Bloco1_Left, (Bloco1_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Endereco2", Bloco1_Left, (Bloco1_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "CNPJ", Bloco1_Left, (Bloco1_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "IE", Bloco1_Left, (Bloco1_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");

                Relatorio.CriarObjLabel("Fixo", "", "A importância de R$ " + nValor + " (" + sValorExtenso + ") \nReferente ao pagamento do título abaixo relacionado:", Left, "6.4", "10", "Black", "L", "Body", "18", "");

                Relatorio.CriarObjLabel("Fixo", "", "DUPLICATA", Left, Bloco2_Top.ToString(), "10", "Black", "L", "Body", "4.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", "VALOR", Left, (Bloco2_Top += espLinha).ToString(), "10", "Black", "L", "Body", "4.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", "DATA DE EMISSÃO", Left, (Bloco2_Top += espLinha).ToString(), "10", "Black", "L", "Body", "4.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", "DATA DE VENCIMENTO", Left, (Bloco2_Top += espLinha).ToString(), "10", "Black", "L", "Body", "4.51332", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Duplicata", Bloco2_Left, Bloco2_TopC.ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "nValor", Bloco2_Left, (Bloco2_TopC += espLinha).ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "dtEmissao", Bloco2_Left, (Bloco2_TopC += espLinha).ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "dtVencimento", Bloco2_Left, (Bloco2_TopC += espLinha).ToString(), "10", "black", "L", "Body", "10", "");

                Relatorio.CriarObjLabel("Fixo", "", "DEVEDOR:", Left, Bloco3_Top.ToString(), "10", "Black", "L", "Body", "4.51332", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Nome", Bloco3_Left, Bloco3_Top.ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Endereco", Bloco3_Left, (Bloco3_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Endereco2", Bloco3_Left, (Bloco3_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "CNPJ", Bloco3_Left, (Bloco3_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "IE", Bloco3_Left, (Bloco3_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");

                Relatorio.CriarObjLabel("Fixo", "", "FAVORECIDO:", Left, Bloco4_Top.ToString(), "10", "Black", "L", "Body", "4.51332", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_Nome", Bloco4_Left, Bloco4_Top.ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_Endereco", Bloco4_Left, (Bloco4_Top += espLinha).ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_Endereco2", Bloco4_Left, (Bloco4_Top += espLinha).ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_CNPJ", Bloco4_Left, (Bloco4_Top += espLinha).ToString(), "10", "black", "L", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_IE", Bloco4_Left, (Bloco4_Top += espLinha).ToString(), "10", "black", "L", "Body", "10", "");

                Relatorio.CriarObjLabel("Fixo", "", "Tendo recebido o valor do título mencionado, damos quitação e AUTORIZAMOS CANCELAR O PROTESTO,", Left, "16.95", "10", "Black", "L", "Body", "18", "");
                Relatorio.CriarObjLabel("Fixo", "", "nos termos da Lei 6690/79, com as alterações introduzidas pela Lei n.º 7401/85.", Left, (16.95 + espLinha).ToString(), "10", "Black", "L", "Body", "18", "");

                Relatorio.CriarObjLabel("Fixo", "", sDtExtenso, Left, "18.6", "10", "Black", "L", "Body", "18", "");

                Relatorio.CriarObjLabel("Fixo", "", "_________________________", Left, "20.3", "12", "Black", "L", "Body", "18", "");
                Relatorio.CriarObjLabel("Fixo", "", sNomeResponsavel, Left, "20.8", "10", "Black", "L", "Body", "18", "");
                Relatorio.CriarObjLabel("Fixo", "", "CPF " + sCPF, Left, "21.3", "10", "Black", "L", "Body", "18", "");

                Relatorio.CriarObjLabel("Fixo", "", "_____________________________________________________", "0", Bloco5_Top.ToString(), "15", "black", "L", "Body", "18", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_Nome", Bloco5_Left, (Bloco5_Top += espLinha + 0.2).ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_Endereco", Bloco5_Left, (Bloco5_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_Endereco2", Bloco5_Left, (Bloco5_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_CNPJ", Bloco5_Left, (Bloco5_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_IE", Bloco5_Left, (Bloco5_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");
                Relatorio.CriarObjLabel("Query", "sqlDetail1", "Empresa_Telefone", Bloco5_Left, (Bloco5_Top += espLinha).ToString(), "10", "black", "B", "Body", "10", "");

                Relatorio.CriarPDF_Excel("Carta de Anuência", 1);
                //Relatorio.Imprimir("Carta de Anuência");
            }
        }


        private void duplicata()
        {
            int sIdioma = 0;
            string asterisco = new String('*', 189);

            ClsReport Relatorio = new ClsReport();

            DataSet DsTable = new DataSet();


            if (nEmpresaLogadaIdioma == "245")
                sIdioma = 1;
            else if (nEmpresaLogadaIdioma == "246")
                sIdioma = 0;
            else if (nEmpresaLogadaIdioma == "247")
                sIdioma = 2;

            string[,] arrSqlQuery;

            string Query1 = "";
            Query1 = " SELECT " +
                        " CONVERT(VARCHAR,a.dtEmissao,103) AS dtEmissao, STR(b.ValorTotalPedido,14,2) AS ValorTotalPedido, c.NotaFiscal, STR(a.Valor,14,2) AS Valor, a.Duplicata, " +
                        " CONVERT(VARCHAR,a.dtVencimento,103) AS dtVencimento, UPPER(d.Nome) AS Nome, d.PessoaID, " +
                        " RTRIM(ISNULL(e.Endereco,SPACE(0))) + SPACE(1) + RTRIM(ISNULL(e.Numero,SPACE(0))) + SPACE(1) + RTRIM(ISNULL(e.Complemento,SPACE(0))) + SPACE(1) + RTRIM(ISNULL(e.Bairro,SPACE(0))) AS Endereco, " +
                        " e.CEP, f.Localidade AS Cidade, g.CodigoLocalidade2 AS UF, ISNULL(dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 101, 111, 0),SPACE(1)) AS CNPJCPF, " +
                        " ISNULL(dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 102, 112, 0),SPACE(1)) AS IERG, " +
                        " dbo.fn_ValorExtenso(a.Valor," + sIdioma + ") AS sValorExtenso " +
                     " FROM " +
                        " Financeiro a WITH(NOLOCK) " +
                            " INNER JOIN Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) " +
                            " INNER JOIN NotasFiscais c WITH(NOLOCK) ON (c.NotaFiscalID = b.NotaFiscalID) " +
                            " INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = a.PessoaID) " +
                            " INNER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (e.PessoaID = a.PessoaID) " +
                            " INNER JOIN Localidades f WITH(NOLOCK) ON (f.LocalidadeID = e.CidadeID) " +
                            " INNER JOIN Localidades g WITH(NOLOCK) ON (g.LocalidadeID = e.UFID) " +
                            " INNER JOIN Operacoes h WITH(NOLOCK) ON (h.OperacaoID = b.TransacaoID) " +
                     " WHERE " +
                        " e.Ordem=1 AND b.TipoPedidoID=602 AND h.Resultado=1 AND c.EstadoID = 67 AND a.FinanceiroID = " + nFinanceiroID;

            arrSqlQuery = new string[,] { { "Query1", Query1 } };


            Relatorio.CriarRelatório("Duplicata", arrSqlQuery, "BRA", "Portrait", "1");

            DataSet dsFin = new DataSet();
            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            for (int i = 0; i < Datateste; i++)
            {
                if (dsFin.Tables[i].Rows.Count == 0)
                    nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não é possivel imprimir Duplicata para essa CFOP.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {
                string sValorExtenso = dsFin.Tables["Query1"].Rows[0]["sValorExtenso"].ToString();

                //Linha1
                Relatorio.CriarObjLabel("Query", "Query1", "Nome", "4.55", "5", "11", " ", "B", "Body", "9.54792", "");
                Relatorio.CriarObjLabel("Query", "Query1", "PessoaID", "15", "5", "11", " ", "B", "Body", "5.54792", "");

                //Linha2
                Relatorio.CriarObjLabel("Query", "Query1", "Endereco", "4.55", "5.5", "11", " ", "B", "Body", "13.54792", "");

                //Linha3
                Relatorio.CriarObjLabel("Query", "Query1", "Cidade", "4.55", "6", "11", " ", "B", "Body", "9.54792", "");
                Relatorio.CriarObjLabel("Query", "Query1", "UF", "15", "6", "11", " ", "B", "Body", "5.54792", "");

                //Linha4
                Relatorio.CriarObjLabel("Query", "Query1", "CEP", "4.55", "6.5", "11", " ", "B", "Body", "9.54792", "");
                Relatorio.CriarObjLabel("Query", "Query1", "Cidade", "6.55", "6.5", "11", " ", "B", "Body", "9.54792", "");
                Relatorio.CriarObjLabel("Query", "Query1", "UF", "10.55", "6.5", "11", " ", "B", "Body", "9.54792", "");

                //Linha5
                Relatorio.CriarObjLabel("Query", "Query1", "CNPJCPF", "4.55", "7", "11", " ", "B", "Body", "7.54792", "");
                Relatorio.CriarObjLabel("Query", "Query1", "IERG", "12.55", "7", "11", " ", "B", "Body", "7.54792", "");

                Relatorio.CriarObjLabel("Fixo", "", sValorExtenso + " " + asterisco, "2.5", "8.5", "11", " ", "B", "Body", "13.54792", "");


                //Tabela
                Relatorio.CriarObjTabela("0.1", "3", "Query1", false);

                //Coluna1
                Relatorio.CriarObjColunaNaTabela(" ", "dtEmissao", false, "3", "Center", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "dtEmissao", "DetailGroup", "Null", "Null", 0, true, "10");

                //Coluna2
                Relatorio.CriarObjColunaNaTabela(" ", "ValorTotalPedido", false, "4", "Center", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "ValorTotalPedido", "DetailGroup", "Null", "Null", 0, true, "10");

                //Coluna3
                Relatorio.CriarObjColunaNaTabela(" ", "NotaFiscal", false, "3", "Center", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "NotaFiscal", "DetailGroup", "Null", "Null", 0, true, "10");

                //Coluna4
                Relatorio.CriarObjColunaNaTabela(" ", "Valor", false, "3", "Center", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup", "Null", "Null", 0, true, "10");

                //Coluna5
                Relatorio.CriarObjColunaNaTabela(" ", "Duplicata", false, "3", "Center", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "Duplicata", "DetailGroup", "Null", "Null", 0, true, "10");

                //Coluna6
                Relatorio.CriarObjColunaNaTabela(" ", "dtVencimento", false, "4.5", "Center", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "dtVencimento", "DetailGroup", "Null", "Null", 0, true, "10");

                Relatorio.TabelaEnd();

                //Exporta 
                Relatorio.CriarPDF_Excel("Duplicata", 1);
            }
        }


        //Descontos Concedidos DescontosConcedidos ID=40184
        private void vendasRecebidas()
        {
            //Recebe variaveis do modalprint_financeiro.js
            string txtFiltro2 = HttpContext.Current.Request.Params["txtFiltro2"];
            string dtInicio = HttpContext.Current.Request.Params["dtInicio"];
            string dtFim = HttpContext.Current.Request.Params["dtFim"];
            string txtDataInicio3 = HttpContext.Current.Request.Params["txtDataInicio3"];
            string txtDataFim3 = HttpContext.Current.Request.Params["txtDataFim3"];
            int selTipoValue = Convert.ToInt32(HttpContext.Current.Request.Params["selTipoValue"]);
            int selTipoSelectedIndex = Convert.ToInt32(HttpContext.Current.Request.Params["selTipoSelectedIndex"]);
            string selTipoInnerText = HttpContext.Current.Request.Params["selTipoInnerText"];

            string nomeRelatorio = "Descontos Concedidos";

            string sFiltro = (txtFiltro2 != "" ? " AND " + txtFiltro2 + " " : "");

            string sFiltroInicio = "";
            if (dtInicio != "null")
            {
                sFiltroInicio = " AND a.dtBalancete >= " + "'" + dtInicio + "' ";
                dtInicio = "Início: " + txtDataInicio3;
            }
            else
                dtInicio = " ";

            string sFiltroFim = "";
            if (dtFim != "null")
            {
                sFiltroFim = " AND a.dtBalancete <= " + "'" + dtFim + "' ";
                dtFim = "Fim: " + txtDataFim3;
            }
            else dtFim = " ";

            if (selTipoInnerText != "")
            {
                selTipoInnerText = "Tipo: " + selTipoInnerText;
            }
            else selTipoInnerText = " ";

            string sFiltroTipo = "";
            if (selTipoSelectedIndex > 0)
                sFiltroTipo = " AND a.TipoOcorrenciaID = " + selTipoValue + " ";


            SQLDetail1 = " SELECT " +
                                " CONVERT(VARCHAR,a.dtBalancete,103) AS 'dtBalancete', a.Valor, f.ItemMasculino AS 'Tipo', b.FinanceiroID, b.PedidoID, b.Duplicata, c.Fantasia AS 'Parceiro', " +
                                " CONVERT(VARCHAR,b.dtEmissao,103) AS 'dtEmissao', e.ItemAbreviado AS 'FormaPagamento', b.PrazoPagamento, CONVERT(VARCHAR,b.dtVencimento,103) AS 'dtVencimento', " +
                                " d.HistoricoPadrao, g.SimboloMoeda, b.Valor AS 'ValorFinanceiro', 1 AS '_Registros' " +
                            " FROM " +
                                " Financeiro_Ocorrencias a WITH(NOLOCK) " +
                                    " INNER JOIN Financeiro b WITH(NOLOCK) ON (b.FinanceiroID = a.FinanceiroID) " +
                                    " INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = b.ParceiroID) " +
                                    " INNER JOIN HistoricosPadrao d WITH(NOLOCK) ON (d.HistoricoPadraoID = b.HistoricoPadraoID) " +
                                    " INNER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (e.ItemID = b.FormaPagamentoID) " +
                                    " INNER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (f.ItemID = a.TipoOcorrenciaID) " +
                                    " INNER JOIN Conceitos g WITH(NOLOCK) ON (g.ConceitoID = b.MoedaID) " +
                            " WHERE " +
                                    " a.Valor IS NOT NULL " +
                                " AND b.EmpresaID = " + nEmpresaLogada + sFiltro + sFiltroInicio + sFiltroFim + sFiltroTipo +
                            " ORDER BY " +
                                " a.dtBalancete, b.FinanceiroID ";

            ClsReport Relatorio = new ClsReport();

            DataSet Ds = new DataSet();

            string[,] arrQuerys;

            arrQuerys = new string[,] { { "SQLDetail1", SQLDetail1 } };

            double headerX = 0;
            double headerY = 0;

            double tabelaX = 0;
            double tabelaY = 0.001;

            Relatorio.CriarRelatório(nomeRelatorio, arrQuerys, "BRA", "Landscape");

            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;


            if (dsFin.Tables["SQLDetail1"].Rows.Count == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há nada para ser impresso.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {
                //Header
                Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, headerX.ToString(), headerY.ToString(), "11", "Black", "B", "Body", "2.5", "");

                Relatorio.CriarObjLabel("Fixo", "", selTipoInnerText, (headerX + 2.7).ToString(), headerY.ToString(), "11", "Black", "L", "Body", "4", "");

                Relatorio.CriarObjLabel("Fixo", "", dtInicio, (headerX + 7.0).ToString(), headerY.ToString(), "11", "Black", "L", "Body", "3.2", "");
                Relatorio.CriarObjLabel("Fixo", "", dtFim, (headerX + 11.2).ToString(), headerY.ToString(), "11", "Black", "L", "Body", "3.2", "");

                Relatorio.CriarObjLabel("Fixo", "", nomeRelatorio, (headerX + 15).ToString(), headerY.ToString(), "11", "Blue", "B", "Body", "5", "");
                Relatorio.CriarObjLabelData("Now", "", "", (headerX + 20).ToString(), headerY.ToString(), "11", "Black", "B", "Body", "2.5", "Dia");


                //Tabela
                Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "SQLDetail1", false);

                Relatorio.CriarObjColunaNaTabela("dtBalancete", "dtBalancete", false, "3.5", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "dtBalancete", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.CriarObjColunaNaTabela("Valor", "Valor", false, "3.1", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.CriarObjColunaNaTabela("Tipo", "Tipo", false, "4.5", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "Tipo", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.CriarObjColunaNaTabela("FinanceiroID", "FinanceiroID", false, "4", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "FinanceiroID", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.CriarObjColunaNaTabela("PedidoID", "PedidoID", false, "3.5", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "PedidoID", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.CriarObjColunaNaTabela("Duplicata", "Duplicata", false, "3.5", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "Duplicata", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.CriarObjColunaNaTabela("Parceiro", "Parceiro", false, "6", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "Parceiro", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.CriarObjColunaNaTabela("dtEmissao", "dtEmissao", false, "3.5", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "dtEmissao", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.CriarObjColunaNaTabela("FormaPagamento", "FormaPagamento", false, "4.5", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "FormaPagamento", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.CriarObjColunaNaTabela("PrazoPagamento", "PrazoPagamento", false, "4.5", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "PrazoPagamento", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.CriarObjColunaNaTabela("dtVencimento", "dtVencimento", false, "4.5", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "dtVencimento", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.CriarObjColunaNaTabela("HistoricoPadrao", "HistoricoPadrao", false, "6", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "HistoricoPadrao", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.CriarObjColunaNaTabela("SimboloMoeda", "SimboloMoeda", false, "4.2", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "SimboloMoeda", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.CriarObjColunaNaTabela("ValorFinanceiro", "ValorFinanceiro", false, "4.5", "Default", "10");
                Relatorio.CriarObjCelulaInColuna("Query", "ValorFinanceiro", "DetailGroup", "Null", "Null", 0, false, "10");

                Relatorio.TabelaEnd();

                Relatorio.CriarPDF_Excel(nomeRelatorio, formato);
            }
        }


        //Contas a Pagar/Receber ID=40183
        private void fluxoCaixa()
        {
            string sTipoFluxo = "";
            string SQLOrderBy = "";
            string sFiltro = "";
            string sFiltroTotal = "";


            //Recebe variaveis do modalprint_financeiro.js
            int nUserID = Convert.ToInt32(HttpContext.Current.Request.Params["nUserID"]);
            bool dirA1 = Convert.ToBoolean(HttpContext.Current.Request.Params["dirA1"]);
            bool dirA2 = Convert.ToBoolean(HttpContext.Current.Request.Params["dirA2"]);
            int selContasValue = Convert.ToInt32(HttpContext.Current.Request.Params["selContasValue"]);
            int selContasSelectedIndex = Convert.ToInt32(HttpContext.Current.Request.Params["selContasSelectedIndex"]);
            int selOrdem = Convert.ToInt32(HttpContext.Current.Request.Params["selOrdem"]);
            int selMoeda = Convert.ToInt32(HttpContext.Current.Request.Params["selMoeda"]);
            string sEstados = HttpContext.Current.Request.Params["sEstados"];
            string sSituacao = HttpContext.Current.Request.Params["sSituacao"];
            string sFormaPagamento = HttpContext.Current.Request.Params["sFormaPagamento"];
            string sVendedor = HttpContext.Current.Request.Params["sVendedor"];
            string sdtInicio = HttpContext.Current.Request.Params["sdtInicio"];
            string sdtFim = HttpContext.Current.Request.Params["sdtFim"];
            string txtFiltro = HttpContext.Current.Request.Params["txtFiltro"];
            bool chkEmpresa = Convert.ToBoolean(HttpContext.Current.Request.Params["chkEmpresa"]);
            bool chkTaxaAtualizacao = Convert.ToBoolean(HttpContext.Current.Request.Params["chkTaxaAtualizacao"]);
            bool chkInadimplencia = Convert.ToBoolean(HttpContext.Current.Request.Params["chkInadimplencia"]);
            string sDataPosicao = HttpContext.Current.Request.Params["sDataPosicao"];
            string sEmpresas = HttpContext.Current.Request.Params["sEmpresas"];
            string DATE_SQL_PARAM = HttpContext.Current.Request.Params["DATE_SQL_PARAM"];
            bool chkDadosSeguradora = Convert.ToBoolean(HttpContext.Current.Request.Params["chkDadosSeguradora"]);
            string strParams = HttpContext.Current.Request.Params["strParams"];
            int iQLinhas = 0;

            string Title = "";

            if (selContasSelectedIndex == 0)
                Title = "Contas a Pagar/Receber";
            else if (selContasSelectedIndex == 1)
                Title = "Contas a Pagar";
            else if (selContasSelectedIndex == 2)
                Title = "Contas a Receber";


            if (selContasValue != 0)
                sTipoFluxo = " AND a.TipoFinanceiroID= " + selContasValue;

            if (selOrdem == 0)
                SQLOrderBy = " ORDER BY a.dtVencimento ";
            else
                SQLOrderBy = " ORDER BY Pessoas.Fantasia ";

            sFiltro = txtFiltro;
            if (sFiltro != "")
                sFiltro = " AND (" + sFiltro + ")";

            if (chkEmpresa)
                sFiltro += " AND (dbo.fn_Pessoa_Empresa(NULL, a.ParceiroID, NULL, NULL) = 0) ";

            if (chkTaxaAtualizacao)
                sFiltro += " AND (a.TaxaAtualizacao > 0) ";

            if (chkInadimplencia)
                sFiltro += " AND (Transacoes.Inadimplencia = 1) ";

            if (!(dirA1 && dirA2))
                sFiltroTotal = " AND (a.ProprietarioID = " + nUserID + ")";

            sFiltroTotal += sTipoFluxo + sFormaPagamento + sVendedor + sEstados + sSituacao + sFiltro;

            string parametro = "103";

            if(sLinguaLogada != "246")
                parametro = "101";



            SQLDetail1 = " CREATE TABLE #FluxoCaixa " +
                            " (PKID INT, Empresa VARCHAR(32), Vencimento_ VARCHAR(32), Vencimento VARCHAR(32), Ano_Mes VARCHAR(8), Atraso INT, Tipo VARCHAR(32), Forma VARCHAR(8),Prazo INT, " +
                            " PessoaID INT, Pessoa VARCHAR(64), Financ INT, E VARCHAR(8), Situacao VARCHAR(8), Pedido INT, Transacao VARCHAR(32), Valor NUMERIC(11,2), SaldoDevedor NUMERIC(11,2), " +
                            " SaldoAtualizado NUMERIC(11,2), Col VARCHAR(8), Colaborador VARCHAR(64), dtEmissao VARCHAR(32), dtFluxoCaixa VARCHAR(32), Duplicata VARCHAR(32), Observacao VARCHAR(64), " +
                            " Observacoes VARCHAR(MAX), ValorTotalPedido NUMERIC(11,2), CPF_CNPJ VARCHAR(32), _Registro INT, Pais VARCHAR(8), UF VARCHAR(8), Cidade VARCHAR(64), CEP VARCHAR(32),  " +
                            " Endereco VARCHAR(64), Bairro VARCHAR(64), Telefone VARCHAR(9), Email VARCHAR(80), NossoNumero VARCHAR(64), /*dtEntrada DATETIME,*/ PedidoObservacao VARCHAR(64), " +
                            " Seguradora VARCHAR(32), ValorSegurado NUMERIC(11,2), SaldoAtualizadoAcumulado NUMERIC(11,2)) " +

                        " INSERT INTO #FluxoCaixa " +
                            " (PKID, Empresa, Vencimento_, Vencimento, Ano_Mes, Atraso, Tipo, Forma, Prazo, PessoaID, Pessoa, Financ, E, Situacao, Pedido, Transacao, " +
                            " Valor, SaldoDevedor, SaldoAtualizado, Col, Colaborador, dtEmissao, dtFluxoCaixa, Duplicata, Observacao, Observacoes, ValorTotalPedido, CPF_CNPJ, _Registro, Pais, UF, " +
                            " Cidade, CEP, Endereco, Bairro, Telefone, Email, NossoNumero, PedidoObservacao, Seguradora, ValorSegurado, SaldoAtualizadoAcumulado) " +

                        "EXEC sp_Financeiro_Relatorio_FluxoCaixa " +
                            sEmpresas + ", " + selMoeda + ", " + sDataPosicao + ", " + DATE_SQL_PARAM + ", " + sdtInicio + ", " +
                            sdtFim + ", '" + sFiltroTotal + "', '" + SQLOrderBy + "', " + (chkDadosSeguradora ? "1" : "0") +

                        " SELECT " +
                            " PKID, Empresa, Vencimento_, Vencimento, Ano_Mes AS Ano_Mes, Atraso, Tipo, Forma, Prazo, PessoaID, Pessoa, Financ, E, Situacao, Pedido, Transacao, " +
                            " dbo.fn_Formata_Numero(Valor, 2, " + parametro + ") AS Valor, dbo.fn_Formata_Numero(SaldoDevedor, 2, " + parametro + ") AS SaldoDevedor, " +
                            " dbo.fn_Formata_Numero(SaldoAtualizado, 2, " + parametro + ") AS SaldoAtualizado, Col, Colaborador, dtEmissao, dtFluxoCaixa, Duplicata, Observacao, Observacoes, " +
                            " dbo.fn_Formata_Numero(ValorTotalPedido, 2, " + parametro + ") AS ValorTotalPedido, CPF_CNPJ, _Registro, Pais, UF, Cidade, CEP, Endereco, Bairro, Telefone, Email, " +
                            " NossoNumero, PedidoObservacao, Seguradora, ValorSegurado, dbo.fn_Formata_Numero(SaldoAtualizadoAcumulado, 2, " + parametro + ") AS SaldoAtualizadoAcumulado " +
                        " FROM #FluxoCaixa " +

                        " DROP TABLE #FluxoCaixa ";

            var Header_Body = (formato == 1 ? "Header" : "Body");

            ClsReport Relatorio = new ClsReport();

            string[,] arrQuerys;

            arrQuerys = new string[,] { { "SQLDetail1", SQLDetail1 } };


            if (formato == 1)
            {
                Relatorio.CriarRelatório(Title, arrQuerys, "BRA", "Landscape", "Default", "Default", "Default", true);
            }
            else
            {
                Relatorio.CriarRelatório(Title, arrQuerys, "BRA", "Landscape");
            }



            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            iQLinhas = dsFin.Tables["SQLDetail1"].Rows.Count;


            if (iQLinhas == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {

                if (formato == 1)
                {
                    double headerX = 0.2;
                    double headerY = 0.4;
                    double tabelaTitulo = 1.40;

                    double tabelaX = 0.2;
                    double tabelaY = 0;
                    

                    //Labels Cabeçalho
                    Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "12", "black", "B", "Header", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 12.5).ToString(), headerY.ToString(), "12", "#0113a0", "B", "Header", "5", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 23.4).ToString(), headerY.ToString(), "10", "black", "B", "Header", "3.72187", "Horas");

                    Relatorio.CriarNumeroPagina((headerX + 28).ToString(), (headerY).ToString(), "10", "Black", "B", false);

                    //Parametros(Filtros) selecionados
                    Relatorio.CriarObjLabel("Fixo", "", strParams, (headerX).ToString(), (headerY + 0.5).ToString(), "8", "black", "B", "Header", "29", "");

                    //Linha
                    Relatorio.CriarObjLinha((headerX).ToString(), (headerY + 0.9).ToString(), "29", "", "Header");

                    //Cabeçalho da tabela
                    Relatorio.CriarObjLabel("Fixo", "", "Vencimento", (headerX).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.84", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Atr", (headerX+1.84).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "0.78", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Tipo", (headerX + 2.62).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "0.91", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Forma", (headerX + 3.53).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.13", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Prazo", (headerX + 4.8).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.05", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Pessoa", (headerX + 5.71).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "3.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Financ", (headerX + 9.59).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.42", "");
                    Relatorio.CriarObjLabel("Fixo", "", "E", (headerX + 10.63).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "0.49", "");
                    Relatorio.CriarObjLabel("Fixo", "", "S", (headerX + 11.12).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "0.7", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Pedido", (headerX + 12.159).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.42", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Transação", (headerX + 13.24).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "3.30", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Valor", (headerX + 17.56).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.92", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Saldo Atualiz", (headerX + 18.55).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "2", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Acumulado", (headerX + 20.88).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "2.08", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Col", (headerX + 22.52).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.34", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Duplicata", (headerX + 23.85).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "2.03", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Observação", (headerX + 25.92).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "3.5", "");
                    

                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "SQLDetail1", false);
                    
                    Relatorio.CriarObjColunaNaTabela(" ", "Vencimento_", false, "1.84", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Vencimento_", "DetailGroup", "Null", "Null", 0, false, "8");
                    
                    Relatorio.CriarObjColunaNaTabela(" ", "Atraso", false, "0.78", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Atraso", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Tipo", false, "0.91", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Tipo", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Forma", false, "1.13", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Forma", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Prazo", false, "1.05", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Prazo", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Pessoa", false, "3.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Pessoa", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Financ", false, "1.42", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Financ", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "E", false, "0.49", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "E", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Situacao", false, "0.7", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Situacao", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Pedido", false, "1.42", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Pedido", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Transacao", false, "3.3", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Transacao", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Valor", false, "1.92", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "SaldoAtualizado", false, "2", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "SaldoAtualizado", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "SaldoAtualizadoAcumulado", false, "2.08", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "SaldoAtualizadoAcumulado", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Col", false, "1.34", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Col", "DetailGroup", "Null", "Null", 0, false, "8");
                    
                    Relatorio.CriarObjColunaNaTabela(" ", "Duplicata", false, "2.03", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Duplicata", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Observacao", false, "3.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Observacao", "DetailGroup", "Null", "Null", 0, false, "8");
                    
                    Relatorio.TabelaEnd();

                    Relatorio.CriarObjLinha((tabelaX).ToString(), (tabelaY + 0.1).ToString(), "29", "", "Body");

                    Relatorio.CriarObjLabel("Fixo", "", "Quantidade de Registros: " + iQLinhas, (tabelaX).ToString(), (tabelaY + 0.15).ToString(), "8", "black", "B", "Body", "29", "");
                }

                else if (formato == 2)
                {
                    double tabelaX = 0;
                    double tabelaY = 0;

                    //Tabela
                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "SQLDetail1", false);

                    Relatorio.CriarObjColunaNaTabela("Empresa", "Empresa", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Empresa", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Vencimento", "Vencimento", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Vencimento", "DetailGroup", "Null", "Null", 0, false, "10");


                    Relatorio.CriarObjColunaNaTabela("AnoMes", "Ano_Mes", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Ano_Mes", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Atraso", "Atraso", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Atraso", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Tipo", "Tipo", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Tipo", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Forma", "Forma", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Forma", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Prazo", "Prazo", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Prazo", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("PessoaID", "PessoaID", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "PessoaID", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Pessoa", "Pessoa", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Pessoa", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Financ", "Financ", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Financ", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("E", "E", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "E", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Situacao", "Situacao", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Situacao", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Pedido", "Pedido", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Pedido", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Transacao", "Transacao", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Transacao", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Valor", "Valor", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("SaldoDevedor", "SaldoDevedor", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "SaldoDevedor", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("SaldoAtualizado", "SaldoAtualizado", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "SaldoAtualizado", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Acumulado", "SaldoAtualizadoAcumulado", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "SaldoAtualizadoAcumulado", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Colaborador", "Colaborador", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Colaborador", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("dtEmissao", "dtEmissao", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtEmissao", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("dtFluxoCaixa", "dtFluxoCaixa", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtFluxoCaixa", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Duplicata", "Duplicata", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Duplicata", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Observacao", "Observacao", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Observacao", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Observacoes", "Observacoes", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Observacoes", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("ValorTotalPedido", "ValorTotalPedido", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorTotalPedido", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("CPF_CNPJ", "CPF_CNPJ", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "CPF_CNPJ", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Pais", "Pais", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Pais", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("UF", "UF", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "UF", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Cidade", "Cidade", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Cidade", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("CEP", "CEP", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "CEP", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Endereco", "Endereco", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Endereco", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Bairro", "Bairro", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Bairro", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Telefone", "Telefone", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Telefone", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Email", "Email", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Email", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("NossoNumero", "NossoNumero", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "NossoNumero", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("PedidoObservacao", "PedidoObservacao", true, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "PedidoObservacao", "DetailGroup", "Null", "Null", 0, false, "10");

                    if (chkDadosSeguradora)
                    {
                        Relatorio.CriarObjColunaNaTabela("Seguradora", "Seguradora", true, "3.5", "Default", "10");
                        Relatorio.CriarObjCelulaInColuna("Query", "Seguradora", "DetailGroup", "Null", "Null", 0, false, "10");

                        Relatorio.CriarObjColunaNaTabela("ValorSegurado", "ValorSegurado", true, "3.5", "Default", "10");
                        Relatorio.CriarObjCelulaInColuna("Query", "ValorSegurado", "DetailGroup", "Null", "Null", 0, false, "10");
                    }

                    Relatorio.TabelaEnd();
                }

                Relatorio.CriarPDF_Excel(Title, formato);
            }
        }

        private void campanhas()
        {
            string Title = "Campanhas";
            int iQLinhas = 0;


            //Resumido
            SQLDetail1 = "EXEC sp_Financeiro_Campanhas " +
                            selEmpresaCampanha + ", " +
                            selFabricante + ", " +
                            selCampanhas + ", " +
                            selResponsavelID + ", " +
                            chkMoedaSistema + ", " +
                            chkPendentes + ", " +
                            txtDataInicio2 + ", "+
                            txtDataFim2 + ", " +
                            txtFiltro3 + ", 2," +
                            sLinguaLogada;

            //Detalhe
            SQLDetail2 = "EXEC sp_Financeiro_Campanhas " +
                            selEmpresaCampanha + ", " +
                            selFabricante + ", " +
                            selCampanhas + ", " +
                            selResponsavelID + ", " +
                            chkMoedaSistema + ", " +
                            chkPendentes + ", " +
                            txtDataInicio2 + ", " +
                            txtDataFim2 + ", " +
                            txtFiltro3 + ", 3,"+
                            sLinguaLogada;

            //Totais
            SQLDetail3 = "EXEC sp_Financeiro_Campanhas " +
                            selEmpresaCampanha + ", " +
                            selFabricante + ", " +
                            selCampanhas + ", " +
                            selResponsavelID + ", " +
                            chkMoedaSistema + ", " +
                            chkPendentes + ", " +
                            txtDataInicio2 + ", " +
                            txtDataFim2 + ", " +
                            txtFiltro3 + ", 4,"+
                            sLinguaLogada;

            var Header_Body = (formato == 1 ? "Header" : "Body");

            ClsReport Relatorio = new ClsReport();

            string[,] arrQuerys;
            string tabelaContar = "SQLDetail1";

            if (formato == 1 && chkResumido == "true")
            {
                arrQuerys = new string[,] { { "SQLDetail1", SQLDetail1 },
                                        { "SQLDetail3", SQLDetail3 }};
            }
            else if (formato == 1 && chkResumido == "false")
            {
                arrQuerys = new string[,] { { "SQLDetail1", SQLDetail1 },
                                        { "SQLDetail2", SQLDetail2 },
                                        { "SQLDetail3", SQLDetail3 }};
            }
            else if (formato == 2 && chkResumido == "true")
            {
                arrQuerys = new string[,] {{ "SQLDetail1", SQLDetail1 }};
            }
            else //if (formato == 2 && chkResumido == "false")
            {
                arrQuerys = new string[,] { { "SQLDetail2", SQLDetail2 } };
                tabelaContar = "SQLDetail2";
            }

            string[,] arrIndexKey = new string[,] { { "SQLDetail2", "Indice", "Indice" } };

            if (formato == 1)
            {
                Relatorio.CriarRelatório(Title, arrQuerys, "BRA", "Landscape", "1.8", "Default", "Default", true);
            }
            else
            {
                Relatorio.CriarRelatório(Title, arrQuerys, "BRA", "Excel");
            }



            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            iQLinhas = dsFin.Tables[tabelaContar].Rows.Count;


            if (iQLinhas == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {

                if (formato == 1)
                {
                    double headerX = 0.2;
                    double headerY = 0.4;
                    double tabelaTitulo = 1.40;

                    double tabelaX = 0.2;
                    double tabelaY = 0;


                    //Labels Cabeçalho
                    Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "12", "black", "B", "Header", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 12.5).ToString(), headerY.ToString(), "12", "#0113a0", "B", "Header", "5", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 23).ToString(), headerY.ToString(), "10", "black", "B", "Header", "3.72187", "Horas");

                    Relatorio.CriarNumeroPagina((headerX + 27).ToString(), (headerY).ToString(), "10", "Black", "B", false);

                    //Parametros(Filtros) selecionados
                    Relatorio.CriarObjLabel("Fixo", "", strParams, (headerX).ToString(), (headerY + 0.5).ToString(), "8", "black", "B", "Header", "29", "");

                    //Linha
                    Relatorio.CriarObjLinha((headerX).ToString(), (headerY + 0.9).ToString(), "29", "", "Header");

                    if(chkResumido == "true")
                    {
                        //Cabeçalho
                        Relatorio.CriarObjLabel("Fixo", "", "Empresa", (headerX).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "3", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Tipo", (headerX += 3).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.6", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Fabricante", (headerX += 1.6).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "4", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Pedidos", (headerX += 3.8).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "3", "Right");
                        Relatorio.CriarObjLabel("Fixo", "", "Quant", (headerX += 3).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "3", "Right");
                        Relatorio.CriarObjLabel("Fixo", "", "Valor Total", (headerX += 3).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "3", "Right");
                        Relatorio.CriarObjLabel("Fixo", "", "Saldo Devedor", (headerX += 3).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "3", "Right");
                        Relatorio.CriarObjLabel("Fixo", "", "Recebido", (headerX += 3).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "3", "Right");

                        //Tabela
                        Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "SQLDetail1", false);

                        Relatorio.CriarObjColunaNaTabela(" ", "Empresa", false, "3", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Empresa", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "TipoCampanha", false, "1.6", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "TipoCampanha", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "Fabricante", false, "4", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Fabricante", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "Pedido", false, "3", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pedido", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "Quantidade", false, "3", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "ValorTotal", false, "3", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "SaldoDevedor", false, "3", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "SaldoDevedor", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "Recebido", false, "3", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Recebido", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.TabelaEnd();

                        Relatorio.CriarObjLinha((tabelaX).ToString(), (tabelaY + 0.1).ToString(), "29", "", "Body");

                        //Linha de totais
                        Relatorio.CriarObjLabel("Fixo", "", "Totais", (tabelaX).ToString(), (tabelaY + 0.15).ToString(), "8", "black", "B", "Body", "3", "");
                        Relatorio.CriarObjLabel("Query", "SQLDetail3", "Pedidos", "8.7", (tabelaY + 0.15).ToString(), "8", "black", "B", "Body", "3", "Right");
                        Relatorio.CriarObjLabel("Query", "SQLDetail3", "Quantidade", "11.7", (tabelaY + 0.15).ToString(), "8", "black", "B", "Body", "3", "Right");
                        Relatorio.CriarObjLabel("Query", "SQLDetail3", "ValorTotal", "14.7", (tabelaY + 0.15).ToString(), "8", "black", "B", "Body", "3", "Right");
                        Relatorio.CriarObjLabel("Query", "SQLDetail3", "SaldoDevedor", "17.7", (tabelaY + 0.15).ToString(), "8", "black", "B", "Body", "3", "Right");
                        Relatorio.CriarObjLabel("Query", "SQLDetail3", "Recebido", "20.7", (tabelaY + 0.15).ToString(), "8", "black", "B", "Body", "3", "Right");
                    }
                    else if (chkResumido == "false")
                    {
                        //Cabeçalho
                        Relatorio.CriarObjLabel("Fixo", "", "Empresa", (headerX).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.5", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Tipo", (headerX += 1.5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Fabricante", (headerX += 1).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "2", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Data", (headerX += 2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.2", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Pedido", (headerX += 1.2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.3", "");
                        Relatorio.CriarObjLabel("Fixo", "", "NF", (headerX += 1.3).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Parceiro", (headerX += 1).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "2", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Identificador", (headerX += 2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "2", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Codigo", (headerX += 2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.2", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Resp", (headerX += 1.2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "2", "");
                        Relatorio.CriarObjLabel("Fixo", "", "ID", (headerX += 2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.2", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Produto", (headerX += 1.2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "2", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Modelo", (headerX += 2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "2", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Quant", (headerX += 2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.2", "Right");
                        Relatorio.CriarObjLabel("Fixo", "", "$", (headerX += 1.2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "0.7", "Right");
                        Relatorio.CriarObjLabel("Fixo", "", "Valor Unit", (headerX += 0.7).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.7", "Right");
                        Relatorio.CriarObjLabel("Fixo", "", "Valor Total", (headerX += 1.7).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.7", "Right");
                        Relatorio.CriarObjLabel("Fixo", "", "Saldo Dev", (headerX += 1.7).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.8", "Right");
                        Relatorio.CriarObjLabel("Fixo", "", "Recebido", (headerX += 1.8).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.7", "Right");

                        //Tabela
                        //Tabela
                        Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), arrIndexKey, false);

                        Relatorio.CriarObjColunaNaTabela(" ", "Empresa", false, "1.5", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Empresa", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "TipoCampanha", false, "1", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "TipoCampanha", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Fabricante", false, "2", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Fabricante", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Data", false, "1.2", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Pedido", false, "1.3", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pedido", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "NF", false, "1", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "NF", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Parceiro", false, "2", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Parceiro", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Identificador", false, "2", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Identificador", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Codigo", false, "1.2", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Codigo", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Responsavel", false, "2", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Responsavel", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "ProdutoID", false, "1.2", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Produto", false, "2", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Modelo", false, "2", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Modelo", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Quantidade", false, "1.2", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Moeda", false, "0.7", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Moeda", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "ValorUnitario", false, "1.7", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "ValorUnitario", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "ValorTotal", false, "1.7", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "SaldoDevedor", false, "1.8", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "SaldoDevedor", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Recebido", false, "1.7", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Recebido", "DetailGroup", "Null", "Null", 0, false, "6");

                        Relatorio.TabelaEnd();

                        //Totais
                        Relatorio.CriarObjLinha((tabelaX).ToString(), (tabelaY + 0.1).ToString(), "29", "", "Body");

                        Relatorio.CriarObjTabela(tabelaX.ToString(), (tabelaY + 0.15).ToString(), "SQLDetail3", false);

                        Relatorio.CriarObjColunaNaTabela(" ", "Totais", false, "6.5", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Totais", "DetailGroup", "Null", "Null", 0, true, "7");

                        Relatorio.CriarObjColunaNaTabela(" ", "Pedidos", false, "13.1", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pedidos", "DetailGroup", "Null", "Null", 0, true, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Quantidade", false, "2", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup", "Null", "Null", 0, true, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "ValorTotal", false, "4.1", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup", "Null", "Null", 0, true, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "SaldoDevedor", false, "1.8", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "SaldoDevedor", "DetailGroup", "Null", "Null", 0, true, "6");

                        Relatorio.CriarObjColunaNaTabela(" ", "Recebido", false, "1.7", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Recebido", "DetailGroup", "Null", "Null", 0, true, "6");

                        Relatorio.TabelaEnd();
                    }
                }

                else if (formato == 2)
                {

                    if (chkResumido == "true")
                    {
                        //Tabela
                        Relatorio.CriarObjTabela("0", "0", "SQLDetail1", true,false);

                        Relatorio.CriarObjColunaNaTabela("EmpresaID", "EmpresaID", true, "EmpresaID");
                        Relatorio.CriarObjCelulaInColuna("Query", "EmpresaID", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Empresa", "Empresa", true, "Empresa");
                        Relatorio.CriarObjCelulaInColuna("Query", "Empresa", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("TipoCampanha", "TipoCampanha", true, "TipoCampanha");
                        Relatorio.CriarObjCelulaInColuna("Query", "TipoCampanha", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Fabricante", "Fabricante", true, "Fabricante");
                        Relatorio.CriarObjCelulaInColuna("Query", "Fabricante", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Pedidos", "Pedido", true, "Pedidos");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pedido", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Quantidade", "Quantidade", true, "Pedidos");
                        Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Valor Total", "ValorTotal", true, "Valor Total");
                        Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Saldo Devedor", "SaldoDevedor", true, "Saldo Devedor");
                        Relatorio.CriarObjCelulaInColuna("Query", "SaldoDevedor", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Recebido", "Recebido", true, "Recebido");
                        Relatorio.CriarObjCelulaInColuna("Query", "Recebido", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Registros", "Registros", true, "Registros");
                        Relatorio.CriarObjCelulaInColuna("Query", "Registros", "DetailGroup", "Null", "Null");

                        Relatorio.TabelaEnd();
                    }
                    else if (chkResumido == "false")
                    {
                        //Tabela
                        Relatorio.CriarObjTabela("0", "0", "SQLDetail2", true, false);

                        Relatorio.CriarObjColunaNaTabela("CampanhaID", "CampanhaID", true, "CampanhaID");
                        Relatorio.CriarObjCelulaInColuna("Query", "CampanhaID", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("EmpresaID", "EmpresaID", true, "EmpresaID");
                        Relatorio.CriarObjCelulaInColuna("Query", "EmpresaID", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Empresa", "Empresa", true, "Empresa");
                        Relatorio.CriarObjCelulaInColuna("Query", "Empresa", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("TipoCampanha", "TipoCampanha", true, "TipoCampanha");
                        Relatorio.CriarObjCelulaInColuna("Query", "TipoCampanha", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Fabricante", "Fabricante", true, "Fabricante");
                        Relatorio.CriarObjCelulaInColuna("Query", "Fabricante", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Data", "Data", true, "Data");
                        Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Ano/Mes", "AnoMes", true, "Ano/Mês");
                        Relatorio.CriarObjCelulaInColuna("Query", "AnoMes", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("PedidoID", "Pedido", true, "PedidoID");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pedido", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("NF", "NF", true, "NF");
                        Relatorio.CriarObjCelulaInColuna("Query", "NF", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Parceiro", "Parceiro", true, "Parceiro");
                        Relatorio.CriarObjCelulaInColuna("Query", "Parceiro", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Identificador", "Identificador", true, "Identificador");
                        Relatorio.CriarObjCelulaInColuna("Query", "Identificador", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Codigo", "Codigo", true, "Codigo");
                        Relatorio.CriarObjCelulaInColuna("Query", "Codigo", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Responsavel", "Responsavel", true, "Responsavel");
                        Relatorio.CriarObjCelulaInColuna("Query", "Responsavel", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Devedor", "Devedor", true, "Devedor");
                        Relatorio.CriarObjCelulaInColuna("Query", "Devedor", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("ProdutoID", "ProdutoID", true, "ProdutoID");
                        Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Produto", "Produto", true, "Produto");
                        Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Modelo", "Modelo", true, "Modelo");
                        Relatorio.CriarObjCelulaInColuna("Query", "Modelo", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Quantidade", "Quantidade", true, "Quantidade");
                        Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("$", "Moeda", true, "$");
                        Relatorio.CriarObjCelulaInColuna("Query", "Moeda", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("ValorUnitario", "ValorUnitario", true, "ValorUnitario");
                        Relatorio.CriarObjCelulaInColuna("Query", "ValorUnitario", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("ValorTotal", "ValorTotal", true, "ValorTotal");
                        Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Saldo Devedor", "SaldoDevedor", true, "Saldo Devedor");
                        Relatorio.CriarObjCelulaInColuna("Query", "SaldoDevedor", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Recebido", "Recebido", true, "Recebido");
                        Relatorio.CriarObjCelulaInColuna("Query", "Recebido", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Campanha", "Campanha", true, "Campanha");
                        Relatorio.CriarObjCelulaInColuna("Query", "Campanha", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("PartNumber", "PartNumber", true, "PartNumber");
                        Relatorio.CriarObjCelulaInColuna("Query", "PartNumber", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Registros", "Registros", true, "Registros");
                        Relatorio.CriarObjCelulaInColuna("Query", "Registros", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Inicio_Campanha", "InicioCampanha", true, "Inicio_Campanha");
                        Relatorio.CriarObjCelulaInColuna("Query", "InicioCampanha", "DetailGroup", "Null", "Null");

                        Relatorio.CriarObjColunaNaTabela("Fim_Campanha", "FimCampanha", true, "Fim_Campanha");
                        Relatorio.CriarObjCelulaInColuna("Query", "FimCampanha", "DetailGroup", "Null", "Null");

                        Relatorio.TabelaEnd();
                    }


                }

                Relatorio.CriarPDF_Excel(Title, formato);
            }
        }

        private void DepositoBancario()
        {
            //Recebe variaveis do modalprint_financeiro.js
            string Title = Convert.ToString(HttpContext.Current.Request.Params["sTitle"]);
            string nEmpresaData7 = Convert.ToString(HttpContext.Current.Request.Params["nEmpresaData7"]);
            string nEmpresaData8 = Convert.ToString(HttpContext.Current.Request.Params["nEmpresaData8"]);
            string sEstado = Convert.ToString(HttpContext.Current.Request.Params["sEstado"]);
            string dtData = Convert.ToString(HttpContext.Current.Request.Params["dtData"]);
            string sMoeda = Convert.ToString(HttpContext.Current.Request.Params["sMoeda"]);
            string nTotal = Convert.ToString(HttpContext.Current.Request.Params["nTotal"]);
            string sSelPadrao = HttpContext.Current.Request.Params["sSelPadrao"].ToString();

            string _sLinguaLogada = "103";
            if (sLinguaLogada != "246")
                _sLinguaLogada = "101";

            string SQL1 = "";
            string SQL2 = "";
            string SQL3 = "";


            SQL1 = " SELECT " +
                        " 0 AS Indice,  dbo.fn_Numero_Formata(dbo.fn_Financeiro_DepositoBancario_Posicao(a.FinanceiroID, 4), 2, 1, " + _sLinguaLogada + ") AS Valor, 1 AS Dinheiro " +
                    " FROM " +
                        " Financeiro a WITH(NOLOCK) " +
                    " WHERE " +
                        " a.FinanceiroID = " + nFinanceiroID + " AND ISNULL(dbo.fn_Financeiro_DepositoBancario_Posicao(a.FinanceiroID, 4), 0) > 0 " +

                    " UNION ALL " +

                    " SELECT " +
                        " 1 AS Indice, dbo.fn_Numero_Formata(b.Valor, 2, 1, " + _sLinguaLogada + ") AS Valor, 0 AS Dinheiro " +
                    " FROM " +
                        " Financeiro_ValoresDepositados a WITH(NOLOCK) " +
                            " INNER JOIN ValoresLocalizar b WITH(NOLOCK) ON(b.ValorID = a.ValorID) " +
                    " WHERE " +
                        " (a.FinanceiroID = " + nFinanceiroID + " AND b.FormaPagamentoID = 1032) ";

            SQL2 = " SELECT " +
                        " CONVERT(VARCHAR(10), f.dtVencimento, " + _sLinguaLogada + ") AS Data, " +
                        " dbo.fn_Numero_Formata(dbo.fn_Financeiro_DepositoBancario_Posicao(a.FinanceiroID, 6), 2, 1, " + _sLinguaLogada + ") AS ValorTotal, " +
                        " dbo.fn_Numero_Formata(dbo.fn_Financeiro_DepositoBancario_Posicao(a.FinanceiroID, 4), 2, 1, " + _sLinguaLogada + ") AS ValorDinTotal, " +
                        " dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) AS Conta, e.Fantasia " +
                    " FROM " +
                        " ValoresLocalizar_Financeiros a WITH(NOLOCK) " +
                            " INNER JOIN ValoresLocalizar b WITH(NOLOCK) ON(b.ValorID = a.ValorID) " +
                            " INNER JOIN RelacoesPessoas_Contas c WITH(NOLOCK) ON(c.RelPesContaID = b.RelPesContaID) " +
                            " INNER JOIN RelacoesPessoas d WITH(NOLOCK) ON(d.RelacaoID = c.RelacaoID) " +
                            " INNER JOIN Pessoas e WITH(NOLOCK) ON(e.PessoaID = d.ObjetoID) " +
                            " INNER JOIN Financeiro f WITH(NOLOCK) ON(f.FinanceiroID = a.FinanceiroID) " +
                    " WHERE a.FinanceiroID = " + nFinanceiroID;

            if (sSelPadrao == "1")
            {
                SQL3 = " SELECT " +
                            " 1 AS Indice, a.ValorID, c.RecursoAbreviado AS Est, dbo.fn_Tradutor(d.ItemMasculino, " + nEmpresaData7 + ", " + nEmpresaData8 + ", NULL) AS Forma, b.MotivoDevolucao, " +
                            " b.Documento, b.Emitente, b.dtEmissao, b.dtApropriacao, b.BancoAgencia, b.NumeroDocumento, f.SimboloMoeda AS Moeda, " +
                            " dbo.fn_Formata_Numero(b.Valor,2," + _sLinguaLogada + ") AS 'Valor', b.Identificador, b.FormaPagamentoID AS Ordem, " +
                                " (SELECT dbo.fn_Formata_Numero(SUM(b.Valor), 2, " + _sLinguaLogada + ") " +
                                " FROM Financeiro_ValoresDepositados a WITH(NOLOCK) " +
                                    " INNER JOIN ValoresLocalizar b WITH(NOLOCK) ON(a.ValorID = b.ValorID) " +
                                    " LEFT OUTER JOIN Pessoas e WITH(NOLOCK) ON(b.PessoaID = e.PessoaID) " +
                                    " INNER JOIN Recursos c WITH(NOLOCK) ON(b.EstadoID = c.RecursoID) " +
                                    " INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON(b.FormaPagamentoID = d.ItemID) " +
                                    " INNER JOIN Conceitos f WITH(NOLOCK) ON(b.MoedaID = f.ConceitoID) " +
                                " WHERE(a.FinanceiroID = " + nFinanceiroID + ")) AS Soma " +
                            " FROM " +
                                " Financeiro_ValoresDepositados a WITH(NOLOCK) " +
                                    " INNER JOIN ValoresLocalizar b WITH(NOLOCK) ON(a.ValorID = b.ValorID) " +
                                    " LEFT OUTER JOIN Pessoas e WITH(NOLOCK)ON(b.PessoaID = e.PessoaID) " +
                                    " INNER JOIN Recursos c WITH(NOLOCK) ON(b.EstadoID = c.RecursoID) " +
                                    " INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON(b.FormaPagamentoID = d.ItemID) " +
                                    " INNER JOIN Conceitos f WITH(NOLOCK) ON(b.MoedaID = f.ConceitoID) " +
                            " WHERE (a.FinanceiroID = " + nFinanceiroID + ") " +
                            " ORDER BY Indice, Ordem ";
            }
            else if (sSelPadrao == "0")
            {
                SQL3 = " SELECT " +
                                    " a.ValorID, c.RecursoAbreviado AS Est, dbo.fn_Tradutor(d.ItemAbreviado, " + nEmpresaData7 + ", " + nEmpresaData8 + ", NULL) AS Forma, b.MotivoDevolucao, " +
                                    " b.Documento, b.Emitente, CONVERT(VARCHAR(10), b.dtEmissao, " + _sLinguaLogada + ") AS dtEmissao, " +
                                    " CONVERT(VARCHAR(10), b.dtApropriacao, " + _sLinguaLogada + ") AS dtApropriacao, b.BancoAgencia, b.NumeroDocumento, f.SimboloMoeda AS Moeda, " +
                                    " dbo.fn_Formata_Numero(b.Valor,2," + _sLinguaLogada + ") AS 'Valor', b.Identificador, " +
                                        " ( SELECT dbo.fn_Formata_Numero(SUM(b.Valor), 2, 103) " +
                                            " FROM Financeiro_ValoresDepositados a WITH(NOLOCK) " +
                                                " INNER JOIN ValoresLocalizar b WITH(NOLOCK) ON(a.ValorID = b.ValorID) " +
                                                " LEFT OUTER JOIN Pessoas e WITH(NOLOCK) ON(b.PessoaID = e.PessoaID) " +
                                                " INNER JOIN Recursos c WITH(NOLOCK) ON(b.EstadoID = c.RecursoID) " +
                                                " INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON(b.FormaPagamentoID = d.ItemID) " +
                                                " INNER JOIN Conceitos f WITH(NOLOCK) ON(b.MoedaID = f.ConceitoID) " +
                                            " WHERE(a.FinanceiroID = " + nFinanceiroID + ") " +
                                        " ) AS 'Soma' " +
                                " FROM " +
                                    " Financeiro_ValoresDepositados a WITH(NOLOCK) " +
                                        " INNER JOIN ValoresLocalizar b WITH(NOLOCK) ON (a.ValorID = b.ValorID) " +
                                        " LEFT OUTER JOIN Pessoas e WITH(NOLOCK) ON (b.PessoaID = e.PessoaID) " +
                                        " INNER JOIN Recursos c WITH(NOLOCK) ON (b.EstadoID = c.RecursoID) " +
                                        " INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (b.FormaPagamentoID = d.ItemID) " +
                                        " INNER JOIN Conceitos f WITH(NOLOCK) ON (b.MoedaID = f.ConceitoID) " +
                                " WHERE (a.FinanceiroID = " + nFinanceiroID + ") " +
                                " ORDER BY b.FormaPagamentoID, b.ValorID ";
            }


            ClsReport Relatorio = new ClsReport();

            string[,] arrQuerys = { };

            if (sSelPadrao == "1")
            {
                arrQuerys = new string[,] { { "SQL1", SQL1 },
                                        { "SQL2", SQL2 },
                                        { "SQL3", SQL3 } };

                Relatorio.CriarRelatório(Title, arrQuerys, "USA", "Portrait", "10.27", "Default", "0.83", false, "Default", false, "0.83");
            }
            else if(sSelPadrao == "0")
            {
                arrQuerys = new string[,] {{ "SQL2", SQL2 },
                                            { "SQL3", SQL3 } };

                if (formato == 1)
                    Relatorio.CriarRelatório(Title, arrQuerys, "BRA", "Portrait", "1.5", "Default", "Default", true);
                else if (formato == 2)
                    Relatorio.CriarRelatório(Title, arrQuerys, "BRA", "Landscape");
            }

            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            int iQLinhas = 0;

            if (sSelPadrao == "1")
            {
                iQLinhas = dsFin.Tables["SQL1"].Rows.Count;
            }
            else if(sSelPadrao == "0")
            {
                iQLinhas = dsFin.Tables["SQL3"].Rows.Count;
            }


            if (iQLinhas == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {
                
                if (formato == 1) //SE PDF
                {
                    if (sSelPadrao == "1") //SE PADRÃO USA
                    {
                        double nTopTicket = 0.085;
                        double nTopOld = 0;
                        double nLeftTicket = 4.8;
                        double nLeftOld = 0;
                        double nTopOld2 = 0;

                        //Cabeçalho com ID
                        Relatorio.CriarObjLabel("Fixo", "", "#" + nFinanceiroID, (nLeftTicket).ToString(), (nTopTicket).ToString(), "12", "black", "B", "Header", "4", "");

                        double nCounter = 0;
                        bool bCash = false;

                        nTopOld = nTopTicket;
                        nLeftOld = nLeftTicket;
                        nLeftTicket = 7.8;
                        nTopTicket = 0.11;
                        nTopOld2 = nTopTicket;

                        //While, para incluir valores
                        int i = 0;
                        while (i < iQLinhas)
                        {
                            nCounter++;

                            if (nCounter <= 19)
                            {
                                if ((nCounter == 1) && (dsFin.Tables["SQL1"].Rows[i]["Dinheiro"].ToString() == "0"))
                                {
                                    nTopTicket += 0.73;
                                    bCash = true;
                                    continue;
                                }

                                Relatorio.CriarObjLabel("Fixo", "", dsFin.Tables["SQL1"].Rows[i]["Valor"].ToString(), (nLeftTicket).ToString(), (nTopTicket).ToString(), "10", "black", "L", "Header", "3", "Right");

                                nTopTicket += 0.72;

                                if (nCounter % 7 == 0)
                                {
                                    nLeftTicket += 4.9;
                                    nTopTicket = nTopOld2;
                                }
                            }

                            i++;
                        }

                        nTopTicket = nTopOld;
                        nLeftTicket = nLeftOld;

                        
                        if (dsFin.Tables["SQL2"].Rows.Count == 1)
                        {
                            //Data e valor total
                            Relatorio.CriarObjLabel("Query", "SQL2", "Data", (nLeftTicket - 2.98).ToString(), (nTopTicket + 3.3).ToString(), "10", "black", "L", "Header", "5", "");
                            Relatorio.CriarObjLabel("Query", "SQL2", "ValorTotal", (nLeftTicket + 10.9).ToString(), (nTopTicket + 3.5).ToString(), "10", "black", "L", "Header", "5", "Right");

                            if (bCash)
                                nCounter--;

                            string sValorTotal = dsFin.Tables["SQL2"].Rows[0]["ValorTotal"].ToString();

                            //Quantidade total de dados e valor total
                            Relatorio.CriarObjLabel("Fixo", "", "" + nCounter, (nLeftTicket + 9.7).ToString(), (nTopTicket + 6.02).ToString(), "10", "black", "L", "Header", "5", "");
                            Relatorio.CriarObjLabel("Fixo", "", '$' + sValorTotal, (nLeftTicket + 10.9).ToString(), (nTopTicket + 6.02).ToString(), "10", "black", "L", "Header", "5", "Right");

                            //Titulo
                            Relatorio.CriarObjLabel("Fixo", "", "Deposit Summary", (8.335).ToString(), (nTopTicket + 8).ToString(), "16", "black", "B", "Header", "5", "");
                            
                            string sBanco = dsFin.Tables["SQL2"].Rows[0]["Fantasia"].ToString();
                            string sConta = dsFin.Tables["SQL2"].Rows[0]["Conta"].ToString();
                            string sData = dsFin.Tables["SQL2"].Rows[0]["Data"].ToString();

                            //Cabeçalho, com banco e data
                            Relatorio.CriarObjLabel("Fixo", "", "Summary of deposits to " + sBanco + " " + sConta + " on " + sData, (0.835).ToString(), (nTopTicket + 9.26).ToString(), "10", "black", "B", "Header", "18", "");
                        }

                        //Titulo da tabela
                        Relatorio.CriarObjLabel("Fixo", "", "ID", (0.835).ToString(), (nTopTicket + 9.9).ToString(), "8", "black", "B", "Header", "18", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Type", (2.635).ToString(), (nTopTicket + 9.9).ToString(), "8", "black", "B", "Header", "18", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Number", (4.625).ToString(), (nTopTicket + 9.9).ToString(), "8", "black", "B", "Header", "18", "");
                        Relatorio.CriarObjLabel("Fixo", "", "From", (6.49).ToString(), (nTopTicket + 9.9).ToString(), "8", "black", "B", "Header", "18", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Amount", (10.1).ToString(), (nTopTicket + 9.9).ToString(), "8", "black", "B", "Header", "18", "");

                        string sSoma = dsFin.Tables["SQL3"].Rows[0]["Soma"].ToString();


                        //Tabela
                        Relatorio.CriarObjTabela("0.835", "0", "SQL3", false);

                        Relatorio.CriarObjColunaNaTabela(" ", "ValorID", false, "1.8", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "ValorID", "DetailGroup");
                        Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "Forma", false, "1.99", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Forma", "DetailGroup");
                        Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "NumeroDocumento", false, "1.865", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "NumeroDocumento", "DetailGroup");
                        Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "Emitente", false, "2.95", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Emitente", "DetailGroup", "Null", "Null", 0, false, "0", "");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Deposit Total", "TotalGroup", "", "DetailGroup", 1, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "Valor", false, "1.9", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");
                        Relatorio.CriarObjCelulaInColuna("Fixo", sSoma, "TotalGroup", "", "DetailGroup", 1, true);

                        Relatorio.TabelaEnd();

                        Relatorio.Imprimir(Title);
                    }

                    else if(sSelPadrao == "0") //Se padrão BRA
                    {
                        
                        double headerX = 0.2;
                        double headerY = 0.2;
                        double tabelaTitulo = 1.2;

                        double tabelaX = 0.2;
                        double tabelaY = 0;

                        //Header
                        Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "11", "black", "B", "Header", "8", "");
                        Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 8).ToString(), headerY.ToString(), "11", "#0113a0", "B", "Header", "9", "");
                        Relatorio.CriarObjLabelData("Now", "", "", (headerX + 15).ToString(), headerY.ToString(), "9", "black", "B", "Header", "3.72187", "Horas");
                        Relatorio.CriarNumeroPagina((headerX + 18.5).ToString(), (headerY).ToString(), "9", "Black", "B",false,"Right");


                        //Cria dados, para colocar no cabeçalho
                        string sConta = dsFin.Tables["SQL2"].Rows[0]["Conta"].ToString();
                        string nDinheiro = dsFin.Tables["SQL2"].Rows[0]["ValorDinTotal"].ToString();

                        string strParams = "ID: " + nFinanceiroID + "   " + "Est: " + sEstado + "   " + "Data: " + dtData + "   " + "Conta: " + sConta + "   " +
                                            "Moeda: " + sMoeda + "   " + "Dinheiro: " + nDinheiro + "   " + "Total: " + nTotal;

                        //Header Parametros
                        Relatorio.CriarObjLabel("Fixo", "", strParams, (headerX).ToString(), (headerY + 0.5).ToString(), "8", "black", "B", "Header", "19", "");

                        //Linha
                        Relatorio.CriarObjLinha((headerX).ToString(), (headerY + 0.9).ToString(), "20.5", "", "Header");
                        
                        //Header titulo da tabela
                        Relatorio.CriarObjLabel("Fixo", "", "Forma", (headerX).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "ID", (headerX + 1).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Est", (headerX + 2.5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Mot", (headerX + 3.3).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Documento", (headerX + 4.1).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Emitente", (headerX + 6.1).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Emissão", (headerX + 9.1).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Aprop", (headerX + 10.9).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Banco", (headerX + 12.7).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Número", (headerX + 14.3).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "$", (headerX + 15.8).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Valor", (headerX + 17.6).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Identificador", (headerX + 18.5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");



                        string sSoma = dsFin.Tables["SQL3"].Rows[0]["Soma"].ToString();

                        //Tabela
                        Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "SQL3", false);

                        Relatorio.CriarObjColunaNaTabela(" ", "Forma", false, "1", "Default","1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Forma", "DetailGroup", "Null", "Null", 0, false, "8");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Total", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "ValorID", false, "1.5", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "ValorID", "DetailGroup", "Null", "Null", 0, false, "8");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "Est", false, "0.8", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Est", "DetailGroup", "Null", "Null", 0, false, "8");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "MotivoDevolucao", false, "0.8", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "MotivoDevolucao", "DetailGroup", "Null", "Null", 0, false, "8");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "Documento", false, "2", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Documento", "DetailGroup", "Null", "Null", 0, false, "8");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "Emitente", false, "3", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Emitente", "DetailGroup", "Null", "Null", 0, false, "8");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "dtEmissao", false, "1.8", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "dtEmissao", "DetailGroup", "Null", "Null", 0, false, "8");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "dtApropriacao", false, "1.8", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "dtApropriacao", "DetailGroup", "Null", "Null", 0, false, "8");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "BancoAgencia", false, "1.6", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "BancoAgencia", "DetailGroup", "Null", "Null", 0, false, "8");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "NumeroDocumento", false, "1.5", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "NumeroDocumento", "DetailGroup", "Null", "Null", 0, false, "8");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "Moeda", false, "0.9", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Moeda", "DetailGroup", "Null", "Null", 0, false, "8");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "Valor", false, "1.8", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup", "Null", "Null", 0, false, "8");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "" + sSoma, "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "Identificador", false, "2", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Identificador", "DetailGroup", "Null", "Null", 0, false, "8");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.TabelaEnd();


                        Relatorio.CriarPDF_Excel(Title, formato);
                    }
                }
                else if(formato == 2) // se Excel
                {
                    double headerX = 0.0;
                    double headerY = 0.0;

                    double tabelaX = 0.0;
                    double tabelaY = 0.2;

                    Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "8", "black", "L", "Body", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 5.8).ToString(), headerY.ToString(), "8", "black", "L", "Body", "4", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 13.8).ToString(), headerY.ToString(), "8", "black", "L", "Body", "3.72187", "Horas");

                    string sSoma = dsFin.Tables["SQL3"].Rows[0]["Soma"].ToString();

                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "SQL3", false);

                    Relatorio.CriarObjColunaNaTabela("ValorID", "ValorID", false, "5");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorID", "DetailGroup", "Null", "Null", 0, false, "8");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("Est", "Est", false, "0.8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Est", "DetailGroup", "Null", "Null", 0, false, "8");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("Forma", "Forma", false, "4");
                    Relatorio.CriarObjCelulaInColuna("Query", "Forma", "DetailGroup", "Null", "Null", 0, false, "8");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Total", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("MotivoDevolucao", "MotivoDevolucao", false, "4");
                    Relatorio.CriarObjCelulaInColuna("Query", "MotivoDevolucao", "DetailGroup", "Null", "Null", 0, false, "8");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("Documento", "Documento", false, "3.72187");
                    Relatorio.CriarObjCelulaInColuna("Query", "Documento", "DetailGroup", "Null", "Null", 0, false, "8");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("Emitente", "Emitente", false, "3");
                    Relatorio.CriarObjCelulaInColuna("Query", "Emitente", "DetailGroup", "Null", "Null", 0, false, "8");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("dtEmissao", "dtEmissao", false, "1.8");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtEmissao", "DetailGroup", "Null", "Null", 0, false, "8");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("dtApropriacao", "dtApropriacao", false, "2.5");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtApropriacao", "DetailGroup", "Null", "Null", 0, false, "8");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("BancoAgencia", "BancoAgencia", false, "3");
                    Relatorio.CriarObjCelulaInColuna("Query", "BancoAgencia", "DetailGroup", "Null", "Null", 0, false, "8");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("NumeroDocumento", "NumeroDocumento", false, "3.2");
                    Relatorio.CriarObjCelulaInColuna("Query", "NumeroDocumento", "DetailGroup", "Null", "Null", 0, false, "8");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("Moeda", "Moeda", false, "1.8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Moeda", "DetailGroup", "Null", "Null", 0, false, "8");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("Valor", "Valor", false, "1.8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup", "Null", "Null", 0, false, "8");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "" + sSoma, "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("Identificador", "Identificador", false, "2");
                    Relatorio.CriarObjCelulaInColuna("Query", "Identificador", "DetailGroup", "Null", "Null", 0, false, "8");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, formato);
                }
            }
        }

        protected dataInterface DataInterfaceObj = new dataInterface(
        System.Configuration.ConfigurationManager.AppSettings["application"]);

        private DataTable RequestDatatable(string SQLQuery, string NomeQuery)
        {
            string strConn = "";

            Object oOverflySvrCfg = null;

            DataTable Dt = new DataTable();

            DataSet DsContainer = new DataSet();

            oOverflySvrCfg = comCreateObject("OverflySvrCfg.OverflyMTS");
            strConn = ((OVERFLYSVRCFGLib.OverflyMTS)oOverflySvrCfg).DotNetDataBaseStrConn(DataInterfaceObj.ApplicationName);

            SqlCommand cmd = new SqlCommand(SQLQuery);
            using (SqlConnection con = new SqlConnection(strConn))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    cmd.CommandTimeout = 0;
                    sda.SelectCommand = cmd;

                    sda.Fill(DsContainer, NomeQuery);
                }
            }

            Dt = DsContainer.Tables[NomeQuery];

            return Dt;
        }

        private static Object comCreateObject(string sProgID)
        {
            Type oType = Type.GetTypeFromProgID(sProgID);
            if (oType != null)
            {
                return Activator.CreateInstance(oType);
            }
            return null;
        }
    }
}