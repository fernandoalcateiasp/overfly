﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class CombosFabEmp : System.Web.UI.OverflyPage
    {
        protected override void PageLoad(object sender, EventArgs e)
        {
            string SQL = "";
            SQL = "DECLARE @Fornecedor_Campanha TABLE (FornecedorID INT) " +
                   "INSERT INTO @Fornecedor_Campanha " +
	                    "SELECT DISTINCT FornecedorID " +
		                    "FROM Campanhas WITH(NOLOCK) " +
                    "SELECT '1' AS [Indice],0 AS [PessoaID], '' AS [Fantasia] " +
                       "UNION ALL " +
                    "SELECT '1', a.PessoaID, a.Fantasia " +
                       "FROM Pessoas a WITH (NOLOCK) " +
                           "INNER JOIN RelacoesPesRec b WITH (NOLOCK) ON (a.PessoaID = b.SujeitoID) " +
                       "WHERE b.ObjetoID = 999 AND b.TipoRelacaoID = 12 and A.EstadoID = 2 AND b.EstadoID = 2 " +
                       "UNION ALL " +
                    "SELECT '2' AS [Indice],0 AS [PessoaID], '' AS [Fantasia] " +
                       "UNION ALL " +
                    "SELECT DISTINCT '2', c.PessoaID, c.Fantasia " +
                       "FROM Pessoas c WITH (NOLOCK) " +
                           "INNER JOIN RelacoesPesCon d WITH (NOLOCK) ON ( d.SujeitoID = c.PessoaID ) " +
                           "INNER JOIN @Fornecedor_Campanha e ON (e.FornecedorID = d.SujeitoID) " +
                       "WHERE d.EstadoID = 2 and d.TipoRelacaoID = 62 AND c.PessoaID > 10000 " +
                        "ORDER BY Indice, Fantasia ";

            WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
        }
    }
}