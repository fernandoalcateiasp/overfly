﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class pesqdemonstrativoretencoes : System.Web.UI.OverflyPage
    {
        private Integer Valor;

        public Integer nValor
        {
            get { return Valor; }
            set { Valor = value; }
        }
        private Integer FinanceiroID;

        public Integer nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value; }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            ProcedureParameters[] ProcParams = new ProcedureParameters[3];

            ProcParams[0] = new ProcedureParameters(
               "@FinanceiroID",
               System.Data.SqlDbType.Int,
               FinanceiroID == null ? System.DBNull.Value : (Object)FinanceiroID.ToString());

            ProcParams[1] = new ProcedureParameters(
                "@ImpostoID",
                System.Data.SqlDbType.Int,
                Valor == null ? System.DBNull.Value : (Object)Convert.ToInt32(Valor));

            ProcParams[2] = new ProcedureParameters(
                "@ImpostoID",
                System.Data.SqlDbType.Date,
                System.DBNull.Value);

            WriteResultXML(DataInterfaceObj.execQueryProcedure(
                "sp_Financeiro_DemonstrativoRetencoes", ProcParams));
        }
    }
}