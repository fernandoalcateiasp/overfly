﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using Overfly3.systemEx.serverside;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class gerarfinanceiros : System.Web.UI.OverflyPage
    {
        string sql = "";
        string Resultado = "";
        int FinanceiroID;
        int ValorId;
        protected static Integer Zero = new Integer(0);

        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = (value != null ? value : Zero); }
        }
        private Integer TipoFinanceiroID;

        public Integer nTipoFinanceiroID
        {
            get { return TipoFinanceiroID; }
            set { TipoFinanceiroID = (value != null ? value : Zero); }
        }
        private Integer[] PessoaID;

        public Integer[] nPessoaID
        {
            get { return PessoaID; }
            set { PessoaID = value; }
        }
        private string DtEmissao;

        public string dtEmissao
        {
            get { return DtEmissao; }
            set { DtEmissao = value; }
        }
        private string MoedaID;

        public string nMoedaID
        {
            get { return MoedaID; }
            set { MoedaID = value; }
        }
        private string TaxaAtualizacao;

        public string nTaxaAtualizacao
        {
            get { return TaxaAtualizacao; }
            set { TaxaAtualizacao = value; }
        }
        private string EhImporte;

        public string bEhImporte
        {
            get { return EhImporte; }
            set { EhImporte = value; }
        }
        private string HistoricoPadraoID;

        public string nHistoricoPadraoID
        {
            get { return HistoricoPadraoID; }
            set { HistoricoPadraoID = value; }
        }
        private Integer ImpostoID;

        public Integer nImpostoID
        {
            get { return ImpostoID; }
            set { ImpostoID = (value != null ? value : Zero); }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = (value != null ? value : Zero); }
        }
        private Integer RelPesContaID;

        public Integer nRelPesContaID
        {
            get { return RelPesContaID; }
            set { RelPesContaID = (value != null ? value : Zero); }
        }
        private string[] Duplicata;

        public string[] nDuplicata
        {
            get { return Duplicata; }
            set
            {
                Duplicata = value;

                for (int i = 0; i < Duplicata.Length; i++)
                {
                    if (Duplicata[i] == null)
                        Duplicata[i] = "0";
                }
            }
        }
        private Integer[] FormaPagamentoID;

        public Integer[] nFormaPagamentoID
        {
            get { return FormaPagamentoID; }
            set
            {
                FormaPagamentoID = value;

                for (int i = 0; i < FormaPagamentoID.Length; i++)
                {
                    if (FormaPagamentoID[i] == null)
                        FormaPagamentoID[i] = Zero;
                }
            }
        }
        private string[] DtVencimento;

        public string[] dtVencimento
        {
            get { return DtVencimento; }
            set { DtVencimento = value; }
        }
        private java.lang.Double[] Valor = Constants.DBL_EMPTY_SET;

        public java.lang.Double[] nValor
        {
            get { return Valor; }
            set
            {
                Valor = value;
            }
        }
        private string[] Observacao;

        public string[] sObservacao
        {
            get { return Observacao; }
            set { Observacao = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            for (int i = 0; i < Duplicata.Length; i++)
            {
                ProcedureParameters[] procParams = new ProcedureParameters[20];

                procParams[0] = new ProcedureParameters(
                    "@EmpresaID",
                    System.Data.SqlDbType.Int,
                EmpresaID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(EmpresaID));

                procParams[1] = new ProcedureParameters(
                    "@TipoFinanceiroID",
                    System.Data.SqlDbType.Int,
                TipoFinanceiroID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(TipoFinanceiroID.ToString()));

                procParams[2] = new ProcedureParameters(
                    "@Duplicata",
                    System.Data.SqlDbType.VarChar,
                Duplicata[i].ToString() == "0" ? DBNull.Value : (Object)Duplicata[i].ToString());

                procParams[3] = new ProcedureParameters(
                    "@PessoaID",
                    System.Data.SqlDbType.Int,
                PessoaID[i].intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(PessoaID[i].ToString()));

                procParams[4] = new ProcedureParameters(
                    "@dtEmissao",
                    System.Data.SqlDbType.DateTime,
                dtEmissao.ToString() == null ? DBNull.Value : (Object)(dtEmissao.ToString()));

                procParams[5] = new ProcedureParameters(
                    "@FormaPagamentoID",
                    System.Data.SqlDbType.Int,
                FormaPagamentoID[i].intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(FormaPagamentoID[i].ToString()));

                procParams[6] = new ProcedureParameters(
                    "@PrazoPagamento",
                    System.Data.SqlDbType.VarChar,
                DBNull.Value);

                procParams[7] = new ProcedureParameters(
                    "@dtVencimento",
                    System.Data.SqlDbType.Date,
                 dtVencimento[i].ToString() == null ? DBNull.Value : (Object)(dtVencimento[i].ToString()));

                procParams[8] = new ProcedureParameters(
                    "@MoedaID",
                    System.Data.SqlDbType.Int,
                MoedaID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(MoedaID.ToString()));

                procParams[9] = new ProcedureParameters(
                    "@Valor",
                    System.Data.SqlDbType.Money,
                (Valor[i].doubleValue() == 0.00 ? DBNull.Value : (Object)Valor[i].doubleValue()));

                procParams[10] = new ProcedureParameters(
                    "@TaxaAtualizacao",
                    System.Data.SqlDbType.Int,
                TaxaAtualizacao.ToString() == null ? DBNull.Value : (Object)Convert.ToDouble(TaxaAtualizacao.ToString()));

                procParams[11] = new ProcedureParameters(
                    "@EhEstorno",
                    System.Data.SqlDbType.Bit,
                false);

                procParams[12] = new ProcedureParameters(
                    "@EhImporte",
                    System.Data.SqlDbType.Bit,
                EhImporte.ToString() == "0" ? false : true);

                procParams[13] = new ProcedureParameters(
                    "@HistoricoPadraoID",
                    System.Data.SqlDbType.Int,
                HistoricoPadraoID.ToString() == "0" ? DBNull.Value : (Object)Convert.ToInt32(HistoricoPadraoID.ToString()));

                procParams[14] = new ProcedureParameters(
                    "@ImpostoID",
                    System.Data.SqlDbType.Int,
                ImpostoID.ToString() == "0" ? DBNull.Value : (Object)Convert.ToInt32(ImpostoID.ToString()));

                procParams[15] = new ProcedureParameters(
                    "@Observacao",
                    System.Data.SqlDbType.VarChar,
                Observacao[i].ToString() == null ? DBNull.Value : (Object)Observacao[i].ToString());

                procParams[16] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                UsuarioID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(UsuarioID.ToString()));

                procParams[17] = new ProcedureParameters(
                    "@RelPesContaID",
                    System.Data.SqlDbType.Int,
                RelPesContaID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(RelPesContaID.ToString()));

                procParams[18] = new ProcedureParameters(
                    "@FinanceiroID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[18].Length = 8000;

                procParams[19] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[19].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure("sp_Financeiro_Gerador", procParams);

                if (procParams[18].Data != DBNull.Value)
                    FinanceiroID = Convert.ToInt32(procParams[18].Data.ToString());

                if (procParams[19].Data != DBNull.Value)
                    Resultado += procParams[19].Data.ToString();

                sql += (sql == "" ? "SELECT" : "UNION SELECT") + " " +
                    (Resultado != null ? ("'" + Resultado + "' as Resultado ") : " ");
                //(FinanceiroID.ToString() != null ? ("' " + FinanceiroID + "' as FinanceiroID, ") : " ");
                //(ValorId.ToString() != null ? ("' " + ValorId + "' as ValorID ") : " ");
            }
 
            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    sql));


        }
    }
}