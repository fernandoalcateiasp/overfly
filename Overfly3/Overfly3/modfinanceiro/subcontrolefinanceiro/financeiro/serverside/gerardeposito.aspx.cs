﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.financeiroEx.serverside
{
    public partial class gerardeposito : System.Web.UI.OverflyPage
    {
        int FinanceiroID;
        string ValorId;
        protected static Integer Zero = new Integer(0);

        private Integer RelPesContaID;

        public Integer nRelPesContaID
        {
            get { return RelPesContaID; }
            set { RelPesContaID = value != null ? value : Zero; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value != null ? value : Zero; }
        }
        private string ValorID;

        public string nValorID
        {
            get { return ValorID; }
            set { ValorID = value != null ? value : ""; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string sql = "";
            string Resultado = "";

            ProcedureParameters[] procParams = new ProcedureParameters[6];

            procParams[0] = new ProcedureParameters(
                "@RelPesContaID",
                System.Data.SqlDbType.Int,
            RelPesContaID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(RelPesContaID));

            procParams[1] = new ProcedureParameters(
                "@ValoresDepositados",
                System.Data.SqlDbType.VarChar,
            ValorID == "" ? DBNull.Value : (Object)ValorID.ToString());

            procParams[2] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
            UsuarioID.intValue() == 0 ? DBNull.Value : (Object)UsuarioID.ToString());

            procParams[3] = new ProcedureParameters(
                "@FinanceiroID",
                System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);

            procParams[4] = new ProcedureParameters(
                "@ValorID",
                System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);

            procParams[5] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[5].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure("sp_ServicoBancario_Deposito", procParams);

            if (procParams[3].Data != DBNull.Value)
                FinanceiroID = Convert.ToInt32(procParams[3].Data.ToString());

            if (procParams[4].Data != DBNull.Value)
                ValorId = procParams[4].Data.ToString();

            if (procParams[5].Data != DBNull.Value)
                Resultado += procParams[5].Data.ToString();

            sql += (sql == "" ? "SELECT" : "UNION SELECT") + " " +
                (Resultado != null ? ("'" + Resultado + "' as Resultado, ") : " ") +
                (FinanceiroID.ToString() != null ? ("'" + FinanceiroID + "' as FinanceiroID ") : " ");

            WriteResultXML(DataInterfaceObj.getRemoteData(sql));
        }
    }
}