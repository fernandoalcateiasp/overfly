/********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 *, ' +
               'CONVERT(VARCHAR, dtEmissao, ' + DATE_SQL_PARAM + ') as V_dtEmissao,' +
               'CONVERT(VARCHAR, dtVencimento, ' + DATE_SQL_PARAM + ') as V_dtVencimento,' +
               'CONVERT(VARCHAR, dtFluxoCaixa, ' + DATE_SQL_PARAM + ') as V_dtFluxoCaixa,' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=Financeiro.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'dbo.fn_Financeiro_Posicao(Financeiro.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, ' +
	           'dbo.fn_Financeiro_Posicao(Financeiro.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, ' +
               'dbo.fn_Financeiro_Totais(Financeiro.FinanceiroID, GETDATE(), 11) AS TotalPrincipal, ' +
               'dbo.fn_Financeiro_Totais(Financeiro.FinanceiroID, GETDATE(), 12) AS TotalEncargos, ' +
               'dbo.fn_Financeiro_Totais(Financeiro.FinanceiroID, GETDATE(), 13) AS TotalDespesas, ' +
               'dbo.fn_Financeiro_Totais(Financeiro.FinanceiroID, GETDATE(), 21) AS TotalAbatimentos, ' +
               'dbo.fn_Financeiro_Totais(Financeiro.FinanceiroID, GETDATE(), 22) AS TotalAmortizacoes, ' +
               'dbo.fn_Financeiro_Totais(Financeiro.FinanceiroID, GETDATE(), 30) AS PercAmortizacao, ' +
               'dbo.fn_HistoricoPadrao_TipoDetalhamento(Financeiro.HistoricoPadraoID, (CASE WHEN Financeiro.EhImporte = 1 THEN 1573 ' +
					'WHEN Financeiro.TipoFinanceiroID = 1001 THEN 1571 ELSE 1572 END), 1543) AS bShowImposto, ' +
			   '(SELECT COUNT(*) FROM HistoricosPadrao WITH(NOLOCK) WHERE (Financeiro.HistoricoPadraoID = HistoricoPadraoID AND ISNULL(PesoContabilizacao, 0) <> 0)) AS PermiteOcorrenciasCruzadas, ' +
               '(SELECT COUNT(*) FROM Financeiro_Ocorrencias WITH(NOLOCK) WHERE (Financeiro.FinanceiroID = FinanceiroID AND TipoOcorrenciaID = 1055 AND FinOcorrenciaReferenciaID IS NOT NULL)) AS OcorrenciasCruzadas, ' +
			   '(CASE WHEN (Financeiro.HistoricoPadraoID = 4) THEN 1 ELSE dbo.fn_HistoricoPadrao_UsoEspecial(Financeiro.HistoricoPadraoID, 1517, NULL) END) AS CreditoFornecedores, ' +
			   'CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(Valor, TaxaMoeda), 2) AS ValorConvertido, ' +
				'dbo.fn_Financeiro_Cor(Financeiro.FinanceiroID,' + nUserID + ',GETDATE(),1) AS CorObservacao, ' +
				'dbo.fn_TipoAuxiliar_Item(GuiaRecolhimentoID,2) AS GuiaRec, ' +
				'dbo.fn_Financeiro_ContaTomacao(FinanceiroID, 1) AS HistPadraoConta ' +
               'FROM Financeiro WITH(NOLOCK)  ' +
               'WHERE ProprietarioID = ' + nID + 'ORDER BY FinanceiroID DESC';
    else
        sSQL = 'SELECT *, ' +
               'CONVERT(VARCHAR, dtEmissao, ' + DATE_SQL_PARAM + ') as V_dtEmissao,' +
               'CONVERT(VARCHAR, dtVencimento, ' + DATE_SQL_PARAM + ') as V_dtVencimento,' +
               'CONVERT(VARCHAR, dtFluxoCaixa, ' + DATE_SQL_PARAM + ') as V_dtFluxoCaixa,' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=Financeiro.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'dbo.fn_Financeiro_Posicao(Financeiro.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, ' +
	           'dbo.fn_Financeiro_Posicao(Financeiro.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, ' +
               'dbo.fn_Financeiro_Totais(Financeiro.FinanceiroID, GETDATE(), 11) AS TotalPrincipal, ' +
               'dbo.fn_Financeiro_Totais(Financeiro.FinanceiroID, GETDATE(), 12) AS TotalEncargos, ' +
               'dbo.fn_Financeiro_Totais(Financeiro.FinanceiroID, GETDATE(), 13) AS TotalDespesas, ' +
               'dbo.fn_Financeiro_Totais(Financeiro.FinanceiroID, GETDATE(), 21) AS TotalAbatimentos, ' +
               'dbo.fn_Financeiro_Totais(Financeiro.FinanceiroID, GETDATE(), 22) AS TotalAmortizacoes, ' +
               'dbo.fn_Financeiro_Totais(Financeiro.FinanceiroID, GETDATE(), 30) AS PercAmortizacao, ' +
               'dbo.fn_HistoricoPadrao_TipoDetalhamento(Financeiro.HistoricoPadraoID, (CASE WHEN Financeiro.EhImporte = 1 THEN 1573 ' +
					'WHEN Financeiro.TipoFinanceiroID = 1001 THEN 1571 ELSE 1572 END), 1543) AS bShowImposto, ' +
			   '(SELECT COUNT(*) FROM HistoricosPadrao WITH(NOLOCK) WHERE (Financeiro.HistoricoPadraoID = HistoricoPadraoID AND ISNULL(PesoContabilizacao, 0) <> 0)) AS PermiteOcorrenciasCruzadas, ' +
               '(SELECT COUNT(*) FROM Financeiro_Ocorrencias WITH(NOLOCK) WHERE (Financeiro.FinanceiroID = FinanceiroID AND TipoOcorrenciaID = 1055 AND FinOcorrenciaReferenciaID IS NOT NULL)) AS OcorrenciasCruzadas, ' +
			   '(CASE WHEN (Financeiro.HistoricoPadraoID = 4) THEN 1 ELSE dbo.fn_HistoricoPadrao_UsoEspecial(Financeiro.HistoricoPadraoID, 1517, NULL) END) AS CreditoFornecedores, ' +
			   'CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(Valor, TaxaMoeda), 2) AS ValorConvertido, ' +
				'dbo.fn_Financeiro_Cor(Financeiro.FinanceiroID,' + nUserID + ',GETDATE(),1) AS CorObservacao, ' +
				'dbo.fn_TipoAuxiliar_Item(GuiaRecolhimentoID,2) AS GuiaRec, ' +
				'dbo.fn_Financeiro_ContaTomacao(FinanceiroID, 1) AS HistPadraoConta ' +
               'FROM Financeiro WITH(NOLOCK) WHERE FinanceiroID = ' + nID + ' ORDER BY FinanceiroID DESC';

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var nUserID = getCurrUserID();
    
    var sql;

    sql = 'SELECT *, ' +
            'CONVERT(VARCHAR, dtEmissao, ' + DATE_SQL_PARAM + ') as V_dtEmissao,' +
            'CONVERT(VARCHAR, dtVencimento, ' + DATE_SQL_PARAM + ') as V_dtVencimento,' +
            'CONVERT(VARCHAR, dtFluxoCaixa, ' + DATE_SQL_PARAM + ') as V_dtFluxoCaixa, ' +
            '0 AS SaldoDevedor, 0 AS SaldoAtualizado, 0 AS TotalPrincipal, 0 AS TotalEncargos, ' +
            '0 AS TotalAcrescimos, 0 AS TotalAbatimentos, 0 AS TotalAmortizacoes, 0 AS PercAmortizacao, 0 AS Prop1, 0 AS Prop2, ' +
            'dbo.fn_HistoricoPadrao_TipoDetalhamento(Financeiro.HistoricoPadraoID, (CASE WHEN Financeiro.EhImporte = 1 THEN 1573 ' +
				'WHEN Financeiro.TipoFinanceiroID = 1001 THEN 1571 ELSE 1572 END), 1543) AS bShowImposto, ' +
            '(SELECT COUNT(*) FROM HistoricosPadrao WITH(NOLOCK) WHERE (Financeiro.HistoricoPadraoID = HistoricoPadraoID AND ISNULL(PesoContabilizacao, 0) <> 0)) AS PermiteOcorrenciasCruzadas, ' +
            '(SELECT COUNT(*) FROM Financeiro_Ocorrencias WITH(NOLOCK) WHERE (Financeiro.FinanceiroID = FinanceiroID AND TipoOcorrenciaID = 1055 AND FinOcorrenciaReferenciaID IS NOT NULL)) AS OcorrenciasCruzadas, ' +
			'dbo.fn_HistoricoPadrao_UsoEspecial(Financeiro.HistoricoPadraoID, 1517, NULL) AS CreditoFornecedores, ' +
			'CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(Valor, TaxaMoeda), 2) AS ValorConvertido, ' +
			'dbo.fn_Financeiro_Cor(Financeiro.FinanceiroID,' + nUserID + ',GETDATE(),1) AS CorObservacao, ' +
            'dbo.fn_TipoAuxiliar_Item(GuiaRecolhimentoID,2) AS GuiaRec, ' +
            'dbo.fn_Financeiro_ContaTomacao(FinanceiroID, 1) AS HistPadraoConta ' +
          'FROM ' +
            'Financeiro WITH(NOLOCK) ' +
          'WHERE ' +
            'FinanceiroID = 0';
    
    dso.SQL = sql;          
}              
