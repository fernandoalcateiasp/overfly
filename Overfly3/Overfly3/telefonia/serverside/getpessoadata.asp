
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
   
	'Captura o ID do form passado por par�metro
	Dim i, nPessoaID, nEmpresaID, sDDD, sNumero
	
	nPessoaID = 0
	For i = 1 To Request.QueryString("nPessoaID").Count    
	  nPessoaID = Request.QueryString("nPessoaID")(i)
	Next

	nEmpresaID = 0
	For i = 1 To Request.QueryString("nEmpresaID").Count    
	  nEmpresaID = Request.QueryString("nEmpresaID")(i)
	Next

	sDDD = ""
	For i = 1 To Request.QueryString("sDDD").Count    
	  sDDD = Request.QueryString("sDDD")(i)
	Next

	sNumero = ""
	For i = 1 To Request.QueryString("sNumero").Count    
	  sNumero = Request.QueryString("sNumero")(i)
	Next

    Dim rsData, strSQL

    Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT TOP 1 1 AS Indice, '" & sDDD & "' AS DDD, '" & sNumero & "' AS Numero, b.DDDI AS Parametro " & _
		"FROM Pessoas_Enderecos a WITH(NOLOCK), Localidades_DDDs b WITH(NOLOCK) " & _
		"WHERE a.PessoaID = " & CStr(nEmpresaID) & " AND a.EndFaturamento=1 AND a.Ordem=1 AND a.CidadeID = b.LocalidadeID " & _
		"UNION ALL " & _
		"SELECT TOP 1 2 AS Indice, '" & sDDD & "' AS DDD, '" & sNumero & "' AS Numero, CONVERT(VARCHAR(10), a.CidadeID) AS Parametro " & _
		"FROM Pessoas_Enderecos a WITH(NOLOCK) " & _
		"WHERE a.PessoaID = " & CStr(nPessoaID) & " AND a.EndFaturamento=1 AND a.Ordem=1 " & _
		"UNION ALL " & _
		"SELECT TOP 1 3 AS Indice, '" & sDDD & "' AS DDD, '" & sNumero & "' AS Numero, CONVERT(VARCHAR(10), a.TipoTelefoneID) AS Parametro " & _
		"FROM Pessoas_Telefones a WITH(NOLOCK) " & _
		"WHERE a.PessoaID = " & CStr(nPessoaID) & " AND a.DDD='" & sDDD & "' AND a.Numero='" & sNumero & "' "

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    rsData.Save Response, adPersistXML

    rsData.Close
    Set rsData = Nothing
%>
