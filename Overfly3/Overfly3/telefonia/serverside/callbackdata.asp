
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    
    'String de conexao
    Dim strConn

    strConn = "Provider=MSDASQL.1;Persist Security Info=True;User ID=sa; Password=telefonia;Data Source=Overfly_Telefonia"
%>

<%
Function ReplaceTest(baseStr, patrn, replStr)
    Dim regEx                         ' Create variables.
    Set regEx = New RegExp            ' Create regular expression.
    regEx.Pattern = patrn             ' Set pattern.
    regEx.IgnoreCase = True           ' Make case insensitive.
    regEx.Global = True               ' Replace globally
    ReplaceTest = regEx.Replace(baseStr, replStr)   ' Make replacement.
End Function

Function ReturnStringBetweenPipe( pipe, currPipePos, stringBase )
    Dim i, retStr
    
    retStr = ""
    For i=(currPipePos + 1) To Len(stringBase)
        If (Mid(stringBase, i, 1) = pipe) Then
            retStr = Mid(stringBase, currPipePos + 1, i - 1 - currPipePos)
            Exit For
        End If
    Next
    
    ReturnStringBetweenPipe = retStr

End Function

Function MountPipe(ByVal sNumberToCall, ByVal nEmpresaID, ByVal nEmpresaAlternativaID)

    If ( (sNumberToCall = "") OR (nEmpresaID = 0) ) Then
        MountPipe = ""
        Exit Function
    End If

    Dim i
	Dim rsData, sPipe
	Dim strSQL
	Dim sDDI, sDDD, nPessoaID, nContatoID
	Dim sFantasia, sClassificacao, sCidade, sUF, sContato1, sContato2, sContato3 

    sDDI = ""
    sDDD = ""
    
    If (Len(sNumberToCall) <= 8) Then
        sDDD = "11"
    ElseIf (Len(sNumberToCall) = 10) Then
        sDDD = Left(sNumberToCall, 2)
        sNumberToCall = Mid(sNumberToCall, 3, Len(sNumberToCall) - 2)
    Else
        sDDD = Mid(sNumberToCall,4, 2)
        sNumberToCall = Mid(sNumberToCall, 6, Len(sNumberToCall))
    End If
    
    'On Error Resume Next

	sDDI = "55"
	nPessoaID = 0
	nContatoID = 0
	sFantasia = ""
	sClassificacao = ""
	sCidade = ""
	sUF = "" 
	sContato1 = ""
	sContato2 = ""
	sContato3 = ""
    
    Set rsData = Server.CreateObject("ADODB.Recordset")
    rsData.CursorLocation = adUseClient

    If (CLng(nPessoaID) <> 0) Then
        strSQL = "SELECT Parceiros.PessoaID " & _
                    "FROM Parceiros WITH(NOLOCK) " & _
                    "WHERE(Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " ," & CStr(nEmpresaAlternativaID) & ") AND " & _
                        "Parceiros.PessoaID = " & CStr(nPessoaID) & ") "
    
		rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        'Nao e Parceiro
        If (rsData.BOF AND rsData.EOF) Then
            strSQL = "SELECT Parceiros.PessoaID " & _
                        "FROM Parceiros WITH(NOLOCK) " & _
                        "WHERE(Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " ," & CStr(nEmpresaAlternativaID) & ") AND " & _
                            "Parceiros.ContatoID = " & CStr(nPessoaID) & ") "

            If rsData.State = adStateOpen Then
                rsData.Close
            End If
		    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (NOT (rsData.BOF AND rsData.EOF)) Then
		    	rsData.MoveFirst
                nContatoID = nPessoaID
                nPessoaID = rsData.Fields("PessoaID").Value
            End If        

        End If

    End If
            
    'Descobre o PessoaID
    If (CLng(nPessoaID) = 0) Then
        'DDR
        If (Len(sNumberToCall) <= 5) Then
            strSQL = "SELECT DISTINCT Atendentes.AtendenteID AS PessoaID " & _
                        "FROM Atendentes WITH(NOLOCK) " & _
                        "WHERE(Atendentes.DDR = " & Right(CStr(sNumberToCall), 5) & " AND " & _
                            "Atendentes.EmpresaID IN ( " & CStr(nEmpresaID) & " ," & CStr(nEmpresaAlternativaID) & ")) " & _
                            "ORDER BY Atendentes.AtendenteID "
    
            If (rsData.State = adStateOpen) Then
                rsData.Close
            End If
		    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
        'Telefone
        Else
            strSQL = "SELECT DISTINCT Parceiros.PessoaID " & _
                        "FROM Telefones WITH(NOLOCK), Parceiros WITH(NOLOCK) " & _
                        "WHERE(Telefones.DDI = " & CStr(sDDI) & " AND " & _ 
                            "Telefones.DDD = " & CStr(sDDD) & " AND " & _
                            "Telefones.Numero = " & CStr(sNumberToCall) & " AND " & _
                            "Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " ," & CStr(nEmpresaAlternativaID) & ") AND " & _
                            "((Telefones.PessoaID = Parceiros.PessoaID) OR " & _
                            "(Telefones.PessoaID = Parceiros.ContatoID))) " & _
                            "ORDER BY Parceiros.PessoaID "

            If rsData.State = adStateOpen Then
                rsData.Close
            End If
    	    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (rsData.BOF AND rsData.EOF) Then
                strSQL = "SELECT DISTINCT Pessoas.PessoaID " & _
                            "FROM Telefones WITH(NOLOCK), Pessoas WITH(NOLOCK) " & _
                            "WHERE(Telefones.DDI = " & CStr(sDDI) & " AND " & _ 
                                "Telefones.DDD = " & CStr(sDDD) & " AND " & _
                                "Telefones.Numero = " & CStr(sNumberToCall) & " AND " & _
                                "Telefones.PessoaID = Pessoas.PessoaID) " & _
                                "ORDER BY Pessoas.PessoaID "
                If rsData.State = adStateOpen Then
                    rsData.Close
                End If
			    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
            End If
                            
        End If

        If (NOT(rsData.BOF AND rsData.EOF)) Then
            If (rsData.RecordCount = 1) Then
				rsData.MoveFirst
                nPessoaID = rsData.Fields("PessoaID").Value
            End If

        End If        

        If rsData.State = adStateOpen Then
            rsData.Close
        End If
        
    End If
    
    If (CLng(nPessoaID) <> 0) Then
        'Select 1
        strSQL = "SELECT DISTINCT Pessoas.Fantasia, " & _
                        "Parceiros.Classificacao, Pessoas.Cidade, Pessoas.UF " & _
                    "FROM Parceiros WITH(NOLOCK), Pessoas WITH(NOLOCK) " & _
                    "WHERE(Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " ," & CStr(nEmpresaAlternativaID) & ") AND " & _
                        "Parceiros.PessoaID = " & CStr(nPessoaID) & " AND " & _
                        "Parceiros.PessoaID = Pessoas.PessoaID) " & _
                        "ORDER BY Parceiros.EhCliente"

        If rsData.State = adStateOpen Then
            rsData.Close
        End If

		rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        If (rsData.BOF AND rsData.EOF) Then
            'Select 2 (se o select 1 nao retornou dado)
            strSQL = "SELECT DISTINCT Pessoas.Fantasia, " & _
							"Pessoas.Classificacao, Pessoas.Cidade, Pessoas.UF " & _
						"FROM Pessoas WITH(NOLOCK) " & _
						"WHERE(Pessoas.PessoaID = " & CStr(nPessoaID) & " )"

            If rsData.State = adStateOpen Then
                rsData.Close
            End If
		    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

			If (NOT(rsData.BOF AND rsData.EOF)) Then
				rsData.MoveFirst
                sFantasia = rsData.Fields("Fantasia").Value
				sClassificacao = rsData.Fields("Classificacao").Value
				sCidade = rsData.Fields("Cidade").Value
				sUF = rsData.Fields("UF").Value 
			End If

        Else                 
			rsData.MoveFirst
            sFantasia = rsData.Fields("Fantasia").Value
			sClassificacao = rsData.Fields("Classificacao").Value
			sCidade = rsData.Fields("Cidade").Value
			sUF = rsData.Fields("UF").Value 

            'Select 3 (se o select 1 retornou dado)
            strSQL = "SELECT DISTINCT Contatos.Fantasia " & _
						"FROM Telefones WITH(NOLOCK), Parceiros WITH(NOLOCK), Pessoas Contatos WITH(NOLOCK) " & _
						"WHERE(Telefones.DDI = " & CStr(sDDI) & " AND Telefones.DDD = " & CStr(sDDD) & " AND " & _
							"Telefones.Numero = " & CStr(sNumberToCall) & " AND Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " , " & CStr(nEmpresaAlternativaID) & " ) AND " & _
							"Parceiros.PessoaID = " & CStr(nPessoaID) & " AND Telefones.PessoaID = Parceiros.PessoaID AND " & _
							"Parceiros.ContatoID = Contatos.PessoaID AND " & _
							CStr(nContatoID) & " = 0) " & _
						"ORDER BY Contatos.Fantasia"

            If rsData.State = adStateOpen Then
                rsData.Close
            End If
		    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (rsData.BOF AND rsData.EOF) Then
                'Select 4 (se o select 3 nao retornou dado)
                strSQL = "SELECT DISTINCT Contatos.Fantasia " & _
							"FROM Telefones WITH(NOLOCK), Parceiros WITH(NOLOCK), Pessoas Contatos WITH(NOLOCK) " & _
							"WHERE(Telefones.DDI = " & CStr(sDDI) & " AND Telefones.DDD = " & CStr(sDDD) & " AND " & _
								"Telefones.Numero = " & CStr(sNumberToCall) & " AND Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " , " & CStr(nEmpresaAlternativaID) & " ) AND " & _
								"Parceiros.PessoaID = " & CStr(nPessoaID) & " AND Telefones.PessoaID = Parceiros.ContatoID AND " & _
								"Parceiros.ContatoID = Contatos.PessoaID AND " & _
								"((Parceiros.ContatoID = " & CStr(nContatoID) & ") OR (" & CStr(nContatoID) & " = 0))) " & _
							"ORDER BY Contatos.Fantasia"
                          
                If rsData.State = adStateOpen Then
                    rsData.Close
                End If
			    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

                If (rsData.BOF AND rsData.EOF) Then
                    'Select 5 (se o select 4 nao retornou dado)
                    strSQL = "SELECT DISTINCT Contatos.Fantasia " & _
								"FROM Parceiros WITH(NOLOCK), Pessoas Contatos WITH(NOLOCK) " & _
								"WHERE(Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " , " & CStr(nEmpresaAlternativaID) & " ) AND " & _
									"Parceiros.PessoaID = " & CStr(nPessoaID) & " AND " & _
									"Parceiros.ContatoID = Contatos.PessoaID) " & _
								"ORDER BY Contatos.Fantasia "

                    If rsData.State = adStateOpen Then
                        rsData.Close
                    End If
				    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

                End If

            End If

            'Contatos
            '0152122632142
            If (NOT(rsData.BOF AND rsData.EOF)) Then
				rsData.MoveFirst
                sContato1 = rsData.Fields("Fantasia").Value
                rsData.MoveNext

                If (NOT rsData.EOF) Then
					sContato2 = rsData.Fields("Fantasia").Value
					rsData.MoveNext

					If (NOT rsData.EOF) Then
						sContato3 = rsData.Fields("Fantasia").Value
					End If
				End If

            End If

        End If

    End If

	If (CLng(nPessoaID) = 0) Then
		nPessoaID = ""
	End If		

    strSQL = "SELECT " & Chr(39) & CStr(sDDI) & Chr(39) & " AS DDI, " & _
                         Chr(39) & CStr(sDDD) & Chr(39) & " AS DDD, " & _
                         Chr(39) & CStr(sNumberToCall) & Chr(39) & " AS Numero, " & _
    	                 Chr(39) & CStr(nPessoaID) & Chr(39) & " AS PessoaID, " & _
    	                 Chr(39) & CStr(sFantasia) & Chr(39) & " AS Fantasia, " & _
    	                 Chr(39) & CStr(sClassificacao) & Chr(39) & " AS Classificacao, " & _
		                 Chr(39) & CStr(sCidade) & Chr(39) & " AS Cidade, " & _
		                 Chr(39) & CStr(sUF) & Chr(39) & " AS UF, " & _
		                 Chr(39) & CStr(sContato1) & Chr(39) & " AS Contato1, " & _
		                 Chr(39) & CStr(sContato2) & Chr(39) & " AS Contato2, " & _
		                 Chr(39) & CStr(sContato3) & Chr(39) & " AS Contato3"

    If rsData.State = adStateOpen Then
        rsData.Close
    End If
        
	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    sPipe = ""
    
    If (NOT (rsData.BOF AND rsData.EOF)) Then

        If (Not IsNull(rsData.Fields("DDI").Value)) Then
            sPipe = sPipe & rsData.Fields("DDI").Value
        End If
        
        sPipe = sPipe & "|"
                
        If (Not IsNull(rsData.Fields("DDD").Value)) Then
            sPipe = sPipe & rsData.Fields("DDD").Value
        End If
                
        sPipe = sPipe & "|"
                    
        If (Not IsNull(rsData.Fields("Numero").Value)) Then
            sPipe = sPipe & rsData.Fields("Numero").Value
        End If
                
        sPipe = sPipe & "|"
                
        If (Not IsNull(rsData.Fields("PessoaID").Value)) Then
            sPipe = sPipe & rsData.Fields("PessoaID").Value
        End If
                    
        sPipe = sPipe & "|"
                
        If (Not IsNull(rsData.Fields("Fantasia").Value)) Then
            sPipe = sPipe & rsData.Fields("Fantasia").Value
        End If
        
        sPipe = sPipe & "|"
                
        If (Not IsNull(rsData.Fields("Classificacao").Value)) Then
            sPipe = sPipe & rsData.Fields("Classificacao").Value
        End If
                    
        sPipe = sPipe & "|"
                
        If (Not IsNull(rsData.Fields("Cidade").Value)) Then
            sPipe = sPipe & rsData.Fields("Cidade").Value
        End If
                    
        sPipe = sPipe & "|"
                
        If (Not IsNull(rsData.Fields("UF").Value)) Then
            sPipe = sPipe & rsData.Fields("UF").Value
        End If
        
        sPipe = sPipe & "|"

        If (Not IsNull(rsData.Fields("Contato1").Value)) Then
            sPipe = sPipe & rsData.Fields("Contato1").Value
        End If
                    
        sPipe = sPipe & "|"

        If (Not IsNull(rsData.Fields("Contato2").Value)) Then
            sPipe = sPipe & rsData.Fields("Contato2").Value
        End If
                    
        sPipe = sPipe & "|"

        If (Not IsNull(rsData.Fields("Contato3").Value)) Then
            sPipe = sPipe & rsData.Fields("Contato3").Value                    
        End If
        
        sPipe = sPipe & "|"
    End If


    If rsData.State = adStateOpen Then
        rsData.Close
    End If
    Set rsData = Nothing

    MountPipe = sPipe

End Function

%>

<%
	Sub LogIntoCB_Rep (ByVal nTipoOperacao, ByVal sDevice, ByVal sNumeroTelefone, ByVal sPessoaID)
	
	' Tres situacoes sao posiveis:
	' Inser��o de Agendamento (OverFly) - nTipoOperacao = 1
	' Altera��o de hor�rio (OverFly) - nTipoOperacao = 2
	' Dele��o do Call Back (OverFly) - nTipoOperacao = 3
	
	'TABELA: rpt_cb 
	'CAMPOS:
	'int_CB_id     bigint(10)  NOT NULL , {Controle}
	'chr_Ramal     varchar(10) NOT NULL , {Device '26xx'}
	'chr_TelNumCli varchar(20) NOT NULL , {Telefone do Cliente 'ddMCDUxxxx'}
	'chr_ClientID  varchar(10)          , {Id do Cliente}
	'dtm_DateExec  datetime    NOT NULL , {Data de execu��o do comando '2002-01-13 13:13:57'}
	'int_Command   int(3)      NOT NULL , {Comando executado}
	
	'valores possiveis para o campo	int_Command e seus responsaveis
	'1 - Inser��o de CallBack (URA)
	'2 - Inser��o de Agendamento (OverFly)
	'3 - Altera��o de hor�rio (OverFly)
	'4 - Dele��o do Call Back (OverFly)
	'5 - Efetuar liga��o (URA)
	'6 - Sucesso na liga�ao (URA)
	'7 - Falha na Liga��o (URA)
	
	Dim strSQLOperacao, rsData, tempSPessoaID
	
	strSQLOperacao = ""
	
	If ( IsNull(sPessoaID) OR (sPessoaID = "") ) Then
		tempSPessoaID = "NULL"
	Else
		tempSPessoaID = CStr(sPessoaID)
	End If	
	
    strSQLOperacao = "INSERT INTO rpt_cb (chr_Ramal, chr_TelNumCli, " & _
        "chr_ClientID, dtm_DateExec, int_Command) " & _
	    "VALUES (" & Chr(39) & CStr(sDevice) & Chr(39) & ", " & _
	    Chr(39) & CStr(sNumeroTelefone) & Chr(39) & ", " & _
	    tempSPessoaID & ", " & _
	    "NOW(), " & CStr(nTipoOperacao + 1) & ")"
    
    If ( CLng(nTipoOperacao) <> 0 ) Then
        Set rsData = Server.CreateObject("ADODB.Recordset")
        rsData.Open strSQLOperacao, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If rsData.State = adStateOpen Then
			rsData.Close
		End If
        
        Set rsData = Nothing
    End If
		
	End Sub
%>

<%
    'CallBack - Clientes
    Dim i
	Dim rsData
	Dim strSQL1
	Dim sDevice
	
	Dim sNumeroTelefone, nCallTimeInterval, nEmpresaID, nEmpresaAlternativaID
	Dim nCallBackID, sPipe
	Dim nPipePos
	
	'nTipoOperacao = 0 -> Somente obter os dados
	'nTipoOperacao = 1 -> (Inserir)
	'nTipoOperacao = 2 -> (Ligar)
	'nTipoOperacao = 3 -> (Reprog)
	'nTipoOperacao = 4 -> (Excluir)
	'nTipoOperacao = 5 -> (Sucesso na ligacao direta de callback Excluir)
	Dim nTipoOperacao
	Dim strSQLOperacao
	
	Dim nCBCount
		
	sDevice = ""
	nTipoOperacao = 0
	strSQLOperacao = ""
	
	sNumeroTelefone = ""
	nCallTimeInterval = 0
	nEmpresaID = 0
	nEmpresaAlternativaID = 0
	nCallBackID = 0
	sPipe = ""
    nPipePos = 0
    
    nCBCount = 0
    
    For i = 1 To Request.QueryString("nEmpresaID").Count    
        nEmpresaID = Request.QueryString("nEmpresaID")(i)
    Next

    For i = 1 To Request.QueryString("nEmpresaAlternativaID").Count
        nEmpresaAlternativaID = Request.QueryString("nEmpresaAlternativaID")(i)
    Next

    If ( CLng(nEmpresaID) = 2 OR CLng(nEmpresaID) = 4 ) Then
        nEmpresaID = 2
        nEmpresaAlternativaID = 4
    End If

    For i = 1 To Request.QueryString("sDevice").Count    
        sDevice = Request.QueryString("sDevice")(i)
    Next    

    For i = 1 To Request.QueryString("nTipoOperacao").Count
        nTipoOperacao = Request.QueryString("nTipoOperacao")(i)
    Next

    For i = 1 To Request.QueryString("sNumeroTelefone").Count
        sNumeroTelefone = Request.QueryString("sNumeroTelefone")(i)
    Next
    
    For i = 1 To Request.QueryString("nCallTimeInterval").Count
        nCallTimeInterval = Request.QueryString("nCallTimeInterval")(i)
    Next
    
    For i = 1 To Request.QueryString("nCallBackID").Count
        nCallBackID = Request.QueryString("nCallBackID")(i)
    Next
    
    For i = 1 To Request.QueryString("sPipe").Count
        sPipe = Request.QueryString("sPipe")(i)
    Next
    
    For i = 1 To Request.QueryString("nCBCount").Count
        nCBCount = Request.QueryString("nCBCount")(i)
    Next
    
    If ( IsNull(nCBCount) OR IsEmpty(nCBCount) ) Then
		nCBCount = 0
    End If
        
    If (sPipe = "") Then
        sPipe = MountPipe(sNumeroTelefone, nEmpresaID, nEmpresaAlternativaID)
    End If

	'Obtem o PessoaID e o numero de telefone com DDD
	Dim k, sSlicedData, ncurrPipePos
	Dim sClienteID, sPhoneNumber
	
	k = 0
	sSlicedData = ""
	nCurrPipePos = 0
	sClienteID = ""
	sPhoneNumber = ""
	
	For k = 0 To 10
		If (k > 3) Then
			Exit For
		End If
	
		sSlicedData = ""
		sSlicedData = ReturnStringBetweenPipe( "|", nCurrPipePos, sPipe)
	
		'k=1->DDD, k=2->NumeroTelefone, k=3->ClienteID
		If ((k = 1) OR (k = 2)) Then
			sPhoneNumber = sPhoneNumber + Trim(sSlicedData)
		End If
		
		If (k = 3) Then
			sClienteID = Trim(sSlicedData)
		End If
			
		nCurrPipePos = nCurrPipePos + Len(CStr(sSlicedData)) + 1
	Next
    
    Select Case CLng(nTipoOperacao)
        Case 1
			
			Dim tempSClienteID
        
			If ( IsNull(sClienteID) OR (sClienteID = "") ) Then
				tempSClienteID = "NULL"
			Else
				tempSClienteID = CStr(sClienteID)
			End If	
        
            strSQLOperacao = "INSERT INTO queue (nBoxNumber, dtDate_Posted, nTrial, vcType, vcTel_Number, nCB_count, " & _
                "dtDate_Schedule, nLocked, nInterval, nLine, nID, nPriority, vcClient_Id, vcOwner, vcClient2) " & _
	            "VALUES (10, NOW(), 2, 'W', " & Chr(39) & CStr(sNumeroTelefone) & Chr(39) & ", " & CStr(nCBCount) & ", DATE_ADD(NOW(), " & _
	            "INTERVAL " & CStr(nCallTimeInterval) & " MINUTE), 0, 15, '', 0, -1, " & Chr(39) & sPipe & Chr(39) & ", " &_
	            Chr(39) & CStr(sDevice) & Chr(39) & ", " &_
	            tempSClienteID & ")"
	            
	        LogIntoCB_Rep 1, sDevice, sNumeroTelefone, sClienteID
        Case 2
            strSQLOperacao = "UPDATE queue SET dtDate_Schedule = NOW(), " & _
                "nPriority = 0, nCB_status = 6 " & _
                "WHERE CallBackID = " & CStr(nCallBackID)
        Case 3
            strSQLOperacao = "UPDATE queue SET dtDate_Schedule = DATE_ADD(dtDate_Schedule, INTERVAL " & CStr(nCallTimeInterval) & _
                " MINUTE) WHERE CallBackID = " & CStr(nCallBackID)
                
            LogIntoCB_Rep 2, sDevice, sPhoneNumber, sClienteID    
        Case 4
            strSQLOperacao = "DELETE FROM queue WHERE CallBackID = " & CStr(nCallBackID)
            
            LogIntoCB_Rep 3, sDevice, sPhoneNumber, sClienteID
        Case 5
            strSQLOperacao = "DELETE FROM queue WHERE CallBackID = " & CStr(nCallBackID)
            
            LogIntoCB_Rep 5, sDevice, sPhoneNumber, sClienteID    
    End Select
    
    If ( CLng(nTipoOperacao) <> 0 ) Then
        Set rsData = Server.CreateObject("ADODB.Recordset")
        rsData.Open strSQLOperacao, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If rsData.State = adStateOpen Then
        rsData.Close
		End If
        
        Set rsData = Nothing
    End If
    
    'Obtem os dados vale para todos os tipos de operacao
    Set rsData = Server.CreateObject("ADODB.Recordset")
    
    strSQL1 = "SELECT CallBackID, dtDate_Schedule AS Data, " & _
                    "'55' AS DDI, " & _
                    "'' AS DDD, " & _
                    "vcTel_Number AS Numero, " & _
                    "nCB_status AS CBStatus, " & _
                    "IFNULL(vcClient_Id, '|||||||||||') AS vcClient_Id " & _
                "FROM queue " & _
                "WHERE (vcOwner = " & sDevice & " ) " & _
                "ORDER BY dtDate_Schedule "
    
    On Error Resume Next

    rsData.Open strSQL1, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If rsData.Fields.Count = 0 Then
        dataBaseInactive
    Else
        dataBaseActive rsData
    End If
%>

<%    
Sub dataBaseActive(ByRef rsData)

	Dim rsNew
	Dim nNumberCallLength
    
    Set rsNew = Server.CreateObject("ADODB.Recordset")
    rsNew.CursorLocation = adUseServer

    'DDI
    rsNew.Fields.Append "DDI", adVarChar, 4, adFldMayBeNull OR adFldUpdatable    
    'DDD
    rsNew.Fields.Append "DDD", adVarChar, 4, adFldMayBeNull OR adFldUpdatable    
    'Numero
    rsNew.Fields.Append "Numero", adVarChar, 8, adFldMayBeNull OR adFldUpdatable    
    'PessoaID
    rsNew.Fields.Append "PessoaID", adDecimal, 10, adFldMayBeNull OR adFldUpdatable
    'Fantasia
    rsNew.Fields.Append "Fantasia", adVarChar, 20, adFldMayBeNull OR adFldUpdatable    
    'Classificacao
    rsNew.Fields.Append "Classificacao", adVarChar, 30, adFldMayBeNull OR adFldUpdatable    
    'Cidade
    rsNew.Fields.Append "Cidade", adVarChar, 30, adFldMayBeNull OR adFldUpdatable    
    'UF
    rsNew.Fields.Append "UF", adVarChar, 2, adFldMayBeNull OR adFldUpdatable    
    'Data
    rsNew.Fields.Append "Data", adDBTimeStamp, 16, adFldMayBeNull OR adFldUpdatable
    'Contato
    rsNew.Fields.Append "Contato", adVarChar, 62, adFldMayBeNull OR adFldUpdatable    
    'CallBackID
    rsNew.Fields.Append "CallBackID", adDecimal, 10, adFldMayBeNull OR adFldUpdatable
    'CBStatusID
    rsNew.Fields.Append "CBStatusID", adDecimal, 10, adFldMayBeNull OR adFldUpdatable
    'CBStatus
    rsNew.Fields.Append "CBStatus", adVarChar, 32, adFldMayBeNull OR adFldUpdatable  
      
    rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

    While NOT rsData.EOF
    
        Dim j, currPipePos
        Dim tempSlicedData
        ReDim aData(10)
    
        currPipePos = 0
        For j = 0 To 10
            tempSlicedData = ""
            tempSlicedData = ReturnStringBetweenPipe( "|", currPipePos, rsData.Fields("vcClient_Id").Value)
            aData(j) = Trim( tempSlicedData )
            currPipePos = currPipePos + Len(CStr(tempSlicedData)) + 1
        Next
    
        rsNew.AddNew
        
        If (IsNull(rsData.Fields("DDI").Value) = FALSE ) Then
            rsNew.Fields("DDI").Value = Trim(rsData.Fields("DDI").Value)
        End If

        If (IsNull(rsData.Fields("Numero").Value) = FALSE ) Then
            
            nNumberCallLength = Len(Trim(rsData.Fields("Numero").Value))
            
            If (nNumberCallLength >= 13) Then
                rsNew.Fields("DDD").Value = Mid(Trim(rsData.Fields("Numero").Value), 4, 2)
                rsNew.Fields("Numero").Value = Right(Trim(rsData.Fields("Numero").Value), 8)
                
            ElseIf ( (nNumberCallLength = 9) OR (nNumberCallLength = 10) ) Then
                rsNew.Fields("DDD").Value = Left(Trim(rsData.Fields("Numero").Value), 2)
                rsNew.Fields("Numero").Value = Mid(Trim(rsData.Fields("Numero").Value), 3, nNumberCallLength - 2)
            Else
                rsNew.Fields("DDD").Value = "11"
                
                If ( nNumberCallLength = 5 ) Then
                    rsNew.Fields("Numero").Value = Mid(Trim(rsData.Fields("Numero").Value), 2, nNumberCallLength - 1)
                ElseIf ( nNumberCallLength > 8 ) Then
                    rsNew.Fields("Numero").Value = Right(Trim(rsData.Fields("Numero").Value), 8)
                Else
                    rsNew.Fields("Numero").Value = Trim(rsData.Fields("Numero").Value)
                End If    
            End If

        End If
        
        If (aData(3) <> "") Then
            rsNew.Fields("PessoaID").Value = CLng(aData(3))
        End If

        rsNew.Fields("Fantasia").Value = aData(4)

        rsNew.Fields("Classificacao").Value = aData(5)

        rsNew.Fields("Cidade").Value = aData(6)

        rsNew.Fields("UF").Value = aData(7)
            
        rsNew.Fields("Contato").Value = aData(8)

        If (aData(9) <> "") Then
            If ( rsNew.Fields("Contato").Value <> "" ) Then
                rsNew.Fields("Contato").Value = rsNew.Fields("Contato").Value & "/" & aData(9)
            Else
                rsNew.Fields("Contato").Value = aData(9)            
            End If    
        End If

        If (aData(10) <> "") Then
            If ( rsNew.Fields("Contato").Value <> "" ) Then
                rsNew.Fields("Contato").Value = rsNew.Fields("Contato").Value & "/" & aData(10)
            Else
                rsNew.Fields("Contato").Value = aData(10)
            End If    
        End If

        If (IsNull(rsData.Fields("Data").Value) = FALSE) Then
            rsNew.Fields("Data").Value = CDate(Trim(rsData.Fields("Data").Value))
        End If

        If (IsNull(rsData.Fields("CallBackID").Value) = FALSE) Then
            rsNew.Fields("CallBackID").Value = CLng(Trim(rsData.Fields("CallBackID").Value))
        End If
        
        If (IsNull(rsData.Fields("CBStatus").Value) = FALSE) Then
        
			rsNew.Fields("CBStatusID").Value = CLng(Trim(rsData.Fields("CBStatus").Value))
        
			Select Case CLng(Trim(rsData.Fields("CBStatus").Value))
				Case 0
					rsNew.Fields("CBStatus").Value = ""
				Case 1
					rsNew.Fields("CBStatus").Value = "N�o atendido"
				Case 2
					rsNew.Fields("CBStatus").Value = "N�mero � Fax ou Modem"
				Case 3
					rsNew.Fields("CBStatus").Value = "Destinat�rio desligou"
				Case 4
					rsNew.Fields("CBStatus").Value = "N�o teve linha"
				Case 5
					rsNew.Fields("CBStatus").Value = "Destinat�rio Ocupado"
				Case 6
					rsNew.Fields("CBStatus").Value = "Chamada em andamento"	
				Case 7
					rsNew.Fields("CBStatus").Value = "Chamador desligou"		
				Case Else 
					rsNew.Fields("CBStatus").Value = ""
			End Select
        
        Else
			rsNew.Fields("CBStatusID").Value = 0
			rsNew.Fields("CBStatus").Value = ""    
        End If

        rsNew.Update
        rsData.MoveNext
    Wend
    
    ' send the new data back to the client
    rsNew.Save Response, adPersistXML
      
    rsNew.Close
    Set rsNew = Nothing
    
    rsData.Close
    Set rsData = Nothing
    
End Sub
%>

<%
Sub dataBaseInactive
    Dim rsNew, fldF
    Dim fldName
    
    fldName = "fldError"
    
    Set rsNew = Server.CreateObject("ADODB.Recordset")
    rsNew.CursorLocation = adUseServer
    
    rsNew.Fields.Append fldName, adVarChar, 5, adFldUpdatable      
    
    rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

    rsNew.AddNew
    rsNew.Fields(fldName).Value = "Error"
      
    ' send the new data back to the client
    rsNew.Save Response, adPersistXML
      
    rsNew.Close
    Set rsNew = Nothing
End Sub
%>