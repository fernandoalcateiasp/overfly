
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    
    'String de conexao
    Dim strConn

    strConn = "Provider=MSDASQL.1;Persist Security Info=True;User ID=sa; Password=telefonia;Data Source=Overfly_Telefonia"
%>

<%
    Dim i
	Dim rsData
	Dim strSQL
	Dim nEmpresaID, nEmpresaAlternativaID, sDDI, sDDD, sNumberToCall, nPessoaID, nContatoID
	Dim sFantasia, sClassificacao, sCidade, sUF, sContato1, sContato2, sContato3 
    
    'On Error Resume Next

	nEmpresaID = 0
	nEmpresaAlternativaID = 0
	sDDI = "55"
	sDDD = "11"
	sNumberToCall = ""
	nPessoaID = 0
	nContatoID = 0
	sFantasia = ""
	sClassificacao = ""
	sCidade = ""
	sUF = "" 
	sContato1 = ""
	sContato2 = ""
	sContato3 = ""
    
    For i = 1 To Request.QueryString("nEmpresaID").Count    
        nEmpresaID = Request.QueryString("nEmpresaID")(i)
    Next

    For i = 1 To Request.QueryString("nEmpresaAlternativaID").Count    
        nEmpresaAlternativaID = Request.QueryString("nEmpresaAlternativaID")(i)
    Next

    For i = 1 To Request.QueryString("sDDI").Count    
        sDDI = Request.QueryString("sDDI")(i)
    Next

    For i = 1 To Request.QueryString("sDDD").Count    
        sDDD = Request.QueryString("sDDD")(i)
    Next

    For i = 1 To Request.QueryString("sNumberToCall").Count    
        sNumberToCall = Request.QueryString("sNumberToCall")(i)
    Next
    
    If sNumberToCall = "" Then
        sNumberToCall = "0"
    End If
    
    For i = 1 To Request.QueryString("nPessoaID").Count    
        nPessoaID = Request.QueryString("nPessoaID")(i)
    Next

    Set rsData = Server.CreateObject("ADODB.Recordset")
    rsData.CursorLocation = adUseClient

    If (CLng(nPessoaID) <> 0) Then
        strSQL = "SELECT Parceiros.PessoaID " & _
                    "FROM Parceiros WITH(NOLOCK) " & _
                    "WHERE(Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " ," & CStr(nEmpresaAlternativaID) & ") AND " & _
                        "Parceiros.PessoaID = " & CStr(nPessoaID) & ") "
    
		rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        'Nao e Parceiro
        If (rsData.BOF AND rsData.EOF) Then
            strSQL = "SELECT Parceiros.PessoaID " & _
                        "FROM Parceiros WITH(NOLOCK) " & _
                        "WHERE(Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " ," & CStr(nEmpresaAlternativaID) & ") AND " & _
                            "Parceiros.ContatoID = " & CStr(nPessoaID) & ") "

            If rsData.State = adStateOpen Then
                rsData.Close
            End If
		    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (NOT (rsData.BOF AND rsData.EOF)) Then
		    	rsData.MoveFirst
                nContatoID = nPessoaID
                nPessoaID = rsData.Fields("PessoaID").Value
            End If        

        End If

    End If
            
    'Descobre o PessoaID
    If (CLng(nPessoaID) = 0) Then
        'DDR
        If (Len(sNumberToCall) <= 5) Then
            strSQL = "SELECT DISTINCT Atendentes.AtendenteID AS PessoaID " & _
                        "FROM Atendentes WITH(NOLOCK) " & _
                        "WHERE(Atendentes.DDR = " & Right(CStr(sNumberToCall), 5) & " AND " & _
                            "Atendentes.EmpresaID IN ( " & CStr(nEmpresaID) & " ," & CStr(nEmpresaAlternativaID) & ")) " & _
                            "ORDER BY Atendentes.AtendenteID "
    
            If (rsData.State = adStateOpen) Then
                rsData.Close
            End If
		    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
        'Telefone
        Else
            strSQL = "SELECT DISTINCT Parceiros.PessoaID " & _
                        "FROM Telefones WITH(NOLOCK), Parceiros WITH(NOLOCK) " & _
                        "WHERE(Telefones.DDI = " & CStr(sDDI) & " AND " & _ 
                            "Telefones.DDD = " & CStr(sDDD) & " AND " & _
                            "Telefones.Numero = " & CStr(sNumberToCall) & " AND " & _
                            "Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " ," & CStr(nEmpresaAlternativaID) & ") AND " & _
                            "((Telefones.PessoaID = Parceiros.PessoaID) OR " & _
                            "(Telefones.PessoaID = Parceiros.ContatoID))) " & _
                            "ORDER BY Parceiros.PessoaID "

            If rsData.State = adStateOpen Then
                rsData.Close
            End If
    	    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (rsData.BOF AND rsData.EOF) Then
                strSQL = "SELECT DISTINCT Pessoas.PessoaID " & _
                            "FROM Telefones WITH(NOLOCK), Pessoas WITH(NOLOCK) " & _
                            "WHERE(Telefones.DDI = " & CStr(sDDI) & " AND " & _ 
                                "Telefones.DDD = " & CStr(sDDD) & " AND " & _
                                "Telefones.Numero = " & CStr(sNumberToCall) & " AND " & _
                                "Telefones.PessoaID = Pessoas.PessoaID) " & _
                                "ORDER BY Pessoas.PessoaID "
                If rsData.State = adStateOpen Then
                    rsData.Close
                End If
			    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
            End If
                            
        End If

        If (NOT(rsData.BOF AND rsData.EOF)) Then
            If (rsData.RecordCount = 1) Then
				rsData.MoveFirst
                nPessoaID = rsData.Fields("PessoaID").Value
            End If

        End If        

        If rsData.State = adStateOpen Then
            rsData.Close
        End If
        
    End If
    
    If (CLng(nPessoaID) <> 0) Then
        'Select 1
        strSQL = "SELECT DISTINCT Pessoas.Fantasia, " & _
                        "Parceiros.Classificacao, Pessoas.Cidade, Pessoas.UF " & _
                    "FROM Parceiros WITH(NOLOCK), Pessoas WITH(NOLOCK) " & _
                    "WHERE(Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " ," & CStr(nEmpresaAlternativaID) & ") AND " & _
                        "Parceiros.PessoaID = " & CStr(nPessoaID) & " AND " & _
                        "Parceiros.PessoaID = Pessoas.PessoaID) " & _
                        "ORDER BY Parceiros.EhCliente"

        If rsData.State = adStateOpen Then
            rsData.Close
        End If

		rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        If (rsData.BOF AND rsData.EOF) Then
            'Select 2 (se o select 1 nao retornou dado)
            strSQL = "SELECT DISTINCT Pessoas.Fantasia, " & _
							"Pessoas.Classificacao, Pessoas.Cidade, Pessoas.UF " & _
						"FROM Pessoas WITH(NOLOCK) " & _
						"WHERE(Pessoas.PessoaID = " & CStr(nPessoaID) & " )"

            If rsData.State = adStateOpen Then
                rsData.Close
            End If
		    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

			If (NOT(rsData.BOF AND rsData.EOF)) Then
				rsData.MoveFirst
                sFantasia = rsData.Fields("Fantasia").Value
				sClassificacao = rsData.Fields("Classificacao").Value
				sCidade = rsData.Fields("Cidade").Value
				sUF = rsData.Fields("UF").Value 
			End If

        Else                 
			rsData.MoveFirst
            sFantasia = rsData.Fields("Fantasia").Value
			sClassificacao = rsData.Fields("Classificacao").Value
			sCidade = rsData.Fields("Cidade").Value
			sUF = rsData.Fields("UF").Value 

            'Select 3 (se o select 1 retornou dado)
            strSQL = "SELECT DISTINCT Contatos.Fantasia " & _
						"FROM Telefones WITH(NOLOCK), Parceiros WITH(NOLOCK), Pessoas Contatos WITH(NOLOCK) " & _
						"WHERE(Telefones.DDI = " & CStr(sDDI) & " AND Telefones.DDD = " & CStr(sDDD) & " AND " & _
							"Telefones.Numero = " & CStr(sNumberToCall) & " AND Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " , " & CStr(nEmpresaAlternativaID) & " ) AND " & _
							"Parceiros.PessoaID = " & CStr(nPessoaID) & " AND Telefones.PessoaID = Parceiros.PessoaID AND " & _
							"Parceiros.ContatoID = Contatos.PessoaID AND " & _
							CStr(nContatoID) & " = 0) " & _
						"ORDER BY Contatos.Fantasia"

            If rsData.State = adStateOpen Then
                rsData.Close
            End If
		    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (rsData.BOF AND rsData.EOF) Then
                'Select 4 (se o select 3 nao retornou dado)
                strSQL = "SELECT DISTINCT Contatos.Fantasia " & _
							"FROM Telefones WITH(NOLOCK), Parceiros WITH(NOLOCK), Pessoas Contatos WITH(NOLOCK) " & _
							"WHERE(Telefones.DDI = " & CStr(sDDI) & " AND Telefones.DDD = " & CStr(sDDD) & " AND " & _
								"Telefones.Numero = " & CStr(sNumberToCall) & " AND Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " , " & CStr(nEmpresaAlternativaID) & " ) AND " & _
								"Parceiros.PessoaID = " & CStr(nPessoaID) & " AND Telefones.PessoaID = Parceiros.ContatoID AND " & _
								"Parceiros.ContatoID = Contatos.PessoaID AND " & _
								"((Parceiros.ContatoID = " & CStr(nContatoID) & ") OR (" & CStr(nContatoID) & " = 0))) " & _
							"ORDER BY Contatos.Fantasia"
                          
                If rsData.State = adStateOpen Then
                    rsData.Close
                End If
			    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

                If (rsData.BOF AND rsData.EOF) Then
                    'Select 5 (se o select 4 nao retornou dado)
                    strSQL = "SELECT DISTINCT Contatos.Fantasia " & _
								"FROM Parceiros WITH(NOLOCK), Pessoas Contatos WITH(NOLOCK) " & _
								"WHERE(Parceiros.EmpresaID IN ( " & CStr(nEmpresaID) & " , " & CStr(nEmpresaAlternativaID) & " ) AND " & _
									"Parceiros.PessoaID = " & CStr(nPessoaID) & " AND " & _
									"Parceiros.ContatoID = Contatos.PessoaID) " & _
								"ORDER BY Contatos.Fantasia "

                    If rsData.State = adStateOpen Then
                        rsData.Close
                    End If
				    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

                End If

            End If

            'Contatos
            '0152122632142
            If (NOT(rsData.BOF AND rsData.EOF)) Then
				rsData.MoveFirst
                sContato1 = rsData.Fields("Fantasia").Value
                rsData.MoveNext

                If (NOT rsData.EOF) Then
					sContato2 = rsData.Fields("Fantasia").Value
					rsData.MoveNext

					If (NOT rsData.EOF) Then
						sContato3 = rsData.Fields("Fantasia").Value
					End If
				End If

            End If

        End If

    End If

	If (CLng(nPessoaID) = 0) Then
		nPessoaID = ""
	End If		

    strSQL = "SELECT " & Chr(39) & CStr(sDDI) & Chr(39) & " AS DDI, " & _
                         Chr(39) & CStr(sDDD) & Chr(39) & " AS DDD, " & _
                         Chr(39) & CStr(sNumberToCall) & Chr(39) & " AS Numero, " & _
    	                 Chr(39) & CStr(nPessoaID) & Chr(39) & " AS PessoaID, " & _
    	                 Chr(39) & CStr(sFantasia) & Chr(39) & " AS Fantasia, " & _
    	                 Chr(39) & CStr(sClassificacao) & Chr(39) & " AS Classificacao, " & _
		                 Chr(39) & CStr(sCidade) & Chr(39) & " AS Cidade, " & _
		                 Chr(39) & CStr(sUF) & Chr(39) & " AS UF, " & _
		                 Chr(39) & CStr(sContato1) & Chr(39) & " AS Contato1, " & _
		                 Chr(39) & CStr(sContato2) & Chr(39) & " AS Contato2, " & _
		                 Chr(39) & CStr(sContato3) & Chr(39) & " AS Contato3"

    If rsData.State = adStateOpen Then
        rsData.Close
    End If
        
	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    rsData.Save Response, adPersistXML
    If rsData.State = adStateOpen Then
        rsData.Close
    End If
    Set rsData = Nothing
%>
