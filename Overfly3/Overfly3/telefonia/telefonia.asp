
<%@ LANGUAGE=VBSCRIPT %>

<%
    Option Explicit
    Response.Expires = 0
    'Forca recarregamento da pagina
    Response.ExpiresAbsolute=#May 31,1996 13:30:15# 

    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, strConnMySQL
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    strConnMySQL = "Provider=MSDASQL.1;Persist Security Info=True;User ID=sa; Password=telefonia;Data Source=Overfly_Telefonia"
    
    Response.Write "<script ID=" & Chr(34) & "serverCfgVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
    Response.Write vbCrLf
    
    Response.Write "var __APP_NAME__ = " & Chr(39) & Application("appName") & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __PAGES_DOMINIUM__ = " & Chr(39) & objSvrCfg.PagesDominium(Application("appName")) & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __PAGES_URLROOT__ = " & Chr(39) & objSvrCfg.PagesURLRoot(Application("appName")) & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __DATABASE_DOMINIUM__ = " & Chr(39) & objSvrCfg.DatabaseDominium(Application("appName")) & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __DATABASE_ASPURLROOT__ = " & Chr(39) & objSvrCfg.DatabaseASPPagesURLRoot(Application("appName")) & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __DATABASE_WSURLROOT__ = " & Chr(39) & objSvrCfg.PagesDominium(Application("appName")) & "/WSOverflyRDS" & Chr(39) & ";"
    Response.Write vbcrlf
    
    Response.Write "</script>"
    Response.Write vbCrLf    
    
    Set objSvrCfg = Nothing
%>

<html id="telefoniaHtml" name="telefoniaHtml">

<head>

<title>Telefonia</title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/telefonia/telefonia.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_browsers.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    'Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    'Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    'Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf                
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_facilitiesnonforms.js" & Chr(34) & "></script>" & vbCrLf    
    
    'Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/telefonia/telefonia.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/telefonia/apicode.js" & Chr(34) & "></script>" & vbCrLf    
%>

<%
Response.Write "<script ID=" & Chr(34) & "telephonyVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbCrLf

'As variaveis abaixo devem ter seu uso implementado no form:
'nEmpresaID, nEmpresaAlternativaID, nUserID, glb_device
'O ramal ficara duro ate a janela receber efetivamente o parametro
'A outra pendencia do form diz respeito a seguranca

Dim i
Dim nEmpresaID, nEmpresaAlternativaID, nUserID
Dim VOICE_OCX_CLSID

nEmpresaID = 0
nEmpresaAlternativaID = 0
nUserID = 0
VOICE_OCX_CLSID = ""

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

'Inicio de OCX da Voice *********************************************
For i = 1 To Request.QueryString("VOICE_OCX_CLSID").Count    
    VOICE_OCX_CLSID = Request.QueryString("VOICE_OCX_CLSID")(i)
Next

'Remove {} do CLSID
VOICE_OCX_CLSID = Mid(VOICE_OCX_CLSID, 2, Len(VOICE_OCX_CLSID) - 2)

'Final de OCX da Voice **********************************************

If (CLng(nEmpresaID) = 2) Then
    nEmpresaAlternativaID = 4
Else    
    nEmpresaAlternativaID = nEmpresaID
End If    

Response.Write "var glb_machineNumber = " & Chr(39) & CStr(Request.ServerVariables("REMOTE_ADDR")) & Chr(39) & ";"
Response.Write vbCrLf

Dim rsData, strSQL, nDevice

nDevice = 0

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT Ramal FROM Atendentes WITH(NOLOCK) WHERE (EmpresaID IN (" & CStr(nEmpresaID) & "," & CStr(nEmpresaAlternativaID) & _
    ") AND AtendenteID = " & CStr(nUserID) & ") ORDER BY EmpresaID"

On Error Resume Next
						
rsData.Open strSQL, strConnMySQL, adOpenForwardOnly, adLockReadOnly, adCmdText

If (rsData.Fields.Count = 0) Then
    Response.Write "var glb_device = '0';"
ElseIf (rsData.BOF AND rsData.EOF) Then
    Response.Write "var glb_device = '0';"
Else
    If (IsNull(rsData.Fields("Ramal").Value)) Then
        Response.Write "var glb_device = '0';"
    Else
		nDevice = CLng(rsData.Fields("Ramal").Value)
        Response.Write "var glb_device = " & Chr(39) & CStr(rsData.Fields("Ramal").Value) & Chr(39) & ";"
    End If    
End If
    
Response.Write vbCrLf

If (rsData.Fields.Count <> 0) Then
    rsData.Close
    Set rsData = Nothing
End If

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT (NewMSG + UrgMSG) AS QtyMsgsVoiceMail " & _
		 "FROM voiceMail " & _
		 "WHERE boxNumber = " & CStr(nDevice)

rsData.Open strSQL, strConnMySQL, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write "var glb_qtyMsgsVoiceMail = 0;"

If (Not (rsData.BOF AND rsData.EOF)) Then
	Response.Write vbCrLf
	Response.Write "glb_qtyMsgsVoiceMail = " & CStr(rsData.Fields("QtyMsgsVoiceMail").Value) & ";"
End If

Response.Write vbCrLf

Response.Write "</script>"
Response.Write vbCrLf

If (rsData.Fields.Count <> 0) Then
    rsData.Close
    Set rsData = Nothing
End If
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--
    
//-->
</script>

<script ID="vbFunctions" LANGUAGE="vbscript">
<!--
'Funcao de uso geral, retorna um caracter replicate n vezes
Function replicate(ByVal sString, ByVal nVezes)

    Dim i, sNewString

    sNewString = ""
    
    If (IsNull(sString)) Then
        sString = ""
    End If

    For i=1 To nVezes
        sNewString = sNewString & sString
    Next

    replicate = sNewString

End Function

'Funcao de uso geral, PadL
Function padL(ByVal s, ByVal n, ByVal c)

    If (IsNull(s)) Then
        s = ""
    End If
    
    s = Trim(s)

    padL = replicate(c, n - Len(s)) & s

End Function

'Funcao de uso geral, PadR
Function padR(ByVal s, ByVal n, ByVal c)
    If (IsNull(s)) Then
        s = ""
    End If

    s = Trim(s)
    padR = s & replicate(c, n - Len(s))
End Function

Function DateIsInInterval( dateBase, dateToCheck, intInDays )
    DateIsInInterval = False
    
    Dim actualIntInDays
    
    actualIntInDays = daysBetween(dateBase, dateToCheck)
    
    If (intInDays >= 0) Then
        If ( actualIntInDays >= 0 ) Then    
            If (intInDays >= actualIntInDays) Then
                DateIsInInterval = True
            End If
        End If
    Else
        If ( actualIntInDays <= 0 ) Then
            If ( intInDays <= actualIntInDays ) Then
                DateIsInInterval = True
            End If
        End If    
    End If    
    
End Function

Function DateOfToday()
    DateOfToday = Date
End Function

Function chkTypeData( sType, vValue )
    If sType = "D" Then ' Date Format
       chkTypeData = IsDate(vValue)
    ElseIf sType = "N" Or sType = "M" Then ' Number Format
       chkTypeData = IsNumeric(vValue)
    Else
       chkTypeData = TRUE
    End If
End Function

Function formatCurrValue(nValue)
    formatCurrValue = FormatNumber(nValue,2)
End Function

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

Function daysSUM(dDate, nDias)
    daysSUM = DateAdd("d",nDias, Ddate)
End Function

Function DateToStr(dDate, sDateFormat)
    Dim sDay,sMonth,sYear
    sDay = Day(dDate)
    sMonth = Month(dDate)
    sYear = Year(dDate)
    If (sDateFormat = "DD/MM/YYYY") Then
        DateToStr = sDay & "/" & sMonth & "/" & sYear
    Else
        DateToStr = sMonth & "/" & sDay & "/" & sYear
    End If    
End Function

Function dataExtenso( dData, sIdioma)
    Dim sDataExtenso
    Dim aMonths
    Dim nDay, nMonth, nYear

    If not IsDate(dData) Then
        dataExtenso = Null
        Exit Function
    End If
    
    sIdioma = UCase(sIdioma)
    nDay = Day(dData)
    nMonth = Month(dData) - 1
    nYear = Year(dData)
    
    If sIdioma = "I" Then
        aMonths = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")
        sDataExtenso = aMonths(nMonth) & " " & CStr(nDay) & ", " & CStr(nYear)
    Else
        aMonths = Array("Janeiro", "Fevereiro", "Mar�o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro")
        sDataExtenso = CStr(nDay) & " de " & aMonths(nMonth) & " de " & CStr(nYear)
    End If    
        
    dataExtenso = sDataExtenso
End Function

Function timeToPrgCallBack(minPar)
    Dim sTime
    Dim sYear, sMonth, sDay
    Dim sHour, sMin, sSec
    
    sTime = DateAdd("n", minPar, Now)
    
    sYear = CStr(Year(sTime))
    
    sMonth = CStr(Month(sTime))
    sMonth = padL(sMonth, 2, "0")
    
    sDay = CStr(Day(sTime))
    sDay = padL(sDay, 2, "0")    
        
    sHour = CStr(Hour(sTime))
    sHour = padL(sHour, 2, "0")
    
    sMin = CStr(Minute(sTime))
    sMin = padL(sMin, 2, "0")
    
    sSec = CStr(Second(sTime))
    sSec = padL(sSec, 2, "0")
    
    timeToPrgCallBack = sYear & "-" & sMonth & "-" & sDay & Chr(32) & _
                        sHour & ":" & sMin & ":" & sSec
End Function

Function hourInMinutesToPrgCallBack(hourPar, minPar)
    Dim sTime, sTempTime
    Dim sHour, sMin, sSec
    
    If ( IsEmpty(hourPar)  ) Then
        sTime = DateAdd("n", minPar, Now)
    Else
        sTime = DateAdd("n", minPar, hourPar)
    End If
            
    sHour = CStr(Hour(sTime))
    sHour = padL(sHour, 2, "0")
    
    sMin = CStr(Minute(sTime))
    sMin = padL(sMin, 2, "0")
    
    'Return the minutes pasted after 0:00 hour
    hourInMinutesToPrgCallBack = ((CLng(sHour) * 60) + CLng(sMin))
End Function

'Eventos da telefonia

Sub API_AgentStateChanged()
   api_js_APIAgentStateChanged
End Sub

Sub API_APILogonState(State)
    api_js_APILogonState State
End Sub

Sub API_CallStateChanged()
   api_js_CallStateChanged
End Sub

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell(fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
 js_fg_AfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
 js_telefonia_AfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<script LANGUAGE="javascript" FOR=fg EVENT=BeforeSort>
<!--
fg_telefonia_BeforeSort(fg, arguments[0]);
//-->
</script>
<script LANGUAGE="javascript" FOR=fg EVENT=AfterSort>
<!--
fg_telefonia_AfterSort(fg, arguments[0]);
//-->
</script>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
var glb_PassedOneInDblClick;
glb_PassedOneInDblClick = false;

if (glb_PassedOneInDblClick == true)
{
    glb_PassedOneInDblClick = false;
    return;
}

glb_PassedOneInDblClick = true;

js_fg_telefonia_DblClick(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=overflyGen EVENT=TimerPulse>
<!--
 overflyGen_TimerPulse();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=overflyGen EVENT=UserIsIdle>
<!--
 overflyGen_UserIsIdle();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=overflyGen EVENT=UserIsOperating>
<!--
 overflyGen_UserIsOperating();
 //-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=overflyGen EVENT=ModlessClose>
<!--
	overflyGen_ModlessClose(arguments[0], overflyGen.DlgModlessBtnThatClose);
//-->
</SCRIPT>

</head>

<body id="telefoniaBody" name="telefoniaBody" LANGUAGE="javascript" onload="return window_onload()" onbeforeunload="return window_onbeforeunload()" onunload="return window_onunload()">

    <!-- Objeto OverflyGen -->
    <object CLASSID="clsid:CBD8996E-E238-4A48-AB72-7A7250F700C1" ID="overflyGen" HEIGHT="0" WIDTH="0"></object>

    <!-- Objeto Telefonia -->
    <object CLASSID="clsid:<%Response.Write VOICE_OCX_CLSID%>" ID="API" HEIGHT="0" WIDTH="0"></object>

    <div id="divBarSup" name="divBarSup" class="divExtern">
    
        <input type="button" id="btnChamadasCB" name="btnChamadasCB" class="btnGeneral" value="Chamadas" title="Comutar para Callbacks" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
    
        <div id="divChamadas" name="divChamadas" class="divExtern">
            <input type="button" id="btnDiscarCC" name="btnDiscarCC" class="btnGeneral" value="Discar" title="Efetuar uma liga��o" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
            <input type="text" id="txtNumeroDiscarCC" name="txtNumeroDiscarCC" class="fldGeneral" title="Numero a discar"></input>
            <input type="button" id="btnAtender" name="btnAtender" class="btnGeneral" value="Atender" title="Atender a liga��o" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
            <input type="button" id="btnEspera" name="btnEspera" class="btnGeneral" value="Espera" title="Colocar a liga��o em espera" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
            <input type="radio" id="rdTransferir" name="rdTransferir" class="btnGeneral" title="Desviar liga��es para o ramal" language=javascript onclick="return rdsPadraoOnClick(this)" onmousedown="return rdsPadraoOnMouseDown(this)" onkeydown="return rdsPadraoOnKeyDown(this)"></input>
            <input type="button" id="btnTransferir" name="btnTransferir" class="btnGeneral" value="Transfer" title="Transferir a liga��o" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
            <input type="button" id="btnConferencia" name="btnConferencia" class="btnGeneral" value="Confer" title="Fazer confer�ncia" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
            <input type="button" id="btnDesligar" name="btnDesligar" class="btnGeneral" value="Desligar" title = "Desligar a liga��o" onclick="return btnTelefoniaOnClick(this)"></input>
            
            <input type="radio" id="rdCallback" name="rdCallback" class="btnGeneral" title="Desviar liga��es para Callback" language=javascript onclick="return rdsPadraoOnClick(this)" onmousedown="return rdsPadraoOnMouseDown(this)" onkeydown="return rdsPadraoOnKeyDown(this)"></input>
            <input type="button" id="btnCallback" name="btnCallback" class="btnGeneral" value="Callback" title="Colocar a liga��o em Callback" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
            <input type="radio" id="rdCorreioVoz" name="rdCorreioVoz" class="btnGeneral" title="Desviar liga��es para Correio de Voz" language=javascript onclick="return rdsPadraoOnClick(this)" onmousedown="return rdsPadraoOnMouseDown(this)" onkeydown="return rdsPadraoOnKeyDown(this)"></input>
            <input type="button" id="btnCorreioVoz" name="btnCorreioVoz" class="btnGeneral" value="Correio" title="Transferir a liga��o para Correio de Voz" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
            <p id="lblNumMsgCorreio" name="lblNumMsgCorreio" class="fldGeneral"></p>
            <input type="radio" id="rdURA" name="rdURA" class="btnGeneral" title="Desviar liga��es para URA" language=javascript onclick="return rdsPadraoOnClick(this)" onmousedown="return rdsPadraoOnMouseDown(this)" onkeydown="return rdsPadraoOnKeyDown(this)"></input>
            <input type="button" id="btnURA" name="btnURA" class="btnGeneral" value="URA" title = "Transferir a liga��o para URA" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
        </div>
    
        <div id="divCallbacks" name="divCallbacks" class="divExtern">
            <input type="button" id="btnAgendarCB" name="btnAgendarCB" class="btnGeneral" value="Agendar" title="Agendar uma chamada nova" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
            <input type="text" id="txtNumeroAgendarCB" name="txtNumeroAgendarCB" class="fldGeneral" title="Numero a discar"></input>
			<input type="checkbox" id="chkLigarDireto" name="chkLigarDireto" class="fldGeneral"></input>
            <input type="button" id="btnLigar" name="btnLigar" class="btnGeneral" value="Ligar" title="Efetuar imediatamente a liga��o selecionada" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
            <input type="button" id="btnReprogramar" name="btnReprogramar" class="btnGeneral" value="Reprog" title="Reprogramar hor�rio da liga��o" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
            <select id="selTimeInterval" name="selTimeInterval" class="fldGeneral" title="Intervalo de tempo" language=javascript onchange="return selTimeIntervalOnChange(this)">
                <option value=60>+60 min</option>
                <option value=30>+30 min</option>
                <option value=15>+15 min</option>
                <option value=5>+5 min</option>
                <option value=-5>-5 min</option>
                <option value=-15>-15 min</option>
                <option value=-30>-30 min</option>
                <option value=-60>-60 min</option>
            </select>
            <input type="button" id="btnCancelar" name="btnCancelar" class="btnGeneral" value="Cancelar" title="Cancelar a liga��o" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
            <input type="button" id="btnRefreshCB" name="btnRefreshCB" class="btnGeneral" value="Refresh" title="Refrescar o grid" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
            <p id="lblMessageCB" name="lblMessageCB" class="fldGeneral"></p>
        </div>
    
        <div id="divCommonControls" name="divCommonControls" class="divExtern">
            <input type="button" id="btnPessoa" name="btnPessoa" class="btnGeneral" value="Pessoa" title="Detalhar Pessoa" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
            <input type="button" id="btnListaPrecos" name="btnListaPrecos" class="btnGeneral" value="Pre�os" title="Detalhar Lista de Pre�os" language=javascript onclick="return btnTelefoniaOnClick(this)"></input>
        </div>
    </div>

    <div id="divFG" name="divFG" class="divExtern">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
    </div>

    <textarea id="txtAreaTrace" name="txtAreaTrace" class="fldGeneral"></textarea>
    <input type="button" id="btnClearTrace" name="btnClearTrace" class="btnGeneral" value="Clear Trace" onclick="return btnTelefoniaOnClick(this)"></input>
    
</body>

</html>
