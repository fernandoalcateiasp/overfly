/********************************************************************
telefonia.js

Library javascript para o telefonia.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// Variavel para exibir ou nao msg no beforeunload
var beforeUnloadMsg = true;
// Controla estado de travamento do browser filho
var __bInterfaceLocked = false;

var glb_groupsGap = 6;
var glb_btnTop = 1;
var glb_btnHeight = 24;
var glb_btnWidth = 66;
var glb_btnDeltaWidth = 20;

var glb_rdGap = 2;
var glb_rdTop = 5;
var glb_rdWidth = 17;
var glb_rdHeight = 17;

var glb_lastStateRd = null;

var glb_telefoniaTimer = null;

// Usuario esta ou nao operando o Overfly
// var glb_UserTimerInterval = (1000 * 60 * 3);
// var glb_UserTimerInterval = (1000 * 15);

var glb_LASTLINESELID = '';

var glb_bTelephonyIsLoading = true;

var glb_currCCSelInGrid = null;
var glb_currCBSelInGrid = null;

var glb_aStatesBitMask = null;

var glb_RefreshCallbackDataTimer = null;
var glb_RefreshCallbackDataExecuting = false;

var glb_TimerPulseInterval = 30;

// As variaveis abaixo precisam ser no formato hh:mm
var glb_MAX_HOURCB = '18:00';
var glb_MIN_HOURCB = '08:00';

var glb_VOICEMAILNUM = 2666;
var glb_VOICEMAILNAME = 'Correio de Voz';

var glb_OPERADORA = '015';

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RESUMO DAS FUNCOES

window_onload()
window_onbeforeunload()
window_onunload()
sendJsCarrierFromForm(idElement, param1, param2, especChildBrowserName)
wndJSProc(idElement, msg, param1, param2)
carrierArrived(idElement, idBrowser, param1, param2)
carrierReturning(idElement, idBrowser, param1, param2)

overflyGen_TimerPulse()

overflyGen_UserIsIdle()
overflyGen_UserIsOperating()

overflyGen_ModlessClose(msgID, btnID)

userIsOperating()
userIsNotOperating()

setupPage()
adjustToobar()
adjustDivChamadas()
adjustDivCallbacks()
adjustDivCommonControls()

changeDivsBackgroundColor(mode, bForceCC_BKGrd)

btnChamadasCBClick(ctrl)
getInterfaceMode()
setInterfaceMode(mode)
putInModeChamadas()
putInModeCallbacks()

btnPessoaClick(ctrl)
btnListaPrecosClick(ctrl)
listaPrecos_DSC()

rdsPadraoOnKeyDown(ctrl)
rdsPadraoOnMouseDown(ctrl)
rdsPadraoOnClick(ctrl)

btnDiscarCCOnClick(ctrl)
btnAtenderOnClick(ctrl)
btnEsperaOnClick(ctrl)
btnTransferirOnClick(ctrl)
btnConferenciaOnClick(ctrl)
btnDesligarOnClick(ctrl)

btnCallbackOnClick(ctrl)
btnCorreioVozOnClick(ctrl)
btnURAOnClick(ctrl)

btnAgendarCBOnClick(ctrl)
btnLigarOnClick(ctrl)
btnReprogramarClick(ctrl)
btnCancelarClick(ctrl)
btnRefreshCBClick(ctrl)

lblNumMsgCorreio_ondblclick()

selTimeIntervalOnChange(ctrl)
chkCallback()

putHeaderLineInGrid(grid)

initializeCCAndCB()

getCallbackData()
getCallbackData_DSC()
refreshCallbackData_DSC()
go_getCallbackDataError_DSC()
refreshCallbackData()

setGridAndLogUser()
logInOnCentral()

fillGridInChamadasMode()
fillGridInCallbacksMode()

go_fillgridChamadas()
go_resetGridInChamadasMode()

blinkTelefonia()

fillMessageCB()

selGridLineByCall()
putBitValuesInControls()
especificToAttendDigital()
callPassedTroughtURA()
setControlsStateInBar()
callInLineGridCanTransfer()
callInLineGridCanConfer()
callHasState26()
operationByRadioButtons()
hasRdBtnChecked()
cbInValidHour()

js_fg_telefonia_DblClick( grid, Row, Col)
js_telefonia_AfterRowColChange
fg_telefonia_BeforeSort(grid, col)
fg_telefonia_AfterSort(grid, col)

useTrace(bUse)
btnClearTraceOnClick()
trace(txtToShow)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
	var aEmpresa = getCurrEmpresaData();
	
    if (param1 == 'TELEFONIA_DIAL')
    {
        if ( !glb_isLogged )
            return null;
        
        // param2 - traz array:
            // o DDD do numero do telefone a discar
            // o numero do telefone a discar
            // o PessoaID proprietario do telefone

        param2[0] = trimStr(param2[0]);
        param2[1] = trimStr(param2[1]);
        
		// Se nao veio o numero do telefone
		if ( (param2[1] == null) || (param2[1] == '') )
			return null;

		if (param2[2] != null)
			glb_nPessoaIDByCarrier = param2[2];
		else    
			glb_nPessoaIDByCarrier = 0;

        var strPars = '?nPessoaID=' + glb_nPessoaIDByCarrier +
			'&nEmpresaID=' + escape(aEmpresa[0]) +
			'&sDDD=' + escape(param2[0]) +
			'&sNumero=' + escape(param2[1]);

		dsoPessoaData.URL = SYS_ASPURLROOT + '/telefonia/serverside/getpessoadata.asp'+strPars;
		dsoPessoaData.ondatasetcomplete = dsoPessoaData_DSC;
		dsoPessoaData.refresh();
        return new Array(getHtmlId(), null, null);
    }
    // Programador alterar conforme necessario
    return null;
}

function dsoPessoaData_DSC()
{
	var aEmpresa = getCurrEmpresaData();
	var nCidadeEmpresaID = aEmpresa[2];
	var nDDDEmpresa = 0;
	var nCidadePessoa = 0;
	var bCelular = false;
	var sDDD = '';
	var sNumero = '';

	if (!((dsoPessoaData.recordset.BOF)&&(dsoPessoaData.recordset.EOF)))
	{
		while (!dsoPessoaData.recordset.EOF)
		{
			//
			if (dsoPessoaData.recordset('Indice').value == 1)
			{
				nDDDEmpresa = dsoPessoaData.recordset('Parametro').value;
			}
			else if (dsoPessoaData.recordset('Indice').value == 2)
			{
				nCidadePessoa = dsoPessoaData.recordset('Parametro').value;
			}
			else if (dsoPessoaData.recordset('Indice').value == 3)
			{
				bCelular = (dsoPessoaData.recordset('Parametro').value == 121);
			}
			
			sDDD = dsoPessoaData.recordset('DDD').value;
			sNumero = dsoPessoaData.recordset('Numero').value;
			
			dsoPessoaData.recordset.moveNext();
		}

		if (sDDD != nDDDEmpresa)
			txtNumeroDiscarCC.value = '015' + sDDD + sNumero;
		else if ( (!bCelular) && (nCidadePessoa!=nCidadeEmpresaID) )
			txtNumeroDiscarCC.value = '015' + sDDD + sNumero;
		else
			txtNumeroDiscarCC.value = sNumero;

		telefoniaDiscarCC(btnDiscarCC);
	}
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{
    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Evento de timer do OverflyGen
********************************************************************/
function overflyGen_TimerPulse()
{
    chkCallback();
}

/********************************************************************
Eventos de acao do usuario no OverflyGen
********************************************************************/
function overflyGen_UserIsIdle()
{
	userIsNotOperating();
}

/********************************************************************
Eventos de acao do usuario no OverflyGen
********************************************************************/
function overflyGen_UserIsOperating()
{
	userIsOperating();
}

/********************************************************************
Eventos de fechamento de mensagem modless no OverflyGen
********************************************************************/
function overflyGen_ModlessClose(msgID, btnID)
{
	switch (msgID)
	{
		case _MSG_GANCHO_ID:
		{
			setControlsStateInBar();
		}
		break;

		case _MSG_RAMALVALIDO_ID:
		{
			window.focus();
                
			if ( !txtNumeroDiscarCC.disabled )
				txtNumeroDiscarCC.focus();

            rdCallback.checked = false;
            //rdCorreioVoz.checked = false;
            rdURA.checked = false;
		
			setControlsStateInBar();
		}
		break;

		case _MSG_RAMALVALIDO2_ID:
		{
            window.focus();
            if ( !txtNumeroDiscarCC.disabled )
				txtNumeroDiscarCC.focus();

			setControlsStateInBar();
		}
		break;
		
		case _MSG_NUMEROVALIDO_ID:
		{
			window.focus();
        
			if ( !txtNumeroAgendarCB.disabled )
				txtNumeroAgendarCB.focus();
    
			txtNumeroAgendarCB.disabled = false;
			setControlsStateInBar();
		}
		break;

		case _MSG_NUMEROVALIDO2_ID:
		{
			window.focus();
			if ( !txtNumeroDiscarCC.disabled )
				txtNumeroDiscarCC.focus();

			txtNumeroDiscarCC.disabled = false;
			setControlsStateInBar();
		}
		break;
		
		case _MSG_CALLBACKFORAHORARIO_ID:
		{
			setControlsStateInBar();
		}
		break;

		default:
			return null;
	}
}

/********************************************************************
Percorre o callback para checar se tem ligacoes pendentes de faze-las
********************************************************************/
function chkCallback()
{
    if (dsoCallbacks.recordset.Fields.Count == 0)
    {
		if ( getInterfaceMode() == 'CC' )
		{
			changeDivsBackgroundColor('CC', null);				
		}	
        return null;
    }    
        
    if (dsoCallbacks.recordset.BOF && dsoCallbacks.recordset.EOF)
    {
		if ( getInterfaceMode() == 'CC' )
		{
			changeDivsBackgroundColor('CC', null);				
		}	
        return null;
    }    
        
    var currDateTime = new Date();        

    dsoCallbacks.recordset.moveFirst();

    while (!dsoCallbacks.recordset.EOF)
    {
        if (dsoCallbacks.recordset('Data').value < currDateTime)
		{
			// 06/12/2002 - Comuta para callback ou da refresh
			// no mesmo se ja esta em callback
			// trapeado
			/**********************
			if ( getInterfaceMode() == 'CC' )    
				btnChamadasCBClick(btnChamadasCB);
			else				
				btnRefreshCBClick(btnRefreshCB);								
			**********************/
			if ( getInterfaceMode() == 'CC' )
			{
				changeDivsBackgroundColor('CC', true);				
			}	

            break;
		}            
        dsoCallbacks.recordset.moveNext();
    }
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	// Ze em 17/03/08
	dealWithObjects_Load();
	dealWithGrid_Load();
	// Fim de Ze em 17/03/08
	
    var elem;
    elem = divCallbacks;
    elem.style.visibility = 'hidden';
    elem = divChamadas;
    elem.style.visibility = 'hidden';
    
    // variavel do js_gridEx.js
    glb_HasGridButIsNotFormPage = true;
    
    // ajusta parametros fixos dos dsos da pagina
    dsoFixedParams();
    
    // coloca classe nos grids
    elem = window.document.getElementById('fg');
    
    if ( elem != null )
        elem.className = 'fldGeneral';
    
    elem = window.document.getElementById('fg');
    
    if ( elem != null )
        elem.className = 'fldGeneral';
    
    elem = document.getElementById('telefoniaBody');
    with (elem)
    {
        style.width = '100%';
    }    
    
    setupPage();
    
	// Translada interface pelo dicionario do sistema
	translateInterface(window, null);
	
	window.document.title = translateTerm(window.document.title, null);

    // ajusta o body do html
    elem = document.getElementById('telefoniaBody');
    with (elem)
    {
        style.width = '100%';
        style.backgroundColor = 'white';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // Forca scroll para o topo da pagina
    elem.scrollIntoView(true);
    
    // forca preenchimento do array de controle de travamento dos
    // elementos da interface
    lockControls(true);
    lockControls(false);
    
    rdURA.style.visibility = 'hidden';
            
    // inicializa interface de chamadas
    // e obtem os dados das chamadas em callback
    initializeCCAndCB();
}    

/********************************************************************
Confirma se deve ou nao fechar o browser
********************************************************************/
function window_onbeforeunload()
{
    try
    {
        overflyGen.TimerPulseInterval = 0;
    
        if (dsoCallbacks.recordset.Fields.Count == 0)
            return null;

        // Salva variavel controladora de fechamento do Overfly
        var numCBs = dsoCallbacks.recordset.RecordCount;
        var eventText = '';

        sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CBPENDENTES', numCBs);
               
        if ( beforeUnloadMsg )
        {
            if ( numCBs == 1 )
                eventText = '\n\n\n' + SYS_NAME + '\n' + 'Existe ' + numCBs + ' liga��o em callback!!!' + '\n\n\n';        
            else if ( numCBs > 1 )
                eventText = '\n\n\n' + SYS_NAME + '\n' + 'Existem ' + numCBs + ' liga��es em callback!!!' + '\n\n\n';
            else
                eventText = SYS_NAME;
                
            event.returnValue = eventText;
        }
    
        if (glb_isLogged)
            overflyGen.TimerPulseInterval = glb_TimerPulseInterval;
    }        
    catch(e)
    {
        window.close();
    }    
}

/********************************************************************
Diversos cleanup do sistema
********************************************************************/
function window_onunload()
{
	dealWithObjects_Unload();
	
    // o if abaixo deve estar aqui obrigatoriamente
    try
    {
        overflyGen.TimerPulseInterval = 0;
    
        // tenta desconectar a telefonia
        telefoniaDisconnect();
    
        if ( window.opener && !window.opener.closed )
        {
           removeChildBrowserFromControl(window.name);
           
           // no caso de interrupcao do carregamento do browser filho
           // destrava a interface do browser mae
           // implantacao em 07/08/2001 - remover se comportamento
           // estranho do sistema
           sendJSMessage('telefoniaHtml', JS_CHILDBROWSERLOADED, null, null);
           
           // fechou a janela de telefonia
           sendJSMessage(getHtmlId(), JS_NOMFORMCLOSE, 1, null);
        }   
    }
    catch(e)   
    {
        ;
    }
}

/********************************************************************
Funcao de comunicacao do carrier:
Chamada do arquivo js_sysbase.js.
Veio da funcao sendJSCarrier definida no arquivo js_sysbase.js e chamada
em qualquer arquivo de form.

Localizacao:
telefonia.asp

Chama:
Funcao sendJsCarrierFromChildBrowser(window.top.name,
                                     idElement, param1, param2)

do arquivo overfly.asp do browser mae.

Parametros:
idElement             - Obrigatorio. Id do html que manda o carrier.
param1                - De uso do programador. Qualquer objeto ou null.
param2                - De uso do programador. Qualquer objeto ou null.
especChildBrowserName - De uso do programador. Nome de um browser filho
                        para receber o carrier. Se null o carrier sera
                        propagado para todos os browsers.

Retorno     - null se nao tem browser filho.
********************************************************************/
function sendJsCarrierFromForm(idElement, param1, param2, especChildBrowserName)
{
    var wndTop = window.opener;

	if (wndTop == null)
		return null;

    // Chamada do carrier. Chama funcao do browser mae no overfly.asp
    wndTop.sendJsCarrierFromChildBrowser(window.top.name, idElement,
                                         param1, param2,
                                         especChildBrowserName);
}

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
        // Msg do carrier chegada vindo de um outro form
        // Se intercepta sempre devolver array:
        // array[0] = getHtmlId() ou null
        // array[1] = qualquer coisa ou null
        // array[2] = qualquer coisa ou null
        case JS_CARRIERCAMING :
        {
            return carrierArrived(idElement[0], idElement[1], param1, param2);
        }    
        break;
        
        // Msg do carrier retorno vindo de um form que interceptou
        // Veja nota em case JS_CARRIERCAMING. Sempre tras:
        // idElement[0] = null ou id do html do arquivo que interceptou
        // idElement[1] = nome do browser filho que contem o arquivo
        //                que interceptou
        // param1 e o array[1] passado = qualquer coisa ou null
        // param2 e o array[2] passado = qualquer coisa ou null
        case JS_CARRIERRET :
        {
            lockInterface(false);
            return carrierReturning(idElement[0], idElement[1], param1, param2);
        }
        break;    
        
        case JS_MAINBROWSERCLOSING :
        {
            overflyGen.ForceDlgModalClose();
            overflyGen.ForceDlgModlessClose();
            
            beforeUnloadMsg = false;
            window.close();
        }
        break;
        
        // retorna ou altera e retorna o status da flag de interface
        // interface travada do browser filho
        // param1 == false -> so retorna
        case JS_CHILDBROWSERLOCKED:
        {
            if ( param1 == false )
                return __bInterfaceLocked;
            else if ( param1 == true )    
            {
                __bInterfaceLocked = ! __bInterfaceLocked;
                return __bInterfaceLocked;
            }    
        }
        break;
        
        case JS_WIDEMSG:
        {
            if ( param1 == JS_FOCUSFORM )
            {
                window.focus();
                return 0;
            }    
        }
        break;    
                     
        default:
            return null;
    }
}

/********************************************************************
Ajusta interface da pagina
********************************************************************/
function setupPage()
{
    var elem, ctrl;
        
    adjustToobar();
    adjustDivChamadas();
    adjustDivCallbacks();
    adjustDivCommonControls();
    
    // O div do grid de chamadas
    elem = divFG;
    with ( elem.style )
    {
        left = 0;
        top = parseInt(divBarSup.currentStyle.top, 10) +
              parseInt(divBarSup.currentStyle.height, 10) + 1;
        width = telefoniaBody.offsetWidth - 4;
        backgroundColor = 'transparent';
        visibility = 'hidden';
        height = Math.abs(telefoniaBody.offsetHeight - parseInt(elem.currentStyle.top, 10)) - 2;
    }
    elem.scroll = 'no';
    
    // o grid de chamadas
    elem = fg;
    with ( elem.style )
    {
        left = ELEM_GAP;
        top = ELEM_GAP;
        width = parseInt(divFG.currentStyle.width) - (2 * ELEM_GAP);
        height = parseInt(divFG.currentStyle.height) - (2 * ELEM_GAP);
        paddingLeft = 5;
        paddingRigth = 5;
        visibility = 'hidden';
    }
    
    putHeaderLineInGrid(elem);
    
    putBitValuesInControls();
    
    // usa ou nao o trace
    useTrace(false);
    //useTrace(true);
}

/********************************************************************
Usar txt e botao de Trace
********************************************************************/
function useTrace(bUse)
{
    // txtAreaTrace
    elem = txtAreaTrace;
    with ( elem.style )
    {
        left = ELEM_GAP;
        top = parseInt(divFG.currentStyle.top, 10) +
              parseInt(divFG.currentStyle.height, 10) + 1;
        width = telefoniaBody.offsetWidth - (2 * ELEM_GAP) - 4;
        backgroundColor = 'yellow';
        visibility = 'inherit';
        height = 400;
    }
    txtAreaTrace.value = '';
    
    // btnClearTrace
    elem = btnClearTrace;
    with ( elem.style )
    {
        left = ELEM_GAP;
        top = parseInt(txtAreaTrace.currentStyle.top, 10) +
              parseInt(txtAreaTrace.currentStyle.height, 10) + 1;
        width = telefoniaBody.offsetWidth - (2 * ELEM_GAP) - 4;
        visibility = 'inherit';
    }
    
    if ( !bUse )
    {
        txtAreaTrace.style.visibility = 'hidden'; 
        txtAreaTrace.disabled = true;
        btnClearTrace.style.visibility = 'hidden'; 
        btnClearTrace.disabled = true;  
    }
}

/********************************************************************
Programador clicou o botao de trace
********************************************************************/
function btnClearTraceOnClick()
{
    txtAreaTrace.value = '';
}

/********************************************************************
Texto a colocar no trace
********************************************************************/
function trace(txtToShow)
{
    txtAreaTrace.value = txtAreaTrace.value + txtToShow + '\n';
}

/********************************************************************
Ajusta o tollbar superior dos controles
********************************************************************/
function adjustToobar()
{
    var elem;
        
    // O div externo que e o control bar superior
    // div bar (externo aos div dos controles)
    elem = divBarSup;
    with ( elem.style )
    {
        left = 0;
        top = 0;
        width = telefoniaBody.offsetWidth - 4;
        height = glb_btnHeight + 2;
        backgroundColor = 'silver';
        visibility = 'inherit';
    }
    elem.scroll = 'no';
 
    // O botao chamadas em curso/callbacks
    elem = btnChamadasCB;
    with ( elem.style )
    {
        left = 1;
        top = glb_btnTop;
        width = glb_btnWidth;
        height = glb_btnHeight;
        visibility = 'inherit';
    }
}

/********************************************************************
Ajusta os controles de chamadas em curso
********************************************************************/
function adjustDivChamadas()
{
    var elem;
    var elemRight;
    
    elem = btnDiscarCC;
    with ( elem.style )
    {
        left = 1;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth + 3;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    elem.setAttribute('numberToCall', '0', 1);
    elem.setAttribute('numberInHold', '0', 1);
    
    elem = txtNumeroDiscarCC;
    with ( elem.style )
    {
        left = elemRight + 5;
        top = glb_btnTop + 1;
        width = glb_btnWidth + glb_btnDeltaWidth;
        height = 21;
        backgroundColor = 'white';
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    
    elem.setAttribute('thePrecision', 18, 1);
    elem.setAttribute('theScale', 0, 1);
    elem.setAttribute('verifyNumPaste', 1);
    elem.onkeypress = verifyNumericEnterNotLinked;
    elem.onkeyup = txtNumeroDiscarCC_OnKeyUp;
    elem.setAttribute('minMax', new Array(0, 999999999999999999), 1);
    elem.onfocus = selFieldContent;
    elem.maxLenght = 18;
    
    elem = btnAtender;
    with ( elem.style )
    {
        left = elemRight + 5;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth + 3;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    
    elem = btnEspera;
    with ( elem.style )
    {
        left = elemRight + 1;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth + 3;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    
    elem = rdTransferir;
    with ( elem.style )
    {
        left = elemRight + glb_rdGap;
        top = glb_rdTop;
        width = glb_rdWidth;
        height = glb_rdHeight;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    
    elem = btnTransferir;
    with ( elem.style )
    {
        left = elemRight + 1;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth + 3;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    
    elem = btnConferencia;
    with ( elem.style )
    {
        left = elemRight + 1;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth + 3;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    
    elem = btnDesligar;
    with ( elem.style )
    {
        left = elemRight + 1;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth + 3;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    
    elem = rdCallback;
    with ( elem.style )
    {
        left = elemRight + glb_rdGap;
        top = glb_rdTop;
        width = glb_rdWidth;
        height = glb_rdHeight;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    // Atributo para uso da automacao
    elem.setAttribute('byAuto', 0, 1);
    
    elem = btnCallback;
    with ( elem.style )
    {
        left = elemRight + 1;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    
    elem = rdCorreioVoz;
    with ( elem.style )
    {
        left = elemRight + glb_rdGap;
        top = glb_rdTop;
        width = glb_rdWidth;
        height = glb_rdHeight;
        // visibility = 'inherit';
        // Luciano pediu para esconder em 22/08/2006
        visibility = 'hidden';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    
    elem = btnCorreioVoz;
    with ( elem.style )
    {
        left = elemRight + 1;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth;
        // visibility = 'inherit';
        // luciano pediu para esconder este botao em 21/08/2006
        visibility = 'hidden';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }

    elem = lblNumMsgCorreio;
    with ( elem.style )
    {
        left = elemRight + 1;
        top = glb_btnTop;
        width = 23;
        visibility = 'inherit';
        backgroundColor = 'transparent';
        //elemRight = parseInt(elem.currentStyle.left, 10) + 
        //               parseInt(elem.currentStyle.width, 10);

		cursor = 'default';       
        textAlign = 'center';
        color = 'navy';
        paddingTop = 3;
    }
    elem.title = 'Mensagens no Correio de Voz\nPara acessar, tire o fone do gancho\ne clique duas vezes';
    elem.innerText = glb_qtyMsgsVoiceMail;
    elem.ondblclick = lblNumMsgCorreio_ondblclick;
    
    elem = rdURA;
    with ( elem.style )
    {
        left = elemRight + glb_rdGap;
        top = glb_rdTop;
        width = glb_rdWidth;
        height = glb_rdHeight;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
        
    elem = btnURA;
    with ( elem.style )
    {
        left = elemRight + 1 + 6;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    
    elem = divChamadas;
    with ( elem.style )
    {
        left = parseInt(btnChamadasCB.currentStyle.left, 10) +
               parseInt(btnChamadasCB.currentStyle.width, 10) + glb_groupsGap;
        top = 0;
        width = elemRight + 1;
        height = parseInt(divBarSup.currentStyle.height, 10);
        backgroundColor = 'silver';
        visibility = 'inherit';
    }
    elem.scroll = 'no';
}

/********************************************************************
Usuario teclou enter, disca chamadas em curso
********************************************************************/
function txtNumeroDiscarCC_OnKeyUp()
{
    if (event.keyCode != 13)
        rdTransferir.checked = false;

    if ( !((event.keyCode == 13) && ( !btnDiscarCC.disabled )) )
        return true;
        
    if ( getInterfaceMode() == 'CC' )    
        btnDiscarCCOnClick(btnDiscarCC);        
}

/********************************************************************
Ajusta os controles de chamadas em callback
********************************************************************/
function adjustDivCallbacks()
{
    var elem;
    var elemRight;

    elem = btnAgendarCB;
    with ( elem.style )
    {
        left = 1;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth + 3;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    elem = txtNumeroAgendarCB;
    with ( elem.style )
    {
        left = elemRight + 5;
        top = glb_btnTop + 1;
        width = glb_btnWidth + glb_btnDeltaWidth;
        height = 21;
        backgroundColor = 'white';
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    
    elem.setAttribute('thePrecision', 18, 1);
    elem.setAttribute('theScale', 0, 1);
    elem.setAttribute('verifyNumPaste', 1);
    elem.onkeypress = verifyNumericEnterNotLinked;
    elem.onkeyup = txtNumeroAgendarCB_OnKeyUp;
    elem.setAttribute('minMax', new Array(0, 9999999999), 1);
    elem.onfocus = selFieldContent;
    elem.maxLenght = 10;

	elem = chkLigarDireto;
    with ( elem.style )
    {
        left = elemRight + 7;
        top = glb_btnTop;
        width = 23;
        height = 23;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    elem.title = 'Ligar direto do telefone.\nTire o fone do gancho e clique o bot�o Ligar';

    elem = btnLigar;
    with ( elem.style )
    {
        left = elemRight + 1;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth + 3;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
        
    elem = btnReprogramar;
    with ( elem.style )
    {
        left = elemRight + 1;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth + 3;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }

    elem = selTimeInterval;
    with ( elem.style )
    {
        fontSize = '8pt';
        left = elemRight + 5;
        top = glb_btnTop + 2;
        width = glb_btnWidth;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    selTimeInterval.selectedIndex = 2;
        
    elem = btnCancelar;
    with ( elem.style )
    {
        left = elemRight + 5;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth + 3;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    
    elem = btnRefreshCB;
    with ( elem.style )
    {
        left = elemRight + 1;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth + 3;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    } 
    
    elem = lblMessageCB;
    with ( elem.style )
    {
        left = elemRight + 6;
        top = glb_btnTop;
        width = 4 *(glb_btnWidth - glb_btnDeltaWidth);
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
        paddingTop = 4;
        backgroundColor = 'transparent';               
    } 
        
    elem = divCallbacks;
    with ( elem.style )
    {
        left = parseInt(btnChamadasCB.currentStyle.left, 10) +
               parseInt(btnChamadasCB.currentStyle.width, 10) + glb_groupsGap;
        top = 0;
        width = elemRight + 1;
        height = parseInt(divBarSup.currentStyle.height, 10);
        backgroundColor = 'silver';
        visibility = 'hidden';
    }
    elem.scroll = 'no';
}

/********************************************************************
Usuario teclou enter, disca chamadas em callback
********************************************************************/
function txtNumeroAgendarCB_OnKeyUp()
{
    if ( !((event.keyCode == 13) && ( !btnAgendarCB.disabled )) )
        return true;
        
    if ( getInterfaceMode() == 'CB' )    
        btnAgendarCBOnClick(btnAgendarCB);        
}

/********************************************************************
Ajusta os controles comuns
********************************************************************/
function adjustDivCommonControls()
{
    var elem;
    var elemRight;
    
    elem = btnPessoa;
    with ( elem.style )
    {
        left = 1;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
    
    elem = btnListaPrecos;
    with ( elem.style )
    {
        left = elemRight + 1;
        top = glb_btnTop;
        width = glb_btnWidth - glb_btnDeltaWidth;
        visibility = 'inherit';
        elemRight = parseInt(elem.currentStyle.left, 10) + 
                       parseInt(elem.currentStyle.width, 10);
    }
            
    elem = divCommonControls;
    with ( elem.style )
    {
        top = 0;
        width = elemRight + 1;
        height = parseInt(divBarSup.currentStyle.height, 10);
        
        left = parseInt(divBarSup.currentStyle.width, 10) -
               parseInt(divCommonControls.currentStyle.width, 10) - 1;
        
        backgroundColor = 'silver';
        visibility = 'inherit';
    }
    elem.scroll = 'no';
}

/********************************************************************
Interface chaveou de CC para CB ou vice versa
********************************************************************/
function changeDivsBackgroundColor(mode, bForceCC_BKGrd)
{
    if ( mode == 'CB' )
    {
        divChamadas.style.backgroundColor = '#F0E68C';
        divCallbacks.style.backgroundColor = '#F0E68C';
        divCommonControls.style.backgroundColor = '#F0E68C';
        divBarSup.style.backgroundColor = '#F0E68C';
    }    
    else if ( (mode == 'CC') && (bForceCC_BKGrd == null) )
    {
        divChamadas.style.backgroundColor = 'silver';
        divCallbacks.style.backgroundColor = 'silver';
        divCommonControls.style.backgroundColor = 'silver';
        divBarSup.style.backgroundColor = 'silver';
    }
    else if ( (mode == 'CC') && (bForceCC_BKGrd == true) )
    {
        divChamadas.style.backgroundColor = 'red';
        divCallbacks.style.backgroundColor = 'red';
        divCommonControls.style.backgroundColor = 'red';
        divBarSup.style.backgroundColor = 'red';
    }
    
}

/********************************************************************
Usuario clicou um botao da interface
********************************************************************/
function btnTelefoniaOnClick(ctrl)
{
    if ( ctrl == btnChamadasCB)
        btnChamadasCBClick(ctrl);
    else if ( ctrl == btnDiscarCC)
        btnDiscarCCOnClick(ctrl);
    else if ( ctrl == btnAtender)
        btnAtenderOnClick(ctrl);
    else if ( ctrl == btnEspera)
        btnEsperaOnClick(ctrl);
    else if ( ctrl == btnTransferir)    
        btnTransferirOnClick(ctrl);
    else if ( ctrl == btnConferencia)
        btnConferenciaOnClick(ctrl);
    else if ( ctrl == btnDesligar)        
        btnDesligarOnClick(ctrl);
    else if ( ctrl == btnCallback)
        btnCallbackOnClick(ctrl);
    else if ( ctrl == btnCorreioVoz)    
        btnCorreioVozOnClick(ctrl);
    else if ( ctrl == btnURA)
        btnURAOnClick(ctrl);
    else if ( ctrl == btnAgendarCB)
        btnAgendarCBOnClick(ctrl);
    else if ( ctrl == btnLigar)    
        btnLigarOnClick(ctrl);
    else if ( ctrl == btnReprogramar)    
        btnReprogramarClick(ctrl);
    else if ( ctrl == btnCancelar)    
        btnCancelarClick(ctrl);
    else if ( ctrl == btnRefreshCB)    
        btnRefreshCBClick(ctrl);
    else if ( ctrl == btnPessoa)    
        btnPessoaClick(ctrl);
    else if ( ctrl == btnListaPrecos)    
        btnListaPrecosClick(ctrl);
    else if ( ctrl == btnClearTrace)
        btnClearTraceOnClick(ctrl);
}

/********************************************************************
Usuario clicou botao de modo da interface
********************************************************************/
function btnChamadasCBClick(ctrl)
{
    var currMode = ctrl.getAttribute('currMode', 1);
    
    fg.Rows = 1;
    
    if ( currMode != null )
        currMode = currMode.toUpperCase();
    
    if ( (currMode == null) || (currMode == 'CC') )
    {
        ctrl.value = 'Callbacks';
        ctrl.title = 'Comutar para Chamadas';
        putInModeCallbacks();
    }
    else if (currMode == 'CB')
    {
        if (glb_isLogged)
            overflyGen.TimerPulseInterval = glb_TimerPulseInterval;
            
        ctrl.value = 'Chamadas';
        ctrl.title = 'Comutar para Callbacks';
        putInModeChamadas();
    }
}

/********************************************************************
Retorna o modo da interface

chamadas ativas - CC
callback        - CB
********************************************************************/
function getInterfaceMode()
{
    var ctrl = btnChamadasCB;

    return ctrl.getAttribute('currMode', 1);
}

/********************************************************************
Seta o modo da interface (chamadas ativas ou callback)
mode
********************************************************************/
function setInterfaceMode(mode)
{
    if ( (mode == null) || (mode == '') )
        return null;
    
    mode = (mode.toString()).toUpperCase();
    
    if ( !((mode == 'CC') || (mode == 'CB')) )
        return null;
    
    var ctrl = btnChamadasCB;

    ctrl.setAttribute('currMode', mode, 1);
    
    changeDivsBackgroundColor(mode);
}

/********************************************************************
Coloca interface em modo de chamadas em curso
********************************************************************/
function putInModeChamadas()
{
    divChamadas.style.visibility = 'inherit';
    divFG.style.visibility = 'inherit';
    divCallbacks.style.visibility = 'hidden';
    
    setInterfaceMode('CC');
    
    fillGridInChamadasMode();
}

/********************************************************************
Coloca interface em modo de callbacks
********************************************************************/
function putInModeCallbacks()
{
    divCallbacks.style.visibility = 'inherit';
    divChamadas.style.visibility = 'hidden';
    divFG.style.visibility = 'inherit';
    
    setInterfaceMode('CB');
    
    fillGridInCallbacksMode();
}

/********************************************************************
Preenche o grid em modo de chamadas
********************************************************************/
function fillGridInChamadasMode()
{
    // Prepara o header do grid
    var nCurrCCSelInGrid = 0;
    
    if ((fg.Rows > 1) && (fg.Row > 0) && (fg.Cols >= 12))
        nCurrCCSelInGrid = fg.TextMatrix(fg.Row, 11);

    // zera a listagem
    fg.Rows = 1;
        
    fg.Editable = false;
    fg.Redraw = 0;
    
    headerGrid(fg,['DDI', 
                   'DDD', 
                   'Telefone', 
                   'ID', 
                   'Fantasia', 
                   'Classifica��o', 
                   'Cidade', 
                   'UF',
                   'Call Status', 
                   'Hora', 
                   'Contato',
                   'InternalCallID',
                   'CallStateID',
                   'CallStateToID',
                   'CallCause',
                   'Key',
                   'CBCount'],[0, 11, 12, 13, 14, 15, 16]);
                   // 'CallCause'],[]);
    
    fg.FontSize = '8';
    
    alignColsInGrid(fg,[0, 1, 2, 3]);               
    
    fg.ColKey(0) = 'DDI';
    fg.ColKey(1) = 'DDD';
    fg.ColKey(2) = 'Numero'; 
    fg.ColKey(3) = 'PessoaID';
    fg.ColKey(4) = 'Fantasia';
    fg.ColKey(5) = 'Classificacao';
    fg.ColKey(6) = 'Cidade';
    fg.ColKey(7) = 'UF';
    fg.ColKey(8) = 'CallState';
    fg.ColKey(9) = 'Data';
    fg.ColKey(10) = 'Contato';
    fg.ColKey(11) = 'InternalCallID';
    fg.ColKey(12) = 'CallStateID';
    fg.ColKey(13) = 'CallStateToID';
    fg.ColKey(14) = 'CallCause';
    fg.ColKey(15) = 'Key';
    fg.ColKey(16) = 'CBCount';
    
    if ( numElemsInCCArray('F') == 0 )
        go_resetGridInChamadasMode();
    else
        go_fillgridChamadas();
    
    if ( nCurrCCSelInGrid != 0 )
        glb_currCCSelInGrid = nCurrCCSelInGrid;
    
    selGridLineByCall();
    
    setControlsStateInBar();
}

/********************************************************************
Preenche o grid em modo de callbacks
********************************************************************/
function fillGridInCallbacksMode()
{
    fg.Rows = 1;
    
    refreshCallbackData();
}

/********************************************************************
Zera grid em modo chamadas
********************************************************************/
function go_resetGridInChamadasMode()
{
    if ( glb_telefoniaTimer != null )
    {
        window.clearInterval(glb_telefoniaTimer);
        glb_telefoniaTimer = null;
    }

    overflyGen.StopSpeakerBeep();

    fg.Editable = false;
    fg.ExplorerBar = 5;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;    
        
    // destrava interface
    lockControls(false);
    
    setControlsStateInBar();
}

/********************************************************************
Reseleciona linha no grid corrente em funcao da ultima chamada selecionada
********************************************************************/
function selGridLineByCall()
{
    var i;
    
    if ( fg.Rows <= 1 )
        return null;
    
    var interfaceMode = getInterfaceMode();
    var lineToSel = null;
    var gridCol = null;
    
    // chamadas em curso
    if ( interfaceMode == 'CC' )
    {
        lineToSel = glb_currCCSelInGrid;
        gridCol = 12;
    }    
    // chamadas em callback
    else if ( interfaceMode == 'CB' )
    {
        lineToSel = glb_currCBSelInGrid;
        gridCol = 10;
    }    

    if ( (lineToSel == null) || (gridCol == null) )        
    {
        fg.Row = 1;
        return null;
    }    
    
    fg.Row = 1;
        
    for ( i=1; i<fg.Rows; i++ )
    {
        if ( fg.ValueMatrix(i, gridCol) == lineToSel )
        {
            fg.Row = i;
            break;
        }
    }
    
    return null;
}

/************************************************************************************
Habilita ou desabilita os controles do toolbar de acordo com o estado das ligacoes
************************************************************************************/
function setControlsStateInBar()
{
	if ( window.top.overflyGen.DlgModalIsOpened != 0)
	{
		lockControls(true);
		return null;
	}
	else
		lockControls(false);

    var bHasRows = fg.Rows > 1;
    var i, j, k;
    var nCurrState = null;
    var nCBStatusID = null;
    var nCurrStatePos = -1;
    var coll = null;
    var elem;

    if (bHasRows)
    {
        nCurrState = parseInt(getCellValueByColKey(fg, 'CallStateID', fg.Row), 10);
        
        nCBStatusID = getCellValueByColKey(fg, 'CBStatusID', fg.Row);
        
        if ( (nCBStatusID != null) && (! isNaN(nCBStatusID)) )
			nCBStatusID = parseInt(nCBStatusID, 10);
    }    

    if ( (nCurrState != null) && (! isNaN(nCurrState)) )
    {
        for (i=0; i<glb_aStatesBitMask.length; i++)
        {
            if (nCurrState == glb_aStatesBitMask[i][0])
            {
                nCurrStatePos = i;
                break;
            }
        }
    }

    for (k=0; k<2; k++)
    {
        coll = window.document.getElementsByTagName((k==0 ? 'INPUT' : 'SELECT'));
        
        for ( j=0; j<coll.length; j++ )
        {
            elem = coll.item(j);
            
            if ( ((elem.parentElement).currentStyle.visibility == 'hidden') ||
                 (elem.currentStyle.visibility == 'hidden') )
                continue;
            
            if ( (elem.type != 'button') && (elem.type != 'radio') &&
                 (elem.type != 'select-one') )
                continue;
    
            if (elem.getAttribute('bitValue', 1) != null)
            {
                if (nCurrStatePos != -1)
                {
                    elem.disabled = ! ((glb_aStatesBitMask[nCurrStatePos][1] & elem.getAttribute('bitValue', 1)) ==
                        elem.getAttribute('bitValue', 1));
                }
                else
                    elem.disabled = true;
                
                // Botoes que serao sempre habilitados
                if ((1 & elem.getAttribute('bitValue', 1)) == 1)
                    elem.disabled = false;
                    
                // Botoes que so depedendem de ter linha no grid p/ habilitar
                if ( ((2 & elem.getAttribute('bitValue', 1)) == 2 ) && (fg.Rows > 1) )
                    elem.disabled = false;
            }

            if ( (elem.type == 'select-one') && (elem.options.length == 0) )
            {
                elem.disabled = true;
                continue;
            }

            if (elem.getAttribute('evalToEnable', 1) != null)
            {
                if ( ! eval(elem.getAttribute('evalToEnable', 1)) )
                    elem.disabled = true;
                    continue;
            }
            
            if ( nCBStatusID == 6 )
            {
				btnLigar.disabled = true;
				btnReprogramar.disabled = true;
				btnCancelar.disabled = true;
            }
        }
    }
}

/********************************************************************
Retorna true se e valido fazer transferencia para a corrente
linha de grid. Caso contrario retorna false.
********************************************************************/
function callInLineGridCanTransfer()
{
    if ( getInterfaceMode() != 'CC' )
        return true;
    
    if ( (fg.Rows > 0) && (fg.Row < 1) )
        return true;
    
    var i;
    var stateID = parseInt(fg.TextMatrix(fg.Row, 12), 10);
    var internalCallID = fg.TextMatrix(fg.Row, 11);
    var callStateToID = getCellValueByColKey(fg, 'CallStateToID', fg.Row);
    var bHasHoldCall = false;
    var bHasActCall = false;

    if ( !((stateID == 7) ||
           (stateID == 10) ||
           (stateID == 26)) )
        return false;
    
    if ( (stateID == 1) || (stateID == 7) || (stateID == 26 && callStateToID == 7) )
        return true;

    for(i=1; i<=API.Calls.Count; i++)
    {
        if ( API.Calls(i).InternalCallID == internalCallID )
        {
            // verifica chamada em hold
            if (API.Calls(i).CallState == 10)
            {
                bHasHoldCall = true;
                break;
            }
        }
    }

    if ( !bHasHoldCall )
        return false;

    for(i=1; i<=API.Calls.Count; i++)
    {
        if ( API.Calls(i).InternalCallID == internalCallID )
        {
            // verifica a chamada ativa
            if ( (API.Calls(i).CallState == 7) || (API.Calls(i).CallState == 26) )
            {
                bHasActCall = true;
                break;
            }
        }
    }
    
    return bHasActCall;
}

/********************************************************************
Retorna true se e valido fazer conferencia para a corrente
linha de grid. Caso contrario retorna false.
********************************************************************/
function callInLineGridCanConfer()
{
    if ( getInterfaceMode() != 'CC' )
        return true;
    
    if ( (fg.Rows > 0) && (fg.Row < 1) )
        return true;
    
    var i;
    var stateID = parseInt(fg.TextMatrix(fg.Row, 12), 10);
    var internalCallID = fg.TextMatrix(fg.Row, 11);
    var bHasHoldCall = false;
    var bHasActCall = false;

    if ( !((stateID == 7) ||
           (stateID == 10) ||
           (stateID == 26)) )
        return false;
    
    for(i=1; i<=API.Calls.Count; i++)
    {
        if ( API.Calls(i).InternalCallID == internalCallID )
        {
            // verifica chamada em hold
            if (API.Calls(i).CallState == 10)
            {
                bHasHoldCall = true;
                break;
            }
        }
    }

    if ( !bHasHoldCall )
        return false;

    for(i=1; i<=API.Calls.Count; i++)
    {
        if ( API.Calls(i).InternalCallID == internalCallID )
        {
            // verifica a chamada ativa
            if ( (API.Calls(i).CallState == 7) || (API.Calls(i).CallState == 26) )
            {
                bHasActCall = true;
                break;
            }
        }
    }
    
    return bHasActCall;
}

/********************************************************************
Tratamento para chamada state 26 - 'ORIGINADO'

Retorna true se pode liberar o botao, caso contrario retorna false
********************************************************************/
function callHasState26()
{
    if ( getInterfaceMode() != 'CC' )
        return true;
    
    if ( (fg.Rows > 0) && (fg.Row < 1) )
        return true;
    
    var i;
    var callStateID = parseInt(fg.TextMatrix(fg.Row, 12), 10);
    var callStateToID = parseInt(fg.TextMatrix(fg.Row, 13), 10);
    var callCauseID = parseInt(fg.TextMatrix(fg.Row, 14), 10);
    
    if ( callStateID != 26 )
        return true;        
    
    // Ringing
    if ( (callStateToID == 3) && (callCauseID == 22) )
        return false;
            
    // Talking
    if ( (callStateToID == 7) && (callCauseID == 28) )
        return true;
    
}

/********************************************************************
Opera ligacao em funcao do estado dos radios
********************************************************************/
function operationByRadioButtons()
{
    if ( window.top.overflyGen.DlgModalIsOpened == 0)
		lockControls(false);
    
    if ( glb_telefoniaTimer != null )
    {
        window.clearInterval(glb_telefoniaTimer);
        glb_telefoniaTimer = null;
    }
    
    // descheca o rdCallback.checked se hora e gao nao sao apropriados
    // para agendar callback
    cbInValidHour();
    
    var sRadioChecked = '';
    var i, nCurrState, nInternalCallID;

    if (rdTransferir.checked)
        sRadioChecked = 'T';
    else if (rdCallback.checked)
        sRadioChecked = 'C';
    //else if (rdCorreioVoz.checked)
    //    sRadioChecked = 'V';
    else if (rdURA.checked)
        sRadioChecked = 'U';

    if (sRadioChecked == '')
        return false;

    // Transferencia cega
    txtNumeroDiscarCC.value = getNumAlphaStriped(txtNumeroDiscarCC.value);

    if ( (sRadioChecked == 'T') && ((txtNumeroDiscarCC.value).length < 4) &&
         (txtNumeroDiscarCC.value != 9) )
        return false;

    if ( getInterfaceMode() == 'CC' )
    {
        fg.Redraw = 0;
        for (i=1; i<fg.Rows; i++)
        {
            nCurrState = parseInt(getCellValueByColKey(fg, 'CallStateID', i), 10);
            nInternalCallID = parseInt(getCellValueByColKey(fg, 'CallStateID', i), 10);
            
            if ( ((nCurrState == 1) && (ascan(glb_aChamadasAtendidas,nInternalCallID,false) == -1)) ||
                  (nCurrState == 3) )
            {
                fg.Row = i;

                fg.Redraw = 2;
                
                if ( window.top.overflyGen.DlgModalIsOpened == 0)
					lockControls(false);

                if (sRadioChecked == 'T')
                    btnTransferirOnClick(btnTransferir, true);
                else if (sRadioChecked == 'C')
                {
                    // btnCallbackOnClick(btnCallback);

					btnCallback.disabled = true;
					telefoniaCallback();
                }    
                else if (sRadioChecked == 'V')
                    btnCorreioVozOnClick(btnCorreioVoz);
                else if (sRadioChecked == 'U')
                    btnURAOnClick(btnURA);
                
                return false;
            }
        }
    
        fg.Redraw = 2;
    }
    else if ( getInterfaceMode() == 'CB' )
    {
        lockControls(true);
        
        for (i=0; i<glb_aObjCCData.length; i++)
        {
            if ( !glb_aObjCCData[i].dataValid )
                continue;
                
            nCurrState = parseInt(glb_aObjCCData[i].CallStateID, 10);
            nInternalCallID = parseInt(glb_aObjCCData[i].InternalCallID, 10);
            
            if ( ((nCurrState == 1) && (ascan(glb_aChamadasAtendidas,nInternalCallID,false) == -1)) ||
                  (nCurrState == 3))
            {
				if ( window.top.overflyGen.DlgModalIsOpened == 0)
					lockControls(false);

                if (sRadioChecked == 'T')
                    telefoniaTransferir(i);
                else if (sRadioChecked == 'C')
                    telefoniaCallback(i);
                else if (sRadioChecked == 'V')
                    telefoniaCorreioVoz(i);
                else if (sRadioChecked == 'U')
                    telefoniaURA(i);
                
                return false;
            }
        }
        
        if ( window.top.overflyGen.DlgModalIsOpened == 0)
			lockControls(false);
    }    
}

/********************************************************************
Interface de chamadas ativas tem radiobutton checado
********************************************************************/
function hasRdBtnChecked()
{
    // return rdTransferir.checked || rdCallback.checked || rdCorreioVoz.checked || rdURA.checked;
    return rdTransferir.checked || rdCallback.checked || rdURA.checked;
}

/********************************************************************
Usuario clicou botao de Pessoa
********************************************************************/
function btnPessoaClick(ctrl)
{
    if ( fg.Row <1 )
        return true;
    
    lockControls(true);
    
    var empresa = getCurrEmpresaData();
    var nPessoaID = 0;
    
    if (getInterfaceMode() == 'CC')
    {
        nPessoaID = fg.TextMatrix(fg.Row, 3);
    }
    else if (getInterfaceMode() == 'CB')
    {
        nPessoaID = fg.TextMatrix(fg.Row, 3);
    }

    // Manda o id da pessoa a detalhar 
    if ( !((nPessoaID == '') || (nPessoaID == '0')) )
    {
        sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], nPessoaID));
    }
    
    lockControls(false);
}

/********************************************************************
Usuario clicou botao de Lista de Precos
********************************************************************/
function btnListaPrecosClick(ctrl)
{
    if ( fg.Row <1 )
        return true;
        
    lockControls(true);
    
    var empresa = getCurrEmpresaData();
    var nPessoaID = 0;
    var strPars = new String();
    
    if (getInterfaceMode() == 'CC')
    {
        nPessoaID = fg.TextMatrix(fg.Row, 3);
    }
    else if (getInterfaceMode() == 'CB')
    {
        nPessoaID = fg.TextMatrix(fg.Row, 3);
    }

    if ( (nPessoaID == '') || (nPessoaID == '0') )
    {
        lockControls(false);
        return true;
    }

    strPars = '?';
    strPars += 'nClienteID=';
    strPars += escape(nPessoaID.toString());
    strPars += '&nEmpresaID=';
    strPars += escape(empresa[0].toString());
                
    dsoGen01.URL = SYS_ASPURLROOT + '/serversidegen/listaprecosdata.asp'+strPars;
    dsoGen01.ondatasetcomplete = listaPrecos_DSC;
    dsoGen01.refresh();
}

/********************************************************************
Retorno do servidor - Usuario clicou botao de Lista de Precos
********************************************************************/
function listaPrecos_DSC()
{
    var empresa = getCurrEmpresaData();
    var aListaPrecoData = new Array();
    
    if ( !(dsoGen01.recordset.BOF && dsoGen01.recordset.EOF) )
    {
        aListaPrecoData[0] = dsoGen01.recordset.Fields('Fantasia').Value;
        aListaPrecoData[1] = dsoGen01.recordset.Fields('PessoaID').Value;
        aListaPrecoData[2] = dsoGen01.recordset.Fields('UFID').Value;
        aListaPrecoData[3] = dsoGen01.recordset.Fields('EmpresaSistema').Value;
        aListaPrecoData[4] = dsoGen01.recordset.Fields('ListaPreco').Value;
        aListaPrecoData[5] = dsoGen01.recordset.Fields('PMP').Value;
        aListaPrecoData[6] = dsoGen01.recordset.Fields('NumeroParcelas').Value;
        aListaPrecoData[7] = dsoGen01.recordset.Fields('FinanciamentoPadrao').Value;
        aListaPrecoData[8] = dsoGen01.recordset.Fields('ClassificacaoInterna').Value;
        aListaPrecoData[9] = dsoGen01.recordset.Fields('ClassificacaoExterna').Value;
    
        // Manda o id da pessoa a detalhar 
        sendJSCarrier(getHtmlId(), 'SETPESSOAPRICELIST', new Array(empresa[0], aListaPrecoData));
    }
    
    lockControls(false);
}


/***
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , new Array(
                      fg.TextMatrix(fg.Row, 2), 
                      fg.TextMatrix(fg.Row, 1), 
                      fg.TextMatrix(fg.Row, 7),
                      fg.TextMatrix(fg.Row, 12),
                      fg.TextMatrix(fg.Row, 8),
                      fg.TextMatrix(fg.Row, 9),
                      fg.TextMatrix(fg.Row, 10),
                      fg.TextMatrix(fg.Row, 11)) );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    

}
    fillGridMask(fg,dsoPesq,['fldName',
                             'fldID',
                             'Fantasia',
                             'Documento',
                             'Cidade',
                             'UF',
                             'Pais',
                             'UFID',
                             'ListaPreco',
							 'PMP',
							 'NumeroParcelas',
							 'FinanciamentoPadrao',
							 'EmpresaSistema'],['','','','','','','','','','','','','']);



***/

/********************************************************************
Usuario apertou uma tecla no radio do padrao para duplo clique
no grid.
rdCallback - rdCorreioVoz - rdURA
********************************************************************/
function rdsPadraoOnKeyDown(ctrl)
{
    if ( (window.event.keyCode == 32) || (window.event.keyCode == 13) )
    {
        glb_lastStateRd = ctrl.checked;
    }    
}

/********************************************************************
Usuario abaixou o botao esquerdo do mouse radio do padrao para duplo clique
no grid.
rdCallback - rdCorreioVoz - rdURA
********************************************************************/
function rdsPadraoOnMouseDown(ctrl)
{
    if (window.event.button == 1)
    {
        glb_lastStateRd = ctrl.checked;
    }    
}

/********************************************************************
Usuario clicou radio do padrao para duplo clique no grid.
rdCallback - rdCorreioVoz - rdURA
********************************************************************/
function rdsPadraoOnClick(ctrl)
{
	rdCallback.setAttribute('byAuto', 0, 1);
	
    if (ctrl == rdCallback)
    {
        if ( !cbInValidHour() )
            return true;
    }    

    ctrl.checked = !glb_lastStateRd;
    
    if ( ctrl.checked )
    {
        if ( ctrl.id == rdCallback.id )
        {
            //rdCorreioVoz.checked = false;
            rdURA.checked = false;
            rdTransferir.checked = false;
        }
        /*else if ( ctrl.id == rdCorreioVoz.id )
        {
            rdCallback.checked = false;
            rdURA.checked = false;
            rdTransferir.checked = false;
        }*/
        else if ( ctrl.id == rdURA.id )
        {
            rdCallback.checked = false;
            //rdCorreioVoz.checked = false;
            rdTransferir.checked = false;
        }
        else if ( ctrl.id == rdTransferir.id )
        {
            // aceita radio para telefonista
            if ( getNumAlphaStriped(txtNumeroDiscarCC.value) == 9 )
            {
                txtNumeroDiscarCC.value = getNumAlphaStriped(txtNumeroDiscarCC.value);
            }
            else if ( getNumAlphaStriped(txtNumeroDiscarCC.value).length != 4 )
            {
                rdTransferir.checked = false;
                
				window.top.overflyGen.AlertModless(_MSG_RAMALVALIDO_ID, 'Digite um ramal v�lido.');
				setControlsStateInBar();
            }    

            rdCallback.checked = false;
            //rdCorreioVoz.checked = false;
            rdURA.checked = false;
        }
    }
}

/********************************************************************
Configura o header do grid, apenas para estetica da tela ao ser mostrada
pela primeira vez
********************************************************************/
function putHeaderLineInGrid(grid)
{
    startGridInterface(grid, 1, 1);
    
    grid.Cols = 1;
    grid.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
 
    // zera a listagem
    grid.Rows = 1;
    
    grid.Redraw = 0;
    
    // preenche o cabecalho do grid
    headerGrid(grid,['DDD', 
                   'Telefone', 
                   'ID', 
                   'Fantasia', 
                   'Classifica��o', 
                   'Cidade', 
                   'UF',
                   'Call Status', 
                   'Hora', 
                   'Contato'],[]);
    
    grid.FontSize = '8';

    alignColsInGrid(fg,[0, 1, 2]);
        
    grid.ExplorerBar = 5;
    grid.AutoSizeMode = 0;
    grid.AutoSize(0,fg.Cols-1);
    grid.Redraw = 2;    
}

/********************************************************************
Seta o bit value de cada botao

    state == 1  -> callStatus = 'Espera'
    state == 2  -> callStatus = 'Discando'
    state == 3  -> callStatus = 'Ringing'
    state == 4  -> callStatus = 'Ocupado'
    state == 5  -> callStatus = 'Nao atendido'
    state == 7  -> callStatus = 'Talking'
    state == 9  -> callStatus = 'Interrompido'
    state == 10 -> callStatus = 'Hold';
    state == 11 -> callStatus = 'Transferido'
    state == 12 -> callStatus = 'Confer�ncia'
    state == 14 -> callStatus = 'Ativando'
    state == 15 -> callStatus = 'Transferindo'
    state == 16 -> callStatus = 'Talking'
    state == 25 -> callStatus = 'Interrompido'
    state == 26 -> callStatus = 'Originado'
    state == 27 -> callStatus = 'Livre'
    
    btnAlwaysEnabled = 1
    btnEnabledWhenHasLineInGrid = 10;

    btnChamadasCB = 100 | 1 = 101   1 = mascara, 01 = estado
    btnPessoa = 1000 | 10 = 1010   10 = mascara, 10 = estado
    ... ... ...
    
    se for fazer operacoes logicas com as mascara dos botoes, 2 shifts
    a direita para desprezar o estado
    
********************************************************************/
function putBitValuesInControls()
{
    var btnAlwaysEnabled = Math.pow(2, 0);
    var btnEnabledWhenHasLineInGrid = Math.pow(2, 1);
    var nExponent = 2;
    
    // Botoes de nivel 1, disponiveis em telefonia e callback
    btnChamadasCB.setAttribute('bitValue', Math.pow(2, nExponent) | btnAlwaysEnabled, 1);
    btnPessoa.setAttribute('bitValue', Math.pow(2, ++nExponent) | btnEnabledWhenHasLineInGrid, 1);
    btnPessoa.setAttribute('evalToEnable', 'hasPessoaIDInGrid()', 1);
    btnListaPrecos.setAttribute('bitValue', Math.pow(2, ++nExponent) | btnEnabledWhenHasLineInGrid, 1);
    btnListaPrecos.setAttribute('evalToEnable', 'hasPessoaIDInGrid()', 1);
    
    // Botoes de nivel 2, disponiveis em telefonia
    btnDiscarCC.setAttribute('bitValue', btnAlwaysEnabled, 1);
    btnAtender.setAttribute('bitValue', Math.pow(2, ++nExponent), 1);
    btnAtender.setAttribute('evalToEnable', 'callPassedTroughtURA() && hasCallInTalkingAndWait()', 1);
    
    btnEspera.setAttribute('bitValue', Math.pow(2, ++nExponent), 1);
    btnEspera.setAttribute('evalToEnable', 'callHasState26()', 1);
    
    btnTransferir.setAttribute('bitValue', Math.pow(2, ++nExponent), 1);
    btnTransferir.setAttribute('evalToEnable', 'callInLineGridCanTransfer()', 1);
    
    btnConferencia.setAttribute('bitValue', Math.pow(2, ++nExponent), 1);
    btnConferencia.setAttribute('evalToEnable', 'callInLineGridCanConfer()', 1);
        
    btnDesligar.setAttribute('bitValue', Math.pow(2, ++nExponent), 1);
    
    rdCallback.setAttribute('bitValue', 1, 1);
    btnCallback.setAttribute('bitValue', Math.pow(2, ++nExponent), 1);
    btnCallback.setAttribute('evalToEnable', 'callHasState26()', 1);
    
    //rdCorreioVoz.setAttribute('bitValue', 1, 1);
    btnCorreioVoz.setAttribute('bitValue', Math.pow(2, ++nExponent), 1);
    
    rdURA.setAttribute('bitValue', 1, 1);
    btnURA.setAttribute('bitValue', Math.pow(2, ++nExponent), 1);
    btnURA.setAttribute('evalToEnable', 'callHasState26()', 1);
    
    rdTransferir.setAttribute('bitValue', 1, 1);

    // Botoes de nivel 2, disponiveis em callback
    btnAgendarCB.setAttribute('bitValue', btnAlwaysEnabled, 1);
    btnLigar.setAttribute('bitValue', btnEnabledWhenHasLineInGrid, 1);
    btnReprogramar.setAttribute('bitValue', btnEnabledWhenHasLineInGrid, 1);
    btnCancelar.setAttribute('bitValue', btnEnabledWhenHasLineInGrid, 1);
    btnRefreshCB.setAttribute('bitValue', btnAlwaysEnabled, 1);
    
    btnLigar.setAttribute('callExec_CB', 1, 1);
    btnReprogramar.setAttribute('callExec_CB', 1, 1);
    btnCancelar.setAttribute('callExec_CB', 1, 1);

    // 1 - Espera
    var bitMaskWaitMode =
        btnDiscarCC.getAttribute('bitValue', 1) |
        btnAtender.getAttribute('bitValue', 1) |
        btnCallback.getAttribute('bitValue', 1) |
        btnCorreioVoz.getAttribute('bitValue', 1) |
        btnURA.getAttribute('bitValue', 1);
    
    // 2 - Dialing    
    var bitMaskDialingMode =
        btnDesligar.getAttribute('bitValue', 1);

    // 3 - Ring
    var bitMaskRingMode = 
        btnDiscarCC.getAttribute('bitValue', 1) |
        btnCallback.getAttribute('bitValue', 1) |
        btnCorreioVoz.getAttribute('bitValue', 1) |
        btnURA.getAttribute('bitValue', 1);

    // 7 / 16 - Talking
    var bitMaskTalkingMode =
        btnDiscarCC.getAttribute('bitValue', 1) |
        btnEspera.getAttribute('bitValue', 1) |
        btnTransferir.getAttribute('bitValue', 1) |
        btnConferencia.getAttribute('bitValue', 1) |
        btnDesligar.getAttribute('bitValue', 1) |
        btnCallback.getAttribute('bitValue', 1) |
        btnURA.getAttribute('bitValue', 1);

    // 10 - Hold
    var bitMaskHoldMode =
        btnAtender.getAttribute('bitValue', 1) |
        btnTransferir.getAttribute('bitValue', 1) |
        btnConferencia.getAttribute('bitValue', 1);

    // 12 - Conference            
    var bitMaskConferenceMode =
        btnDesligar.getAttribute('bitValue', 1);
    
    // 26 - CallMake
    var bitMaskCallMake =
        btnEspera.getAttribute('bitValue', 1) |
        btnTransferir.getAttribute('bitValue', 1) |
        btnConferencia.getAttribute('bitValue', 1) |
        btnDesligar.getAttribute('bitValue', 1) |
        btnCallback.getAttribute('bitValue', 1) |
        btnURA.getAttribute('bitValue', 1);
    
    // 27 - FreeMode
    var bitMaskFreeMode =
        btnAgendarCB.getAttribute('bitValue', 1) |
        btnAtender.getAttribute('bitValue', 1);
    
    glb_aStatesBitMask = [ [1 , bitMaskWaitMode],
                           [2 , bitMaskDialingMode],
                           [3 , bitMaskRingMode],
                           [7 , bitMaskTalkingMode],
                           [10, bitMaskHoldMode],
                           [12, bitMaskConferenceMode],
                           [16, bitMaskTalkingMode],
                           [26, bitMaskCallMake],
                           [27, bitMaskFreeMode] ];

}

/********************************************************************
Seta o bit value do botao atender para telefonia digital
********************************************************************/
function especificToAttendDigital()
{
	// especifico para atender de telefone digital    
    if ( glb_RAMALDIGITAL )
	{
		glb_aStatesBitMask[2][1] = glb_aStatesBitMask[2, 1] |
								   btnAtender.getAttribute('bitValue', 1);
	}							   
}

/********************************************************************
Verifica se a ligacao selecionada passou pela ura.
********************************************************************/
function callPassedTroughtURA()
{
    if (fg.Rows < 2)
        return false;
        
    if (fg.Row < 1)
        return false;
        
    var i, sInternalCallID, nCallState;
       
    sInternalCallID = getCellValueByColKey(fg, 'InternalCallID', fg.Row);
    nCallState = getCellValueByColKey(fg, 'CallStateID', fg.Row);
    
    if (nCallState != 3)
        return true;
    
    for (i=1; i<=API.Calls.Count; i++)
    {
        if (API.Calls(i).InternalCallID == sInternalCallID)
        {
            // A chamada passou pela URA
            if (trimStr(API.Calls(i).Informations.InformationValue('CID')) != '')
            {
                return true;
            }    
        }
    }
    
    return false;
}

/********************************************************************
Verifica se a ligacao a ser atendida esta em espera e existe uma outra
ativa, neste caso o botao atender nao deve ser habilitado.
********************************************************************/
function hasCallInTalkingAndWait()
{
    if (fg.Rows < 2)
        return false;
        
    if (fg.Row < 1)
        return false;
        
    var i, nCallState;
    var callStateToID = getCellValueByColKey(fg, 'CallStateToID', fg.Row);

    nCallState = getCellValueByColKey(fg, 'CallStateID', fg.Row);

    if (nCallState != 1)
        return true;
    
    try
    {
        for (i=1; i<=API.Calls.Count; i++)
        {
            if ((API.Calls(i).CallState == 7) || 
                (API.Calls(i).CallState == 26 && API.Calls(i).CallStateTo == 7))
            {
                return false;
            }
        }
    }
    catch (e)
    {
        return false;
    }
    
    return true;
}


/********************************************************************
Verifica se a linha atual do grid tem pessoaID
********************************************************************/
function hasPessoaIDInGrid()
{
    var retVal = false;

    if (fg.Rows < 2)
        return retVal;

    var sPessoaID;

    if ( getInterfaceMode() == 'CC' )
        sPessoaID = getCellValueByColKey(fg, 'PessoaID', fg.Row);
    else
        sPessoaID = getCellValueByColKey(fg, 'PessoaID*', fg.Row);
        
    if ( ! ((sPessoaID == null) || (trimStr(sPessoaID) == '')) )
        retVal = true;

    return retVal;
}

/********************************************************************
Inicializa interfaces e objetos de chamadas e callback
********************************************************************/
function initializeCCAndCB()
{
    lockControls(true);

    glb_telefoniaTimer = window.setInterval('getCallbackData()', 550, 'JavaScript');
}

/********************************************************************
Requisita dados de callback ao servidor
********************************************************************/
function getCallbackData()
{
    if ( glb_telefoniaTimer != null )
    {
        window.clearInterval(glb_telefoniaTimer);
        glb_telefoniaTimer = null;
    }
    
    lockControls(true);
    
    var strPars = new String();

   	var sDevice = glb_device;
	
    strPars = '?';
    strPars += 'sDevice=';
    strPars += escape(sDevice);
                
    dsoCallbacks.URL = SYS_ASPURLROOT + '/telefonia/serverside/callbackdata.asp' + strPars;
    dsoCallbacks.ondatasetcomplete = getCallbackData_DSC;
    dsoCallbacks.refresh();
}

/********************************************************************
Requisita dados de callback ao servidor para refrescar o grid
ou
insere na tabela de callback, conforme o tipo de operacao
********************************************************************/
function refreshCallbackData(sNumberToCall, sPipe, nCBCount)
{
    if (glb_RefreshCallbackDataExecuting)
        return null;
    
    glb_RefreshCallbackDataExecuting = true;
    
    if ( glb_telefoniaTimer != null )
    {
        window.clearInterval(glb_telefoniaTimer);
        glb_telefoniaTimer = null;
    }
    
    lockControls(true);
    
    var strPars = new String();
    
    if ( nCBCount == null )
		nCBCount = 0;
	
	if ( sNumberToCall == null )
	{
        strPars = '?';
        strPars += 'sDevice=';
        strPars += escape(glb_device);
    }
    else
    {
        strPars = '?nTipoOperacao=1';
   	    strPars += '&sDevice=' + escape(glb_device);
   	    strPars += '&sNumeroTelefone=' + escape(sNumberToCall);
   	    strPars += '&sPipe=' + escape(sPipe);
   	    strPars += '&nCallTimeInterval=' + escape(Math.abs(parseInt(selTimeInterval.value, 10)));
    }
    
    strPars += '&nCBCount=' + escape(nCBCount);
                
    dsoCallbacks.URL = SYS_ASPURLROOT + '/telefonia/serverside/callbackdata.asp' + strPars;
    dsoCallbacks.ondatasetcomplete = refreshCallbackData_DSC;
    dsoCallbacks.refresh();
}

/********************************************************************
Retorno do servidor da requisicao de dados de callback
********************************************************************/
function getCallbackData_DSC()
{
    // Verifica ocorrencia de erro no servidor
    // neste caso o banco tem o campo fldError
    
    for ( i=0; i<dsoCallbacks.recordset.Fields.Count; i++ )
    {
        if ( (dsoCallbacks.recordset.Fields(i).Name).toUpperCase() == 'FLDERROR' )
        {
            glb_telefoniaTimer = window.setInterval('go_getCallbackDataError_DSC()', 10, 'JavaScript');
            return null;
        }
    }
    
    // Salva variavel controladora de fechamento do Overfly
    var numCBs = dsoCallbacks.recordset.RecordCount;
    sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CBPENDENTES', numCBs);

    glb_telefoniaTimer = window.setInterval('setGridAndLogUser()', 10, 'JavaScript');    
}
    
/********************************************************************
Retorno do servidor da requisicao de dados de callback para refrescar o grid
********************************************************************/
function refreshCallbackData_DSC()
{    
    if ( glb_telefoniaTimer != null )
    {
        window.clearInterval(glb_telefoniaTimer);
        glb_telefoniaTimer = null;
    }
    
    if ( getInterfaceMode() != 'CB' )
    {
        // destrava interface 
        lockControls(false);
    
        glb_RefreshCallbackDataTimer = window.setInterval('setVarRefreshCallbackDataExecuting()', 10, 'JavaScript');
    
        return null;
    }    

    // overflyGen.StopHookKBAndMouse();
    
    var dso = dsoCallbacks;
    var nCurrCBSelInGrid = 0;
    var i;
    
    if ( dso.recordset.Fields.Count == 0 )
    {
        lockControls(false);

        glb_RefreshCallbackDataTimer = window.setInterval('setVarRefreshCallbackDataExecuting()', 10, 'JavaScript');

        return true;
    }    

    // Salva variavel controladora de fechamento do Overfly
    var numCBs = dsoCallbacks.recordset.RecordCount;
    sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CBPENDENTES', numCBs);

    // sorteia o dso se tiver dados
    if ( !(dso.recordset.BOF && dso.recordset.EOF) )
    {
        dso.SortColumn ='Data';
        dso.SortDirection = true;
        dso.recordset.MoveFirst();
    }

    if ((fg.Rows > 1) && (fg.Row > 0))
        nCurrCBSelInGrid = fg.TextMatrix(fg.Row, 10);

    // zera a listagem
    fg.Rows = 1;
    
    fg.Editable = false;
    fg.Redraw = 0;
    
    headerGrid(fg,['DDI', 
                   'DDD', 
                   'Telefone', 
                   'ID', 
                   'Fantasia', 
                   'Classifica��o', 
                   'Cidade', 
                   'UF',
                   'Status',
                   'Hora', 
                   'Contato',
                   'CallBackID',
                   'CBStatusID'],[0, 11, 12]);
    
    fg.FontSize = '8';

    fillGridMask(fg, dso, ['DDI', 
                           'DDD', 
                           'Numero', 
                           'PessoaID*', 
                           'Fantasia*', 
                           'Classificacao*', 
                           'Cidade*', 
                           'UF*',
                           'CBStatus*',
                           'Data*', 
                           'Contato*',
                           'CallBackID',
                           'CBStatusID'],
                          ['99', '99', '99999999', '','', '', '', '','', '', '', '', ''],
                          ['', '', '', '','', '', '', '', '', 'hh:mm:ss', '', '', '']);

    alignColsInGrid(fg,[0, 1, 2, 3]);

    var currDateTime = new Date();
    var i,j;

	if ( !(dsoCallbacks.recordset.BOF && dsoCallbacks.recordset.EOF) )
		dsoCallbacks.recordset.MoveFirst();

    while (!dsoCallbacks.recordset.EOF)
    {
        if (dsoCallbacks.recordset('Data').value < currDateTime)
		{
			for (i=1; i<fg.Rows; i++)
			{
				if (dsoCallbacks.recordset('CallBackID').value == parseInt(getCellValueByColKey(fg, 'CallBackID', i), 10))
				{
					fg.Select(i, j, i, fg.Cols-1);
					fg.FillStyle = 1;
					fg.CellForeColor = 0X0000FF;
				}
			}
		}            
        dsoCallbacks.recordset.moveNext();
    }

	window.focus();

    fg.Editable = false;
    fg.ExplorerBar = 5;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    fg.Redraw = 2;

    glb_RefreshCallbackDataTimer = window.setInterval('setVarRefreshCallbackDataExecuting()', 10, 'JavaScript');

    // destrava interface 
    lockControls(false);

    if (nCurrCBSelInGrid != 0)
        glb_currCBSelInGrid = nCurrCBSelInGrid;

    selGridLineByCall();

    setControlsStateInBar();
    
    // overflyGen.StartHookKBAndMouse(glb_UserTimerInterval, 1);
}

/********************************************************************
Seta a variavel glb_RefreshCallbackDataExecuting
********************************************************************/
function setVarRefreshCallbackDataExecuting()
{
    if ( glb_RefreshCallbackDataTimer != null )
    {
        window.clearInterval(glb_RefreshCallbackDataTimer);
        glb_RefreshCallbackDataTimer = null;
    }
    glb_RefreshCallbackDataExecuting = false;
}

/********************************************************************
Mostra o grid pela primeira vez e loga o usuario na telefonia
********************************************************************/
function setGridAndLogUser()
{
    if ( glb_telefoniaTimer != null )
    {
        window.clearInterval(glb_telefoniaTimer);
        glb_telefoniaTimer = null;
    }
    
    if ( glb_bTelephonyIsLoading == false )
        return null;
        
    glb_bTelephonyIsLoading = false;
    
    // coloca o grid visivel (pela primeira vez)
    if ( fg.style.visibility != 'inherit' )
    {
        fg.Rows = 1;
        fg.style.visibility = 'inherit';    
        
        // Interface em modo de chamadas
        putInModeChamadas();
        
        // loga o usuario na telefonia
        logInOnCentral();
    }
}

/********************************************************************
Execucao por timer
Retorno do servidor da requisicao de dados de callback com error
********************************************************************/
function go_getCallbackDataError_DSC()
{    
    if ( glb_telefoniaTimer != null )
    {
        window.clearInterval(glb_telefoniaTimer);
        glb_telefoniaTimer = null;
    }
    
    var dso = dsoCallbacks;

    // zera a listagem
    fg.Rows = 1;
    
    fg.Editable = false;
    fg.Redraw = 0;

    headerGrid(fg,['DDI', 
                   'DDD', 
                   'Telefone', 
                   'ID', 
                   'Fantasia', 
                   'Classifica��o', 
                   'Cidade', 
                   'UF',
                   'Status',
                   'Hora', 
                   'Contato',
                   'CallBackID',
                   'CBStatusID'],[0, 11, 12]);

    fg.FontSize = '8';
    
    fg.Editable = true;
    fg.ExplorerBar = 5;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;    
       
    // coloca o grid visivel (pela primeira vez)
    if ( fg.style.visibility != 'inherit' )
        fg.style.visibility = 'inherit';    
    
    window.focus();
    
    setControlsStateInBar();
        
    if ( window.top.overflyGen.Alert('Telefonia n�o est� dispon�vel') == 0 )
        return null;
        
	// trava a interface
    lockControls(true);        
}

/********************************************************************
Execucao por timer
Preenche o grid com dados das chamadas.
********************************************************************/
function go_fillgridChamadas()
{    
    if ( glb_telefoniaTimer != null )
    {
        window.clearInterval(glb_telefoniaTimer);
        glb_telefoniaTimer = null;
    }
        
    fg.Editable = false;
    fg.Redraw = 0;

    fg.Rows = 1;
    
    var i;
    var elemInArray;
    
    for ( i=0; i<numElemsInCCArray('F'); i++ )
    {
        elemInArray = glb_aObjCCData[i];
        
        if ( !elemInArray.dataValid )
            continue;
        
        fg.Rows = fg.Rows + 1;
    
        if ( fg.Rows > 1 )
            fg.Row = fg.Rows - 1;
            
        fg.TextMatrix(fg.Rows - 1, 0) = elemInArray.DDI;
        fg.TextMatrix(fg.Rows - 1, 1) = elemInArray.DDD;
        
        if ( elemInArray.Numero.length == 5 )
            fg.TextMatrix(fg.Rows - 1, 2) = elemInArray.Numero.substr(1);
        else
            fg.TextMatrix(fg.Rows - 1, 2) = elemInArray.Numero;
        
        if ( elemInArray.ClienteID != 0 )
            fg.TextMatrix(fg.Rows - 1, 3) = elemInArray.ClienteID;
        else
            fg.TextMatrix(fg.Rows - 1, 3) = '';
            
        fg.TextMatrix(fg.Rows - 1, 4) = elemInArray.Fantasia;
        fg.TextMatrix(fg.Rows - 1, 5) = elemInArray.Classificacao;
        fg.TextMatrix(fg.Rows - 1, 6) = elemInArray.Cidade;
        fg.TextMatrix(fg.Rows - 1, 7) = elemInArray.UF;
        fg.TextMatrix(fg.Rows - 1, 8) = elemInArray.CallState;
        fg.TextMatrix(fg.Rows - 1, 9) = elemInArray.DateTime;
        fg.TextMatrix(fg.Rows - 1, 10) = elemInArray.Contato1 + 
                                        (elemInArray.Contato2 != '' ? '/' + elemInArray.Contato2 : '') + 
                                        (elemInArray.Contato3 != '' ? '/' + elemInArray.Contato3 : '');
        fg.TextMatrix(fg.Rows - 1, 11) = elemInArray.InternalCallID;
        fg.TextMatrix(fg.Rows - 1, 12) = elemInArray.CallStateID;
        fg.TextMatrix(fg.Rows - 1, 13) = elemInArray.CallStateToID;
        fg.TextMatrix(fg.Rows - 1, 14) = elemInArray.CallCauseID;
        fg.TextMatrix(fg.Rows - 1, 15) = elemInArray.Key;
        fg.TextMatrix(fg.Rows - 1, 16) = elemInArray.CBCount;
    }

    paintReadOnlyCols(fg);
    putMasksInGrid(fg, ['99', '99', '99999999', '','', '', '', '','', '', '', '', '', '', '', ''],
                       ['', '', '', '','', '', '', '', '', 'hh:mm:ss', '', '', '', '', '', '']);
        
    alignColsInGrid(fg,[0, 1, 2, 3]);

    fg.Editable = false;
    fg.ExplorerBar = 5;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;    
        
    // destrava interface
    lockControls(false);
    
    setControlsStateInBar();
    
    blinkTelefonia();
        
    //var dObj = new Date();
    //trace('***Fim api_js_CallStateChanged()' + dObj.getSeconds() + ':' + dObj.getMilliseconds());   
}

/********************************************************************
Coloca foco ou pisca a janela de telefonia
********************************************************************/
function blinkTelefonia()
{
    var i;
    
    overflyGen.StopSpeakerBeep();
    
    for (i=1; i<fg.Rows; i++)
    {
        if ( ((fg.TextMatrix(i, 8)).toUpperCase()).substr(0,7)  == 'RINGING' )        
        {    
            overflyGen.StartSpeakerBeep();
            
            window.focus();
            //fg.focus();
            break;
        }
    
    }
}

/********************************************************************
Preenche campo de mensagens historiando ligacoes ativas, 
quanto a interface esta no modo callback
********************************************************************/
function fillMessageCB()
{
    var chamadasTodas = 0;
    var chamadasRingando = 0;
    var i;
    var theMsg = '';    
    var elemInArray;
    
    for ( i=0; i<numElemsInCCArray('F'); i++ )
    {
        elemInArray = glb_aObjCCData[i];
        
        if ( !elemInArray.dataValid )
            continue;
        
        chamadasTodas++;
            
        if ( ((elemInArray.CallStateID == 1) && (ascan(glb_aChamadasAtendidas,elemInArray.InternalCallID,false) == -1)) ||
              (elemInArray.CallStateID == 3))
            chamadasRingando++;

        //if ( elemInArray.CallStateID == 3 )
        //    chamadasRingando++;
    }
    
    if ( chamadasTodas == 1 )
        theMsg = chamadasTodas.toString() + ' chamada';
    else if ( chamadasTodas > 1 )
        theMsg = chamadasTodas.toString() + ' chamadas';
        
    if ( chamadasRingando > 0 )
    {
        if ( theMsg != '' )    
            theMsg += ', ';    
    
        theMsg += '<B><FONT color = red>' + chamadasRingando.toString() + ' ringing</FONT></B>';
    }    
        
    lblMessageCB.innerHTML = theMsg;
}

/********************************************************************
Usuario clicou o botao discar da interface em modo chamadas
********************************************************************/
function btnDiscarCCOnClick(ctrl)
{
    if ( ctrl.disabled )
        return true;
            
    ctrl.disabled = true;
    
    // ctrl.disabled = telefoniaDiscarCC(ctrl);
    telefoniaDiscarCC(ctrl);
}

/********************************************************************
Usuario clicou o botao atender
********************************************************************/
function btnAtenderOnClick(ctrl)
{
    if ( ctrl.disabled )
        return true;
            
    ctrl.disabled = true;
    
    //ctrl.disabled = telefoniaAtender();
    telefoniaAtender();
}

/********************************************************************
Usuario clicou o botao hold
********************************************************************/
function btnEsperaOnClick(ctrl)
{
    if ( ctrl.disabled )
        return true;
    
    ctrl.disabled = true;
    
    // ctrl.disabled = telefoniaEspera();
    telefoniaEspera();
}

/********************************************************************
Usuario clicou o botao transferir
********************************************************************/
function btnTransferirOnClick(ctrl, bForce)
{
    if ( bForce == null )
    {
        if ( (ctrl.disabled) && (!hasRdBtnChecked()) )
            return true;
    }    
    
    ctrl.disabled = true;
    
    // ctrl.disabled = telefoniaTransferir();
    telefoniaTransferir();
}

/********************************************************************
Usuario clicou o botao conferencia
********************************************************************/
function btnConferenciaOnClick(ctrl)
{
    if ( ctrl.disabled )
        return true;
    
    ctrl.disabled = true;
    
    // ctrl.disabled = telefoniaConferencia();
    telefoniaConferencia();
}

/********************************************************************
Usuario clicou o botao desligar
********************************************************************/
function btnDesligarOnClick(ctrl)
{
    if ( ctrl.disabled )
        return true;
    
    ctrl.disabled = true;
    
    // ctrl.disabled = telefoniaDesligar();
    telefoniaDesligar();
}

/********************************************************************
Usuario clicou o botao callback
********************************************************************/
function btnCallbackOnClick(ctrl)
{
    if ( (ctrl.disabled) && (!hasRdBtnChecked()) )
        return true;
    
    if ( !cbInValidHour() )
    {
		window.top.overflyGen.AlertModless(_MSG_CALLBACKFORAHORARIO_ID, 'Callback s� program�vel entre:\n' + glb_MIN_HOURCB + ' e ' + glb_MAX_HOURCB + ' hrs.');
        setControlsStateInBar();	

        return true;
    }
    
    ctrl.disabled = true;
    
    // ctrl.disabled = telefoniaCallback();
    telefoniaCallback();
}

/********************************************************************
Usuario clicou o botao correio de voz
********************************************************************/
function btnCorreioVozOnClick(ctrl)
{
    if ( (ctrl.disabled) && (!hasRdBtnChecked()) )
        return true;
    
    ctrl.disabled = true;
    
    // ctrl.disabled = telefoniaCorreioVoz();
    telefoniaCorreioVoz();
}

/********************************************************************
Usuario clicou o botao URA
********************************************************************/
function btnURAOnClick(ctrl)
{
    if ( (ctrl.disabled) && (!hasRdBtnChecked()) )
        return true;
    
    ctrl.disabled = true;
    
    // ctrl.disabled = telefoniaURA();
    telefoniaURA();
}

/********************************************************************
Usuario clicou o botao agendar da interface em modo callback
********************************************************************/
function btnAgendarCBOnClick(ctrl)
{
    if ( ctrl.disabled )
        return true;
    
    if ( !cbInValidHour() )
    {
		window.top.overflyGen.AlertModless(_MSG_CALLBACKFORAHORARIO_ID, 'Callback s� program�vel entre:\n' + glb_MIN_HOURCB + ' e ' + glb_MAX_HOURCB + ' hrs.');
        setControlsStateInBar();	
                    
        return true;
    }
        
    ctrl.disabled = true;
        
    var retVal;
    
    txtNumeroAgendarCB.value = getNumAlphaStriped(txtNumeroAgendarCB.value);
    
    retVal = txtNumeroAgendarCB.value;
    
    if ( (retVal != null) &&  (retVal != '') )
    {
        // coloca zero se nao for ligacao de ramal para ramal
        if (retVal.length != 4)
            retVal = '0' + retVal;
        
        telefoniaAgendarCB(retVal);
    }
    else
    {
		window.top.overflyGen.AlertModless(_MSG_NUMEROVALIDO_ID, 'Digite um n�mero v�lido.');
		setControlsStateInBar();
    }    
}

/********************************************************************
Usuario clicou o botao ligar
********************************************************************/
function btnLigarOnClick(ctrl)
{
    if ( ctrl.disabled )
        return true;
    
    ctrl.disabled = true;
    
    // ctrl.disabled = telefoniaLigar();
    telefoniaLigar();
}

/********************************************************************
Usuario clicou o botao reprogramar
********************************************************************/
function btnReprogramarClick(ctrl)
{
    if ( ctrl.disabled )
        return true;
    
    if ( fg.Row > 0 )
    {
        if ( !cbInValidHour(trimStr(getCellValueByColKey(fg, 'Data*', fg.Row))) )
        {
			window.top.overflyGen.AlertModless(_MSG_CALLBACKFORAHORARIO_ID, 'Callback s� program�vel entre:\n' + glb_MIN_HOURCB + ' e ' + glb_MAX_HOURCB + ' hrs.');
			setControlsStateInBar();	

            return true;
        }
    }
    
    ctrl.disabled = true;
    
    // ctrl.disabled = telefoniaReprogramar();
    telefoniaReprogramar();
}

/********************************************************************
Usuario clicou o botao cancelar
********************************************************************/
function btnCancelarClick(ctrl)
{
    if ( ctrl.disabled )
        return true;
    
    ctrl.disabled = true;
    
    // ctrl.disabled = telefoniaCancelar();
    telefoniaCancelar();
}

/********************************************************************
Usuario clicou o botao para refrescar o grid de callback
********************************************************************/
function btnRefreshCBClick(ctrl)
{
    refreshCallbackData();
}

/********************************************************************
Usuario deu um duplo clique no label de qtd de ligacoes no voice mail
********************************************************************/
function lblNumMsgCorreio_ondblclick()
{
	if ( btnDiscarCC. disabled )
		return true;
		
	this.disabled = true;
		
	telefoniaDiscarCC(btnDiscarCC, glb_VOICEMAILNUM);
	
	this.disabled = false;
}

/********************************************************************
Usuario trocou o option do selTimeInterval
********************************************************************/
function selTimeIntervalOnChange(ctrl)
{
    cbInValidHour();
}

/********************************************************************
Valida se a hora corrente e admissivel para max/min hora de callback
Se nao for valida tambem descheca o radio rdCallback

Parametros  : hora basica
Retorno     : true/false 
********************************************************************/
function cbInValidHour(hourBase)
{
    var retVal = false;
    
    var nMaxHour = (parseInt(glb_MAX_HOURCB.substr(0,2), 10) * 60) +
                   parseInt(glb_MAX_HOURCB.substr(3,2), 10);
   
    var nMinHour = (parseInt(glb_MIN_HOURCB.substr(0,2), 10) * 60) +
                   parseInt(glb_MIN_HOURCB.substr(3,2), 10);                
    
    var hourCB = hourInMinutesToPrgCallBack(hourBase, parseInt(selTimeInterval.value, 10));
    
    retVal = (hourCB >= nMinHour) && (hourCB <= nMaxHour);
    
    if ( !retVal )
        rdCallback.checked = false;
    
    return retVal;
}

/********************************************************************
Usuario esta operando o Overfly. Reestarta o timer de controle de
operacao

Parametros  : nenhum
Retorno     : nenhum
********************************************************************/
function userIsOperating()
{
    // Desmarca o radio de transferir e tira 9 do campo de discar
    // se a telefonia estiver assim
    if ( (rdCallback.checked == true) && (parseInt(rdCallback.getAttribute('byAuto', 1)) == 1) )
    {
        rdCallback.checked = false;
        rdCallback.setAttribute('byAuto', 0, 1);
    }    
}

/********************************************************************
Usuario nao esta operando o Overfly.

Parametros  : nenhum
Retorno     : nenhum
********************************************************************/
function userIsNotOperating()
{
    // Desvia as ligacoes para a telefonista
    // se nao tiver outro desvio acionado
    // (rdCorreioVoz.checked == false) &&
    if ( (rdTransferir.checked == false) &&
         (rdCallback.checked == false) &&
         (rdURA.checked == false) )
    {
        rdCallback.checked = true;
        rdCallback.setAttribute('byAuto', 1, 1);
    }
}

// EVENTOS DE GRID PARTICULARES DESTA PAGINA ========================

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_telefonia_AfterRowColChange (grid, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 0;
    
    if ( glb_totalCols__ != false )
        firstLine = 1;
        
    if (NewRow > firstLine)
    {
        grid.Row = NewRow;
    }
    
    // salva ligacao correspondente a linha selecionada
    if ( (grid.Rows > 1) && (grid.Row > 0) )
    {
        if (getInterfaceMode() == 'CC')
        {
            glb_currCCSelInGrid = grid.TextMatrix(grid.Row, 12);
        }
        else if (getInterfaceMode() == 'CB')
        {
            glb_currCBSelInGrid = grid.TextMatrix(grid.Row, 10);
        }
        
        setControlsStateInBar();
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_telefonia_DblClick( grid, Row, Col)
{
	if ( window.top.overflyGen.DlgModalIsOpened != 0)
	{
		return null;
	}	

    if ( Row < 1 )
        return true;
        
    var sInterfMode = getInterfaceMode();
    var nCurrCallState = 0;
    
    if ( sInterfMode == 'CC' )
    {
        nCurrCallState = parseInt(getCellValueByColKey(grid, 'CallStateID', Row), 10);
        
        if ( nCurrCallState == null )
            return true;
    
        // Dialing
        if ( nCurrCallState == 2 )        
            btnDesligarOnClick(btnDesligar);
        // Ringing
        else if ( nCurrCallState == 3 )
            btnAtenderOnClick(btnAtender);    
        // Talking
        else if ( (nCurrCallState == 7) || (nCurrCallState == 16) )
            btnEsperaOnClick(btnEspera);    
        // Espera
        else if ( nCurrCallState == 1 )
            btnAtenderOnClick(btnAtender);
        // Hold
        else if ( nCurrCallState == 10 )        
            btnAtenderOnClick(btnAtender);
        // Conferencia
        else if ( nCurrCallState == 12 )        
            btnDesligarOnClick(btnDesligar);
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_telefonia_BeforeSort(grid, col)
{
    if (grid.Row > 0)
        glb_LASTLINESELID = grid.TextMatrix(grid.Row, 10);
    else
        glb_LASTLINESELID = '';
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_telefonia_AfterSort(grid, col)
{
    var i;
    
    if ( (glb_LASTLINESELID != '') && (grid.Rows > 1) )
    {
        for (i=1; i<grid.Rows; i++)
        {
            if (grid.TextMatrix(i, 10) == glb_LASTLINESELID)
            {
                grid.TopRow = i;
                grid.Row = i;        
                break;
            }    
        }
    }
}
// FINAL DE EVENTOS DE GRID PARTICULARES DESTA PAGINA ===============
