/********************************************************************
apicode.js

Library javascript para o codigo da telefonia
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// IDs das janelas modless do overflygen
var _MSG_GANCHO_ID = 100;
var _MSG_RAMALVALIDO_ID = 200;
var _MSG_RAMALVALIDO2_ID = 300;
var _MSG_NUMEROVALIDO_ID = 400;
var _MSG_NUMEROVALIDO2_ID = 500;
var _MSG_CALLBACKFORAHORARIO_ID = 600;

// O TIPO DE RAMAL !!! (Colocar em tabela?)
var glb_RAMALDIGITAL = false;

var glb_isLogged = false;

var glb_execTelephonEvents = true;

var glb_aChamadasAtendidas = new Array();

var glb_nKeyDataOriginadoInProcess = 0;

var glb_nPessoaIDByCarrier = 0;

var glb_CallStateChangedExecuting = false;

var glb_GetCallDataExecuting = false;

var glb_callBackCallingDirectExec = false;

var glb_callBackCallingDirTimer = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RESUMO DAS FUNCOES
    logInOnCentral()

    api_js_APIAgentStateChanged()
    api_js_CallStateChanged()
    api_js_APILogonState(State)
    
    telefoniaDisconnect()    
    
    telefoniaDiscarCC(ctrl, theNumber)
    telefoniaAtender()
    telefoniaEspera()
    telefoniaTransferir(objIndex)
    telefoniaConferencia()
    telefoniaDesligar()
    telefoniaCallback(objIndex)
    telefoniaCorreioVoz(objIndex)
    telefoniaURA(objIndex)
    
    telefoniaAgendarCB(sNumber)
    telefoniaLigar()
    telefoniaReprogramar()
    telefoniaCancelar()
    
    setObjByChamadasCollection()
    writeCallbackInfo( thisCall )
    getCallData(sNumberToCall)
    dsoOriginadas_DSC()
    recoveryHoldCall()
    pesqCallByCallStateID(nCallStateID)

    fillObjCCByArray(nENumInArray, internalCallID, callStateID, sCallData, callStateToID, callCauseID, key, dateTime, nCallStateMsg, nCB_Count, nCL7)
    
    callBackCallingDirect(nCallBackID, theNumber)
    callBackCallingDirect_DSC()
    callBackCallingDirect_Timer()
    
    sliceData(sData)
    removeZeroOnLeftSide(sNumber, bOnlyFirst)
    
    equalizeCCArrayWithCallNumber(nChamadas)
    clearOneObjCCInCCArray(objIndex)
    clearAllObjCCInCCArray()
    numElemsInCCArray(sCountMode)
    findCallInCCArray(nInternalCallID)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Loga o usuario do Overfly na telefonia
********************************************************************/
function logInOnCentral()
{
    // Usar um ou outro (nao necessariamente os dois)
    var machineName = '';
    var machineNumber = glb_machineNumber;

    // Valor fixo
    var appType = 4;

    // ID e senha do usuario logado no Overfly
    var userId = sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CURRUSERID', null);
    
    if (userId == null)
    {
        lockControls(true);
        // fechar a janela de telefonia
        sendJSMessage(getHtmlId(), JS_NOMFORMCLOSE, 1, window.name);
        return null;
    }    
    
    // password e um valor qualquer
    var pass = 'abcd';
    
    // Para teste de ramal digital
    if (glb_device == 2634)
		glb_RAMALDIGITAL = true;
    // Fim de Para teste de ramal digital
    
    // Ramal a conectar
    var device = glb_device;
    
    API.ApplicationId.MachineName = machineName;
    API.ApplicationId.MachineNumber = machineNumber;
	API.ApplicationId.ApplicationType = appType;	
	
	API.ApplicationId.UserId = userId;
	API.ApplicationId.Password = pass;
	
	// Porta do server do CTI
	API.Connection.Port = SERVER_CTI_PORT;
	// IP do server do CTI
	API.Connection.Address = SERVER_MACH_ADDRESS;
	
	API.Device.Extension= device;
    
    if ( (glb_device != '0') && (glb_device != '2626') )
    {
        if ( !(API.Connect() == 0) )
            lockControls(true);
        else
        {
            glb_isLogged = true;                
            overflyGen.TimerPulseInterval = glb_TimerPulseInterval;
            // overflyGen.StartHookKBAndMouse(glb_UserTimerInterval, 1);
        }    
    }    
    else
    {
        lockControls(true);
        return null;
    }
    
    // Se ramal digiatl
	if ( glb_RAMALDIGITAL )    
		especificToAttendDigital();    
}

/********************************************************************
Evento da central
********************************************************************/
function api_js_APIAgentStateChanged()
{
    if ( !glb_execTelephonEvents )
        return true;
        
    // Este evento nao e recebido no caso do Overfly    
}

/********************************************************************
Evento da central
********************************************************************/
function api_js_CallStateChanged()
{
    if ( !glb_execTelephonEvents )
        return true;
 
    //var dObj = new Date();
    //trace('***In�cio api_js_CallStateChanged()' + dObj.getSeconds() + ':' + dObj.getMilliseconds());   
    
    setObjByChamadasCollection();
}

/********************************************************************
Evento da central
State       - 0 desconectado
            - 1 conectado
********************************************************************/
function api_js_APILogonState(State)
{
    if ( !glb_execTelephonEvents )
        return true;
    
    if ( State == 0 )
        lockControls(true);
}

/********************************************************************
Desconecta o agente da telefonia
********************************************************************/
function telefoniaDisconnect()
{
    try
    {
        glb_execTelephonEvents = false;
        
        if ( glb_isLogged )       
            API.Disconnect();
    }
    catch(e)   
    {
        ;
    }
}

/********************************************************************
Discar em modo de chamadas ativas
ctrl		- o controle da interface que invocou a funcao
theNumber   - numero a discar
********************************************************************/
function telefoniaDiscarCC(ctrl, theNumber)
{
    var i, retVal;
    var stateID;
    var internalCallID;
    // commandAvaliable 0->Hold / Discar, 1->Discar
    var commandAvaliable = 0;
    var newCall;
    var newDeviceTo;
    var retNumber;
    var tempVar;

	if ( theNumber != null )
		retNumber = getNumAlphaStriped(theNumber);
	else
	{
		txtNumeroDiscarCC.value = getNumAlphaStriped(txtNumeroDiscarCC.value);
		retNumber = txtNumeroDiscarCC.value;
    }

    if (fg.Rows > 1)
    {
        stateID = parseInt(fg.TextMatrix(fg.Row, 12), 10);
        internalCallID = fg.TextMatrix(fg.Row, 11);
        
        if ( (stateID == 7) || (stateID == 26 ) )
            commandAvaliable = 0;
        else
            commandAvaliable = 1;
            
        if ( (stateID == 7) && (getInterfaceMode() == 'CB') )    
			commandAvaliable = 1;
    }
    else
        commandAvaliable = 1;
    
    if ( (retNumber != null) &&  (retNumber != '') )
    {
        // coloca zero se nao for ligacao de ramal para ramal
        if ( (retNumber.length != 4) && (retNumber.length != 5) )
            retNumber = '0' + retNumber;
    }
    else
    {
        window.top.overflyGen.AlertModless(_MSG_NUMEROVALIDO2_ID, 'Digite um n�mero v�lido.');
        setControlsStateInBar();
        return false;
    }    

    retVal = true;
    if ( commandAvaliable == 0 )
    {
        for(i=1; i<=API.Calls.Count; i++)
        {
            if ( API.Calls(i).InternalCallID == internalCallID )
            {
                newDeviceTo = API.NewDevice();
                newDeviceTo.Extension = retNumber;
                retVal = API.Calls(i).CallConsultation(newDeviceTo);
                
                if ( retVal != 0 )
                    retVal = false;
                    
                break;    
            }
        }
    }
    else
    {
        newCall = API.NewCall();
        newCall.Device.Extension = retNumber;
        
        retVal = API.MakeCall(newCall, API.Device);
    }

    return retVal;
}

/********************************************************************
Atender uma ligacao
********************************************************************/
function telefoniaAtender()
{
    var i, retVal;
    var stateID = parseInt(fg.TextMatrix(fg.Row, 12), 10);
    var internalCallID = fg.TextMatrix(fg.Row, 11);
    var nKey = getCellValueByColKey(fg, 'Key', fg.Row);
        
    retVal = true;

    if ( !((stateID == 1) ||
           (stateID == 3) ||
           (stateID == 10)) )
        return false;
        
    btnAtender.disabled = true;
    
    for(i=1; i<=API.Calls.Count; i++)
    {
        if ( API.Calls(i).Key == nKey )
        {
            // recupera chamada em hold
            if (stateID == 10)
                retVal = API.Calls(i).CallGet();
            // especifico para atender ramal digital    
            else if ( (stateID == 3) && (glb_RAMALDIGITAL) )
				API.Calls(i).CallAnswer();
            // recupera chamada da fila (chamada inicial ou em espera)
            // vindo da URA ou de ligacao em espera
            else
            {
				// especifico para ramal digital
				if ( glb_RAMALDIGITAL )
				{
					var sInformation = API.NewInformations();
	
			        sInformation.Add('CL8', '1', '');
			        API.Calls(i).SetData(sInformation);
				}
				
                retVal = API.Calls(i).CallDeflect(API.Device);
            }    
            
            if ( retVal != 0 )
                retVal = false;
            break;    
        }
    }
    
    return retVal;
}

/********************************************************************
Colocar uma ligacao em hold
********************************************************************/
function telefoniaEspera()
{
    var i, retVal;
    var stateID = parseInt(fg.TextMatrix(fg.Row, 12), 10);
    var internalCallID = fg.TextMatrix(fg.Row, 11);
    var newDevice;
    var clsSendTo;
    
    if ( !( /* (stateID == 1) || */
           (stateID == 7) ||
           (stateID == 26 )) )
        return false;
    
    retVal = true;
    
    for(i=1; i<=API.Calls.Count; i++)
    {
        if ( API.Calls(i).InternalCallID == internalCallID )
        {
            // so pode colocar em hold a ligacao ativa recebida (state = 7)
            // ou a ligacao ativa originada (state = 26) 
            if ( (stateID == 7) || (stateID == 26) )
            {
                newDevice = API.NewDevice();
    
                // fila de espera fixa para holds
                newDevice.Extension = '3050';

                clsSendTo = API.NewSendToCollection();
                clsSendTo.Add(API.Device, newDevice, '');

                retVal = API.Calls(i).SetData(clsSendTo);

                if (retVal == 0)
                {
                    retVal = API.Calls(i).CallDeflect(newDevice);
                    
                    // Recupera chamada em hold
                    if (retVal == 0)
                        recoveryHoldCall();
                }    
            }    

            if ( retVal != 0 )
                retVal = false;
                
            break;    
        }
    }
    
    return retVal;
}

/********************************************************************
Transferir uma ligacao
********************************************************************/
function telefoniaTransferir(objIndex)
{
    var i, retVal;
    var stateID = 0;
    var internalCallID = 0;
    var holdCall = null;
    retVal = true;
    var nRamalToTransfer = getNumAlphaStriped(txtNumeroDiscarCC.value);
    var uraDevice;

    if ( objIndex == null )
    {
        stateID = parseInt(fg.TextMatrix(fg.Row, 12), 10);
        internalCallID = fg.TextMatrix(fg.Row, 11);
    }
    else
    {
        stateID = parseInt(glb_aObjCCData[objIndex].CallStateID, 10);
        internalCallID = glb_aObjCCData[objIndex].InternalCallID;
    }

    if ( !((stateID == 1) ||
           (stateID == 3) ||
           (stateID == 7) ||
           (stateID == 10) ||
           (stateID == 26)) )
        return false;
    
    for(i=1; i<=API.Calls.Count; i++)
    {
        if ( API.Calls(i).InternalCallID == internalCallID )
        {
            // recupera chamada em hold
            if (API.Calls(i).CallState == 10)
            {
                holdCall = API.Calls(i);
                break;
            }
        }
    }

    if ( holdCall == null )
    {
        if ( (nRamalToTransfer.length == 4) || (nRamalToTransfer == '9') )
        {
            uraDevice = API.NewDevice();
            uraDevice.Extension = nRamalToTransfer;

            for(i=1; i<=API.Calls.Count; i++)
            {
                if ( (API.Calls(i).InternalCallID == internalCallID) &&
                     (API.Calls(i).CallState == stateID) )
                {
                    retVal = API.Calls(i).CallDeflect(uraDevice);

                    // Recupera chamada em hold
                    if (retVal == 0)
                        recoveryHoldCall();

                    break;
                }
            }

            if ( retVal != 0 )
                retVal = false;
        }
        else
        {
            if ( objIndex == null )
            {
				window.top.overflyGen.AlertModless(_MSG_RAMALVALIDO2_ID, 'Digite um ramal v�lido.');
				setControlsStateInBar();
            }
        }
    
        return retVal;
    }
    else
    {    
        for(i=1; i<=API.Calls.Count; i++)
        {
            if ( API.Calls(i).InternalCallID == internalCallID )
            {
                // transfere a chamada que estava em hold
                if ( (API.Calls(i).CallState == 7) || (API.Calls(i).CallState == 26) )
                {
                    API.Calls(i).Transfer(holdCall);
                    break;
                }
            }
        }
    }
    
    return retVal;
}

/********************************************************************
Colocar uma ligacao em conferencia
********************************************************************/
function telefoniaConferencia()
{
    var i, retVal;
    var stateID = parseInt(fg.TextMatrix(fg.Row, 12), 10);
    var internalCallID = fg.TextMatrix(fg.Row, 11);
    var holdCall = null;
    retVal = true;

    if ( !((stateID == 7) ||
           (stateID == 10) ||
           (stateID == 26)) )
        return false;
    
    for(i=1; i<=API.Calls.Count; i++)
    {
        if ( API.Calls(i).InternalCallID == internalCallID )
        {
            // recupera chamada em hold
            if (API.Calls(i).CallState == 10)
            {
                holdCall = API.Calls(i);
                break;
            }
        }
    }

    for(i=1; i<=API.Calls.Count; i++)
    {
        if ( API.Calls(i).InternalCallID == internalCallID)
        {
            // recupera chamada em hold
            if ( (API.Calls(i).CallState == 7) || (API.Calls(i).CallState == 26) )
            {
                API.Calls(i).CallConference(holdCall);
                break;
            }
        }
    }
    
    return retVal;
}

/********************************************************************
Desligar uma ligacao
********************************************************************/
function telefoniaDesligar()
{
    var i, retVal;
    var internalCallID = fg.TextMatrix(fg.Row, 11);
    var stateID = fg.TextMatrix(fg.Row, 12);
    var newDevice = null;
    
    if ( !((stateID == 7) ||
           (stateID == 12) ||
           (stateID == 26) ||
           (stateID == 31)) )
        return false;
        
    retVal = true;

    for(i=1; i<=API.Calls.Count; i++)
    {
        if ( API.Calls(i).InternalCallID == internalCallID )
        {
            // Artificio Damovo para
            // recuperar eventual chamada em hold
            if (pesqCallByCallStateID(10) == -1)
                retVal = API.Calls(i).HangUp();
            else
            {
                newDevice = API.NewDevice();
                newDevice.Extension = '2996';

                retVal = API.Calls(i).CallDeflect(newDevice);
                    
                // Recupera chamada em hold
                if (retVal == 0)
                    recoveryHoldCall();
            }    
            
            if ( retVal != 0 )
                retVal = false;
                
            break;    
        }
    }    
    
    return retVal;
}

/********************************************************************
Colocar uma ligacao em callback
********************************************************************/
function telefoniaCallback(objIndex)
{
    var i, retVal;
    var stateID = 0;
    var nCallStateToID = 0;
    var nCallCause = 0;
    var internalCallID = 0;
    var nNumberReceived = 0;
    var nCBCount;
    var nPessoaID;

    if ( objIndex == null )
    {
        stateID = parseInt(fg.TextMatrix(fg.Row, 12), 10);
        nCallStateToID = getCellValueByColKey(fg, 'CallStateToID', fg.Row);
        nCallCause = getCellValueByColKey(fg, 'CallCause', fg.Row);
        internalCallID = fg.TextMatrix(fg.Row, 11);
        
        nNumberReceived = trimStr(fg.TextMatrix(fg.Row, 2));
        nNumberReceived = (( (nNumberReceived.length == 7) || (nNumberReceived.length == 8) ) ? (trimStr(fg.TextMatrix(fg.Row, 1)) + nNumberReceived) : nNumberReceived);
        nCBCount = getCellValueByColKey(fg, 'CBCount', fg.Row);
        nPessoaID = getCellValueByColKey(fg, 'PessoaID', fg.Row);
    }
    else
    {
        stateID = glb_aObjCCData[objIndex].CallStateID;
        nCallStateToID = glb_aObjCCData[objIndex].CallStateToID;
        nCallCause = glb_aObjCCData[objIndex].CallCauseID;
        internalCallID = glb_aObjCCData[objIndex].InternalCallID;
        
        nNumberReceived = glb_aObjCCData[objIndex].Numero;
        nNumberReceived = (( (nNumberReceived.length == 7) || (nNumberReceived.length == 8) ) ? (glb_aObjCCData[objIndex].DDD + nNumberReceived) : nNumberReceived);
        nCBCount = glb_aObjCCData[objIndex].CBCount;
        nPessoaID = glb_aObjCCData[objIndex].ClienteID;
    }
    
    if ( nPessoaID == null )
		nPessoaID = '';
    
    if ( (nCBCount == null) || (nCBCount == '') )
		nCBCount = 0;
	
	if ( nCBCount >= 0 )
	{
		nCBCount = parseInt(nCBCount, 10);
		nCBCount++;
	}	
    
    if ( (nNumberReceived == null) ||
		 (nNumberReceived == '0') ||
		 (nNumberReceived == '00') ||
		 (nNumberReceived == '000') ||
		 (nNumberReceived == '9') ||
         (nNumberReceived == glb_device) )
        return false;

    var uraDevice = null;
    var sInformation;
    
    retVal = true;

    if ( !((stateID == 1) ||
           (stateID == 3) || (stateID == 7) || 
           ((stateID == 26) && (nCallStateToID == 7) && (nCallCause == 28))) )
        return false;
    
    uraDevice = API.NewDevice();
    uraDevice.Extension = 'ura';
    
    try
    {
		for(i=1; i<=API.Calls.Count; i++)
		{
		    if ( API.Calls(i).InternalCallID == internalCallID )
		    {
		        sInformation = API.NewInformations();

		        // tipo da aplicacao, neste caso callback
		        sInformation.Add('CL1', '1', '');
		        
		        // Insercao do callback direto pelo Overfly no banco de dados
		        // refreshCallbackData(nNumberReceived, API.Calls(i).Informations.InformationValue('CID'), nCBCount);
		       
		        // date time no formato yyyy-mm-dd hh:mm:ss
		        sInformation.Add('CL5', timeToPrgCallBack(parseInt(selTimeInterval.value, 10)), '');

				// Inicio de Insercao do callback pela URA
				
				sInformation.Add('CL2', nNumberReceived, '');
				sInformation.Add('CL3', nPessoaID, '');
				sInformation.Add('CL4', glb_device, '');
				sInformation.Add('CL6', nCBCount, '');
				
				// Fim de Insercao do callback pela URA

		        API.Calls(i).SetData(sInformation);

		        retVal = API.Calls(i).CallDeflect(uraDevice);

		        // Recupera chamada em hold
		        if (retVal == 0)
		            recoveryHoldCall();

		        if ( retVal != 0 )
		            retVal = false;
		    
		        break;    
		    }
		    
		}
	}
    catch(e)
    {
		;
    }
    
    return retVal;
}

/********************************************************************
Colocar uma ligacao no coreio de voz
********************************************************************/
function telefoniaCorreioVoz(objIndex)
{
    var i, retVal;
    var stateID = 0;
    var internalCallID = 0;
    var voiceMailDevice = null;
    var sInformation;
    retVal = true;
    
    if (objIndex == null)
    {
        stateID = parseInt(fg.TextMatrix(fg.Row, 12), 10);
        internalCallID = fg.TextMatrix(fg.Row, 11);
    }
    else
    {
        stateID = glb_aObjCCData[objIndex].CallStateID;
        internalCallID = glb_aObjCCData[objIndex].InternalCallID;
    }    

    if ( !((stateID == 1) ||
           (stateID == 3)) )
        return false;
    
    voiceMailDevice = API.NewDevice();
    voiceMailDevice.Extension = 'VMail';
    
    for(i=1; i<=API.Calls.Count; i++)
    {
        if ( API.Calls(i).InternalCallID == internalCallID )
        {
            sInformation = API.NewInformations();
            sInformation.Add('BTO', API.Device.Extension, '');
            
            API.Calls(i).SetData(sInformation);

            retVal = API.Calls(i).CallDeflect(voiceMailDevice);
            
            // Recupera chamada em hold
            if (retVal == 0)
                recoveryHoldCall();
            
            if ( retVal != 0 )
                retVal = false;
            break;    
        }
    }
    
    return retVal;
}

/********************************************************************
Colocar uma ligacao em URA
********************************************************************/
function telefoniaURA(objIndex)
{
    var i, retVal;
    var stateID = 0;
    var internalCallID = 0;
    var uraDevice = null;
    var newDevice = null;
    var clsSendTo = null;
    var sInformation;
    
    if (objIndex == null)
    {
        stateID = parseInt(fg.TextMatrix(fg.Row, 12), 10);
        internalCallID = fg.TextMatrix(fg.Row, 11);
    }
    else
    {
        stateID = glb_aObjCCData[objIndex].CallStateID;
        internalCallID = glb_aObjCCData[objIndex].InternalCallID;
    }    
    
    retVal = true;

    if ( !((stateID == 1) ||
           (stateID == 3) ||
           (stateID == 7) ||
           (stateID == 26)) )
        return false;
    
    uraDevice = API.NewDevice();
    uraDevice.Extension = 'ura';
    
    for(i=1; i<=API.Calls.Count; i++)
    {
        if ( API.Calls(i).InternalCallID == internalCallID )
        {
            sInformation = API.NewInformations();
            sInformation.Add('CL1', '2', '');
            
            API.Calls(i).SetData(sInformation);

            // Monitora o retorno da ura
            newDevice = API.NewDevice();
    
            // fila de espera fixa para holds
            newDevice.Extension = '3050';
                
            clsSendTo = API.NewSendToCollection();
            clsSendTo.Add(API.Device, newDevice, '');

            retVal = API.Calls(i).SetData(clsSendTo);
                
            if ( retVal == 0 )
            {
                retVal = API.Calls(i).CallDeflect(uraDevice);

                // Recupera chamada em hold
                if (retVal == 0)
                    recoveryHoldCall();
            }    

            if ( retVal != 0 )
                retVal = false;
                
            break;    
        }
    }
    
    return retVal;
}

/********************************************************************
Discar em modo de callback, na verdade inserir na tabela de callback
********************************************************************/
function telefoniaAgendarCB(sNumber)
{
	// overflyGen.StopHookKBAndMouse();

    lockControls(true);
    
    var empresa = getCurrEmpresaData();
    
    var strPars = new String();

   	strPars = '?nTipoOperacao=1';
   	strPars += '&sDevice=' + escape(glb_device);
   	strPars += '&sNumeroTelefone=' + escape(getNumAlphaStriped(txtNumeroAgendarCB.value));
   	strPars += '&nCallTimeInterval=' + escape(Math.abs(parseInt(selTimeInterval.value, 10)));
   	strPars += '&nEmpresaID=' + escape(empresa[0]);
   	strPars += '&nCBCount=' + escape(-1);

    dsoCallbacks.URL = SYS_ASPURLROOT + '/telefonia/serverside/callbackdata.asp' + strPars;
    dsoCallbacks.ondatasetcomplete = refreshCallbackData_DSC;
    dsoCallbacks.refresh();
}

/********************************************************************
Ligar uma ligacao que esta em callback
********************************************************************/
function telefoniaLigar()
{
	// overflyGen.StopHookKBAndMouse();
	
    if ( chkLigarDireto.checked == false )
    {
		lockControls(true);
		
		var strPars = new String();

   		strPars = '?nTipoOperacao=2';
   		strPars += '&sDevice=' + escape(glb_device);
   		strPars += '&nCallBackID=' + escape(getCellValueByColKey(fg, 'CallBackID', fg.Row));

		dsoCallbacks.URL = SYS_ASPURLROOT + '/telefonia/serverside/callbackdata.asp' + strPars;
		dsoCallbacks.ondatasetcomplete = refreshCallbackData_DSC;
		dsoCallbacks.refresh();
    }
    else
    {
		var nDDD, nNumero;
			
		nDDD = getCellValueByColKey(fg, 'DDD', fg.Row);
		nNumero = getCellValueByColKey(fg, 'Numero', fg.Row);
	
		btnLigar.disabled = false;
	
		if ( nDDD != 11 )
			telefoniaDiscarCC(btnDiscarCC, glb_OPERADORA + nDDD.toString() + nNumero.toString());
		else	
			telefoniaDiscarCC(btnDiscarCC, nNumero.toString());
    }
}

/********************************************************************
Adiar uma ligacao que esta em callback
********************************************************************/
function telefoniaReprogramar()
{
	// overflyGen.StopHookKBAndMouse();
	
    lockControls(true);
    
    var strPars = new String();
    
    var empresa = getCurrEmpresaData();
    
   	strPars = '?nTipoOperacao=3';
   	strPars += '&sDevice=' + escape(glb_device);
   	strPars += '&nCallBackID=' + escape(getCellValueByColKey(fg, 'CallBackID', fg.Row));
    strPars += '&nCallTimeInterval=' + escape(selTimeInterval.value);
    strPars += '&nEmpresaID=' + escape(empresa[0]);
    strPars += '&sNumeroTelefone=' + escape(getCellValueByColKey(fg, 'DDD', fg.Row) + getCellValueByColKey(fg, 'Numero', fg.Row));

    dsoCallbacks.URL = SYS_ASPURLROOT + '/telefonia/serverside/callbackdata.asp' + strPars;
    dsoCallbacks.ondatasetcomplete = refreshCallbackData_DSC;
    dsoCallbacks.refresh();
}

/********************************************************************
Cancelar uma ligacao que esta em callback
********************************************************************/
function telefoniaCancelar()
{
	// overflyGen.StopHookKBAndMouse();
	
    lockControls(true);
    
    var strPars = new String();
	var empresa = getCurrEmpresaData();
    
   	strPars = '?nTipoOperacao=4';
   	strPars += '&sDevice=' + escape(glb_device);
   	strPars += '&nCallBackID=' + escape(getCellValueByColKey(fg, 'CallBackID', fg.Row));
	strPars += '&nEmpresaID=' + escape(empresa[0]);
    strPars += '&sNumeroTelefone=' + escape(getCellValueByColKey(fg, 'DDD', fg.Row) + getCellValueByColKey(fg, 'Numero', fg.Row));

    dsoCallbacks.URL = SYS_ASPURLROOT + '/telefonia/serverside/callbackdata.asp' + strPars;
    dsoCallbacks.ondatasetcomplete = refreshCallbackData_DSC;
    dsoCallbacks.refresh();
}

/********************************************************************
Repreenche obj com dados das chamadas

Valores possiveis para API.Calls(i).CallState que interessam para atualizar
os grids
********************************************************************/
function setObjByChamadasCollection()
{
    if (glb_CallStateChangedExecuting)
        return null;

    glb_CallStateChangedExecuting = true;
    
    var i, j, k, aElemCount;
    var nChamadaIndex = -1;
    var numeroDeviceOrigem = ''; 
    var callID = 0;
    var internalCallID = 0;
    var sCallData = '';
    var nCallState = 0;
    var strPars = '';
    
    var dateTime = '';
    
    var nCB_Count = null;
    
    var nCallStateTo = 0;
    var nCause = 0;
    var nKey = 0;
    
    var alertMsg = '';
    var sNumCaller = '';
    var sToRep = ' ' + String.fromCharCode(124);
    
    var lastInternalCallID = 0;    
    var lastCallID = 0;
    var nCallStateMsg = null;
    var nCL7 = '';
    var sInformation;
        
    // agente nao tem mais nenhuma chamada
    if ( API.Calls.Count == 0 )
    {
        // lockControls(true);

        clearAllObjCCInCCArray();

        fillMessageCB();

        if ( getInterfaceMode() == 'CC' )
            fillGridInChamadasMode();

        // lockControls(false);
    }
    else
    {
        // lockControls(true);

        for (k=0; k<glb_aChamadasAtendidas.length; k++)
        {
            for (i=1; i<=API.Calls.Count; i++)
            {
                if (glb_aChamadasAtendidas[k] == API.Calls(i).InternalCallId)
                {
                    nChamadaIndex = k;
                    break;
                }
            }
            
            if (nChamadaIndex == -1)
                glb_aChamadasAtendidas[k] = 0;
                
            nChamadaIndex = -1;
        }        
        
        // primeira vez que a chamada recebida vem da URA
        for (i=1; i<=API.Calls.Count; i++)
        {
            // Ramal digital, atende automatico 
            // quando ringing e vindo apos CallDeflect, ver
            // botao Atender
            if ( (glb_RAMALDIGITAL) && (API.Calls(i).CallState == 3) )
            {
				if ( API.Calls(i).Informations.InformationValue('CL8') == '1' )
				{
					sInformation = API.NewInformations();
	
			        sInformation.Add('CL8', '', '');
			        API.Calls(i).SetData(sInformation);
			        				
					API.Calls(i).CallAnswer();
					glb_CallStateChangedExecuting = false;
					return null;
				}	
            }

        
            if ( (API.Calls(i).CallState == 7) || (API.Calls(i).CallState == 26) )
            {
                if ( findCallInCCArray(API.Calls(i).InternalCallId) > -1 )
                {
                    if (glb_aChamadasAtendidas.length == 0)
                        glb_aChamadasAtendidas[glb_aChamadasAtendidas.length] = API.Calls(i).InternalCallId;
                    else
                    {
                        for (k=0; k<glb_aChamadasAtendidas.length; k++)
                        {
                            if (glb_aChamadasAtendidas[k] == 0)
                            {
                                nChamadaIndex = k;
                                break;
                            }
                        }
                        
                        if (nChamadaIndex == -1)
                            glb_aChamadasAtendidas[glb_aChamadasAtendidas.length] = API.Calls(i).InternalCallId;
                        else
                            glb_aChamadasAtendidas[k] = API.Calls(i).InternalCallId;
                    }
                }
            }
        }

        equalizeCCArrayWithCallNumber(API.Calls.Count);
        
        aElemCount = 0;

        trace('------INICIO LOOP---------------------------');
        try
        {
            for (i=1; i<=API.Calls.Count; i++)
            {
                numeroDeviceOrigem = API.Calls(i).Device.Extension;
                internalCallID = API.Calls(i).InternalCallId;
                callID = API.Calls(i).CallId;

                nCallState = API.Calls(i).CallState;
                
                if ( nCallState == 27 )
                {
					writeCallbackInfo(API.Calls(i));
						
					continue;
				}	
                
                dateTime = API.Calls(i).Informations.InformationValue('DTI');
                
                // Inicio de Ligacao provem ou nao do CallBack
                nCB_Count = API.Calls(i).Informations.InformationValue('CL6');
                
                if ( (nCB_Count == null) || (nCB_Count == '') )
					nCB_Count = 0;
				else
				{
					try
					{
						nCB_Count = parseInt(nCB_Count, 10);
					}
					catch (e)
					{
						nCB_Count = 0;
					}
					
				}
				// Fim de Ligacao provem ou nao do CallBack	
                
                nCallStateTo = API.Calls(i).CallStateTo;
                nCause = API.Calls(i).Cause;
                nKey = API.Calls(i).Key;

                nCallStateMsg = null;

                // Atende chamada em callback, entre outros casos
                // Se a fila nao e a 3050 entao a chamada e ringing, senao
                // obedece o dado da API
                if ( (nCallState == 1) && (ascan(glb_aChamadasAtendidas,internalCallID,false) == -1) && (numeroDeviceOrigem != 3050) )
                    nCallStateMsg = 3;
                
                // A chamada nao passou pela URA
                if (trimStr(API.Calls(i).Informations.InformationValue('CID')) == '')
                {
                    // Se chamada originada
                    if (nCallState == 26)
                    {
                        // sNumCaller = trimStr(API.Calls(i).Called.Extension);
                        sNumCaller = trimStr(API.Calls(i).Informations.InformationValue('CLD'));
                                                
                        trace('***' + sNumCaller);
                        
                        if ( (sNumCaller == null) || (sNumCaller == '') )
                        {
                            sNumCaller = trimStr(btnDiscarCC.getAttribute('numberToCall',1));
                            trace('***' + sNumCaller);
                        }    

                        sCallData = replicate(sToRep, 2) + sNumCaller + replicate(sToRep, 7);
                        trace('***' + sNumCaller);
                    }
                    else
                    {
                        // if (trimStr(API.Calls(i).Caller.Extension) == API.Device.Extension)
                        if ( trimStr(API.Calls(i).Informations.InformationValue('CLR')) == API.Device.Extension )
                        {
                            // sNumCaller = removeZeroOnLeftSide(trimStr(API.Calls(i).Called.Extension), true);
                            sNumCaller = removeZeroOnLeftSide(trimStr(API.Calls(i).Informations.InformationValue('CLD')), true);
                            
                            trace('***' + sNumCaller);
                        }    
                        else
                        {
                            // sNumCaller = removeZeroOnLeftSide(trimStr(API.Calls(i).Caller.Extension), true);
                            sNumCaller = removeZeroOnLeftSide(trimStr(API.Calls(i).Informations.InformationValue('CLR')), true);
                            
                            trace('***' + sNumCaller);
                        }    
                        
                        // Trata celulares antigos com 7 numeros
                        if ( (sNumCaller.length == 9) && (sNumCaller.substr(2, 1) == '9') )
                        {
                            sNumCaller = sNumCaller.substr(0, 2) + '9' + sNumCaller.substr(2);
                            trace('***' + sNumCaller);
                        }
                        
                        sCallData = replicate(sToRep, 2) + sNumCaller + replicate(sToRep, 7);
                        trace('***' + sNumCaller);
                    }

                    trace('sCallData: ' + sCallData + '   CallState: ' + nCallState);
                    // Chamada originada ringando
                    // Nao buscar o dado mais de uma vez qdo nao ha dado.
                    if ( ( ((nCallState == 26) && (nCallStateTo == 3)) || (nCallState == 3) || (nCallState == 1) ) && 
                        (glb_nKeyDataOriginadoInProcess != API.Calls(i).Key) )
                    {
                        // Vai ao servidor buscar os dados da ligacao;
                        glb_nKeyDataOriginadoInProcess = API.Calls(i).Key;
                    
                        var sNumberToCall = '';
                    
                        for (j=0; j<sCallData.length; j++)
                        {
                            if ( (sCallData.charAt(j) >= '0') && (sCallData.charAt(j) <= '9') )
                                sNumberToCall += sCallData.charAt(j);
                        }    

                        // if (sNumberToCall.length >= 8)
                        if (sNumberToCall.length >= 7)
                            sNumberToCall = removeZeroOnLeftSide(sNumberToCall, true);

                        if (trimStr(sNumberToCall) != '')
                            getCallData(sNumberToCall);
                    }
                }
                else
                {
                    sCallData = API.Calls(i).Informations.InformationValue('CID');
                    trace('|PELA URA| sCallData: ' + sCallData + '   CallState: ' + nCallState);
                }    

                lastInternalCallID = internalCallID;
                lastCallID = callID;     

                if ( (nCallState == 1) ||
                     (nCallState == 2) ||
                     (nCallState == 3) ||
                     (nCallState == 4) ||
                     (nCallState == 5) ||
                     (nCallState == 7) ||
                     (nCallState == 9) ||
                     (nCallState == 10) ||
                     (nCallState == 11) ||
                     (nCallState == 12) ||
                     (nCallState == 14) ||
                     (nCallState == 15) ||
                     (nCallState == 16) ||
                     (nCallState == 25) ||
                     (nCallState == 26) ||
                     (nCallState == 27) )
                {
                    if (nCallState == 27)
                    {
                        continue;
                    }    

					nCL7 = API.Calls(i).Informations.InformationValue('CL7');
					
                    fillObjCCByArray(aElemCount, internalCallID, nCallState, sCallData, nCallStateTo, nCause, nKey, dateTime, nCallStateMsg, nCB_Count, nCL7);
                    aElemCount++;
                }
                // pedir ao usuario para refazer a operacao
                else if ( nCallState == 31 )
                {
                    alertMsg = 'Favor colocar o telefone no gancho.';
                    window.focus();
                    window.top.overflyGen.AlertModless(_MSG_GANCHO_ID, alertMsg);
                    setControlsStateInBar();
                    glb_CallStateChangedExecuting = false;
                    return null;
                }
            }
        }
        catch(e)
        {
            glb_CallStateChangedExecuting = false;
            return null;
        }
        trace('------FIM LOOP---------------------------');
        
        fillMessageCB();
        
        if ( getInterfaceMode() == 'CC' )
        {
            fillGridInChamadasMode();
            // lockControls(false);
        }    
        
        glb_telefoniaTimer = window.setInterval('operationByRadioButtons()', 950, 'JavaScript');
    }
    
    glb_CallStateChangedExecuting = false;
    
    return null;
}

/********************************************************************
Escreve callbackID em information CL7 do objeto da ligacao, se for o caso
********************************************************************/
function writeCallbackInfo( thisCall )
{
	// se nao esta em modo callback, sai fora
	if ( getInterfaceMode() != 'CB' )
		return null;

	// se nao tem linha selecionada no grid, sai fora
	if ( fg.Rows == 1 )
		return null;
		
	if ( fg.Row < 1 )	
		return null;

	if ( !((thisCall.Informations.InformationValue('CL7') == null) ||
	     (thisCall.Informations.InformationValue('CL7') == '')) )
		return null;
	
	var nCallbackID;
	
	nCallBackID = getCellValueByColKey(fg, 'CallBackID', fg.Row);
	
	var sInformation = API.NewInformations();
	sInformation.Add('CL7', nCallBackID.toString(), '');

	thisCall.SetData(sInformation);
	
	trace('***************** Inserir CL7 = ' + nCallBackID.toString());
}

/********************************************************************
Volta do servidor com os dados de ligacoes originadas
********************************************************************/
function getCallData(sNumberToCall)
{
    if ( glb_GetCallDataExecuting )
        return null;
        
    glb_GetCallDataExecuting = true;

    var empresa = getCurrEmpresaData();
    var nEmpresaID = 0;
    var nEmpresaAlternativaID = 0;
    var sDDD = '';

    nEmpresaID = empresa[0];
    
    if ((nEmpresaID == 2) || (nEmpresaID == 4))
    {
        nEmpresaID = 2;
        nEmpresaAlternativaID = 4;
    }
    else
        nEmpresaAlternativaID = nEmpresaID;
    
    if (sNumberToCall.length <= 8)
        sDDD = '11';
    else if ( (sNumberToCall.length == 9) || (sNumberToCall.length == 10) )
    {
        sDDD = sNumberToCall.substr(0, 2);
        sNumberToCall = sNumberToCall.substr(2);
    }
    else
    {
        sDDD = sNumberToCall.substr(3, 2);
        sNumberToCall = sNumberToCall.substr(5);    
    }        
    
    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nEmpresaAlternativaID=' + escape(nEmpresaAlternativaID);
    strPars += '&sDDI=' + escape(55);
    strPars += '&sDDD=' + escape(sDDD);
    strPars += '&sNumberToCall=' + escape(sNumberToCall);
    strPars += '&nPessoaID=' + escape(glb_nPessoaIDByCarrier);
    
    glb_nPessoaIDByCarrier = 0;

    dsoOriginadas.URL = SYS_ASPURLROOT + '/telefonia/serverside/dadosligacoes.asp' + strPars;
    dsoOriginadas.ondatasetcomplete = dsoOriginadas_DSC;
    dsoOriginadas.refresh();
}

/********************************************************************
Volta do servidor com os dados de ligacoes originadas
********************************************************************/
function dsoOriginadas_DSC()
{
    var i;
    var sInformation = null;
    var sData = '';

    if ( ! ((dsoOriginadas.recordset.BOF) && (dsoOriginadas.recordset.EOF)) )
    {
        for(i=1; i<=API.Calls.Count; i++)
        {
            if ( API.Calls(i).Key == glb_nKeyDataOriginadoInProcess )
            {
                sInformation = API.NewInformations();
                
                if (dsoOriginadas.recordset.Fields('DDI').Value != null)
                    sData += dsoOriginadas.recordset.Fields('DDI').Value;

                sData += '|';
                
                if (dsoOriginadas.recordset.Fields('DDD').Value != null)
                    sData += dsoOriginadas.recordset.Fields('DDD').Value;
                
                sData += '|';
                    
                if (dsoOriginadas.recordset.Fields('Numero').Value != null)
                    sData += dsoOriginadas.recordset.Fields('Numero').Value;
                
                sData += '|';
                
                if (dsoOriginadas.recordset.Fields('PessoaID').Value != null)
                    sData += dsoOriginadas.recordset.Fields('PessoaID').Value;
                    
                sData += '|';
                
                if (dsoOriginadas.recordset.Fields('Fantasia').Value != null)
                    sData += dsoOriginadas.recordset.Fields('Fantasia').Value;

                sData += '|';
                
                if (dsoOriginadas.recordset.Fields('Classificacao').Value != null)
                    sData += dsoOriginadas.recordset.Fields('Classificacao').Value;
                    
                sData += '|';
                
                if (dsoOriginadas.recordset.Fields('Cidade').Value != null)
                    sData += dsoOriginadas.recordset.Fields('Cidade').Value;
                    
                sData += '|';
                
                if (dsoOriginadas.recordset.Fields('UF').Value != null)
                    sData += dsoOriginadas.recordset.Fields('UF').Value;

                sData += '|';

                if (dsoOriginadas.recordset.Fields('Contato1').Value != null)
                    sData += dsoOriginadas.recordset.Fields('Contato1').Value;
                    
                sData += '|';

                if (dsoOriginadas.recordset.Fields('Contato2').Value != null)
                    sData += dsoOriginadas.recordset.Fields('Contato2').Value;
                    
                sData += '|';

                if (dsoOriginadas.recordset.Fields('Contato3').Value != null)
                    sData += dsoOriginadas.recordset.Fields('Contato3').Value;                    

                sData += '|';

                sInformation.Add('CID', sData, '');
                    
                API.Calls(i).SetData(sInformation);
                                
                break;
            }
        }
    }
    
    glb_GetCallDataExecuting = false;
}                

/********************************************************************
Recupera chamada em hold caso exista, apos um callDeflect.
********************************************************************/
function recoveryHoldCall()
{
    var i;

    for(i=1; i<=API.Calls.Count; i++)
    {
        // recupera chamada em hold
        if (API.Calls(i).CallState == 10)
        {
            API.Calls(i).CallGet();
            break;
        }
    }

    return null;
}

/********************************************************************
Pesquisa uma chamada pelo seu callStateID, e retorna a sua posicao em relacao a colecao
********************************************************************/
function pesqCallByCallStateID(nCallStateID)
{
    var i;
    retval = -1;

    for(i=1; i<=API.Calls.Count; i++)
    {
        // Se achou uma ligacao com o CallStateID
        if (API.Calls(i).CallState == nCallStateID)
        {
            retval = i;
            break;
        }
    }

    return retval;
}

/********************************************************************
Preenche objeto de dados de ligacao no array de objetos de dados de
ligacoes correntes.

O pipe e o char 124 -> String.fromCharCode(124)

Parametros:
nENumInArray    - numero do elemento do array
internalCallID  - internalCallID da ligacao
callStateID     - estado da ligacao
sCallData       - dados da ligacao
callStateToID   - stateTo da ligacao
callCauseID     - cause da ligacao
dateTime        - date.hora da ligacao
nCallStateMsg   - estado somente para mensagem
nCB_Count       - originada pelo CallBack
nCL7            - CallBackID (se a ligacao estava em callback)

********************************************************************/
function fillObjCCByArray(nENumInArray, internalCallID, callStateID, sCallData, callStateToID, callCauseID, key, dateTime, nCallStateMsg, nCB_Count, nCL7)
{
    var callState = '';
    var elemInArray;
    var aCallData = null;
    var bCallBackCallingDirect = false;
    
    nCallStateMsg = (nCallStateMsg == null ? callStateID : nCallStateMsg);
    
    if ( nCallStateMsg == 1 )
        callState = 'Espera';
    else if ( nCallStateMsg == 2 )
        callState = 'Dialing';
    else if ( nCallStateMsg == 3 )
    {
		if ( nCB_Count < 0)
			callState = 'Ringing - seu Agendamento';
		else if ( nCB_Count == 0)
			callState = 'Ringing';
		else if ( nCB_Count > 0)
			callState = 'Ringing - ' + nCB_Count + '� ret Callback';
			
		if ( nCB_Count < 0)
			nCB_Count = 0;	
    }    
    else if ( nCallStateMsg == 4 )
        callState = 'Ocupado';
    else if ( nCallStateMsg == 5 )
        callState = 'Nao atendido';
    else if ( nCallStateMsg == 7 )
        callState = 'Talking';
    else if ( nCallStateMsg == 9 )
        callState = 'Interrompido';
    else if ( nCallStateMsg == 10 )
        callState = 'Hold';
    else if ( nCallStateMsg == 11 )
        callState = 'Transferido';
    else if ( nCallStateMsg == 12 )
        callState = 'Confer�ncia';
    else if ( nCallStateMsg == 14 )
        callState = 'Ativando';
    else if ( nCallStateMsg == 15 )
        callState = 'Transferindo';
    else if ( nCallStateMsg == 16 )
        callState = 'Talking';
    else if ( nCallStateMsg == 25 )
        callState = 'Interrompido';
    // Originado
    else if ( nCallStateMsg == 26 )
        callState = 'Talking';
    else if ( nCallStateMsg == 27 )
        callState = 'Livre';
    
    // Inicio de callStateID == 26 tratamento diferenciado ----------
    // Chamada originada e o telefone chamado nao existe
    // vem callStateID = 31 e nao tratamos
            
    // Chamada originada e esta ringando o telefone chamado - 3 = DIALING
    if ( (nCallStateMsg == 26) && ( (callStateToID == 0) || (callStateToID == 3) ) && (callCauseID == 22) )
        callState = 'Dialing';
                
    // Chamada originada e esta ocupado o telefone chamado - 4 = OCUPADO
    // nao tratamos
            
    // Chamada originada e esta falando com o telefone chamado
    if ( (nCallStateMsg == 26) && (callStateToID == 7) && (callCauseID == 28) )
    {
		bCallBackCallingDirect = true;
        callState = 'Talking';
    }    
            
    // Chamada originada e o telefone chamado desligou
    // nao tratamos
    
    // Final de callStateID == 26 tratamento diferenciado -----------

    clearOneObjCCInCCArray(nENumInArray);

    elemInArray = glb_aObjCCData[nENumInArray];
    
    elemInArray.dataValid = true;

    elemInArray.CallStateID = callStateID;
    
    elemInArray.CallState = trimStr(callState);
    elemInArray.InternalCallID = parseInt(trimStr(internalCallID), 10);
    
    elemInArray.CallStateToID = callStateToID;
    elemInArray.CallCauseID = callCauseID;
    elemInArray.Key = key;
    
    elemInArray.CBCount = nCB_Count;
    
    aCallData = sliceData(sCallData);

    if ( aCallData.length >= 7 )
    {
        elemInArray.DDI = trimStr(aCallData[0]);
        
        aCallData[1] = trimStr(aCallData[1]);
        aCallData[2] = trimStr(aCallData[2]);
        aCallData[2] = removeZeroOnLeftSide(aCallData[2], true);
        
        if ( isNaN(aCallData[2]) )
            aCallData[2] = '';

        if ( (aCallData[1] == '') && (aCallData[2].length > 8) )
        {
        	aCallData[1] = aCallData[2].substr(0, 2);
        	aCallData[2] = aCallData[2].substr(2);
        }
        // else if ( (aCallData[1] == '') && (aCallData[2].length == 8) )
        else if ( (aCallData[1] == '') && ((aCallData[2].length == 7) || (aCallData[2].length == 8)) )
        {
        	aCallData[1] = '11';
        }
        
        elemInArray.DDD = aCallData[1];

        elemInArray.Numero = aCallData[2];
    
        if ( isNaN(parseInt(trimStr(aCallData[3]), 10)) )
            elemInArray.ClienteID = '';
        else    
            elemInArray.ClienteID = parseInt(trimStr(aCallData[3]), 10);
    
        elemInArray.Fantasia = trimStr(aCallData[4]);
        elemInArray.Classificacao = trimStr(aCallData[5]);
        elemInArray.Cidade = trimStr(aCallData[6]);
        elemInArray.UF = trimStr(aCallData[7]);
        elemInArray.Contato1 = trimStr(aCallData[8]);
        elemInArray.Contato2 = trimStr(aCallData[9]);
        elemInArray.Contato3 = trimStr(aCallData[10]);
    
        elemInArray.DateTime = dateTime;
        
        if ( elemInArray.Numero == glb_VOICEMAILNUM )
			elemInArray.Fantasia = glb_VOICEMAILNAME;
			
		if (bCallBackCallingDirect)
			callBackCallingDirect(nCL7, elemInArray.Numero);
    }    
}

/********************************************************************
Trata uma chamada direta de um numero da lista de callbacks, no 
banco de dados

Parametro:
nCL7		- CallBackID
theNumber	- numero chamado

********************************************************************/
function callBackCallingDirect(nCallBackID, theNumber)
{
	if (glb_callBackCallingDirectExec)
		return null;
	
	if ( (dsoCallbacks.recordset.BOF && dsoCallbacks.recordset.EOF) )
		return null;
	
	dsoCallbacks.recordset.MoveFirst();

	// se a ligacao existe no dso de callback, vai a banco
    while (!dsoCallbacks.recordset.EOF)
    {
        if ( dsoCallbacks.recordset('CallBackID').value == parseInt(nCallBackID, 10) )
		{
			if (  (theNumber.toString()).indexOf((dsoCallbacks.recordset('Numero').value).toString()) != -1 )
			{
				glb_callBackCallingDirectExec = true;		
				
				lockControls(true);
				    
				var strPars = new String();
				var empresa = getCurrEmpresaData();
				    
				strPars = '?nTipoOperacao=5';
				strPars += '&sDevice=' + escape(glb_device);
				strPars += '&nCallBackID=' + escape(nCallBackID);
				strPars += '&nEmpresaID=' + escape(empresa[0]);
				strPars += '&sNumeroTelefone=' + escape((dsoCallbacks.recordset('DDD').value).toString() + (dsoCallbacks.recordset('Numero').value).toString());

				dsoCallbacks.URL = SYS_ASPURLROOT + '/telefonia/serverside/callbackdata.asp' + strPars;
				dsoCallbacks.ondatasetcomplete = callBackCallingDirect_DSC;
				dsoCallbacks.refresh();
				
				break;
			}
		}            
        dsoCallbacks.recordset.moveNext();
    }
}

/********************************************************************
Retorno do banco de dados apos tratar uma chamada direta de um numero
da lista de callbacks

Parametro:
nenhum
********************************************************************/
function callBackCallingDirect_DSC()
{
	lockControls(false);
	
	glb_callBackCallingDirTimer = window.setInterval('callBackCallingDirect_Timer()', 10, 'JavaScript');	
}

/********************************************************************
Funcao chamada por timer, apos retorno do banco de dados, 
apos tratar uma chamada direta de um numero
da lista de callbacks

Parametro:
nenhum
********************************************************************/
function callBackCallingDirect_Timer()
{
	if ( glb_callBackCallingDirTimer != null )
	{
		window.clearInterval(glb_callBackCallingDirTimer);
		glb_callBackCallingDirTimer = null;
	}
	
	if ( getInterfaceMode() == 'CB' )
		refreshCallbackData_DSC();
	
	glb_callBackCallingDirectExec = false;
}

/********************************************************************
Preenche e retorna array com substrings de string, cujo pipe e |

Parametro:

nChamadas   - numero de chamadas correntes
********************************************************************/

function sliceData(sData)
{
    var re = '|';
    return sData.split(re);
}

/********************************************************************
Remove zeros a esquerda de um string number

Parametro:
    sNumber - string numero a tratar
    bOnlyFirst - trata apenas o primeiro caracter
Retorno:
    sNumber sem os zeros a esquerda
********************************************************************/
function removeZeroOnLeftSide(sNumber, bOnlyFirst)
{
    if ( (sNumber == null) || (sNumber == '') )
        return sNumber;
        
    var firstNonZeroPos = 0;
    var nNumberLen = sNumber.length;
    var i = 0; 
    
    if ( nNumberLen == 1 )
    {
        if ( sNumber == '0' )
            return '';
        else
            return sNumber;
    }
    
    for ( i=0; i<nNumberLen; i++ )
    {
        if ( sNumber.charAt(i) != '0' )
        {
            firstNonZeroPos = i;
            break;
        }    
        
        if ( bOnlyFirst )
        {    
            if ( sNumber.charAt(i) == '0' )
                firstNonZeroPos = i + 1;
            
            break;
        }    
    }
    
    return sNumber.substr(firstNonZeroPos);
}

/********************************************************************
Zera o array de objetos de chamadas
Acrescenta novos objetos de chamadas ao array se necessario

Parametro:

nChamadas   - numero de chamadas correntes
********************************************************************/
function equalizeCCArrayWithCallNumber(nChamadas)
{
    var i;
    
    // Zera o array de objetos de chamadas
    clearAllObjCCInCCArray();
    
    // Acrescenta novos objetos de chamadas ao array se necessario
    for ( i=glb_aObjCCData.length; i<=nChamadas; i++ )
        glb_aObjCCData[glb_aObjCCData.length] = new __objChamadaData();
}

/********************************************************************
Zera um objeto de dados de chamadas, no array de objetos
de dados de chamadas.
********************************************************************/
function clearOneObjCCInCCArray(objIndex)
{
    if ( objIndex >= glb_aObjCCData.length )
        return null;

    glb_aObjCCData[objIndex].CallStateID = null;
    glb_aObjCCData[objIndex].CallState = null;
    glb_aObjCCData[objIndex].InternalCallID = null;
        
    glb_aObjCCData[objIndex].CallStateToID = null;
    glb_aObjCCData[objIndex].CallCauseID = null;
    glb_aObjCCData[objIndex].Key = 0;
    
    glb_aObjCCData[objIndex].CBCount = 0;
        
    glb_aObjCCData[objIndex].DDI = '';
    glb_aObjCCData[objIndex].DDD = '';
    glb_aObjCCData[objIndex].Numero = '';
    glb_aObjCCData[objIndex].ClienteID = 0;
    glb_aObjCCData[objIndex].Fantasia = '';
    glb_aObjCCData[objIndex].Classificacao = '';
    glb_aObjCCData[objIndex].Cidade = '';
    glb_aObjCCData[objIndex].UF = '';
    glb_aObjCCData[objIndex].DateTime = '';
    glb_aObjCCData[objIndex].Contato1 = '';
    glb_aObjCCData[objIndex].Contato2 = '';
    glb_aObjCCData[objIndex].Contato3 = '';
    
    glb_aObjCCData[objIndex].dataValid = false;
}

/********************************************************************
Zera todos os objetos de dados de chamadas, no array de objetos
de dados de chamadas.
********************************************************************/
function clearAllObjCCInCCArray()
{
    var i;
    
    for ( i=0; i<glb_aObjCCData.length; i++ )
    {
        glb_aObjCCData[i].CallStateID = null;
        glb_aObjCCData[i].CallState = null;
        glb_aObjCCData[i].InternalCallID = null;
            
        glb_aObjCCData[i].CallStateToID = null;
        glb_aObjCCData[i].CallCauseID = null;
        glb_aObjCCData[i].Key = 0;
        
        glb_aObjCCData[i].CBCount = 0;
            
        glb_aObjCCData[i].DDI = '';
        glb_aObjCCData[i].DDD = '';
        glb_aObjCCData[i].Numero = '';
        glb_aObjCCData[i].ClienteID = 0;
        glb_aObjCCData[i].Fantasia = '';
        glb_aObjCCData[i].Classificacao = '';
        glb_aObjCCData[i].Cidade = '';
        glb_aObjCCData[i].UF = '';
        glb_aObjCCData[i].DateTime = '';
        glb_aObjCCData[i].Contato1 = '';
        glb_aObjCCData[i].Contato2 = '';
        glb_aObjCCData[i].Contato3 = '';
    
        glb_aObjCCData[i].dataValid = false;
    }    
}

/********************************************************************
Retorna numero de elementos do array, em funcao de parametros.

Parametro:
sCountMode  - null ou 'A' numero de elementos existentes
            - 'F' numero de elementos preenchidos
            - 'E' numero de elementos vazios

Retorna -1 se parametro invalido
********************************************************************/
function numElemsInCCArray(sCountMode)
{
    var retVal = -1;
    var i, qtyFs;
    
    if ( sCountMode == null )
         sCountMode = 'A';
         
    sCountMode = sCountMode.toUpperCase();
    
    qtyFs = 0;
    for (i=0; i<glb_aObjCCData.length; i++)
        (glb_aObjCCData[i].dataValid) ? qtyFs++ : null;
    
    if ( sCountMode == 'A' )
    {
        retVal = glb_aObjCCData.length;
    }
    else if ( sCountMode == 'F' )
    {
        retVal = qtyFs;
    }
    else if ( sCountMode == 'E' )
    {
        retVal = glb_aObjCCData.length - qtyFs;
    }
    
    return retVal;
}

/********************************************************************
Verifica se uma ligacao existe no array de objetos de dados de chamadas.

Parametro:
nInternalCallID - o internal call ID da ligacao

Retorna a posicao no array se existe, caso contrario retorna -1
********************************************************************/
function findCallInCCArray(nInternalCallID)
{
    var i;
    
    for ( i=0; i<glb_aObjCCData.length; i++ )
    {
        if ( glb_aObjCCData[i].InternalCallID == nInternalCallID )
            return i;
    }
    
    return -1;
}

// OBJETO DADOS LIGACOES ATIVAS =====================================

function __objChamadaData()
{
    var CallStateID = null;
    var CallState = null;
    var InternalCallID = null;
    
    var CallStateToID = null;
    var CallCauseID = null;
    var Key = 0;
    
    var CBCount = 0;
    
    var DDI = '';
    var DDD = '';
    var Numero = '';
    var ClienteID = 0;
    var Fantasia = '';
    var Classificacao = '';
    var Cidade = '';
    var UF = '';
    var DateTime = '';
    var Contato1 = '';
    var Contato2 = '';
    var Contato3 = '';
    
    var dataValid = false;
}

var glb_aObjCCData = new Array();

// FINAL DE OBJETO DADOS LIGACOES ATIVAS ============================
// TESTE DE ALTERA��O SVN 