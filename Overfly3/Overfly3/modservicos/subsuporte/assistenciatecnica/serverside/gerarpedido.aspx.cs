using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modservicos.subsuporte.assistenciatecnica.serverside
{
	public partial class gerarpedido : System.Web.UI.OverflyPage
	{
		protected Integer pedidoID;
		protected Integer novoPedItemID;

		protected override void PageLoad(object sender, EventArgs e)
		{
			PedidoGerador();
			PedidoItemGerador();
			PedidoParcelas();

			WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
		}

		protected Integer userID = Constants.INT_ZERO;
		protected Integer nUserID
		{
			get { return userID; }
			set { if(value != null) userID = value; }
		}
		
		protected Integer depositoID = Constants.INT_ZERO;
		protected Integer nDepositoID
		{
			get { return depositoID; }
			set { if(value != null) depositoID = value; }
		}

		protected Integer empresaID = Constants.INT_ZERO;
		protected Integer nEmpresaID
		{
			get { return empresaID; }
			set { if(value != null) empresaID = value; }
		}

		protected Integer �Cliente = Constants.INT_ZERO;
		protected Integer nEhCliente
		{
			get { return �Cliente; }
			set { if(value != null) �Cliente = value; }
		}

		protected Integer pessoaID = Constants.INT_ZERO;
		protected Integer nPessoaID
		{
			get { return pessoaID; }
			set { if(value != null) pessoaID = value; }
		}

		protected Integer transacaoID = Constants.INT_ZERO;
		protected Integer nTransacaoID
		{
			get { return transacaoID; }
			set { if(value != null) transacaoID = value; }
		}

		protected Integer financiamentoID = Constants.INT_ZERO;
		protected Integer nFinanciamentoID
		{
			get { return financiamentoID; }
			set { if (value != null) financiamentoID = value; }
		}

		protected Integer[] produtoID = Constants.INT_EMPTY_SET;
		protected Integer[] nProdutoID
		{
			get { return produtoID; }
			set { if(value != null) produtoID = value; }
		}

        protected Integer[] pedItemReferencia = Constants.INT_EMPTY_SET;
        protected Integer[] nPedItemReferencia
		{
            get { return pedItemReferencia; }
            set { if (value != null) pedItemReferencia = value; }
		}

		protected Integer[] pedItemEncomendaID = Constants.INT_EMPTY_SET;
		protected Integer[] nPedItemEncomendaID
		{
			get { return pedItemEncomendaID; }
			set { if(value != null) pedItemEncomendaID = value; }
		}
		
		protected Integer pedidoGeradoID = Constants.INT_ZERO;
		protected Integer nPedidoGeradoID
		{
			get { return pedidoGeradoID; }
			set { if(value != null) pedidoGeradoID = value; }
		}
		
		protected Integer ultimo = Constants.INT_ZERO;
		protected Integer bUltimo
		{
			get { return ultimo; }
			set { if (value != null) ultimo = value; }
		}
		
		protected void PedidoGerador()
		{
			ProcedureParameters[] param = new ProcedureParameters[20];
			
			if(pedidoGeradoID.intValue() == 0)
			{
				param = new ProcedureParameters[23];
				
				param[0] = new ProcedureParameters("@EmpresaID",SqlDbType.Int, empresaID.intValue());
				param[1] = new ProcedureParameters("@PessoaID",SqlDbType.Int, pessoaID.intValue());
				param[2] = new ProcedureParameters("@ParceiroID", SqlDbType.Int, pessoaID.intValue());
				param[3] = new ProcedureParameters("@EhCliente",SqlDbType.Int, �Cliente.intValue());
				param[4] = new ProcedureParameters("@TransacaoID",SqlDbType.Int, transacaoID.intValue());
				param[5] = new ProcedureParameters("@Observacao",SqlDbType.VarChar, transacaoID.intValue());
				param[5].Length = 40;
				param[6] = new ProcedureParameters("@SeuPedido",SqlDbType.VarChar, DBNull.Value);
				param[6].Length = 30;
				param[7] = new ProcedureParameters("@OrigemPedidoID",SqlDbType.Int, DBNull.Value);
				param[8] = new ProcedureParameters("@FinanciamentoID",SqlDbType.Int, financiamentoID.intValue());
				param[9] = new ProcedureParameters("@dtPrevisaoEntrega",SqlDbType.DateTime, DBNull.Value);
				param[10] = new ProcedureParameters("@TransportadoraID",SqlDbType.Int, DBNull.Value);
				param[11] = new ProcedureParameters("@MeioTransporteID",SqlDbType.Int, DBNull.Value);
				param[12] = new ProcedureParameters("@ModalidadeTransporteID",SqlDbType.Int, DBNull.Value);
				param[13] = new ProcedureParameters("@Frete",SqlDbType.Bit, 0);
				param[14] = new ProcedureParameters("@DepositoID",SqlDbType.Int, depositoID.intValue());
				param[15] = new ProcedureParameters("@ProprietarioID",SqlDbType.Int, userID.intValue());
				param[16] = new ProcedureParameters("@AlternativoID",SqlDbType.Int, userID.intValue());
				param[17] = new ProcedureParameters("@Observacoes",SqlDbType.VarChar, DBNull.Value);
				param[17].Length = 8000;
                param[18] = new ProcedureParameters("@NumProjeto", SqlDbType.VarChar, DBNull.Value);
                param[18].Length = 20;
                param[19] = new ProcedureParameters("@ProgramaMarketingID",SqlDbType.Int, DBNull.Value);
                param[20] = new ProcedureParameters("@URLEtiqueta", SqlDbType.VarChar, DBNull.Value);   
				param[21] = new ProcedureParameters("@PedidoID",SqlDbType.Int, DBNull.Value,ParameterDirection.InputOutput);
				param[22] = new ProcedureParameters("@Resultado",SqlDbType.VarChar, ParameterDirection.InputOutput);
				param[22].Length = 100;
				
				DataInterfaceObj.execNonQueryProcedure("sp_Pedido_Gerador", param);
				
				pedidoID = !param[21].Data.Equals(DBNull.Value) ? 
					new Integer((int) param[21].Data) : 
					null;
			}
			else
			{
				pedidoID = nPedidoGeradoID;
			}
		}
		
		protected void PedidoItemGerador()
		{
			ProcedureParameters[] param = new ProcedureParameters[15];

            param[0] = new ProcedureParameters("@PedidoID", SqlDbType.Int, pedidoID);
            param[1] = new ProcedureParameters("@ProdutoID", SqlDbType.Int, DBNull.Value);
            param[2] = new ProcedureParameters("@ServicoID", SqlDbType.Int, DBNull.Value);
            param[3] = new ProcedureParameters("@Quantidade", SqlDbType.Int, DBNull.Value);
            param[4] = new ProcedureParameters("@ValorInterno", SqlDbType.Money, DBNull.Value);
            param[5] = new ProcedureParameters("@ValorICMSSTInterno", SqlDbType.Money, DBNull.Value);
            param[6] = new ProcedureParameters("@ValorRevenda", SqlDbType.Money, DBNull.Value);
            param[7] = new ProcedureParameters("@ValorICMSSTRevenda", SqlDbType.Money, DBNull.Value);
            param[8] = new ProcedureParameters("@ValorUnitario", SqlDbType.Money, DBNull.Value);
            param[9] = new ProcedureParameters("@FinalidadeID", SqlDbType.Int, DBNull.Value);
            param[10] = new ProcedureParameters("@CFOPID", SqlDbType.Int, DBNull.Value);
            param[11] = new ProcedureParameters("@PedItemReferenciaID", SqlDbType.Int, DBNull.Value);
            param[12] = new ProcedureParameters("@PedItemEncomendaID", SqlDbType.Int, DBNull.Value);
            param[13] = new ProcedureParameters("@TipoGerador", SqlDbType.Int, 2);
            param[14] = new ProcedureParameters("@NovoPedItemID", SqlDbType.Int, DBNull.Value,   ParameterDirection.InputOutput);

			for(int i = 0; i < produtoID.Length; i++)
			{
				param[1].Data = produtoID[i].intValue();
                param[2].Data = DBNull.Value;
                param[4].Data = DBNull.Value;
                param[5].Data = DBNull.Value;
                param[6].Data = DBNull.Value;
                param[7].Data = DBNull.Value;
				
				// Fornecedor
				if(�Cliente.intValue() == 0)
				{
					param[11].Data = DBNull.Value;
					param[12].Data = DBNull.Value;
				}
				// Cliente
				else
				{
					//param[10].Data = nPedItemReferencia[i].intValue();

                    if (nPedItemReferencia[i].intValue() != 0)
					{
						param[11].Data = nPedItemReferencia[i].intValue();
					}
					else
					{
                        param[11].Data = DBNull.Value;
                        param[12].Data = DBNull.Value;
					}
				}

				// Roda a procedure.
				DataInterfaceObj.execNonQueryProcedure("sp_PedidoItem_Gerador", param);

				novoPedItemID = !param[14].Data.Equals(DBNull.Value) ? 
					new Integer((int)param[14].Data) : 
					null;

				if(�Cliente.intValue() != 0 && novoPedItemID != null)
				{
					DataInterfaceObj.ExecuteSQLCommand(
						"UPDATE Pedidos_Itens SET EhEncomenda = 1 " +
						"WHERE PedItemID = " + novoPedItemID + " " +
							 "AND PedItemEncomendaID IS NOT NULL"
					);
				}
			}
		}
		
		protected void PedidoParcelas()
		{
			ProcedureParameters[] param = new ProcedureParameters[2];

			param[0] = new ProcedureParameters("@PedidoID", SqlDbType.Int, pedidoID);
			param[1] = new ProcedureParameters("@FormaPagamentoID", SqlDbType.Int, DBNull.Value);

			DataInterfaceObj.execNonQueryProcedure("sp_Pedido_Parcelas", param);
		}
		
		protected string SQL
		{
			get
			{
				string sql = "select " + pedidoID + " as PedidoID";
				
				return sql;
			}
		}
	}
}
