using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modservicos.subsuporte.assistenciatecnica.serverside
{
	public partial class currrelation : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
		}
		
		protected Integer empresaID = Constants.INT_ZERO;
		protected Integer nEmpresaID
		{
			get { return empresaID; }
			set { if(value != null) empresaID = value; }
		}
		    
		protected Integer registroID = Constants.INT_ZERO;
		protected Integer nRegistroID
		{
			get { return registroID; }
			set { if(value != null) registroID = value; }
		}

		protected Integer tipoPesquisa = Constants.INT_ZERO;
		protected Integer nTipoPesquisa
		{
			get { return tipoPesquisa; }
			set { if(value != null) tipoPesquisa = value; }
		}
		
		protected string SQL
		{
			get
			{
				string sql = "";
				
				if(tipoPesquisa.intValue() == 2)
				{
					sql = "SELECT TOP 1 NotaFiscalID AS fldID " +
                        "FROM Pedidos WITH(NOLOCK) " +
						"WHERE PedidoID = " + nRegistroID;
				}
				else if(tipoPesquisa.intValue() == 3)
				{
					sql = "SELECT TOP 1 RelacaoID AS fldID " +
                        "FROM RelacoesPesCon WITH(NOLOCK) " +
						"WHERE TipoRelacaoID=61 " +
						"AND SujeitoID = " + nEmpresaID + " " +
						"AND ObjetoID = " + nRegistroID;
				}
				
				return sql;
			}
		}
	}
}
