﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_asstec : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        private string mEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);
        private int formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string sFiltroEspecifico = Convert.ToString(HttpContext.Current.Request.Params["sFiltroEspecifico"]);
        private string chkCliente1 = Convert.ToString(HttpContext.Current.Request.Params["chkCliente1"]);
        private string chkCliente2 = Convert.ToString(HttpContext.Current.Request.Params["chkCliente2"]);
        private string chkFornecedor1 = Convert.ToString(HttpContext.Current.Request.Params["chkFornecedor1"]);
        private string chkFornecedor2 = Convert.ToString(HttpContext.Current.Request.Params["chkFornecedor2"]);
        private string sFiltroEstado = Convert.ToString(HttpContext.Current.Request.Params["sFiltroEstado"]);
        private string sFiltroFornecedor = Convert.ToString(HttpContext.Current.Request.Params["sFiltroFornecedor"]);
        private string sFiltroMarca = Convert.ToString(HttpContext.Current.Request.Params["sFiltroMarca"]);
        private string sFiltroProprietario = Convert.ToString(HttpContext.Current.Request.Params["sFiltroProprietario"]);
        private string sFiltroCliente = Convert.ToString(HttpContext.Current.Request.Params["sFiltroCliente"]);
        private string sFiltroVendedor = Convert.ToString(HttpContext.Current.Request.Params["sFiltroVendedor"]);
        private string sFiltroDtInicial = Convert.ToString(HttpContext.Current.Request.Params["sFiltroDtInicial"]);
        private string sFiltroDtFinal = Convert.ToString(HttpContext.Current.Request.Params["sFiltroDtFinal"]);
        private string sFiltro = Convert.ToString(HttpContext.Current.Request.Params["sFiltro"]);

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                if (sRelatorioID == "40175")
                {
                    relatorioAsstec();
                }

            }
        }

        public void relatorioAsstec()
        {
            // Excel
            int Formato = 2;
            string Title = "Relatório de Asstec";

            string strSQL = "EXEC sp_Asstec_Relatorio " + nEmpresaID + ", " + sFiltroEspecifico + ", " + chkCliente1 +" , " +
                            chkCliente2 +", " + chkFornecedor1 +", " + chkFornecedor2 +" , " + sFiltroEstado +
                          ", " + sFiltroFornecedor + ", " + sFiltroMarca + ", " + sFiltroProprietario + ", " + sFiltroCliente + ", " + sFiltroVendedor +
                          ", " + sFiltroDtInicial + ", " + sFiltroDtFinal + ", " + sFiltro; 

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "2.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "5.13834", "0.0", "11", "#0113a0", "B", Header_Body, "7.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "13.0", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }
    }
}
