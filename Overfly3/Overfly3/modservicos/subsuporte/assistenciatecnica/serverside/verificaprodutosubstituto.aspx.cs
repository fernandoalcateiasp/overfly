using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modservicos.subsuporte.assistenciatecnica.serverside
{
	public partial class verificaprodutosubstituto : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
		}
		
		protected Integer userID = Constants.INT_ZERO;
		protected Integer nUserID
		{
			get { return userID; }
			set { if(value != null) userID = value; }
		}

		protected Integer empresaID = Constants.INT_ZERO;
		protected Integer nEmpresaID
		{
			get { return empresaID; }
			set { if(value != null) empresaID = value; }
		}

		protected Integer[] produtoID = Constants.INT_EMPTY_SET;
		protected Integer[] nProdutoID
		{
			get { return produtoID; }
			set { if (value != null) produtoID = value; }
		}

		protected Integer[] linha = Constants.INT_EMPTY_SET;
		protected Integer[] nLinha
		{
			get { return linha; }
			set { if(value != null) linha = value; }
		}
		
		protected string SQL
		{
			get
			{
				string sql = "";
				string query = "";
				DataSet ds = null;

				for(int i = 0; i < nLinha.Length; i++)
				{
					query = "SELECT Produtos.ConceitoID " +
                        "FROM  RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK) " + 
						"WHERE ProdutosEmpresa.ObjetoID = " + nProdutoID[i] + " AND " + 
							"ProdutosEmpresa.SujeitoID = " + nEmpresaID + " AND " + 
							"ProdutosEmpresa.TipoRelacaoID = 61 AND " + 
							"ProdutosEmpresa.EstadoID <> 1 AND " + 
							"ProdutosEmpresa.EstadoID <> 5 AND " + 
							"ProdutosEmpresa.ObjetoID = Produtos.ConceitoID AND " + 
							"Produtos.EstadoID <> 1 AND " + 
							"Produtos.EstadoID <> 5 ";

					if(query.Length != 0)
					{
						ds = DataInterfaceObj.getRemoteData(query);

						if(ds.Tables[1].Rows.Count == 0)
						{
							if (sql.Length > 0)
							{
								sql += " UNION ";
							}

							sql += "select " +
								nProdutoID[i] + " as ProdutoID, " +
								nLinha[i] + " as Linha";
						}
					}
				}

				if (sql.Length == 0)
				{
					sql = "select NULL as ProdutoID, NULL as Linha";
				}

				return sql;
			}
		}
	}
}
