using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modservicos.subsuporte.assistenciatecnica.serverside
{
	public partial class fillcomboslupa : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
		}

		protected string comboID = Constants.STR_EMPTY;
		protected string sComboID
		{
			get { return comboID; }
			set { if(value != null) comboID = value; }
		}

		protected string param1 = Constants.STR_EMPTY;
		protected string sParam1
		{
			get { return param1; }
			set { if(value != null) param1 = value; }
		}

		protected string param2 = Constants.STR_EMPTY;
		protected string sParam2
		{
			get { return param2; }
			set { if(value != null) param2 = value; }
		}

		protected string numeroSerie = Constants.STR_EMPTY;
		protected string sNumeroSerie
		{
			get { return numeroSerie; }
			set { if(value != null) numeroSerie = value; }
		}
		
		protected string SQL
		{
			get
			{
				string sql = "";

				if (sComboID.Equals("selFornecedorID"))
				{
					sql = "SELECT Fornecedores.FornecedorID AS fldID, Pessoas.Fantasia AS fldName, " +
						"Fornecedores.Ordem AS Ordem, " +
						"dbo.fn_Asstec_Fornecedor( " + sParam2 + "," + sParam1 + ", '" + sNumeroSerie + "') AS FornecedorID " +
                        "FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK), Pessoas WITH(NOLOCK) " +
						"WHERE (ProdutosEmpresa.ObjetoID = " + sParam1 + " AND " +
						"ProdutosEmpresa.SujeitoID = " + sParam2 + " AND " +
						"ProdutosEmpresa.TipoRelacaoID = 61 AND ProdutosEmpresa.RelacaoID = Fornecedores.RelacaoID AND " +
						"Fornecedores.FornecedorID = Pessoas.PessoaID) " +
						"ORDER BY Ordem ";
				}
				else
				{
					sql = "SELECT NULL AS fldID, NULL AS fldName, NULL AS Ordem, NULL AS FornecedorID";
				}
					
				return sql;
			}
		}
	}
}
