using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modservicos.subsuporte.assistenciatecnica.serverside
{
	public partial class verificacaoasstec : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"SELECT dbo.fn_Asstec_Verifica( " + nAsstecID + ", " + nCurrEstadoID + ", " + nNewEstadoID + ") AS Verificacao"
			));
		}

		protected Integer asstecID = Constants.INT_ZERO;
		protected Integer nAsstecID
		{
			get { return asstecID; }
			set { if(value != null) asstecID = value; }
		}

		protected Integer currEstadoID = Constants.INT_ZERO;
		protected Integer nCurrEstadoID
		{
			get { return currEstadoID; }
			set { if(value != null) currEstadoID = value; }
		}

		protected Integer newEstadoID = Constants.INT_ZERO;
		protected Integer nNewEstadoID
		{
			get { return newEstadoID; }
			set { if(value != null) newEstadoID = value; }
		}
	}
}
