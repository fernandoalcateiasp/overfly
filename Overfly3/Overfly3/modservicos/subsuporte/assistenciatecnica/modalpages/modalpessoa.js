/********************************************************************
modalpessoa.js

Library javascript para o modalpessoa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// Variavel de timer
var modPessoa_TimerHandler = null;

var dsoPesq = new CDatatransport("dsoPesq");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
LISTA DAS FUNCOES

window_onload()
setupPage()
txtPessoa_onKeyPress()
txtPessoa_ondigit(ctl)
btnFindPesquisa_onclick(ctl)
btn_onclick(ctl)
startPesqPessoa(strPesquisa)
startPesqPessoa_DSC()

Eventos particulares de grid
fg_ModPessoaKeyPress(KeyAscii)
fg_ModPessoaDblClick()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modalpessoaBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    window.focus();
    if ( document.getElementById('txtPessoa').disabled == false )
        txtPessoa.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    if ( glb_nBtnClicked == 1 )
        secText('Selecionar Pessoa', 1);
    else if ( glb_nBtnClicked == 2 )
        secText('Selecionar Parceiro', 1);
    
    // ajusta elementos da janela
    var elem;

    // ajusta o divPessoa
    adjustElementsInForm([['lblChavePesquisa', 'selChavePesquisa', 12, 1, -10],
                          ['lblPessoa', 'txtPessoa', 31, 1, -3]],
                          null,null,true);
                          
    // btnFindPesquisa
    btnFindPesquisa.disabled = true;
    with (btnFindPesquisa.style)
    {
        top = parseInt(txtPessoa.style.top, 10);
        left = parseInt(txtPessoa.style.left, 10) + parseInt(txtPessoa.style.width, 10) + 2;
        width = 80;
        height = 24;
    }

    elem = window.document.getElementById('divPessoa');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height, 10);
        width = parseInt(btnFindPesquisa.currentStyle.left, 10) + parseInt( btnFindPesquisa.currentStyle.width, 10);
        height = parseInt(txtPessoa.currentStyle.top, 10) + parseInt( txtPessoa.currentStyle.height, 10);
    }

    var aOptions = [[0,'Nome'],
                     [1,'Fantasia'],
                     [2,'Documento']];

    for (i=0; i<aOptions.length; i++)
    {
        var oOption = document.createElement("OPTION");
        oOption.value = aOptions[i][0];
        oOption.text = aOptions[i][1];
        selChavePesquisa.add(oOption);
    }
        
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPessoa.currentStyle.top) + parseInt(divPessoa.currentStyle.height) + ELEM_GAP;
        width = parseInt(divPessoa.currentStyle.width);
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);

    headerGrid(fg,['Nome',
                   'ID',
                   'Fantasia',
                   'Documento',
                   'Cidade',
                   'UF',
                   'Pa�s'], [2]);
    
    fg.Redraw = 2;
    
    txtPessoa.onkeypress = txtPessoa_onKeyPress;
    txtPessoa.onkeyup = txtPessoa_ondigit;
}

/********************************************************************
Usuario digitou Enter no campo txtPessoa
********************************************************************/
function txtPessoa_onKeyPress()
{
    if ( event.keyCode == 13 )
    {
        lockControlsInModalWin(true);
        modPessoa_TimerHandler = window.setInterval('btnFindPesquisa_onclick()', 30, 'JavaScript');
    }        
}

/********************************************************************
Usuario levantou uma tecla pressionada no campo txtPessoa
********************************************************************/
function txtPessoa_ondigit(ctl)
{
    if ( ctl == null )
        ctl = this;
        
    changeBtnState(ctl.value);
}

/********************************************************************
Clique no botao de pesquisa para o grid
********************************************************************/
function btnFindPesquisa_onclick(ctl)
{
    if ( modPessoa_TimerHandler != null )
    {
        window.clearInterval(modPessoa_TimerHandler);
        modPessoa_TimerHandler = null;
    }
    
    txtPessoa.value = trimStr(txtPessoa.value);
    
    changeBtnState(txtPessoa.value);

    if (btnFindPesquisa.src == glb_LUPA_IMAGES[1].src)
        return;
    
    startPesqPessoa(txtPessoa.value);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , null );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

/********************************************************************
Requer dados ao servidor para montar o grid de pessoas
********************************************************************/
function startPesqPessoa(strPesquisa)
{
    lockControlsInModalWin(true);
    
    var strSQL = '';
    
    startPesqPessoa_DSC();

/**********************    
    setConnection(dsoPesq);

    dsoPesq.SQL = strSQL;
    dsoPesq.ondatasetcomplete = startPesqPessoa_DSC;
    dsoPesq.Refresh();
**********************/    
}

/********************************************************************
Retorno do servidor dos dados para montar o grid de pessoas
********************************************************************/
function startPesqPessoa_DSC() {
    startGridInterface(fg);
    
    fg.FrozenCols = 0;

    headerGrid(fg,['Nome',
                   'ID',
                   'Fantasia',
                   'Documento',
                   'Cidade',
                   'UF',
                   'Pa�s'], [2]);
                   
    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    fg.Redraw = 0;
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    fg.Redraw = 2;
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
    }    
    else
    {
        btnOK.disabled = true;    
        txtPessoa.focus();
    }    
}

// EVENTOS DE GRID PARTICULARES DESTA PAGINA ************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPessoaKeyPress(KeyAscii)
{
    // Enter
    if ( (KeyAscii == 13) && (fg.Row > 0) )
    {
        btn_onclick(btnOK);
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPessoaDblClick()
{
    // Se tem pessoa selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

// FINAL DE EVENTOS DE GRID PARTICULARES DESTA PAGINA ***************