/********************************************************************
modalgerarpedido.js

Library javascript para o modalgerarpedido.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Variavel de timer
var modPesqProd_TimerHandler = null;
// controla o retorno do servidor dos dados de combos dinamicos
var glb_CounterCmbsDynamics = 0;

// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_LocalClickedIsReadOnly = false;

// Variaveis controlam primeiro combo selecionado
var glb_cmbPessoaAsstecSel = null;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 720;
var glb_nPedidoGeradoID = 0;
var glb_nEmpresa = null;

var dsoPesq = new CDatatransport("dsoPesq");    
//  Dados dos combos dinamicos (selProdutoID .SQL 
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");
//  Dados dos combos dinamicos (selFornecedorID .SQL 
var dsoCmbDynamic02 = new CDatatransport("dsoCmbDynamic02");
//  Dados dos combos dinamicos (selTransacaoID .SQL 
var dsoCmbDynamic03 = new CDatatransport("dsoCmbDynamic03");
//  Dados dos combos dinamicos (selFinanciamentoID .SQL 
var dsoCmbDynamic04 = new CDatatransport("dsoCmbDynamic04");
//  Dados da pagina ASP que gera o pedido no servidor 
var dsoVerProdSubstituto = new CDatatransport("dsoVerProdSubstituto");    
//  Dados da pagina ASP que gera o pedido no servidor 
var dsoGerarPedido = new CDatatransport("dsoGerarPedido");    
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
LISTA DAS FUNCOES

window_onload()
setupPage()
selPessoaID_onchange()
selPrestadorID_onchange()
selTransacaoID_onchange()
selFinanciamentoID_onchange()
changeBtnFindPesquisaGif()
btnFindPessoa_onclick(ctl)
btnFindPesquisa_onclick(ctl)
btn_onclick(ctl)
startPesq()
dsopesq_DSC()
cleanGrid()
cleanSelsTransacaoAndFinanciamento()
invertChkBox()
startDynamicCmbs()
dsoCmbDynamic_DSC()
loadselTransacaoIDAndFinanciamento()
loadselTransacaoIDAndFinanciamento_DSC()
btnOK_Status()
verificaProdutoSubstituto()
verificaProdutoSubstituto_DSC()
gerarPedido()
gerarPedido_DSC()

Eventos particulares de grid
js_fg_modGerPed_KeyPressEdit (grid, Row, Col, KeyAscii)
js_fg_modGerPedBeforeMouseDown (grid, Button, Shift, X, Y, Cancel)
js_fg_modGerPedDblClick( grid, Row, Col)
js_fg_modGerPedCellChanged(grid, Row, Col)
js_fg_modGerPedAfterRowColChange (grid, OldRow, OldCol, NewRow, NewCol)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    glb_nEmpresa = getCurrEmpresaData();


    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modalgerarpedidoBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);


    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 49;
    modalFrame.style.left = 0;

    chkEhCliente_onclick();
   
    
    // coloca foco no campo apropriado
    window.focus();
    if ( document.getElementById('chkEhCliente').disabled == false )
        chkEhCliente.focus();
    else if ( document.getElementById('btnFindPessoa').disabled == false )
        btnFindPessoa.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Gerar Pedido', 1);

    var elem;
    var frameRect, modWidth, modHeight;
    var borderThickness = 6;
    
    // Ajusta elementos da janela
    
    // dimensoes da modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // ajusta o divProduto
    adjustElementsInForm([['lblEhCliente', 'chkEhCliente', 3, 1, -10],
                          ['lblDefeito', 'chkDefeito', 3, 1, -5],
                          ['lblProprio', 'chkProprio', 3, 1, -5],
                          ['lblPessoaID', 'selPessoaID', 20, 1, -6],
                          ['btnFindPessoa', 'btn', 21, 1, 8],
                          ['lblPrestadorID', 'selPrestadorID', 20, 1],
                          ['lblTransacao', 'selTransacaoID', 65, 2, -10],
                          ['lblFinanciamentoID', 'selFinanciamentoID', 15, 2]],
                          null,null,true);
                          
    elem = window.document.getElementById('divProduto');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height, 10);
        width = modWidth - (2 * ELEM_GAP) - borderThickness ;
        height = parseInt(selFinanciamentoID.currentStyle.top, 10) + parseInt( selFinanciamentoID.currentStyle.height, 10);
    }

    // btnFindPesquisa
    btnFindPesquisa.disabled = true;
    with (btnFindPesquisa.style)
    {
        top = parseInt(selPrestadorID.style.top, 10);
        left = parseInt(divProduto.currentStyle.width, 10) - parseInt(btnFindPesquisa.currentStyle.width, 10);
        width = 80;
        height = 24;
    }

    btnFindPessoa.src = glb_LUPA_IMAGES[0].src;
        
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divProduto.currentStyle.top) + parseInt(divProduto.currentStyle.height) + ELEM_GAP;
        width = parseInt(divProduto.currentStyle.width);
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 2) + 120;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);

    headerGrid(fg,['Produto',
                   'ID',
                   'N�mero S�rie',
                   'AsstecID',
                   'Data',
                   'Fornecedor',
                   'OK'], []);
    
    fg.Redraw = 2;
    
    lblEhCliente.onclick = invertChkBox;
    chkEhCliente.onclick =  chkEhCliente_onclick;
    
    selPessoaID.onchange = selPessoaID_onchange;
    selPrestadorID.onchange = selPrestadorID_onchange;
    
    setLabelOfControlEx(lblPessoaID, selPessoaID);
    setLabelOfControlEx(lblPrestadorID, selPrestadorID);
    
    selTransacaoID.onchange = selTransacaoID_onchange;
    selFinanciamentoID.onchange = selFinanciamentoID_onchange;
    
    // Campos entram desabilitados
    chkEhCliente.disabled = false;
    selPessoaID.disabled = true;
    selPrestadorID.disabled = true;
    selTransacaoID.disabled = true;
    selFinanciamentoID.disabled = true;
}

/********************************************************************
Troca do item selecionado no combo pessoas
********************************************************************/
function selPessoaID_onchange()
{
    setLabelOfControlEx(lblPessoaID, selPessoaID);

    cleanGrid();
    
    changeBtnFindPesquisaGif();

	// Cliente
	if (chkEhCliente.checked)
	{
		if ( selPessoaID.value != 0 )
		    loadselTransacaoIDAndFinanciamento();
		else
		{
		    selTransacaoID.disabled = true;
		    selFinanciamentoID.disabled = true;
		    clearComboEx(['selTransacaoID', 'selFinanciamentoID']);        
		}        
	}
	else
	{
	    if (this.value == 0)
	    {
	        if (glb_cmbPessoaAsstecSel == this.id)
	        {
	            glb_cmbPessoaAsstecSel = null;
	            startDynamicCmbs();    
	        }
	    }
	    else
	    {
            if (glb_cmbPessoaAsstecSel == this.id)
            {
		        selTransacaoID.disabled = true;
		        selFinanciamentoID.disabled = true;
		        clearComboEx(['selTransacaoID', 'selFinanciamentoID']);        
	            startDynamicCmbs();    
            }
            else if (glb_cmbPessoaAsstecSel == null)
            {
                glb_cmbPessoaAsstecSel = this.id;
	            startDynamicCmbs();    
            }
	    }
	}
}

/********************************************************************
Troca do item selecionado no combo asstec
********************************************************************/
function selPrestadorID_onchange()
{
    setLabelOfControlEx(lblPrestadorID, selPrestadorID);

    cleanGrid();
    changeBtnFindPesquisaGif();
    
	// Fornecedor
    if (selPrestadorID.value != 0)
    {
        if (glb_cmbPessoaAsstecSel == this.id)
        {
	        startDynamicCmbs();    
        }
        else if (glb_cmbPessoaAsstecSel == null)
        {
            glb_cmbPessoaAsstecSel = this.id;
	        startDynamicCmbs();    
        }
        else
	        loadselTransacaoIDAndFinanciamento();
    }
	else
	{
	    selTransacaoID.disabled = true;
	    selFinanciamentoID.disabled = true;
	    clearComboEx(['selTransacaoID', 'selFinanciamentoID']);        

	    if (glb_cmbPessoaAsstecSel == this.id)
	    {
	        glb_cmbPessoaAsstecSel = null;
	        startDynamicCmbs();    
	    }
	}        
}

/********************************************************************
Troca do item selecionado no combo Transacao
********************************************************************/
function selTransacaoID_onchange()
{
    
}

/********************************************************************
Troca do item selecionado no combo financiamento
********************************************************************/
function selFinanciamentoID_onchange()
{
    
}

/********************************************************************
Troca da imagem habilitado/desabilitado do botao de pesquisa de dados
para preenchimento do grid e do de lupa para o preenchimento do
combo de pessoas
********************************************************************/
function changeBtnFindPesquisaGif()
{
    var lock = false;
	
    // Fornecedor so libera botao de pesquisa se selecionado um Asstec
    if ((!chkEhCliente.checked) && (selPrestadorID.value == 0))
        lock = true;
    // Cliente so libera botao de pesquisa se selecionado uma Pessoa
    else if ( (chkEhCliente.checked) && (selPessoaID.value == 0) )
		lock = true;
    
    // Destrava
    if ( (!lock) && (selTransacaoID.options.length > 0) )
    {
		btnFindPesquisa.disabled = false;
    }
    else
    // Trava
    {
		btnFindPesquisa.disabled = true;
    }
}

/********************************************************************
Clique no botao de pesquisa de dados para o combo pessoas
********************************************************************/
function btnFindPessoa_onclick(ctl)
{
    if ( modPesqProd_TimerHandler != null )
    {
        window.clearInterval(modPesqProd_TimerHandler);
        modPesqProd_TimerHandler = null;
    }

    if (btnFindPessoa.src == glb_LUPA_IMAGES[1].src)
        return;

    glb_cmbPessoaAsstecSel = null;
        
    cleanGrid();
    cleanSelsTransacaoAndFinanciamento();
    
    startDynamicCmbs();    
}

/********************************************************************
Clique no botao de pesquisa de dados para o grid
********************************************************************/
function btnFindPesquisa_onclick(ctl)
{
    if ( modPesqProd_TimerHandler != null )
    {
        window.clearInterval(modPesqProd_TimerHandler);
        modPesqProd_TimerHandler = null;
    }

    if (btnFindPesquisa.disabled)
        return;
    
    startPesq();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    var _retMsg = null;
    
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);
    
    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        //sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null );\
        // sendJSMessage acima movido para a funcao gerarPedido_DSC()
		
		_retMsg = window.top.overflyGen.Confirm('Gerar Pedido?');
		
		if ( _retMsg == 0 )
		    return null;
		else if ( _retMsg == 1 )
		{
		    // Fornecedor
		    if ( !chkEhCliente.checked )
		    	gerarPedido();
		    else	
		    	verificaProdutoSubstituto();
		}
		else if ( _retMsg == 2 )
		{
		    lockControlsInModalWin(false);
		    return null;
		}
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
}

/********************************************************************
Pesquida de dados no servidor para preenchimento do grid
********************************************************************/
function startPesq()
{
    lockControlsInModalWin(true);

    var nPessoaID = 0;
    var nPrestadorID = 0;
    var strSelect1 = '';    
    var strSelect2 = '';    
    var strSelect3 = '';     
    var strWhere1 = '';    
    var strWhere2 = '';    
    var strWhere3 = '';    
    var strOrder = '';    
    var strSQL = '';
    var nCliente = 0;
    var nDefeito = 0;
    var nPessoaID = 0;
    var nEmpresaID = 0;
    var nOperacaoMaeID = 0;
    var nBaixaID = selTransacaoID.options(selTransacaoID.selectedIndex).getAttribute('BaixaID', 1);

    nPessoaID = selPessoaID.value;
    nPrestadorID = selPrestadorID.value;
  
  
    
    // Fornecedor
    if (!chkEhCliente.checked) 
    {
        nFornecedorID = ' AND d.PessoaID = ' + nPessoaID + ' ';
        nEmpresaID = ' AND a.EmpresaID = ' + glb_nEmpresaID + ' ';
        
        if (nPessoaID == 0)
            nFornecedorID = ' ';


	    strSelect1 = ' SELECT DISTINCT e.Conceito, a.ProdutoID, a.NumeroSerie, a.AsstecID, a.dtAsstec, d.Fantasia, 0 AS OK, SPACE(0) AS ProdutoSubtID,  SPACE(0) AS PedItemReferencia, SPACE(0) AS PedItemEncomendaID ' +
		              ' FROM    dbo.Asstec a WITH ( NOLOCK )' +
		              ' INNER JOIN dbo.RelacoesPesCon b WITH ( NOLOCK ) ON ((a.ProdutoID = b.ObjetoID) AND (a.EmpresaID = b.SujeitoID))' +
		              ' INNER JOIN dbo.RelacoesPesCon_Fornecedores c WITH (NOLOCK) ON ((b.RelacaoID = b.RelacaoID) AND (c.FornecedorID = a.FornecedorID))  ' +
		              ' INNER JOIN dbo.Pessoas d WITH (NOLOCK) ON (c.FornecedorID = d.PessoaID)' +
		              ' INNER JOIN dbo.Conceitos e WITH (NOLOCK) ON (a.ProdutoID = e.ConceitoID) ' +    
		              ' INNER JOIN RelacoesPessoas f WITH (NOLOCK) ON (d.PessoaID = f.SujeitoID)' +
		              ' INNER JOIN dbo.Pessoas g WITH (NOLOCK) ON (f.ObjetoID = g.PessoaID)';

	    strWhere1 = ' WHERE a.EstadoID = 65 AND d.EstadoID = 2 AND f.EstadoID = 2 AND g.EstadoID = 2 AND c.AsstecProprio = 1 AND b.TipoRelacaoID = 61' +
	                    nEmpresaID +
	                    nFornecedorID +
	                  ' AND ( a.Defeito = 1 ) AND ( a.Cliente = 1 ) AND ( a.Fornecedor = 0 )';

	}   
    // Cliente
	else 
	{
        nDefeito = ' AND a.Defeito = ' + (!chkDefeito.checked ? '0' : '1') + ' ';
        nCliente = ' AND a.Cliente = ' + (!chkEhCliente.checked ? '0' : '1') + ' ';
        nPessoaID = ' AND b.PessoaID = ' + nPessoaID + ' ';
        nEmpresaID = ' AND a.EmpresaID = ' + glb_nEmpresaID + ' ';
        nOperacaoMaeID = '  AND i.OperacaoMaeID = (SELECT OperacaoMaeID FROM dbo.Operacoes a WITH(NOLOCK) WHERE a.OperacaoID =' + selTransacaoID.value + nCliente + ' )';

        strSelect1 = 'SELECT DISTINCT d.Conceito, a.ProdutoID, a.NumeroSerie, a.AsstecID,a.dtAsstec, c.Fantasia, 0 AS OK,  SPACE(0) AS ProdutoSubtID, g.PedItemID AS PedItemReferencia, g.PedItemEncomendaID ' + 
		            ' FROM    dbo.Asstec a WITH ( NOLOCK )'+
		            ' INNER JOIN dbo.Asstec_Pessoas b WITH ( NOLOCK ) ON a.AsstecID = b.AsstecID'+
		            ' INNER JOIN Pessoas c WITH ( NOLOCK ) ON b.PessoaID = c.PessoaID'+
		            ' INNER JOIN dbo.Conceitos d WITH ( NOLOCK ) ON a.ProdutoID = d.ConceitoID'+
		            ' INNER JOIN dbo.NumerosSerie_Movimento e WITH ( NOLOCK ) ON a.AsstecID = e.AsstecID'+
		            ' INNER JOIN dbo.Pedidos f WITH ( NOLOCK ) ON e.PedidoID = f.PedidoID '+
		            ' INNER JOIN dbo.Pedidos_Itens g WITH ( NOLOCK ) ON f.PedidoID = g.PedidoID AND a.ProdutoID  = g.ProdutoID ' +
		            ' LEFT OUTER JOIN dbo.Pedidos_Itens h WITH ( NOLOCK ) ON g.PedItemReferenciaID = h.PedItemID'+
		            ' INNER JOIN dbo.Operacoes i WITH (NOLOCK) ON f.TransacaoID = i.OperacaoID';

		strWhere1 = ' WHERE a.EstadoID = 65 AND a.Proprio = ' + (!chkProprio.checked ? '0' : '1') + ' AND f.EhCliente = 1 AND f.TipoPedidoID = 601  AND f.EstadoID >= 30 ' +
		              nEmpresaID + nPessoaID + nCliente + nDefeito + nOperacaoMaeID;
		
		strSelect2 =' UNION ALL'+
		            ' SELECT DISTINCT  d.Conceito, a.ProdutoID, a.NumeroSerie, a.AsstecID,a.dtAsstec, c.Fantasia, 0 AS OK,  SPACE(0) AS ProdutoSubtID, g.PedItemID  AS PedItemReferencia, g.PedItemEncomendaID ' + 
		            ' FROM    dbo.Asstec a WITH ( NOLOCK )'+
		            ' INNER JOIN dbo.Asstec_Pessoas b WITH ( NOLOCK ) ON a.AsstecID = b.AsstecID'+
		            ' INNER JOIN Pessoas c WITH ( NOLOCK ) ON b.PessoaID = c.PessoaID'+
		            ' INNER JOIN dbo.Conceitos d WITH ( NOLOCK ) ON a.ProdutoID = d.ConceitoID'+
		            ' INNER JOIN dbo.Pedidos_ProdutosSeparados e WITH ( NOLOCK ) ON a.AsstecID = e.AsstecID'+
		            ' INNER JOIN dbo.Pedidos f WITH ( NOLOCK ) ON e.PedidoID = f.PedidoID '+
		            ' INNER JOIN dbo.Pedidos_Itens g WITH ( NOLOCK ) ON f.PedidoID = g.PedidoID AND a.ProdutoID  = g.ProdutoID ' +
		            ' LEFT OUTER JOIN dbo.Pedidos_Itens h WITH ( NOLOCK ) ON g.PedItemReferenciaID = h.PedItemID'+
		            ' INNER JOIN dbo.Operacoes i WITH (NOLOCK) ON f.TransacaoID = i.OperacaoID';

		strWhere2 = ' WHERE a.EstadoID = 65 AND a.Proprio = ' + (!chkProprio.checked ? '0' : '1') + ' AND f.EhCliente = 1 AND f.TipoPedidoID = 601  AND f.EstadoID >= 30 ' +
		              nEmpresaID + nPessoaID + nCliente + nDefeito + nOperacaoMaeID;
		
		strSelect3 =' UNION ALL'+
		            ' SELECT DISTINCT d.Conceito, a.ProdutoID, a.NumeroSerie, a.AsstecID,a.dtAsstec, c.Fantasia, 0 AS OK,  SPACE(0) AS ProdutoSubtID, g.PedItemID  AS PedItemReferencia, g.PedItemEncomendaID ' + 
		            ' FROM    dbo.Asstec a WITH ( NOLOCK )'+
		            ' INNER JOIN dbo.Asstec_Pessoas b WITH ( NOLOCK ) ON a.AsstecID = b.AsstecID'+
		            ' INNER JOIN Pessoas c WITH ( NOLOCK ) ON b.PessoaID = c.PessoaID'+
		            ' INNER JOIN dbo.Conceitos d WITH ( NOLOCK ) ON a.ProdutoID = d.ConceitoID'+
		            ' INNER JOIN dbo.Pedidos_ProdutosSeparados e WITH ( NOLOCK ) ON a.AsstecID = e.AsstecID'+
		            ' INNER JOIN dbo.Pedidos f WITH ( NOLOCK ) ON e.PedidoID = f.PedidoID '+
		            ' INNER JOIN dbo.Pedidos_Itens g WITH ( NOLOCK ) ON f.PedidoID = g.PedidoID AND a.ProdutoID  = g.ProdutoID ' +
		            ' LEFT OUTER JOIN dbo.Pedidos_Itens h WITH ( NOLOCK ) ON g.PedItemReferenciaID = h.PedItemID' +
		            ' INNER JOIN dbo.Operacoes i WITH (NOLOCK) ON f.TransacaoID = i.OperacaoID';

		strWhere3 = ' WHERE a.EstadoID = 65 AND a.Proprio = ' + (!chkProprio.checked ? '0' : '1') + ' AND f.EhCliente = 1 AND f.TipoPedidoID = 601  AND f.EstadoID >= 30 ' +
		              nEmpresaID + nPessoaID + nCliente + nDefeito + nOperacaoMaeID;

	}

    strSQL = strSelect1  + strWhere1 + 
				strSelect2 + strWhere2 + 
				strSelect3 + strWhere3 + 
				strOrder;

    setConnection(dsoPesq);

    dsoPesq.SQL = strSQL;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();

}

/********************************************************************
Retorno do servidor da funcao startPesq
********************************************************************/
function dsopesq_DSC() {
    var dTFormat = '';
    var aHiddenCol;
    var transacaoid = selTransacaoID.value;
        
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

	startGridInterface(fg);
    
    fg.FrozenCols = 0;

    if ((!chkEhCliente.checked) || (transacaoid = 516))
        aHiddenCol = [7, 8, 9];
    else
        aHiddenCol = [8, 9];		

    headerGrid(fg,['Produto',
                   'ID',
                   'N�mero S�rie',
                   'AsstecID',                  
                   'Data',
                   'Fornecedor',
                   'OK',
                   'Produto',
                   'PedItemReferencia',
                   'PedItemEncomendaID'], aHiddenCol);
    
    fillGridMask(fg,dsoPesq,['Conceito*',
                             'ProdutoID*',
                             'NumeroSerie*',
                             'AsstecID*',
                             'dtAsstec*',
                             'Fantasia*', 
                             'OK',
                             'ProdutoSubtID',
                             'PedItemReferencia',
                             'PedItemEncomendaID'],
                             ['','','','','','','','99999','',''],
                             ['','','','',dTFormat,'','','','','']);

    alignColsInGrid(fg, [1, 3]);
    
    fg.MergeCells = 2;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    
    fg.Editable = true;
    fg.FrozenCols = 1;
    fg.ColDataType(6) = 11;

    lockControlsInModalWin(false);

    fg.Redraw = 0; 
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    fg.Redraw = 2;
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        fg.Row = 1;
        window.focus();
        fg.focus();
    }    
    else
    {
        btnFindPessoa.focus();
    }    
        
    // writeInStatusBar('child', 'cellMode', 'XXXXXXXX');    
}

/********************************************************************
Remove as linhas do grid
********************************************************************/
function cleanGrid()
{
    if ( fg.Rows > 1 )
        fg.Rows = 1;
        
    btnOK.disabled = true;    
}

/********************************************************************
Limpa e trava os combos Transacao e financiamento
********************************************************************/
function cleanSelsTransacaoAndFinanciamento()
{
    selTransacaoID.disabled = true;
    selFinanciamentoID.disabled = true;
    clearComboEx(['selTransacaoID', 'selFinanciamentoID']);
}

/********************************************************************
Usuario clicou o check box EhCliente
********************************************************************/
function chkEhCliente_onclick()
{
	if ( this.checked )
	{
	    lblPrestadorID.style.visibility = 'hidden';
	    selPrestadorID.style.visibility = 'hidden';
	    lblDefeito.style.visibility = 'inherit';
	    chkDefeito.style.visibility = 'inherit';
	}
	else
	{
	    lblPrestadorID.style.visibility = 'inherit';
	    selPrestadorID.style.visibility = 'inherit';
	    lblDefeito.style.visibility = 'hidden';
	    chkDefeito.style.visibility = 'hidden';
	}
	
	btnFindPessoa_onclick(btnFindPessoa);	
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
	chkEhCliente_onclick();
}

/********************************************************************
Funcao do programador
Configura e dispara dsos para ober dados para combos de pessoa e
asstec no servidor
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{
    lockControlsInModalWin(true);

    var nPessoaID = 0;
    var nPrestadorID = 0;
    var strSelect1 = '';        
    var strFrom1 = '';        
    var strWhere1 = '';        
    var strOrder = '';    
    var strSQL = '';

    nPessoaID = selPessoaID.value;
    nPrestadorID = selPrestadorID.value;
    nDefeito = (!chkDefeito.checked ? '0' : '1');
    nCliente = (!chkEhCliente.checked ? '0' : '1');
    
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selPessoaID e selPrestadorID)
	
	// Fornecedor carrega combo de Pessoa e Asstec
	if( !chkEhCliente.checked )
	{
	    if ( glb_cmbPessoaAsstecSel == null )
		    glb_CounterCmbsDynamics = 2;
		else    
		    glb_CounterCmbsDynamics = 1;
	}	
	// Cliente so carrega combo de Pessoa	
	else		
		glb_CounterCmbsDynamics = 1;

if ((glb_cmbPessoaAsstecSel == null) || (glb_cmbPessoaAsstecSel == 'selPrestadorID'))

    {
        // parametrizacao do dso dsoCmbDynamic01 (designado para selPessoaID)
        setConnection(dsoCmbDynamic01);
        
        var strTempTable1 = 'SET NOCOUNT ON CREATE TABLE #TempTable (fldID INT, fldName VARCHAR(40), AsstecID INT) ';
        
        var strInsertTable1 = '';
        
        // Combo de Pessoa - Fornecedor
        if ( !chkEhCliente.checked )
	    {	
	        strInsertTable1 = 'INSERT INTO #TempTable ' +
                        'SELECT DISTINCT Pessoas.PessoaID AS fldID, Pessoas.Fantasia AS fldName, Asstec.AsstecID ' +	    	            
    	                    'FROM Asstec WITH(NOLOCK) ' +
    	                            'INNER JOIN Pessoas WITH(NOLOCK) ON Asstec.FornecedorID = Pessoas.PessoaID ' +
    	                    'WHERE Asstec.EmpresaID = ' + glb_nEmpresaID + ' AND Asstec.EstadoID = 65 ';

            // Carrega o combo de Pessoa em funcao do Asstec selecionado
	        if (glb_cmbPessoaAsstecSel == 'selPrestadorID')
            {
                strInsertTable1 = 'INSERT INTO #TempTable ' +
                        'SELECT DISTINCT Pessoas.PessoaID AS fldID, Pessoas.Fantasia AS fldName, Asstec.AsstecID ' +	    	            
    	                    'FROM Asstec WITH(NOLOCK) ' +	    	                            
    	                            'INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON ProdutosEmpresa.ObjetoID = Asstec.ProdutoID ' + 
    	                            'INNER JOIN RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK) ON ProdutosEmpresa.RelacaoID = Fornecedores.RelacaoID ' +
    	                                                                                                'AND Asstec.FornecedorID = Fornecedores.FornecedorID ' +	    	                                                                                                
                                    'INNER JOIN Pessoas WITH(NOLOCK) ON Asstec.FornecedorID = Pessoas.PessoaID ' +
    	                    'WHERE Asstec.EmpresaID = ' + glb_nEmpresaID + ' ' +
    	                            'AND Asstec.EstadoID = 65 ' +
    	                             'AND Asstec.Cliente = ' + nCliente + ' ' +
                                    'AND Asstec.Defeito = ' + nDefeito + ' ' +
    	                            'AND ProdutosEmpresa.SujeitoID = ' + glb_nEmpresaID + ' ' +
    	                            'AND ProdutosEmpresa.TipoRelacaoID = 61 ' +
                                    'AND Fornecedores.AsstecProprio = 1 ' +
			                        'AND Pessoas.EstadoID = 2 ' +
			                        'AND Pessoas.PessoaID = ' + nPrestadorID + ' ' +
		                        
		                'UNION ALL ' +
		        				
		        		'SELECT DISTINCT Pessoas.PessoaID AS fldID, Pessoas.Fantasia AS fldName, Asstec.AsstecID ' +
	    	                'FROM Asstec WITH(NOLOCK) ' + 
	    	                        'INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON ProdutosEmpresa.ObjetoID = Asstec.ProdutoID ' +
	    	                        'INNER JOIN RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK) ON ProdutosEmpresa.RelacaoID = Fornecedores.RelacaoID ' +
                                                                                            'AND Asstec.FornecedorID = Fornecedores.FornecedorID ' +
				                    'INNER JOIN Pessoas WITH(NOLOCK) ON Fornecedores.FornecedorID = Pessoas.PessoaID ' +
				                    'INNER JOIN RelacoesPessoas RelAsstec WITH(NOLOCK) ON Pessoas.PessoaID = RelAsstec.SujeitoID ' +
				                    'INNER JOIN Pessoas Asstecs WITH(NOLOCK) ON RelAsstec.ObjetoID = Asstecs.PessoaID ' +
                            'WHERE Asstec.EmpresaID = ' + glb_nEmpresaID + ' ' +
                                    'AND Asstec.EstadoID = 65 ' +
                                    'AND Asstec.Cliente = ' + nCliente + ' ' +
                                    'AND Asstec.Defeito = ' + nDefeito + ' ' +
                                    'AND ProdutosEmpresa.SujeitoID = ' + glb_nEmpresaID + ' ' +
                                    'AND ProdutosEmpresa.TipoRelacaoID = 61 ' +
				                    'AND Fornecedores.AsstecTerceirizado = 1 ' + 
				                    'AND Pessoas.EstadoID =  2 ' +
				                    'AND RelAsstec.TipoRelacaoID = 22 ' +
				                    'AND RelAsstec.EstadoID = 2 ' +
				                    'AND Asstecs.EstadoID = 2 ' +
				                    'AND Asstecs.PessoaID = ' + nPrestadorID + ' ';
            }           
            
	    
	        var strSelectTable1 = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
        				            'UNION ALL ' +
        				        'SELECT DISTINCT fldID, fldName FROM #TempTable ORDER BY fldName ';
    	    
            var strDropTable1 = 'DROP TABLE #TempTable SET NOCOUNT OFF ';
            
            strSQL = strTempTable1 + strInsertTable1 + strSelectTable1 + strDropTable1;
            
	    }
	    // Cliente
	    else
	    {
	    	// Pessoa
	    	strSelect1 = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
        				    'UNION ALL ' +
        		            'SELECT DISTINCT Clientes.PessoaID AS fldID, Clientes.Fantasia AS fldName ';
	    	            
	    	strFrom1 = 'FROM Asstec WITH(NOLOCK), Asstec_Pessoas WITH(NOLOCK), Pessoas Clientes WITH(NOLOCK) ';

	    	strWhere1 = 'WHERE ' +
	    		'(Asstec.EmpresaID = ' + glb_nEmpresaID + ' AND Asstec.EstadoID = 65 AND ' +
	    		'(Asstec.Proprio = 0) AND ' +
	    		'(Asstec.AsstecID = Asstec_Pessoas.AsstecID AND Asstec_Pessoas.EhCliente = 1 AND ' +
	    		'Asstec_Pessoas.PessoaID = Clientes.PessoaID)) ';
            strOrder = 'ORDER BY fldName ';	    	
            
            strSQL = strSelect1 + strFrom1 + strWhere1 + strOrder;
	    }
	    	
	    dsoCmbDynamic01.SQL = strSQL;
        dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
        dsoCmbDynamic01.Refresh();
    }

	// Combo de Asstec - Fornecedor
	if ( (!chkEhCliente.checked) && ( (glb_cmbPessoaAsstecSel == null) || (glb_cmbPessoaAsstecSel == 'selPessoaID' ) ) )
    {
        // parametrizacao do dso dsoCmbDynamic02 (designado para selPrestadorID)
		setConnection(dsoCmbDynamic02);
    
		// Asstec Independente
		
		// Carrega o combo de Asstec em funcao da Pessoa selecionada		
		var sPessoaID = (glb_cmbPessoaAsstecSel == 'selPessoaID' ? ' AND Pessoas.PessoaID = ' + nPessoaID + ' ' : '');
		
		var strTempTable = 'SET NOCOUNT ON CREATE TABLE #Pessoas (fldID INT, fldName VARCHAR(40), AsstecID INT)';
		
		var strInsert = 'INSERT INTO #Pessoas ' + 
		            'SELECT DISTINCT Pessoas.PessoaID AS fldID, Pessoas.Fantasia AS fldName, Asstec.AsstecID ' +
		                'FROM Asstec WITH(NOLOCK) ' +
		                    'INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON ProdutosEmpresa.ObjetoID = Asstec.ProdutoID ' +
		                    'INNER JOIN RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK) ON ProdutosEmpresa.RelacaoID = Fornecedores.RelacaoID AND Asstec.FornecedorID = Fornecedores.FornecedorID ' + 
		                    'INNER JOIN Pessoas WITH(NOLOCK) ON Fornecedores.FornecedorID = Pessoas.PessoaID ' +
                        'WHERE Asstec.EmpresaID = ' + glb_nEmpresaID + ' ' + 
                            'AND ProdutosEmpresa.SujeitoID = ' + glb_nEmpresaID + ' ' +
						    'AND Asstec.EstadoID = 65 AND Asstec.Fornecedor = 0 AND Asstec.Defeito= 1 ' +
		                    'AND ProdutosEmpresa.TipoRelacaoID = 61 ' +		                
		                    'AND Fornecedores.AsstecProprio = 1 ' + 
		                    'AND Pessoas.EstadoID = 2 ' + sPessoaID + ' ' +
		            'UNION ALL ' +
                    'SELECT DISTINCT Asstecs.PessoaID AS fldID, Asstecs.Fantasia AS fldName, Asstec.AsstecID ' +
		                'FROM Asstec WITH(NOLOCK) ' + 
		                    'INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON ProdutosEmpresa.ObjetoID = Asstec.ProdutoID ' + 
                            'INNER JOIN RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK) ON ProdutosEmpresa.RelacaoID = Fornecedores.RelacaoID AND Asstec.FornecedorID = Fornecedores.FornecedorID ' +
		                    'INNER JOIN Pessoas WITH(NOLOCK) ON Fornecedores.FornecedorID = Pessoas.PessoaID ' + 
                            'INNER JOIN RelacoesPessoas RelAsstec WITH(NOLOCK) ON Pessoas.PessoaID = RelAsstec.SujeitoID ' +
                            'INNER JOIN Pessoas Asstecs WITH(NOLOCK) ON RelAsstec.ObjetoID = Asstecs.PessoaID ' +
		                'WHERE Asstec.EmpresaID = ' + glb_nEmpresaID + ' ' +
		                    'AND ProdutosEmpresa.SujeitoID = ' + glb_nEmpresaID + ' ' +
		                    'AND Asstec.EstadoID = 65 AND Asstec.Fornecedor = 0 AND Asstec.Defeito= 1 ' +
		                    'AND ProdutosEmpresa.TipoRelacaoID = 61 ' +
		                    'AND Fornecedores.AsstecTerceirizado = 1 ' +
		                    'AND Pessoas.EstadoID = 2 ' +
		                    'AND RelAsstec.TipoRelacaoID = 22 ' +
		                    'AND RelAsstec.EstadoID = 2 ' +
		                    'AND Asstecs.EstadoID = 2 ' + sPessoaID;
    
        /*var strUpdateTempTable = ' UPDATE #Pessoas SET EstoqueID = dbo.fn_Asstec_Estoque(AsstecID) ';*/
        
        var strSelectTemtable = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
						            'UNION ALL ' +
						        'SELECT DISTINCT fldID, fldName ' +
						            'FROM #Pessoas ' +
						           // 'WHERE EstoqueID IN (343,348) ' + 
						            'ORDER BY fldName ';

        var strDropTempTable = 'DROP TABLE #Pessoas SET NOCOUNT OFF ';
						    
        strSQL = strTempTable + strInsert +  strSelectTemtable + strDropTempTable;
        
		dsoCmbDynamic02.SQL = strSQL;

		dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
		dsoCmbDynamic02.Refresh();
	}
}

/********************************************************************
Funcao disparada pelo programador.
Funcao do ondatasetcomplete disparada pela funcao startDynamicCmbs()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics;
    var aDSOsDunamics;

	// Fornecedor carrega combo de Pessoa e Asstec
	if( !chkEhCliente.checked )
	{
        if (glb_cmbPessoaAsstecSel == 'selPessoaID')
        {
            aCmbsDynamics = [selPrestadorID];
		    aDSOsDunamics = [dsoCmbDynamic02];
		    clearComboEx(['selPrestadorID']);
        }
        else if (glb_cmbPessoaAsstecSel == 'selPrestadorID')
        {
		    aCmbsDynamics = [selPessoaID];
		    aDSOsDunamics = [dsoCmbDynamic01];
            clearComboEx(['selPessoaID']);
        }
        else
        {
            aCmbsDynamics = [selPessoaID, selPrestadorID];
		    aDSOsDunamics = [dsoCmbDynamic01, dsoCmbDynamic02];
		    clearComboEx(['selPessoaID', 'selPrestadorID']);

        }
	}
	// Cliente so carrega combo de Pessoa	
	else
	{
		aCmbsDynamics = [selPessoaID];
		aDSOsDunamics = [dsoCmbDynamic01];
		clearComboEx(['selPessoaID', 'selPrestadorID']);
	}

	setLabelOfControlEx(lblPrestadorID, selPrestadorID);
    setLabelOfControlEx(lblPessoaID, selPessoaID);

    // Inicia o carregamento de combos dinamicos (selPessoaID
    // e selPrestadorID)
    glb_CounterCmbsDynamics --;
    
    if (glb_CounterCmbsDynamics == 0)
    {
        for (i=0; i<aCmbsDynamics.length; i++)
        {
            // So preenche os combos se  vem mais de um registro
            // pois se vem um registro so este e a opcao em
            // branco do combo
            if ( !(aDSOsDunamics[i].recordset.BOF && aDSOsDunamics[i].recordset.BOF) )
            {
                aDSOsDunamics[i].recordset.MoveLast();
                aDSOsDunamics[i].recordset.MoveFirst();
                if ( aDSOsDunamics[i].recordset.RecordCount() > 1 )
                {
					recordset_fldName = aDSOsDunamics[i].recordset['fldName'];
					recordset_fldID = aDSOsDunamics[i].recordset['fldID'];
                
                    while (! aDSOsDunamics[i].recordset.EOF )
                    {
						var oOption = document.createElement("OPTION");
                        oOption.text = recordset_fldName.value;
                        oOption.value = recordset_fldID.value;
                        aCmbsDynamics[i].add(oOption);
                        aDSOsDunamics[i].recordset.MoveNext();
                    }
                    
                    recordset_fldName = null;
					recordset_fldID = null;
                }
            }    
        }
 	}
 	
 	lockControlsInModalWin(false);
    window.focus();
 	
 	// trava/destrava os combos de pessoas e asstec se tem dados
 	// seleciona o botao btnFindPessoa
 	if ( selPessoaID.options.length == 0 )
 	{
 	    selPessoaID.disabled = true;
 	    
 	}    
    else
    {
 	    selPessoaID.disabled = false;
 	    // seleciona o botao btnFindPessoa
        btnFindPessoa.focus();
    }

    if (selPrestadorID.options.length == 0)
        selPrestadorID.disabled = true;
 	else
 	    selPrestadorID.disabled = false;
 	    
    changeBtnFindPesquisaGif();

    if ((selPrestadorID.value != 0) && (glb_cmbPessoaAsstecSel == 'selPrestadorID'))
        loadselTransacaoIDAndFinanciamento();
}

/********************************************************************
Funcao do programador
Configura e dispara dsos para ober dados para combos de Transacao e
financiamento no servidor
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function loadselTransacaoIDAndFinanciamento()
{
    var nPrestadorID;
    var nPessoaID;
    var nPessoaAsstecID;
    var nParceiroID;
    var nCliente;
    var sFiltroEhCliente;
	var nSujeitoID;
	var nObjetoID;
	var strSQLSelect = '';
	var strSQLFrom = '';
	var strSQLWhere = '';
	var strSQLOrderBy = '';

    lockControlsInModalWin(true);

    // controla o retorno do servidor dos dados de combos dinamicos
    // (selTransacaoID e selFinanciamentoID)
    glb_CounterCmbsDynamics = 2;

    // parametrizacao do dso dsoCmbDynamic03 (designado para selTransacaoID)
    setConnection(dsoCmbDynamic03);

    nPrestadorID = selPrestadorID.value;
    nPessoaID = selPessoaID.value;
    
    // Fornecedor
    if (!chkEhCliente.checked)
	{
	    nPessoaAsstecID = nPrestadorID;
	    sFiltroEhCliente = ' AND a.Fornecedor = 1 ';
	}
	// Cliente
	else		
	{
		nPessoaAsstecID = nPessoaID;
        sFiltroEhCliente = ' AND a.Cliente = 1 ';
    }

    strSQLSelect = 'SELECT ' +
			'a.OperacaoID AS fldID, ' +
			'LTRIM(RTRIM(CONVERT(VARCHAR(10), a.OperacaoID))) + SPACE(1) + a.Operacao AS fldName, ' +
			'a.TransacaoBaixaID AS BaixaID ';
	
	strSQLFrom = 'FROM Operacoes a WITH(NOLOCK) ';

	strSQLWhere = 'WHERE ' +
			'(a.TipoOperacaoID = 652 AND a.EstadoID = 2 AND a.Nivel = 3 AND a.Asstec = 1 AND ' +
			'a.Entrada = 0 ' + sFiltroEhCliente + ') ' ;

	strSQLOrderBy = 'ORDER BY ISNULL(a.Ordem, 999), a.OperacaoID ';

	dsoCmbDynamic03.SQL = strSQLSelect + strSQLFrom + strSQLWhere + strSQLOrderBy;
    
    dsoCmbDynamic03.ondatasetcomplete = loadselTransacaoIDAndFinanciamento_DSC;
    dsoCmbDynamic03.Refresh();
    
    // parametrizacao do dso dsoCmbDynamic04 (designado para selFinanciamentoID)
    setConnection(dsoCmbDynamic04);
    
	// Fornecedor
    if ( !chkEhCliente.checked )
	{
        nSujeitoID = glb_nEmpresaID;
        nObjetoID = nPrestadorID;
        nParceiroID = nPrestadorID;
        nCliente = 0;
	}
	// Cliente
	else
	{
        nSujeitoID = nPessoaID;
        nObjetoID = glb_nEmpresaID;
        nParceiroID = nPessoaID;
        nCliente = 1;
	}
	
    strSQLSelect = 'SELECT ' +
			'Financiamentos.FinanciamentoID AS fldID, Financiamentos.Financiamento AS fldName, ' +
			'FinancPadrao.FinanciamentoID AS Padrao ';
	
	strSQLFrom = 'FROM ' +
			'Pessoas WITH(NOLOCK), Pessoas_Creditos PesCredito WITH(NOLOCK) RelacoesPessoas RelPessoas WITH(NOLOCK), FinanciamentosPadrao FinancPadrao WITH(NOLOCK), ' +
			'FinanciamentosPadrao FinancLimite WITH(NOLOCK), FinanciamentosPadrao Financiamentos WITH(NOLOCK) ';

	strSQLWhere = 'WHERE ' +
        '(Pessoas.PessoaID = ' + nSujeitoID + ' AND Pessoas.PessoaID = RelPessoas.SujeitoID AND PesCredito.PessoaID = Pessoas.PessoaID AND PesCredito.PaisID = ' + glb_nEmpresa[1] + ' ' +
			'RelPessoas.ObjetoID = ' + nObjetoID + ' AND RelPessoas.TipoRelacaoID = 21 AND ' +
			'FinancPadrao.FinanciamentoID = dbo.fn_RelacoesPessoas_FinanciamentoPadrao(' + nParceiroID + ', ' + glb_nEmpresaID + ', ' + nCliente + ') AND FinancPadrao.EstadoID = 2 AND ' +
			'PesCredito.FinanciamentoLimiteID = FinancLimite.FinanciamentoID AND ' +
			'Financiamentos.EstadoID = 2 AND Financiamentos.NumeroParcelas = 1 AND ' + 
			'(dbo.fn_Financiamento_PMP(Financiamentos.FinanciamentoID,NULL,NULL,NULL)) <= (dbo.fn_Financiamento_PMP(FinancLimite.FinanciamentoID,NULL,NULL,NULL))) ';
		
	strSQLOrderBy = 'ORDER BY Financiamentos.Ordem ';

	dsoCmbDynamic04.SQL = strSQLSelect + strSQLFrom + strSQLWhere + strSQLOrderBy;

    dsoCmbDynamic04.ondatasetcomplete = loadselTransacaoIDAndFinanciamento_DSC;
    dsoCmbDynamic04.Refresh();
}

/********************************************************************
Funcao disparada pelo programador.
Funcao do ondatasetcomplete disparada pela funcao
loadselTransacaoIDAndFinanciamento()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function loadselTransacaoIDAndFinanciamento_DSC() {
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selTransacaoID, selFinanciamentoID];
    var aDSOsDunamics = [dsoCmbDynamic03, dsoCmbDynamic04];

    // Inicia o carregamento de combos dinamicos (selTransacaoID
    // e selFinanciamentoID)

    clearComboEx(['selTransacaoID', 'selFinanciamentoID']);
    
    glb_CounterCmbsDynamics --;
    
    if (glb_CounterCmbsDynamics == 0)
    {
        for (i=0; i<aCmbsDynamics.length; i++)
        {
            while (! aDSOsDunamics[i].recordset.EOF )
            {
                optionStr = aDSOsDunamics[i].recordset['fldName'].value;
	        	optionValue = aDSOsDunamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                
                // Somente para o cmb de Transacao
                if (i==0)
					oOption.setAttribute('BaixaID', aDSOsDunamics[i].recordset['BaixaID'].value, 1);
                
                aCmbsDynamics[i].add(oOption);
                aDSOsDunamics[i].recordset.MoveNext();
            }
        }
 	}
 	
	changeBtnFindPesquisaGif();
 	lockControlsInModalWin(false);
    window.focus();
 	
 	// trava/destrava os combos de Transacao e financiamentos se tem dados
 	// seleciona o combo de asstec
 	if ( selTransacaoID.options.length == 0 )
 	    selTransacaoID.disabled = true;
    else
    {
 		dsoCmbDynamic03.recordset.MoveFirst();
 		
 		for ( i=0; i<selTransacaoID.options.length; i++ )
 		{
 			if (i == 1)
 			{
 				selTransacaoID.options.item(i).selected = true;
 				break;
 			}
 		}
 	    selTransacaoID.disabled = false;
    }    
 	
 	if ( selFinanciamentoID.options.length == 0 )
 	    selFinanciamentoID.disabled = true;
 	else
 	{
 		dsoCmbDynamic04.recordset.MoveFirst();
 		
 		// Seleciona opcao padrao chkEhCliente nao checado (se nao e cliente)
 		if ( chkEhCliente.checked )
 		    selFinanciamentoID.options.item(0).selected = true;
 		else    
 		{
 		    for ( i=0; i<selFinanciamentoID.options.length; i++ )
 		    {
 		    	if ( selFinanciamentoID.options.item(i).value == dsoCmbDynamic04.recordset['Padrao'].value )
 		    	{
 		    		selFinanciamentoID.options.item(i).selected = true;
 		    		break;
 		    	}
 		    }
 		}
 		    
 	    selFinanciamentoID.disabled = false;
 	}    

    // seleciona o combo de Asstec no caso de Fornecedor
	if (!chkEhCliente.checked)
	{
	    if (selPrestadorID.style.visibility == 'inherit')
	        selPrestadorID.focus();
	}
    // seleciona o combo de Pessoa no caso de Cliente
	else
	{
		if ( selPessoaID.style.visibility == 'inherit' )
			selPessoaID.focus();
	}
}

/********************************************************************
Habilita desabilita o botao OK
********************************************************************/
function btnOK_Status()
{
    btnOK.disabled = true;
    
    var i;
    var btnOKDisabled = true;
    
    // critica do grid
    for (i=1; i<fg.Rows; i++)
    {
        if ( fg.ValueMatrix(i,6) != 0 )
        {
            btnOKDisabled = false;
            break;
        }
    }
    
    // critica do selTransacaoID
    if ( selTransacaoID.value <= 0 )
        btnOKDisabled = true;
        
    // critica do selFinanciamentoID
    if ( selFinanciamentoID.value <= 0 )
        btnOKDisabled = true;
        
    btnOK.disabled = btnOKDisabled;
}

/********************************************************************
Chama pagina ASP para verificar os produtos substitutos no servidor
********************************************************************/
function verificaProdutoSubstituto()
{
	
    var i;
    var strPars = new String();
    
    strPars = '?';

    strPars += 'nUserID=';
    strPars += escape((window.top.userID).toString());

    strPars += '&nEmpresaID=';
    strPars += escape((glb_nEmpresaID).toString());
    
    for ( i=1; i<fg.Rows; i++ )
		fg.TextMatrix(i,7) = trimStr(fg.TextMatrix(i,7));
    
    for ( i=1; i<fg.Rows; i++ )
    {
        if ( (fg.ValueMatrix(i,6) != 0) && (fg.TextMatrix(i,7) != '') )
        {
			strPars += '&nProdutoID=';
			strPars += escape((fg.TextMatrix(i, 7)).toString());
        }
    }
    
    for ( i=1; i<fg.Rows; i++ )
    {
        if ( (fg.ValueMatrix(i,6) != 0) && (fg.TextMatrix(i,7) != '') )
        {
			strPars += '&nLinha=';
			strPars += escape((i).toString());
        }
    }
    
    dsoVerProdSubstituto.URL = SYS_ASPURLROOT + '/modservicos/subsuporte/assistenciatecnica/serverside/verificaprodutosubstituto.aspx' + strPars;
    dsoVerProdSubstituto.ondatasetcomplete = verificaProdutoSubstituto_DSC;
    dsoVerProdSubstituto.refresh();
}

/********************************************************************
Retorno do servidor da pagina ASP para verificar os produtos substitutos
********************************************************************/
function verificaProdutoSubstituto_DSC() {
    var nRetorno = null;
	var sProdErr = '';
	var nGridLine = 0;
	var _retMsg = null;
	
	if ( !(dsoVerProdSubstituto.recordset.BOF && dsoVerProdSubstituto.recordset.EOF) )
	{
		dsoVerProdSubstituto.recordset.MoveFirst();
		
		while ( !dsoVerProdSubstituto.recordset.EOF )			
		{
			if (dsoVerProdSubstituto.recordset["ProdutoID"].value != null)
			{
			    nGridLine = dsoVerProdSubstituto.recordset["Linha"].value;
			    sProdErr += (trimStr(fg.TextMatrix(nGridLine, 1)) + trimStr(fg.TextMatrix(nGridLine, 2)) + ' - ');
				sProdErr += dsoVerProdSubstituto.recordset["ProdutoID"].value + '\n';
			}	
			
			dsoVerProdSubstituto.recordset.MoveNext();
		}		
	}
	
	if ( sProdErr != '' )
	{
		lockControlsInModalWin(false);		
		
		_retMsg = window.top.overflyGen.Alert('Produto(s) inv�lido(s): \n' + sProdErr);
		    
		if ( _retMsg == 0 )
		    return null;
		        
		window.focus();
		fg.focus();        		
	}
	else
		gerarPedido();	

}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function gerarPedido()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;
	var bOk = 0;
	

	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
	glb_nPedidoGeradoID = 0;

    lockControlsInModalWin(true);

    for (i = 1; i < fg.Rows; i++) 
    {
       bOK = fg.textMatrix(i, getColIndexByColKey(fg, 'OK'));

       if (bOK != 0)
		{
			nBytesAcum=strPars.length;
			
			if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
			{
				nBytesAcum = 0;
				if (strPars != '')
				{
					glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
					strPars = '';
					nDataLen = 0;
				}
				
				strPars = '?';
				strPars += 'nUserID=' + escape(window.top.userID);
				strPars += '&nDepositoID=' + escape(glb_nEmpresaID);
				strPars += '&nEmpresaID=' + escape(glb_nEmpresaID);
				strPars += '&nEhCliente=' + (!chkEhCliente.checked ? '0' : '1');
				strPars += '&nPessoaID=' + (!chkEhCliente.checked ? selPrestadorID.value : selPessoaID.value);
				strPars += '&nTransacaoID=' + escape(selTransacaoID.value);
				strPars += '&nFinanciamentoID=' + escape(selFinanciamentoID.value);
			}
		
			nDataLen++;
			// Fornecedor
			if ( !chkEhCliente.checked )
			{
				strPars += '&nProdutoID=';
				strPars += escape((fg.ValueMatrix(i, 1)).toString());
			}
			// Cliente
			else 
			{
				if ( fg.TextMatrix(i,7) == '' )
				{
					strPars += '&nProdutoID=';
					strPars += escape((fg.ValueMatrix(i, 1)).toString());
				}
				else
				{
					strPars += '&nProdutoID=';
					strPars += escape((fg.ValueMatrix(i, 7)).toString());
				}

				strPars += '&nPedItemReferencia=';
				strPars += escape((fg.ValueMatrix(i, 8)).toString());
				
				strPars += '&nPedItemEncomendaID=';
				strPars += escape((fg.ValueMatrix(i, 9)).toString());
			}
		}	
	}
	
	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
		
	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGerarPedido.URL = SYS_ASPURLROOT + '/modservicos/subsuporte/assistenciatecnica/serverside/gerarpedido.aspx' + 
				glb_aSendDataToServer[glb_nPointToaSendDataToServer] + 
				'&bUltimo=' + escape(glb_nPointToaSendDataToServer == (glb_aSendDataToServer.length - 1) ? '1' : '0') +
				'&nPedidoGeradoID=' + escape(glb_nPedidoGeradoID);
				
			dsoGerarPedido.ondatasetcomplete = sendDataToServer_DSC;
			dsoGerarPedido.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_OcorrenciasTimerInt = window.setInterval('gerarPedido_DSC()', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );
	}
}

function sendDataToServer_DSC() {
    var nRetorno = null;
	var _retMsg = null;

	if (glb_nPedidoGeradoID==0)
	{
		if ( !(dsoGerarPedido.recordset.BOF && dsoGerarPedido.recordset.EOF) )
		{
			if (dsoGerarPedido.recordset["PedidoID"].value != null)
				glb_nPedidoGeradoID = dsoGerarPedido.recordset["PedidoID"].value;
			else
			{
				_retMsg = window.top.overflyGen.Alert('Pedido n�o foi gerado.');
				    
				if ( _retMsg == 0 )
					return null;
					
				sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
				return null;
			}
		}
		else
		{
			_retMsg = window.top.overflyGen.Alert('Pedido n�o foi gerado.');
				
			if ( _retMsg == 0 )
				return null;

			sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
			return null;
		}
	}

	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

/********************************************************************
Retorno da pagina ASP chamada para gerar o pedido no servidor,
pela funcao gerarPedido()
********************************************************************/
function gerarPedido_DSC() {
    if (glb_OcorrenciasTimerInt != null)
	{
		window.clearInterval(glb_OcorrenciasTimerInt);
		glb_OcorrenciasTimerInt = null;
	}
	
	var nRetorno = null;
	var _retMsg = null;
	
	if (glb_nPedidoGeradoID != 0)
	{
		_retMsg = window.top.overflyGen.Confirm('Gerado o Pedido: ' + 
			glb_nPedidoGeradoID + '\n' + 'Detalhar Pedido?');

		if ( _retMsg == 0 )
		    return null;
		else if ( _retMsg == 1 )
			nRetorno = glb_nPedidoGeradoID;
	}
	else
	{
		_retMsg = window.top.overflyGen.Alert('Pedido n�o foi gerado.');

		if ( _retMsg == 0 )
		    return null;
	}
	
    // retorna ao pesqlist
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , nRetorno );
}

// EVENTOS DE GRID PARTICULARES DESTA PAGINA ************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modGerPedAfterRowColChange (grid, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 0;
    
    if ( glb_totalCols__ != false )
        firstLine = 1;
        
    if (NewRow > firstLine)
    {
        grid.Row = NewRow;
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modGerPedBeforeMouseDown (grid, Button, Shift, X, Y, Cancel)
{
    if ( (X > grid.ColWidth(0)) && (X < (grid.ColWidth(0) + grid.ColWidth(1) + grid.ColWidth(2) + grid.ColWidth(3) + grid.ColWidth(4) + grid.ColWidth(5) + grid.ColWidth(6))) &&
         (Y < grid.RowHeight(0)) )
        glb_LocalClickedIsReadOnly = false;
    else    
        glb_LocalClickedIsReadOnly = true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modGerPedDblClick( grid, Row, Col)
{
    if ( glb_LocalClickedIsReadOnly == true )
        return true;
        
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    // trap (so coluna 6)
    if ( !(Col == 6) )
        return true;
        
    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;
    
    lockControlsInModalWin(true);
    
    glb_PassedOneInDblClick = true;
    
    // limpa coluna se tem um check box checado
    for ( i=1; i<grid.Rows; i++ )
    {
        if ( grid.ValueMatrix(i, Col) != 0 )
        {
            bFill = false;
            break;
        }
    }
    
    for ( i=1; i<grid.Rows; i++ )
    {
        if ( bFill )
        {
            grid.TextMatrix(i, Col) = 1;
        }    
        else
        {
            grid.TextMatrix(i, Col) = 0;
        }    
    }
    
    lockControlsInModalWin(false);
    
    grid.Editable = true;
    grid.Redraw = 2;
    window.focus();
    grid.focus();
    btnOK_Status();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modGerPedCellChanged(grid, Row, Col)
{
    if ( grid.Editable == false )
        return true;
    
    if ( !(Col == 6) )
        return true;

    window.focus();
    grid.focus();
    btnOK_Status();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modGerPed_KeyPressEdit (grid, Row, Col, KeyAscii)
{
    // O codigo abaixo so executa quando o grid e montado para
    // cliente
    if ( !chkEhCliente.checked )
        return true;
        
    if ( (KeyAscii == 13) && Col == 7 )
    {
        grid.TextMatrix(Row, 6) = 1;
    }
}

// FINAL DE EVENTOS DE GRID PARTICULARES DESTA PAGINA ***************