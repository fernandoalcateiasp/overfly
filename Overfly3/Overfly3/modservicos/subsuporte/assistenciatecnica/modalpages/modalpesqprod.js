/********************************************************************
modalpesqprod.js

Library javascript para o modalpesqprod.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Variavel de timer
var modPesqProd_TimerHandler = null;

// Variavel do modo do grid: 'L' = Listagem 'H' = Historico
var glb_gridMode = null; 

var dsoPesq = new CDatatransport("dsoPesq");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
LISTA DAS FUNCOES

window_onload()
setupPage()
selChavePesquisa_onchange()
txtArgumento_onKeyPress()
txtArgumento_ondigit()
btnFindPesquisa_onclick(ctl)
btn_onclick(ctl)
startPesqGridList()
startPesqGridList_DSC()
startPesqGridHistorico()
startPesqGridHistorico_DSC()
invertChkBox()
chkEhProprio_onclick()
btnOK_Clicked()

Eventos particulares de grid
fg_ModPesqProdDblClick()
fg_ModPesqProdKeyPress(KeyAscii)
********************************************************************/

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modalpesqprodBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    selChavePesquisa_onchange();
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    window.focus();
    if ( document.getElementById('txtArgumento').disabled == false )
        txtArgumento.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Produto', 1);

    var elem;
    var frameRect, modWidth, modHeight;
    var borderThickness = 6;
    
    // Ajusta elementos da janela
    
    // dimensoes da modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // ajusta o divPesquisa
    adjustElementsInForm([['lblEhProprio','chkEhProprio',3,1, -10],
                          ['lblChavePesquisa', 'selChavePesquisa', 15, 1, -5],
                          ['lblArgumento', 'txtArgumento', 28, 1, 0],
                          ['lblArgumento2', 'txtArgumento2', 28, 1, 0]],
                          null,null,true);
                          
    elem = window.document.getElementById('divPesquisa');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height, 10);
        width = modWidth - (2 * ELEM_GAP) - borderThickness ;
        height = parseInt(txtArgumento.currentStyle.top, 10) + parseInt( txtArgumento.currentStyle.height, 10);
    }

    // btnFindPesquisa
    btnFindPesquisa.disabled = true;
    with (btnFindPesquisa.style)
    {
        top = parseInt(txtArgumento2.style.top, 10);
        left = left = parseInt(divPesquisa.currentStyle.width, 10) - parseInt(btnFindPesquisa.currentStyle.width, 10);
        width = 80;
        height = 24;
    }

    var aOptions = [['CB','C�digo Barra'],
                     ['NS','Identificador/NS'],
                     ['NF','Nota Fiscal'],
                     ['P/C','Produto/Parceiro']];

    for (i=0; i<aOptions.length; i++)
    {
        var oOption = document.createElement("OPTION");
        oOption.value = aOptions[i][0];
        oOption.text = aOptions[i][1];
        selChavePesquisa.add(oOption);
    }
        
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.currentStyle.top) + parseInt(divPesquisa.currentStyle.height) + ELEM_GAP;
        width = parseInt(divPesquisa.currentStyle.width);
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 2) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);

    headerGrid(fg,['Produto',
                   'ID',
                   'N�mero S�rie',
                   'Pedido',
                   'CFOP',
                   'NF',
                   'Data',
                   'Cliente',
                   'ID',
                   'Gar',
                   'Observa��o'], []);
    
    fg.Redraw = 2;
    
    lblEhProprio.onclick = invertChkBox;
    chkEhProprio.onclick =  chkEhProprio_onclick;    
    
    selChavePesquisa.onchange = selChavePesquisa_onchange;
    txtArgumento.onkeypress = txtArgumento_onKeyPress;
    txtArgumento2.onkeypress = txtArgumento_onKeyPress;
}

function selChavePesquisa_onchange()
{
    ctl = selChavePesquisa;    

    if ((ctl.value == 'P/C') || (ctl.value == 'NS'))
    {
        lblArgumento2.style.visibility = 'inherit';
        txtArgumento2.style.visibility = 'inherit';
    }
    else
    {
        lblArgumento2.style.visibility = 'hidden';
        txtArgumento2.style.visibility = 'hidden';
    }

    txtArgumento_ondigit();
}

function txtArgumento_onKeyPress()
{
    if ( event.keyCode == 13 )
    {
        lockControlsInModalWin(true);
        modPesqProd_TimerHandler = window.setInterval('btnFindPesquisa_onclick()', 30, 'JavaScript');
    }
}

function txtArgumento_ondigit()
{
    if ( txtArgumento2.currentStyle.visibility == 'hidden' )
        changeBtnState(txtArgumento.value);
    else
        changeBtnState(txtArgumento.value + txtArgumento2.value);    
}

function btnFindPesquisa_onclick(ctl)
{
    if ( modPesqProd_TimerHandler != null )
    {
        window.clearInterval(modPesqProd_TimerHandler);
        modPesqProd_TimerHandler = null;
    }
    
    txtArgumento.value = trimStr(txtArgumento.value);
    txtArgumento2.value = trimStr(txtArgumento2.value);

    txtArgumento_ondigit();

    if (btnFindPesquisa.disabled)
    {
        lockControlsInModalWin(false);
        return;
    }    
    
    startPesqGridList();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);
    
    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        // sendJSMessage abaixo movido para btnOK_Clicked()
        // sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null );
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
}

/********************************************************************
Chamada ao servidor para obtencao de dados para montar o grid
em modo de listagem
********************************************************************/
function startPesqGridList()
{
    var nLastPos = (trimStr(txtArgumento.value)).length - 1;
    var sOperator = ' >= ';
    if ( ((trimStr(txtArgumento.value)).substr(0,1) == '%') ||
         ((trimStr(txtArgumento.value)).substr(nLastPos,1) == '%') )
         sOperator = ' LIKE ';
         
    var nLastPos2 = (trimStr(txtArgumento2.value)).length - 1;
    var sOperator2 = ' >= ';
    if ( ((trimStr(txtArgumento2.value)).substr(0,1) == '%') ||
         ((trimStr(txtArgumento2.value)).substr(nLastPos2,1) == '%') )
         sOperator2 = ' LIKE ';

    lockControlsInModalWin(true);
    sPesquisa = 'NULL';
    var sPesquisa = '';
    var sPesquisaSNS = '';
	var	sFiltroPedido = '';
    var nProdutoID = 0;
    var sNumeroSerie = '';
    var strSelect = '';
    var strSelectSNS = '';
    var strFrom = '';
    var strFromSNS = '';
    var strWhere1 = '';
    var strWhere1SNS = '';
    var strWhere2 = '';    
    var strOrder = '';
    var strOrderSNS = '';    
    var sOuter = ' LEFT OUTER JOIN ';    
    var strSQL = '';
    var strSelectLote = '';
    var strFromLote = '';
    var strWhere1Lote = '';
    var strWhere2Lote = '';
    var sPesquisaLote = '';

    // Codigo de Barra
    if ( selChavePesquisa.value == 'CB' )    
    {
        nProdutoID = parseInt((txtArgumento.value).substr(0,5), 10);
        sNumeroSerie = txtArgumento.value.substr(5);

        if ( isNaN(nProdutoID) )
        {
            if ( window.top.overflyGen.Alert('C�digo de Barra inv�lido!') == 0 )
                return null;
   
            lockControlsInModalWin(false);
            
            txtArgumento.focus();
   
            return null;
        }
        else
        {
            sPesquisa = ' AND Produtos.ConceitoID = ' + nProdutoID + ' AND NumerosSerie.NumeroSerie >= ' + '\'' + sNumeroSerie + '\'' + ' ';
            sPesquisaLote = ' AND Produtos.ConceitoID = ' + nProdutoID + ' AND Movimentos.Lote >= ' + '\'' + sNumeroSerie + '\'' + ' ';
            strOrder = ' ORDER BY Conceito, ConceitoID ';
            sPesquisaSNS = ' AND Produtos.ConceitoID = ' + nProdutoID + ' ';
            strOrderSNS = ' ORDER BY Produtos.Conceito, Produtos.ConceitoID ';
        }
    }
    // Nota Fiscal
    else if ( selChavePesquisa.value == 'NF' )    
    {
        if (isNaN(txtArgumento.value))
        {
            if ( window.top.overflyGen.Alert('Nota Fiscal inv�lida!') == 0 )
                return null;
            
            lockControlsInModalWin(false);
            txtArgumento.focus();
            return false;
        }
        else
        {
            sPesquisa = ' AND NotasFiscais.NotaFiscal >= ' + txtArgumento.value + ' ';
            sPesquisaSNS = '';
            sOuter = ' INNER JOIN ';
            strOrder = ' ORDER BY NotaFiscal, Conceito, ConceitoID';
            strOrderSNS = '';
        }
    }
    // Produto / Parceiro
    else if ( selChavePesquisa.value == 'P/C' )    
    {
        sPesquisa = ' AND Produtos.Conceito ' + sOperator +  '\'' + txtArgumento.value + '\'' + 
            ' AND Pessoas.Fantasia ' + sOperator2 + '\'' + txtArgumento2.value + '\'' + ' ';
		sPesquisaSNS = '';
        strOrder = ' ORDER BY Conceito, ConceitoID, Fantasia';
        strOrderSNS = '';
    }

	if (!chkEhProprio.checked)
		sFiltroPedido = ' AND Pedidos.TipoPedidoID = 602 AND Pedidos.EstadoID >= 29 AND Movimentos.EstoqueID = 341 ';
	else
		sFiltroPedido = ' AND Pedidos.TipoPedidoID = 601 AND Pedidos.EstadoID >= 30 AND Movimentos.EstoqueID = 341 ';

   if ( (selChavePesquisa.value == 'P/C') ||
		(selChavePesquisa.value == 'NF') ||
		(selChavePesquisa.value == 'CB') )
   {
			strSelect = 'SELECT TOP 100 ' +
				'Produtos.Conceito, Produtos.ConceitoID, NumerosSerie.NumeroSerie, ' +
				'Pedidos.PedidoID, Transacoes.OperacaoID AS Codigo, NotasFiscais.NotaFiscal, NotasFiscais.dtNotaFiscal, ' +
				'Pessoas.Fantasia, Pessoas.PessoaID, 0 AS Garantia, SPACE(0) AS Observacao ';

			strFrom = 'FROM ' +
				'NumerosSerie_Movimento Movimentos WITH(NOLOCK) ' +
				'INNER JOIN NumerosSerie WITH(NOLOCK) ON (Movimentos.NumeroSerieID = NumerosSerie.NumeroSerieID) ' +
				'INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (NumerosSerie.ProdutoID = Produtos.ConceitoID) ' +
				'INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (ProdutosEmpresa.ObjetoID = Produtos.ConceitoID) ' +
				'INNER JOIN Pedidos WITH(NOLOCK) ON (Movimentos.PedidoID = Pedidos.PedidoID) ' +
				sOuter + ' NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID) ' +
				'INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID = Transacoes.OperacaoID) ' +
				'INNER JOIN Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID = Pessoas.PessoaID) ';
			
			strWhere1 = 'WHERE ' +
				'(Movimentos.NumMovimentoID = ' +
					'(SELECT TOP 1 Movimentos2.NumMovimentoID ' +
						'FROM NumerosSerie_Movimento Movimentos2 WITH(NOLOCK), Pedidos Pedidos2 WITH(NOLOCK) ' +
						'WHERE (NumerosSerie.NumeroSerieID = Movimentos2.NumeroSerieID AND ' + 
							'Movimentos2.PedidoID = Pedidos2.PedidoID AND Pedidos2.EstadoID >= 29 AND ' +
							'Pedidos2.EmpresaID = Pedidos.EmpresaID) ' +
						'ORDER BY Movimentos2.NumMovimentoID DESC) AND ';

			strWhere2 = 'ProdutosEmpresa.SujeitoID = ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID = 61 AND ' +
				'(ProdutosEmpresa.TipoGatilhamento IN(1, 2, 3)) AND ' +
				'Pedidos.EmpresaID = ' + glb_nEmpresaID + ' ' + sFiltroPedido + ' ) ';

			strSQL = strSelect + strFrom + strWhere1 + strWhere2 + sPesquisa;

		//INICIO DE NOVO NS-------------------------------------------------------------------

			strSelectLote = 'SELECT TOP 100 ' +
				'Produtos.Conceito, Produtos.ConceitoID, Movimentos.Lote, ' +
				'Pedidos.PedidoID, Transacoes.OperacaoID AS Codigo, NotasFiscais.NotaFiscal, NotasFiscais.dtNotaFiscal, ' +
				'Pessoas.Fantasia, Pessoas.PessoaID, 0 AS Garantia, SPACE(0) AS Observacao ';

			strFromLote = 'FROM Pedidos_ProdutosLote Movimentos WITH(NOLOCK) ' +
                'INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Movimentos.ProdutoID = Produtos.ConceitoID) ' +
                'INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (ProdutosEmpresa.ObjetoID = Produtos.ConceitoID) ' +
                'INNER JOIN Pedidos WITH(NOLOCK) ON (Movimentos.PedidoID = Pedidos.PedidoID) ' +
                sOuter + ' NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID = 67) ' +
                'INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID = Transacoes.OperacaoID) ' +
                'INNER JOIN Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID = Pessoas.PessoaID) ';
			
			strWhere1Lote = 'WHERE ' +
				'(Movimentos.PedProdutoID = ' +
					'(SELECT TOP 1 Movimentos2.PedProdutoID ' +
						'FROM Pedidos_ProdutosLote Movimentos2 WITH(NOLOCK), Pedidos Pedidos2 WITH(NOLOCK) ' +
						'WHERE (Movimentos.PedProdutoID = Movimentos2.PedProdutoID AND ' + 
							'Movimentos2.PedidoID = Pedidos2.PedidoID AND Pedidos2.EstadoID >= 29 AND ' +
							'Pedidos2.EmpresaID = Pedidos.EmpresaID) ' +
						'ORDER BY Movimentos2.PedProdutoID DESC) AND ';

			strWhere2Lote = 'ProdutosEmpresa.SujeitoID = ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID = 61 AND ' +
				'(ProdutosEmpresa.TipoGatilhamento IN(1, 2, 3)) AND ' +
				'Pedidos.EmpresaID = ' + glb_nEmpresaID + ' ' + sFiltroPedido + ' ) ';

			strSQL = strSQL + 'UNION ALL ' + strSelectLote + strFromLote + strWhere1Lote + strWhere2Lote + sPesquisaLote;


		//FIM DE NOVO NS-------------------------------------------------------------------

			if ((glb_sCaller == 'S') && (chkEhProprio.checked) && (selChavePesquisa.value == 'CB'))
			{
				strSelectSNS = 'SELECT TOP 100 ' +
					'Produtos.Conceito, Produtos.ConceitoID, SPACE(0) AS NumeroSerie, ' +
					'NULL AS PedidoID, NULL AS Codigo, NULL AS NotaFiscal, NULL AS dtNotaFiscal, ' +
					'NULL AS Fantasia, NULL AS PessoaID, 0 AS Garantia, SPACE(0) AS Observacao ';

				strFromSNS = 'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK) ';
				
				strWhere1SNS = 'WHERE (ProdutosEmpresa.SujeitoID=' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID = 61 AND ' +
					'ProdutosEmpresa.ObjetoID = Produtos.ConceitoID AND ProdutosEmpresa.TipoGatilhamento = 0 AND Produtos.ProdutoSeparavel = 1 AND ' +
					'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID, ProdutosEmpresa.ObjetoID, 341, NULL, GETDATE(), NULL) > 0 ' + sPesquisaSNS + ') ';
			
				strSQL += 'UNION ALL ' + strSelectSNS + strFromSNS + strWhere1SNS;
			}

			strSQL += strOrder;
	}		
	// Numero de Serie
    else if ( selChavePesquisa.value == 'NS' )    
    {	
		var TipoResultadoID = 1;			
		var sArgumento1 = null;
		var sArgumento2 = null;
		var EhProprio = 0;
		
		sArgumento1 = (txtArgumento.value);
		sArgumento2 = (txtArgumento2.value);
       
       if (sArgumento1 == '')
			sArgumento1 = 'NULL';
	   else
			sArgumento1 = '\'' + txtArgumento.value + '\'';
			
	   if (sArgumento2 == '')
			sArgumento2 = 'NULL';
		else
			sArgumento2 = '\'' + txtArgumento2.value + '\'';
			
       if (chkEhProprio.checked)
			EhProprio = 1;
		else
			EhProprio = 0;
       
       strSQL = 'EXEC sp_Asstec_PesquisaProduto ' + sArgumento1 + ',' + sArgumento2 + ',' + glb_nEmpresaID + ',' + EhProprio + ',' + TipoResultadoID;
    }

    glb_gridMode = 'L';
        
    setConnection(dsoPesq);

    dsoPesq.SQL = strSQL;
    dsoPesq.ondatasetcomplete = startPesqGridList_DSC;
    dsoPesq.Refresh();
}

/********************************************************************
Retorno do servidor com dados para montar o grid em modo de listagem
********************************************************************/
function startPesqGridList_DSC() {
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

	var lblPessoa = '';
	
	if (!chkEhProprio.checked)
	{
		lblPessoa = 'Cliente';
	}
	else
	{
		lblPessoa = 'Fornecedor';
	}

	startGridInterface(fg);
    
    fg.FrozenCols = 0;

    headerGrid(fg,['Produto',
                   'ID',
                   'N�mero S�rie',
                   'Pedido',
                   'CFOP',
                   'NF',
                   'Data',
                   lblPessoa,
                   'ID',
                   'Gar',
                   'Observa��o'], []);
                   
    fillGridMask(fg,dsoPesq,['Conceito*',
                              'ConceitoID*',
                              'NumeroSerie*',
                              'PedidoID*',
                              'Codigo*',
                              'NotaFiscal*', 
                              'dtNotaFiscal*',
                              'Fantasia*',
                              'PessoaID*',
                              'Garantia*',
                              'Observacao*'],
                              ['','','','','','','','','','',''],
                              ['','','','','','',dTFormat,'','','','']);

    alignColsInGrid(fg,[1,3,4,5,8]);                           
    
    fg.ColDataType(9) = 11;

    fg.Editable = false;
    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    fg.Redraw = 0;
    
    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    fg.Redraw = 2;
    
    // destrava botao OK se tem linhas no grid
    // apenas para form em modo detalhe
    if (fg.Rows > 1)
    {
        if ( glb_sCaller == 'S' )
            btnOK.disabled = false;
            
        fg.Row = 1;
        window.focus();
        fg.focus();
    }    
    else
    {
        btnOK.disabled = true;    
        txtArgumento.focus();
    }    
        
    // writeInStatusBar('child', 'cellMode', 'XXXXXXXX');    
}

/********************************************************************
Chamada ao servidor para obtencao de dados para montar o grid
em modo de historico de produto
********************************************************************/
function startPesqGridHistorico()
{
    lockControlsInModalWin(true);
    
    var nProdutoID = 0;
    var sNumeroSerie = '';
    var strSelect = '';    
    var strFrom = '';    
    var strWhere = '';    
    var strSelectLote = '';    
    var strFromLote = '';    
    var strWhereLote = '';
	var strOrder = '';   
    var strSQL = '';

    nProdutoID = trimStr(fg.TextMatrix(fg.Row, 1));
    
    sNumeroSerie = trimStr(fg.TextMatrix(fg.Row, 2));

//INICIO DE NOVO NS--------------------------------------------------    

    strSelect = 'SELECT ' +
        'Produtos.Conceito, Produtos.ConceitoID, NumerosSerie.NumeroSerie, ' +
		'Pedidos.PedidoID, Transacoes.OperacaoID AS Codigo, NotasFiscais.NotaFiscal, NotasFiscais.dtNotaFiscal, ' +
		'Pessoas.Fantasia, Pessoas.PessoaID, Movimentos.AsstecID, ' +
		'CASE WHEN NotasFiscais.dtNotaFiscal IS NULL THEN (GETDATE() + 10000) ELSE NotasFiscais.dtNotaFiscal END AS dtNFOrderby '; 
/*		
		'(SELECT TOP 1 CASE WHEN (Itens.dtMovimento IS NULL) THEN (GETDATE() + 10000) ELSE Itens.dtMovimento END FROM Pedidos_Itens Itens ' +
                             'WHERE (Pedidos.PedidoID = Itens.PedidoID AND NumerosSerie.ProdutoID = Itens.ProdutoID) ' +
                             'ORDER BY Itens.dtMovimento DESC) AS dtMovimentoOrderby ';
*/
	strFrom = 'FROM ' +
        'NumerosSerie WITH(NOLOCK) ' +
            'INNER JOIN NumerosSerie_Movimento Movimentos WITH(NOLOCK) ON (NumerosSerie.NumeroSerieID = Movimentos.NumeroSerieID) ' +
            'INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (NumerosSerie.ProdutoID = Produtos.ConceitoID) ' +
            'INNER JOIN Pedidos WITH(NOLOCK) ON (Movimentos.PedidoID = Pedidos.PedidoID) ' +
            'LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) ' +
            'INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID = Transacoes.OperacaoID) ' +
            'INNER JOIN Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID = Pessoas.PessoaID) ';
	
	strWhere = 'WHERE ' +
	    '(NumerosSerie.ProdutoID = ' + nProdutoID + ' AND NumerosSerie.NumeroSerie = ' + '\'' + sNumeroSerie + '\'' + ' AND ' +
		'Pedidos.EmpresaID = ' + glb_nEmpresaID + ') ';

    strSQL = strSelect + strFrom + strWhere;
        
    strSelectLote = 'SELECT ' +
        'Produtos.Conceito, Produtos.ConceitoID, Movimentos.Lote, ' +
		'Pedidos.PedidoID, Transacoes.OperacaoID AS Codigo, NotasFiscais.NotaFiscal, NotasFiscais.dtNotaFiscal, ' +
		'Pessoas.Fantasia, Pessoas.PessoaID, Movimentos.AsstecID, ' +
		'CASE WHEN NotasFiscais.dtNotaFiscal IS NULL THEN (GETDATE() + 10000) ELSE NotasFiscais.dtNotaFiscal END AS dtNFOrderby '; 
/*		
		'(SELECT TOP 1 CASE WHEN (Itens.dtMovimento IS NULL) THEN (GETDATE() + 10000) ELSE Itens.dtMovimento END ' +
							 'FROM Pedidos_Itens Itens ' +
                             'WHERE (Pedidos.PedidoID = Itens.PedidoID AND Movimentos.ProdutoID = Itens.ProdutoID) ' +
                             'ORDER BY Itens.dtMovimento DESC) AS dtMovimentoOrderby ';
*/
	strFromLote = 'FROM ' + 'Pedidos_ProdutosLote Movimentos WITH(NOLOCK) ' +
	    'INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Movimentos.ProdutoID = Produtos.ConceitoID) ' +
	    'INNER JOIN Pedidos WITH(NOLOCK) ON (Movimentos.PedidoID = Pedidos.PedidoID) ' +
	    'LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) ' +
	    'INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID = Transacoes.OperacaoID) ' +
	    'INNER JOIN Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID = Pessoas.PessoaID) ';
	
	strWhereLote = 'WHERE ' + '(Movimentos.ProdutoID = ' + nProdutoID + ' AND Movimentos.Lote = ' + '\'' + sNumeroSerie + '\'' + ' AND ' +
		'Pedidos.EmpresaID = ' + glb_nEmpresaID + ') ';
		
	//strOrder = ' ORDER BY dtMovimentoOrderby';
	strOrder = ' ORDER BY dtNFOrderby';
    
    strSQL = strSQL + ' UNION ALL ' + strSelectLote + strFromLote + strWhereLote;
    strSQL += strOrder; 
    
//FIM DE NOVO NS--------------------------------------------------    

    glb_gridMode = 'H';
        
    setConnection(dsoPesq);

    dsoPesq.SQL = strSQL;
    dsoPesq.ondatasetcomplete = startPesqGridHistorico_DSC;
    dsoPesq.Refresh();
}

/********************************************************************
Retorno do servidor com dados para montar o grid em modo de historico
de produto
********************************************************************/
function startPesqGridHistorico_DSC() {
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

	startGridInterface(fg);
    
    fg.FrozenCols = 0;

    headerGrid(fg,['Produto',
                   'ID',
                   'N�mero S�rie',
                   'Pedido',
                   'CFOP',
                   'NF',
                   'Data',
                   'Pessoa',
                   'ID',
                   'Asstec'], []);
                   
    fillGridMask(fg,dsoPesq,['Conceito*',
                              'ConceitoID*',
                              'NumeroSerie*',
                              'PedidoID*',
                              'Codigo*',
                              'NotaFiscal*', 
                              'dtNotaFiscal*',
                              'Fantasia*',
                              'PessoaID*',
                              'AsstecID*'],
                              ['','','','','','','','','',''],
                              ['','','','','','',dTFormat,'','','']);

    alignColsInGrid(fg,[1,3,4,5,8,9]);                           
    
    fg.Editable = false;
    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    fg.Redraw = 0;
    
    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    fg.Redraw = 2;
    
    // destrava botao OK se tem linhas no grid
    // apenas para form em modo detalhe
    if (fg.Rows > 1)
    {
        if ( glb_sCaller == 'S' )
            btnOK.disabled = false;
            
        fg.Row = 1;
        window.focus();
        fg.focus();
    }    
    else
    {
        btnOK.disabled = true;    
        txtArgumento.focus();
    }    
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    chkEhProprio_onclick();
}

/********************************************************************
Usuario clicou checkBox
********************************************************************/
function chkEhProprio_onclick()
{
    btnOK.disabled = true;
    
    fg.Redraw = 0;
    
    if ( fg.Rows > 1 )
        fg.Rows = 1;
    
    startGridInterface(fg);        
    
    headerGrid(fg,['Produto',
                   'ID',
                   'N�mero S�rie',
                   'Pedido',
                   'CFOP',
                   'NF',
                   'Data',
                   'Cliente',
                   'ID',
                   'Gar',
                   'Observa��o'], []);
    
    fg.Redraw = 2;
}

/********************************************************************
Usuario clicou botao OK da modal quando visivel em modo detalhe
********************************************************************/
function btnOK_Clicked()
{
    if ( fg.Row < 1 )
        return null;
 
    var param2 = new Array();
    param2[0] = chkEhProprio.checked;
    param2[1] = fg.TextMatrix(fg.Row, 0);
    param2[2] = fg.TextMatrix(fg.Row, 1);
    param2[3] = fg.TextMatrix(fg.Row, 2);
       
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , param2 );
}

// EVENTOS DE GRID PARTICULARES DESTA PAGINA ************************

/********************************************************************
Esta modal e usada no pesqlist e no sup.
No pesqlist o botao OK da modal e sempre desabilitado.

Os eventos de grid: Duplo Clique e Enter.
1. Acionam o botao OK da modal quando ela foichamada pelo sup e
    devolve dados para alterar campos do sup
2. Remontam o grid pela seguinte regra:
2.1. Se o grid corrente e listagem de produtos -> monta grid de historico
    do produto.
2.2. Se o grid corrente e historico do produto -> mota grid de listagem
    de produtos.

Flags a usar
1. Modo do grid: listagem ou historico -> glb_gridMode
    'L' = Listagem 'H' = Historico
2. Quem chamou: pesqlist ou sup -> glb_sCaller
    
********************************************************************/

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqProdDblClick()
{
    if ( fg.Row < 1 )
        return true;
        
    // Duplo clique no grid quando chamado pelo pesqlist
    if ( glb_sCaller == 'PL' )
    {
        // grid esta em modo de listagem -> carrega em modo de historico
        if ( glb_gridMode == 'L' )
            startPesqGridHistorico();
        // grid esta em modo de historico -> carrega em modo de listagem
        else if ( glb_gridMode == 'H' )
            startPesqGridList();
    }
    // Duplo clique no grid quando chamado pelo detalhe
    else if ( glb_sCaller == 'S' )
    {
        if ( btnOK.disabled == false )
            btn_onclick(btnOK);
    }    
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqProdKeyPress(KeyAscii)
{
    if ( fg.Row < 1 )
        return true;
        
    // Enter no grid quando chamado pelo pesqlist
    if ( glb_sCaller == 'PL' )
    {
        // grid esta em modo de listagem -> carrega em modo de historico
        if ( glb_gridMode == 'L' )
            startPesqGridHistorico();
        // grid esta em modo de historico -> carrega em modo de listagem
        else if ( glb_gridMode == 'H' )
            startPesqGridList();
    }
    // Enter no grid quando chamado pelo detalhe
    else if ( glb_sCaller == 'S' )
    {
        if ( (KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false) )
            btn_onclick(btnOK);
    }    
}

// FINAL DE EVENTOS DE GRID PARTICULARES DESTA PAGINA ***************