/********************************************************************
modalprint_asstec.js

Library javascript para o modalprint.asp
Valores a Localizar
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_CounterCmbsDynamics = 0;
var glb_CounterCmbsStatics = 0;
var glb_FillCmbFirstTime = false;

//  dso usado para envia e-mails 
var dsoEMail = new CDatatransport("dsoEMail");
//  Dados dos combos dinamicos (selProdutoID .SQL 
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");
var dsoCmbDynamic02 = new CDatatransport("dsoCmbDynamic02");
var dsoMarca = new CDatatransport("dsoMarca");
var dsoProprietario = new CDatatransport("dsoProprietario");
var dsoVendedor = new CDatatransport("dsoVendedor");


var dsoEstado = new CDatatransport("dsoEstado");



// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    linksDivAndReports()
    invertChkBox()
    chkBox_Clicked()
    selReports_Change()
    btnOK_Status()
    btnOK_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    noRights()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	glb_bUseProgressBar = true;
	asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    with (modalprintBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    glb_FillCmbFirstTime = true;
    fillStaticCmbs();
}

function window_onload_2ndPart()
{

    window.focus();
    
    if ( selReports.disabled == false )
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
     if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {
		pb_StopProgressBar(true);
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
	}        
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
 
    // texto da secao01
    secText('Relat�rios', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;    
    
    // largura livre
    widthFree = modWidth - frameBorder;
    
    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    with (lblReports.style)
    {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta combo de relatorios
    with (selReports.style)
    {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta o divRelatorioAsstec
    with (divRelatorioAsstec.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }
    // ajusta o divRelatorioEIA
    with (divRelatorioEIA.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }
    
    // ajusta o divEtiquetaProduto
    with (divEtiquetaProduto.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }
   //Justes dos Bot�es OK e Cancelar
    btnOK.style.top = (parseInt(txtFiltro.style.top, 10) * 2) - 130;
    btnCanc.style.top = btnOK.style.top;

    selEstadoID.onchange = selEstadoID_onchange;
    selFiltros.onchange = selFiltros_onchange;

    // O estado do botao btnOK
    btnOK_Status();
    
    // O usuario tem direito ou nao
    noRights();
  //Heraldo
    txtDtInicial.disabled = true;
    txtDtFinal.disabled = true;
    
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;
    
    selReports.onchange = selReports_Change;
    
    // carrega a lista de relatorios
    loadReportsInList();
    
    // linka os divs nos relatorios
    linkDivsAndReports();
    
    // ajusta os divs

    // ajusta o divRelatorioAsstec
    adjustdivRelatorioAsstec();

    // ajusta o divRelatorioAsstec
    adjustdivRelatorioEIA();
    
    // ajusta o divRelatorioVendas
    adjustdivEtiquetaProduto();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList()
{
    var i;
    
    if ( glb_arrayReports != null )
    {
        for ( i=0; i<glb_arrayReports.length; i++)    
        {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);
            
            if ( glb_arrayReports[i][2] )
                selReports.selectedIndex = i;
        }
        
        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports()
{
	divEtiquetaProduto.setAttribute('report', 40174 , 1);
    divRelatorioAsstec.setAttribute('report', 40175 , 1);
    divRelatorioEIA.setAttribute('report', 40176 , 1);
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    // O estado do botao btnOK
    btnOK_Status();
    
    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();
    
    return true;
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change()
{
    // mostra o div correspondente ao relatorio
    showDivByReportID();
    
    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if ( this.disabled == false )
        this.focus();
    
    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado

********************************************************************/
function btnOK_Status()
{
    var btnOKStatus = true;
    var i, elem;
    
    if ( selReports.selectedIndex != -1 )
    {
        if (selReports.value == 40174)
            btnOKStatus = false;
        else if ((selReports.value == 40175) /*&& (hasEstoqueChecado())*/)
            btnOKStatus = false;
        else if (selReports.value == 40176)
            btnOKStatus = false;          
   }        
    btnOK.disabled = btnOKStatus;
}

function selEstadoID_onchange() {
    
    if (selEstadoID.value != 65) {

        //Desbilita os CheckBox para Estados Diferente de "N"


        /*chkEPD.disabled = true;
        chkEPC.disabled = true;
        chkETO.disabled = true;
        chkETD.disabled = true;
        chkETC.disabled = true;
        chkEPD.disabled = true;*/

       
        //Habilita campos Data para estados Diferente de "N"
        
        txtDtInicial.disabled = false;
        txtDtFinal.disabled = false;

        //Desabilita Checks para Estados  Diferente de "N"

        /*chkEPD.checked = false;
        chkEPC.checked = false;
        chkETO.checked = false;
        chkETD.checked = false;
        chkETC.checked = false;
        chkEPD.checked = false;*/
        chkCliente1.checked = false;
        chkCliente2.checked = false;
        chkFornecedor1.checked = false;
        chkFornecedor2.checked = false;

    
    }
    if (selEstadoID.value == 65) {

        //Limpa os campos de Data para novas consultas

        txtDtInicial.value = null;
        txtDtFinal.value = null;

        txtDtInicial.value = '';
        txtDtFinal.value = '';


        //Habilita campos Data para estados em "N"

        txtDtInicial.disabled = true;
        txtDtFinal.disabled = true;

        //Habilita Checks para Estados em "N"
        
        /*chkEPD.disabled = false;
        chkEPC.disabled = false;
        chkETO.disabled = false;
        chkETD.disabled = false;
        chkETC.disabled = false;
        chkEPD.disabled = false;*/
        chkCliente1.disabled = false;
        chkCliente2.disabled = false;
        chkFornecedor1.disabled = false;
        chkFornecedor2.disabled = false;

       
        
    }

  
}
/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked() {


    //Verifica se o Estado � N e se todos os Check est�o habilitados
    if (selReports.value == 40175) 
    {
        if ((selEstadoID.value == 65) && (hasEstoqueChecado() == false)) {
              
            btnOKStatus = false;
            btnOK_Status();
            window.top.overflyGen.Alert('Favor escolher pelo menos um Estoque.');
            lockControlsInModalWin(false);
            return null;
        }

        //Valida datas
        
        if (txtDtInicial.value > txtDtFinal.value)     
        {
            window.top.overflyGen.Alert('Data inicial ' + txtDtInicial.value + '  � maior que a data final ' + txtDtFinal.value + ' , favor corrigir.');
            lockControlsInModalWin(false);
            return null;
        }

        //Valida Datas
        
        if (txtDtFinal.value < txtDtInicial.value)	     
	      {
            window.top.overflyGen.Alert('Data inicial ' + txtDtFinal.value + '  � menor que a data final' + txtDtInicial.value + '  , favor corrigir.');
            lockControlsInModalWin(false);
            return null;
        }
        
        //Verifica se a data � v�lida

        if ((verificaData(txtDtInicial.value) == false) || ((verificaData(txtDtFinal.value)) == false)) {

            lockControlsInModalWin(false);
            return null;
        } 
    }     

	// Etiqueta de Produto
	if ( (selReports.value == 40174) && (glb_sCaller == 'PL') )
        etiquetaProduto();
    // Relatorio de Valores a Localizar
    else if ( (selReports.value == 40175) && (glb_sCaller == 'PL') )
        relatorioAsstec();
    else if ( (selReports.value == 40176) && (glb_sCaller == 'S') )
        relatorioEIA();
    else
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null);
}

/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID()
{
    var i, coll, attr, currRep;
    
    attr = null;
    currRep = 0;
    
    currRep = selReports.value;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( attr == currRep )
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';    
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr()
{
    var i, coll, attr, retVal;
    
    attr = null;
    retVal = 0;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( coll[i].style.visibility == 'visible' )
            {
                retVal = attr;
                break;
            }    
        }
    }
    return retVal;    
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights()
{
    if ( selReports.options.length != 0 )
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;
    
    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
            coll[i].style.visibility = 'hidden';    
    }
    
    // desabilita o combo de relatorios
    selReports.disabled = true;
    
    // desabilita o botao OK
    btnOK.disabled = true;
        
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];
    
    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);
       
    elem = document.getElementById('selReports');
    
    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
        
    // a altura livre    
    modHeight -= topFree;
        
    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt'; 
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';
    
    // acrescenta o elemento
    window.document.body.appendChild(elem);
        
    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);
        
    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;
        
    return null;
}

function adjustdivRelatorioAsstec()
{
    adjustElementsInForm([['lblFiltros', 'selFiltros', 17, 1, -10],
                          /*
 	                      ['lblEPD','chkEPD',3,1,-10],					  
                          ['lblEPC','chkEPC',3,1],
                          ['lblETO','chkETO',3,1],
                          ['lblETD','chkETD',3,1],
                          ['lblETC','chkETC',3,1],
                          */
						  ['lblCliente1','chkCliente1',3,1, 1],
						  ['lblCliente2','chkCliente2',3,1],
						  ['lblFornecedor1','chkFornecedor1',3,1],
						  ['lblFornecedor2', 'chkFornecedor2', 3, 1],
						  ['lblEstadoID', 'selEstadoID', 5, 1],
						  //['lblTipo','selTipo',21, 2,-10],
						  ['lblFornecedorID','selFornecedorID',21,2, -10],
						  ['lblMarcaID','selMarcaID',21,3,-10],
						  ['lblProprietarioID','selProprietarioID',21,3],
						  ['lblClienteID','selClienteID',21,4,-10],
						  ['lblVendedorID', 'selVendedorID', 21, 4],
						  ['lblDtInicial', 'txtDtInicial', 21, 5, -10],
						  ['lblDtFinal', 'txtDtFinal', 21, 5],				   
						  ['lblFiltro','txtFiltro',43,6,-10]],null,null,true);

	/*chkEPD.onclick = chkEstoquesOnClick;
	chkEPC.onclick = chkEstoquesOnClick;
	chkETO.onclick = chkEstoquesOnClick;
	chkETD.onclick = chkEstoquesOnClick;
	chkETC.onclick = chkEstoquesOnClick;*/

    selFornecedorID.onchange = selFornecedorID_onchange;
    selFiltros.onchange = selFiltros_onchange;
}

function adjustdivRelatorioEIA()
{
	txtVias.value = '1';
    txtVias.onkeypress = verifyNumericEnterNotLinked;
    txtVias.setAttribute('thePrecision', 2, 1);
    txtVias.setAttribute('theScale', 0, 1);
    txtVias.setAttribute('minMax', new Array(0, 99), 1);
    txtVias.setAttribute('verifyNumPaste', 1);
    txtVias.onfocus = selFieldContent;

    adjustElementsInForm([['lblVias','txtVias',3,1]],null,null,true);
}

function adjustdivEtiquetaProduto()
{
    adjustElementsInForm([['lblVias2','txtVias2',5,1,-10,-10]],null,null,true);

    txtVias2.onkeypress = verifyNumericEnterNotLinked;
    txtVias2.setAttribute('verifyNumPaste', 1);
    txtVias2.setAttribute('thePrecision', 3, 1);
    txtVias2.setAttribute('theScale', 0, 1);
    txtVias2.setAttribute('minMax', new Array(0, 999), 1);
    txtVias2.onfocus = selFieldContent;
    txtVias2.value = '1';

	return null;
}

function selFornecedorID_onchange()
{
	setLabelOfControlEx(lblFornecedorID, selFornecedorID);
}
function selFiltros_onchange() {

    var sFiltroEspecifico = selFiltros.value;

    if (sFiltroEspecifico == '(a.Defeito = 1 AND a.Fornecedor = 0)') {
        chkCliente1.checked = true;
        chkCliente2.checked = false;
        chkFornecedor1.checked = false;
        chkFornecedor2.checked = false;
    }
    else if (sFiltroEspecifico == '(a.Fornecedor = 1)') {
        chkCliente1.checked = true;
        chkCliente2.checked = false;
        chkFornecedor1.checked = true;
        chkFornecedor2.checked = false;
    }
    else if (sFiltroEspecifico == '(((a.Defeito = 0) AND (a.Cliente = 1) AND (Fornecedor = 0)) OR ((a.Proprio = 1) AND (a.Cliente = 1)))') {
        chkCliente1.checked = true;
        chkCliente2.checked = false;
        chkFornecedor1.checked = true;
        chkFornecedor2.checked = true;
    }
    else {
        chkCliente1.checked = true;
        chkCliente2.checked = true;
        chkFornecedor1.checked = true;
        chkFornecedor2.checked = true;
    }
}


/*function selTipo_onchange()
{
	lockControlsInModalWin(true);
	
	glb_CounterCmbsDynamics = 2;

	var sEhCliente = '-1';
	var strPars = '';

	// Somente cliente
	if (selTipo.value == 1)
		//sEhCliente = ' AND Pedidos.EhCliente = 1 ';
		sEhCliente = '1';
	// Somente fornecedores
	else if (selTipo.value == 2)
		//sEhCliente = ' AND Pedidos.EhCliente = 0 ';
		sEhCliente = '0';
		
	setConnection(dsoCmbDynamic01);
	dsoCmbDynamic01.SQL = 'SELECT 0 AS Indice, 0 AS fldID, ' + '\'' + '' + '\'' + ' AS fldName ' +
		'UNION ALL ' +
	'SELECT DISTINCT 1 AS Indice, c.PessoaID AS fldID, c.Fantasia AS fldName  ' +
		'FROM Asstec a WITH(NOLOCK) ' +
			'INNER JOIN Asstec_Pessoas b WITH(NOLOCK) ON (a.AsstecID = b.AsstecID) ' +
			'INNER JOIN Pessoas c WITH(NOLOCK) ON (b.PessoaID = c.PessoaID) ' +
		'WHERE (a.EmpresaID = ' + glb_nEmpresaID + ' AND a.EstadoID = 65 AND b.EhCliente = 0) ' +
	'ORDER BY Indice, fldName';

	dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
	dsoCmbDynamic01.Refresh();
  
	setConnection(dsoCmbDynamic02);
	dsoCmbDynamic02.SQL = 'SELECT 0 AS Indice, 0 AS fldID, ' + '\'' + '' + '\'' + ' AS fldName ' +
		'UNION ALL ' +
	'SELECT DISTINCT 1 AS Indice, c.PessoaID AS fldID, c.Fantasia AS fldName  ' +
		'FROM Asstec a WITH(NOLOCK) ' +
			'INNER JOIN Asstec_Pessoas b WITH(NOLOCK) ON (a.AsstecID = b.AsstecID) ' +
			'INNER JOIN Pessoas c WITH(NOLOCK) ON (b.PessoaID = c.PessoaID) ' +
		'WHERE (a.EmpresaID = ' + glb_nEmpresaID + ' AND a.EstadoID = 65 AND b.EhCliente = 1) ' +
	'ORDER BY Indice, fldName';

	dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
	dsoCmbDynamic02.Refresh();
}*/

/********************************************************************
Funcao disparada pelo programador.
Funcao do ondatasetcomplete disparada pela funcao startDynamicCmbs()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    glb_CounterCmbsDynamics--;
    
    if (glb_CounterCmbsDynamics == 0)
    {
		var i;
		var optionStr, optionValue;
		var aCmbsDynamics;
		var aDSOsDunamics;


	
		aCmbsDynamics = [selFornecedorID, selClienteID, selEstadoID];
		aDSOsDunamics = [dsoCmbDynamic01, dsoCmbDynamic02,dsoEstado];
		clearComboEx(['selFornecedorID', 'selClienteID', 'selEstadoID']);

		setLabelOfControlEx(lblFornecedorID, selFornecedorID);
		setLabelOfControlEx(lblClienteID, selClienteID);
		setLabelOfControlEx(lblEstadoID, selEstadoID);

		// Inicia o carregamento de combos dinamicos (selFornecedorID)
    
        for (i=0; i<aCmbsDynamics.length; i++)
        {
            // So preenche os combos se  vem mais de um registro
            // pois se vem um registro so este e a opcao em
            // branco do combo
            if ( !(aDSOsDunamics[i].recordset.BOF && aDSOsDunamics[i].recordset.BOF) )
            {
                aDSOsDunamics[i].recordset.MoveLast();
                aDSOsDunamics[i].recordset.MoveFirst();
                if ( aDSOsDunamics[i].recordset.RecordCount() > 1 )
                {
                    while (! aDSOsDunamics[i].recordset.EOF )
                    {
                        optionStr = aDSOsDunamics[i].recordset['fldName'].value;
	                	optionValue = aDSOsDunamics[i].recordset['fldID'].value;

                        var oOption = document.createElement("OPTION");
                        oOption.text = optionStr;
                        oOption.value = optionValue;
                        aCmbsDynamics[i].add(oOption);
                        
                        aDSOsDunamics[i].recordset.MoveNext();
                    }
                }
            }    
        }
        
 		lockControlsInModalWin(false);
		window.focus();
	 	
		selFornecedorID.disabled = (selFornecedorID.options.length == 0);
		selClienteID.disabled = (selClienteID.options.length == 0);
		selEstadoID.disabled = (selEstadoID.options.length == 0);
		

		if (glb_FillCmbFirstTime)
		{
			glb_FillCmbFirstTime = false;
			window_onload_2ndPart();
		}
    }
    selEstadoID.selectedIndex = 1;
}

function hasEstoqueChecado()
{
    return null;
    /*
    return (chkEPD.checked || chkEPC.checked || chkETO.checked ||
	chkETD.checked || chkETC.checked);
	*/
}

function chkEstoquesOnClick() {


   btnOK_Status();
  
}


function verificaData(sData) {
    var sData = trimStr(sData);
    var bDataIsValid = true;

    if (sData != '')
        bDataIsValid = chkDataEx(sData);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        return false;
    }
    return true;
}



function relatorioAsstec()
{
    var sEhCliente = '';
    var sFiltro = '';
    var sFiltroMarca = '';
    var sFiltroProprietario = '';
    var sFiltroVendedor = '';
    var sFiltroFornecedor = '';
    var sFiltroCliente = '';
    var sFiltroEstado = '';
    var sFiltroDtInicial = '';
    var sFiltroDtFinal = '';

    var sFiltroEspecifico = selFiltros.value;

    if (sFiltroEspecifico.length <= 0)
        sFiltroEspecifico = 'NULL';
    else
        sFiltroEspecifico = '\'' + ' ' + sFiltroEspecifico + ' ' + '\'';

    var sFiltroDtInicial = (txtDtInicial.value);
    var sFiltroDtFinal = (txtDtFinal.value);


    if (((sFiltroDtInicial != '') || (sFiltroDtFinal != '')) || ((sFiltroDtInicial != '') || (sFiltroDtFinal != ''))) {


        sFiltroDtInicial.toString();
        sFiltroDtFinal.toString();

        verificaData(sFiltroDtInicial);
        verificaData(sFiltroDtFinal);


        var sFiltroDtInicial = '\'' + dateFormatToSearch(sFiltroDtInicial) + '\'';
        var sFiltroDtFinal = '\'' + dateFormatToSearch(sFiltroDtFinal) + '\'';
    }
    else {
        sFiltroDtInicial = null;
        sFiltroDtFinal = null;

    }



    sFiltro = trimStr(txtFiltro.value);

    if (sFiltro != '')
        sFiltro = '\'' + ' ' + sFiltro + ' ' + '\'';
    else
        sFiltro = 'NULL';

    var sEstoques = '';

    if (selFornecedorID.selectedIndex > 0)
        sFiltroFornecedor = selFornecedorID.value;
    else
        sFiltroFornecedor = ' NULL ';

    if (selMarcaID.selectedIndex > 0)
        sFiltroMarca = selMarcaID.value;
    else
        sFiltroMarca = ' NULL ';

    if (selProprietarioID.selectedIndex > 0)
        sFiltroProprietario = selProprietarioID.value;
    else
        sFiltroProprietario = ' NULL ';

    if (selClienteID.selectedIndex > 0)
        sFiltroCliente = selClienteID.value;
    else
        sFiltroCliente = ' NULL ';

    if (selVendedorID.selectedIndex > 0)
        sFiltroVendedor = selVendedorID.value;
    else
        sFiltroVendedor = ' NULL ';

    // pega o estado do asstec
    sFiltroEstado = selEstadoID.value;

 

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var formato = 2;

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&sFiltroEspecifico=" + sFiltroEspecifico + "&chkCliente1=" + (chkCliente1.checked ? 1 : 0) + "&chkCliente2=" + (chkCliente2.checked ? 1 : 0)+
                        "&chkFornecedor1=" + (chkFornecedor1.checked ? 1 : 0) + "&chkFornecedor2=" + (chkFornecedor2.checked ? 1 : 0) + "&sFiltroEstado=" + sFiltroEstado+
                        "&sFiltroFornecedor=" + sFiltroFornecedor+"&sFiltroMarca=" + sFiltroMarca+"&sFiltroProprietario=" + sFiltroProprietario+"&sFiltroCliente=" + sFiltroCliente+
                        "&sFiltroVendedor=" + sFiltroVendedor+"&sFiltroDtInicial=" + sFiltroDtInicial+"&sFiltroDtFinal=" + sFiltroDtFinal+"&sFiltro=" + sFiltro;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modservicos/subsuporte/assistenciatecnica/serverside/Reports_asstec.aspx?' + strParameters;
}

function relatorioEIA()
{
    var nPrintBarcode = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrPrinterBarCode()');
    var nLabelGap = sendJSMessage(getHtmlId(), JS_WIDEMSG, 'LABELBARCODE_GAP', null);
    var empresaData = getCurrEmpresaData();
    var sEmpresaFantasia = empresaData[3];
    var nAsstecID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['AsstecID'].value");
    var nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selProdutoID.value');
    var sProduto;
    var nVias = 1;
    var i;
    
	if (!isNaN(txtVias.value))
		nVias = parseInt(txtVias.value, 10);
        
    sProduto = nProdutoID + ' ' + sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selProdutoID.options(selProdutoID.selectedIndex).innerText') + ' EIA';

	// Reseta o buffer de impressao
	oPrinter.ResetPrinterBarLabel(1, 9, 9);
    
    for (i = 1; i <=nVias; i++)
    {
		oPrinter.InsertBarLabelData(sEmpresaFantasia.substr(0, 13), 
		                            removeCharFromString(sProduto, '"'),
		                            nAsstecID);
	}

	oPrinter.PrintBarLabels(nPrintBarcode, true, LABELBARCODE_HEIGHT, nLabelGap);

    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null);
}

function etiquetaProduto()
{
    var nVias = 1;
    var nLeft = 3;

    if(txtVias2.value!='')
        nVias = parseInt(txtVias2.value,10);
        
	if ( (isNaN(nVias)) || (nVias < 1) )
		nVias = 1;

    // seta os parametros de inicializacao da impressora
    oPrinter.DefaultPrinterNFParams(6,6,10,'N',false);
    oPrinter.ParallelPort = 1;
    oPrinter.ResetPrinter();
    oPrinter.ResetTextForPrint();

    i=0;
	for (i=1; i<=nVias; i++)
	{
	    //oPrinter.SendTextForPrint(nLeft, 'Produto N�o-Conforme' + replicate(' ', 25) + 'RNC:' + replicate('_', 18),'C',false);
		
		oPrinter.SendTextForPrint(nLeft, 'Produto N�o-Conforme' + replicate(' ', 2) + 'OP:' + replicate('_', 18) + '  RNC:' + replicate('_', 18),'C',false);
		oPrinter.MoveToNewLine(2);
		
		oPrinter.SendTextForPrint(nLeft, replicate('_', 67),'C',false);
		oPrinter.MoveToNewLine(1);
		oPrinter.SendTextForPrint(nLeft, replicate('_', 67),'C',false);
		oPrinter.MoveToNewLine(1);
		
		oPrinter.SendTextForPrint(nLeft, 'Colaborador:' + replicate('_', 32) + '  Data:____/____/______','C',false);
		
		oPrinter.FormFeed();
	}

    pb_StopProgressBar(true);
    oPrinter.PrintText();
}

function oPrinter_OnPrintFinish()
{
	winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
}

function fillStaticCmbs()
{
	var strSQL = '';
	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	var nEmpresaID = aEmpresaData[0];
	
	glb_CounterCmbsStatics = 3;
	glb_CounterCmbsDynamics = 3;

	strSQL = 'SELECT 0 as fldID, SPACE(0) AS fldName ' +
		'UNION ' +
		'SELECT DISTINCT d.ConceitoID as fldID, d.Conceito AS fldName ' +
		'FROM Asstec aa WITH(NOLOCK) ' +
			'INNER JOIN RelacoesPesCon a WITH(NOLOCK) ON a.ObjetoID=aa.ProdutoID AND aa.EstadoID=65 ' +
			'INNER JOIN Conceitos c WITH(NOLOCK) ON a.ObjetoID=c.ConceitoID ' +
			'INNER JOIN Conceitos d WITH(NOLOCK) ON c.MarcaID=d.ConceitoID ' +
		'WHERE a.SujeitoID = ' + nEmpresaID + ' AND a.TipoRelacaoID=61 ' +
		'ORDER BY fldName';

	setConnection(dsoMarca);
	dsoMarca.SQL = strSQL;
	dsoMarca.ondatasetcomplete = fillStaticCmbs_DSC;
	dsoMarca.Refresh();

	strSQL = 'SELECT 0 as fldID, SPACE(0) AS fldName ' +
		'UNION ' +
		'SELECT DISTINCT b.PessoaID as fldID, b.Fantasia AS fldName ' +
		'FROM Asstec aa WITH(NOLOCK) ' +
		'INNER JOIN RelacoesPesCon a WITH(NOLOCK) ON a.ObjetoID=aa.ProdutoID AND aa.EstadoID=65 ' +
		'INNER JOIN Pessoas b WITH(NOLOCK) ON a.ProprietarioID=b.PessoaID ' +
		'WHERE a.SujeitoID = ' + nEmpresaID + ' AND a.TipoRelacaoID=61 ' +
		'ORDER BY fldName';

	setConnection(dsoProprietario);
	dsoProprietario.SQL = strSQL;
	dsoProprietario.ondatasetcomplete = fillStaticCmbs_DSC;
	dsoProprietario.Refresh();
	
	strSQL = 'SELECT 0 as fldID, SPACE(0) AS fldName ' +
		'UNION ' +
		'SELECT DISTINCT b.PessoaID as fldID, b.Fantasia AS fldName ' +
		'FROM Asstec aa WITH(NOLOCK) ' +
		'INNER JOIN Asstec_Pessoas bb WITH(NOLOCK) ON aa.AsstecID=bb.AsstecID ' +
		'INNER JOIN RelacoesPessoas a WITH(NOLOCK) ON a.SujeitoID=bb.PessoaID AND a.ObjetoID=aa.EmpresaID ' +
		'INNER JOIN Pessoas b WITH(NOLOCK) ON a.ProprietarioID=b.PessoaID ' +
		'WHERE aa.EstadoID=65 AND bb.EhCliente=1 AND aa.EmpresaID = ' + nEmpresaID + ' AND a.TipoRelacaoID=21 ' +
		'ORDER BY fldName';

	setConnection(dsoVendedor);
	dsoVendedor.SQL = strSQL;
	dsoVendedor.ondatasetcomplete = fillStaticCmbs_DSC;
	dsoVendedor.Refresh();
	
	setConnection(dsoCmbDynamic01);
	dsoCmbDynamic01.SQL = 'SELECT 0 AS Indice, 0 AS fldID, ' + '\'' + '' + '\'' + ' AS fldName ' +
		'UNION ALL ' +
	'SELECT DISTINCT 1 AS Indice, c.PessoaID AS fldID, c.Fantasia AS fldName  ' +
		'FROM Asstec a WITH(NOLOCK) ' +
			'INNER JOIN Asstec_Pessoas b WITH(NOLOCK) ON (a.AsstecID = b.AsstecID) ' +
			'INNER JOIN Pessoas c WITH(NOLOCK) ON (b.PessoaID = c.PessoaID) ' +
		'WHERE (a.EmpresaID = ' + glb_nEmpresaID + ' AND a.EstadoID = 65 AND b.EhCliente = 0) ' +
	'ORDER BY Indice, fldName';

	dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
	dsoCmbDynamic01.Refresh();




	/**************************************************************************/

	setConnection(dsoEstado);
	dsoEstado.SQL = strSQL;
	dsoEstado.ondatasetcomplete = fillStaticCmbs_DSC;
	dsoEstado.Refresh();

	setConnection(dsoEstado);
	dsoEstado.SQL = ' SELECT DISTINCT A.EstadoID AS fldID ,R.RecursoAbreviado AS fldName FROM Recursos R WITH(NOLOCK) ' +
                            ' INNER JOIN Asstec A WITH(NOLOCK) ' +
                            ' ON A.EstadoID = R.RecursoID ' +
                            ' WHERE RecursoAbreviado IS NOT NULL';
	                       

	dsoEstado.ondatasetcomplete = dsoCmbDynamic_DSC;
	dsoEstado.Refresh();
	
	/**************************************************************************/	
	
	
	
  
	setConnection(dsoCmbDynamic02);
	dsoCmbDynamic02.SQL = 'SELECT 0 AS Indice, 0 AS fldID, ' + '\'' + '' + '\'' + ' AS fldName ' +
		'UNION ALL ' +
	'SELECT DISTINCT 1 AS Indice, c.PessoaID AS fldID, c.Fantasia AS fldName  ' +
		'FROM Asstec a WITH(NOLOCK) ' +
			'INNER JOIN Asstec_Pessoas b WITH(NOLOCK) ON (a.AsstecID = b.AsstecID) ' +
			'INNER JOIN Pessoas c WITH(NOLOCK) ON (b.PessoaID = c.PessoaID) ' +
		'WHERE (a.EmpresaID = ' + glb_nEmpresaID + ' AND a.EstadoID = 65 AND b.EhCliente = 1) ' +
	'ORDER BY Indice, fldName';

	dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
	dsoCmbDynamic02.Refresh();
}

function fillStaticCmbs_DSC() {
    glb_CounterCmbsStatics--;
	
	if (glb_CounterCmbsStatics > 0)
		return;

    var i;
    var optionStr, optionValue;
    var aCmbsDynamics;
    var aDSOsDunamics;

    aCmbsDynamics = [selMarcaID, selProprietarioID, selVendedorID, selFornecedorID, selClienteID, selEstadoID];
	aDSOsDunamics = [dsoMarca, dsoProprietario, dsoVendedor, dsoCmbDynamic01, dsoCmbDynamic02, dsoEstado];
	clearComboEx(['selMarcaID', 'selProprietarioID', 'selVendedorID', 'selFornecedorID', 'selClienteID', 'selEstadoID']);

    for (i=0; i<aCmbsDynamics.length; i++)
    {
        if ( !(aDSOsDunamics[i].recordset.BOF && aDSOsDunamics[i].recordset.BOF) )
        {
            aDSOsDunamics[i].recordset.MoveLast();
            aDSOsDunamics[i].recordset.MoveFirst();
            if ( aDSOsDunamics[i].recordset.RecordCount() > 1 )
            {
                while (! aDSOsDunamics[i].recordset.EOF )
                {
                    optionStr = aDSOsDunamics[i].recordset['fldName'].value;
	                optionValue = aDSOsDunamics[i].recordset['fldID'].value;

                    var oOption = document.createElement("OPTION");
                    oOption.text = optionStr;
                    oOption.value = optionValue;
                    aCmbsDynamics[i].add(oOption);
                    
                    sLast_optionStr = optionValue;
                    
                    aDSOsDunamics[i].recordset.MoveNext();
                }
            }
        }    
    }

    selOptByValueInSelect(getHtmlId(), 'selProprietarioID', getCurrUserID());
    selOptByValueInSelect(getHtmlId(), 'selVendedorID', getCurrUserID());
    
    //Altera��o Heraldo. Estado N como default.
    selEstadoID.selectedIndex = 1;
}
