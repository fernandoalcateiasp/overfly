<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalgerarpedidoHtml" name="modalgerarpedidoHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modservicos/subsuporte/assistenciatecnica/modalpages/modalgerarpedido.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modservicos/subsuporte/assistenciatecnica/modalpages/modalgerarpedido.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "var glb_USERID = 0;"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nEmpresaID, nContextoID, nTipoEmpresaID, nEmpresaPaisID, nEmpresaUFID, sCaller

nEmpresaID = 0
nContextoID = 0
nTipoEmpresaID = 0
nEmpresaPaisID = 0
nEmpresaUFID = 0
sCaller = ""

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

For i = 1 To Request.QueryString("nTipoEmpresaID").Count    
    nTipoEmpresaID = Request.QueryString("nTipoEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaPaisID").Count    
    nEmpresaPaisID = Request.QueryString("nEmpresaPaisID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaUFID").Count    
    nEmpresaUFID = Request.QueryString("nEmpresaUFID")(i)
Next

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nContextoID = " & CStr(nContextoID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nTipoEmpresaID = " & CStr(nTipoEmpresaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaPaisID = " & CStr(nEmpresaPaisID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaUFID = " & CStr(nEmpresaUFID) & ";"
Response.Write vbcrlf

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPressEdit>
<!--
 js_fg_modGerPed_KeyPressEdit (fg, arguments[0], arguments[1], arguments[2]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
js_fg_modGerPedAfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=CellChanged>
<!--
js_fg_modGerPedCellChanged (fg, arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modGerPedDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modGerPedBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

</head>

<body id="modalgerarpedidoBody" name="modalgerarpedidoBody" LANGUAGE="javascript" onload="return window_onload()">
     <div id="divProduto" name="divProduto" class="divGeneral">
        <p  id="lblEhCliente" name="lblEhCliente" class="lblGeneral">C</p>
        <input type="checkbox" id="chkEhCliente" name="chkEhCliente" class="fldGeneral" title="� Cliente?"></input>
        <p  id="lblDefeito" name="lblDefeito" class="lblGeneral">D</p>
        <input type="checkbox" id="chkDefeito" name="chkDefeito" class="fldGeneral" title="Defeito"></input>
        <p  id="lblProprio" name="lblProprio" class="lblGeneral">P</p>
        <input type="checkbox" id="chkProprio" name="chkProprio" class="fldGeneral" title="Proprio"></input>
        <p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">Pessoa</p>
        <select id="selPessoaID" name="selPessoaID" class="fldGeneral" title="Pessoa"></select>
        <input type="image" id="btnFindPessoa" name="btnFindPessoa" class="fldGeneral" title="Preencher Combo" LANGUAGE="javascript" onclick="return btnFindPessoa_onclick(this)" WIDTH="24" HEIGHT="23">
        <p id="lblPrestadorID" name="lblPrestadorID" class="lblGeneral">Prestador</p>
        <select id="selPrestadorID" name="selPrestadorID" class="fldGeneral" title = "Prestador"></select>
        <input type="button" id="btnFindPesquisa" name="btnFindPesquisa" value="Listar" LANGUAGE="javascript" onclick="return btnFindPesquisa_onclick(this)" class="btns">

        <p id="lblTransacao" name="lblTransacao" class="lblGeneral">Transa��o</p>
        <select id="selTransacaoID" name="selTransacaoID" class="fldGeneral" title = "Transacao"></select>
        <p id="lblFinanciamentoID" name="lblFinanciamentoID" class="lblGeneral">Prazo</p>
        <select id="selFinanciamentoID" name="selFinanciamentoID" class="fldGeneral" title = "Financiamento"></select>
    </div>    

     <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
    
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
