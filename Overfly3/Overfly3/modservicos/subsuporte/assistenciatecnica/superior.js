/********************************************************************
asstec.js

Library javascript para o asstec.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// controla o retorno do servidor dos dados de combos dinamicos
var glb_CounterCmbsDynamics = 0;

// guarda o botao clicado enviado pelo framework
var glb_BtnFromFramWork;

// guarda o status corrente na barra de status
var glb_lstStatusInStatusBar = '';

// guarda id do combo de lupa a preencher
var glb_cmbLupaID;

//  Dados do registro corrente .SQL 
var dsoSup01 = new CDatatransport("dsoSup01");
//  Descricao do estado atual do registro .SQL 
var dsoStateMachine = new CDatatransport("dsoStateMachine");
//  //@@ Os dsos sao definidos de acordo com o form 
//  Dados dos combos dinamicos (selProdutoID .SQL 
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");
//  Dados dos combos dinamicos (selFornecedorID .SQL 
var dsoCmbDynamic02 = new CDatatransport("dsoCmbDynamic02");
//  Dados dos combos de Lupa .URL 
var dsoCmbsLupa = new CDatatransport("dsoCmbsLupa");
//  Busca data corrente no servidor .URL 
var dsoCurrData = new CDatatransport("dsoCurrData");
//  Verifica asstec no servidor. 
var dsoVerificacao = new CDatatransport("dsoVerificacao");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modservicos/subsuporte/assistenciatecnica/inferior.asp',
                              SYS_PAGESURLROOT + '/modservicos/subsuporte/assistenciatecnica/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'AsstecID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID','txtEstadoID',2,1],
                          ['lblProprio','chkProprio',3,1, -6],
                          ['lblDefeito', 'chkDefeito', 3, 1, -6],
                          ['lblCliente', 'chkCliente', 3, 1, -6],
                          ['lblFornecedor', 'chkFornecedor', 3, 1, -6],
                          ['lbldtAsstec','txtdtAsstec',10,1, -6],
                          ['lblProdutoID','selProdutoID',25,1],
                          ['btnFindProduto','btn',21,1],
                          ['lblNumeroSerie','txtNumeroSerie',20,1],
                          ['lblFornecedorID','selFornecedorID',20,2],
                          ['btnFindFornecedor','btn',21,2],
                          ['lblOrdemProducaoID','txtOrdemProducaoID',10,2],
                          ['lblCaseNumber','txtCaseNumber',15,2],
                          ['lblRMA','txtRMA',15,2],
                          ['lblObservacao','txtObservacao',40,3]], null, null, true);
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWASSTEC')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
            
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        window.top.focus();
        
        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
        
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    //@@
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selProdutoID)
    glb_BtnFromFramWork = btnClicked;
    if ( (btnClicked == 'SUPDET') ||
         (btnClicked == 'SUPREFR') ||
         (btnClicked == 'SUPANT') ||
         (btnClicked == 'SUPSEG') )
    {
        startDynamicCmbs();
    }    
    else    
        // volta para a automacao    
	    finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
        
	
	
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    if ( btnClicked == 'SUPINCL' )
    {
   		// funcao que controla campos read-only
		controlReadOnlyFields(false);
		
        if ( !((dsoCurrData.recordset.BOF) && (dsoCurrData.recordset.EOF)) )
        {
			txtdtAsstec.value = dsoCurrData.recordset['currDate'].value;
        }
        
        //txtEstoque.title = '';      
    }
    else
    {
        setLabelProduto(dsoSup01.recordset['ProdutoID'].value);
        setLabelFornecedor(dsoSup01.recordset['FornecedorID'].value);
        //tEstoque.title = dsoSup01.recordset['EstoqueTitle'].value;
    }
    
    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
    {
        adjustSupInterface();
    }    
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    //@@ padrao do frame work
    verifyAsstecInServer(currEstadoID, newEstadoID);
	return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
    // Produto
    if ( btnClicked.id == 'btnFindProduto' )
        openModalPesqprod(btnClicked);
    // Fornecedor
    else if ( btnClicked.id == 'btnFindFornecedor' )
        getDataAndLoadCmbsLupaNoModal('selFornecedorID');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	var nEstadoID = 0;
	var nEstoqueID = 0;
	var sAnchor = '';
    var empresa = getCurrEmpresaData();
	var nOrdemProducaoID = 0;

	if (controlBar=='SUP')
	{
		// Documentos
		if (btnClicked == 1) {
			__openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
		}
		if (btnClicked == 2)
			openModalPrint();
		else if (btnClicked == 3)
		{
			nEstadoID = dsoSup01.recordset['EstadoID'].value;
			//nEstoqueID = dsoSup01.recordset['EstoqueID'].value;

			if (nEstadoID == 71)
				sAnchor = '221';		
			else if (nEstadoID == 48)
				sAnchor = '231';		
			else if (nEstadoID == 65)
			{
				if ( (nEstoqueID == 343) || (nEstoqueID == 348) )
					sAnchor = '2314';		
				else if ( (nEstoqueID == 344) || (nEstoqueID == 349) )
					sAnchor = '2315';		
				else if ( nEstoqueID == 346 )
					sAnchor = '2317';		
			}
			window.top.openModalControleDocumento('S', '', 751, null, sAnchor, 'T');
		}	
		else if (btnClicked == 6)
		{
			nOrdemProducaoID = dsoSup01.recordset['OrdemProducaoID'].value;
			sendJSCarrier(getHtmlId(), 'SHOWORDEMPRODUCAO', new Array(empresa[0], nOrdemProducaoID));
		}
	}
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    var nEmpresaID = getCurrEmpresaData();
    
    if (btnClicked == 'SUPINCL')
    {
        if (window.top.overflyGen.Alert('Asstecs s� podem ser criados por meio de um pedido.') == 0)
            return null;

        return true;
                
        /*
        setLabelProduto();
        setLabelFornecedor();
        
        // Obtem data no servidor para preencher
	    dsoCurrData.URL = SYS_ASPURLROOT + '/serversidegenEx/currdate.aspx' + '?currDateFormat=' + escape(DATE_FORMAT);
	    dsoCurrData.ondatasetcomplete = dsoCurrDataComplete_DSC;
	    dsoCurrData.refresh();
	    // para a automacao, para prosseguir na funcao
	    // dsoCurrDataComplete_DSC() - retorno da data do servidor
	    return true;
	    */
    }
    else if (btnClicked == 'SUPOK')
    {
        // Se e um novo registro
        if(dsoSup01.recordset[glb_sFldIDName].value == null)
        {
            // Coloca o ID da Empresa logada no campo do dso
            dsoSup01.recordset['EmpresaID'].value = nEmpresaID[0];
        }
    }

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Modal de impressao
    if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }    
    }
    // Pesquisar produto
    if ( idElement.toUpperCase() == 'MODALPESQPRODHTML' )
    {
        if ( param1 == 'OK' )                
        {
            chkProprio.checked = param2[0];
            transferDataProdutoFromModal(selProdutoID, param2[1], param2[2]);
            
            txtNumeroSerie.value = param2[3];

            setLabelProduto(param2[2]);
            
            // zera o combo FornecedorID e campo correspondente
            // no dso
            clearComboEx(['selFornecedorID']);
            dsoSup01.recordset['FornecedorID'].value = null;
                    
            // Fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            
            // trava o combo FornecedorID
            selFornecedorID.disabled = true;
            
            // destrava o botao de lupa de Fornecedor
            lockBtnLupa(btnFindFornecedor, false);
            
            getDataAndLoadCmbsLupaNoModal('selFornecedorID');
            
            // escreve na barra de status
            //writeInStatusBar('child', 'cellMode', 'Listagem');

            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            // writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
    	if (param1 == 'OK') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    	else if (param1 == 'CANCEL') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // funcao que controla campos read-only
	controlReadOnlyFields(true);
	
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // mostra quatro botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1]);
    // os hints dos botoes especificos
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Pesquisar Produto', 'Gerar Pedido', 'Detalhar OP']);

}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selProdutoID e selFornecedorID)
    glb_CounterCmbsDynamics = 2;

    // parametrizacao do dso dsoCmbDynamic01 (designado para selProdutoID)
    setConnection(dsoCmbDynamic01);
    
    dsoCmbDynamic01.SQL = 'SELECT ConceitoID AS fldID, Conceito AS fldName ' +
                          'FROM Conceitos WITH(NOLOCK) ' +
                          'WHERE ConceitoID = ' + dsoSup01.recordset['ProdutoID'].value;

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();
    
    // parametrizacao do dso dsoCmbDynamic02 (designado para selFornecedorID)
    setConnection(dsoCmbDynamic02);
    
    dsoCmbDynamic02.SQL = 'SELECT PessoaID AS fldID, Fantasia AS fldName ' +
                          'FROM Pessoas WITH(NOLOCK) ' +
                          'WHERE PessoaID = ' + dsoSup01.recordset['FornecedorID'].value;

    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic02.Refresh();
}

/********************************************************************
Funcao disparada pelo programador.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selProdutoID, selFornecedorID];
    var aDSOsDunamics = [dsoCmbDynamic01, dsoCmbDynamic02];

    // Inicia o carregamento de combos dinamicos (selProdutoID
    // e selFornecedorID)

    clearComboEx(['selProdutoID', 'selFornecedorID']);
    
    glb_CounterCmbsDynamics --;
    
    if (glb_CounterCmbsDynamics == 0)
    {
        for (i=0; i<2; i++)
        {
            while (! aDSOsDunamics[i].recordset.EOF )
            {
                optionStr = aDSOsDunamics[i].recordset['fldName'].value;
	        	optionValue = aDSOsDunamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[i].add(oOption);
                aDSOsDunamics[i].recordset.MoveNext();
            }
        }

        setLabelProduto(dsoSup01.recordset['ProdutoID'].value);
        setLabelFornecedor(dsoSup01.recordset['FornecedorID'].value);

        // volta para a automacao
	    finalOfSupCascade(glb_BtnFromFramWork);
	}
	return null;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblProdutoID, de acordo com o ID do 
Produto selecionado

Parametro:
nProdutoID   ID do Produto

Retorno:
nenhum
********************************************************************/
function setLabelProduto(nProdutoID)
{
    if (nProdutoID == null)
        nProdutoID = '';
        
    var sLabel = 'Produto ' + nProdutoID;
    lblProdutoID.style.width = sLabel.length * FONT_WIDTH;
    lblProdutoID.innerText = sLabel;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblFornecedorID, de acordo com o ID do
Fornecedor selecionado

Parametro:
nFornecedorID   ID do Fornecedor

Retorno:
nenhum
********************************************************************/
function setLabelFornecedor(nFornecedorID)
{
    if (nFornecedorID == null)
        nFornecedorID = '';
        
    var sLabel = 'Fornecedor ' + nFornecedorID;
    lblFornecedorID.style.width = sLabel.length * FONT_WIDTH;
    lblFornecedorID.innerText = sLabel;
}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Read Only

Parametro:
bAlteracao - true se Alteracao
		   - false se Inclusao

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields(bAlteracao)
{
    // Campos sempre travados
    //txtEstoque.readOnly = true;


    chkProprio.disabled = true;
    chkDefeito.disabled = true;
    chkCliente.disabled = true;
    chkFornecedor.disabled = true;
    
    //chkDefeito.disabled = (chkProprio.checked);
        
    
    txtNumeroSerie.readOnly = true;
        
    // Alternativas de edicao ou nao
    if ( bAlteracao )
    {
        if ( selProdutoID.options.length != 0 )
            selProdutoID.disabled = false;
        else    
            selProdutoID.disabled = true;
        
        if ( selFornecedorID.options.length != 0 )
            selFornecedorID.disabled = false;
        else    
            selFornecedorID.disabled = true;
    }    
    else
    {
        selProdutoID.disabled = true;
        
        // trava o botao de lupa de Fornecedor
        lockBtnLupa(btnFindFornecedor, true);
        selFornecedorID.disabled = true;
    }
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de pesquisa de produto

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPesqprod()
{
    var htmlPath;
    var strPars = new String();
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];
    var nContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nTipoEmpresaID = aEmpresa[5];
    var nEmpresaPaisID = aEmpresa[1];
    var nEmpresaUFID = aEmpresa[4];
    
    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa);
    strPars += '&nContextoID=' + escape(nContextoID[1]);
    strPars += '&nTipoEmpresaID=' + escape(nTipoEmpresaID);
    strPars += '&nEmpresaPaisID=' + escape(nEmpresaPaisID);
    strPars += '&nEmpresaUFID=' + escape(nEmpresaUFID);
    strPars += '&sCaller=' + escape('S');
    
    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modservicos/subsuporte/assistenciatecnica/modalpages/modalpesqprod.asp'+strPars;
    showModalWin(htmlPath, new Array(672,386));
}

/********************************************************************
Funcao criada pelo programador.
Recolhe dados no servidor e preenche combos de lupa sem janela modal.

Parametro:
idCombo         - id do combo a preencher

Retorno:
nenhum
********************************************************************/
function getDataAndLoadCmbsLupaNoModal(idCombo)
{
    glb_cmbLupaID = idCombo;
        
    var sParam1 = '';
    var sParam2 = '';
    var sNumeroSerie = '';
    var nEmpresaID = getCurrEmpresaData();

    // Combo de Fornecedor depende do combo do produto
    if ( idCombo == 'selFornecedorID' )
    {
        // sParam1 -> ProdutoID
        // sParam2 -> EmpresaID
        
        // Se e um novo registro
        if(dsoSup01.recordset[glb_sFldIDName].value == null)
        {
            sParam1 = selProdutoID.options(selProdutoID.selectedIndex).value;
            sParam2 = nEmpresaID[0];

        }
        else
        {
            sParam1 = dsoSup01.recordset['ProdutoID'].value;
            sParam2 = dsoSup01.recordset['EmpresaID'].value;
        }
    }     
    
    lockInterface(true);
    
    glb_lstStatusInStatusBar = getDataInStatusBar('child', 'cellMode');
    writeInStatusBar('child', 'cellMode', 'Processando', true);
    
    var strPars = new String();
    strPars = '?';
    strPars += 'sComboID=';
    strPars += escape(idCombo.toString());
    strPars += '&sParam1=';
    strPars += escape(sParam1.toString());
    strPars += '&sParam2=';
    strPars += escape(sParam2.toString());
    
    strPars += '&sNumeroSerie=';
    
    strPars += escape(sNumeroSerie);
        
    dsoCmbsLupa.URL = SYS_ASPURLROOT + '/modservicos/subsuporte/assistenciatecnica/serverside/fillcomboslupa.aspx' + strPars;
    dsoCmbsLupa.ondatasetcomplete = getDataAndLoadCmbsLupaNoModal_DSC;
    dsoCmbsLupa.refresh();
}

/********************************************************************
Funcao criada pelo programador.
Recebe dados no servidor e preenche combos de lupa sem janela modal.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function getDataAndLoadCmbsLupaNoModal_DSC() {
    var cmbRef;
    var nFornecedorToSelect = 0;

    cmbRef = document.getElementById(glb_cmbLupaID);

    clearComboEx([glb_cmbLupaID]);
            	    
    var optionStr,optionValue;
    
    while (!dsoCmbsLupa.recordset.EOF)
    {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoCmbsLupa.recordset['fldName'].value;
        oOption.value = dsoCmbsLupa.recordset['fldID'].value;
        cmbRef.add(oOption);
        
        if ( (nFornecedorToSelect == 0) && (dsoCmbsLupa.recordset['FornecedorID'].value != null) )
            nFornecedorToSelect = dsoCmbsLupa.recordset['FornecedorID'].value;
        
        dsoCmbsLupa.recordset.MoveNext();
    }
            	    
    // garante o combo fornecedor deselecionado se alteracao
    if ( (lastBtnUsedInCtrlBar('sup') == 'ALT') && (cmbRef.id == 'selFornecedorID') )
        dsoSup01.recordset['FornecedorID'].value = null;       	    
    
    cmbRef.selectedIndex = -1;
    
    if ( nFornecedorToSelect != 0 )
        dsoSup01.recordset['FornecedorID'].value = nFornecedorToSelect;

    // destrava a interface
    lockInterface(false);
    
    cmbRef.disabled = true;
    
    if ( cmbRef.options.length != 0 )
    {
        // destrava e coloca foco no combo se tem options
        cmbRef.disabled = false;
        cmbRef.focus();
    }

    // escreve na barra de status
    if (glb_lstStatusInStatusBar != '')
        writeInStatusBar('child', 'cellMode', glb_lstStatusInStatusBar);
}

/********************************************************************
Funcao criada pelo programador.
Recebe dados de janela modal e altera combo de produto.

Parametro:
cmbRef       - referencia ao combo a alterar
theText      - texto do combo
theValue     - value do combo

Retorno:
nenhum
********************************************************************/
function transferDataProdutoFromModal(cmbRef, theText, theValue)
{
    clearComboEx([cmbRef.id]);
            	    
    var optionStr,optionValue;
    
    var oOption = document.createElement("OPTION");
    oOption.text = theText;
    oOption.value = theValue;
    cmbRef.add(oOption);
    
    cmbRef.selectedIndex = 0;
    dsoSup01.recordset['ProdutoID'].value = theValue;
}

/********************************************************************
Retorno do servidor quando o usuario clica incluir, na funcao
btnBarNotEspecClicked(controlBar, btnClicked), para a automacao,
vai ao servidor e volta nesta funcao ()

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function dsoCurrDataComplete_DSC() {
    // Prossegue a automacao interrompida no botao de inclusao
    lockAndOrSvrSup_SYS();
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       
    strPars += '&currDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modservicos/subsuporte/assistenciatecnica/modalpages/modalprint.asp'+strPars;
    
    showModalWin(htmlPath, new Array(300,400));
}

/********************************************************************
Verifica Asstec no banco
********************************************************************/
function verifyAsstecInServer(nCurrEstadoID, nNewEstadoID)
{
    var nAsstecID = dsoSup01.recordset['AsstecID'].value;
    var strPars = '';

    strPars = '?nAsstecID=' + escape(nAsstecID);
    strPars += '&nCurrEstadoID=' + escape(nCurrEstadoID);
    strPars += '&nNewEstadoID=' + escape(nNewEstadoID);
    
    dsoVerificacao.URL = SYS_ASPURLROOT + '/modservicos/subsuporte/assistenciatecnica/serverside/verificacaoasstec.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyAsstecInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verifica Asstec
********************************************************************/
function verifyAsstecInServer_DSC() {
    // NULL - Verificacao OK
    var sResultado = dsoVerificacao.recordset['Verificacao'].value;
    
    if ((sResultado == null) || (sResultado == ''))
    {
        stateMachSupExec('OK');
    }    
    else 
    {
        if ( window.top.overflyGen.Alert(sResultado) == 0 )
            return null;
            
        stateMachSupExec('CANC');
    }    
}