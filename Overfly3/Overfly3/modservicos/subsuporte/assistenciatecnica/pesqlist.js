/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form asstec
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
//  Dados da listagem da pesquisa 
var dsoListData01 = new CDatatransport("dsoListData01");
//  Dados dos combos de contexto e filtros 
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");
//  Dados dos proprietarios para o pesqlist .URL 
var dsoPropsPL = new CDatatransport("dsoPropsPL");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'P', 'D', 'C', 'F', 'Cliente', 'Fornecedor', 'Data', 'Produto', 'ID', 'N�mero de S�rie', 'OP', 'Case', 'RMA', 'CorCliente', 'CorFornecedor', 'CorGeral');

    //@@ Formatacao das colunas do grid de pesquisa, definicao opcional
    // padrao do framework = null se variavel nao for definida aqui
    var dTFormat = '';
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', dTFormat, '', '', '', '', '', '', '', '');

    adjustElementsInForm([['lblRefrInf', 'chkRefrInf', 3, 1],
                              ['lblOrdem', 'chkOrdem', 3, 1, -9],
                              ['lblPriorizar', 'chkPriorizar', 3, 1, -9],
                              ['lblRegistrosVencidos', 'chkRegistrosVencidos', 3, 1, -9],
                              ['lblProprietariosPL', 'selProprietariosPL', 15, 1, -5],
                              ['btnGetProps', 'btn', 20, 1],
                              ['lblRegistros', 'selRegistros', 6, 1, -3],
                              ['lblMetodoPesq', 'selMetodoPesq', 9, 1, -9],
                              ['lblPesquisa', 'selPesquisa', 14, 1, -4],
                              ['lblArgumento', 'txtArgumento', 18, 1, -4],
                              ['lblFiltro', 'txtFiltro', 41, 1, -4]]);


    

    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
    var nCorCliente = getColIndexByColKey(fg, 'CorCliente');
    var nCorFornecedor = getColIndexByColKey(fg, 'CorFornecedor');
    var nCorGeral = getColIndexByColKey(fg, 'CorGeral');
    
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1]);
    // os hints dos botoes especificos
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Pesquisar Produto', 'Gerar Pedido', 'Detalhar OP']);
  
    setupEspecBtnsControlBar('sup', 'HHHHHD');

    glb_aCelHint = [[0, 2, 'Produto � pr�prio?'],
        [0, 3, 'Produto est� com defeito?'],
        [0, 4, 'Pend�ncia com cliente?'],
        [0, 5, 'Pend�ncia com fornecedor?']];

    alignColsInGrid(fg, [0, 8, 11]);

    if (fg.Cols > 1) {
        fg.ColHidden(nCorCliente) = true;
        fg.ColHidden(nCorFornecedor) = true;
        fg.ColHidden(nCorGeral) = true;
    }

    var i;

    FillStyle = 1;

    for (i = 1; i < fg.Rows; i++) {
    
        var nCorCliente = getColIndexByColKey(fg, 'CorCliente');
        var nCorFornecedor = getColIndexByColKey(fg, 'CorFornecedor');
        var nCorGeral = getColIndexByColKey(fg, 'CorGeral');
        
        // Cliente
        if (fg.TextMatrix(i, nCorCliente) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'Cliente'), i, getColIndexByColKey(fg, 'Cliente')) = eval(fg.TextMatrix(i, nCorCliente));

        if (fg.TextMatrix(i, nCorFornecedor) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'Fornecedor'), i, getColIndexByColKey(fg, 'Fornecedor')) = eval(fg.TextMatrix(i, nCorFornecedor));

        /*if (fg.TextMatrix(i, nCorGeral) != '')
           fg.Cell(6, i, getColIndexByColKey(fg, 'Data'), i, getColIndexByColKey(fg, 'Data')) = eval(fg.TextMatrix(i, nCorGeral));*/
    }
    FillStyle = 0;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           12
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	// Usuario clicou botao documentos
	if (btnClicked == 1) {
		if (fg.Rows > 1) {
			__openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
		}
		else {
		    window.top.overflyGen.Alert('Selecione um registro.');
		}
	}
	// Pesquisa de produto
    if ( btnClicked == 2)
        openModalPrint();
	// Procedimento
    else if ( btnClicked == 3)
		window.top.openModalControleDocumento('PL', '', 751, null, '23', 'T');
    // Pesquisa de produto
    else if ( btnClicked == 4)
        openModalPesqprod();
    // Gerar Pedido
    else if ( btnClicked == 5)
        openModalGerarPedido();
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Modal de impressao
    if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Pesquisar produto
    if ( idElement.toUpperCase() == 'MODALPESQPRODHTML' )
    {
        if ( param1 == 'OK' )                
        {
            // Fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            //writeInStatusBar('child', 'cellMode', 'Listagem');

            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            // writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }    
    // Gerar Pedido
    if ( idElement.toUpperCase() == 'MODALGERARPEDIDOHTML' )
    {
        if ( param1 == 'OK' )                
        {
			var empresa = getCurrEmpresaData();

            // Fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            //writeInStatusBar('child', 'cellMode', 'Listagem');

            // Carrier para mostrar o pedido
            if ( param2 != null )
            {
                sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], param2));
            }
            
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            // writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
    	if (param1 == 'OK') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    	else if (param1 == 'CANCEL') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearchEx()
{
    var strCustomFields = '';
    strCustomFields += '&asstec_Priorizar=' + escape(chkPriorizar.checked ? '1' : '0');

    return strCustomFields;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de pesquisa de produto

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPesqprod()
{
    var htmlPath;
    var strPars = new String();
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];
    var nContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nTipoEmpresaID = aEmpresa[5];
    var nEmpresaPaisID = aEmpresa[1];
    var nEmpresaUFID = aEmpresa[4];
    
    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa);
    strPars += '&nContextoID=' + escape(nContextoID[1]);
    strPars += '&nTipoEmpresaID=' + escape(nTipoEmpresaID);
    strPars += '&nEmpresaPaisID=' + escape(nEmpresaPaisID);
    strPars += '&nEmpresaUFID=' + escape(nEmpresaUFID);
    strPars += '&sCaller=' + escape('PL');

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modservicos/subsuporte/assistenciatecnica/modalpages/modalpesqprod.asp'+strPars;
    showModalWin(htmlPath, new Array(672,386));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de preenchimento do combo de produto

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalGerarPedido()
{
    var htmlPath;
    var strPars = new String();
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];
    var nContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nTipoEmpresaID = aEmpresa[5];
    var nEmpresaPaisID = aEmpresa[1];
    var nEmpresaUFID = aEmpresa[4];
    
    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa);
    strPars += '&nContextoID=' + escape(nContextoID[1]);
    strPars += '&nTipoEmpresaID=' + escape(nTipoEmpresaID);
    strPars += '&nEmpresaPaisID=' + escape(nEmpresaPaisID);
    strPars += '&nEmpresaUFID=' + escape(nEmpresaUFID);
    strPars += '&sCaller=' + escape('PL');
        
    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modservicos/subsuporte/assistenciatecnica/modalpages/modalgerarpedido.asp'+strPars;
    showModalWin(htmlPath, new Array(1000,545));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    // por questoes esteticas, coloca foco no grid de pesquisa
    if ( fg.disabled == false )
        fg.focus();
    
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       
    strPars += '&currDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modservicos/subsuporte/assistenciatecnica/modalpages/modalprint.asp'+strPars;

   // showModalWin(htmlPath, new Array(368, 370));
    showModalWin(htmlPath, new Array(368, 400));
}