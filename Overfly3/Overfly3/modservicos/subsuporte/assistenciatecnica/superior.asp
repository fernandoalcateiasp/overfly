<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="asstecsup01Html" name="asstecsup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modservicos/subsuporte/assistenciatecnica/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modservicos/subsuporte/assistenciatecnica/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="asstecsup01Body" name="asstecsup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" name="txtEstadoID" class="fldGeneral">
        
        <!---<p  id="lblSuspenso" name="lblSuspenso" class="lblGeneral">S</p>
        <input type="checkbox" id="chkSuspenso" name="chkSuspenso" DATASRC="#dsoSup01" DATAFLD="Suspenso" class="fldGeneral" title="Asstec est� suspensa?"></input> -->

        <p  id="lblProprio" name="lblProprio" class="lblGeneral">P</p>
        <input type="checkbox" id="chkProprio" name="chkProprio" DATASRC="#dsoSup01" DATAFLD="Proprio" class="fldGeneral" title="Produto � pr�prio?"></input>
        
        <p  id="lblDefeito" name="lblDefeito" class="lblGeneral">D</p>
        <input type="checkbox" id="chkDefeito" name="chkDefeito" DATASRC="#dsoSup01" DATAFLD="Defeito" class="fldGeneral" title="Produto est� com defeito?"></input>
        
        <p  id="lblCliente" name="lblCliente" class="lblGeneral">C</p>
        <input type="checkbox" id="chkCliente" name="chkCliente" DATASRC="#dsoSup01" DATAFLD="Cliente" class="fldGeneral" title="Pend�ncia com cliente?"></input>
        
        <p  id="lblFornecedor" name="lblFornecedor" class="lblGeneral">F</p>
        <input type="checkbox" id="chkFornecedor" name="chkFornecedor" DATASRC="#dsoSup01" DATAFLD="Fornecedor" class="fldGeneral" title="Pend�ncia com fornecedor?"></input>
        
        <p id="lbldtAsstec" name="lbldtAsstec" class="lblGeneral">Data</p>
        <input type="text" id="txtdtAsstec" name="txtdtAsstec" DATASRC="#dsoSup01" DATAFLD="V_dtAsstec" class="fldGeneral">
        <p id="lblProdutoID" name="lblProdutoID" class="lblGeneral">Produto</p>
        <select id="selProdutoID" name="selProdutoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ProdutoID"></select>
        <input type="image" id="btnFindProduto" name="btnFindProduto" class="fldGeneral" title="Preencher Combo" WIDTH="24" HEIGHT="23">
        <p id="lblNumeroSerie" name="lblNumeroSerie" class="lblGeneral">N�mero S�rie</p>
        <input type="text" id="txtNumeroSerie" name="txtNumeroSerie" DATASRC="#dsoSup01" DATAFLD="NumeroSerie" class="fldGeneral">

        <p id="lblFornecedorID" name="lblFornecedorID" class="lblGeneral">Fornecedor</p>
        <select id="selFornecedorID" name="selFornecedorID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FornecedorID"></select>
        <input type="image" id="btnFindFornecedor" name="btnFindFornecedor" class="fldGeneral" title="Preencher Combo" WIDTH="24" HEIGHT="23">


        <p id="lblOrdemProducaoID" name="lblOrdemProducaoID" class="lblGeneral">OP</p>
        <input type="text" id="txtOrdemProducaoID" name="txtOrdemProducaoID" DATASRC="#dsoSup01" DATAFLD="OrdemProducaoID" class="fldGeneral">
        <p id="lblCaseNumber" name="lblCaseNumber" class="lblGeneral">Case</p>
        <input type="text" id="txtCaseNumber" name="txtCaseNumber" DATASRC="#dsoSup01" DATAFLD="CaseNumber" class="fldGeneral">
        <p id="lblRMA" name="lblRMA" class="lblGeneral">RMA</p>
        <input type="text" id="txtRMA" name="txtRMA" DATASRC="#dsoSup01" DATAFLD="RMA" class="fldGeneral">
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral">
    </div>
</body>

</html>
