/********************************************************************
inferioresp.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Asstec
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();
    var sLinguaLogada = getDicCurrLang();

    setConnection(dso);

    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.AsstecID,a.ProprietarioID,a.AlternativoID,a.Observacoes FROM Asstec a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.AsstecID = '+idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 27001) // Pessoas
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Asstec_Pessoas WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'AsstecID = ' + idToFind + ' ' +
                  'ORDER BY AstPessoaID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 27002) // Pedidos
    {
        dso.SQL = 'SELECT dbo.fn_Data_Fuso(dbo.fn_Pedido_Datas(c.PedidoID, 1), NULL, ' + nEmpresaData[2] + ') AS dtMovimento, ' +
                          'ISNULL(dbo.fn_Pedido_Datas(c.PedidoID, 1), GETDATE() + 10000) AS dtMovimentoOrderby, ' +
                        'c.PedidoID AS PedidoID, d.NotaFiscal, ' +
				        'CONVERT(VARCHAR, g.OperacaoID) +' + '\' - ' + '\'' + '+  dbo.fn_Tradutor(g.Operacao, 246, ' + sLinguaLogada + ', NULL ) AS Transacao, e.Fantasia AS Pessoa, f.Conceito AS Produto, b.ProdutoID, b.NumeroSerie, c.EhCliente AS EhCliente ' +
                    'FROM NumerosSerie_Movimento a WITH(NOLOCK) ' +
                        'INNER JOIN NumerosSerie b WITH(NOLOCK) ON (a.NumeroSerieID = b.NumeroSerieID) ' +
                        'INNER JOIN Conceitos f WITH(NOLOCK) ON (b.ProdutoID = f.ConceitoID) ' +
                        'INNER JOIN Pedidos c WITH(NOLOCK) ON (a.PedidoID = c.PedidoID) ' +
                        'LEFT OUTER JOIN NotasFiscais d WITH(NOLOCK) ON (c.NotaFiscalID = d.NotaFiscalID AND d.EstadoID=67) ' +
                        'INNER JOIN Pessoas e WITH(NOLOCK) ON (c.PessoaID = e.PessoaID) ' +
                        'INNER JOIN Operacoes g WITH(NOLOCK) ON (c.TransacaoID = g.OperacaoID) ' +
                    'WHERE (a.AsstecID = ' + idToFind + ') ' +
				'UNION ALL ' +
				'SELECT dbo.fn_Data_Fuso(dbo.fn_Pedido_Datas(b.PedidoID, 1), NULL, ' + nEmpresaData[2] + ') AS dtMovimento, ' +
                          'ISNULL(dbo.fn_Pedido_Datas(b.PedidoID, 1), GETDATE() + 10000) AS dtMovimentoOrderby, ' +
                        'b.PedidoID AS PedidoID, c.NotaFiscal, ' +
				        'CONVERT(VARCHAR, f.OperacaoID) +' + '\' - ' + '\'' + '+  dbo.fn_Tradutor(f.Operacao, 246, ' + sLinguaLogada + ', NULL ) AS  Transacao, d.Fantasia AS Pessoa, e.Conceito AS Produto, a.ProdutoID, NULL AS NumeroSerie, b.EhCliente AS EhCliente ' +
                    'FROM Pedidos_ProdutosSeparados a WITH(NOLOCK) ' +
                        'INNER JOIN Conceitos e WITH(NOLOCK) ON (a.ProdutoID = e.ConceitoID) ' +
                        'INNER JOIN Pedidos b WITH(NOLOCK) ON (a.PedidoID = b.PedidoID) ' +
                        'LEFT OUTER JOIN NotasFiscais c WITH(NOLOCK) ON (b.NotaFiscalID = c.NotaFiscalID AND c.EstadoID=67) ' +
                        'INNER JOIN Pessoas d WITH(NOLOCK) ON (b.PessoaID = d.PessoaID) ' +
                        'INNER JOIN Operacoes f WITH(NOLOCK) ON (b.TransacaoID = f.OperacaoID) ' +
                    'WHERE (a.AsstecID = ' + idToFind + ') ' +
    //INICIO DE NOVO NS----------------------------------------------
                'UNION ALL ' +
				'SELECT dbo.fn_Data_Fuso(dbo.fn_Pedido_Datas(b.PedidoID, 1), NULL, ' + nEmpresaData[2] + ') AS dtMovimento, ' +
                          'ISNULL(dbo.fn_Pedido_Datas(b.PedidoID, 1), GETDATE() + 10000) AS dtMovimentoOrderby, ' +
                        'b.PedidoID AS PedidoID, c.NotaFiscal, ' +
				        'CONVERT(VARCHAR, f.OperacaoID) +' + '\' - ' + '\'' + '+  dbo.fn_Tradutor(f.Operacao, 246, ' + sLinguaLogada + ', NULL ) AS Transacao, d.Fantasia AS Pessoa, e.Conceito AS Produto, a.ProdutoID, a.Lote AS NumeroSerie, b.EhCliente AS EhCliente ' +
                    'FROM Pedidos_ProdutosLote a WITH(NOLOCK) ' +
                        'INNER JOIN Conceitos e WITH(NOLOCK) ON (a.ProdutoID = e.ConceitoID) ' +
                        'INNER JOIN Pedidos b WITH(NOLOCK) ON (a.PedidoID = b.PedidoID) ' +
                        'LEFT OUTER JOIN NotasFiscais c WITH(NOLOCK) ON (b.NotaFiscalID = c.NotaFiscalID AND c.EstadoID=67) ' +
                        'INNER JOIN Pessoas d WITH(NOLOCK) ON (b.PessoaID = d.PessoaID) ' +
                        'INNER JOIN Operacoes f WITH(NOLOCK) ON (b.TransacaoID = f.OperacaoID) ' +
                    'WHERE (a.AsstecID = ' + idToFind + ') ' +
	//FIM DE NOVO NS----------------------------------------------                        
                    'ORDER BY dtMovimentoOrderby, EhCliente DESC';
        
        return 'dso01Grid_DSC';
    }
    else if (folderID == 27003) // Servicos
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Asstec_Servicos WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'AsstecID = ' + idToFind + ' ' +
                  'ORDER BY AstServicoID';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
       
    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
	// Pessoas
    else if ( pastaID == 27001)
    {
		if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT a.PessoaID AS PessoaID, b.Fantasia AS Pessoa, ' +
					  'a.ParceiroID AS ParceiroID, c.Fantasia AS Parceiro ' +
					  'FROM Asstec_Pessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
                      'WHERE (a.AsstecID = ' + nRegistroID + ' AND ' +
                      'a.PessoaID = b.PessoaID AND a.ParceiroID = c.PessoaID)';
        }

    }
	// Servicos
    else if ( pastaID == 27003)
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ItemMasculino,ItemID ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoID = 702 ' +
                      'ORDER BY Ordem';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG
 	
    vPastaName = window.top.getPastaName(27001);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 27001); //Pessoas

    vPastaName = window.top.getPastaName(27002);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 27002); //Pedidos

    vPastaName = window.top.getPastaName(27003);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 27003); //Servicos

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;

    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        // Parametros:
        // grid --> O grid a ser tratado
        // dsoData --> Datasource
        // nLine --> Linha do grid a ser gravada
        // aFields --> Array que contem as colunas obrigatorias
        // aID --> string 'PessoaID' mais vlr do PessoaID, se 0 nao grava
        // aColsHiddenWriteable --> Array das colunas escondidas que sao gravaveis
            
        // esta funcao retorna a linha corrente do grid 
        // -1 a gravacao nao sera feita
        // maior que 0 vai gravar o registro
    
        if (folderID == 27001) //Pessoas
            currLine = criticLineGridAndFillDsoEx(fg,currDSO,keepCurrLineInGrid(),[14,15],['AsstecID',registroID],[14,15]);
        else if (folderID == 27003) //Servicos
            currLine = criticLineGridAndFillDsoEx(fg,currDSO,keepCurrLineInGrid(),[0,5],['AsstecID',registroID],[]);

        if (currLine < 0)
            return false;
    }   

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 27001) // Pessoas
    {
        headerGrid(fg,['C',
                       'Pessoa',
                       'Parceiro',
                       'Prev Retorno',
                       'Ext',
                       'Atend',
                       'Atraso',
                       'Gar',
                       'Or�',
                       'TP',
                       'TC',
                       'Or�amento',
                       'Liq',
                       'Observa��o',
                       'PessoaID',
                       'ParceiroID', 
                       'AstPessoaID'], [14,15,16]);
        
        // array de celulas com hint
        // glb_aCelHint = [[Row,Col,'Hint'], [Row,Col,'Hint'], ...]
        glb_aCelHint = [[0,0,'� cliente?'],
                        [0,4,'� problema de atraso Externo?'],
                        [0,7,'Produto est� na garantia?'],
                        [0,8,'Or�amento est� aprovado?'],
                        [0,9,'Tempo previsto'],
                        [0,10,'Tempo cobrado'],
                        [0,12,'Or�amento liquidado?']];
                       
        fillGridMask(fg,currDSO,['EhCliente*',
                                 '^PessoaID^dso01GridLkp^PessoaID^Pessoa*',
                                 '^ParceiroID^dso01GridLkp^ParceiroID^Parceiro*',
                                 'dtPrevisaoRetorno',
                                 'AtrasoExterno',
                                 'PrazoAtendimento',
                                 'AtrasoAtendimento',
                                 'Garantia',
                                 'Orcamento',
                                 'TempoPrevisto',
                                 'TempoCobrado',
                                 'ValorOrcamento',
                                 'Liquidado',
                                 'Observacao',
                                 'PessoaID',
                                 'ParceiroID',
                                 'AstPessoaID'],
                                 ['','','','99/99/9999','','999','999','','','','','999999999.99','','','','',''],
                                 ['','','',dTFormat,'','','','','','','','###,###,##0.00','','','','','']);

        alignColsInGrid(fg,[5,6,9,10,11]);                           

        fg.FrozenCols = 2;
    }
    else if (folderID == 27002) // Pedidos
    {
        headerGrid(fg,['Data           Hora     ',
                       'Pedido',
                       'NF',
                       'Transa��o',
                       'Pessoa',
                       'Produto',
                       'ID',
                       'N�mero de S�rie'],[]);
                       
        fillGridMask(fg,currDSO,['dtMovimento',
                                 'PedidoID',
                                 'NotaFiscal',
                                 'Transacao',
                                 'Pessoa',
                                 'Produto',
                                 'ProdutoID',
                                 'NumeroSerie'],
                                 ['99/99/9999','','','','','','',''],
                                 [dTFormat + ' hh:mm:ss','','','','','','','']);

        alignColsInGrid(fg,[1,2,6]);                           
    }
    else if (folderID == 27003) // Servicos
    {
        headerGrid(fg,['In�cio',
                       'Fim',
                       'Dura��o',
                       'Cobrar',
                       'Acumulado',
                       'Tipo',
                       'Observa��o',
                       'AstServicoID'], [7]);
                       
        fillGridMask(fg,currDSO,['dtServicoInicio',
                                 'dtServicoFim',
                                 '_CALC_Duracao_5*',
                                 'Cobrar',
                                 '_CALC_DuracaoAcumulada_5*',
                                 'TipoServicoID',
                                 'Observacao*',
                                 'AstServicoID'],
                                 ['99/99/9999 99:99','99/99/9999 99:99','999.99','','999.99','','',''],
                                 [dTFormat + ' hh:mm:ss',dTFormat + ' hh:mm:ss','##0.00','','##0.00','','','']);
    }
}
