/********************************************************************
js_especificsup.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister) {

   var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true) 
    {
        sSQL = 'SELECT TOP 1 *, ' + //a.Numero AS Numero, a.FilEmiss,' +
                '0 AS Automatico, ' +
                'dbo.fn_DocumentoTransporte_Modalidade(a.DocumentoTransporteID) AS ModalidadeTransporteID, ' +
                'CONVERT(VARCHAR(10), dtEmissao, 103) AS V_dtEmissao, 0 AS Prop1, 0 AS Prop2, ' +
                'dbo.fn_TransportadoraDados_Identificador(a.TransportadoraArquivoDetalheID) AS Identificador, ' +
                'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID,1) AS ValorTotalParcelasFrete, ' +
                'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID,2) AS ValorDiferenca, ' +
                'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID,3) AS PercentualDiferenca, ' +
                '(CASE dbo.fn_DocumentoTransporte_Pessoa(a.DocumentoTransporteID, 1) ' +
                    'WHEN 1 THEN ' + '\'' + 'Remetente' + '\'' + ' ' +
                    'WHEN 2 THEN ' + '\'' + 'Destinatario' + '\'' + ' ' +
                    'ELSE ' + '\'' + 'Pessoa' + '\'' + ' END) AS LabelPessoa, ' +
                'dbo.fn_Pessoa_Fantasia((SELECT TOP 1 cc.PessoaID FROM ' +
                'DocumentosTransporte_NotasFiscais bb WITH (NOLOCK) ' +
			    'INNER JOIN Pedidos cc WITH (NOLOCK) ON (cc.NotaFiscalID = bb.NotafiscalID) ' +
			    'WHERE bb.DocumentoTransporteID = a.DocumentoTransporteID),0) AS Pessoa, ' +
			    '(SELECT TOP 1 cc.PessoaID FROM ' +
                'DocumentosTransporte_NotasFiscais bb WITH (NOLOCK) ' +
			    'INNER JOIN Pedidos cc WITH (NOLOCK) ON (cc.NotaFiscalID = bb.NotafiscalID) ' +
			    'WHERE bb.DocumentoTransporteID = a.DocumentoTransporteID) AS PessoaID, ' +
		        '(SELECT TOP 1 dd.TransportadoraArquivoID FROM ' +
		        'TransportadoraDados_Detalhes dd WITH(NOLOCK) ' +
		        'WHERE dd.TransportadoraArquivoDetalheID = a.TransportadoraArquivoDetalheID) AS TransportadoraArquivoID, ' +
		        '(SELECT TOP 1 ee.Numero FROM ' +
		        'FaturasTransporte ee WITH(NOLOCK) ' +
	            'INNER JOIN FaturasTransporte_DocumentosTransporte ff WITH(NOLOCK) ON (ee.FaturaID = ff.FaturaID) ' +
	            'WHERE ff.DocumentoTransporteID = a.DocumentoTransporteID) AS NumeroFatura, ' +
                'dbo.fn_Pad(DAY(dtEmissao), 2, ' + '\'' + '0' + '\'' + ', ' + '\'' + 'L' + '\'' + ') AS Dia, ' + 
                'dbo.fn_Pad(MONTH(dtEmissao), 2, ' + '\'' + '0' + '\'' + ', ' + '\'' + 'L' + '\'' + ') AS Mes, ' + 
                'YEAR(dtEmissao) AS Ano, '+ 
                'ISNULL(a.ChaveAcesso,' + '\'' + '\'' +') AS NomeArquivo ' +
                'FROM DocumentosTransporte a WITH(NOLOCK)' +
                'WHERE a.ProprietarioID = ' + nID + ' ORDER BY a.DocumentoTransporteID DESC';
    }
    else 
    {
        sSQL = 'SELECT *, ' + //a.Numero AS Numero, a.FilEmiss,' +
                '(CASE ISNULL(dbo.fn_Log_Usuario(5410, 24330, ' + nID + ', 30, NULL, NULL, 1), 999) WHEN 999 THEN 1 ELSE 0 END) AS Automatico, ' +
                'a.EmpresaID AS EmpresaID, ' +
                'dbo.fn_DocumentoTransporte_Modalidade(a.DocumentoTransporteID) AS ModalidadeTransporteID, ' +
                'CONVERT(VARCHAR(10), dtEmissao, 103) AS V_dtEmissao, 0 AS Prop1, 0 AS Prop2, ' +
                'dbo.fn_TransportadoraDados_Identificador(a.TransportadoraArquivoDetalheID) AS Identificador, ' +
                'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID,1) AS ValorTotalParcelasFrete, ' +
                'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID,2) AS ValorDiferenca, ' +
                'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID,3) AS PercentualDiferenca, ' +
                '(CASE dbo.fn_DocumentoTransporte_Pessoa(a.DocumentoTransporteID, 1) ' +
                    'WHEN 1 THEN ' + '\'' + 'Remetente' + '\'' + ' ' +
                    'WHEN 2 THEN ' + '\'' + 'Destinatario' + '\'' + ' ' +
                    'ELSE ' + '\'' + 'Pessoa' + '\'' + ' END) AS LabelPessoa, ' +
                'dbo.fn_Pessoa_Fantasia((SELECT TOP 1 cc.PessoaID FROM ' +
                'DocumentosTransporte_NotasFiscais bb WITH (NOLOCK) ' +
			    'INNER JOIN Pedidos cc WITH (NOLOCK) ON (cc.NotaFiscalID = bb.NotafiscalID) ' +
			    'WHERE bb.DocumentoTransporteID = a.DocumentoTransporteID),0) AS Pessoa, ' +
			    '(SELECT TOP 1 cc.PessoaID FROM ' +
                'DocumentosTransporte_NotasFiscais bb WITH (NOLOCK) ' +
			    'INNER JOIN Pedidos cc WITH (NOLOCK) ON (cc.NotaFiscalID = bb.NotafiscalID) ' +
			    'WHERE bb.DocumentoTransporteID = a.DocumentoTransporteID) AS PessoaID, ' +
		        '(SELECT TOP 1 dd.TransportadoraArquivoID FROM ' +
		        'TransportadoraDados_Detalhes dd WITH(NOLOCK) ' +
		        'WHERE dd.TransportadoraArquivoDetalheID = a.TransportadoraArquivoDetalheID) AS TransportadoraArquivoID, ' +
		        '(SELECT TOP 1 ee.Numero FROM ' +
		        'FaturasTransporte ee WITH(NOLOCK) ' +
	            'INNER JOIN FaturasTransporte_DocumentosTransporte ff WITH(NOLOCK) ON (ee.FaturaID = ff.FaturaID) ' +
	            'WHERE ff.DocumentoTransporteID = a.DocumentoTransporteID) AS NumeroFatura, ' +
                'dbo.fn_Pad(DAY(dtEmissao), 2, ' + '\'' + '0' + '\'' + ', ' + '\'' + 'L' + '\'' + ') AS Dia, ' +
                'dbo.fn_Pad(MONTH(dtEmissao), 2, ' + '\'' + '0' + '\'' + ', ' + '\'' + 'L' + '\'' + ') AS Mes, ' +
                'YEAR(dtEmissao) AS Ano, ' +
                'ISNULL(a.ChaveAcesso,' + '\'' + '\'' + ') AS NomeArquivo ' +
                'FROM DocumentosTransporte a WITH(NOLOCK)' +
                'WHERE a.DocumentoTransporteID = ' + nID + ' ORDER BY a.DocumentoTransporteID DESC';
    }
       
             
    setConnection(dso);

    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;

    sql = 'SELECT TOP 1 *, ' + //a.Numero AS Numero, a.FilEmiss,' +
                'dbo.fn_DocumentoTransporte_Modalidade(a.DocumentoTransporteID) AS ModalidadeTransporteID, ' +
                'CONVERT(VARCHAR(10), dtEmissao, 103) AS V_dtEmissao, 0 AS Prop1, 0 AS Prop2, ' +
                'dbo.fn_TransportadoraDados_Identificador(a.TransportadoraArquivoDetalheID) AS Identificador, ' +
                'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID,1) AS ValorTotalParcelasFrete, ' +
                'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID,2) AS ValorDiferenca, ' +
                'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID,3) AS PercentualDiferenca, ' +
                '(CASE dbo.fn_DocumentoTransporte_Pessoa(a.DocumentoTransporteID, 1) ' +
                    'WHEN 1 THEN ' + '\'' + 'Remetente' + '\'' + ' ' +
                    'WHEN 2 THEN ' + '\'' + 'Destinatario' + '\'' + ' ' +
                    'ELSE ' + '\'' + 'Pessoa' + '\'' + ' END) AS LabelPessoa, ' +
                'dbo.fn_Pessoa_Fantasia((SELECT TOP 1 cc.PessoaID FROM ' +
                'DocumentosTransporte_NotasFiscais bb WITH(NOLOCK) ' +
			    'INNER JOIN Pedidos cc WITH(NOLOCK) ON (cc.NotaFiscalID = bb.NotafiscalID) ' +
			    'WHERE bb.DocumentoTransporteID = a.DocumentoTransporteID),0) AS Pessoa, ' +
			    '(SELECT TOP 1 cc.PessoaID FROM ' +
                'DocumentosTransporte_NotasFiscais bb WITH(NOLOCK) ' +
			    'INNER JOIN Pedidos cc WITH(NOLOCK) ON (cc.NotaFiscalID = bb.NotafiscalID) ' +
			    'WHERE bb.DocumentoTransporteID = a.DocumentoTransporteID) AS PessoaID, ' +
		        '(SELECT TOP 1 dd.TransportadoraArquivoID FROM ' +
		        'TransportadoraDados_Detalhes dd WITH(NOLOCK) ' +
		        'WHERE dd.TransportadoraArquivoDetalheID = a.TransportadoraArquivoDetalheID) AS TransportadoraArquivoID, ' +
		        '(SELECT TOP 1 ee.Numero FROM ' +
		        'FaturasTransporte ee WITH(NOLOCK) ' +
	            'INNER JOIN FaturasTransporte_DocumentosTransporte ff WITH(NOLOCK) ON (ee.FaturaID = ff.FaturaID) ' +
	            'WHERE ff.DocumentoTransporteID = a.DocumentoTransporteID) AS NumeroFatura, ' +
                'dbo.fn_Pad(DAY(dtEmissao), 2, ' + '\'' + '0' + '\'' + ', ' + '\'' + 'L' + '\'' + ') AS Dia, ' +
                'dbo.fn_Pad(MONTH(dtEmissao), 2, ' + '\'' + '0' + '\'' + ', ' + '\'' + 'L' + '\'' + ') AS Mes, ' +
                'YEAR(dtEmissao) AS Ano, ' +
                'ISNULL(a.ChaveAcesso,' + '\'' + '\'' + ') AS NomeArquivo ' +
                'FROM DocumentosTransporte a WITH(NOLOCK)' +
                'WHERE a.DocumentoTransporteID = 0 ORDER BY a.DocumentoTransporteID DESC';
    
    dso.SQL = sql;          
}              
