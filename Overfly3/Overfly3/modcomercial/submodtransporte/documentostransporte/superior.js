/********************************************************************
Documentos Transporte
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoSup01 = new CDatatransport("dsoSup01");
var dsoStateMachine = new CDatatransport("dsoStateMachine");
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
var dsoCurrData = new CDatatransport("dsoCurrData");
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");
//var dsoCmbDynamic02 = new CDatatransport("dsoCmbDynamic02");
var dsoVerificacao = new CDatatransport("dsoVerificacao");

// Cadastrado
var ESTADO_C = 1;
// Validado
var ESTADO_V = 8;
// Deletar
var ESTADO_Z = 5;
// Pendente
var ESTADO_P = 65;
// Liquidado
var ESTADO_L = 32;

var glb_sLabelPessoa = '';

var glb_sPessoaID = '';

var glb_sLabelTransportadora = '';


// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
putSpecialAttributesInControls()
prgServerSup(btnClicked)
prgInterfaceSup(btnClicked)
optChangedInCmb(cmb)
btnLupaClicked(btnClicked)
finalOfSupCascade(btnClicked)
btnBarClicked(controlBar, btnClicked)
btnBarNotEspecClicked(controlBar, btnClicked)
modalInformForm(idElement, param1, param2)
supInitEditMode()
formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
stateMachOpened( currEstadoID )
stateMachBtnOK( currEstadoID, newEstadoID )
stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
carrierArrived(idElement, idBrowser, param1, param2)
carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);

    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    //glb_aStaticCombos = ([['selUFDesembaraco', '1'], ['selTipoFreteID', '2']]);

    glb_aStaticCombos = ([['selTipoRegistroID', '1'],
                          ['selTipoTransporteID', '2'],
                          ['selCondicaoFreteID', '3'],
                          ['selModalidadeTransporteID', '4']]);
    
    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modcomercial/submodtransporte/documentostransporte/inferior.asp',
                              SYS_PAGESURLROOT + '/modcomercial/submodtransporte/documentostransporte/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);

    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'DocumentoTransporteID';
      
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';

    glb_BtnFromFramWork = '';
    //startDynamicCmbs();
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
     adjustDivs('sup',[[1,'divSup01_01']]);

     //@@ *** Div principal superior divSup01_01 ***/

     adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 8, 1],
						['lblEstadoID', 'txtEstadoID', 2, 1],
						['lblDocumentoTransporte', 'txtDocumentoTransporte', 8, 1],
						['lblSerie', 'txtSerie', 5, 1],
						['lblDtEmissao', 'txtDtEmissao', 10, 1],
                        //['lblCFOPID', 'selCFOPID', 10, 1, -9],
						['lblCFOP', 'txtCFOP', 5, 1, -9],
						['lblTipoTransporteID', 'selTipoTransporteID', 15, 1],
						['lblCondicaoFreteID', 'selCondicaoFreteID', 7, 1],
						['lblModalidadeTransporteID', 'selModalidadeTransporteID', 15, 1],
						['lblIdentificador', 'txtIdentificador', 12, 1],
						['lblAutomatico', 'chkAutomatico', 3, 1],
						['lblTipoRegistroID', 'selTipoRegistroID', 10, 1],
						['lblTransportadoraID', 'selTransportadoraID', 30, 2],
						['btnFindTransportadora', 'btn', 24, 2],
						['lblPessoa', 'txtPessoa', 33, 2],
						['lblFilialEmissora', 'txtFilialEmissora', 33, 2],
						['lblPesoTransportado', 'txtPesoTransportado', 11, 3],
						['lblValorTotal', 'txtValorTotal', 13, 3],
						['lblValorTotalParcelasFrete', 'txtValorTotalParcelasFrete', 13, 3],
						['lblValorDiferenca', 'txtValorDiferenca', 13, 3, -16],
						['lblPercentualDiferenca', 'txtPercentualDiferenca', 6, 3],
						['lblValorDesconto', 'txtValorDesconto', 13, 3],
						['lblAjusteParcelaFrete', 'txtAjusteParcelaFrete', 13, 3],
                        ['lblObservacao', 'txtObservacao', 30, 3, -8],
						['lblGRIS', 'txtGRIS', 11, 4],
						['lblFretePeso', 'txtFretePeso', 11, 4],
						['lblValorFrete', 'txtValorFrete', 11, 4],
						['lblValorITR', 'txtValorITR', 11, 4],
						['lblValorAdeme', 'txtValorAdeme', 11, 4],
						['lblValorDespacho', 'txtValorDespacho', 11, 4],
						['lblValorPedagio', 'txtValorPedagio', 11, 4, -16],
                        ['lblComplemento', 'txtComplemento', 11, 4, -16],
						['lblBaseCalculo', 'txtBaseCalculo', 11, 5],
						['lblAliquota', 'txtAliquota', 7, 5],
						['lblValorImposto', 'txtValorImposto', 11, 5],
						['lblSubstituicaoTributaria', 'chkSubstituicaoTributaria', 3, 5, -1],
                        ['lblChaveAcesso', 'txtChaveAcesso', 44, 5, -16],
                        ['lblDocumentoTransporteReferenciaID', 'txtDocumentoTransporteReferenciaID', 20, 5, -16],
                        ['lblFatura', 'txtFatura', 15, 5]], null, null, true);

     chkAutomatico.disabled = true;
 }

 // CARRIER **********************************************************

 /********************************************************************
 DISPARAR UM CARRIER:
 Usar a seguinte funcao:

 sendJSCarrier(getHtmlId(),param1, param2);

 Onde:
 getHtmlId() - fixo, remete o id do html que disparou o carrier.
 param1      - qualquer coisa. De uso do programador.
 param2      - qualquer coisa. De uso do programador.

 ********************************************************************/

 /********************************************************************
 Funcao disparada pelo frame work.
 CARRIER CHEGANDO NO ARQUIVO.
 Executa quando um carrier e disparado por outro form.

 Parametros: 
 idElement   - id do html que disparou o carrier
 idBrowser   - id do browser que disparou o carrier
 param1      - parametro um trazido pelo carrier
 param2      - parametro dois trazido pelo carrier

 Retorno:
 Se intercepta sempre retornar o seguinte array:
 array[0] = getHtmlId() ou null
 array[1] = qualquer coisa ou null
 array[2] = qualquer coisa ou null
 Se nao intercepta retornar null
 ********************************************************************/
 function carrierArrived(idElement, idBrowser, param1, param2) 
 {
     if (param1 == 'SHOWDOCUMENTOTRANSPORTE') 
     {
         // Da automacao, deve constar de todos os ifs do carrierArrived
         if (__currFormState() != 0)
             return null;

         // param2 - traz array:
         // o id da empresa do form que mandou
         // o id do registro a abrir
         // No sendJSMessage abaixo:
         // param2[1] = null significa que nao deve paginar
         // param2[2] = null significa que nao ha array de ids

         var empresa = getCurrEmpresaData();

         if (param2[0] != empresa[0])
             return null;

         window.top.focus();

         // Exclusivo para exibir detalhe em form
         showDetailByCarrier(param2[1]);

         return new Array(getHtmlId(), null, null);
     }
     
     // Programador alterar conforme necessario
     return null;
 }

 /********************************************************************
 Funcao disparada pelo frame work.
 CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
 Executa quando um carrier e disparado por este form e apos executar
 nos demais forms abertos, retorna a este form.

 Parametros: 
 idElement   - string id do html que interceptou o carrier
 idBrowser   - string id do browser que contem o form que interceptou
 o carrier
 param1      - parametro um trazido pelo carrier
 param2      - parametro dois trazido pelo carrier

 Retorno:
 Sempre retorna null
 ********************************************************************/
 function carrierReturning(idElement, idBrowser, param1, param2) 
 {
     // Nao mexer - Inicio de automacao ==============================
     return null;
     // Final de Nao mexer - Inicio de automacao =====================
 }
 // FINAL DE CARRIER *************************************************

 /********************************************************************
 Funcao disparada pelo frame work.
 Coloca atributos em campos para serem manobrados graficamente
 (esconde/mostra e habilita desabilita) pelo framework.

 Parametros: 
 nenhum

 Retorno:
 nenhum
 ********************************************************************/
 function putSpecialAttributesInControls() {
     //@@

     // Tabela de atributos especiais:
     // 1. fnShowHide - seta funcao do programador que o framework executa
     //                 ao mostrar/esconder o controle.
     // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
     //                 habiitar/desabilitar o controle e transfere para
     //                 o programador. False ou null devolve para o framework.

 }

 /********************************************************************
 Funcao disparada pelo frame work.
 Esta funcao faz a chamada de todas as operacoes particulares de servidor
 do e preenchimentos de combos do sup, quando o usuario clica
 um botao da barra do sup.
           
 Parametros: 
 ultimo botao clicado na barra de botoes superior

 Retorno:
 nenhum
 ********************************************************************/
 function prgServerSup(btnClicked) {
     // chama funcao do programador que inicia o carregamento
     // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
     glb_BtnFromFramWork = btnClicked;

     var sLabelPessoa = dsoSup01.recordset["LabelPessoa"].value;
     var sPessoaID = dsoSup01.recordset["PessoaID"].value;

     if (sPessoaID == null)
         sPessoaID = '';

     if (sLabelPessoa == null)
         sLabelPessoa = '';

     var sLabelDestRem = sLabelPessoa + ' ' + sPessoaID;
     var TransportadoraID = dsoSup01.recordset["TransportadoraID"].value;

     if (TransportadoraID == null)
         TransportadoraID = '';
     var sLabelTransportadora = 'Transportadora ' + TransportadoraID;

     setLabelTransportadora(sLabelTransportadora);
     setLabelPessoa(sLabelDestRem);


     //Ir� Alterar Label Tipo
     var sTipoTransporteID = dsoSup01.recordset["TipoTransporteID"].value;

     if (sTipoTransporteID == null)
         sTipoTransporteID = '';

     var sLabelTipo = 'Tipo Transporte ' + sTipoTransporteID;

     setLabelTipo(sLabelTipo);

     //Ir� alterar label Tipo de Registro     
     var sTipoRegistroID = dsoSup01.recordset["TipoDocumentoTransporteID"].value;
     var sLabelTipoRegrstro = 'Tipo ' + (sTipoRegistroID == null ? '' : sTipoRegistroID);
     setLabelTipoRegistro(sLabelTipoRegrstro);

     //Ir� alterar labels de impostos
     setLabelImpostos();
     
     //Ir� Alterar Label Frete
     var sCondicaoFreteID = dsoSup01.recordset["CondicaoFreteID"].value;

     if (sCondicaoFreteID == null)
         sCondicaoFreteID = '';

     var sLabelCondicaoFrete = 'Frete ' + sCondicaoFreteID;

     setLabelCondicaoFrete(sLabelCondicaoFrete);

     //Ir� Alterar Label Modalidade
     var sModalidadeTransporteID = dsoSup01.recordset["ModalidadeTransporteID"].value;

     if (sModalidadeTransporteID == null)
         sModalidadeTransporteID = '';

     var sLabelModalidadeTransporte = 'Modalidade ' + sModalidadeTransporteID;

     setLabelModalidadeTransporte(sLabelModalidadeTransporte);
     
     setLabelCTE();

     if ((btnClicked == 'SUPDET') ||
         (btnClicked == 'SUPREFR') ||
         (btnClicked == 'SUPANT') ||
         (btnClicked == 'SUPSEG'))
     {
         startDynamicCmbsTransp();
         //        startDynamicCmbs();
     }
     else
         // volta para a automacao
         finalOfSupCascade(glb_BtnFromFramWork);
 }

 /********************************************************************
 Funcao disparada pelo frame work.
 Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
 retorno de dados do servidor, feitas pelo programador.
           
 Parametros: 
 nenhum

 Retorno:
 nenhum
 ********************************************************************/
 function finalOfSupCascade(btnClicked) {
     //@@

     // Nao mexer - Inicio de automacao ==============================
     // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
     // para as operacoes de bancos de dados da automacao.
     sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
     // Final de Nao mexer - Inicio de automacao =====================
 }

 /********************************************************************
 Funcao disparada pelo frame work.
 Esta funcao faz a chamada de todas as operacoes particulares
 do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
 OPERACAO DE BANCO DE DADOS.
           
 Parametros: 
 ultimo botao clicado na barra de botoes superior

 Retorno:
 nenhum
 ********************************************************************/
 function prgInterfaceSup(btnClicked) {
     //@@
     // Chama funcoes particulares do form que forem necessarias
     // depois de trocar o div

//    clearAndLockCmbsDynamics(btnClicked);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento']);
    //setupEspecBtnsControlBar('sup', 'DDDH');

     if (btnClicked == 'SUPINCL')
         setReadOnlyFields();

     // Nao mexer - Inicio de automacao ==============================
     sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
     // Final de Nao mexer - Inicio de automacao =====================
 }

 /********************************************************************
 Funcao disparada pelo frame work.
 Esta funcao recebe todos os onchange de combos do form e executa as
 funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
 Parametros: 
 nenhum

 Retorno:
 nenhum
 ********************************************************************/
 function optChangedInCmb(cmb) {
     var cmbID = cmb.id;

     //@@ Os if e os else if abaixo sao particulares de cada form
     // Troca a interface do sup em funcao de item selecionado
     // no combo de tipo.
     // Se for necessario ao programador, repetir o if abaixo
     // definido, antes deste comentario.
     // Nao mexer - Inicio de automacao ==============================
     if (cmbID == 'selTipoRegistroID')
         adjustSupInterface();
     // Final de Nao mexer - Inicio de automacao =====================

     setLabelImpostos();

     setLabelCTE();
 }

 /********************************************************************
 Funcao disparada pelo frame work.
 Dispara logo apos abrir a maquina de estado. 
           
 Parametros: 
 currEstadoID    - EstadoID do registro

 Retorno:
 nenhum
 ********************************************************************/
 function stateMachOpened(currEstadoID) {

 }

 /********************************************************************
 Funcao disparada pelo frame work.
 Usuario clicou OK na maquina de estado
           
 Parametros: 
 currEstadoID    - EstadoID do registro

 Retorno:
 false - automacao prossegue
 true  - automacao para        
       
 Nota:
 Se o programador retornar diferente de null nesta funcao,
 o sistema permanecera travado, ao final do seu codigo o programador
 dever� chamar a seguinte funcao da automacao:
 stateMachSupExec(action)
 onde action = 'OK' - o novo estado sera gravado
 'CANC' - a gravacao sera cancelada
 para prosseguir a automacao
 ********************************************************************/
function stateMachBtnOK(currEstadoID, newEstadoID) {

    verifyDocumentosInServer(currEstadoID, newEstadoID);
    return true;
}


function verifyDocumentosInServer(currEstadoID, newEstadoID) 
{
    var nDocumentoTransporteID = dsoSup01.recordset['DocumentoTransporteID'].value;
    var nTransportadoraArquivoID = dsoSup01.recordset['TransportadoraArquivoID'].value;
    var nTransportadoraArquivoDetalheID = dsoSup01.recordset['TransportadoraArquivoDetalheID'].value;
    var strPars = new String();

    strPars = '?nDocumentoTransporteID=' + escape(nDocumentoTransporteID);
    strPars += '&nEstadoDeID=' + escape(currEstadoID);
    strPars += '&nEstadoParaID=' + escape(newEstadoID);
    strPars += '&nTransportadoraArquivoID=' + escape(nTransportadoraArquivoID);
    strPars += '&nTransportadoraArquivoDetalheID=' + escape(nTransportadoraArquivoDetalheID);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modcomercial/submodtransporte/documentostransporte/serverside/verificacaodocumentostransporte.aspx' + strPars;

    dsoVerificacao.ondatasetcomplete = verifyDocumentosInServer_DSC;
    dsoVerificacao.refresh();
}

 /********************************************************************
 Funcao disparada pelo frame work.
 Dispara se o usuario trocou o estado.
 Dispara logo apos fechar a maquina de estado. 
           
 Parametros: 
 oldEstadoID     - previo EstadoID do registro
 currEstadoID    - novo EstadoID do registro
 bSavedID        - o novo estadoID foi gravado no banco (true/false)

 Retorno:
 nenhum
 ********************************************************************/
 function stateMachClosed(oldEstadoID, currEstadoID, bSavedID) {

 }

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
    //@@
    var cmbID = '';
    var nTipoRelacaoID = '';
    var nRegExcluidoID = '';
    
    if (btnClicked.id == 'btnFindTransportadora')
    {
       openModalTransportadora();
        
    }
}

function btnBarClicked(controlBar, btnClicked) {
    var nEstadoID = dsoSup01.recordset['EstadoID'].value;
    var nAnchorID = '';

    // Documentos
    if (btnClicked == 1) {
        __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
    }
    // Relat�rios
    if (btnClicked == 2) {
        openModalPrint();
    }
    // Procedimento
    if (btnClicked == 3) {
        window.top.openModalControleDocumento('S', '', 720, null, null, 'T');
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
- Avancar, retroagir, incluir, alterar, estado, 
excluir, OK, cancelar refresh, anterior e proximo. 
(Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    var nEmpresaID = getCurrEmpresaData();
    var nEstadoID = getCurrEstadoID();
    var sResultado;



    if (btnClicked == 'SUPOK') {
        // Se e um novo registro
        if (dsoSup01.recordset[glb_sFldIDName] == null || dsoSup01.recordset[glb_sFldIDName].value == null)
            dsoSup01.recordset['EmpresaID'].value = nEmpresaID[0];


        //Valida��es Insert
        if ((txtDocumentoTransporte.value == null) || (txtDocumentoTransporte.value == '')) {
            sResultado = 'O Campo Numero n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
        else if ((txtSerie.value == null) || (txtSerie.value == '')) {
            sResultado = 'O Campo Serie n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
        else if ((txtDtEmissao.value == null) || (txtDtEmissao.value == '')) {
            sResultado = 'O Campo Data Emiss�o n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }

        else if (!((txtDtEmissao.value == null) || (txtDtEmissao.value == ''))) {

            var dtHoje = getCurrDate();
            var dtEmissao = (txtDtEmissao.value); //Data de Emissao digitada pelo usuario
            var dtHojeOK = parseInt(ComparisonDate(dtHoje, null)); //dtHoje formatada como YYYYMMDD para compara��o
            var dtEmissaoOK = parseInt(ComparisonDate(dtEmissao, null)); //dtEmissao formatada como YYYYMMDD para compara��o

            if (dtEmissaoOK > dtHojeOK) {
                sResultado = 'Data inv�lida';
                window.top.overflyGen.Alert(sResultado);
                return false;
            }
        }
/*        else if (((selCFOPID.value == "null") || (selCFOPID.value == ''))  && (selTipoRegistroID.value == 990)) {
            sResultado = 'O Campo CFOPID n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
        else if (((selCFOPID.value != "null") && (selCFOPID.value != '')) && (selTipoRegistroID.value != 990)) {
            sResultado = 'N�o preencher campo CFOP para Rps ou Minutas...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
*/

        else if ((txtCFOP.value == null) || (txtCFOP.value == '')) {
            sResultado = 'O Campo CFOP n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }

        else if ((selTipoTransporteID.value == null) || (selTipoTransporteID.value == '')) {
            sResultado = 'O Campo Tipo n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
        else if ((selCondicaoFreteID.value == null) || (selCondicaoFreteID.value == '')) {
            sResultado = 'O Campo Frete n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
        else if ((selTransportadoraID.value == null) || (selTransportadoraID.value == '')) {
            sResultado = 'O Campo Transportadora n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
        else if ((txtPesoTransportado.value == null) || (txtPesoTransportado.value == '')) {
            sResultado = 'O Campo Peso n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
        else if ((txtValorTotal.value == null) || (txtValorTotal.value == '')) {
            sResultado = 'O Campo Total Documento n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
        else if (((txtBaseCalculo.value == null) || (txtBaseCalculo.value == '')) && (selTipoRegistroID.value == 990)) {
            sResultado = 'O Campo Base ICMS n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
        else if (((txtAliquota.value == null) || (txtAliquota.value == '')) && (selTipoRegistroID.value == 990)) {
            sResultado = 'O Campo Al�q ICMS n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
        else if (((txtValorImposto.value == null) || (txtValorImposto.value == '')) && (selTipoRegistroID.value == 990)) {
            sResultado = 'O Campo Valor ICMS n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
        else if (((txtBaseCalculo.value == null) || (txtBaseCalculo.value == '')) && (selTipoRegistroID.value == 991)) {
            sResultado = 'O Campo Base ISSQN n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
        else if (((txtAliquota.value == null) || (txtAliquota.value == '')) && (selTipoRegistroID.value == 991)) {
            sResultado = 'O Campo Al�q ISSQN n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
        else if (((txtValorImposto.value == null) || (txtValorImposto.value == '')) && (selTipoRegistroID.value == 991)) {
            sResultado = 'O Campo Valor ISSQN n�o foi preenchido...';
            window.top.overflyGen.Alert(sResultado);
            return false;
        }
    }

    else if (btnClicked == 'SUPINCL') {
        clearComboEx(['selTransportadoraID']);
    }

    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Modal de relacoes
    if (idElement.toUpperCase() == 'MODALTRANSPORTADORAHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            fillFieldsByRelationModal(param2);

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode() {

    setReadOnlyFields();

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

function setReadOnlyFields() {

    var identificador = dsoSup01.recordset["Identificador"].value;
    var currEstadoID = dsoSup01.recordset["EstadoID"].value;

    // Campos que sempre s�o travados.
    txtRegistroID.readOnly = true;
    txtPessoa.readOnly = true;
    txtValorDiferenca.readOnly = true;
    txtPercentualDiferenca.readOnly = true;
    txtIdentificador.readOnly = true;

    // estado do registro EDI
    if (identificador != null) {
        txtDocumentoTransporte.disabled = true;
        txtSerie.disabled = false;
        txtDtEmissao.disabled = true;
        //selCFOPID.disable = true;
        txtCFOP.disabled = true;
        selTipoTransporteID.disabled = false;
        selCondicaoFreteID.disabled = true;
        selModalidadeTransporteID.disabled = true;
        selTransportadoraID.disabled = true;
        txtPessoa.disabled = true;
        txtPesoTransportado.disabled = true;
        txtValorTotal.disabled = true;
        txtValorTotalParcelasFrete.disabled = true;
        txtValorDiferenca.disabled = true;
        txtPercentualDiferenca.disabled = true;
        txtValorDesconto.disabled = true;
        txtAjusteParcelaFrete.disabled = true;
        txtBaseCalculo.disabled = true;
        txtAliquota.disabled = true;
        txtValorImposto.disabled = true;
        txtGRIS.disabled = true;
        txtFretePeso.disabled = true;
        txtValorFrete.disabled = true;
        txtValorITR.disabled = true;
        txtValorAdeme.disabled = true;
        txtValorDespacho.disabled = true;
        txtValorPedagio.disabled = true;
        txtFilialEmissora.disabled = true;
    }
    // estado do registro for C - Inclus�o de um novo Documento
    else if (((currEstadoID == ESTADO_C) || (currEstadoID == null)) && (identificador == null)) {
        txtDocumentoTransporte.disabled = false;
        txtSerie.disabled = false;
        txtDtEmissao.disabled = false;
        //selCFOPID.disable = false;
        txtCFOP.disable = false;
        selTipoTransporteID.disabled = false;
        selTipoTransporteID.value = 990;
        selCondicaoFreteID.disabled = false;
        selModalidadeTransporteID.disabled = true;
        selTransportadoraID.disabled = true;
        txtPessoa.disabled = true;
        txtPesoTransportado.disabled = false;
        txtValorTotal.disabled = false;
        txtValorTotalParcelasFrete.disabled = true;
        txtValorDiferenca.disabled = false;
        txtPercentualDiferenca.disabled = false;
        txtValorDesconto.disabled = false;
        txtAjusteParcelaFrete.disabled = false;
        txtBaseCalculo.disabled = false;
        txtAliquota.disabled = false;
        txtValorImposto.disabled = false;
        chkSubstituicaoTributaria.disabled = false;
        txtGRIS.disabled = false;
        txtFretePeso.disabled = false;
        txtValorFrete.disabled = false;
        txtValorITR.disabled = false;
        txtValorAdeme.disabled = false;
        txtValorDespacho.disabled = false;
        txtValorPedagio.disabled = false;
        txtFilialEmissora.disabled = false;

    }

    // estado do registro for C
    if ((currEstadoID == ESTADO_Z || currEstadoID == ESTADO_C) && (dsoSup01.recordset["Automatico"].value == 1)) {
        txtDocumentoTransporte.disabled = true;
        txtSerie.disabled = true;
        txtDtEmissao.disabled = true;
        txtCFOP.disable = true;
        selTipoTransporteID.disabled = true;
        selCondicaoFreteID.disabled = true;
        selModalidadeTransporteID.disabled = true;
        selTransportadoraID.disabled = true;
        selTipoRegistroID.disabled = true;
        txtPessoa.disabled = true;
        txtPesoTransportado.disabled = true;
        txtValorTotal.disabled = true;
        txtValorTotalParcelasFrete.disabled = true;
        txtValorDiferenca.disabled = true;
        txtPercentualDiferenca.disabled = true;
        txtValorDesconto.disabled = true;
        txtAjusteParcelaFrete.disabled = true;
        txtBaseCalculo.disabled = true;
        txtAliquota.disabled = true;
        txtValorImposto.disabled = true;
        chkSubstituicaoTributaria.disabled = true;
        txtGRIS.disabled = true;
        txtFretePeso.disabled = true;
        txtValorFrete.disabled = true;
        txtValorITR.disabled = true;
        txtValorAdeme.disabled = true;
        txtValorDespacho.disabled = true;
        txtValorPedagio.disabled = true;
        txtFilialEmissora.disabled = true;
        txtObservacao.disabled = true;
        lockBtnLupa(btnFindTransportadora, true);
    }    
    
    // estado do registro for V 
    else if (currEstadoID == ESTADO_V)
    {
        txtDocumentoTransporte.disabled = true;
        txtSerie.disabled = true;
        txtDtEmissao.disabled = true;
        //selCFOPID.disable = true;
        txtCFOP.disable = false;
        selTipoTransporteID.disabled = true;
        selCondicaoFreteID.disabled = true;
        selModalidadeTransporteID.disabled = true;
        selTransportadoraID.disabled = true;
        txtPessoa.disabled = true;
        txtPesoTransportado.disabled = true;
        txtValorTotal.disabled = true;
        txtValorTotalParcelasFrete.disabled = true;
        txtValorDiferenca.disabled = true;
        txtPercentualDiferenca.disabled = true;
        txtValorDesconto.disabled = true;
        txtAjusteParcelaFrete.disabled = true;
        txtBaseCalculo.disabled = true;
        txtAliquota.disabled = true;
        txtValorImposto.disabled = true;
        chkSubstituicaoTributaria.disabled = true;
        txtGRIS.disabled = true;
        txtFretePeso.disabled = true;
        txtValorFrete.disabled = true;
        txtValorITR.disabled = true;
        txtValorAdeme.disabled = true;
        txtValorDespacho.disabled = true;
        txtValorPedagio.disabled = true;
        txtFilialEmissora.disabled = true;

    }


    // estado do registro for L 
    else if (currEstadoID == ESTADO_L)
    {
        txtDocumentoTransporte.disabled = true;
        txtSerie.disabled = false;
        txtDtEmissao.disabled = true;
        //selCFOPID.disable = true;
        txtCFOP.disable = false;
        selTipoTransporteID.disabled = false;
        selCondicaoFreteID.disabled = true;
        selModalidadeTransporteID.disabled = true;
        selTransportadoraID.disabled = true;
        txtPessoa.disabled = true;
        txtPesoTransportado.disabled = true;
        txtValorTotal.disabled = true;
        txtValorTotalParcelasFrete.disabled = true;
        txtValorDiferenca.disabled = true;
        txtPercentualDiferenca.disabled = true;
        txtValorDesconto.disabled = true;
        txtAjusteParcelaFrete.disabled = true;
        txtBaseCalculo.disabled = true;
        txtAliquota.disabled = true;
        txtValorImposto.disabled = true;
        chkSubstituicaoTributaria.disabled = true;
        txtGRIS.disabled = true;
        txtFretePeso.disabled = true;
        txtValorFrete.disabled = true;
        txtValorITR.disabled = true;
        txtValorAdeme.disabled = true;
        txtValorDespacho.disabled = true;
        txtValorPedagio.disabled = true;
        txtFilialEmissora.disabled = true;

    }

    // estado do registro for Z 
    else if (currEstadoID == ESTADO_Z)
    {
        txtDocumentoTransporte.disabled = true;
        txtSerie.disabled = true;
        txtDtEmissao.disabled = true;
        //selCFOPID.disable = true;
        txtCFOP.disable = false;
        selTipoTransporteID.disabled = true;
        selCondicaoFreteID.disabled = true;
        selModalidadeTransporteID.disabled = true;
        selTransportadoraID.disabled = true;
        selTipoRegistroID.disabled = false;
        txtPessoa.disabled = true;
        txtPesoTransportado.disabled = true;
        txtValorTotal.disabled = true;
        txtValorTotalParcelasFrete.disabled = true;
        txtValorDiferenca.disabled = true;
        txtPercentualDiferenca.disabled = true;
        txtValorDesconto.disabled = true;
        txtAjusteParcelaFrete.disabled = true;
        txtBaseCalculo.disabled = true;
        txtAliquota.disabled = true;
        txtValorImposto.disabled = true;
        chkSubstituicaoTributaria.disabled = true;
        txtGRIS.disabled = true;
        txtFretePeso.disabled = true;
        txtValorFrete.disabled = true;
        txtValorITR.disabled = true;
        txtValorAdeme.disabled = true;
        txtValorDespacho.disabled = true;
        txtValorPedagio.disabled = true;
        txtFilialEmissora.disabled = true;
        lockBtnLupa(btnFindTransportadora, false);

    }
    // estado do registro for P
    else if (currEstadoID == ESTADO_P)
    {
        txtDocumentoTransporte.disabled = true;
        txtSerie.disabled = false;
        txtDtEmissao.disabled = true;
        //selCFOPID.disable = true;
        txtCFOP.disable = false;
        selTipoTransporteID.disabled = false;
        selCondicaoFreteID.disabled = true;
        selModalidadeTransporteID.disabled = true;
        selTransportadoraID.disabled = true;
        txtPessoa.disabled = true;
        txtPesoTransportado.disabled = true;
        txtValorTotal.disabled = true;
        txtValorTotalParcelasFrete.disabled = true;
        txtValorDiferenca.disabled = true;
        txtPercentualDiferenca.disabled = true;
        txtValorDesconto.disabled = true;
        txtAjusteParcelaFrete.disabled = true;
        txtBaseCalculo.disabled = true;
        txtAliquota.disabled = true;
        txtValorImposto.disabled = true;
        chkSubstituicaoTributaria.disabled = true;
        lockBtnLupa(btnFindTransportadora, true);
        txtGRIS.disabled = true;
        txtFretePeso.disabled = true;
        txtValorFrete.disabled = true;
        txtValorITR.disabled = true;
        txtValorAdeme.disabled = true;
        txtValorDespacho.disabled = true;
        txtValorPedagio.disabled = true;
        txtFilialEmissora.disabled = true;

    }

}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad() {
    // seta botao de impressao
    especBtnIsPrintBtn('sup', 1);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento']);
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Transportadora 

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalTransportadora()
{
    var htmlPath;
    var strPars = "";
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa);
    strPars += '&sCaller=' + escape('S');

    htmlPath = SYS_PAGESURLROOT + '/modalgen/modaltransportadora.asp' + strPars;
    showModalWin(htmlPath, new Array(671, 284));
}

// FINAL DE CARRIER *************************************************




/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbsTransp() 
{
    // parametrizacao do dso dsoCmbDynamic01 (designado para selTransportadoraID)
    setConnection(dsoCmbDynamic01);

    dsoCmbDynamic01.SQL = "SELECT PessoaID AS fldID, Fantasia AS fldName " +
		"FROM Pessoas WITH (NOLOCK) " +
		"WHERE PessoaID = " + dsoSup01.recordset["TransportadoraID"].value;

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic1_DSC;
    dsoCmbDynamic01.refresh();

/*    //CFOPID
    setConnection(dsoCmbDynamic02);

    dsoCmbDynamic02.SQL =   "SELECT NULL AS fldID, '' AS fldName " +
                            "UNION " +
                            "SELECT OperacaoID AS fldID, Codigo + ' (' + Operacao + ')' AS fldName " +
	                            "FROM Operacoes WITH (NOLOCK) " +
	                            "WHERE OperacaoID IN (51530,53510,53520,53530,53570,53600,53610,53630, " +
							                            "63510,63520,63530,63540,63560,63570,69320)";

    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic2_DSC;
    dsoCmbDynamic02.refresh();*/
}  

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{

    // parametrizacao do dso dsoCmbDynamic04 (designado para selTipoTransporteID)
    setConnection(dsoCmbDynamic04);     
    
   dsoCmbDynamic04.SQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName " +
		"FROM TiposAuxiliares_Itens WITH (NOLOCK) " +
		"WHERE EstadoID=2 AND TipoID=422 " + 
		"Order By Ordem";

    dsoCmbDynamic04.ondatasetcomplete = dsoCmbDynamic4_DSC;
    dsoCmbDynamic04.refresh();

    // parametrizacao do dso dsoCmbDynamic05 (designado para selCondicaoFreteID)
    setConnection(dsoCmbDynamic05);

    dsoCmbDynamic05.SQL = "SELECT ItemID AS fldID, ItemAbreviado AS fldName " +
		"FROM TiposAuxiliares_Itens WITH (NOLOCK) " +
		"WHERE EstadoID=2 AND TipoID=403 " +
		"Order By Ordem";

    dsoCmbDynamic05.ondatasetcomplete = dsoCmbDynamic5_DSC;
    dsoCmbDynamic05.refresh();

    // parametrizacao do dso dsoCmbDynamic06 (designado para selModalidadeTransporteID)
    setConnection(dsoCmbDynamic06);

    dsoCmbDynamic06.SQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName " +
		"FROM TiposAuxiliares_Itens WITH (NOLOCK) " +
		"WHERE EstadoID=2 AND TipoID=405  AND Filtro like \'\%<N>%\'\'" +
		"Order By Ordem";

    dsoCmbDynamic06.ondatasetcomplete = dsoCmbDynamic6_DSC;
    dsoCmbDynamic06.refresh();

    // parametrizacao do dso dsoCmbDynamic06 (designado para selModalidadeTransporteID)
    setConnection(dsoCmbDynamic07);

    dsoCmbDynamic07.SQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName " +
		"FROM TiposAuxiliares_Itens WITH (NOLOCK) " +
		"WHERE EstadoID=2 AND TipoID=428 " +
		"Order By Ordem";

    dsoCmbDynamic07.ondatasetcomplete = dsoCmbDynamic7_DSC;
    dsoCmbDynamic07.refresh();
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblPessoa, de acordo com o Tipo da Pessoa Selecionada

Parametro:
sLabelPessoa   Tipo Pessoa

Retorno:
nenhum
********************************************************************/
function setLabelPessoa(sLabelPessoa) {

    if (sLabelPessoa == null)
        sLabelPessoa = '';

    lblPessoa.style.width = sLabelPessoa.length * FONT_WIDTH;
    lblPessoa.innerText = sLabelPessoa;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o labels dos impostos do Documento de Transporte

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function setLabelImpostos() {

    if (selTipoRegistroID.value == 990) {
        sLabelBaseCalculo = 'Base ICMS';
        sLabelAliquota = 'Aliq ICMS';
        sLabelValorImposto = 'Valor ICMS';

        lblBaseCalculo.style.visibility = 'inherit';
        txtBaseCalculo.style.visibility = 'inherit';
        lblAliquota.style.visibility = 'inherit';
        txtAliquota.style.visibility = 'inherit';
        lblValorImposto.style.visibility = 'inherit';
        txtValorImposto.style.visibility = 'inherit';
        lblSubstituicaoTributaria.style.visibility = 'inherit';
        chkSubstituicaoTributaria.style.visibility = 'inherit';
        chkSubstituicaoTributaria.checked = false;
        
         adjustElementsInForm([
        ['lblObservacao', 'txtObservacao', 30, 3, 725],
        ['lblFatura', 'txtFatura', 15, 5, 290]], null, null, true);
        
         lblBaseCalculo.style.width = sLabelBaseCalculo.length * FONT_WIDTH;
         lblBaseCalculo.innerText = sLabelBaseCalculo;
         txtBaseCalculo.value = '';
         lblAliquota.style.width = sLabelAliquota.length * FONT_WIDTH;
         lblAliquota.innerText = sLabelAliquota;
         txtAliquota.value = '';
         lblValorImposto.style.width = sLabelValorImposto.length * FONT_WIDTH;
         lblValorImposto.innerText = sLabelValorImposto;
         txtValorImposto.value = '';
    }
    else if (selTipoRegistroID.value == 991) {
        sLabelBaseCalculo = 'Base ISSQN';
        sLabelAliquota = 'Aliq ISSQN';
        sLabelValorImposto = 'Valor ISSQN';

        lblBaseCalculo.style.visibility = 'inherit';
        txtBaseCalculo.style.visibility = 'inherit';
        lblAliquota.style.visibility = 'inherit';
        txtAliquota.style.visibility = 'inherit';
        lblValorImposto.style.visibility = 'inherit';
        txtValorImposto.style.visibility = 'inherit';
        lblSubstituicaoTributaria.style.visibility = 'hidden';
        chkSubstituicaoTributaria.style.visibility = 'hidden';
        chkSubstituicaoTributaria.checked = false;
        
        adjustElementsInForm([
        ['lblObservacao', 'txtObservacao', 30, 3, 725],
        ['lblFatura', 'txtFatura', 15, 5, 260]], null, null, true);

        lblBaseCalculo.style.width = sLabelBaseCalculo.length * FONT_WIDTH;
        lblBaseCalculo.innerText = sLabelBaseCalculo;
        txtBaseCalculo.value = '';
        lblAliquota.style.width = sLabelAliquota.length * FONT_WIDTH;
        lblAliquota.innerText = sLabelAliquota;
        txtAliquota.value = '';
        lblValorImposto.style.width = sLabelValorImposto.length * FONT_WIDTH;
        lblValorImposto.innerText = sLabelValorImposto;
        txtValorImposto.value = ''; 
    }
    
    else {
        BaseCalculo = 'Base';
        sLabelAliquota = 'Aliq';
        sLabelValorImposto = 'Valor';

        lblBaseCalculo.style.visibility = 'hidden';
        txtBaseCalculo.style.visibility = 'hidden';
        lblAliquota.style.visibility = 'hidden';
        txtAliquota.style.visibility = 'hidden';
        lblValorImposto.style.visibility = 'hidden';
        txtValorImposto.style.visibility = 'hidden';
        lblSubstituicaoTributaria.style.visibility = 'hidden';
        chkSubstituicaoTributaria.style.visibility = 'hidden';

        adjustElementsInForm([
        ['lblObservacao', 'txtObservacao', 30, 3, 725],
        ['lblFatura', 'txtFatura', 15, 5]], null, null, true);

        txtBaseCalculo.value = '';
        txtAliquota.value = '';
        txtValorImposto.value = '';
     }
}

function setLabelCTE()
{
    if (selTipoRegistroID.value == 993 || selTipoRegistroID.value == 994 || selTipoRegistroID.value == 995 || selTipoRegistroID.value == 996)
    {
        lblGRIS.style.visibility = 'hidden';
        txtGRIS.style.visibility = 'hidden';

        lblFretePeso.style.visibility = 'hidden';
        txtFretePeso.style.visibility = 'hidden';

        lblValorFrete.style.visibility = 'hidden';
        txtValorFrete.style.visibility = 'hidden';

        lblValorITR.style.visibility = 'hidden';
        txtValorITR.style.visibility = 'hidden';

        lblValorAdeme.style.visibility = 'hidden';
        txtValorAdeme.style.visibility = 'hidden';
         
        lblValorDespacho.style.visibility = 'hidden';
        txtValorDespacho.style.visibility = 'hidden';

        lblValorPedagio.style.visibility = 'hidden';
        txtValorPedagio.style.visibility = 'hidden';

        lblComplemento.style.visibility = 'inherit';
        txtComplemento.style.visibility = 'inherit';

        lblChaveAcesso.style.visibility = 'inherit';
        txtChaveAcesso.style.visibility = 'inherit';

        lblDocumentoTransporteReferenciaID.style.visibility = 'inherit';
        txtDocumentoTransporteReferenciaID.style.visibility = 'inherit';

        adjustElementsInForm([['lblComplemento', 'txtComplemento', 120, 4],
                              ['lblChaveAcesso', 'txtChaveAcesso', 44, 5],
                              ['lblDocumentoTransporteReferenciaID', 'txtDocumentoTransporteReferenciaID', 20, 5],
                              ['lblFatura', 'txtFatura', 15, 5]
                              ], null, null, true);

    }
    else
    {

        lblGRIS.style.visibility = 'inherit';
        txtGRIS.style.visibility = 'inherit';

        lblFretePeso.style.visibility = 'inherit';
        txtFretePeso.style.visibility = 'inherit';

        lblValorFrete.style.visibility = 'inherit';
        txtValorFrete.style.visibility = 'inherit';

        lblValorITR.style.visibility = 'inherit';
        txtValorITR.style.visibility = 'inherit';

        lblValorAdeme.style.visibility = 'inherit';
        txtValorAdeme.style.visibility = 'inherit';

        lblValorDespacho.style.visibility = 'inherit';
        txtValorDespacho.style.visibility = 'inherit';

        lblValorPedagio.style.visibility = 'inherit';
        txtValorPedagio.style.visibility = 'inherit';

        lblComplemento.style.visibility = 'hidden';
        txtComplemento.style.visibility = 'hidden';

        lblChaveAcesso.style.visibility = 'hidden';
        txtChaveAcesso.style.visibility = 'hidden';

        lblDocumentoTransporteReferenciaID.style.visibility = 'hidden';
        txtDocumentoTransporteReferenciaID.style.visibility = 'hidden';

    }

}
    

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblPessoa, de acordo com o Tipo da Pessoa Selecionada

Parametro:
sLabelTipo   Tipo 

Retorno:
nenhum
********************************************************************/
function setLabelTipo(sLabelTipo)
{
    if (sLabelTipo == null)
        sLabelTipo = '';

    lblTipoTransporteID.style.width = sLabelTipo.length * FONT_WIDTH;
    lblTipoTransporteID.innerText = sLabelTipo;
}


/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblPessoa, de acordo com o Tipo da Pessoa Selecionada

Parametro:
sLabelCondicaoFrete   CondicaoFrete 

Retorno:
nenhum
********************************************************************/
function setLabelCondicaoFrete(sLabelCondicaoFrete)
{
    if (sLabelCondicaoFrete == null)
        sLabelCondicaoFrete = '';

    lblCondicaoFreteID.style.width = sLabelCondicaoFrete.length * FONT_WIDTH;
    lblCondicaoFreteID.innerText = sLabelCondicaoFrete;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblPessoa, de acordo com o Tipo da Pessoa Selecionada

Parametro:
sLabelCondicaoFrete   CondicaoFrete 

Retorno:
nenhum
********************************************************************/
function setLabelTipoRegistro(sLabelTipoRegistro) {
    if (sLabelTipoRegistro == null)
        sLabelTipoRegistro = '';

    lblTipoRegistroID.style.width = sLabelTipoRegistro.length * FONT_WIDTH;
    lblTipoRegistroID.innerText = sLabelTipoRegistro;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblPessoa, de acordo com o Tipo da Pessoa Selecionada

Parametro:
setLabelModalidadeTransporte   ModalidadeTransporte 

Retorno:
nenhum
********************************************************************/
function setLabelModalidadeTransporte(sLabelModalidadeTransporte)
{
    if (sLabelModalidadeTransporte == null)
        sLabelModalidadeTransporte = '';

    lblModalidadeTransporteID.style.width = sLabelModalidadeTransporte.length * FONT_WIDTH;
    lblModalidadeTransporteID.innerText = sLabelModalidadeTransporte;
}
/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblPessoa, de acordo com o Tipo da Pessoa Selecionada

Parametro:
sLabelTransportadora   Tipo Pessoa

Retorno:
nenhum
********************************************************************/
function setLabelTransportadora(sLabelTransportadora)
{
    if (sLabelTransportadora == null)
        sLabelTransportadora = '';

    lblTransportadoraID.style.width = sLabelTransportadora.length * FONT_WIDTH;
    lblTransportadoraID.innerText = sLabelTransportadora;
}

/**********************************************************************
 *
 **********************************************************************/
function dsoCmbDynamic1_DSC() {
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selTransportadoraID];
    var aDSOsDunamics = [dsoCmbDynamic01];

    // Inicia o carregamento de combos dinamicos (selTransportadoraID)
    clearComboEx(['selTransportadoraID']);
    
    for (i=0; i<aCmbsDynamics.length; i++)
    {
        while (! aDSOsDunamics[i].recordset.EOF )
        {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
        	optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }
    }
    
    // volta para a automacao
    finalOfSupCascade(glb_BtnFromFramWork);
	
	return null;
}

/*function dsoCmbDynamic2_DSC() {
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selCFOPID];
    var aDSOsDunamics = [dsoCmbDynamic02];

    // Inicia o carregamento de combos dinamicos (selTransportadoraID)
    clearComboEx(['selCFOPID']);

    for (i = 0; i < aCmbsDynamics.length; i++) {
        while (!aDSOsDunamics[i].recordset.EOF) {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }
    }

    // volta para a automacao
    finalOfSupCascade(glb_BtnFromFramWork);

    return null;
}*/

function dsoCmbDynamic4_DSC() {
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selTipoTransporteID];
    var aDSOsDunamics = [dsoCmbDynamic04];

    // Inicia o carregamento de combos dinamicos (selTipoTransporteID)
    clearComboEx(['selTipoTransporteID']);

    for (i = 0; i < aCmbsDynamics.length; i++) {
        while (!aDSOsDunamics[i].recordset.EOF) {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
            
        }
    }

    // volta para a automacao
    finalOfSupCascade(glb_BtnFromFramWork);

    return null;
}    
    
function dsoCmbDynamic5_DSC() {
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selCondicaoFreteID];
    var aDSOsDunamics = [dsoCmbDynamic05];

    // Inicia o carregamento de combos dinamicos (selCondicaoFreteID)
    clearComboEx(['selCondicaoFreteID']);

    for (i = 0; i < aCmbsDynamics.length; i++) {
        while (!aDSOsDunamics[i].recordset.EOF) {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }
    }

    // volta para a automacao
    finalOfSupCascade(glb_BtnFromFramWork);

    return null;
}

function dsoCmbDynamic6_DSC() {
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selModalidadeTransporteID];
    var aDSOsDunamics = [dsoCmbDynamic06];

    // Inicia o carregamento de combos dinamicos (selModalidadeTransporteID)
    clearComboEx(['selModalidadeTransporteID']);

    for (i = 0; i < aCmbsDynamics.length; i++) {
        while (!aDSOsDunamics[i].recordset.EOF) {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }
    }

    // volta para a automacao
    finalOfSupCascade(glb_BtnFromFramWork);

    return null;
}

function dsoCmbDynamic7_DSC()
{
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selModalidadeTransporteID];
    var aDSOsDunamics = [dsoCmbDynamic07];

    // Inicia o carregamento de combos dinamicos (selModalidadeTransporteID)
    clearComboEx(['selTipoRegistroID']);

    for (i = 0; i < aCmbsDynamics.length; i++) {
        while (!aDSOsDunamics[i].recordset.EOF) {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }
    }

    // volta para a automacao
    finalOfSupCascade(glb_BtnFromFramWork);

    return null;
}






/********************************************************************
Retorno do servidor - Verificacoes do Documentos
********************************************************************/
function verifyDocumentosInServer_DSC() {
    var sResultado = dsoVerificacao.recordset['Resultado'].value;

    if ((sResultado != null) && (sResultado != '')) {
        stateMachSupExec('CANC');
        window.top.overflyGen.Alert(sResultado);
    }
    else
        stateMachSupExec('OK');
}    








/********************************************************************
Funcao criada pelo programador.
Preenche os campos do subform corrente, com os dados vindos da janela
modal de relacoes

Parametro:
aData   -   array de dados
            aData[0] - quem e na relacao ('SUJ' ou 'OBJ')
            aData[1] - ID do tipo da relacao
            aData[2] - ID do campo do subform corrente
            aData[3] - ID selecionado no grid da modal
            aData[4] - texto correspondente ao ID selecionado no grid da modal
            
            os demais elementos sao opcionais

Retorno:
nenhum
********************************************************************/
function fillFieldsByRelationModal(aData)
{
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var elem;
    var addSexOrServ = null;

    clearComboEx(['selTransportadoraID']);

    oOption = document.createElement("OPTION");
    oOption.text = aData[2];
    oOption.value = aData[0];
    selTransportadoraID.add(oOption);
    selTransportadoraID.selectedIndex = 0;
}

function getCurrDate() {
    var d, s = "";
    d = new Date();

    if (DATE_SQL_PARAM == 103) {
        s += padL(d.getDate().toString(), 2, '0') + "/";
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
    }
    else {
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
        s += padL(d.getDate().toString(), 2, '0') + "/";
    }

    s += d.getYear();

    return (s);
}

/********************************************************************
Fun��o para compara��o de data
EX: Par�metro 01/23/2012
Retorno 20120123
    
for�ar formato => 1 = ingles
********************************************************************/
function ComparisonDate(par, formato) {
    //formato 0 = portugues; 1 = ingles
    var sfinalDate = "";
    var aParts = new Array();

    try {
        aParts = par.split('/');
        if (aParts[0].length == par.length)
            aParts = par.split('-');

        if (aParts[0].length == 1)
            aParts[0] = "0" + aParts[0];  //Dia      

        if (aParts[1].length == 1)
            aParts[1] = "0" + aParts[1];  //M�s

        if (aParts[2].length == 2)
            aParts[2] = "20" + aParts[2]; //Ano

        //Se o S.O. estiver em Portugu�s
        if (DATE_SQL_PARAM == 103)
            sfinalDate = aParts[2].toString() + aParts[1].toString() + aParts[0].toString();
        //Se o S.O. estiver em outro idioma exceto Portugu�s
        else
            sfinalDate = aParts[2].toString() + aParts[0].toString() + aParts[1].toString();

        //For�a o formato a ser ingles, sobrescrevendo a data gerada no trecho anterior
        if (formato == 1)
            sfinalDate = aParts[2].toString() + aParts[0].toString() + aParts[1].toString();
    }
    catch (e) {
        sfinalDate = '0';
    }

    return (sfinalDate);
}



/*********************************************
Chama modal Visualizar XML
*********************************************/
function openModalViewXML() {
    var htmlPath;
    var nNotaFiscalID = dsoSup01.recordset('NotaFiscalID').value;

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/notasfiscais/modalpages/modalviewnfe.asp';

    // Veio do sup ou do inf?
    var htmlId = (getHtmlId()).toUpperCase();
    //notaFiscalID = dsoSup01.recordset('NotaFiscalID').value;

    // Passar o id do label do combo que vai preencher
    // para configurar a janela
    // e o id do combo que vai preencher, que volta com a mensagem
    // para saber o combo a preencher

    //  &nEmpresaID=' +nEmpresaID +  '&nNotaFiscal=' + nNotaFiscal ;

    var strPars = new String();

    strPars = '?sCaller=' + escape('S');
    strPars += '&NotaFiscalID=' + escape(nNotaFiscalID);

    showModalWin((htmlPath + strPars), new Array(1000, 590));

    return null;
}


/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/

function openModalPrint() {
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars += '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);
    //strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodtransporte/documentostransporte/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(470, 230));
}
