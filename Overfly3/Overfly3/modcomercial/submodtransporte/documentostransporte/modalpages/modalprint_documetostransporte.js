/********************************************************************
modalprint_faturatransporte.js

Library javascript para o modalprint.asp
Relacoes entre Pessoas 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// variavel de timer
var __timerInt;

//  dso usado para envia e-mails 
var dsoEMail = new CDatatransport("dsoEMail");
//  dso usado para verificar o count de DocumentoTransportes
var dsoDocumentoTransporte = new CDatatransport("dsoDocumentoTransporte");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
setupPage()
loadDataAndTreatInterface()
loadReportsInList()
linksDivAndReports()
invertChkBox()
chkBox_Clicked()
selReports_Change()
btnOK_Status()
btnOK_Clicked()
btnInserir_Clicked()
inserir_DSC()
showDivByReportID()
getCurrDivReportAttr()

    
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    glb_bUseProgressBar = true;
    asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalprintBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    window.focus();

    if (selReports.disabled == false)
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();

    btnOK_Status();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    window.focus();

    if (ctl.id == btnOK.id) {
        btnOK.focus();
    }
    else if (ctl.id == btnCanc.id) {
        btnCanc.focus();
    }

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Relatórios', 1);

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
   /* adjustElementsInForm([['lblProgramasMarketing', 'selProgramasMarketing', 20, 1, -10, -10],
                                      ['lblFiltro2', 'txtFiltro2', 30, 2, -10]], null, null, true);
*/

    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;

    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relatório', null);

    adjustElementsInForm([['lblReports', 'selReports', 56, 1, -1, 10],
                            ['lblDtInicio', 'txtDtInicio', 9,2,-1],
                            ['lblDtFim', 'txtDtFim', 9,2,10],
                            ['lblCadastrado', 'chkCadastrado', 3, 3,-1],
                            ['lblValidado', 'ChkValidado', 3, 3],
                            ['lblPendente', 'chkPendente', 3, 3],
                            ['lblDeletado', 'chkDeletado', 3, 3],
                            ['lblFechado', 'chkFechado', 3, 3],
                            ['lblTransportadora', 'txtTransportadora', 12, 2, 178]],
                             null, null, true);

    var lstHeight = 150;
    
    txtDtInicio.onfocus = selFieldContent;
    txtDtInicio.maxLength = 10;
    txtDtInicio.value = glb_dCurrDate;
    txtDtFim.onfocus = selFieldContent;
    txtDtFim.maxLength = 10;
    txtDtFim.value = glb_dCurrDate;

    txtTransportadora.onkeypress = verifyNumericEnterNotLinked;
    txtTransportadora.setAttribute('verifyNumPaste', 1);
    txtTransportadora.setAttribute('thePrecision', 10, 1);
    txtTransportadora.setAttribute('theScale', 0, 1);
    txtTransportadora.setAttribute('minMax', new Array(0, 9999999999), 1);
    txtTransportadora.onfocus = selFieldContent;
    txtTransportadora.maxLength = 7;    
                           
    
    /*
    
    with (lblReports.style) {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // ajusta combo de relatorios
    with (selReports.style) {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }*/

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // O estado do botao btnOK
    btnOK_Status();

    
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    //btnOK.disabled = false;

    selReports.onchange = selReports_Change;

    // carrega a lista de relatorios
    loadReportsInList();

    // linka os divs nos relatorios
    linkDivsAndReports();

    // restringe digitacao em campos numericos
    setMaskAndLengthToInputs();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList() {
    var i;

    if (glb_arrayReports != null) {
        for (i = 0; i < glb_arrayReports.length; i++) {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);

            if (glb_arrayReports[i][2])
                selReports.selectedIndex = i;
        }

        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports()
{
    divDocumentos.setAttribute('report', 40213, 1);
    
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox() {
    var ctl = this.nextSibling;

    if (ctl.disabled == true)
        return;

    ctl.checked = !ctl.checked;

    // O estado do botao btnOK
    btnOK_Status();

    window.focus();
    if (ctl.disabled == false)
        ctl.focus();

    return true;
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change() {
    // mostra o div correspondente ao relatorio
    showDivByReportID();

    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if (this.disabled == false)
        this.focus();

    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status() {
    var btnOKStatus = true;
    var i, elem;

    if (selReports.selectedIndex != -1) {
        if (selReports.value == 40213)
            btnOKStatus = false;
        else if (selReports.value == 40215)
            btnOKStatus = false;
    }

    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked() {
    if (selReports.value == 40213)
        RelatorioDocumentoTransporte();
    else if (selReports.value == 40215)
        carregaXML();
    else
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
}

function showDivByReportID() {
    var i, coll, attr, currRep;

    attr = null;
    currRep = 0;

    currRep = selReports.value;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (attr == currRep)
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr() {
    var i, coll, attr, retVal;

    attr = null;
    retVal = 0;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (coll[i].style.visibility == 'visible') {
                retVal = attr;
                break;
            }
        }
    }
    return retVal;
}

/********************************************************************
Restringe quantidade de caracteres digitados, coloca
mascara para digitacao em campos numericos,
seleciona conteudo do campo quando em foco etc
********************************************************************/
function setMaskAndLengthToInputs() {
    ;
    /*txtEtiquetaVertical.onkeypress = verifyNumericEnterNotLinked;
    txtEtiquetaHorizontal.onkeypress = verifyNumericEnterNotLinked;
    txtAltura.onkeypress = verifyNumericEnterNotLinked;
    txtLargura.onkeypress = verifyNumericEnterNotLinked;
    txtMargemSuperior.onkeypress = verifyNumericEnterNotLinked;
    txtMargemEsquerda.onkeypress = verifyNumericEnterNotLinked;
    txtCEPInicio.onkeypress = verifyNumericEnterNotLinked;
    txtCEPFim.onkeypress = verifyNumericEnterNotLinked;

    txtEtiquetaVertical.setAttribute('verifyNumPaste', 1);
    txtEtiquetaHorizontal.setAttribute('verifyNumPaste', 1);
    txtAltura.setAttribute('verifyNumPaste', 1);
    txtLargura.setAttribute('verifyNumPaste', 1);
    txtMargemSuperior.setAttribute('verifyNumPaste', 1);
    txtMargemEsquerda.setAttribute('verifyNumPaste', 1);
    txtCEPInicio.setAttribute('verifyNumPaste', 1);
    txtCEPFim.setAttribute('verifyNumPaste', 1);

    txtEtiquetaVertical.setAttribute('thePrecision', 2, 1);
    txtEtiquetaHorizontal.setAttribute('thePrecision', 2, 1);
    txtAltura.setAttribute('thePrecision', 2, 1);
    txtLargura.setAttribute('thePrecision', 3, 1);
    txtMargemSuperior.setAttribute('thePrecision', 2, 1);
    txtMargemEsquerda.setAttribute('thePrecision', 2, 1);
    txtCEPInicio.setAttribute('thePrecision', 9, 1);
    txtCEPFim.setAttribute('thePrecision', 9, 1);
    
    txtEtiquetaVertical.setAttribute('theScale', 0, 1);
    txtEtiquetaHorizontal.setAttribute('theScale', 0, 1);
    txtAltura.setAttribute('theScale', 0, 1);
    txtLargura.setAttribute('theScale', 0, 1);
    txtMargemSuperior.setAttribute('theScale', 0, 1);
    txtMargemEsquerda.setAttribute('theScale', 0, 1);
    txtCEPInicio.setAttribute('theScale', 0, 1);
    txtCEPFim.setAttribute('theScale', 0, 1);

    txtEtiquetaVertical.setAttribute('minMax', new Array(0, 99), 1);
    txtEtiquetaHorizontal.setAttribute('minMax', new Array(0, 99), 1);
    txtAltura.setAttribute('minMax', new Array(0, 99), 1);
    txtLargura.setAttribute('minMax', new Array(0, 999), 1);
    txtMargemSuperior.setAttribute('minMax', new Array(0, 99), 1);
    txtMargemEsquerda.setAttribute('minMax', new Array(0, 99), 1);
    txtCEPInicio.setAttribute('minMax', new Array(0, 999999999), 1);
    txtCEPFim.setAttribute('minMax', new Array(0, 999999999), 1);
    
    txtEtiquetaVertical.onfocus = selFieldContent;
    txtEtiquetaHorizontal.onfocus = selFieldContent;
    txtAltura.onfocus = selFieldContent;
    txtLargura.onfocus = selFieldContent;
    txtMargemSuperior.onfocus = selFieldContent;
    txtMargemEsquerda.onfocus = selFieldContent;
    txtCEPInicio.onfocus = selFieldContent;
    txtCEPFim.onfocus = selFieldContent;*/
}

function RelatorioDocumentoTransporte() {
    var nEmpresaID;
    var ndtInicio = txtDtInicio.value;
    var ndtFim = txtDtFim.value;
    var nEstadoID = '';
    var nTransportadoraID = txtTransportadora.value;
    var nFiltroTransportadora;
    var sFiltroEstado;

    if (!verificaData(ndtInicio))
        return null;
    if (!verificaData(ndtFim))
        return null;

    ndtInicio = dateFormatToSearch(ndtInicio);
    ndtFim = dateFormatToSearch(ndtFim);

    if ((chkCadastrado.checked) || (chkValidado.checked) || (chkPendente.checked) || (chkDeletado.checked) || (chkFechado.checked)) {
        if (chkCadastrado.checked) {
            nEstadoID = '1';
        }
        if (chkValidado.checked) {
            nEstadoID += (nEstadoID != '') ? ', 8' : '8';
        }
        if (chkPendente.checked) {
            nEstadoID += (nEstadoID != '') ? ', 65' : '65';
        }
        if (chkDeletado.checked) {
            nEstadoID += (nEstadoID != '') ? ', 5' : '5';
        }
        if (chkFechado.checked) {
            nEstadoID += (nEstadoID != '') ? ', 48' : '48';
        }
    }

    if (txtTransportadora.value != '') 
    {
        nFiltroTransportadora = 'AND TransportadoraID = ' + nTransportadoraID;
    } 
    else 
    {
        nFiltroTransportadora = '';
    }

    if (nEstadoID != '') {
        sFiltroEstado = 'AND a.EstadoID IN (' + nEstadoID + ')';
    }
    else {
        sFiltroEstado = '';
    }


    
    nEmpresaID = getCurrEmpresaData()[0];
    
    var strSQL = 'SELECT COUNT(a.DocumentoTransporteID) AS Quantidade ' +
                    'FROM  DocumentosTransporte a WITH(NOLOCK) ' +
                        'INNER JOIN TransportadoraDados_Detalhes_Erros b WITH(NOLOCK) ON (b.RegistroID = a.DocumentoTransporteID) ' +
                    'WHERE b.CodigoErro = 20 AND a.dtEmissao BETWEEN \'' + ndtInicio + '\' AND \'' + ndtFim + '\' AND b.TipoRegistroID = 1901 ' +
                        'AND a.EmpresaID = ' + nEmpresaID + sFiltroEstado + nFiltroTransportadora;

    setConnection(dsoDocumentoTransporte);
    dsoDocumentoTransporte.SQL = strSQL;
    dsoDocumentoTransporte.ondatasetcomplete = RelatorioDocumentoDiferencaPercentual_DSC;
    dsoDocumentoTransporte.Refresh();

}

function RelatorioDocumentoDiferencaPercentual_DSC() {
    var i;
    var dirA1;
    var dirA2;
    var nEmpresaID;
    var ndtInicio = txtDtInicio.value;
    var ndtFim = txtDtFim.value;
    var nEstadoID = '';
    var nTransportadoraID = txtTransportadora.value;
    var nFiltroTransportadora;
    var sFiltroEstado;

    if (!verificaData(ndtInicio))
        return null;
    if (!verificaData(ndtFim))
        return null;

    ndtInicio = dateFormatToSearch(ndtInicio);
    ndtFim = dateFormatToSearch(ndtFim);

    if ((chkCadastrado.checked) || (chkValidado.checked) || (chkPendente.checked) || (chkDeletado.checked) || (chkFechado.checked))
    {
        if (chkCadastrado.checked) 
        {
            nEstadoID = '1';
        }
        if (chkValidado.checked) 
        {
            nEstadoID += (nEstadoID != '') ? ', 8' : '8';
        }
        if (chkPendente.checked) 
        {
            nEstadoID += (nEstadoID != '') ? ', 65' : '65';
        }
        if (chkDeletado.checked) 
        {
            nEstadoID += (nEstadoID != '') ? ', 5' : '5';
        }
        if (chkFechado.checked) 
        {
            nEstadoID += (nEstadoID != '') ? ', 48' : '48';
        }
    }

    if (txtTransportadora.value != '')
    {
        nFiltroTransportadora = 'AND TransportadoraID = ' + nTransportadoraID;
    } 
    else
    {
        nFiltroTransportadora = '';
    }

    if (nEstadoID != '') {
        sFiltroEstado = 'AND a.EstadoID IN (' + nEstadoID + ')';
    }
    else {
        sFiltroEstado = '';
    }

    
    if (!(dsoDocumentoTransporte.recordset.BOF && dsoDocumentoTransporte.recordset.EOF))
        var quantidadeDocumentoTransporte = dsoDocumentoTransporte.recordset['Quantidade'].value;

    nEmpresaID = getCurrEmpresaData()[0];

    var strParameters = "RelatorioID=" + selReports.value + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&ndtInicio=" + ndtInicio +
						"&ndtFim=" + ndtFim + "&nEmpresaID=" + nEmpresaID + "&sFiltroEstado=" + sFiltroEstado + "&nFiltroTransportadora=" + nFiltroTransportadora +
                        "&quantidadeDocumentoTransporte=" + quantidadeDocumentoTransporte;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modcomercial/submodtransporte/documentostransporte/serverside/Reports_documentosTransporte.aspx?' + strParameters;

}

function verificaData(sData) {
    var sData = trimStr(sData);
    var bDataIsValid = true;

    if (sData != '')
        bDataIsValid = chkDataEx(sData);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data inválida.') == 0)
            return null;

        lockControlsInModalWin(false);
        return false;
    }
    return true;
}

function carregaXML() {
    var caminho = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset(' + '\'' + 'Caminho' + '\'' + ').value');
    var estadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset(' + '\'' + 'EstadoID' + '\'' + ').value');
    var sTipo = "Recebidas";
    var sAutorizada = "Autorizadas";
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var sEmpresaID = aEmpresaData[0];
    var sAno = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Ano' + '\'' + '].value');
    var sMes = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Mes' + '\'' + '].value');
    var sDia = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Dia' + '\'' + '].value');
    var sNomeArquivo = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'NomeArquivo' + '\'' + '].value');


    if (estadoID == 5)
    {
        sAutorizada = "Cancelada";
    }

    if (sEmpresaID.toString().length < 2) {
        sEmpresaID = '0' + sEmpresaID;
    }
    
    
    //Nova estrutura de arquivamentos dos XML's
    var fullPath = SYS_ASPURLROOT + "/NFe/" + sEmpresaID + "/DocumentoEletronico/CT-e/" + sTipo + "/" + sAutorizada + "/" + sAno + "/" + sMes + "/" + sDia + "/" + sNomeArquivo + ".xml";
    window.open(fullPath);

    lockControlsInModalWin(false);
}
