/********************************************************************
aprovacaodocumentotransporte.js

Library javascript para o aprovacaodocumentotransporte.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport("dsoPesq");
var dsoAprovacao = new CDatatransport("dsoAprovacao");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() 
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    var elem = document.getElementById('aprovacaodocumentotransporteHtmlBody');

    with (elem) 
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }


    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
}
/********************************************************************
Configura o html
********************************************************************/

function js_fg_modalAprovacaoDblClick(grid, Row, Col) {

    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if ((Col != getColIndexByColKey(grid, 'Aprovar')))
        return true;

    // trap so header
    if (Row != 1)
        return true;

    var i;
    var bFill = true;

    fg.Editable = false;
    fg.Redraw = 0;
    lockControlsInModalWin(true);



    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 1; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    lockControlsInModalWin(false);
    fg.Editable = true;
    fg.Redraw = 2;
    window.focus();
    fg.focus();

 //   GridLinhaTotal(fg);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() 
{
    // texto da secao01
    secText('Aprovar Documento de Transporte', 1);

    // ajusta elementos da janela
    var elem;
    var temp;

    // ajusta o divDocumentoTransporte
    elem = window.document.getElementById('divDocumentoTransporte');
    with (elem.style) 
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;

    }

    //btnAprovar
    elem = window.document.getElementById('btnAprovar');
    with (elem.style) {
        top = 16;
        left = 0;
        width = 80;
        height = 24;
    }
    btnAprovar.onclick = aprovarDocumentoTransporte;

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 2;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divDocumentoTransporte').style.top) + parseInt(document.getElementById('divDocumentoTransporte').style.height) + ELEM_GAP;
        width = temp + 400;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 100;
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width) + 50;
        height = parseInt(document.getElementById('divFG').style.height) + 63;
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    startPesq();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
}

function startPesq()
{
    lockControlsInModalWin(true);

    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EmpresaID' + '\'' + '].value');
    var nDocumentoTransporteID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'DocumentoTransporteID' + '\'' + '].value');

    setConnection(dsoPesq);

    dsoPesq.SQL = 'SELECT CONVERT(BIT, 0) AS Aprovar, a.CodigoErro, b.MensagemErro, dbo.fn_Pessoa_Fantasia(a.AprovadorID, 0) AS Aprovador, ' +
                            'dtAprovacao, TransportadoraErroID ' +
	                    'FROM dbo.TransportadoraDados_Detalhes_Erros a WITH(NOLOCK) ' +
	                    'INNER JOIN FreteErros b WITH(NOLOCK) ON (b.FreteErroID = a.CodigoErro) ' +
	                    'WHERE a.RegistroID = ' +
	                        '(SELECT aa.DocumentoTransporteID FROM dbo.DocumentosTransporte aa WITH(NOLOCK) WHERE aa.DocumentoTransporteID = ' + nDocumentoTransporteID + ') ' +
	                        'AND (a.AprovadorID IS NULL) AND b.PermiteAprovacao = 1 ' +
                        'ORDER BY a.CodigoErro';

    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC() 
{
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    fg.FrozenCols = 0;

    headerGrid(fg, ['Aprovar', 'Codigo', 'Erro', 'Aprovador', 'Data', 'TransportadoraErroID'], [5]);

    fillGridMask(fg, dsoPesq, ['Aprovar', 'CodigoErro', 'MensagemErro', 'Aprovador', 'dtAprovacao', 'TransportadoraErroID'],
                              ['', '', '', '', '']);

    alignColsInGrid(fg, [0]);

    lockControlsInModalWin(false);

    window.focus();

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;

    glb_GridIsBuilding = false;

    // destrava botao Incluir se tem linhas no grid
    if (fg.Rows > 1) 
    {
        fg.focus();
        fg.Editable = true;
    }
}

function aprovarDocumentoTransporte() 
{
    var strPars = '';
    var nUserID = getCurrUserID();
    var nDocumentoTransporteID = null;
    
    for (i = 1; i < fg.Rows; i++) 
    {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aprovar')) != 0) 
        {
            if (strPars == '')
                strPars += '?nTransportadoraErros=' + escape('/' + fg.TextMatrix(i, getColIndexByColKey(fg, 'TransportadoraErroID')) + '/');
            else
                strPars += escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'TransportadoraErroID')) + '/');
        }
    }

    if (strPars != '') 
    {
        strPars += '&nDocumentoTransporteID=' + escape(nDocumentoTransporteID);
        strPars += '&nUserID=' + escape(nUserID);

        setConnection(dsoAprovacao);
        dsoAprovacao.URL = SYS_ASPURLROOT + '/modcomercial/submodtransporte/documentostransporte/serverside/aprovacaoDocumentoTransporte.aspx' + strPars;
        dsoAprovacao.ondatasetcomplete = Aprovacao_DSC;
        dsoAprovacao.Refresh();
    }
    else
        if (window.top.overflyGen.Alert('Selecione pelo menos um registro!') == 0)
        return null;
}

function Aprovacao_DSC()
{
    var resultado = dsoAprovacao.recordset['Resultado'].value;

    if (resultado == 0) 
    {
        if (window.top.overflyGen.Alert('Documento de Transporte aprovado com sucesso!') == 0)
            return null;
    }
    else 
    {
        if (window.top.overflyGen.Alert('Erro ao aprovar Documento de Transporte!') == 0)
            return null;
    }

    startPesq();
}