/********************************************************************
modalInf.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport("dsoPesq");
var dsoIncluirNotaDocumentoTransporte = new CDatatransport("dsoIncluirNotaDocumentoTransporte");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    var elem = document.getElementById('modalnfBody');

    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no campo apropriado
    if (document.getElementById('txtNotaFiscal').disabled == false)
        txtNotaFiscal.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Incluir Notas Fiscais', 1);

    // ajusta elementos da janela
    var elem;
    var temp;

    // ajusta o divNotaFiscal
    elem = window.document.getElementById('divNotaFiscal');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;

    }

    // txtNotaFiscal
    elem = window.document.getElementById('txtNotaFiscal');
    elem.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (elem.style) {
        left = 0;
        top = 16;
        width = (elem.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
       
        txtNotaFiscal.onkeypress = verifyNumericEnterNotLinked;
        txtNotaFiscal.setAttribute('thePrecision', 10, 1);
        txtNotaFiscal.setAttribute('theScale', 0, 1);
        txtNotaFiscal.setAttribute('verifyNumPaste', 1);
        txtNotaFiscal.setAttribute('minMax', new Array(1, 9999999999), 1);
        txtNotaFiscal.value = '';
    }

    // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');
    // btnFindPesquisa
    btnFindPesquisa.disabled = true;

    with (elem.style) {
        top = parseInt(document.getElementById('txtNotaFiscal').style.top);
        left = parseInt(document.getElementById('txtNotaFiscal').style.left) + parseInt(document.getElementById('txtNotaFiscal').style.width) + 2;
        width = 80;
        height = 24;
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divNotaFiscal').style.top) + parseInt(document.getElementById('divNotaFiscal').style.height) + ELEM_GAP;
        width = temp + 450;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 30;
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }

    startGridInterface(fg);

   

    headerGrid(fg, [      'NotaFiscalID',
						  'NotaFiscal',
						  'Data',
						  'Pedido',
						  'Transacao',
						  'PessoaID',
						  'Pessoa',
						  'TransportadoraID',
						  'Transportadora',
                          'ChaveAcesso'], [0]);
    
    //fg.Redraw = 0;
    //fg.ColWidth(getColIndexByColKey(fg, 'NotaFiscalID')) = 1430;
    fg.Redraw = 2;
    
}

function txtNotaFiscal_ondigit(ctl) {
    changeBtnState(ctl.value);

    if (event.keyCode == 13)
        btnFindPesquisa_onclick(btnFindPesquisa);
}

function btnFindPesquisa_onclick(ctl) {
    txtNotaFiscal.value = trimStr(txtNotaFiscal.value);

    changeBtnState(txtNotaFiscal.value);

    if (btnFindPesquisa.disabled)
        return;

    startPesq(txtNotaFiscal.value);
}

//------------------------------------------------------------------
function incluirNotaDocumentoTransporte() {
    var strPars = new String();

    var nDocumentoTransporteID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'DocumentoTransporteID' + '\'' + '].value');
    var nNotaFiscalID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'NotaFiscalID'));
    var bAutomatico = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'chkAutomatico.checked');
    var sChaveAcesso = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ChaveAcesso'));
    var sProtocolo = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'dso01GridLkp.recordset[' + '\'' + 'Protocolo' + '\'' + '].value');

    strPars = '?nDocumentoTransporteID=' + escape(nDocumentoTransporteID);
    strPars += '&nNotaFiscalID=' + escape(nNotaFiscalID);
    strPars += '&bAutomatico=' + (bAutomatico ? "1" : "0");
    strPars += '&sChaveAcesso=' + sChaveAcesso;
    strPars += '&sProtocolo=' + sProtocolo;

    setConnection(dsoIncluirNotaDocumentoTransporte);

    dsoIncluirNotaDocumentoTransporte.URL = SYS_ASPURLROOT + '/serversidegenEx/incluirNotaDocumentoTransporte.aspx' + strPars;
    dsoIncluirNotaDocumentoTransporte.ondatasetcomplete = dsoIncluirNotaDocumentoTransporte_DSC;
    dsoIncluirNotaDocumentoTransporte.refresh();
}

function dsoIncluirNotaDocumentoTransporte_DSC() {
    var sResultado = dsoIncluirNotaDocumentoTransporte.recordset['resultServ'].value;

    if (sResultado != null)
        window.top.overflyGen.Alert(sResultado);

    txtNotaFiscal.value = '';
    fg.rows = 1;
    lockControlsInModalWin(false);
    btnOK.disabled = true;
}

//------------------------------------------------------------------

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        incluirNotaDocumentoTransporte();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);

}

function startPesq(strPesquisa) {
    lockControlsInModalWin(true);

    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EmpresaID' + '\'' + '].value');
    var nTipoTransporteID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoTransporteID' + '\'' + '].value');
    var dEmissao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'dtEmissao' + '\'' + '].value');
    
    setConnection(dsoPesq);

    dsoPesq.SQL = 
    'SELECT a.NotaFiscalID AS NotaFiscalID, a.NotaFiscal AS NotaFiscal, a.dtNotaFiscal AS Data, a.PedidoID AS PedidoID, b.PessoaID AS PessoaID, c.Operacao AS Transacao, ' +
		'dbo.fn_Pessoa_Fantasia(b.PessoaID, 0) AS Pessoa, b.TransportadoraID AS TransportadoraID, ' +
		'CASE WHEN (b.EmpresaID = b.TransportadoraID) ' +
				'THEN \'Nosso carro\' ' +
				'WHEN (b.ParceiroID = b.TransportadoraID) ' +
				'THEN \'O mesmo\' ' +
			'ELSE ISNULL(dbo.fn_Pessoa_Fantasia(b.TransportadoraID, 0),SPACE(0)) END AS Transportadora, a.ChaveAcesso AS ChaveAcesso ' +
	'FROM NotasFiscais a WITH (NOLOCK) ' +
		'INNER JOIN Pedidos b WITH (NOLOCK) ON (b.NotaFiscalID = a.NotaFiscalID) ' +
		'INNER JOIN Operacoes c WITH (NOLOCK) ON (c.OperacaoID = b.TransacaoID) ' +
		'INNER JOIN NotasFiscais_Pessoas d WITH(NOLOCK) ON (d.NotaFiscalID = a.NotaFiscalID) ' +
	'WHERE a.NotaFiscal = \'' + txtNotaFiscal.value + '\'' + ' AND a.EmpresaID = ' + nEmpresaID + ' AND a.EstadoID NOT IN (7, 62, 5, 70, 69) AND d.TipoID = 793 ' + 
	    ((nTipoTransporteID != "904") ? ' AND a.ModeloNF = ' + '\'' +  55 + '\''  : ' ') +
		(((nTipoTransporteID == "901") || (nTipoTransporteID == "904")) ? 'AND a.NotaFiscalID NOT IN (SELECT bb.NotaFiscalID ' +
									    'FROM DocumentosTransporte aa WITH(NOLOCK) ' +
										    'INNER JOIN DocumentosTransporte_NotasFiscais bb WITH(NOLOCK) ON (bb.DocumentoTransporteID = aa.DocumentoTransporteID) ' +
										    'INNER JOIN NotasFiscais cc WITH(NOLOCK) ON (cc.NotaFiscalID = bb.NotaFiscalID) ' +
									    'WHERE (aa.EmpresaID = ' + nEmpresaID + ') AND ' +
											    '(aa.TipoTransporteID = ' + nTipoTransporteID + ') AND (aa.EstadoID NOT IN (3,5)) AND ' +
											    '(cc.NotaFiscal = ' + '\'' + txtNotaFiscal.value + '\'' + ')) AND ' +
                                                '(a.dtNotaFiscal <= ' + 'CONVERT(DATETIME,\'' + dEmissao + '\', 103)' +  ') ' : ' ') +
	'ORDER BY a.NotaFiscal ';

    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC() {

    startGridInterface(fg);

    fg.FrozenCols = 0;

    headerGrid(fg, ['NotaFiscalID', 'NotaFiscal', 'Data', 'Pedido', 'Transacao', 'PessoaID', 'Pessoa', 'TransportadoraID', 'Transportadora', 'ChaveAcesso'], [0]);

    fillGridMask(fg, dsoPesq, ['NotaFiscalID', 'NotaFiscal', 'Data', 'PedidoID', 'Transacao', 'PessoaID', 'Pessoa', 'TransportadoraID', 'Transportadora', 'ChaveAcesso'], ['', '', '', '', '', '', '', '', '','']);
            
    lockControlsInModalWin(false);

    window.focus();

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        btnOK.disabled = false;
        fg.focus();
    }
    else {
        btnOK.disabled = true;
        txtNotaFiscal.focus();
    }
}
