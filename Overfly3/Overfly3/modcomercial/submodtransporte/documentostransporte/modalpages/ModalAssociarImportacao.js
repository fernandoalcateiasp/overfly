/********************************************************************
ModalAssociarImportacao.js

Library javascript para o ModalAssociarImportacao.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************

var modPesqRel_TimerHandler = null;
var glb_aEmpresaData = getCurrEmpresaData();

var dsoGrava = new CDatatransport("dsoGrava");
var dsoPesq = new CDatatransport("dsoPesq");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // ajusta o body do html
    with (ModalAssociarImportacaoBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no campo apropriado
    if (document.getElementById('txtProcesso').disabled == false)
        txtProcesso.focus();

    btnOK.disabled = true;

    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Associa Importação');
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Associa Importação', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;

    // btnOK nao e usado nesta modal
    with (btnOK) {
        disabled = true;
        style.visibility = 'hidden';
    }
    // btnCanc nao e usado nesta modal
    with (btnCanc) {
        style.visibility = 'hidden';
    }

    // ajusta o divPesquisa
    with (divPesquisa.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        height = 40;

    }

    // txtProcesso
    txtProcesso.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (txtProcesso.style) {
        left = 0;
        top = 16;
        width = (txtProcesso.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }

    txtProcesso.onkeypress = txtProcesso_onKeyPress;

    // btnFindPesquisa
    with (btnFindPesquisa.style) {
        top = parseInt(txtProcesso.style.top, 10);
        left = parseInt(txtProcesso.style.left, 10) + parseInt(txtProcesso.style.width, 10) + 2;
        width = 80;
        height = 24;
    }

    // btnAssociar
    //btnAssociar.disabled = true;
    with (btnAssociar.style) {
        top = parseInt(txtProcesso.style.top, 10);
        left = parseInt(btnFindPesquisa.style.left, 10) + parseInt(btnFindPesquisa.style.width, 10) + 2;
        width = 80;
        height = 24;
    }
    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
    }

    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.style.top, 10) + parseInt(divPesquisa.style.height, 10) + ELEM_GAP - 5;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 48;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    startGridInterface(fg);
    headerGrid(fg, ['ImportacaoID', 'Processo',
                   'Tipo', 'Via', 'Total FOB'],
                   [0]);

    fg.Redraw = 2;

}

function txtProcesso_onKeyPress() {
    if (event.keyCode == 13) {
        /*
        modPesqRel_TimerHandler = window.setInterval('btnFindPesquisa_onclick()', 30, 'JavaScript');
        */

        btnFindPesquisa_onclick();
    }
}

function btnFindPesquisa_onclick(ctl)
{
    /*
    if (modPesqRel_TimerHandler != null)
    {
        window.clearInterval(modPesqRel_TimerHandler);
        modPesqRel_TimerHandler = null;
    }
    */

    txtProcesso.value = trimStr(txtProcesso.value);

    /*
    changeBtnState(txtProcesso.value);

    if (btnFindPesquisa.disabled) {
        lockControlsInModalWin(false);
        return;
    }
    */

    startPesq(txtProcesso.value);
}


function fg_ModPesqRelDblClick()
{
    if (fg.Row >= 1)
        AssociarImportacao();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqRelKeyPress(KeyAscii) {
    if (fg.Row < 1)
        return true;

    if ((KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false))
        btn_onclick(btnOK);

}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', null);
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);

}

function startPesq(strPesquisa)
{
    lockControlsInModalWin(true);

    var Processo = txtProcesso.value;
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];
    var sSQL = '';

    sSQL = 'SELECT a.ImportacaoID, a.ProcessoImportacao as Processo, dbo.fn_TipoAuxiliar_Item(a.TipoImportacaoID, 0) as Tipo, dbo.fn_TipoAuxiliar_Item(a.TipoFreteID, 0) as Via, ' +
                    '(dbo.fn_Importacao_Totais(a.ImportacaoID, 1) * a.TaxaMoeda) AS TotalFOB ' +
                'FROM Importacao a WITH(NOLOCK) ' +
                    'LEFT OUTER JOIN DocumentosTransporte_Importacoes b WITH(NOLOCK) ON (b.ImportacaoID = a.ImportacaoID) ' +
                'WHERE ((a.EstadoID IN (172, 173, 174, 175, 48)) AND (a.EmpresaID = ' + nEmpresaID + ') AND (b.DocImportacaoID IS NULL) ' +
                    ((Processo.length > 0) ? 'AND ((a.ProcessoImportacao LIKE \'%' + Processo + '%\') OR (a.ImportacaoID LIKE \'%' + Processo + '%\')))' : ')') +
                'ORDER BY a.ImportacaoID DESC ';

    dsoPesq.SQL = sSQL;
    dsoPesq.ondatasetcomplete = eval(dsopesq_DSC);
    dsoPesq.refresh();
}

function dsopesq_DSC()
{
    startGridInterface(fg);
    fg.FrozenCols = 0;

    headerGrid(fg, ['ImportacaoID', 'Processo',
                   'Tipo', 'Via', 'Total FOB'], []);

    fillGridMask(fg, dsoPesq, ['ImportacaoID', 'Processo',
                               'Tipo', 'Via', 'TotalFOB'],
                             ['', '', '', '', '999999999.99'],
							 ['', '', '', '', '###,###,##0.00']);

    fg.Editable = false;
    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
    }
    else
        btnOK.disabled = true;
}


function js_modalAssociaImportacao_AfterEdit(grid, Row, Col) {
    /*
    if (grid.Editable)
    {
        if (Col == getColIndexByColKey(grid, 'Associar'))
        {
            for (var i = 1; i < grid.Rows; i++) {
                if (i != Row) {
                    grid.TextMatrix(i, getColIndexByColKey(grid, 'Associar')) = 0;
                }
                else {
                    grid.TextMatrix(Row, getColIndexByColKey(grid, 'Associar')) = 1;
                    btnAssociar.disabled = false;
                }
            }
        }
    }
    */
    ;
}

function js_modalAssociaImportacao_BeforeEdit(grid, Row, Col) {

    // return null;
    var nomeColuna = grid.ColKey(Col);

    var lastPos = nomeColuna.length - 1;

    // retira o asterisco do nome do campo
    if (nomeColuna.substr(lastPos, 1) == '*') {
        nomeColuna = nomeColuna.substr(0, lastPos);
    }

    if ((dsoPesq.recordset[nomeColuna].type == 200) || (dsoPesq.recordset[nomeColuna].type == 129))
        fieldMaxLength(fg, Col, dsoPesq);
}


function btnAssociar_onclick(ctl)
{
    AssociarImportacao();
}

function AssociarImportacao()
{
    if (fg.Row <= 0)
        return;

    lockControlsInModalWin(true);
    var strPars = new String();
    var ImportacaoID = fg.textMatrix(fg.Row, getColIndexByColKey(fg, 'ImportacaoID'));
    var DocumentoTransporteID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'DocumentoTransporteID' + '\'' + '].value');

    strPars += '?DocumentoTransporteID=' + escape(DocumentoTransporteID);
    strPars += '&ImportacaoID=' + escape(ImportacaoID);

    dsoGrava.URL = SYS_ASPURLROOT + '/modcomercial/submodtransporte/documentostransporte/serverside/AssociarImportacao.aspx' + strPars;
    dsoGrava.ondatasetcomplete = Gravar_DSC;
    dsoGrava.refresh();
}


function Gravar_DSC() {

    lockControlsInModalWin(false);

    var sResultado = '';

    sResultado = dsoGrava.recordset['Resultado'].value;

    if (sResultado != '')
        window.top.overflyGen.Alert(sResultado);

    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', null);
}