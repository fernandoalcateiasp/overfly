<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="documentostransportetranspsup01Html" name="documentostransportetranspsup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf               
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodtransporte/documentostransporte/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodtransporte/documentostransporte/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
	Dim strSQL, rsData
	Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
	Response.Write vbcrlf
	
	Response.Write "glb_nIdiomaID = 0;"
	Response.Write vbcrlf
	
	strSQL = "SELECT IdiomaID FROM Recursos WITH(NOLOCK) WHERE (RecursoID = 999)"
	
	Set rsData = Server.CreateObject("ADODB.Recordset")         
	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (NOT (rsData.BOF AND rsData.EOF)) Then
		Response.Write "glb_nIdiomaID = " & CStr(rsData.Fields("IdiomaID").Value) & ";"
	End If
	
	rsData.Close()
	Set rsData = Nothing
	Response.Write "</script>"
	Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--
//-->
</script>

</head>

<!-- //@@ -->
<body id="DocumentosTranspsup01Body" name="DocumentosTranspsup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral"/>
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->        
      
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral"></input>
        
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral"></input>
        
        <p id="lblDocumentoTransporte" name="lblDocumentoTransporte" class="lblGeneral">Numero</p>
        <input type="text" id="txtDocumentoTransporte" name="txtDocumentoTransporte" DATASRC="#dsoSup01" DATAFLD="Numero" class="fldGeneral">
       
        <p id="lblSerie" name="lblSerie" class="lblGeneral">Serie</p>
        <input type="text" id="txtSerie" name="txtSerie" DATASRC="#dsoSup01" DATAFLD="Serie" class="fldGeneral"></input>

        <p id="lblDtEmissao" name="lblDtEmissao" class="lblGeneral">Data de Emiss�o</p>
		<input type="text" id="txtDtEmissao" name="txtDtEmissao" DATASRC="#dsoSup01" DATAFLD="V_dtEmissao" class="fldGeneral"></input>

        <!-- <p id="lblCFOPID" name="lblCFOPID" class="lblGeneral">CFOP</p> -->
        <!-- <select id="selCFOPID" name="selCFOPID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="CFOPID" title="C�digo Fiscal da Natureza de Opera��o"></select> -->

        <p id="lblCFOP" name="lblCFOP" class="lblGeneral">CFOP</p>
        <input type="text" id="txtCFOP" name="txtCFOP" DATASRC="#dsoSup01" DATAFLD="CFOP" class="fldGeneral" title="C�digo Fiscal da Natureza de Opera��o"></input>

        <p id="lblTipoTransporteID" name="lblTipoTransporteID" class="lblGeneral">Tipo Transporte</p>
		<select id="selTipoTransporteID" name="selTipoTransporteID" DATASRC="#dsoSup01" DATAFLD="TipoTransporteID" class="fldGeneral"></select>
		
		<p id="lblCondicaoFreteID" name="lblCondicaoFreteID" class="lblGeneral">Frete</p>
        <select id="selCondicaoFreteID" name="selCondicaoFreteID" DATASRC="#dsoSup01" DATAFLD="CondicaoFreteID" class="fldGeneral"></select>
		
		<p id="lblModalidadeTransporteID" name="lblModalidadeTransporteID" class="lblGeneral">Modalidade</p>
        <select id="selModalidadeTransporteID" name="selModalidadeTransporteID" DATASRC="#dsoSup01" DATAFLD="ModalidadeTransporteID" class="fldGeneral"></select>
		
		<p id="lblIdentificador" name="lblIdentificador" class="lblGeneral">Identificador</p>
        <input type="text" id="txtIdentificador" name="txtIdentificador" DATASRC="#dsoSup01" DATAFLD="Identificador" class="fldGeneral" title="Identificador de Registro do Arquivo EDI"></input>
        
        <p id="lblAutomatico" name="lblAutomatico" class="lblGeneral">Aut</p>
		<input type="checkbox" id="chkAutomatico" name="chkAutomatico" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Automatico" title="A entrada do documento de transporte ocorreu pelo processo EDI autom�tico do sistema."></input> 
		
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo</p>
        <select id="selTipoRegistroID" name="selTipoRegistroID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoDocumentoTransporteID"></select>    
        
        <p id="lblTransportadoraID" name="lblTransportadoraID" class="lblGeneral">Transportadora</p>
		<select id="selTransportadoraID" name="selTransportadoraID" DATASRC="#dsoSup01" DATAFLD="TransportadoraID" class="fldGeneral"></select>
		<input type="image" id="btnFindTransportadora" name="btnFindTransportadora" class="fldGeneral" title="Selecionar a transportadora" WIDTH="24" HEIGHT="23"></input>
        
        <p id="lblPessoa" name="lblPessoa" class="lblGeneral">Pessoa</p>
		<input type="text" id="txtPessoa" name="txtPessoa" DATASRC="#dsoSup01" DATAFLD="Pessoa" class="fldGeneral"></input>
        
        <p id="lblFilialEmissora" name="lblFilialEmissora" class="lblGeneral">Filial Emissora</p>
		<input type="text" id="txtFilialEmissora" name="txtFilialEmissora" DATASRC="#dsoSup01" DATAFLD="TransportadoraFilial" class="fldGeneral"></input>
        
        <p id="lblPesoTransportado" name="lblPesoTransportado" class="lblGeneral">Peso</p>
        <input type="text" id="txtPesoTransportado" name="txtPesoTransportado" DATASRC="#dsoSup01" DATAFLD="PesoTransportado" class="fldGeneral" title="Peso Transportado (Kg)"></input>
        
        <p id="lblValorTotal" name="lblValorTotal" class="lblGeneral">Valor Total</p>
        <input type="text" id="txtValorTotal" name="txtValorTotal" DATASRC="#dsoSup01" DATAFLD="ValorTotal" class="fldGeneral"></input>
        
        <p id="lblValorTotalParcelasFrete" name="lblValorTotalParcelasFrete" class="lblGeneral">Total Parcelas Frete</p>
        <input type="text" id="txtValorTotalParcelasFrete" name="txtValorTotalParcelasFrete" DATASRC="#dsoSup01" DATAFLD="ValorTotalParcelasFrete" class="fldGeneral"></input>
        
        <p id="lblValorDiferenca" name="lblValorDiferenca" class="lblGeneral">Diferen�a</p>
        <input type="text" id="txtValorDiferenca" name="txtValorDiferenca" DATASRC="#dsoSup01" DATAFLD="ValorDiferenca" class="fldGeneral" title="Diferen�a entre o total do documento de transporte e o total das parcelas frete"></input>
        
        <p id="lblPercentualDiferenca" name="lblPercentualDiferenca" class="lblGeneral">Dif %</p>
        <input type="text" id="txtPercentualDiferenca" name="txtPercentualDiferenca" DATASRC="#dsoSup01" DATAFLD="PercentualDiferenca" class="fldGeneral"></input>
        
        <p id="lblValorDesconto" name="lblValorDesconto" class="lblGeneral">Desconto</p>
        <input type="text" id="txtValorDesconto" name="txtValorDesconto" DATASRC="#dsoSup01" DATAFLD="ValorDesconto" class="fldGeneral" title="Desconto obtido com a transportadora"></input>

        <p id="lblAjusteParcelaFrete" name="lblAjusteParcelaFrete" class="lblGeneral">Ajuste Parcela Frete</p>
        <input type="text" id="txtAjusteParcelaFrete" name="txtAjusteParcelaFrete" DATASRC="#dsoSup01" DATAFLD="AjusteParcelaFrete" class="fldGeneral" title="Ajuste para parcela frete excedente"></input>

        <p id="lblChaveAcesso" name="lblChaveAcesso" class="lblGeneral">Chave de Acesso</p>
        <input type="text" id="txtChaveAcesso" name="txtChaveAcesso" DATASRC="#dsoSup01" DATAFLD="ChaveAcesso" class="fldGeneral"></input>

        
        <p id="lblGRIS" name="lblGRIS" class="lblGeneral">Valor SEC - CAT</p>
        <input type="text" id="txtGRIS" name="txtGRIS" DATASRC="#dsoSup01" DATAFLD="ValorGris" class="fldGeneral" title="Valor SEC - CAT (GRIS)"></input>
        
        <p id="lblFretePeso" name="lblFretePeso" class="lblGeneral">Frete por Peso</p>
        <input type="text" id="txtFretePeso" name="txtFretePeso" DATASRC="#dsoSup01" DATAFLD="ValorPesoFrete" class="fldGeneral"></input>
        
        <p id="lblValorFrete" name="lblValorFrete" class="lblGeneral">Frete Valor</p>
        <input type="text" id="txtValorFrete" name="txtValorFrete" DATASRC="#dsoSup01" DATAFLD="ValorFrete" class="fldGeneral" title="Valor do frete sem encargos"></input>
        
        <p id="lblValorITR" name="lblValorITR" class="lblGeneral">Valor ITR</p>
        <input type="text" id="txtValorITR" name="txtValorITR" DATASRC="#dsoSup01" DATAFLD="ValorITR" class="fldGeneral" title="ITR - Incremento ao Transporte Rodovi�rio"></input>

        <p id="lblValorAdeme" name="lblValorAdeme" class="lblGeneral">Valor Ademe</p>
        <input type="text" id="txtValorAdeme" name="txtValorAdeme" DATASRC="#dsoSup01" DATAFLD="ValorAdeme" class="fldGeneral" title="Ademe � Adicional de Emerg�ncia"></input>

        <p id="lblValorDespacho" name="lblValorDespacho" class="lblGeneral">Valor do Despacho</p>
        <input type="text" id="txtValorDespacho" name="txtValorDespacho" DATASRC="#dsoSup01" DATAFLD="ValorDespacho" class="fldGeneral"></input>

        <p id="lblValorPedagio" name="lblValorPedagio" class="lblGeneral">Valor do Ped�gio</p>
        <input type="text" id="txtValorPedagio" name="txtValorPedagio" DATASRC="#dsoSup01" DATAFLD="ValorPedagio" class="fldGeneral"></input>

        <p id="lblComplemento" name="lblComplemento" class="lblGeneral">Complemento</p>
        <input type="text" id="txtComplemento" name="txtComplemento" DATASRC="#dsoSup01" DATAFLD="Complemento" class="fldGeneral"></input>


        <p id="lblBaseCalculo" name="lblBaseCalculo" class="lblGeneral">Base</p>
        <input type="text" id="txtBaseCalculo" name="txtBaseCalculo" DATASRC="#dsoSup01" DATAFLD="BaseCalculo" class="fldGeneral"></input>
        
        <p id="lblAliquota" name="lblAliquota" class="lblGeneral">Al�q</p>
        <input type="text" id="txtAliquota" name="txtAliquota" DATASRC="#dsoSup01" DATAFLD="Aliquota" class="fldGeneral"></input>
        
        <p id="lblValorImposto" name="lblValorImposto" class="lblGeneral">Valor Imposto</p>
        <input type="text" id="txtValorImposto" name="txtValorImposto" DATASRC="#dsoSup01" DATAFLD="ValorImposto" class="fldGeneral"></input>
		      
        <p id="lblSubstituicaoTributaria" name="lblSubstituicaoTributaria" class="lblGeneral">ST</p>
		<input type="checkbox" id="chkSubstituicaoTributaria" name="chkSubstituicaoTributaria" class="fldGeneral"	DATASRC="#dsoSup01" DATAFLD="SubstituicaoTributaria" title="Possui Substitui��o tribut�ria?"></input> 
		
		<p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
        
        <p id="lblFatura" name="lblFatura" class="lblGeneral">Fatura</p>
		<input type="text" id="txtFatura" name="txtFatura" DATASRC="#dsoSup01" DATAFLD="NumeroFatura" class="fldGeneral"/>

        <p id="lblDocumentoTransporteReferenciaID" name="lblDocumentoTransporteReferenciaID" class="lblGeneral">Documento Referencia</p>
        <input type="text" id="txtDocumentoTransporteReferenciaID" name="txtDocumentoTransporteReferenciaID" DATASRC="#dsoSup01" DATAFLD="DocumentoTransporteReferenciaID" class="fldGeneral"></input>

        
        				
    </div>
    
</body>

</html>
