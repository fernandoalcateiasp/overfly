/********************************************************************
especificinf.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

     setConnection(dso);


     // Notas Fiscais
     if (folderID == 24331)
     {
         dso.SQL = 'SELECT *, SUBSTRING(CHAVEACESSOLOCALIZAR, 26, 9) AS NotaChaveAcessoLocalizar ' +
                    'FROM DocumentosTransporte_NotasFiscais WITH(NOLOCK) ' +
                        'WHERE DocumentoTransporteID = ' + idToFind;
                    //' ORDER BY DocumentoTransporteID DESC';
         return 'dso01Grid_DSC';
     }

    // Notas Fiscais
     else if (folderID == 24332)
     {
         dso.SQL = 'SELECT * ' +
                    'FROM DocumentosTransporte_Importacoes WITH(NOLOCK) ' +
                        'WHERE DocumentoTransporteID = ' + idToFind;
         //' ORDER BY DocumentoTransporteID DESC';
         return 'dso01Grid_DSC';
     }

    //@@
    else if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.DocumentoTransporteID, a.ProprietarioID, a.AlternativoID, a.Observacoes FROM DocumentosTransporte a WITH (NOLOCK) WHERE ' +
			sFiltro + 'a.DocumentoTransporteID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data     DESC';
        return 'dso01Grid_DSC';
    }
    //Erros
    else if (folderID == 24352) {
    dso.SQL = 'SELECT a.* ' +
	            'FROM dbo.TransportadoraDados_Detalhes_Erros a WITH(NOLOCK) ' +
                'WHERE (a.RegistroID = ' + idToFind + ') AND (a.TipoRegistroID = 1901) ' +
                'ORDER BY a.CodigoErro';
        return 'dso01Grid_DSC';
    }
    // Impostos
    else if (folderID == 24355) {
        dso.SQL = 'SELECT b.Imposto AS Imposto, a.Aliquota AS Aliquota, a.BaseCalculo AS BaseCalculo, a.ValorImposto AS ValorImposto, a.TemCredito AS TemCredito, a.CSTID AS CSTID, a.DocImpostoID AS DocImpostoID ' +
                  'FROM dbo.DocumentosTransporte_Impostos a WITH(NOLOCK) ' +
                  'INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ImpostoID) ' +
                  'WHERE (a.DocumentoTransporteID = ' + idToFind + ')' +
                  'ORDER BY ImpostoID';
        return 'dso01Grid_DSC';
    }


    // Lancamentos
    else if (folderID == 24353) {
        dso.SQL = 'EXEC sp_EventosContabeis_Lancamentos ' + glb_nFormID + ', NULL, ' + idToFind;
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    var aEmpresaID = getCurrEmpresaData();
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Erros
    else if (pastaID == 24352) {
        if (dso.id == 'dso05GridLkp') {
            dso.SQL = 'SELECT a.*, ' +
                                    'b.FreteErroID, ' +
                                    'b.Interno, ' +
                                    'b.Fiscal, ' +
                                    'b.PermiteAprovacao, ' +
                                    'b.MensagemErro, ' +
                                    'dbo.fn_Pessoa_Fantasia(a.AprovadorID, 0) AS Aprovador, ' +
                                    'a.dtAprovacao, ' +
                                    'a.Observacao ' +
	                            'FROM dbo.TransportadoraDados_Detalhes_Erros a WITH(NOLOCK) ' +
	                                'INNER JOIN FreteErros b WITH(NOLOCK) ON (b.FreteErroID = a.CodigoErro) ' +
	                            'WHERE (a.RegistroID = ' + nRegistroID + ') AND (a.TipoRegistroID = 1901) ' +
                                'ORDER BY a.CodigoErro';
            return 'dso05GridLkp';
        }
    }
    else if (pastaID == 24331)
    {
        if (dso.id == 'dso01GridLkp')
        {
            dso.SQL = 'SELECT CONVERT(VARCHAR(10), b.dtNotaFiscal, 103) AS dtNotaFiscal, ' +
		                        'a.NotaFiscalLocalizar, ' +
		                        'a.Protocolo, ' +
		                        'b.NotaFiscal AS NotaFiscal, ' +
		                        'b.NotaFiscalID AS NotaFiscalID, ' +
		                        'b.PedidoID AS PedidoID, ' +
		                        'c.PessoaID, ' +
		                        'dbo.fn_Pessoa_Fantasia(c.PessoaID, 0) AS Pessoa, ' +
		                        'CONVERT(VARCHAR,c.TotalPesoBruto) +  \' kg\' AS PesoBruto, ' +
		                        'CONVERT(VARCHAR,c.TotalPesoLiquido) +  \' kg\'  AS PesoLiquido, ' +
		                        '(CASE dbo.fn_Pedido_Cubagem(c.PedidoID) WHEN 0 THEN NULL ELSE (CONVERT(VARCHAR,dbo.fn_Pedido_Cubagem(c.PedidoID)) + \'m3\') END) AS Cubagem, ' +
		                        'c.NumeroVolumes AS Volumes, ' +
		                        'd.Operacao AS Transacao, ' +
		                        '(SELECT SUM(b.ValorParcela) ' +
			                        'FROM Pedidos_Parcelas b WITH(NOLOCK) ' +
			                        'WHERE b.PedidoID = c.PedidoID AND b.HistoricoPadraoID = 3173) AS ValorParcela, ' +
		                        'c.MinutaID, ' +
		                        'b.ValorFrete, ' +
                                'c.ValorTotalPedido AS ValorTotalPedido, ' +
		                        'a.DocNotaFiscalID ' +
	                        'FROM DocumentosTransporte_NotasFiscais a WITH(NOLOCK) ' +
		                        'LEFT OUTER JOIN NotasFiscais b WITH(NOLOCK) ON (b.NotaFiscalID = a.NotaFiscalID) ' +
		                        'LEFT OUTER JOIN Pedidos c WITH(NOLOCK) ON (c.NotaFiscalID = b.NotaFiscalID) ' +
		                        'LEFT OUTER JOIN Operacoes d WITH(NOLOCK) ON (d.OperacaoID = c.TransacaoID) ' +
	                        'WHERE a.DocumentoTransporteID = ' + nRegistroID;
        }
    }

    else if (pastaID == 24332)
    {
        if (dso.id == 'dso01GridLkp')
        {
            dso.SQL = 'SELECT TOP 100 a.DocImportacaoID, b.ProcessoImportacao AS Processo, b.NumeroDTA AS DTA, (dbo.fn_Importacao_Totais(a.ImportacaoID, 1) * b.TaxaMoeda) AS TotalFOB ' +
                        'FROM DocumentosTransporte_Importacoes a WITH(NOLOCK) ' +
                        'INNER JOIN Importacao b WITH(NOLOCK) ON (b.ImportacaoID = a.ImportacaoID) ' +
                        'WHERE a.DocumentoTransporteID = ' + nRegistroID;
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); // Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); // Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); // LOG

    vPastaName = window.top.getPastaName(24331);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24331); // Notas Fiscais

    vPastaName = window.top.getPastaName(24332);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24332); // Importacoes

    vPastaName = window.top.getPastaName(24352);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24352); // Erros

    vPastaName = window.top.getPastaName(24355);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24355); // Impostos

    vPastaName = window.top.getPastaName(24353);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24353); //Lancamentos
 
    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
	var currLine = -1;
	
    //@@ return true se tudo ok, caso contrario return false;
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {

        if (folderID == 24331) // Pega a Linha em que se deve gravar a altera��o.
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [0], ['DocNotaFiscalID', registroID]);

        if (folderID == 24332) // Pega a Linha em que se deve gravar a altera��o.
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [0], ['DocImportacaoID', registroID]);

        if (folderID == 24352) // Pega a Linha em que se deve gravar a altera��o.
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [0], ['DocumentoTransporteID', registroID]);
	    
        if (currLine < 0)
        {
            if (fg.disabled == false)
            {
				try
                {
                    fg.Editable = true;
					window.focus();
					fg.focus();
				}	
                catch(e)
                {
					;
                }
            }    
        
            return false;
        }    
    }

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    
    // Impostos
    if (folderID == 24355)
    {
        headerGrid(fg, ['Imposto',
                       'Aliquota',
                       'Base Calculo',
                       'Valor',
                       'Credito',
                       'DocImpostoID'], [5]);

        fg.ColDataType(4) = 11;

        fillGridMask(fg, currDSO, ['Imposto',
                                   'Aliquota',
                                   'BaseCalculo',
                                   'ValorImposto',
                                   'TemCredito',
                                   'DocImpostoID'],
                                 ['', '999.99', '999999999.99', '999999999.99', '', ''],
                                 ['', '##0.00', '###,###,##0.00', '###,###,##0.00', '', '']);

        glb_aCelHint = [[0, 4, '� Credito']];

        fg.Editable = false;
    }


    //Notas Fiscais
    else if (folderID == 24331)
    {
        headerGrid(fg, ['NF Arquivo',
                        'NF Chave',
                            'Protocolo',
                            'Nota Fiscal',
                            'Data',
                            'Pedido',
                            'Minuta',
                            'Transa��o',
                            'PessoaID',
                            'Pessoa',                            
                            'Peso Bruto',
                            'Peso L�quido',
                            'Cubagem',
                            'Volumes',
                            'Valor Frete',
                            'Valor Pedido',
                            'Observa��o',
                            'NotaFiscalID',
                            'PedidoID',
                            'DocNotaFiscalID'], [19]);

        fillGridMask(fg, currDSO, ['NotaFiscalLocalizar*',
                                    'NotaChaveAcessoLocalizar*',
                                    'Protocolo*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^NotaFiscal*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^dtNotaFiscal*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^PedidoID*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^MinutaID*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^Transacao*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^PessoaID*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^Pessoa*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^PesoBruto*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^PesoLiquido*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^Cubagem*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^Volumes*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^ValorFrete*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^ValorTotalPedido*',
                                    'Observacao*',
                                    'NotaFiscalID*',
                                    '^DocNotaFiscalID^dso01GridLkp^DocNotaFiscalID^PedidoID*',
                                    'DocNotaFiscalID'],
                                    ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '999999999999.99', '999999999999.99', '', '', ''],
                                    ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '###,###,###,##0.00', '###,###,###,##0.00', '', '', '']);
    }

        //Importacoes
    else if (folderID == 24332)
    {
        headerGrid(fg, ['Importa��o',
                        'Processo',
                        'DTA',
                        'Total FOB R$',
                        'DocImportacaoID'], [4]);

        fillGridMask(fg, currDSO, ['ImportacaoID*',
                                   '^DocImportacaoID^dso01GridLkp^DocImportacaoID^Processo*',
                                   '^DocImportacaoID^dso01GridLkp^DocImportacaoID^DTA*',
                                   '^DocImportacaoID^dso01GridLkp^DocImportacaoID^TotalFOB*',
                                   'DocImportacaoID'],
                                    ['', '', '', '', '999999999999.99'],
                                    ['', '', '', '', '###,###,###,##0.00']);
    }

    //Erros
    else if (folderID == 24352) {
        headerGrid(fg, ['Codigo',
                        'Int',
                        'Fis',
                        'PA',
                        'Erro',
                        'Aprovador',
                        'Data Aprova��o',
                        'Observacao',
                        'TransportadoraErroID'], [8]);

        glb_aCelHint = [[0, 1, 'Erro interno ou da transportadora'],
						[0, 2, 'Erro tem origem fiscal'],
						[0, 3, 'Permite Aprova��o Especial']];

        fillGridMask(fg, currDSO, ['^CodigoErro^dso05GridLkp^FreteErroID^FreteErroID*',
                                   '^CodigoErro^dso05GridLkp^FreteErroID^Interno*',
                                   '^CodigoErro^dso05GridLkp^FreteErroID^Fiscal*',
                                   '^CodigoErro^dso05GridLkp^FreteErroID^PermiteAprovacao*',
                                   '^CodigoErro^dso05GridLkp^FreteErroID^MensagemErro*',
                                   '^CodigoErro^dso05GridLkp^FreteErroID^Aprovador*',
                                   'dtAprovacao*',
                                   'Observacao',
                                   'TransportadoraErroID'],
                                    ['', '', '', '', '', '', '', ''],
                                    ['', '', '', '', '', '', '', '']);
    }

    // Lancamentos contabeis
    else if (folderID == 24353) 
    {
        headerGrid(fg, ['Lan�amento',
                        'Est',
                        'Data',
                        'Balancete',
                        'Est',
                        'Tipo',
                        'Quant',
                        'Hist�rico Padr�o',
                        'Hist�rico Complementar',
                        'Detalhe',
                        'D',
                        'C',
                        'Moeda',
                        'Total Lan�amento',
                        'Taxa',
                        'Total Convertido',
                        'Observa��o',
                        'EventoID'], [17]);

        glb_aCelHint = [[0, 4, '� estorno?'],
						[0, 6, 'Quantidade de detalhes'],
						[0, 10, 'N�mero de D�bitos'],
						[0, 11, 'N�mero de Cr�ditos'],
						[0, 11, 'Taxa de convers�o para moeda comum']];

        fillGridMask(fg, currDSO, ['LancamentoID',
                                 'Estado',
                                 'dtLancamento',
                                 'dtBalancete',
                                 'EhEstorno',
                                 'TipoLancamento',
                                 'Quantidade',
                                 'HistoricoPadrao',
                                 'HistoricoComplementar',
                                 'Detalhe',
                                 'NumeroDebitos',
                                 'NumeroCreditos',
                                 'Moeda',
                                 'ValorTotalLancamento',
                                 'TaxaMoeda',
                                 'ValorTotalConvertido',
                                 'Observacao',
                                 'EventoID'],
                                 ['9999999999', '', '99/99/9999', '99/99/9999', '', '', '999999', '', '', '', '99', '99', '',
									'999999999.99', '9999999999999.99', '999999999.99', '', ''],
								 ['##########', '', dTFormat, dTFormat, '', '', '######', '', '', '', '##', '##', '',
									'###,###,##0.00', '#,###,###,###,##0.0000000', '###,###,##0.00', '', '']);

        alignColsInGrid(fg, [0, 6, 10, 11, 13, 14, 15]);
        fg.FrozenCols = 2;
    }
       
}
