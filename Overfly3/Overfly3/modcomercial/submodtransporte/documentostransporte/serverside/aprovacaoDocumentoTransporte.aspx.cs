﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodtransporte.documentostransporte.serverside
{
    public partial class aprovacaodocumentotransporte : System.Web.UI.OverflyPage
    {
        private static string emptyStr = "";
        private static Integer Zero = new Integer(0);
        
        private string TransportadoraErros;
        protected string nTransportadoraErros
        {
            get { return TransportadoraErros; }
            set { TransportadoraErros = (value != null) ? value : emptyStr; }
        }
        
        private Integer DocumentoTransporteID;
        protected Integer nDocumentoTransporteID
        {
            get { return DocumentoTransporteID; }
            set { DocumentoTransporteID = (value != null) ? value : Zero; }
        }

        private Integer UserID;
        protected Integer nUserID
        {
            get { return UserID; }
            set { UserID = (value != null) ? value : Zero; }
        }

        protected int aprovaFatura()
        {
            ProcedureParameters[] param = new ProcedureParameters[]
			{
				new ProcedureParameters("@TransportadoraErros", SqlDbType.VarChar, nTransportadoraErros),
				new ProcedureParameters("@RegistroID", SqlDbType.Int, (DocumentoTransporteID != Zero ? (Object)int.Parse(DocumentoTransporteID.ToString()) : (Object)System.DBNull.Value)),
                new ProcedureParameters("@UsuarioID", SqlDbType.Int, nUserID.intValue()),
                new ProcedureParameters("@TipoRegistroID", SqlDbType.Int, 2),
                new ProcedureParameters("@ocorrencia",SqlDbType.Int,DBNull.Value, ParameterDirection.Output)
            };

            DataInterfaceObj.execNonQueryProcedure("sp_DocumentoTransporteFatura_Aprovacao", param);

            return param[4].Data != DBNull.Value ? (int)param[4].Data : 0;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            int resultado = aprovaFatura();

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " + resultado + " as Resultado"
            ));
        }
    }
}
