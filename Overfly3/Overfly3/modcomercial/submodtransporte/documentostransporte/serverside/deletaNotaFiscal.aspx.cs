﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;

namespace Overfly3.modcomercial.submodtransporte.documentostransporte.serverside
{
    public partial class deletaNotaFiscal : System.Web.UI.OverflyPage
    {
        private string NotaFiscalID;
        public string nNotaFiscalID
        {
            set { NotaFiscalID = value != null ? value : "0"; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            DataInterfaceObj.ExecuteSQLCommand(
                        "UPDATE DocumentosTransporte_NotasFiscais SET NotaFiscalID = NULL WHERE NotaFiscalID = " + NotaFiscalID);

            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT " + NotaFiscalID + " AS resultServ"));
        }
    }
}
