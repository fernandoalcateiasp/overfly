using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodtransporte.documentostransporte.serverside
{
    public partial class verificacaodocumentostransporte : System.Web.UI.OverflyPage
	{
		private Integer DocumentoTransporteID;
        protected Integer nDocumentoTransporteID
		{
            get { return DocumentoTransporteID; }
            set { DocumentoTransporteID = value; }
		}

		private Integer estadoDeID;
		protected Integer nEstadoDeID
		{
			get { return estadoDeID; }
			set { estadoDeID = value; }
		}

		private Integer estadoParaID;
		protected Integer nEstadoParaID
		{
			get { return estadoParaID; }
			set { estadoParaID = value; }
		}

        private Integer transportadoraArquivoID;
        protected Integer nTransportadoraArquivoID
        {
            get { return transportadoraArquivoID; }
            set { transportadoraArquivoID = value; }
        }

        private Integer transportadoraArquivoDetalheID;
        protected Integer nTransportadoraArquivoDetalheID
        {
            get { return transportadoraArquivoDetalheID; }
            set { transportadoraArquivoDetalheID = value; }
        }

		protected string DocumentoVerifica()
		{
			ProcedureParameters[] param = new ProcedureParameters[]
			{
				new ProcedureParameters("@DocumentoTransporteID", SqlDbType.Int, nDocumentoTransporteID.intValue()),
				new ProcedureParameters("@EstadoDeID", SqlDbType.Int, nEstadoDeID.intValue()),
				new ProcedureParameters("@EstadoParaID", SqlDbType.Int, nEstadoParaID.intValue()),
                new ProcedureParameters("@TransportadoraArquivoID",SqlDbType.Int, ((nTransportadoraArquivoID != null) ? (Object)nTransportadoraArquivoID.intValue() : DBNull.Value)),
                new ProcedureParameters("@TransportadoraArquivoDetalheID",SqlDbType.Int, ((nTransportadoraArquivoDetalheID != null) ? (Object)nTransportadoraArquivoDetalheID.intValue() : DBNull.Value)),
                new ProcedureParameters("@MensagemErro",SqlDbType.VarChar,DBNull.Value, ParameterDirection.Output,8000),
                new ProcedureParameters("@CodigoErro",SqlDbType.Int,DBNull.Value, ParameterDirection.Output)
            };

            DataInterfaceObj.execNonQueryProcedure("sp_DocumentoTransporte_Verifica", param);

			return param[5].Data != DBNull.Value ? (string) param[5].Data : null;
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
            string resultado = DocumentoVerifica();

			WriteResultXML(DataInterfaceObj.getRemoteData(
				"select " + 
					(resultado != null ? ("'" + resultado + "'") : ("NULL")) + 
				" as Resultado"
			));
		}
	}
}
