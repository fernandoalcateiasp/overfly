﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modcomercial.submodtransporte.documentostransporte.serverside
{
    public partial class AssociarImportacao : System.Web.UI.OverflyPage
    {
        private Integer nDocumentoTransporteID;

        public Integer DocumentoTransporteID
        {
            get { return nDocumentoTransporteID; }
            set { nDocumentoTransporteID = value; }
        }
        
        private Integer nImportacaoID;

        public Integer ImportacaoID
        {
            get { return nImportacaoID; }
            set { nImportacaoID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string sql = "INSERT INTO DocumentosTransporte_Importacoes " +
                            "SELECT " + nDocumentoTransporteID.ToString() + ", " + nImportacaoID.ToString();

            string sResultado = "";

            try
            {
                int resultado = DataInterfaceObj.ExecuteSQLCommand(sql);
            }
            catch (System.Exception ex)
            {
                sResultado = ex.Message;
            }

            WriteResultXML(
                DataInterfaceObj.getRemoteData(" SELECT '" + sResultado + "' AS Resultado")
            );

        }
    }
}