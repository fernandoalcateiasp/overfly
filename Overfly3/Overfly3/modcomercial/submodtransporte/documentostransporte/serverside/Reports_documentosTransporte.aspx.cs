﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_documentosTransporte : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private DateTime ddtInicio = Convert.ToDateTime(HttpContext.Current.Request.Params["ndtInicio"]);
        private DateTime ddtFim = Convert.ToDateTime(HttpContext.Current.Request.Params["ndtFim"]);
        private string sFiltroEstado = Convert.ToString(HttpContext.Current.Request.Params["sFiltroEstado"]);
        private string sEmpresaID = Convert.ToString(HttpContext.Current.Request.Params["nEmpresaID"]);
        private string sFiltroTransportadora = Convert.ToString(HttpContext.Current.Request.Params["nFiltroTransportadora"]);
        private string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        private string sQuantidadeDocumentoTransporte = Convert.ToString(HttpContext.Current.Request.Params["quantidadeDocumentoTransporte"]);


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string nomeRel = Request.Params["relatorioID"].ToString();

                switch (nomeRel)
                {
                    // Documentos com Diferenças Percentuais
                    case "40213":
                        documentosDiferencasPercentuais();
                        break;
                }
            }
        }

        public void documentosDiferencasPercentuais()
        {
            // Excel
            int Formato = 2;
            string Title = "Documentos com Diferenças Percentuais";
            string Title2 = "Quantidade de Documentos de Transporte: " + sQuantidadeDocumentoTransporte;
            string EmpresaNome = sEmpresaFantasia;

            string strSQL = "SELECT a.DocumentoTransporteID, " +
                                "c.Fantasia AS Empresa, " +
                                "d.RecursoAbreviado AS Estado, " +
                                "a.Numero, " +
                                "CONVERT(VARCHAR,a.dtEmissao,103) AS dtEmissao, " +
                                "a.ValorTotal, " +
                                "a.ValorImposto, " +
                                "a.Aliquota, " +
                                "dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID, 2) AS ValorDiferenca, " +
                                "dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID, 3) AS PercentualDiferenca, " +
                                "e.ItemMasculino AS TipoTransporte " +
                            "FROM  DocumentosTransporte a WITH(NOLOCK) " +
                                "INNER JOIN TransportadoraDados_Detalhes_Erros b WITH(NOLOCK) ON (b.RegistroID = a.DocumentoTransporteID) " +
                                "INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.EmpresaID) " +
                                "INNER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = a.EstadoID) " +
                                "INNER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (e.ItemID = a.TipoTransporteID) " +
                            "WHERE b.CodigoErro = 20 AND a.dtEmissao BETWEEN \'" + ddtInicio + "\' AND \'" + ddtFim + "\' AND b.TipoRegistroID = 1901 AND a.EmpresaID = " + sEmpresaID +
                            sFiltroEstado + sFiltroTransportadora;
            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", EmpresaNome, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "7.13834", "0.0", "11", "#0113a0", "B", Header_Body, "10.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "20.0", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");
                Relatorio.CriarObjLabel("Fixo", "", Title2, "0.001", "0.4", "11", "#0113a0", "B", Header_Body, "10.51332", "");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.6", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }
    }
}
