<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="faturatranspsup01Html" name="faturatranspsup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf               
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodtransporte/faturatransporte/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodtransporte/faturatransporte/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
	Dim strSQL, rsData
	Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
	Response.Write vbcrlf
	
	Response.Write "glb_nIdiomaID = 0;"
	Response.Write vbcrlf
	
	strSQL = "SELECT IdiomaID FROM Recursos WITH(NOLOCK) WHERE (RecursoID = 999)"
	
	Set rsData = Server.CreateObject("ADODB.Recordset")         
	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (NOT (rsData.BOF AND rsData.EOF)) Then
		Response.Write "glb_nIdiomaID = " & CStr(rsData.Fields("IdiomaID").Value) & ";"
	End If
	
	rsData.Close()
	Set rsData = Nothing
	Response.Write "</script>"
	Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--
//-->
</script>

</head>

<!-- //@@ -->
<body id="faturaTranspsup01Body" name="faturaTranspsup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral"/>
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->        
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral"></input>
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral"></input>
        <p id="lblNumero" name="lblNumero" class="lblGeneral">Numero</p>
        <input type="text" id="txtNumero" name="txtNumero" DATASRC="#dsoSup01" DATAFLD="Numero" class="fldGeneral" />
        <p id="lblSerie" name="lblSerie" class="lblGeneral">S�rie</p>
        <input type="text" id="txtSerie" name="txtSerie" DATASRC="#dsoSup01" DATAFLD="Serie" class="fldGeneral" />
        <p id="lbldtEmissao" name="lbldtEmissao" class="lblGeneral">Emiss�o</p>
        <input type="text" id="txtdtEmissao" name="txtdtEmissao" DATASRC="#dsoSup01" DATAFLD="V_dtEmissao" class="fldGeneral" /> 
        <p id="lbldtVencimento" name="lbldtVencimento" class="lblGeneral">Vencimento</p>
        <input type="text" id="txtdtVencimento" name="txtdtVencimento" DATASRC="#dsoSup01" DATAFLD="V_dtVencimento" class="fldGeneral" /> 
        <p id="lblIdentificador" name="lblIdentificador" class="lblGeneral">Identificador</p>
        <input type="text" id="txtIdentificador" name="txtIdentificador" DATASRC="#dsoSup01" DATAFLD="Identificador" class="fldGeneral" /> 
        <p id="lblTipoDocCob" name="lblTipoDocCob" class="lblGeneral">Tipo Documento</p>
        <input type="text" id="txtTipoDocCob" name="txtTipoDocCob" DATASRC="#dsoSup01" DATAFLD="TipoDocCob" class="fldGeneral" title="0 = NOTA FISCAL FATURA; 1 = ROMANEIO"/> 

        <p id="lblAutomatico" name="lblAutomatico" class="lblGeneral">Aut</p>
        <input type="checkbox" id="chkAutomatico" name="chkAutomatico" DATASRC="#dsoSup01" DATAFLD="Automatico" class="fldGeneral" title="A entrada da fatura ocorreu pelo processo EDI autom�tico do sistema."/>         
        
        <p id="lblFilialEmissFat" name="lblFilialEmissFat" class="lblGeneral">Filial Emissao</p>
        <input type="text" id="txtFilialEmissFat" name="txtFilialEmissFat" DATASRC="#dsoSup01" DATAFLD="FilEmissFatura" class="fldGeneral"/> 
        <p id="lblValTotalFatura" name="lblValTotalFatura" class="lblGeneral">Total Fatura</p>
        <input type="text" id="txtValTotalFatura" name="txtValTotalFatura" DATASRC="#dsoSup01" DATAFLD="ValorTotal" class="fldGeneral" /> 
        <p id="lblValTotalICMS" name="lblValTotalICMS" class="lblGeneral">Total ICMS</p>
        <input type="text" id="txtValTotalICMS" name="txtValTotalICMS" DATASRC="#dsoSup01" DATAFLD="ValorTotalImposto" class="fldGeneral" /> 
        <p id="lblJurosDiarios" name="lblJurosDiarios" class="lblGeneral">Valor Juros</p>
        <input type="text" id="txtJurosDiarios" name="txtJurosDiarios" DATASRC="#dsoSup01" DATAFLD="ValorEncargos" class="fldGeneral" /> 
        <p id="lblDesconto" name="lblDesconto" class="lblGeneral">Valor Desconto</p>
        <input type="text" id="txtDesconto" name="txtDesconto" DATASRC="#dsoSup01" DATAFLD="ValorDesconto" class="fldGeneral" /> 
        <p id="lbldtLimiteDesconto" name="lbldtLimiteDesconto" class="lblGeneral">Data Desconto</p>
        <input type="text" id="txtdtLimiteDesconto" name="txtdtLimiteDesconto" DATASRC="#dsoSup01" DATAFLD="V_dtLimiteDesconto" class="fldGeneral" /> 
        <p id="lblAgenteCobranca" name="lblAgenteCobranca" class="lblGeneral">Agente de Cobran�a</p>     
        <input type="text" id="txtAgenteCobranca" name="txtAgenteCobranca" DATASRC="#dsoSup01" DATAFLD="IDAgenteCob" class="fldGeneral" title="Banco"/> 
        <p id="lblAgencia" name="lblAgencia" class="lblGeneral">Agencia</p>     
        <input type="text" id="txtAgencia" name="txtAgencia" DATASRC="#dsoSup01" DATAFLD="NumAgenciaBancaria" class="fldGeneral" title="N�mero da Ag�ncia Banc�ria"/> 
        <p id="lblDigitoAgencia" name="lblDigitoAgencia" class="lblGeneral">DV</p>     
        <input type="text" id="txtDigitoAgencia" name="txtDigitoAgencia" DATASRC="#dsoSup01" DATAFLD="DigAgenciaBancaria" class="fldGeneral" title="Digito Verificador da Ag�ncia"/> 
        <p id="lblContaCorrente" name="lblContaCorrente" class="lblGeneral">Conta Corrente</p>     
        <input type="text" id="txtContaCorrente" name="txtContaCorrente" DATASRC="#dsoSup01" DATAFLD="NumContaCorrente" class="fldGeneral" title="N�mero da Conta Corrente"/> 
        <p id="lblDigitoConta" name="lblDigitoConta" class="lblGeneral">DV</p>     
        <input type="text" id="txtDigitoConta" name="txtDigitoConta" DATASRC="#dsoSup01" DATAFLD="DigContaCorrente" class="fldGeneral" title="Digito Verificador da Conta Corrente"/> 
        <p id="lblQuantidadeDocumentoTransporte" name="lblQuantidadeDocumentoTransporte" class="lblGeneral">Documentos de Transporte</p>
        <input type="text" id="txtQuantidadeDocumentoTransporte" name="txtQuantidadeDocumentoTransporte" DATASRC="#dsoSup01" DATAFLD="QuantidadeDocumentoTransporte" class="fldGeneral" title="Quantidade de Documento de Transporte no arquivo da Fatura."/>

        <p id="lblQuantidadeDocumentoTransporteEncontrados" name="lblQuantidadeDocumentoTransporteEncontrados" class="lblGeneral">Encontrados</p>
        <input type="text" id="txtQuantidadeDocumentoTransporteEncontrados" name="txtQuantidadeDocumentoTransporteEncontrados" DATASRC="#dsoSup01" DATAFLD="QuantidadeDocumentoTransporteEncontrados" class="fldGeneral" title="Quantidade de Documentos de transporte encontrados"/>
        <p id="lblQuantidadeDocumentoTransporteFaltantes" name="lblQuantidadeDocumentoTransporteFaltantes" class="lblGeneral">Faltantes</p>
        <input type="text" id="txtQuantidadeDocumentoTransporteFaltantes" name="txtQuantidadeDocumentoTransporteFaltantes" DATASRC="#dsoSup01" DATAFLD="QuantidadeDocumentoTransporteFaltantes" class="fldGeneral" title="Quantidade de Documentos faltantes"/>

        <p id="lblTotalParcelasFrete" name="lblTotalParcelasFrete" class="lblGeneral">Parcelas Frete</p>     
        <input type="text" id="txtTotalParcelasFrete" name="txtTotalParcelasFrete" DATASRC="#dsoSup01" DATAFLD="TotalParcelasFrete" class="fldGeneral" title="Soma das parcelas frete dos Documento de Transporte."/>         
        
        <p id="lblTransportadoraID" name="lblTransportadoraID" class="lblGeneral">Transportadora</p>
		<select id="selTransportadoraID" name="selTransportadoraID" DATASRC="#dsoSup01" DATAFLD="TransportadoraID" class="fldGeneral"></select>
		<input type="image" id="btnFindTransportadora" name="btnFindTransportadora" class="fldGeneral" title="Selecionar a transportadora" WIDTH="24" HEIGHT="23"></input>
		
        				
    </div>
    
</body>

</html>
