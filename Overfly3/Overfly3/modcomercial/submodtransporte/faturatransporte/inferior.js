/********************************************************************
faturatransporteinf.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_timerInfRefrInterval = null;

//Dados do registro corrente .SQL (no minimo: regID, observacoes, propID e alternativoID)
var dso01JoinSup = new CDatatransport("dso01JoinSup");
// Dados de combos estaticos do form .URL
//var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// Dados de combos dinamicos do form .URL
var dsoDynamicCmbs = new CDatatransport("dsoDynamicCmbs");
// Descricao dos estados atuais dos registros do grid corrente .SQL
var dsoStateMachineLkp = new CDatatransport("dsoStateMachineLkp");
// Dados do combo de filtros da barra inferior .SQL
var dsoFiltroInf = new CDatatransport("dsoFiltroInf");
// Dados dos combos de proprietario e alternativo
var dsoCmbsPropAlt = new CDatatransport("dsoCmbsPropAlt");

// Dados de um grid .SQL
var dso01Grid = new CDatatransport("dso01Grid");
var dsoCmb01Grid_01 = new CDatatransport("dsoCmb01Grid_01");
var dsoCmb01Grid_02 = new CDatatransport("dsoCmb01Grid_02");
var dso01PgGrid = new CDatatransport('dso01PgGrid');
var dso01GridLkp = new CDatatransport("dso01GridLkp");
var dso02GridLkp = new CDatatransport("dso02GridLkp");
var dso03GridLkp = new CDatatransport("dso03GridLkp");
var dso04GridLkp = new CDatatransport("dso04GridLkp");
var dso05GridLkp = new CDatatransport("dso05GridLkp");
var dso06GridLkp = new CDatatransport("dso06GridLkp");
var dso07GridLkp = new CDatatransport("dso07GridLkp");
var dsoParallelGrid = new CDatatransport("dsoParallelGrid");
var dsoSincronizacao = new CDatatransport("dsoSincronizacao");
var dsoPedidos = new CDatatransport("dsoPedidos");

// Cadastrado
var ESTADO_C = 1;
// Validado
var ESTADO_V = 8;
// Deletar
var ESTADO_Z = 5;
// Pendente
var ESTADO_N = 65;
// Liquidado
var ESTADO_L = 32;
//Fechado
var ESTADO_F = 48;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    adjustElementsInDivsNonSTD()
    putSpecialAttributesInControls()
    optChangedInCmb(cmb)
    prgServerInf(btnClicked)
    prgInterfaceInf(btnClicked, pastaID, dso)
    finalOfInfCascade(btnClicked)
    treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, currDSO)
    btnLupaClicked(btnClicked)
    addedLineInGrid(folderID, grid, nLineInGrid, dso)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    btnAltPressedInGrid( folderID )
    btnAltPressedInNotGrid( folderID )
    fillCmbFiltsInfAutomatic(folderID)
    selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
    frameIsAboutToApplyRights(folderID)

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG
    fg_AfterRowColChange_Prg()
    fg_DblClick_Prg()
    fg_ChangeEdit_Prg()
    fg_ValidateEdit_Prg()
    fg_BeforeRowColChange_Prg()
    fg_EnterCell_Prg()
    fg_MouseUp_Prg()
    fg_MouseDown_Prg()
    fg_BeforeEdit_Prg()

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    // Ze em 17/03/08
    dealWithGrid_Load();

    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array dos ids dos dsos que tenham campos linkados
    // a controles HTMLs.
    // Estes controles HTMls estao em divs e sao linkados a campos do dso
    // que sejam: obrigatorios, numericos ou de datas, nos dsos.
    // Se nao aplicavel ao form este array deve ser null
    glb_aDsosToCloneFieldsEtc = ['dso01JoinSup'];
    
    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;
    
    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:

    glb_aGridStaticCombos = [[20010, [[0, 'dso01GridLkp', '', ''],
                                      [0, 'dso02GridLkp', '', ''],
                                      [0, 'dso03GridLkp', '', ''],
                                      [0, 'dso04GridLkp', '', '']]],
						     [24352, [[0, 'dso05GridLkp', '', '']]],
                             [21067, [[0, 'dso06GridLkp', '', '']]],
                             [24356, [[0, 'dso07GridLkp', '', '']]]];

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03'],
                               [20008, 20009, [24354, 21067, 24352, 24351, 20010, 24011, 20161, 24356]]);

    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();
}

function setupPage()
{
    //@@ Ajustar os divs
    // Nada a codificar
    
    adjustDivs('inf',[[1,'divInf01_01'],
                      [1,'divInf01_02'],
                      [1,'divInf01_03']]);
}

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg()
{

}

function fg_DblClick_Prg()
{

}

function fg_ChangeEdit_Prg()
{

}
function fg_ValidateEdit_Prg()
{
    
}
function fg_BeforeRowColChange_Prg()
{
    
}

function fg_EnterCell_Prg()
{

}

function fg_MouseUp_Prg()
{

}

function fg_MouseDown_Prg()
{

}

function fg_BeforeEdit_Prg()
{

}

// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD()
{
    //@@
    // Este form nao tem este caso    
}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    // troca a pasta default para Traducoes
    //glb_pastaDefault = PASTA_PEDIDOS;
    //@@

    // troca a pasta default para itens
    glb_pastaDefault = 24351;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

     
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    /*
    if ( cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();*/
    
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div

	// Mover esta funcao para os finais retornos de operacoes no
	// banco de dados
	finalOfInfCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso)
{
    showBtnsEspecControlBar('inf', false, [1,1,1,1]);
    tipsBtnsEspecControlBar('inf', ['','','','']);

    if (pastaID == 24351) 
    {
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 1, 1]);
        tipsBtnsEspecControlBar('inf', ['Incluir Documento de Transporte', 'Pesquisar Documento de Transporte', 'Detalhar Documento de Transporte', 'Sincronizar Documento de Transporte Faltantes', 'Aprova��o em Lote']);
    }

    if (pastaID == 24352) {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Aprova��o', '', '', '']);
    }

    if (pastaID == 20161) // Financeiros
    {
        showBtnsEspecControlBar('inf', true, [1, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Financeiro', '', '']);
    }
    // Pedidos
    else if (pastaID == 21067) {
        showBtnsEspecControlBar('inf', true, [1, 1]);
        tipsBtnsEspecControlBar('inf', ['Gerar Pedidos', 'Detalhar Pedido']);
    }

    //Reembolso
    if (pastaID == 24356)
    {
        showBtnsEspecControlBar('inf', false, [0, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['', '', '', '']);
    }
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ( (btnClicked.toUpperCase()).indexOf('SUP') >= 0 )
        // usario clicou botao no control bar sup
	    sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
	else
	    // usario clicou botao no control bar inf
	    sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID) {
    var btn1;
    var btn2;
    var btn3;
    var btn4;
    var strBtns;
    var aStrBtns;
    var currEstadoID;
    var nTransportadoraArquivoDetalheID;

    //setupEspecBtnsControlBar('inf', 'DD');

    setupEspecBtnsControlBar('sup', 'HH');

    currEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
    chkAutomatico = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Automatico' + '\'' + '].value');
    nTransportadoraArquivoDetalheID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransportadoraArquivoDetalheID' + '\'' + '].value');

    //Desabilita bot�o alterar para DocTransporte autom�tico.
    if (chkAutomatico == 1) {
        var strBtns = currentBtnsCtrlBarString('sup');
        var aStrBtns = strBtns.split('');

        aStrBtns[3] = 'D';

        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window, 'sup', strBtns);
    }
    
    if (chkAutomatico == 0) {
        setupEspecBtnsControlBar('sup', 'DD');
    }
    
    // Desabilita os bot�es para exclus�o, altera��o e troca de estado se o estado for V.
    if ((currEstadoID == ESTADO_V) || (currEstadoID == ESTADO_N) || (currEstadoID == ESTADO_Z) || (currEstadoID == ESTADO_F))
    {
        strBtns = currentBtnsCtrlBarString('sup');
        aStrBtns = strBtns.split('');

        aStrBtns[1] = 'H';
        aStrBtns[3] = 'D';

        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window, 'sup', strBtns);
    }

    // Desabilita os bot�es para exclus�o, altera��o e troca de estado se for a Pasta Erros
    // Erros
    if (folderID == 24352) {
        strBtns = currentBtnsCtrlBarString('inf');
        aStrBtns = strBtns.split('');

        if (currEstadoID == ESTADO_C || currEstadoID == ESTADO_N)
            aStrBtns[3] = 'H';
        else
            aStrBtns[3] = 'D';
            
        aStrBtns[4] = 'D';
        aStrBtns[5] = 'D';
        aStrBtns[2] = 'D';

        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window, 'inf', strBtns);

        // 
        btn1 = (fg.Rows > 1 ? 'H' : 'D');
        btn2 = (fg.Rows > 1 ? 'H' : 'D');

        if ((currEstadoID == ESTADO_C) || (currEstadoID == ESTADO_N))
            setupEspecBtnsControlBar('inf', btn1 + btn2);
        else
            setupEspecBtnsControlBar('inf', 'D' + 'D');
    }

    // Financeiros
    if (folderID == 20161) {
        strBtns = currentBtnsCtrlBarString('inf');
        aStrBtns = strBtns.split('');

        if (currEstadoID == ESTADO_C || currEstadoID == ESTADO_N)
            aStrBtns[3] = 'H';
        else
            aStrBtns[3] = 'D';

        aStrBtns[4] = 'D';
        aStrBtns[5] = 'D';
        aStrBtns[2] = 'D';

        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window, 'inf', strBtns);

        // Bot�es 1, 2 e 3
        setupEspecBtnsControlBar('inf', 'HDD');
    }

    // Pedidos
    if (folderID == 21067) {
        strBtns = currentBtnsCtrlBarString('inf');
        aStrBtns = strBtns.split('');

        aStrBtns[2] = 'H';
        aStrBtns[3] = 'H';
        aStrBtns[4] = 'D';
        aStrBtns[5] = 'H';

        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window, 'inf', strBtns);

        // Bot�es 1, 2
        setupEspecBtnsControlBar('inf', 'HD');
    }
    
    // Totais
    if (folderID == 24354) {
        strBtns = currentBtnsCtrlBarString('inf');
        aStrBtns = strBtns.split('');

        aStrBtns[2] = 'D';
        aStrBtns[3] = 'D';
        aStrBtns[4] = 'D';
        aStrBtns[5] = 'D';

        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window, 'inf', strBtns);
    }
    
    // Documentos de Transporte
    if (folderID == 24351) 
    {
        strBtns = currentBtnsCtrlBarString('inf');
        aStrBtns = strBtns.split('');

        aStrBtns[2] = 'D';
        aStrBtns[4] = 'D';
        aStrBtns[3] = 'D';


        if (((currEstadoID == ESTADO_C) || (currEstadoID == ESTADO_N)) && (fg.Rows > 1))
            aStrBtns[5] = 'H';

        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window, 'inf', strBtns);

        btn1 = 'D';
        btn2 = (fg.Rows > 1 ? 'H' : 'D');
        btn3 = (fg.Rows > 1 ? 'H' : 'D');
        btn4 = 'D';
        btn5 = 'D';

        if (currEstadoID == ESTADO_C) {
            if (chkAutomatico == 1)
                btn4 = 'H';
            
            btn1 = 'H';
        }
        else if (currEstadoID == ESTADO_N)
            btn4 = 'H';

        // Apenas mostra bot�o 4 se for automatico
        if (chkAutomatico == 0) {
            btn4 = 'D';
        }

        // Se existe linhas ent�o � possivel aceder � aprova��o em lote
        if (fg.Rows > 1) {
            btn5 = 'H';
        }

        btn4 = 'H';

        setupEspecBtnsControlBar('inf', btn1 + btn2 + btn3 + btn4 + btn5);
            
        
/*      
        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
              'fillCmbsFiltInf_DSC(' + folderID + ')');    */
    }

    // Pedidos
    else if (folderID == 21067) {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HH');
        else
            setupEspecBtnsControlBar('inf', 'DD');
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso) {

    //DocumentoTransporte
    if (folderID == 24351)
        setupEspecBtnsControlBar('inf', 'DDDD');  
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    var aEmpresa = getCurrEmpresaData();
    var DocumentoTransporteID = 0;

    if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 24351))
        openModalincluiDocumentoTransporte();
    else if ((controlBar == 'INF') && (btnClicked == 2) && (keepCurrFolder() == 24351))
        openModalPesquisaDocumentoTransporte();
    else if ((controlBar == 'INF') && (btnClicked == 4) && (keepCurrFolder() == 24351))
        sincronizaFaturaDocumentoTransporte();
    else if ((controlBar == 'INF') && (btnClicked == 5) && (keepCurrFolder() == 24351))
        aprovacaoLote();
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 24352))
        openModalAprovacaoFatura();
    else if ((controlBar == 'INF') && (btnClicked == 3) && (keepCurrFolder() == 24351)) {
        if (fg.Rows > 1) {
            //Detalhar DocumentoTransporte
            sendJSCarrier(getHtmlId(), 'SHOWDOCUMENTOTRANSPORTE', new Array(aEmpresa[0], fg.TextMatrix(fg.Row, 0)));
        }
    }
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 20161))
        sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(aEmpresa[0], fg.TextMatrix(fg.Row, 0)));
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 21067))
        gerarPedido();
    else if ((controlBar == 'INF') && (btnClicked == 2) && (keepCurrFolder() == 21067))
        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(aEmpresa[0], parseInt(fg.TextMatrix(fg.Row, 0), 10)));
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
- Avancar, retroagir, incluir, alterar, estado, 
excluir, OK, cancelar refresh, anterior e proximo. 
(Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) 
{
    if (idElement.toUpperCase() == 'INCLUIDOCUMENTOTRANSPORTEHTML') 
    {
        if (param1 == 'OK') 
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            //__btn_REFR('inf');
            // escreve na barra de status
            //return 0;
        }
        else if (param1 == 'CANCEL') 
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            //__btn_REFR('inf');
            //return 0;
        }

        __btn_REFR('inf');

        return 0;
    }
    else if (idElement.toUpperCase() == 'PESQUISADOCUMENTOTRANSPORTEHTML') 
    {
        if (param1 == 'OK')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            //__btn_REFR('inf');
            // escreve na barra de status
            //return 0;
        }
        else if (param1 == 'CANCEL') 
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            //__btn_REFR('inf');
            //return 0;
        }

        return 0;
    }
    else if (idElement.toUpperCase() == 'APROVACAOFATURAHTML') 
    {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            __btn_REFR('inf');
            // escreve na barra de status
            //return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            __btn_REFR('inf');
            //return 0;
        }

        return 0;
    }
    else if (idElement.toUpperCase() == 'APROVACAOLOTEHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            __btn_REFR('inf');
            // escreve na barra de status
            //return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            __btn_REFR('inf');
            //return 0;
        }

        return 0;
    }
    else if (idElement.toUpperCase() == 'VINCULANOTAFISCALHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            //__btn_REFR('inf');
            // escreve na barra de status
            //return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            //__btn_REFR('inf');
            //return 0;
        }

        __btn_REFR('inf');

        return 0;
    }
    
    return 0;
}

function refresh() 
{
    __btn_REFR('inf');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid(folderID) {
    setupEspecBtnsControlBar('inf', 'DDDDDD');

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid(folderID) {
    ;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID) {
    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.
    if (folderID == 24351)
        glb_scmbKeyPesqSel = 'bNumero';

    return true;
}


/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
{
    //@@ retornar null para prosseguir a automacao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra infeior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID)
{
    var currEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

	// Pasta de LOG Read Only
	if (folderID == 20010) {
		// trava as barras do inf para a pasta acima definida
		glb_BtnsIncAltEstExcl[0] = true;
		glb_BtnsIncAltEstExcl[1] = true;
		glb_BtnsIncAltEstExcl[2] = true;
		glb_BtnsIncAltEstExcl[3] = true;
    }

    //Pasta DocumentoTransporte
    if (folderID == 24351) {
        if ((currEstadoID == ESTADO_V) || (currEstadoID == ESTADO_Z) || (currEstadoID == ESTADO_L)) {
            //Desabilita o Bot�o Excluir quando o Documento de Transporte estiver em V ou N.
            glb_BtnsIncAltEstExcl[3] = true;
        } 
  
    }
    /*//Pasta pedidos
    else if (folderID == 21067)
    {
        if ((currEstadoID == ESTADO_N) || (currEstadoID == ESTADO_C))
        {
            glb_BtnsIncAltEstExcl[0] = false;
            glb_BtnsIncAltEstExcl[1] = false;
            glb_BtnsIncAltEstExcl[2] = false;
            glb_BtnsIncAltEstExcl[3] = false;
        }
    }*/
    

	return null;
}

function verifyDocumentoTransporteID_INF(nDocumentoTransporteID) {
    for (a = 2; a < fg.Rows; a++) {
        if (nDocumentoTransporteID == dso01Grid.recordset['DocumentoTransporteID'].value) {
            var bTemDocumentoTransporteID = 1;
            return bTemDocumentoTransporteID;
        }    
    }
}


/********************************************************************
Retorno do servidor - Sincronizar Adi��es
********************************************************************/
function openModalincluiDocumentoTransporte() {

    //var nEstadoID;
    var htmlPath;
    var strPars = "";
    var nCurrEmpresa;
    var nTransportadoraID;

    //var aEmpresa = getCurrEmpresaData();

    //nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");



    nCurrEmpresa = getCurrEmpresaData();
    nTransportadoraID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransportadoraID' + '\'' + '].value');

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa[0]);
    strPars += '&sCaller=' + escape('I');
    strPars += '&nTransportadoraID=' + escape(nTransportadoraID);

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodtransporte/faturatransporte/modalpages/incluidocumentotransporte.asp' + strPars;
    showModalWin(htmlPath, new Array(800, 300));
}

/********************************************************************
Retorno do servidor - Sincronizar Adi��es
********************************************************************/
function openModalVinculaNotaFiscal() {

    //var nEstadoID;
    var htmlPath;
    var strPars = "";
    var nCurrEmpresa;
    var nTransportadoraID;

    //var aEmpresa = getCurrEmpresaData();

    //nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");



    nCurrEmpresa = getCurrEmpresaData();
    nTransportadoraID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransportadoraID' + '\'' + '].value');

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa[0]);
    strPars += '&sCaller=' + escape('I');
    strPars += '&nTransportadoraID=' + escape(nTransportadoraID);

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodtransporte/faturatransporte/modalpages/vinculanotafiscal.asp' + strPars;
    showModalWin(htmlPath, new Array(800, 300));
}





/********************************************************************
Retorno do servidor - Sincronizar Adi��es
********************************************************************/
function openModalPesquisaDocumentoTransporte() {

    //var nEstadoID;
    var htmlPath;
    var strPars = "";
    var nCurrEmpresa;

    nCurrEmpresa = getCurrEmpresaData();

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa[0]);
    strPars += '&sCaller=' + escape('I');

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodtransporte/faturatransporte/modalpages/modalpesquisadocumentotransporte.asp' + strPars;
    showModalWin(htmlPath, new Array(800, 300));
}

function openModalAprovacaoFatura()
{

    //var nEstadoID;
    var htmlPath;

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodtransporte/faturatransporte/modalpages/modalaprovacaofatura.asp';
    showModalWin(htmlPath, new Array(800, 400));
}

function sincronizaFaturaDocumentoTransporte() {

    lockInterface(true);

    var strPars = "";
    var nCurrEmpresa;
    var nTransportadoraArquivoDetalheID;
    var nFaturaID = null;

    nFaturaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FaturaID' + '\'' + '].value');
    nTransportadoraArquivoDetalheID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransportadoraArquivoDetalheID' + '\'' + '].value');

    // mandar os parametros para o servidor
    strPars = '?nFaturaID=' + escape(nFaturaID);
    strPars += '&sCaller=' + escape('I');
    strPars += '&nTransportadoraArquivoDetalheID=' + escape(nTransportadoraArquivoDetalheID);

    if (nFaturaID != null && nTransportadoraArquivoDetalheID != null) 
    {
        setConnection(dsoSincronizacao);
        dsoSincronizacao.URL = SYS_ASPURLROOT + '/modcomercial/submodtransporte/faturatransporte/serverside/sincronizaFaturaDocumentoTransporte.aspx' + strPars;
        dsoSincronizacao.ondatasetcomplete = sincronizaFaturaDocumentoTransporte_DSC;
        dsoSincronizacao.refresh();
    }


}

/********************************************************************
Retorno do servidor - Sincroniza��o do DocumentoTransporte
********************************************************************/
function sincronizaFaturaDocumentoTransporte_DSC() {
    var sResultado = dsoSincronizacao.recordset['Resultado'].value;

    lockInterface(false);

    if ((sResultado != null) && (sResultado != ''))
        window.top.overflyGen.Alert(sResultado);
        __btn_REFR('inf');
}

function aprovacaoLote() {
    var htmlPath;
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodtransporte/faturatransporte/modalpages/modalaprovacaolote.asp';
    showModalWin(htmlPath, new Array(980, 580));
}



function gerarPedido() {

    lockInterface(true);

    var strPars = "";
    var nCurrEmpresa;
    var nFaturaID = null;
    var userID = getCurrUserID();

    nFaturaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FaturaID' + '\'' + '].value');
    

    // mandar os parametros para o servidor
    strPars = '?nFaturaID=' + escape(nFaturaID);
    strPars += '&sCaller=' + escape('I');
    strPars += '&nUserID=' + escape(userID);

    if (nFaturaID != null && userID != null) {
        setConnection(dsoPedidos);
        dsoPedidos.URL = SYS_ASPURLROOT + '/modcomercial/submodtransporte/faturatransporte/serverside/gerarPedido.aspx' + strPars;
        dsoPedidos.ondatasetcomplete = gerarPedido_DSC;
        dsoPedidos.refresh();
    }


}

/********************************************************************
Retorno do servidor - Sincroniza��o do DocumentoTransporte
********************************************************************/
function gerarPedido_DSC() {
    var sResultado = dsoPedidos.recordset['Resultado'].value;

    lockInterface(false);

    if ((sResultado != null) && (sResultado != '')) {
        window.top.overflyGen.Alert(sResultado);
    }
    else {
        window.top.overflyGen.Alert("Processo finalizado!"/*sResultado*/);
    }
    __btn_REFR('inf');
}
