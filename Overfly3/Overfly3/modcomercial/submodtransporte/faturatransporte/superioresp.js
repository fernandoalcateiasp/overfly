/********************************************************************
js_especificsup.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister) {

   var sSQL;
   
    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 a.*, ' +
			   '0 AS Automatico, ' +
               'CONVERT(VARCHAR, a.dtEmissao, ' + DATE_SQL_PARAM + ') AS V_dtEmissao, ' +
               'CONVERT(VARCHAR, a.dtVencimento, ' + DATE_SQL_PARAM + ') AS V_dtVencimento, ' +
               '(SELECT c.Identificador ' +
			        'FROM dbo.TransportadoraDados_Detalhes b WITH ( NOLOCK )  ' +
			            'INNER JOIN dbo.TransportadoraDados c WITH ( NOLOCK ) ON ( b.TransportadoraArquivoID = c.TransportadoraArquivoID ) ' +
			        'WHERE ( a.TransportadoraArquivoDetalheID = b.TransportadoraArquivoDetalheID )) AS Identificador, ' +

               '(SELECT COUNT(1) ' +
	                'FROM TransportadoraDados_Detalhes aa WITH(NOLOCK) ' +
		                'WHERE aa.TransportadoraArquivoID = (SELECT bb.TransportadoraArquivoID ' +
												                'FROM TransportadoraDados_Detalhes bb WITH(NOLOCK) ' +
													                'WHERE (bb.TransportadoraArquivoDetalheID = a.TransportadoraArquivoDetalheID)) ' +
				                'AND (aa.TipoRegistroID = 353)) AS QuantidadeDocumentoTransporte, ' +

               '(SELECT COUNT(1) ' +
	                'FROM FaturasTransporte_DocumentosTransporte aa WITH(NOLOCK) ' +
		                'WHERE aa.FaturaID = a.FaturaID) AS QuantidadeDocumentoTransporteEncontrados, ' +

               '(SELECT COUNT(1) FROM TransportadoraDados_Detalhes aa WITH(NOLOCK) ' +
	               'WHERE aa.TipoRegistroID = 353 AND ((SELECT COUNT(1) ' +
											               'FROM FaturasTransporte_DocumentosTransporte aaa WITH(NOLOCK) ' +
												               'INNER JOIN DocumentosTransporte bbb WITH(NOLOCK) ON (bbb.DocumentoTransporteID = aaa.DocumentoTransporteID) ' +
											               'WHERE aaa.FaturaID =   a.FaturaID   AND bbb.EstadoID <> 5 AND bbb.Numero = SUBSTRING(aa.ConteudoLinha, 19, 12)) = 0) ' +
						'AND aa.TransportadoraArquivoDetalheID > a.TransportadoraArquivoDetalheID ' +
						'AND aa.TransportadoraArquivoDetalheID < (SELECT TOP 1 aaa.TransportadoraArquivoDetalheID ' +
															'FROM TransportadoraDados_Detalhes aaa WITH(NOLOCK) ' +
															'WHERE aaa.TransportadoraArquivoDetalheID > a.TransportadoraArquivoDetalheID AND TipoRegistroID IN (352, 355))) AS QuantidadeDocumentoTransporteFaltantes, ' +

				"(SELECT ISNULL(SUM(VlrTotParc), 0) " +
					"FROM FaturasTransporte_DocumentosTransporte aa WITH(NOLOCK) " +
						"INNER JOIN DocumentosTransporte_NotasFiscais bb WITH(NOLOCK) ON (bb.DocumentoTransporteID = aa.DocumentoTransporteID) " +
						"INNER JOIN Pedidos cc WITH(NOLOCK) ON (cc.NotaFiscalID = bb.NotaFiscalID) " +
						"INNER JOIN vw_PedidoTotais_Parcelas dd WITH(NOLOCK,NOEXPAND) ON (dd.PedidoID = cc.PedidoID) " +
						"INNER JOIN DocumentosTransporte ee WITH(NOLOCK) ON (ee.DocumentoTransporteID = bb.DocumentoTransporteID) " +
					"WHERE (aa.FaturaID = a.FaturaID) AND dd.HistoricoPadraoID = 3173 AND ee.TipoTransporteID = 901) AS TotalParcelasFrete, " +
               '0 AS Prop1, 0 AS Prop2, ' +
               'CONVERT(VARCHAR, a.dtLimiteDesconto, ' + DATE_SQL_PARAM + ') AS V_dtLimiteDesconto ' +
                'FROM FaturasTransporte a WITH(NOLOCK) ' +
                'WHERE a.ProprietarioID = ' + nID + ' ORDER BY a.FaturaID DESC';

    else
        sSQL = 'SELECT TOP 1 a.*, ' +
               '(CASE ISNULL(dbo.fn_Log_Usuario(5420,24350, ' + nID + ', 28, NULL, NULL, 0), 999) WHEN 999 THEN 1 ELSE 0 END) AS Automatico, ' +
               'CONVERT(VARCHAR, a.dtEmissao, ' + DATE_SQL_PARAM + ') AS V_dtEmissao, ' +
               'CONVERT(VARCHAR, a.dtVencimento, ' + DATE_SQL_PARAM + ') AS V_dtVencimento, ' +
               '(SELECT c.Identificador ' +
			        'FROM dbo.TransportadoraDados_Detalhes b WITH ( NOLOCK )  ' +
			            'INNER JOIN dbo.TransportadoraDados c WITH ( NOLOCK ) ON ( b.TransportadoraArquivoID = c.TransportadoraArquivoID ) ' +
			        'WHERE ( a.TransportadoraArquivoDetalheID = b.TransportadoraArquivoDetalheID )) AS Identificador, ' +
		        '(SELECT COUNT(1) ' +
			        'FROM TransportadoraDados_Detalhes aa WITH(NOLOCK) ' +
			        'WHERE aa.TransportadoraArquivoID = b.TransportadoraArquivoID AND aa.TipoRegistroID = 353 ' +
				        'AND aa.TransportadoraArquivoDetalheID > a.TransportadoraArquivoDetalheID ' +
				        'AND aa.TransportadoraArquivoDetalheID <  ' +
					        '(SELECT TOP 1 aaa.TransportadoraArquivoDetalheID  ' +
						        'FROM TransportadoraDados_Detalhes aaa WITH(NOLOCK) ' +
						        'WHERE aaa.TransportadoraArquivoID = b.TransportadoraArquivoID AND TipoRegistroID IN (352, 355) ' +
							        'AND aaa.TransportadoraArquivoDetalheID > a.TransportadoraArquivoDetalheID)) AS QuantidadeDocumentoTransporte, ' +
		        '(SELECT COUNT(1) FROM FaturasTransporte_DocumentosTransporte aa WITH(NOLOCK) WHERE aa.FaturaID = a.FaturaID) AS QuantidadeDocumentoTransporteEncontrados, ' +
		        '(SELECT COUNT(1)  ' +
			        'FROM TransportadoraDados_Detalhes aa WITH(NOLOCK) ' +
			        'WHERE aa.TransportadoraArquivoID = b.TransportadoraArquivoID AND aa.TipoRegistroID = 353 ' +
				        'AND aa.TransportadoraArquivoDetalheID > a.TransportadoraArquivoDetalheID ' +
				        'AND aa.TransportadoraArquivoDetalheID <  ' +
					        '(SELECT TOP 1 aaa.TransportadoraArquivoDetalheID  ' +
						        'FROM TransportadoraDados_Detalhes aaa WITH(NOLOCK) ' +
						        'WHERE aaa.TransportadoraArquivoID = b.TransportadoraArquivoID AND TipoRegistroID IN (352, 355) ' +
							        'AND aaa.TransportadoraArquivoDetalheID > a.TransportadoraArquivoDetalheID) ' +
				        'AND ((SELECT COUNT(1) ' +
						        'FROM FaturasTransporte_DocumentosTransporte bbb WITH(NOLOCK) ' +
							        'INNER JOIN DocumentosTransporte ccc WITH(NOLOCK) ON (ccc.DocumentoTransporteID = bbb.DocumentoTransporteID) ' +
						        'WHERE bbb.FaturaID = a.FaturaID AND SUBSTRING(aa.ConteudoLinha, 19, 12) = ccc.Numero) = 0)) AS QuantidadeDocumentoTransporteFaltantes, ' +
				'(SELECT ISNULL(SUM(VlrTotParc), 0) ' +
					'FROM FaturasTransporte_DocumentosTransporte aa WITH(NOLOCK) ' +
						'INNER JOIN DocumentosTransporte_NotasFiscais bb WITH(NOLOCK) ON (bb.DocumentoTransporteID = aa.DocumentoTransporteID) ' +
						'INNER JOIN Pedidos cc WITH(NOLOCK) ON (cc.NotaFiscalID = bb.NotaFiscalID) ' +
						'INNER JOIN vw_PedidoTotais_Parcelas dd WITH(NOLOCK,NOEXPAND) ON (dd.PedidoID = cc.PedidoID) ' +
						'INNER JOIN DocumentosTransporte ee WITH(NOLOCK) ON (ee.DocumentoTransporteID = bb.DocumentoTransporteID) ' +
					'WHERE (aa.FaturaID = a.FaturaID) AND dd.HistoricoPadraoID = 3173 AND ee.TipoTransporteID = 901) AS TotalParcelasFrete, ' +
               '0 AS Prop1, 0 AS Prop2, ' +
               'CONVERT(VARCHAR, a.dtLimiteDesconto, ' + DATE_SQL_PARAM + ') AS V_dtLimiteDesconto ' +
                'FROM FaturasTransporte a WITH(NOLOCK) ' +
                'LEFT JOIN TransportadoraDados_Detalhes b WITH(NOLOCK) ON (b.TransportadoraArquivoDetalheID = a.TransportadoraArquivoDetalheID) ' +
                'WHERE a.FaturaID = ' + nID;
     
    setConnection(dso);

    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;

    sql = 'SELECT TOP 1 a.*, ' +
               'CONVERT(VARCHAR, a.dtEmissao, ' + DATE_SQL_PARAM + ') AS V_dtEmissao, ' +
               'CONVERT(VARCHAR, a.dtVencimento, ' + DATE_SQL_PARAM + ') AS V_dtVencimento, ' +
               '(SELECT c.Identificador ' +
			        'FROM dbo.TransportadoraDados_Detalhes b WITH ( NOLOCK )  ' +
			            'INNER JOIN dbo.TransportadoraDados c WITH ( NOLOCK ) ON ( b.TransportadoraArquivoID = c.TransportadoraArquivoID ) ' +
			        'WHERE ( a.TransportadoraArquivoDetalheID = b.TransportadoraArquivoDetalheID )) AS Identificador, ' +

               '(SELECT COUNT(1) ' +
	                'FROM TransportadoraDados_Detalhes aa WITH(NOLOCK) ' +
		                'WHERE aa.TransportadoraArquivoID = (SELECT bb.TransportadoraArquivoID ' +
												                'FROM TransportadoraDados_Detalhes bb WITH(NOLOCK) ' +
													                'WHERE (bb.TransportadoraArquivoDetalheID = a.TransportadoraArquivoDetalheID)) ' +
				                'AND (aa.TipoRegistroID = 353)) AS QuantidadeDocumentoTransporte, ' +

               '(SELECT COUNT(1) ' +
	                'FROM FaturasTransporte_DocumentosTransporte aa WITH(NOLOCK) ' +
		                'WHERE aa.FaturaID = a.FaturaID) AS QuantidadeDocumentoTransporteEncontrados, ' +

               '(SELECT COUNT(1) FROM TransportadoraDados_Detalhes aa WITH(NOLOCK) ' +
	               'WHERE aa.TipoRegistroID = 353 AND ((SELECT COUNT(1) ' +
											               'FROM FaturasTransporte_DocumentosTransporte aaa WITH(NOLOCK) ' +
												               'INNER JOIN DocumentosTransporte bbb WITH(NOLOCK) ON (bbb.DocumentoTransporteID = aaa.DocumentoTransporteID) ' +
											               'WHERE aaa.FaturaID =   a.FaturaID   AND bbb.EstadoID <> 5 AND bbb.Numero = SUBSTRING(aa.ConteudoLinha, 19, 12)) = 0) ' +
						'AND aa.TransportadoraArquivoDetalheID > a.TransportadoraArquivoDetalheID ' +
						'AND	aa.TransportadoraArquivoDetalheID < (SELECT TOP 1 aaa.TransportadoraArquivoDetalheID ' +
															'FROM TransportadoraDados_Detalhes aaa WITH(NOLOCK) ' +
															'WHERE aaa.TransportadoraArquivoDetalheID > a.TransportadoraArquivoDetalheID AND TipoRegistroID IN (352, 355))) AS QuantidadeDocumentoTransporteFaltantes, ' +

				"(SELECT ISNULL(SUM(VlrTotParc), 0) " +
					"FROM FaturasTransporte_DocumentosTransporte aa WITH(NOLOCK) " +
						"INNER JOIN DocumentosTransporte_NotasFiscais bb WITH(NOLOCK) ON (bb.DocumentoTransporteID = aa.DocumentoTransporteID) " +
						"INNER JOIN Pedidos cc WITH(NOLOCK) ON (cc.NotaFiscalID = bb.NotaFiscalID) " +
						"INNER JOIN vw_PedidoTotais_Parcelas dd WITH(NOLOCK,NOEXPAND) ON (dd.PedidoID = cc.PedidoID) " +
						"INNER JOIN DocumentosTransporte ee WITH(NOLOCK) ON (ee.DocumentoTransporteID = bb.DocumentoTransporteID) " +
					"WHERE (aa.FaturaID = a.FaturaID) AND dd.HistoricoPadraoID = 3173 AND ee.TipoTransporteID = 901) AS TotalParcelasFrete, " +
               '0 AS Prop1, 0 AS Prop2, ' +
               'CONVERT(VARCHAR, a.dtLimiteDesconto, ' + DATE_SQL_PARAM + ') AS V_dtLimiteDesconto ' +
                'FROM FaturasTransporte a WITH(NOLOCK) ' +
                'WHERE a.FaturaID = 0 ORDER BY a.FaturaID DESC';  

    dso.SQL = sql;          
}              
