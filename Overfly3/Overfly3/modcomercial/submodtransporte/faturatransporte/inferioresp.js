/********************************************************************
especificinf.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;
var glb_bRefresh;
var glb_sFiltr;
var glb_idToFind;

// FINAL DE VARIAVEIS GLOBAIS ***************************************
// Cabe�alho, totaliza��o das linhas do grid, para folderID 24351 (desconsidera doctransporte duplicado)
var dsoTotalDocTransporte = new CDatatransport('dsoTotalDocTransporte');

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/

function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    glb_sFiltr = sFiltro;
    glb_idToFind = idToFind;
    

    // Observacoes ou Prop/Altern
    if ((folderID == 20008) || (folderID == 20009)) {
        dso.SQL = 'SELECT a.FaturaID, a.ProprietarioID, a.AlternativoID, a.Observacoes FROM FaturasTransporte a WITH (NOLOCK) WHERE ' +
			sFiltro + 'a.FaturaID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010) {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.RegistroID = ' + idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // DocumentoTransporte
    else if (folderID == 24351) {
        dso.SQL = 'SELECT b.*, a.Numero AS NumeroDocumentoTransporte, ' +
		                    'f.RecursoAbreviado AS Estado, ' +
		                    'a.dtEmissao AS dtEmissaoDocumentoTransporte, ' +
		                    'a.ValorTotal, ' +
		                    'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID, 1) AS ValorTotalParcelasFrete, ' +
		                    'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID, 2) AS ValorDiferenca, ' +
		                    'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID, 3) AS PercentualDiferenca, ' +
		                    'dbo.fn_TipoAuxiliar_Item(a.TipoDocumentoTransporteID,2) AS Tipo, ' +
		                    'd.MensagemErro AS Problema, ' +
		                    'a.Observacao AS Obs2 ' +
	                    'FROM DocumentosTransporte a WITH(NOLOCK) ' +
		                    'INNER JOIN FaturasTransporte_DocumentosTransporte b WITH(NOLOCK) ON (b.DocumentoTransporteID = a.DocumentoTransporteID) ' +
		                    'LEFT OUTER JOIN TransportadoraDados_Detalhes_Erros c WITH(NOLOCK)  ' +
			                    'ON (c.RegistroID = a.DocumentoTransporteID AND c.TipoRegistroID = 1901) ' +
		                    'LEFT OUTER JOIN FreteErros d WITH(NOLOCK) ON (d.FreteErroID = c.CodigoErro) ' +
		                    'INNER JOIN Recursos f WITH(NOLOCK) ON (f.RecursoID = a.EstadoID) ' +
	                    'WHERE ' + sFiltro + ' b.FaturaID = ' + idToFind + ' ' +
	                    'ORDER BY a.DocumentoTransporteID';

        return 'dso01Grid_DSC';
    }

    // Erros
    else if (folderID == 24352) {
        dso.SQL = 'SELECT a.* ' +
	            'FROM dbo.TransportadoraDados_Detalhes_Erros a WITH(NOLOCK) ' +
	            'WHERE (a.RegistroID = ' + idToFind + ') AND (a.TipoRegistroID = 1902) ' +
                'ORDER BY a.CodigoErro';
        return 'dso01Grid_DSC';
    }

    // Financeiros
    else if (folderID == 20161) {
          dso.SQL = 'SELECT a.*, ' +
                            'c.SimboloMoeda, ' +
                            'd.RecursoAbreviado, ' +
                            'b.PedidoID, ' +
                            'b.Duplicata, ' +
                            'e.Fantasia AS Pessoa, ' +
                            'f.ItemAbreviado, ' +
                            'b.PrazoPagamento, ' +
                            'b.dtEmissao, ' +
                            'b.dtVencimento, ' +
                            'b.Valor, ' +
                            'dbo.fn_Financeiro_Posicao(b.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, ' +
                            'dbo.fn_Financeiro_Posicao(b.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, ' +
                            '\'Fatura\' AS Tipo, ' +
                            'a.FatFinanceiroID ' +
                    'FROM FaturasTransporte_Financeiros a WITH(NOLOCK) ' +
                        'INNER JOIN Financeiro b WITH(NOLOCK) ON (b.FinanceiroID = a.FinanceiroID) ' +
                        'INNER JOIN Conceitos c WITH(NOLOCK) ON (b.MoedaID = c.ConceitoID) ' +
                        'INNER JOIN Recursos d WITH(NOLOCK) ON (b.EstadoID = d.RecursoID) ' +
                        'INNER JOIN Pessoas e WITH(NOLOCK) ON (b.PessoaID = e.PessoaID) ' +
                        'INNER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (b.FormaPagamentoID = f.ItemID) ' +
                    'WHERE a.FaturaID = ' + idToFind + ' ' +
                /*
                    'UNION ALL ' +
                    'SELECT a.*, ' +
                        'd.SimboloMoeda, ' +
                        'e.RecursoAbreviado, ' +
                        'c.PedidoID, ' +
                        'c.Duplicata, ' +
                        'f.Fantasia AS Pessoa, ' +
                        'g.ItemAbreviado, ' +
                        'c.PrazoPagamento, ' +
                        'c.dtEmissao, ' +
                        'c.dtVencimento, ' +
                        'c.Valor, ' +
                        'dbo.fn_Financeiro_Posicao(c.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, ' +
                        'dbo.fn_Financeiro_Posicao(c.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, ' +
                        '\'Pedido\' AS Tipo, ' +
                        'a.FatPedidoID ' +
                    'FROM FaturasTransporte_Pedidos a WITH(NOLOCK) ' +
                        'INNER JOIN Pedidos_Parcelas b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) ' +
                        'INNER JOIN Financeiro c WITH(NOLOCK) ON (c.FinanceiroID = b.FinanceiroID) ' +
                        'INNER JOIN Conceitos d WITH(NOLOCK) ON (c.MoedaID = d.ConceitoID) ' +
                        'INNER JOIN Recursos e WITH(NOLOCK) ON (c.EstadoID = e.RecursoID) ' +
                        'INNER JOIN Pessoas f WITH(NOLOCK) ON (c.PessoaID = f.PessoaID) ' +
                        'INNER JOIN TiposAuxiliares_Itens g WITH(NOLOCK) ON (c.FormaPagamentoID = g.ItemID) ' +
                    'WHERE a.FaturaID = ' + idToFind + ' ' +*/
                    'ORDER BY a.FinanceiroID ';

        return 'dso01Grid_DSC';
    }
    // Pedidos
    else if (folderID == 21067){
        dso.SQL = 'SELECT a.FaturaID AS FaturaID, '+
                  'a.PedidoID AS PedidoID, ' +
                  'a.Observacao AS Observacao, ' +
                  'a.NotaFiscal AS NotaFiscal, ' +
                  'a.ChaveAcesso AS ChaveAcesso, ' +
                  'a.CNPJ AS CNPJ, ' + 
                  'a.Codigo AS Codigo, ' +
                  'a.ValorTotalNota AS ValorTotalNota, ' +
                  'a.FatPedidoID AS FatPedidoID ' +
	              'FROM FaturasTransporte_Pedidos a WITH(NOLOCK) ' +
		                /*'INNER JOIN Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) ' +
		                'INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = b.EstadoID) ' +
		                'INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = b.MoedaID) ' +
		                'INNER JOIN Pessoas e WITH(NOLOCK) ON (e.PessoaID = b.EmpresaID) ' +
		                'INNER JOIN Operacoes g WITH(NOLOCK) ON (g.OperacaoID = TransacaoID) ' +
		                'LEFT OUTER JOIN NotasFiscais h WITH(NOLOCK) ON (h.NotaFiscalID = b.NotaFiscalID) ' +*/
	                'WHERE a.FaturaID =  ' + idToFind;

        return 'dso01Grid_DSC';
    }
    // Totais
    else if (folderID == 24354) {
        dso.SQL = 'SELECT CONVERT(VARCHAR(7),dbo.fn_TipoAuxiliar_Item(c.TipoDocumentoTransporteID, 2)) AS Tipo, ' +
	                    'COUNT(1) AS Qtd, ' +
	                    '(CASE c.TipoDocumentoTransporteID WHEN 990 THEN \'ICMS\' WHEN 991 THEN \'ISSQN\' ELSE \'\' END) AS TipoImposto, ' +
	                    'CONVERT(NUMERIC(11,2), SUM(c.ValorImposto)) AS TotalImposto, ' +
	                    'CONVERT(NUMERIC(11,2), SUM(c.ValorTotal)) AS ValorTotal ' +
                    'FROM FaturasTransporte a WITH(NOLOCK) ' +
	                    'INNER JOIN FaturasTransporte_DocumentosTransporte b WITH(NOLOCK) ON (b.FaturaID = a.FaturaID) ' +
	                    'INNER JOIN DocumentosTransporte c WITH(NOLOCK) ON (c.DocumentoTransporteID = b.DocumentoTransporteID) ' +
	                'WHERE a.FaturaID =  ' + idToFind + ' ' +
	                'GROUP BY c.TipoDocumentoTransporteID ' +
	                'ORDER BY c.TipoDocumentoTransporteID';

        return 'dso01Grid_DSC';
    }

    // Reembolso
    else if (folderID == 24356) {
        dso.SQL = 'SELECT a.* ' +
                'FROM FaturasTransporte_Reembolso a WITH(NOLOCK) ' +
                'WHERE a.FaturaID =  ' + idToFind + ' ';
	
        return 'dso01Grid_DSC';
    }



}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    var aEmpresaID = getCurrEmpresaData();
    
    // LOG
    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }

    // Erros
    else if (pastaID == 24352) {
        if (dso.id == 'dso05GridLkp') {
            dso.SQL = 'SELECT a.*, ' +
                                    'b.FreteErroID, ' +
                                    'b.Interno, ' +
                                    'b.Fiscal, ' +
                                    'b.PermiteAprovacao, ' +
                                    'b.MensagemErro, ' +
                                    'dbo.fn_Pessoa_Fantasia(a.AprovadorID, 0) AS Aprovador, ' +
                                    'a.dtAprovacao, ' +
                                    'a.Observacao ' +
	                            'FROM dbo.TransportadoraDados_Detalhes_Erros a WITH(NOLOCK) ' +
	                                'INNER JOIN FreteErros b WITH(NOLOCK) ON (b.FreteErroID = a.CodigoErro) ' +
	                            'WHERE (a.RegistroID = ' + nRegistroID + ') AND (a.TipoRegistroID = 1902) ' +
                                'ORDER BY a.CodigoErro';
            return 'dso05GridLkp';
        }
    }
    //pedidos
    else if (pastaID == 21067) {
        if (dso.id == 'dso06GridLkp') {
            dso.SQL = ' SELECT a.*,  c.RecursoAbreviado AS RecursoAbreviado ' +
                       ' FROM dbo.FaturasTransporte_Pedidos a WITH(NOLOCK)  ' +
                       ' INNER JOIN Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID)  ' +
                       ' INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = b.EstadoID) ' +
                       ' WHERE (a.FaturaID = ' + nRegistroID + ') AND (b.TransacaoID = 222) ';
            return 'dso06GridLkp';
        }
    }
    // Reembolso
    else if (pastaID == 24356) {
        if (dso.id == 'dso07GridLkp') {
            dso.SQL = 'SELECT a.*, ' +
                      'CONVERT(VARCHAR, b.EmpresaID) + \'-\' + dbo.fn_Pessoa_Fantasia(b.EmpresaID, 0) AS Empresa, ' +
	                  'CONVERT(VARCHAR, b.TransacaoID) + \'-\' + c.Operacao AS Operacao, ' +
	                  'dbo.fn_Pessoa_Fantasia(b.PessoaID, 0) AS Pessoa, ' +
	                  'b.ValorTotalPedido, ' +
	                  'd.NotaFiscalID, ' +
                      '(SELECT SUM(Valor) FROM FaturasTransporte_Reembolso z where z.FaturaID = a.FaturaID AND z.Reembolso = 1) AS TotalReembolso ' +
                      'FROM FaturasTransporte_Reembolso a WITH(NOLOCK) ' +
                      'LEFT JOIN Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) ' +
                      'LEFT JOIN Operacoes c WITH(NOLOCK) ON (c.OperacaoID = b.TransacaoID) ' +
                      'LEFT JOIN Notasfiscais d WITH(NOLOCK) ON (d.NotaFiscalID = b.NotaFiscalID) ' +
	                  'WHERE (a.FaturaID = ' + nRegistroID + ') ' +
                      'ORDER BY b.EmpresaID, b.TransacaoID';
            return 'dso07GridLkp';
        }
    }

}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); // Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); // Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); // LOG

    vPastaName = window.top.getPastaName(24011);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24011); // Aprovacoes Especiais

    vPastaName = window.top.getPastaName(24351);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24351); // DocumentoTransporte

    vPastaName = window.top.getPastaName(24352);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24352); // Erros

    vPastaName = window.top.getPastaName(20161);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20161); // Financeiros

    vPastaName = window.top.getPastaName(21067);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 21067); // Pedidos

    vPastaName = window.top.getPastaName(24354);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24354); // Totais

    vPastaName = window.top.getPastaName(24356);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24356); // Reembolso

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
	var currLine = -1;
	
    //@@ return true se tudo ok, caso contrario return false;
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
       if (folderID == 24351) // Pega a Linha em que se deve gravar a altera��o.
           currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [11], ['FatDocumentoTransporteID', registroID]);

       if (folderID == 24352) // Pega a Linha em que se deve gravar a altera��o.
           currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [5], ['TransportadoraErroID', registroID]);

       if (folderID == 20161) // Pega a Linha em que se deve gravar a altera��o.
           currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [5], ['FatFinanceiroID', registroID]);

       if (folderID == 21067) // Pega a Linha em que se deve gravar a altera��o.
           currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['FaturaID', registroID]);

       if (folderID == 24356) // Pega a Linha em que se deve gravar a altera��o.
           currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['FaturaID', registroID]);

       if (currLine < 0)
       {
            if (fg.disabled == false)
            {
				try
                {
                    fg.Editable = true;
					window.focus();
					fg.focus();
				}	
                catch(e)
                {
					;
                }
            }    
        
            return false;
       }    
    }

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }

    // DocumentoTransporte   
    else if (folderID == 24351) {

        fg.FontSize = '8';
        fg.FrozenCols = 3;

        headerGrid(fg, ['ID',
                        'Numero',
                        'E',
                        'Data',
                        'Valor Total',
                        'Total Parcelas',
                        'Diferen�a',
                        'Dif %',
                        'Tipo',
                        'Problema',
                        'Observa��o',
                        'FatDocumentoTransporteID'], [11]);

        fillGridMask(fg, currDSO, ['DocumentoTransporteID*',
                                   'NumeroDocumentoTransporte*',
                                   'Estado*',
                                   'dtEmissaoDocumentoTransporte*',
                                   'ValorTotal*',
                                   'ValorTotalParcelasFrete*',
                                   'ValorDiferenca*',
                                   'PercentualDiferenca*',
                                   'Tipo*',
                                   'Problema*',
                                   'Obs2*',
                                   'FatDocumentoTransporteID'],
                                   ['', '', '', '', '', '', '', '', '', '', '', ''],
                                   ['', '', '', dTFormat, '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###,###.00', '', '', '', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C'],
                                                              [4, '###,###,###,###.00', 'S'],
                                                              [5, '###,###,###,###.00', 'S'],
                                                              [6, '###,###,###,###.00', 'S'],
                                                              [7, '###,###,###,###.00', 'S']], null);
        TotalDocTransporte();

        alignColsInGrid(fg, [0, 3, 4, 5, 6, 7]);

        // Merge de Colunas
        fg.MergeCells = 0;
        fg.MergeCol(-1) = true;
    }

    //Erros
    else if (folderID == 24352) {
        headerGrid(fg, ['Codigo',
                        'Int',
                        'Fis',
                        'PA',
                        'Erro',
                        'Aprovador',
                        'Data Aprova��o',
                        'Observacao',
                        'TransportadoraErroID'], [8]);

        glb_aCelHint = [[0, 1, 'Erro interno ou da transportadora'],
						[0, 2, 'Erro tem origem fiscal'],
						[0, 3, 'Permite Aprova��o Especial']];

        fillGridMask(fg, currDSO, ['^CodigoErro^dso05GridLkp^FreteErroID^FreteErroID*',
                                   '^CodigoErro^dso05GridLkp^FreteErroID^Interno*',
                                   '^CodigoErro^dso05GridLkp^FreteErroID^Fiscal*',
                                   '^CodigoErro^dso05GridLkp^FreteErroID^PermiteAprovacao*',
                                   '^CodigoErro^dso05GridLkp^FreteErroID^MensagemErro*',
                                   '^CodigoErro^dso05GridLkp^FreteErroID^Aprovador*',
                                   'dtAprovacao*',
                                   'Observacao',
                                   'TransportadoraErroID'],
                                    ['', '', '', '', '', '', '', ''],
                                    ['', '', '', '', '', '', '', '']);
    }

    // Financeiros
    else if (folderID == 20161){
        headerGrid(fg, ['Financeiro',
                       'Valor',
                       'Observa��o',
                       'Est',
                       'Pedido',
                       'Duplicata',
                       'Pessoa',
                       'Forma',
                       'Prazo',
                       'Emiss�o',
                       'Vencimento',
                       '$',
                       'Valor',
                       'Saldo',
                       'Saldo Atual',
                       'Tipo',
                       'FatFinanceiroID'], [16]);

        glb_aCelHint = [[0, 3, 'Aplicado? (Baixado no Financeiro)']];

        fillGridMask(fg, dso01Grid, ['FinanceiroID*',
                                   'Valor*',
                                   'Observacao*',
                                   'RecursoAbreviado*',
                                   'PedidoID*',
                                   'Duplicata*',
                                   'Pessoa*',
                                   'ItemAbreviado*',
                                   'PrazoPagamento*',
                                   'dtEmissao*',
                                   'dtVencimento*',
                                   'SimboloMoeda*',
                                   'Valor*',
                                   'SaldoDevedor*',
                                   'SaldoAtualizado*',
                                   'Tipo*',
                                   'FatFinanceiroID'],
									['', '999999999.99', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
									['', '###,###,###.00', '', '', '', '', '', '', '', dTFormat, dTFormat, '', '###,###,###.00', '###,###,###.00', '###,###,###.00', '', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1, '###,###,###,###.00', 'S'],
															  [4, '########', 'C'],
															  [12, '###,###,###,###.00', 'S'],
															  [13, '###,###,###,###.00', 'S'],
															  [14, '###,###,###,###.00', 'S']]);

        // colunas a serem alinhadas a direita
        alignColsInGrid(fg, [0, 1, 4, 8, 12, 13, 14]);

        fg.FrozenCols = 1;
    }

    // Pedidos
    else if (folderID == 21067){ 
        /*headerGrid(fg, ['Data',
                       'Pedido', 
                       'Est',
                       'Nota',
                       'Data Nota',
                       'CFOP',
                       'Moeda',
                       'Total',
                       'Empresa',
                       'Observa��o'], [9]);

        fillGridMask(fg, currDSO, ['dtPedido*',
                                 'PedidoID*',
                                 'RecursoAbreviado*',
                                 'NotaFiscal*',
                                 'dtNotaFiscal*',
                                 'Codigo*',
                                 'SimboloMoeda*',
                                 'TotalPedido*',
                                 'Fantasia*',
                                 'Observacao*'],
                                 ['', '', '', '', '', '', '', '', ''],
                                 [dTFormat, '', '', '', '', '', '###,###,###.00', '', '']);
        */

        headerGrid(fg, ['PedidoID',
                        'Est',
                        'Nota Fiscal',
                        'Chave Acesso',
                        'CNPJ',
                        'Codigo',
                        'Valor Nota',
                        'Observa��o',
                        'FatPedidoID'], [8]);

        fillGridMask(fg, currDSO, ['PedidoID*',
                        '^FatPedidoID^dso06GridLkp^FatPedidoID^RecursoAbreviado*',
                        'NotaFiscal',
                        'ChaveAcesso',
                        'CNPJ',
                        'Codigo',
                        'ValorTotalNota',
                        'Observacao',
                        'FatPedidoID'],
                        ['', '', '', '', '', '', '999999999.99', '', ''],
                        ['', '', '', '', '', '', '###,###,###.00', '', '']);


        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    
    // Totais
    else if (folderID == 24354) { 
        headerGrid(fg, ['Tipo',
                       'Qtd', 
                       'Imposto',
                       'Total Imposto',
                       'Valor',
                       ''], [5]);

        fillGridMask(fg, currDSO, ['Tipo*',
                                 'Qtd*',
                                 'TipoImposto*',
                                 'TotalImposto*',
                                 'ValorTotal*'],
                                 ['', '', '', '', ''],
                                 ['', '', '', '###,###,###.00', '###,###,###.00']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[3, '###,###,###,###.00', 'S'],
															  [4, '###,###,###,###.00', 'S'],]);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }


    // Reembolso
    else if (folderID == 24356)
    {
        var dTFormat = '';
        var i;

        if (DATE_FORMAT == "DD/MM/YYYY")
            dTFormat = 'dd/mm/yyyy';
        else if (DATE_FORMAT == "MM/DD/YYYY")
            dTFormat = 'mm/dd/yyyy';

        fg.Redraw = 0;
        fg.Editable = false;
        startGridInterface(fg);

        fg.FrozenCols = 0;
        

        headerGrid(fg, ['Empresa',
                        'Transa��o',
                        'Pedido',
                        'Pessoa',
                        'Valor Pedido',
                        'NotaFiscal',
                        'Data Nota',
                        'Valor Reembolso',
                        'Reembolso',
                        'OK',
                        'Etiqueta',
                        'TotalReembolso',
                        'FaturaID',
                        'FatReembolsoID'], [11, 12, 13]);

        alignColsInGrid(fg, [4,7]);
        
        
        fillGridMask(fg, currDSO, ['^FatReembolsoID^dso07GridLkp^FatReembolsoID^Empresa*',
                        '^FatReembolsoID^dso07GridLkp^FatReembolsoID^Operacao*',
                        '^FatReembolsoID^dso07GridLkp^FatReembolsoID^PedidoID*',
                        '^FatReembolsoID^dso07GridLkp^FatReembolsoID^Pessoa*',
                        '^FatReembolsoID^dso07GridLkp^FatReembolsoID^ValorTotalPedido*',
                        'NotaFiscal',
                        'dtNotaFiscal',
                        'Valor',
                        'Reembolso',
                        'OK',
                        'Etiqueta',
                        '^FatReembolsoID^dso07GridLkp^FatReembolsoID^TotalReembolso*',
                        'FaturaID',
                        'FatReembolsoID'],
                        ['', '', '', '', '999999999.99', '', '99/99/9999', '999999999.99', '', '', '', '999999999.99', '', ''],
                        ['', '', '', '', '###,###,###.00', '', dTFormat, '###,###,###.00', '', '', '', '###,###,###.00', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
        fg.MergeCol(0) = true;
        fg.MergeCol(1) = true;

        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
        fg.Redraw = 2;
        fg.ExplorerBar = 5;
        fg.Col = getColIndexByColKey(fg, 'NotaFiscal');

        glb_GridIsBuilding = false;
        fg.Redraw = 2;
    }

}

// Apenas quando tem pelo menos 1 registro no grid

function TotalDocTransporte() {

    if (!(fg.Rows > 1))
        return null;

    dsoTotalDocTransporte.SQL = 'SELECT COUNT(b.DocumentoTransporteID) AS Total, SUM(a.ValorTotal) AS ValorTotal, ' +
                                    'SUM(dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID, 1)) AS TotalParcela, ' +
                                    'SUM(dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID, 2)) AS TotalDiferenca ' +
                                'FROM DocumentosTransporte a WITH(NOLOCK) ' +
                                    'INNER JOIN FaturasTransporte_DocumentosTransporte b WITH(NOLOCK) ON (b.DocumentoTransporteID = a.DocumentoTransporteID) ' +
                            'WHERE ' + glb_sFiltr + ' b.FaturaID = ' + glb_idToFind;

    dsoTotalDocTransporte.ondatasetcomplete = TotalDocTransporte_DSC;
    dsoTotalDocTransporte.refresh();
}

function TotalDocTransporte_DSC() {
    var nTotalDoc = dsoTotalDocTransporte.recordset['Total'].value;
    var nValorTotal = dsoTotalDocTransporte.recordset['ValorTotal'].value;
    var nToatlParc = dsoTotalDocTransporte.recordset['TotalParcela'].value;
    var nValorDiferenca = dsoTotalDocTransporte.recordset['TotalDiferenca'].value;
    var nDifPer = ((nValorDiferenca / nValorTotal) * 100);

    fg.TextMatrix(1, 0) = nTotalDoc;
    fg.TextMatrix(1, 4) = nValorTotal;
    fg.TextMatrix(1, 5) = nToatlParc;
    fg.TextMatrix(1, 6) = nValorDiferenca;
    fg.TextMatrix(1, 7) = nDifPer;
}