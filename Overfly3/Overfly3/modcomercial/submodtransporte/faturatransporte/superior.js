/********************************************************************
Faturas de Transporte
superior.js

Library javascript para o superior.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************
var dsoSup01 = new CDatatransport("dsoSup01");
var dsoStateMachine = new CDatatransport("dsoStateMachine");
var dsoCurrData = new CDatatransport("dsoCurrData");
var dsoCmbDynamic03 = new CDatatransport("dsoCmbDynamic03");
var dsoVerificacao = new CDatatransport("dsoVerificacao");

// Cadastrado
var ESTADO_C = 1;
// Validado
var ESTADO_V = 8;
// Deletar
var ESTADO_Z = 5;
// Pendente
var ESTADO_N = 65;
// Liquidado
var ESTADO_L = 32;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
putSpecialAttributesInControls()
prgServerSup(btnClicked)
prgInterfaceSup(btnClicked)
optChangedInCmb(cmb)
btnLupaClicked(btnClicked)
finalOfSupCascade(btnClicked)
btnBarClicked(controlBar, btnClicked)
btnBarNotEspecClicked(controlBar, btnClicked)
modalInformForm(idElement, param1, param2)
supInitEditMode()
formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
stateMachOpened( currEstadoID )
stateMachBtnOK( currEstadoID, newEstadoID )
stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
carrierArrived(idElement, idBrowser, param1, param2)
carrierReturning(idElement, idBrowser, param1, param2)
********************************************************************/

/********************************************************************
// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/
function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);

    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    //glb_aStaticCombos = ([['selUFDesembaraco', '1'], ['selTipoFreteID', '2']]);
    glb_aStaticCombos = null;

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modcomercial/submodtransporte/faturatransporte/inferior.asp',
                              SYS_PAGESURLROOT + '/modcomercial/submodtransporte/faturatransporte/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);

    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'FaturaID';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
     adjustDivs('sup',[[1,'divSup01_01']]);

     //@@ *** Div principal superior divSup01_01 ***/
     adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
                           ['lblEstadoID', 'txtEstadoID', 2, 1],
                           ['lblNumero', 'txtNumero', 10, 1],
                           ['lblSerie', 'txtSerie', 5, 1],
                           ['lbldtEmissao', 'txtdtEmissao', 10, 1],
                           ['lbldtVencimento', 'txtdtVencimento', 10, 1],
                           ['lblIdentificador', 'txtIdentificador', 12, 1],
                           ['lblTipoDocCob', 'txtTipoDocCob', 10, 1],
                           ['lblAutomatico', 'chkAutomatico', 3, 1],
                           ['lblTransportadoraID', 'selTransportadoraID', 30, 2],
                           ['btnFindTransportadora', 'btnFindTransportadora', 24, 2],
                           ['lblFilialEmissFat', 'txtFilialEmissFat', 30, 2],
                           ['lblValTotalFatura', 'txtValTotalFatura', 11, 3],
                           ['lblValTotalICMS', 'txtValTotalICMS', 11, 3],
                           ['lblJurosDiarios', 'txtJurosDiarios', 11, 3],
                           ['lblDesconto', 'txtDesconto', 11, 3, -3],
                           ['lbldtLimiteDesconto', 'txtdtLimiteDesconto', 10, 3],
                           ['lblAgenteCobranca', 'txtAgenteCobranca', 30, 4],
                           ['lblAgencia', 'txtAgencia', 5, 4],
                           ['lblDigitoAgencia', 'txtDigitoAgencia', 2, 4, -3],
                           ['lblContaCorrente', 'txtContaCorrente', 12, 4, -3],
                           ['lblDigitoConta', 'txtDigitoConta', 2, 4, -3],
                           ['lblQuantidadeDocumentoTransporte', 'txtQuantidadeDocumentoTransporte', 17, 5],
                           ['lblQuantidadeDocumentoTransporteEncontrados', 'txtQuantidadeDocumentoTransporteEncontrados', 17, 5, -8],
                           ['lblQuantidadeDocumentoTransporteFaltantes', 'txtQuantidadeDocumentoTransporteFaltantes', 17, 5],
                           ['lblTotalParcelasFrete', 'txtTotalParcelasFrete', 15, 5]
                           ], null, null, true);

    chkAutomatico.disabled = true;
}
 
 // CARRIER **********************************************************
 /********************************************************************
 DISPARAR UM CARRIER:
 Usar a seguinte funcao:

 sendJSCarrier(getHtmlId(),param1, param2);

 Onde:
 getHtmlId() - fixo, remete o id do html que disparou o carrier.
 param1      - qualquer coisa. De uso do programador.
 param2      - qualquer coisa. De uso do programador.
 ********************************************************************/

 /********************************************************************
 Funcao disparada pelo frame work.
 CARRIER CHEGANDO NO ARQUIVO.
 Executa quando um carrier e disparado por outro form.

 Parametros: 
 idElement   - id do html que disparou o carrier
 idBrowser   - id do browser que disparou o carrier
 param1      - parametro um trazido pelo carrier
 param2      - parametro dois trazido pelo carrier

 Retorno:
 Se intercepta sempre retornar o seguinte array:
 array[0] = getHtmlId() ou null
 array[1] = qualquer coisa ou null
 array[2] = qualquer coisa ou null
 Se nao intercepta retornar null
 ********************************************************************/
 function carrierArrived(idElement, idBrowser, param1, param2) 
 {
     if (param1 == 'SHOWFATURATRANSPORTE') 
     {
         // Da automacao, deve constar de todos os ifs do carrierArrived
         if (__currFormState() != 0)
             return null;

         // param2 - traz array:
         // o id da empresa do form que mandou
         // o id do registro a abrir
         // No sendJSMessage abaixo:
         // param2[1] = null significa que nao deve paginar
         // param2[2] = null significa que nao ha array de ids

         var empresa = getCurrEmpresaData();

         if (param2[0] != empresa[0])
             return null;

         window.top.focus();

         // Exclusivo para exibir detalhe em form
         showDetailByCarrier(param2[1]);

         return new Array(getHtmlId(), null, null);
     }
 }

 /********************************************************************
 Funcao disparada pelo frame work.
 CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
 Executa quando um carrier e disparado por este form e apos executar
 nos demais forms abertos, retorna a este form.

 Parametros: 
 idElement   - string id do html que interceptou o carrier
 idBrowser   - string id do browser que contem o form que interceptou
 o carrier
 param1      - parametro um trazido pelo carrier
 param2      - parametro dois trazido pelo carrier

 Retorno:
 Sempre retorna null
 ********************************************************************/
 function carrierReturning(idElement, idBrowser, param1, param2) 
 {
     // Nao mexer - Inicio de automacao ==============================
     return null;
     // Final de Nao mexer - Inicio de automacao =====================
 }
 // FINAL DE CARRIER *************************************************

 /********************************************************************
 Funcao disparada pelo frame work.
 Coloca atributos em campos para serem manobrados graficamente
 (esconde/mostra e habilita desabilita) pelo framework.

 Parametros: 
 nenhum

 Retorno:
 nenhum
 ********************************************************************/
 function putSpecialAttributesInControls() 
 {
     //@@

     // Tabela de atributos especiais:
     // 1. fnShowHide - seta funcao do programador que o framework executa
     //                 ao mostrar/esconder o controle.
     // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
     //                 habiitar/desabilitar o controle e transfere para
     //                 o programador. False ou null devolve para o framework.
 }

 /********************************************************************
 Funcao disparada pelo frame work.
 Esta funcao faz a chamada de todas as operacoes particulares de servidor
 do e preenchimentos de combos do sup, quando o usuario clica
 um botao da barra do sup.
           
 Parametros: 
 ultimo botao clicado na barra de botoes superior

 Retorno:
 nenhum
 ********************************************************************/
 function prgServerSup(btnClicked) 
 {
     var TransportadoraID;
     var sLabelTransportadora;  
     
     // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
     glb_BtnFromFramWork = btnClicked;

     // chama funcao do programador que inicia o carregamento
     if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR') || (btnClicked == 'SUPANT') || (btnClicked == 'SUPSEG')) 
         startDynamicCmbsTransp();
     else
     // volta para a automacao
         finalOfSupCascade(glb_BtnFromFramWork);

     TransportadoraID = dsoSup01.recordset["TransportadoraID"].value;

     if (TransportadoraID == null)
         TransportadoraID = '';
     
     sLabelTransportadora = 'Transportadora ' + TransportadoraID;

     setLabelTransportadora(sLabelTransportadora);
 }

 /********************************************************************
 Funcao disparada pelo frame work.
 Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
 retorno de dados do servidor, feitas pelo programador.
           
 Parametros: 
 nenhum

 Retorno:
 nenhum
 ********************************************************************/
 function finalOfSupCascade(btnClicked) 
 {
     //@@
     // Nao mexer - Inicio de automacao ==============================
     // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
     // para as operacoes de bancos de dados da automacao.
     sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
     // Final de Nao mexer - Inicio de automacao =====================
 }

 /********************************************************************
 Funcao disparada pelo frame work.
 Esta funcao faz a chamada de todas as operacoes particulares
 do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
 OPERACAO DE BANCO DE DADOS.
           
 Parametros: 
 ultimo botao clicado na barra de botoes superior

 Retorno:
 nenhum
 ********************************************************************/
 function prgInterfaceSup(btnClicked) 
 {
     //@@
     // Chama funcoes particulares do form que forem necessarias
     // depois de trocar o div

     if (btnClicked == 'SUPINCL')
         setReadOnlyFields();

     // Nao mexer - Inicio de automacao ==============================
     sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
     // Final de Nao mexer - Inicio de automacao =====================
 }

 /********************************************************************
 Funcao disparada pelo frame work.
 Esta funcao recebe todos os onchange de combos do form e executa as
 funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
 Parametros: 
 nenhum

 Retorno:
 nenhum
 ********************************************************************/
 function optChangedInCmb(cmb) 
 {
    // var cmbID = cmb.id;

     //@@ Os if e os else if abaixo sao particulares de cada form
     // Troca a interface do sup em funcao de item selecionado
     // no combo de tipo.
     // Se for necessario ao programador, repetir o if abaixo
     // definido, antes deste comentario.
     // Nao mexer - Inicio de automacao ==============================
   //  if (cmbID == 'selTipoRegistroID')
        // adjustSupInterface();
     // Final de Nao mexer - Inicio de automacao =====================
 }

 /********************************************************************
 Funcao disparada pelo frame work.
 Dispara logo apos abrir a maquina de estado. 
           
 Parametros: 
 currEstadoID    - EstadoID do registro

 Retorno:
 nenhum
 ********************************************************************/
 function stateMachOpened(currEstadoID) 
 {
     return null;
 }

 /********************************************************************
 Funcao disparada pelo frame work.
 Usuario clicou OK na maquina de estado
           
 Parametros: 
 currEstadoID    - EstadoID do registro

 Retorno:
 false - automacao prossegue
 true  - automacao para        
       
 Nota:
 Se o programador retornar diferente de null nesta funcao,
 o sistema permanecera travado, ao final do seu codigo o programador
 dever� chamar a seguinte funcao da automacao:
 stateMachSupExec(action)
 onde action = 'OK' - o novo estado sera gravado
 'CANC' - a gravacao sera cancelada
 para prosseguir a automacao
 ********************************************************************/
 function stateMachBtnOK(currEstadoID, newEstadoID) 
 {
    if (newEstadoID == 5)
        var _retMsg = window.top.overflyGen.Confirm('Todos os Documentos de Transporte ser�o desvinculados! Deseja deletar?');

    if ((_retMsg == 0) || (_retMsg == 2))
        stateMachSupExec('CANC');
    else {
        verifyFaturasInServer(currEstadoID, newEstadoID);
        return true;
    }
 }
 
 function verifyFaturasInServer(currEstadoID, newEstadoID) {
     var nFaturaID = dsoSup01.recordset['FaturaID'].value;
     var nUserID = getCurrUserID();
     
     var strPars = new String();

     strPars = '?nFaturaID=' + escape(nFaturaID);
     strPars += '&nEstadoDeID=' + escape(currEstadoID);
     strPars += '&nEstadoParaID=' + escape(newEstadoID);
     strPars += '&nUserID=' + escape(nUserID);        
        
     setConnection(dsoVerificacao);

     dsoVerificacao.URL = SYS_ASPURLROOT + '/modcomercial/submodtransporte/faturatransporte/serverside/verificacaoFatura.aspx' + strPars;

     dsoVerificacao.ondatasetcomplete = verifyFaturasInServer_DSC;
     dsoVerificacao.refresh();
 }

 /********************************************************************
 Funcao disparada pelo frame work.
 Dispara se o usuario trocou o estado.
 Dispara logo apos fechar a maquina de estado. 
           
 Parametros: 
 oldEstadoID     - previo EstadoID do registro
 currEstadoID    - novo EstadoID do registro
 bSavedID        - o novo estadoID foi gravado no banco (true/false)

 Retorno:
 nenhum
 ********************************************************************/
 function stateMachClosed(oldEstadoID, currEstadoID, bSavedID) 
 {

 }

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
    if (btnClicked.id == 'btnFindTransportadora')
    {
       openModalTransportadora();
        
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) 
{
    // Documentos
    if (btnClicked == 1) {
        __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
    }
    if (btnClicked == 2)
        openModalPrint();
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
- Avancar, retroagir, incluir, alterar, estado, 
excluir, OK, cancelar refresh, anterior e proximo. 
(Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) 
{
    /*
    var nEstadoID = getCurrEstadoID();
    var sResultado;
*/
    var nEmpresaID = getCurrEmpresaData();

    if (btnClicked == 'SUPOK') {
        // Se e um novo registro
        if (dsoSup01.recordset[glb_sFldIDName] == null || dsoSup01.recordset[glb_sFldIDName].value == null)
            dsoSup01.recordset['EmpresaID'].value = nEmpresaID[0];

        //Valida��es
        if (!((txtdtEmissao.value == null) || (txtdtEmissao.value == ''))) {

            var dtHoje = getCurrDate();
            var dtEmissao = (txtdtEmissao.value); //Data de emiss�o digitada pelo usuario
            var dtHojeOK = parseInt(ComparisonDate(dtHoje, null)); //dtHoje formatada como YYYYMMDD para compara��o
            var dtEmissaoOK = parseInt(ComparisonDate(dtEmissao, null)); //dtEmissao formatada como YYYYMMDD para compara��o

            if (dtEmissaoOK > dtHojeOK) {
                sResultado = 'Data inv�lida';
                window.top.overflyGen.Alert(sResultado);
                return false;
            }
        }

        /*if ((txtSerie.value == null) || (txtSerie.value == '')) 
            sResultado += (sResultado == '' ? '' : '\n') + 'O Campo Serie n�o foi preenchido.';
        
        if ((selTransportadoraID.value == null) || (selTransportadoraID.value == '')) 
            sResultado += (sResultado == '' ? '' : '\n') + 'O Campo Transportadora n�o foi preenchido.';
        
        if ((txtNumero.value == null) || (txtNumero.value == '')) 
            sResultado += (sResultado == '' ? '' : '\n') + 'O Campo N�mero n�o foi preenchido.';
        
        if ((txtdtVencimento.value == null) || (txtdtVencimento.value == '')) 
            sResultado += (sResultado == '' ? '' : '\n') + 'O Campo Data Vencimento n�o foi preenchido.';
        
        if ((txtValTotalFatura.value == null) || (txtValTotalFatura.value == '')) 
            sResultado += (sResultado == '' ? '' : '\n') + 'O Campo Valor Total da Fatura n�o foi preenchido.';
        
        if ((txtValTotalICMS.value == null) || (txtValTotalICMS.value == ''))
            sResultado += (sResultado == '' ? '' : '\n') + 'O Campo Valor Total ICMS n�o foi preenchido.';
            
        if (sResultado != '')
            window.top.overflyGen.Alert(sResultado);*/

    }

    else if (btnClicked == 'SUPINCL') {
        clearComboEx(['selTransportadoraID']);
     }
        
    // Para prosseguir a automacao retornar null
    return null;                        
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) 
{
    // Modal Transportadora
    if (idElement.toUpperCase() == 'MODALTRANSPORTADORAHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            fillFieldsByRelationModal(param2);

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de impressao
    else if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode() 
{

    setReadOnlyFields();
    
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

function setReadOnlyFields() 
{
    var identificador;
    var currEstadoID;
    
    identificador = dsoSup01.recordset["Identificador"].value;
    currEstadoID = dsoSup01.recordset["EstadoID"].value;
    
    // Campos que sempre s�o travados.
    txtIdentificador.readOnly = true;
    txtEstadoID.readOnly = true;
    txtRegistroID.readOnly = true;
    selTransportadoraID.readOnly = true;
    chkAutomatico.readOnly = true;
       
    // estado do registro EDI
    if (identificador != null) 
    {    
        txtSerie.disabled = true;
        txtdtEmissao.disabled = true;
        selTransportadoraID.disabled = true;
        txtNumero.disabled = true;
        txtdtVencimento.disabled = true;
        txtValTotalFatura.disabled = true;
        txtJurosDiarios.disabled = true;
        txtDesconto.disabled = true;
        txtdtLimiteDesconto.disabled = true;
        txtValTotalICMS.disabled = true;
        txtTipoDocCob.disabled = true; 
        txtFilialEmissFat.disabled = true; 
        txtAgenteCobranca.disabled = true;
        txtAgencia.disabled = true;
        txtDigitoAgencia.disabled = true;
        txtContaCorrente.disabled = true;
        txtDigitoConta.disabled = true;
          
    }
    // Inclusao do registro for C (Manual)
    else if (((currEstadoID == ESTADO_C) || (currEstadoID == null)) && (identificador == null)) 
    {    
        txtSerie.disabled = false;
        txtdtEmissao.disabled = false;
        selTransportadoraID.disabled = true;
        txtNumero.disabled = false;
        txtdtVencimento.disabled = false;
        txtValTotalFatura.disabled = false;
        txtJurosDiarios.disabled = false;
        txtDesconto.disabled = false;
        txtdtLimiteDesconto.disabled = false;
        txtValTotalICMS.disabled = false;
        txtTipoDocCob.disabled = false;
        txtFilialEmissFat.disabled = false;
        txtAgenteCobranca.disabled = false;
        txtAgencia.disabled = false;
        txtDigitoAgencia.disabled = false;
        txtContaCorrente.disabled = false;
        txtDigitoConta.disabled = false;
    }
    
    // estado do registro for C
    if (currEstadoID == ESTADO_C && (dsoSup01.recordset["Automatico"].value == 1)) {
        txtSerie.disabled = true;
        txtdtEmissao.disabled = true;
        selTransportadoraID.disabled = true;
        txtNumero.disabled = true;
        txtdtVencimento.disabled = true;
        txtValTotalFatura.disabled = true;
        txtJurosDiarios.disabled = true;
        txtDesconto.disabled = true;
        txtdtLimiteDesconto.disabled = true;
        txtValTotalICMS.disabled = true;
        txtTipoDocCob.disabled = true;
        txtFilialEmissFat.disabled = true;
        txtAgenteCobranca.disabled = true;
        txtAgencia.disabled = true;
        txtDigitoAgencia.disabled = true;
        txtContaCorrente.disabled = true;
        txtDigitoConta.disabled = true;
        lockBtnLupa(btnFindTransportadora, true);
    }
    
    // estado do registro for V (Manual)
    else if ((currEstadoID == ESTADO_V) && (identificador == null)) {
        txtSerie.disabled = true;
        txtdtEmissao.disabled = true;
        selTransportadoraID.disabled = true;
        txtNumero.disabled = true;
        txtdtVencimento.disabled = true;
        txtValTotalFatura.disabled = true;
        txtJurosDiarios.disabled = true;
        txtDesconto.disabled = true;
        txtdtLimiteDesconto.disabled = true;
        txtValTotalICMS.disabled = true;
        txtTipoDocCob.disabled = true;
        txtFilialEmissFat.disabled = true;
        txtAgenteCobranca.disabled = true;
        txtAgencia.disabled = true;
        txtDigitoAgencia.disabled = true;
        txtContaCorrente.disabled = true;
        txtDigitoConta.disabled = true;
        lockBtnLupa(btnFindTransportadora, false);
    }
    // estado do registro for L (Manual)
    else if ((currEstadoID == ESTADO_L) && (identificador == null)) {
        txtSerie.disabled = true;
        txtdtEmissao.disabled = true;
        selTransportadoraID.disabled = true;
        txtNumero.disabled = true;
        txtdtVencimento.disabled = true;
        txtValTotalFatura.disabled = true;
        txtJurosDiarios.disabled = true;
        txtDesconto.disabled = true;
        txtdtLimiteDesconto.disabled = true;
        txtValTotalICMS.disabled = true;
        txtTipoDocCob.disabled = true;
        txtFilialEmissFat.disabled = true;
        txtAgenteCobranca.disabled = true;
        txtAgencia.disabled = true;
        txtDigitoAgencia.disabled = true;
        txtContaCorrente.disabled = true;
        txtDigitoConta.disabled = true;
    }
    // estado do registro for Z (Manual)
    else if ((currEstadoID == ESTADO_Z) && (identificador == null)) {
        txtSerie.disabled = true;
        txtdtEmissao.disabled = true;
        selTransportadoraID.disabled = true;
        txtNumero.disabled = true;
        txtdtVencimento.disabled = true;
        txtValTotalFatura.disabled = true;
        txtJurosDiarios.disabled = true;
        txtDesconto.disabled = true;
        txtdtLimiteDesconto.disabled = true;
        txtValTotalICMS.disabled = true;
        txtTipoDocCob.disabled = true;
        txtFilialEmissFat.disabled = true;
        txtAgenteCobranca.disabled = true;
        txtAgencia.disabled = true;
        txtDigitoAgencia.disabled = true;
        txtContaCorrente.disabled = true;
        txtDigitoConta.disabled = true;
    }
    // estado do registro for N (Manual)
    else if ((currEstadoID == ESTADO_N) && (identificador == null)) {
        txtSerie.disabled = true;
        txtdtEmissao.disabled = true;
        selTransportadoraID.disabled = true;
        txtNumero.disabled = true;
        txtdtVencimento.disabled = true;
        txtValTotalFatura.disabled = true;
        txtJurosDiarios.disabled = true;
        txtDesconto.disabled = true;
        txtdtLimiteDesconto.disabled = true;
        txtValTotalICMS.disabled = true;
        txtTipoDocCob.disabled = true;
        txtFilialEmissFat.disabled = true;
        txtAgenteCobranca.disabled = true;
        txtAgencia.disabled = true;
        txtDigitoAgencia.disabled = true;
        txtContaCorrente.disabled = true;
        txtDigitoConta.disabled = true;
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad() 
{
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    showBtnsEspecControlBar('sup', true, [1, 11]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios']);
    //setupEspecBtnsControlBar('inf', 'HH');
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Transportadora 

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalTransportadora() 
{
    var htmlPath;
    var strPars = "";
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa);
    strPars += '&sCaller=' + escape('S');

    htmlPath = SYS_PAGESURLROOT + '/modalgen/modaltransportadora.asp' + strPars;
    showModalWin(htmlPath, new Array(671, 284));
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbsTransp() 
{
    // parametrizacao do dso dsoCmbDynamic03 (designado para selTransportadoraID)
    setConnection(dsoCmbDynamic03);

    dsoCmbDynamic03.SQL = "SELECT PessoaID AS fldID, Fantasia AS fldName " +
		"FROM Pessoas WITH (NOLOCK) " +
		"WHERE PessoaID = " + dsoSup01.recordset["TransportadoraID"].value;

    dsoCmbDynamic03.ondatasetcomplete = dsoCmbDynamic3_DSC;
    dsoCmbDynamic03.refresh();
}

/**********************************************************************
 Fun��o do Programador
 
 Preenche Combo Transportadora automaticamente,
 se registro possuir transportadora.
 **********************************************************************/
function dsoCmbDynamic3_DSC() 
{
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selTransportadoraID];
    var aDSOsDunamics = [dsoCmbDynamic03];

    // Inicia o carregamento de combos dinamicos (selTransportadoraID)
    clearComboEx(['selTransportadoraID']);
    
    for (i=0; i<aCmbsDynamics.length; i++)
    {
        while (! aDSOsDunamics[i].recordset.EOF )
        {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
        	optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }
    }
    
    // volta para a automacao
    finalOfSupCascade(glb_BtnFromFramWork);
	
	return null;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblPessoa, de acordo com o Tipo da Pessoa Selecionada

Parametro:
sLabelTransportadora   Tipo Pessoa

Retorno:
nenhum
********************************************************************/
function setLabelTransportadora(sLabelTransportadora) 
{
    if (sLabelTransportadora == null)
        sLabelTransportadora = '';

    lblTransportadoraID.style.width = sLabelTransportadora.length * FONT_WIDTH;
    lblTransportadoraID.innerText = sLabelTransportadora;
}

/********************************************************************
Retorno do servidor - Verificacoes do DocumentoTransporte
********************************************************************/
function verifyFaturasInServer_DSC() {
    var sResultado = dsoVerificacao.recordset['Resultado'].value;

    if ((sResultado != null) && (sResultado != '')) {
        stateMachSupExec('CANC');
        window.top.overflyGen.Alert(sResultado);
    }
    else
        stateMachSupExec('OK');
}   

/********************************************************************
Funcao criada pelo programador.
Preenche os campos do subform corrente, com os dados vindos da janela
modal Transportadora.

Parametro:
aData   -   array de dados
            aData[0] - quem � na relacao ('SUJ' ou 'OBJ')
            aData[1] - ID do tipo da relacao
            aData[2] - ID do campo do subform corrente
            aData[3] - ID selecionado no grid da modal
            aData[4] - texto correspondente ao ID selecionado no grid da modal
            
            os demais elementos sao opcionais

Retorno:
nenhum
********************************************************************/
function fillFieldsByRelationModal(aData)
{
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var elem;
    var addSexOrServ = null;

    clearComboEx(['selTransportadoraID']);

    oOption = document.createElement("OPTION");
    oOption.text = aData[2];
    oOption.value = aData[0];
    selTransportadoraID.add(oOption);
    selTransportadoraID.selectedIndex = 0;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() 
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars += '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodtransporte/faturatransporte/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(346, 230));
}

function getCurrDate() {
    var d, s = "";
    d = new Date();

    if (DATE_SQL_PARAM == 103) {
        s += padL(d.getDate().toString(), 2, '0') + "/";
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
    }
    else {
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
        s += padL(d.getDate().toString(), 2, '0') + "/";
    }

    s += d.getYear();

    return (s);
}

/********************************************************************
Fun��o para compara��o de data
EX: Par�metro 01/23/2012
Retorno 20120123
    
for�ar formato => 1 = ingles
********************************************************************/
function ComparisonDate(par, formato) {
    //formato 0 = portugues; 1 = ingles
    var sfinalDate = "";
    var aParts = new Array();

    try {
        aParts = par.split('/');
        if (aParts[0].length == par.length)
            aParts = par.split('-');

        if (aParts[0].length == 1)
            aParts[0] = "0" + aParts[0];  //Dia      

        if (aParts[1].length == 1)
            aParts[1] = "0" + aParts[1];  //M�s

        if (aParts[2].length == 2)
            aParts[2] = "20" + aParts[2]; //Ano

        //Se o S.O. estiver em Portugu�s
        if (DATE_SQL_PARAM == 103)
            sfinalDate = aParts[2].toString() + aParts[1].toString() + aParts[0].toString();
        //Se o S.O. estiver em outro idioma exceto Portugu�s
        else
            sfinalDate = aParts[2].toString() + aParts[0].toString() + aParts[1].toString();

        //For�a o formato a ser ingles, sobrescrevendo a data gerada no trecho anterior
        if (formato == 1)
            sfinalDate = aParts[2].toString() + aParts[0].toString() + aParts[1].toString();
    }
    catch (e) {
        sfinalDate = '0';
    }

    return (sfinalDate);
}