﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodtransporte.faturatransporte.serverside
{
    public partial class gerarPedido : System.Web.UI.OverflyPage
    {
        private Integer faturaID;
        protected Integer nFaturaID
        {
            get { return faturaID; }
            set { faturaID = value; }
        }

        private Integer userID;
        protected Integer nUserID
        {
            get { return userID; }
            set { userID = value; }
        }


        protected string GeracaoPedido()
        {
            ProcedureParameters[] param = new ProcedureParameters[]
			{
				new ProcedureParameters("@FaturaID", SqlDbType.Int, nFaturaID.intValue()),
                new ProcedureParameters("@UsuarioID", SqlDbType.Int, nUserID.intValue()),
				new ProcedureParameters("@MensagemErro",SqlDbType.VarChar,DBNull.Value, ParameterDirection.Output,8000)                
            };

            DataInterfaceObj.execNonQueryProcedure("sp_FaturasTransporte_PedidosGerar", param);

            return param[2].Data != DBNull.Value ? (string)param[2].Data : null;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string resultado = GeracaoPedido();

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " +
                    (resultado != null ? ("'" + resultado + "'") : ("NULL")) +
                " as Resultado"
            ));
        }
    }
}
