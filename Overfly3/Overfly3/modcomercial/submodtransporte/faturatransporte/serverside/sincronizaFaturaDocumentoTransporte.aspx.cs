﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodtransporte.faturatransporte.serverside
{
    public partial class sincronizaFaturaDocumentoTransporte : System.Web.UI.OverflyPage
    {
        private Integer faturaID;
        protected Integer nFaturaID
        {
            get { return faturaID; }
            set { faturaID = value; }
        }

        private Integer transportadoraArquivoDetalheID;
        protected Integer nTransportadoraArquivoDetalheID
        {
            get { return transportadoraArquivoDetalheID; }
            set { transportadoraArquivoDetalheID = value; }
        }


        protected string FaturaDocumentoTransporteSincroniza()
        {
            ProcedureParameters[] param = new ProcedureParameters[]
			{
				new ProcedureParameters("@FaturaID", SqlDbType.Int, nFaturaID.intValue()),
                new ProcedureParameters("@TransportadoraArquivoDetalheID", SqlDbType.Int, nTransportadoraArquivoDetalheID.intValue()),
                new ProcedureParameters("@TipoProcessamento", SqlDbType.Int, 2),
				new ProcedureParameters("@MensagemErro",SqlDbType.VarChar,DBNull.Value, ParameterDirection.Output,8000)                
            };

            DataInterfaceObj.execNonQueryProcedure("sp_FaturaDocumentoTransporte_Gerar", param);

            return param[3].Data != DBNull.Value ? (string)param[3].Data : null;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string resultado = FaturaDocumentoTransporteSincroniza();

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " +
                    (resultado != null ? ("'" + resultado + "'") : ("NULL")) +
                " as Resultado"
            ));
        }
    }
}
