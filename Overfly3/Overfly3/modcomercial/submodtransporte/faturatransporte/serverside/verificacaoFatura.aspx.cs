﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodtransporte.faturatransporte.serverside
{
    public partial class verificacaoFatura : System.Web.UI.OverflyPage
    {
        private Integer FaturaID;
        protected Integer nFaturaID
        {
            get { return FaturaID; }
            set { FaturaID = value; }
        }

        private Integer estadoDeID;
        protected Integer nEstadoDeID
        {
            get { return estadoDeID; }
            set { estadoDeID = value; }
        }

        private Integer estadoParaID;
        protected Integer nEstadoParaID
        {
            get { return estadoParaID; }
            set { estadoParaID = value; }
        }

        private Integer UserID;
        protected Integer nUserID
        {
            get { return UserID; }
            set { UserID = value; }
        }

        protected string FaturaVerifica()
        {
            ProcedureParameters[] param = new ProcedureParameters[]
			{
				new ProcedureParameters("@FaturaID", SqlDbType.Int, nFaturaID.intValue()),
				new ProcedureParameters("@EstadoDeID", SqlDbType.Int, nEstadoDeID.intValue()),
				new ProcedureParameters("@EstadoParaID", SqlDbType.Int, nEstadoParaID.intValue()),
                new ProcedureParameters("@UsuarioID", SqlDbType.Int, nUserID.intValue()),
				new ProcedureParameters("@MensagemErro",SqlDbType.VarChar,DBNull.Value, ParameterDirection.Output,8000),
                new ProcedureParameters("@CodigoErro",SqlDbType.Int,DBNull.Value, ParameterDirection.Output)
            };

            DataInterfaceObj.execNonQueryProcedure("sp_FaturaTransporte_Verifica", param);

            return param[4].Data != DBNull.Value ? (string)param[4].Data : null;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string resultado = FaturaVerifica();

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " +
                    (resultado != null ? ("'" + resultado + "'") : ("NULL")) +
                " as Resultado"
            ));
        }
    }
}
