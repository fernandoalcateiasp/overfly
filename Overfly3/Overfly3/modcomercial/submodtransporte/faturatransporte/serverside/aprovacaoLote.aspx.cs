﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodtransporte.faturatransporte.serverside
{
    public partial class aprovacaoLote : System.Web.UI.OverflyPage
    {
        //private static string emptyStr = "";
        private static Integer Zero = new Integer(0);

        private Integer[] TransportadoraErroID;
        public Integer[] nTransportadoraErroID
        {
            set{ TransportadoraErroID = value; }
        }

        private Integer UserID;
        protected Integer nUserID
        {
            get { return UserID; }
            set { UserID = value;}
        }

        protected int aprovaDocumentos(int i)
        {
            ProcedureParameters[] param = new ProcedureParameters[]
			{
				new ProcedureParameters("@TransportadoraErroID", SqlDbType.Int, TransportadoraErroID[i]),
                new ProcedureParameters("@UsuarioID", SqlDbType.Int, nUserID.intValue()),
                new ProcedureParameters("@AvançadoOK",SqlDbType.Int,DBNull.Value, ParameterDirection.Output)
            };

            DataInterfaceObj.execNonQueryProcedure("sp_DocumentoTransporteFatura_AprovacaoLote", param);

            return param[2].Data != DBNull.Value ? (int)param[2].Data : 0;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            int resultado = 0;

            for (int i = 0; i < TransportadoraErroID.Length; i++)
            {
                int validouDoc = aprovaDocumentos(i);
                resultado += validouDoc;
            }

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " + resultado + " as Resultado"
            ));
        }
    }
}
