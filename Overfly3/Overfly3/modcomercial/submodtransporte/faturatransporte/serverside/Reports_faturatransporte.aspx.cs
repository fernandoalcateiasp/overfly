﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_faturatransporte : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int mEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        private string mEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);
        private int formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string nTipoData = Convert.ToString(HttpContext.Current.Request.Params["nTipoData"]);
        private string ndtInicio = Convert.ToString(HttpContext.Current.Request.Params["ndtInicio"]);
        private string ndtFim = Convert.ToString(HttpContext.Current.Request.Params["ndtFim"]);
        private string nEstadoID = Convert.ToString(HttpContext.Current.Request.Params["nEstadoID"]);
        private string TransportadoraIn = Convert.ToString(HttpContext.Current.Request.Params["TransportadoraIn"]);
        int nFaturaID = Convert.ToInt32(HttpContext.Current.Request.Params["nFaturaID"]);
        int nNumero = Convert.ToInt32(HttpContext.Current.Request.Params["nNumero"]);

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string nomeRel = Request.Params["RelatorioID"].ToString();

                switch (nomeRel)
                {
                    // Diferencial Parcelas Frete 
                    case "40214":
                        RelatorioFaturaTransporteDiferencial();
                        break;

                    // Documentos Transporte 
                    case "40211":
                        RelatorioDocumentoTransporte();
                        break;

                    // Documentos Transporte Faltantes
                    case "40212":
                        RelatorioDocumentoTransporteFaltantes();
                        break;
                }
            }
        }

        public void RelatorioFaturaTransporteDiferencial()
        {
            // Excel
            int Formato = 2;
            string Title = "Diferencial parcelas frete";

            string strSQL = "SELECT a.FaturaID, b.Fantasia, Numero, dtEmissao, dtVencimento, ValorTotal, " +
                                "(SELECT ISNULL(SUM(VlrTotParc), 0) " +
                                "FROM FaturasTransporte_DocumentosTransporte aa WITH(NOLOCK) " +
                                    "INNER JOIN DocumentosTransporte_NotasFiscais bb WITH(NOLOCK) ON (bb.DocumentoTransporteID = aa.DocumentoTransporteID) " +
                                    "INNER JOIN Pedidos cc WITH(NOLOCK) ON (cc.NotaFiscalID = bb.NotaFiscalID) " +
                                    "INNER JOIN vw_PedidoTotais_Parcelas dd WITH(NOLOCK,NOEXPAND) ON (dd.PedidoID = cc.PedidoID) " +
                                    "INNER JOIN DocumentosTransporte ee WITH(NOLOCK) ON (ee.DocumentoTransporteID = bb.DocumentoTransporteID) " +
                                "WHERE (aa.FaturaID = a.FaturaID) AND dd.HistoricoPadraoID = 3173 AND ee.TipoTransporteID = 901) AS TotalParcelasFrete, " +
                                    "(SELECT (SELECT ISNULL(SUM(VlrTotParc), 0) " +
                                    "FROM FaturasTransporte_DocumentosTransporte aa WITH(NOLOCK) " +
                                        "INNER JOIN DocumentosTransporte_NotasFiscais bb WITH(NOLOCK) ON (bb.DocumentoTransporteID = aa.DocumentoTransporteID) " +
                                        "INNER JOIN Pedidos cc WITH(NOLOCK) ON (cc.NotaFiscalID = bb.NotaFiscalID) " +
                                        "INNER JOIN vw_PedidoTotais_Parcelas dd WITH(NOLOCK,NOEXPAND) ON (dd.PedidoID = cc.PedidoID) " +
                                        "INNER JOIN DocumentosTransporte ee WITH(NOLOCK) ON (ee.DocumentoTransporteID = bb.DocumentoTransporteID) " +
                                    "WHERE (aa.FaturaID = a.FaturaID) AND dd.HistoricoPadraoID = 3173 AND ee.TipoTransporteID = 901) - (a.ValorTotal)) AS Diferença, " +
                                    "(CASE (CASE ISNULL(dbo.fn_Log_Usuario(5420,24350, a.FaturaID, 28, NULL, NULL, 0), 999) WHEN 999 THEN 1 ELSE 0 END) WHEN 1 THEN \' Sim \' ELSE \' Não \' END) AS Automatico, " +
                                    "TransportadoraID " +
                            "FROM FaturasTransporte a WITH(NOLOCK) " +
                                "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.TransportadoraID = b.PessoaID) " +
                            "WHERE ((CASE " + nTipoData + " WHEN 1 THEN a.dtEmissao ELSE a.dtVencimento END)  BETWEEN \'" + ndtInicio + "\' AND \'" + ndtFim + "\') " +
                                "AND a.EmpresaID = " + mEmpresaID;

            if(nEstadoID != "")
            {
                strSQL += " AND a.EstadoID IN (" + nEstadoID + ") ";
            }

            if(TransportadoraIn != "()")
            {
                strSQL += " AND TransportadoraID IN " + TransportadoraIn;
            }

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "10.13834", "0.0", "11", "#0113a0", "B", Header_Body, "6.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "16.795", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }


        public void RelatorioDocumentoTransporte()
        {
            bool nulo = false;

            string Title = "Documentos Transporte";

            string SQLDetail1 = " SELECT " +
                                    " COUNT(1) AS Quantidade " +
                                " FROM " +
                                    " FaturasTransporte_DocumentosTransporte a WITH(NOLOCK) " +
                                " WHERE " +
                                    " a.FaturaID = " + nFaturaID;

            string SQLDetail2 = " SELECT " +
                                    " a.DocumentoTransporteID, b.Numero, c.RecursoAbreviado AS Estado, b.ValorTotal, " +
                                    " dbo.fn_DocumentoTransporte_Totais(b.DocumentoTransporteID, 1) AS ValorTotalParcelasFrete, " +
                                    " dbo.fn_DocumentoTransporte_Totais(b.DocumentoTransporteID, 2) AS ValorDiferenca, " +
                                    " dbo.fn_DocumentoTransporte_Totais(b.DocumentoTransporteID, 3) AS PercentualDiferenca, ISNULL(i.MensagemErro,'') AS Erro, e.ItemMasculino AS TipoTransporte, " +
                                    " ISNULL(f.ItemAbreviado,'') AS CondicaoFrete, ISNULL(CAST(d.CodigoErro AS VARCHAR),'') AS Cd, h.NotaFiscal AS NF, j.ItemAbreviado AS Documento " +
                                " FROM " +
                                    " FaturasTransporte_DocumentosTransporte a WITH(NOLOCK) " +
                                        " INNER JOIN DocumentosTransporte b WITH(NOLOCK) ON (b.DocumentoTransporteID = a.DocumentoTransporteID) " +
                                        " INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = b.EstadoID) " +
                                        " LEFT OUTER JOIN TransportadoraDados_Detalhes_Erros d WITH(NOLOCK)ON (d.RegistroID = b.DocumentoTransporteID) AND (d.TipoRegistroID = 1901) " +
                                        " INNER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (e.ItemID = b.TipoTransporteID) " +
                                        " INNER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (f.ItemID = b.CondicaoFreteID) " +
                                        " INNER JOIN DocumentosTransporte_NotasFiscais g WITH(NOLOCK) ON (g.DocumentoTransporteID = b.DocumentoTransporteID) " +
                                        " INNER JOIN NotasFiscais h WITH(NOLOCK) ON (h.NotaFiscalID = g.NotaFiscalID) " +
                                        " LEFT OUTER JOIN FreteErros i WITH(NOLOCK) ON (i.FreteErroID = d.CodigoErro) " +
                                        " INNER JOIN TiposAuxiliares_Itens j WITH(NOLOCK) ON (j.ItemID = b.TipoDocumentoTransporteID) " +
                                " WHERE " +
                                    " a.FaturaID = " + nFaturaID + " " +
                                " ORDER BY DocumentoTransporteID ";

            DataSet Ds = new DataSet();

            var Header_Body = (formato == 1 ? "Header" : "Body");

            string[,] arrQuerys;

            arrQuerys = new string[,] { { "SQLDetail1", SQLDetail1 },
                                        { "SQLDetail2", SQLDetail2 }};

            if (formato == 1)
            {
                ;
            }

            else if (formato == 2) // Se Excel
            {

                double headerX = 0;
                double headerY = 0;

                double tabelaX = 0;
                double tabelaY = 0.8;

                Relatorio.CriarRelatório(Title, arrQuerys, "BRA", "Portrait");

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                if (dsFin.Tables["SQLDetail2"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");

                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    string quantidade = dsFin.Tables["SQLDetail1"].Rows[0]["Quantidade"].ToString();

                    //Header
                    Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, headerX.ToString(), headerY.ToString(), "10", "Black", "B", Header_Body, "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 7.7).ToString(), headerY.ToString(), "10", "Blue", "B", Header_Body, "5", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 14).ToString(), headerY.ToString(), "10", "Black", "B", Header_Body, "5.5", "Horas");

                    Relatorio.CriarObjLabel("Fixo", "", "Fatura: " + nNumero, headerX.ToString(), (headerY + 0.4).ToString(), "10", "Black", "B", "Body", "7.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Quantidade de Documento de Transporte na Fatura: " + quantidade, (headerX + 7.7).ToString(), (headerY + 0.4).ToString(), "10", "Black", "B", "Body", "12", "");


                    //Tabela
                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "SQLDetail2", false);

                    Relatorio.CriarObjColunaNaTabela("DocumentoTransporteID", "DocumentoTransporteID", false, "6", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "DocumentoTransporteID", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Numero", "Numero", false, "3.1", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Numero", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Estado", "Estado", false, "2.25", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Estado", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("ValorTotal", "ValorTotal", false, "4", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("ValorTotalParcelasFrete", "ValorTotalParcelasFrete", false, "5.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorTotalParcelasFrete", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("ValorDiferenca", "ValorDiferenca", false, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorDiferenca", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("PercentualDiferenca", "PercentualDiferenca", false, "5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "PercentualDiferenca", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Erro", "Erro", false, "8", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Erro", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("TipoTransporte", "TipoTransporte", false, "4.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "TipoTransporte", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("CondicaoFrete", "CondicaoFrete", false, "4.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "CondicaoFrete", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Cd", "Cd", false, "2", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Cd", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("NF", "NF", false, "2.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "NF", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Documento", "Documento", false, "3.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Documento", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.TabelaEnd();
                }

                Relatorio.CriarPDF_Excel(Title, formato);
            }
        }

        public void RelatorioDocumentoTransporteFaltantes()
        {

            int nQtdFaltantes = Convert.ToInt32(HttpContext.Current.Request.Params["nQtdFaltantes"]);

            bool nulo = false;

            string Title = "Documentos Transporte Faltantes";

            string SQLDetail1 = "EXEC sp_FaturaDocumentoTransporte_Faltante " + nFaturaID;

            DataSet Ds = new DataSet();

            var Header_Body = (formato == 1 ? "Header" : "Body");

            string[,] arrQuerys;

            arrQuerys = new string[,] { { "SQLDetail1", SQLDetail1 }};

            if (formato == 1)
            {
                ;
            }

            else if (formato == 2) // Se Excel
            {

                double headerX = 0;
                double headerY = 0;

                double tabelaX = 0;
                double tabelaY = 0.8;

                Relatorio.CriarRelatório(Title, arrQuerys, "BRA", "Portrait");

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                if (dsFin.Tables["SQLDetail1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");

                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    //Header
                    Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, headerX.ToString(), headerY.ToString(), "10", "Black", "B", Header_Body, "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 6.7).ToString(), headerY.ToString(), "10", "Blue", "B", Header_Body, "7", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 14).ToString(), headerY.ToString(), "10", "Black", "B", Header_Body, "5.5", "Horas");

                    Relatorio.CriarObjLabel("Fixo", "", "Fatura: " + nNumero, headerX.ToString(), (headerY + 0.4).ToString(), "10", "Black", "B", "Body", "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Quantidade de Documento de Transporte na Fatura: " + nQtdFaltantes, (headerX + 6.7).ToString(), (headerY + 0.4).ToString(), "10", "Black", "B", "Body", "12", "");

                    //Tabela
                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "SQLDetail1", false);

                    Relatorio.CriarObjColunaNaTabela("NumeroDocumentoTransporte", "NumeroDocumentoTransporte", false, "6", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "NumeroDocumentoTransporte", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("ValorFrete", "ValorFrete", false, "4.1", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "ValorFrete", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("dtDocumentoTransporte", "dtDocumentoTransporte", false, "6", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtDocumentoTransporte", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("CGCRemetenteNF", "CGCRemetenteNF", false, "4", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "CGCRemetenteNF", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("CGCDestinatarioNF", "CGCDestinatarioNF", false, "4", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "CGCDestinatarioNF", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Serie", "Serie", false, "1.25", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Serie", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("Nf", "Nf", false, "2.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Nf", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("CGCEmissorDocTransp", "CGCEmissorDocTransp", false, "5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "CGCEmissorDocTransp", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("CGCEmissorFatura", "CGCEmissorFatura", false, "4.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "CGCEmissorFatura", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.CriarObjColunaNaTabela("CGCDivergente", "CGCDivergente", false, "4.5", "Default", "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "CGCDivergente", "DetailGroup", "Null", "Null", 0, false, "10");

                    Relatorio.TabelaEnd();
                }

                Relatorio.CriarPDF_Excel(Title, formato);
            }
        }
    }
}