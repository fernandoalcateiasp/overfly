/********************************************************************
aprovacaofatura.js

Library javascript para o aprovacaofatura.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport("dsoPesq");
var dsoAprovacao = new CDatatransport("dsoAprovacao");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() 
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    var elem = document.getElementById('aprovacaofaturaHtmlBody');

    with (elem) 
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }


    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no campo apropriado
    /*    if (document.getElementById('txtDocumentoTransporte').disabled == false)
    txtDocumentoTransporte.focus();*/
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() 
{
    // texto da secao01
    secText('Aprovar Fatura de Transporte', 1);

    // ajusta elementos da janela
    var elem;
    var temp;

    // ajusta o divDocumentoTransporte
    elem = window.document.getElementById('divDocumentoTransporte');
    with (elem.style) 
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;

    }

    /*    // txtDocumentoTransporte
    elem = window.document.getElementById('txtDocumentoTransporte');
    elem.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (elem.style) 
    {
    left = 0;
    top = 16;
    width = (elem.maxLength + 8) * FONT_WIDTH - 4;
    heigth = 24;
       
    txtDocumentoTransporte.onkeypress = verifyNumericEnterNotLinked;
    txtDocumentoTransporte.setAttribute('thePrecision', 10, 1);
    txtDocumentoTransporte.setAttribute('theScale', 0, 1);
    txtDocumentoTransporte.setAttribute('verifyNumPaste', 1);
    txtDocumentoTransporte.setAttribute('minMax', new Array(1, 9999999999), 1);
    txtDocumentoTransporte.value = '';
    }*/

    //btnAprovar
    elem = window.document.getElementById('btnAprovar');
    with (elem.style) {
        top = 16; //parseInt(document.getElementById('txtDocumentoTransporte').style.top);
        left = 0;//parseInt(document.getElementById('btnFindPesquisa').style.left) + parseInt(document.getElementById('btnFindPesquisa').style.width) + 2;
        width = 80;
        height = 24;
    }
    btnAprovar.onclick = aprovarFatura;

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 2;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divDocumentoTransporte').style.top) + parseInt(document.getElementById('divDocumentoTransporte').style.height) + ELEM_GAP;
        width = temp + 400;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 100;
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width) + 50;
        height = parseInt(document.getElementById('divFG').style.height) + 63;
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    startPesq();
}

/*function txtDocumentoTransporte_ondigit(ctl) {
// changeBtnState(ctl.value);
if (trimStr(txtDocumentoTransporte.value) != '')
changeBtnState(txtDocumentoTransporte.value);        

if (event.keyCode == 13)
btn_onclick(btnFindPesquisa);
}*/



/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
/*    else if (ctl.id = 'btnFindPesquisa') 
    {
        txtDocumentoTransporte.value = trimStr(txtDocumentoTransporte.value);

        changeBtnState(txtDocumentoTransporte.value);

        if (btnFindPesquisa.disabled)
            return;

        startPesq(txtDocumentoTransporte.value);
    }*/
}

function startPesq()
{
    lockControlsInModalWin(true);

    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EmpresaID' + '\'' + '].value');
    var nFaturaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FaturaID' + '\'' + '].value');

    setConnection(dsoPesq);

    dsoPesq.SQL = 'SELECT CONVERT(BIT, 0) AS Aprovar, a.CodigoErro, b.MensagemErro, dbo.fn_Pessoa_Fantasia(a.AprovadorID, 0) AS Aprovador, ' +
                            'a.dtAprovacao, a.Observacao, a.TransportadoraErroID, b.Interno ' +
	                    'FROM dbo.TransportadoraDados_Detalhes_Erros a WITH(NOLOCK) ' +
	                    '   INNER JOIN FreteErros b WITH(NOLOCK) ON (b.FreteErroID = a.CodigoErro) ' +
	                    'WHERE (a.RegistroID = ' + nFaturaID + ') ' +
	                        'AND (a.TipoRegistroID = 1902) AND (a.AprovadorID IS NULL) AND b.PermiteAprovacao = 1 ' + 
                        'ORDER BY a.CodigoErro';

    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC() 
{
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    fg.FrozenCols = 0;

    headerGrid(fg, ['Aprovar', 'Codigo', 'Erro', 'Aprovador', 'Data', 'Observacao', 'TransportadoraErroID'], [6]);

    fillGridMask(fg, dsoPesq, ['Aprovar', 'CodigoErro*', 'MensagemErro*', 'Aprovador*', 'dtAprovacao*', 'Observacao', 'TransportadoraErroID'],
                              ['', '', '', '', '', '']);

    alignColsInGrid(fg, [0]);

    lockControlsInModalWin(false);

    window.focus();

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;

    glb_GridIsBuilding = false;

    // destrava botao Incluir se tem linhas no grid
    if (fg.Rows > 1) 
    {
        fg.focus();
        fg.Editable = true;
    }
}

function aprovarFatura() 
{
    var strPars = '';
    var nUserID = getCurrUserID();
    var nFaturaID = null;
    
    for (i = 1; i < fg.Rows; i++) 
    {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aprovar')) != 0) 
        {
            if (strPars == '')
                strPars += '?nTransportadoraErros=' + escape('/' + fg.TextMatrix(i, getColIndexByColKey(fg, 'TransportadoraErroID')) + '/');
            else
                strPars += escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'TransportadoraErroID')) + '/');
        }
    }

    if (strPars != '') 
    {
        strPars += '&nFaturaID=' + escape(nFaturaID);
        strPars += '&nUserID=' + escape(nUserID);

        setConnection(dsoAprovacao);
        dsoAprovacao.URL = SYS_ASPURLROOT + '/modcomercial/submodtransporte/faturatransporte/serverside/aprovacaoFatura.aspx' + strPars;
        dsoAprovacao.ondatasetcomplete = Aprovacao_DSC;
        dsoAprovacao.Refresh();
    }
    else
        if (window.top.overflyGen.Alert('Selecione pelo menos um registro!') == 0)
            return null;
}

function Aprovacao_DSC()
{
    var resultado = dsoAprovacao.recordset['Resultado'].value;

    if (resultado == 0) 
    {
        if (window.top.overflyGen.Alert('Fatura aprovada com sucesso!') == 0)
            return null;
    }
    else 
    {
        if (window.top.overflyGen.Alert('Erro ao aprovar Fatura!') == 0)
            return null;
    }

    startPesq();
}