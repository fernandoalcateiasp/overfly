/********************************************************************
modalprint_faturatransporte.js

Library javascript para o modalprint.asp
Relacoes entre Pessoas 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// variavel de timer
var __timerInt;

//  dso usado para envia e-mails 
var dsoEMail = new CDatatransport("dsoEMail");
//  dso usado para verificar o count de DocumentoTransportes
var dsoDocumentoTransporte = new CDatatransport("dsoDocumentoTransporte");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
setupPage()
loadDataAndTreatInterface()
loadReportsInList()
linksDivAndReports()
invertChkBox()
chkBox_Clicked()
selReports_Change()
btnOK_Status()
btnOK_Clicked()
btnInserir_Clicked()
inserir_DSC()
showDivByReportID()
getCurrDivReportAttr()
noRights()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    glb_bUseProgressBar = true;
    asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalprintBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    window.focus();

    if (selReports.disabled == false)
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();

    btnOK_Status();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    window.focus();

    if (ctl.id == btnOK.id) {
        btnOK.focus();
    }
    else if (ctl.id == btnCanc.id) {
        btnCanc.focus();
    }

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Relat�rios', 1);

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;

    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    with (lblReports.style) {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // ajusta combo de relatorios
    with (selReports.style) {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // ajusta o divDiferencialParcelasFrete
    with (divDiferencialParcelasFrete.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // O estado do botao btnOK
    btnOK_Status();

    // O usuario tem direito ou nao
    noRights();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;

    selReports.onchange = selReports_Change;

    // carrega a lista de relatorios
    loadReportsInList();

    // linka os divs nos relatorios
    linkDivsAndReports();

    // ajusta o divDiferencialParcelasFrete
    adjustdivDiferencialParcelasFrete();

    // restringe digitacao em campos numericos
    setMaskAndLengthToInputs();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList() {
    var i;

    if (glb_arrayReports != null) {
        for (i = 0; i < glb_arrayReports.length; i++) {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);

            if (glb_arrayReports[i][2])
                selReports.selectedIndex = i;
        }

        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports() {
    divDiferencialParcelasFrete.setAttribute('report', 40214, 1);
    ;
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox() {
    var ctl = this.nextSibling;

    if (ctl.disabled == true)
        return;

    ctl.checked = !ctl.checked;

    // O estado do botao btnOK
    btnOK_Status();

    window.focus();
    if (ctl.disabled == false)
        ctl.focus();

    return true;
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change() {
    // mostra o div correspondente ao relatorio
    showDivByReportID();

    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if (this.disabled == false)
        this.focus();

    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status() {
    var btnOKStatus = true;
    var i, elem;

    if (selReports.selectedIndex != -1) {
        if ((selReports.value == 40211) || (selReports.value == 40212) || (selReports.value == 40214))
            btnOKStatus = false;
    }

    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked() {
    if (selReports.value == 40211)
        RelatorioDocumentoTransporte();
    else if (selReports.value == 40212)
        RelatorioDocumentoTransporteFaltantes();
    else if (selReports.value == 40214)
        RelatorioFaturaTransporteDiferencial();
    else
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
}

function showDivByReportID() {
    var i, coll, attr, currRep;

    attr = null;
    currRep = 0;

    currRep = selReports.value;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (attr == currRep)
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr() {
    var i, coll, attr, retVal;

    attr = null;
    retVal = 0;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (coll[i].style.visibility == 'visible') {
                retVal = attr;
                break;
            }
        }
    }
    return retVal;
}

/********************************************************************
Restringe quantidade de caracteres digitados, coloca
mascara para digitacao em campos numericos,
seleciona conteudo do campo quando em foco etc
********************************************************************/
function setMaskAndLengthToInputs() {
    ;
    /*txtEtiquetaVertical.onkeypress = verifyNumericEnterNotLinked;
    txtEtiquetaHorizontal.onkeypress = verifyNumericEnterNotLinked;
    txtAltura.onkeypress = verifyNumericEnterNotLinked;
    txtLargura.onkeypress = verifyNumericEnterNotLinked;
    txtMargemSuperior.onkeypress = verifyNumericEnterNotLinked;
    txtMargemEsquerda.onkeypress = verifyNumericEnterNotLinked;
    txtCEPInicio.onkeypress = verifyNumericEnterNotLinked;
    txtCEPFim.onkeypress = verifyNumericEnterNotLinked;

    txtEtiquetaVertical.setAttribute('verifyNumPaste', 1);
    txtEtiquetaHorizontal.setAttribute('verifyNumPaste', 1);
    txtAltura.setAttribute('verifyNumPaste', 1);
    txtLargura.setAttribute('verifyNumPaste', 1);
    txtMargemSuperior.setAttribute('verifyNumPaste', 1);
    txtMargemEsquerda.setAttribute('verifyNumPaste', 1);
    txtCEPInicio.setAttribute('verifyNumPaste', 1);
    txtCEPFim.setAttribute('verifyNumPaste', 1);

    txtEtiquetaVertical.setAttribute('thePrecision', 2, 1);
    txtEtiquetaHorizontal.setAttribute('thePrecision', 2, 1);
    txtAltura.setAttribute('thePrecision', 2, 1);
    txtLargura.setAttribute('thePrecision', 3, 1);
    txtMargemSuperior.setAttribute('thePrecision', 2, 1);
    txtMargemEsquerda.setAttribute('thePrecision', 2, 1);
    txtCEPInicio.setAttribute('thePrecision', 9, 1);
    txtCEPFim.setAttribute('thePrecision', 9, 1);
    
    txtEtiquetaVertical.setAttribute('theScale', 0, 1);
    txtEtiquetaHorizontal.setAttribute('theScale', 0, 1);
    txtAltura.setAttribute('theScale', 0, 1);
    txtLargura.setAttribute('theScale', 0, 1);
    txtMargemSuperior.setAttribute('theScale', 0, 1);
    txtMargemEsquerda.setAttribute('theScale', 0, 1);
    txtCEPInicio.setAttribute('theScale', 0, 1);
    txtCEPFim.setAttribute('theScale', 0, 1);

    txtEtiquetaVertical.setAttribute('minMax', new Array(0, 99), 1);
    txtEtiquetaHorizontal.setAttribute('minMax', new Array(0, 99), 1);
    txtAltura.setAttribute('minMax', new Array(0, 99), 1);
    txtLargura.setAttribute('minMax', new Array(0, 999), 1);
    txtMargemSuperior.setAttribute('minMax', new Array(0, 99), 1);
    txtMargemEsquerda.setAttribute('minMax', new Array(0, 99), 1);
    txtCEPInicio.setAttribute('minMax', new Array(0, 999999999), 1);
    txtCEPFim.setAttribute('minMax', new Array(0, 999999999), 1);
    
    txtEtiquetaVertical.onfocus = selFieldContent;
    txtEtiquetaHorizontal.onfocus = selFieldContent;
    txtAltura.onfocus = selFieldContent;
    txtLargura.onfocus = selFieldContent;
    txtMargemSuperior.onfocus = selFieldContent;
    txtMargemEsquerda.onfocus = selFieldContent;
    txtCEPInicio.onfocus = selFieldContent;
    txtCEPFim.onfocus = selFieldContent;*/
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights() {
    if (selReports.options.length != 0)
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;

    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else
            coll[i].style.visibility = 'hidden';
    }

    // desabilita o combo de relatorios
    selReports.disabled = true;

    // desabilita o botao OK
    btnOK.disabled = true;

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];

    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);

    elem = document.getElementById('selReports');

    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);

    // a altura livre    
    modHeight -= topFree;

    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt';
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';

    // acrescenta o elemento
    window.document.body.appendChild(elem);

    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);

    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;

    return null;
}

function RelatorioDocumentoTransporte() {
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var nFaturaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FaturaID' + '\'' + '].value');
    var nNumero = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Numero' + '\'' + '].value');
    var formato = 2; //Excel

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&formato=" + formato +
                        "&nFaturaID=" + nFaturaID + "&nNumero=" + nNumero;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modcomercial/submodtransporte/faturatransporte/serverside/Reports_faturatransporte.aspx?" + strParameters;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
}

function RelatorioDocumentoTransporteFaltantes() {
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var nFaturaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FaturaID' + '\'' + '].value');
    var nNumero = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Numero' + '\'' + '].value');
    var nQtdFaltantes = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'QuantidadeDocumentoTransporteFaltantes' + '\'' + '].value');
    var formato = 2; //Excel

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&formato=" + formato +
                        "&nFaturaID=" + nFaturaID + "&nNumero=" + nNumero + "&nQtdFaltantes=" + nQtdFaltantes;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modcomercial/submodtransporte/faturatransporte/serverside/Reports_faturatransporte.aspx?" + strParameters;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
}

function verificaData(sData) {
    var sData = trimStr(sData);
    var bDataIsValid = true;

    if (sData != '')
        bDataIsValid = chkDataEx(sData);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        lockControlsInModalWin(false);
        return false;
    }
    return true;
}

function adjustdivDiferencialParcelasFrete() {
    if (glb_sCaller != 'PL')
        return null;

    var optionValue;
    var optionStr;

    txtDataInicio.maxLength = 10;
    txtDataFim.maxLength = 10;

    txtDataInicio.value = glb_dCurrDate;
    txtDataFim.value = glb_dCurrDate;

    adjustElementsInForm([['lblData', 'selData', 18, 1, -10, -10],
                          ['lblTransportadoras', 'selTransportadoras', 13, 1, 102],
                          ['lblDataInicio', 'txtDataInicio', 15, 2, -10],
                          ['lblDataFim', 'txtDataFim', 15, 2, -3],
                          ['lblCadastrado', 'chkCadastrado', 3, 3, -10],
                          ['lblPendente', 'chkPendente', 3, 3, -8],
                          ['lblValidado', 'chkValidado', 3, 3, -8]],
                            null, null, true);

    selTransportadoras.style.height = 118;
    selTransportadoras.style.width = 170;
    
    // campo digitacao maxima: 12/12/2002 12:12:12
    txtDataInicio.maxLength = 19;
    txtDataFim.maxLength = 19;
    // e seleciona conteudo quando em foco
    txtDataInicio.onfocus = selFieldContent;
    txtDataFim.onfocus = selFieldContent;
    // e so aceita caracteres coerentes com data
    txtDataInicio.onkeypress = verifyDateTimeNotLinked;
    txtDataFim.onkeypress = verifyDateTimeNotLinked;
}

function RelatorioFaturaTransporteDiferencial() {
    var ndtInicio = txtDataInicio.value;
    var ndtFim = txtDataFim.value;
    var nTipoData = selData.value;
    var nEstadoID = '';

    if (!verificaData(ndtInicio))
        return null;
    if (!verificaData(ndtFim))
        return null;

    ndtInicio = dateFormatToSearch(ndtInicio);
    ndtFim = dateFormatToSearch(ndtFim);

    if ((chkCadastrado.checked) || (chkPendente.checked) || (chkValidado.checked)) {
        if (chkCadastrado.checked) {
            nEstadoID = '1';
        }
        if (chkPendente.checked) {
            nEstadoID += (nEstadoID != '') ? ',8' : '8';
        }
        if (chkValidado.checked) {
            nEstadoID += (nEstadoID != '') ? ',65' : '65';
        }
    }

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var formato = 2;
    
    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&Formato=" + formato + "&nTipoData=" + nTipoData + "&ndtInicio=" + ndtInicio +
                        "&ndtFim=" + ndtFim + "&nEstadoID=" + nEstadoID + "&TransportadoraIn=" + TransportadoraIn() + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modcomercial/submodtransporte/faturatransporte/serverside/Reports_faturatransporte.aspx?' + strParameters;
}

function TransportadoraIn() {
    var i;
    var transportadoraID;

    // Come�a a montar a lista de transportadoras.
    transportadoraID = "(";

    // Monta a lista de transportadora.
    for (i = 0; i < selTransportadoras.options.length; i++) {
        // Se a transportadora estiver selecionada, coloca ela na lista.
        if (selTransportadoras.options[i].selected == true) {
            if (transportadoraID != "(") transportadoraID += ",";

            transportadoraID += selTransportadoras.options[i].value;
        }
    }

    // Termina de montar a lista das transportadoras.
    transportadoraID += ")";

    return transportadoraID;
}