/********************************************************************
aprovacaolote.js

Library javascript para o aprovacaolote.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport("dsoPesq");
var dsoAprovacao = new CDatatransport("dsoAprovacao");

//variáveis para enviar dados ao servidor (excesso de registros no grid)
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();
    
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 20;
    //modalFrame.style.left = 0;
    
    // ajusta o body do html
    var elem = document.getElementById('aprovacaoloteHtmlBody');

    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }


    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no campo apropriado
    /*    if (document.getElementById('txtDocumentoTransporte').disabled == false)
    txtDocumentoTransporte.focus();*/
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Aprovar Fatura de Transporte', 1);

    // ajusta elementos da janela
    var elem;
    var temp;

    // ajusta o divDocumentoTransporte
    elem = window.document.getElementById('divDocumentoTransporte');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;

    }

    //btnAprovar
    elem = window.document.getElementById('btnAprovar');
    with (elem.style) {
        top = 0; //parseInt(document.getElementById('txtDocumentoTransporte').style.top);
        left = 0; //parseInt(document.getElementById('btnFindPesquisa').style.left) + parseInt(document.getElementById('btnFindPesquisa').style.width) + 2;
        width = 80;
        height = 24;
    }

    btnAprovar.onclick = saveDataFromGrid /*aprovarFatura*/;

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 2;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        //top = parseInt(document.getElementById('divDocumentoTransporte').style.top) + parseInt(document.getElementById('divDocumentoTransporte').style.height) + ELEM_GAP;
        top = parseInt(document.getElementById('btnAprovar').style.top) + 30;
        width = temp + 630;
        height = parseInt(document.getElementById('divDocumentoTransporte').style.height);  //(MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 100;
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        //top = 0;
        top = parseInt(document.getElementById('divFG').style.top) + ELEM_GAP;
        width = parseInt(document.getElementById('divFG').style.width);
        height = 495;
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    startPesq();

    divFG.Redraw = 2;
    fg.Redraw = 2;
}

/*function txtDocumentoTransporte_ondigit(ctl) {
// changeBtnState(ctl.value);
if (trimStr(txtDocumentoTransporte.value) != '')
changeBtnState(txtDocumentoTransporte.value);        

if (event.keyCode == 13)
btn_onclick(btnFindPesquisa);
}*/



/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
    /*    else if (ctl.id = 'btnFindPesquisa') 
    {
    txtDocumentoTransporte.value = trimStr(txtDocumentoTransporte.value);

        changeBtnState(txtDocumentoTransporte.value);

        if (btnFindPesquisa.disabled)
    return;

        startPesq(txtDocumentoTransporte.value);
    }*/
}

function startPesq() {
    lockControlsInModalWin(true);

    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EmpresaID' + '\'' + '].value');
    var nFaturaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FaturaID' + '\'' + '].value');

    setConnection(dsoPesq);

    dsoPesq.SQL = 'SELECT CONVERT(BIT, 0) AS Ok, a.DocumentoTransporteID, b.Numero, b.ValorTotal, ' +
		                'dbo.fn_DocumentoTransporte_Totais(b.DocumentoTransporteID, 1) AS ValorTotalParcelasFrete, ' +
		                'dbo.fn_DocumentoTransporte_Totais(b.DocumentoTransporteID, 2) AS ValorDiferenca, ' +
		                'dbo.fn_DocumentoTransporte_Totais(b.DocumentoTransporteID, 3) AS PercentualDiferenca, ' +
		                'd.MensagemErro, c.TransportadoraErroID ' +
	                'FROM FaturasTransporte_DocumentosTransporte a WITH(NOLOCK) ' +
		                'INNER JOIN DocumentosTransporte b WITH(NOLOCK) ON (b.DocumentoTransporteID = a.DocumentoTransporteID) ' +
		                'INNER JOIN TransportadoraDados_Detalhes_Erros c WITH(NOLOCK) ON (c.RegistroID = b.DocumentoTransporteID) ' +
		                'INNER JOIN FreteErros d WITH(NOLOCK) ON (d.FreteErroID = c.CodigoErro) ' +
	                'WHERE a.FaturaID = ' + nFaturaID + ' AND b.EstadoID <> 8 AND c.TipoRegistroID = 1901 AND c.AprovadorID IS NULL AND d.PermiteAprovacao = 1 ' +
	                'ORDER BY b.DocumentoTransporteID, c.TransportadoraErroID';

    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC() {
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    fg.FrozenCols = 0;

    headerGrid(fg, ['Ok', 'ID', 'Numero', 'Valor Total', 'Total Parcelas', 'Diferença', 'Dif', 'Erro', 'TransportadoraErroID'], [8]);

    fillGridMask(fg, dsoPesq, ['Ok', 'DocumentoTransporteID', 'Numero*', 'ValorTotal*', 'ValorTotalParcelasFrete*', 'ValorDiferenca*', 'PercentualDiferenca',
                                'MensagemErro', 'TransportadoraErroID'],
                                ['', '', '', '', '', '', '', '']);

    //alignColsInGrid(fg, [0]);

    lockControlsInModalWin(false);

    window.focus();

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;

    // adiciona classificação pelo header do grid
    fg.ExplorerBar = 5;

    glb_GridIsBuilding = false;

    // destrava botao Incluir se tem linhas no grid
    if (fg.Rows > 1) {
        fg.focus();
        fg.Editable = true;
    }
}

function js_fg_modalaprovacaoloteDblClick(grid, Row, Col) {

    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if ((Col != getColIndexByColKey(grid, 'Ok')))
        return true;

    // trap so header
    if (Row != 1)
        return true;

    var i;
    var bFill = true;

    fg.Editable = false;
    fg.Redraw = 0;
    lockControlsInModalWin(true);


    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 1; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    lockControlsInModalWin(false);
    fg.Editable = true;
    fg.Redraw = 2;
    window.focus();
    fg.focus();
}

/*function aprovarFatura() {
    var strPars = '';
    var nUserID = getCurrUserID();
    
    for (i = 1; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Ok')) != 0) {
            if (strPars == '')
                strPars += '?nTransportadoraErroID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'TransportadoraErroID')));
            else
                strPars += '&nTransportadoraErroID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'TransportadoraErroID')));
        }
    }

    if (strPars != '') {
        strPars += '&nUserID=' + escape(nUserID);

        setConnection(dsoAprovacao);
        dsoAprovacao.URL = SYS_ASPURLROOT + '/modcomercial/submodtransporte/faturatransporte/serverside/aprovacaoLote.aspx' + strPars;
        dsoAprovacao.ondatasetcomplete = Aprovacao_DSC;
        dsoAprovacao.Refresh();
    }
    else
        if (window.top.overflyGen.Alert('Selecione pelo menos um registro!') == 0)
        return null;
}

function Aprovacao_DSC() {
    var resultado = dsoAprovacao.recordset['Resultado'].value;

    if (resultado == 0) {
        if (window.top.overflyGen.Alert('Erros aprovados!') == 0)
            return null;
    }
    else {
        if (window.top.overflyGen.Alert('Erros aprovados! ' + resultado + ' documentos de transporte validados.') == 0)
            return null;
    }

    startPesq();
}*/

/********************************************************************
Salvar os dados do grid (marcados com OK) no banco (btnGravar)
********************************************************************/
function saveDataFromGrid() {

    var nUserID = getCurrUserID();
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    lockControlsInModalWin(true);

    for (i = 1; i <= fg.Rows; i++) {
        if ((getCellValueByColKey(fg, 'OK', i) != 0)
            && (getCellValueByColKey(fg, 'TransportadoraErroID', i) != (0 || null))) {

            nBytesAcum = strPars.length;

            if ((nBytesAcum <= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                    strPars = '';
                    nDataLen = 0;
                }
                strPars += '?nTransportadoraErroID=' + escape(getCellValueByColKey(fg, 'TransportadoraErroID', i));
                strPars += '&nUserID=' + escape(nUserID);
            }
            nDataLen++;
        }
    }

    if (nDataLen > 0) {
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
        sendDataToServer();
    }
    else {
        window.top.overflyGen.Alert('Selecione pelo menos um registro!');
        setupPage();
    }
}

/********************************************************************
Envio de dados ao servidor para gravar (btnGravar)
********************************************************************/
function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {

            setConnection(dsoAprovacao);
            dsoAprovacao.URL = SYS_ASPURLROOT + '/modcomercial/submodtransporte/faturatransporte/serverside/aprovacaoLote.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoAprovacao.ondatasetcomplete = sendDataToServer_DSC;
            dsoAprovacao.refresh();
        }
        else {
            lockControlsInModalWin(true);
        }
    }
    catch (e) {
        lockControlsInModalWin(false);
    }
}

/********************************************************************
Retorno do servidor (btnGravar)
********************************************************************/
function sendDataToServer_DSC() {

    if (glb_nPointToaSendDataToServer >= (glb_aSendDataToServer.length - 1)) {

        var resultado = dsoAprovacao.recordset['Resultado'].value;

        if (resultado == 0) {
            if (window.top.overflyGen.Alert('Erros aprovados!') == 0)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Erros aprovados! ' + (glb_nPointToaSendDataToServer + 1) + ' documentos de transporte validados.') == 0)
                return null;
        }
        setupPage();
        startPesq();
    }
    else {
        glb_nPointToaSendDataToServer++;
        glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
    }
}