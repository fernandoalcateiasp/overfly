/********************************************************************
pesquisaDocumentoTransporte.js

Library javascript para o pesquisaDocumentoTransporte.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport("dsoPesq");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    var elem = document.getElementById('pesquisaDocumentoTransporteHtmlBody');

    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }


    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no campo apropriado
    if (document.getElementById('txtDocumentoTransporte').disabled == false)
        txtDocumentoTransporte.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() 
{
    // texto da secao01
    secText('Pesquisar Documento de Transporte', 1);

    // ajusta elementos da janela
    var elem;
    var temp;

    // ajusta o divDocumentoTransporte
    elem = window.document.getElementById('divDocumentoTransporte');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;

    }

    // txtDocumentoTransporte
    elem = window.document.getElementById('txtDocumentoTransporte');
    elem.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (elem.style) {
        left = 0;
        top = 16;
        width = (elem.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;

        txtDocumentoTransporte.onkeypress = verifyNumericEnterNotLinked;
        txtDocumentoTransporte.setAttribute('thePrecision', 10, 1);
        txtDocumentoTransporte.setAttribute('theScale', 0, 1);
        txtDocumentoTransporte.setAttribute('verifyNumPaste', 1);
        txtDocumentoTransporte.setAttribute('minMax', new Array(1, 9999999999), 1);
        txtDocumentoTransporte.value = '';
    }

    // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');
    // btnFindPesquisa
    btnFindPesquisa.disabled = true;

    with (elem.style) {
        top = parseInt(document.getElementById('txtDocumentoTransporte').style.top);
        left = parseInt(document.getElementById('txtDocumentoTransporte').style.left) + parseInt(document.getElementById('txtDocumentoTransporte').style.width) + 2;
        width = 80;
        height = 24;
    }

    //btnDetalhar
    elem = window.document.getElementById('btnDetalhar');
    with (elem.style) {
        top = parseInt(document.getElementById('txtDocumentoTransporte').style.top);
        left = parseInt(document.getElementById('btnFindPesquisa').style.left) + parseInt(document.getElementById('btnFindPesquisa').style.width) + 2;
        width = 80;
        height = 24;
    }

    btnDetalhar.onclick = detalharDocumentoTransporte;

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divDocumentoTransporte').style.top) + parseInt(document.getElementById('divDocumentoTransporte').style.height) + ELEM_GAP;
        width = temp + 450;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 30;
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height) + 31;
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    startGridInterface(fg);

    headerGrid(fg, ['ID',
                    'Documento de Transporte',
                    'Data',
                    'Total Documento de Transporte',
                    'Total Parcelas Frete',
                    'Diferença',
                    'Dif %',
                    'Desconto',
                    'Abono',
                    'Observação'], [1]);
    
    //fg.Redraw = 0;
    //fg.ColWidth(getColIndexByColKey(fg, 'NotaFiscalID')) = 1430;
    fg.Redraw = 2;
    
}

function txtDocumentoTransporte_ondigit(ctl) {
    // changeBtnState(ctl.value);
    if (trimStr(txtDocumentoTransporte.value) != '')
        changeBtnState(txtDocumentoTransporte.value);        

    if (event.keyCode == 13)
        btn_onclick(btnFindPesquisa);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
    else if (ctl.id = 'btnFindPesquisa') 
    {
        txtDocumentoTransporte.value = trimStr(txtDocumentoTransporte.value);

        changeBtnState(txtDocumentoTransporte.value);

        if (btnFindPesquisa.disabled)
            return;

        startPesq(txtDocumentoTransporte.value);
    }
}

function startPesq(strPesquisa) 
{
    lockControlsInModalWin(true);

    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EmpresaID' + '\'' + '].value');
    var nFaturaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FaturaID' + '\'' + '].value');
    
    setConnection(dsoPesq);

    dsoPesq.SQL = 'SELECT a.DocumentoTransporteID, ' +
                         'a.EmpresaID, ' +
	                     'a.Numero AS NumeroDocumentoTransporte,  ' +
	                     'a.dtEmissao AS V_dtEmissaoDocumentoTransporte, ' +
	                     'a.ValorTotal, ' +
	                     'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID, 1) AS ValorTotalParcelasFrete, ' +
	                     'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID, 2) AS ValorDiferenca, ' +
	                     'dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID, 3) AS PercentualDiferenca, ' +
	                     'a.ValorDesconto, ' +
	                     'a.Observacoes ' +
	                  'FROM dbo.DocumentosTransporte a WITH(NOLOCK) ' +
	                    'INNER JOIN FaturasTransporte_DocumentosTransporte b WITH(NOLOCK) ON (b.DocumentoTransporteID = a.DocumentoTransporteID) ' +
	                  'WHERE a.Numero = ' + txtDocumentoTransporte.value +
                             ' AND b.FaturaID = ' + nFaturaID +
                             ' AND a.EmpresaID = ' + nEmpresaID +
                             ' AND a.EstadoID IN (65, 8) ' +
		          ' ORDER BY a.Numero';

    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC() {

    startGridInterface(fg);

    fg.FrozenCols = 0;

    headerGrid(fg, ['Numero', 'Data', 'Valor Total', 'Total Parcelas', 'Diferença', 'Dif %', 'Desconto', 'Observações', 'ID'], [8]);

    fillGridMask(fg, dsoPesq, ['NumeroDocumentoTransporte', 'V_dtEmissaoDocumentoTransporte', 'ValorTotal',
                               'ValorTotalParcelasFrete', 'ValorDiferenca', 'PercentualDiferenca', 'ValorDesconto', 'Observacoes', 'DocumentoTransporteID'],
                              ['', '', '', '', '', '', '']);

    alignColsInGrid(fg, [0, 2, 3, 4, 5, 6, 8]);                               
            
    lockControlsInModalWin(false);

    window.focus();

    // destrava botao Incluir se tem linhas no grid
    if (fg.Rows > 1) {
        //btnOK.disabled = false;
        fg.focus();
    }
    else {
        //btnOK.disabled = true;
        txtDocumentoTransporte.focus();
    }
}

function detalharDocumentoTransporte()
{
    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EmpresaID' + '\'' + '].value');
    
    if (fg.Rows > 1)
    {
        DocumentoTransporteID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'DocumentoTransporteID'));
        sendJSCarrier(getHtmlId(), 'SHOWDOCUMENTOTRANSPORTE', new Array(nEmpresaID, DocumentoTransporteID));
    }
}

function js_fg_modalpesquisaDocumentoTransporteDblClick(grid, Row, Col) 
{
    detalharDocumentoTransporte();
}