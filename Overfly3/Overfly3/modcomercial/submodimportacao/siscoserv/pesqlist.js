/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form visitas
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Dados da listagem da pesquisa
var dsoListData01 = new CDatatransport("dsoListData01");
// Dados dos combos de contexto e filtros
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");  
// Dados dos proprietarios para o pesqlist .URL
var dsoPropsPL = new CDatatransport("dsoPropsPL");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID',
		'Est',
		'Data In�cio',
		'Data Fim',
		'RAS',
		'RP',
        'RASProblema',
		'RPProblema',
		'Observa��o',
		'EstadoID');

    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    glb_aCOLPESQFORMAT = new Array('', '', dTFormat, dTFormat, '');

                                 
    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
    // seta botao de impressao
    especBtnIsPrintBtn('sup', 1);

    showBtnsEspecControlBar('sup', true, [1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos','Relat�rios','Procedimento']);

    setupEspecBtnsControlBar('sup', 'HDH');

    alignColsInGrid(fg, [0]);

    fg.ColHidden(getColIndexByColKey(fg, 'EstadoID')) = true;
    fg.ColHidden(getColIndexByColKey(fg, 'RasProblema')) = true;
    fg.ColHidden(getColIndexByColKey(fg, 'RPProblema')) = true;

    var i, j, lastRow;
    var bgColorYellow = 0x00ffff;
    
    for (i = 1; i < fg.Rows; i++)
    {
        // RAS
        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'RASProblema')) != '0')
            fg.Cell(6, i, getColIndexByColKey(fg, 'RAS'), i, getColIndexByColKey(fg, 'RAS')) = bgColorYellow;
        
        // RP
        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'RPProblema')) != '0')
            fg.Cell(6, i, getColIndexByColKey(fg, 'RP'), i, getColIndexByColKey(fg, 'RP')) = bgColorYellow;

    }
	
    
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) 
{
    var contexto = 0;
    var nEstadoID = 0;
    var nProcedimentoID = 0;
    var sAnchor = '';


    if (controlBar == 'SUP') 
    {
        // Documentos
        if (btnClicked == 1) 
        {
            if (fg.Rows > 1)
                __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
            else
                window.top.overflyGen.Alert('Selecione um registro.');
        }

        // Procedimentos
        else if (btnClicked == 3) 
        {
            contexto = getCmbCurrDataInControlBar('sup', 1);
            if (fg.Rows > 1)
                nEstadoID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EstadoID'));
            else
                nEstadoID = 0;
                
            nProcedimentoID = 935; // Siscoserv

            // Siscoserv
            if (contexto[1] == 5321) 
            {
                // Cadastrado
                if (nEstadoID == 1)
                    sAnchor = '42';
                // Gerado
                else if (nEstadoID == 62)
                    sAnchor = '43';
                // Enviado
                else if (nEstadoID == 63)
                    sAnchor = '44';
                // Pendente
                else if (nEstadoID == 65)
                    sAnchor = '45';
                else
                    sAnchor = 'null';
            }
            window.top.openModalControleDocumento('S', '', nProcedimentoID, null, sAnchor, 'T');
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
 // Modal de documentos
     if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') 
     {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de Procedimento
    else if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }     
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}
