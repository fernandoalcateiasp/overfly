using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodimportacao.siscoserv.serverside
{
    public partial class gravaretorno: System.Web.UI.OverflyPage
    {
        private Integer siscoservID;
        private Integer userId;

        private string resultado;
        
        protected Integer nSiscoServID
        {
            set { siscoservID = value != null ? value : new Integer(0); }
        }

        protected Integer nUserID
        {
            set { userId = value != null ? value : new Integer(0); }
        }

        protected void ProcessaRetorno()
        {
            // Roda a procedure sp_SiscoServ_Retorno.
            ProcedureParameters[] procparam = new ProcedureParameters[3];
            procparam[0] = new ProcedureParameters("@SiscoservID", SqlDbType.Int, siscoservID.intValue());
            procparam[1] = new ProcedureParameters("@UsuarioID", SqlDbType.Int, userId.intValue());
            procparam[2] = new ProcedureParameters("@Resultado", SqlDbType.VarChar, DBNull.Value, ParameterDirection.InputOutput);
            procparam[2].Length = 8000;


            DataInterfaceObj.execNonQueryProcedure("sp_Siscoserv_Retorno", procparam);

            // Obt�m o resultado da execu��o.
            resultado = procparam[2].Data.ToString();
        }


        protected override void PageLoad(object sender, EventArgs e)
        {
            ProcessaRetorno();

            // Gera o resultado.
            WriteResultXML( DataInterfaceObj.getRemoteData("select '" + resultado + "' as Resultado "));
        }
    }
}
