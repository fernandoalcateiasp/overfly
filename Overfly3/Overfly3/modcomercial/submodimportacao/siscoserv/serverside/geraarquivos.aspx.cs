using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodimportacao.siscoserv.serverside
{
    public partial class geraarquivos: System.Web.UI.OverflyPage
    {
        private Integer siscoservID;
        private Integer userId;

        protected Integer nSiscoServID
        {
            set { siscoservID = value != null ? value : new Integer(0); }
        }

        protected Integer nUserID
        {
            set { userId = value != null ? value : new Integer(0); }
        }
        
        protected override void PageLoad(object sender, EventArgs e)
        {
            // Roda a procedure sp_SiscoServ_Gerador.(RAS)
            ProcedureParameters[] procparam = new ProcedureParameters[3];
            procparam[0] = new ProcedureParameters("@SiscoservID", SqlDbType.Int, siscoservID.intValue());
            procparam[1] = new ProcedureParameters("@UsuarioID", SqlDbType.Int, userId.intValue());
            procparam[2] = new ProcedureParameters("@TipoID", SqlDbType.Int, 1);

            DataInterfaceObj.execNonQueryProcedure("sp_Siscoserv_Gerador", procparam);

            procparam[0] = new ProcedureParameters("@SiscoservID", SqlDbType.Int, siscoservID.intValue());
            procparam[1] = new ProcedureParameters("@UsuarioID", SqlDbType.Int, userId.intValue());
            procparam[2] = new ProcedureParameters("@TipoID", SqlDbType.Int, 2);
            DataInterfaceObj.execNonQueryProcedure("sp_Siscoserv_Gerador", procparam);
            
            // Gera o resultado.
            WriteResultXML(DataInterfaceObj.getRemoteData("select null as resultServ"));
        }
    }
}
