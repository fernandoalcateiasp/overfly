/********************************************************************
especificinf.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.SiscoServID, a.ProprietarioID,a.AlternativoID,a.Observacoes FROM Siscoserv a WITH(NOLOCK) WHERE '+
			sFiltro + 'a.SiscoservID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    //PASTA RAS
    else if (folderID == 24311 || folderID == 24312) 
    {
	    currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
	    aValue = currCmb2Data[1].split(',');
	    if (aValue[1] != "(1=1)")
	        aValue[1] = aValue[1] + " AND (ISNULL(dbo.fn_TipoAuxiliar_Item(d.ErroID, 5), 1) = 1) ";
        //@@ As variaveis a seguir sao da automacao
        //e devem ser definidas pelo programador
        
        glb_vOptParam1 = aValue[1];
        // Final da automacao =========================================================

        return execPaging(dso, 'SiscoservID', idToFind, folderID);
        
    }
    // Pasta Protocolos
    else if (folderID == 24313) {
        dso.SQL = 'SELECT SisProtocoloID AS SisProtocoloID, SiscoservID AS SiscoservID, TipoProtocoloID AS TipoProtocoloID, DataGeracao AS DataGeracao, ' +
                  'ISNULL(Protocolo, SPACE(0)) AS Protocolo, ISNULL(StatusArquivoID, SPACE(0)) AS StatusArquivoID, ArquivoZip AS ArquivoZip, ISNULL(dbo.fn_TipoAuxiliar_Item(a.StatusArquivoID, 0), SPACE(1)) AS StatusDescricao ' +
                  'FROM Siscoserv_Protocolos a WITH(NOLOCK) ' +
                  'WHERE a.SiscoServID = ' + idToFind;

        return 'dso01Grid_DSC';
    }
    // Pasta Retorno
    else if (folderID == 24314) {
        dso.SQL = ' SELECT b.ArquivoZip AS ArquivoZip, B.Protocolo as Protocolo, a.NomeArquivo as NomeArquivo, CONVERT(VARCHAR(10), a.DataGeracao, ' + DATE_SQL_PARAM + ') as DataGeracao, ' +
                  ' dbo.fn_TipoAuxiliar_Item(a.StatusArquivoID, 2) AS StatusArquivo, dbo.fn_TipoAuxiliar_Item(a.ErroID, 2) AS Erro, a.NumeroRasRP as NumeroRasRP, ' +
                  ' a.Observacao AS Observacao, a.SisRetornoID AS SisRetornoID, ' +
                  ' ISNULL(dbo.fn_TipoAuxiliar_Item(a.StatusArquivoID, 0), SPACE(1)) AS StatusDescricao, ISNULL(dbo.fn_TipoAuxiliar_Item(a.ErroID, 0), SPACE(1)) AS ErroDescricao ' +
                  ' FROM SiscoServ_Retornos a WITH(NOLOCK) ' +
	              ' INNER JOIN Siscoserv_Protocolos b WITH(NOLOCK) ON a.SisProtocoloID = b.SisProtocoloID ' +
	              ' WHERE a.SiscoServID = ' + idToFind;

        return 'dso01Grid_DSC';
    }
    
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    var aEmpresaID = getCurrEmpresaData();
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
	else if(pastaID == 24313)
	{
	    if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM Siscoserv_Protocolos a WITH(NOLOCK) ' + 
                      'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.TipoProtocoloID = b.ItemID ' +
                      'WHERE (a.SiscoservID = ' + nRegistroID + ')';
        }

        if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM Siscoserv_Protocolos a WITH(NOLOCK) ' +
                      'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.StatusArquivoID = b.ItemID ' +
                      'WHERE (a.SiscoservID = ' + nRegistroID + ')';
        }

        
        
	}
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); // Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); // Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20010); // LOG

    vPastaName = window.top.getPastaName(24311);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24311); // Servi�os

    vPastaName = window.top.getPastaName(24312);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24312); // Pagamentos
    
    vPastaName = window.top.getPastaName(24313);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24313); // Protocolos
        
    vPastaName = window.top.getPastaName(24314);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24314); // Retorno
        
    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
	var currLine = -1;
	
    //@@ return true se tudo ok, caso contrario return false;
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 24311) 
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['SisPedidoID', registroID], []);
        }
        if (currLine < 0)
        {
            if (fg.disabled == false)
            {
				try
                {
                    fg.Editable = true;
					window.focus();
					fg.focus();
				}	
                catch(e)
                {
					;
                }
            }    
        
            return false;
        }    
    }

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010) {
        headerGrid(fg, ['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);

        fillGridMask(fg, currDSO, ['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['', '99/99/9999', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm:ss', '', '', '', '', '']);
        
    }
    //ras
    else if (folderID == 24311) {

        headerGrid(fg, ['Arquivo Zip',
                       'Pedido',
					   'NBS',
					   'Data Inicio',
					   'Data Fim',
					   'Valor',
					   'Saldo',
					   'Arquivo',
					   'RAS',
                       'Status',
                       'Erro',
					   'Observa��o',
					   'Arquivo XML',
					   'Status Descricao',
					   'Erro Descricao',
					   'Erro Aplica',
					   'SisPedidoID'], [12, 13, 14, 15, 16]);

        fillGridMask(fg, currDSO, ['ArquivoZip',
                                   'PedidoID',
                                   'NBS',
                                   'dtInicio',
					               'dtFim',
					               'Valor',
					               'Saldo',
					               'Arquivo',
					               'RAS',
					               'StatusArquivo',
                                   'Erro',
                                   'Observacao',
                                   'ArquivoXML',
                                   'StatusDescricao',
					               'ErroDescricao',
					               'ErroAplica',
                                   'SisPedidoID'],
								 ['', '', '', '99/99/9999', '99/99/9999', '', '', '', '999999999.99', '999999999.99', '', '', '', '', '', '', ''],
                                 ['', '', '', dTFormat, dTFormat, '', '', '', '###,###,##0.00', '###,###,##0.00', '', '', '', '', '', '', '']);
        fg.Editable = false;

        alignColsInGrid(fg, [5,6]);
        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
        fg.ExplorerBar = 5;
        
        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(0) = true;
        fg.MergeCol(1) = true;
        fg.MergeCol(2) = true;
        fg.MergeCol(3) = true;
        fg.MergeCol(4) = true;
        pintaPendente(folderID);
        
        
    }

    //Pagamentos
    else if (folderID == 24312) 
    {
        headerGrid(fg, ['Arquivo Zip',
                       'Financeiro',
					   'ValorID',
					   'Perc Pago',
					   'Arquivo',
					   'RP',
					   'Status',
                       'Erro',
					   'Observa��o',
					   'Status Descricao',
					   'Erro Descricao',
					   'Erro Aplica',
					   'SisPagamentoID'], [9, 10, 11, 12]);

        glb_aCelHint = [[0, 3, 'Percentual Pago']];
        

        fillGridMask(fg, currDSO, ['ArquivoZip',
                                   'FinanceiroID',
                                   'ValorID',
					               'PercentualPago',
					               'Arquivo',
					               'RP',
					               'StatusArquivo',
                                   'Erro',
                                   'Observacao',
                                   'StatusDescricao',
					               'ErroDescricao',
					               'ErroAplica',
                                   'SisPagamentoID'],
								 ['', '', '', '999999999.99', '', '', '', '', '', '', '', '', ''],
                                 ['', '', '', '###,###,##0.00', '', '', '', '', '', '', '', '', '']);

        fg.Editable = false;
        alignColsInGrid(fg, [3]);
        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);


        pintaPendente(folderID);
        fg.ExplorerBar = 5;
        
        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(0) = true;
        fg.MergeCol(1) = true;
        fg.MergeCol(2) = true;
        fg.MergeCol(3) = true;
        fg.MergeCol(4) = true;
    }


    //Protocolos
    else if (folderID == 24313) 
    {
        headerGrid(fg, ['Tipo',
                        'Arquivo',
                        'Protocolo',
                        'Data Gera��o',
                        'Status',
                        'StatusDescricao',
					    'SisProtocoloID'], [5,6]);

        fillGridMask(fg, currDSO, ['^TipoProtocoloID^dso01GridLkp^ItemID^ItemAbreviado',
                                   'ArquivoZip',
                                   'Protocolo',
                                   'DataGeracao',
                                   '^StatusArquivoID^dso02GridLkp^ItemID^ItemAbreviado',
                                   'StatusDescricao',
                                   'SisProtocoloID'],
								 ['', '', '', '99/99/9999', '', '', ''],
                                 ['', '', '', dTFormat, '', '', '']);
                                 
        fg.Editable = false;
        alignColsInGrid(fg, []);
        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
        fg.ExplorerBar = 5;
        
        fg.MergeCells = 4;
        fg.MergeCol(0) = true;
        fg.MergeCol(1) = true;
        fg.MergeCol(2) = true;

        pintaPendente(folderID);
        
    }
    //Retorno
    else if (folderID == 24314) 
    { 
        headerGrid(fg, ['Arquivo Zip',
                        'Protocolo',
                        'Data Gera��o',
                        'Status Arquivo',
                        'Nome Arquivo',
                        'N�mero RAS/RP',
                        'Erro',
                        'Observa��o',
                        'StatusDescricao',
                        'ErroDescricao',
                        'SisRetornoID'],[8, 9, 10]);

        fillGridMask(fg, currDSO, ['ArquivoZip',
                                    'Protocolo',
                                    'DataGeracao',
                                    'StatusArquivo',
                                    'NomeArquivo',
                                    'NumeroRasRP',
                                    'Erro',
                                    'Observacao',
                                    'StatusDescricao',
                                    'ErroDescricao',
                                    'SisRetornoID'],
                                   ['', '', '99/99/9999', '', '', '', '', '', '', '', ''],
                                   ['', '', dTFormat, '', '', '', '', '', '', '', '']);
        fg.Editable = false;
        fg.ExplorerBar = 5;
        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(0) = true;
        fg.MergeCol(1) = true;
        fg.MergeCol(2) = true;
        fg.MergeCol(3) = true;
        fg.MergeCol(4) = true;

        pintaPendente(folderID);
    }
}

function pintaPendente(folderID) 
{
    var bgColorYellow = 0x00ffff;
    var aHint = new Array();
    var j = -1;
    
    lockInterface(true);
    for (i = 1; i < fg.Rows; i++) 
    {
        if (folderID == 24311) // RAS
        {
            if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'RAS')) == ' ') && (fg.TextMatrix(i, getColIndexByColKey(fg, 'ErroAplica')) == '1')) 
            {
                fg.Cell(6, i, getColIndexByColKey(fg, 'StatusArquivo'), i, getColIndexByColKey(fg, 'StatusArquivo')) = bgColorYellow;
                fg.Cell(6, i, getColIndexByColKey(fg, 'RAS'), i, getColIndexByColKey(fg, 'RAS')) = bgColorYellow;
            }
        }
        else if (folderID == 24312) //RP
        {
            if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'RP')) == ' ') && (fg.TextMatrix(i, getColIndexByColKey(fg, 'ErroAplica')) == '1')) 
            {
                fg.Cell(6, i, getColIndexByColKey(fg, 'StatusArquivo'), i, getColIndexByColKey(fg, 'StatusArquivo')) = bgColorYellow;
                fg.Cell(6, i, getColIndexByColKey(fg, 'RP'), i, getColIndexByColKey(fg, 'RP')) = bgColorYellow;
            }

        }

        else if (folderID == 24313) //Protocolo
        {
            if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Protocolo')) == '') 
            {
                fg.Cell(6, i, getColIndexByColKey(fg, '^StatusArquivoID^dso02GridLkp^ItemID^ItemAbreviado'), i, getColIndexByColKey(fg, '^StatusArquivoID^dso02GridLkp^ItemID^ItemAbreviado')) = bgColorYellow;
                fg.Cell(6, i, getColIndexByColKey(fg, 'Protocolo'), i, getColIndexByColKey(fg, 'Protocolo')) = bgColorYellow;
            }

        }

        if (folderID == 24311 || folderID == 24312 || folderID == 24314) {
            j++;
            aHint[j] = [i, getColIndexByColKey(fg, 'StatusArquivo'), fg.TextMatrix(i, getColIndexByColKey(fg, 'StatusDescricao'))];
            j++;
            aHint[j] = [i, getColIndexByColKey(fg, 'Erro'), fg.TextMatrix(i, getColIndexByColKey(fg, 'ErroDescricao'))];
        }
        else if (folderID == 24313) 
        {
            j++;
            aHint[j] = [i, getColIndexByColKey(fg, '^StatusArquivoID^dso02GridLkp^ItemID^ItemAbreviado'), fg.TextMatrix(i, getColIndexByColKey(fg, 'StatusDescricao'))];
        }
        

    }

    glb_aCelHint = aHint;
    lockInterface(false);
    
    return true;
    

}