/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoSup01 = new CDatatransport("dsoSup01");
var dsoStateMachine = new CDatatransport("dsoStateMachine");
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
var dsoCurrData = new CDatatransport("dsoCurrData");
var dsoGeraArquivos = new CDatatransport("dsoGeraArquivos");
var dsoCmbDynamic03 = new CDatatransport("dsoCmbDynamic03");
var dsoVerificacao = new CDatatransport("dsoVerificacao");
var dsoAvancaEstado = new CDatatransport("dsoAvancaEstado");
var dsoVerificaArquivos = new CDatatransport("dsoVerificaArquivos");
var dsoGravaRetorno = new CDatatransport("dsoGravaRetorno"); 

var glb_CurrEstadoID;
var glb_NewEstadoID;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{

    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modcomercial/submodimportacao/siscoserv/inferior.asp',
                              SYS_PAGESURLROOT + '/modcomercial/submodimportacao/siscoserv/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null,null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'SiscoservID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup', [[1, 'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',8,1],
						  ['lblEstadoID', 'txtEstadoID', 2, 1],
						  ['lblDtInicio', 'txtDtInicio', 10, 1],
						  ['lblDtFim', 'txtDtFim', 10, 1],
						  ['lblObservacao', 'txtObservacao', 30, 1]], null, null, true);
	
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
    //@@
    var cmbID = '';
    var nTipoRelacaoID = '';
    var nRegExcluidoID = '';
    
}


// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWSISCOSERV') 
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if (__currFormState() != 0)
            return null;

        // param2 - traz array:
        // o id da empresa do form que mandou
        // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();

        /*if (param2[0] != empresa[0])
            return null;*/

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);

        window.top.focus();

        return new Array(getHtmlId(), null, null);
    }

    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;
    /*if ( (btnClicked == 'SUPDET') ||
         (btnClicked == 'SUPREFR') ||
         (btnClicked == 'SUPANT') ||
         (btnClicked == 'SUPSEG') )
    {
        startDynamicCmbs();
    }    
    else    
        // volta para a automacao    */
	finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{
    ;
}


/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked) 
{

    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    if (btnClicked == 'SUPINCL')
        adjustSupInterface();
	
	
    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    /*// Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();
    // Final de Nao mexer - Inicio de automacao =====================
    */
}


/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    
    glb_CurrEstadoID = currEstadoID;
    glb_NewEstadoID = newEstadoID;
    //Gera arquivos (C>G OU P>G)
    if ((currEstadoID == 1 || currEstadoID == 65) && (newEstadoID == 62))
        geraArquivos();
        //Gera Retorno (E>P)
    else if (currEstadoID == 63 && newEstadoID == 66)
        gravaRetorno();
    else
        verifySiscoserveInServer(currEstadoID, newEstadoID);

    return true;
    
}

function verifySiscoserveInServer(currEstadoID, newEstadoID)
{
    var nSiscoServID = dsoSup01.recordset['SiscoservID'].value;
    var strPars = new String();

    strPars = '?nSiscoservID=' + escape(nSiscoServID);
    strPars += '&nCurrEstadoID='+escape(currEstadoID);
    strPars += '&nNewEstadoID='+escape(newEstadoID);
    strPars += '&nUserID=' + escape(getCurrUserID()); 

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/siscoserv/serverside/verificacaosiscoserv.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifySiscoservInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes do Financeiro
********************************************************************/
function verifySiscoservInServer_DSC()
{
    var nResultado = dsoVerificacao.recordset['Resultado'].value;
    var sMensagem = dsoVerificacao.recordset['Mensagem'].value;

    if (nResultado == 0)
    {
        stateMachSupExec('CANC');
        window.top.overflyGen.Alert(sMensagem);
    }
    else
        stateMachSupExec('OK');
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosedEx(oldEstadoID, currEstadoID, bSavedID) 
{
    
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) 
{
    var contexto = 0;
    var nEstadoID = 0;
    var nProcedimentoID = 0;
    var sAnchor = '';
    
    if (controlBar == 'SUP') 
    {
        // Documentos
        if (btnClicked == 1)
            __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
        
        // Procedimentos
        else if (btnClicked == 3) 
        {
            contexto = getCmbCurrDataInControlBar('sup', 1);
            nEstadoID = dsoSup01.recordset['EstadoID'].value;
            nProcedimentoID = 935; // Siscoserv

            // Siscoserv
            if (contexto[1] == 5321) 
            {
                // Cadastrado
                if (nEstadoID == 1) 
                    sAnchor = '42';
                // Gerado
                else if (nEstadoID == 62) 
                    sAnchor = '43';
                // Enviado
                else if (nEstadoID == 63) 
                    sAnchor = '44';
                // Pendente
                else if (nEstadoID == 65) 
                    sAnchor = '45';
                else 
                    sAnchor = 'null';
            }
            window.top.openModalControleDocumento('S', '', nProcedimentoID, null, sAnchor, 'T');
        }
    } 
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    var nEmpresaID = getCurrEmpresaData();

    if (btnClicked == 'SUPOK') 
    {
        // Se e um novo registro
        if (dsoSup01.recordset[glb_sFldIDName] == null || dsoSup01.recordset[glb_sFldIDName].value == null)
            dsoSup01.recordset['EmpresaID'].value = nEmpresaID[0];
    }
    
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) 
{

    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    
}

/********************************************************************
Funcao criada pelo programador.
Preenche os campos do subform corrente, com os dados vindos da janela
modal de relacoes

Parametro:
aData   -   array de dados
            aData[0] - quem e na relacao ('SUJ' ou 'OBJ')
            aData[1] - ID do tipo da relacao
            aData[2] - ID do campo do subform corrente
            aData[3] - ID selecionado no grid da modal
            aData[4] - texto correspondente ao ID selecionado no grid da modal
            
            os demais elementos sao opcionais

Retorno:
nenhum
********************************************************************/
function fillFieldsByRelationModal(aData)
{
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var elem;
    var addSexOrServ = null;
    
    elem = window.document.getElementById(aData[2]);
    
    if ( elem == null )
        return;
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}


/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // mostra dois botoes especificos habilitados
    showBtnsEspecControlBar('sup', true,[0,0]);
    tipsBtnsEspecControlBar('sup',[]);
    
    // os hints dos botoes especificos
    //tipsBtnsEspecControlBar('sup', ['Documentos']);
    //setupEspecBtnsControlBar('sup', 'HHHH');

}

function geraArquivos() {
    var nSiscoservID = dsoSup01.recordset['SiscoservID'].value;

    var _retConf = window.top.overflyGen.Confirm('Deseja gerar os arquivos do SiscoservID ' + nSiscoservID + '? \nEste processo pode demorar um pouco.');
    if (_retConf == 0)
        return null;
    else if (_retConf == 1) 
    {
        lockInterface(true);
        window.status = 'Processando...';
        strPars = '?nSiscoServID=' + escape(nSiscoservID);
        strPars += '&nUserID=' + escape(getCurrUserID());
        
        dsoGeraArquivos.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/siscoserv/serverside/geraarquivos.aspx' + strPars;
        dsoGeraArquivos.ondatasetcomplete = geraArquivos_DSC;
        dsoGeraArquivos.refresh();

        return true;
    }

}

function geraArquivos_DSC() 
{
    avancaEstado(glb_CurrEstadoID, glb_NewEstadoID);
}

function avancaEstado(nEstadoDeID, nEstadoParaID) 
{
    
    var nSiscoservID = dsoSup01.recordset['SiscoservID'].value;
    
    lockInterface(true);
    strPars = '?nSiscoservID=' + escape(nSiscoservID);
    strPars += '&nEstadoDeID=' + escape(nEstadoDeID);
    
    strPars += '&nEstadoParaID=' + escape(nEstadoParaID);
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    dsoAvancaEstado.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/siscoserv/serverside/avancaestado.aspx' + strPars;
    dsoAvancaEstado.ondatasetcomplete = avancaEstado_DSC;
    dsoAvancaEstado.refresh();
}

function avancaEstado_DSC() 
{
    var nResultado = dsoAvancaEstado.recordset['Resultado'].value;
    var sMensagem = dsoAvancaEstado.recordset['Mensagem'].value;
    
    lockInterface(false);
    window.status = '';
    
    __btn_REFR('sup');   
    
    if (nResultado == 2)
    {
        window.top.overflyGen.Alert('Gerado com Sucesso');
        if (glb_NewEstadoID == 65)
            stateMachSupExec('CANC');
        else
            stateMachSupExec('OK');
    }
    else
    {
        if (glb_NewEstadoID == 66)
        {
            glb_CurrEstadoID = 63;
            glb_NewEstadoID = 65;
            stateMachSupExec('CANC');
            avancaEstado(glb_CurrEstadoID, glb_NewEstadoID);
            
        }
        else {
            window.top.overflyGen.Alert(sMensagem);
            stateMachSupExec('CANC');
        }
    }
    
     
}

function gravaRetorno() 
{
    var nSiscoservID = dsoSup01.recordset['SiscoservID'].value;

    var _retConf = window.top.overflyGen.Confirm('Deseja gravar o Retorno do SiscoservID ' + nSiscoservID + ' ?');
    if (_retConf == 0)
        return null;
    else if (_retConf == 1) {
        lockInterface(true);
        strPars = '?nSiscoServID=' + escape(nSiscoservID);
        strPars += '&nUserID=' + escape(getCurrUserID());

        dsoGravaRetorno.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/siscoserv/serverside/gravaretorno.aspx' + strPars;
        dsoGravaRetorno.ondatasetcomplete = gravaRetorno_DSC;
        dsoGravaRetorno.refresh();

        return true;
    }

}

function gravaRetorno_DSC() {
    var sResultado = dsoGravaRetorno.recordset['Resultado'].value;

    if (sResultado == "")
        avancaEstado(glb_CurrEstadoID, glb_NewEstadoID);
    else 
    {
        window.top.overflyGen.Alert(sResultado);
        lockInterface(false);
        stateMachSupExec('CANC');
    }
        
    
}
