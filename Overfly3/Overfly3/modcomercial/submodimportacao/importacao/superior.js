/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// controla o retorno do servidor dos dados de combos dinamicos
var glb_CounterCmbsDynamics = 0;

var dsoSup01 = new CDatatransport("dsoSup01");
var dsoStateMachine = new CDatatransport("dsoStateMachine");
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
var dsoCurrData = new CDatatransport("dsoCurrData");
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");
var dsoCmbDynamic02 = new CDatatransport("dsoCmbDynamic02");
var dsoCmbDynamic03 = new CDatatransport("dsoCmbDynamic03");
var dsoCmbDynamic04 = new CDatatransport("dsoCmbDynamic04");
var dsoCmbDynamic05 = new CDatatransport("dsoCmbDynamic05");
var dsoCmbDynamic06 = new CDatatransport("dsoCmbDynamic06");
var dsoCmbDynamic07 = new CDatatransport("dsoCmbDynamic07");
var dsoCmbDynamic08 = new CDatatransport("dsoCmbDynamic08");
var dsoCmbDynamic09 = new CDatatransport("dsoCmbDynamic09");
var dsoVerificacao = new CDatatransport("dsoVerificacao");
var dsoNotificacao = new CDatatransport("dsoNotificacao");

var ESTADO_A = 41;
var ESTADO_C = 1;
var ESTADO_O = 171;
var ESTADO_D = 172;
var ESTADO_E = 173;
var ESTADO_G = 174;
var ESTADO_F = 48;
var glb_Alteracao = false;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload() {
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);

    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    /***********************************************************************************************************************************************************************
    Combos Retirados BJBN Projeto de Importa��o:
    ['selUFDesembaraco', '1'], 
    ************************************************************************************************************************************************************************/
    glb_aStaticCombos = ([['selTipoFreteID', '2'], ['selMeioTransporteID', '3'], ['selTipoImportacao', '4'], ['selTratamentoCarga', '5'], ['selCanalDTA', 6], ['selCanalDIID', 6]]);

    windowOnLoad_1stPart();

    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modcomercial/submodimportacao/importacao/inferior.asp',
                              SYS_PAGESURLROOT + '/modcomercial/submodimportacao/importacao/pesquisa.asp');

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);

    windowOnLoad_2ndPart();

    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'ImportacaoID';

    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

function setupPage() {
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup', [[1, 'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 7, 1],
						['lblEstadoID', 'txtEstadoID', 2, 1],
                        ['lblProcesso', 'txtProcesso', 7, 1],
                        ['lblTipoImportacao', 'selTipoImportacao', 13, 1],
                        ['lblImportadorID', 'selImportadorID', 12, 1],
                        ['lblSituacao', 'txtSituacao', 45, 1],
                        ['lblTotalFOB', 'txtTotalFOB', 10, 1],
                        ['lblPrevisaoEntrega', 'txtPrevisaoEntrega', 12, 1, -13],
                        ['lblAtrasoEntrega', 'txtAtrasoEntrega', 5, 1, -3],
                        ['lblAgenteCargaID', 'selAgenteCargaID', 19, 2],
                        ['lblMeioTransporteID', 'selMeioTransporteID', 12, 2],
						['lblTipoFreteID', 'selTipoFreteID', 8, 2],
                        ['lblPortoOrigemID', 'selPortoOrigemID', 19, 2],
                        ['lblCompanhiaID', 'selCompanhiaID', 19, 2],
                        ['lblPortoDestinoID', 'selPortoDestinoID', 19, 2],
                        ['lblRecintoAduaneiroID', 'selRecintoAduaneiroID', 19, 2],
                        ['lblAutorizacaoEmbarque', 'txtAutorizacaoEmbarque', 10, 3],
                        ['lblDocumentoEmbarque', 'txtDocumentoEmbarque', 12, 3],
                        ['lbldtDocumentoEmbarque', 'txtdtDocumentoEmbarque', 10, 3],
                        ['lbldtEmbarque', 'txtdtEmbarque', 10, 3],
                        ['lbldtChegada', 'txtdtChegada', 10, 3],
                        ['lblContainers', 'txtContainers', 10, 3],
                        ['lblPalletes', 'txtPalletes', 10, 3],
                        ['lblCaixas', 'txtCaixas', 10, 3, -2],
                        ['lblTransportadoraDTAID', 'selTransportadoraDTAID', 19, 3],
                        ['lblTratamentoCarga', 'selTratamentoCarga', 10, 3],
                        ['lblDTA', 'txtDTA', 10, 4],
                        ['lblRegistroDTA', 'txtRegistroDTA', 10, 4],
                        ['lblCanalDTA', 'selCanalDTA', 10, 4],
                        ['lbldtLiberacaoDTA', 'txtdtLiberacaoDTA', 10, 4],
                        ['lblAdcTarifario', 'txtAdcTarifario', 10, 4],
                        ['lblNIC', 'txtNIC', 10, 4],
                        ['lbldtAutorizacao', 'txtdtAutorizacao', 10, 4],
                        ['lblDIID', 'txtDIID', 10, 4],
						['lblDtRegistro', 'txtDtRegistro', 10, 4],
                        ['lblCanalDIID', 'selCanalDIID', 11, 4, 1],
                        ['lblTaxaMoeda', 'txtTaxaMoeda', 9, 4],
                        ['lblDtDesembaraco', 'txtDtDesembaraco', 10, 5,0,-3],
                        ['lblDespachanteID', 'selDespachanteID', 18, 5],
                        ['lblProcDespachante', 'txtProcDespachante', 15, 5],
                        ['lblTransportadora', 'selTransportadora', 18, 5],
                        ['lblEntrega', 'txtEntrega', 10, 5],
                        ['lblLacre', 'txtLacre', 8, 5, 0],
                        ['lbldtEtiquetagem', 'txtdtEtiquetagem', 10, 5],
                        ['lblObservacao', 'txtObservacao', 25, 5, 0]], null, null, true);

    /*
                            ['lblRecintoAduaneiro', 'txtRecintoAduaneiro', 62, 2],
                            ['lblArmazem', 'txtArmazem', 18, 2, -4],
                            ['lblUFDesembaraco', 'selUFDesembaraco', 6, 2, -2],
                            ['lblFOB', 'txtFOB', 12, 3],
                            ['lblCapatazia', 'txtCapatazia', 10, 3],
                            ['lblFrete', 'txtFrete', 12, 3],
                            ['lblValorSeguro', 'txtValorSeguro', 12, 3],
                            ['lblValorCIF', 'txtValorCIF', 12, 3],
                            ['lblValorCIFReais', 'txtValorCIFReais', 12, 3],
                            ['lblValorII', 'txtValorII', 13, 4],
                            ['lblValorIOF', 'txtValorIOF', 13, 4],
                            ['lblDespesasAduaneiras', 'txtDespesasAduaneiras', 13, 4],
                            ['lblQuantidade', 'txtQuantidade', 8, 4, -10],
                            ['lblPesoLiquidoTotal', 'txtPesoLiquidoTotal', 15, 4],
                            ['lblObservacao', 'txtObservacao', 30, 4],
                            ['lblSituacao', 'txtSituacao', 30, 5, 0, -5],
                            ['lblTransportadora', 'selTransportadora', 20, 5],
                            ['lblEntrega', 'txtEntrega', 20, 5],
                            ['lblLacre', 'txtLacre', 20, 5, 0]], null, null, true);
    */
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {
    //@@
    var cmbID = '';
    var nTipoRelacaoID = '';
    var nRegExcluidoID = '';
    /*
    if ( btnClicked.id == 'btnFindImportador' )
    {
        nTipoRelacaoID = 21;
        nRegExcluidoID = '';
        showModalRelacoes(window.top.formID, 'S', 'selImportadorID' , 'IMPOSTOS', getLabelNumStriped(lblImportadorID.innerText) , nTipoRelacaoID, nRegExcluidoID);
        return null;
    }*/
}


// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2) {
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2) {

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@

    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.

}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked) {
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;

    if (btnClicked == 'SUPINCL') {
        txtProcesso.maxLength = 6;
        txtDocumentoEmbarque.maxLength = 12;
        txtDTA.maxLength = 10;
        txtProcDespachante.maxLength = 16;
        txtLacre.maxLength = 8;
        txtdtDocumentoEmbarque.maxLength = 10;
        txtdtEmbarque.maxLength = 10;
        txtdtChegada.maxLength = 10;
        txtDTA.maxLength = 10;
        txtdtLiberacaoDTA.maxLength = 10;
        txtNIC.maxLength = 10;
        txtdtAutorizacao.maxLength = 10;
        txtDtRegistro.maxLength = 10;
        txtDtDesembaraco.maxLength = 10;
        txtdtEtiquetagem.maxLength = 10;
        txtEntrega.maxLength = 10;
    }

    if ((btnClicked == 'SUPDET') ||
         (btnClicked == 'SUPREFR') ||
         (btnClicked == 'SUPANT') ||
         (btnClicked == 'SUPSEG')) {
        startDynamicCmbs();
    }
    else
        // volta para a automacao    
        finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs() {
    glb_CounterCmbsDynamics = 9;

    // parametrizacao do dso dsoCmbDynamic03 (designado para selImportadorID)
    setConnection(dsoCmbDynamic01);

    dsoCmbDynamic01.SQL = "SELECT PessoaID AS fldID, Fantasia AS fldName " +
		"FROM Pessoas WITH(NOLOCK) " +
		"WHERE PessoaID = " + dsoSup01.recordset["ImportadorID"].value;

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic3_DSC;
    dsoCmbDynamic01.refresh();

    setConnection(dsoCmbDynamic02);

    // selAgenteCargaID
    dsoCmbDynamic02.SQL = "SELECT PessoaID AS fldID, Fantasia AS fldName " +
		"FROM Pessoas WITH(NOLOCK) " +
		"WHERE PessoaID = " + dsoSup01.recordset["AgenteCargaID"].value;

    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic3_DSC;
    dsoCmbDynamic02.refresh();

    // selPortoOrigemID
    dsoCmbDynamic03.SQL = "SELECT PessoaID AS fldID, Fantasia AS fldName " +
		"FROM Pessoas WITH(NOLOCK) " +
		"WHERE PessoaID = " + dsoSup01.recordset["PortoOrigemID"].value;

    dsoCmbDynamic03.ondatasetcomplete = dsoCmbDynamic3_DSC;
    dsoCmbDynamic03.refresh();

    // selCompanhiaID
    dsoCmbDynamic04.SQL = "SELECT PessoaID AS fldID, Fantasia AS fldName " +
		"FROM Pessoas WITH(NOLOCK) " +
		"WHERE PessoaID = " + dsoSup01.recordset["CompanhiaID"].value;

    dsoCmbDynamic04.ondatasetcomplete = dsoCmbDynamic3_DSC;
    dsoCmbDynamic04.refresh();

    // selPortoDestinoID
    dsoCmbDynamic05.SQL = "SELECT PessoaID AS fldID, Fantasia AS fldName " +
		"FROM Pessoas WITH(NOLOCK) " +
		"WHERE PessoaID = " + dsoSup01.recordset["PortoDestinoID"].value;

    dsoCmbDynamic05.ondatasetcomplete = dsoCmbDynamic3_DSC;
    dsoCmbDynamic05.refresh();

    // selRecintoAduaneiroID
    dsoCmbDynamic06.SQL = "SELECT PessoaID AS fldID, Fantasia AS fldName " +
		"FROM Pessoas WITH(NOLOCK) " +
		"WHERE PessoaID = " + dsoSup01.recordset["RecintoAduaneiroID"].value;

    dsoCmbDynamic06.ondatasetcomplete = dsoCmbDynamic3_DSC;
    dsoCmbDynamic06.refresh();

    // selDespachanteID
    dsoCmbDynamic07.SQL = "SELECT PessoaID AS fldID, Fantasia AS fldName " +
		"FROM Pessoas WITH(NOLOCK) " +
		"WHERE PessoaID = " + dsoSup01.recordset["DespachanteID"].value;

    dsoCmbDynamic07.ondatasetcomplete = dsoCmbDynamic3_DSC;
    dsoCmbDynamic07.refresh();

    // selTransportadoraID
    dsoCmbDynamic08.SQL = "SELECT PessoaID AS fldID, Fantasia AS fldName " +
		"FROM Pessoas WITH(NOLOCK) " +
		"WHERE PessoaID = " + dsoSup01.recordset["TransportadoraID"].value;

    dsoCmbDynamic08.ondatasetcomplete = dsoCmbDynamic3_DSC;
    dsoCmbDynamic08.refresh();

    // selTransportadoraDTAID
    dsoCmbDynamic09.SQL = "SELECT PessoaID AS fldID, Fantasia AS fldName " +
		"FROM Pessoas WITH(NOLOCK) " +
		"WHERE PessoaID = " + dsoSup01.recordset["TransportadoraDTAID"].value;

    dsoCmbDynamic09.ondatasetcomplete = dsoCmbDynamic3_DSC;
    dsoCmbDynamic09.refresh();
}

/**********************************************************************
 *
 **********************************************************************/
function dsoCmbDynamic3_DSC() {
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selImportadorID, selAgenteCargaID, selPortoOrigemID, selCompanhiaID, selPortoDestinoID, selRecintoAduaneiroID, selDespachanteID, selTransportadora, selTransportadoraDTAID];
    var aDSOsDunamics = [dsoCmbDynamic01, dsoCmbDynamic02, dsoCmbDynamic03, dsoCmbDynamic04, dsoCmbDynamic05, dsoCmbDynamic06, dsoCmbDynamic07, dsoCmbDynamic08, dsoCmbDynamic09];

    // Inicia o carregamento de combos dinamicos (selImportadorID)
    clearComboEx(['selImportadorID', 'selAgenteCargaID', 'selPortoOrigemID', 'selCompanhiaID', 'selPortoDestinoID', 'selRecintoAduaneiroID', 'selDespachanteID', 'selTransportadora', 'selTransportadoraDTAID']);

    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics == 0) {
        for (i = 0; i < aCmbsDynamics.length; i++) {
            while (!aDSOsDunamics[i].recordset.EOF) {
                optionStr = aDSOsDunamics[i].recordset['fldName'].value;
                optionValue = aDSOsDunamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[i].add(oOption);
                aDSOsDunamics[i].recordset.MoveNext();
            }
        }

        setLabelOfControl(lblImportadorID, selImportadorID.value);
        setLabelOfControl(lblAgenteCargaID, selAgenteCargaID.value);
        setLabelOfControl(lblPortoOrigemID, selPortoOrigemID.value);
        setLabelOfControl(lblCompanhiaID, selCompanhiaID.value);
        setLabelOfControl(lblPortoDestinoID, selPortoDestinoID.value);
        setLabelOfControl(lblRecintoAduaneiroID, selRecintoAduaneiroID.value);
        setLabelOfControl(lblDespachanteID, selDespachanteID.value);
        setLabelOfControl(lblTransportadora, selTransportadora.value);
        setLabelOfControl(lblTransportadoraDTAID, selTransportadoraDTAID.value);

        // volta para a automacao
        finalOfSupCascade(glb_BtnFromFramWork);
    }
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked) {
    //@@


    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    // mostra quatro botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1]);
    // os hints dos botoes especificos
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimentos', 'Preencher Combos', 'Enviar Email', 'Rateios']);

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
    setReadOnlyFields();
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selTipoRegistroID')
        adjustSupInterface();

    if (cmbID == 'selTratamentoCarga')
        setReadOnlyFields();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK(currEstadoID, newEstadoID) {
    verifyImportacaoInServer(currEstadoID, newEstadoID);
    return true;
}

function verifyImportacaoInServer(currEstadoID, newEstadoID) {
    var nImportacaoID = dsoSup01.recordset['ImportacaoID'].value;
    var strPars = new String();

    strPars = '?nImportacaoID=' + escape(nImportacaoID);
    strPars += '&nCurrEstadoID=' + escape(currEstadoID);
    strPars += '&nNewEstadoID=' + escape(newEstadoID);
    strPars += '&nUserID=' + escape(getCurrUserID());

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/importacao/serverside/verificacaoimportacao.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyImportacaoInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes do Financeiro
********************************************************************/
function verifyImportacaoInServer_DSC() {
    var nResultado = dsoVerificacao.recordset['Resultado'].value;
    var sMensagem = dsoVerificacao.recordset['Mensagem'].value;

    if (nResultado == 0) {
        stateMachSupExec('CANC');
        window.top.overflyGen.Alert(sMensagem);
    }
    else
        stateMachSupExec('OK');
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID, bSavedID) {
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    var nImportacaoID = dsoSup01.recordset['ImportacaoID'].value;

    // Documentos
    if ((controlBar == 'SUP') && (btnClicked == 1)) {
        __openModalDocumentos(controlBar, nImportacaoID);
    }
        // usuario clicou botao imprimir
    else if ((controlBar == 'SUP') && (btnClicked == 2))
        openModalPrint();

    else if ((controlBar == 'SUP') && (btnClicked == 4))
        openModalPreencheCombos();

    else if ((controlBar == 'SUP') && (btnClicked == 5))
        Notificacao();

    else if ((controlBar == 'SUP') && (btnClicked == 6))
        openModalRateio();
}
/*******************************************************************
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    var nEmpresaID = getCurrEmpresaData();

    if (btnClicked == 'SUPOK') {
        // Se e um novo registro
        if (dsoSup01.recordset[glb_sFldIDName] == null || dsoSup01.recordset[glb_sFldIDName].value == null)
            dsoSup01.recordset['EmpresaID'].value = nEmpresaID[0];
    }
    else if (btnClicked == 'SUPINCL') {
        clearComboEx(['selImportadorID']);
    }

    if ((btnClicked == 'SUPALT') || (btnClicked == 'SUPINCL'))
        glb_Alteracao = true;
    else
        glb_Alteracao = false;


    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Modal de relacoes
    if (idElement.toUpperCase() == 'MODALRATEIOSHTML')
    {
        if (param1 == 'OK')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

    if (idElement.toUpperCase() == 'MODALPREENCHECOMBOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            setLabelOfControl(lblImportadorID, selImportadorID.value);
            setLabelOfControl(lblAgenteCargaID, selAgenteCargaID.value);
            setLabelOfControl(lblPortoOrigemID, selPortoOrigemID.value);
            setLabelOfControl(lblCompanhiaID, selCompanhiaID.value);
            setLabelOfControl(lblPortoDestinoID, selPortoDestinoID.value);
            setLabelOfControl(lblRecintoAduaneiroID, selRecintoAduaneiroID.value);
            setLabelOfControl(lblDespachanteID, selDespachanteID.value);
            setLabelOfControl(lblTransportadora, selTransportadora.value);

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

    // Modal de impressao
    if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao criada pelo programador.
Preenche os campos do subform corrente, com os dados vindos da janela
modal de relacoes

Parametro:
aData   -   array de dados
            aData[0] - quem e na relacao ('SUJ' ou 'OBJ')
            aData[1] - ID do tipo da relacao
            aData[2] - ID do campo do subform corrente
            aData[3] - ID selecionado no grid da modal
            aData[4] - texto correspondente ao ID selecionado no grid da modal
            
            os demais elementos sao opcionais

Retorno:
nenhum
********************************************************************/
function fillFieldsByRelationModal(aData) {
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var elem;
    var addSexOrServ = null;

    elem = window.document.getElementById(aData[2]);

    if (elem == null)
        return;

    clearComboEx([elem.id]);

    if (aData[2] == 'selImportadorID') {
        dsoSup01.recordset['ImportadorID'].value = aData[3];
    }

    oOption = document.createElement("OPTION");
    oOption.text = aData[4];
    oOption.value = aData[3];
    elem.add(oOption);
    elem.selectedIndex = 0;
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode() {
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();

    //selImportadorID
    setReadOnlyFields();


    // AQUI !!!!

    if (glb_Alteracao)
        setupEspecBtnsControlBar('sup', 'DDDHDD');

}

function setReadOnlyFields()
{
    var currEstadoID = dsoSup01.recordset["EstadoID"].value;
    desabilitaCamposDTA();

    txtProcesso.maxLength = 6;
    txtDocumentoEmbarque.maxLength = 12;
    txtDTA.maxLength = 10;
    txtProcDespachante.maxLength = 16;
    txtLacre.maxLength = 8;
    txtdtDocumentoEmbarque.maxLength = 10;
    txtdtEmbarque.maxLength = 10;
    txtdtChegada.maxLength = 10;
    txtDTA.maxLength = 10;
    txtdtLiberacaoDTA.maxLength = 10;
    txtNIC.maxLength = 10;
    txtdtAutorizacao.maxLength = 10;
    txtDtRegistro.maxLength = 10;
    txtDtDesembaraco.maxLength = 10;
    txtdtEtiquetagem.maxLength = 10;
    txtEntrega.maxLength = 10;
    txtAtrasoEntrega.maxLength = 5;

    txtAtrasoEntrega.readOnly = false;

    txtRegistroID.readOnly = true;
    txtEstadoID.readOnly = true;
    txtSituacao.readOnly = true;
    txtTotalFOB.readOnly = true;

    txtProcesso.readOnly = false;
    selTipoImportacao.readOnly = false;
    selImportadorID.readOnly = false;
    selAgenteCargaID.readOnly = false;
    selMeioTransporteID.readOnly = false;
    selTipoFreteID.readOnly = false;
    selPortoOrigemID.readOnly = false;
    selCompanhiaID.readOnly = false;
    selPortoDestinoID.readOnly = false;
    selRecintoAduaneiroID.readOnly = false;
    txtAutorizacaoEmbarque.readOnly = false;
    txtDocumentoEmbarque.readOnly = false;
    txtdtDocumentoEmbarque.readOnly = false;
    txtdtEmbarque.readOnly = false;
    txtdtChegada.readOnly = false;
    txtContainers.readOnly = false;
    txtPalletes.readOnly = false;
    txtCaixas.readOnly = false;
    selTransportadoraDTAID.readOnly = false;
    selTratamentoCarga.readOnly = false;
    txtDTA.readOnly = false;
    txtRegistroDTA.readOnly = false;
    selCanalDTA.readOnly = false;
    txtdtLiberacaoDTA.readOnly = false;
    txtNIC.readOnly = false;
    txtdtAutorizacao.readOnly = false;
    txtDIID.readOnly = false;
    txtDtRegistro.readOnly = false;
    selCanalDIID.readOnly = false;
    txtTaxaMoeda.readOnly = false;
    txtDtDesembaraco.readOnly = false;
    selDespachanteID.readOnly = false;
    txtProcDespachante.readOnly = false;
    txtdtEtiquetagem.readOnly = false;
    selTransportadora.readOnly = false;
    txtEntrega.readOnly = false;
    txtLacre.readOnly = false;

    if (currEstadoID == ESTADO_D) {
        txtProcesso.readOnly = true;
        selTipoImportacao.readOnly = true;
        selImportadorID.readOnly = true;
        selAgenteCargaID.readOnly = true;
        selMeioTransporteID.readOnly = true;
        selTipoFreteID.readOnly = true;
        selPortoOrigemID.readOnly = true;
        selCompanhiaID.readOnly = true;
        selPortoDestinoID.readOnly = true;
        txtAutorizacaoEmbarque.readOnly = true;
        txtDocumentoEmbarque.readOnly = true;
        txtdtDocumentoEmbarque.readOnly = true;
        txtdtEmbarque.readOnly = true;
    }

    if (currEstadoID == ESTADO_E) {
        txtProcesso.readOnly = true;
        selTipoImportacao.readOnly = true;
        selImportadorID.readOnly = true;
        selAgenteCargaID.readOnly = true;
        selMeioTransporteID.readOnly = true;
        selTipoFreteID.readOnly = true;
        selPortoOrigemID.readOnly = true;
        selCompanhiaID.readOnly = true;
        selPortoDestinoID.readOnly = true;
        txtAutorizacaoEmbarque.readOnly = true;
        txtDocumentoEmbarque.readOnly = true;
        txtdtDocumentoEmbarque.readOnly = true;
        txtdtEmbarque.readOnly = true;
        selRecintoAduaneiroID.readOnly = true;
        txtdtChegada.readOnly = true;
        txtContainers.readOnly = true;
        txtPalletes.readOnly = true;
        txtCaixas.readOnly = true;
        selTransportadoraDTAID.readOnly = true;
        selTratamentoCarga.readOnly = true;
        txtDTA.readOnly = true;
        txtRegistroDTA.readOnly = true;
        selCanalDTA.readOnly = true;
        txtdtLiberacaoDTA.readOnly = true;
    }

    if (currEstadoID == ESTADO_G) {
        txtProcesso.readOnly = true;
        selTipoImportacao.readOnly = true;
        selImportadorID.readOnly = true;
        selAgenteCargaID.readOnly = true;
        selMeioTransporteID.readOnly = true;
        selTipoFreteID.readOnly = true;
        selPortoOrigemID.readOnly = true;
        selCompanhiaID.readOnly = true;
        selPortoDestinoID.readOnly = true;
        txtAutorizacaoEmbarque.readOnly = true;
        txtDocumentoEmbarque.readOnly = true;
        txtdtDocumentoEmbarque.readOnly = true;
        txtdtEmbarque.readOnly = true;
        selRecintoAduaneiroID.readOnly = true;
        txtdtChegada.readOnly = true;
        txtContainers.readOnly = true;
        txtPalletes.readOnly = true;
        txtCaixas.readOnly = true;
        selTransportadoraDTAID.readOnly = true;
        selTratamentoCarga.readOnly = true;
        txtDTA.readOnly = true;
        txtRegistroDTA.readOnly = true;
        selCanalDTA.readOnly = true;
        txtdtLiberacaoDTA.readOnly = true;
        txtNIC.readOnly = true;
        txtdtAutorizacao.readOnly = true;
        txtDIID.readOnly = true;
        txtDtRegistro.readOnly = true;
        selCanalDIID.readOnly = true;
        txtTaxaMoeda.readOnly = true;
        txtDtDesembaraco.readOnly = true;
        selDespachanteID.readOnly = true;
        txtProcDespachante.readOnly = true;
        txtdtEtiquetagem.readOnly = true;
        selTransportadora.readOnly = true;
        txtEntrega.readOnly = true;
        txtLacre.readOnly = true;
    }

    /*
    // Campos que sempre s�o travados.
	txtFOB.readOnly = true;
	txtValorCIF.readOnly = true;
	txtValorCIFReais.readOnly = true;
	txtValorII.readOnly = true;
	txtPesoLiquidoTotal.readOnly = true;
	txtQuantidade.readOnly = true;


	
    // N�o permite alterar o exportador, o importador e o DI se o
    // estado do registro for A.

	if (currEstadoID == ESTADO_C)
    {
		selImportadorID.disabled = false;
		btnFindImportador.disabled = false;
		txtDIID.readOnly = false;
		txtDtRegistro.readOnly = false;
		txtDtDesembaraco.readOnly = false;
		txtRecintoAduaneiro.readOnly = false;
		txtArmazem.readOnly = false;
		selUFDesembaraco.disabled = false;
		selMeioTransporteID.disabled = false;		
		selTipoImportacao.disabled = false;
		txtValorAFRMM.disabled = false;
		selTipoFreteID.disabled = false;
		txtFrete.disabled = false;
		txtValorSeguro.readOnly = false;
		txtTaxaMoeda.readOnly = false;
		txtValorIOF.readOnly = false;
		txtDespesasAduaneiras.readOnly = false;
		txtQuantidade.readOnly = false;
		txtObservacao.readOnly = false;
    } 
    else
    {
		selImportadorID.disabled = true;
		btnFindImportador.disabled = true;
		txtDIID.readOnly = true;
		txtDtRegistro.readOnly = true;
		txtDtDesembaraco.readOnly = true;
		txtRecintoAduaneiro.readOnly = true;
		txtArmazem.readOnly = true;
		selUFDesembaraco.disabled = true;
		selMeioTransporteID.disabled = true;
		selTipoImportacao.disabled = true;
		txtValorAFRMM.disabled = true;
		selTipoFreteID.disabled = true;
		txtFrete.disabled = true;
		txtValorSeguro.readOnly = true;
		txtTaxaMoeda.readOnly = true;
		txtValorIOF.readOnly = true;
		txtDespesasAduaneiras.readOnly = true;
		txtQuantidade.readOnly = true;
		txtObservacao.readOnly = true;
    } 
    */
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function desabilitaCamposDTA() {

    var desabilitaCamposDTA = false;

    if (selTratamentoCarga.value == 1387)
        desabilitaCamposDTA = true;

    selTransportadoraDTAID.disabled = desabilitaCamposDTA;
    selCanalDTA.disabled = desabilitaCamposDTA;
    txtDTA.disabled = desabilitaCamposDTA;
    txtRegistroDTA.disabled = desabilitaCamposDTA;
    txtdtLiberacaoDTA.disabled = desabilitaCamposDTA;
    lblDTA.disabled = desabilitaCamposDTA;
    lblRegistroDTA.disabled = desabilitaCamposDTA;
    lblCanalDTA.disabled = desabilitaCamposDTA;
    lbldtLiberacaoDTA.disabled = desabilitaCamposDTA;
    lblTransportadoraDTAID.disabled = desabilitaCamposDTA;

    if (desabilitaCamposDTA) {
    selTransportadoraDTAID.value = 0;
    selCanalDTA.value = 0;
    txtDTA.value = '';
    txtRegistroDTA.value = '';
    txtdtLiberacaoDTA.value = '';
    setLabelOfControl(lblTransportadoraDTAID, selTransportadoraDTAID.value);
    }

}

function formFinishLoad() {
    // mostra quatro botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1]);
    // os hints dos botoes especificos
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimentos', 'Preencher Combos', 'Enviar Email', 'Rateios']);
    //setupEspecBtnsControlBar('sup', 'HHHH');

}

function openModalPreencheCombos() {
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodImportacao/Importacao/modalpages/modalPreencheCombos.asp';
    showModalWin(htmlPath, new Array(500, 450));
}

function openModalRateio()
{
    var htmlPath;
    var strPars = new String();
    var nImportacaoID = 0;

    nImportacaoID = dsoSup01.recordset['ImportacaoID'].value;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');
    strPars += '&nImportacaoID=' + escape(nImportacaoID);

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodImportacao/Importacao/modalpages/modalRateios.asp' + strPars;
    showModalWin(htmlPath, new Array(995, 532));
}


/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    var ImportacaoID = dsoSup01.recordset['ImportacaoID'].value;
    var ProcessoImportacao = dsoSup01.recordset['ProcessoImportacao'].value;

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&nImportacaoID=' + escape(ImportacaoID);
    strPars += '&sProcessoImportacao=' + escape(ProcessoImportacao);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodimportacao/importacao/modalpages/modalprint.asp' + strPars;
    showModalWin(htmlPath, new Array(346, 200));
}

function modalCombosPreenche(Combo, fldID, fldName) {
    clearComboEx([Combo.id]);

    var oOption = document.createElement("OPTION");
    oOption.text = fldName;
    oOption.value = fldID;
    Combo.add(oOption);
}

function Notificacao() {
    var nImportacaoID = dsoSup01.recordset['ImportacaoID'].value;

    setConnection(dsoNotificacao);

    dsoNotificacao.SQL = 'SELECT dbo.fn_Importacao_Destinatario(' + nImportacaoID + ', 2) AS Para, ' +
		                    'dbo.fn_Importacao_Destinatario(' + nImportacaoID + ', 4) AS CC, ' +
		                    'dbo.fn_Importacao_Destinatario(' + nImportacaoID + ', 5) AS Cco, ' +
		                    'dbo.fn_Importacao_Destinatario(' + nImportacaoID + ', 6) AS Anexos, ' +
                            'dbo.fn_Importacao_Notificacao(' + nImportacaoID + ', 1) AS Assunto, ' +
                            'dbo.fn_Importacao_Notificacao(' + nImportacaoID + ', 2) AS Corpo';

    dsoNotificacao.ondatasetcomplete = Notificacao_DSC;
    dsoNotificacao.Refresh();
}

function Notificacao_DSC() {
    var aAnexos = new Array();

    var nProcessoImportacao = dsoSup01.recordset['ProcessoImportacao'].value;

    var Para;
    var CC;
    var Cco;
    var Anexos;
    var Assunto;
    var Corpo;
    var theApp;
    var theMailItem;

    if (!(dsoNotificacao.recordset.BOF || dsoNotificacao.recordset.EOF)) {

        Para = dsoNotificacao.recordset['Para'].value;
        CC = dsoNotificacao.recordset['CC'].value;
        Cco = dsoNotificacao.recordset['Cco'].value;
        Anexos = dsoNotificacao.recordset['Anexos'].value;
        Assunto = dsoNotificacao.recordset['Assunto'].value;
        Corpo = dsoNotificacao.recordset['Corpo'].value;

        try
        {
            aAnexos = Anexos.split(',');
        }
        catch (ex)
        {
            ;
        }

        try
        {
            var theApp = new ActiveXObject("Outlook.Application");

            var theMailItem = theApp.CreateItem(0);

            theMailItem.to = Para;

            theMailItem.cc = CC;

            theMailItem.Subject = (Assunto);

            theMailItem.HtmlBody = (Corpo);

            for (i = 0; i < aAnexos.length; i++)
            {
                try
                {
                    theMailItem.Attachments.Add("/" + "/172.16.2.3/1 Empresa/1 Departamentos/3 Opera��es/2 Importa��o/1 Importa��o/Alcateia/Documentos de Importa��o/" + nProcessoImportacao + "/" + aAnexos[i]);
                }
                catch (e)
                {
                    alert('Problema ao anexar ' + aAnexos[i] + '.\n' + e.message);
                }
            }

            theMailItem.display();
        }

        catch (err) {

            alert('Erro ao enviar email. \n' + err.message);
        }
    }
    else
        alert('N�o h� notifica��es.');

}