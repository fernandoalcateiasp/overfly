/********************************************************************
especificinf.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID) {
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    //@@
    if ((folderID == 20008) || (folderID == 20009)) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.ImportacaoID, a.ProprietarioID, a.AlternativoID, a.Observacoes FROM Importacao a WITH(NOLOCK) WHERE ' +
			sFiltro + 'a.ImportacaoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
        // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.RegistroID = ' + idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
        // Pedidos
    else if (folderID == 24301) {
        dso.SQL =
			"SELECT i.* " +
			"FROM " +
				"Importacao_Pedidos i WITH(NOLOCK) " +
			"WHERE i.ImportacaoID = " + idToFind + sFiltro;

        return 'dso01Grid_DSC';
    }

        // Adi��es
    else if (folderID == 24302)
    {
        dso.SQL =
        "SELECT a.*, " +
			    "b.TotalII AS TotalII, " +
			    "b.Peso AS Peso, " +
                "b.ValorTotal AS ValorTotal " +
			"FROM " +
				"Importacao_Adicoes a WITH(NOLOCK) " +
					"LEFT JOIN (SELECT ImpAdicaoID, " +
									"SUM(dbo.fn_Importacao_Item_II(ImpAdicaoItemID, 2) * Quantidade) AS TotalII, " +
									"SUM(PesoLiquido * Quantidade)  AS Peso, " +
									"SUM(ValorUnitario * Quantidade) AS ValorTotal " +
								"FROM Importacao_Adicoes_Itens WITH(NOLOCK) " +
								"WHERE ImportacaoID = " + idToFind + " " +
								"GROUP BY ImpAdicaoID) b ON a.ImpAdicaoID = b.ImpAdicaoID " +
			"WHERE " +
			    sFiltro + "a.ImportacaoID = " + idToFind + " " +
			"ORDER BY a.Adicao";

        return 'dso01Grid_DSC';
    }
        // Invoices
    else if (folderID == 24304)
    {
        dso.SQL =
			"SELECT a.InvoiceID, a.Invoice, b.Fantasia AS Exportador, a.ValorTotalInvoice AS ValorTotal, a.TaxaMoeda " +
            " FROM Invoices a WITH(NOLOCK) " +
            " INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ExportadorID) " +
			"WHERE a.ImportacaoID = " + idToFind + sFiltro;

        return 'dso01Grid_DSC';
    }
        // Custos
    else if (folderID == 24305)
    {
        dso.SQL =
		    "SELECT a.PedidoID, a.FinanceiroID, a.ImpCustoID, a.ImportacaoID, a.TipoCustoID, a.Descricao, a.Valor, a.Observacao " +
                "FROM Importacao_Custos a WITH(NOLOCK) " +
                "WHERE a.ImportacaoID = " + idToFind + sFiltro + " " +
                "ORDER BY a.TipoCustoID ASC ";

        return 'dso01Grid_DSC';
    }
     // Financeiros
    else if (folderID == 24306) {
        dso.SQL =
		    "SELECT b.FinanceiroID , c.RecursoAbreviado AS Est, CONVERT(VARCHAR(40), d.PessoaID) + ' - ' + d.Fantasia AS Pessoa, CONVERT(VARCHAR(10), b.dtEmissao, 103) AS Emissao, " +
		        "CONVERT(VARCHAR(10), b.dtVencimento, 103) AS Vencimento, CONVERT(VARCHAR(40), e.HistoricoPadraoID) + ' - ' +  e.HistoricoPadrao AS HP, b.Valor, b.TaxaMoeda " +
                "FROM Importacao_Custos a WITH(NOLOCK) " +
                    "INNER JOIN Financeiro b WITH(NOLOCK) ON b.FinanceiroID = a.FinanceiroID " +
                    "INNER JOIN Recursos c WITH(NOLOCK) ON  c.RecursoID = b.EstadoID " +
                    "INNER JOIN Pessoas d WITH(NOLOCK) ON  d.PessoaID = b.PessoaID " +
                    "INNER JOIN HistoricosPadrao e WITH(NOLOCK) ON e.HistoricoPadraoID = b.HistoricoPadraoID " +
                "WHERE a.ImportacaoID = " + idToFind + sFiltro + " " +
                "ORDER BY b.FinanceiroID ";

        return 'dso01Grid_DSC';
    }

}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID) {
    setConnection(dso);
    var aEmpresaID = getCurrEmpresaData();

    // LOG
    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
    }
        // CAMPO Pedidos
    else if (pastaID == 24301) {

        // Campo Pedidos.
        if (dso.id == 'dso01GridLkp') {
            

            dso.SQL = "SELECT a.ImpPedidoID, a.PedidoID AS PedidoID, " +
				"CONVERT(VARCHAR(10), b.dtPedido, " + DATE_SQL_PARAM + ") as DataPedido, " +
				"f.RecursoAbreviado as Estado, " +
				"d.Fantasia as NomePessoa, " +
				"c.Fantasia as NomeVendedor, " +
				"g.NotaFiscal, " +
                "i.Operacao AS Transacao, " +
				"CONVERT(VARCHAR(10), g.dtNotaFiscal, " + DATE_SQL_PARAM + ") as DataNota, " +
				"h.SimboloMoeda, " +
				"b.ValorTotalPedido, " +
				"e.Fantasia as NomeEmpresa, " +
				"b.Observacao, " +
				"g.NotaFiscalID AS NotaFiscalID " +
			"FROM " +
				"Importacao_Pedidos a WITH(NOLOCK) " +
				"INNER JOIN Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) " +
				"INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = b.ProprietarioID) " +
				"INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = b.PessoaID) " +
				"INNER JOIN Pessoas e WITH(NOLOCK) ON (e.PessoaID = b.EmpresaID) " +
				"INNER JOIN Recursos f WITH(NOLOCK) ON (f.RecursoID = b.EstadoID) " +
				"INNER JOIN Conceitos h WITH(NOLOCK) ON (h.ConceitoID = b.MoedaID) " +
				"LEFT OUTER JOIN NotasFiscais g WITH(NOLOCK) ON (g.NotaFiscalID = b.NotaFiscalID AND g.EstadoID=67) " +
                "INNER JOIN Operacoes i WITH(NOLOCK) ON (i.OperacaoID = b.TransacaoID) " +
			"WHERE a.ImportacaoID = " + nRegistroID + " " +
			"ORDER BY b.dtPedido";
        }
    }
    else if (pastaID == 24305)
    {
        // Campo Despesas.
        if (dso.id == 'dsoCmb01Grid_01')
        {

            dso.SQL = "SELECT a.ItemID, a.ItemMasculino " +
                    "  FROM  TiposAuxiliares_Itens a WITH(NOLOCK) " +
                    "  WHERE a.TipoID = 439" +
                    " ORDER BY a.Ordem ";
        }

        if (dso.id == 'dso02GridLkp')
        {
            dso.SQL = "SELECT a.TipoCustoID AS TipoCustoID, SUM(a.Valor) AS TotalCustos " +
                        "FROM Importacao_Custos a WITH(NOLOCK) " +
                        "WHERE (a.ImportacaoID =  " + nRegistroID + ") " +
                        "GROUP BY a.TipoCustoID";
        }

        if (dso.id == 'dsoCmb01Grid_02')
        {

            dso.SQL = 'SELECT \'\' AS Descricao ' +
                      'UNION ALL ' +
                      'SELECT DISTINCT REPLACE(REPLACE(LTRIM(RTRIM(a.Descricao)), CHAR(13), \'\'), CHAR(10), \'\') AS Descricao ' +
                        'FROM Importacao_Custos a WITH(NOLOCK) ' +
                        'WHERE (a.Descricao IS NOT NULL) ' +
	                    'ORDER BY Descricao';
        }
    }

    // Adi��es
    else if (pastaID == 24302) {
        // Campo Adi��o.
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL =
				"SELECT " +
					"ROW_NUMBER() OVER (ORDER BY ImpadicaoID DESC ) AS Adicao " +
				"FROM " +
					"Importacao_Adicoes WITH(NOLOCK) " +
				"WHERE " +
					"ImportacaoID = " + nRegistroID;
        }

            // Campo Fabricante.
        else if (dso.id == 'dso01GridLkp') {
            dso.SQL =
				"SELECT DISTINCT a.FabricanteID, b.Fantasia as Nome " +
				"FROM Importacao_Adicoes a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " +
				"WHERE a.ImportacaoID = " + nRegistroID + " AND " +
					"b.PessoaID = a.FabricanteID";
        }
            // Campo Classifica��o Fiscal.
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL =
				"SELECT " +
					"Conceitos.ConceitoID, " +
					"Conceitos.ClassificacaoFiscal " +
				"FROM " +
					"Conceitos WITH(NOLOCK) " +
					"INNER JOIN Importacao_Adicoes WITH(NOLOCK) ON (Importacao_Adicoes.ClassificacaoFiscalID = Conceitos.ConceitoID) " +
				"WHERE " +
					"Importacao_Adicoes.ImportacaoID = " + nRegistroID;
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID) {
    var i = 0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

    cleanupSelInControlBar('inf', 1);

    vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20008); // Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20009); // Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20010); // LOG

    vPastaName = window.top.getPastaName(24304);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24304); // Invoices

    vPastaName = window.top.getPastaName(24305);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24305); // Custos

    vPastaName = window.top.getPastaName(24302);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24302); // Adi��es

    vPastaName = window.top.getPastaName(24301);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24301); // Pedidos

    vPastaName = window.top.getPastaName(24306);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24306); // Financeiros

    // Nao mexer - Inicio de automacao ============================== 	
    // Tenta selecionar novamente a pasta anterior selecionada no combo
    // Se nao mais existe a pasta anteriormente selecionada no combo
    // seleciona a pasta default

    stripFoldersByRight(currSelection);

    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID) {
    var currLine = -1;

    //@@ return true se tudo ok, caso contrario return false;
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == PASTA_ADICOES) {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['ImportacaoID', registroID], []);
        }
        else if (folderID == PASTA_PEDIDOS) {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['ImportacaoID', registroID], []);
        }
        else if (folderID == PASTA_DESPESAS) {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['ImportacaoID', registroID], []);
        }

        if (currLine < 0) {
            if (fg.disabled == false) {
                try {
                    fg.Editable = true;
                    window.focus();
                    fg.focus();
                }
                catch (e) {
                    ;
                }
            }

            return false;
        }
    }

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO) {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.FrozenCols = 0;

    // LOG
    if (folderID == 20010) {
        headerGrid(fg, ['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);

        fillGridMask(fg, currDSO, ['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['', '99/99/9999', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm:ss', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }

        // Pedidos
    else if (folderID == 24301) {
        headerGrid(fg, ['Data',
                       'Pedido',
					   'Est',
					   'Pessoa',
					   'Nota',
					   'Data Nota',
                       'Transa��o',
                       'Moeda',
                       'Total',
                       'Colaborador',
                       'Empresa',
                       'Observacao',
                       'NotaFiscalID',
                       'ImpPedidoID'], [12, 13]);

        fillGridMask(fg, currDSO, ['^ImpPedidoID^dso01GridLkp^ImpPedidoID^DataPedido',
								'PedidoID',
								'^ImpPedidoID^dso01GridLkp^ImpPedidoID^Estado',
								'^ImpPedidoID^dso01GridLkp^ImpPedidoID^NomePessoa',
								'^ImpPedidoID^dso01GridLkp^ImpPedidoID^NotaFiscal',
								'^ImpPedidoID^dso01GridLkp^ImpPedidoID^DataNota',
                                '^ImpPedidoID^dso01GridLkp^ImpPedidoID^Transacao',
                                '^ImpPedidoID^dso01GridLkp^ImpPedidoID^SimboloMoeda',
								'^ImpPedidoID^dso01GridLkp^ImpPedidoID^ValorTotalPedido',
								'^ImpPedidoID^dso01GridLkp^ImpPedidoID^NomeVendedor',
								'^ImpPedidoID^dso01GridLkp^ImpPedidoID^NomeEmpresa',
								'^ImpPedidoID^dso01GridLkp^ImpPedidoID^Observacao',
								'^ImpPedidoID^dso01GridLkp^ImpPedidoID^NotaFiscalID',
								'ImpPedidoID'],
								 ['', '', '', '', '', '', '', '', '999999999.99', '', '', '', '', ''],
                                 ['', '##########', '', '', '', '######', '', '', '###,###,##0.00', '', '', '', '', '']);
    }

        // Adicoes
    else if (folderID == 24302) {
        var e;

        headerGrid(fg, ['Adi��o',
                       'Fabricante',
					   'NCM',
					   'Al�quota II',
					   'Total II',
					   'Peso Liquido',
					   'Valor Total',
					   'Valor Desconto',
					   'ImpAdicaoID'], [8]);

        fillGridMask(fg, currDSO, ['Adicao*',
								'^FabricanteID^dso01GridLkp^FabricanteID^Nome*',
								'^ClassificacaoFiscalID^dso02GridLkp^ConceitoID^ClassificacaoFiscal*',
								'AliquotaII*',
								'TotalII*',
								'Peso*',
								'ValorTotal*',
								'ValorDesconto*',
								'ImpAdicaoID'],
								 ['', '', '', '999.99', '999999999.99', '9999,999999999', '999999999.99', '999999999.99', ''],
								 ['', '', '', '##0.00', '###,###,##0.00', '####,#########0.000000000', '###,###,##0.00', '###,###,##0.00', '']);
    }

        // Pedidos
    else if (folderID == 24304) {
        headerGrid(fg, ['InvoiceID',
                        'Invoice',
                       'Exportador',
					   'Valor Total',
					   'Taxa Moeda'], []);

        fillGridMask(fg, currDSO, ['InvoiceID', 'Invoice', 'Exportador', 'ValorTotal', 'TaxaMoeda'],
								 ['', '', '', '999999999.99', '999999999.99'],
                                 ['', '', '', '###,###,##0.00', '###,###,##0.00']);
    }

     // Custos
    else if (folderID == 24305) {
        headerGrid(fg, ['Pedido',
                        'Financeiro',                       
                        'Tipo Custo',
                        'Total',
                        'Descri��o',
					    'Valor',
                        'Observa��o',
                        'ImpCustoID'], [7]);

        fillGridMask(fg, currDSO, ['PedidoID',
                                   'FinanceiroID',
                                   'TipoCustoID',
                                   '^TipoCustoID^dso02GridLkp^TipoCustoID^TotalCustos*',
                                   'Descricao',
                                   'Valor',
                                   'Observacao',
                                   'ImpCustoID'],
								 ['', '', '', '999999999.99', '', '999999999.99', '', ''],
                                 ['', '', '', '###,###,##0.00', '', '###,###,##0.00', '', '']);

        fg.MergeCells = 3;
        fg.MergeCol(2) = true;
        fg.MergeCol(3) = true;
    }
    // Financeiros
    else if (folderID == 24306) {
        headerGrid(fg, ['Financeiro',
                        'Est',
                        'Pessoa',
                        'Emiss�o',
                        'Vencimento',
					    'Hist�rico Padr�o',
                        'Valor',
                        'Taxa'], []);

        fillGridMask(fg, currDSO, ['FinanceiroID*',
                                   'Est*',
                                   'Pessoa*',
                                   'Emissao*',
                                   'Vencimento*',
                                   'HP*',
                                   'Valor*',
                                   'TaxaMoeda*'],
								 ['', '', '', '999999999.99', '', '999999999.99', '', ''],
                                 ['', '', '', '###,###,##0.00', '', '###,###,##0.00', '', '']);

        fg.MergeCells = 3;
        fg.MergeCol(2) = true;
        fg.MergeCol(3) = true;
    }

}
