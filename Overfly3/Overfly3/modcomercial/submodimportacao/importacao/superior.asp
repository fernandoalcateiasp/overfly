<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="visitassup01Html" name="visitassup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodimportacao/importacao/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodimportacao/importacao/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
	Dim strSQL, rsData
	Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
	Response.Write vbcrlf
	
	Response.Write "glb_nIdiomaID = 0;"
	Response.Write vbcrlf
	
	strSQL = "SELECT IdiomaID FROM Recursos WITH(NOLOCK) WHERE (RecursoID = 999)"
	
	Set rsData = Server.CreateObject("ADODB.Recordset")         
	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (NOT (rsData.BOF AND rsData.EOF)) Then
		Response.Write "glb_nIdiomaID = " & CStr(rsData.Fields("IdiomaID").Value) & ";"
	End If
	
	rsData.Close()
	Set rsData = Nothing
	Response.Write "</script>"
	Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--
//-->
</script>

</head>

<!-- //@@ -->
<body id="importacaosup01Body" name="importacaosup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral"/>
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->

        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral"/>
        
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" toread class="fldGeneral"/>

        <p id="lblProcesso" name="lblProcesso" class="lblGeneral">Processo</p>
        <input type="text" id="txtProcesso" name="txtProcesso" DATASRC="#dsoSup01" DATAFLD="ProcessoImportacao" class="fldGeneral"/>

        <p id="lblTipoImportacao" name="lblTipoImportacao" class="lblGeneral">Tipo Importa��o</p>        
        <select id="selTipoImportacao" name="selTipoImportacao" DATASRC="#dsoSup01" DATAFLD="TipoImportacaoID" class="fldGeneral"></select>
        
		<p id="lblImportadorID" name="lblImportadorID" class="lblGeneral">Importador</p>
		<select id="selImportadorID" name="selImportadorID" DATASRC="#dsoSup01" DATAFLD="ImportadorID" class="fldGeneral"></select>
		
	
        <p id="lblSituacao" name="lblSituacao" class="lblGeneral">Situa��o</p>
        <input type="text" id="txtSituacao" name="txtSituacao" DATASRC="#dsoSup01" DATAFLD="Situacao" class="fldGeneral"/>

        <p id="lblTotalFOB" name="lblTotalFOB" class="lblGeneral">Total FOB (US$)</p>
        <input type="text" id="txtTotalFOB" name="txtTotalFOB" DATASRC="#dsoSup01" DATAFLD="TotalFOB" class="fldGeneral"/>

		<p id="lblPrevisaoEntrega" name="lblPrevisaoEntrega" class="lblGeneral" title="Previs�o Entrega">Prev Entrega</p>
        <input type="text" id="txtPrevisaoEntrega" name="txtPrevisaoEntrega" DATASRC="#dsoSup01" DATAFLD="V_dtPrevisaoEntrega" class="fldGeneral"/>

		<p id="lblAtrasoEntrega" name="lblAtrasoEntrega" class="lblGeneral" title="Atraso Entrega">Atraso</p>
        <input type="text" id="txtAtrasoEntrega" name="txtAtrasoEntrega" DATASRC="#dsoSup01" DATAFLD="AtrasoEntrega" class="fldGeneral"/>

		<p id="lblAgenteCargaID" name="lblAgenteCargaID" class="lblGeneral">Agente de Cargas</p>
        <select id="selAgenteCargaID" name="selAgenteCargaID" DATASRC="#dsoSup01" DATAFLD="AgenteCargaID" class="fldGeneral"></select>

        <p id="lblMeioTransporteID" name="lblMeioTransporteID" class="lblGeneral">Meio Transporte</p>        
        <select id="selMeioTransporteID" name="selMeioTransporteID" DATASRC="#dsoSup01" DATAFLD="MeioTransporteID" class="fldGeneral"></select>

        <p id="lblTipoFreteID" name="lblTipoFreteID" class="lblGeneral">Tipo Frete</p>
        <select id="selTipoFreteID" name="selTipoFreteID" DATASRC="#dsoSup01" DATAFLD="TipoFreteID" class="fldGeneral"></select>

        <p id="lblPortoOrigemID" name="lblPortoOrigemID" class="lblGeneral">Origem</p>
        <select id="selPortoOrigemID" name="selPortoOrigemID" DATASRC="#dsoSup01" DATAFLD="PortoOrigemID" class="fldGeneral"></select>

        <p id="lblCompanhiaID" name="lblCompanhiaID" class="lblGeneral">Companhia</p>
        <select id="selCompanhiaID" name="selCompanhiaID" DATASRC="#dsoSup01" DATAFLD="CompanhiaID" class="fldGeneral"></select>

        <p id="lblPortoDestinoID" name="lblPortoDestinoID" class="lblGeneral">Destino</p>
        <select id="selPortoDestinoID" name="selPortoDestinoID" DATASRC="#dsoSup01" DATAFLD="PortoDestinoID" class="fldGeneral"></select>

        <p id="lblRecintoAduaneiroID" name="lblRecintoAduaneiroID" class="lblGeneral">Recinto Aduaneiro</p>
        <select id="selRecintoAduaneiroID" name="selRecintoAduaneiroID" DATASRC="#dsoSup01" DATAFLD="RecintoAduaneiroID" class="fldGeneral"></select>

		<p id="lblAutorizacaoEmbarque" name="lblAutorizacaoEmbarque" class="lblGeneral" title="Autoriza��o de Embarque">Aut Embarque</p>
        <input type="text" id="txtAutorizacaoEmbarque" name="txtAutorizacaoEmbarque" DATASRC="#dsoSup01" DATAFLD="V_dtAutorizacaoEmbarque" class="fldGeneral"/>

		<p id="lblDocumentoEmbarque" name="lblDocumentoEmbarque" class="lblGeneral">Documento</p>
        <input type="text" id="txtDocumentoEmbarque" name="txtDocumentoEmbarque" DATASRC="#dsoSup01" DATAFLD="DocumentoEmbarque" class="fldGeneral"/>

		<p id="lbldtDocumentoEmbarque" name="lbldtDocumentoEmbarque" class="lblGeneral">Emiss�o</p>
        <input type="text" id="txtdtDocumentoEmbarque" name="txtdtDocumentoEmbarque" DATASRC="#dsoSup01" DATAFLD="V_dtDocumentoEmbarque" class="fldGeneral"/>

        <p id="lbldtEmbarque" name="lbldtEmbarque" class="lblGeneral" title="Actual Time Departure">Embarque (ATD)</p>
        <input type="text" id="txtdtEmbarque" name="txtdtEmbarque" DATASRC="#dsoSup01" DATAFLD="V_dtEmbarque" class="fldGeneral"/>

        <p id="lbldtChegada" name="lbldtChegada" class="lblGeneral">Chegada</p>
        <input type="text" id="txtdtChegada" name="txtdtChegada" DATASRC="#dsoSup01" DATAFLD="V_dtChegada" class="fldGeneral"/>

        <p id="lblContainers" name="lblContainers" class="lblGeneral">Containers</p>
        <input type="text" id="txtContainers" name="txtContainers" DATASRC="#dsoSup01" DATAFLD="Containers" class="fldGeneral"/>

        <p id="lblPalletes" name="lblPalletes" class="lblGeneral">Palletes</p>
        <input type="text" id="txtPalletes" name="txtPalletes" DATASRC="#dsoSup01" DATAFLD="Palletes" class="fldGeneral"/>
        
        <p id="lblCaixas" name="lblCaixas" class="lblGeneral">Caixas</p>
        <input type="text" id="txtCaixas" name="txtCaixas" DATASRC="#dsoSup01" DATAFLD="Caixas" class="fldGeneral"/>

        <p id="lblTransportadoraDTAID" name="lblTransportadoraDTAID" class="lblGeneral">Transportadora DTA</p>
        <select id="selTransportadoraDTAID" name="selTransportadoraDTAID" DATASRC="#dsoSup01" DATAFLD="TransportadoraDTAID" class="fldGeneral"></select>

        <p id="lblTratamentoCarga" name="lblTratamentoCarga" class="lblGeneral" title="Tratamento de Carga">TC</p>
        <select id="selTratamentoCarga" name="selTratamentoCarga" DATASRC="#dsoSup01" DATAFLD="TratamentoCargaID" class="fldGeneral"></select>
        
        <p id="lblDTA" name="lblDTA" class="lblGeneral" title="Declara��o de Tr�nsito Aduaneiro">DTA</p>
        <input type="text" id="txtDTA" name="txtDTA" DATASRC="#dsoSup01" DATAFLD="NumeroDTA" class="fldGeneral"/>

        <p id="lblRegistroDTA" name="lblRegistroDTA" class="lblGeneral">Registro DTA</p>
        <input type="text" id="txtRegistroDTA" name="txtRegistroDTA" DATASRC="#dsoSup01" DATAFLD="V_dtRegistroDTA" class="fldGeneral"/>
        
        <p id="lblCanalDTA" name="lblCanalDTA" class="lblGeneral">Canal DTA</p>
        <select id="selCanalDTA" name="selCanalDTA" DATASRC="#dsoSup01" DATAFLD="CanalDTAID" class="fldGeneral"></select>

        <p id="lbldtLiberacaoDTA" name="lbldtLiberacaoDTA" class="lblGeneral">Libera��o DTA</p>
        <input type="text" id="txtdtLiberacaoDTA" name="txtdtLiberacaoDTA" DATASRC="#dsoSup01" DATAFLD="V_dtLiberacaoDTA" class="fldGeneral"/>

        <p id="lblAdcTarifario" name="lblAdcTarifario" class="lblGeneral">Ad Tarif�rio</p>
        <input type="text" id="txtAdcTarifario" name="txtAdcTarifario" DATASRC="#dsoSup01" DATAFLD="AdicionalTarifario" class="fldGeneral"/>

        <p id="lblNIC" name="lblNIC" class="lblGeneral" title="N�mero Identificador de Carga (Data de Presen�a da Carga)">NIC</p>
        <input type="text" id="txtNIC" name="txtNIC" DATASRC="#dsoSup01" DATAFLD="V_dtNIC" class="fldGeneral"/>

        <p id="lbldtAutorizacao" name="lbldtAutorizacao" class="lblGeneral">Autoriza��o</p>
        <input type="text" id="txtdtAutorizacao" name="txtdtAutorizacao" DATASRC="#dsoSup01" DATAFLD="V_dtAutorizacao" class="fldGeneral"/>
		
        <p id="lblDIID" name="lblDIID" class="lblGeneral" title="Declara��o de Importa��o">DI</p>
        <input type="text" id="txtDIID" name="txtDIID" DATASRC="#dsoSup01" DATAFLD="DI" class="fldGeneral"/>
        
		<p id="lblDtRegistro" name="lblDtRegistro" class="lblGeneral">Registro DI</p>
		<input type="text" id="txtDtRegistro" name="txtDtRegistro" DATASRC="#dsoSup01" DATAFLD="V_dtRegistro" class="fldGeneral"/>
        
		<p id="lblCanalDIID" name="lblCanalDIID" class="lblGeneral">Canal DI</p>
		<select id="selCanalDIID" name="selCanalDIID" DATASRC="#dsoSup01" DATAFLD="CanalDIID" class="fldGeneral"></select>
		
        <p id="lblTaxaMoeda" name="lblTaxaMoeda" class="lblGeneral">Taxa Moeda</p>
		<input type="text" id="txtTaxaMoeda" name="txtTaxaMoeda" DATASRC="#dsoSup01" DATAFLD="TaxaMoeda" class="fldGeneral"/>
        
	    <p id="lblDtDesembaraco" name="lblDtDesembaraco" class="lblGeneral">Desembara�o</p>
        <input type="text" id="txtDtDesembaraco" name="txtDtDesembaraco" DATASRC="#dsoSup01" DATAFLD="V_dtDesembaraco" class="fldGeneral"/>

        <p id="lblDespachanteID" name="lblDespachanteID" class="lblGeneral">Despachante</p>
        <select id="selDespachanteID" name="selDespachanteID" DATASRC="#dsoSup01" DATAFLD="DespachanteID" class="fldGeneral"></select>
        
        <p id="lblProcDespachante" name="lblProcDespachante" class="lblGeneral">Processo Despachante</p>
        <input type="text" id="txtProcDespachante" name="txtProcDespachante" DATASRC="#dsoSup01" DATAFLD="ProcessoDespachante" class="fldGeneral"/>

        <p id="lbldtEtiquetagem" name="lbldtEtiquetagem" class="lblGeneral">Etiquetagem</p>
        <input type="text" id="txtdtEtiquetagem" name="txtdtEtiquetagem" DATASRC="#dsoSup01" DATAFLD="V_dtEtiquetagem" class="fldGeneral"/>

        <p id="lblTransportadora" name="lblTransportadora" class="lblGeneral">Transportadora</p>
        <select id="selTransportadora" name="selTransportadora" DATASRC="#dsoSup01" DATAFLD="TransportadoraID" class="fldGeneral"></select>

        <p id="lblEntrega" name="lblEntrega" class="lblGeneral">Entrega</p>
        <input type="text" id="txtEntrega" name="txtEntrega" DATASRC="#dsoSup01" DATAFLD="V_dtEntrega" class="fldGeneral"/>

        <p id="lblLacre" name="lblLacre" class="lblGeneral">Lacre</p>
        <input type="text" id="txtLacre" name="txtLacre" DATASRC="#dsoSup01" DATAFLD="Lacre" class="fldGeneral"/>

        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"/>


		<!--		
		<p id="lblRecintoAduaneiro" name="lblRecintoAduaneiro" class="lblGeneral">Recinto Aduaneiro</p>
		<input type="text" id="txtRecintoAduaneiro" name="txtRecintoAduaneiro" DATASRC="#dsoSup01" DATAFLD="RecintoAduaneiro" class="fldGeneral"/>

		<p id="lblArmazem" name="lblArmazem" class="lblGeneral">Armazem</p>
		<input type="text" id="txtArmazem" name="txtArmazem" DATASRC="#dsoSup01" DATAFLD="Armazem" class="fldGeneral"/>

        <p id="lblUFDesembaraco" name="lblUFDesembaraco" class="lblGeneral">UF Des</p>
        <select id="selUFDesembaraco" name="selUFDesembaraco" DATASRC="#dsoSup01" DATAFLD="UFDesembaracoID" class="fldGeneral"></select>
		
        <p id="lblValorAFRMM" name="lblValorAFRMM" class="lblGeneral">Valor AFRMM</p>
        <input type="text" id="txtValorAFRMM" name="txtValorAFRMM" DATASRC="#dsoSup01" DATAFLD="ValorAFRMM" class="fldGeneral"/>
        
		<p id="lblFOB" name="lblFOB" class="lblGeneral">FOB US$</p>
		<input type="text" id="txtFOB" name="txtFOB" DATASRC="#dsoSup01" DATAFLD="ValorFOB" toread class="fldGeneral">

        <p id="lblCapatazia" name="lblCapatazia" class="lblGeneral">Capatazia US$</p>
		<input type="text" id="txtCapatazia" name="txtCapatazia" DATASRC="#dsoSup01" DATAFLD="Capatazia" class="fldGeneral"
			onchange=""/>

		<p id="lblFrete" name="lblFrete" class="lblGeneral">Valor Frete US$</p>
		<input type="text" id="txtFrete" name="txtFrete" DATASRC="#dsoSup01" DATAFLD="ValorFrete" class="fldGeneral"
			onchange=""/>

		<p id="lblValorSeguro" name="lblValorSeguro" class="lblGeneral">Valor Seguro US$</p>
		<input type="text" id="txtValorSeguro" name="txtValorSeguro" DATASRC="#dsoSup01" DATAFLD="ValorSeguro" class="fldGeneral"
			onchange=""/>

		<p id="lblValorCIF" name="lblValorCIF" class="lblGeneral">Valor CIF US$</p>
		<input type="text" id="txtValorCIF" name="txtValorCIF" class="fldGeneral" DATASRC="#dsoSup01" toread DATAFLD="ValorCIF">

		<p id="lblValorCIFReais" name="lblValorCIFReais" class="lblGeneral">Valor CIF R$</p>
		<input type="text" id="txtValorCIFReais" name="txtValorCIFReais" DATASRC="#dsoSup01" DATAFLD="ValorCIFConvertido" toread class="fldGeneral"/>

		<p id="lblValorII" name="lblValorII" class="lblGeneral">Valor II(R$)</p>
		<input type="text" id="txtValorII" name="txtValorII" class="fldGeneral" DATASRC="#dsoSup01" toread DATAFLD="ValorII">

		<p id="lblValorIOF" name="lblValorIOF" class="lblGeneral">Valor IOF R$</p>
		<input type="text" id="txtValorIOF" name="txtValorIOF" DATASRC="#dsoSup01" DATAFLD="ValorIOF" class="fldGeneral"/>

		<p id="lblDespesasAduaneiras" name="lblDespesasAduaneiras" class="lblGeneral">Desp Aduaneiras R$</p>
		<input type="text" id="txtDespesasAduaneiras" name="txtDespesasAduaneiras" DATASRC="#dsoSup01" DATAFLD="DespesasAduaneiras" class="fldGeneral"/>

		<p id="lblQuantidade" name="lblQuantidade" class="lblGeneral">Quantidade</p>
		<input type="text" id="txtQuantidade" name="txtQuantidade" DATASRC="#dsoSup01" DATAFLD="Quantidade" toread class="fldGeneral"/>

        <p id="lblPesoLiquidoTotal" name="lblPesoLiquidoTotal" class="lblGeneral">Peso Liq. Total</p>
        <input type="text" id="txtPesoLiquidoTotal" name="txtPesoLiquidoTotal" DATASRC="#dsoSup01" DATAFLD="PesoLiquidoTotal" class="fldGeneral"/>

        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"/>
        -->

    </div>
    
</body>

</html>
