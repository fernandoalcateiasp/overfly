/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form visitas
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Dados da listagem da pesquisa
var dsoListData01 = new CDatatransport("dsoListData01");
// Dados dos combos de contexto e filtros
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");  
// Dados dos proprietarios para o pesqlist .URL
var dsoPropsPL = new CDatatransport("dsoPropsPL");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array(
		"ID",
		"Est",
        "Situa��o",
        "Processo",
        "Marcas",
        "Invoices",
        "Itens",
        "Total FOB",
        "Previs�o Entrega",
        "Atraso Entrega",
		"DI",
		"Registro",
		//"Exportador",
		"NF",
        "Observa��o",
		"Observa��es");

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', '', '###,###,##0.00', dTFormat, '', '', '', '', '', '');
                                 
    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{

    // seta botao de impressao
    especBtnIsPrintBtn('sup', 1);

    showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1,1]);    
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimentos', 'Preencher Combos', 'Enviar Email', 'Rateios', 'Incluir Documentos']);

    setupEspecBtnsControlBar('sup', 'HHHDDHH');

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
  // Documentos
    if ((controlBar == 'SUP') && (btnClicked == 1)) {
        if (fg.Rows > 1)
            __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));

        else
            window.top.overflyGen.Alert('Selecione um registro.');
    }
        // usuario clicou botao imprimir
    else if (btnClicked == 2)
        openModalPrint();

    else if (btnClicked == 6)
        openModalRateios();

    else if (btnClicked == 7)
        openModalIncluirDocumentos();
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
 // Modal de documentos
     if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') 
     {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }    
     }
     else if (idElement.toUpperCase() == 'MODALPRINTHTML') {
         if (param1 == 'OK') {
             // esta funcao fecha a janela modal e destrava a interface
             restoreInterfaceFromModal();
             // escreve na barra de status
             writeInStatusBar('child', 'cellMode', 'Listagem');

             // nao mexer
             return 0;
         }
         else if (param1 == 'CANCEL') {
             // esta funcao fecha a janela modal e destrava a interface
             restoreInterfaceFromModal();
             // escreve na barra de status
             writeInStatusBar('child', 'cellMode', 'Listagem');

             // nao mexer
             return 0;
         }
     }
     else if (idElement.toUpperCase() == 'MODALRATEIOSHTML') {
         if (param1 == 'OK') {
             // esta funcao fecha a janela modal e destrava a interface
             restoreInterfaceFromModal();

             // escreve na barra de status
             writeInStatusBar('child', 'cellMode', 'Detalhe');

             // nao mexer
             return 0;
         }
         else if (param1 == 'CANCEL') {
             // esta funcao fecha a janela modal e destrava a interface
             restoreInterfaceFromModal();
             // escreve na barra de status
             writeInStatusBar('child', 'cellMode', 'Detalhe');

             // nao mexer
             return 0;
         }
     }

     else if (idElement.toUpperCase() == 'MODALINCLUIRDOCUMENTOSHTML')
     {
         if (param1 == 'OK')
         {
             // esta funcao fecha a janela modal e destrava a interface
             restoreInterfaceFromModal();
             // escreve na barra de status
             writeInStatusBar('child', 'cellMode', 'Detalhe');
             return 0;
         }
         else if (param1 == 'CANCEL')
         {
             // esta funcao fecha a janela modal e destrava a interface
             restoreInterfaceFromModal();
             // escreve na barra de status
             writeInStatusBar('child', 'cellMode', 'Detalhe');
         }
     }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}
/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
//    var nNotaFiscalID = dsoSup01.recordset['NotaFiscalID'].value;

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);
 //   strPars += '&nNotaFiscalID=' + escape(nNotaFiscalID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodimportacao/importacao/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(346, 200));
}


function openModalRateios()
{
    var htmlPath;
    var strPars = new String();
    var nImportacaoID = 0;

    if (fg.Row > 0)
    {
        nImportacaoID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ImportacaoID'));

        // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
        strPars = '?sCaller=' + escape('PL');
        strPars += '&nImportacaoID=' + escape(nImportacaoID);

        htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodImportacao/Importacao/modalpages/modalRateios.asp' + strPars;
        showModalWin(htmlPath, new Array(995, 532));
    }
}

function openModalIncluirDocumentos()
{
    var strPars = new String();

    strPars = '?sCaller=' + escape('PL');

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodImportacao/importacao/modalpages/modalincluirdocumentos.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 600));
}