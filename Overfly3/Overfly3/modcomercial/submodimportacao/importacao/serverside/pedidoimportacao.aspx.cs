using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodimportacao.importacao.serverside
{
    public partial class pedidoimportacao : System.Web.UI.OverflyPage
	{
		private static Integer zeroInt = new Integer(0);
		
		private Integer pedidoID;
        private Integer importacaoID;

        protected Integer nPedidoID
        {
            set { pedidoID = value != null ? value : zeroInt; }
        }

        protected Integer nImportacaoID
        {
            set { importacaoID = value != null ? value : zeroInt; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string sql;

            sql = "INSERT INTO Importacao_Pedidos (ImportacaoID, PedidoID)" +
                    "SELECT " + importacaoID.ToString() + ", " + pedidoID.ToString();

            int rowsAffected = DataInterfaceObj.ExecuteSQLCommand(sql);

            WriteResultXML(DataInterfaceObj.getRemoteData(
                    "SELECT " + rowsAffected + " as Resultado"                                        
                )
            );
        }
    }
}