﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodimportacao.importacao.serverside
{
    public partial class sincronizaCustos : System.Web.UI.OverflyPage
    {
        private string Resultado;
        
        private Integer importacaoID;

        public Integer nImportacaoID
        {
            get { return importacaoID; }
            set { importacaoID = value; }
        }

        /** Roda a procedure sp_Importacao_CustosSincroniza */
        private void ImportacaoSincronizaCustos()
        {
            ProcedureParameters[] parameters = new ProcedureParameters[1];

            parameters[0] = new ProcedureParameters(
                            "@ImportacaoID",
                            System.Data.SqlDbType.Int,
                            importacaoID.ToString());

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Importacao_CustosSincroniza",
                parameters);

        }


        protected override void PageLoad(object sender, EventArgs e)
        {

            ImportacaoSincronizaCustos();

            WriteResultXML(DataInterfaceObj.getRemoteData("select '' as Resultado "));
        }
    }
}