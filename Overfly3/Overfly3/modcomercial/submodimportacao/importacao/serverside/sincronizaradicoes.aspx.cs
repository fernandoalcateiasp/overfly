using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodimportacao.importacao.serverside
{
    public partial class sincronizaradicoes : System.Web.UI.OverflyPage
    {
        private Integer importacaoID;

        protected Integer nImportacaoID
        {
            set { importacaoID = value != null ? value : new Integer(0); }
        }

        
        protected override void PageLoad(object sender, EventArgs e)
        {
            // Roda a procedure sp_Importacao_Adicoes.

            // Deleta as adi��es
            ProcedureParameters[] procparam = new ProcedureParameters[2];
            procparam[0] = new ProcedureParameters("@ImportacaoID", SqlDbType.Int, importacaoID.intValue());
            procparam[1] = new ProcedureParameters("@Tipo", SqlDbType.Int, 2);

            DataInterfaceObj.execNonQueryProcedure("sp_Importacao_Adicoes", procparam);

            // Cria as adi��es
            procparam[0] = new ProcedureParameters("@ImportacaoID", SqlDbType.Int, importacaoID.intValue());
            procparam[1] = new ProcedureParameters("@Tipo", SqlDbType.Int, 1);

            DataInterfaceObj.execNonQueryProcedure("sp_Importacao_Adicoes", procparam);

            // Gera o resultado.
            WriteResultXML(DataInterfaceObj.getRemoteData("select null as resultServ"));
        }
    }
}
