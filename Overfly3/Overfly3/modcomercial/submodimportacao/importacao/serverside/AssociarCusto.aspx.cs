﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modcomercial.submodimportacao.importacao.serverside
{
    public partial class AssociarCusto : System.Web.UI.OverflyPage
    {
		int nImpCustoID = 0;
        DataSet dsResult = null;

        private Integer dataLen = Constants.INT_ZERO;
		public Integer nDataLen
		{
			get { return dataLen; }
			set { dataLen = value; }
		}

        private Integer[] importacaoID = Constants.INT_EMPTY_SET;
        public Integer[] ImportacaoID
		{
			get { return importacaoID; }
            set { importacaoID = value; }
		}

        private Integer[] pedidoID = Constants.INT_EMPTY_SET;
        public Integer[] PedidoID
        {
            get { return pedidoID; }
            set { pedidoID = value; }
        }

        private Integer[] financeiroID = Constants.INT_EMPTY_SET;
        public Integer[] FinanceiroID
        {
            get { return financeiroID; }
            set { financeiroID = value; }
        }
		private Integer[] tipoCustoID = Constants.INT_EMPTY_SET;
        public Integer[] TipoCustoID
		{
            get { return tipoCustoID; }
            set { tipoCustoID = value; }
		}

        private java.lang.Double[] valor = Constants.DBL_EMPTY_SET;
        public java.lang.Double[] Valor
		{
            get { return valor; }
            set { valor = value; }
		}

		private string[] descricao = Constants.STR_EMPTY_SET;
        public string[] Descricao
		{
            get { return descricao; }
            set { descricao = value; }
		}
		protected string SQL
		{
            get
			{
				string sql = "";

				for (int i = 0; i < nDataLen.intValue(); i++)
				{
                    dsResult = null;
                    nImpCustoID = 0;

                    if ((TipoCustoID[i].intValue() == 1391) || (TipoCustoID[i].intValue() == 1392))
                    {
                        dsResult = DataInterfaceObj.getRemoteData(
                                "SELECT ImpCustoID " +
                                    "FROM Importacao_Custos WITH(NOLOCK) " +
                                    "WHERE TipoCustoID = " + TipoCustoID[i].intValue().ToString() + " " +
                                        "AND ImportacaoID = " + importacaoID[i].intValue().ToString()
                            );

                        if (dsResult.Tables[1].Rows.Count > 0)
                        {
                            nImpCustoID = int.Parse(dsResult.Tables[1].Rows[0]["ImpCustoID"].ToString());
                        }
                    }
                    
                    if (nImpCustoID == 0)
                    {
                        sql += "INSERT Importacao_Custos(ImportacaoID, FinanceiroID, PedidoID, TipoCustoID, Descricao, Valor)  " +
                                    "SELECT " + importacaoID[i].intValue() + ", "
                                              + (financeiroID[i] != null ? (object)financeiroID[i].intValue() : "NULL") + ", " 
                                              + (pedidoID[i] != null ? (object)pedidoID[i].intValue() : "NULL") + ", "
                                              + TipoCustoID[i].intValue() + ", " + descricao[i].ToString() + ", " + valor[i].doubleValue();
                    }
                    else
                    {
                        sql += "UPDATE Importacao_Custos SET Descricao = " + descricao[i].ToString() + ", Valor = " + valor[i].doubleValue().ToString() + " " +
                                    "WHERE ((ImportacaoID = " + importacaoID[i].intValue().ToString() + ") " +
                                        "AND (TipoCustoID = " + TipoCustoID[i].intValue().ToString() + "))";
                    }
				}

				return sql;
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.ExecuteSQLCommand(SQL);

			WriteResultXML(DataInterfaceObj.getRemoteData("select '' as Resultado"));
		}
    }
}