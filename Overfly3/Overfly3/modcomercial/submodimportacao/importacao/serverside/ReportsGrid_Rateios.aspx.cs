using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.PrintJet
{
	public partial class Rateios : System.Web.UI.OverflyPage
	{
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey; 
        private string controlID = Convert.ToString(HttpContext.Current.Request.Params["controlID"]);
        private int nLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["nLinguaLogada"]);
        private string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["sEmpresaFantasia"]);
        private string controle = Convert.ToString(HttpContext.Current.Request.Params["controle"]);
        private int selRateio = Convert.ToInt32(HttpContext.Current.Request.Params["selRateio"]);
        private int glb_nTipoImportacaoID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nTipoImportacaoID"]);
        private int glb_nImportacaoID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nImportacaoID"]);

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                switch (controle)
                {
                    case "listaExcel":
                        listaExcelRateios();
                        break;
                }
            }
        }
        public void listaExcelRateios()
        {
            DateTime DataHoje = DateTime.Now;
             string _data = DataHoje.ToString("dd_MM_yyyy");

            if (nLinguaLogada != 246)
            {
                _data = DataHoje.ToString("MM_dd_yyyy");
            }

            string Title = "";

            string Cast1 = "";
            string Cast2 = "";

            if (controlID == "btnExcel")
            {
                Cast1 = "CAST(";
                Cast2 = "AS NUMERIC(16, 4))";
            }

            string strSQL = "";

            if (selRateio == 1) //Nacionalização
            {
                if (glb_nTipoImportacaoID == 1351) //Nacionalização Trading
                {
                    Title = "Nacionalização_Trading_" + sEmpresaFantasia + "_" + _data;

                    strSQL = "SELECT Inv.ExportadorID, InvItem.InvItemID AS InvItemID, Inv.Invoice AS Invoice, Exportador.Fantasia AS Exportador, InvItem.ProdutoID AS ID, LEFT(InvItem.Produto, 50) AS Produto, " +
                                    "InvItem.Quantidade AS Quantidade, ((InvItem.ValorFOBUnitario * Imp.TaxaMoeda) * InvItem.Quantidade) AS FOBTotal, " +
                                    Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 2)" + Cast2 + " AS RateioFOB, " +
                                    Cast1 + "(InvItem.PesoLiquitoUnitario * InvItem.Quantidade)" + Cast2 + " AS PesoLiquido, " +
                                    Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 1)"+ Cast2 +" AS RateioPL, " +
                                    Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 3)" + Cast2 + " AS Frete, " +
                                    Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 4)" + Cast2 + " AS RateioFOBFrete, " +
                                    Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 5)" + Cast2 + " AS Seguro, " +
                                    "ISNULL(NFPIS.BaseCalculo, dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 4, 3)) AS BasePIS, " +
                                    "ISNULL(NFPIS.Aliquota, dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 4, 2)) AS AliquotaPIS, " +
                                    "ISNULL(NFPIS.ValorImposto, dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 4, 1)) AS ValorPIS, " +
                                    "ISNULL(NFCofins.BaseCalculo, dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 5, 3)) AS BaseCOFINS, " +
                                    "ISNULL(NFCofins.Aliquota, dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 5, 2)) AS AliquotaCOFINS, " +
                                    "ISNULL(NFCofins.ValorImposto, dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 5, 1)) AS ValorCOFINS, " +
                                    "ISNULL(NFIPI.BaseCalculo, dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 8, 3)) AS BaseIPI, " +
                                    "ISNULL(NFIPI.Aliquota, dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 8, 2)) AS AliquotaIPI, " +
                                    "ISNULL(NFIPI.ValorImposto, dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 8, 1)) AS ValorIPI, " +
                                    "ISNULL(NFICMS.BaseCalculo, dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 9, 3)) AS BaseICMS, " +
                                    "ISNULL(NFICMS.Aliquota, dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 9, 2)) AS AliquotaICMS, " +
                                    "ISNULL(NFICMS.ValorImposto, dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 9, 1)) AS ValorICMS, " +
                                    "ISNULL(NFICMSST.BaseCalculo, 0) AS BaseICMSST, " +
                                    "ISNULL(NFICMSST.Aliquota, 0) AS AliquotaICMSST, " +
                                    "ISNULL(NFICMSST.ValorImposto, 0) AS ValorICMSST, " +
                                    "ISNULL(NFeTrading.ValorUnitario, CONVERT(NUMERIC(16, 4), (dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 8) / InvItem.Quantidade))) AS UnitarioNF, NFeTrading.InvIteNFeTradingID " +
                                "FROM Importacao Imp WITH(NOLOCK) " +
                                    "INNER JOIN Invoices Inv WITH(NOLOCK) ON (Inv.ImportacaoID = Imp.ImportacaoID) " +
                                    "INNER JOIN Invoices_Itens InvItem WITH(NOLOCK) ON (InvItem.InvoiceID = Inv.InvoiceID) " +
                                    "INNER JOIN Pessoas Exportador WITH(NOLOCK) ON (Exportador.PessoaID = Inv.ExportadorID) " +
                                    "INNER JOIN RelacoesPesCon ProdutoEmpresa WITH(NOLOCK) ON ((ProdutoEmpresa.ObjetoID = InvItem.ProdutoID) AND (ProdutoEmpresa.SujeitoID = Inv.EmpresaID)) " +
                                    "LEFT OUTER JOIN Invoices_Itens_NFeTrading NFeTrading WITH(NOLOCK) ON (NFeTrading.InvItemID = InvItem.InvItemID) " +
                                    "LEFT OUTER JOIN Invoices_Itens_NFeTrading_Impostos NFPIS WITH(NOLOCK) ON (NFPIS.InvIteNFeTradingID = NFeTrading.InvIteNFeTradingID) AND (NFPIS.ImpostoID = 4) " +
                                    "LEFT OUTER JOIN Invoices_Itens_NFeTrading_Impostos NFCofins WITH(NOLOCK) ON (NFCofins.InvIteNFeTradingID = NFeTrading.InvIteNFeTradingID) AND (NFCofins.ImpostoID = 5) " +
                                    "LEFT OUTER JOIN Invoices_Itens_NFeTrading_Impostos NFIPI WITH(NOLOCK) ON (NFIPI.InvIteNFeTradingID = NFeTrading.InvIteNFeTradingID) AND (NFIPI.ImpostoID = 8) " +
                                    "LEFT OUTER JOIN Invoices_Itens_NFeTrading_Impostos NFICMS WITH(NOLOCK) ON (NFICMS.InvIteNFeTradingID = NFeTrading.InvIteNFeTradingID) AND (NFICMS.ImpostoID = 9) " +
                                    "LEFT OUTER JOIN Invoices_Itens_NFeTrading_Impostos NFICMSST WITH(NOLOCK) ON (NFICMSST.InvIteNFeTradingID = NFeTrading.InvIteNFeTradingID) AND (NFICMSST.ImpostoID = 25) " +
                                "WHERE (Imp.ImportacaoID = " + glb_nImportacaoID + ") " +
                                "ORDER BY Exportador.Fantasia, Inv.InvoiceID";
                }
                else //Nacionalização
                {
                    Title = "Nacionalização_" + sEmpresaFantasia + "_" + _data;

                    strSQL = "SELECT Inv.ExportadorID, InvItem.InvItemID AS InvItemID, Inv.Invoice AS Invoice, NCM.Conceito AS NCM, Exportador.Fantasia AS Exportador, InvItem.ProdutoID AS ID, LEFT(InvItem.Produto, 50) AS Produto, " +
                                    "InvItem.Quantidade AS Quantidade, ((InvItem.ValorFOBUnitario * Imp.TaxaMoeda) * InvItem.Quantidade) AS FOBTotal, " +
                                    Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 2)" + Cast2 + " AS RateioFOB, " +
                                    Cast1 + "(InvItem.PesoLiquitoUnitario * InvItem.Quantidade)" + Cast2 + " AS PesoLiquido, " +
                                    Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 1)" + Cast2 + " AS RateioPL, " +
                                    Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 3)" + Cast2 + " AS Frete, " +
                                    Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 4)" + Cast2 + " AS RateioFOBFrete, " +
                                    Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 5)" + Cast2 + " AS Seguro, " +
                                    Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 6)" + Cast2 + " AS SISCOMEX, " +
                                    "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 7) AS CIF, " +
                                    "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 7, 2) AS AliquotaII, " +
                                    "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 7, 1) AS ValorII, " +
                                    "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 4, 2) AS AliquotaPIS, " +
                                    "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 4, 1) AS ValorPIS, " +
                                    "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 5, 2) AS AliquotaCOFINS, " +
                                    "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 5, 1) AS ValorCOFINS, " +
                                    "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 8, 3) AS BaseIPI, " +
                                    "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 8, 2) AS AliquotaIPI, " +
                                    "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 8, 1)AS ValorIPI, " +
                                    "CONVERT(BIT, (CASE WHEN (Imp.EmpresaID = 10) THEN 1 ELSE 0 END)) AS BeneficioICMS, " +
                                    "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 9, 3) AS BaseICMS, " +
                                    "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 9, 2) AS AliquotaICMS, " +
                                    "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 9, 1) AS ValorICMS, " +
                                    "CONVERT(NUMERIC(16, 4), (dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 8) / InvItem.Quantidade)) AS UnitarioNF, " +
                                    "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 8) AS TotalProdutos, " +
                                    Cast1 + "(dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 9) - dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 8))" + Cast2 + " AS DifTotal, " +
                                    Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 9)" + Cast2 + " AS TotalNF, " +
                                    "ProdutoEmpresa.RelacaoID " +
                                "FROM Importacao Imp WITH(NOLOCK) " +
                                    "INNER JOIN Invoices Inv WITH(NOLOCK) ON (Inv.ImportacaoID = Imp.ImportacaoID) " +
                                    "INNER JOIN Invoices_Itens InvItem WITH(NOLOCK) ON (InvItem.InvoiceID = Inv.InvoiceID) " +
                                    "INNER JOIN Pessoas Exportador WITH(NOLOCK) ON (Exportador.PessoaID = Inv.ExportadorID) " +
                                    "INNER JOIN RelacoesPesCon ProdutoEmpresa WITH(NOLOCK) ON ((ProdutoEmpresa.ObjetoID = InvItem.ProdutoID) AND (ProdutoEmpresa.SujeitoID = Inv.EmpresaID)) " +
                                    "LEFT OUTER JOIN Conceitos NCM WITH(NOLOCK) ON (NCM.ConceitoID = ProdutoEmpresa.ClassificacaoFiscalID) " +
                                "WHERE (Imp.ImportacaoID = " + glb_nImportacaoID + ") " +
                                "ORDER BY Exportador.Fantasia, Inv.InvoiceID";
                }

            }
            else if (selRateio == 2) //Contabil
            {
                Title = "Contábil_" + sEmpresaFantasia + "_" + _data;

                strSQL = "SELECT InvItem.InvItemID AS InvItemID, Inv.Invoice AS Invoice, NCM.Conceito AS NCM, Inv.ExportadorID, Exportador.Fantasia AS Exportador, InvItem.ProdutoID AS ID, " +
                                "LEFT(InvItem.Produto, 50) AS Produto, InvItem.Quantidade AS Quantidade, " +
                                Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 2)" + Cast2 + " AS RateioFOB, " +
                                Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 11)" + Cast2 + " AS Despesas, " +
                                Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 13)" + Cast2 + " AS RateioContabil, " +
                                Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 15)" + Cast2 + " AS NFComplementar, " +
                                "(SELECT COUNT(1) " +
                                    "FROM Pedidos aa WITH(NOLOCK) " +
                                        "INNER JOIN Importacao_Pedidos bb WITH(NOLOCK) ON (bb.PedidoID = aa.PedidoID) " +
                                    "WHERE ((bb.ImportacaoID = Imp.ImportacaoID) AND (aa.TransacaoID <> 111))) AS PedidosAjustes, ProdutoEmpresa.RelacaoID  " +
                            "FROM Importacao Imp WITH(NOLOCK) " +
                                "INNER JOIN Invoices Inv WITH(NOLOCK) ON (Inv.ImportacaoID = Imp.ImportacaoID) " +
                                "INNER JOIN Invoices_Itens InvItem WITH(NOLOCK) ON (InvItem.InvoiceID = Inv.InvoiceID) " +
                                "INNER JOIN Pessoas Exportador WITH(NOLOCK) ON (Exportador.PessoaID = Inv.ExportadorID) " +
                                "INNER JOIN RelacoesPesCon ProdutoEmpresa WITH(NOLOCK) ON ((ProdutoEmpresa.ObjetoID = InvItem.ProdutoID) " +
                                    "AND (ProdutoEmpresa.SujeitoID = Inv.EmpresaID)) " +
                                "LEFT OUTER JOIN Conceitos NCM WITH(NOLOCK) ON (NCM.ConceitoID = ProdutoEmpresa.ClassificacaoFiscalID) " +
                            "WHERE (Imp.ImportacaoID = " + glb_nImportacaoID + ") " +
                            "ORDER BY Exportador.Fantasia, Inv.InvoiceID";
            }
            else if (selRateio == 3) //Gerencial
            {
                Title = "Gerencial_" + sEmpresaFantasia + "_" + _data;

                strSQL = "SELECT InvItem.InvItemID AS InvItemID, Inv.Invoice AS Invoice, NCM.Conceito AS NCM, Exportador.Fantasia AS Exportador, InvItem.ProdutoID AS ID, " +
                                "LEFT(InvItem.Produto, 50) AS Produto, InvItem.Quantidade AS Quantidade, " +
                                "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 7, 1) AS ValorII, " +
                                "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 4, 1) AS ValorPIS, " +
                                "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 5, 1) AS ValorCOFINS, " +
                                "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 8, 1)AS ValorIPI, " +
                                "dbo.fn_Importacao_Itens_Impostos(Imp.ImportacaoID, InvItem.InvItemID, 9, 1) AS ValorICMS, " +
                                Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 2)" + Cast2 + " AS RateioFOB, " +
                                Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 11)" + Cast2 + " AS Despesas, " +
                                Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 14)" + Cast2 + " AS RateioGerencial, " +
                                Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 16)" + Cast2 + " AS Gerencial, " +
                                "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 17) AS Desconto, " +
                                Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 18)" + Cast2 + " AS FreteAjustado, " +
                                Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 3)" + Cast2 + " AS Frete, " +
                                Cast1 + "dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 19)" + Cast2 + " AS AjusteGerencial, " +
                                "(SELECT COUNT(1) " +
                                    "FROM Pedidos aa WITH(NOLOCK) " +
                                        "INNER JOIN Importacao_Pedidos bb WITH(NOLOCK) ON (bb.PedidoID = aa.PedidoID) " +
                                    "WHERE ((bb.ImportacaoID = Imp.ImportacaoID) AND (aa.TransacaoID <> 111))) AS PedidosAjustes, " +
                                "ProdutoEmpresa.RelacaoID " +
                            "FROM Importacao Imp WITH(NOLOCK) " +
                                "INNER JOIN Invoices Inv WITH(NOLOCK) ON (Inv.ImportacaoID = Imp.ImportacaoID) " +
                                "INNER JOIN Invoices_Itens InvItem WITH(NOLOCK) ON (InvItem.InvoiceID = Inv.InvoiceID) " +
                                "INNER JOIN Pessoas Exportador WITH(NOLOCK) ON (Exportador.PessoaID = Inv.ExportadorID) " +
                                "INNER JOIN RelacoesPesCon ProdutoEmpresa WITH(NOLOCK) ON ((ProdutoEmpresa.ObjetoID = InvItem.ProdutoID) " +
                                    "AND (ProdutoEmpresa.SujeitoID = Inv.EmpresaID)) " +
                                "LEFT OUTER JOIN Conceitos NCM WITH(NOLOCK) ON (NCM.ConceitoID = ProdutoEmpresa.ClassificacaoFiscalID) " +
                            "WHERE (Imp.ImportacaoID = " + glb_nImportacaoID + ") " +
                            "ORDER BY Exportador.Fantasia, Inv.InvoiceID";
            }
            else if (selRateio == 4) //Resumo
            {
                Title = "Resumo_" + sEmpresaFantasia + "_" + _data;

                strSQL = "SELECT InvItem.InvItemID AS InvItemID, Inv.Invoice AS Invoice, Exportador.Fantasia AS Exportador, InvItem.ProdutoID AS ID, NCM.Conceito AS NCM, " +
                                "LEFT(InvItem.Produto, 50) AS Produto, InvItem.Quantidade AS Quantidade, "+ Cast1 + "(InvItem.ValorFOBUnitario * Imp.TaxaMoeda)" + Cast2 + " AS FOBUnitarioReal, " +
                                Cast1 + "(dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 9) / InvItem.Quantidade)" + Cast2 + " AS NFUnitario, " +
                                Cast1 + "(dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 15) / InvItem.Quantidade)" + Cast2 + " AS NFComplementarUnitario, " +
                                Cast1 + "(dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 16) / InvItem.Quantidade)" + Cast2 + " AS GerencialUnitario, " +
                                Cast1 + "(dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 17) / InvItem.Quantidade)" + Cast2 + " AS DescontoUnitario, " +
                                "(dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 19) / InvItem.Quantidade) AS AjusteGerencialUnitario, " +
                                "Imp.TaxaMoeda, CONVERT(NUMERIC(16, 4), (InvItem.ValorFOBUnitario - (dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 25) / InvItem.Quantidade))) AS FOBUnitarioDolar, " +
                                "CONVERT(NUMERIC(16, 4), ((dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 20) / InvItem.Quantidade) / Imp.TaxaMoeda)) AS CustoMedio, " +
                                "CONVERT(NUMERIC(16, 4), ((((dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 20) / InvItem.Quantidade)/ Imp.TaxaMoeda) / " +
                                        "CONVERT(NUMERIC(16, 4), (InvItem.ValorFOBUnitario - (dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 25) / InvItem.Quantidade))) ))) AS FatorInternacaoEntrada, " +
                                "dbo.fn_Preco_Preco(Imp.EmpresaID, InvItem.ProdutoID, NULL, CONVERT(NUMERIC(16, 4), ((dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 20) / InvItem.Quantidade) / Imp.TaxaMoeda)), " +
                                    "0, NULL, 541, GETDATE(), 1, NULL, NULL, NULL, Imp.EmpresaID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL) AS Preco, " +
                                "CONVERT(NUMERIC(16, 4), (dbo.fn_Preco_Preco(Imp.EmpresaID, InvItem.ProdutoID, NULL, CONVERT(NUMERIC(16, 4), ((dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 20) / InvItem.Quantidade) / Imp.TaxaMoeda)), " +
                                    "0, NULL, 541, GETDATE(), 1, NULL, NULL, NULL, Imp.EmpresaID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL) / " +
                                        "CONVERT(NUMERIC(16, 4), (InvItem.ValorFOBUnitario - (dbo.fn_Importacao_Itens_Totais(Imp.ImportacaoID, InvItem.InvItemID, 25) / InvItem.Quantidade))) )) AS FatorInternacaoSaida, " +
                                "(SELECT TOP 1 aa.FatorInternacaoSaida " +
                                    "FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) " +
                                    "WHERE (aa.RelacaoID = ProdutoEmpresa.RelacaoID) " +
                                    "ORDER BY aa.Ordem) AS FISPesCon, " +
                                "ProdutoEmpresa.RelacaoID " +
                            "FROM Importacao Imp WITH(NOLOCK) " +
                                "INNER JOIN Invoices Inv WITH(NOLOCK) ON (Inv.ImportacaoID = Imp.ImportacaoID) " +
                                "INNER JOIN Invoices_Itens InvItem WITH(NOLOCK) ON (InvItem.InvoiceID = Inv.InvoiceID) " +
                                "INNER JOIN Pessoas Exportador WITH(NOLOCK) ON (Exportador.PessoaID = Inv.ExportadorID) " +
                                "INNER JOIN RelacoesPesCon ProdutoEmpresa WITH(NOLOCK) ON ((ProdutoEmpresa.ObjetoID = InvItem.ProdutoID) " +
                                    "AND (ProdutoEmpresa.SujeitoID = Inv.EmpresaID)) " +
                                "LEFT OUTER JOIN Conceitos NCM WITH(NOLOCK) ON (NCM.ConceitoID = ProdutoEmpresa.ClassificacaoFiscalID) " +
                            "WHERE (Imp.ImportacaoID = " + glb_nImportacaoID + ") " +
                            "ORDER BY Exportador.Fantasia, Inv.InvoiceID";
            }

            if (controlID == "btnExcel")//SE EXCEL
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                string[,] arrIndexKey = new string[,] { { "" } };

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                int Datateste = 0;
                bool nulo = false;

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, true,true);
                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, 2);
                }
            }
            else//Se Lista
            {
                DataSet dtTeste = DataInterfaceObj.getRemoteData(strSQL);
                WriteResultXML(dtTeste);
            }
        }
	}
}
