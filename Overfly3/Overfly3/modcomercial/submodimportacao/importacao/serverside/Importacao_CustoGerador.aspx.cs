﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodimportacao.importacao.serverside
{
    public partial class Importacao_CustoGerador : System.Web.UI.OverflyPage
    {
        private string Resultado;

        private string impCustos;
        public string sImpCustos
        {
            get { return impCustos; }
            set { impCustos = value; }
        }

        private Integer pessoaID;
        public Integer nPessoaID
        {
            get { return pessoaID; }
            set { pessoaID = value; }
        }

        private Integer servicoID;
        public Integer nServicoID
        {
            get { return servicoID; }
            set { servicoID = value; }
        }

        private string documento;
        public string sDocumento
        {
            get { return documento; }
            set { documento = value; }
        }

        private string vencimento;
        public string dtVencimento
        {
            get { return vencimento; }
            set { vencimento = value; }
        }

        private java.lang.Double valor;
        public java.lang.Double nValor
        {
            get { return valor; }
            set { valor = value; }
        }

        private java.lang.Double valorDesconto;
        public java.lang.Double nValorDesconto
        {
            get { return valorDesconto; }
            set { valorDesconto = value; }
        }

        private Integer documentoTransporteID;
        public Integer nDocumentoTransporteID
        {
            get { return documentoTransporteID; }
            set { documentoTransporteID = value; }
        }

        private Integer tipoID;
        public Integer nTipoID
        {
            get { return tipoID; }
            set { tipoID = value; }
        }

        /** Roda a procedure sp_Importacao_CustoGerador */
        private void ImportacaoCustoGerador()
        {
            ProcedureParameters[] parameters = new ProcedureParameters[10];
            try
            {

                parameters[0] = new ProcedureParameters(
                                "@ImpCustos",
                                System.Data.SqlDbType.VarChar,
                                sImpCustos);

                parameters[1] = new ProcedureParameters(
                                "@PessoaID",
                                System.Data.SqlDbType.Int,
                                nPessoaID == null ? DBNull.Value : (Object)nPessoaID.intValue().ToString());

                parameters[2] = new ProcedureParameters(
                                "@ServicoID",
                                System.Data.SqlDbType.Int,
                                nServicoID == null ? DBNull.Value : (Object)nServicoID.intValue().ToString());

                parameters[3] = new ProcedureParameters(
                                "@Documento",
                                System.Data.SqlDbType.BigInt,
                                sDocumento == string.Empty ? DBNull.Value : (Object)sDocumento);

                parameters[4] = new ProcedureParameters(
                                "@dtVencimento",
                                System.Data.SqlDbType.DateTime,
                                dtVencimento == string.Empty ? DBNull.Value : (object)dtVencimento.ToString());

                parameters[5] = new ProcedureParameters(
                                "@Valor",
                                System.Data.SqlDbType.Money,
                                nValor == null ? DBNull.Value : (Object)nValor.doubleValue());

                parameters[6] = new ProcedureParameters(
                                "@ValorDesconto",
                                System.Data.SqlDbType.Money,
                                nValorDesconto == null ? DBNull.Value : (Object)nValorDesconto.doubleValue());

                parameters[7] = new ProcedureParameters(
                                "@DocumentoTransporteID",
                                System.Data.SqlDbType.Int,
                                nDocumentoTransporteID == null ? DBNull.Value : (Object)nDocumentoTransporteID.intValue().ToString());

                parameters[8] = new ProcedureParameters(
                                "@TipoID",
                                System.Data.SqlDbType.Int,
                                nTipoID.intValue().ToString());

                parameters[9] = new ProcedureParameters(
                                "@MensagemErro",
                                System.Data.SqlDbType.VarChar,
                                DBNull.Value,
                                ParameterDirection.Output);

                parameters[9].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                        "sp_Importacao_CustoGerador",
                        parameters);

                Resultado = parameters[9].Data.ToString();
            }
            catch (System.Exception E)
            {
                Resultado = E.Message.ToString();
            }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            ImportacaoCustoGerador();

            WriteResultXML(
                   DataInterfaceObj.getRemoteData(
                       "select " + (Resultado != null ? "'" + Resultado + "' " : "''") +
                       " as Resultado "));
        }
    }
}