﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class ReportsImportacao : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;

        private int RelatorioID;

        private int glb_nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        private int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);


        bool dirA1 = Convert.ToBoolean(HttpContext.Current.Request.Params["dirA1"]);
        bool dirA2 = Convert.ToBoolean(HttpContext.Current.Request.Params["dirA2"]);

        string glb_USERID = HttpContext.Current.Request.QueryString["glb_USERID"];


        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string nomeRel = Request.Params["RelatorioID"].ToString();
                RelatorioID = Convert.ToInt32(Request.Params["RelatorioID"].ToString());

                switch (nomeRel)
                {
                    case "40220":
                        ImportacaoAndamento();
                        break;

                    case "40221":
                        Instrucao();
                        break;

                    case "40222":
                        Adendo();
                        break;

                    case "40223":
                        Itens();
                        break;

                    case "40224":
                        NVE();
                        break;

                    case "40225":
                        VFEI();
                        break;
                }
            }
        }
        public void ImportacaoAndamento()
        {
            string strSQL;
            string Title = "ImportacoesEmAndamento";

            strSQL = "SELECT  " +
                    " a.ImportacaoID  " +
                    " , a.ProcessoImportacao  " +
                    " , b.Fantasia AS Empresa  " +
                    " , dbo.fn_Recursos_Campos(a.EstadoID, 2) as EstadoImportacao " +
                    " , null as Situacao " +
                    " , c.Fantasia AS Importador  " +
                    " , ISNULL(null, SPACE(0)) AS Marcas  " +
                    " , a.DI  " +
                    " , a.dtRegistro  " +
                    " , a.RecintoAduaneiro  " +
                    " , a.Armazem  " +
                    " , e.Localidade as UFDesembaraco  " +
                    " , a.dtDesembaraco  " +
                    " , dbo.fn_TipoAuxiliar_Item(a.TipoFreteID,0) as TipoFrete  " +
                    " , a.Capatazia  " +
                    " , a.ValorFrete  " +
                    " , a.ValorSeguro  " +
                    " , a.TaxaMoeda  " +
                    " , a.ValorIOF  " +
                    " , a.DespesasAduaneiras  " +
                    " , (SELECT TOP 1 aa.Valor " +
	                        " FROM Importacao_Custos aa WITH(NOLOCK) " +
	                        " WHERE ((aa.ImportacaoID = a.ImportacaoID) AND (aa.Descricao = 'AFRMM') AND (aa.Valor IS NOT NULL)) " +
                            " ORDER BY aa.ImpCustoID ASC) AS ValorAFRMM " +
                    " , dbo.fn_TipoAuxiliar_Item(a.MeioTransporteID,0) as MeioTransporte  " +
                    " , dbo.fn_TipoAuxiliar_Item(a.TipoImportacaoID,0) as TipoImportacao     " +
                    " , ISNULL(f.Fantasia, SPACE(0)) AS AgenteCarga " +
                    " , ISNULL(g.Fantasia, SPACE(0)) AS PortoOrigem " +
                    " , ISNULL(h.Fantasia, SPACE(0)) AS Companhia " +
                    " , ISNULL(i.Fantasia, SPACE(0)) AS PortoDestino " +
                    " , a.dtAutorizacaoEmbarque  " +
                    " , a.DocumentoEmbarque  " +
                    " , a.dtDocumentoEmbarque  " +
                    " , a.dtChegada  " +
                    " , a.AdicionalTarifario  " +
                    " , a.Outros  " +
                    " , a.Containers  " +
                    " , a.Palletes  " +
                    " , a.Caixas  " +
                    " , a.PesoBruto  " +
                    " , dbo.fn_TipoAuxiliar_Item(a.TratamentoCargaID,0) as TratamentoCarga  " +
                    " , a.NumeroDTA " +
                    " , a.dtRegistroDTA  " +
                    " , a.dtEntradaDTA  " +
                    " , a.CanalDTAID  " +
                    " , a.dtNIC  " +
                    " , a.dtAutorizacao " +
                    " , ISNULL(j.Fantasia, SPACE(0)) AS PortoDestino " +
                    " , a.ProcessoDespachante  " +
                    " , a.CanalDIID  " +
                    " , a.dtEtiquetagem  " +
                    " , ISNULL(l.Fantasia, SPACE(0)) AS Transportadora " +
                    " , a.dtEntrega  " +
                    " , a.Lacre  " +
                    " , a.Observacao " +
                    " , a.Observacoes  " +
                    " FROM Importacao a WITH(NOLOCK) " +
                    " LEFT JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.EmpresaID) " +
                    " LEFT JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.ImportadorID) " +
                    " LEFT JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = a.ExportadorID_Original) " +
                    " LEFT JOIN Localidades e WITH(NOLOCK) ON (e.LocalidadeID = UFDesembaracoID) " +
                    " LEFT JOIN Pessoas f WITH(NOLOCK) ON (f.PessoaID = a.AgenteCargaID) " +
                    " LEFT JOIN Pessoas g WITH(NOLOCK) ON (g.PessoaID = a.PortoOrigemID) " +
                    " LEFT JOIN Pessoas h WITH(NOLOCK) ON (h.PessoaID = a.CompanhiaID) " +
                    " LEFT JOIN Pessoas i WITH(NOLOCK) ON (i.PessoaID = a.PortoDestinoID) " +
                    " LEFT JOIN Pessoas j WITH(NOLOCK) ON (j.PessoaID = a.DespachanteID) " +
                    " LEFT JOIN Pessoas l WITH(NOLOCK) ON (l.PessoaID = a.TransportadoraID)" +
                    "	WHERE a.EstadoID <> 48 " +
                    " AND a.EmpresaID = " + glb_nEmpresaID;

            arrSqlQuery = new string[,] {
                                        {"Query1", strSQL}  };

            if (Formato == 1)
            {
                Relatorio.CriarRelatório("ImportacaoEmAndamento", arrSqlQuery, "BRA", "Landscape", "1.00");
                Relatorio.CriarObjLabel("Fixo", "", "Importações em Andamentos", "10.13834", "0.1", "11", "#0113a0", "B", "Header", "4.51332", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Importações em Andamentos", "1.013834", "0.01", "11", "#0113a0", "B", "Header", "4.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "19.795", "0.1", "11", "black", "B", "Header", "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "1.9795", "0.01", "11", "black", "B", "Header", "3.72187", "Horas");
                Relatorio.CriarObjTabela("0", "0", "Query1", false, true);
                //Relatorio.CriarObjTabela("0", "0", "Query1", false, true);
                Relatorio.TabelaEnd();
            }
            else if (Formato == 2)
            {
                Relatorio.CriarRelatório("ImportacoesEmAndamento", arrSqlQuery, "BRA", "Excel");
                Relatorio.CriarObjLabelData("Now", "", "", "16.795", "0.0", "11", "black", "B", "Body", "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "1.6795", "0.0", "11", "black", "B", "Body", "3.72187", "Horas");
                Relatorio.CriarObjTabela("0.0", "0.0001", "Query1", false, true);
                //Relatorio.CriarObjTabela("0.00", "0.00001", "Query1", false, true);
                Relatorio.TabelaEnd();
            }
            Relatorio.CriarPDF_Excel(Title, Formato);
        }
        public void Instrucao()
        {
            int ImportacaoID = Convert.ToInt32(HttpContext.Current.Request.Params["ImportacaoID"]);
            string ProcessoImportacao = HttpContext.Current.Request.Params["ProcessoImportacao"].ToString();
            string Title = "Instrução de Embarque";
            string NomeArquivo = Title + " " + ProcessoImportacao;
            string strSQLTitle;
            string strSQL;
            string strSQLEmpresa;

            strSQLEmpresa = "SELECT * FROM Pessoas  " +
                                  "WHERE PessoaID = " + glb_nEmpresaID;

            strSQLTitle = "SELECT  a.ProcessoImportacao " +
                                " , 'Instrução de Embarque' as Relatorio " +
                                " , b.Nome AS Empresa " +
                                " , c.Numero " +
                                " , ISNULL(a.RecintoAduaneiroID, SPACE(0)) AS RecintoAduaneiroID " +
                                " , a.RecintoAduaneiro " +
                                " , dbo.fn_TipoAuxiliar_Item(a.TipoFreteID,0) as IncotermID " +
                                " , dbo.fn_TipoAuxiliar_Item(a.MeioTransporteID,0) as Via " +
                                " , ISNULL(d.Fantasia, SPACE(0)) AS Destino " +
                                ", dbo.fn_Pessoa_Nome((CASE WHEN a.TipoImportacaoID = 1350 THEN ISNULL(a.EmpresaID, SPACE(0)) ELSE ISNULL(a.DespachanteID, 2) END)) AS Consignee " +
                                " , dbo.fn_Pessoa_Documento( " +
                                " (CASE WHEN a.TipoImportacaoID = 1350 THEN ISNULL(a.EmpresaID,SPACE(0)) ELSE ISNULL(a.DespachanteID, 2) END)  " +
                                " ,e.PaisID, e.UFID, 111, NULL, NULL " +
                                " ) AS DocumentoConsignee " +
                                " , dbo.fn_Pessoa_Endereco((CASE WHEN a.TipoImportacaoID = 1350 THEN ISNULL(a.EmpresaID, SPACE(0)) ELSE ISNULL(a.DespachanteID, 2) END), 2) as Endereco " +
                                " , a.Observacao " +
                            " FROM Importacao a WITH(NOLOCK) " +
                                " LEFT JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.EmpresaID) " +
                                " LEFT JOIN Pessoas_Documentos c WITH(NOLOCK) ON ((c.PessoaID = B.PessoaID) AND (c.TipoDocumentoID = 111)) " +
                                " LEFT JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = a.PortoDestinoID) " +
                                " LEFT JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (e.PessoaID = (CASE WHEN a.TipoImportacaoID = 1350 THEN ISNULL(a.EmpresaID, SPACE(0)) ELSE ISNULL(a.DespachanteID, 2) END)) " +
                            " WHERE a.ImportacaoID = " + ImportacaoID;


            strSQL = "SELECT  h.Conceito as NCM " +
                        " , e.Conceito as Marca " +
                        " , d.Modelo " +
                    " FROM Invoices a WITH(NOLOCK) " +
                        " LEFT JOIN Invoices_Itens b WITH(NOLOCK) ON (b.InvoiceID = a.InvoiceID)" +
                        " LEFT JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = b.ProdutoID) " +
                        " LEFT JOIN Conceitos e WITH(NOLOCK) ON (e.ConceitoID = d.MarcaID) " +
                        " LEFT JOIN RelacoesPesCon g WITH(NOLOCK) ON ((g.ObjetoID = d.ConceitoID) AND (g.SujeitoID = a.EmpresaID)) " +
                        " LEFT JOIN Conceitos h WITH(NOLOCK) ON (h.ConceitoID = g.ClassificacaoFiscalID) " +
                    " WHERE a.ImportacaoID =" + ImportacaoID;

            arrSqlQuery = new string[,] {
                                        {"strSQLTitle", strSQLTitle},
                                        {"Query1", strSQL}, 
                                        {"strSQLEmpresa", strSQLEmpresa}};

            if (Formato == 1)
            {
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Portrait", "0.80");

                //Labels Cabeçalho
                Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.2", "0.1", "11", "black", "B", "Header", "3.5", "");
                //Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.02", "0.01", "11", "black", "B", "Header", "3.5", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "8.34", "0.1", "11", "#0113a0", "B", "Header", "4.5", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "0.834", "0.01", "11", "#0113a0", "B", "Header", "4.5", "");
                Relatorio.CriarObjLabelData("Now", "", "", "17.2225", "0.1", "11", "black", "B", "Header", "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "1.72225", "0.01", "11", "black", "B", "Header", "3.72187", "Horas");
                Relatorio.CriarObjLinha("0.00", "0.58", "21", "", "Header");
                //Relatorio.CriarObjLinha("0.00", "0.058", "21", "", "Header");

                Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.2", "0.1", "18", " ", "Bold", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.02", "0.01", "18", " ", "Bold", "Body", "8", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "2.5", "0.1", "18", " ", "Bold", "Body", "18", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "0.25", "0.01", "18", " ", "Bold", "Body", "18", "");
                Relatorio.CriarObjLinha("0.00", "1.1", "21", "", "Body");
                //Relatorio.CriarObjLinha("0.00", "0.11", "21", "", "Body");

                Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.2", "1.8", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.02", "0.18", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "3.2", "1.8", "10", " ", "", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "0.32", "0.18", "10", " ", "", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.2", "2.3", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.02", "0.23", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "3.2", "2.3", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "0.32", "0.23", "10", " ", "L", "Body", "8", "");

                Relatorio.CriarObjLabel("Fixo", "", "Local:", "0.2", "2.8", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Local:", "0.02", "0.28", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "RecintoAduaneiroID", "3.2", "2.8", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "RecintoAduaneiroID", "0.32", "0.28", "10", " ", "L", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "Recinto Alfandegado:", "0.2", "3.2", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Recinto Alfandegado:", "0.02", "0.32", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "RecintoAduaneiro", "3.2", "3.2", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "RecintoAduaneiro", "0.32", "0.32", "10", " ", "L", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "Incoterm:", "0.2", "3.7", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Incoterm:", "0.02", "0.37", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "IncotermID", "3.2", "3.7", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "IncotermID", "0.32", "0.37", "10", " ", "L", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "Via:", "0.2", "4.2", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Via:", "0.02", "0.42", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Via", "3.2", "4.2", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Via", "0.32", "0.42", "10", " ", "L", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "Destino:", "0.2", "4.7", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Destino:", "0.02", "0.47", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Destino", "3.2", "4.7", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Destino", "0.32", "0.47", "10", " ", "L", "Body", "8", "");

                Relatorio.CriarObjLabel("Fixo", "", "Consignee:", "0.2", "5.7", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Consignee:", "0.02", "0.57", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Consignee", "3.2", "5.7", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Consignee", "0.32", "0.57", "10", " ", "L", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.2", "6.2", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.02", "0.62", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "DocumentoConsignee", "3.2", "6.2", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "DocumentoConsignee", "0.32", "0.62", "10", " ", "L", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "Endereço:", "0.2", "6.7", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Endereço:", "0.02", "0.67", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Endereco", "3.2", "6.7", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Endereco", "0.32", "0.67", "10", " ", "L", "Body", "8", "");

                Relatorio.CriarObjLabel("Fixo", "", "Observação:", "0.2", "7.7", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Observação:", "0.02", "0.77", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Observacao", "3.2", "7.7", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Observacao", "0.32", "0.77", "10", " ", "L", "Body", "8", "");


                Relatorio.CriarObjTabela("0.4", "9.0", "Query1", true, true);
                //Relatorio.CriarObjTabela("0.04", "0.90", "Query1", true, true);

                Relatorio.TabelaEnd();
            }
            else if (Formato == 2)
            {
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Excel");
                //Labels Cabeçalho
                Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.2", "0.1", "11", "black", "B", "Body", "3.5", "");
                //Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.02", "0.01", "11", "black", "B", "Body", "3.5", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "8.34", "0.1", "11", "#0113a0", "B", "Body", "4.5", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "0.834", "0.01", "11", "#0113a0", "B", "Body", "4.5", "");
                Relatorio.CriarObjLabelData("Now", "", "", "17.2225", "0.1", "11", "black", "B", "Body", "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "1.72225", "0.01", "11", "black", "B", "Body", "3.72187", "Horas");
                Relatorio.CriarObjLinha("0.00", "0.58", "21", "", "Body");
                //Relatorio.CriarObjLinha("0.00", "0.058", "21", "", "Body");

                Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.2", "0.1", "18", " ", "Bold", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.02", "0.01", "18", " ", "Bold", "Body", "8", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "2.5", "0.1", "18", " ", "Bold", "Body", "18", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "0.25", "0.01", "18", " ", "Bold", "Body", "18", "");
                Relatorio.CriarObjLinha("0.00", "1.1", "21", "", "Body");
                //Relatorio.CriarObjLinha("0.00", "0.11", "21", "", "Body");

                Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.2", "1.8", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.02", "0.18", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "3.2", "1.8", "10", " ", "", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "0.32", "0.18", "10", " ", "", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.2", "2.3", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.02", "0.23", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "3.2", "2.3", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "0.32", "0.23", "10", " ", "L", "Body", "8", "");

                Relatorio.CriarObjLabel("Fixo", "", "Local:", "0.2", "2.8", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Local:", "0.02", "0.28", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "RecintoAduaneiroID", "3.2", "2.8", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "RecintoAduaneiroID", "0.32", "0.28", "10", " ", "L", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "Recinto Alfandegado:", "0.2", "3.2", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Recinto Alfandegado:", "0.02", "0.32", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "RecintoAduaneiro", "3.2", "3.2", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "RecintoAduaneiro", "0.32", "0.32", "10", " ", "L", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "Incoterm:", "0.2", "3.7", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Incoterm:", "0.02", "0.37", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "IncotermID", "3.2", "3.7", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "IncotermID", "0.32", "0.37", "10", " ", "L", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "Via:", "0.2", "4.2", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Via:", "0.02", "0.42", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Via", "3.2", "4.2", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Via", "0.32", "0.42", "10", " ", "L", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "Destino:", "0.2", "4.7", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Destino:", "0.02", "0.47", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Destino", "3.2", "4.7", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Destino", "0.32", "0.47", "10", " ", "L", "Body", "8", "");

                Relatorio.CriarObjLabel("Fixo", "", "Consignee:", "0.2", "5.7", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Consignee:", "0.02", "0.57", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Consignee", "3.2", "5.7", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Consignee", "0.32", "0.57", "10", " ", "L", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.2", "6.2", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.02", "0.62", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "DocumentoConsignee", "3.2", "62", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "DocumentoConsignee", "0.32", "0.62", "10", " ", "L", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "Endereço:", "0.2", "6.7", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Endereço:", "0.02", "0.67", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Endereco", "3.2", "6.7", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Endereco", "0.32", "0.67", "10", " ", "L", "Body", "8", "");

                Relatorio.CriarObjLabel("Fixo", "", "Observação:", "0.2", "7.7", "10", " ", "B", "Body", "2.5054", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Observação:", "0.02", "0.77", "10", " ", "B", "Body", "2.5054", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Observacao", "3.2", "7.7", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Observacao", "0.32", "0.77", "10", " ", "L", "Body", "8", "");


                Relatorio.CriarObjTabela("0.4", "9", "Query1", true, true);
                //Relatorio.CriarObjTabela("0.04", "0.90", "Query1", true, true);

                Relatorio.TabelaEnd();

            }
            Relatorio.CriarPDF_Excel(NomeArquivo, Formato);
        }
        public void Adendo()
        {
            int ImportacaoID = Convert.ToInt32(HttpContext.Current.Request.Params["ImportacaoID"]);
            string ProcessoImportacao = HttpContext.Current.Request.Params["ProcessoImportacao"].ToString();
            string Title = "Adendo";
            string NomeArquivo = Title + " " + ProcessoImportacao;           
            string strSQLTitle;
            string strSQL;
            string strSQL2;
            string strSQLEmpresa;

            strSQLEmpresa = "SELECT * FROM Pessoas  " +
                                  "WHERE PessoaID = " + glb_nEmpresaID;

            strSQLTitle = "SELECT  a.ProcessoImportacao " +
                        "  , 'Adendo' as Relatorio " +
                        "    , b.Nome AS Empresa , c.Numero	  " +
                        " FROM Importacao a WITH(NOLOCK)" +
                            " LEFT JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.EmpresaID)" +
                            " LEFT JOIN Pessoas_Documentos c WITH(NOLOCK) ON ((c.PessoaID = B.PessoaID) AND (c.TipoDocumentoID = 111))" +
                        " WHERE a.ImportacaoID = " + ImportacaoID;


            strSQL = "	EXEC sp_Importacao_RelatorioAdendo " + ImportacaoID;

            arrSqlQuery = new string[,] {
                                        {"strSQLTitle", strSQLTitle},
                                        {"Query", strSQL},                                      
                                        {"strSQLEmpresa", strSQLEmpresa}};
            if (Formato == 1)
            {
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape", "3.10");

                //Labels Cabeçalho
                Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.2", "0.1", "11", "black", "B", "Header", "3.5", "");
                //Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.02", "0.01", "11", "black", "B", "Header", "3.5", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "15.34", "0.1", "11", "#0113a0", "B", "Header", "4.5", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "1.534", "0.01", "11", "#0113a0", "B", "Header", "4.5", "");
                Relatorio.CriarObjLabelData("Now", "", "", "24.2225", "0.1", "11", "black", "B", "Header", "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "2.42225", "0.01", "11", "black", "B", "Header", "3.72187", "Horas");
                Relatorio.CriarObjLinha("0.2", "0.58", "29", "", "Header");
                //Relatorio.CriarObjLinha("0.02", "0.058", "29", "", "Header");

                Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.2", "0.8", "18", " ", "Bold", "Header", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.02", "0.08", "18", " ", "Bold", "Header", "8", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "02.5", "0.8", "18", " ", "Bold", "Header", "18", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "0.25", "0.08", "18", " ", "Bold", "Header", "18", "");
                Relatorio.CriarObjLinha("0.2", "2.8", "29", "", "Header");
                //Relatorio.CriarObjLinha("0.02", "0.28", "29", "", "Header");

                Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.2", "1.8", "10", " ", "B", "Header", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.02", "0.18", "10", " ", "B", "Header", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "3.2", "1.8", "10", " ", "", "Header", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "0.32", "0.18", "10", " ", "", "Header", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.2", "2.3", "10", " ", "B", "Header", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.02", "0.23", "10", " ", "B", "Header", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "3.2", "2.3", "10", " ", "L", "Header", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "0.32", "0.23", "10", " ", "L", "Header", "8", "");

                Relatorio.CriarObjTabela("0.2", "0.1", "Query", true, false, true);
                //Relatorio.CriarObjTabela("0.02", "0.01", "Query", true, false, true);

                Relatorio.CriarObjColunaNaTabela("Item", "Coluna1", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna1", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("SKU", "Coluna2", false, "6.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna2", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Qtd", "Coluna3", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna3", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Modelo", "Coluna4", false, "2.8");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna4", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("NCM", "Coluna5", false, "3.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna5", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("NBS", "Coluna6", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna6", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Peso Bruto", "Coluna7", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna7", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Total Bruto", "Coluna8", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna8", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Peso Líquido", "Coluna9", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna9", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Total Líquido", "Coluna10", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna10", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Invoice", "Coluna11", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna11", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("CC", "Coluna12", false, "1.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna12", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("RC", "Coluna13", false, "1.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna13", "DetailGroup");

                Relatorio.TabelaEnd();

            }
            else if (Formato == 2)
            {
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Excel");
                //Labels Cabeçalho
                Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.2", "0.1", "11", "black", "B", "Body", "3.5", "");
                //Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.02", "0.01", "11", "black", "B", "Body", "3.5", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "15.34", "0.1", "11", "#0113a0", "B", "Body", "4.5", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "1.534", "0.01", "11", "#0113a0", "B", "Body", "4.5", "");
                Relatorio.CriarObjLabelData("Now", "", "", "24.2225", "0.1", "11", "black", "B", "Body", "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "2.42225", "0.01", "11", "black", "B", "Body", "3.72187", "Horas");


                Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.2", "0.1", "18", " ", "Bold", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.02", "0.01", "18", " ", "Bold", "Body", "8", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "2.5", "0.1", "18", " ", "Bold", "Body", "18", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "0.25", "0.01", "18", " ", "Bold", "Body", "18", "");


                Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.2", "1.03958", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.02", "0.103958", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "3.2", "1.03958", "10", " ", "", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "0.32", "0.103958", "10", " ", "", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.2", "1.5", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.02", "0.15", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "3.2", "1.5", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "0.32", "0.15", "10", " ", "L", "Body", "8", "");

                Relatorio.CriarObjTabela("0.1", "2.5", "Query", true, false);
                //Relatorio.CriarObjTabela("0.01", "0.25", "Query", true, false);


                Relatorio.CriarObjColunaNaTabela("Item", "Coluna1", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna1", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("SKU", "Coluna2", false, "6.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna2", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Qtd", "Coluna3", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna3", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Modelo", "Coluna4", false, "2.8");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna4", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("NCM", "Coluna5", false, "3.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna5", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("NBS", "Coluna6", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna6", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Peso Bruto", "Coluna7", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna7", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Total Bruto", "Coluna8", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna8", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Peso Líquido", "Coluna9", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna9", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Total Líquido", "Coluna10", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna10", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Invoice", "Coluna11", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna11", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("CC", "Coluna12", false, "1.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna12", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("RC", "Coluna13", false, "1.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna13", "DetailGroup");


                Relatorio.TabelaEnd();
            }
            Relatorio.CriarPDF_Excel(NomeArquivo, Formato);
        }
        public void Itens()
        {
            int ImportacaoID = Convert.ToInt32(HttpContext.Current.Request.Params["ImportacaoID"]);
            string Title = "Itens"; 
            string ProcessoImportacao = HttpContext.Current.Request.Params["ProcessoImportacao"].ToString();            
            string NomeArquivo = Title + " " + ProcessoImportacao;
            string strSQLTitle;
            string strSQL;
            string strSQLEmpresa;

            strSQLEmpresa = "SELECT * FROM Pessoas  " +
                                  "WHERE PessoaID = " + glb_nEmpresaID;

            strSQLTitle = "SELECT  a.ProcessoImportacao " +
                        "  , 'Itens' as Relatorio " +
                        "    , b.Nome AS Empresa , c.Numero	  " +
                        "	, ISNULL(a.Palletes, SPACE(0)) AS Palletes" +
                        "  , ISNULL(a.Caixas, SPACE(0)) AS Caixas" +
                        " FROM Importacao a WITH(NOLOCK)" +
                            " LEFT JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.EmpresaID)" +
                            " LEFT JOIN Pessoas_Documentos c WITH(NOLOCK) ON ((c.PessoaID = B.PessoaID) AND (c.TipoDocumentoID = 111))" +
                        " WHERE a.ImportacaoID = " + ImportacaoID;

            strSQL = " EXEC sp_Importacao_RelatorioItens " + ImportacaoID;

            arrSqlQuery = new string[,] {
                                        {"strSQLTitle", strSQLTitle},
                                        {"Query1", strSQL}, 
                                        {"strSQLEmpresa", strSQLEmpresa}};
            if (Formato == 1)
            {

                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape", "3.10");

                //Labels Cabeçalho

                Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.2", "0.1", "11", "black", "B", "Header", "3.5", "");
                //Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.02", "0.01", "11", "black", "B", "Header", "3.5", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "15.34", "0.1", "11", "#0113a0", "B", "Header", "4.5", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "1.534", "0.01", "11", "#0113a0", "B", "Header", "4.5", "");
                Relatorio.CriarObjLabelData("Now", "", "", "24.2225", "0.1", "11", "black", "B", "Header", "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "2.42225", "0.01", "11", "black", "B", "Header", "3.72187", "Horas");
                //Verificar com o Victor o motivo desta linha abaixo. Comentado temporariamente. BJBN 04/07/2016
                //Relatorio.CriarObjLabelPaginas("2.02225", "0.01", "11", "black", "B", "Header", "3.72187","4");
                Relatorio.CriarObjLinha("0.2", "0.58", "29", "", "Header");
                //Relatorio.CriarObjLinha("0.02", "0.058", "29", "", "Header");

                Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.2", "0.8", "18", " ", "Bold", "Header", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.02", "0.08", "18", " ", "Bold", "Header", "8", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "2.5", "0.8", "18", " ", "Bold", "Header", "18", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "0.25", "0.08", "18", " ", "Bold", "Header", "18", "");
                Relatorio.CriarObjLinha("0.2", "2.8", "29", "", "Header");
                //Relatorio.CriarObjLinha("0.02", "0.28", "29", "", "Header");

                Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.2", "1.8", "10", " ", "B", "Header", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.02", "0.18", "10", " ", "B", "Header", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "3.2", "1.8", "10", " ", "", "Header", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "0.32", "0.18", "10", " ", "", "Header", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.2", "2.3", "10", " ", "B", "Header", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.02", "0.23", "10", " ", "B", "Header", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "3.2", "2.3", "10", " ", "L", "Header", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "0.32", "0.23", "10", " ", "L", "Header", "8", "");

                Relatorio.CriarObjTabela("0.2", "0.1", "Query1", true, false, true);
                //Relatorio.CriarObjTabela("0.02", "0.01", "Query1", true, false, true);

                Relatorio.CriarObjColunaNaTabela("Item", "Coluna1", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna1", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("SKU", "Coluna2", false, "3.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna2", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Qtd", "Coluna3", false, "1.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna3", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Valor", "Coluna4", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna4", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Total", "Coluna5", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna5", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Produto", "Coluna6", false, "3.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna6", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Marca", "Coluna7", false, "2.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna7", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Modelo", "Coluna8", false, "3.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna8", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("NCM", "Coluna9", false, "3.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna9", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("II", "Coluna10", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna10", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("IPI", "Coluna11", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna11", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Pis", "Coluna12", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna12", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Cofins", "Coluna13", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna13", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("ICMS", "Coluna14", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna14", "DetailGroup");

                Relatorio.TabelaEnd();
            }
            else if (Formato == 2)
            {

                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Excel");

                //Labels Cabeçalho
                Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.2", "0.1", "11", "black", "B", "Body", "3.5", "");
                //Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.02", "0.01", "11", "black", "B", "Body", "3.5", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "15.34", "0.1", "11", "#0113a0", "B", "Body", "4.5", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "1.534", "0.01", "11", "#0113a0", "B", "Body", "4.5", "");
                Relatorio.CriarObjLabelData("Now", "", "", "24.2225", "0.1", "11", "black", "B", "Body", "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "2.42225", "0.01", "11", "black", "B", "Body", "3.72187", "Horas");

                Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.2", "0.1", "18", " ", "Bold", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.02", "0.01", "18", " ", "Bold", "Body", "8", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "2.5", "0.1", "18", " ", "Bold", "Body", "18", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "0.25", "0.01", "18", " ", "Bold", "Body", "18", "");

                Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.2", "1.03958", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.02", "0.103958", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "3.2", "1.03958", "10", " ", "", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "0.32", "0.103958", "10", " ", "", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.2", "1.5", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.02", "0.15", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "3.2", "1.5", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "0.32", "0.15", "10", " ", "L", "Body", "8", "");

                Relatorio.CriarObjTabela("0.2", "0.1", "Query1", true, false, true);
                //Relatorio.CriarObjTabela("0.02", "0.01", "Query1", true, false, true);

                Relatorio.CriarObjColunaNaTabela("Item", "Coluna1", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna1", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("SKU", "Coluna2", false, "3.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna2", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Qtd", "Coluna3", false, "1.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna3", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Valor", "Coluna4", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna4", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Total", "Coluna5", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna5", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Produto", "Coluna6", false, "3.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna6", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Marca", "Coluna7", false, "2.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna7", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Modelo", "Coluna8", false, "3.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna8", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("NCM", "Coluna9", false, "3.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna9", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("II", "Coluna10", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna10", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("IPI", "Coluna11", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna11", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Pis", "Coluna12", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna12", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Cofins", "Coluna13", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna13", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("ICMS", "Coluna14", false, "1.5");
                Relatorio.CriarObjCelulaInColuna("Query", "Coluna14", "DetailGroup");

                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(NomeArquivo, Formato);

        }
        public void VFEI()
        {
            int ImportacaoID = Convert.ToInt32(HttpContext.Current.Request.Params["ImportacaoID"]);
            string Title = "VFEI";
            string ProcessoImportacao = HttpContext.Current.Request.Params["ProcessoImportacao"].ToString();
            string NomeArquivo = Title + " " + ProcessoImportacao;            
            string strSQLTitle;
            string strSQL;
            string strSQLEmpresa;

            strSQLEmpresa = "SELECT * FROM Pessoas  " +
                                  "WHERE PessoaID = " + glb_nEmpresaID;

            strSQLTitle = "SELECT  a.ProcessoImportacao " +
                        " , b.Nome AS Empresa , dbo.fn_TipoAuxiliar_Item(a.EstadoID, 0) as Estado " +
                        "  , 'VFEI' as Relatorio " +
                        " , c.Numero	 " +
                        " FROM Importacao a WITH(NOLOCK)" +
                            " LEFT JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.EmpresaID)" +
                            " LEFT JOIN Pessoas_Documentos c WITH(NOLOCK) ON ((c.PessoaID = B.PessoaID) AND (c.TipoDocumentoID = 111))" +
                        " WHERE a.ImportacaoID = " + ImportacaoID;


            strSQL = "SELECT b.Item, " +
                "b.ProdutoID,  " +
                "ISNULL(d.Nome, SPACE(0)) as Fabricante,  " +
                "ISNULL(e.Nome, SPACE(0)) as Exportador, " +
                "(CASE WHEN c.FabricanteID = a.ExportadorID THEN 'O fabricante/produtor é o exportador' ELSE 'O fabricante/produtor não é o exportador' END) AS Vinculo,  " +
                "(CASE WHEN a.ExportadorID = 7 THEN 'Com vinculação, sem influencia no preço' ELSE 'Sem vinculação' END) AS VinculoExportador " +

                "FROM Invoices a WITH(NOLOCK) " +
                "LEFT JOIN Invoices_Itens b WITH(NOLOCK) ON (b.InvoiceID = a.InvoiceID) " +
                "LEFT JOIN Invoices_Itens_Fabricantes c WITH(NOLOCK) ON (c.InvItemID = b.InvItemID) " +
                "LEFT JOIN Pessoas d WITH(NOLOCK) ON  (d.PessoaID = c.FabricanteID) " +
                "LEFT JOIN Pessoas e WITH(NOLOCK) ON  (e.PessoaID = a.ExportadorID)" +
                " WHERE a.ImportacaoID =" + ImportacaoID;

            arrSqlQuery = new string[,] {
                                        {"strSQLTitle", strSQLTitle},
                                        {"Query1", strSQL}, 
                                        {"strSQLEmpresa", strSQLEmpresa}};

            if (Formato == 1)
            {
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape", "3.10");

                //Labels Cabeçalho
                Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.2", "0.1", "11", "black", "B", "Header", "3.5", "");
                //Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.02", "0.01", "11", "black", "B", "Header", "3.5", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "15.34", "0.1", "11", "#0113a0", "B", "Header", "4.5", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "1.534", "0.01", "11", "#0113a0", "B", "Header", "4.5", "");
                Relatorio.CriarObjLabelData("Now", "", "", "24.2225", "0.1", "11", "black", "B", "Header", "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "2.42225", "0.01", "11", "black", "B", "Header", "3.72187", "Horas");
                Relatorio.CriarObjLinha("0.2", "0.58", "29", "", "Header");
                //Relatorio.CriarObjLinha("0.02", "0.058", "29", "", "Header");

                Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.2", "0.8", "18", " ", "Bold", "Header", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.02", "0.08", "18", " ", "Bold", "Header", "8", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "2.5", "0.8", "18", " ", "Bold", "Header", "18", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "0.25", "0.08", "18", " ", "Bold", "Header", "18", "");
                Relatorio.CriarObjLinha("0.2", "2.8", "29", "", "Header");
                //Relatorio.CriarObjLinha("0.02", "0.28", "29", "", "Header");

                Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.2", "1.8", "10", " ", "B", "Header", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.02", "0.18", "10", " ", "B", "Header", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "3.2", "1.8", "10", " ", "", "Header", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "0.32", "0.18", "10", " ", "", "Header", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.2", "2.3", "10", " ", "B", "Header", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.02", "0.23", "10", " ", "B", "Header", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "3.2", "2.3", "10", " ", "L", "Header", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "0.32", "0.23", "10", " ", "L", "Header", "8", "");

                Relatorio.CriarObjTabela("0.1", "0.1", "Query1", true, false);
                //Relatorio.CriarObjTabela("0.01", "0.01", "Query1", true, false);

                Relatorio.CriarObjColunaNaTabela("Ítem", "Item", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Item", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Produto", "ProdutoID", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Fabricante", "Fabricante", false, "6.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Fabricante", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Exportador", "Exportador", false, "6.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Exportador", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Vinculo Fabricante/Exportador", "Vinculo", false, "7.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Vinculo", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Vinculo Exportador/Importador", "VinculoExportador", false, "6.0");
                Relatorio.CriarObjCelulaInColuna("Query", "VinculoExportador", "DetailGroup");

                Relatorio.TabelaEnd();
            }
            else if (Formato == 2)
            {
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Excel");
                //Labels Cabeçalho
                Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.2", "0.1", "11", "black", "B", "Body", "3.5", "");
                //Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.02", "0.01", "11", "black", "B", "Body", "3.5", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "15.34", "0.1", "11", "#0113a0", "B", "Body", "4.5", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "1.534", "0.01", "11", "#0113a0", "B", "Body", "4.5", "");
                Relatorio.CriarObjLabelData("Now", "", "", "24.2225", "0.1", "11", "black", "B", "Body", "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "2.42225", "0.01", "11", "black", "B", "Body", "3.72187", "Horas");


                Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.2", "0.1", "18", " ", "Bold", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.02", "0.01", "18", " ", "Bold", "Body", "8", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Estado", "2.5", "0.1", "18", " ", "Bold", "Body", "18", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Estado", "0.25", "0.01", "18", " ", "Bold", "Body", "18", "");


                Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.2", "1.03958", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.02", "0.103958", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "3.2", "1.03958", "10", " ", "", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "0.32", "0.103958", "10", " ", "", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.2", "1.5", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.02", "0.15", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "3.2", "1.5", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "0.32", "0.15", "10", " ", "L", "Body", "8", "");

                Relatorio.CriarObjTabela("0.1", "2.5", "Query1", true, false);
                //Relatorio.CriarObjTabela("0.01", "0.25", "Query1", true, false);

                Relatorio.CriarObjColunaNaTabela("Ítem", "Item", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Item", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Produto", "ProdutoID", false, "2.0");
                Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Fabricante", "Fabricante", false, "6.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Fabricante", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Exportador", "Exportador", false, "6.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Exportador", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Vinculo Fabricante/Exportador", "Vinculo", false, "7.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Vinculo", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Vinculo Exportador/Importador", "VinculoExportador", false, "6.0");
                Relatorio.CriarObjCelulaInColuna("Query", "VinculoExportador", "DetailGroup");


                Relatorio.TabelaEnd();

            }
            Relatorio.CriarPDF_Excel(NomeArquivo, Formato);
        }
        public void NVE()
        {
            int ImportacaoID = Convert.ToInt32(HttpContext.Current.Request.Params["ImportacaoID"]);
            string Title = "NVE - Nomenclatura de Valoração e Estatística";
            string ProcessoImportacao = HttpContext.Current.Request.Params["ProcessoImportacao"].ToString();
            string NomeArquivo = Title + " " + ProcessoImportacao;
            string strSQLTitle;
            string strSQL;
            string strSQLCaracteristica;
            string strSQLEmpresa;

            strSQLEmpresa = "SELECT * FROM Pessoas  " +
                                  "WHERE PessoaID = " + glb_nEmpresaID;

            strSQLTitle = "SELECT  a.ProcessoImportacao " +
                        " , b.Nome AS Empresa , dbo.fn_TipoAuxiliar_Item(a.EstadoID, 0) as Estado " +
                        "  , 'NVE - Nomenclatura de Valoração e Estatística' as Relatorio " +
                        " , c.Numero	 " +
                        " FROM Importacao a WITH(NOLOCK)" +
                            " LEFT JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.EmpresaID)" +
                            " LEFT JOIN Pessoas_Documentos c WITH(NOLOCK) ON ((c.PessoaID = B.PessoaID) AND (c.TipoDocumentoID = 111))" +
                        " WHERE a.ImportacaoID = " + ImportacaoID;


            strSQL = "SELECT c.ProdutoID as SKU " +
                    " , c.ProdutoID  " +
                    " , e.Conceito " +
                    " , d.Modelo " +
                    " FROM Importacao a WITH(NOLOCK) " +
                        " LEFT JOIN Invoices b WITH(NOLOCK) ON (b.ImportacaoID = a.ImportacaoID) " +
                        " LEFT JOIN Invoices_Itens c WITH(NOLOCK) ON (c.InvoiceID = b.InvoiceID) " +
                        " LEFT JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = c.ProdutoID) " +
                        " LEFT JOIN Conceitos e WITH(NOLOCK) ON (e.ConceitoID = d.ProdutoID) " +
                        " INNER JOIN Conceitos_NVE f WITH(NOLOCK) ON (f.ConceitoID = d.ConceitoID) " +
                " WHERE a.ImportacaoID =" + ImportacaoID;

            strSQLCaracteristica = " SELECT c.ProdutoID as SKU " +
                                    " , ISNULL(f.Atributo, SPACE(0)) as Atributo " +
                                    " , ISNULL(f.Descricao, SPACE(0)) as Descricao " +
                                    " , ISNULL(f.Ordem, SPACE(0)) as Ordem " +
                                    " , ISNULL(f.Especificacao, SPACE(0)) as Especificacao " +

                                    " FROM Importacao a WITH(NOLOCK) " +
                                        " LEFT JOIN Invoices b WITH(NOLOCK) ON (b.ImportacaoID = a.ImportacaoID) " +
                                        " LEFT JOIN Invoices_Itens c WITH(NOLOCK) ON (c.InvoiceID = b.InvoiceID) " +
                                        " LEFT JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = c.ProdutoID) " +
                                        " LEFT JOIN Conceitos e WITH(NOLOCK) ON (e.ConceitoID = d.ProdutoID) " +
                                        " INNER JOIN Conceitos_NVE f WITH(NOLOCK) ON (f.ConceitoID = d.ConceitoID) " +
                                     " WHERE a.ImportacaoID =" + ImportacaoID;

            arrSqlQuery = new string[,] {
                                        {"strSQLTitle", strSQLTitle},
                                        {"Query", strSQL}, 
                                        {"Query2", strSQLCaracteristica}, 
                                        {"strSQLEmpresa", strSQLEmpresa}};

            arrIndexKey = new string[,] { { "Query2", "SKU;Ordem", "SKU" } };


            if (Formato == 1)
            {
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape", "3.0599");

                //Labels Cabeçalho
                Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.2", "0.1", "11", "black", "B", "Header", "3.5", "");
                //Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.02", "0.01", "11", "black", "B", "Header", "3.5", "");
                Relatorio.CriarObjLabel("Fixo", "", "NVE", "15.34", "0.1", "11", "#0113a0", "B", "Header", "4.5", "");
                //Relatorio.CriarObjLabel("Fixo", "", "NVE", "1.534", "0.01", "11", "#0113a0", "B", "Header", "4.5", "");
                Relatorio.CriarObjLabelData("Now", "", "", "24.2225", "0.1", "11", "black", "B", "Header", "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "2.42225", "0.01", "11", "black", "B", "Header", "3.72187", "Horas");
                Relatorio.CriarObjLinha("0.2", "0.58", "29", "", "Header");
                //Relatorio.CriarObjLinha("0.02", "0.058", "29", "", "Header");

                Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.2", "0.8", "18", " ", "Bold", "Header", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.02", "0.08", "18", " ", "Bold", "Header", "8", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "2.5", "0.8", "18", " ", "Bold", "Header", "18", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "0.25", "0.08", "18", " ", "Bold", "Header", "18", "");
                Relatorio.CriarObjLinha("0.2", "2.8", "29", "", "Header");
                //Relatorio.CriarObjLinha("0.02", "0.28", "29", "", "Header");

                Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.2", "1.8", "10", " ", "B", "Header", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.02", "0.18", "10", " ", "B", "Header", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "3.2", "1.8", "10", " ", "", "Header", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "0.32", "0.18", "10", " ", "", "Header", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.2", "2.3", "10", " ", "B", "Header", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.02", "0.23", "10", " ", "B", "Header", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "3.2", "2.3", "10", " ", "L", "Header", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "0.32", "0.23", "10", " ", "L", "Header", "8", "");


                Relatorio.CriarObjTabela("0.2", "0.1", arrIndexKey, true, false);
                //Relatorio.CriarObjTabela("0.02", "0.01", arrIndexKey, true, false);

                Relatorio.CriarObjColunaNaTabela("SKU", "ProdutoID", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup", null, null, 0, true);

                Relatorio.CriarObjColunaNaTabela("Produto", "Conceito", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "Conceito", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Modelo", "Modelo", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "Modelo", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Atributo", "Atributo", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "Atributo", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Descrição", "Descricao", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "Descricao", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Ordem", "Ordem", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "Ordem", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Especificação", "Especificacao", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "Especificacao", "DetailGroup");

                Relatorio.TabelaEnd();
            }
            else if (Formato == 2)
            {
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Excel");
                //Labels Cabeçalho
                Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.2", "0.1", "11", "black", "B", "Body", "3.5", "");
                //Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.02", "0.01", "11", "black", "B", "Body", "3.5", "");
                Relatorio.CriarObjLabel("Fixo", "", "NVE", "15.34", "0.1", "11", "#0113a0", "B", "Body", "4.5", "");
                //Relatorio.CriarObjLabel("Fixo", "", "NVE", "1.534", "0.01", "11", "#0113a0", "B", "Body", "4.5", "");
                Relatorio.CriarObjLabelData("Now", "", "", "24.2225", "0.1", "11", "black", "B", "Body", "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "2.42225", "0.01", "11", "black", "B", "Body", "3.72187", "Horas");

                Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.2", "0.8", "18", " ", "Bold", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "ProcessoImportacao", "0.02", "0.08", "18", " ", "Bold", "Body", "8", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "2.5", "0.8", "18", " ", "Bold", "Body", "18", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Relatorio", "0.25", "0.08", "18", " ", "Bold", "Body", "18", "");

                Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.0", "1.8", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Importador:", "0.02", "0.18", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "3.2", "1.8", "10", " ", "", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Empresa", "0.32", "0.18", "10", " ", "", "Body", "8", "");
                Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.2", "2.3", "10", " ", "B", "Body", "2.0354", "");
                //Relatorio.CriarObjLabel("Fixo", "", "CNPJ:", "0.02", "0.23", "10", " ", "B", "Body", "2.0354", "");
                Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "3.2", "2.3", "10", " ", "L", "Body", "8", "");
                //Relatorio.CriarObjLabel("Query", "strSQLTitle", "Numero", "0.32", "0.23", "10", " ", "L", "Body", "8", "");

                Relatorio.CriarObjTabela("0.2", "4", arrIndexKey, true, false);
                //Relatorio.CriarObjTabela("0.02", "0.40", arrIndexKey, true, false);

                Relatorio.CriarObjColunaNaTabela("SKU", "ProdutoID", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup", null, null, 0, true);

                Relatorio.CriarObjColunaNaTabela("Produto", "Conceito", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "Conceito", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Modelo", "Modelo", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "Modelo", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Atributo", "Atributo", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "Atributo", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Descrição", "Descricao", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "Descricao", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Ordem", "Ordem", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "Ordem", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Especificação", "Especificacao", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "Especificacao", "DetailGroup");

                Relatorio.TabelaEnd();
            }
            Relatorio.CriarPDF_Excel(NomeArquivo, Formato);
        }

        private DataTable RequestDatatable(string SQLQuery, string NomeQuery)
        {
            string strConn = "";

            Object oOverflySvrCfg = null;

            DataTable Dt = new DataTable();

            DataSet DsContainer = new DataSet();

            oOverflySvrCfg = comCreateObject("OverflySvrCfg.OverflyMTS");
            strConn = ((OVERFLYSVRCFGLib.OverflyMTS)oOverflySvrCfg).DotNetDataBaseStrConn(DataInterfaceObj.ApplicationName);

            SqlCommand cmd = new SqlCommand(SQLQuery);
            using (SqlConnection con = new SqlConnection(strConn))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    cmd.CommandTimeout = 0;
                    sda.SelectCommand = cmd;

                    sda.Fill(DsContainer, NomeQuery);
                }
            }
            Dt = DsContainer.Tables[NomeQuery];
            return Dt;
        }
        public string translateTherm(string sTherm, string[,] aTranslate)
        {
            int nIdiomaDeID = Convert.ToInt32(HttpContext.Current.Request.Params["nIdiomaDeID"]);
            int nIdiomaParaID = Convert.ToInt32(HttpContext.Current.Request.Params["nIdiomaParaID"]);
            string retVal = "";
            int nSeek;

            if ((sTherm == null) || (sTherm == "") || (nIdiomaDeID == nIdiomaParaID) || (nIdiomaParaID == 246))
                return sTherm;

            nSeek = Assek(aTranslate, sTherm);

            if (nSeek >= 0)
                retVal = aTranslate[nSeek, 1];
            else
                retVal = "";

            return retVal;
        }
        public int Assek(string[,] aArray, string strToSeek)
        {
            int nFinal = (((aArray.Length) / 2) - 1);
            int retVal = -1;

            for (int i = 0; i <= nFinal; i++)
            {
                if (aArray[i, 0].CompareTo(strToSeek) == 0)
                {
                    retVal = i;
                    break;
                }
            }
            return retVal;
        }
        private static Object comCreateObject(string sProgID)
        {
            Type oType = Type.GetTypeFromProgID(sProgID);
            if (oType != null)
            {
                return Activator.CreateInstance(oType);
            }
            return null;
        }
    }
}