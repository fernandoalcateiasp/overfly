﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodimportacao.importacao.serverside
{
    public partial class Importacao_PedidoGerador : System.Web.UI.OverflyPage
    {
        private string Resultado;
        private Integer importacaoID;

        public Integer nImportacaoID
        {
            get { return importacaoID; }
            set { importacaoID = value; }
        }
        private Integer tipoPedidoID;

        public Integer nTipoPedidoID
        {
            get { return tipoPedidoID; }
            set { tipoPedidoID = value; }
        }

        /** Roda a procedure sp_Importacao_PedidoGerador */
        private void ImportacaoPedidoGerador()
        {
            ProcedureParameters[] parameters = new ProcedureParameters[3];
            try
            {

                parameters[0] = new ProcedureParameters(
                                "@ImportacaoID",
                                System.Data.SqlDbType.Int,
                                importacaoID.ToString());

                parameters[1] = new ProcedureParameters(
                                "@TipoPedidoID",
                                System.Data.SqlDbType.Int,
                                tipoPedidoID.ToString());

                parameters[2] = new ProcedureParameters(
                                "@Resultado",
                                System.Data.SqlDbType.VarChar,
                                DBNull.Value,
                                ParameterDirection.Output);

                parameters[2].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                        "sp_Importacao_PedidoGerador",
                        parameters);
            }
            catch (System.Exception E)
            {

                Resultado = E.Message.ToString();
            }

            Resultado = parameters[2].Data.ToString();

        }


        protected override void PageLoad(object sender, EventArgs e)
        {

            ImportacaoPedidoGerador();

            WriteResultXML(
                   DataInterfaceObj.getRemoteData(
                       "select " + (Resultado != null ? "'" + Resultado + "' " : "NULL") +
                       " as Resultado "));
        }
    }
}