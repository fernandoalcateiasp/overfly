﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modcomercial.submodimportacao.importacao.serverside
{
    public partial class Update_Invoce_Itens : System.Web.UI.OverflyPage
    {
        DataSet dsResult = null;
        private int InvIteNFeTradingID;

        private Integer invItemID;

        public Integer nInvItemID
        {
            get { return invItemID; }
            set { invItemID = value; }
        }
        
        private Integer InvIteNFeTradingID2;

        public Integer nInvIteNFeTradingID
        {
            get { return InvIteNFeTradingID2; }
            set { InvIteNFeTradingID2 = value; }
        }

        private Integer TipoImportacaoID;
        public Integer nTipoImportacaoID
        {
            get { return TipoImportacaoID; }
            set { TipoImportacaoID = value; }
        }
        
        private Integer TipoPedido;
        public Integer nTipoPedido
        {
            get { return TipoPedido; }
            set { TipoPedido = value; }
        }

        private String Despesas;

        public String nDespesas
        {
            get { return Despesas; }
            set { Despesas = value; }
        }

        private String Desconto;

        public String nDesconto
        {
            get { return Desconto; }
            set { Desconto = value; }
        }

        private String ValorUnitario;
        public String nValorUnitario
        {
            get { return ValorUnitario; }
            set { ValorUnitario = value; }
        }

        private String BasePIS;
        public String nBasePIS
        {
            get { return BasePIS; }
            set { BasePIS = value; }
        }

        private String AliquotaPIS;
        public String nAliquotaPIS
        {
            get { return AliquotaPIS; }
            set { AliquotaPIS = value; }
        }

        private String ValorPIS;
        public String nValorPIS
        {
            get { return ValorPIS; }
            set { ValorPIS = value; }
        }

        private String BaseCOFINS;
        public String nBaseCOFINS
        {
            get { return BaseCOFINS; }
            set { BaseCOFINS = value; }
        }

        private String AliquotaCOFINS;
        public String nAliquotaCOFINS
        {
            get { return AliquotaCOFINS; }
            set { AliquotaCOFINS = value; }
        }

        private String ValorCOFINS;
        public String nValorCOFINS
        {
            get { return ValorCOFINS; }
            set { ValorCOFINS = value; }
        }

        private String BaseIPI;
        public String nBaseIPI
        {
            get { return BaseIPI; }
            set { BaseIPI = value; }
        }

        private String AliquotaIPI;
        public String nAliquotaIPI
        {
            get { return AliquotaIPI; }
            set { AliquotaIPI = value; }
        }

        private String ValorIPI;
        public String nValorIPI
        {
            get { return ValorIPI; }
            set { ValorIPI = value; }
        }

        private String BaseICMS;
        public String nBaseICMS
        {
            get { return BaseICMS; }
            set { BaseICMS = value; }
        }

        private String AliquotaICMS;
        public String nAliquotaICMS
        {
            get { return AliquotaICMS; }
            set { AliquotaICMS = value; }
        }

        private String ValorICMS;
        public String nValorICMS
        {
            get { return ValorICMS; }
            set { ValorICMS = value; }
        }

        private String BaseICMSST;
        public String nBaseICMSST
        {
            get { return BaseICMSST; }
            set { BaseICMSST = value; }
        }

        private String AliquotaICMSST;
        public String nAliquotaICMSST
        {
            get { return AliquotaICMSST; }
            set { AliquotaICMSST = value; }
        }

        private String ValorICMSST;
        public String nValorICMSST
        {
            get { return ValorICMSST; }
            set { ValorICMSST = value; }
        }

        protected string SQL
        {
            get
            {
                string sql = "";
                string sql2 = "";

                InvIteNFeTradingID = InvIteNFeTradingID2.intValue();
                
                if (TipoPedido.intValue() == 2)
                    sql += " UPDATE Invoices_Itens SET DespesaTotal = " + Despesas +
                            " WHERE InvItemID = " + invItemID.ToString();
                else if (TipoPedido.intValue() == 3)
                    sql += " UPDATE Invoices_Itens SET DespesaTotal = " + Despesas + ", " +
                                "DescontoTotal = " + Desconto +
                            " WHERE InvItemID = " + invItemID.ToString();
                else
                {
                    if (InvIteNFeTradingID > 0)
                    {
                        sql += " UPDATE Invoices_Itens_NFeTrading SET ValorUnitario = " + ValorUnitario + " " + // ValorUnitario + " " +
                                    "WHERE InvIteNFeTradingID = " + InvIteNFeTradingID.ToString();

                        sql += " UPDATE Invoices_Itens_NFeTrading_Impostos " +
                                        "SET BaseCalculo = " + BaseCOFINS + ", " +
                                        "Aliquota = " + AliquotaCOFINS + ", " +
                                        "ValorImposto = " + ValorCOFINS + " " +
                                    "WHERE InvIteNFeTradingID = " + InvIteNFeTradingID.ToString() + " AND ImpostoID = 5 " +
                                " UPDATE Invoices_Itens_NFeTrading_Impostos " +
                                        "SET BaseCalculo = " + BasePIS + ", " +
                                        "Aliquota = " + AliquotaPIS + ", " +
                                        "ValorImposto = " + ValorPIS + " " +
                                    "WHERE InvIteNFeTradingID = " + InvIteNFeTradingID.ToString() + " AND ImpostoID = 4 " +
                                " UPDATE Invoices_Itens_NFeTrading_Impostos " +
                                        "SET BaseCalculo = " + BaseIPI + ", " +
                                        "Aliquota = " + AliquotaIPI + ", " +
                                        "ValorImposto = " + ValorIPI + " " +
                                    "WHERE InvIteNFeTradingID = " + InvIteNFeTradingID.ToString() + " AND ImpostoID = 8 " +
                                " UPDATE Invoices_Itens_NFeTrading_Impostos " +
                                        "SET BaseCalculo = " + BaseICMS + ", " +
                                        "Aliquota = " + AliquotaICMS + ", " +
                                        "ValorImposto = " + ValorICMS + " " +
                                    "WHERE InvIteNFeTradingID = " + InvIteNFeTradingID.ToString() + " AND ImpostoID = 9 " +
                                " UPDATE Invoices_Itens_NFeTrading_Impostos " +
                                        "SET BaseCalculo = " + BaseICMSST + ", " +
                                        "Aliquota = " + AliquotaICMSST + ", " +
                                        "ValorImposto = " + ValorICMSST + " " +
                                    "WHERE InvIteNFeTradingID = " + InvIteNFeTradingID.ToString() + " AND ImpostoID = 25 ";
                    }
                    else
                    {
                        sql2 = " INSERT INTO Invoices_Itens_NFeTrading (InvItemID, ValorUnitario) " +
                                    "SELECT " + invItemID.ToString() + ", " + ValorUnitario;

                        DataInterfaceObj.ExecuteSQLCommand(sql2);

                        sql2 = "SELECT InvIteNFeTradingID FROM Invoices_Itens_NFeTrading WITH(NOLOCK) WHERE invItemID = " + invItemID.ToString();
                        
                        dsResult = null;

                        dsResult = DataInterfaceObj.getRemoteData(sql2);

                        if (dsResult.Tables[1].Rows.Count > 0)
                        {
                            InvIteNFeTradingID = int.Parse(dsResult.Tables[1].Rows[0]["InvIteNFeTradingID"].ToString());
                        }

                        if (InvIteNFeTradingID > 0)
                        {
                            sql += " INSERT INTO Invoices_Itens_NFeTrading_Impostos (InvIteNFeTradingID, ImpostoID, BaseCalculo, Aliquota, ValorImposto) " +
                                        "SELECT " + InvIteNFeTradingID.ToString() + ", 4, " + BasePIS + ", " +
                                            AliquotaPIS + ", " + ValorPIS + " UNION ALL " +
                                        "SELECT " + InvIteNFeTradingID.ToString() + ", 5, " + BaseCOFINS + ", " +
                                            AliquotaCOFINS + ", " + ValorCOFINS + " UNION ALL " +
                                        "SELECT " + InvIteNFeTradingID.ToString() + ", 8, " + BaseIPI + ", " +
                                            AliquotaIPI + ", " + ValorIPI + " UNION ALL " +
                                        "SELECT " + InvIteNFeTradingID.ToString() + ", 9, " + BaseICMS + ", " +
                                            AliquotaICMS + ", " + ValorICMS + " UNION ALL " +
                                        "SELECT " + InvIteNFeTradingID.ToString() + ", 25, " + BaseICMSST + ", " +
                                            AliquotaICMSST + ", " + ValorICMSST;
                        }
                    }
                }

                return sql;
            }
        }


        protected override void PageLoad(object sender, EventArgs e)
        {
            DataInterfaceObj.ExecuteSQLCommand(SQL);

            WriteResultXML(DataInterfaceObj.getRemoteData("select '' as Resultado"));
        }
    }
}