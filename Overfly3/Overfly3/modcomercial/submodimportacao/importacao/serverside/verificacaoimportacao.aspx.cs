using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodimportacao.importacao.serverside
{
    public partial class verificacaoimportacao : System.Web.UI.OverflyPage
	{
		private static Integer zero = new Integer(0);

		private Integer importacaoID = zero;
		private Integer currEstadoId = zero;
		private Integer newEstadoID = zero;
        private Integer userId = zero;			
		
		private int response;
		private string mensagem;
		
		protected Integer nImportacaoID
		{
			set { importacaoID = value != null ? value : zero; }
		}
		
		protected Integer nCurrEstadoID
		{
			set { currEstadoId = value != null ? value : zero; }
		}

		protected Integer nNewEstadoID
		{
			set { newEstadoID = value != null ? value : zero; }
		}

        protected Integer nUserID
        {
            set { userId = value != null ? value : zero; }
        }
		
		protected void ImportacaoVerifica()
		{
			ProcedureParameters[] procparam = new ProcedureParameters[6];
			
			procparam[0] = new ProcedureParameters(
				"@ImportacaoID", 
				SqlDbType.Int,
                importacaoID.ToString());
			procparam[1] = new ProcedureParameters(
				"@EstadoDeID",
				SqlDbType.Int,
				currEstadoId.ToString()); 
			procparam[2] = new ProcedureParameters(
				"@EstadoParaID",
				SqlDbType.Int,
				newEstadoID.ToString());
            procparam[3] = new ProcedureParameters(
                "@UsuarioID",
                SqlDbType.Int,
                userId.ToString()); 
            procparam[4] = new ProcedureParameters(
				"@Resultado",
				SqlDbType.Int,
				DBNull.Value,
				ParameterDirection.InputOutput);            
			procparam[5] = new ProcedureParameters(
				"@Mensagem",
				SqlDbType.VarChar,
				DBNull.Value,
				ParameterDirection.InputOutput);
			procparam[5].Length = 8000;
			
			// Executa a procedure.
			DataInterfaceObj.execNonQueryProcedure(
				"sp_Importacao_Verifica",
				procparam
			);
			
			// Obt�m o resultado da execu��o.
			response = int.Parse(procparam[4].Data.ToString());
			mensagem = procparam[5].Data.ToString();
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
            ImportacaoVerifica();
			
			
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select " + response + " as Resultado, '" +
						mensagem + "' as Mensagem"
				)
			);
		}
	}
}
