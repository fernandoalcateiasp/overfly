﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modcomercial.submodimportacao.importacao.serverside
{
    public partial class AssociarInvoice : System.Web.UI.OverflyPage
    {


        private Integer nInvoiceID;

        public Integer InvoiceID
        {
            get { return nInvoiceID; }
            set { nInvoiceID = value; }
        }

        private Integer nImportacaoID;

        public Integer ImportacaoID
        {
            get { return nImportacaoID; }
            set { nImportacaoID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            int resultado = DataInterfaceObj.ExecuteSQLCommand("UPDATE Invoices SET ImportacaoID =  " + nImportacaoID.ToString() + " WHERE InvoiceID = " + InvoiceID.ToString());

            WriteResultXML(
                DataInterfaceObj.getRemoteData(" SELECT " + resultado + " AS Resultado")
            );

        }
    }
}