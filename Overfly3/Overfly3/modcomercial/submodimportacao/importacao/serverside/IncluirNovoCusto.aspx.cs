﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modcomercial.submodimportacao.importacao.serverside
{
    public partial class IncluirNovoCusto : System.Web.UI.OverflyPage
    {

        private Integer importacaoID;
        public Integer nImportacaoID
		{
			get { return importacaoID; }
            set { importacaoID = value; }
		}

		private Integer tipoCustoID;
        public Integer nTipoCustoID
		{
            get { return tipoCustoID; }
            set { tipoCustoID = value; }
		}

        private string descricao;
        public string sDescricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        
        private java.lang.Double valor;
        public java.lang.Double nValor
		{
            get { return valor; }
            set { valor = value; }
		}

        private string observacao;
        public string sObservacao
        {
            get { return observacao; }
            set { observacao = value; }
        }

		protected string SQL
		{
            get
			{
				string sql = "";

                sql += "INSERT Importacao_Custos(ImportacaoID, TipoCustoID, Descricao, Valor, Observacao)  " +
                            "SELECT " + nImportacaoID.intValue().ToString() + ", " + nTipoCustoID.intValue().ToString() + ", '" + sDescricao.ToString() + "', " + 
                                        nValor.doubleValue().ToString() + ", '" + sDescricao.ToString() + "'";

				return sql;
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.ExecuteSQLCommand(SQL);

			WriteResultXML(DataInterfaceObj.getRemoteData("select '' as Resultado"));
		}
    }
}