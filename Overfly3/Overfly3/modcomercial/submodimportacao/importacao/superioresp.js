/********************************************************************
js_especificsup.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1  ProcessoImportacao AS Processo, * , ' +
               'CONVERT(VARCHAR, dtPrevisaoEntrega, ' + DATE_SQL_PARAM + ') as V_dtPrevisaoEntrega,' +
               'CONVERT(VARCHAR, dtRegistro, '+DATE_SQL_PARAM+') as V_dtRegistro,' +
               'CONVERT(VARCHAR, dtDesembaraco, ' + DATE_SQL_PARAM + ') as V_dtDesembaraco,' +
               'CONVERT(VARCHAR, dtAutorizacaoEmbarque, ' + DATE_SQL_PARAM + ') as V_dtAutorizacaoEmbarque,' +
               'CONVERT(VARCHAR, dtDocumentoEmbarque, ' + DATE_SQL_PARAM + ') as V_dtDocumentoEmbarque,' +
               'CONVERT(VARCHAR, dtEmbarque, ' + DATE_SQL_PARAM + ') as V_dtEmbarque, ' +
               'CONVERT(VARCHAR, dtChegada, ' + DATE_SQL_PARAM + ') as V_dtChegada,' +
               'CONVERT(VARCHAR, dtRegistroDTA, ' + DATE_SQL_PARAM + ') as V_dtRegistroDTA,' +
               'CONVERT(VARCHAR, dtLiberacaoDTA, ' + DATE_SQL_PARAM + ') as V_dtLiberacaoDTA,' +
               'CONVERT(VARCHAR, dtEntradaDTA, ' + DATE_SQL_PARAM + ') as V_dtEntradaDTA,' +
               'CONVERT(VARCHAR, dtEtiquetagem, ' + DATE_SQL_PARAM + ') as V_dtEtiquetagem,' +
               'CONVERT(VARCHAR, dtNIC, ' + DATE_SQL_PARAM + ') as V_dtNIC,' +
               'CONVERT(VARCHAR, dtAutorizacao, ' + DATE_SQL_PARAM + ') as V_dtAutorizacao,' +
               'CONVERT(VARCHAR, dtEntrega, ' + DATE_SQL_PARAM + ') as V_dtEntrega,' +
               '(dbo.fn_Importacao_Totais(ImportacaoID, 1) + ISNULL(Capatazia, 0)) AS ValorFOB,' +
               '(dbo.fn_Importacao_Totais(ImportacaoID, 2) + ISNULL(Capatazia, 0)) AS ValorCIF,' +
               'CONVERT(NUMERIC(11,2), (dbo.fn_Importacao_Totais(ImportacaoID, 2) + ISNULL(Capatazia, 0)) * ISNULL(TaxaMoeda, 0)) AS ValorCIFConvertido,' +
               'dbo.fn_Importacao_Totais(ImportacaoID, 3) AS ValorII,' +          
               'dbo.fn_Importacao_Totais(ImportacaoID, 4) AS Quantidade,' +
               'dbo.fn_Importacao_Situacao(ImportacaoID) AS SituacaoID, ' +
               'dbo.fn_Importacao_SituacaoDescricao(ImportacaoID) AS Situacao, ' +
               '(SELECT SUM(aa.ValorTotalInvoice) ' +
                    'FROM Invoices aa WITH(NOLOCK) ' +
	                'WHERE (aa.ImportacaoID = Importacao.ImportacaoID)) AS TotalFOB, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=Importacao.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               '(SELECT	SUM(c.PesoLiquido * c.Quantidade) ' +
                   'FROM Importacao a WITH(NOLOCK) ' +
                      'INNER JOIN Importacao_Adicoes b WITH(NOLOCK) ON (b.ImportacaoID = a.ImportacaoID) ' +
                      'INNER JOIN Importacao_Adicoes_Itens c WITH(NOLOCK) ON (c.ImpAdicaoID = b.ImpAdicaoID) ' +
                   'WHERE a.ImportacaoID = Importacao.ImportacaoID) AS PesoLiquidoTotal, ' +
                '(SELECT COUNT(cc.RelacaoID) ' +
	               'FROM Invoices aa WITH(NOLOCK) ' +
	                   'INNER JOIN Invoices_Itens bb WITH(NOLOCK) ON aa.InvoiceID = bb.InvoiceID ' +
	                   'INNER JOIN RelacoesPesCon cc WITH(NOLOCK) ON bb.ProdutoID = cc.ObjetoID AND cc.SujeitoID = aa.EmpresaID ' +
	               'WHERE cc.EstadoID IN (1, 4) ' +
                       'AND aa.ImportacaoID = Importacao.ImportacaoID) AS ProdutosCD, ' +
                       '(SELECT SUM(CASE WHEN cc.RelacaoID IS NULL THEN 1 ELSE 0   END)' +
		            'FROM Invoices aa WITH (NOLOCK)' +
		                'INNER JOIN Invoices_Itens bb WITH (NOLOCK) ON aa.InvoiceID = bb.InvoiceID ' +
		                'LEFT JOIN RelacoesPesCon cc WITH (NOLOCK) ON bb.ProdutoID = cc.ObjetoID AND cc.SujeitoID = aa.EmpresaID ' +
		               'WHERE aa.ImportacaoID = Importacao.ImportacaoID) AS ProdutosSemR, ' +
                'dbo.fn_ProdutosSemRelacao(Importacao.ImportacaoID) AS ProdutosSemRelacao, ' +
                'dbo.fn_Importacao_ProdutosInconsistentes(Importacao.ImportacaoID) AS ProdutosInconsistentes ' +
               'FROM Importacao WITH(NOLOCK)  ' +
               'WHERE ProprietarioID = '+ nID + 'ORDER BY ImportacaoID DESC';
    else
        sSQL = 'SELECT ProcessoImportacao AS Processo, *, ' +
               'CONVERT(VARCHAR, dtPrevisaoEntrega, ' + DATE_SQL_PARAM + ') as V_dtPrevisaoEntrega,' +
               'CONVERT(VARCHAR, dtRegistro, '+DATE_SQL_PARAM+') as V_dtRegistro,' +
               'CONVERT(VARCHAR, dtDesembaraco, ' + DATE_SQL_PARAM + ') as V_dtDesembaraco,' +
               'CONVERT(VARCHAR, dtAutorizacaoEmbarque, ' + DATE_SQL_PARAM + ') as V_dtAutorizacaoEmbarque,' +
               'CONVERT(VARCHAR, dtDocumentoEmbarque, ' + DATE_SQL_PARAM + ') as V_dtDocumentoEmbarque,' +
               'CONVERT(VARCHAR, dtEmbarque, ' + DATE_SQL_PARAM + ') as V_dtEmbarque, ' +
               'CONVERT(VARCHAR, dtChegada, ' + DATE_SQL_PARAM + ') as V_dtChegada,' +
               'CONVERT(VARCHAR, dtRegistroDTA, ' + DATE_SQL_PARAM + ') as V_dtRegistroDTA,' +
               'CONVERT(VARCHAR, dtLiberacaoDTA, ' + DATE_SQL_PARAM + ') as V_dtLiberacaoDTA,' +
               'CONVERT(VARCHAR, dtEntradaDTA, ' + DATE_SQL_PARAM + ') as V_dtEntradaDTA,' +
               'CONVERT(VARCHAR, dtEtiquetagem, ' + DATE_SQL_PARAM + ') as V_dtEtiquetagem,' +
               'CONVERT(VARCHAR, dtNIC, ' + DATE_SQL_PARAM + ') as V_dtNIC,' +
               'CONVERT(VARCHAR, dtAutorizacao, ' + DATE_SQL_PARAM + ') as V_dtAutorizacao,' +
               'CONVERT(VARCHAR, dtEntrega, ' + DATE_SQL_PARAM + ') as V_dtEntrega,' +
               '(dbo.fn_Importacao_Totais(ImportacaoID, 1)  + ISNULL(Capatazia, 0)) AS ValorFOB,' +
               '(dbo.fn_Importacao_Totais(ImportacaoID, 2) + ISNULL(Capatazia, 0)) AS ValorCIF,' +
               'CONVERT(NUMERIC(11,2), (dbo.fn_Importacao_Totais(ImportacaoID, 2) + ISNULL(Capatazia, 0)) * ISNULL(TaxaMoeda, 0)) AS ValorCIFConvertido,' +
               'dbo.fn_Importacao_Totais(ImportacaoID, 3) AS ValorII,' +               
               'dbo.fn_Importacao_Totais(ImportacaoID, 4) AS Quantidade,' +
               'dbo.fn_Importacao_Situacao(ImportacaoID) AS SituacaoID, ' +
               'dbo.fn_Importacao_SituacaoDescricao(ImportacaoID) AS Situacao, ' +
               '(SELECT SUM(aa.ValorTotalInvoice) ' +
                    'FROM Invoices aa WITH(NOLOCK) ' +
	                'WHERE (aa.ImportacaoID = Importacao.ImportacaoID)) AS TotalFOB, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=Importacao.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               '(SELECT   SUM (c.PesoLiquido * c.Quantidade) ' +
                   'FROM Importacao a WITH(NOLOCK) ' +
                      'INNER JOIN Importacao_Adicoes b WITH(NOLOCK) ON (b.ImportacaoID = a.ImportacaoID) ' +
                      'INNER JOIN Importacao_Adicoes_Itens c WITH(NOLOCK) ON (c.ImpAdicaoID = b.ImpAdicaoID) ' +
                   'WHERE a.ImportacaoID = Importacao.ImportacaoID) AS PesoLiquidoTotal, ' +
                '(SELECT COUNT(cc.RelacaoID) ' +
	               'FROM Invoices aa WITH(NOLOCK) ' +
	                   'INNER JOIN Invoices_Itens bb WITH(NOLOCK) ON aa.InvoiceID = bb.InvoiceID ' +
	                   'INNER JOIN RelacoesPesCon cc WITH(NOLOCK) ON bb.ProdutoID = cc.ObjetoID AND cc.SujeitoID = aa.EmpresaID ' +
	               'WHERE cc.EstadoID IN (1, 4) ' +
                       'AND aa.ImportacaoID = Importacao.ImportacaoID) AS ProdutosCD, ' +
                '(SELECT SUM(CASE WHEN cc.RelacaoID IS NULL THEN 1 ELSE 0   END)' +
		            'FROM Invoices aa WITH (NOLOCK)' +
		                'INNER JOIN Invoices_Itens bb WITH (NOLOCK) ON aa.InvoiceID = bb.InvoiceID '+
		                'LEFT JOIN RelacoesPesCon cc WITH (NOLOCK) ON bb.ProdutoID = cc.ObjetoID AND cc.SujeitoID = aa.EmpresaID '+
		               'WHERE aa.ImportacaoID = Importacao.ImportacaoID) AS ProdutosSemR, ' +
                'dbo.fn_ProdutosSemRelacao(Importacao.ImportacaoID) AS ProdutosSemRelacao, ' +
                'dbo.fn_Importacao_ProdutosInconsistentes(Importacao.ImportacaoID) AS ProdutosInconsistentes ' +
               'FROM Importacao WITH(NOLOCK) WHERE ImportacaoID = ' + nID + ' ORDER BY ImportacaoID DESC';

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;
    
    sql = 'SELECT *, ' +
                'CONVERT(VARCHAR, dtPrevisaoEntrega, ' + DATE_SQL_PARAM + ') as V_dtPrevisaoEntrega,' +
                'CONVERT(VARCHAR, dtRegistro, ' + DATE_SQL_PARAM + ') AS V_dtRegistro,' +
                'CONVERT(VARCHAR, dtDesembaraco, ' + DATE_SQL_PARAM + ') AS V_dtDesembaraco,' +
                'CONVERT(VARCHAR, dtAutorizacaoEmbarque, ' + DATE_SQL_PARAM + ') as V_dtAutorizacaoEmbarque,' +
                'CONVERT(VARCHAR, dtDocumentoEmbarque, ' + DATE_SQL_PARAM + ') as V_dtDocumentoEmbarque,' +
                'CONVERT(VARCHAR, dtEmbarque, ' + DATE_SQL_PARAM + ') as V_dtEmbarque, ' +
                'CONVERT(VARCHAR, dtChegada, ' + DATE_SQL_PARAM + ') as V_dtChegada,' +
                'CONVERT(VARCHAR, dtRegistroDTA, ' + DATE_SQL_PARAM + ') as V_dtRegistroDTA,' +
                'CONVERT(VARCHAR, dtLiberacaoDTA, ' + DATE_SQL_PARAM + ') as V_dtLiberacaoDTA,' +
                'CONVERT(VARCHAR, dtEntradaDTA, ' + DATE_SQL_PARAM + ') as V_dtEntradaDTA,' +
                'CONVERT(VARCHAR, dtEtiquetagem, ' + DATE_SQL_PARAM + ') as V_dtEtiquetagem,' +
                'CONVERT(VARCHAR, dtNIC, ' + DATE_SQL_PARAM + ') as V_dtNIC,' +
                'CONVERT(VARCHAR, dtAutorizacao, ' + DATE_SQL_PARAM + ') as V_dtAutorizacao,' +
                'CONVERT(VARCHAR, dtEntrega, ' + DATE_SQL_PARAM + ') as V_dtEntrega,' +
                '0 AS ValorFOB, ' +
                '0 AS ValorCIF, ' +
                '0 AS ValorCIFConvertido, ' +
                '0 AS ValorII,' +
                '0 AS Quantidade, ' +
                '0 AS SituacaoID, ' +
                '\'\' AS Situacao, ' +
                '0 AS TotalFOB, ' +
                '0 AS Prop1, 0 AS Prop2, ' +
                '0 AS PesoLiquidoTotal, ' +
                '0 AS ProdutosCD, ' +
                '0 AS ProdutosSemR, ' +
                '\'\' AS ProdutosInconsistentes ' +
            'FROM Importacao WHERE ImportacaoID = 0';
    
    dso.SQL = sql;          
}              
