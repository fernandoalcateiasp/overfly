/********************************************************************
modalrelacoes.js

Library javascript para o modalrelacoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var modPesqrelacoes_Timer = null;

var dsoPesq = new CDatatransport("dsoPesq");
var dsoCmbadicao = new CDatatransport("dsoCmbadicao");
var dsoAtualiza = new CDatatransport("dsoAtualiza");
var dsoSelAdicaoID = new CDatatransport("dsoSelAdicaoID");
var dsoCmbfabricante = new CDatatransport("dsoCmbfabricante");
var dsoCmbNCM = new CDatatransport("dsoCmbNCM");

var glb_GridAnterior = 'Adi��es';
var glb_GridAdicoesEditado = false;
var glb_GridAdicoesItensEditado = false;
var glb_ContDSO = 0;

var glb_CurrEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

var ESTADO_A = 41;
var ESTADO_C = 1;
var ESTADO_F = 48;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES

NOTA: Ver tambem o arquivo commonrelacoes.js

FUNCOES GERAIS:
window_onload()
setupPage()
btn_onclick(ctl)
startPesq(strPesquisa)
dataToReturn()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	window_onload_1stPart();

	// ajusta o body do html
	with (modalrelacoesBody)
	{
		style.backgroundColor = 'transparent';
		scroll = 'no';
		style.visibility = 'visible';
	}

	// configuracao inicial do html
	setupPage();   

	// mostra a janela modal com o arquivo carregado
	showExtFrame(window, true);

    //
	modalFrame = getFrameInHtmlTop('frameModal');
	modalFrame.style.top = 49;
	modalFrame.style.left = 0;

	// coloca foco no campo apropriado
	if ( document.getElementById('selAdicaoID').disabled == false )
		selAdicaoID.focus();

	// Lista baseando-se na adi��o selecionada.
	btnListar_onclick(null);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Itens', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) 
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Aqui ajusta os controles
    adjustElementsInForm([['lblGrid', 'selGrid', 10, 1, -10, -10],
						  ['lblAdicao', 'selAdicaoID', 10, 1],
						  ['lblQuantidade', 'txtQuantidade', 6, 1],
						  ['btnIncluir', 'btn', btnWidth, 1],
						  ['btnDuplicar', 'btn', btnWidth, 1, -74],
						  ['btnExcluir', 'btn', btnWidth, 1],
						  ['btnListar', 'btn', btnWidth, 1],
						  ['btnGravar', 'btn', btnWidth, 1]], null, null, true);

    selAdicaoID.maxLength = 15;
    btnIncluir.style.left = 234;
    
    // Ajusta o divAdicao
    with (divAdicao.style) 
	{
		border = 'none';
		backgroundColor = 'transparent';
		left = ELEM_GAP;
		top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
		width = modWidth; //(30 * modWidth) + 24 + 20 + 42;
		height = modHeight;
	}
    
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = (txtQuantidade.offsetHeight * 2)  + txtQuantidade.offsetTop + (ELEM_GAP * 2);
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - divFG.offsetTop - (2 * ELEM_GAP);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    with (btnOK.style) {
        width = 0;
        height = 0;
    }
    
    with (btnCanc.style) {
        width = 0;
        height = 0;
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.style.width, 10) * 18;
    fg.Redraw = 2;
}


/********************************************************************
 * Bloqueio dos bot�es se o estado estiver em F.
 ********************************************************************/
function statusButtuns() {
    btnIncluir.disabled = false; // (glb_CurrEstadoID != ESTADO_C);
    btnDuplicar.disabled = false; // (glb_CurrEstadoID != ESTADO_C);
    btnExcluir.disabled = false; // (glb_CurrEstadoID != ESTADO_C);
    btnGravar.disabled = false; //(glb_CurrEstadoID != ESTADO_C);
}

/********************************************************************
 * Valida a seq��ncia ou a adi��o. Os dados preenchidos devem ser 
 * uma sequ��ncia de 1 a n. Onde n � o n�mero de linhas no grid.
 ********************************************************************/
function validaSequencia(fg, coluna) {
	var result = true;
	var linha = 0;
	var sequencia = new Array(fg.Rows);
	var nDesconsiderar = 0;
	
	// Copia os dados da coluna indicada.
    for(linha = 0; linha < fg.Rows - 1; linha++) {
		if(fg.TextMatrix(linha + 1, coluna) != "") {
			if(parseInt(fg.TextMatrix(linha + 1, coluna)) >= sequencia.length) {
				return false;
			}
			
			sequencia[parseInt(fg.TextMatrix(linha + 1, coluna)) - 1] = 
				parseInt(fg.TextMatrix(linha + 1, coluna)) - 1;
		}
	}
	
	if(glb_GridAnterior != 'Adi��es') 
	{			            
	    // Valida o array
	    for(linha = 0; linha < fg.Rows - 1; linha++) {
            if (fg.TextMatrix(linha + 1, getColIndexByColKey(fg, 'ImpAdicaoItemMaeID')) != '')
	            result &= true;
            else
                result &= (sequencia[linha] == (linha - nDesconsiderar));
    		
		    if(!result) {
			    return false;
		    }
	    }
    }
    else
    {
        // Valida o array
	    for(linha = 0; linha < fg.Rows - 1; linha++) {
		    result &= (sequencia[linha] == linha);
    		
		    if(!result) {
			    return false;
		    }
	    }
    }
	
	return true;
}

/********************************************************************
 * Verifica se pode alternar de adi��es para adi��es itens.
 ********************************************************************/
function deAdicaoParaAdicaoItens() {
    var linha;
    
    var adicaoPreenchido = true;
    var adicao;
    
    var aliquotaPreenchido = true;
    var aliquota;
    
    // Verifica se todas as adi��es est�o preenchidas.
    for(linha = 1; linha < fg.Rows; linha++) {
		adicaoPreenchido &= (fg.TextMatrix(linha, getColIndexByColKey(fg, 'Adicao')) != "");
		aliquotaPreenchido &= (fg.TextMatrix(linha, getColIndexByColKey(fg, 'AliquotaII*')) != "");
    }
    
    if(glb_GridAdicoesEditado) {
		alert("Grave as adi��es antes de mudar para os itens.");
		return false;
    }
    
	if(!adicaoPreenchido || fg.Rows <= 1) {
		alert("� necess�rio preencher todas as adi��es para se exibir os itens.");
		return false;
	}
	
	if(!aliquotaPreenchido) {
		alert("N�o � poss�vel prosseguir sem aliquot de II no cadastro.\nEntre em contato com o departamento de cadastro.");
		return false;
	}
/*
	if (glb_CurrEstadoID != ESTADO_C) 
	{
		alert("O estado do registro deve estar em C para se exibir os itens.");
		return false;
	}
*/
	if(!validaSequencia(fg, getColIndexByColKey(fg, 'Adicao'))) {
		alert("A coluna adi��es n�o cont�m dados v�lidos.\n" + 
			"Os valores devem come�ar em 1 e ir at� n.\n" + 
			"Onde n � o n�mero de linhas do grid.");
		return false;
	}
		
	return true;
}

/********************************************************************
Usuario clicou o botao btnDuplicar
********************************************************************/
function btnDuplicar_onclick(ctl)
{
    if ((txtQuantidade.value != '') && (txtQuantidade.value > 0) && 
        (txtQuantidade.value < fg.ValueMatrix(fg.row, getColIndexByColKey(fg, 'Qtd*'))) && 
        (fg.ValueMatrix(fg.row, getColIndexByColKey(fg, 'ImpAdicaoItemID')) != 0))
    {        
        var nImpAdicaoID = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ImpAdicaoID'));
        var nSequencia = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'Sequencia'));
        var nID = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ID*'));
        var sProduto = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'Produto*'));
        var sPartNumber = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'PartNumber*'));
        var nQtdAtualizada = (fg.ValueMatrix(fg.row, getColIndexByColKey(fg, 'Qtd*')) - txtQuantidade.value);
        var nQtdDuplicada = txtQuantidade.value;
        var nPeso = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'Peso'));
        var nValorUnitario = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ValorUnitario'));        
        var nImpAdicaoItemMaeID = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ImpAdicaoItemID'));
        var nPedItemID = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'PedItemID'));
        var nImportacaoID = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ImportacaoID'));
        
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'Qtd*')) = nQtdAtualizada;    
        
        fg.rows += 1;
        fg.row = fg.rows - 1;
        fg.TopRow = fg.rows - 1;            
        
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ImpAdicaoID')) = nImpAdicaoID;
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'Sequencia')) = nSequencia;
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ID*')) = nID;
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'Produto*')) = sProduto;
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'PartNumber*')) = sPartNumber;
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'Qtd*')) = nQtdDuplicada;
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'Peso')) = nPeso;
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ValorUnitario')) = nValorUnitario;
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ValorTotal*')) = '';
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ValorII*')) = '';
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ValorTotalII*')) = '';
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ImpAdicaoItemID')) = 0;
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ImpAdicaoItemMaeID')) = nImpAdicaoItemMaeID;
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'PedItemID')) = nPedItemID;
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ImportacaoID')) = nImportacaoID;
        
        txtQuantidade.value = '';
    }
    else if (fg.ValueMatrix(fg.row, getColIndexByColKey(fg, 'ImpAdicaoItemID')) == 0)
    {
        txtQuantidade.value = '';
        window.top.overflyGen.Alert('Esta j� � uma linha duplicada');
        return null;
    }
    else
    {
        txtQuantidade.value = '';
        window.top.overflyGen.Alert('Quantidade n�o permite para duplica��o');
        return null;
    }
}

/********************************************************************
Usuario clicou o botao btnIncluir
********************************************************************/
function btnIncluir_onclick(ctl)
{
    fg.rows += 1;
    fg.row = fg.rows - 1;
    fg.TopRow = fg.rows - 1;  
    fg.Col = 0;
}

/********************************************************************
Usuario clicou o botao btnListar
********************************************************************/
function btnListar_onclick(ctl)
{
    lockControlsInModalWin(true);
    
    setConnection(dsoPesq);
    
    // Se o controle est� nulo significa que foi precionado o 
    // bot�o 1 no inferior. Ent�o mostro as adi��es.
    if(ctl == null) {
		listarAdicoes();
    }
	// Se foi selecionado uma mudan�a no grid ou se pediu para listar...
	else if(ctl.id == "selGrid" || ctl.id == "btnListar" || ctl.id == "btnGravar") {
		// ... verifica se � pra mostrar as adi��es ou ...
		if(selGrid.value == "Adi��es") {			            
			listarAdicoes();
		} 
		// ... se � para mostrar os itens.
		else if(selGrid.value == "Itens") {
			// Se o grid anterior for adi��es, ...
			if(glb_GridAnterior == 'Adi��es') {
				// ... verifica, se for o caso, a permiss�o de se exibir os itens e, ...
				if(!deAdicaoParaAdicaoItens()) {
					lockControlsInModalWin(false);
					statusButtuns();
					
					selGrid.value = "Adi��es";
					
					return null;
				}

				// ... preenche a sele��o dos itens das adi��es.
				preencheSelAdicaoID();
			}
		    
		    // Lista os itens das adi�oes.			
			listarAdicoesItens();
		}
	}
	// Caso tenha-se selecionado uma adi��o na sele��o de adi��es,
	// mostra os itens desta adi��o.
	else if(ctl.id == "selAdicaoID") {
	    // Lista os itens das adi�oes.
		listarAdicoesItens();
    }
}

/********************************************************************
 * Lista as adi��es da importa��o.
 ********************************************************************/
function listarAdicoes() {
	// Desabilita a sele��o dos itens das adi��es.
	lblAdicao.style.visibility = 'hidden';	
	selAdicaoID.style.visibility = 'hidden';
	btnIncluir.style.visibility = 'inherit';
    btnDuplicar.style.visibility = 'hidden';
	lblQuantidade.style.visibility = 'hidden';
    txtQuantidade.style.visibility = 'hidden';			
	
	glb_ContDSO = 4;

    setConnection(dsoAtualiza);

	// Monta a consulta para gravar as adi��es.
    dsoAtualiza.SQL =  
		"SELECT * " +
		"FROM " +
			"Importacao_Adicoes a WITH(NOLOCK) " +
		"WHERE " +
			"a.ImportacaoID = " + sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ImportacaoID'].value") + " " +
		"ORDER BY a.Adicao";
		
	dsoAtualiza.ondatasetcomplete = listarAdicoes_DSC;
	dsoAtualiza.refresh();

	// Pega o sql de pesquisa.
	dsoPesq.SQL = "SELECT a.Adicao, a.AliquotaII, a.ValorDesconto, a.FabricanteID, " +
			"a.ClassificacaoFiscalID, a.ImpAdicaoID " +
		"FROM " +
			"Importacao_Adicoes a WITH(NOLOCK) " +
			"LEFT OUTER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.FabricanteID) " +
			"INNER JOIN Conceitos c  WITH(NOLOCK) ON (a.ClassificacaoFiscalID = c.ConceitoID) " +
			"INNER JOIN Pessoas_Enderecos d WITH(NOLOCK) ON (b.PessoaID = d.PessoaID) " +
			                                        "AND (d.Ordem = 1) " +
		    "INNER JOIN Localidades e WITH(NOLOCK) ON (d.PaisID = e.LocalidadeID) " +
		"WHERE " +
			"a.ImportacaoID = " + sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ImportacaoID'].value") +
		" ORDER BY a.Adicao";

	dsoPesq.ondatasetcomplete = listarAdicoes_DSC;
    dsoPesq.Refresh();
    
    //Combo de fabricantes
    setConnection(dsoCmbfabricante);

	strSQL = "SELECT \'\' AS PessoaID, \'\' AS Fantasia " +
	    "UNION ALL " +
	    "SELECT a.PessoaID AS PessoaID, (a.Fantasia + SPACE(1) + CHAR(40) + c.CodigoLocalidade2 + CHAR(41)) AS Fantasia " +
		"FROM Pessoas a WITH(NOLOCK) " +
		    "INNER JOIN Pessoas_Enderecos b WITH(NOLOCK) ON (a.PessoaID = b.PessoaID) " +
		    "INNER JOIN Localidades c WITH(NOLOCK) ON (b.PaisID = c.LocalidadeID) " +
		"WHERE (a.EstadoID = 2 AND a.EhFabrica = 1 AND b.EndFaturamento = 1 AND b.Ordem = 1) " +
		"GROUP BY a.PessoaID, a.Fantasia, c.CodigoLocalidade2 " +
		"ORDER BY Fantasia";

    dsoCmbfabricante.SQL = strSQL;
    dsoCmbfabricante.ondatasetcomplete = listarAdicoes_DSC;
    dsoCmbfabricante.Refresh();
    
    //Combo de NCM
    setConnection(dsoCmbNCM);

	strSQL = 'SELECT DISTINCT NCM.ConceitoID AS NCMID, NCM.ClassificacaoFiscal AS NCM ' +
                'FROM dbo.Importacao WITH(NOLOCK) ' +
	                'INNER JOIN dbo.Importacao_Pedidos ImportacaoPedidos WITH(NOLOCK) ON ImportacaoPedidos.ImportacaoID=dbo.Importacao.ImportacaoID ' +
	                'INNER JOIN dbo.Pedidos_Itens Itens WITH(NOLOCK) ON Itens.PedidoID=ImportacaoPedidos.PedidoID ' +
	                'INNER JOIN dbo.Conceitos Produtos WITH(NOLOCK) ON Produtos.ConceitoID=Itens.ProdutoID ' +
	                'INNER JOIN dbo.RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON ProdutosEmpresa.SujeitoID=Importacao.EmpresaID AND  ' +
		                'ProdutosEmpresa.ObjetoID=Itens.ProdutoID AND  ' +
		                'ProdutosEmpresa.TipoRelacaoID=61 ' +
	                'INNER JOIN dbo.Conceitos NCM WITH(NOLOCK) ON NCM.ConceitoID=ProdutosEmpresa.ClassificacaoFiscalID ' +
	                'LEFT OUTER JOIN dbo.Conceitos_Impostos NCMAliquotas WITH(NOLOCK) ON NCMAliquotas.ConceitoID=NCM.ConceitoID AND NCMAliquotas.ImpostoID=7 ' +
                'WHERE Importacao.ImportacaoID=' + sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ImportacaoID'].value") + ' ' +
                'ORDER BY NCM.ClassificacaoFiscal';

    dsoCmbNCM.SQL = strSQL;
    dsoCmbNCM.ondatasetcomplete = listarAdicoes_DSC;
    dsoCmbNCM.Refresh();
}

/********************************************************************
 * Retorno de pagina asp no servidor com dados para preencher grid
 ********************************************************************/
function listarAdicoes_DSC() {
    glb_ContDSO--;
	if (glb_ContDSO != 0)
	    return null;
	
    startGridInterface(fg);
    
	fg.FontSize = '8';
    fg.FrozenCols = 0;
    fg.Editable = false;

	// Monta o grid para as adi��es.
    headerGrid(fg,['Adi��o',
                   'Fabricante', 
				   'Class Fiscal',
				   'Al�quota II',
				   'Valor Desconto',
				   'ImpAdicaoID'], [5]);

	fillGridMask(fg,dsoPesq,['Adicao',
							'FabricanteID',
							'ClassificacaoFiscalID',
							'AliquotaII*',
							'ValorDesconto*',
							'ImpAdicaoID'],
							 ['999','','','999.99','999999999.99',''],
							 ['###','','','##0.00','###,###,##0.00','']);

	removeCombo(fg);
    insertcomboData(fg, getColIndexByColKey(fg, 'FabricanteID'), dsoCmbfabricante, 'Fantasia', 'PessoaID');
    insertcomboData(fg, getColIndexByColKey(fg, 'ClassificacaoFiscalID'), dsoCmbNCM, 'NCM', 'NCMID');

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;
    
    fg.Editable = true; // (glb_CurrEstadoID == ESTADO_C);
    lockControlsInModalWin(false);
    statusButtuns();
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
        btnOK.disabled = false;
    } else {
        btnOK.disabled = true;    
	}

	// Guarda o grid atual para a pr�xima mudan�a.
	glb_GridAnterior = 'Adi��es';
}

/********************************************************************
 * Lista os itens das adi��es da importa��o.
 ********************************************************************/
function listarAdicoesItens() {
	lblAdicao.style.visibility = 'inherit';
	selAdicaoID.style.visibility = 'inherit';
    btnIncluir.style.visibility = 'hidden';
    btnDuplicar.style.visibility = 'inherit';
	lblQuantidade.style.visibility = 'inherit';
    txtQuantidade.style.visibility = 'inherit';			

    glb_ContDSO = 3;
    
    //Atualiza dados
    setConnection(dsoAtualiza);
    
    if(selAdicaoID.value != 0) {
		dsoAtualiza.SQL = "select * from Importacao_Adicoes_Itens " +
						"where ImpAdicaoID = " + selAdicaoID.value;
	} else {
		dsoAtualiza.SQL = "select * " +
			"from Importacao_Adicoes_Itens WITH(NOLOCK) " +
			"where ImpAdicaoItemID in " +
				"(" +
					"select " +
						"Importacao_Adicoes_Itens.ImpAdicaoItemID " +
					"from " +
						"Importacao_Adicoes_Itens WITH(NOLOCK) " +
						"LEFT OUTER JOIN Importacao_Adicoes WITH(NOLOCK) ON (Importacao_Adicoes.ImpAdicaoID = Importacao_Adicoes_Itens.ImpAdicaoID) " +
					"where " +
						"Importacao_Adicoes_Itens.ImportacaoID = " + 
						sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ImportacaoID'].value") + 
				")";
	}
	
	dsoAtualiza.ondatasetcomplete = listarAdicoesItens_DSC;
	dsoAtualiza.refresh();    

    //Preenche Grid
    dsoPesq.SQL = "SELECT " +
			"b.Adicao, " +
			"a.Sequencia, " +
			"c.ProdutoID AS ID, " +
			"d.Conceito AS Produto, " +
			"d.PartNumber AS PartNumber, " +
			"a.Quantidade AS Qtd, " +
			"ISNULL(a.PesoLiquido, e.PesoLiquitoUnitario) AS Peso, " +
			"a.Quantidade * a.PesoLiquido AS PesoTotal, " +
			"a.ValorUnitario, " +
			"a.Quantidade * a.ValorUnitario as ValorTotal, " +
			"dbo.fn_Importacao_Item_II(a.ImpAdicaoItemID, 2) AS ValorII, " +
			"(a.Quantidade * dbo.fn_Importacao_Item_II(a.ImpAdicaoItemID, 2)) AS ValorTotalII, " +
			"a.ImpAdicaoItemID, " +
			"a.ImpAdicaoItemMaeID, " +
			"a.PedItemID, " +
			"a.ImpAdicaoID, " +
			"a.ImportacaoID " +
		"FROM " +
			"Importacao_Adicoes_Itens a WITH(NOLOCK) " +
			"LEFT JOIN Importacao_Adicoes b WITH(NOLOCK) ON (a.ImpAdicaoID = b.ImpAdicaoID) " +
			"INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (a.PedItemID = c.PedItemID) " +
            "INNER JOIN Pedidos cc WITH(NOLOCK) ON (cc.PedidoID = c.PedidoID) " +
			"INNER JOIN Conceitos d WITH(NOLOCK) ON (c.ProdutoID = d.ConceitoID) " +
            "LEFT OUTER JOIN Invoices_Itens e WITH(NOLOCK) ON (e.PedItemID = c.PedItemID) " +
        "WHERE cc.EstadoID <> 5 ";
		
	if(parseInt(selAdicaoID.value) > 0) {
		dsoPesq.SQL += " AND b.ImpAdicaoID = " + selAdicaoID.value;
	} else {
		dsoPesq.SQL += " AND a.ImpAdicaoItemID in " +
				"(" +
					"select " +
						"Importacao_Adicoes_Itens.ImpAdicaoItemID " +
					"from " +
						"Importacao_Adicoes_Itens " +
						"LEFT JOIN Importacao_Adicoes WITH(NOLOCK) ON (Importacao_Adicoes.ImpAdicaoID = Importacao_Adicoes_Itens.ImpAdicaoID) " +
					"where " +
						"Importacao_Adicoes_Itens.ImportacaoID = " + 
						sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ImportacaoID'].value") + 
				")";
	}
	
	dsoPesq.SQL += " order by b.Adicao, (CASE WHEN a.Sequencia IS NULL THEN 1 ELSE 0 END), a.Sequencia";
	
	dsoPesq.ondatasetcomplete = listarAdicoesItens_DSC;
    dsoPesq.Refresh();
    
    //Combo Adi��o
    setConnection(dsoCmbadicao);

    dsoCmbadicao.SQL = "SELECT DISTINCT " +
			"a.Adicao AS Adicao, " +			
			"a.ImpAdicaoID AS ImpAdicaoID " +
		"FROM " +
			"Importacao_Adicoes a WITH(NOLOCK) " +
		"WHERE a.ImportacaoID = " + sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ImportacaoID'].value") + " " +
		"ORDER BY a.Adicao";
			
    dsoCmbadicao.ondatasetcomplete = listarAdicoesItens_DSC;
    dsoCmbadicao.Refresh();
}

/********************************************************************
 * Lista os itens das adi��es da importa��o.
 ********************************************************************/
function listarAdicoesItens_DSC() {
    glb_ContDSO--;
    
    if (glb_ContDSO != 0)
        return null;
    
    startGridInterface(fg);
    
	fg.FontSize = '8';
    fg.FrozenCols = 0;
    fg.Editable = false;

	// Monta o grid para os itens.
	headerGrid(fg,['Adi��o',
				   'Seq��ncia',
				   'ID',
				   'Produto',
				   'Part Number',
				   'Qtd',
				   'Peso Un',
				   'Peso Total',
				   'Valor Unit�rio',
				   'Valor Total',
				   'Valor II',
				   'Valor Total II',
				   'ImpAdicaoItemID',
				   'ImpAdicaoItemMaeID',
				   'PedItemID',
				   'ImportacaoID'], [12,13,14,15]);

	fillGridMask(fg,dsoPesq,['ImpAdicaoID',
						   'Sequencia',
						   'ID*',
						   'Produto*',
						   'PartNumber*',
						   'Qtd*',
						   'Peso',
						   'PesoTotal*',
						   'ValorUnitario',
						   'ValorTotal*',
						   'ValorII*',
						   'ValorTotalII*',
						   'ImpAdicaoItemID',
						   'ImpAdicaoItemMaeID',
						   'PedItemID',
						   'ImportacaoID'],
							 ['','999','','','','99999' ,'9999.999999999','9999.999999999','99999999999.9999999','99999999999.99','99999999999.9999999','99999999999.99','','','',''],
							 ['','999','','','','##,##0','####,#########0.000000000','####,#########0.000000000','##,###,###,##0.0000000','##,###,###,##0.00','##,###,###,##0.0000000','##,###,###,##0.00','','','','']);
	
	alignColsInGrid(fg,[0,1,2,5,6,7,8,9,10,11]);
    
	
	fg.Editable = true; //(glb_CurrEstadoID == ESTADO_C);
	
	fg.AutoSizeMode = 0;
	fg.AutoSize(0,fg.Cols-1);
	fg.ColWidth(getColIndexByColKey(fg, 'ValorTotal*')) = 100 * 14;    
	fg.ColWidth(getColIndexByColKey(fg, 'ValorTotalII*')) = 100 * 12;
	fg.Redraw = 2;
	
	removeCombo(fg);
	insertcomboData(fg, getColIndexByColKey(fg, 'ImpAdicaoID'), dsoCmbadicao, 'Adicao', 'ImpAdicaoID');

    lockControlsInModalWin(false);
    statusButtuns();
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
        btnOK.disabled = false;
    } else {
        btnOK.disabled = true;
	}
					    
	// Guarda o grid atual para a pr�xima mudan�a.
	glb_GridAnterior = 'Itens';
}

/********************************************************************
Usuario clicou o botao btnExcluir
********************************************************************/
function btnExcluir_onclick(ctl)
{
    lockControlsInModalWin(true);
	
	// Grava as adi�oes.
	if(selGrid.value == "Adi��es") {
        excluirAdicoes();
	}
	else if (selGrid.value == "Itens")
	{
	    excluirAdicoesItens();
	}
}

/********************************************************************
Usuario clicou o botao btnGravar
********************************************************************/
function btnGravar_onclick(ctl)
{
    lockControlsInModalWin(true);
	
	// Grava as adi�oes.
	if(selGrid.value == "Adi��es") {
        atualizarAdicoes();
	}
	// Monta a consulta para gravar os itens.
	else if(selGrid.value == "Itens") {
		atualizarAdicoesItens();
	}
}

/********************************************************************
 * Grava os dados do grid na coluna adi��es dos dados do DSO.
 ********************************************************************/
function atualizarAdicoes() 
{
    glb_GridAdicoesEditado = false;
    
	var linha;
	var impAdicaoID;
	var adicao;
	var fabricanteID;
	var classificacaoFiscalID;
	
	// Passa pelas linhas do GRID.
	for(linha = 1; linha < fg.Rows; linha++) {
		// Pega o ID que deve ser atualizado.
		impAdicaoID = fg.TextMatrix(linha, getColIndexByColKey(fg, 'ImpAdicaoID'));
		
		if (impAdicaoID == '')
		{
		    if ((fg.ValueMatrix(linha, getColIndexByColKey(fg, 'Adicao')) == 0) || 
		        (fg.ValueMatrix(linha, getColIndexByColKey(fg, 'FabricanteID')) == 0) ||
		        (fg.ValueMatrix(linha, getColIndexByColKey(fg, 'ClassificacaoFiscalID')) == 0))
		    {
                window.top.overflyGen.Alert("Existem linhas no grid com clunas em branco");
                lockControlsInModalWin(false);
                statusButtuns();
                
                return	null;	        
		    }
            else
            {
		        dsoAtualiza.recordset.AddNew();
		        dsoAtualiza.recordset['ImportacaoID'].value = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ImportacaoID'].value");
		        dsoAtualiza.recordset['Adicao'].value = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'Adicao'));
		        dsoAtualiza.recordset['FabricanteID'].value = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'FabricanteID'));
		        dsoAtualiza.recordset['ClassificacaoFiscalID'].value = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'ClassificacaoFiscalID'));
            }
        }
		else
		{
		    // Filtra os registros.
		    dsoAtualiza.recordset.Find('ImpAdicaoID', parseInt(impAdicaoID));
		    // Se n�o houver registros, passa para a pr�xima linha.
		    if(dsoAtualiza.recordset.EOF) {
			    continue;
		    }
    		
		    // Pega os dados editaveis do grid.
		    adicao = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'Adicao'));
		    fabricanteID = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'FabricanteID'));
		    classificacaoFiscalID = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'ClassificacaoFiscalID'));

		    // Atualiza o dado no DSO.
		    dsoAtualiza.recordset["Adicao"].value = (adicao == 0) ? null : parseInt(adicao);
		    dsoAtualiza.recordset["FabricanteID"].value = (fabricanteID == 0) ? null : parseInt(fabricanteID);
		    dsoAtualiza.recordset["ClassificacaoFiscalID"].value = (classificacaoFiscalID == 0) ? null : parseInt(classificacaoFiscalID);
		    
        }
	}
	
	dsoAtualiza.ondatasetcomplete = atualizaFinish_DSC;
	dsoAtualiza.SubmitChanges();
}

/********************************************************************
Exclui os dados do grid na coluna adi��es dos dados do DSO.
*********************************************************************/
function excluirAdicoes()
{
    // Passa pelas linhas do GRID.    
    dsoAtualiza.recordset.Find('ImpAdicaoID', fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ImpAdicaoID')));
    if (!(dsoAtualiza.recordset.EOF && dsoAtualiza.recordset.BOF))
    {
        dsoAtualiza.recordset.Delete();
        fg.RemoveItem(fg.Row);
        lockControlsInModalWin(false);
        statusButtuns();
    }
}

/********************************************************************
Exclui os dados do grid na coluna adi��es dos dados do DSO.
*********************************************************************/
function excluirAdicoesItens()
{
    // Passa pelas linhas do GRID.    
    dsoAtualiza.recordset.Find('ImpAdicaoItemID', fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ImpAdicaoItemID')));
    if ((!(dsoAtualiza.recordset.EOF && dsoAtualiza.recordset.BOF)) && 
        (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ImpAdicaoItemMaeID')) != ''))
    {
        dsoAtualiza.recordset.Delete();
        
        var nImpAdicaoItemMaeID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ImpAdicaoItemMaeID'));
        var Qtd = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Qtd*'));
        var LinhaDeletar = fg.row;
        
        var Linha = 0;
        while (Linha < fg.rows)
        {
            if (fg.ValueMatrix(Linha, getColIndexByColKey(fg, 'ImpAdicaoItemID')) == nImpAdicaoItemMaeID)
            {
                fg.TextMatrix(Linha, getColIndexByColKey(fg, 'Qtd*')) = (fg.ValueMatrix(Linha, getColIndexByColKey(fg, 'Qtd*')) + Qtd);
                Linha = fg.rows;
            }
            else
                Linha++;
        }
        
        fg.editable = false;	
        fg.RemoveItem(LinhaDeletar);
        fg.row = LinhaDeletar - 1;
        fg.AutoSizeMode = 0;
	    fg.AutoSize(0,fg.Cols-1);	    
	    fg.Redraw = 2;
	    fg.editable = true;
        lockControlsInModalWin(false);
        statusButtuns();
    }
    else
    {
        window.top.overflyGen.Alert("Somente linhas Duplicadas podem ser excluidas");
        lockControlsInModalWin(false);
        statusButtuns();
        
        return null;
    }
}
	
/********************************************************************
 * Grava os dados na tabela Importacao_Adicoes_Itens.
 ********************************************************************/
function atualizarAdicoesItens() {
    glb_GridAdicoesItensEditado = false;
	
/*	
	if(!validaSequencia(fg, getColIndexByColKey(fg, "Sequencia"))) {
        window.top.overflyGen.Alert("A coluna seq��ncia n�o cont�m dados v�lidos.\n" + 
			"Os valores devem come�ar em 1 e ir at� n.\n" + 
			"Onde n � o n�mero de linhas do grid.");
        
        lockControlsInModalWin(false);
        statusButtuns();
        
		return null;            
	}
*/
	var linha;
	var impAdicaoItemID;
	var impAdicaoID;
	var sequencia;
	var valorUnitario;
	var qtd;
	var ImportacaoID;
	var PesoUnitario;
	
	// Passa pelas linhas do GRID.
	for(linha = 1; linha < fg.Rows; linha++) {
		// Pega o ID do item da adi��o.
		impAdicaoItemID = fg.TextMatrix(linha, getColIndexByColKey(fg, 'ImpAdicaoItemID'));

		if (impAdicaoItemID == 0)
		{
    	    dsoAtualiza.recordset.AddNew();
            dsoAtualiza.recordset['ImpAdicaoID'].value = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'ImpAdicaoID'));
            dsoAtualiza.recordset['Sequencia'].value = (fg.ValueMatrix(linha, getColIndexByColKey(fg, 'Sequencia')) == 0 ? null : fg.TextMatrix(linha, getColIndexByColKey(fg, 'Sequencia')));
            dsoAtualiza.recordset['Quantidade'].value = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'Qtd*'));
            dsoAtualiza.recordset['PesoLiquido'].value = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'Peso'));
            dsoAtualiza.recordset['ValorUnitario'].value = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'ValorUnitario'));
            dsoAtualiza.recordset['ImpAdicaoItemMaeID'].value = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'ImpAdicaoItemMaeID'));
            dsoAtualiza.recordset['PedItemID'].value = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'PedItemID'));
            dsoAtualiza.recordset['ImportacaoID'].value = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'ImportacaoID'));
		}
		else
		{
		    // Busca o registro a atualizar.
		    dsoAtualiza.recordset.Find("ImpAdicaoItemID", impAdicaoItemID);
    		
		    // Se o registro n�o existir passa para a pr�xima linha.
		    if(dsoAtualiza.recordset.EOF) { continue; }

		    // Pega os dados editaveis do grid.
		    sequencia = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'Sequencia'));
		    valorUnitario = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'ValorUnitario'));
		    qtd = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'Qtd*'));
		    ImportacaoID = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'ImportacaoID'));
		    impAdicaoID = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'ImpAdicaoID'));
		    PesoUnitario = fg.ValueMatrix(linha, getColIndexByColKey(fg, 'Peso'));

		    // Grava os dados no DSO.
		    dsoAtualiza.recordset["Sequencia"].value = (sequencia == "") ? null : parseInt(sequencia);
		    dsoAtualiza.recordset["ValorUnitario"].value = (valorUnitario == "") ? null : parseFloat(valorUnitario);
		    dsoAtualiza.recordset["Quantidade"].value = (qtd == "") ? null : parseInt(qtd);
		    dsoAtualiza.recordset["ImportacaoID"].value = (ImportacaoID == "") ? null : parseInt(ImportacaoID);
		    dsoAtualiza.recordset["ImpAdicaoID"].value = (impAdicaoID == 0) ? null : parseInt(impAdicaoID);
		    dsoAtualiza.recordset["PesoLiquido"].value = (PesoUnitario == 0) ? null : PesoUnitario;
        }
	}
	
	dsoAtualiza.ondatasetcomplete = atualizaFinish_DSC;
	dsoAtualiza.SubmitChanges();
}

/********************************************************************
Atualiza os dados na tabela Importacao_Adicoes_Itens.
********************************************************************/
function atualizaFinish_DSC() {
    btnListar_onclick(btnGravar);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function preencheSelAdicaoID() {
	setConnection(dsoSelAdicaoID);
	
	dsoSelAdicaoID.SQL = "SELECT ImpAdicaoID AS fldID, Adicao AS fldName " +
				"FROM Importacao_Adicoes WITH(NOLOCK) " +
				"WHERE ImportacaoID = " + glb_nImportacaoID + " " +
				"ORDER BY fldName";

	dsoSelAdicaoID.ondatasetcomplete = preencheSelAdicaoID_DSC;
	
	dsoSelAdicaoID.refresh();
}

function preencheSelAdicaoID_DSC() {
    var i;
	
	// Elimina as op��es anteriores para colocar as novas.
	for (i = selAdicaoID.length - 1; i >= 0 && !dsoSelAdicaoID.recordset.EOF; i--) {
		selAdicaoID.remove(i);
	}

	// Coloca a primeira op��o como vazia.
	var firstOption = document.createElement('option');

	// Preenche os dados.
	firstOption.text = "";
	firstOption.value = 0;
	
	// Insere o primeiro elemento.
	selAdicaoID.add(firstOption); 
  	
  	// Passa pelos registros para preenchet a sele��o.
	for(dsoSelAdicaoID.recordset.moveFirst(); !dsoSelAdicaoID.recordset.EOF; dsoSelAdicaoID.recordset.moveNext()) {
		// Se n�o houver nada a ser exibido, n�o mostra a op��o.
		
		if ((dsoSelAdicaoID.recordset["fldName"] == null) || (dsoSelAdicaoID.recordset["fldName"].value == null))
		{
			continue;
		}
		
		// Cria um novo elemento para a sele��o.
		var newOption = document.createElement('option');

		// Preenche os dados.
		newOption.text = dsoSelAdicaoID.recordset["fldName"].value;
		newOption.value = dsoSelAdicaoID.recordset["fldID"].value;
		
		// Insere o novo elemento.
		selAdicaoID.add(newOption); 
	}
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
	window.focus();

	if (ctl.id == btnOK.id )
		btnOK.focus();
	else if (ctl.id == btnCanc.id )
		btnCanc.focus();

	// esta funcao trava o html contido na janela modal
	lockControlsInModalWin(true);

	// 1. O usuario clicou o botao OK
	if (ctl.id == btnOK.id ) {
		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, dataToReturn() );    
	}
	// 2. O usuario clicou o botao Cancela
	else if (ctl.id == btnCanc.id ) {
		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
	}
}

/********************************************************************
Monta e retorna array de dados a serem enviados para o sub form que chamou
a modal
********************************************************************/
function dataToReturn()
{
    var aData = new Array();
    
	aData[0] = glb_sCaller;
	aData[1] = glb_nImportacaoID;
	aData[2] = glb_nImpAdicaoID;
	
    return aData;
}

/********************************************************************
Eventos de grid particulares desta pagina
********************************************************************/
function js_ModalAdicoes_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_ModalAdicoes_AfterEdit(Row, Col)
{
	if(selGrid.value == "Adi��es") {
		glb_GridAdicoesEditado = true;
	} 
	else if(selGrid.value == "Itens") {
		glb_GridAdicoesItensEditado = true;
	}
	
    if (glb_GridIsBuilding)
        return;

    if((Row>0) && (Col>0))
    {
        if (Col == getColIndexByColKey(fg, 'ValorUnitario'))
        {
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorUnitario')));
        }
        else if (Col == getColIndexByColKey(fg, 'Peso'))
        {
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, getColIndexByColKey(fg, 'Peso')));
            if (fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Peso')) > 0)
            {
                fg.TextMatrix(Row, getColIndexByColKey(fg, 'PesoTotal*')) = (fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Peso')) * fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Qtd*')));                
            }
            else
            {
                fg.TextMatrix(Row, getColIndexByColKey(fg, 'PesoTotal*')) = 0;
            }
        }
    }
    
    
    
}


function js_ModalAdicoesKeyPress(KeyAscii)
{
    ;
}

function js_fg_ModalAdicoesDblClick(grid, Row, Col)
{
	return;
}

function js_fg_ModalAdicoesBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;

	return true;
}

function js_fg_ModalAdicoesAfterRowColChange(grid, oldRow, oldCol, newRow, newCol)
{
	if (glb_GridIsBuilding)
		return null;
		
	return true;
}