/********************************************************************
modalRateios.js

Library javascript para o modalRateios.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_aEmpresaData = getCurrEmpresaData();
var glb_nTotalRows = 0;
var glb_sReadOnly = '';
var glb_TotalDespesas = 0;
var glb_TotalDesconto = 0;
var glb_nTipoImportacaoID = 0;
var glb_nSituacaoID = 0;
var glb_Incluir = 0;
var glb_controlID = 'btnListar';

var dsoItens = new CDatatransport("dsoItens");
var dsoGravar = new CDatatransport("dsoGravar");
var dsoGerarPedido = new CDatatransport("dsoGerarPedido");
var dsoFabricantes = new CDatatransport("dsoFabricantes");
var dsoVariaveis = new CDatatransport("dsoVariaveis");
var dsoDireitos = new CDatatransport("dsoDireitos");

var glb_bGerarPedido = false;
var glb_Row = 0;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalRateiosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);

    btnOK.disabled = true;
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Rateios', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    //var nSituacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'SituacaoID' + '\'' + '].value');

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));

    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 39;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = (parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP) + 40;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP) + 0;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = (parseInt(divFG.style.height, 10)) + 40;
    }

    // selRateio
    selRateio.maxLength = 8;  // aceita vinte e hum caracteres de digitacao
    with (selRateio.style) {
        top = 40;
        left = 10;
        width = (selRateio.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }

    with (lblRateio.style) {
        top = 25;
        left = 10;
        width = (selRateio.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }

    selFabricanteID.maxLength = 8;
    with (selFabricanteID.style)
    {
        top = 40;
        left = 140;
        width = (selRateio.maxLength + 8) * FONT_WIDTH + 20;
        heigth = 24;
    }

    with (lblFabricanteID.style)
    {
        top = 25;
        left = 140;
        width = (selFabricanteID.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    with (btnListar.style) {
        top = 40;
        left = 295;
        width = 80;
        height = 24;
    }

    with (btnGravar.style) {
        top = 40;
        left = 380;
        width = 80;
        height = 24;
    }

    with (btnGerarPedido.style) {
        top = 40;
        left = 465;
        width = 80;
        height = 24;
    }

    with (btnExcel.style) {
        top = 40;
        left = parseInt(btnGerarPedido.style.left, 10) + parseInt(btnGerarPedido.style.width, 10)  + 10;
        width = 80;
        height = 24;
    }

    btnGravar.disabled = true;
    btnExcel.disabled = true;

    btnListar.onclick = Listar;
    btnGerarPedido.onclick = btn_GerarPedido;
    btnExcel.onclick = gerarExcel;
    btnGravar.onclick = btn_gravarDespesas;
    selRateio.onchange = changeRateio;
    selFabricanteID.onchange = changeFabricante;

    startGridInterface(fg);
    fg.Redraw = 2;

    verificaDireitos();
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalRateiosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

function js_fg_modalRateiosDblClick(grid, Row, Col) {
    var relacaoID = grid.valueMatrix(Row, getColIndexByColKey(grid, 'RelacaoID'));

    if (Col == 3) {
        sendJSCarrier(getHtmlId(), 'SHOWRELPESCON',
                       new Array(glb_aEmpresaData[0], relacaoID));
    }
}

function js_modalRateiosKeyPress(KeyAscii) {
    ;
}

function js_modalRateios_ValidateEdit() {
    ;
}

function js_modalRateios_AfterEdit(Row, Col)
{
    var nInvItemID;
    var nDespesas;
    var nDesconto;
    var nAliquotaPIS;
    var nAliquotaCOFINS;
    var nAliquotaIPI;
    var nTotalProdutos;
    var nFOBTotal;
    var nResultado;
    var nFatorInternacao;
    var nTotalDespesa = 0;
    var nTotalDesconto = 0;
    var nPercentualEspecifico = 0;
    var sTipoPedido = '';

    //var nTipoImportacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoImportacaoID' + '\'' + '].value');

    if ((glb_nTipoImportacaoID == 1351) && (selRateio.value == 1))
    {
        fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[5, '#########', 'S'],
                                                             [6, '###,###,###,###.0000', 'S'],
                                                             [8, '###,###,###,###.0000', 'S'],
                                                             [10, '###,###,###,###.0000', 'S'],
                                                             [12, '###,###,###,###.0000', 'S'],
                                                             [13, '###,###,###,###.0000', 'S'],
                                                             [15, '###,###,###,###.0000', 'S'],
                                                             [16, '###,###,###,###.0000', 'S'],
                                                             [18, '###,###,###,###.0000', 'S'],
                                                             [19, '###,###,###,###.0000', 'S'],
                                                             [21, '###,###,###,###.0000', 'S'],
                                                             [22, '###,###,###,###.0000', 'S'],
                                                             [24, '###,###,###,###.0000', 'S'],
                                                             [25, '###,###,###,###.0000', 'S']]);
    }

    /*
    else
    {
        if ((Col == getColIndexByColKey(fg, 'Despesas')) || (Col == getColIndexByColKey(fg, 'Desconto')))
        {
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

            // Contabil
            if (selRateio.value == 2)
            {
                sTipoPedido = 'NFComplementar*';
                nPercentualEspecifico = fg.valueMatrix(Row, getColIndexByColKey(fg, 'RateioContabil*'));
            }
            // Gerencial
            if (selRateio.value == 3)
            {
                sTipoPedido = 'Gerencial*';
                nPercentualEspecifico = fg.valueMatrix(Row, getColIndexByColKey(fg, 'RateioGerencial*'));
            }

            nInvItemID = fg.textMatrix(Row, getColIndexByColKey(fg, 'InvItemID'));
            nDespesas = fg.valueMatrix(Row, getColIndexByColKey(fg, 'Despesas'));
            nDesconto = fg.valueMatrix(Row, getColIndexByColKey(fg, 'Desconto'));
            nAliquotaPIS = fg.valueMatrix(Row, getColIndexByColKey(fg, 'AliquotaPIS'));
            nAliquotaCOFINS = fg.valueMatrix(Row, getColIndexByColKey(fg, 'AliquotaCOFINS'));
            nAliquotaIPI = fg.valueMatrix(Row, getColIndexByColKey(fg, 'AliquotaIPI'));
            nTotalProdutos = fg.valueMatrix(Row, getColIndexByColKey(fg, 'TotalProdutos'));
            nFOBTotal = fg.valueMatrix(Row, getColIndexByColKey(fg, 'FOBTotal'));

            nResultado = (((nTotalProdutos + (nDespesas - nDesconto)) / (1 - (nAliquotaPIS + nAliquotaCOFINS))) * (1 + nAliquotaIPI));
            nFatorInternacao = (nResultado / nFOBTotal);

            fg.textMatrix(Row, getColIndexByColKey(fg, 'Resultado*')) = nResultado;
            fg.textMatrix(Row, getColIndexByColKey(fg, 'FatorInternacao*')) = nFatorInternacao;
            fg.textMatrix(Row, getColIndexByColKey(fg, sTipoPedido)) = (nDespesas * (nPercentualEspecifico / 100.00));

            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'Resultado*')) = treatNumericCell(fg.TextMatrix(Row, getColIndexByColKey(fg, 'Resultado*')));
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'FatorInternacao*')) = treatNumericCell(fg.TextMatrix(Row, getColIndexByColKey(fg, 'FatorInternacao*')));
            fg.textMatrix(Row, getColIndexByColKey(fg, sTipoPedido)) = treatNumericCell(fg.TextMatrix(Row, getColIndexByColKey(fg, sTipoPedido)));

            //nTotalDespesa = calculaDespesaTotal();

            gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[5, '#########', 'S'],
                                                                 [6, '###,###,###,###.0000', 'S'],
                                                                 [7, '###,###,###,###.0000', 'S'],
                                                                 [8, '###,###,###,###.0000', 'S'],
                                                                 [9, '###,###,###,###.0000', 'S'],
                                                                 [10, '###,###,###,###.0000', 'S'],
                                                                 [11, '###,###,###,###.00', 'S'],
                                                                 [12, '###,###,###,###.00', 'S'],
                                                                 [16, '###,###,###,###.00', 'S']]);

            nTotalDespesa = fg.ValueMatrix(1, getColIndexByColKey(fg, 'Despesas'));
            fg.TextMatrix(1, getColIndexByColKey(fg, 'Despesas')) = (glb_TotalDespesas - nTotalDespesa);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'Despesas')) = treatNumericCell(fg.TextMatrix(1, getColIndexByColKey(fg, 'Despesas')));

            nTotalDesconto = fg.ValueMatrix(1, getColIndexByColKey(fg, 'Desconto'));
            fg.TextMatrix(1, getColIndexByColKey(fg, 'Desconto')) = (glb_TotalDesconto - nTotalDesconto);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'Desconto')) = treatNumericCell(fg.TextMatrix(1, getColIndexByColKey(fg, 'Desconto')));
        }
    }
    */
}

// FINAL DE EVENTOS DE GRID *****************************************

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id)
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);

}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
- null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // mostra a janela modal
    //preencheFabricante();

    ;
}

function Listar()
{
    var empresaData = getCurrEmpresaData();
    var nLinguaLogada = getDicCurrLang();

    strPars = '?glb_nImportacaoID=' + glb_nImportacaoID;
    strPars += '&controle=' + 'listaExcel';
    strPars += '&sEmpresaFantasia=' + empresaData[3];
    strPars += '&nLinguaLogada=' + nLinguaLogada;
    strPars += '&controlID=' + glb_controlID;
    strPars += '&selRateio=' + selRateio.value;
    strPars += '&glb_nTipoImportacaoID=' + glb_nTipoImportacaoID;
    
    if (glb_controlID == 'btnExcel') {
        lockControlsInModalWin(true);
        var frameReport = document.getElementById("frmReport");
        frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
        frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modcomercial/submodimportacao/importacao/serverside/ReportsGrid_Rateios.aspx' + strPars;

        glb_controlID = 'btnListar';
    }
    else {

        lockControlsInModalWin(true);
        dsoItens.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/importacao/serverside/ReportsGrid_Rateios.aspx' + strPars;

        // Nacionaliza��o
        if (selRateio.value == 1) {
            if (glb_nTipoImportacaoID == 1351){
                dsoItens.ondatasetcomplete = fillGridDataNacionalizacaoTrading_DSC;
            }               
            else
                dsoItens.ondatasetcomplete = fillGridDataNacionalizacao_DSC;
        }
        // Contabil
        else if (selRateio.value == 2)
            dsoItens.ondatasetcomplete = fillGridDataContabil_DSC;
        // Gerencial
        if (selRateio.value == 3)
            dsoItens.ondatasetcomplete = fillGridDataGerencial_DSC;
        //Resumo
        if (selRateio.value == 4)
            dsoItens.ondatasetcomplete = fillGridDataResumo_DSC;

        dsoItens.refresh();
    }
}

function fillGridDataNacionalizacao_DSC()
{
    if (selFabricanteID.value != 0)
    {
        dsoItens.recordset.moveFirst();
        dsoItens.recordset.setFilter('ExportadorID = ' + selFabricanteID.value);
    }
    else
    {
        dsoItens.recordset.moveFirst();
        dsoItens.recordset.setFilter('');
    }

    startGridInterface(fg);
    fg.FrozenCols = 0;
    fg.FontSize = '8';

    headerGrid(fg, ['InvItemID',
                    'Invoice',
                    'Exportador',
                    'NCM',
                    'ID',
                    'Produto',
                    'Quant',
                    'Valor FOB',
                    'Rateio FOB',
                    'Peso Liquido',
                    'Rateio PL',
                    'Frete',
                    'Rateio FOB + Frete',
                    'Seguro',
                    'SISCOMEX',
                    'Valor CIF',
                    'II',
                    'Valor II',
                    'PIS',
                    'Valor PIS',
                    'COFINS',
                    'Valor COFINS',
                    'Base IPI',
                    'IPI',
                    'Valor IPI',
                    'Benef�cio ICMS',
                    'Base ICMS',
                    'ICMS',
                    'Valor ICMS',
                    'Valor NF Nac Unit',                    
                    'Valor NF Nac',
                    'Outras Desp + Imp',
                    'Total NF Nac', 'RelacaoID' ], [0, 33]);

    glb_aCelHint = [[0, 6, 'Quantidade'],
                    [0, 7, 'Valor FOB (R$) = (Valor FOB Unit�rio (R$) * Quantidade)'],
                    [0, 8, 'Rateio FOB (%) = (Valor FOB (R$) / Somatoria Valor FOB (R$))'],
                    [0, 9, 'Peso Liquido (Kg) = (Peso Liquido Unit�rio (Kg) * Quantidade)'],
                    [0, 10, 'Rateio Peso Liquido (%) = (Peso Liquido (Kg) / Somatoria Peso Liquido (Kg))'],
                    [0, 11, 'Frete (R$) = (Somatoria Frete (R$) * Rateio Peso Liquido (%))'],
                    [0, 12, 'Rateio FOB + Frete (%) = ((Valor FOB (R$) + Frete (R$)) / Somatoria Valor FOB (R$) + Frete (R$))'],
                    [0, 13, 'Seguro (R$) = (Somatoria Seguro (R$) * Rateio FOB + Frete (%))'],
                    [0, 14, 'SISCOMEX (R$) = (Somatoria SISCOMEX (R$) * Rateio FOB (%))'],
                    [0, 15, 'Valor CIF (R$) = (Valor FOB (R$) + Frete (R$) + Seguro (R$))'],
                    [0, 16, 'Aliquota II (%)'],
                    [0, 17, 'Valor II (R$) = (Valor CIF (R$) * II (%))'],
                    [0, 18, 'Aliquota PIS (%)'],
                    [0, 19, 'Valor PIS (R$) = (Valor CIF (R$) * PIS (%))'],
                    [0, 20, 'Aliquota COFINS (%)'],
                    [0, 21, 'Valor COFINS (R$) = (Valor CIF (R$) * COFINS (%))'],
                    [0, 22, 'Base IPI (R$) = (Valor CIF (R$) + Valor II (R$))'],
                    [0, 23, 'Aliquota IPI (%)'],
                    [0, 24, 'Valor IPI (R$) = (Base IPI (R$) * IPI (%))'],
                    [0, 25, 'Tem beneficio de ICMS?'],
                    [0, 26, 'Base ICMS (R$) = ((Valor CIF (R$) + SISCOMEX (R$) + Valor II (R$) + Valor PIS (R$) + Valor COFINS (R$) + Valor IPI (R$)) / (1 - ICMS (%)))'],
                    [0, 27, 'Aliquota ICMS (%)'],
                    [0, 28, 'Valor ICMS (R$) = (Se tem Beneficio; 0; (Base ICMS (R$) * ICMS (%)))'],
                    [0, 29, 'Valor NF Nacionaliza��o Unit�rio (R$) = (Valor NF Nacionaliza��o (R$) / Quantidade)'],
                    [0, 30, 'Valor NF Nacionaliza��o (R$) = (Valor CIF (R$) + II (R$))'],
                    [0, 31, 'Outras Despesas + Impostos (R$) = ((SISCOMEX (R$) + Valor PIS (R$) + Valor COFINS (R$)) + (Valor IPI (R$) + Valor ICMS (R$)))'],
                    [0, 32, 'Total NF Nacionaliza��o (R$) = (Valor NF Nacionaliza��o (R$) + Outras Despesas + Impostos (R$))']];

    fillGridMask(fg, dsoItens, ['InvItemID',
                                'Invoice',
                                'Exportador',
                                'NCM',
                                'ID',
                                'Produto',
                                'Quantidade',
                                'FOBTotal',
                                'RateioFOB',
                                'PesoLiquido',
                                'RateioPL',
                                'Frete',
                                'RateioFOBFrete',
                                'Seguro',
                                'SISCOMEX',
                                'CIF',
                                'AliquotaII',
                                'ValorII',
                                'AliquotaPIS',
                                'ValorPIS',
                                'AliquotaCOFINS',
                                'ValorCOFINS',
                                'BaseIPI',
                                'AliquotaIPI',
                                'ValorIPI',
                                'BeneficioICMS',
                                'BaseICMS',
                                'AliquotaICMS',
                                'ValorICMS',
                                'UnitarioNF',
                                'TotalProdutos',
                                'DifTotal',
                                'TotalNF',
                                'RelacaoID'],
                                ['', '', '', '', '', '', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999',
                                    '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999',
                                    '999999999.9999', '', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', ''],
                                ['', '', '', '', '', '', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000',
                                    '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000',
                                    '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000',
                                    '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '']);


    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[4, '#########', 'C'],
                                                          [6, '#########', 'S'],
                                                          [7, '###,###,###,###.0000', 'S'],
                                                          [8, '###,###,###,###.0000', 'S'],
                                                          [9, '###,###,###,###.0000', 'S'],
                                                          [10, '###,###,###,###.0000', 'S'],
                                                          [11, '###,###,###,###.0000', 'S'],
                                                          [12, '###,###,###,###.0000', 'S'],
                                                          [13, '###,###,###,###.0000', 'S'],
                                                          [14, '###,###,###,###.0000', 'S'],
                                                          [15, '###,###,###,###.0000', 'S'],
                                                          [17, '###,###,###,###.0000', 'S'],
                                                          [19, '###,###,###,###.0000', 'S'],
                                                          [21, '###,###,###,###.0000', 'S'],
                                                          [22, '###,###,###,###.0000', 'S'],
                                                          [24, '###,###,###,###.0000', 'S'],
                                                          [26, '###,###,###,###.0000', 'S'],
                                                          [28, '###,###,###,###.0000', 'S'],
                                                          [29, '###,###,###,###.0000', 'S'],
                                                          [30, '###,###,###,###.0000', 'S'],
                                                          [31, '###,###,###,###.0000', 'S'],
                                                          [32, '###,###,###,###.0000', 'S']]);

    fg.FrozenCols = 4;

    fg.MergeCells = 1;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;

    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);

    fg.focus();

    fg.Redraw = 2;

    habilitaGravar(false);

    if (fg.Rows > 2)
        btnExcel.disabled = false;

}

function fillGridDataContabil_DSC()
{
    if (selFabricanteID.value != 0)
    {
        dsoItens.recordset.moveFirst();
        dsoItens.recordset.setFilter('ExportadorID = ' + selFabricanteID.value);
    }
    else
    {
        dsoItens.recordset.moveFirst();
        dsoItens.recordset.setFilter('');
    }

    startGridInterface(fg);
    fg.FrozenCols = 0;

    fg.FontSize = '8';

    headerGrid(fg, ['InvItemID',
                    'Invoice',
                    'Exportador',
                    'NCM',
                    'ID',
                    'Produto',
                    'Quant',
                    'Rateio FOB',
                    'Total NF Compl',
                    'RelacaoID'], [0,9]);

    glb_aCelHint = [[0, 6, 'Quantidade'],
                    [0, 7, 'Rateio FOB (%) = (Valor FOB (R$) / Somatoria Valor FOB (R$))'],
                    [0, 8, 'Total NF Complementar (R$) = (Somatoria dos Custos Cont�beis (R$) * Rateio FOB (%))']];

    fillGridMask(fg, dsoItens, ['InvItemID',
                                'Invoice',
                                'Exportador',
                                'NCM',
                                'ID',
                                'Produto',
                                'Quantidade',
                                'RateioFOB',
                                'NFComplementar',
                                'RelacaoID'],
                                ['', '', '', '', '', '', '999999999.9999', '999999999.9999',''],
                                ['', '', '', '', '', '', '###,###,####0.0000', '###,###,####0.0000','']);

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[4, '#########', 'C'],
                                                          [6, '#########', 'S'],
                                                          [7, '#########', 'S'],
                                                          [8, '###,###,###,###.0000', 'S']]);

    fg.FrozenCols = 4;

    fg.MergeCells = 1;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;

    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);
    //fg.Editable = true;

    //glb_TotalDespesas = fg.valueMatrix(1, getColIndexByColKey(fg, 'Despesas'));

    //fg.TextMatrix(1, getColIndexByColKey(fg, 'Despesas')) = (glb_TotalDespesas - fg.ValueMatrix(1, getColIndexByColKey(fg, 'Despesas')));
    //fg.TextMatrix(1, getColIndexByColKey(fg, 'Desconto')) = (glb_TotalDesconto - fg.ValueMatrix(1, getColIndexByColKey(fg, 'Desconto')));

    habilitaGravar(true);

    if (fg.Rows > 2)
        btnExcel.disabled = false;
}

function fillGridDataGerencial_DSC()
{
    startGridInterface(fg);
    fg.FrozenCols = 0;

    fg.FontSize = '8';

    headerGrid(fg, ['InvItemID',
                    'Invoice',
                    'Exportador',
                    'NCM',
                    'ID',
                    'Produto',
                    'Quant',
                    'Rateio FOB',
                    'Total Pedido Ger',
                    'Total Pedido Desc',
                    'Frete Ajustado',
                    'Frete',
                    'Total Pedido Ajuste (+)',
                    'Total Pedido Ajuste (-)',
                    'RelacaoID'], [0, 14]);
                    /*
                    'Valor FOB Unit',
                    'Custo M�dio Unit',
                    'FIE',
                    'Pre�o Unit',
                    'FIS'], [0]);
                    */
    glb_aCelHint = [[0, 6, 'Quantidade'],
                    [0, 7, 'Rateio FOB (%) = (Valor FOB (R$) / Somatoria Valor FOB (R$))'],
                    [0, 8, 'Total Pedido Gerencial (R$) = (Somatoria Total Pedido Gerencial (R$) * Rateio FOB (%))'],
                    [0, 9, 'Total Pedido Desconto (R$) = (Base ICMS (R$) * ((ICMS (%) * 0.75) * 0.6666)), se ES'],
                    [0, 10, 'Frete Ajustado (R$) = (Somatoria Frete (R$) * Rateio FOB (%))'],
                    [0, 11, 'Frete (R$) = (Somatoria Frete (R$) * Rateio Peso Liquido (%))'],
                    [0, 12, 'Total Pedido Ajuste (+) (R$) = (Frete Ajustado (R$) - Frete (R$)), se positivo'],
                    [0, 13, 'Total Pedido Ajuste (-) (R$) = (Frete Ajustado (R$) - Frete (R$)), se negativo']];
                    /*
                    [0, 15, 'Valor FOB Unit�rio (R$) = (Valor FOB Unit�rio (US$) * Taxa (R$))'],
                    [0, 16, 'Custo M�dio Unit�rio (R$): Custo m�dio desta importa��o'],
                    [0, 17, 'Fator de Interna��o de Entrada = (Custo M�dio Unit�rio (R$) / Valor FOB Unit�rio (R$))'],
                    [0, 18, 'Pre�o Unit�rio (R$): Pre�o do produto com margem de contribui��o zero e prazo de estoque zero'],
                    [0, 19, 'Fator de Interna��o de Sa�da = (Pre�o Unit�rio (R$) / Valor FOB Unit�rio (R$))']];
                    */

    fillGridMask(fg, dsoItens, ['InvItemID',
                                'Invoice',
                                'Exportador',
                                'NCM',
                                'ID',
                                'Produto',
                                'Quantidade',
                                'RateioFOB',
                                'Gerencial',
                                'Desconto',
                                'FreteAjustado',
                                'Frete',
                                'AjusteGerencial',
                                'AjusteGerencial',
                                'RelacaoID'],
                                /*
                                'FobUnitario',
                                'CustoMedio',
                                'FatorInternacaoEntrada',
                                'Preco',
                                'FatorInternacaoSaida'],
                                */
                                ['', '', '', '', '', '', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', ''],
                                ['', '', '', '', '', '', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000',
                                    '###,###,####0.0000', '###,###,####0.0000', '']);

    // Pinta campos negativos com vermelho
    var nVermelho = 0X0000FF;

    for (i = 1; i < fg.Rows; i++)
    {
        if (fg.ValueMatrix(i, 12) < 0)
        {
            fg.TextMatrix(i, 12) = '';
        }

        if (fg.ValueMatrix(i, 13) > 0)
        {
            fg.TextMatrix(i, 13) = '';
        }
        else
        {
            fg.Select(i, 13, i, 13);
            fg.FillStyle = 1;
            fg.CellForeColor = nVermelho;
        }
    }

    fg.Editable = true;

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[4, '#########', 'C'],
                                                          [6, '#########', 'S'],
                                                          [7, '#########', 'S'],
                                                          [8, '###,###,###,###.0000', 'S'],
                                                          [9, '###,###,###,###.0000', 'S'],
                                                          [10, '###,###,###,###.0000', 'S'],
                                                          [11, '###,###,###,###.0000', 'S'],
                                                          [12, '###,###,###,###.0000', 'S'],
                                                          [13, '###,###,###,###.0000', 'S']]);

    fg.Editable = false;

    fg.FrozenCols = 4;

    fg.MergeCells = 1;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;

    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);
    //fg.Editable = true;

    //glb_TotalDespesas = fg.valueMatrix(1, getColIndexByColKey(fg, 'Despesas'));

    //fg.TextMatrix(1, getColIndexByColKey(fg, 'Despesas')) = (glb_TotalDespesas - fg.ValueMatrix(1, getColIndexByColKey(fg, 'Despesas')));
    //fg.TextMatrix(1, getColIndexByColKey(fg, 'Desconto')) = (glb_TotalDesconto - fg.ValueMatrix(1, getColIndexByColKey(fg, 'Desconto')));

    habilitaGravar(true);

    if (fg.Rows > 2)
        btnExcel.disabled = false;
}


//Resumo ////////////////////////////////////////////////////////////
function fillGridDataResumo_DSC()
{
    var nDifFIS = 0;

    var nFOBUnitarioReal = 0;
    var nNFUnitario = 0;
    var nNFComplementarUnitario = 0;
    var nGerencialUnitario = 0;
    var nDescontoUnitario = 0;
    var nAjusteGerencialUnitarioP = 0;
    var nAjusteGerencialUnitarioN = 0;
    var nTaxaMoeda = 0;
    var nFOBUnitarioDolar = 0;
    var nCustoMedio = 0;
    var nPreco = 0;

    startGridInterface(fg);
    fg.FrozenCols = 0;

    fg.FontSize = '8';

    headerGrid(fg, ['InvItemID',
                    'Invoice',
                    'Exportador',
                    'NCM',
                    'ID',
                    'Produto',
                    'Quant',
                    'Valor FOB Unit',
                    'NF Nac Unit',
                    'NF Compl Unit',
                    'Pedido Ger Unit',
                    'Pedido Desc Unit',
                    'Pedido Ajuste Unit (+)',
                    'Pedido Ajuste Unit (-)',
                    'Taxa',
                    'Valor FOB Unit',
                    'Custo M�dio',
                    'FIE',
                    'Pre�o',
                    'FIS',
                    'FIS PesCon',
                    'RelacaoID'], [0, 20, 21]);

    glb_aCelHint = [[0, 6, 'Quantidade'],
                    [0, 7, 'Valor FOB Unit�rio (R$) = (Valor FOB Unit�rio (US$) * Taxa (R$))'],
                    [0, 8, 'NF Nacionaliza��o Unit�rio (R$) = ((Valor NF Nacionaliza��o (R$) + Outras Despesas + Impostos (R$)) / Quantidade)'],
                    [0, 9, 'NF Complementar Unit�rio (R$) = ((Despesas (R$) * Rateio Contabil (%)) / Quantidade)'],
                    [0, 10, 'Pedido Gerencial Unit�rio (R$) = ((Despesas (R$) * Rateio Gerencial (%)) / Quantidade), se ES'],
                    [0, 11, 'Pedido Desconto Unit�rio (R$) = ((Base ICMS (R$) * ((ICMS (%) * 0.75) * 0.6666)) / Quantidade)'],
                    [0, 12, 'Pedido Ajuste Unit�rio (+) (R$) = ((Frete Ajustado (R$) - Frete (R$)) / Quantidade), se positivo'],
                    [0, 13, 'Pedido Ajuste Unit�rio (-) (R$) = ((Frete Ajustado (R$) - Frete (R$)) / Quantidade), se negativo'],
                    [0, 14, 'Taxa (R$)'],
                    [0, 15, 'Valor FOB Unit�rio (US$)'],
                    [0, 16, 'Custo M�dio (US$): Custo m�dio desta importa��o'],
                    [0, 17, 'Fator de Interna��o de Entrada = (Custo M�dio (US$) / Valor FOB Unit�rio (US$))'],
                    [0, 18, 'Pre�o (US$): Pre�o do produto com margem de contribui��o zero e prazo de estoque zero'],
                    [0, 19, 'Fator de Interna��o de Sa�da = (Pre�o (US$) / Valor FOB Unit�rio (US$))'],
                    [0, 20, 'Fator de Interna��o de Sa�da cadastrado no fornecedor de ordem 1 do PesCon']];

    fillGridMask(fg, dsoItens, ['InvItemID',
                                'Invoice',
                                'Exportador',
                                'NCM',
                                'ID',
                                'Produto',
                                'Quantidade',
                                'FOBUnitarioReal',
                                'NFUnitario',
                                'NFComplementarUnitario',
                                'GerencialUnitario',
                                'DescontoUnitario',
                                'AjusteGerencialUnitario',
                                'AjusteGerencialUnitario',
                                'TaxaMoeda',
                                'FOBUnitarioDolar',
                                'CustoMedio',
                                'FatorInternacaoEntrada',
                                'Preco',
                                'FatorInternacaoSaida',
                                'FISPesCon',
                                'RelacaoID'],
                                ['', '', '', '', '', '', '', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999',
                                    '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', ''],
                                ['', '', '', '', '', '', '', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000',
                                    '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '']);

    alignColsInGrid(fg, [4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]);

    // Cores
    var nVermelho = 0X7280FA;
    var nVermelhoNegativo = 0X0000FF;
    var nAmarelo = 0X8CE6F0;
    var nBranco = 0XFFFFFF;

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[4, '#########', 'C'],
                                                          [6, '#########', 'S'],
                                                          [7, '###,###,###,###.0000', 'S'],
                                                          [8, '###,###,###,###.0000', 'S'],
                                                          [9, '###,###,###,###.0000', 'S'],
                                                          [10, '###,###,###,###.0000', 'S'],
                                                          [11, '###,###,###,###.0000', 'S'],
                                                          [12, '###,###,###,###.0000', 'S'],
                                                          [13, '###,###,###,###.0000', 'S'],
                                                          [15, '###,###,###,###.0000', 'S'],
                                                          [16, '###,###,###,###.0000', 'S'],
                                                          [17, '###,###,###,###.0000', 'M'],
                                                          [18, '###,###,###,###.0000', 'S'],
                                                          [19, '###,###,###,###.0000', 'M']]);

    for (i = 2; i < fg.Rows; i++)
    {
        nDifFIS = 0;

        glb_aCelHint[glb_aCelHint.length] = [i, getColIndexByColKey(fg, 'FatorInternacaoSaida'), fg.TextMatrix(i, getColIndexByColKey(fg, 'FISPesCon'))];

        if (fg.ValueMatrix(i, 12) < 0)
        {
            fg.TextMatrix(i, 12) = '';
        }

        if (fg.ValueMatrix(i, 13) > 0)
        {
            fg.TextMatrix(i, 13) = '';
        }
        else
        {
            // Pinta campos negativos com vermelho
            fg.Select(i, 13, i, 13);
            fg.FillStyle = 1;
            fg.CellForeColor = nVermelhoNegativo;
        }

        // Trecho que mostra problemas no fator de interna��o de saida atrav�s das cores - Inicio
        nDifFIS = (fg.ValueMatrix(i, getColIndexByColKey(fg, 'FatorInternacaoSaida')) - fg.ValueMatrix(i, getColIndexByColKey(fg, 'FISPesCon')));

        if (nDifFIS < 0)
            nDifFIS = (nDifFIS * -1);

        // Caso a diferen��o entre o FIS da Importa��o e do PesCon seja entre 0.04 e 0.1, pinta a celula de Amarelo.
        if ((nDifFIS > 0.03) && (nDifFIS <= 0.1))
            fg.Cell(6, i, getColIndexByColKey(fg, 'FatorInternacaoSaida'), i, getColIndexByColKey(fg, 'FatorInternacaoSaida')) = nAmarelo;
        // Caso a diferen��o entre o FIS da Importa��o e do PesCon seja maior que 0.1, pinta a celula de Vermelho.
        else if (nDifFIS > 0.1)
            fg.Cell(6, i, getColIndexByColKey(fg, 'FatorInternacaoSaida'), i, getColIndexByColKey(fg, 'FatorInternacaoSaida')) = nVermelho;
        // Caso a diferen��o entre o FIS da Importa��o e do PesCon seja menor que 0.04, deixa a celula Branca.
        else
            fg.Cell(6, i, getColIndexByColKey(fg, 'FatorInternacaoSaida'), i, getColIndexByColKey(fg, 'FatorInternacaoSaida')) = nBranco;
        // Trecho que mostra problemas no fator de interna��o de saida atrav�s das cores - Fim

        nFOBUnitarioReal = nFOBUnitarioReal + (fg.ValueMatrix(i, getColIndexByColKey(fg, 'FOBUnitarioReal')) * fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade')));
        nNFUnitario = nNFUnitario + (fg.ValueMatrix(i, getColIndexByColKey(fg, 'NFUnitario')) * fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade')));
        nNFComplementarUnitario = nNFComplementarUnitario + (fg.ValueMatrix(i, getColIndexByColKey(fg, 'NFComplementarUnitario')) * fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade')));
        nGerencialUnitario = nGerencialUnitario + (fg.ValueMatrix(i, getColIndexByColKey(fg, 'GerencialUnitario')) * fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade')));
        nDescontoUnitario = nDescontoUnitario + (fg.ValueMatrix(i, getColIndexByColKey(fg, 'DescontoUnitario')) * fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade')));
        nAjusteGerencialUnitarioP = nAjusteGerencialUnitarioP + (fg.ValueMatrix(i, 12) * fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade')));
        nAjusteGerencialUnitarioN = nAjusteGerencialUnitarioN + (fg.ValueMatrix(i, 13) * fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade')));
        nFOBUnitarioDolar = nFOBUnitarioDolar + (fg.ValueMatrix(i, getColIndexByColKey(fg, 'FOBUnitarioDolar')) * fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade')));
        nCustoMedio = nCustoMedio + (fg.ValueMatrix(i, getColIndexByColKey(fg, 'CustoMedio')) * fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade')));
        nPreco = nPreco + (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Preco')) * fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade')));
    }

    fg.TextMatrix(1, getColIndexByColKey(fg, 'FOBUnitarioReal')) = nFOBUnitarioReal;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'NFUnitario')) = nNFUnitario;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'NFComplementarUnitario')) = nNFComplementarUnitario;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'GerencialUnitario')) = nGerencialUnitario;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'DescontoUnitario')) = nDescontoUnitario;
    fg.TextMatrix(1, 12) = nAjusteGerencialUnitarioP;
    fg.TextMatrix(1, 13) = nAjusteGerencialUnitarioN;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'FOBUnitarioDolar')) = nFOBUnitarioDolar;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'CustoMedio')) = nCustoMedio;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'Preco')) = nPreco;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'FatorInternacaoEntrada')) = (nCustoMedio / nFOBUnitarioDolar);
    fg.TextMatrix(1, getColIndexByColKey(fg, 'FatorInternacaoSaida')) = (nPreco / nFOBUnitarioDolar);

    fg.Editable = false;

    fg.FrozenCols = 5;

    fg.MergeCells = 1;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;
    fg.MergeCol(5) = true;

    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);
    //fg.Editable = true;

    //glb_TotalDespesas = fg.valueMatrix(1, getColIndexByColKey(fg, 'Despesas'));

    //fg.TextMatrix(1, getColIndexByColKey(fg, 'Despesas')) = (glb_TotalDespesas - fg.ValueMatrix(1, getColIndexByColKey(fg, 'Despesas')));
    //fg.TextMatrix(1, getColIndexByColKey(fg, 'Desconto')) = (glb_TotalDesconto - fg.ValueMatrix(1, getColIndexByColKey(fg, 'Desconto')));

    btnGerarPedido.disabled = true;
    btnGravar.disabled = true;

    if (fg.Rows > 2)
        btnExcel.disabled = false;
}
//Resumo ////////////////////////////////////////////////////////////

function btn_GerarPedido()
{
    //var nTipoImportacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoImportacaoID' + '\'' + '].value');

    // Pedidos de Ajuste
    if (selRateio.value > 1)
    {
        //GerarPedido();

        glb_bGerarPedido = true;
        glb_Row = 2;
        gravarDespesas();

    }
    else // Pedido de Compra
    {
        if (glb_nTipoImportacaoID == 1351)
        {
            glb_bGerarPedido = true;
            glb_Row = 2;
            gravarDespesas();
        }
        else
            GerarPedido();
    }
}

function GerarPedido()
{
    lockControlsInModalWin(true);

    var strPars = new String();
    //var nImportacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ImportacaoID' + '\'' + '].value');
    var nTipoPedidoID = selRateio.value;

    strPars += '?nImportacaoID=' + escape(glb_nImportacaoID);
    strPars += '&nTipoPedidoID=' + escape(nTipoPedidoID);

    try
    {
        dsoGerarPedido.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/importacao/serverside/Importacao_PedidoGerador.aspx' + strPars;
        dsoGerarPedido.ondatasetcomplete = GerarPedido_DSC;
        dsoGerarPedido.refresh();
    }
    catch (e)
    {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
    }
}

function GerarPedido_DSC() {
    var sResultado = '';

    if (!((dsoGerarPedido.recordset.BOF) && (dsoGerarPedido.recordset.EOF))) {
        sResultado = dsoGerarPedido.recordset['Resultado'].value;

        if (sResultado != '') {
            if (window.top.overflyGen.Alert(sResultado) == 0)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Pedido gerado com sucesso !') == 0)
                return null;
        }
    }
    Listar();
}

function gerarExcel()
{
    glb_controlID = 'btnExcel';
    Listar();
}

function preencheFabricante()
{
    //var nImportacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ImportacaoID' + '\'' + '].value');

    lockControlsInModalWin(true);

    setConnection(dsoFabricantes);

    var sWHERE = '';

    dsoFabricantes.SQL = 'SELECT 0 AS FabricanteID, \'\' AS Fabricante ' +
	                     'UNION ' +
	                     'SELECT b.PessoaID AS FabricanteID, b.Fantasia AS Fabricante ' +
                            'FROM Invoices a WITH(NOLOCK) ' +
                                'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ExportadorID) ' +
                            'WHERE (a.ImportacaoID  = ' + glb_nImportacaoID + ')';

    dsoFabricantes.ondatasetcomplete = preencheFabricante_DSC;
    dsoFabricantes.Refresh();
}

function preencheFabricante_DSC() {
    clearComboEx(['selFabricanteID']);

    if (!(dsoFabricantes.recordset.BOF || dsoFabricantes.recordset.EOF))
    {
        dsoFabricantes.recordset.moveFirst;

        while (!dsoFabricantes.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoFabricantes.recordset['Fabricante'].value;
            oOption.value = dsoFabricantes.recordset['FabricanteID'].value;
            selFabricanteID.add(oOption);
            dsoFabricantes.recordset.MoveNext();
        }

        selFabricanteID.disabled = ((selFabricanteID.length <= 1) ? true : false);
    }

    lockControlsInModalWin(false);

    Listar();
}

/*
function calculaDespesaTotal()
{
    var nTotalDespesa = 0;

    for (i = 2; i < fg.Rows; i++)
    {
        nTotalDespesa = (nTotalDespesa + fg.ValueMatrix(i, getColIndexByColKey(fg, 'Despesas')));
    }

    return nTotalDespesa;
}
*/

function changeFabricante()
{
    lblFabricanteID.style.width = 200;

    if (selFabricanteID.value != 0)
        lblFabricanteID.innerText = 'Fabricante ' + selFabricanteID.value;
    else
        lblFabricanteID.innerText = 'Fabricante';

    Listar();
}

function changeRateio()
{
    fg.Rows = 0;

    //var nSituacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'SituacaoID' + '\'' + '].value');

    selFabricanteID.value = 0;

    if (selRateio.value > 2)
        selFabricanteID.disabled = true;
    else
        selFabricanteID.disabled = ((selFabricanteID.length <= 1) ? true : false);

    if (glb_Incluir == 1)
    {
        // Nacionaliza��o
        if (((selRateio.value == 1) && (glb_nSituacaoID == 1375)) || ((selRateio.value > 1) && (glb_nSituacaoID == 1377)))
            btnGerarPedido.disabled = false;
        else
            btnGerarPedido.disabled = true;
    }

    Listar();
}

function btn_gravarDespesas()
{
    glb_Row = 2;
    gravarDespesas();
}

function gravarDespesas()
{
    var nTotalDespesas = 0;

    /*
    if (selRateio.value != 1)
        nTotalDespesas = fg.valueMatrix(1, getColIndexByColKey(fg, 'Despesas'));
    */

    //var nTipoImportacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoImportacaoID' + '\'' + '].value');

    /*
    if (nTotalDespesas != 0)
    {
        if (window.top.overflyGen.Alert('N�o � possivel realizar a opera��o com divergencia no valor das Despesas.') == 0)
            return null;

        return null;
    }
    */

    lockControlsInModalWin(true);

    var strPars = new String();
    var nInvItemID;
    var nDespesas = 0;
    var nDesconto = 0;
    var nValorUnitario = 0;
    var nBasePIS = 0;
    var nAliquotaPIS = 0;
    var nValorPIS = 0;
    var nBaseCOFINS = 0;
    var nAliquotaCOFINS = 0;
    var nValorCOFINS = 0;
    var nBaseIPI = 0;
    var nAliquotaIPI = 0;
    var nValorIPI = 0;
    var nBaseICMS = 0;
    var nAliquotaICMS = 0;
    var nValorICMS = 0;
    var nBaseICMSST = 0;
    var nAliquotaICMSST = 0;
    var nValorICMSST = 0;
    var nInvIteNFeTradingID = 0;

    if (glb_Row < fg.Rows)
    {
        nInvItemID = fg.textMatrix(glb_Row, getColIndexByColKey(fg, 'InvItemID'));

        if ((glb_nTipoImportacaoID == 1351) && (selRateio.value == 1))
        {
            nInvIteNFeTradingID = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'InvIteNFeTradingID'));
            nValorUnitario = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'UnitarioNF'));
            nBasePIS = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'BasePIS'));
            nAliquotaPIS = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'AliquotaPIS'));
            nValorPIS = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'ValorPIS'));
            nBaseCOFINS = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'BaseCOFINS'));
            nAliquotaCOFINS = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'AliquotaCOFINS'));
            nValorCOFINS = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'ValorCOFINS'));
            nBaseIPI = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'BaseIPI'));
            nAliquotaIPI = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'AliquotaIPI'));
            nValorIPI = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'ValorIPI'));
            nBaseICMS = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'BaseICMS'));
            nAliquotaICMS = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'AliquotaICMS'));
            nValorICMS = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'ValorICMS'));
            /*
            nBaseICMS = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'BaseICMSST'));
            nAliquotaICMS = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'AliquotaICMST'));
            nValorICMS = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'ValorICMSST'));
            */
        }
        else if (selRateio.value != 1)
        {
            //nDespesas = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'Despesas'));

            // Gerencial
            if (selRateio.value == 3)
                nDesconto = fg.valueMatrix(glb_Row, getColIndexByColKey(fg, 'Desconto'));
        }
        else
            return;

        strPars += '?nInvItemID=' + escape(nInvItemID);
        strPars += '&nInvIteNFeTradingID=' + escape(nInvIteNFeTradingID);
        strPars += '&nTipoImportacaoID=' + escape(glb_nTipoImportacaoID);
        strPars += '&nTipoPedido=' + escape(selRateio.value);
        strPars += '&nDespesas=' + escape(nDespesas);
        strPars += '&nDesconto=' + escape(nDesconto);
        strPars += '&nValorUnitario=' + escape(nValorUnitario);
        strPars += '&nBasePIS=' + escape(nBasePIS);
        strPars += '&nAliquotaPIS=' + escape(nAliquotaPIS);
        strPars += '&nValorPIS=' + escape(nValorPIS);
        strPars += '&nBaseCOFINS=' + escape(nBaseCOFINS);
        strPars += '&nAliquotaCOFINS=' + escape(nAliquotaCOFINS);
        strPars += '&nValorCOFINS=' + escape(nValorCOFINS);
        strPars += '&nBaseIPI=' + escape(nBaseIPI);
        strPars += '&nAliquotaIPI=' + escape(nAliquotaIPI);
        strPars += '&nValorIPI=' + escape(nValorIPI);
        strPars += '&nBaseICMS=' + escape(nBaseICMS);
        strPars += '&nAliquotaICMS=' + escape(nAliquotaICMS);
        strPars += '&nValorICMS=' + escape(nValorICMS);
        strPars += '&nBaseICMSST=' + escape(nBaseICMSST);
        strPars += '&nAliquotaICMSST=' + escape(nAliquotaICMSST);
        strPars += '&nValorICMSST=' + escape(nValorICMSST);

        try
        {
            dsoGravar.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/importacao/serverside/Update_Invoce_Itens.aspx' + strPars;
            dsoGravar.ondatasetcomplete = gravarDespesas_DSC;
            dsoGravar.refresh();
        }
        catch (e)
        {
            if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
                return null;

            lockControlsInModalWin(false);
        }
    }
    else
    {
        if (glb_bGerarPedido)
        {
            glb_bGerarPedido = false;
            GerarPedido();
        }
        else
        {
            Listar();
        }
    }
}

function gravarDespesas_DSC()
{
    var sResultado = '';

    if (!((dsoGravar.recordset.BOF) && (dsoGravar.recordset.EOF)))
    {
        sResultado = dsoGravar.recordset['Resultado'].value;

        if (sResultado != '')
        {
            if (window.top.overflyGen.Alert('Erro ao gravar Despesas') == 0)
                return null;

            glb_Row = 0;

            Listar();

            return null;
        }
        else
        {
            glb_Row++;
            gravarDespesas();
        }
    }
    else
    {
        glb_Row++;
        gravarDespesas();
    }
}

function habilitaGravar(habilita)
{
    btnGravar.disabled = true;

    if (habilita)
    {
        if (selRateio.value == 1)
            btnGravar.disabled = btnGerarPedido.disabled;
    }
}


function PreencheVariaveis()
{
    lockControlsInModalWin(true);

    //var nImportacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ImportacaoID' + '\'' + '].value');

    var sSQL = '';

    sSQL = 'SELECT a.TipoImportacaoID AS TipoImportacaoID, dbo.fn_Importacao_Situacao(a.ImportacaoID) AS SituacaoID ' +
                'FROM Importacao a WITH(NOLOCK) ' +
	            'WHERE (a.ImportacaoID = ' + glb_nImportacaoID + ')';

    dsoVariaveis.SQL = sSQL;
    dsoVariaveis.ondatasetcomplete = PreencheVariaveis_DSC;
    dsoVariaveis.refresh();    
}

function PreencheVariaveis_DSC()
{
    if (!((dsoVariaveis.recordset.BOF) && (dsoVariaveis.recordset.EOF)))
    {
        glb_nTipoImportacaoID = dsoVariaveis.recordset['TipoImportacaoID'].value;
        glb_nSituacaoID = dsoVariaveis.recordset['SituacaoID'].value;
    }

    lockControlsInModalWin(false);

    if (glb_Incluir == 1)
    {
        // Nacionaliza��o
        if (((selRateio.value == 1) && (glb_nSituacaoID == 1375)) || ((selRateio.value > 1) && (glb_nSituacaoID == 1377)))
            btnGerarPedido.disabled = false;
        else
            btnGerarPedido.disabled = true;
    }

    preencheFabricante();
}

function fillGridDataNacionalizacaoTrading_DSC()
{
    if (selFabricanteID.value != 0) {
        dsoItens.recordset.moveFirst();
        dsoItens.recordset.setFilter('ExportadorID = ' + selFabricanteID.value);
    }
    else {
        dsoItens.recordset.moveFirst();
        dsoItens.recordset.setFilter('');
    }

    startGridInterface(fg);
    fg.FrozenCols = 0;
    fg.FontSize = '8';

    headerGrid(fg, ['InvItemID',
                    'Invoice',
                    'Exportador',
                    'ID',
                    'Produto',
                    'Quantidade',
                    'FOB Total',
                    'Rateio FOB',
                    'Peso Liquido',
                    'Rateio PL',
                    'Frete',
                    'Rateio (FOB + Frete)',
                    'Seguro',
                    'Base PIS',
                    'Al�quota PIS',
                    'Valor PIS',
                    'Base CONFIS',
                    'Al�quota COFINS',
                    'Valor COFINS',
                    'Base IPI',
                    'Al�quota IPI',
                    'Valor IPI',
                    'Base ICMS',
                    'Al�quota ICMS',
                    'Valor ICMS',
                    'Base ICMSST',
                    'Al�quota ICMSST',
                    'Valor ICMSST',
                    'Unitario NF',
                    'InvIteNFeTradingID'], [0, 29]);

    fillGridMask(fg, dsoItens, ['InvItemID',
                                'Invoice*',
                                'Exportador*',
                                'ID*',
                                'Produto*',
                                'Quantidade*',
                                'FOBTotal*',
                                'RateioFOB*',
                                'PesoLiquido*',
                                'RateioPL*',
                                'Frete*',
                                'RateioFOBFrete*',
                                'Seguro*',
                                'BasePIS',
                                'AliquotaPIS',
                                'ValorPIS',
                                'BaseCOFINS',
                                'AliquotaCOFINS',
                                'ValorCOFINS',
                                'BaseIPI',
                                'AliquotaIPI',
                                'ValorIPI',
                                'BaseICMS',
                                'AliquotaICMS',
                                'ValorICMS',
                                'BaseICMSST',
                                'AliquotaICMSST',
                                'ValorICMSST',
                                'UnitarioNF',
                                'InvIteNFeTradingID'],
                                ['', '', '', '', '', '', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999',
                                    '999999999.9999', '999999999.9999', '', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.9999', 
                                    '999999999.9999', '999999999.9999', ''],
                                ['', '', '', '', '', '', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000',
                                    '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000',
                                    '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000', '']);


    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[5, '#########', 'S'],
                                                         [6, '###,###,###,###.0000', 'S'],
                                                         [8, '###,###,###,###.0000', 'S'],
                                                         [9, '###,###,###,###.0000', 'S'],
                                                         [10, '###,###,###,###.0000', 'S'],
                                                         [12, '###,###,###,###.0000', 'S'],
                                                         [13, '###,###,###,###.0000', 'S'],
                                                         [15, '###,###,###,###.0000', 'S'],
                                                         [16, '###,###,###,###.0000', 'S'],
                                                         [18, '###,###,###,###.0000', 'S'],
                                                         [19, '###,###,###,###.0000', 'S'],
                                                         [21, '###,###,###,###.0000', 'S'],
                                                         [22, '###,###,###,###.0000', 'S'],
                                                         [24, '###,###,###,###.0000', 'S'],
                                                         [25, '###,###,###,###.0000', 'S'],
                                                         [26, '###,###,###,###.0000', 'S'],
                                                         [27, '###,###,###,###.0000', 'S'],
                                                         [28, '###,###,###,###.0000', 'S']]);

    fg.FrozenCols = 4;

    fg.MergeCells = 1;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;

    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);
    fg.Editable = true;

    fg.focus();

    fg.Redraw = 2;

    habilitaGravar(true);

    if (fg.Rows > 2)
        btnExcel.disabled = false;
}

function verificaDireitos()
{
    lockControlsInModalWin(true);

    //var nImportacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ImportacaoID' + '\'' + '].value');

    var sSQL = '';

    var nUserID = getCurrUserID();

    sSQL = 'SELECT TOP 1 Direitos.Incluir ' +
                'FROM RelacoesPesRec RelPesRec WITH(NOLOCK), RelacoesPesRec_Perfis RelPesRecPerfis WITH(NOLOCK), Recursos Perfis WITH(NOLOCK), Recursos Contextos WITH(NOLOCK), Recursos_Direitos Direitos WITH(NOLOCK) ' +
                'WHERE (RelPesRec.SujeitoID IN ((SELECT ' + nUserID + ' ' +
                                                'UNION ALL  ' +
                                                'SELECT UsuarioDeID  ' +
                                                    'FROM DelegacaoDireitos WITH(NOLOCK)  ' +
                                                    'WHERE (UsuarioParaID = ' + nUserID + ' AND GETDATE() BETWEEN dtInicio AND dtFim))) AND ' +
                    'RelPesRec.ObjetoID = 999 AND RelPesRec.TipoRelacaoID = 11 AND RelPesRec.RelacaoID = RelPesRecPerfis.RelacaoID ' +
                        'AND RelPesRecPerfis.EmpresaID = ' + glb_aEmpresaData[0] + ' AND RelPesRecPerfis.PerfilID = Perfis.RecursoID ' +
                        'AND Perfis.EstadoID = 2 AND Perfis.RecursoID = Direitos.PerfilID AND Direitos.RecursoMaeID = 24300 ' +
			            'AND Direitos.RecursoID = 40006 AND Direitos.ContextoID = Contextos.RecursoID AND Contextos.RecursoID = 5311) ' +
	            'ORDER BY Direitos.Alterar1 DESC, Direitos.Alterar2 DESC';

    dsoDireitos.SQL = sSQL;
    dsoDireitos.ondatasetcomplete = verificaDireitos_DSC;
    dsoDireitos.refresh();
}

function verificaDireitos_DSC()
{
    if (!((dsoDireitos.recordset.BOF) && (dsoDireitos.recordset.EOF)))
    {
        glb_Incluir = dsoDireitos.recordset['Incluir'].value;
    }
    
    lockControlsInModalWin(false);

    if (glb_Incluir == 0)
    {
        btnGerarPedido.disabled = true;
        btnGravar.disabled = true;
    }

    PreencheVariaveis();
}


function reports_onreadystatechange() {

    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
        (frameReport.contentWindow.document.readyState == 'interactive') ||
        (frameReport.contentWindow.document.readyState == 'complete')) {

        lockControlsInModalWin(false);
    }
}