<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalPreencheCombosHtml" name="modalPreencheCombosHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/submodimportacao/Importacao/modalpages/modalPreencheCombos.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodimportacao/Importacao/modalpages/modalPreencheCombos.js" & Chr(34) & "></script>" & vbCrLf
%>

</head>

<body id="modalPreencheCombosBody" name="modalPreencheCombosBody" LANGUAGE="javascript" onload="return window_onload()">
    <div id="divPesquisa" name="divPesquisa" class="divGeneral">
        
        <p id="lblPesquisaImportador" name="lblPesquisaImportador" class="lblGeneral">Pesquisa Importador</p>
        <input type="text" name="txtPesquisaImportador" id="txtPesquisaImportador" class="fldGeneral"></input>
    
        <p id="lblImportadorID" name="lblImportadorID" class="lblGeneral">Importador</p>
        <select id="selImportadorID" name="selImportadorID" class="fldGeneral"></select>

        <p id="lblPesquisaAgenteCarga" name="lblPesquisaAgenteCarga" class="lblGeneral">Pesquisa Agente de Cargas</p>
        <input type="text" name="txtPesquisaAgenteCarga" id="txtPesquisaAgenteCarga" class="fldGeneral"></input>
    
        <p id="lblAgenteCargaID" name="lblAgenteCargaID" class="lblGeneral">Agente de Cargas</p>
        <select id="selAgenteCargaID" name="selAgenteCargaID" class="fldGeneral"></select>

        <p id="lblPesquisaPortoOrigem" name="lblPesquisaPortoOrigem" class="lblGeneral">Pesquisa Origem</p>
        <input type="text" name="txtPesquisaPortoOrigem" id="txtPesquisaPortoOrigem" class="fldGeneral"></input>
    
        <p id="lblPortoOrigemID" name="lblPortoOrigemID" class="lblGeneral">Origem</p>
        <select id="selPortoOrigemID" name="selPortoOrigemID" class="fldGeneral"></select>

        <p id="lblPesquisaCompanhia" name="lblPesquisaCompanhia" class="lblGeneral">Pesquisa Companhia</p>
        <input type="text" name="txtPesquisaCompanhia" id="txtPesquisaCompanhia" class="fldGeneral"></input>
    
        <p id="lblCompanhiaID" name="lblCompanhiaID" class="lblGeneral">Companhia</p>
        <select id="selCompanhiaID" name="selCompanhiaID" class="fldGeneral"></select>

        <p id="lblPesquisaPortoDestino" name="lblPesquisaPortoDestino" class="lblGeneral">Pesquisa Destino</p>
        <input type="text" name="txtPesquisaPortoDestino" id="txtPesquisaPortoDestino" class="fldGeneral"></input>

        <p id="lblPortoDestinoID" name="lblPortoDestinoID" class="lblGeneral">Destino</p>
        <select id="selPortoDestinoID" name="selPortoDestinoID" class="fldGeneral"></select>

        <p id="lblPesquisaRecintoAduaneiro" name="lblPesquisaRecintoAduaneiro" class="lblGeneral">Pesquisa Recinto Aduaneiro</p>
        <input type="text" name="txtPesquisaRecintoAduaneiro" id="txtPesquisaRecintoAduaneiro" class="fldGeneral"></input>
    
        <p id="lblRecintoAduaneiroID" name="lblRecintoAduaneiroID" class="lblGeneral">Recinto Aduaneiro</p>
        <select id="selRecintoAduaneiroID" name="seselRecintoAduaneiroIDlPortoDestinoID" class="fldGeneral"></select>

        <p id="lblPesquisaTransportadoraDTA" name="lblPesquisaTransportadoraDTA" class="lblGeneral">Pesquisa Transportadora DTA</p>
        <input type="text" name="txtPesquisaTransportadoraDTA" id="txtPesquisaTransportadoraDTA" class="fldGeneral"></input>

        <p id="lblTransportadoraDTAID" name="lblTransportadoraDTAID" class="lblGeneral">Transportadora DTA</p>
        <select id="selTransportadoraDTAID" name="selTransportadoraDTAID" class="fldGeneral"></select>

        <p id="lblPesquisaDespachante" name="lblPesquisaDespachante" class="lblGeneral">Pesquisa DespachanteID</p>
        <input type="text" name="txtPesquisaDespachante" id="txtPesquisaDespachante" class="fldGeneral"></input>
    
        <p id="lblDespachanteID" name="lblDespachanteID" class="lblGeneral">DespachanteID</p>
        <select id="selDespachanteID" name="selDespachanteID" class="fldGeneral"></select>

        <p id="lblPesquisaTransportadora" name="lblPesquisaTransportadora" class="lblGeneral">Pesquisa Transportadora</p>
        <input type="text" name="txtPesquisaTransportadora" id="txtPesquisaTransportadora" class="fldGeneral"></input>
    
        <p id="lblTransportadoraID" name="lblTransportadoraID" class="lblGeneral">Transportadora</p>
        <select id="selTransportadoraID" name="selTransportadoraID" class="fldGeneral"></select>

        <input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" class="btns">
    </div>

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
