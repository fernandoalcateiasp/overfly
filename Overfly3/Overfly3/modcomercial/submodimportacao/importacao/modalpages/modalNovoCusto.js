/********************************************************************
modalNovoCusto.js

Library javascript para modalNovoCusto.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************

var modPesqRel_TimerHandler = null;
var glb_aEmpresaData = getCurrEmpresaData();
var glb_nRow = 0;
var glb_nTotalRows = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_bFirstFill = false;

// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;

var dsoGrava = new CDatatransport("dsoGrava");
var dsoPesq = new CDatatransport("dsoPesq");
var dsoTipo = new CDatatransport("dsoTipo");
var dsoTipoCusto = new CDatatransport("dsoTipoCusto");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // ajusta o body do html
    with (modalNovoCustoBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    //
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 49;
    modalFrame.style.left = 0;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no campo apropriado
    if (document.getElementById('selTipoCustoID').disabled == false)
        selTipoCustoID.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {

    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }


    // texto da secao01
    secText('Associar Custo', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;

    // ajusta o divPesquisa
    with (divPesquisa.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        height = 40;

    }

    adjustElementsInForm([['lblTipoCustoID', 'selTipoCustoID', 20, 1, -10, -10],
    	                  ['lblDescricao', 'txtDescricao', 30, 1, -2],
		                  ['lblValor', 'txtValor', 12, 1, -3],
                          ['lblObservacao', 'txtObservacao', 64, 2, -10],
                          ['btnIncluir', 'btn', btn_width, 3, -4, -12]], null, null, true);

    txtValor.onkeypress = verifyNumericEnterNotLinked;
    txtValor.setAttribute('verifyNumPaste', 1);
    txtValor.setAttribute('thePrecision', 11, 1);
    txtValor.setAttribute('theScale', 2, 1);
    txtValor.setAttribute('minMax', new Array(0, 999999999.99), 1);
    txtValor.maxLength = 11;

    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    cbmTipo();
}

function js_modalNovoCusto_KeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqRelKeyPress(KeyAscii) {
    if (fg.Row < 1)
        return true;

    if ((KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false))
        btn_onclick(btnOK);

}
function js_modalNovoCusto_BeforeEdit(grid, Row, Col) {
    ;
}
function js_modalNovoCusto_AfterEdit(grid, Row, Col) {

    var bHabilita = 0;

    if (grid.Editable) {
        if (Col == getColIndexByColKey(grid, 'Associar')) {
            for (var i = 1; i < grid.Rows; i++) {
                if (grid.TextMatrix(i, getColIndexByColKey(grid, 'Associar')) != 0) {
                    bHabilita = 1;
                    btnIncluir.disabled = false;
                }
                else if ((grid.TextMatrix(i, getColIndexByColKey(grid, 'Associar')) == 0) && bHabilita == 0) {
                    btnIncluir.disabled = true;
                }

            }
        }
    }
}
/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('OK_CALLFORM_I'), null);
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_I'), null);
    }
}

function btnIncluir_onclick(ctl)
{
    lockControlsInModalWin(true);

    var strPars = new String();    
    var nImportacaoID;
    var nTipoCustoID = selTipoCustoID.value;
    var sDescricao = txtDescricao.value;
    var nValor = txtValor.value;
    var sObservacao = txtObservacao.value;

    nImportacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ImportacaoID' + '\'' + '].value');

    strPars = '?nImportacaoID=' + escape(nImportacaoID);
    strPars += '&nTipoCustoID=' + escape(nTipoCustoID);
    strPars += '&sDescricao=' + escape(sDescricao);
    strPars += '&nValor=' + escape(nValor);
    strPars += '&sObservacao=' + escape(sObservacao);

    try
    {
        dsoGrava.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/importacao/serverside/IncluirNovoCusto.aspx' + strPars;
        dsoGrava.ondatasetcomplete = btnIncluir_onclick_DSC;
        dsoGrava.refresh();
    }
    catch (e)
    {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
    }
}

function btnIncluir_onclick_DSC()
{
    lockControlsInModalWin(false);

    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF))
    {
        dsoGrava.recordset.MoveFirst();

        if ((dsoGrava.recordset['Resultado'].value != null) && (dsoGrava.recordset['Resultado'].value != ''))
        {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0)
                return null;
        }
        else
        {
            if (window.top.overflyGen.Alert('Registro incluido com sucesso!') == 0)
                return null;

            sendJSMessage(getHtmlId(), JS_DATAINFORM, ('OK_CALLFORM_I'), null);
        }
    }
    else
    {
        if (window.top.overflyGen.Alert('Erro ao incluir Registro!') == 0)
            return null;
    }
}

function cbmTipo()
{
    lockControlsInModalWin(true);

    setConnection(dsoTipo);
    dsoTipo.SQL = 'SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName ' +
                    'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' + 
                    'WHERE ((a.TipoID = 439) AND ((a.Numero > 1) OR (a.ItemID = 1398)))';

    dsoTipo.ondatasetcomplete = fillTipo_DSC;
    dsoTipo.Refresh();
}

function fillTipo_DSC()
{
    if (!((dsoTipo.recordset.BOF) && (dsoTipo.recordset.EOF)))
    {
        clearComboEx(['selTipoCustoID']);

        while (!dsoTipo.recordset.EOF)
        {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoTipo.recordset['fldName'].value;
            oOption.value = dsoTipo.recordset['fldID'].value;
            selTipoCustoID.add(oOption);
            dsoTipo.recordset.MoveNext();
        }
    }

    lockControlsInModalWin(false);
}