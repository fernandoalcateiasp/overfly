/********************************************************************
modalpedidos.js

Library javascript para o modalpedidos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var modPesqpedidos_Timer = null;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dsoPedidos = new CDatatransport("dsoPedidos");
var dsoImportacao = new CDatatransport("dsoImportacao");

/********************************************************************
RELACAO DAS FUNCOES

NOTA: Ver tambem o arquivo commonpedidos.js

FUNCOES GERAIS:
window_onload()
setupPage()
txtPesquisa_onKeyPress()
txtPesquisa_ondigit(ctl)
btnFindPesquisa_onclick(ctl)
btn_onclick(ctl)
startPesq(strPesquisa)
dataToReturn()

Eventos de grid particulares desta janela:
fg_ModPesqpedidosDblClick()
fg_ModPesqpedidosKeyPress(KeyAscii)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    
    with (modalpedidosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no grid
    fg.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Pedidos ', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    
    
    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }

    with (divFG.style)
    {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = (ELEM_GAP * 2) - 1;
        top = (ELEM_GAP * 3) + 10;
        width = modWidth - 50;
        height = MAX_FRAMEHEIGHT_OLD - 150;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.style.width, 10) * 18;
    fg.Redraw = 2;

    btnOK.style.visibility = 'hidden';    
    btnCanc.value = 'Cancelar';
    btnCanc.style.visibility = 'hidden';
    btnIncluir.disabled = true;
    btnIncluir.onclick = btnIncluir_onclick;
    
    btnIncluir.style.top = parseInt(divFG.style.width, 10) - 298;
    btnIncluir.style.left = 330;    
    
    pedidosImp();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqpedidosDblClick()
{
    btnIncluir_onclick();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('OK_CALLFORM_INF'), null );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_INF'), null );
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqpedidosKeyPress(KeyAscii)
{
    return null;
}

/********************************************************************
grava o pedido na importação
********************************************************************/
function btnIncluir_onclick()
{
	var strPars = '';
	
	strPars += '?nImportacaoID=' + sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'ImportacaoID' + '\'' + '].value');
	strPars += '&nPedidoID=' + fg.ValueMatrix(fg.row, getColIndexByColKey(fg, 'Pedido'));
	
    setConnection(dsoImportacao);
	dsoImportacao.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/importacao/serverside/pedidoimportacao.aspx' + strPars;
	dsoImportacao.ondatasetcomplete = dsoImportacao_DSC;
	dsoImportacao.refresh();    
}

function dsoImportacao_DSC()
{
    if (dsoImportacao.recordset['Resultado'].value != null)
        window.top.overflyGen.Alert("Pedido incluido com sucesso!");
    pedidosImp();
}

/*******************************************************************
Select que preenche o grid
*******************************************************************/
function pedidosImp()
{
    var nEmpresa = getCurrEmpresaData();
	var exportadorID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ExportadorID'].value");
	
    setConnection(dsoPedidos);

    dsoPedidos.SQL = 'SELECT DISTINCT CONVERT(VARCHAR(10), a.dtPedido, 103) AS Data, a.PedidoID AS Pedido, b.RecursoAbreviado AS Est, c.Fantasia AS Pessoa, ' +
		                     'g.SimboloMoeda AS Moeda, a.ValorTotalPedido AS Total, d.Fantasia AS Vendedor, a.Observacao AS Observacao ' +
                        'FROM Pedidos a WITH(NOLOCK) ' +
			                'INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID) ' +
			                'INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.PessoaID) ' +
			                'INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = a.ProprietarioID) ' +
			                'INNER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (e.PessoaID = a.EmpresaID) ' +
			                'INNER JOIN Pessoas_Enderecos f WITH(NOLOCK) ON (f.PessoaID = a.PessoaID) ' + 
			                'INNER JOIN Conceitos g WITH(NOLOCK) ON (g.ConceitoID = a.MoedaID) ' +
                            'INNER JOIN Operacoes h WITH(NOLOCK) ON (a.TransacaoID = h.OperacaoID) ' +                       			
                        'WHERE	(a.EstadoID = 24) AND (a.EmpresaID = ' + nEmpresa[0] + ' ) AND ' + 								
                                '(f.PaisID <> e.PaisID) AND (h.Entrada = 1) AND ' +
		                        '((SELECT COUNT(*) ' +
		                            'FROM Importacao aa WITH(NOLOCK) ' +
		                                    ' INNER JOIN Importacao_Pedidos bb WITH(NOLOCK) ON (aa.ImportacaoID = bb.ImportacaoID) ' +
		                            'WHERE (aa.EstadoID <> 5) AND (bb.PedidoID = a.PedidoID)) = 0)';
	
	dsoPedidos.ondatasetcomplete = pedidosImp_DSC;
    dsoPedidos.refresh();
}

function pedidosImp_DSC() {
    startGridInterface(fg);
    fg.FontSize = '8';
    fg.FrozenCols = 0;    
	
	headerGrid(fg,['Data',
				   'Pedido',
				   'Est',
				   'Pessoa',
				   'Moeda',
				   'Total',
				   'Vendedor',
				   'Observacao'], []);
				   
	fillGridMask(fg,dsoPedidos,['Data',
		                     'Pedido',
		                     'Est',
		                     'Pessoa',
		                     'Moeda',
		                     'Total',
		                     'Vendedor',
		                     'Observacao'],
							 ['','9999999999','','','','999999999.99','',''],
							 ['','','','','','###,###,##0.00','','']);	
	
    alignColsInGrid(fg,[4,5]);
                             
    fg.FrozenCols = 1;
    
    fg.ColWidth(1) = 1100;
    fg.ColWidth(3) = 1600;
    fg.ColWidth(5) = 1250;
    fg.ColWidth(6) = 1600;
    fg.Redraw = 2;
    
    lockControlsInModalWin(false);
    
    // destrava botao Incluir se tem linhas no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
        btnIncluir.disabled = false;
    }    
    else
        btnIncluir.disabled = true; 
}