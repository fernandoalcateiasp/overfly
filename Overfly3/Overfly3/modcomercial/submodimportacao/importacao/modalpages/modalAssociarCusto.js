/********************************************************************
modalAssociarCusto.js

Library javascript para modalAssociarCusto.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************

var modPesqRel_TimerHandler = null;
var glb_aEmpresaData = getCurrEmpresaData();
var glb_nRow = 0;
var glb_nTotalRows = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_bFirstFill = false;

// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;

var dsoGrava = new CDatatransport("dsoGrava");
var dsoPesq = new CDatatransport("dsoPesq");
var dsoTipo = new CDatatransport("dsoTipo");
var dsoTipoCusto = new CDatatransport("dsoTipoCusto");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // ajusta o body do html
    with (modalAssociarCustoBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    //
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 49;
    modalFrame.style.left = 0;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no campo apropriado
    if (document.getElementById('selTipoID').disabled == false)
        selTipoID.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {

    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }


    // texto da secao01
    secText('Associar Custo', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;

    // ajusta o divPesquisa
    with (divPesquisa.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        height = 40;

    }

    // selTipoID
    selTipoID.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (selTipoID.style) {
        left = 0;
        top = 16;
        width = 100;
        heigth = 24;
    }

    selTipoID.onkeypress = selTipoID_onKeyPress;

    // txtPesquisa
    //txtPesquisa.onkeypress = verifyNumericEnterNotLinked;
    txtPesquisa.maxLength = 30;  // aceita vinte e hum caracteres de digitacao

    with (txtPesquisa.style) {
        top = parseInt(selTipoID.style.top, 10);
        left = parseInt(selTipoID.style.left, 10) + parseInt(selTipoID.style.width, 10) + 10;
        width = 100;
        height = 24;
    }

    with (lblPesquisa.style) {
        top = parseInt(txtPesquisa.style.top, 10) - 15;
        left = parseInt(txtPesquisa.style.left, 10) + 2;
        width = (txtPesquisa.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }

    // btnFindPesquisa
    with (btnFindPesquisa.style) {
        top = parseInt(txtPesquisa.style.top, 10);
        left = parseInt(txtPesquisa.style.left, 10) + parseInt(selTipoID.style.width, 10) + 8;
        width = 80;
        height = 24;
    }

    // btnAssociar
    btnAssociar.disabled = true;
    with (btnAssociar.style) {
        top = parseInt(selTipoID.style.top, 10);
        left = parseInt(btnFindPesquisa.style.left, 10) + parseInt(btnFindPesquisa.style.width, 10) + 8;
        width = 80;
        height = 24;
    }
    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
    }

    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.style.top, 10) + parseInt(divPesquisa.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 200;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    cbmTipo();

    startGridInterface(fg);
    headerGrid(fg, ['FinanceiroID',
                    'Pessoa',
                    'Tipo Custo',
                    'Descri��o',
                    'Valor',
                    'Associar'], []);
    fg.Redraw = 2;
}

function selTipoID_onKeyPress() {
    if (event.keyCode == 13) {
        btnFindPesquisa_onclick();
    }
}

function js_modalAssociarCusto_KeyPress(KeyAscii) {
    ;
}

function btnFindPesquisa_onclick(ctl) {
    selTipoID.value = trimStr(selTipoID.value);

    startPesq(selTipoID.value);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqRelKeyPress(KeyAscii) {
    if (fg.Row < 1)
        return true;

    if ((KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false))
        btn_onclick(btnOK);

}
function js_modalAssociarCusto_BeforeEdit(grid, Row, Col) {
    ;
}
function js_modalAssociarCusto_AfterEdit(grid, Row, Col) {

    var bHabilita = 0;

    if (grid.Editable) {
        if (Col == getColIndexByColKey(grid, 'Associar')) {
            for (var i = 1; i < grid.Rows; i++) {
                if (grid.TextMatrix(i, getColIndexByColKey(grid, 'Associar')) != 0) {
                    bHabilita = 1;
                    btnAssociar.disabled = false;
                }
                else if ((grid.TextMatrix(i, getColIndexByColKey(grid, 'Associar')) == 0) && bHabilita == 0) {
                    btnAssociar.disabled = true;
                }

            }
        }
    }
}
/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('OK_CALLFORM_I'), null);
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_I'), null);
    }
}

function startPesq(TipoID) {
    lockControlsInModalWin(true);

    glb_nDSOs = 2;


    if (TipoID == 1) {

        setConnection(dsoPesq);
        dsoPesq.SQL = "SELECT DISTINCT a.FinanceiroID, c.Fantasia AS Pessoa,  '' AS TipoCustoID, '' AS Descricao, '' AS Valor , 0 as Associar " +
                           "FROM Financeiro a WITH(NOLOCK) " +
                               "INNER JOIN HistoricosPadrao b WITH(NOLOCK) ON (b.HistoricoPadraoID = a.HistoricoPadraoID) " +
                               "INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.PessoaID) " +
                               "LEFT JOIN Importacao_Custos d WITH(NOLOCK) ON (d.FinanceiroID = a.FinanceiroID) " +
                           "WHERE ((a.EstadoID = 41 OR a.EstadoID = 48) AND b.HistoricoPadraoID = 1146 AND dbo.fn_Importacao_Custo_Saldo(a.FinanceiroID, 2) > 0) " +
                                   (txtPesquisa.value == "" ? "" : " AND a.FinanceiroID = " + txtPesquisa.value);
        dsoPesq.ondatasetcomplete = eval(dsoTipos_DSC);
        dsoPesq.refresh();



        setConnection(dsoTipoCusto);
        dsoTipoCusto.SQL = 'SELECT ItemID AS TipoCustoID, ItemMasculino AS TipoCusto ' +
                                'FROM TiposAuxiliares_Itens ' +
                                'WHERE TipoID = 439 ' +
                                'ORDER BY TipoCusto';

        dsoTipoCusto.ondatasetcomplete = eval(dsoTipos_DSC);
        dsoTipoCusto.refresh();
    }

    if (TipoID == 2) {

        setConnection(dsoPesq);
        dsoPesq.SQL = " SELECT a.PedidoID, dbo.fn_Pessoa_Fantasia(a.PessoaID, 0) as Pessoa, '' AS TipoCustoID, '' AS Descricao, '' AS Valor , 0 as Associar " +
                            "FROM Pedidos a WITH(NOLOCK) 	 " +
                        "WHERE ((a.ContaID = 1105011000) AND (a.EmpresaID = " + glb_aEmpresaData[0] + ") AND (A.EstadoID >= 30 ) " +
                                        " AND (dbo.fn_Importacao_Custo_Saldo(a.PedidoID, 1) > 0)) " +
                                        (txtPesquisa.value == "" ? "" : " AND a.PedidoID = " + txtPesquisa.value);

        dsoPesq.ondatasetcomplete = eval(dsoTipos_DSC);
        dsoPesq.refresh();

        setConnection(dsoTipoCusto);
        dsoTipoCusto.SQL = 'SELECT ItemID AS TipoCustoID, ItemMasculino AS TipoCusto ' +
                                'FROM TiposAuxiliares_Itens ' +
                                'WHERE TipoID = 439 ' +
                                'ORDER BY TipoCusto';

        dsoTipoCusto.ondatasetcomplete = eval(dsoTipos_DSC);
        dsoTipoCusto.refresh();
    }

    lockControlsInModalWin(false);

}

function dsopesq_DSC() {
    if (selTipoID.value == 1) {
        startGridInterface(fg);
        fg.FrozenCols = 0;

        headerGrid(fg, ['Financeiro',
                        'Pessoa',
                        'Tipo Custo',
                        'Descri��o',
                        'Valor',
                        'Associar'], []);

        fillGridMask(fg, dsoPesq, ['FinanceiroID*',
                                    'Pessoa*',
                                    'TipoCustoID',
                                    'Descricao',
                                    'Valor',
                                    'Associar'],
                                    ['', '', '', '', '999999999.99', ''],
                                    ['', '', '', '', '###,###,##0.00', '']);
    }
    else if (selTipoID.value == 2) {
        startGridInterface(fg);
        fg.FrozenCols = 0;

        headerGrid(fg, ['Pedido',
                        'Pessoa',
                        'Tipo Custo',
                        'Descri��o',
                        'Valor',
                        'Associar'], []);

        fillGridMask(fg, dsoPesq, ['PedidoID*',
                                    'Pessoa*',
                                    'TipoCustoID',
                                    'Descricao',
                                    'Valor',
                                    'Associar'],
                                    ['', '', '', '', '999999999.99', ''],
                                    ['', '', '', '', '###,###,##0.00', '']);

    }


    insertcomboData(fg, getColIndexByColKey(fg, 'TipoCustoID'), dsoTipoCusto, 'TipoCusto', 'TipoCustoID');

    with (fg) {
        ColDataType(5) = 11;
    }

    fg.Editable = true;
    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.ColWidth(2) = 1800;
    fg.ColWidth(3) = 2500;
    fg.ColWidth(4) = 1500;
    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
    }
    else
        btnOK.disabled = true;
}

function btnAssociar_onclick(ctl) {

    lockControlsInModalWin(true);

    var strPars = new String();    
    var nImportacaoID;
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;

    
    nImportacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ImportacaoID' + '\'' + '].value');

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    for (i = 1; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'Associar', i) != 0) {
            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                    strPars = ''; 
                    nDataLen = 0;
                }

                strPars += '?StartPackge=' + escape(0);
            }

            nDataLen++;
            strPars += '&ImportacaoID=' + escape(nImportacaoID);            
            strPars += '&TipoCustoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoCustoID')));

            strPars += '&Descricao=' + escape('\'' + fg.TextMatrix(i, getColIndexByColKey(fg, 'Descricao')) + '\'');

            strPars += '&Valor=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')));

            if (selTipoID.value == 1) {
                strPars += '&PedidoID=' + escape('NULL');
                strPars += '&FinanceiroID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'FinanceiroID*')));
            }
            else {
                strPars += '&FinanceiroID=' + escape('NULL');
                strPars += '&PedidoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PedidoID*')));
            }


        }
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);

    sendDataToServer();
}

function sendDataToServer(strPars) {

    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/importacao/serverside/AssociarCusto.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            lockControlsInModalWin(false);
            glb_OcorrenciasTimerInt = window.setInterval('setupPage()', 10, 'JavaScript');
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_OcorrenciasTimerInt = window.setInterval('setupPage()', 10, 'JavaScript');
    }
}

function sendDataToServer_DSC() {

    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        dsoGrava.recordset.MoveFirst();
        if ((dsoGrava.recordset['Resultado'].value != null) &&
			 (dsoGrava.recordset['Resultado'].value != '')) {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0)
                return null;

            glb_OcorrenciasTimerInt = window.setInterval('setupPage()', 10, 'JavaScript');
            return null;
        }
    }

    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    lockControlsInModalWin(false);

    glb_OcorrenciasTimerInt = window.setInterval('setupPage()', 10, 'JavaScript');
}

function cbmTipo() {

    lockControlsInModalWin(true);

    setConnection(dsoTipo);
    dsoTipo.SQL = "SELECT 1 AS fldID, 'Financeiro' AS fldName " +
                        "UNION ALL " +
                        "SELECT 2 AS fldID, 'Pedido' AS fldName ";

    dsoTipo.ondatasetcomplete = fillTipo_DSC;
    dsoTipo.Refresh();
}

function fillTipo_DSC() {
    if (!((dsoTipo.recordset.BOF) && (dsoTipo.recordset.EOF))) {
        clearComboEx(['selTipoID']);

        while (!dsoTipo.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoTipo.recordset['fldName'].value;
            oOption.value = dsoTipo.recordset['fldID'].value;
            selTipoID.add(oOption);
            dsoTipo.recordset.MoveNext();
        }
    }
    lockControlsInModalWin(false);
}
function txtPesquisa_onKeyPress() {
    ;
}
function dsoTipos_DSC() {
    glb_nDSOs--;

    if (glb_nDSOs > 0)
        return null;


    dsopesq_DSC();

    glb_bFirstFill = true;

}