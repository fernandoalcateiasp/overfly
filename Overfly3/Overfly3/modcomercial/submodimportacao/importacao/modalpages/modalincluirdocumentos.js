/********************************************************************
modalincluirdocumentos.js

Library javascript para modalincluirdocumentos.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************

var modPesqRel_TimerHandler = null;
var glb_aEmpresaData = getCurrEmpresaData();
var glb_nRow = 0;
var glb_nTotalRows = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_bFirstFill = false;

// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;

var dsoGrid = new CDatatransport("dsoGrid");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoPesq = new CDatatransport("dsoPesq");
var dsoTipo = new CDatatransport("dsoTipo");
var dsoTipoCusto = new CDatatransport("dsoTipoCusto");
var dsoCombos = new CDatatransport("dsoCombos");

var glbCombo = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // ajusta o body do html
    with (modalincluirdocumentosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    //
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 49;
    modalFrame.style.left = 0;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no campo apropriado
    if (document.getElementById('selTipoID').disabled == false)
        selTipoID.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    // texto da secao01
    secText('Associar Custo', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
    var grid_height = 0;

    // ajusta o divPesquisa
    with (divPesquisa.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        height = 90;
    }

    if (glb_sCaller == 'I')
    {
        lblPesquisaServico.style.visibility = 'inherit';
        txtPesquisaServico.style.visibility = 'inherit';
        lblServico.style.visibility = 'inherit';
        selServico.style.visibility = 'inherit';
        lblDocumentoTransporteID.style.visibility = 'inherit';
        selDocumentoTransporteID.style.visibility = 'inherit';
        lblGerencial.style.visibility = 'hidden';
        chkGerencial.style.visibility = 'hidden';
        txtValor.disabled = false;

        adjustElementsInForm([['lblTipoID', 'selTipoID', 25, 1, -13, -10],
    	                      ['lblPesquisaPessoa', 'txtPesquisaPessoa', 20, 1, -2],
                              ['lblPessoa', 'selPessoa', 20, 1, -2],
    	                      ['lblPesquisaServico', 'txtPesquisaServico', 20, 1, -2],
                              ['lblServico', 'selServico', 33, 1, -2],
		                      ['lblDocumento', 'txtDocumento', 20, 2, -13],
                              ['lblValor', 'txtValor', 15, 2, -3],
                              ['lblValorDesconto', 'txtValorDesconto', 15, 2, -3],
                              ['lblVencimento', 'txtVencimento', 15, 2, -3],
                              ['lblDocumentoTransporteID', 'selDocumentoTransporteID', 20, 2, -3],
                              ['btnIncluir', 'btn', btn_width, 2, 2]], null, null, true);
    }
    else if (glb_sCaller == 'PL')
    {
        lblPesquisaServico.style.visibility = 'hidden';
        txtPesquisaServico.style.visibility = 'hidden';
        lblServico.style.visibility = 'hidden';
        selServico.style.visibility = 'hidden';
        lblDocumentoTransporteID.style.visibility = 'hidden';
        selDocumentoTransporteID.style.visibility = 'hidden';
        lblGerencial.style.visibility = 'inherit';
        chkGerencial.style.visibility = 'inherit';
        txtValor.disabled = true;

        selTipoID.value = 2;
        selTipoID.disabled = true;

        adjustElementsInForm([['lblTipoID', 'selTipoID', 25, 1, -13, -10],
    	                      ['lblPesquisaPessoa', 'txtPesquisaPessoa', 20, 1, -2],
                              ['lblPessoa', 'selPessoa', 20, 1, -2],
                              ['lblGerencial', 'chkGerencial', 3, 1, -2],
		                      ['lblDocumento', 'txtDocumento', 20, 2, -13],
                              ['lblValor', 'txtValor', 15, 2, -3],
                              ['lblVencimento', 'txtVencimento', 15, 2, -3],
                              ['btnIncluir', 'btn', btn_width, 2, 2]], null, null, true);
    }

    selPessoa.disabled = true;
    selServico.disabled = true;
    selDocumentoTransporteID.disabled = true;

    txtPesquisaPessoa.onkeypress = PesquisaPessoa_OnKeyPress;
    txtPesquisaServico.onkeypress = PesquisaServico_OnKeyPress;
    selPessoa.onchange = selPessoa_OnChange;
    selTipoID.onchange = selTipoID_OnChange;
    chkGerencial.onclick = Gerencial_OnClick;

    txtValor.onkeypress = verifyNumericEnterNotLinked;
    txtValor.setAttribute('verifyNumPaste', 1);
    txtValor.setAttribute('thePrecision', 11, 1);
    txtValor.setAttribute('theScale', 2, 1);
    txtValor.setAttribute('minMax', new Array(0, 999999999.99), 1);
    txtValor.maxLength = 11;

    txtVencimento.maxLength = 10;
    txtVencimento.onkeypress = verifyDateTimeNotLinked;

    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
    }

    if (glb_sCaller == 'PL')
        grid_height = 300;
    else if (glb_sCaller == 'I')
        grid_height = 150;

    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.style.top, 10) + parseInt(divPesquisa.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + grid_height;
    }

    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    startGridInterface(fg);
    headerGrid(fg, ['ImportacaoID',
                    'Tipo Custo',
                    'Descri��o',
                    'Valor',
                    'OK',
                    'Observa��o',
                    'ImpCustoID'], [6]);

    fg.Redraw = 2;

    fillGrid();
}

function js_modalincluirdocumentos_BeforeEdit(grid, Row, Col)
{
    ;
}

function js_modalincluirdocumentos_AfterEdit(grid, Row, Col)
{
    if ((Col == getColIndexByColKey(grid, 'OK')) && (glb_sCaller == 'PL'))
    {
        calculaTotalOK();
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('OK_CALLFORM_' + glb_sCaller), null);
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_' + glb_sCaller), null);
    }
}

function fillGrid()
{
    lockControlsInModalWin(true);

    txtValor.value = '';

    var nImportacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ImportacaoID' + '\'' + '].value');

    setConnection(dsoGrid);

    if (glb_sCaller == 'I')
        dsoGrid.SQL = 'SELECT a.ImportacaoID, b.ItemMasculino AS TipoCusto, a.Descricao, a.Valor, 0 AS OK, a.Observacao, a.ImpCustoID ' +
                        'FROM Importacao_Custos a WITH(NOLOCK) ' +
                            'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.TipoCustoID) ' +
                        'WHERE ((a.ImportacaoID = ' + nImportacaoID + ') AND (b.Numero IN (1, 2)) AND (a.FinanceiroID IS NULL) AND (a.PedidoID IS NULL) AND (DocumentoTransporteID IS NULL)) ' +
                        'ORDER BY b.Numero ASC, a.Descricao ASC';
    else if (glb_sCaller == 'PL')
    {
        if (!chkGerencial.checked)
            dsoGrid.SQL = 'SELECT c.ImportacaoID, b.ItemMasculino AS TipoCusto, a.Descricao, a.Valor, 0 AS OK, a.Observacao, a.ImpCustoID, a.TipoCustoID ' +
                            'FROM Importacao_Custos a WITH(NOLOCK) ' +
                                'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.TipoCustoID) ' +
                                'INNER JOIN Importacao c WITH(NOLOCK) ON (c.ImportacaoID = a.ImportacaoID) ' +
                            'WHERE ((c.EmpresaID = ' + glb_aEmpresaData[0] + ') AND (a.TipoCustoID = 1392) AND (a.FinanceiroID IS NULL) AND (a.PedidoID IS NULL) AND (DocumentoTransporteID IS NULL)) ' +
                            'ORDER BY c.ImportacaoID, a.Descricao ASC';
        else
            dsoGrid.SQL = 'SELECT c.ImportacaoID, b.ItemMasculino AS TipoCusto, a.Descricao, a.Valor, 0 AS OK, a.Observacao, a.ImpCustoID, a.TipoCustoID ' +
                                'FROM Importacao_Custos a WITH(NOLOCK) ' +
                                    'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.TipoCustoID) ' +
                                    'INNER JOIN Importacao c WITH(NOLOCK) ON (c.ImportacaoID = a.ImportacaoID) ' +
                                'WHERE ((c.EmpresaID = ' + glb_aEmpresaData[0] + ') AND ((a.TipoCustoID = 1395) AND (a.Descricao LIKE \'%Seguro%\')) ' +
		                            'AND (a.FinanceiroID IS NULL) AND (a.PedidoID IS NULL) AND (DocumentoTransporteID IS NULL)) ' +
	                            'ORDER BY c.ImportacaoID, a.Descricao ASC';
    }


    dsoGrid.ondatasetcomplete = fillGrid_DSC;
    dsoGrid.refresh();
}

function fillGrid_DSC() 
{
    startGridInterface(fg);
    fg.FrozenCols = 0;

    headerGrid(fg, ['ImportacaoID',
                    'Tipo Custo',
                    'Descri��o',
                    'Valor',
                    'OK',
                    'Observa��o',
                    'ImpCustoID'], [6]);

    fillGridMask(fg, dsoGrid, ['ImportacaoID*',
                                'TipoCusto*',
                                'Descricao*',
                                'Valor*',
                                'OK',
                                'Observacao*',
                                'ImpCustoID'],
                                ['', '', '', '999999999.99', '', '', ''],
                                ['', '', '', '###,###,##0.00', '', '', '']);

    alignColsInGrid(fg, [3]);

    fg.ColDataType(getColIndexByColKey(fg, 'OK')) = 11;

    fg.Editable = true;

    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    fg.Redraw = 2;

    lockControlsInModalWin(false);
}

function btnIncluir_onclick(ctl) {
    lockControlsInModalWin(true);

    var strPars = new String();
    var sImpCustos = '';
    var nPessoaID = selPessoa.value;
    var nServicoID = selServico.value;
    var sDocumento = txtDocumento.value;
    var dtVencimento = dateFormatToSearch(txtVencimento.value);
    var nValor = txtValor.value;
    var nValorDesconto = txtValorDesconto.value;
    var nDocumentoTransporteID = selDocumentoTransporteID.value;
    var nTipoID = selTipoID.value;

    for (i = 1; i < fg.Rows; i++)
    {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0)
        {
            if (sImpCustos != '')
                sImpCustos += fg.TextMatrix(i, getColIndexByColKey(fg, 'ImpCustoID')) + '|';
            else
                sImpCustos = '|' + fg.TextMatrix(i, getColIndexByColKey(fg, 'ImpCustoID')) + '|';
        }
            
    }

    if (sImpCustos == '')
    {
        if (window.top.overflyGen.Alert('Selecione as linhas para cria��o do custo.') == 0)
            return null;

        lockControlsInModalWin(false);

        return;
    }

    //Ajuste para o valor do desconto sempre usar ".". LYF 07/12/2018
    nValorDesconto = replaceStr(nValorDesconto, ',', '.');

    strPars = '?sImpCustos=' + escape(sImpCustos);
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nServicoID=' + escape(nServicoID);
    strPars += '&sDocumento=' + escape(sDocumento);
    strPars += '&dtVencimento=' + escape(dtVencimento);
    strPars += '&nValor=' + escape(nValor);
    strPars += '&nValorDesconto=' + escape(nValorDesconto);
    strPars += '&nDocumentoTransporteID=' + escape(nDocumentoTransporteID);
    strPars += '&nTipoID=' + escape(nTipoID);

    try
    {
        dsoGrava.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/importacao/serverside/Importacao_CustoGerador.aspx' + strPars;
        dsoGrava.ondatasetcomplete = btnIncluir_onclick_DSC;
        dsoGrava.refresh();
    }
    catch (e)
    {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
    }
}

function btnIncluir_onclick_DSC()
{
    lockControlsInModalWin(false);

    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF))
    {
        dsoGrava.recordset.MoveFirst();

        if ((dsoGrava.recordset['Resultado'].value != null) && (dsoGrava.recordset['Resultado'].value != ''))
        {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0)
                return null;
        }
        else
        {
            if (window.top.overflyGen.Alert('Custo gerado com sucesso!') == 0)
                return null;

            fillGrid();
        }
    }
    else
    {
        if (window.top.overflyGen.Alert('Erro gerar Custo!') == 0)
            return null;
    }
}

function PesquisaPessoa_OnKeyPress()
{
    if (event.keyCode == 13)
    {
        glbCombo = selPessoa;
        preencheComboPessoa(txtPesquisaPessoa.value);
        txtPesquisaPessoa.value = '';
    }
}

function PesquisaServico_OnKeyPress()
{
    if (event.keyCode == 13)
    {
        glbCombo = selServico;
        preencheComboServico(txtPesquisaServico.value);
        txtPesquisaServico.value = '';
    }
} 

function preencheComboPessoa(Pesquisa)
{
    var sWhere = '';

    lockControlsInModalWin(true);

    if ((Pesquisa != undefined) && (Pesquisa != ''))
    {
        setConnection(dsoCombos);

        if ((Pesquisa.length == 14) && (!isNaN(Pesquisa)))
            sWhere = 'WHERE ((SELECT COUNT(1) FROM Pessoas_Documentos aa WITH(NOLOCK) WHERE (aa.PessoaID = a.PessoaID) AND (aa.TipoDocumentoID = 111) AND (aa.Numero = \'' + Pesquisa + '\')) > 0) ';
        else
            sWhere = 'WHERE ((((ISNUMERIC(\'' + Pesquisa + '\') = 1) AND (a.PessoaID = \'' + Pesquisa + '\')) ' +
                        'OR ((ISNUMERIC(\'' + Pesquisa + '\') = 0) AND (a.Fantasia LIKE \'%' + Pesquisa + '%\'))) ' +
                    'AND (a.EstadoID = 2) AND (a.TipoPessoaID = 52) AND (a.ClassificacaoID IN (56, 59, 60, 65, 68, 70))) ';

        dsoCombos.SQL = 'SELECT 0 AS fldID, \'\' AS fldName ' +
                             'UNION ALL ' +
                             'SELECT TOP 50 a.PessoaID AS fldID, a.Fantasia AS fldName ' +
                                'FROM Pessoas a WITH(NOLOCK) ' +
                                sWhere +
                                'ORDER BY fldName ASC';

        dsoCombos.ondatasetcomplete = preencheCombo_DSC;
        dsoCombos.Refresh();
    }
    else
    {
        lockControlsInModalWin(false);

        if (window.top.overflyGen.Alert('Digite algo no campo de pesquisa!') == 0)
            return null;
    }
}

function preencheComboServico(Pesquisa)
{
    if ((selPessoa.disabled == true) || (selPessoa.value == 0))
    {
        if (window.top.overflyGen.Alert('Selecione uma pessoa!') == 0)
            return null;

        return;
    }

    if (selTipoID.value != 1)
    {
        if (window.top.overflyGen.Alert('Selecione o tipo de documento "Nota Fiscal de Servi�o"') == 0)
            return null;

        return;
    }

    var nPessoaID = selPessoa.value;

    lockControlsInModalWin(true);

    if ((Pesquisa != undefined) && (Pesquisa != ''))
    {
        setConnection(dsoCombos);

        dsoCombos.SQL = 'SELECT 0 AS fldID, \'\' AS fldName ' +
                             'UNION ALL ' +
                         'SELECT a.ConServicoID AS fldID, (a.Codigo + \' - \' + a.DescricaoLocal) AS fldName ' +
                            'FROM Conceitos_Servicos a WITH(NOLOCK) ' +
                            'WHERE ((a.LocalidadeID = dbo.fn_Pessoa_Localidade(' + nPessoaID + ', 3, NULL, NULL)) AND (a.EstadoID = 2) ' +
                                'AND (((ISNUMERIC(\'' + Pesquisa + '\') = 1) AND (a.Codigo = \'' + Pesquisa + '\')) ' +
                                    'OR ((ISNUMERIC(\'' + Pesquisa + '\') = 0) AND (a.DescricaoLocal LIKE \'%' + Pesquisa + '%\'))))';

        dsoCombos.ondatasetcomplete = preencheCombo_DSC;
        dsoCombos.Refresh();
    }
    else
    {
        lockControlsInModalWin(false);

        if (window.top.overflyGen.Alert('Digite algo no campo de pesquisa!') == 0)
            return null;
    }
}

function preencheComboServico(Pesquisa)
{
    if ((selPessoa.disabled == true) || (selPessoa.value == 0))
    {
        if (window.top.overflyGen.Alert('Selecione uma pessoa!') == 0)
            return null;

        return;
    }

    if (selTipoID.value != 1)
    {
        if (window.top.overflyGen.Alert('Selecione o tipo de documento "Nota Fiscal de Servi�o"') == 0)
            return null;

        return;
    }

    var nPessoaID = selPessoa.value;

    lockControlsInModalWin(true);

    if ((Pesquisa != undefined) && (Pesquisa != ''))
    {
        setConnection(dsoCombos);

        dsoCombos.SQL = 'SELECT 0 AS fldID, \'\' AS fldName ' +
                             'UNION ALL ' +
                         'SELECT a.ConServicoID AS fldID, (a.Codigo + \' - \' + a.DescricaoLocal) AS fldName ' +
                            'FROM Conceitos_Servicos a WITH(NOLOCK) ' +
                            'WHERE ((a.LocalidadeID = dbo.fn_Pessoa_Localidade(' + nPessoaID + ', 3, NULL, NULL)) AND (a.EstadoID = 2) ' +
                                'AND (((ISNUMERIC(\'' + Pesquisa + '\') = 1) AND (a.Codigo = \'' + Pesquisa + '\')) ' +
                                    'OR ((ISNUMERIC(\'' + Pesquisa + '\') = 0) AND (a.DescricaoLocal LIKE \'%' + Pesquisa + '%\'))))';

        dsoCombos.ondatasetcomplete = preencheCombo_DSC;
        dsoCombos.Refresh();
    }
    else
    {
        lockControlsInModalWin(false);

        if (window.top.overflyGen.Alert('Digite algo no campo de pesquisa!') == 0)
            return null;
    }
}

function preencheComboDocumentoTransporte()
{
    var nImportacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ImportacaoID' + '\'' + '].value');

    lockControlsInModalWin(true);
        
    setConnection(dsoCombos);

    dsoCombos.SQL = 'SELECT 0 AS fldID, \'\' AS fldName ' +
                            'UNION ALL ' +
                    'SELECT a.DocumentoTransporteID AS fldID, CONVERT(VARCHAR(100), b.Numero) AS fldName ' +
                        'FROM DocumentosTransporte_Importacoes a WITH(NOLOCK) ' +
                            'INNER JOIN DocumentosTransporte b WITH(NOLOCK) ON (b.DocumentoTransporteID = a.DocumentoTransporteID) ' +
                        'WHERE (a.ImportacaoID = ' + nImportacaoID + ')';

    dsoCombos.ondatasetcomplete = preencheCombo_DSC;
    dsoCombos.Refresh();
}

function preencheCombo_DSC()
{
    clearComboEx([glbCombo.id]);

    if (!(dsoCombos.recordset.BOF || dsoCombos.recordset.EOF)) {
        dsoCombos.recordset.moveFirst;

        while (!dsoCombos.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoCombos.recordset['fldName'].value;
            oOption.value = dsoCombos.recordset['fldID'].value;
            glbCombo.add(oOption);
            dsoCombos.recordset.MoveNext();
        }

        lockControlsInModalWin(false);

        glbCombo.disabled = ((glbCombo.length <= 1) ? true : false);

        if (!glbCombo.disabled)
            glbCombo.focus();
    }
}

function selPessoa_OnChange()
{
    limpaCombo(selServico);
}

function selTipoID_OnChange()
{
    limpaCombo(selServico);

    if (selTipoID.value == 3)
    {
        glbCombo = selDocumentoTransporteID;
        preencheComboDocumentoTransporte();
    }
    else
        limpaCombo(selDocumentoTransporteID);
}

function limpaCombo(Combo)
{
    clearComboEx([Combo.id]);
    Combo.disabled = true;
}

function calculaTotalOK()
{
    var nValorTotal = 0;

    for (i = 1; i < fg.Rows; i++)
    {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0)
        {
            nValorTotal += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor*'));
        }
    }

    txtValor.value = nValorTotal;
}

function Gerencial_OnClick()
{
    fillGrid();
}