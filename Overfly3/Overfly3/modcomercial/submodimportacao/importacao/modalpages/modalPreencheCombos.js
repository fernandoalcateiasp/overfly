/********************************************************************
modalPreencheCombos.js

Library javascript para o modalPreencheCombos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_aEmpresaData = getCurrEmpresaData();
var glb_nTotalRows = 0;
var glb_sReadOnly = '';


var dsoItens = new CDatatransport("dsoItens");
var dsoGravar = new CDatatransport("dsoGravar");
var dsoCombos = new CDatatransport("dsoCombos");

var glbCombo = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalPreencheCombosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);

    btnOK.disabled = true;
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Preenche Combos', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));

    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 39;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles
    adjustElementsInForm([['lblPesquisaImportador', 'txtPesquisaImportador', 30, 1, -7, 20],
                          ['lblImportadorID', 'selImportadorID', 30, 1, -5],
                          ['lblPesquisaAgenteCarga', 'txtPesquisaAgenteCarga', 30, 2, -7],
                          ['lblAgenteCargaID', 'selAgenteCargaID', 30, 2, -5],
                          ['lblPesquisaPortoOrigem', 'txtPesquisaPortoOrigem', 30, 3, -7],
                          ['lblPortoOrigemID', 'selPortoOrigemID', 30, 3, -5],
                          ['lblPesquisaCompanhia', 'txtPesquisaCompanhia', 30, 4, -7],
                          ['lblCompanhiaID', 'selCompanhiaID', 30, 4, -5],
                          ['lblPesquisaPortoDestino', 'txtPesquisaPortoDestino', 30, 5, -7],
                          ['lblPortoDestinoID', 'selPortoDestinoID', 30, 5, -5],
                          ['lblPesquisaRecintoAduaneiro', 'txtPesquisaRecintoAduaneiro', 30, 6, -7],
                          ['lblRecintoAduaneiroID', 'selRecintoAduaneiroID', 30, 6, -5],
                          ['lblPesquisaTransportadoraDTA', 'txtPesquisaTransportadoraDTA', 30, 7, -7],
                          ['lblTransportadoraDTAID', 'selTransportadoraDTAID', 30, 7, -5],
                          ['lblPesquisaDespachante', 'txtPesquisaDespachante', 30, 8, -7],
                          ['lblDespachanteID', 'selDespachanteID', 30, 8, -5],
                          ['lblPesquisaTransportadora', 'txtPesquisaTransportadora', 30, 9, -7],
                          ['lblTransportadoraID', 'selTransportadoraID', 30, 9, -5],
						  ['btnGravar', 'btn', btnWidth, 10, 0, -12]], null, null, true);

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    btnGravar.onclick = preencheComboSuperor;

    selImportadorID.disabled = true;
    selAgenteCargaID.disabled = true;
    selPortoOrigemID.disabled = true;
    selCompanhiaID.disabled = true;
    selPortoDestinoID.disabled = true;
    selRecintoAduaneiroID.disabled = true;
    selTransportadoraDTAID.disabled = true;
    selDespachanteID.disabled = true;
    selTransportadoraID.disabled = true;

    txtPesquisaImportador.onkeypress = PesquisaImportador_OnKeyPress;
    txtPesquisaAgenteCarga.onkeypress = PesquisaAgenteCarga_OnKeyPress;
    txtPesquisaPortoOrigem.onkeypress = PesquisaPortoOrigem_OnKeyPress;
    txtPesquisaCompanhia.onkeypress = PesquisaCompanhia_OnKeyPress;
    txtPesquisaPortoDestino.onkeypress = PesquisaPortoDestino_OnKeyPress;
    txtPesquisaRecintoAduaneiro.onkeypress = PesquisaRecintoAduaneiro_OnKeyPress;
    txtPesquisaTransportadoraDTA.onkeypress = PesquisaTransportadoraDTA_OnKeyPress;
    txtPesquisaDespachante.onkeypress = PesquisaDespachante_OnKeyPress;
    txtPesquisaTransportadora.onkeypress = PesquisaTransportadora_OnKeyPress;

    selImportadorID.onchange = ImportadorOnchange;
    selAgenteCargaID.onchange = AgenteCargaOnchange;
    selPortoOrigemID.onchange = PortoOrigemOnchange;
    selCompanhiaID.onchange = CompanhiaOnchange;
    selPortoDestinoID.onchange = PortoDestinoOnchange;
    selRecintoAduaneiroID.onchange = RecintoAduaneiroOnchange;
    selTransportadoraDTAID.onchange = TransportadoraDTAOnchange;
    selDespachanteID.onchange = DespachanteOnchange;
    selTransportadoraID.onchange = TransportadoraOnchange;
}

function PesquisaImportador_OnKeyPress()
{
    if (event.keyCode == 13)
    {
        glbCombo = selImportadorID;
        preencheCombo(txtPesquisaImportador.value);
        txtPesquisaImportador.value = '';
    }
}

function PesquisaAgenteCarga_OnKeyPress()
{
    if (event.keyCode == 13)
    {
        glbCombo = selAgenteCargaID;
        preencheCombo(txtPesquisaAgenteCarga.value);
        txtPesquisaAgenteCarga.value = '';
    }
}

function PesquisaPortoOrigem_OnKeyPress() 
{
    if (event.keyCode == 13)
    {
        glbCombo = selPortoOrigemID;
        preencheCombo(txtPesquisaPortoOrigem.value);
        txtPesquisaPortoOrigem.value = '';
    }
}

function PesquisaCompanhia_OnKeyPress() 
{
    if (event.keyCode == 13)
    {
        glbCombo = selCompanhiaID;
        preencheCombo(txtPesquisaCompanhia.value);
        txtPesquisaCompanhia.value = '';
    }
}

function PesquisaPortoDestino_OnKeyPress() 
{
    if (event.keyCode == 13)
    {
        glbCombo = selPortoDestinoID;
        preencheCombo(txtPesquisaPortoDestino.value);
        txtPesquisaPortoDestino.value = '';
    }
}

function PesquisaRecintoAduaneiro_OnKeyPress() 
{
    if (event.keyCode == 13)
    {
        glbCombo = selRecintoAduaneiroID;
        preencheCombo(txtPesquisaRecintoAduaneiro.value);
        txtPesquisaRecintoAduaneiro.value = '';
    }
}

function PesquisaTransportadoraDTA_OnKeyPress()
{
    if (event.keyCode == 13)
    {
        glbCombo = selTransportadoraDTAID;
        preencheCombo(txtPesquisaTransportadoraDTA.value);
        txtPesquisaTransportadoraDTA.value = '';
    }
}

function PesquisaDespachante_OnKeyPress() 
{
    if (event.keyCode == 13)
    {
        glbCombo = selDespachanteID;
        preencheCombo(txtPesquisaDespachante.value);
        txtPesquisaDespachante.value = '';
    }
}

function PesquisaTransportadora_OnKeyPress()
{
    if (event.keyCode == 13)
    {
        glbCombo = selTransportadoraID;
        preencheCombo(txtPesquisaTransportadora.value);
        txtPesquisaTransportadora.value = '';
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id)
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S', null);
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null);

}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
- null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // mostra a janela modal
    ;
}

function preencheCombo(Pesquisa)
{
    var sWhere = '';

    lockControlsInModalWin(true);

    setConnection(dsoCombos);

    if ((Pesquisa != undefined) && (Pesquisa != ''))
    {
        if ((Pesquisa.length == 14) && (!isNaN(Pesquisa)))
            sWhere = 'WHERE ((SELECT COUNT(1) FROM Pessoas_Documentos aa WITH(NOLOCK) WHERE (aa.PessoaID = a.PessoaID) AND (aa.TipoDocumentoID = 111) AND (aa.Numero = \'' + Pesquisa + '\')) > 0) ';
        else
            sWhere = 'WHERE ((((ISNUMERIC(\'' + Pesquisa + '\') = 1) AND (a.PessoaID = \'' + Pesquisa + '\')) ' +
                        'OR ((ISNUMERIC(\'' + Pesquisa + '\') = 0) AND (a.Fantasia LIKE \'%' + Pesquisa + '%\'))) ' +
                    'AND (a.EstadoID = 2) AND (a.TipoPessoaID = 52) AND (a.ClassificacaoID IN (56, 59, 60, 65, 68, 70))) ';

        dsoCombos.SQL = 'SELECT 0 AS fldID, \'\' AS fldName ' +
                             'UNION ALL ' +
                             'SELECT TOP 50 a.PessoaID AS fldID, a.Fantasia AS fldName ' +
                                'FROM Pessoas a WITH(NOLOCK) ' +
                                sWhere +
                                'ORDER BY fldName ASC';

        dsoCombos.ondatasetcomplete = preencheCombo_DSC;
        dsoCombos.Refresh();
    }
}

function preencheCombo_DSC()
{
    clearComboEx([glbCombo.id]);

    if (!(dsoCombos.recordset.BOF || dsoCombos.recordset.EOF))
    {
        dsoCombos.recordset.moveFirst;

        while (!dsoCombos.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoCombos.recordset['fldName'].value;
            oOption.value = dsoCombos.recordset['fldID'].value;
            glbCombo.add(oOption);
            dsoCombos.recordset.MoveNext();
        }

        lockControlsInModalWin(false);

        glbCombo.disabled = ((glbCombo.length <= 1) ? true : false);

        if (!glbCombo.disabled)
            glbCombo.focus();
    }
}

function preencheComboSuperor()
{
    if (selImportadorID.value > 0)
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'modalCombosPreenche(selImportadorID, ' + selImportadorID.value + ', \'' + document.getElementById("selImportadorID").options[document.getElementById("selImportadorID").selectedIndex].text + '\')');

    if (selAgenteCargaID.value > 0)
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'modalCombosPreenche(selAgenteCargaID, ' + selAgenteCargaID.value + ', \'' + document.getElementById("selAgenteCargaID").options[document.getElementById("selAgenteCargaID").selectedIndex].text + '\')');

    if (selPortoOrigemID.value > 0)
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'modalCombosPreenche(selPortoOrigemID, ' + selPortoOrigemID.value + ', \'' + document.getElementById("selPortoOrigemID").options[document.getElementById("selPortoOrigemID").selectedIndex].text + '\')');

    if (selCompanhiaID.value > 0)
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'modalCombosPreenche(selCompanhiaID, ' + selCompanhiaID.value + ', \'' + document.getElementById("selCompanhiaID").options[document.getElementById("selCompanhiaID").selectedIndex].text + '\')');

    if (selPortoDestinoID.value > 0)
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'modalCombosPreenche(selPortoDestinoID, ' + selPortoDestinoID.value + ', \'' + document.getElementById("selPortoDestinoID").options[document.getElementById("selPortoDestinoID").selectedIndex].text + '\')');

    if (selRecintoAduaneiroID.value > 0)
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'modalCombosPreenche(selRecintoAduaneiroID, ' + selRecintoAduaneiroID.value + ', \'' + document.getElementById("selRecintoAduaneiroID").options[document.getElementById("selRecintoAduaneiroID").selectedIndex].text + '\')');

    if (selTransportadoraDTAID.value > 0)
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'modalCombosPreenche(selTransportadoraDTAID, ' + selTransportadoraDTAID.value + ', \'' + document.getElementById("selTransportadoraDTAID").options[document.getElementById("selTransportadoraDTAID").selectedIndex].text + '\')');

    if (selDespachanteID.value > 0)
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'modalCombosPreenche(selDespachanteID, ' + selDespachanteID.value + ', \'' + document.getElementById("selDespachanteID").options[document.getElementById("selDespachanteID").selectedIndex].text + '\')');

    if (selTransportadoraID.value > 0)
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'modalCombosPreenche(selTransportadora, ' + selTransportadoraID.value + ', \'' + document.getElementById("selTransportadoraID").options[document.getElementById("selTransportadoraID").selectedIndex].text + '\')');

    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S', null);
}

function ImportadorOnchange()
{
    lblImportadorID.innerText = 'Importador';

    lblImportadorID.style.width = 200;

    if (selImportadorID.length > 0)
    {
        if (selImportadorID.value > 0)
            lblImportadorID.innerText = lblImportadorID.innerText + ' ' + selImportadorID.value;
    }
}

function AgenteCargaOnchange()
{
    lblAgenteCargaID.innerText = 'Agente de Cargas';

    lblAgenteCargaID.style.width = 200;

    if (selAgenteCargaID.length > 0)
    {
        if (selAgenteCargaID.value > 0)
            lblAgenteCargaID.innerText = lblAgenteCargaID.innerText + ' ' + selAgenteCargaID.value;
    }
}

function PortoOrigemOnchange()
{
    lblPortoOrigemID.innerText = 'Origem';

    lblPortoOrigemID.style.width = 200;

    if (selPortoOrigemID.length > 0)
    {
        if (selPortoOrigemID.value > 0)
            lblPortoOrigemID.innerText = lblPortoOrigemID.innerText + ' ' + selPortoOrigemID.value;
    }
}

function CompanhiaOnchange()
{
    lblCompanhiaID.innerText = 'Companhia';

    lblCompanhiaID.style.width = 200;

    if (selCompanhiaID.length > 0)
    {
        if (selCompanhiaID.value > 0)
            lblCompanhiaID.innerText = lblCompanhiaID.innerText + ' ' + selCompanhiaID.value;
    }
}

function PortoDestinoOnchange()
{
    lblPortoDestinoID.innerText = 'Destino';

    lblPortoDestinoID.style.width = 200;

    if (selPortoDestinoID.length > 0)
    {
        if (selPortoDestinoID.value > 0)
            lblPortoDestinoID.innerText = lblPortoDestinoID.innerText + ' ' + selPortoDestinoID.value;
    }
}

function RecintoAduaneiroOnchange()
{
    lblRecintoAduaneiroID.innerText = 'Recinto Aduaneiro';

    lblRecintoAduaneiroID.style.width = 200;

    if (selRecintoAduaneiroID.length > 0)
    {
        if (selRecintoAduaneiroID.value > 0)
            lblRecintoAduaneiroID.innerText = lblRecintoAduaneiroID.innerText + ' ' + selRecintoAduaneiroID.value;
    }
}

function TransportadoraDTAOnchange()
{
    lblTransportadoraDTAID.innerText = 'Transportadora DTA';

    lblTransportadoraDTAID.style.width = 200;

    if (selTransportadoraDTAID.length > 0)
    {
        if (selTransportadoraDTAID.value > 0)
            lblTransportadoraDTAID.innerText = lblTransportadoraDTAID.innerText + ' ' + selTransportadoraDTAID.value;
    }
}

function DespachanteOnchange()
{
    lblDespachanteID.innerText = 'DespachanteID';

    lblDespachanteID.style.width = 200;

    if (selDespachanteID.length > 0)
    {
        if (selDespachanteID.value > 0)
            lblDespachanteID.innerText = lblDespachanteID.innerText + ' ' + selDespachanteID.value;
    }
}

function TransportadoraOnchange()
{
    lblTransportadoraID.innerText = 'Transportadora';

    lblTransportadoraID.style.width = 200;

    if (selTransportadoraID.length > 0)
    {
        if (selTransportadoraID.value > 0)
            lblTransportadoraID.innerText = lblTransportadoraID.innerText + ' ' + selTransportadoraID.value;
    }
}