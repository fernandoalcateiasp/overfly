<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalRateiosHtml" name="modalRateiosHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/submodimportacao/Importacao/modalpages/modalRateios.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodimportacao/Importacao/modalpages/modalRateios.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Dim i, sCaller, nImportacaoID

sCaller = ""
nImportacaoID = 0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

'nImportacaoID
For i = 1 To Request.QueryString("nImportacaoID").Count    
    nImportacaoID = Request.QueryString("nImportacaoID")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nImportacaoID = " & Chr(39) & nImportacaoID & Chr(39) & ";"
Response.Write vbcrlf

'Monta array dos relatorios
Response.Write "// Array dos relatorios, com seus ids, nomes"
Response.Write vbcrlf

Response.Write "</script>"

Response.Write vbcrlf

%>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
<!--
    js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    //-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
    js_modalRateios_ValidateEdit();
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
    js_modalRateios_AfterEdit(arguments[0], arguments[1]);
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
    js_modalRateiosKeyPress(arguments[0]);
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
    js_fg_modalRateiosDblClick(fg, fg.Row, fg.Col);
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
    js_fg_modalRateiosBeforeMouseDown(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
    js_fg_EnterCell(fg);
    //-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
    fg_AfterRowColChange();
    //-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
    js_fg_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
    //-->
</SCRIPT>

</head>

<body id="modalRateiosBody" name="modalRateiosBody" LANGUAGE="javascript" onload="return window_onload()">

    <div id="divPesquisa" name="divPesquisa" class="divGeneral">
        <p id="lblRateio" name="lblRateio" class="lblGeneral">Rateio</p>
        <select id="selRateio" name="selRateio" class="fldGeneral">
            <option value="1">Nacionalização</option>
            <option value="2" SELECTED>Contábil</option>
            <option value="3">Gerencial</option>
            <option value="4">Resumo</option>
        </select>
    
        <p id="lblFabricanteID" name="lblFabricanteID" class="lblGeneral">Fabricante</p>
        <select id="selFabricanteID" name="selFabricanteID" class="fldGeneral"></select>

        <input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" class="btns">
        <input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" class="btns">
        <input type="button" id="btnGerarPedido" name="btnGerarPedido" value="Gerar Pedido" LANGUAGE="javascript" class="btns">
        <input type="button" id="btnExcel" name="btnExcel" value="Excel" LANGUAGE="javascript" class="btns">
    </div>

    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
    
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>

</body>

</html>
