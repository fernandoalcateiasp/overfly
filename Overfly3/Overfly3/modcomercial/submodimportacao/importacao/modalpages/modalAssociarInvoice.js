/********************************************************************
modalAssociarInvoice.js

Library javascript para o modalAssociarInvoice.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************

var modPesqRel_TimerHandler = null;
var glb_aEmpresaData = getCurrEmpresaData();
var glb_nRow = 0;
var glb_nTotalRows = 0;

var dsoGrava = new CDatatransport("dsoGrava");
var dsoPesq = new CDatatransport("dsoPesq");
var dsoFabricante = new CDatatransport("dsoFabricante");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // ajusta o body do html
    with (modalAssociarInvoiceBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no campo apropriado
    if (document.getElementById('selFabricanteID').disabled == false)
        selFabricanteID.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Associar Invoices', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;

    // ajusta o divPesquisa
    with (divPesquisa.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        height = 40;

    }

    // selFabricanteID
    selFabricanteID.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (selFabricanteID.style) {
        left = 0;
        top = 16;
        width = (selFabricanteID.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }

    selFabricanteID.onkeypress = selFabricanteID_onKeyPress;

    // btnFindPesquisa
    with (btnFindPesquisa.style) {
        top = parseInt(selFabricanteID.style.top, 10);
        left = parseInt(selFabricanteID.style.left, 10) + parseInt(selFabricanteID.style.width, 10) + 2;
        width = 80;
        height = 24;
    }

    // btnAssociar
    btnAssociar.disabled = true;
    with (btnAssociar.style) {
        top = parseInt(selFabricanteID.style.top, 10);
        left = parseInt(btnFindPesquisa.style.left, 10) + parseInt(btnFindPesquisa.style.width, 10) + 2;
        width = 80;
        height = 24;
    }
    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
    }

    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.style.top, 10) + parseInt(divPesquisa.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 46;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    cbmExportador();

    startGridInterface(fg);
    headerGrid(fg, ['InvoiceID',
                    'Invoice',
                    'Exportador',
                    'Prazo',
                    'Total FOB',
                    'Observacao',
                    'Associar'], [0]);

    fg.Redraw = 2;
}

function selFabricanteID_onKeyPress() {
    if (event.keyCode == 13) {
        btnFindPesquisa_onclick();
    }
}

function btnFindPesquisa_onclick(ctl)
{
    selFabricanteID.value = trimStr(selFabricanteID.value);

    startPesq(selFabricanteID.value);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqRelKeyPress(KeyAscii) {
    if (fg.Row < 1)
        return true;

    if ((KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false))
        btn_onclick(btnOK);

}
function js_modalAssociarInvoice_BeforeEdit(grid, Row, Col){
    ;
}
function js_modalAssociarInvoice_AfterEdit(grid, Row, Col) {

    var bHabilita = 0;

    if (grid.Editable) {
        if (Col == getColIndexByColKey(grid, 'Associar')) {
            for (var i = 1; i < grid.Rows; i++) {
                if (grid.TextMatrix(i, getColIndexByColKey(grid, 'Associar')) != 0)
                {
                    bHabilita = 1;
                    btnAssociar.disabled = false;
                }
                else if ((grid.TextMatrix(i, getColIndexByColKey(grid, 'Associar')) == 0) && bHabilita == 0)
                {
                    btnAssociar.disabled = true;
                }
                    
            }
        }
    }
}
/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('OK_CALLFORM_I'), null);
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_I'), null);
    }
}

function startPesq(strPesquisa)
{
    lockControlsInModalWin(true);

    var nFabricanteID = selFabricanteID.value;
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];
    var sSQL = '';

    sSQL = 'SELECT a.InvoiceID, a.Invoice, b.Fantasia [Exportador], a.Prazo,  a.ValorTotalInvoice [TotalFOB], a.Observacao, 0 [Associar] ' +
                'FROM Invoices a WITH(NOLOCK) ' +
                    'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ExportadorID) ' +
            'WHERE ((a.EstadoID = 67) AND (a.ImportacaoID IS NULL) AND (a.EmpresaID = ' + nEmpresaID + ') ' + (nFabricanteID == 0 ? ') ' : ' AND (a.ExportadorID = ' + nFabricanteID + ')) ') +
            'ORDER BY ImportacaoID DESC';

    dsoPesq.SQL = sSQL;
    dsoPesq.ondatasetcomplete = eval(dsopesq_DSC);
    dsoPesq.refresh();

}

function dsopesq_DSC()
{
    startGridInterface(fg);
    fg.FrozenCols = 0;

    headerGrid(fg, ['InvoiceID',
                    'Invoice',
                    'Exportador',
                    'Prazo',
                    'Total FOB',
                    'Observa��o',
                    'Associar'], [0]);

    fillGridMask(fg, dsoPesq, ['InvoiceID*',
                                'Invoice*',
                                'Exportador*',
                                'Prazo*',
                                'TotalFOB*',
                                'Observacao*',
                                'Associar'],
                                ['', '', '', '', '', '', '']);

    with (fg)
    {
        ColDataType(6) = 11;
    }

    fg.Editable = true;
    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
    }
    else
        btnOK.disabled = true;
}

function btnAssociar_onclick(ctl)
{
    lockControlsInModalWin(true);

    var strPars = new String();
    var Associar;
    var nInvoiceID;
    var nImportacaoID;

    if (glb_nRow == 0)
    {
        glb_nTotalRows = fg.Rows;
        glb_nRow = 1;
    }

    if (glb_nRow < glb_nTotalRows)
    {
        var strPars = '';

        Associar = (fg.ValueMatrix(glb_nRow, getColIndexByColKey(fg, 'Associar')) != 0);

        if (Associar) {
            nInvoiceID = fg.textMatrix(glb_nRow, getColIndexByColKey(fg, 'InvoiceID*'));
            nImportacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ImportacaoID' + '\'' + '].value');

            strPars += '?InvoiceID=' + escape(nInvoiceID);
            strPars += '&ImportacaoID=' + escape(nImportacaoID);

            sendDataToServer(strPars);
        }
    }
    else
    {
        glb_nTotalRows = 0;
        glb_nRow = 0;

        selFabricanteID.value = trimStr(selFabricanteID.value);

        startPesq(selFabricanteID.value);
    }
}

function sendDataToServer(strPars) {
    try {
        dsoGrava.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/importacao/serverside/AssociarInvoice.aspx' + strPars;
        dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
        dsoGrava.refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
    }
}

function sendDataToServer_DSC() {
    if (glb_nRow > 0) {
        glb_nRow++;
        btnAssociar_onclick();
    }
}

function cbmExportador() {

    lockControlsInModalWin(true);

    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];

    setConnection(dsoFabricante);
    dsoFabricante.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
                        'UNION ALL ' +
                        'SELECT DISTINCT a.PessoaID AS fldID, a.Fantasia AS fldName ' +
                            'FROM Pessoas a WITH(NOLOCK) ' +
                            'INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (b.SujeitoID = a.PessoaID) ' +
                        'WHERE ((a.ClassificacaoID = 59) AND (a.EstadoID = 2) AND (b.ObjetoID = ' + nEmpresaID + ' /*EmpresaID*/) AND (dbo.fn_Pessoa_Localidade(a.PessoaID, 1, NULL, NULL) <> 130)) ' +
                        'ORDER BY fldName ASC ';
    dsoFabricante.ondatasetcomplete = fillExportador_DSC;
    dsoFabricante.Refresh();
}

function fillExportador_DSC() {
    if (!((dsoFabricante.recordset.BOF) && (dsoFabricante.recordset.EOF))) {
        clearComboEx(['selFabricanteID']);

        while (!dsoFabricante.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoFabricante.recordset['fldName'].value;
            oOption.value = dsoFabricante.recordset['fldID'].value;
            selFabricanteID.add(oOption);
            dsoFabricante.recordset.MoveNext();
        }
    }
    lockControlsInModalWin(false);
}