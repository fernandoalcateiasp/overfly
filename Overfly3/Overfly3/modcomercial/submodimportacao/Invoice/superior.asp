<%@  language="VBSCRIPT" enablesessionstate="False" %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="invoicesup01Html" name="invoicesup01Html">

<head>

    <title></title>

    <%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodimportacao/Invoice/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodimportacao/Invoice/superioresp.js" & Chr(34) & "></script>" & vbCrLf
    %>

    <%
	Dim strSQL, rsData
	Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
	Response.Write vbcrlf
	
	Response.Write "glb_nIdiomaID = 0;"
	Response.Write vbcrlf
	
	strSQL = "SELECT IdiomaID FROM Recursos WITH(NOLOCK) WHERE (RecursoID = 999)"
	
	Set rsData = Server.CreateObject("ADODB.Recordset")         
	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (NOT (rsData.BOF AND rsData.EOF)) Then
		Response.Write "glb_nIdiomaID = " & CStr(rsData.Fields("IdiomaID").Value) & ";"
	End If
	
	rsData.Close()
	Set rsData = Nothing
	Response.Write "</script>"
	Response.Write vbcrlf
    %>

    <script id="wndJSProc" language="javascript">
<!--
    //-->
    </script>

</head>

<!-- //@@ -->
<body id="invoicesup01Body" name="invoicesup01Body" language="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral" />

    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->

        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral" />

        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" datasrc="#dsoStateMachine" datafld="Estado" toread class="fldGeneral" />

        <p id="lblInvoice" name="lblInvoice" class="lblGeneral">Invoice</p>
        <input type="text" id="txtInvoice" name="txtInvoice" class="fldGeneral" datasrc="#dsoSup01" datafld="Invoice" class="fldGeneral" />

        <p id="lbldtEmissao" name="lbldtEmissao " class="lblGeneral">Emiss�o</p>
        <input type="text" id="txtdtEmissao" name="txtdtEmissao" datasrc="#dsoSup01" datafld="V_dtEmissao" class="fldGeneral" />

        <p id="lblExportadorID" name="lblExportadorID" class="lblGeneral">Exportador</p>
        <select id="selExportadorID" name="selExportadorID" class="fldGeneral" datasrc="#dsoSup01" datafld="ExportadorID"></select>
        <input type="image" id="btnFindPessoa" name="btnFindPessoa" class="fldGeneral" title="Preencher Combo"
            width="24" height="23">

        <p id="lblPrazo" name="lblPrazo" class="lblGeneral">Prazo</p>
        <input type="text" id="txtPrazo" name="txtPrazo" datasrc="#dsoSup01" datafld="Prazo" class="fldGeneral" />
          
        <p id="lblPedidoCompraID" name="lblPedidoCompraID" class="lblGeneral">Pedido de Compra</p>
        <input type="text" id="txtPedidoCompraID" name="txtPedidoCompraID" datasrc="#dsoSup01" datafld="PedidoCompraID" class="fldGeneral" title="Purchase Order"/>

        <p id="lblPedidoFornecedor" name="lblPedidoFornecedor" class="lblGeneral">Pedido Fornecedor</p>
        <input type="text" id="txtPedidoFornecedor" name="txtPedidoFornecedor" datasrc="#dsoSup01" datafld="PedidoFornecedor" class="fldGeneral" title="Sales Order"/>
      
        <p id="lblPesoBrutoTotal" name="lblPesoBrutoTotal" class="lblGeneral">Peso Bruto</p>
        <input type="text" id="txtPesoBrutoTotal" name="txtPesoBrutoTotal" datasrc="#dsoSup01" datafld="PesoBrutoTotal" class="fldGeneral" />

        <p id="lblPesoLiquidoTotal" name="lblPesoLiquidoTotal" class="lblGeneral">Peso Liquido</p>
        <input type="text" id="txtPesoLiquidoTotal" name="txtPesoLiquidoTotal" datasrc="#dsoSup01" datafld="PesoLiquidoTotal" class="fldGeneral" />

        <p id="lblItens" name="lblItens" class="lblGeneral">Itens</p>
        <input type="text" id="txtItens" name="txtItens" datasrc="#dsoSup01" datafld="ItensInvoice" class="fldGeneral" />

        <p id="lblMoedaID" name="lblMoedaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaID" name="selMoedaID" class="fldGeneral" datasrc="#dsoSup01" datafld="MoedaID"></select>

        <p id="lblTaxaMoeda" name="lblTaxaMoeda" class="lblGeneral">Taxa</p>
        <input type="text" id="txtTaxaMoeda" name="txtTaxaMoeda" datasrc="#dsoSup01" datafld="TaxaMoeda" class="fldGeneral" />

        <p id="lblValorTotalInvoice" name="lblValorTotalInvoice" class="lblGeneral">Valor Total</p>
        <input type="text" id="txtValorTotalInvoice" name="txtValorTotalInvoice" datasrc="#dsoSup01" datafld="ValorTotalInvoice" class="fldGeneral" />

        <p id="lblTipoFreteID" name="lblTipoFreteID" class="lblGeneral">Tipo Frete</p>
        <select id="selTipoFreteID" name="selTipoFreteID" DATASRC="#dsoSup01" DATAFLD="TipoFreteID" class="fldGeneral"></select>

        <p id="lblProcessoImportacao" name="lblProcessoImportacao" class="lblGeneral">Importa��o</p>
        <input type="text" id="txtProcessoImportacao" name="txtProcessoImportacao" datasrc="#dsoSup01" datafld="ProcessoImportacao" class="fldGeneral" />

        <p id="lblValorFreteTotal" name="lblValorFreteTotal" class="lblGeneral">Frete Total</p>
        <input type="text" id="txtValorFreteTotal" name="txtValorFreteTotal" datasrc="#dsoSup01" datafld="ValorFreteTotal" class="fldGeneral" />

        <p id="lblValorSeguroTotal" name="lblValorSeguroTotal" class="lblGeneral">Seguro Total</p>
        <input type="text" id="txtValorSeguroTotal" name="txtValorSeguroTotal" datasrc="#dsoSup01" datafld="ValorSeguroTotal" class="fldGeneral" />

        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" datasrc="#dsoSup01" datafld="Observacao" class="fldGeneral" />

    </div>

</body>

</html>
