/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form Invoice
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Dados da listagem da pesquisa
var dsoListData01 = new CDatatransport("dsoListData01");
// Dados dos combos de contexto e filtros
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");  
// Dados dos proprietarios para o pesqlist .URL
var dsoPropsPL = new CDatatransport("dsoPropsPL");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/
// IMPLEMENTACAO DAS FUNCOES
function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    /*InvoiceID(ID), EmpresaID(Empresa), EstadoID(e), Invoice, dtEmissao(Emiss�o), ExportadorID(Exportador), ValorTotalInvoice*/
    glb_COLPESQORDER = new Array(
		"ID",
        "Est",
        "Invoice", 
        "Emiss�o",
        "Exportador",
        "Itens",
        "Valor Total Invoice");

    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';


    glb_aCOLPESQFORMAT = new Array('', '', '', dTFormat, '', '', '###,###,##0.00');
    
    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
    showBtnsEspecControlBar('sup', true, [1,1,1,0]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimentos']);

    setupEspecBtnsControlBar('sup', 'HHHD');
    
    alignColsInGrid(fg,[0,2,5]);
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/       
function btnBarClicked(controlBar, btnClicked)
{
  // Documentos
  if ((controlBar == 'SUP') && (btnClicked == 1)) 
    {
    if (fg.Rows > 1)
        __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
    
    else 
        window.top.overflyGen.Alert('Selecione um registro.');
  }



}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
     // Modal de documentos
     if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') 
     {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }    
     }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}




