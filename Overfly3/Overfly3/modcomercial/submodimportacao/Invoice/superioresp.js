/********************************************************************
js_especificsup.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister) {
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL =
            'SELECT TOP 1 *, ' +
               'CONVERT(VARCHAR, a.dtEmissao, ' + DATE_SQL_PARAM + ') as V_dtEmissao, ' +
               '(SELECT aa.ProcessoImportacao FROM Importacao aa WITH(NOLOCK) WHERE (aa.ImportacaoID = a.ImportacaoID)) AS ProcessoImportacao, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID = AlternativoID )> 0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM Invoices a  WITH(NOLOCK) ' +
               'WHERE ProprietarioID = ISNULL(dbo.fn_Fornecedor_Responsavel(a.ExportadorID, a.EmpresaID, 1) , ' + nID + ')  ORDER BY a.InvoiceID DESC';



    else
        sSQL = 'SELECT * , ' +
               'CONVERT(VARCHAR, a.dtEmissao, ' + DATE_SQL_PARAM + ') as V_dtEmissao, ' +
               '(SELECT aa.ProcessoImportacao FROM Importacao aa WITH(NOLOCK) WHERE (aa.ImportacaoID = a.ImportacaoID)) AS ProcessoImportacao, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID = a.AlternativoID )>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM dbo.Invoices a WITH(NOLOCK) ' +
               'WHERE   a.InvoiceID = ' + nID + ' ORDER BY a.InvoiceID DESC';

    setConnection(dso);
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();

    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso) {
    setConnection(dso);
    var sql;
    sql = 'SELECT * ,  ' +
               'CONVERT(VARCHAR, dtEmissao, ' + DATE_SQL_PARAM + ') as V_dtEmissao ' +
            'FROM Invoices  WITH(NOLOCK) WHERE InvoiceID = 0';

    dso.SQL = sql;
}
