/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)       
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID) {
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);


    //@@
    if ((folderID == 20008) || (folderID == 20009)) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.InvoiceID, a.ProprietarioID,a.AlternativoID,a.Observacoes FROM Invoices a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.InvoiceID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.RegistroID = ' + idToFind + ' ' +
                    'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24321) // Itens
    {
        dso.SQL = 'SELECT a.* ' +
                    'FROM Invoices_Itens a WITH (NOLOCK) ' +
                    'WHERE a.InvoiceID = ' + idToFind + ' ' +
                    'ORDER BY a.Item';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28003) // Lancamentos
    {
        dso.SQL = 'EXEC sp_EventosContabeis_Lancamentos ' + glb_nFormID + ', NULL, ' + idToFind;
        return 'dso01Grid_DSC';
    }

}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID) {
    setConnection(dso);
    var nUsuarioID = getCurrUserID();

    var nEmpresaData = getCurrEmpresaData();
    var empresaId = nEmpresaData[0];

    // LOG
    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a, Recursos b ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a, Pessoas b ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a, TiposAuxiliares_Itens b ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a, Recursos b ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }

    }
    
    // Itens
    else if (pastaID == 24321)
    {
        if (dso.id == 'dsoCmb01Grid_01')
        {
            dso.SQL = "SELECT DISTINCT c.ConceitoID as MoedaID, c.SimboloMoeda as Moeda " +
                            "FROM RelacoesPesRec a WITH(NOLOCK) " +
                                "INNER JOIN RelacoesPesRec_Moedas b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) " +
                                "INNER JOIN Conceitos c WITH(NOLOCK) ON (b.MoedaID = c.ConceitoID) " +
                            "WHERE (a.SujeitoID = " + empresaId + " AND a.ObjetoID = 999)";
        }
        else if (dso.id == 'dso01GridLkp')
        {
            dso.SQL = 'SELECT a.InvItemID, (a.ValorFOBUnitario * a.Quantidade) AS ValorFOBTotal, (a.PesoBrutoUnitario * a.Quantidade) AS PesoBrutoTotal, ' + 
                            '(a.PesoLiquitoUnitario * a.Quantidade) AS PesoLiquitoTotal, (a.ValorFOBUnitario - (dbo.fn_Importacao_Itens_Totais(b.ImportacaoID, a.InvItemID, 23) / a.Quantidade)) AS ValorFobAtualizado ' +
                      'FROM Invoices_Itens a WITH(NOLOCK) ' +
                        'INNER JOIN Invoices b WITH(NOLOCK) ON (b.InvoiceID = a.InvoiceID) ' +
                      'WHERE (a.InvoiceID = ' + nRegistroID + ')';
        }
    }
}
/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID) {
    var i = 0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

    cleanupSelInControlBar('inf', 1);

    vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG  

    vPastaName = window.top.getPastaName(24321);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24321); //invoices itens

    vPastaName = window.top.getPastaName(28003);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28003); // Lancamento
    

    // Nao mexer - Inicio de automacao ============================== 	
    // Tenta selecionar novamente a pasta anterior selecionada no combo
    // Se nao mais existe a pasta anteriormente selecionada no combo
    // seleciona a pasta default

    stripFoldersByRight(currSelection);

    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID) {
    //@@ return true se tudo ok, caso contrario return false;

    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 24321) // invoices Produtos
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [0, 3, 4, 5, 7, 9], ['InvoiceID', registroID]);
        }        
    }

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO) {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.FrozenCols = 0;

    if (folderID == 20010) // LOG
    {
        headerGrid(fg, ['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);

        fillGridMask(fg, currDSO, ['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['', '99/99/9999', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm:ss', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 24321) // invoices
    {
        headerGrid(fg, ['Item',
                        'ID',
                        'Produto',
                        'Quantidade',
                        'Moeda',
                        'Valor Unitario',
                        'Valor Unitario Atualizado',
                        'Valor Total',
                        'Peso Bruto Unitario',
                        'Peso Bruto Total',
                        'Peso Liquido Unitario',
                        'Peso Liquido Total',
                        'InvItemID'], [12]);

        fillGridMask(fg, currDSO, ['Item',
                                   'ProdutoID',
                                   'Produto',
                                   'Quantidade',
                                   'MoedaID',
                                   'ValorFOBUnitario',
                                   '^InvItemID^dso01GridLkp^InvItemID^ValorFobAtualizado*',
                                   '^InvItemID^dso01GridLkp^InvItemID^ValorFOBTotal*',
                                   'PesoBrutoUnitario',
                                   '^InvItemID^dso01GridLkp^InvItemID^PesoBrutoTotal*',
                                   'PesoLiquitoUnitario',
                                   '^InvItemID^dso01GridLkp^InvItemID^PesoLiquitoTotal*',
                                   'InvItemID'],
                                   ['9999999999', '9999999999', '', '9999999999', '', '999999999.9999', '999999999.9999', '999999999.9999', '999999.9999', '999999.9999', '999999.9999', '999999.9999', ''],
                                   ['##########', '##########', '', '##########', '', '#,###,###,###,####0.0000', '#,###,###,###,####0.0000', '#,###,###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000',
                                        '###,###,####0.0000', '###,###,####0.0000', '']);

        // Linha de Totais
        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[7, '#,###,###,###,####0.0000', 'S'],
                                                              [9, '###,###,####0.0000', 'S'],
                                                              [11, '###,###,####0.0000', 'S']]);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;

    }
    else if (folderID == 28003) // Lancamentos contabeis
    {
        headerGrid(fg, ['Lan�amento',
                        'Est',
                        'Data',
                        'Balancete',
                        'Est',
                        'Tipo',
                        'Quant',
                        'Hist�rico Padr�o',
                        'Hist�rico Complementar',
                        'Detalhe',
                        'D',
                        'C',
                        'Moeda',
                        'Total Lan�amento',
                        'Taxa',
                        'Total Convertido',
                        'Observa��o',
                        'EventoID'], [17]);

        glb_aCelHint = [[0, 4, '� estorno?'],
						[0, 6, 'Quantidade de detalhes'],
						[0, 10, 'N�mero de D�bitos'],
						[0, 11, 'N�mero de Cr�ditos'],
						[0, 11, 'Taxa de  convers�o para moeda comum']];

        fillGridMask(fg, currDSO, ['LancamentoID',
                                 'Estado',
                                 'dtLancamento',
                                 'dtBalancete',
                                 'EhEstorno',
                                 'TipoLancamento',
                                 'Quantidade',
                                 'HistoricoPadrao',
                                 'HistoricoComplementar',
                                 'Detalhe',
                                 'NumeroDebitos',
                                 'NumeroCreditos',
                                 'Moeda',
                                 'ValorTotalLancamento',
                                 'TaxaMoeda',
                                 'ValorTotalConvertido',
                                 'Observacao',
                                 'EventoID'],
                                 ['9999999999', '', '99/99/9999', '99/99/9999', '', '', '999999', '', '', '', '99', '99', '',
									'999999999.99', '9999999999999.99', '999999999.99', '', ''],
								 ['##########', '', dTFormat, dTFormat, '', '', '######', '', '', '', '##', '##', '',
									'###,###,##0.00', '#,###,###,###,##0.0000000', '###,###,##0.00', '', '']);

        alignColsInGrid(fg, [0, 6, 10, 11, 13, 14, 15]);
        fg.FrozenCols = 2;
    }
}
