﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modcomercial.submodimportacao.Invoice.serverside
{
    public partial class EditarItens : System.Web.UI.OverflyPage
    {

        private Integer nInvItemID;
        public Integer InvItemID
        {
            get { return nInvItemID; }
            set { nInvItemID = value; }
        }

        private Integer nItem;
        public Integer Item
        {
            get { return nItem; }
            set { nItem = value; }
        }

        private Integer nProdutoID;
        public Integer ProdutoID
        {
            get { return nProdutoID; }
            set { nProdutoID = value; }
        }

        private string sProduto;
		public string Produto
		{
			get { return sProduto; }
			set { sProduto = value != null ? value : " "; }
		}

        private Integer nQuantidade;
        public Integer Quantidade
        {
            get { return nQuantidade; }
            set { nQuantidade = value; }
        }

        private java.lang.Double nValorFOBUnitario;
        protected java.lang.Double ValorFOBUnitario
        {
            get { return nValorFOBUnitario; }
            set { nValorFOBUnitario = value; }
        }

        private java.lang.Double nPesoBrutoUnitario;
        protected java.lang.Double PesoBrutoUnitario
        {
            get { return nPesoBrutoUnitario; }
            set { nPesoBrutoUnitario = value; }
        }

        private java.lang.Double nPesoLiquitoUnitario;
        protected java.lang.Double PesoLiquitoUnitario
        {
            get { return nPesoLiquitoUnitario; }
            set { nPesoLiquitoUnitario = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            int resultado = DataInterfaceObj.ExecuteSQLCommand("UPDATE Invoices_Itens SET Item = " + nItem.ToString() + ", ProdutoID = " + nProdutoID.ToString() + ", Produto = '" + sProduto.ToString() + "', " +
                                                                        "Quantidade = " + nQuantidade.ToString() + ", ValorFOBUnitario = " + nValorFOBUnitario.doubleValue() + ", " +
                                                                        "PesoBrutoUnitario = " + nPesoBrutoUnitario.doubleValue() + ", PesoLiquitoUnitario = " + PesoLiquitoUnitario.doubleValue() + " " +
                                                                    "WHERE InvItemID = " + nInvItemID.ToString());

            WriteResultXML(
                DataInterfaceObj.getRemoteData(" SELECT " + resultado + " AS Resultado")
            );

        }
    }
}