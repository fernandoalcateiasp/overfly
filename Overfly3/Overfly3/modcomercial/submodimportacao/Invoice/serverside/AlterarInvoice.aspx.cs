﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modcomercial.submodimportacao.Invoice.serverside
{
    public partial class AlterarInvoice : System.Web.UI.OverflyPage
    {

        private Integer nInvIteFabricanteID;

        public Integer InvIteFabricanteID
        {
            get { return nInvIteFabricanteID; }
            set { nInvIteFabricanteID = value; }
        }
        private Integer nQtdFabricante;

        public Integer Quantidade
        {
            get { return nQtdFabricante; }
            set { nQtdFabricante = value; }
        }
        private Integer nFabricanteID;

        public Integer FabricanteID
        {
            get { return nFabricanteID; }
            set { nFabricanteID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            int resultado = DataInterfaceObj.ExecuteSQLCommand("UPDATE Invoices_Itens_Fabricantes SET FabricanteID = " + nFabricanteID.ToString() + ", Quantidade = " + nQtdFabricante.ToString() +
                                                                " WHERE InvIteFabricanteID = " + nInvIteFabricanteID.ToString());

            WriteResultXML(
                DataInterfaceObj.getRemoteData(" SELECT " + resultado + " AS Resultado")
            );

        }
    }
}