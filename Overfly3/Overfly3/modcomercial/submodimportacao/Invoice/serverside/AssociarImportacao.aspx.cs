﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modcomercial.submodimportacao.Invoice.serverside
{
    public partial class AssociarImportacao : System.Web.UI.OverflyPage
    {

        private Integer nImportacaoID;

        public Integer ImportacaoID
        {
            get { return nImportacaoID; }
            set { nImportacaoID = value; }
        }
        private Integer nInvoiceID;

        public Integer InvoiceID
        {
            get { return nInvoiceID; }
            set { nInvoiceID = value; }
        }

        private Integer dissociar;

        public Integer nDissociar
        {
            get { return dissociar; }
            set { dissociar = value; }
        }


        protected override void PageLoad(object sender, EventArgs e)
        {
            string sql = "UPDATE Invoices SET ImportacaoID =  " + ((dissociar.intValue() == 1) ? "NULL" : nImportacaoID.ToString()) +
                            " WHERE InvoiceID = " + nInvoiceID.ToString();

            string sResultado = "";

            try
            {
                int resultado = DataInterfaceObj.ExecuteSQLCommand(sql);
            }
            catch (System.Exception ex)
            {
                sResultado = ex.Message;
            }

            WriteResultXML(
                DataInterfaceObj.getRemoteData(" SELECT '" + sResultado + "' AS Resultado")
            );

        }
    }
}