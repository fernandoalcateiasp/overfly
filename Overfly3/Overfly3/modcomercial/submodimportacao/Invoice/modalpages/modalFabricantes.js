/********************************************************************
modalFabricantes.js

Library javascript para o modalFabricantes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_InvoiceID = null;
var glb_aEmpresaData = getCurrEmpresaData();
var glb_nRow = 0;
var glb_nTotalRows = 0;

var dsoFabricantes = new CDatatransport("dsoFabricantes");
var dsoFabricante = new CDatatransport("dsoFabricante");
var dsoGravar = new CDatatransport("dsoGravar");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalFabricantesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
      
    // coloca foco no campo apropriado
    if (document.getElementById('txtPesquisa').disabled == false)
        txtPesquisa.focus();

    btnOK.disabled = true;
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Fabricantes', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));

    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 39;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = (parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP) + 40;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP) + 0;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = (parseInt(divFG.style.height, 10)) + 40;
    }

    // txtPesquisa
    txtPesquisa.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (txtPesquisa.style) {
        top = 40;
        left = 10;
        width = (txtPesquisa.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }

    with (lblPesquisa.style) {
        top = 25;
        left = 10;
        width = (txtPesquisa.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    with (btnFindPesquisa.style)
    {
        top = 40;
        left = 320;
        width = 80;
        height = 24;
    }

    with (btnGravar.style) {
        top = 40;
        left = 407;
        width = 80;
        height = 24;
    }

    if (nEstadoID == 1)
        btnGravar.disabled = false;
    else
        btnGravar.disabled = true;

    startGridInterface(fg);
    headerGrid(fg, ['InvIteFabricanteID',
                   'Item',
                   'ID',
                   'Produto',
                   'Quant Item',
                   'Quant Total',
                   'Valor FOB',
                   'Fabricante',
                   'Quant Fabricante',
                   'Pa�s'], [0]);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalitensBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

function js_fg_modalitensDblClick(grid, Row, Col) {
    ;
}

function js_modalitensKeyPress(KeyAscii) {
    ;
}

function js_modalitens_ValidateEdit() {
    ;
}

function js_modalitens_AfterEdit(Row, Col) {
    ;
}
// FINAL DE EVENTOS DE GRID *****************************************

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id)
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , new Array(
                      fg.TextMatrix(fg.Row, 0), fg.TextMatrix(fg.Row, 1)));
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
- null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar parametros e dados aqui
    glb_InvoiceID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'InvoiceID' + '\'' + '].value');

    // mostra a janela modal
    fillGridData('');

}

function btnFindPesquisa_onclick(ctl) {

    txtPesquisa.value = trimStr(txtPesquisa.value);

    fillGridData(txtPesquisa.value);

    btnFindPesquisa.disable = false;
}

function btnGravar_onclick(ctl)
{
    lockControlsInModalWin(true);

    var strPars = new String();
    var nInvIteFabricanteID;
    var nFabricanteID;
    var nQtdFabricante;

    if (glb_nRow == 0)
    {
        glb_nTotalRows = fg.Rows;
        glb_nRow = 1;
    }

    if (glb_nRow < glb_nTotalRows)
    {
        var strPars = '';

        nInvIteFabricanteID = fg.textMatrix(glb_nRow, getColIndexByColKey(fg, 'InvIteFabricanteID*'));
        nQtdFabricante = fg.textMatrix(glb_nRow, getColIndexByColKey(fg, 'QtdFabricante'));
        nFabricanteID = fg.textMatrix(glb_nRow, getColIndexByColKey(fg, 'FabricanteID'));

        strPars += '?InvIteFabricanteID=' + escape(nInvIteFabricanteID);
        strPars += '&Quantidade=' + escape(nQtdFabricante);
        strPars += '&FabricanteID=' + escape(nFabricanteID);

        sendDataToServer(strPars);
    }
    else
    {
        glb_nTotalRows = 0;
        glb_nRow = 0;

        fillGridData(txtPesquisa.value);
    }
}

function fillGridData(strPesquisa)
{
    lockControlsInModalWin(true);
    nInvoiceID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'InvoiceID' + '\'' + '].value');
    var sSQL = '';

    sSQL = 'SELECT b.InvIteFabricanteID, a.Item, a.ProdutoID, dbo.fn_Produto_Descricao2(a.ProdutoID, 246, 15) [Produto], a.Quantidade [QtdItem], ' +
                '(SELECT SUM(bb.Quantidade) ' +
                    'FROM Invoices_Itens_Fabricantes bb WITH(NOLOCK) ' +
                    'WHERE bb.InvItemID = a.InvItemID)  [QtdTotal], a.ValorFOBUnitario, b.FabricanteID, b.Quantidade [QtdFabricante], e.Localidade [Pais] ' +
                'FROM Invoices_Itens a WITH(NOLOCK) ' +
                    'INNER JOIN Invoices_Itens_Fabricantes b WITH(NOLOCK) ON (b.InvItemID = a.InvItemID) ' +
                    'INNER JOIN Pessoas_Enderecos d WITH(NOLOCK) ON (d.PessoaID = b.FabricanteID) ' +
                    'INNER JOIN Localidades e WITH(NOLOCK) ON (e.LocalidadeID = d.PaisID) ' +
                'WHERE a.InvoiceID = ' + nInvoiceID + (strPesquisa == '' ? '' : 'AND ((CONVERT(VARCHAR(10), a.ProdutoID) + SPACE(0) + a.Produto) LIKE ' + '\'' + '%' + strPesquisa + '%' + '\'' + ') ');

    dsoFabricantes.SQL = sSQL;
    dsoFabricantes.ondatasetcomplete = fillGridData_DSC;
    dsoFabricantes.refresh();

    setConnection(dsoFabricante);
    dsoFabricante.SQL = 'SELECT DISTINCT a.PessoaID AS FabricanteID, a.Fantasia AS Fabricante ' +
                            'FROM Pessoas a WITH(NOLOCK) ' +
                            'INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (b.SujeitoID = a.PessoaID) ' + 
                        'WHERE ((a.ClassificacaoID = 59) AND (a.EstadoID = 2) AND (b.ObjetoID = ' + glb_aEmpresaData[0] + ') AND (dbo.fn_Pessoa_Localidade(a.PessoaID, 1, NULL, NULL) <> 130)) ' +
                        'ORDER BY a.Fantasia ASC ';

    dsoFabricante.ondatasetcomplete = fillGridData_DSC;
    dsoFabricante.Refresh();
    
}

function fillGridData_DSC()
{
    startGridInterface(fg);
    fg.FrozenCols = 0;

    headerGrid(fg, ['InvIteFabricanteID',
                   'Item',
                   'ID',
                    'Produto',
                    'Quant',
                    'Total',
                    'Valor FOB',
                    'Fabricante',
                    'Quant Fabricante',
                    'Pa�s'], [0]);

    fillGridMask(fg, dsoFabricantes, ['InvIteFabricanteID*', 
                                'Item*',
                                'ProdutoID*',
                                'Produto*',
                                'QtdItem*',
                                'QtdTotal*',
                                'ValorFOBUnitario*',
                                'FabricanteID',
                                'QtdFabricante',
                                'Pais*'],
                                ['', '', '', '', '', '', '', '', '', ''],
                                ['', '', '', '', '', '', '', '', '', '', '']);

    insertcomboData(fg, getColIndexByColKey(fg, 'FabricanteID'), dsoFabricante, '*Fabricante|FabricanteID', 'FabricanteID');


    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.MergeCells = 3;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;
    fg.MergeCol(5) = true;
    fg.MergeCol(6) = true;

    lockControlsInModalWin(false);
    fg.Editable = true;
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
    }    
    else
        btnOK.disabled = true;

    fg.Redraw = 2;

    pintaGrid();

}

function sendDataToServer(strPars) {
    try
    {
        dsoGravar.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/Invoice/serverside/AlterarInvoice.aspx' + strPars;
        dsoGravar.ondatasetcomplete = sendDataToServer_DSC;
        dsoGravar.refresh();
    }
    catch (e)
    {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
    }
}

function sendDataToServer_DSC()
{
    if (glb_nRow > 0)
    {
        glb_nRow++;
        btnGravar_onclick();
    }

}

function pintaGrid() {
    var bgColorRed = 0X0000FF;

    lockInterface(true);
    for (i = 1; i < fg.Rows; i++) {
        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'QtdItem*')) != fg.TextMatrix(i, getColIndexByColKey(fg, 'QtdTotal*')))
            //Pinta cor da fonte
            fg.Cell(7, i, getColIndexByColKey(fg, 'QtdTotal*'), i, getColIndexByColKey(fg, 'QtdTotal*')) = bgColorRed;
    }
    lockInterface(false);
}