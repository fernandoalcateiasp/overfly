/********************************************************************
modalEditarItens.js

Library javascript para o modalEditarItens.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_aEmpresaData = getCurrEmpresaData();
var glb_nRow = 0;
var glb_nTotalRows = 0;
var glb_sReadOnly = '';


var dsoItens = new CDatatransport("dsoItens");
var dsoGravar = new CDatatransport("dsoGravar");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalEditarItensBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);

    // coloca foco no campo apropriado
    if (document.getElementById('txtPesquisa').disabled == false)
        txtPesquisa.focus();

    btnOK.disabled = true;
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Editar Itens', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));

    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 39;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = (parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP) + 40;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP) + 0;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = (parseInt(divFG.style.height, 10)) + 40;
    }

    // txtPesquisa
    txtPesquisa.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (txtPesquisa.style) {
        top = 40;
        left = 10;
        width = (txtPesquisa.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }

    with (lblPesquisa.style) {
        top = 25;
        left = 10;
        width = (txtPesquisa.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    with (btnFindPesquisa.style) {
        top = 40;
        left = 320;
        width = 80;
        height = 24;
    }

    with (btnGravar.style) {
        top = 40;
        left = 407;
        width = 80;
        height = 24;
    }

    startGridInterface(fg);
    headerGrid(fg, ['InvItemID',
                   'Item',
                   'ID',
                    'Produto',
                    'Quant',
                    'Valor FOB', 
                    'Peso Bruto Unitario', 
                    'Peso Liquido Unitario'], [0]);

    fg.Redraw = 2;
    
    lblPesquisa.innerText = 'Pesquisa';
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalitensBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

function js_fg_modalitensDblClick(grid, Row, Col) {
    ;
}

function js_modalitensKeyPress(KeyAscii) {
    ;
}

function js_modalitens_ValidateEdit() {
    ;
}

function js_modalitens_AfterEdit(Row, Col)
{   
    if ((Col == getColIndexByColKey(fg, 'ValorFOBUnitario')) || (Col == getColIndexByColKey(fg, 'PesoBrutoUnitario')) || (Col == getColIndexByColKey(fg, 'PesoLiquitoUnitario')))
        fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
}
// FINAL DE EVENTOS DE GRID *****************************************

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', new Array(
                      fg.TextMatrix(fg.Row, 0), fg.TextMatrix(fg.Row, 1)));
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);

}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
- null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {

    // mostra a janela modal
    fillGridData('');

}

function btnFindPesquisa_onclick(ctl) {

    txtPesquisa.value = trimStr(txtPesquisa.value);

    fillGridData(txtPesquisa.value);

    btnFindPesquisa.disable = false;
}

function btnGravar_onclick(ctl) {
    lockControlsInModalWin(true);

    var strPars = new String();
    var nInvItemID;
    var nItem;
    var nProdutoID;
    var sProduto;
    var nQuantidade;
    var nValorFOBUnitario;
    var nPesoBrutoUnitario;
    var nPesoLiquitoUnitario;

    if (glb_nRow == 0) {
        glb_nTotalRows = fg.Rows;
        glb_nRow = 1;
    }

    if (glb_nRow < glb_nTotalRows) {
        var strPars = '';

        nInvItemID = fg.textMatrix(glb_nRow, getColIndexByColKey(fg, 'InvItemID*'));
        nItem = fg.textMatrix(glb_nRow, getColIndexByColKey(fg, 'Item'));
        nProdutoID = fg.textMatrix(glb_nRow, getColIndexByColKey(fg, 'ProdutoID'));
        sProduto = fg.textMatrix(glb_nRow, getColIndexByColKey(fg, 'Produto' + glb_sReadOnly));
        nQuantidade = fg.textMatrix(glb_nRow, getColIndexByColKey(fg, 'Quantidade'));
        nValorFOBUnitario = transformStringInNumeric(fg.textMatrix(glb_nRow, getColIndexByColKey(fg, 'ValorFOBUnitario')));
        nPesoBrutoUnitario = transformStringInNumeric(fg.textMatrix(glb_nRow, getColIndexByColKey(fg, 'PesoBrutoUnitario')));
        nPesoLiquitoUnitario = transformStringInNumeric(fg.textMatrix(glb_nRow, getColIndexByColKey(fg, 'PesoLiquitoUnitario')));


        strPars += '?InvItemID=' + escape(nInvItemID);
        strPars += '&Item=' + escape(nItem);
        strPars += '&ProdutoID=' + escape(nProdutoID);
        strPars += '&Produto=' + escape(sProduto);
        strPars += '&Quantidade=' + escape(nQuantidade);
        strPars += '&ValorFOBUnitario=' + nValorFOBUnitario;
        strPars += '&PesoBrutoUnitario=' + nPesoBrutoUnitario;
        strPars += '&PesoLiquitoUnitario=' + nPesoLiquitoUnitario;

        sendDataToServer(strPars);
    }
    else {
        glb_nTotalRows = 0;
        glb_nRow = 0;

        fillGridData(txtPesquisa.value);
    }
}

function fillGridData(strPesquisa)
{
    lockControlsInModalWin(true);

    var sSQL = '';
    var nInvoiceID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'InvoiceID' + '\'' + '].value');

    sSQL = 'SELECT a.* ' +
                'FROM Invoices_Itens a WITH(NOLOCK) ' +
                'WHERE a.InvoiceID = ' + nInvoiceID + (strPesquisa == '' ? '' : 'AND ((CONVERT(VARCHAR(10), a.ProdutoID) + SPACE(0) + a.Produto) LIKE ' + '\'' + '%' + strPesquisa + '%' + '\'' + ') ');

    dsoItens.SQL = sSQL;
    dsoItens.ondatasetcomplete = fillGridData_DSC;
    dsoItens.refresh();

}

function fillGridData_DSC()
{
    startGridInterface(fg);
    fg.FrozenCols = 0;


    if (!((dsoItens.recordset.BOF) && (dsoItens.recordset.EOF)))
    {
        if (dsoItens.recordset['Produto'].value != null)
            glb_sReadOnly = '*';
    }


    headerGrid(fg, ['InvItemID',
                    'Item',
                    'ID',
                    'Produto',
                    'Quant',
                    'Valor FOB',
                    'Peso Bruto Unitario',
                    'Peso Liquito Unitario'], [0]);

    fillGridMask(fg, dsoItens, ['InvItemID*', 
                                'Item',
                                'ProdutoID',
                                'Produto' + glb_sReadOnly,
                                'Quantidade',
                                'ValorFOBUnitario',
                                'PesoBrutoUnitario',
                                'PesoLiquitoUnitario'],
                                ['', '', '', '', '', '999999999.9999', '999999.9999', '999999.9999'],
                                ['', '', '', '', '', '###,###,####0.0000', '###,###,####0.0000', '###,###,####0.0000']);

    lockControlsInModalWin(false);
    fg.Editable = true;

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
    }    
    else
        btnOK.disabled = true;

    fg.Redraw = 2;
}

function sendDataToServer(strPars) {
    try {
        dsoGravar.URL = SYS_ASPURLROOT + '/modcomercial/submodimportacao/Invoice/serverside/EditarItens.aspx' + strPars;
        dsoGravar.ondatasetcomplete = sendDataToServer_DSC;
        dsoGravar.refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
    }
}

function sendDataToServer_DSC() {
    if (glb_nRow > 0) {
        glb_nRow++;
        btnGravar_onclick();
    }

}