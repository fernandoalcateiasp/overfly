/********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true) 
    {
        sSQL = 'SELECT TOP 1 *, ' +
               'CONVERT(VARCHAR, dtData, ' + DATE_SQL_PARAM + ') as V_dtData, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID = AlternativoID )>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM    dbo.RegrasFiscais  WITH(NOLOCK)' +
               'WHERE ( ProprietarioID = ' + nID + ')  ';

        if (window.top.registroID_Type == 16)
            sSQL += 'AND Nivel = ' + (txtNivel.value == '' ? '0' : txtNivel.value) + ' ';

        sSQL += 'ORDER BY RegraFiscalID DESC';               
    }
    else
        sSQL = 'SELECT *, ' +
               'CONVERT(VARCHAR, dtData, ' + DATE_SQL_PARAM + ') as V_dtData, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID = AlternativoID )>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM    dbo.RegrasFiscais  WITH(NOLOCK)' +
               'WHERE ( RegraFiscalID = ' + nID + ')  ORDER BY RegraFiscalID DESC';

    setConnection(dso);

    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();

    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;

    sql = 'SELECT *, ' +
          'CONVERT(VARCHAR, dtData, ' + DATE_SQL_PARAM + ') as V_dtData, ' +
          '0 AS Prop1, 0 AS Prop2 ' +
          'FROM RegrasFiscais WITH(NOLOCK) WHERE RegraFiscalID = 0';
    
    dso.SQL = sql;          
}              

