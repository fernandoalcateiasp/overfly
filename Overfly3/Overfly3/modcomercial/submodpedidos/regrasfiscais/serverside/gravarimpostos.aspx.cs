using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.regrasfiscais.serverside
{
	public partial class gravarimpostos : System.Web.UI.OverflyPage
	{
        protected static Integer Zero = new Integer(0);
        private static int Resultado = Zero.intValue();

        private static Integer[] RegImpostoID;
        private static Integer[] RegVigenciaInicioID;
        private static Integer[] RegVigenciaFimID;
        private static Integer[] RegraFiscalID;
        private string[] DataVigFim;

        private static Integer ImpostoID;
        private static Integer CodigoTributacaoID;
        private static string[] BaseCalculo;
        private static string[] Aliquota;
        private static string[] IVAST;
        private static string[] BCICMS;
        private static string[] Observacao;
        private static string VigInicio;
        private static string VigFim;

        private static Integer[] TipoID;

        public string Mensagem;

        public Integer[] nRegImpostoID
        {
            set
            {
                RegImpostoID = value;

                for (int i = 0; i < RegImpostoID.Length; i++)
                {
                    if (RegImpostoID[i] == null)
                        RegImpostoID[i] = Zero;
                }
            }
        }

        public Integer[] nRegVigenciaInicioID
        {
            set
            {
                RegVigenciaInicioID = value;

                for (int i = 0; i < RegVigenciaInicioID.Length; i++)
                {
                    if (RegVigenciaInicioID[i] == null)
                        RegVigenciaInicioID[i] = Zero;
                }
            }
        }

        public Integer[] nRegVigenciaFimID
        {
            set
            {
                RegVigenciaFimID = value;

                for (int i = 0; i < RegVigenciaFimID.Length; i++)
                {
                    if (RegVigenciaFimID[i] == null)
                        RegVigenciaFimID[i] = Zero;
                }
            }
        }

        public Integer[] nRegraFiscalID 
        {
            set
            {
                RegraFiscalID = value;

                for (int i = 0; i < RegraFiscalID.Length; i++)
                {
                    if (RegraFiscalID[i] == null)
                        RegraFiscalID[i] = Zero;
                }
            }
		}

        public string[] dDataVigFim
        {
            set
            {
                DataVigFim = value;

                for (int i = 0; i < DataVigFim.Length; i++)
                {
                    if (DataVigFim[i] == null || DataVigFim[i] == "")
                        DataVigFim[i] = "0";
                }
            }
		}

        public string[] nBaseCalculo
        {
            set
            {
                BaseCalculo = value;

                for (int i = 0; i < BaseCalculo.Length; i++)
                {
                    if (BaseCalculo[i] == null || BaseCalculo[i] == "")
                        BaseCalculo[i] = "NULL";
                }
            }
        }

        public string[] nAliquota
        {
            set
            {
                Aliquota = value;

                for (int i = 0; i < Aliquota.Length; i++)
                {
                    if (Aliquota[i] == null || Aliquota[i] == "")
                        Aliquota[i] = "NULL";
                }
            }
        }

        public string[] nIVAST
        {
            set
            {
                IVAST = value;

                for (int i = 0; i < IVAST.Length; i++)
                {
                    if (IVAST[i] == null || IVAST[i] == "")
                        IVAST[i] = "NULL";
                }
            }
        }

        public string[] nBCICMS
        {
            set
            {
                BCICMS = value;

                for (int i = 0; i < BCICMS.Length; i++)
                {
                    if (BCICMS[i] == null || BCICMS[i] == "")
                        BCICMS[i] = "NULL";
                }
            }
        }

        public string[] sObservacao
        {
            set
            {
                Observacao = value;

                for (int i = 0; i < Observacao.Length; i++)
                {
                    if (Observacao[i] == null || Observacao[i] == "")
                        Observacao[i] = string.Empty;
                }
            }
        }

        public string sVigInicio
        {
            set
            {
                VigInicio = (value != null ? value : string.Empty);
            }
        }

        public string sVigFim
        {
            set
            {
                VigFim = (value != null ? value : string.Empty);
            }
        }

        public Integer nImpostoID 
        {
            set
            {
                ImpostoID = (value != null ? value : Zero);
            }
        }

        public Integer nCodigoTributacaoID
        {
            set
            {
                CodigoTributacaoID  = (value != null ? value : Zero);
            }
        }

        public Integer[] nTipoID
        {
            set
            {
                TipoID = value;

                for (int i = 0; i < TipoID.Length; i++)
                {
                    if (TipoID[i] == null )
                        TipoID[i] = Zero;
                }
            }
        }

        private string Insercao(int i)
        {
            string sObs = (Observacao[i] != string.Empty) ? Observacao[i].Replace('^', ',') : string.Empty;
            
            ProcedureParameters[] param = new ProcedureParameters[] {
				new ProcedureParameters("@RegraFiscalID", SqlDbType.Int, RegraFiscalID[i]),
				new ProcedureParameters("@ImpostoID", SqlDbType.Int, ImpostoID),
                new ProcedureParameters("@CodigoTributacaoID", SqlDbType.Int, CodigoTributacaoID),
				new ProcedureParameters("@BaseCalculo", SqlDbType.Money, ((BaseCalculo[i] != "NULL") ? (Object)Convert.ToDouble(BaseCalculo[i]) : DBNull.Value)),
                new ProcedureParameters("@Aliquota", SqlDbType.Money, ((Aliquota[i] != "NULL") ? (Object)Convert.ToDouble(Aliquota[i]) : DBNull.Value)),
                new ProcedureParameters("@IVAST", SqlDbType.Money, ((IVAST[i] != "NULL") ? (Object)Convert.ToDouble(IVAST[i]) : DBNull.Value)),
                new ProcedureParameters("@BCICMS", SqlDbType.Money, ((BCICMS[i] != "NULL") ? (Object)Convert.ToDouble(BCICMS[i]) : DBNull.Value)),
                new ProcedureParameters("@Observacao", SqlDbType.VarChar, ((sObs == string.Empty) ? DBNull.Value : (Object)sObs)),
                new ProcedureParameters("@VigInicio", SqlDbType.DateTime, VigInicio),
                new ProcedureParameters("@VigFim", SqlDbType.DateTime, ((VigFim == string.Empty) ? DBNull.Value : (Object)VigFim)),
				new ProcedureParameters("@Resultado", SqlDbType.Int, DBNull.Value, ParameterDirection.InputOutput),
                new ProcedureParameters("@Mensagem", SqlDbType.VarChar, DBNull.Value, ParameterDirection.Output, 200)};

            DataInterfaceObj.execNonQueryProcedure("sp_RegrasFiscais_InsereImposto", param);

            if (param[10].Data != DBNull.Value)
                Resultado = int.Parse(param[10].Data.ToString());

            Mensagem = param[11].Data.ToString();

            return Mensagem; 
        }
        private void Gravacao(int i, bool insert) 
        {
            string sql = string.Empty;

            string sObs = (Observacao[i] != string.Empty) ? Observacao[i].Replace('^', ',') : string.Empty;
            
            sql = "UPDATE RegrasFiscais_Vigencias SET dtVigencia = '" + DataVigFim[i] + "' " +
                      "WHERE RegVigenciaID= " + RegVigenciaFimID[i] + " " +
                  "UPDATE RegrasFiscais_Impostos SET BaseCalculo = " + BaseCalculo[i] + ", Aliquota = " + Aliquota[i] + ", " +
                        "IVAST = " + IVAST[i] + ", BaseCalculoICMS = " + BCICMS[i] + ", Observacao = '" + sObs + "' " +
                      "WHERE RegImpostoID = " + RegImpostoID[i];

            if ((insert) && (DataVigFim[i] != "0"))
                sql += " INSERT INTO RegrasFiscais_Vigencias (RegraFiscalID, RegImpostoID, dtVigencia, Vigente) " +
                          "SELECT " + RegraFiscalID[i] + ", " + RegImpostoID[i] + ", '" + DataVigFim[i] + "', 0";

            DataInterfaceObj.ExecuteSQLCommand(sql);
        }
        private void Delecao(int i)
        {
            string sql;

            sql = "DELETE FROM RegrasFiscais_Vigencias WHERE RegImpostoID = " + RegImpostoID[i];
            DataInterfaceObj.ExecuteSQLCommand(sql);
            
            sql = "DELETE FROM dbo.RegrasFiscais_Impostos WHERE RegImpostoID = " + RegImpostoID[i];
            DataInterfaceObj.ExecuteSQLCommand(sql);
        }

        protected override void PageLoad(object sender, EventArgs e)
		{
            for (int i = 0; i < TipoID.Length; i++)
            {

                if (TipoID[i].intValue() == 1)
                    Gravacao(i, false);
                else if (TipoID[i].intValue() == 2)
                    Insercao(i);
                else if (TipoID[i].intValue() == 3)
                    Delecao(i);
                else if (TipoID[i].intValue() == 4)
                    Gravacao(i, true);
            }

            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT " + ((Mensagem == null || Mensagem.Length == 0) ? "NULL" :
                  "'" + Mensagem + "'") + " AS Mensagem"));
        }
	}
}
