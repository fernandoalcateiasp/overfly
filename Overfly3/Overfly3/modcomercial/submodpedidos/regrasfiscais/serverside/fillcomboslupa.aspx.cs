using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.regrasfiscais.serverside
{
	public partial class fillcomboslupa : System.Web.UI.OverflyPage
	{
		private static string Empty = "";
		
		private static string param1 = "";
		private static string param2 = "";
		private static string text = "";
		private static string dateSqlParam = "103";

		public string sParam1 {
			set { param1 = value != null ? value : Empty; }
		}

		public string sParam2 {
			set { param2 = value != null ? value : Empty; }
		}

		public string sText {
			set { 
				text = value != null ? value : Empty; 
				
				try {
					int.Parse(text);
				} catch(System.Exception) {
					text = "0";
				}
			}
		}

		public string DATE_SQL_PARAM {
			set { dateSqlParam = value != null ? value : "103"; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"SELECT TOP 100 " +
					"a.NotaFiscalID as fldID, " +
					"a.NotaFiscal as fldName, " +
					"CONVERT(VARCHAR, a.dtNotaFiscal, " + dateSqlParam + ") as V_dtNotaFiscal, " +
					"a.PedidoID, " +
					"c.PessoaID, " + 
					"c.Fantasia as Pessoa " +
				"FROM " +
					"NotasFiscais a WITH(NOLOCK) " +
					"INNER JOIN NotasFiscais_Pessoas b WITH(NOLOCK) ON (b.NotaFiscalID = a.NotaFiscalID) " + 
						"AND (((a.Emissor = 1) AND (b.TipoID = 791)) OR ((a.Emissor = 0) AND (b.TipoID = 790))) " +
					"INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = b.PessoaID) " +
				"WHERE " + 
					"a.EmpresaID = " + param2 + " AND " +
					"a.NotaFiscal >= " + text + " " +
				"ORDER BY " +
					"a.NotaFiscal"
			));
		}
	}
}
