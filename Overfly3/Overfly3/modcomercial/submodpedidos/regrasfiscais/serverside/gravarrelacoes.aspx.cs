using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.regrasfiscais.serverside
{
    public partial class gravarrelacoes : System.Web.UI.OverflyPage
	{
        protected static Integer Zero = new Integer(0);
        private static int Resultado = Zero.intValue();

        private static Integer[] ElementoID;
        private static Integer[] Grupo;
        private static Integer[] RegraFiscalID;
        private static Integer[] RegElementoID;
        private static Integer[] RegRelacaoID;
        private static Integer[] MensagemID;
        private static string[] Observacao;
        private static Integer[] Nivel;
        

        private static Integer RegImpostoID;
        private static Integer RegraFiscalImpostoID;
        private static Integer RegraFiscalCorrenteID;
        private static Integer NivelImposto;
        private static Integer NivelCorrente;

        private static Integer[] TipoID;

        public Integer[] nElementoID
        {
            set
            {
                ElementoID = value;

                for (int i = 0; i < ElementoID.Length; i++)
                {
                    if (ElementoID[i] == null)
                        ElementoID[i] = Zero;
                }
            }
        }

        public Integer[] sGrupo
        {
            set
            {
                Grupo = value;

                for (int i = 0; i < Grupo.Length; i++)
                {
                    if (Grupo[i] == null)
                        Grupo[i] = Zero;
                }
            }
        }

        public Integer[] nRegraFiscalID
        {
            set
            {
                RegraFiscalID = value;

                for (int i = 0; i < RegraFiscalID.Length; i++)
                {
                    if (RegraFiscalID[i] == null)
                        RegraFiscalID[i] = Zero;
                }
            }
        }

        public Integer[] nRegElementoID
        {
            set
            {
                RegElementoID = value;

                for (int i = 0; i < RegElementoID.Length; i++)
                {
                    if (RegElementoID[i] == null)
                        RegElementoID[i] = Zero;
                }
            }
        }

        public Integer[] nRegRelacaoID
        {
            set
            {
                RegRelacaoID = value;

                for (int i = 0; i < RegRelacaoID.Length; i++)
                {
                    if (RegRelacaoID[i] == null)
                        RegRelacaoID[i] = Zero;
                }
            }
        }
        
        public Integer[] nMensagemID
        {
            set
            {
                MensagemID = value;

                for (int i = 0; i < MensagemID.Length; i++)
                {
                    if (MensagemID[i] == null)
                        MensagemID[i] = Zero;
                }
            }
        }

        public string[] sObservacao
        {
            set
            {
                Observacao = value;

                for (int i = 0; i < Observacao.Length; i++)
                {
                    if (Observacao[i] == null || Observacao[i] == "")
                        Observacao[i] = string.Empty;
                }
            }
        }

        public Integer[] nNivel
        {
            set
            {
                Nivel = value;

                for (int i = 0; i < Nivel.Length; i++)
                {
                    if (Nivel[i] == null)
                        Nivel[i] = Zero;
                }
            }
        }

        public Integer nRegImpostoID
        {
            set
            {
                RegImpostoID = (value != null ? value : Zero);
            }
        }

        public Integer nRegraFiscalImpostoID
        {
            set
            {
                RegraFiscalImpostoID = (value != null ? value : Zero);
            }
        }

        public Integer nRegraFiscalCorrenteID
        {
            set
            {
                RegraFiscalCorrenteID = (value != null ? value : Zero);
            }
        }

        public Integer nNivelImposto
        {
            set
            {
                NivelImposto = (value != null ? value : Zero);
            }
        }

        public Integer nNivelCorrente
        {
            set
            {
                NivelCorrente = (value != null ? value : Zero);
            }
        }

        public Integer[] nTipoID
        {
            set
            {
                TipoID = value;

                for (int i = 0; i < TipoID.Length; i++)
                {
                    if (TipoID[i] == null )
                        TipoID[i] = Zero;
                }
            }
        }

        private void Inserir(int i)
        {
            string sql = string.Empty;

            sql = "UPDATE dbo.RegrasFiscais_Elementos SET Grupo = " + Grupo[0] + " " +
                      "WHERE RegElementoID = " + RegElementoID[i];

            DataInterfaceObj.ExecuteSQLCommand(sql);
        }
        
        private void Gravar(int i)
        {
            string sql = string.Empty;
            string sMensagem = "NULL";

            string sObs = (Observacao[i] != string.Empty) ? Observacao[i].Replace('^', ',') : string.Empty;

            if (MensagemID[i].intValue() > 0)
            {
                sMensagem = MensagemID[i].ToString();
            }

            sql = "UPDATE dbo.RegrasFiscais_Relacoes SET Observacao = '" + sObs + "', RegMensagemID = " + sMensagem + " " +
                      "WHERE RegRelacaoID= " + RegRelacaoID[i];

            DataInterfaceObj.ExecuteSQLCommand(sql);
        }

        private void Deletar(int i)
        {
            string sql;

            sql = "DELETE FROM dbo.RegrasFiscais_Relacoes WHERE RegRelacaoID = " + RegRelacaoID[i];
            DataInterfaceObj.ExecuteSQLCommand(sql);
        }

        private string Relacionar(int i)
        {
            string sResultado = string.Empty;
            string sql = string.Empty;
            string sGrupo = string.Empty;
            int rowsAffected = 0;

            if (RegraFiscalID[i] != RegraFiscalCorrenteID)
            {
                if ((Nivel[i].intValue() == 0) && (RegraFiscalID[i].intValue() != RegraFiscalImpostoID.intValue()))
                    sResultado = "A Regra Fiscal " + RegraFiscalID[i] + " s� pode ser relacionada a um Imposto dela \n";
                else if ((Nivel[i].intValue() < 2) && (NivelImposto.intValue() == 2))
                    sResultado = "A Regra Fiscal " + RegraFiscalID[i] + " s� pode ser relacionada a um Imposto de uma Regra com o nivel menor ou igual ao dela \n";
            }

            if (Grupo[i] == Zero)
            {
                if (i == 0)
                    sGrupo = "(SELECT ISNULL(MAX(aa.Grupo), 0) + 1 FROM RegrasFiscais_Elementos aa WITH(NOLOCK) WHERE (aa.RegraFiscalID = " + RegraFiscalID[i] + ") " +
                                "AND (aa.ElementoID = " + ElementoID[i] + "))";
                else
                    sGrupo = "(SELECT MAX(aa.Grupo) FROM RegrasFiscais_Elementos aa WITH(NOLOCK) WHERE (aa.RegraFiscalID = " + RegraFiscalID[i] + ") " +
                        "AND (aa.ElementoID = " + ElementoID[i] + "))";

                sql = "UPDATE dbo.RegrasFiscais_Elementos SET Grupo = " + sGrupo + " " +
                        "WHERE RegElementoID = " + RegElementoID[i];

                rowsAffected = DataInterfaceObj.ExecuteSQLCommand(sql);

                if (rowsAffected == 0)
                    sResultado = "Erro ao atribuir grupo";
                else
                    sGrupo = "(SELECT MAX(aa.Grupo) FROM RegrasFiscais_Elementos aa WITH(NOLOCK) WHERE (aa.RegraFiscalID = " + RegraFiscalID[i] + ") " +
                        "AND (aa.ElementoID = " + ElementoID[i] + "))";
            }
            else
            {
                sGrupo = Grupo[i].ToString();
            }

            if (sResultado == string.Empty)
            {
                rowsAffected = 0;
                sql = string.Empty;

                if (((i == 0)) || ((i > 0) && ((!ElementoID[i].Equals(ElementoID[i - 1])) || (!Grupo[i].Equals(Grupo[i - 1])))))
                {
                    sql = "INSERT INTO dbo.RegrasFiscais_Relacoes (RegraFiscalID, ElementoID, Grupo, RegImpostoID) " +
                            "SELECT " + RegraFiscalID[i] + ", " + ElementoID[i] + ", " + sGrupo + ", " + RegImpostoID +
                            "WHERE NOT EXISTS(SELECT aa.RegRelacaoID FROM RegrasFiscais_Relacoes aa WITH(NOLOCK)" +
                            " INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) bb ON (aa.RegraFiscalID = bb.RegraFiscalID) " +
                            " INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(" + RegraFiscalID[i] + ") cc ON (cc.AgrupadorID = bb.AgrupadorID) " +
                            " WHERE (aa.ElementoID = " + ElementoID[i] + ") AND (aa.Grupo = " + sGrupo + ") AND  (aa.RegImpostoID =  " + RegImpostoID + " ))";

                    rowsAffected = DataInterfaceObj.ExecuteSQLCommand(sql);

                    if (rowsAffected == 0)
                        sResultado = "Este Elemento/Grupo j� tem rela��o com este Imposto";
                }
            }
             return sResultado;
        }

        protected override void PageLoad(object sender, EventArgs e)
		{
            string sResultado = string.Empty;

            for (int i = 0; i < TipoID.Length; i++)
            {
                if (TipoID[i].intValue() == 1)
                    sResultado += Relacionar(i);
                else if (TipoID[i].intValue() == 2)
                    Inserir(i);
                else if (TipoID[i].intValue() == 3)
                    Gravar(i);
                else if (TipoID[i].intValue() == 4)
                    Deletar(i);
            }

            if (sResultado == string.Empty)
                sResultado = "SPACE(0)";
            else
                sResultado = "'" + sResultado + "'";

            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT " + sResultado + " AS [Error]"));
        }
	}
}
