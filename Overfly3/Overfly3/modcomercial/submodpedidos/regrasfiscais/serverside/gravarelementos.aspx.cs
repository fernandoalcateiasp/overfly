using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.regrasfiscais.serverside
{
    public partial class gravarelementos : System.Web.UI.OverflyPage
	{
        protected static Integer Zero = new Integer(0);
        private static int Resultado = Zero.intValue();

        private static string[] UFOrigem;
        private static string[] UFDestino;
        private static string[] ProdutoID;
        private static string[] PessoaID;
        private static Integer[] RegraFiscalID; 
        private static Integer[] ElementoID;
        private static Integer[] Grupo;
        private static Integer[] CFOPID;
        private static Integer[] Finalidade;
        private static string[] DataVigFim;
        private static Integer[] RegVigenciaFimID;
        private static Integer[] RegVigenciaInicioID;
        private static string[] NCM;
        private static string[] CNAE;
        private static string[] VigInicio;
        private static string[] VigFim;
        private static string[] Observacao;
        private string[] Excecao;
        private string[] PermiteMesmaUF;
        private static Integer[] RegElementoID;
        private static Integer[] TipoID;

        public string mensagem;
        public Integer[] nRegraFiscalID
        {
          set
            {
                RegraFiscalID = value;

                for (int i = 0; i < RegraFiscalID.Length; i++)
                {
                    if (RegraFiscalID[i] == null)
                        RegraFiscalID[i] = Zero;
                }
            }
        }
        
        public Integer[] nElementoID
        {
            set
            {
                ElementoID = value;

                for (int i = 0; i < ElementoID.Length; i++)
                {
                    if (ElementoID[i] == null)
                        ElementoID[i] = Zero;
                }
            }
        }

        public Integer[] nCFOPID
        {
            set
            {
                CFOPID = value;

                for (int i = 0; i < CFOPID.Length; i++)
                {
                    if (CFOPID[i] == null)
                        CFOPID[i] = Zero;
                }
            }
        }
  
        public Integer[] nFinalidade
        {
            set
            {
                Finalidade = value;

                for (int i = 0; i < Finalidade.Length; i++)
                {
                    if (Finalidade[i] == null)
                        Finalidade[i] = Zero;
                }
            }
        }

        public string[] sUFOrigem
        {
            set
            {
                UFOrigem = value;

                for (int i = 0; i < UFOrigem.Length; i++)
                {
                    if ((UFOrigem[i] == null) || (UFOrigem[i] == ""))
                        UFOrigem[i] = string.Empty;
                }
            }
        }

        public string[] sUFDestino
        {
            set 
            {
                UFDestino = value;

                for (int i = 0; i < UFDestino.Length; i++)
                {
                    if ((UFDestino[i] == null) || (UFDestino[i] == ""))
                        UFDestino[i] = string.Empty;
                }
            }
        }

        public string[] sNCM
        {
            set
            {
                NCM = value;

                for (int i = 0; i < NCM.Length; i++)
                {
                    if (NCM[i] == null)
                        NCM[i] = string.Empty;
                }
            }
        }
        
        public string[] sCNAE
        {
            set
            {
                CNAE = value;

                for (int i = 0; i < CNAE.Length; i++)
                {
                    if (CNAE[i] == null)
                        CNAE[i] = string.Empty;
                }
            }
        }

        public string [] sProduto
        {
            set
            {
                ProdutoID = value;

                for (int i = 0; i < ProdutoID.Length; i++)
                {
                    if ((ProdutoID[i] == null) || (ProdutoID[i] == ""))
                        ProdutoID[i] = string.Empty;
                }
            }
        }

        public string[] sPessoa
        {
            set
            {
                PessoaID = value;

                for (int i = 0; i < PessoaID.Length; i++)
                {
                    if ((PessoaID[i] == null) || (PessoaID[i] == ""))
                        PessoaID[i] = string.Empty;
                }
            }
        }
        public string[] bPermiteMesmaUF
        {
            set
            {
                PermiteMesmaUF = value;

                for (int i = 0; i < PermiteMesmaUF.Length; i++)
                {
                    if (PermiteMesmaUF[i] == null || PermiteMesmaUF[i] == "")
                        PermiteMesmaUF[i] = "0";
                }
            }
        }

        public string[] bExcecao 
        {
            set
            {
                Excecao = value;

                for (int i = 0; i < Excecao.Length; i++)
                {
                    if (Excecao[i] == null || Excecao[i] == "")
                        Excecao[i] = "0";
                }
            }
		}
        public Integer[] nGrupo
        {
            set
            {
                Grupo = value;

                for (int i = 0; i < Grupo.Length; i++)
                {
                    if (Grupo[i] == null)
                        Grupo[i] = Zero;
                }
            }
        }        
        public string[] sVigInicio
        {
            set
            {
                VigInicio = value;

                for (int i = 0; i < VigInicio.Length; i++)
                {
                    if (VigInicio[i] == null)
                        VigInicio[i] = string.Empty;
                }
            }
        }
        
        public string[] sVigFim
        {
            set
            {
                VigFim = value;

                for (int i = 0; i < VigFim.Length; i++)
                {
                    if ((VigFim[i] == null) || (VigFim[i] == ""))
                        VigFim[i] = string.Empty;
                }
            }
        }

        public string[] sObservacao 
        {
            set
            {
                Observacao = value;

                for (int i = 0; i < Observacao.Length; i++)
                {
                    if (Observacao[i] == null)
                        Observacao[i] = string.Empty;
                }
            }
        }
        
        public string[] sDataVigFim
        {
            set
            {
                DataVigFim = value;

                for (int i = 0; i < DataVigFim.Length; i++)
                {
                    if (DataVigFim[i] == null)
                        DataVigFim[i] = string.Empty;
                }
            }
        }
        public Integer[] nRegVigenciaInicioID
        {
            set
            {
                RegVigenciaInicioID = value;

                for (int i = 0; i < RegVigenciaInicioID.Length; i++)
                {
                    if (RegVigenciaInicioID[i] == null)
                        RegVigenciaInicioID[i] = Zero;
                }
            }
        }
        public Integer[]  nRegVigenciaFimID
        {
            set
            {
                RegVigenciaFimID = value;

                for (int i = 0; i < RegVigenciaFimID.Length; i++)
                {
                    if (RegVigenciaFimID[i] == null)
                        RegVigenciaFimID[i] = Zero;
                }
            }
        }
        public Integer[] nRegElementoID
        {
            set
            {
                RegElementoID = value;

                for (int i = 0; i < RegElementoID.Length; i++)
                {
                    if (RegElementoID[i] == null)
                        RegElementoID[i] = Zero;
                }
            }
        }
        public Integer[] nTipoID
        {
            set
            {
                TipoID = value;

                for (int i = 0; i < TipoID.Length; i++)
                {
                    if (TipoID[i] == null)
                        TipoID[i] = Zero;
                }
            }
        }

        private string Insercao(int i)
        {
            string sObs = (Observacao[i] != string.Empty) ? Observacao[i].Replace('^', ',') : string.Empty;
            
            ProcedureParameters[] param = new ProcedureParameters[] {
				    new ProcedureParameters("@RegraFiscalID", SqlDbType.Int, RegraFiscalID[i]),
				    new ProcedureParameters("@ElementoID", SqlDbType.Int, ElementoID[i]),
                    new ProcedureParameters("@CFOPID", SqlDbType.Int, ((!CFOPID[i].Equals(Zero)) ? (Object)CFOPID[i] : DBNull.Value)),
                    new ProcedureParameters("@FinalidadeID", SqlDbType.Int, ((!Finalidade[i].Equals(Zero)) ? (Object)Finalidade[i] : DBNull.Value)),
                    new ProcedureParameters("@UFOrigem", SqlDbType.VarChar, ((UFOrigem[i] != string.Empty) ? (Object)UFOrigem[i] : DBNull.Value)),
                    new ProcedureParameters("@UFDestino", SqlDbType.VarChar, ((UFDestino[i] != string.Empty) ? (Object)UFDestino[i] : DBNull.Value)),
                    new ProcedureParameters("@NCM", SqlDbType.VarChar, ((NCM[i] != string.Empty) ? (Object)Convert.ToString(NCM[i]) : DBNull.Value)),
                    new ProcedureParameters("@CNAE", SqlDbType.VarChar, ((CNAE[i] != string.Empty) ? (Object)Convert.ToString(CNAE[i]) : DBNull.Value)),
                    new ProcedureParameters("@Produto", SqlDbType.VarChar, ((ProdutoID[i] != string.Empty) ? (Object)ProdutoID[i] : DBNull.Value)),
                    new ProcedureParameters("@Pessoa", SqlDbType.VarChar, ((PessoaID[i] != string.Empty) ? (Object)PessoaID[i] : DBNull.Value)),
                    new ProcedureParameters("@Excecao", SqlDbType.VarChar, Excecao[i].ToString()),
                    new ProcedureParameters("@Grupo", SqlDbType.Int,((!Grupo[i].Equals(Zero)) ? (Object)Grupo[i] : DBNull.Value)),
                    new ProcedureParameters("@Observacao", SqlDbType.VarChar, (Object)sObs),
                    new ProcedureParameters("@Deletado", SqlDbType.Bit, DBNull.Value),
				    new ProcedureParameters("@PermiteMesmaUF", SqlDbType.VarChar, PermiteMesmaUF[i].ToString()),
                    new ProcedureParameters("@VigInicio", SqlDbType.DateTime, ((VigInicio[i] == string.Empty) ? DBNull.Value : (Object)VigInicio[i])),
                    new ProcedureParameters("@VigFim", SqlDbType.DateTime, ((VigFim[i] == string.Empty) ? DBNull.Value : (Object)VigFim)),
                    new ProcedureParameters("@Resultado", SqlDbType.Int, DBNull.Value, ParameterDirection.InputOutput),
                    new ProcedureParameters("@Mensagem", SqlDbType.VarChar, DBNull.Value, ParameterDirection.Output, 200)};
            DataInterfaceObj.execNonQueryProcedure("sp_RegrasFiscais_InsereElementos", param);

            if (param[16].Data != DBNull.Value)
                Resultado = int.Parse(param[10].Data.ToString());

            mensagem = (param[18].Data.ToString());

            return mensagem; 
        }

        private void GravacaoInsert(int i, bool insert) 
        {
            string sql = string.Empty;

            string sObs = (Observacao[i] != string.Empty) ? Observacao[i].Replace('^', ',') : string.Empty;

            sql = "UPDATE RegrasFiscais_Vigencias SET dtVigencia = '" + DataVigFim[i] + "' " +
                      "WHERE RegVigenciaID= " + RegVigenciaFimID[i] + " " +

                    "UPDATE RegrasFiscais_Elementos SET Observacao = '" + sObs + "', " +
                        "Grupo = " + ((!Grupo[i].Equals(Zero)) ? Grupo[i].ToString() : "NULL") + 
                        ", Excecao = " + Excecao[i].ToString() + " " +
                        "WHERE RegElementoID = " + RegElementoID[i];

            if ((insert) && (DataVigFim[i].Length > 0))
                sql += " INSERT INTO RegrasFiscais_Vigencias (RegraFiscalID, RegElementoID, dtVigencia, Vigente) " +
                           "SELECT " + RegraFiscalID[i] + ", " + RegElementoID[i] + ", '" + DataVigFim[i] + "', 0";

            DataInterfaceObj.ExecuteSQLCommand(sql);
        }

        private void Delecao(int i)
        {
            string sql;

            sql = "DELETE FROM RegrasFiscais_Vigencias WHERE RegElementoID = " + RegElementoID[i];
            DataInterfaceObj.ExecuteSQLCommand(sql);

            sql = "DELETE FROM RegrasFiscais_Elementos WHERE RegElementoID = " + RegElementoID[i];
            DataInterfaceObj.ExecuteSQLCommand(sql);
        }

        protected override void PageLoad(object sender, EventArgs e)
		{
            for (int i = 0; i < TipoID.Length; i++)
            {

                if (TipoID[i].intValue() == 1)
                {
                    GravacaoInsert(i, false);
                }                
                else if (TipoID[i].intValue() == 2)
                {
                    Delecao(i);
                }                
                else if ((TipoID[i].intValue() == 3))
                {
                    Insercao(i);
                }
                else if (TipoID[i].intValue() == 4)
                {
                    GravacaoInsert(i, true);
                }
            
            }
            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT " + ((mensagem == null || mensagem.Length == 0) ? "NULL" :
					"'" + mensagem + "'") + " AS Mensagem"));
        }
	}
}
