using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.regrasfiscais.serverside
{
    public partial class incluirelementos : System.Web.UI.OverflyPage
	{
        protected static Integer Zero = new Integer(0);

        private static Integer [] RegMensagemID;
        private static Integer[] RegraFiscalID;
        private static Integer [] Ordem;
        private string[] Condicional;
        private static string[] Mensagem;
        private static string[] Observacao;
        private static Integer[] TipoID;

        public Integer[] nRegMensagemID
        {
            set
            {
                RegMensagemID = value;

                for (int i = 0; i < RegMensagemID.Length; i++)
                {
                    if (RegMensagemID[i] == null)
                        RegMensagemID[i] = Zero;
                }
            }
        }
        public Integer[] nRegraFiscalID 
        {
            set
            {
                RegraFiscalID = value;

                for (int i = 0; i < RegraFiscalID.Length; i++)
                {
                    if (RegraFiscalID[i] == null)
                        RegraFiscalID[i] = Zero;
                }
            }
		}
        public Integer[] nOrdem 
        {
			 set
            {
                Ordem = value;

                for (int i = 0; i < Ordem.Length; i++)
                {
                    if (Ordem[i] == null)
                        Ordem[i] = Zero;
                }
            }
		}

		public string[] bCondicional 
        {
            set
            {
                Condicional = value;

                for (int i = 0; i < Condicional.Length; i++)
                {
                    if (Condicional[i] == null || Condicional[i] == "")
                        Condicional[i] = "0" ;
                }
            }
		}
        protected string[] sMensagem
        {
            set
            {
                Mensagem = value;

                for (int i = 0; i < Mensagem.Length; i++)
                {
                    if (Mensagem[i] == null || Mensagem[i].Length == 0)
                        Mensagem[i] = string.Empty;
                }
            }
        }
        public string[] sObservacao 
        {
            set
            {
                Observacao = value;

                for (int i = 0; i < Observacao.Length; i++)
                {
                    if (Observacao[i] == null || Observacao[i].Length == 0)
                        Observacao[i] = string.Empty;
                }
            }
        }
        public Integer[] nTipoID
        {
            set
            {
                TipoID = value;

                for (int i = 0; i < TipoID.Length; i++)
                {
                    if (TipoID[i] == null )
                        TipoID[i] = Zero;
                }
            }
        }

        private void Insercao(int i)
        {
            string sql;

            sql = "INSERT INTO dbo.RegrasFiscais_Mensagens ( RegraFiscalID, Ordem, Condicional, Mensagem, Observacao) VALUES ( " +
               RegraFiscalID[i] + ", " + Ordem[i] + ", " + Condicional[i] + ", '" + Mensagem[i] + "', '" + Observacao[i] + "' ) ";
            DataInterfaceObj.ExecuteSQLCommand(sql);
        }        
        private void Gravacao(int i) 
        {
            string sql;

            sql = "UPDATE dbo.RegrasFiscais_Mensagens SET Condicional = " + Condicional[i] + ", Mensagem = '" + Mensagem[i] + "', Observacao = '" + Observacao[i] +
                      "', Ordem = " + Ordem[i] + "  WHERE RegMensagemID= " + RegMensagemID[i] + " AND RegraFiscalID=  " + RegraFiscalID[i];
                DataInterfaceObj.ExecuteSQLCommand(sql);                    
        }
        private void Delecao(int i)
        {
            string sql;

            sql = "DELETE FROM dbo.RegrasFiscais_Mensagens WHERE RegMensagemID = " + RegMensagemID[i] + " AND RegraFiscalID = " + RegraFiscalID[i];
                DataInterfaceObj.ExecuteSQLCommand(sql);
        }

        protected override void PageLoad(object sender, EventArgs e)
		{
            for (int i = 0; i < TipoID.Length; i++)
            {

                if (TipoID[i].intValue() == 1)
                {
                    Delecao(i);
                }
                else if (TipoID[i].intValue() == 2)
                {
                    Gravacao(i);
                }
                else if ((TipoID[i].intValue() == 3) && (Mensagem[i] != string.Empty))
                    Insercao(i);
            }
            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT SPACE(0) AS [Error]"));
        }
	}
}
