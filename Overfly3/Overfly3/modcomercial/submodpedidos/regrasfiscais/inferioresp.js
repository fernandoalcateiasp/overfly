/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)       
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();
    var nNivel = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Nivel'].value");


    setConnection(dso);

    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.RegraFiscalID, a.ProprietarioID,a.AlternativoID,a.Observacoes FROM RegrasFiscais a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RegraFiscalID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24116) // Mapeamento
    {
        dso.SQL = 'SELECT a.ElementoID AS ElementoID, a.ImpostoID AS ImpostoID, RegMapeamentoID, RegraFiscalID, CadastroNivel0, RelacaoNivel0, ' +
	                        'CadastroNivel12, RelacaoNivel12, Observacao ' +
                        'FROM RegrasFiscais_Mapeamento a WITH(NOLOCK) ' +
                        'WHERE ' + sFiltro + ' a.RegraFiscalID = ' + idToFind + ' ' +
                        'ORDER BY ISNULL(a.ImpostoID, 0), (SELECT aa.Ordem FROM TiposAuxiliares_Itens aa WITH(NOLOCK) WHERE (aa.ItemID = a.ElementoID)), ' +
                            '(SELECT aa.Ordem FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.ImpostoID))';
        return 'dso01Grid_DSC';
    } 
    else if (folderID == 24117) // Relacionamento
    {
        dso.SQL = 'SELECT DISTINCT a.RegraFiscalID, b.RecursoAbreviado as Est, a.RegraFiscal,a.Legislacao, a.dtData AS Data, a.Observacao ' +
                     'FROM RegrasFiscais  a WITH(NOLOCK)' +
                   '  INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) c ON (c.RegraFiscalID = a.RegraFiscalID)' +
                    ' left JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID) ' +
                    ' INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + idToFind + ') d ON (d.AgrupadorID = c.AgrupadorID) ' +
                  ' WHERE (a.Nivel = ' + (nNivel + 1) + ')' +
                    ' ORDER BY a.RegraFiscalID';
       
        return 'dso01Grid_DSC';
    }
    
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }                     
    }
    else if ( pastaID == 24116 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT  NULL AS fldID, SPACE(1) AS fldName, 0 AS Ordem ' +
                       'UNION ALL '+
                       'SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem '+
                       'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' + 
                       'WHERE TipoID = 427 AND EstadoID = 2 ORDER BY Ordem ';
        }
        else if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT  NULL AS fldID, SPACE(1) AS fldName, 0 AS Ordem ' +
                      'UNION ALL ' +
                      'SELECT ConceitoID AS fldID, Imposto AS fldName, Ordem ' +
                      'FROM Conceitos WITH(NOLOCK) ' +
                      'WHERE TipoConceitoID = 306 AND EstadoID = 2 AND PaisID = 130 AND DestacaNota = 1 ORDER BY Ordem ';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
    var nNivel = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Nivel'].value");

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    if (nNivel == 0) 
    {
        vPastaName = window.top.getPastaName(24116);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 24116); // Mapeamento
    }
    if (nNivel < 2) 
    {
        vPastaName = window.top.getPastaName(24117);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 24117); // Relacionamento
    }

    
    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 24116) // Mapeamento
        {
            var nElementoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ElementoID'));
            var nImpostoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ImpostoID'));
            var cC0 = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'CadastroNivel0'));
            var cR0 = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'RelacaoNivel0'));
            var cC12 = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'CadastroNivel12'));
            var cR12 = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'RelacaoNivel12'));

            if ((nElementoID != 0) && (nImpostoID != 0)) {
                if (window.top.overflyGen.Alert('N�o � possivel gravar Imposto e Elemento, escolha somente um.') == 0)
                    return null;

                currLine = -1;
            }
            else if ((nElementoID == 0) && (nImpostoID == 0)) {
                if (window.top.overflyGen.Alert('Preencha Imposto ou Elemento.') == 0)
                    return null;

                currLine = -1;
            }
            else if ((cC0 == 0) && (cR0 == 0) && (cC12 == 0) && (cR12 == 0)) {
                if (window.top.overflyGen.Alert('Selecionar pelo menos uma das Op��es.') == 0)
                    return null;

                currLine = -1;
            }
            else
                currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['RegraFiscalID', registroID]);
        }
        
        if (currLine < 0) {
            if (fg.disabled == false) {
                try {
                    fg.Editable = true;
                    window.focus();
                    fg.focus();
                }
                catch (e) {
                    ;
                }
            }

            return false;
        }    
    
    }
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 24116) // Mapeamento
    {

        headerGrid(fg, ['Elemento',
                        'Imposto',
                        'C N0',
                        'R N0',
                        'C N1/2',
                        'R N1/2',
                        'Observa��o',
                        'RegMapeamentoID'],[7]);



        fillGridMask(fg, currDSO, ['ElementoID','ImpostoID', 'CadastroNivel0', 'RelacaoNivel0','CadastroNivel12','RelacaoNivel12', 'Observacao','RegMapeamentoID'],
                                   ['', '', '', '', '', '', '', '']);

        glb_aCelHint = [[0, 2, 'Cadastro N�vel 0'],
                        [0, 3, 'Rela��o N�vel 0'],
                        [0, 4, 'Cadastro N�vel 1/2'],
                        [0, 5, 'Rela��o N�vel 1/2']];
                                 

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;

    }
    if (folderID == 24117) // Relacionamento
    {
        headerGrid(fg, ['Regra Fiscal ',
                       'Est',
                       'RegraFiscal',
                       'Legisla��o',
                       'Data',
                       'Observa��o'],[]);


        fillGridMask(fg, currDSO, ['RegraFiscalID', 'Est', 'RegraFiscal', 'Legislacao', 'Data', 'Observacao'],
                                   ['', '', '', '', '',  '']);

        alignColsInGrid(fg, [0]);

       /* // Merge de Colunas
        fg.MergeCells = 1;
        fg.MergeCol(-1) = true;*/
    }
}
