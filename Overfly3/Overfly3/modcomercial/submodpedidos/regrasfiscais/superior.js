/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// controla o retorno do servidor dos dados de combos dinamicos
var glb_CounterCmbsDynamics = 0;
// guarda o botao clicado enviado pelo frameword
var glb_BtnFromFrameWork;

// Dados do registro corrente .SQL 
var dsoSup01 = new CDatatransport("dsoSup01");
// Descricao do estado atual do registro .SQL
var dsoStateMachine = new CDatatransport('dsoStateMachine');

// Dados dos combos estaticos .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// Dados do combo de nota fiscal .URL 
var dsoCmbsLupa = new CDatatransport("dsoCmbsLupa");
var dsoCmbsLupa2 = new CDatatransport("dsoCmbsLupa2");
var dsoCombo = new CDatatransport("dsoCombo");

var glb_RegraFiscal = null;
var glb_BtnFromFramWork;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selTipoRegraFiscalID', '1']]);

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modcomercial/submodpedidos/regrasfiscais/inferior.asp',
                              SYS_PAGESURLROOT + '/modcomercial/submodpedidos/regrasfiscais/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'RegraFiscalID';
   
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';

    
}

function setupPage() 
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);

    ajustaCampos();
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/
function CmbDynamics() 
{
    var nRegistroID = dsoSup01.recordset['RegraFiscalID'].value;
    var nNivel = dsoSup01.recordset['Nivel'].value;
    var sql = '';

    setConnection(dsoCombo);
    
    sql = 'SELECT 0 AS fldID, SPACE(0) AS fldName, 0 TipoRegraFiscalID, 0 AS Nivel ' + 
                    'UNION ALL ' +
                'SELECT a.RegraFiscalID AS fldID, (CONVERT(VARCHAR(16),a.RegraFiscalID) + SPACE(2) + a.RegraFiscal) AS fldName, a.TipoRegraFiscalID, a.Nivel ' +
                    'FROM RegrasFiscais a WITH(NOLOCK) ' +
                    'WHERE a.Nivel IN (0, 1)';


    dsoCombo.SQL = sql;
    dsoCombo.ondatasetcomplete = CmbDynamics_DCS;
    dsoCombo.Refresh();
}

function CmbDynamics_DCS() 
{
    var nRegraMae = dsoSup01.recordset['RegraFiscalMaeID'].value;
    var optionStr, optionValue, nTipoRegraFiscalID, nNivel;
    
    clearComboEx(['selRegraFiscalMae']);

    while (!dsoCombo.recordset.EOF) 
    {
        optionStr = dsoCombo.recordset['fldName'].value;
        optionValue = dsoCombo.recordset['fldID'].value;
        nTipoRegraFiscalID = dsoCombo.recordset['TipoRegraFiscalID'].value;
        nNivel = dsoCombo.recordset['Nivel'].value;
        
        var oOption = document.createElement("OPTION");
        
        oOption.text = optionStr;
        oOption.value = optionValue;
        oOption.setAttribute('TipoRegraFiscalID', nTipoRegraFiscalID, 1);
        oOption.setAttribute('Nivel', nNivel, 1);        
        
        selRegraFiscalMae.add(oOption);
        dsoCombo.recordset.MoveNext();        
    }

    if (nRegraMae > 0)
        selRegraFiscalMae.value = nRegraMae;
        
    return null;
}
/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWREGRASFISCAIS')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
            
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        window.top.focus();
        
        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
        
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
  ; //                 o programador. False ou null devolve para o framework.
    
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clic   ado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked) 
{      
    //@@
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;

    CmbDynamics();

    // volta para a automacao
    finalOfSupCascade(glb_BtnFromFramWork);
}


/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad() 
{
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Configura��es de Regras Fiscais', 'Resumo', 'Detalhar M�e']);

    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);
}


/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    // campos read-only
    setReadOnlyFields();

    if (btnClicked == 'SUPINCL') 
    {
        selTipoRegraFiscalID.disabled = (selRegraFiscalMae.value > 0);
        txtNivel.value = 0;
        CmbDynamics();
    }
    else if (btnClicked == 'SUPDET')
        ajustaCampos();

    else if (btnClicked == 'SUPALT')
            txtRegistroID.readOnly = true;
        
    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
       
}
/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    if (cmbID == 'selRegraFiscalMae') {
        var nTipoRegraFiscalID = selRegraFiscalMae.options.item(selRegraFiscalMae.selectedIndex).getAttribute('TipoRegraFiscalID', 1);
        var nNivelMae = selRegraFiscalMae.options.item(selRegraFiscalMae.selectedIndex).getAttribute('Nivel', 1);
        var nNivel = 0;

        selTipoRegraFiscalID.value = (isNaN(nTipoRegraFiscalID) ? -1 : nTipoRegraFiscalID);

        selTipoRegraFiscalID.disabled = (selRegraFiscalMae.value > 0);

        if (selRegraFiscalMae.value > 0)
            nNivel = nNivelMae + 1;

        txtNivel.value = nNivel;

        ajustaCampos();
    }
    else if (cmbID == 'selTipoRegraFiscalID') 
    {
        if (selRegraFiscalMae.value == 0)
            txtNivel.value = 0;
    }        
        
    
    adjustLabelsCombos(false);
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{
;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    //@@ padrao do frame work
    return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
/*function btnLupaClicked(btnClicked)
{
    var cmbID = '';
    
    if ( btnClicked.id == 'btnFindNota' )
    {
        cmbID = 'selNotaFiscalID';
        // Nao mexer - Inicio de automacao ==============================
        // Invoca janela modal
        loadModalOfLupa(cmbID, null);
        // Final de Nao mexer - Inicio de automacao =====================
    }    
    else if ( btnClicked.id == 'btnFindPessoa' )
        openModalPessoa();
}*/

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) 
{
    var empresa = getCurrEmpresaData();
    var RegraFiscalMaeID = dsoSup01.recordset['RegraFiscalMaeID'].value;

    if (controlBar == 'SUP') {
        // Documentos
        if (btnClicked == 1) {
            __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
        }
        // usuario clicou botao imprimir
        else if (btnClicked == 2)
            openModalPrint();
        // Procedimento
        else if (btnClicked == 3)
            window.top.openModalControleDocumento('S', '', 740, null, '22', 'T');
        else if ((controlBar == 'SUP') && (btnClicked == 4)) {
            openModalMensagens();
        }
        else if (btnClicked == 5) {
            var nCurrRegraFiscalID = dsoSup01.recordset['RegraFiscalID'].value;
            window.top.openModalHTML(nCurrRegraFiscalID, 'Regras Fiscal ' + nCurrRegraFiscalID + ' - Resumo (5/5)', 'S', 11, 0);
        }

        else if (btnClicked == 6)
        {
            if (RegraFiscalMaeID != null)
                sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL', [RegraFiscalMaeID, null, null]);
                // sendJSCarrier(getHtmlId(), 'SHOWREGRASFISCAIS', new Array(empresa[0], RegraFiscalMaeID));
         }
    }
}


function openModalMensagens() 
{
    var RegraFiscalID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'RegraFiscalID' + '\'' + '].value');
    var strPars = new String();
   
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)

    strPars = '?sCaller=' + escape('S');
    strPars += '&nRegraFiscalID = ' + escape(RegraFiscalID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/modalpages/modalmensagens.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 539));
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {

    if (btnClicked == 'SUPINCL')
    {
        selTipoRegraFiscalID.selectedIndex = -1;
        selRegraFiscalMae.selectedIndex = -1;
        adjustLabelsCombos(false);
    }

    // Para prosseguir a automacao retornar null
 
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALMENSAGENSHTML')
    {
        if ( param1 == 'OK' )                
        {
            
            // esta funcao que fecha a janela modal e destrava a interface
            // restoreInterfaceFromModal() foi movida para final das funcoes
            // chamadas pela funcao fillComboParceiro;    
            // escreve na barra de status
          //  writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }   
    if (idElement.toUpperCase() == 'MODALELEMENTOSHTML')
    {
        if (param1 == 'OK') {
            
            // esta funcao que fecha a janela modal e destrava a interface
            // restoreInterfaceFromModal() foi movida para final das funcoes
            // chamadas pela funcao fillComboParceiro;    
            // escreve na barra de status
            //  writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }

    if (idElement.toUpperCase() == 'MODALIMPOSTOSHTML') {
        if (param1 == 'OK') {
            
            // esta funcao que fecha a janela modal e destrava a interface
            // restoreInterfaceFromModal() foi movida para final das funcoes
            // chamadas pela funcao fillComboParceiro;    
            // escreve na barra de status
            //  writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALRELACOESHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }           
    // Modal de impressao
    if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALOPENHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }    
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // campos read-only
    setReadOnlyFields();

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}


/********************************************************************
Funcao criada pelo programador.
Recolhe dados no servidor e preenche combos de lupa.

Parametro:
textoDig        - texto digitado pelo usuario na modal 
idCombo         - id do combo a preencher
idForm          - id do form

Retorno:
nenhum
********************************************************************/
/*function getDataAndLoadCmbsLupa(textoDig, idCombo, idForm)
{
    glb_cmbLupaID = idCombo;
    var sParam1 = '';
    
    sParam1 = selNotaFiscalID.value;
    var param2 = getCurrEmpresaData();
        
  	if ( sParam1 == null )
	{
	    clearCombo([idCombo]);
	    document.getElementById(idCombo).disabled = true;
	    return null;
	}

    var strPars = new String();
    strPars = '?sParam1=' + escape(sParam1.toString());
    strPars += '&sParam2=' + escape(param2[0]);
    strPars += '&sText=' + escape(textoDig.toString());
    strPars += '&DATE_SQL_PARAM=' + escape(DATE_SQL_PARAM);

    dsoCmbsLupa.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmlotesEx/serverside/fillcomboslupa.aspx' + strPars;
    dsoCmbsLupa.ondatasetcomplete = getDataAndLoadCmbsLupa_DSC;
    dsoCmbsLupa.refresh();
}*/

/********************************************************************
Funcao criada pelo programador.
Recebe dados no servidor e preenche combos de lupa.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
/*function getDataAndLoadCmbsLupa_DSC() {
    var cmbRef;

    cmbRef = selNotaFiscalID;
        
    clearComboEx(['selNotaFiscalID']);
            	    
    var optionStr,optionValue;
    while (!dsoCmbsLupa.recordset.EOF)
    {   
        var oOption = document.createElement("OPTION");
        oOption.text = dsoCmbsLupa.recordset['fldName'].value;
        oOption.value = dsoCmbsLupa.recordset['fldID'].value;
        cmbRef.add(oOption);
        dsoCmbsLupa.recordset.MoveNext();
    }
            	    
    // destrava a interface e fecha a janela modal
    if (restoreInterfaceFromModal())
    {
        cmbRef.disabled = true;

        if ( cmbRef.options.length != 0 )
        {
            var nNotaID = dsoSup01.recordset['NotaFiscalID'].value;
            if ( (nNotaID != null) && (nNotaID != '') )
            {
                selOptByValueInSelect('lotessup01Html', 'selNotaFiscalID', nNotaID);
                fillFieldsNota(dsoCmbsLupa, nNotaID, 'V_dtNotaFiscal', 'PedidoID', 'Pessoa', 'PessoaID');
            }    
            // destrava e coloca foco no combo se tem options
            cmbRef.disabled = false;
            cmbRef.focus();
        }
    }

    // escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Altera��o');
}*/

/********************************************************************
Funcao do programador
Seta o campos read-only do sup
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setReadOnlyFields() 
{
    txtNivel.readOnly = true;
}

/********************************************************************
Funcao do programador
Preenche os campos read-only que estao na tabela de nota fiscal
           
Parametros: 
dso                 : Objeto RDS on se encontra os dados da Nota Fiscal
nValToPesq          : Numero da Nota se ser pesquisada
sdtNotaFieldName    : Nome do campo no dso que contem a data da nota
sPedidoIDFieldName  : Nome do campo no dso que contem o ID do Pedido
sPessoaFieldName    : Nome do campo no dso que contem o nome da Pesso

Retorno:
nenhum
********************************************************************/
function fillFieldsNota(dso, nValToPesq, sdtNotaFieldName, sPedidoIDFieldName, sPessoaFieldName, sPessoaIDFieldName)
{
    if (! ((dso.recordset.BOF)&&(dso.recordset.EOF)))
    {
        dso.recordset.MoveFirst();
        dso.recordset.Find('fldID', nValToPesq);
        if (!dso.recordset.EOF)
        {
            txtdtNota.value = dso.recordset[sdtNotaFieldName].value;
            txtPedidoID.value = dso.recordset[sPedidoIDFieldName].value;
        }
        else
        {
            txtdtNota.value = '';
            txtPedidoID.value = '';
        }
    }
    else
    {
        txtdtNota.value = '';
        txtPedidoID.value = '';
    }
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);
    strPars += '&sEmissor=' + escape(chkEmissor.checked ? 'nossa' : 'sua');
    strPars += '&nNotaFiscal=' + escape(getCurrDataInControl('sup', 'selNotaFiscalID', 'true'));
    strPars += '&sDtNota=' + escape(getCurrDataInControl('sup', 'txtdtNota'));
    strPars += '&sDtCarta=' + escape(getCurrDataInControl('sup', 'txtdtCartaCorrecao'));
    strPars += '&nPessoaID=' + escape(dsoSup01.recordset['PessoaID'].value);
    strPars += '&nCartaCorrecaoID=' + escape(dsoSup01.recordset['CartaCorrecaoID'].value);    

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/modalpages/modalprint.asp' + strPars;
    
    showModalWin(htmlPath, new Array(346,200));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de parceiro 

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPessoa()
{
    var htmlPath;
    var strPars = new Array();
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];
    var nContextoID = getCmbCurrDataInControlBar('sup', 1);
    
    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa);
    strPars += '&nContextoID=' + escape(nContextoID[1]);

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/regrasfiscais/modalpages/modalpessoa.asp' + strPars;
    showModalWin(htmlPath, new Array(571,284));
}

function adjustLabelsCombos(bPaging)
{
    var nRegraMae = null;

    if ((!dsoSup01.recordset.BOF) || (!dsoSup01.recordset.BOF))
        nRegraMae = dsoSup01.recordset['RegraFiscalMaeID'].value;

    if (nRegraMae == null)
        nRegraMae = '';
   
    if (bPaging) {

        setLabelOfControl(lblTipo, dsoSup01.recordset['TipoRegraFiscalID'].value);
        setLabelOfControl(lblRegraFiscalMae, nRegraMae);
    }
    else {

        setLabelOfControl(lblTipo, selTipoRegraFiscalID.value);
        setLabelOfControl(lblRegraFiscalMae, selRegraFiscalMae.value);
    }
}

function ajustaCampos() 
{
    if ((txtNivel.value != 0) && (txtNivel.value != '')) {
        lblOrdem.style.visibility = 'hidden';
        txtOrdem.style.visibility = 'hidden';
        lblContinua.style.visibility = 'hidden';
        chkContinua.style.visibility = 'hidden';

        //@@ *** Div principal superior divSup01_01 ***/
        adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
                          ['lblEstadoID', 'txtEstadoID', 3, 1, -5],
                          ['lblRegraFiscal', 'txtDescricaoRegraFiscal', 45, 1, -5],
                          ['lblRegraFiscalMae', 'selRegraFiscalMae', 45, 1, -5],
                          ['lblTipo', 'selTipoRegraFiscalID', 17, 1, -5],
                          ['lblNivel', 'txtNivel', 3, 2],
                          ['lblLegislacao', 'txtLegislacao', 80, 2],
                          ['lbldata', 'txtdata', 10, 2],
                          ['lblObservacao', 'txtObservacao', 40, 3]], null, null, true);
    }
    else {
        lblOrdem.style.visibility = 'inherit';
        txtOrdem.style.visibility = 'inherit';
        lblContinua.style.visibility = 'inherit';
        chkContinua.style.visibility = 'inherit';

        //@@ *** Div principal superior divSup01_01 ***/
        adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
                          ['lblEstadoID', 'txtEstadoID', 3, 1, -5],
                          ['lblRegraFiscal', 'txtDescricaoRegraFiscal', 45, 1, -5],
                          ['lblRegraFiscalMae', 'selRegraFiscalMae', 45, 1, -5],
                          ['lblTipo', 'selTipoRegraFiscalID', 17, 1, -5],
                          ['lblNivel', 'txtNivel', 3, 2],
                          ['lblOrdem', 'txtOrdem', 6, 2],
                          ['lblContinua', 'chkContinua', 3, 2],
                          ['lblLegislacao', 'txtLegislacao', 80, 2],
                          ['lbldata', 'txtdata', 10, 2],
                          ['lblObservacao', 'txtObservacao', 40, 3]], null, null, true);
    }
}