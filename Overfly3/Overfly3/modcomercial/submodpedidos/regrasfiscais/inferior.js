/********************************************************************
inferior.js

Library javascript para o inferior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Dados do registro corrente .SQL (no minimo: regID, observacoes, propID e alternativoID)
var dso01JoinSup = new CDatatransport("dso01JoinSup");

// Dados de combos estaticos do form .URL
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");

// Dados de combos dinamicos do form .URL
var dsoDynamicCmbs = new CDatatransport("dsoDynamicCmbs");

// Dados de um grid .SQL
var dso01Grid = new CDatatransport("dso01Grid");
var dsoCmb01Grid_01 = new CDatatransport("dsoCmb01Grid_01");
var dsoCmb02Grid_01 = new CDatatransport("dsoCmb02Grid_01");
var dso01GridLkp = new CDatatransport("dso01GridLkp");
var dso02GridLkp = new CDatatransport("dso02GridLkp");
var dso03GridLkp = new CDatatransport("dso03GridLkp");
var dso04GridLkp = new CDatatransport("dso04GridLkp");

// Descricao dos estados atuais dos registros do grid corrente .SQL
var dsoStateMachineLkp = new CDatatransport("dsoStateMachineLkp");        

// Dados do combo de filtros da barra inferior .SQL
var dsoFiltroInf = new CDatatransport("dsoFiltroInf");

// Dados dos combos de proprietario e alternativo
var dsoCmbsPropAlt = new CDatatransport("dsoCmbsPropAlt");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES:
    adjustElementsInDivsNonSTD()
    putSpecialAttributesInControls()
    optChangedInCmb(cmb)
    prgServerInf(btnClicked)
    prgInterfaceInf(btnClicked, pastaID, dso)
    finalOfInfCascade(btnClicked)
    treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, currDSO)
    btnLupaClicked(btnClicked)
    addedLineInGrid(folderID, grid, nLineInGrid, dso)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    btnAltPressedInGrid( folderID )
    btnAltPressedInNotGrid( folderID )
    fillCmbFiltsInfAutomatic(folderID)
    selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
    frameIsAboutToApplyRights(folderID)

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG
    fg_AfterRowColChange_Prg()
    fg_DblClick_Prg()
    fg_ChangeEdit_Prg()
    fg_ValidateEdit_Prg()
    fg_BeforeRowColChange_Prg()
    fg_EnterCell_Prg()
    fg_MouseUp_Prg()
    fg_MouseDown_Prg()
    fg_BeforeEdit_Prg()

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    // Ze em 17/03/08
    dealWithGrid_Load();
   
    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos.
    glb_aStaticCombos = null;
    
    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ]
    glb_aGridStaticCombos = [[24116, [[0, 'dsoCmb01Grid_01', 'fldName', 'fldID'],
                                      [1, 'dsoCmb02Grid_01', 'fldName', 'fldID']]],
                             [20010, [[0, 'dso01GridLkp', '', ''],
                                      [0, 'dso02GridLkp'   , ''             , ''],
                                      [0, 'dso03GridLkp'   , ''             , ''],
                                      [0, 'dso04GridLkp'   , ''             , '']]]];
    
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03'],
                               [20008, 20009,[24116,24117,20010]]);
        
    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();

}

function setupPage()
{
    //@@ Ajustar os divs
    // Nada a codificar
}

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg()
{

}

function fg_DblClick_Prg() 
{
    DetalhaRelacao();
}

function fg_ChangeEdit_Prg()
{

}
function fg_ValidateEdit_Prg()
{

}
function fg_BeforeRowColChange_Prg()
{

}

function fg_EnterCell_Prg()
{

}

function fg_MouseUp_Prg()
{

}

function fg_MouseDown_Prg()
{

}

function fg_BeforeEdit_Prg()
{

}

// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD()
{
    //@@
    // Este form nao tem este caso    
}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    // troca a pasta default para correcoes
    glb_pastaDefault = 24116;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

     
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div

	// Mover esta funcao para os finais retornos de operacoes no
	// banco de dados
	finalOfInfCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso) {

    showBtnsEspecControlBar('inf', false, [1]);
    tipsBtnsEspecControlBar('inf', ['']);


    if (pastaID == 24117) {
        showBtnsEspecControlBar('inf', true, [1]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Regra Fiscal']);
    }
    else
        showBtnsEspecControlBar('inf', true, [0]);

    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    
    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ( (btnClicked.toUpperCase()).indexOf('SUP') >= 0 )
        // usario clicou botao no control bar sup
	    sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
	else
	    // usario clicou botao no control bar inf
	    sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
	    
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
{

    //@@
    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR')
          || (btnClicked == 'SUPOK') || (btnClicked == 'INFREFR')
          || (btnClicked == 'INFOK') || (btnClicked == 'INFCANC')) {
        setupEspecBtnsControlBar('sup', 'HHDHHH');
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'adjustLabelsCombos(true)');
    }
     else
        setupEspecBtnsControlBar('sup', 'HDHHHH');
    
    if (folderID == 24117) {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'H');
        else
            setupEspecBtnsControlBar('inf', 'D');
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID )
{
    


}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {

    if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 24117)) {
        DetalhaRelacao();
    }
        
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid( folderID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid( folderID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID)
{
    
    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
{

    //@@ retornar null para prosseguir a automacao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra infeior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID)
{
    // Pasta de LOG Read Only
    if ((folderID == 20010) && (folderID == 24117))
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else
    {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao OK do control bar inferior.
QUER GRAVAR BULLET E TAB EM CAMPO TEXTAREA

Parametros: 
controlBar      - 'SUP' ou 'INF'

Retornos:
null            - nao grava, qualquer valor difetente de null grava
********************************************************************/
function saveBulletAndTabInTextArea(controlBar, btnClicked, folderID) 
{
    if ((controlBar == 'INF') && (btnClicked == 'INFOK'))
    {
        if (folderID == 20008)
            return 1;
    }

    return null;
}

function DetalhaRelacao() 
{
    var empresa = getCurrEmpresaData();
    var RegraFiscalID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'RegraFiscalID').value);

    if (RegraFiscalID != null)
        sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL', [RegraFiscalID, null, null]);
    // sendJSCarrier(getHtmlId(), 'SHOWREGRASFISCAIS', new Array(empresa[0], RegraFiscalMaeID));
}