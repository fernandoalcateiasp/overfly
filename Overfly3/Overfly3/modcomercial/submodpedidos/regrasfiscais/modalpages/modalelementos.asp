
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalelementosHtml" name="modalelementosHtml">

<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0"></meta>
<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/regrasfiscais/modalpages/modalelementos.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/regrasfiscais/modalpages/modalelementos.js" & Chr(34) & "></script>" & vbCrLf
%>

<%

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nRegraFiscalID, nPaisEmpresaID, sCaller, rsData, strSQL

sCaller = ""
nRegraFiscalID = 0
nPaisEmpresaID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nRegraFiscalID").Count    
    nRegraFiscalID = Request.QueryString("nRegraFiscalID")(i)
Next

For i = 1 To Request.QueryString("nPaisEmpresaID").Count    
    nPaisEmpresaID = Request.QueryString("nPaisEmpresaID")(i)
Next


Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nRegraFiscalID = " & CStr(nRegraFiscalID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nPaisEmpresaID = " & CStr(nPaisEmpresaID) & ";"
Response.Write vbcrlf

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>


<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalElemento_BeforeEdit(fg, arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
//<!--
 js_fg_modalElementos_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>   

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalElementos_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
    js_fg_modalElementosDblClick(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

</HEAD>

<body id="modalelementosBody" name="modalelementosBody" LANGUAGE="javascript" onload="return window_onload()">
     <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    

    <p id="lblDicas" name="lblDicas" class="lblGeneral">Dicas</p>
    <input type="checkbox" id="chkDicas" name="chkDicas" class="fldGeneral" title="Dicas de uso da Regra Fiscal"></input>
    
    <p id="lblVisao" name="lblVisao" class="lblGeneral">Vis�o</p>
    <select id="selVisao" name="selVisao" class="fldGeneral"  onchange="return cmbOnChange(this)"></select>    
    
    <p id="lblInc" name="lblInc" class="lblGeneral">Incluir</p>  
    <input type="checkbox" id="chkInc" name="chkInc" class="fldGeneral" title="Incluir registros"></input>
    <p id="lblElemento" name="lblElemento" class="lblGeneral">Elemento</p>
	<select id="selElemento" name="selElemento" class="fldGeneral"  onchange="return cmbOnChange(this)"></select>
    <p id="lblUFOrigem" name="lblUFOrigem" class="lblGeneral">UF Origem</p>
    <select id="selOrigem" name="selOrigem" class="fldGeneral" MULTIPLE>
<%
		 Set rsData = Server.CreateObject("ADODB.Recordset")
		strSQL ="SELECT  a.LocalidadeID AS fldID, a.CodigoLocalidade2 AS fldName " & _
				"FROM Localidades a WITH(NOLOCK) " & _
				"WHERE (a.LocalizacaoID = " & nPaisEmpresaID & ") " & _
				"ORDER BY a.CodigoLocalidade2"
							
			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF    
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				
				If ( CLng(rsData.Fields("fldID").Value) = CLng(1) ) Then
					Response.Write " SELECTED "
				End If

				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>		
        </select>	

    <p id="lblUFDestino" name="lblUFDestino" class="lblGeneral">UF Destino</p>
    <select id="SelDestino" name="selDestino" class="fldGeneral"'''' MULTIPLE>    
<%
		 Set rsData = Server.CreateObject("ADODB.Recordset")
		strSQL = "SELECT  a.LocalidadeID AS fldID, a.CodigoLocalidade2 AS fldName " & _
				"FROM Localidades a WITH(NOLOCK) " & _
				"WHERE (a.LocalizacaoID  = " & nPaisEmpresaID & ") " & _
				"ORDER BY a.CodigoLocalidade2"
							
			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				
				If ( CLng(rsData.Fields("fldID").Value) = CLng(1) ) Then
					Response.Write " SELECTED "
				End If

				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>		
        </select>	
    
    <p id="lblGrupo" name="lblGrupo" class="lblGeneral">Grupo</p>
    <select id="selGrupo" name="selGrupo" class="fldGeneral"  onchange="return cmbOnChange(this)"></select>
    <p id="lblUF" name="lblUF" class="lblGeneral">UF</p>
    <input type="checkbox" id="chkUF" name="chkUF" class="fldGeneral" title="Permitir mesma UF Origem e Destino ?"></input>
    <p id="lblExcecao" name="lblExcecao" class="lblGeneral"  title="Excecao">Exc</p>  
    <input type="checkbox" id="chkExcecao" name="chkExcecao" class="fldGeneral" title="Exce��o"></input>
    <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p> 
    <input type="text" id="txtObservacao" name="txtObservacao" class="fldGeneral" title="Observa��o"></input> 
    
    <p id="lblCFOP" name="lblCFOP" class="lblGeneral">CFOP</p> 
    <select id="selCFOP" name="selCFOP" class="fldGeneral"  onchange="return cmbOnChange(this)">
    <%
		 Set rsData = Server.CreateObject("ADODB.Recordset")
		    strSQL = " SELECT 0 AS fldID, ' ' AS fldName" &_
		             " UNION ALL" &_
		             " SELECT OperacaoID AS fldID, CONVERT(VARCHAR(10), OperacaoID) + '   ' +  Operacao AS fldName " &_
                    " FROM   Operacoes WITH ( NOLOCK )" &_
                    " WHERE  ( TipoOperacaoID = 651 ) AND ( Nivel = 3 )  AND ( EstadoID = 2 ) AND (PaisID = " & nPaisEmpresaID & " )"

							
			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				
				If ( CLng(rsData.Fields("fldID").Value) = CLng(1) ) Then
					Response.Write " SELECTED "
				End If

				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>		
        </select>	
  
    <p id="lblFinalidade" name="lblFinalidade" class="lblGeneral">Finalidade</p> 
    <select id="selFinalidade" name="selFinalidade" class="fldGeneral"  onchange="return cmbOnChange(this)"> <%
		 Set rsData = Server.CreateObject("ADODB.Recordset")
		    strSQL = " SELECT 0 AS fldID, ' ' AS fldName" &_
		            " UNION ALL" &_
		            " SELECT ItemID AS fldID, ItemMasculino AS fldName " &_
		            " FROM TiposAuxiliares_Itens WITH(NOLOCK) " &_
		            " WHERE (TipoID = 418) "
    					
			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				
				If ( CLng(rsData.Fields("fldID").Value) = CLng(1) ) Then
					Response.Write " SELECTED "
				End If

				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>		
        </select>	
    <p id="lblNCM" name="lblNCM" class="lblGeneral">NCM</p> 
    <input type="text" id="txtNCM" name="txtNCM" class="fldGeneral" title="NCM"></input> 
    <p id="lblCNAE" name="lblCNAE" class="lblGeneral">CNAE</p> 
    <input type="text" id="txtCNAE" name="txtCNAE" class="fldGeneral" title="CNAE"></input> 
    
    <p id="lblProdutoID" name="lblProdutoID" class="lblGeneral">ID</p> 
    <input type="text" id="txtProdutoID" name="txtProdutoID" class="fldGeneral" title="ProdutoID"></input>
    <!--<p id="lblFabricanteID" name="lblFabricanteID" class="lblGeneral">Fabricante</p>
    <select id="selFabricante" name="selFabricante" class="fldGeneral"  onchange="return cmbOnChange(this)"></select>
    <p id="lblMarca" name="lblMarca" class="lblGeneral">Marca</p>
    <select id="selMarca" name="selMarca" class="fldGeneral"  onchange="return cmbOnChange(this)"></select>-->
    <p id="lblDescricao" name="lblDescricao" class="lblGeneral">Produto</p> 
    <input type="text" id="txtDescricao" name="txtDescricao" class="fldGeneral" title="Descri��o"></input>
    <input type="button" id="btnPesquisar" name="btnPesquisar" value="P" title = "Preencher" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <p id="lblComboProdutos" name="lblComboProdutos" class="lblGeneral">Produtos</p> 
    <select id="selProdutoID" name="selProdutoID" class="fldGeneral" onchange="return cmbOnChange(this)" MULTIPLE></select>
    
    <p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">ID</p> 
    <input type="text" id="txtPessoaID" name="txtPessoaID" class="fldGeneral"></input>
    <p id="lblDocFederal" name="lblDocFederal" class="lblGeneral">Documento Federal</p>
    <input type="text" id="txtDocFederal" name="txtDocFederal" class="fldGeneral"></input>
    <p id="lblFantasia" name="lblFantasia" class="lblGeneral">Pessoa</p>
    <input type="text" id="txtFantasia" name="txtFantasia" class="fldGeneral"></input>
    <p id="lblPessoas" name="lblPessoas" class="lblGeneral">Pessoas</p> 
    <select id="selPessoas" name="selPessoas" class="fldGeneral" onchange="return cmbOnChange(this)" MULTIPLE></select>
       
    <!--<textarea id="txtDicas" name="txtDicas" class="fldGeneral"></textarea> -->
    <div id="divDicas" name="divDicas" class="divGeneral" style="border-width:2px;border-style:solid;border-color:Gray;overflow:auto;font-Family:tahoma;font-Size:12px"></div>

    <p id="lblVigInicio" name="lblVigInicio" class="lblGeneral">Vig�ncia Inicio</p> 
    <input type="text" id="txtVigInicio" name="txtVigInicio" class="fldGeneral"></input>
    <p id="lblVigFim" name="lblVigFim" class="lblGeneral">Vig�ncia Fim</p> 
    <input type="text" id="txtVigFim" name="txtVigFim" class="fldGeneral"></input>
    <input type="button" id="btnIncluir" name="btnIncluir" value="Incluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnVoltar" name="btnVoltar" value="Voltar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">    
    <p id="lblRodapeVigFim" name="lblRodapeVigFim" class="lblGeneral">Vig�ncia Fim</p> 
    <input type="text" id="txtRodapeVigFim" name="txtRodapeVigFim" class="fldGeneral"></input>
    <input type="button" id="btnFinalzar" name="btnFinalzar" value="Finalizar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnDeletar" name="btnDeletar" value="Deletar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">  
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    <input type="button" id="btnAvancar" name="btnAvancar" value="Avan�ar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">

    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
</body>

</html>
