/********************************************************************
modalrelacoes.js

Library javascript para o modalrelacoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport("dsoPesq");
var dsoRelacoes = new CDatatransport("dsoRelacoes");
var dsoGravaRelacoes = new CDatatransport("dsoGravaRelacoes");
var dsoImpostos = new CDatatransport("dsoImpostos");
var dsoCombosElem = new CDatatransport("dsoCombosElem");
var dsoCombosGru = new CDatatransport("dsoCombosGru");
var dsoComboMensagem = new CDatatransport("dsoComboMensagem");
var dsoDicas = new CDatatransport("dsoDicas");
var glb_RegraFiscalID = null;
var glb_RegraFiscalMaeID = null;
var glb_nNivel = null;
var glb_EstadoID = null;
var nBtnsWidth = 72;
var glb_CounterCmbsStatics = 0;
var glb_changeInterface = false;

// FINAL DE VARIAVEIS GLOBAIS ***************************************
     
// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() 
{
    window_onload_1stPart();

    glb_RegraFiscalID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['RegraFiscalID'].value");
    glb_RegraFiscalMaeID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['RegraFiscalMaeID'].value");
    glb_EstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");
    glb_nNivel = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Nivel'].value");

    selGrupo.disabled = true;
    btnAtribuir.disabled = true;
    
    // ajusta o body do html
    with (modalrelacoesBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    // configuracao inicial do html
    setupPage();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 52;
    modalFrame.style.left = 0;

    modalFrame = getFrameInHtmlTop('frameModal');

    comboMensagem();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() 
{

    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Regras Fiscais');
    
    // btnOK nao e usado nesta modal
    with (btnOK) {
        disabled = true;
        style.visibility = 'hidden';
    }
    
    // btnCanc nao e usado nesta modal
    with (btnCanc) {
        style.visibility = 'hidden';
    }
   // texto da secao01
    secText('Regra Fiscal ' + glb_RegraFiscalID + ' - Rela��es (4/5)', 1);
    loadDataAndTreatInterface();

    carregaVisao();

    if (glb_EstadoID == 1)
        chkInc.checked = false;
    else 
    {
        chkInc.checked = false;
        chkInc.disabled = true;
        btnGravar.disabled = true;
        btnDeletar.disabled = true;
    }

    chkInc.onclick = chkInc_onclick;

    divDicas.style.visibility = 'hidden';
    //divDicas.readOnly = true;


    selElementoID.onchange = onchange_Elemento;

    txtObservacao.maxLength = 60;
    
    CarregaCombo(false);

    startGridInterface(fg);
    startGridInterface(fgMembrosElem);
    startGridInterface(fgImpostos);

    if (chkInc.checked) 
    {
        fillGridImpostos();
    }
}

function loadDataAndTreatInterface() 
{

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var x_gap, y_gap;

    topFree = parseInt(divMod01.style.height, 10) + ELEM_GAP;

   // Botao, label, CheckBox

    btnVoltar.style.top = parseInt(btnOK.currentStyle.top, 10) + 8;
    btnVoltar.style.left = 10;
    btnVoltar.style.width = FONT_WIDTH * 7 + 20;

    btnGravar.style.top = parseInt(btnOK.currentStyle.top, 10) + 8;
    btnGravar.style.left = parseInt(btnCanc.style.left, 10) + ELEM_GAP - 130;
    btnGravar.style.width = FONT_WIDTH * 7 + 20;

    btnDeletar.style.top = parseInt(btnOK.currentStyle.top, 10) + 8;
    btnDeletar.style.left = parseInt(btnOK.style.left, 10) + ELEM_GAP + 40;
    btnDeletar.style.width = FONT_WIDTH * 7 + 20;


    btnAvancar.style.top = parseInt(btnOK.currentStyle.top, 10) + 8;
    btnAvancar.style.left = parseInt(btnOK.style.left, 10) + parseInt(btnCanc.style.left, 10) + ELEM_GAP - 20;
    btnAvancar.style.width = FONT_WIDTH * 7 + 20;

    btnListar.style.width = FONT_WIDTH * 7 + 20;

    adjustElementsInForm([['lblDicas', 'chkDicas', 3, 1, 0, 25],
                          ['lblVisao', 'selVisao', 5, 1, -5],
                          ['lblInc', 'chkInc', 3, 1],
                          ['lblElemento', 'selElementoID', 10, 1],
                          ['lblGrupo', 'selGrupo', 6, 1, ((parseInt(btnListar.currentStyle.width, 10) + 5) * 2 - 22)],
                          ['lblObservacao', 'txtObservacao', 53, 1]], null, null, true);

    btnListar.style.top = parseInt(chkDicas.currentStyle.top, 10);
    btnListar.style.left = parseInt(selElementoID.style.left, 10) + parseInt(selElementoID.style.width, 10) + ELEM_GAP;

    btnRelacionar.style.top = parseInt(chkDicas.currentStyle.top, 10);
    btnRelacionar.style.left = parseInt(btnOK.style.left, 10) + parseInt(btnCanc.style.left, 10) + ELEM_GAP - 20;
    btnRelacionar.style.width = FONT_WIDTH * 7 + 20;

    btnAtribuir.style.top = parseInt(chkDicas.currentStyle.top, 10);
    btnAtribuir.style.left = parseInt(selGrupo.style.left, 10) + parseInt(selGrupo.style.width, 10)+ 5 + ELEM_GAP - 48;
    btnAtribuir.style.width = FONT_WIDTH * 7 + 20;

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + parseInt(lblElemento.style.top) + 24;
        width = (40 + (2 * 3)) * FONT_WIDTH + 9 + 284;
        height = 414;
    }
    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10) + 40 + 9 + 265;
        height = 414;
    }

    // ajusta o divFGImpostos
    with (divFGImpostos.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ((40 + (2 * 3)) * FONT_WIDTH + 90) - 120;
        top = parseInt(divMod01.style.height, 10) + parseInt(lblElemento.style.top) + 24;
        width = (40 + (2 * 3)) * FONT_WIDTH + 9 + 148;
        height = 414;
    }
    with (fgImpostos.style) {
        left = 0;
        top = 0;
        width = parseInt(divFGImpostos.style.width, 10) + 40 + 9 + 75;
        height = 414;
    }

    // ajusta o divFGMembrosElem
    with (divFGMembrosElem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = 299;
        width = (40 + (2 * 3)) * FONT_WIDTH + 9 + 30;
        height = 195;
        visibility = 'hidden';
    }
    with (fgMembrosElem.style) {
        left = 0;
        top = 0;
        width = parseInt(divFGMembrosElem.style.width, 10) + 40 + 9 + 519;
        height = 195;
    }

    with (divDicas.style)
    {
        backgroundColor = 'transparent';
        top = parseInt(divFG.style.top, 10);
        left = parseInt(divFG.style.left, 10);
        width = parseInt(fgMembrosElem.style.width, 10);
        height = parseInt(fgMembrosElem.style.height, 10) * 2 + 25;
    }
    
    chkDicas.onclick = mostraEscondeDicas;            

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;

    dicas();
}
function comboMensagem()
{
    var sWhere = '';
    var sSql = '';

    if (glb_RegraFiscalMaeID != null)
        sWhere = 'WHERE a.RegraFiscalID IN (' + glb_RegraFiscalID + ', ' + glb_RegraFiscalMaeID + ')';
    else
        sWhere = 'WHERE a.RegraFiscalID IN (' + glb_RegraFiscalID + ')';

    setConnection(dsoComboMensagem);

    sSql = 'SELECT DISTINCT a.RegMensagemID, dbo.fn_RegraFiscal_Dados(a.RegMensagemID, 1) AS Mensagem ' +
                'FROM RegrasFiscais_Mensagens a WITH(NOLOCK) ' +
                sWhere + ' AND (a.Condicional = 1)';

    if (glb_RegraFiscalMaeID != null) 
    {
        sSql += ' UNION ALL SELECT DISTINCT a.RegMensagemID, dbo.fn_RegraFiscal_Dados(a.RegMensagemID, 1) AS Mensagem ' +
                'FROM RegrasFiscais_Mensagens a WITH(NOLOCK) ' +
                    'INNER JOIN RegrasFiscais b WITH(NOLOCK) ON (b.RegraFiscalID = a.RegraFiscalID) ' +
                'WHERE (a.RegraFiscalID <> ' + glb_RegraFiscalID + ') AND (b.RegraFiscalMaeID = ' + glb_RegraFiscalMaeID + ') ' +
                    'AND ((SELECT aa.RegraFiscalMaeID FROM RegrasFiscais aa WITH(NOLOCK) WHERE (aa.RegraFiscalID = ' + glb_RegraFiscalMaeID + ')) IS NOT NULL) ' +
                    ' AND (a.Condicional = 1)';

        sSql += ' UNION ALL SELECT DISTINCT a.RegMensagemID, dbo.fn_RegraFiscal_Dados(a.RegMensagemID, 1) AS Mensagem ' +
                'FROM RegrasFiscais_Mensagens a WITH(NOLOCK) ' +
                'WHERE (a.RegraFiscalID IN (SELECT aa.RegraFiscalMaeID FROM RegrasFiscais aa WITH(NOLOCK) WHERE aa.RegraFiscalID = ' + glb_RegraFiscalMaeID + ')) ' +
                ' AND (a.Condicional = 1)';
    }
    
    dsoComboMensagem.SQL = sSql;
    dsoComboMensagem.ondatasetcomplete = ComboMensagem_DSC;
    dsoComboMensagem.Refresh();
}

function ComboMensagem_DSC()
{
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
}
/********************************************************************
Preenche o grid de relacoes
********************************************************************/
function fillGridRelacoes_Listagem() 
{
    fgMembrosElem.Rows = 0;
    abreGrid();

    var sWhere = '';
    var sWhereCount = '';
    var sSQL = '';
    var nGrupo = 0;
    var nImpostoID = 0;

    lockControlsInModalWin(true);

    if (selGrupo.selectedIndex != -1)
        nGrupo = selGrupo.value;

    if (selImposto.selectedIndex != -1)
        nImpostoID = selImposto.value;

    if (selVisao.value == 0)
        sWhere = 'WHERE (f.RegraFiscalID = ' + glb_RegraFiscalID + ') ';
    else if (selVisao.value == 1)
        sWhere = 'WHERE ((f.RegraFiscalID = ' + glb_RegraFiscalID + ') OR ((f.Nivel <> ' + glb_nNivel + ') AND (f.Nivel - ' + glb_nNivel + ' BETWEEN -1 AND 1))) ';


    sSQL = 'SELECT 0 AS Det, 0 AS OK, a.RegraFiscalID, d.ItemMasculino AS Elemento, a.Grupo, ' +
                    '(SELECT COUNT(1) ' + 
                        'FROM RegrasFiscais_Elementos aa WITH(NOLOCK) ' +
                            'INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) bb ON (bb.RegraFiscalID = aa.RegraFiscalID) ' +
                            'INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') cc ON (cc.AgrupadorID = bb.AgrupadorID) ' +
                        'WHERE (aa.ElementoID = a.ElementoID) AND (aa.Grupo = a.Grupo)) AS Quant, ' +
                    'dbo.fn_RegraFiscal_Dados(b.RegImpostoID, 2) AS Imposto, a.Observacao, a.RegMensagemID, a.RegRelacaoID, a.ElementoID ' +
	            'FROM RegrasFiscais_Relacoes a WITH(NOLOCK) ' +
		            'INNER JOIN RegrasFiscais_Impostos b WITH(NOLOCK) ON (b.RegImpostoID = a.RegImpostoID) ' +
		            'INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (d.ItemiD = a.ElementoID) ' +
		            'INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) e ON (e.RegraFiscalID = a.RegraFiscalID) ' +
		            'INNER JOIN RegrasFiscais f WITH(NOLOCK) ON (f.RegraFiscalID = a.RegraFiscalID) ' +
		            'INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') g ON (g.AgrupadorID = e.AgrupadorID) ' +
		            'INNER JOIN RegrasFiscais_Impostos h WITH(NOLOCK) ON (h.RegImpostoID = a.RegImpostoID) ' +
		            'INNER JOIN Conceitos i WITH(NOLOCK) ON (i.ConceitoID = h.ImpostoID) ' +
	            sWhere + //'WHERE (e.AgrupadorID = dbo.fn_RegraFiscal_Agrupador(' + glb_RegraFiscalID + ')) AND (f.Nivel <= ' + glb_nNivel + ') ' +
	                'AND ((0 = ' + selElementoID.value + ') OR (a.ElementoID = ' + selElementoID.value + ')) ' +
	                'AND ((0 = ' + nGrupo + ') OR (a.Grupo = ' + nGrupo + ')) ' +
	                'AND ((0 = ' + nImpostoID + ') OR (b.ImpostoID = ' + nImpostoID + ')) ' +
	               'GROUP BY a.RegraFiscalID, d.ItemMasculino, a.Grupo, b.RegImpostoID, a.Observacao, a.RegMensagemID, a.RegRelacaoID, a.ElementoID, i.Ordem ' +
	                'ORDER BY a.RegraFiscalID, d.ItemMasculino, a.Grupo, i.Ordem ';
        
    dsoRelacoes.SQL = sSQL;
    dsoRelacoes.ondatasetcomplete = eval(fillGridRelacoes_Listagem_DSC);
    dsoRelacoes.refresh();
}

function fillGridRelacoes_Listagem_DSC() {
    startGridInterface(fg);
    headerGrid(fg, ['RegraID',
                    'Elemento',
                    'Grupo',
                    'Quant',
                    'Imposto',
                    'Det',
                    'OK',
                    'Mensagem',
                    'Observa��o',
                    'RegRelacaoID',
                    'ElementoID'], [9, 10]);

    fillGridMask(fg, dsoRelacoes, ['RegraFiscalID*', 'Elemento*', 'Grupo*', 'Quant*', 'Imposto*', 'Det', 'OK', 'RegMensagemID', 'Observacao', 'RegRelacaoID*', 'ElementoID*'],
                                   ['', '', '', '', '', '', '', '', '', '']);

    insertcomboData(fg, getColIndexByColKey(fg, 'RegMensagemID'), dsoComboMensagem, 'Mensagem', 'RegMensagemID');

    // Linha de Totais
    gridHasTotalLine(fg, '', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(fg, 'RegraFiscalID*'), '######', 'C']]);

    fg.FrozenCols = 7;

    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    GridLinhaTotal(fg);

    //coloca o tipo das colunas
    with (fg) {
        ColDataType(5) = 11;
        ColDataType(6) = 11;
    }

    paintReadOnlyRows();

    fg.Editable = true;
    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);
}

function fillGridRelacoes() {
    fgMembrosElem.Rows = 0;
    abreGrid();

    var sWhere = '';
    var sSQL = '';

    lockControlsInModalWin(true);

    if (selVisao.value == 0)
        sWhere = 'WHERE (a.RegraFiscalID = ' + glb_RegraFiscalID + ') ';
    else if (selVisao.value == 1)
        sWhere = 'WHERE ((a.RegraFiscalID = ' + glb_RegraFiscalID + ') OR ((a.Nivel <> ' + glb_nNivel + ') AND (a.Nivel - ' + glb_nNivel + ' BETWEEN -1 AND 1))) ';

    sSQL = 'SELECT 0 AS Det, 0 AS OK, b.RegraFiscalID, c.ItemMasculino AS Elemento, b.Grupo, b.ElementoID, COUNT(1) AS Quant, a.Nivel ' +
	                    'FROM RegrasFiscais a WITH(NOLOCK) ' +
	                        'INNER JOIN RegrasFiscais_Elementos b WITH(NOLOCK) ON (b.RegraFiscalID = a.RegraFiscalID) ' +
	                        'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.ItemID = b.ElementoID) ' +
	                        'INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) d ON (d.RegraFiscalID = a.RegraFiscalID) ' +
	                        'INNER JOIN RegrasFiscais_Mapeamento e WITH(NOLOCK) ON (e.ElementoID = b.ElementoID) AND (e.RegraFiscalID = (SELECT TOP 1 bb.RegraFiscalMaeID FROM RegrasFiscais bb WITH(NOLOCK) WHERE bb.RegraFiscalID = d.AgrupadorID)) ' +
	                        'INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') f ON (f.AgrupadorID = d.AgrupadorID) ' +
	                    sWhere +//'WHERE (d.AgrupadorID = dbo.fn_RegraFiscal_Agrupador(' + glb_RegraFiscalID + ')) AND (a.Nivel <= ' + glb_nNivel + ') ' +
	                        'AND ((0 = ' + selElementoID.value + ') OR (b.ElementoID = ' + selElementoID.value + ')) AND ' +
	                        '(1 = (CASE WHEN (' + glb_nNivel + ' = 0) THEN e.RelacaoNivel0 ELSE e.RelacaoNivel12 END)) ' +
	                    'GROUP BY b.RegraFiscalID, c.ItemMasculino, b.Grupo, b.ElementoID, a.Nivel ' +
	                    'ORDER BY b.RegraFiscalID, c.ItemMasculino, b.Grupo';

    dsoRelacoes.SQL = sSQL;
    dsoRelacoes.ondatasetcomplete = eval(fillGridRelacoes_DSC);
    dsoRelacoes.refresh();
}

function fillGridRelacoes_DSC() 
{
    startGridInterface(fg);
    headerGrid(fg, ['RegraID',
                    'Elemento',
                    'Grupo',
                    'Quant',
                    'Det',
                    'OK',
                    'ElementoID',
                    'Nivel'], [6, 7]);

    fillGridMask(fg, dsoRelacoes, ['RegraFiscalID*', 'Elemento*', 'Grupo*', 'Quant*', 'Det', 'OK', 'ElementoID*', 'Nivel*'],
                                   ['', '', '', '', '', '', '', '']);

    // Linha de Totais
    gridHasTotalLine(fg, '', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(fg, 'RegraFiscalID*'), '######', 'C']]);

    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    GridLinhaTotal(fg);

    //coloca o tipo das colunas
    with (fg) {
        ColDataType(4) = 11;
        ColDataType(5) = 11;
    }

    fg.Editable = true;
    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.Redraw = 2;


    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);

    if (chkInc.checked)
        fillGridImpostos();
}

function fillGridImpostos() 
{
    var sWhere = '';
    var sSQL = '';

    lockControlsInModalWin(true);

    if ((chkInc.checked) || (selVisao.value == 1))
        sWhere = 'WHERE ((a.RegraFiscalID = ' + glb_RegraFiscalID + ') OR ((a.Nivel <> ' + glb_nNivel + ') AND (a.Nivel - ' + glb_nNivel + ' BETWEEN -1 AND 1))) ';
    else if (selVisao.value == 0)
        sWhere = 'WHERE (a.RegraFiscalID = ' + glb_RegraFiscalID + ') ';
    
    sSQL = 'SELECT a.RegraFiscalID, c.Imposto, e.ItemAbreviado AS CodigoTributacao, b.BaseCalculo, b.Aliquota, b.IVAST, b.BaseCalculoICMS, b.RegImpostoID, a.Nivel, b.Observacao ' +
	                'FROM RegrasFiscais a WITH(NOLOCK) ' +
	                    'INNER JOIN RegrasFiscais_Impostos b WITH(NOLOCK) ON (b.RegraFiscalID = a.RegraFiscalID) ' +
		                'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ImpostoID) ' +
                        'INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) d ON (d.RegraFiscalID = a.RegraFiscalID) ' +
		                'LEFT OUTER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (e.ItemID = b.CodigoTributacaoID) ' +
		                'INNER JOIN RegrasFiscais_Mapeamento f WITH(NOLOCK) ON (f.ImpostoID = b.ImpostoID) AND (f.RegraFiscalID = (SELECT TOP 1 bb.RegraFiscalMaeID FROM RegrasFiscais bb WITH(NOLOCK) WHERE bb.RegraFiscalID = d.AgrupadorID)) ' +
	                    'INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') g ON (g.AgrupadorID = d.AgrupadorID) ' +
	                sWhere + //'WHERE (d.AgrupadorID = dbo.fn_RegraFiscal_Agrupador(' + glb_RegraFiscalID + ')) AND (a.Nivel <= ' + glb_nNivel + ') ' +
	                        'AND (1 = (CASE WHEN (' + glb_nNivel + ' = 0) THEN f.RelacaoNivel0 ELSE f.RelacaoNivel12 END)) '+
	                        'ORDER BY a.RegraFiscalID, c.Imposto, CodigoTributacao, c.Ordem ';
/*
    if (glb_RegraFiscalMaeID != null)
    {
        sSQL += ' UNION ALL SELECT a.RegraFiscalID, c.Imposto, b.CodigoTributacao, b.BaseCalculo, b.Aliquota, b.IVAST, b.BaseCalculoICMS, b.RegImpostoID, a.Nivel, b.Observacao ' +
	                'FROM RegrasFiscais a WITH(NOLOCK) ' +
	                    'INNER JOIN RegrasFiscais_Impostos b WITH(NOLOCK) ON (b.RegraFiscalID = a.RegraFiscalID) ' +
		                'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ImpostoID) ' +
	                'WHERE (a.RegraFiscalID <> ' + glb_RegraFiscalID + ') AND (a.RegraFiscalMaeID = ' + glb_RegraFiscalMaeID + ') ' +
	                'AND ((SELECT aa.RegraFiscalMaeID FROM RegrasFiscais aa WITH(NOLOCK) WHERE (aa.RegraFiscalID = ' + glb_RegraFiscalMaeID + ')) IS NOT NULL)';

        sSQL += ' UNION ALL SELECT a.RegraFiscalID, c.Imposto, b.CodigoTributacao, b.BaseCalculo, b.Aliquota, b.IVAST, b.BaseCalculoICMS, b.RegImpostoID, a.Nivel, b.Observacao ' +
	                'FROM RegrasFiscais a WITH(NOLOCK) ' +
	                    'INNER JOIN RegrasFiscais_Impostos b WITH(NOLOCK) ON (b.RegraFiscalID = a.RegraFiscalID) ' +
		                'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ImpostoID) ' +
	                'WHERE (a.RegraFiscalID IN (SELECT aa.RegraFiscalMaeID FROM RegrasFiscais aa WITH(NOLOCK) WHERE aa.RegraFiscalID = ' + glb_RegraFiscalMaeID + '))';
    }*/

    dsoImpostos.SQL = sSQL;
    dsoImpostos.ondatasetcomplete = eval(fillGridImpostos_DSC);
    dsoImpostos.refresh();
}

function fillGridImpostos_DSC() {

    headerGrid(fgImpostos, ['RegraID',
                    'Imposto',
                    'CT',
                    'Base C�lculo',
                    'Aliquota',
                    'IVA-ST',
                    'BC ICMS',
                    'Observa��o',
                    'RegImpostoID',
                    'Nivel'], [8, 9]);

    fillGridMask(fgImpostos, dsoImpostos, ['RegraFiscalID*', 'Imposto*', 'CodigoTributacao*', 'BaseCalculo*', 'Aliquota*', 'IVAST*', 'BaseCalculoICMS*',
                                            'Observacao*', 'RegImpostoID*', 'Nivel*'],
                                             ['', '', '', '', '', '', '', '', '', '']);


    fgImpostos.Editable = false;

    fgImpostos.MergeCells = 4;
    fgImpostos.MergeCol(0) = true;
    fgImpostos.MergeCol(1) = true;
    fgImpostos.MergeCol(2) = true;

    gridHasTotalLine(fgImpostos, '', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(fgImpostos, 'RegraFiscalID*'), '######', 'C']]);

    fgImpostos.Redraw = 0;
    fgImpostos.AutoSizeMode = 0;
    fgImpostos.AutoSize(0, fgImpostos.Cols - 1, false, 0);
    fgImpostos.Redraw = 2;



    fgImpostos.ExplorerBar = 5;

    lockControlsInModalWin(false);
}

function fillGridRelacoes_Detalhes() {
    lockControlsInModalWin(true);

    var sElementos = new Array();
    var sGrupos = new Array();
    var nRegrasID = new Array();
    var nRegraID;
    var nElementoID = 0;
    var sGrupo;
    var y;
    var j;
    var sWhere = '';
    var sGrupo = '';
    var sGrupoNull = new Array();
    var bDet;

    for (var i = 1; i < fg.Rows; i++) 
    {
        bDet = fg.textMatrix(i, getColIndexByColKey(fg, 'Det'));

        //Verifica se o ok do grid(fg) esta cliacado.
        if (bDet != 0) 
        {
            nElementoID = fg.textMatrix(i, getColIndexByColKey(fg, 'ElementoID*'));

            if (fg.textMatrix(i, getColIndexByColKey(fg, 'Grupo*')) != '')
                sGrupo = '(b.Grupo = ' + fg.textMatrix(i, getColIndexByColKey(fg, 'Grupo*')) + ')';
            else
                sGrupo = '(b.Grupo IS NULL)';

            if (sWhere == '')
                sWhere += 'WHERE (((b.ElementoID = ' + nElementoID + ') AND ' + sGrupo + ')';
            else
                sWhere += ' OR ((b.ElementoID = ' + nElementoID + ') AND ' + sGrupo + ')';
        }
    }

    if (sWhere != '') 
    {
        sWhere += ') AND (a.RegraFiscalID IN ' +
                        '(SELECT aa.RegraFiscalID FROM dbo.fn_RegraFiscal_Niveis_tbl(NULL) aa ' +
                            'INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') bb ON (aa.AgrupadorID = bb.AgrupadorID)))';

        /*
        if (selVisao.value == 0)
            sWhere += ' AND (a.RegraFiscalID = ' + glb_RegraFiscalID + ') ';
        else if (selVisao.value == 1)
            sWhere += ' AND ((a.RegraFiscalID = ' + glb_RegraFiscalID + ') OR ((a.Nivel <> ' + glb_nNivel + ') AND (a.Nivel - ' + glb_nNivel + ' BETWEEN -1 AND 1))) ';
        */
    }
/*            //Pega RegraID da linha.
            nRegraID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegraFiscalID*'));

            //Verifica se j� existem regras no array, se n existe, inclui.
            if (nRegrasID.length == 0)
                nRegrasID[nRegrasID.length] = nRegraID;
            else {
                y = 0;

                //Verifica se a regra da linha existe no array de regras:
                //Se sim, usa o indice dela para construir os elementos e grupos de acordo coma regra;
                //Se n�o, inclui a regra no arry.
                while (y < nRegrasID.length) {
                    if (nRegrasID[y] == nRegraID)
                        break;

                    y++;

                    if (y >= nRegrasID.length) {
                        nRegrasID[nRegrasID.length] = nRegraID;
                        break;
                    }
                }
            }

            if ((nElementoID != fg.textMatrix(i, getColIndexByColKey(fg, 'ElementoID*'))) || (nElementoID != 0)) {
                nElementoID = fg.textMatrix(i, getColIndexByColKey(fg, 'ElementoID*'));
                if (fg.textMatrix(i, getColIndexByColKey(fg, 'Grupo*')) != '')
                    sGrupo = '(b.Grupo = ' + fg.textMatrix(i, getColIndexByColKey(fg, 'Grupo*')) + ')';
                else
                    sGrupo = '(b.Grupo IS NULL)';

                //Se o array estiver vazio, inclui o primeiro de acordo com a regra da linha.
                if (sElementos.length == 0)
                    sElementos[sElementos.length] = '((a.RegraFiscalID = ' + nRegraID + ') AND (((b.ElementoID = ' + nElementoID + ') AND ' + sGrupo + ')';
                //Verifica se existe uma posi��o do Array de elementos com essa regra:
                //Se n�o, cria um elemento para nova regro com OR no come�o;
                else if (y > (sElementos.length - 1)) {
                    sElementos[(sElementos.length - 1)] += '))';
                    sElementos[sElementos.length] = ' OR ((a.RegraFiscalID = ' + nRegraID + ') AND (((b.ElementoID = ' + nElementoID + ')  AND ' + sGrupo + ')';
                }
                //Se sim, adiciona o elemento na posi��o do Array.
                else
                    sElementos[y] += ' OR ((b.ElementoID = ' + nElementoID + ')  AND ' + sGrupo + ')';
            }
        }
    }

    y = 0;

    if (sElementos.length == 0) {
        fgMembrosElem.Rows = 1;
        abreGrid();
        lockControlsInModalWin(false);
        return null;
    }

    sWhere = 'WHERE ';

    y = 0;

    //adicionar elementos e grupos com suas regras.
    while (y < sElementos.length) {
        sWhere += sElementos[y];  //+ sGrupos[j];

        y++;
    }

    sWhere += '))';*/

    if (sWhere == '')
    {
        fgMembrosElem.Rows = 1;
        abreGrid();
        lockControlsInModalWin(false);
        return null;
    }    

    var sSQL = 'SELECT a.RegraFiscalID, c.ItemMasculino AS Elemento, b.Grupo, ' +
	                '(CASE b.ElementoID ' +
		                'WHEN 970 THEN CONVERT(VARCHAR(10), d.OperacaoID) + \' \' + d.Operacao ' +
                        'WHEN 971 THEN e.ItemMasculino ' +
                        'WHEN 972 THEN f.CodigoLocalidade2 + \'/\' +  g.CodigoLocalidade2 ' +
                        'WHEN 973 THEN b.NCM ' +
                        'WHEN 974 THEN b.CNAE ' +
                        'WHEN 975 THEN h.Conceito ' +
                        'WHEN 976 THEN i.Fantasia ' +
	                'END ) AS Valor1, ' +
	                'b.Excecao, b.Observacao, b.Deletado, Vigencia.dtVigenciaInicio, Vigencia.dtVigenciaFim, 0 AS OK, b.ProdutoID, '+
	                'b.RegElementoID, a.Nivel, b.ElementoID, Vigencia.RegraFiscalFimID ' +
	            'FROM RegrasFiscais a WITH(NOLOCK) ' +
		            'INNER JOIN RegrasFiscais_Elementos b WITH(NOLOCK) ON (b.RegraFiscalID = a.RegraFiscalID) ' +
	                'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.ItemID = b.ElementoID) ' +
	                'LEFT OUTER JOIN Operacoes d WITH(NOLOCK) ON (d.OperacaoID = b.CFOPID) ' +
	                'LEFT OUTER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (e.ItemID = b.FinalidadeID) ' +
	                'LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON (f.LocalidadeID = b.UFOrigemID) ' +
	                'LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON (g.LocalidadeID = b.UFDestinoID) ' +
	                'LEFT OUTER JOIN Conceitos h WITH(NOLOCK) ON (h.ConceitoID = b.ProdutoID) ' +
	                'LEFT OUTER JOIN Pessoas i WITH(NOLOCK) ON (i.PessoaID = b.PessoaID) ' +
	                'INNER JOIN dbo.fn_RegraFiscalElemento_Vigencia_tbl(NULL) Vigencia ON ((Vigencia.RegElementoID = b.RegElementoID) OR (Vigencia.RegraFiscalFimID = b.RegElementoID)) ' +
	            sWhere +
	            ' ORDER BY c.ItemMasculino, a.RegraFiscalID, b.Grupo, Valor1, b.Excecao';

    dsoRelacoes.SQL = sSQL;
    dsoRelacoes.ondatasetcomplete = eval(fillGridRelacoes_Detalhes_DSC);
    dsoRelacoes.refresh();
}

function fillGridRelacoes_Detalhes_DSC() {
    var aHiddenCols = new Array();

    if (chkInc.checked)
        aHiddenCols = [11, 12, 13];
    else
        aHiddenCols = [10, 11, 12, 13];

    headerGrid(fgMembrosElem, ['Elemento',
                    'RegraID',
                    'Grupo',
                    'Valor',
                    'Exce��o',
                    'Observa��o',
                    'Deletado',
                    'Vig�ncia Inicio',
                    'Vig�ncia Fim',
                    'RegraID',
                    'OK',
                    'RegElementoID',
                    'Nivel',
                    'ElementoID'], aHiddenCols);

    fillGridMask(fgMembrosElem, dsoRelacoes, ['Elemento*', 'RegraFiscalID*', 'Grupo*', 'Valor1*', 'Excecao*', 'Observacao*', 'Deletado*', 
                                                'dtVigenciaInicio*', 'dtVigenciaFim*', 'RegraFiscalFimID*', 'OK', 'RegElementoID*', 'Nivel*', 'ElementoID*'],
                                   ['', '', '', '', '', '', '', '', '', '', '']);

    // Linha de Totais
    gridHasTotalLine(fgMembrosElem, '', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(fgMembrosElem, 'RegraFiscalID*'), '######', 'C']]);

    // Merge de Colunas
    fgMembrosElem.MergeCells = 2;
    fgMembrosElem.MergeCol(-1) = true;
    GridLinhaTotal(fgMembrosElem);
    //coloca o tipo das colunas
    with (fgMembrosElem) {
        ColDataType(getColIndexByColKey(fgMembrosElem, 'OK')) = 11;
        ColDataType(getColIndexByColKey(fgMembrosElem, 'Deletado*')) = 11;
    }

    fgMembrosElem.Editable = chkInc.checked;
    fgMembrosElem.Redraw = 0;
    fgMembrosElem.AutoSizeMode = 0;
    fgMembrosElem.AutoSize(0, fgMembrosElem.Cols - 1, false, 0);
    fgMembrosElem.Redraw = 2;
    fgMembrosElem.ExplorerBar = 5;

    abreGrid();

    lockControlsInModalWin(false);
}


/********************************************************************
Fun��o Para Gravar a Linha do grid
********************************************************************/
function GravaRelacoes(nTipoID) 
{
    lockControlsInModalWin(true);

    var nMensagemID;
    var sObservacao;
    var strPars = new String();
    var nElementoID;
    var nGrupo;
    var nRegImpostoID;
    var nRegRelacaoID;
    var nRegElementoID;
    var nRegraFiscalID;
    var nRegraFiscalImpostoID;
    var nNivel;
    var nNivelImposto;
    var bOK;
    var sElemento = '';
    var sMensagemErro = '';
    
    strPars = '';

    //Relacionar
    if (nTipoID == 1) 
    {
        for (var i = 1; i < fg.Rows; i++) 
        {
            bOK = (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0);

            if (bOK) 
           {
                nGrupo = fg.textMatrix(i, getColIndexByColKey(fg, 'Grupo*'));
                nElementoID = fg.textMatrix(i, getColIndexByColKey(fg, 'ElementoID*'));
                nRegraFiscalID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegraFiscalID*'));
                nNivel = fg.textMatrix(i, getColIndexByColKey(fg, 'Nivel*'));

                if (nGrupo != '') 
                {
                    if (strPars == '')
                        strPars += '?nElementoID=' + escape(nElementoID);
                    else
                        strPars += '&nElementoID=' + escape(nElementoID);

                    strPars += '&sGrupo=' + escape(nGrupo);
                    strPars += '&nRegraFiscalID=' + escape(nRegraFiscalID);
                    strPars += '&nNivel=' + escape(nNivel);

                    strPars += '&nTipoID=' + escape(nTipoID);
                }
                else 
                {
                    strPars = '';

                    window.top.overflyGen.Alert('Existem linhas selecionadas sem grupo cadastrado.');

                    break;
                }
            }
        }

        for (var i = 1; i < fgMembrosElem.Rows; i++) 
        {
            bOK = (fgMembrosElem.ValueMatrix(i, getColIndexByColKey(fgMembrosElem, 'OK')) != 0);

            nGrupo = fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'Grupo*'));

            if ((bOK) && (nGrupo == ''))
            {
                nElementoID = fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'ElementoID*'));
                nRegraFiscalID = fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'RegraFiscalID*'));
                nNivel = fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'Nivel*'));
                nRegElementoID = fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'RegElementoID*'));

                if (strPars == '')
                    strPars += '?nElementoID=' + escape(nElementoID);
                else
                    strPars += '&nElementoID=' + escape(nElementoID);

                strPars += '&sGrupo=' + escape(nGrupo);
                strPars += '&nRegraFiscalID=' + escape(nRegraFiscalID);
                strPars += '&nNivel=' + escape(nNivel);
                strPars += '&nRegElementoID=' + escape(nRegElementoID);

                strPars += '&nTipoID=' + escape(nTipoID);
            }
        }

        if (strPars != '')
        {
            nRegImpostoID = fgImpostos.textMatrix(fgImpostos.Row, getColIndexByColKey(fgImpostos, 'RegImpostoID*'));
            nRegraFiscalImpostoID = fgImpostos.textMatrix(fgImpostos.Row, getColIndexByColKey(fgImpostos, 'RegraFiscalID*'));
            nNivelImposto = fgImpostos.textMatrix(fgImpostos.Row, getColIndexByColKey(fgImpostos, 'Nivel*'));

            strPars += '&nRegImpostoID=' + escape(nRegImpostoID);
            strPars += '&nRegraFiscalImpostoID=' + escape(nRegraFiscalImpostoID);
            strPars += '&nRegraFiscalCorrenteID=' + escape(glb_RegraFiscalID);
            strPars += '&nNivelImposto=' + escape(nNivelImposto);
            strPars += '&nNivelCorrente=' + escape(glb_nNivel);
        }
    }
   //Incluir
    else if (nTipoID == 2) 
    {
        nRegraFiscalID = '';
        
        if (selGrupo.selectedIndex != -1) 
        {
            for (var i = 1; i < fgMembrosElem.Rows; i++) 
            {
                bOK = (fgMembrosElem.ValueMatrix(i, getColIndexByColKey(fgMembrosElem, 'OK')) != 0);
                nGrupo = fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'Grupo*'));
                nRegElementoID = fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'RegElementoID*'));

                if (bOK) 
                {
                    if ((nGrupo == '') || (nGrupo == null)) 
                    {

                        if (((fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'RegraFiscalID*')) == nRegraFiscalID) || (nRegraFiscalID == '')) &&
                                ((fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'Elemento*')) == sElemento) || (sElemento == ''))) {
                            if (nRegraFiscalID == '')
                                nRegraFiscalID = fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'RegraFiscalID*'));

                            if (sElemento == '')
                                sElemento = fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'Elemento*'));

                            if (strPars == '')
                                strPars += '?nRegElementoID=' + escape(nRegElementoID);
                            else
                                strPars += '&nRegElementoID=' + escape(nRegElementoID);

                            strPars += '&nTipoID=' + escape(nTipoID);
                        }
                        else
                            sMensagemErro += 'O Elemento ' + sElemento + ' da Regra Fiscal ' + nRegraFiscalID + ' n�o ser� incluido no grupo' + nGrupo + '\n';
                    }
                    else
                        sMensagemErro += 'O Elemento ' + fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'Elemento*')) +
                            ' da Regra Fiscal ' + fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'RegraFiscalID*')) + 
                            ' j� esta no grupo ' + nGrupo + '\n';
                }
            }

            if (strPars != '')
                strPars += '&sGrupo=' + escape(selGrupo.value);
        }
        else
            sMensagemErro += 'Selecione um Grupo';
            
        if (sMensagemErro != '')
        {
            if (strPars == '')
                lockControlsInModalWin(false);

            window.top.overflyGen.Alert(sMensagemErro);
        }
    }
    // GRAVAR E DELETAR 
    else if ((nTipoID == 3) || (nTipoID == 4))
    {
        for (var i = 1; i < fg.Rows; i++) 
        {
            bOK = (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0);

            if (bOK)
            {
                nRegRelacaoID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegRelacaoID*'));
                nMensagemID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegMensagemID'));
                sObservacao = fg.textMatrix(i, getColIndexByColKey(fg, 'Observacao'));
                
                if (strPars == '')
                    strPars += '?nRegRelacaoID=' + escape(nRegRelacaoID);
                else
                    strPars += '&nRegRelacaoID=' + escape(nRegRelacaoID);

                strPars += '&nMensagemID=' + escape(nMensagemID);

                sObservacao = replaceStr(sObservacao, ',', '^');
                strPars += '&sObservacao=' + escape(sObservacao);

                strPars += '&nTipoID=' + escape(nTipoID);
            }
        }
    }

    if (strPars != '')
    {
        var _retConf = 1;
        
        if (nTipoID == 4)
            _retConf = window.top.overflyGen.Confirm('Voc� tem certeza?');

        if (_retConf == 1) 
        {
            dsoGravaRelacoes.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/serverside/gravarrelacoes.aspx' + strPars;
            dsoGravaRelacoes.ondatasetcomplete = GravaRelacoes_DSC;
            dsoGravaRelacoes.refresh();
        }
        else
            lockControlsInModalWin(false);
    }
    else 
    {
        window.top.overflyGen.Alert('Selecione algum regitro desta regra');
        lockControlsInModalWin(false);
    }
}


function GravaRelacoes_DSC() 
{
    if (!(dsoGravaRelacoes.recordset.BOF && dsoGravaRelacoes.recordset.EOF)) 
    {
        if ((dsoGravaRelacoes.recordset['Error'].value != '') && (dsoGravaRelacoes.recordset['Error'].value != null)) 
        {
            window.top.overflyGen.Alert(dsoGravaRelacoes.recordset['Error'].value);
            lockControlsInModalWin(false);
        }
        else
        {
           lockControlsInModalWin(false);
        }
    }

    limpaCampos();

    carregaComboGrupo();

    fgMembrosElem.Rows = 1;
    abreGrid();

    if (chkInc.checked)
        fillGridRelacoes();
    else
        fillGridRelacoes_Listagem();
}

function voltarModal()
{
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.visibility = 'hidden';
    
    var nRegraFiscalID = glb_RegraFiscalID;
    var strPars = new String();

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');
    strPars += '&nRegraFiscalID=' + escape(nRegraFiscalID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/modalpages/modalimpostos.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 539));
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S', null);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null);
    }
    else if (ctl.id == btnListar.id) 
    {
        if (chkInc.checked) 
        {
            fillGridRelacoes();
            fillGridImpostos();
        }
        else
            fillGridRelacoes_Listagem();
    }
    else if (ctl.id == btnGravar.id) 
    {
        if (glb_EstadoID == 1)
            GravaRelacoes(3);
        else 
        {
            window.top.overflyGen.Alert("Opera��o dispon�vel somente para Regras Fiscais no estado Cadastrado.");

            lockControlsInModalWin(false);
        }
    }
    else if (ctl.id == btnRelacionar.id) 
    {
        GravaRelacoes(1);
    }
    else if (ctl.id == btnAtribuir.id) 
    {
        GravaRelacoes(2);
    }
    else if (ctl.id == btnDeletar.id) 
    {
        if (glb_EstadoID == 1) 
        {
            GravaRelacoes(4);
        }
        else {
            window.top.overflyGen.Alert("Opera��o dispon�vel somente para Regras Fiscais no estado Cadastrado.");
        }
        lockControlsInModalWin(false);
    }
    else if (ctl.id == btnAvancar.id) 
    {
        window.top.openModalHTML(glb_RegraFiscalID, 'Regras Fiscal ' + glb_RegraFiscalID + ' - Resumo (5/5)', 'S', 11, 0);
    }
    else if (ctl.id == btnVoltar.id) {
        voltarModal();
        lockControlsInModalWin(false);
    }
}

function OcultaMostraCampos(first)
{
    fgMembrosElem.Rows = 0;
    abreGrid();

    selGrupo.disabled = true;
    btnAtribuir.disabled = true;

    if (chkInc.checked) 
    {
        selVisao.value = 0;
        selVisao.disabled = true;
    }
    else
        selVisao.disabled = false;
    
    if (!first)
        CarregaCombo(true);
    
    if (chkInc.checked) 
    {
        //Grids
        divFGImpostos.style.visibility = 'inherit';

        // ajusta o divFG
        with (divFG.style)
        {
            width = (40 + (2 * 3)) * FONT_WIDTH - 90; //+ 9 + 20;
        }
        with (fg.style)
        {
            width = parseInt(divFG.style.width, 10) + 40;  //+ 9 + 20;
        }
        
        //Campos
        lblObservacao.style.visibility = 'inherit';
        txtObservacao.style.visibility = 'inherit';
        lblImposto.style.visibility = 'hidden';
        selImposto.style.visibility = 'hidden';
        //BTNs
        btnRelacionar.style.visibility = 'inherit';
        btnAtribuir.style.visibility = 'inherit';
        btnGravar.style.visibility = 'hidden';
        btnDeletar.style.visibility = 'hidden';
        btnAvancar.style.visibility = 'hidden';
        btnVoltar.style.visibility = 'hidden';

        adjustElementsInForm([['lblDicas', 'chkDicas', 3, 1, 0, 25],
                          ['lblVisao', 'selVisao', 5, 1, -5],
                          ['lblInc', 'chkInc', 3, 1],
                          ['lblElemento', 'selElementoID', 11, 1],
                          ['lblGrupo', 'selGrupo', 6, 1, ((parseInt(btnListar.currentStyle.width, 10) + 5) * 2) - 70],
                          ['lblObservacao', 'txtObservacao', 53, 1, 80]], null, null, true);

        btnListar.style.left = parseInt(selElementoID.style.left, 10) + parseInt(selElementoID.style.width, 10) + ELEM_GAP;

        fillGridRelacoes();
    }
    else 
    {
        //Grids
        divFGImpostos.style.visibility = 'hidden';

        // ajusta o divFG
        with (divFG.style)
        {
            width = (40 + (2 * 3)) * FONT_WIDTH + 9 + 284;
        }
        with (fg.style)
        {
            width = parseInt(divFG.style.width, 10) + 40 + 9 + 265;
        }

        //Campos
        lblObservacao.style.visibility = 'hidden';
        txtObservacao.style.visibility = 'hidden';
        lblImposto.style.visibility = 'inherit';
        selImposto.style.visibility = 'inherit';
        //BTNs
        btnRelacionar.style.visibility = 'hidden';
        btnAtribuir.style.visibility = 'hidden';
        btnGravar.style.visibility = 'inherit';
        btnDeletar.style.visibility = 'inherit';
        btnAvancar.style.visibility = 'inherit';
        btnVoltar.style.visibility = 'inherit';

        adjustElementsInForm([['lblDicas', 'chkDicas', 3, 1, 0, 25],
                          ['lblVisao', 'selVisao', 5, 1, -5],
                          ['lblInc', 'chkInc', 3, 1],
                          ['lblElemento', 'selElementoID', 11, 1],
                          ['lblGrupo', 'selGrupo', 6, 1],
                          ['lblImposto', 'selImposto', 10, 1],
                          ['lblObservacao', 'txtObservacao', 53, 1]], null, null, true);

        btnListar.style.left = parseInt(btnOK.style.left, 10) + parseInt(btnCanc.style.left, 10) + ELEM_GAP - 20;

        fillGridRelacoes_Listagem();
    }

    carregaComboGrupo();
}

function chkInc_onclick() 
{
    mostraEscondeDicas();
    fg.Rows = 0;
    fgMembrosElem.Rows = 0;
    abreGrid();
}

function onchange_Elemento() 
{
    fg.Rows = 1;
    fgMembrosElem.Rows = 0;
    abreGrid();
    carregaComboGrupo();
}

function CarregaCombo(chengeInterface)
{
    //primeiro combo
    glb_CounterCmbsStatics = 1;

    glb_changeInterface = chengeInterface;
    
    var where = '';

    if (chkInc.checked)
        where = 'AND (1 = (SELECT ' + ((glb_nNivel == 0) ? 'aa.RelacaoNivel0' : 'aa.RelacaoNivel12') + ' FROM RegrasFiscais_Mapeamento aa WITH(NOLOCK) ' +
                        'WHERE (aa.RegraFiscalID = (SELECT TOP 1 bb.RegraFiscalMaeID FROM RegrasFiscais bb WITH(NOLOCK) WHERE bb.RegraFiscalID = c.AgrupadorID)) ' +
                            'AND (aa.ElementoID = a.ElementoID)))';
    else
        where = 'AND (a.ElementoID IN (SELECT aa.ElementoID FROM RegrasFiscais_Relacoes aa WITH(NOLOCK) WHERE (aa.RegraFiscalID = a.RegraFiscalID)))';

    var sSQL = 'SELECT 0 AS fldID, \'\' AS fldName UNION ALL ' +
                'SELECT DISTINCT b.ItemID AS fldID, b.ItemMasculino AS fldName ' +
	                'FROM RegrasFiscais_Elementos a WITH(NOLOCK) ' +
	                'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.ElementoID) ' +
	                'INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) c ON (c.RegraFiscalID = a.RegraFiscalID) ' +
		            'INNER JOIN dbo.RegrasFiscais d ON (d.RegraFiscalID = c.RegraFiscalID) ' +
		            'INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') e ON (e.AgrupadorID = c.AgrupadorID) ' +
		            'WHERE (d.Nivel <= ' + glb_nNivel + ') ' + where;
    
    setConnection(dsoCombosElem);
    dsoCombosElem.SQL = sSQL;

    dsoCombosElem.ondatasetcomplete = CarregaCombo_DSC;
    dsoCombosElem.Refresh();
}

function CarregaCombo_DSC() 
{
    var optionStr, optionValue;
    var aCmbsStatics = [selElementoID];
    var aDSOsStatics = [dsoCombosElem];
    var oOption;
    var i;
    var lFirstRecord;
    var nQtdCmbs = aCmbsStatics.length;

    glb_CounterCmbsStatics--;

    if (glb_CounterCmbsStatics == 0) 
    {
        for (i = 0; i < nQtdCmbs; i++) 
        {
            clearComboEx([aCmbsStatics[i].id]);
            lFirstRecord = true;
            while (!aDSOsStatics[i].recordset.EOF) 
            {
                optionStr = aDSOsStatics[i].recordset['fldName'].value;
                optionValue = aDSOsStatics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsStatics[i].add(oOption);
                aDSOsStatics[i].recordset.MoveNext();
                lFirstRecord = false;
            }

            aCmbsStatics[i].disabled = !(aCmbsStatics[i].length > 0);
        }
    }
    
    if (chkInc.checked)
        fillGridRelacoes();
    else
        fillGridRelacoes_Listagem();
        
    if (!glb_changeInterface)
        OcultaMostraCampos(true);
}

function carregaComboGrupo() 
{   
    setConnection(dsoCombosGru);
    if (chkInc.checked) 
    {
        dsoCombosGru.SQL = 'SELECT DISTINCT ISNULL(a.Grupo, SPACE(1)) AS fldName, ISNULL(a.Grupo,0) AS fldID ' +
                        ' FROM    dbo.RegrasFiscais_Elementos a  WITH ( NOLOCK ) ' +
                        ' WHERE   a.RegraFiscalID = ' + glb_RegraFiscalID + ' AND a.ElementoID = ' + selElementoID.value + '  AND Grupo IS NOT NULL ' +
                        ' UNION ALL ' +
                        ' SELECT (ISNULL(MAX(a.Grupo), 0) + 1) AS fldName, (ISNULL(MAX(a.Grupo), 0) + 1) AS fldID ' +
                        ' FROM    dbo.RegrasFiscais_Elementos a  WITH(NOLOCK) ' +
                        ' WHERE  ( a.RegraFiscalID = ' + glb_RegraFiscalID + 'AND a.ElementoID = ' + selElementoID.value + ' ) ';
    }
    else 
    {
        dsoCombosGru.SQL = 'SELECT DISTINCT ISNULL(a.Grupo, SPACE(1)) AS fldName, ISNULL(a.Grupo,0) AS fldID ' +
	                            'FROM (SELECT a.Grupo ' +
			                            'FROM RegrasFiscais_Elementos a WITH ( NOLOCK ) ' +
				                            'INNER JOIN dbo.RegrasFiscais b WITH ( NOLOCK ) ON a.RegraFiscalID = b.RegraFiscalID ' +
			                            'WHERE a.RegraFiscalID IN (' + glb_RegraFiscalID + ', ' + glb_RegraFiscalMaeID + ') AND a.ElementoID = ' + selElementoID.value + ' ' +
					                            'OR ((b.RegraFiscalMaeID = ' + glb_RegraFiscalMaeID + ') ' +
					                            'AND ((SELECT aa.RegraFiscalMaeID ' +
							                            'FROM   RegrasFiscais aa WITH ( NOLOCK ) ' +
							                            'WHERE  (aa.RegraFiscalID = ' + glb_RegraFiscalMaeID + ')) IS NOT NULL ' +
						                            'AND a.ElementoID = ' + selElementoID.value + ')) ' +
			                            'UNION ALL ' +
			                            '(SELECT a.Grupo ' +
				                            'FROM dbo.RegrasFiscais_Elementos a ' +
					                            'INNER JOIN dbo.RegrasFiscais b WITH ( NOLOCK ) ON a.RegraFiscalID = b.RegraFiscalID ' +
				                            'WHERE a.RegraFiscalID = (SELECT RegraFiscalMaeID ' +
											                            'FROM RegrasFiscais WITH(NOLOCK) ' +
											                            'WHERE (RegraFiscalID = ' + glb_RegraFiscalMaeID + ') AND (a.ElementoID = ' + selElementoID.value + ')))) a ' +
	                            'ORDER BY fldName';
    }

    dsoCombosGru.ondatasetcomplete = carregaComboGrupo_DSC;
    dsoCombosGru.Refresh();
}

function carregaComboGrupo_DSC()
{
    var optionStr, optionValue;
    var oOption;

    clearComboEx(['selGrupo']);

    if (selElementoID.selectedIndex <= 0)
    {
        selGrupo.disabled = true;
        btnAtribuir.disabled = true;
        return null;
    }

    if (!chkInc.checked)
    {
        optionStr = '';
        optionValue = 0;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selGrupo.add(oOption);
    }
    
    while (!dsoCombosGru.recordset.EOF) 
    {
        optionStr = dsoCombosGru.recordset['fldName'].value;
        optionValue = dsoCombosGru.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selGrupo.add(oOption);
        dsoCombosGru.recordset.MoveNext();
    }

    selGrupo.disabled = !(selGrupo.length > 0); //((!chkInc.checked) ? 1 : 0));

    btnAtribuir.disabled = selGrupo.disabled;
}

function limpaCampos()
{
    txtObservacao.value = '';
}


function js_fgMembrosElem_modalRelacoes_AfterEdit(Row, Col) 
{
    var nRegraFiscalID = 0;
    var bCol = 0;
    var sMensagem = '';

    if (Col == getColIndexByColKey(fgMembrosElem, 'OK'))
    {
        bCol = fgMembrosElem.textMatrix(Row, Col);

        if (bCol != 0)
        {
            nRegraFiscalID = fgMembrosElem.textMatrix(Row, getColIndexByColKey(fgMembrosElem, 'RegraFiscalID*'));

            if (nRegraFiscalID != glb_RegraFiscalID)
            {
                sMensagem = 'Somente a Regra Fiscal ' + glb_RegraFiscalID + ' pode ser selecionada para inclus�o de grupo.';
                fgMembrosElem.textMatrix(Row, getColIndexByColKey(fgMembrosElem, 'OK')) = 0;
                window.top.overflyGen.Alert(sMensagem);
            }
        }
    }
    else if (Col == getColIndexByColKey(fgMembrosElem, 'Excecao*'))
    {
        bCol = fgMembrosElem.textMatrix(Row, Col);
        
        if (bCol != 0)
            fgMembrosElem.textMatrix(Row, Col) = 0;
        else
            fgMembrosElem.textMatrix(Row, Col) = 1;
    }
    
    GridLinhaTotal(fgMembrosElem);
}
function mostraEscondeDicas()
{
    if (chkDicas.checked)
    {
        divFG.style.visibility = 'hidden';
        divFGMembrosElem.style.visibility = 'hidden';
        divFGImpostos.style.visibility = 'hidden';
        btnGravar.style.visibility = 'hidden';
        btnVoltar.style.visibility = 'hidden';
        btnDeletar.style.visibility = 'hidden';
        btnAvancar.style.visibility = 'hidden';
        lblInc.style.visibility = 'hidden';
        chkInc.style.visibility = 'hidden';
        lblElemento.style.visibility = 'hidden';
        selElementoID.style.visibility = 'hidden';
        lblGrupo.style.visibility = 'hidden';
        selGrupo.style.visibility = 'hidden';
        lblImposto.style.visibility = 'hidden';
        selImposto.style.visibility = 'hidden';
        btnListar.style.visibility = 'hidden';
        
        lblVisao.style.visibility = 'hidden';
        selVisao.style.visibility = 'hidden';
        
        divDicas.style.visibility = 'inherit';
    }
    else 
    {
        divFG.style.visibility = 'inherit';
        divFGMembrosElem.style.visibility = 'inherit';
        lblInc.style.visibility = 'inherit';
        chkInc.style.visibility = 'inherit';
        lblElemento.style.visibility = 'inherit';
        selElementoID.style.visibility = 'inherit';
        lblGrupo.style.visibility = 'inherit';
        selGrupo.style.visibility = 'inherit';
        lblImposto.style.visibility = 'inherit';
        selImposto.style.visibility = 'inherit';
        divDicas.style.visibility = 'hidden';
        btnListar.style.visibility = 'inherit';

        lblVisao.style.visibility = 'inherit';
        selVisao.style.visibility = 'inherit';
        
        OcultaMostraCampos();
    }
}

function dicas()
{
    var sSQL = '';
    var sWHERE = '';

    if (glb_nNivel == 0)
        sWHERE = 'WHERE a.RegraFiscalID = ' + glb_RegraFiscalID;
    else if (glb_nNivel == 1)
        sWHERE = 'WHERE a.RegraFiscalID = ' + glb_RegraFiscalMaeID;
    else if (glb_nNivel == 2)
        sWHERE = 'WHERE a.RegraFiscalID IN ' +
                    '(SELECT aa.RegraFiscalMaeID FROM RegrasFiscais aa WITH(NOLOCK) WHERE RegraFiscalID = ' + glb_RegraFiscalMaeID + ')';

    sSQL = 'SELECT ISNULL(a.Observacoes, \'\') AS Observacoes ' +
                'FROM RegrasFiscais a WITH(NOLOCK) ' +
                sWHERE;

    dsoDicas.SQL = sSQL;
    dsoDicas.ondatasetcomplete = eval(dicas_DSC);
    dsoDicas.refresh();
}

function dicas_DSC()
{
    if ((!dsoDicas.recordset.EOF) && (!dsoDicas.recordset.BOF))
        divDicas.innerHTML = dsoDicas.recordset['Observacoes'].value;
}


function js_fg_modalRelacoes_Membros_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    //Forca celula read-only    
    if (!glb_GridIsBuilding) {
        if (glb_FreezeRolColChangeEvents)
            return true;

        if (cellIsLocked2(NewRow, NewCol)) {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
    }
}

function cellIsLocked2(nRow, nCol) 
{
    var retVal = false;

    if (((fgMembrosElem.textMatrix(nRow, getColIndexByColKey(fg, 'RegraFiscalID*')) != glb_RegraFiscalID) && (!chkInc.checked)) || (glb_EstadoID != 1))
    {
        retVal = true;
    }

    return retVal;
}

function js_fg_modalRelacoes_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) 
{
    //Forca celula read-only    
    if (!glb_GridIsBuilding) {
        if (glb_FreezeRolColChangeEvents)
            return true;

        if (cellIsLocked(NewRow, NewCol)) {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
    }
} 

function cellIsLocked(nRow, nCol) {
    var retVal = false;

    if (getColIndexByColKey(fg, 'DET') == nCol)
        return false;

    if (((fg.textMatrix(nRow, getColIndexByColKey(fg, 'RegraFiscalID*')) != glb_RegraFiscalID) && (!chkInc.checked) && (nRow > 1)) || (glb_EstadoID != 1))
    {
        retVal = true;
    }

    return retVal;
}


function js_fg_modalRelacoes_AfterEdit(Row, Col) 
{
    if (Col == getColIndexByColKey(fg, 'Det'))
    {
        marcaElementosIguais(Row);
        fillGridRelacoes_Detalhes();
        return null;
    }

    if (fg.Editable) 
    {
        if ((Col != getColIndexByColKey(fg, 'OK')) && (fg.textMatrix(Row, getColIndexByColKey(fg, 'RegraFiscalID*')) == glb_RegraFiscalID))
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;

        if ((fg.textMatrix(Row, getColIndexByColKey(fg, 'RegraFiscalID*')) != glb_RegraFiscalID) && (fg.ColDataType(Col) == 11) && (!chkInc.checked))
        {
            if (fg.textMatrix(Row, Col) != 0)
                fg.textMatrix(Row, Col) = 0;
            else
                fg.textMatrix(Row, Col) = 1;
        }                       
        // fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
    }
    GridLinhaTotal(fg);
}

function GridLinhaTotal(grid) 
{
    var nContador = 0;

    for (i = 2; i < grid.Rows; i++) 
    {
        if (grid.ValueMatrix(i, getColIndexByColKey(grid, 'OK')) != 0)
            nContador++;
    }

    if (grid.Rows > 1)
        grid.TextMatrix(1, getColIndexByColKey(grid, 'RegraFiscalID*')) = nContador;
}
function js_fg_modalRelacoesDblClick(grid, Row, Col) 
{
   
        // trap (grid esta vazio)
        if (grid.Rows <= 1)
            return true;

        // trap so colunas editaveis
        if ((Col != getColIndexByColKey(grid, 'OK')))
            return true;

        // trap so header
        if (Row != 1)
            return true;

        var i;  
        var nRegraFiscalID = 0;
        var bFill = true;

        grid.Editable = false;
        grid.Redraw = 0;
        lockControlsInModalWin(true);
        
        // limpa coluna se tem um check box checado
        for (i = 2; i < grid.Rows; i++) {
            if (grid.ValueMatrix(i, Col) != 0) {
                bFill = false;
                break;
            }
        }

        for (i = 2; i < grid.Rows; i++) 
        {
            if (bFill) 
             {
                if (grid == fgMembrosElem) {
                    nRegraFiscalID = fgMembrosElem.textMatrix(i, getColIndexByColKey(fgMembrosElem, 'RegraFiscalID*'));
                    if (nRegraFiscalID != glb_RegraFiscalID)
                        continue;
                }
                grid.TextMatrix(i, Col) = 1;
            }
            else
                grid.TextMatrix(i, Col) = 0;

        }

        lockControlsInModalWin(false);
        grid.Editable = true;
        grid.Redraw = 2;
        window.focus();
        grid.focus();
    
    GridLinhaTotal(grid);
}

function paintReadOnlyRows() 
{
    if (chkInc.checked)
        return null;    

    for (var i = 2; i < fg.Rows; i++) 
    {
        if ((fg.textMatrix(i, getColIndexByColKey(fg, 'RegraFiscalID*')) != glb_RegraFiscalID) || (glb_EstadoID != 1))
        {
            fg.Cell(6, i, 0, i, getColIndexByColKey(fg, 'DET') - 1) = 0XDCDCDC;
            fg.Cell(6, i, getColIndexByColKey(fg, 'DET') + 1, i, fg.Cols - 1) = 0XDCDCDC;
        }
    }
}

function js_modalRelacoes_BeforeEdit(grid, Row, Col) 
{
    if (Col == getColIndexByColKey(fg, 'Observacao'))
        grid.EditMaxLength = 60;
}

function abreGrid()
{
    if (fgMembrosElem.Rows > 2) 
    {
        divFGMembrosElem.style.visibility = 'inherit';
        fgMembrosElem.style.visibility = 'inherit';
    
        with (divFG.style)
        {
            height = 200;
        }

        with (fg.style)
        {
            height = 200;
        }
        
        if (chkInc.checked)
        {
            with (divFGImpostos.style)
            {
                height = 200;
            }
            with (fgImpostos.style)
            {
                height = 200;
            }
        }
    }
    else
    {
        divFGMembrosElem.style.visibility = 'hidden';
        fgMembrosElem.style.visibility = 'hidden';
        
        with (divFG.style) 
        {
            height = 414;
        }

        with (fg.style) 
        {
            height = 414;
        }

        if (chkInc.checked) 
        {
            with (divFGImpostos.style) 
            {
                height = 414;
            }
            with (fgImpostos.style) 
            {
                height = 414;
            }
        }    
    }
}

function marcaElementosIguais(nRow) 
{
    var nElementoID;
    var nGrupo;

    var nCurrElementoID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ElementoID*')); ;
    var nCurrGrupo = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Grupo*')); ;
    
    for (i = 2; i < fg.Rows; i++)
    {
        if (i != fg.Row)
        {
            nElementoID = fg.TextMatrix(i, getColIndexByColKey(fg, 'ElementoID*'));
            nGrupo = fg.TextMatrix(i, getColIndexByColKey(fg, 'Grupo*'));

            if ((nElementoID == nCurrElementoID) && (nGrupo == nCurrGrupo))
                fg.TextMatrix(i, getColIndexByColKey(fg, 'DET')) = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'DET'));
        }
    }
}

function carregaVisao()
{
    var hint = new Array();

    hint[0] = 'Somente registros da pr�pria regra';
    hint[1] = 'Registros da pr�pria regra, regras de nivel superior e inferior';
    hint[2] = 'Registros da pr�pria regra, regras do mesmo nivel, superior e inferior';

    clearComboEx(['selVisao']);

    for (var i = 0; (i <= (glb_nNivel == 0 ? 1 : glb_nNivel)); i++) {
        var oOption = document.createElement("OPTION");
        oOption.text = i.toString();
        oOption.value = i;
        oOption.title = hint[i];
        selVisao.add(oOption);
    }
}

function cmbOnChange(cmb) 
{
    if ((cmb.id).toUpperCase() == 'SELVISAO') 
    {
        if (chkInc.checked) {
            fillGridRelacoes();
            fillGridImpostos();
        }
        else
            fillGridRelacoes_Listagem();
    }
}