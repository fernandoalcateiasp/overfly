/********************************************************************
modalmensagens.js

Library javascript para o modalmensagens.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport("dsoPesq");
var dsoMensagens = new CDatatransport("dsoMensagens");
var dsoDicas = new CDatatransport("dsoDicas");
var dsoAcoesMensagens = new CDatatransport("dsoAcoesMensagens");
var glb_RegraFiscalID = null;
var glb_RegraFiscalMaeID = null;
var glb_Nivel = null;
var glb_EstadoID = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************
     
// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() 
{
    window_onload_1stPart();

    glb_RegraFiscalID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['RegraFiscalID'].value");
    glb_Nivel = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Nivel'].value");
    glb_RegraFiscalMaeID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['RegraFiscalMaeID'].value");
    glb_EstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");
    
    // ajusta o body do html
    with (modalmensagensBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    // configuracao inicial do html
    setupPage();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 49;
    modalFrame.style.left = 0;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
}    
/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() 
{

    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Regras Fiscais');
    
    // btnOK nao e usado nesta modal
    with (btnOK) {
        disabled = true;
        style.visibility = 'hidden';
    }

    // btnCanc nao e usado nesta modal
    with (btnCanc) {
        style.visibility = 'hidden';
    }
   // texto da secao01
    secText('Regra Fiscal ' + glb_RegraFiscalID + ' - Mensagens (1/5)', 1);

    divDicas.style.visibility = 'hidden';
    //txtDicas.readOnly = true;

    if (glb_EstadoID != 1) 
    {
        btnGravar.disabled = true;
        btnDeletar.disabled = true;
    }

    loadDataAndTreatInterface();
    carregaVisao();
    fillGridMensagens();
}

function loadDataAndTreatInterface() 
{

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var x_gap, y_gap;

    topFree = parseInt(divMod01.style.height, 10) + ELEM_GAP;

   // Botao, label, CheckBox

    btnListar.style.top = parseInt(chkDicas.currentStyle.top, 10) + 21;
    btnListar.style.left = parseInt(btnOK.style.left, 10) + parseInt(btnCanc.style.left, 10) + ELEM_GAP - 26;
    btnAvancar.style.width = FONT_WIDTH * 7 + 20;
    
    btnAvancar.style.top = parseInt(btnOK.currentStyle.top, 10) + 8;
    btnAvancar.style.left = parseInt(btnOK.style.left, 10) + parseInt(btnCanc.style.left, 10) + ELEM_GAP - 20;
    btnAvancar.style.width = FONT_WIDTH * 7 + 20;

    btnGravar.style.top = parseInt(btnOK.currentStyle.top, 10) + 8;
    btnGravar.style.left = parseInt(btnOK.style.left, 10); //parseInt(btnAvancar.style.left, 10) - parseInt(btnCanc.style.left, 10) + ELEM_GAP ;
    btnGravar.style.width = FONT_WIDTH * 7 + 20;

    btnDeletar.style.top = parseInt(btnOK.currentStyle.top, 10) + 8;
    btnDeletar.style.left = parseInt(btnCanc.style.left, 10); //parseInt(btnOK.style.left, 10) + ELEM_GAP + 45 ;
    btnDeletar.style.width = FONT_WIDTH * 7 + 20;
    
    lblDicas.style.top = topFree - 6;
    lblDicas.style.left = ELEM_GAP;
    lblDicas.style.width = FONT_WIDTH * 7;

    chkDicas.style.top = parseInt(lblDicas.currentStyle.top, 10) + 12;
    chkDicas.style.left = -10 ;
    chkDicas.style.width = FONT_WIDTH * 7;

    lblVisao.style.top = topFree - 6;
    lblVisao.style.left = ELEM_GAP * 5 - 2;
    lblVisao.style.width = FONT_WIDTH * 7;

    selVisao.style.top = parseInt(lblVisao.currentStyle.top, 10) + 12;
    selVisao.style.left = parseInt(chkDicas.currentStyle.left, 10) + parseInt(chkDicas.currentStyle.width, 10);
    selVisao.style.width = FONT_WIDTH * 5;    

    lblMensagens.style.top = parseInt(chkDicas.style.top,10) + 28;
    lblMensagens.style.left = ELEM_GAP;
    lblMensagens.style.width = FONT_WIDTH * 7;

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + parseInt(lblMensagens.style.top) - 10;
        width = (80 + (2 * 10)) * FONT_WIDTH + 47;
        height = parseInt(btnOK.currentStyle.top, 10) - ELEM_GAP - 27;
    }
    with (fg.style) 
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10) + 127;
        height = parseInt(divFG.style.height, 10) - 47;
    }

/*    divDicas.style.top = parseInt(divFG.style.top, 10);
    divDicas.style.left = parseInt(divFG.style.left, 10);
    divDicas.style.width = parseInt(divFG.style.width, 10) + 70 + 60;
    divDicas.style.height = parseInt(divFG.style.height, 10) - 45;*/

    with (divDicas.style) 
    {
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divFG.style.top, 10);
        width = parseInt(divFG.style.width, 10) + 70 + 60;
        height = parseInt(divFG.style.height, 10) - 45;
    }        

    chkDicas.onclick = mostraEscondeDicas;

    dicas();

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}
/********************************************************************
Preenche o grid de Mensagens
********************************************************************/
function fillGridMensagens() 
{
    lockControlsInModalWin(true);

    var sWhere = '';

    if (selVisao.value == 0)
        sWhere = 'WHERE (a.RegraFiscalID = ' + glb_RegraFiscalID + ')';
    else if (selVisao.value == 1)
        sWhere = 'WHERE ((a.RegraFiscalID = ' + glb_RegraFiscalID + ') OR ((d.Nivel <> ' + glb_Nivel + ') AND (d.Nivel - ' + glb_Nivel + ' BETWEEN -1 AND 1))) ';
    
    var sSQL = 'SELECT 0 AS Indice, 0 AS OK , a.RegMensagemID, a.RegraFiscalID, a.Ordem, a.Condicional, a.Mensagem, a.Observacao ' +
                    'FROM dbo.RegrasFiscais_Mensagens a WITH(NOLOCK) ' +
                        'INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) b ON (b.RegraFiscalID = a.RegraFiscalID) ' +
                        'INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') c ON (c.AgrupadorID = b.AgrupadorID) ' +
                        'INNER JOIN RegrasFiscais d WITH(NOLOCK) ON (d.RegraFiscalID = b.RegraFiscalID) ' +
                    sWhere +
                ' UNION ALL ' +
                'SELECT 1 AS Indice, 0 AS OK , 0 AS RegMensagemID, ' + glb_RegraFiscalID + ' AS RegraFiscalID,  ' +
                        'NULL AS Ordem, NULL AS Condicional, CONVERT(VARCHAR(256), SPACE(0)) AS Mensagem, CONVERT(VARCHAR(30), SPACE(0)) AS Observacao ' +
                    'ORDER BY Indice, RegraFiscalID, Ordem'; 
   
    dsoMensagens.SQL = sSQL;
    dsoMensagens.ondatasetcomplete = eval(fillGridMensagens_DSC);
    dsoMensagens.refresh();
}

function fillGridMensagens_DSC()
{
                  
   startGridInterface(fg);
   headerGrid(fg, ['RegraID',
                    'RegraID',
                    'OK',
                    'Ordem',
                    'Condicional',
                    'Mensagem',
                    'Observa��o'], [1]);
                    
    glb_aCelHint = [[0, 4, 'Mensagem usada somente se estiver presente em uma rela��o']];

    fillGridMask(fg, dsoMensagens, ['RegraFiscalID*',
                                    'RegMensagemID',
                                    'OK',
                                    'Ordem',
                                    'Condicional',
                                    'Mensagem', 
                                    'Observacao'],
                                   ['','', '', '99', '', '', '']);

    fg.FrozenCols = 3;
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    GridLinhaTotal(fg);
   
    //coloca o tipo das colunas
        with (fg) {
        ColDataType(2) = 11;
    }

    paintReadOnlyRows();
    
    fg.Editable = true;
    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);  
    fg.ColWidth(5) = parseInt(divFG.currentStyle.width, 10) * 13 - 1200;
    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);
    
}


function GridLinhaTotal(grid) {
    var nContador = 0;

    // Linha de Totais
    gridHasTotalLine(grid, '', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(grid, 'RegraFiscalID*'), '######', 'C']]);


    for (i = 2; i < grid.Rows; i++) {

        if (grid.ValueMatrix(i, getColIndexByColKey(grid, 'OK')) != 0)
            nContador++;
    }

    if (grid.Rows > 1)
        grid.TextMatrix(1, getColIndexByColKey(grid, 'RegraFiscalID*')) = nContador;

}
/********************************************************************
Fun��o Para Gravar a Linha do grid
********************************************************************/
function AcoesMensagens(bDelete) 
{
    lockControlsInModalWin(true);

    var nRegMensagemID;
    var nRegraFiscalID;
    var nOrdem;
    var bCondicional;
    var sMensagem = '';
    var sObservacao;
    var nTipoID;
    var separadorMensagem = '^';

    var sMensagens = '';
    var strPars = new String();
    strPars = '';

    for (var i = 1; i < fg.Rows; i++) 
    {
        var bOK = (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0);

        if (bOK != 0)
        {
            nRegMensagemID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegMensagemID'));
            nRegraFiscalID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegraFiscalID*'));
            nOrdem = fg.textMatrix(i, getColIndexByColKey(fg, 'Ordem'));
            bCondicional = fg.textMatrix(i, getColIndexByColKey(fg, 'Condicional'));
            sMensagem = fg.textMatrix(i, getColIndexByColKey(fg, 'Mensagem'));
            sObservacao = fg.textMatrix(i, getColIndexByColKey(fg, 'Observacao'));
            nTipoID = 2;

            if (bCondicional != 0)
                bCondicional = 1;

            if ((nRegMensagemID == 0) || (nRegMensagemID == ''))
                nTipoID = 3;
                
            else if ((bOK != 0) && (bDelete))
                nTipoID = 1;
            
            if (strPars == '')
                strPars += '?nRegMensagemID=' + escape(nRegMensagemID);
            else
                strPars += '&nRegMensagemID=' + escape(nRegMensagemID);
            
            strPars += '&nRegraFiscalID=' + escape(nRegraFiscalID);
            strPars += '&nOrdem=' + escape(nOrdem);
            strPars += '&bCondicional=' + escape(bCondicional);
            strPars += '&sObservacao=' + escape(sObservacao);
            strPars += '&nTipoID=' + escape(nTipoID);

            if (sMensagens.length > 0)
                sMensagens += separadorMensagem + escape(sMensagem);
            else
                sMensagens += '&sMensagem=' + escape(sMensagem);
        }
    }

    strPars += sMensagens;

    if (strPars != '') 
    {
        var _retConf = 1;
        
        if (bDelete)
            _retConf = window.top.overflyGen.Confirm('Voc� tem certeza?');

        if (_retConf == 1) 
        {
            dsoAcoesMensagens.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/serverside/gravarmensagens.aspx' + strPars;
            dsoAcoesMensagens.ondatasetcomplete = AcoesMensagens_DSC;
            dsoAcoesMensagens.refresh();
        }
        else
            lockControlsInModalWin(false);
    }
    else 
    {
        window.top.overflyGen.Alert('Selecione algum registro desta regra');

        lockControlsInModalWin(false);
    }
}


function AcoesMensagens_DSC() 
{
    if (!(dsoAcoesMensagens.recordset.BOF && dsoAcoesMensagens.recordset.EOF)) {
        if (dsoAcoesMensagens.recordset['Error'].value != null && dsoAcoesMensagens.recordset['Error'].value.length > 0) 
        {
          if (window.top.overflyGen.Alert(dsoAcoesMensagens.recordset['Error'].value) == 0) {
               return null;
            }
        }
        else
            lockControlsInModalWin(false);            
    } 
    fillGridMensagens();
}

function AvancarModal() {
    var aEmpresa = getCurrEmpresaData();
    var nPaisEmpresaID = aEmpresa[1];

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.visibility = 'hidden';
    
    strPars = '?sCaller=' + escape('S');
    strPars += '&nRegraFiscalID=' + escape(glb_RegraFiscalID);
    strPars += '&nPaisEmpresaID=' + escape(nPaisEmpresaID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/modalpages/modalelementos.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 539));

}
/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);
    
    if (ctl.id == btnOK.id) {
   // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S', null);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) 
    {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null);
    }
    else if (ctl.id == btnGravar.id) 
    {
     // GravarMensagens
        AcoesMensagens(false); 
        lockControlsInModalWin(false);      
    }
    else if (ctl.id == btnDeletar.id) 
    {
        AcoesMensagens(true);
    }
    else if (ctl.id == btnAvancar.id) {
         lockControlsInModalWin(false);
         AvancarModal();
     }
    else if (ctl.id == btnListar.id)
    {
        fillGridMensagens();
    }
}

function mostraEscondeDicas() 
{
    if (chkDicas.checked)
    {
        divFG.style.visibility = 'hidden';
        divDicas.style.visibility = 'inherit';
        btnGravar.style.visibility = 'hidden';
        btnDeletar.style.visibility = 'hidden';
        btnAvancar.style.visibility = 'hidden';
        lblMensagens.style.visibility = 'hidden';

        lblVisao.style.visibility = 'hidden';
        selVisao.style.visibility = 'hidden';                        
    }
    else
    {
        divFG.style.visibility = 'inherit';
        divDicas.style.visibility = 'hidden';
        btnGravar.style.visibility = 'inherit';
        btnDeletar.style.visibility = 'inherit';
        btnAvancar.style.visibility = 'inherit';
        lblMensagens.style.visibility = 'inherit';

        lblVisao.style.visibility = 'inherit';
        selVisao.style.visibility = 'inherit';                        
    }
}

function dicas()
{
    var sSQL = '';
    var sWHERE = '';
    
    if (glb_Nivel == 0)
        sWHERE = 'WHERE a.RegraFiscalID = ' + glb_RegraFiscalID;
    else if (glb_Nivel == 1) 
        sWHERE = 'WHERE a.RegraFiscalID = ' + glb_RegraFiscalMaeID;
    else if (glb_Nivel == 2)
        sWHERE = 'WHERE a.RegraFiscalID IN ' +
                    '(SELECT aa.RegraFiscalMaeID FROM RegrasFiscais aa WITH(NOLOCK) WHERE RegraFiscalID = ' + glb_RegraFiscalMaeID + ')';

    sSQL = 'SELECT ISNULL(a.Observacoes, \'\') AS Observacoes ' +
                'FROM RegrasFiscais a WITH(NOLOCK) ' +
                sWHERE;

    dsoDicas.SQL = sSQL;
    dsoDicas.ondatasetcomplete = eval(dicas_DSC);
    dsoDicas.refresh();
}

function dicas_DSC()
{
    if ((!dsoDicas.recordset.EOF) && (!dsoDicas.recordset.BOF))
        divDicas.innerHTML = dsoDicas.recordset['Observacoes'].value;
}

function js_fg_modalMensagens_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    //Forca celula read-only    
    if (!glb_GridIsBuilding) {
        if (glb_FreezeRolColChangeEvents)
            return true;

        if (cellIsLocked(NewRow, NewCol)) {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
    }
}

function cellIsLocked(nRow, nCol) {
    var retVal = false;

    if (nCol == getColIndexByColKey(fg, 'OK'))
        return retVal;

    if ((fg.textMatrix(nRow, getColIndexByColKey(fg, 'RegraFiscalID*')) != glb_RegraFiscalID) && (nRow > 1))
    {
        retVal = true;
    }
    
    return retVal;
}


function js_modalMensagens_AfterEdit(Row, Col) 
{
    if (fg.Editable) 
    {
        if ((Col != getColIndexByColKey(fg, 'OK')) && (fg.textMatrix(Row, getColIndexByColKey(fg, 'RegraFiscalID*')) == glb_RegraFiscalID))
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;

        if ((fg.textMatrix(Row, getColIndexByColKey(fg, 'RegraFiscalID*')) != glb_RegraFiscalID) && (fg.ColDataType(Col) == 11) && (Col != getColIndexByColKey(fg, 'OK')))
        {
            if (fg.textMatrix(Row, Col) != 0)
                fg.textMatrix(Row, Col) = 0;
            else
                fg.textMatrix(Row, Col) = 1;
        }           
    }
    GridLinhaTotal(fg);
}

function paintReadOnlyRows() 
{
    for (var i = 2; i < fg.Rows; i++) 
    {
        if ((fg.textMatrix(i, getColIndexByColKey(fg, 'RegraFiscalID*')) != glb_RegraFiscalID)  || (glb_EstadoID != 1))
        {
            fg.Cell(6, i, 0, i, getColIndexByColKey(fg, 'OK') - 1) = 0XDCDCDC;
            fg.Cell(6, i, getColIndexByColKey(fg, 'OK') + 1, i, fg.Cols - 1) = 0XDCDCDC;
        }
    }
}

function js_modalMensagens_BeforeEdit(grid, Row, Col) 
{
    var nomeColuna = grid.ColKey(Col);

    var lastPos = nomeColuna.length - 1;

    // retira o asterisco do nome do campo
    if (nomeColuna.substr(lastPos, 1) == '*') 
    {
        nomeColuna = nomeColuna.substr(0, lastPos);
    }    

    if ((dsoMensagens.recordset[nomeColuna].type == 200) || (dsoMensagens.recordset[nomeColuna].type == 129))
        fieldMaxLength(fg, Col, dsoMensagens);
}

function js_fg_modalMensagensDblClick(grid, Row, Col) {

    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if ((Col != getColIndexByColKey(grid, 'OK')))
        return true;

    // trap so header
    if (Row != 1)
        return true;

    var i;
    var bFill = true;

    fg.Editable = false;
    fg.Redraw = 0;
    lockControlsInModalWin(true);



    // limpa coluna se tem um check box checado
    for (i = 2; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 2; i < grid.Rows; i++) 
    {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    lockControlsInModalWin(false);
    fg.Editable = true;
    fg.Redraw = 2;
    window.focus();
    fg.focus();

    GridLinhaTotal(fg);
}

function carregaVisao() 
{
    var hint = new Array();

    hint[0] = 'Somente registros da pr�pria regra';
    hint[1] = 'Registros da pr�pria regra, regras de nivel superior e inferior';
    hint[2] = 'Registros da pr�pria regra, regras do mesmo nivel, superior e inferior';
    
    clearComboEx(['selVisao']);

    for (var i = 0; (i <= (glb_Nivel == 0 ? 1 : glb_Nivel)); i++) 
    {
        var oOption = document.createElement("OPTION");
        oOption.text = i.toString();
        oOption.value = i;
        oOption.title = hint[i];
        selVisao.add(oOption);
    }
}

function cmbOnChange(cmb) 
{
    if ((cmb.id).toUpperCase() == 'SELVISAO')
        fillGridMensagens();
}