
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalimpostosHtml" name="modalimpostosHtml">

<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/regrasfiscais/modalpages/modalimpostos.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/regrasfiscais/modalpages/modalimpostos.js" & Chr(34) & "></script>" & vbCrLf
%>

<%

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nRegraFiscalID, sCaller, rsData, strSQL

sCaller = ""
nRegraFiscalID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nRegraFiscalID").Count    
    nRegraFiscalID = Request.QueryString("nRegraFiscalID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nRegraFiscalID = " & CStr(nRegraFiscalID) & ";"
Response.Write vbcrlf

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>


<script ID="wndJSProc" LANGUAGE="javascript">
<!--
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalImpostos_BeforeEdit(fg, arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
//<!--
 js_fg_modalImpostos_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
<!--
 js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell(fg);
//-->
</SCRIPT>   
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalImpostos_AfterEdit(arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
    js_fg_modalImpostosDblClick(fg, fg.Row, fg.Col);
//-->
</SCRIPT>


</head>

<body id="modalimpostosBody" name="modalimpostosBody" LANGUAGE="javascript" onload="return window_onload()">
     <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
  
    <p id="lblDicas" name="lblDicas" class="lblGeneral">Dicas</p>
    <input type="checkbox" id="chkDicas" name="chkDicas" class="fldGeneral" title="Dicas de uso da Regra Fiscal"></input>
    
    <p id="lblVisao" name="lblVisao" class="lblGeneral">Vis�o</p>
    <select id="selVisao" name="selVisao" class="fldGeneral"  onchange="return cmbOnChange(this)"></select>        
    
    <p id="lblInc" name="lblInc" class="lblGeneral">Incluir</p>  
    <input type="checkbox" id="chkInc" name="chkInc" class="fldGeneral" title="Incluir registros"></input>
    <p id="lblImposto" name="lblImposto" class="lblGeneral"  title="Impostos">Imposto</p>    
    <select id="selImposto" name="selImposto" class="fldGeneral"></select>
    
    <p id="lblCodigoTributacao" name="lblCodigoTributacao" class="lblGeneral"  title="C�digo Tributa��o">CT</p>
    <select id="selCodigoTributacao" name="selCodigoTributacao" class="fldGeneral" onselect="myFunction"></select>

    <p id="lblBaseCalculo" name="lblBaseCalculo" class="lblGeneral">Base C�lculo</p>
    <input type="text" id="txtBaseCalculo" name="txtBaseCalculo" class="fldGeneral" title="Base C�lculo"></input>
        
    <p id="lblAliquota" name="lblAliquota" class="lblGeneral">Aliquota</p> 
    <input type="text" id="txtAliquota" name="txtAliquota" class="fldGeneral" title="Aliquota"></input>
    
    <p id="lblIVAST" name="lblIVAST" class="lblGeneral">IVA-ST</p> 
    <input type="text" id="txtIVAST" name="txtIVAST" class="fldGeneral" title="IVA-ST"></input>
    
    <p id="lblBCICMS" name="lblBCICMS" class="lblGeneral">BC ICMS</p> 
    <input type="text" id="txtBCICMS" name="txtBCICMS" class="fldGeneral" title="BC ICMS"></input>

    <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p> 
    <input type="text" id="txtObservacao" name="txtObservacao" class="fldGeneral" title="Observa��o"></input>

    <p id="lblVigInicio" name="lblVigInicio" class="lblGeneral">Vig�ncia Inicio</p> 
    <input type="text" id="txtVigInicio" name="txtVigInicio" class="fldGeneral"></input>
        
    <p id="lblVigFim" name="lblVigFim" class="lblGeneral">Vig�ncia Fim</p> 
    <input type="text" id="txtVigFim" name="txtVigFim" class="fldGeneral"></input>
    
    
    <input type="button" id="btnIncluir" name="btnIncluir" value="Incluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnVoltar" name="btnVoltar" value="Voltar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">    

    <p id="lblRodapeVigFim" name="lblRodapeVigFim" class="lblGeneral">Vig�ncia Fim</p> 
    <input type="text" id="txtRodapeVigFim" name="txtRodapeVigFim" class="fldGeneral"></input>
    
    <input type="button" id="btnFinalzar" name="btnFinalzar" value="Finalizar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnDeletar" name="btnDeletar" value="Deletar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">          
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnAvancar" name="btnAvancar" value="Avan�ar" LANGUAGE="javascript" class="btns">

    <!-- <textarea id="txtDicas" name="txtDicas" class="fldGeneral"></textarea> -->
    <div id="divDicas" name="divDicas" class="divGeneral" style="border-width:2px;border-style:solid;border-color:Gray;overflow:auto;font-Family:tahoma;font-Size:12px"></div>
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
</body>

</html>
