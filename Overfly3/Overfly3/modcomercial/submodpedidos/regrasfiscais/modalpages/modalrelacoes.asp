
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalrelacoesHtml" name="modalrelacoesHtml">

<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/regrasfiscais/modalpages/modalrelacoes.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/regrasfiscais/modalpages/modalrelacoes.js" & Chr(34) & "></script>" & vbCrLf
%>

<%

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nRegraFiscalID, sCaller, rsData, strSQL

sCaller = ""
nRegraFiscalID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nRegraFiscalID").Count    
    nRegraFiscalID = Request.QueryString("nRegraFiscalID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nRegraFiscalID = " & CStr(nRegraFiscalID) & ";"
Response.Write vbcrlf

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>


<script ID="wndJSProc" LANGUAGE="javascript">
<!--
//-->
</SCRIPT>
<!-- //@@ Eventos de grid -->
<!-- fg -->

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalRelacoes_BeforeEdit(fg, arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
    js_fg_modalRelacoes_AfterEdit(arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
    js_fg_modalRelacoesDblClick(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgMembrosElem EVENT=DblClick>
<!--
    js_fg_modalRelacoesDblClick(fgMembrosElem, fgMembrosElem.Row, fgMembrosElem.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgMembrosElem EVENT=AfterEdit>
<!--
    js_fgMembrosElem_modalRelacoes_AfterEdit(arguments[0], arguments[1]);
//-->
</SCRIPT>   

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
//<!--
 js_fg_modalRelacoes_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell id=" ">
<!--
 js_fg_EnterCell(fg);
//-->
</SCRIPT>   
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
<!--
 js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgMembrosElem EVENT=BeforeRowColChange>
//<!--
 js_fg_modalRelacoes_Membros_BeforeRowColChange(fgMembrosElem, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fgMembrosElem EVENT=EnterCell id="SCRIPT1">
<!--
 js_fg_EnterCell(fgMembrosElem);
//-->
</SCRIPT>   




</head>

<body id="modalrelacoesBody" name="modalrelacoesBody" LANGUAGE="javascript" onload="return window_onload()">
     <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>
    
     <div id="divFGMembrosElem" name="divFGMembrosElem" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgMembrosElem" name="fgMembrosElem" VIEWASTEXT>
        </object>
        <img id="Img1" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="Img2" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="Img3" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>
    
     <div id="divFGImpostos" name="divFGImpostos" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgImpostos" name="fgImpostos" VIEWASTEXT>
        </object>
        <img id="Img4" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="Img5" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="Img6" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
  
    <p id="lblDicas" name="lblDicas" class="lblGeneral">Dicas</p>
    <input type="checkbox" id="chkDicas" name="chkDicas" class="fldGeneral" title="Dicas de uso da Regra Fiscal"></input>
    
    <p id="lblVisao" name="lblVisao" class="lblGeneral">Vis�o</p>
    <select id="selVisao" name="selVisao" class="fldGeneral"  onchange="return cmbOnChange(this)"></select>            
    
    <p id="lblInc" name="lblInc" class="lblGeneral">Incluir</p>  
    <input type="checkbox" id="chkInc" name="chkInc" class="fldGeneral" title="Incluir registros"></input>
    <p id="lblElemento" name="lblElemento" class="lblGeneral">Elemento</p>
	<select id="selElementoID" name="selElementoID" class="fldGeneral"></select>
    <p id="lblGrupo" name="lblGrupo" class="lblGeneral">Grupo</p>
    <select id="selGrupo" name="selGrupo" class="fldGeneral"></select>	
    <p id="lblImposto" name="lblImposto" class="lblGeneral"  title="Imposto">Imposto</p>    
    <select id="selImposto" name="selImposto" class="fldGeneral"> <%
		 Set rsData = Server.CreateObject("ADODB.Recordset")
		    strSQL = "SELECT 0 AS fldID, '' AS fldName " &_
		            "UNION ALL " &_
		            
		            "SELECT DISTINCT ConceitoID AS fldID, Imposto AS fldName " &_
                        "FROM Conceitos a WITH(NOLOCK) " &_
                            "INNER JOIN RegrasFiscais_Impostos b WITH(NOLOCK) ON (b.ImpostoID = a.ConceitoID) " &_
                            "INNER JOIN RegrasFiscais c WITH(NOLOCK) ON (c.RegraFiscalID = b.RegraFiscalID) " &_
                            "INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) d ON (d.RegraFiscalID = c.RegraFiscalID) " &_
                            "INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(" & nRegraFiscalID & ") e ON (e.AgrupadorID = d.AgrupadorID) " & _
		            "WHERE (c.Nivel <= (SELECT TOP 1 aa.Nivel FROM RegrasFiscais aa WITH(NOLOCK) WHERE aa.RegraFiscalID = c.RegraFiscalID)) "
    					
			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				
				If ( CLng(rsData.Fields("fldID").Value) = CLng(0) ) Then
					Response.Write " SELECTED "
				End If

				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>
    </select>
    
    <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p> 
    <input type="text" id="txtObservacao" name="txtObservacao" class="fldGeneral" title="Observa��o"></input>

    <input type="button" id="btnRelacionar" name="btnRelacionar" value="Relacionar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnAtribuir" name="btnAtribuir" value="Atribuir" title = "Atribuir Grupo" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnVoltar" name="btnVoltar" value="Voltar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">    

    <input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnDeletar" name="btnDeletar" value="Deletar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">          
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnAvancar" name="btnFim" value="Avan�ar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">

    <!-- <textarea id="txtDicas" name="txtDicas" class="fldGeneral"></textarea> -->
    <div id="divDicas" name="divDicas" class="divGeneral" style="border-width:2px;border-style:solid;border-color:Gray;overflow:auto;font-Family:tahoma;font-Size:12px"></div>
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
</body>

</html>
