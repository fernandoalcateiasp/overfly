/********************************************************************
modalimpostos.js

Library javascript para o modalimpostos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport("dsoPesq");
var dsoImpostos = new CDatatransport("dsoImpostos");
var dsoGravaImpostos = new CDatatransport("dsoGravaImpostos");
var dsoCarregaComboCodTributacao = new CDatatransport("dsoCarregaComboCodTributacao");
var dsoDicas = new CDatatransport("dsoDicas");
var dsoComboImpostos = new CDatatransport("dsoComboImpostos");
var glb_RegraFiscalID = null;
var glb_RegraFiscalMaeID = null;
var glb_EstadoID = null;
var glb_Nivel = null;
var glb_Data = null;
var nBtnsWidth = 72;
var glb_CounterCmbsStatics = 0;
var glb_controlaDSO = 0;
var glb_Inicio = false;

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_ListarTimerInt = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_sMensagem = '';

// FINAL DE VARIAVEIS GLOBAIS ***************************************
     
// IMPLEMENTACAO DAS FUNCOES
    
/********************************************************************
Configura o html
********************************************************************/
function window_onload() 
{
    window_onload_1stPart();

    glb_RegraFiscalID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['RegraFiscalID'].value");
    glb_RegraFiscalMaeID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['RegraFiscalMaeID'].value");
    glb_EstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");
    glb_Nivel = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Nivel'].value");
    glb_Data = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['V_dtData'].value");

    // ajusta o body do html
    with (modalimpostosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 52;
    modalFrame.style.left = 0;
        
    // configuracao inicial do html
    setupPage();

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
}    
/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() 
{

    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Regras Fiscais');
    
    // btnOK nao e usado nesta modal
    with (btnOK) {
        disabled = true;
        style.visibility = 'hidden';
    }

    // btnCanc nao e usado nesta modal
    with (btnCanc) {
        style.visibility = 'hidden';
    }
   // texto da secao01
    secText('Regra Fiscal ' + glb_RegraFiscalID + ' - Impostos (3/5)', 1);
    loadDataAndTreatInterface();

    carregaVisao();

    if (glb_EstadoID != 1) 
    {
        chkInc.checked = false;
        chkInc.disabled = true;
        txtRodapeVigFim.disabled = true;
        btnFinalzar.disabled = true;
        btnGravar.disabled = true;
        btnDeletar.disabled = true;
    }
    else
        chkInc.checked = false;

    chkInc.onclick = chkInc_onclick;

    divDicas.style.visibility = 'hidden';
    //divDicas.readOnly = true;

    txtBaseCalculo.onkeypress = verifyNumericEnterNotLinked;
    txtBaseCalculo.setAttribute('verifyNumPaste', 1);
    txtBaseCalculo.setAttribute('thePrecision', 6, 1);
    txtBaseCalculo.setAttribute('theScale', 3, 1);
    txtBaseCalculo.setAttribute('minMax', new Array(0, 999999), 1);
    txtBaseCalculo.onfocus = selFieldContent;
    txtBaseCalculo.maxLength = 6;

    txtAliquota.onkeypress = verifyNumericEnterNotLinked;
    txtAliquota.setAttribute('verifyNumPaste', 1);
    txtAliquota.setAttribute('thePrecision', 5, 1);
    txtAliquota.setAttribute('theScale', 2, 1);
    txtAliquota.setAttribute('minMax', new Array(0, 99999), 1);
    txtAliquota.onfocus = selFieldContent;
    txtAliquota.maxLength = 5;

    txtIVAST.onkeypress = verifyNumericEnterNotLinked;
    txtIVAST.setAttribute('verifyNumPaste', 1);
    txtIVAST.setAttribute('thePrecision', 5, 1);
    txtIVAST.setAttribute('theScale', 2, 1);
    txtIVAST.setAttribute('minMax', new Array(0, 99999), 1);
    txtIVAST.onfocus = selFieldContent;
    txtIVAST.maxLength = 5;

    txtBCICMS.onkeypress = verifyNumericEnterNotLinked;
    txtBCICMS.setAttribute('verifyNumPaste', 1);
    txtBCICMS.setAttribute('thePrecision', 6, 1);
    txtBCICMS.setAttribute('theScale', 3, 1);
    txtBCICMS.setAttribute('minMax', new Array(0, 99999), 1);
    txtBCICMS.onfocus = selFieldContent;
    txtBCICMS.maxLength = 6;

    txtObservacao.maxLength = 30;
    txtRodapeVigFim.maxLength = 10;
    
    txtVigInicio.maxLength = 10;
    txtVigFim.maxLength = 10;

    selImposto.onchange = onchange_Impostos;

    selCodigoTributacao.onchange = onchange_CT;

    btnAvancar.onclick = AvancarModal;

    OcultaMostraCampos();

    txtVigInicio.value = glb_Data;

    fillGridImpostos();
}

function loadDataAndTreatInterface() 
{

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var x_gap, y_gap;

    topFree = parseInt(divMod01.style.height, 10) + ELEM_GAP;

   // Botao, label, CheckBox

    btnVoltar.style.top = parseInt(btnOK.currentStyle.top, 10) + 8;
    btnVoltar.style.left = 10;
    btnVoltar.style.width = FONT_WIDTH * 7 + 20;

    btnGravar.style.top = parseInt(btnOK.currentStyle.top, 10) + 8;
    btnGravar.style.left = parseInt(btnCanc.style.left, 10) + ELEM_GAP - 20;
    btnGravar.style.width = FONT_WIDTH * 7 + 20;

    //Ajuste do campo txtRodapeVigFim - Inicio
    txtRodapeVigFim.style.top = parseInt(btnOK.currentStyle.top, 10) + 8;
    txtRodapeVigFim.style.left = parseInt(btnGravar.style.left, 10) - 175;
    txtRodapeVigFim.style.width = FONT_WIDTH * 7 + 25;
    txtRodapeVigFim.maxLength = 10;

    lblRodapeVigFim.style.top = parseInt(btnGravar.currentStyle.top, 10) - 13;
    lblRodapeVigFim.style.left = parseInt(btnGravar.style.left, 10) - 175;
    lblRodapeVigFim.style.width = FONT_WIDTH * 7 + 20;
    //Ajuste do campo txtRodapeVigFim - Fim

    btnFinalzar.style.top = parseInt(btnOK.currentStyle.top, 10) + 8;
    btnFinalzar.style.left = parseInt(btnGravar.style.left, 10) - 80;
    btnFinalzar.style.width = FONT_WIDTH * 7 + 20;

    btnDeletar.style.top = parseInt(btnOK.currentStyle.top, 10) + 8;
    btnDeletar.style.left = parseInt(btnOK.style.left, 10) + ELEM_GAP + 150;
    btnDeletar.style.width = FONT_WIDTH * 7 + 20;


    btnAvancar.style.top = parseInt(btnOK.currentStyle.top, 10) + 8;
    btnAvancar.style.left = parseInt(btnOK.style.left, 10) + parseInt(btnCanc.style.left, 10) + ELEM_GAP - 20;
    btnAvancar.style.width = FONT_WIDTH * 7 + 20;

    adjustElementsInForm([['lblDicas', 'chkDicas', 3, 1, 0, 25],
                          ['lblVisao', 'selVisao', 5, 1, -5],
                          ['lblInc', 'chkInc', 3, 1],
                          ['lblImposto', 'selImposto', 10, 1, -10],
                          ['lblCodigoTributacao', 'selCodigoTributacao', 5, 1],
                          ['lblBaseCalculo', 'txtBaseCalculo', 10, 1],
                          ['lblAliquota', 'txtAliquota', 8, 1],
                          ['lblIVAST', 'txtIVAST', 8, 1],
                          ['lblBCICMS', 'txtBCICMS', 10, 1],
                          ['lblObservacao', 'txtObservacao', 14, 1],
                          ['lblVigInicio', 'txtVigInicio', 10, 1],
                          ['lblVigFim', 'txtVigFim', 10, 1]], null, null, true);

    btnListar.style.top = parseInt(chkDicas.currentStyle.top, 10);
    btnListar.style.left = parseInt(btnOK.style.left, 10) + parseInt(btnCanc.style.left, 10) + ELEM_GAP - 20;
    btnListar.style.width = FONT_WIDTH * 7 + 20;

    btnIncluir.style.top = parseInt(chkDicas.currentStyle.top, 10);
    btnIncluir.style.left = parseInt(btnOK.style.left, 10) + parseInt(btnCanc.style.left, 10) + ELEM_GAP - 20;
    btnIncluir.style.width = FONT_WIDTH * 7 + 20;

    btnIncluir.style.visibility = 'hidden';
    
    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + parseInt(lblImposto.style.top) + 25;
        width = (80 + (2 * 10)) * FONT_WIDTH + 17 + 30;
        height = parseInt(btnOK.currentStyle.top, 10) - ELEM_GAP - 19;
    }
    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10) + 80 + 17 + 30;
        height = parseInt(divFG.style.height, 10) - 63;
    }

    with (divDicas.style) 
    {
        backgroundColor = 'transparent';
        top = parseInt(divFG.style.top, 10);
        left = parseInt(divFG.style.left, 10);
        width = parseInt(divFG.style.width, 10) + 70 + 60;
        height = parseInt(divFG.style.height, 10) - 62;
    }

    chkDicas.onclick = mostraEscondeDicas;

    dicas();

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;

}
/********************************************************************
Preenche o grid de impostos
********************************************************************/
function fillGridImpostos() {

    if (glb_ListarTimerInt != null) 
    {
        window.clearInterval(glb_ListarTimerInt);
        glb_ListarTimerInt = null; 
    }
    lockControlsInModalWin(true);

    glb_controlaDSO++;

    var imposto = '0';
    var bTudo = false; //chkTudu.checked

    var sWhere = '';

    if (selVisao.value == 0)
        sWhere = 'AND ((Vigencia.RegraFiscalID = ' + glb_RegraFiscalID + ') OR (Vigencia.RegraFiscalFimID = ' + glb_RegraFiscalID + ')) ';
    else if (selVisao.value == 1)
        sWhere = 'AND (((Vigencia.RegraFiscalID = ' + glb_RegraFiscalID + ') OR (Vigencia.RegraFiscalFimID = ' + glb_RegraFiscalID + ')) ' +
            'OR ((a.Nivel <> ' + glb_Nivel + ') AND (a.Nivel - ' + glb_Nivel + ' BETWEEN -1 AND 1))) ';

    //Select para mostrar Regra Mae e Regra Filha. BJBN
    var sSQL = 'SELECT DISTINCT 0 AS OK, b.RegraFiscalID, c.Imposto, f.ItemAbreviado AS CodigoTributacao, b.BaseCalculo, b.Aliquota, b.IVAST, b.BaseCalculoICMS, b.Observacao, Vigencia.dtVigenciaInicio, Vigencia.dtVigenciaFim, b.RegImpostoID, ISNULL(f.ItemMasculino,SPACE(1)) as CST, ' +
	                    'Vigencia.RegVigenciaInicioID AS RegVigenciaInicioID, Vigencia.RegVigenciaFimID AS RegVigenciaFimID, c.Ordem, Vigencia.RegraFiscalFimID ' +
	                'FROM RegrasFiscais a WITH(NOLOCK) ' +
	                    ' INNER JOIN dbo.fn_RegraFiscalElemento_Vigencia_tbl(NULL) Vigencia ON ((Vigencia.RegraFiscalID = a.RegraFiscalID) OR (Vigencia.RegraFiscalFimID = a.RegraFiscalID)) ' +
	                    'INNER JOIN RegrasFiscais_Impostos b WITH(NOLOCK) ON (Vigencia.RegImpostoID = b.RegImpostoID) ' +
		                'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ImpostoID) ' +
                        //'LEFT OUTER JOIN RegrasFiscais_Vigencias d WITH(NOLOCK) ON (d.RegImpostoID = b.RegImpostoID) AND (d.Vigente = 1) ' +
                        //'LEFT OUTER JOIN RegrasFiscais_Vigencias g WITH(NOLOCK) ON (g.RegImpostoID = b.RegImpostoID) AND (g.Vigente = 0) ' +
                        'INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) e ON (e.RegraFiscalID = a.RegraFiscalID) ' +
		                'LEFT OUTER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (f.ItemID = b.CodigoTributacaoID) ' +
		                'INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') g ON (g.AgrupadorID = e.AgrupadorID) ' +
	                'WHERE ((b.ImpostoID = ' + imposto + ') OR (' + imposto + ' = 0)) ' + sWhere +
	            'ORDER BY c.Ordem';

    dsoImpostos.SQL = sSQL;
    dsoImpostos.ondatasetcomplete = fillGridImpostos_DSC;
    dsoImpostos.refresh();
}

function fillGridImpostos_DSC()
{
    glb_controlaDSO--;
    glb_GridIsBuilding = true;

    var dTFormat;
    
    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    
    
   startGridInterface(fg);
   headerGrid(fg,  ['RegraID',
                    'Imposto',
                    'CT',
                    'OK',
                    'Base C�lculo',
                    'Aliquota',
                    'IVA-ST',
                    'BC ICMS',
                    'Observa��o',
                    'Vig�ncia Inicio',
                    'Vig�ncia Fim',
                    'RegraID',
                    'RegImpostoID',
                    'CST',
                    'RegVigenciaInicioID',
                    'RegVigenciaFimID'], [12, 13, 14, 15]);

   fillGridMask(fg, dsoImpostos, ['RegraFiscalID*', 'Imposto*', 'CodigoTributacao*', 'OK', 'BaseCalculo', 'Aliquota', 'IVAST', 'BaseCalculoICMS', 'Observacao', 'dtVigenciaInicio*', 'dtVigenciaFim*',
                                    'RegraFiscalFimID*', 'RegImpostoID*','CST*', 'RegVigenciaInicioID*', 'RegVigenciaFimID*'],
                                   ['', '', '', '', '999.999', '999.99', '999.99', '999.999', '', '99/99/9999', '99/99/9999', '','', '', '', ''],
                                   ['', '', '', '', '###.000', '###.00', '###.00', '###.000', '', dTFormat, dTFormat, '', '','', '', '']);

   fg.FrozenCols = 4;

   fg.MergeCells = 4;
   fg.MergeCol(0) = true;
   fg.MergeCol(1) = true;
   fg.MergeCol(2) = true;

   alignColsInGrid(fg, [0, 2, 4, 5, 6, 7]);

   // Linha de Totais
   gridHasTotalLine(fg, '', 0xC0C0C0, null, true,
                    [[getColIndexByColKey(fg, 'RegraFiscalID*'), '######', 'C']]);

   glb_aCelHint = new Array();

   for (i = 2; i < fg.Rows; i++) {
       glb_aCelHint[glb_aCelHint.length] = new Array(i, getColIndexByColKey(fg, 'CodigoTributacao*'), fg.TextMatrix(i, getColIndexByColKey(fg, 'CodigoTributacao*')) + ' ' + fg.TextMatrix(i, getColIndexByColKey(fg, 'CST*')));

   }

    //coloca o tipo das colunas
    with (fg) 
    {
        ColDataType(3) = 11;
    }

    paintReadOnlyRows();

    fg.Editable = true;
    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    if (glb_controlaDSO == 0)
        lockControlsInModalWin(false);
    glb_GridIsBuilding = false;
}

/********************************************************************
Fun��o Para Gravar a Linha do grid
********************************************************************/
function GravaImpostos(nTipoID) 
{
    lockControlsInModalWin(true);

    var sMensagem = '';
    var strPars = new String();
    var nBaseCalculo;
    var nAliquota;
    var nIVAST;
    var nBCICMS;
    var sObservacao;
    var dVigInicio;
    var dVigFim;
    var bOK;
    var nRegImpostoID;
    var dDataVigFim;
    var nRegVigenciaInicioID;
    var nRegVigenciaFimID = '';
    var nCurrRegraFiscalID = null;
    var nBytesAcum = 0;
    var nDataLen = 0;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_sResultado = '';

    strPars = '';

    if (nTipoID != 2) {
        for (var i = 1; i < fg.Rows; i++) 
        {
            bOK = fg.textMatrix(i, getColIndexByColKey(fg, 'OK'));

            if (bOK != 0) 
            {

                nBytesAcum = strPars.length;

                if ((nTipoID == 4) && (nRegVigenciaFimID != ''))
                    break;
                    
                if ((nCurrRegraFiscalID != glb_RegraFiscalID) && (nTipoID != 4))
                    break;

                if (nTipoID == 4)
                    fg.textMatrix(i, getColIndexByColKey(fg, 'dtVigenciaFim*')) = txtRodapeVigFim.value;
                                
                nRegImpostoID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegImpostoID*'));
                dDataVigFim = dateFormatToSearch(fg.textMatrix(i, getColIndexByColKey(fg, 'dtVigenciaFim*')));
                nRegVigenciaInicioID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegVigenciaInicioID*'));
                nRegVigenciaFimID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegVigenciaFimID*'));
                nBaseCalculo = replaceStr(fg.textMatrix(i, getColIndexByColKey(fg, 'BaseCalculo')), ',', '.');
                nAliquota = replaceStr(fg.textMatrix(i, getColIndexByColKey(fg, 'Aliquota')),',','.');
                nIVAST = replaceStr(fg.textMatrix(i, getColIndexByColKey(fg, 'IVAST')),',','.');
                nBCICMS = replaceStr(fg.textMatrix(i, getColIndexByColKey(fg, 'BaseCalculoICMS')),',','.');
                sObservacao = fg.textMatrix(i, getColIndexByColKey(fg, 'Observacao'));
      
                if (nBytesAcum >= glb_nMaxStringSize) 
                {
                    nBytesAcum = 0;

                    if (strPars != '') 
                    {
                        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                        strPars = '';
                        nDataLen = 0;
                    }
                }
                
                if (strPars == '')
                    strPars += '?nRegImpostoID=' + escape(nRegImpostoID);
                else
                    strPars += '&nRegImpostoID=' + escape(nRegImpostoID);

                strPars += '&nRegraFiscalID=' + escape(glb_RegraFiscalID);
                strPars += '&dDataVigFim=' + escape(dDataVigFim);
                strPars += '&nRegVigenciaInicioID=' + escape(nRegVigenciaInicioID);
                strPars += '&nRegVigenciaFimID=' + escape(nRegVigenciaFimID);
                strPars += '&nBaseCalculo=' + escape(nBaseCalculo);
                strPars += '&nAliquota=' + escape(nAliquota);
                strPars += '&nIVAST=' + escape(nIVAST);
                strPars += '&nBCICMS=' + escape(nBCICMS);

                sObservacao = replaceStr(sObservacao, ',', '^');
                strPars += '&sObservacao=' + escape(sObservacao);
                
                //if (((nRegVigenciaFimID == '') || (nRegVigenciaFimID == null)) && (nTipoID == 1))
                    //nTipoID = 4;

                strPars += '&nTipoID=' + escape(nTipoID);
                nDataLen++;
            }
        }
        if (nDataLen > 0)
            glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
    }
    else 
    {
        nBaseCalculo = txtBaseCalculo.value;
        nAliquota = txtAliquota.value;
        nIVAST = txtIVAST.value;
        nBCICMS = txtBCICMS.value;
        sObservacao = txtObservacao.value;
        dVigInicio = txtVigInicio.value;
        dVigFim = txtVigFim.value;

        if (dVigInicio == '')
            sMensagem += 'O campo Vigencia Inicio deve ser preenchido. \n';

        if (selCodigoTributacao.length == 0)
            sMensagem += 'Deve existir um C�digo de Tributacao selecionado. \n';

        if (selImposto.value == 0)
            sMensagem += 'Deve existir um imposto selecionado.';

        if (sMensagem.length > 0)
        {
            window.top.overflyGen.Alert(sMensagem);
            lockControlsInModalWin(false);
            return null;
        }

        if (nBytesAcum >= glb_nMaxStringSize) 
        {
            nBytesAcum = 0;

            if (strPars != '') {
                glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                strPars = '';
                nDataLen = 0;
            }
        }                       

        strPars += '?nRegraFiscalID=' + escape(glb_RegraFiscalID);
        strPars += '&nCodigoTributacaoID=' + escape(selCodigoTributacao.value);
        strPars += '&nImpostoID=' + escape(selImposto.value);
        strPars += '&nBaseCalculo=' + escape(nBaseCalculo);
        strPars += '&nAliquota=' + escape(nAliquota);
        strPars += '&nIVAST=' + escape(nIVAST);
        strPars += '&nBCICMS=' + escape(nBCICMS);

        sObservacao = replaceStr(sObservacao, ',', '^');
        strPars += '&sObservacao=' + escape(sObservacao);
        
        strPars += '&sVigInicio=' + escape(dateFormatToSearch(dVigInicio));
        strPars += '&sVigFim=' + escape(dateFormatToSearch(dVigFim));
        strPars += '&nTipoID=' + escape(nTipoID);

        nDataLen++;
    }
    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

    if ((nCurrRegraFiscalID != null) && (nCurrRegraFiscalID != glb_RegraFiscalID) && (nTipoID != 4)) 
    {
        window.top.overflyGen.Alert('Selecione somente elementos desta regra');
        lockControlsInModalWin(false);
    }
    else if ((nTipoID == 4) && (nRegVigenciaFimID != '')) 
    {
        window.top.overflyGen.Alert('Selecione somente elementos que ainda n�o est�o finalizados');
        lockControlsInModalWin(false);
    }
    else if (strPars != '') 
    {
        var _retConf = 1;
        
        if (nTipoID == 3)
            _retConf = window.top.overflyGen.Confirm('Voc� tem certeza?');

        if (_retConf == 1) 
        {
        
           sendDataToServer();
            /*dsoGravaImpostos.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/serverside/gravarimpostos.aspx' + strPars;
            dsoGravaImpostos.ondatasetcomplete = GravaImpostos_DSC;
            dsoGravaImpostos.refresh();*/
        }
        else
            lockControlsInModalWin(false);
    }
    else 
    {
        if (nTipoID != 2)
            window.top.overflyGen.Alert('Selecione algum imposto desta regra');
            
        lockControlsInModalWin(false);
    }
}

function sendDataToServer() 
{
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGravaImpostos.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/serverside/gravarimpostos.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGravaImpostos.ondatasetcomplete = GravaImpostos_DSC;
            dsoGravaImpostos.refresh();
        }
        else {
            if (glb_sMensagem != '') {
                if (window.top.overflyGen.Alert(glb_sMensagem) == 0)
                    return null;

                glb_sMensagem = '';

                lockControlsInModalWin(false);
            }
            else {
                lockControlsInModalWin(false);
                glb_ListarTimerInt = window.setInterval('fillGridImpostos()', 10, 'JavaScript');
            }
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
//        glb_ListarTimerInt = window.setInterval('fillGridImpostos()', 10, 'JavaScript');
    }
}

function GravaImpostos_DSC() 
{
    if (!(dsoGravaImpostos.recordset.BOF && dsoGravaImpostos.recordset.EOF)) 
    {
        if (dsoGravaImpostos.recordset['Mensagem'].value != null && dsoGravaImpostos.recordset['Mensagem'].value.length > 0) 
           glb_sMensagem = dsoGravaElementos.recordset['Mensagem'].value;
        else {
            glb_nPointToaSendDataToServer++;
            glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
        }
     }
    
}

function AvancarModal() 
{
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.visibility = 'hidden';

    var RegraFiscalID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'RegraFiscalID' + '\'' + '].value');
    var strPars = new String();

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');
    strPars += '&nRegraFiscalID=' + escape(RegraFiscalID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/modalpages/modalrelacoes.asp' + strPars;

    showModalWin(htmlPath, new Array(1000, 539));
}

function voltarModal()
{
    var aEmpresa = getCurrEmpresaData();
    var nPaisEmpresaID = aEmpresa[1];

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.visibility = 'hidden';

    strPars = '?sCaller=' + escape('S');
    strPars += '&nRegraFiscalID=' + escape(glb_RegraFiscalID);
    strPars += '&nPaisEmpresaID=' + escape(nPaisEmpresaID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/modalpages/modalelementos.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 539));

}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S', null);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null);
    }
    else if (ctl.id == btnListar.id) 
    {
        fillGridImpostos();
    }
    else if (ctl.id == btnFinalzar.id) {
        finalizarOnclick();
    }
    else if (ctl.id == btnGravar.id) {
        GravaImpostos(1);
    }
    else if (ctl.id == btnIncluir.id) {
        GravaImpostos(2);
    }
    else if (ctl.id == btnDeletar.id) 
    {
        if (glb_EstadoID == 1) 
        {
            GravaImpostos(3);
        }
        else 
        {
            window.top.overflyGen.Alert("Opera��o indispon�vel para Regras Fiscais no estado Ativo.");

            lockControlsInModalWin(false);
        }
    }
    else if (ctl.id == btnAvancar.id) 
    {
        lockControlsInModalWin(false);
        AvancarModal();
    }
    else if (ctl.id == btnVoltar.id) 
    {
        voltarModal();
        lockControlsInModalWin(false);
    }
}

function OcultaMostraCampos()
{
    if (chkInc.checked)
        selVisao.disabled = true;
    else
        selVisao.disabled = false;
    
    comboImposto();
    
    if (chkInc.checked) 
    {
        //Grid
        divFG.style.visibility = 'hidden';
        //Campos
        lblCodigoTributacao.style.visibility = 'inherit';
        selCodigoTributacao.style.visibility = 'inherit';
        lblRodapeVigFim.style.visibility = 'hidden';
        txtRodapeVigFim.style.visibility = 'hidden';
        lblBaseCalculo.style.visibility = 'inherit';
        txtBaseCalculo.style.visibility = 'inherit';
        lblAliquota.style.visibility = 'inherit';
        txtAliquota.style.visibility = 'inherit';
        lblIVAST.style.visibility = 'inherit';
        txtIVAST.style.visibility = 'inherit';
        lblBCICMS.style.visibility = 'inherit';
        txtBCICMS.style.visibility = 'inherit';
        lblObservacao.style.visibility = 'inherit';
        txtObservacao.style.visibility = 'inherit';
        lblVigInicio.style.visibility = 'inherit';
        txtVigInicio.style.visibility = 'inherit';
        lblVigFim.style.visibility = 'inherit';
        txtVigFim.style.visibility = 'inherit';
        //BTNs
        btnIncluir.style.visibility = 'inherit';
        btnListar.style.visibility = 'hidden';
        btnFinalzar.style.visibility = 'hidden';
        btnGravar.style.visibility = 'hidden';
        btnDeletar.style.visibility = 'hidden';
        btnAvancar.style.visibility = 'hidden';
        btnVoltar.style.visibility = 'hidden';

        selCodigoTributacao.disabled = !(selCodigoTributacao.length > 0);
    }
    else 
    {
        //Grid
        divFG.style.visibility = 'inherit';
        //Campos
        lblCodigoTributacao.style.visibility = 'hidden';
        selCodigoTributacao.style.visibility = 'hidden';        
        lblRodapeVigFim.style.visibility = 'inherit';
        txtRodapeVigFim.style.visibility = 'inherit';
        lblBaseCalculo.style.visibility = 'hidden';
        txtBaseCalculo.style.visibility = 'hidden';
        lblAliquota.style.visibility = 'hidden';
        txtAliquota.style.visibility = 'hidden';
        lblIVAST.style.visibility = 'hidden';
        txtIVAST.style.visibility = 'hidden';
        lblBCICMS.style.visibility = 'hidden';
        txtBCICMS.style.visibility = 'hidden';
        lblObservacao.style.visibility = 'hidden';
        txtObservacao.style.visibility = 'hidden';
        lblVigInicio.style.visibility = 'hidden';
        txtVigInicio.style.visibility = 'hidden';
        lblVigFim.style.visibility = 'hidden';
        txtVigFim.style.visibility = 'hidden';
        //BTNs
        btnIncluir.style.visibility = 'hidden';
        btnListar.style.visibility = 'inherit';
        btnFinalzar.style.visibility = 'inherit';
        btnGravar.style.visibility = 'inherit';
        btnDeletar.style.visibility = 'inherit';
        btnAvancar.style.visibility = 'inherit';
        btnVoltar.style.visibility = 'inherit';
    }

    selCodigoTributacao.disabled = !(selCodigoTributacao.length > 0);
}

function chkInc_onclick() 
{
    mostraEscondeDicas();
}

function finalizarOnclick() 
{
    if (txtRodapeVigFim.value.length > 0)
        GravaImpostos(4);
    else
    {
        if (window.top.overflyGen.Alert('Informe uma data de vig�ncia fim') == 0)
            return null;

        lockControlsInModalWin(false);
    }
}

function onchange_Impostos() 
{
    fg.Rows = 0;
    CarregaCombo();
}

function CarregaCombo()
{
    glb_CounterCmbsStatics = 1;

    var imposto;

    if (selImposto.selectedIndex <= 0)
    {
        if (glb_controlaDSO == 0)
            lockControlsInModalWin(false);

        clearComboEx(['selCodigoTributacao']);
        selCodigoTributacao.disabled = true;

        selCodigoTributacao.title = '';
        
        return null;
    }
    
    glb_controlaDSO++;

    if (selImposto.selectedIndex > 0)
        imposto = selImposto.value;
    
    //Codigo de Tributa��o
    setConnection(dsoCarregaComboCodTributacao);
    dsoCarregaComboCodTributacao.SQL = 'SELECT ItemID AS fldID, ItemAbreviado AS fldName, ItemMasculino AS Descricao ' +
	                                        'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
	                                        'WHERE (a.EstadoID = 2) AND (a.TipoID = 429) AND (a.Filtro LIKE \'%/' + imposto + '/%\') ' +
	                                        'ORDER BY a.Ordem';

    dsoCarregaComboCodTributacao.ondatasetcomplete = CarregaCombo_DSC;
    dsoCarregaComboCodTributacao.Refresh();
}

function CarregaCombo_DSC() 
{
    glb_controlaDSO--;
    
    var optionStr, optionValue;
    var aCmbsStatics = [selCodigoTributacao];
    var aDSOsStatics = [dsoCarregaComboCodTributacao];
    var i;
    var lFirstRecord;
    var nQtdCmbs = aCmbsStatics.length;

    glb_CounterCmbsStatics--;

    if (glb_CounterCmbsStatics == 0) 
    {
        for (i = 0; i < nQtdCmbs; i++) 
        {
            clearComboEx([aCmbsStatics[i].id]);

            lFirstRecord = true;

            selCodigoTributacao.title = '';
            
            while (!aDSOsStatics[i].recordset.EOF) 
            {
                optionStr = aDSOsStatics[i].recordset['fldName'].value;
                optionValue = aDSOsStatics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                oOption.title = aDSOsStatics[i].recordset['Descricao'].value;
                oOption.setAttribute('Descricao', aDSOsStatics[i].recordset['Descricao'].value, 1);
                aCmbsStatics[i].add(oOption);
                aDSOsStatics[i].recordset.MoveNext();
                lFirstRecord = false;
            }

            aCmbsStatics[i].disabled = !(aCmbsStatics[i].length > 0);
        }
    }

    onchange_CT();
    
    if (glb_controlaDSO == 0)
        lockControlsInModalWin(false);
}

function limpaCampos()
{
    txtBaseCalculo.value = '';
    txtAliquota.value = '';
    txtIVAST.value = '';
    txtBCICMS.value = '';
    txtObservacao.value = '';
    txtVigInicio.value = glb_Data;
    txtVigFim.value = '';
}

function mostraEscondeDicas()
{
    if (chkDicas.checked)
    {
        escondeCamposDicas();
        divFG.style.visibility = 'hidden';
        btnVoltar.style.visibility = 'hidden';
        txtRodapeVigFim.style.visibility = 'hidden';
        lblRodapeVigFim.style.visibility = 'hidden';
        btnFinalzar.style.visibility = 'hidden';
        btnGravar.style.visibility = 'hidden';
        btnDeletar.style.visibility = 'hidden';
        btnAvancar.style.visibility = 'hidden';
        divDicas.style.visibility = 'inherit';

        lblVisao.style.visibility = 'hidden';
        selVisao.style.visibility = 'hidden';        
    }
    else
    {
        divDicas.style.visibility = 'hidden';
        lblInc.style.visibility = 'inherit';
        chkInc.style.visibility = 'inherit';
        lblImposto.style.visibility = 'inherit';
        selImposto.style.visibility = 'inherit';

        lblVisao.style.visibility = 'inherit';
        selVisao.style.visibility = 'inherit';        
        
        OcultaMostraCampos();
    }
}

function dicas()
{
    var sSQL = '';
    var sWHERE = '';

    if (glb_Nivel == 0)
        sWHERE = 'WHERE a.RegraFiscalID = ' + glb_RegraFiscalID;
    else if (glb_Nivel == 1)
        sWHERE = 'WHERE a.RegraFiscalID = ' + glb_RegraFiscalMaeID;
    else if (glb_Nivel == 2)
        sWHERE = 'WHERE a.RegraFiscalID IN ' +
                    '(SELECT aa.RegraFiscalMaeID FROM RegrasFiscais aa WITH(NOLOCK) WHERE RegraFiscalID = ' + glb_RegraFiscalMaeID + ')';

    sSQL = 'SELECT ISNULL(a.Observacoes, \'\') AS Observacoes ' +
                'FROM RegrasFiscais a WITH(NOLOCK) ' +
                sWHERE;

    dsoDicas.SQL = sSQL;
    dsoDicas.ondatasetcomplete = dicas_DSC;
    dsoDicas.refresh();
}

function dicas_DSC()
{
    if ((!dsoDicas.recordset.EOF) && (!dsoDicas.recordset.BOF))
        divDicas.innerHTML = dsoDicas.recordset['Observacoes'].value;
}

function escondeCamposDicas()
{
    lblInc.style.visibility = 'hidden';
    chkInc.style.visibility = 'hidden';
    lblImposto.style.visibility = 'hidden';
    selImposto.style.visibility = 'hidden';
    lblCodigoTributacao.style.visibility = 'hidden';
    selCodigoTributacao.style.visibility = 'hidden';
    lblBaseCalculo.style.visibility = 'hidden';
    txtBaseCalculo.style.visibility = 'hidden';
    lblAliquota.style.visibility = 'hidden';
    txtAliquota.style.visibility = 'hidden';
    lblIVAST.style.visibility = 'hidden';
    txtIVAST.style.visibility = 'hidden';
    lblBCICMS.style.visibility = 'hidden';
    txtBCICMS.style.visibility = 'hidden';
    lblObservacao.style.visibility = 'hidden';
    txtObservacao.style.visibility = 'hidden';
    lblVigInicio.style.visibility = 'hidden';
    txtVigInicio.style.visibility = 'hidden';
    lblVigFim.style.visibility = 'hidden';
    txtVigFim.style.visibility = 'hidden';
    btnIncluir.style.visibility = 'hidden';
    btnListar.style.visibility = 'hidden';
}

function comboImposto() 
{
    lockControlsInModalWin(true);
    //Codigo de Tributa��o
    setConnection(dsoComboImpostos);

    glb_controlaDSO++;

    var sWhere = '';

    if (glb_Nivel == 1)
        sWhere = 'AND (e.AgrupadorID = ' + glb_RegraFiscalID + ') ';
    else if (glb_Nivel == 2)
        sWhere = 'AND (e.AgrupadorID = ' + glb_RegraFiscalMaeID + ') ';

    if (!chkInc.checked)
    {
        dsoComboImpostos.SQL = 'SELECT 0 AS fldID, \'\' AS fldName ' +
                                'UNION ALL SELECT DISTINCT ConceitoID AS fldID, Imposto AS fldName ' +
                                    'FROM Conceitos a WITH(NOLOCK) ' +
                                        'INNER JOIN RegrasFiscais_Impostos b WITH(NOLOCK) ON (b.ImpostoID = a.ConceitoID) ' +
                                        'INNER JOIN RegrasFiscais c WITH(NOLOCK) ON (c.RegraFiscalID = b.RegraFiscalID) ' +
                                        'INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) d ON (d.RegraFiscalID = c.RegraFiscalID) ' +
                                        'INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') e ON (e.AgrupadorID = d.AgrupadorID) ' +
                                    'WHERE (a.TipoConceitoID = 306) AND (a.PaisID = 130) AND (a.DestacaNota = 1) AND ' +
                                            '(c.Nivel <= ' + glb_Nivel + ') ' + sWhere;
    }
    else
    {
        dsoComboImpostos.SQL = 'SELECT 0 AS fldID, \'\' AS fldName ' +
                                'UNION ALL SELECT DISTINCT ConceitoID AS fldID, Imposto AS fldName ' +
                                    'FROM Conceitos a WITH(NOLOCK) ' +
                                        'INNER JOIN RegrasFiscais_Mapeamento b WITH(NOLOCK) ON (b.ImpostoID = a.ConceitoID) ' +
                                        'INNER JOIN RegrasFiscais c WITH(NOLOCK) ON (c.RegraFiscalID = b.RegraFiscalID) ' +
                                        'INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) d ON (d.RegraFiscalID = c.RegraFiscalID) ' +
                                        'INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') e ON (e.AgrupadorID = d.AgrupadorID) ' +
                                    'WHERE (a.TipoConceitoID = 306) AND (a.PaisID = 130) AND (a.DestacaNota = 1) AND ' +
                                            '(c.Nivel <= ' + glb_Nivel + ') AND ' +
                                            '(1 = (CASE WHEN (' + glb_Nivel + ' = 0) THEN CadastroNivel0 ELSE CadastroNivel12 END)) ' + sWhere;
    }

    dsoComboImpostos.ondatasetcomplete = comboImposto_DSC; 
    dsoComboImpostos.Refresh();
}

function comboImposto_DSC()
{
    glb_controlaDSO--;
    
    var optionStr;
    var optionValue;

    clearComboEx(['selImposto']);

    while (!dsoComboImpostos.recordset.EOF)
    {
        optionStr = dsoComboImpostos.recordset['fldName'].value;
        optionValue = dsoComboImpostos.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selImposto.add(oOption);
        dsoComboImpostos.recordset.MoveNext();
    }

    selImposto.disabled = !(selImposto.length > 0);

    CarregaCombo();
}

function onchange_CT() 
{
    if (selCodigoTributacao.disabled)
        return null;
    
    var sDescricao = '';

    sDescricao = selCodigoTributacao.options[selCodigoTributacao.selectedIndex].getAttribute('Descricao', 1);

    selCodigoTributacao.title = sDescricao;
}

function js_fg_modalImpostos_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) 
{
    //Forca celula read-only    
    if (!glb_GridIsBuilding)
    {
        if (glb_FreezeRolColChangeEvents)
            return true;

        if (cellIsLocked(NewRow, NewCol)) {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);

        GridLinhaTotal(fg);
    }
}

function cellIsLocked(nRow, nCol) 
{
    var retVal = false;
    
    if (nCol == getColIndexByColKey(fg, 'OK'))
        return retVal;
        
    if (((fg.textMatrix(nRow, getColIndexByColKey(fg, 'RegraFiscalID*')) != glb_RegraFiscalID) && (nRow > 1)) || (glb_EstadoID != 1))
    {
        retVal = true;
    }
    
    return retVal;
}

function js_modalImpostos_AfterEdit(Row, Col)
{
    if ((glb_GridIsBuilding) || (Row == 1))
        return null;
    
    if (fg.Editable)
    {
        if ((Col != getColIndexByColKey(fg, 'OK')) && (fg.textMatrix(Row, getColIndexByColKey(fg, 'RegraFiscalID*')) == glb_RegraFiscalID))
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;

        if ((fg.textMatrix(Row, getColIndexByColKey(fg, 'RegraFiscalID*')) != glb_RegraFiscalID) && (fg.ColDataType(Col) == 11) && (Col != getColIndexByColKey(fg, 'OK'))) 
        {
            if (fg.textMatrix(Row, Col) != 0)
                fg.textMatrix(Row, Col) = 0;
            else
                fg.textMatrix(Row, Col) = 1;
        }           
        
        if ((Col == getColIndexByColKey(fg, 'BaseCalculo')) || (Col == getColIndexByColKey(fg, 'Aliquota')) || 
            (Col == getColIndexByColKey(fg, 'IVAST')) || (Col = getColIndexByColKey(fg, 'BaseCalculoICMS')))
        {
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
        }
    }
}

function js_modalImpostos_BeforeEdit(grid, Row, Col) 
{
    if (glb_GridIsBuilding)
        return null;
    
    var nomeColuna = grid.ColKey(Col);

    var lastPos = nomeColuna.length - 1;

    // retira o asterisco do nome do campo
    if (nomeColuna.substr(lastPos, 1) == '*') {
        nomeColuna = nomeColuna.substr(0, lastPos);
    }

    if ((dsoImpostos.recordset[nomeColuna].type == 200) || (dsoImpostos.recordset[nomeColuna].type == 129))
        fieldMaxLength(fg, Col, dsoImpostos);

    GridLinhaTotal(fg);
}

function paintReadOnlyRows() 
{
    for (var i = 2; i < fg.Rows; i++) 
    {
        if ((fg.textMatrix(i, getColIndexByColKey(fg, 'RegraFiscalID*')) != glb_RegraFiscalID) || (glb_EstadoID != 1))
        {
            fg.Cell(6, i, 0, i, getColIndexByColKey(fg, 'OK') - 1) = 0XDCDCDC;
            fg.Cell(6, i, getColIndexByColKey(fg, 'OK') + 1, i, fg.Cols - 1) = 0XDCDCDC;
        }
    }
}

function GridLinhaTotal(grid) 
{
    var nContador = 0;

   for (i = 2; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, getColIndexByColKey(grid, 'OK')) != 0)
            nContador++;
    }
    
    if (grid.Rows > 1)
        grid.TextMatrix(1, getColIndexByColKey(grid, 'RegraFiscalID*')) = nContador;
}

function js_fg_modalImpostosDblClick(grid, Row, Col) 
{
    if (glb_GridIsBuilding)
        return null;

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if ((Col != getColIndexByColKey(grid, 'OK')))
        return true;

    // trap so header
    if (Row != 1)
        return true;

    var i;
    var nRegraFiscalID = 0;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);

    // limpa coluna se tem um check box checado
    for (i = 2; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }
    
    for (i = 2; i < grid.Rows; i++) 
    {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;

    }
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    window.focus();
    grid.focus();

    GridLinhaTotal(grid);
}

function carregaVisao() 
{
    var hint = new Array();

    hint[0] = 'Somente registros da pr�pria regra';
    hint[1] = 'Registros da pr�pria regra, regras de nivel superior e inferior';
    hint[2] = 'Registros da pr�pria regra, regras do mesmo nivel, superior e inferior';

    clearComboEx(['selVisao']);

    for (var i = 0; (i <= (glb_Nivel == 0 ? 1 : glb_Nivel)); i++) {
        var oOption = document.createElement("OPTION");
        oOption.text = i.toString();
        oOption.value = i;
        oOption.title = hint[i];
        selVisao.add(oOption);
    }
}

function cmbOnChange(cmb) 
{
    if ((cmb.id).toUpperCase() == 'SELVISAO')
        fillGridImpostos();
}