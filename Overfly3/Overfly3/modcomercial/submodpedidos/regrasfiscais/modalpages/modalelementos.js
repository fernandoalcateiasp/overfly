/********************************************************************
modalelementos.js

Library javascript para o modalelementos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoPesq = new CDatatransport("dsoPesq");
var dsoCombosElem = new CDatatransport("dsoCombosElem");
var dsoCombosGru = new CDatatransport("dsoCombosGru");
var dsoElementos = new CDatatransport("dsoElementos");
var dsoCombosMarcas = new CDatatransport("dsoCombosMarcas");
var dsoCombosProdutos = new CDatatransport("dsoCombosProdutos");
var dsoCombosPessoas = new CDatatransport("dsoCombosPessoas");
var dsoCombosFabricante = new CDatatransport("dsoCombosFabricante");
var dsoCombosDocumentoFederal = new CDatatransport("dsoCombosDocumentoFederal");
var dsoIncluirElementos = new CDatatransport("dsoIncluirElementos");
var dsoGravaElementos = new CDatatransport("dsoGravaElementos");
var dsoDicas = new CDatatransport("dsoDicas");
var dsoComboGrupo = new CDatatransport("dsoComboGrupo");
var glb_RegraFiscalID = null;
var glb_EstadoID = null;
var glb_RegraFiscalMaeID = null;
var glb_Nivel = null;
var glb_Data = null;
var glb_CounterCmbsStatics = 0;
var nBtnsWidth = 72;
var glb_Inicio = false;

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_ListarTimerInt = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_sMensagem = '';
// FINAL DE VARIAVEIS GLOBAIS ***************************************


// IMPLEMENTACAO DAS FUNCOES
/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();
    
    
    glb_RegraFiscalID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['RegraFiscalID'].value");
    glb_RegraFiscalMaeID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['RegraFiscalMaeID'].value");
    glb_EstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");
    glb_Nivel = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Nivel'].value");
    glb_Data = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['V_dtData'].value");

    if (glb_Data == null)
        glb_Data = '';    

    // ajusta o body do html
    with (modalelementosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 52;
    modalFrame.style.left = 0;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Regras Fiscais');
 
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // btnOK nao e usado nesta modal
    with (btnOK) {
        disabled = true;
        style.visibility = 'hidden';
    }
    // btnCanc nao e usado nesta modal
    with (btnCanc) {
        style.visibility = 'hidden';
    }
    // texto da secao01
    secText('Regra Fiscal ' + glb_RegraFiscalID + ' - Elementos (2/5)', 1);

    if (glb_EstadoID != 1) {
        chkInc.checked = false;
        chkInc.disabled = true;
        txtRodapeVigFim.disabled = true;
        btnFinalzar.disabled = true;
        btnGravar.disabled = true;
        btnDeletar.disabled = true;
    }
    else
        chkInc.checked = false;

    ajustarElementos();

    carregaVisao();

    //Carregar Combos Estaticos
    CarregaCombo();
    chkInc.onclick = chkInc_onclick;

    txtObservacao.maxLength = 30;
    txtCNAE.maxLength = 7;
    txtNCM.maxLength = 8;
    txtProdutoID.maxLength = 10;
    txtPessoaID.maxLength = 10;
    txtDocFederal.maxLength = 20;
    txtFantasia.maxLength = 20;
    txtVigInicio.maxLength = 10;
    txtVigFim.maxLength = 10;
    txtRodapeVigFim.maxLength = 10;


    txtProdutoID.onkeypress = preencheProduto_KeyPress;
    txtDescricao.onkeypress = preencheProduto_KeyPress;
    txtPessoaID.onkeypress = preenchePessoa_KeyPress;
    txtDocFederal.onkeypress = preenchePessoa_KeyPress;
    txtFantasia.onkeypress = preenchePessoa_KeyPress;
   
    dicas();
}

function preencheProduto_KeyPress() {
    if (event.keyCode == 13) {
        CarregaComboProdutos();
    }
}
function preenchePessoa_KeyPress() {
    if (event.keyCode == 13) {
        CarregaComboPessoa();
    }
}
function ajustarElementos() {

    // ajusta elementos da janela
    var topFree;
    topFree = parseInt(divMod01.style.height, 10) + ELEM_GAP;
    OcultaMostraCampos();

    // Bot�os

    btnVoltar.style.top = parseInt(btnOK.currentStyle.top, 10) + 7;
    btnVoltar.style.left = 10;
    btnVoltar.style.width = FONT_WIDTH * 7 + 20;

    btnGravar.style.top = parseInt(btnOK.currentStyle.top, 10) + 7;
    btnGravar.style.left = parseInt(btnCanc.style.left, 10) + ELEM_GAP - 20;
    btnGravar.style.width = FONT_WIDTH * 7 + 20;

    btnFinalzar.style.top = parseInt(btnOK.currentStyle.top, 10) + 7;
    btnFinalzar.style.left = parseInt(btnGravar.style.left, 10) - 80;
    btnFinalzar.style.width = FONT_WIDTH * 7 + 20;

    btnDeletar.style.top = parseInt(btnOK.currentStyle.top, 10) + 7;
    btnDeletar.style.left = parseInt(btnOK.style.left, 10) + ELEM_GAP + 150;
    btnDeletar.style.width = FONT_WIDTH * 7 + 20;


    btnAvancar.style.top = parseInt(btnOK.currentStyle.top, 10) + 7;
    btnAvancar.style.left = parseInt(btnOK.style.left, 10) + parseInt(btnCanc.style.left, 10) + ELEM_GAP - 20;
    btnAvancar.style.width = FONT_WIDTH * 7 + 20;

    btnIncluir.style.top = 45;
    btnIncluir.style.left = parseInt(btnAvancar.style.left, 10);
    btnIncluir.style.width = FONT_WIDTH * 7 + 20;

    adjustComboMultiple();

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + 100;
        width = (80 + (2 * 10)) * FONT_WIDTH + 47;
        height = parseInt(btnOK.currentStyle.top, 10) - ELEM_GAP - 72;
    }
    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10) + 80 + 17 + 30;
        height = parseInt(divFG.style.height, 10) - 60;
    }

    divDicas.style.visibility = 'hidden';
    //divDicas.readOnly = true;

    with (divDicas.style) 
    {
        backgroundColor = 'transparent';
        top = parseInt(divFG.style.top, 10);
        left = parseInt(divFG.style.left, 10);
        width = parseInt(divFG.style.width, 10) + 70 + 60;
        height = parseInt(divFG.style.height, 10) - 60;
    }
    
    chkDicas.onclick = mostraEscondeDicas;
    txtVigInicio.value = glb_Data;
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
   

}
function chkInc_onclick() {

    lockControlsInModalWin(true);
    CarregaCombo();
    adjustComboMultiple();
    
    if (chkInc.checked == true) {
        txtRodapeVigFim.disabled = true;
        btnFinalzar.disabled = true;
        btnGravar.disabled = true;
        btnDeletar.disabled = true;
    }
    else {
        txtRodapeVigFim.disabled = false;
        btnFinalzar.disabled = false;
        btnGravar.disabled = false;
        btnDeletar.disabled = false;
    }
    //mostraEscondeDicas(); //OcultaMostraCampos();

}
function adjustComboMultiple() 
{
    var nHeight = (chkInc.checked ? 440 : 70 );

    // Tamanho do Combo Multiple
    selOrigem.style.height = nHeight;
    selDestino.style.height = nHeight;
    selProdutoID.style.height = nHeight;
    selPessoas.style.height = nHeight;
}
/********************************************************************
Mostra/Oculta o Grid
********************************************************************/
function OcultaMostraCampos(elementoDefault) 
{
    if (chkInc.checked)
        selVisao.disabled = true;
    else
        selVisao.disabled = selElemento.disabled;

    if ((selElemento.value == 970) || (elementoDefault == 970)) // CFOP
    {
        divFG.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblCFOP.style.visibility = 'inherit';
        selCFOP.style.visibility = 'inherit';
        lblFinalidade.style.visibility = 'hidden';
        selFinalidade.style.visibility = 'hidden';
        lblNCM.style.visibility =   'hidden';
        txtNCM.style.visibility = 'hidden';
        txtCNAE.style.visibility = 'hidden';
        lblCNAE.style.visibility = 'hidden';
        lblUFOrigem.style.visibility = 'hidden';
        selOrigem.style.visibility = 'hidden';
        lblUFDestino.style.visibility = 'hidden';
        selDestino.style.visibility = 'hidden';
        lblProdutoID.style.visibility = 'hidden';
        txtProdutoID.style.visibility = 'hidden';
        lblDescricao.style.visibility = 'hidden';
        txtDescricao.style.visibility = 'hidden';
        btnPesquisar.style.visibility = 'hidden';
        lblComboProdutos.style.visibility = 'hidden';
        selProdutoID.style.visibility = 'hidden';
        lblPessoaID.style.visibility = 'hidden';
        txtPessoaID.style.visibility = 'hidden';
        lblDocFederal.style.visibility = 'hidden';
        txtDocFederal.style.visibility = 'hidden';
        lblFantasia.style.visibility = 'hidden';
        txtFantasia.style.visibility = 'hidden';
        lblPessoas.style.visibility = 'hidden';
        selPessoas.style.visibility = 'hidden';
        lblUF.style.visibility = 'hidden';
        chkUF.style.visibility = 'hidden';
        btnVoltar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        txtRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnFinalzar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnGravar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnDeletar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnAvancar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        chkExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnIncluir.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnListar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');

        adjustElementsInForm([['lblDicas', 'chkDicas', 3, 2, 0, -23],
                              ['lblVisao', 'selVisao', 5, 2, -5],
                              ['lblInc', 'chkInc', 3, 2],
                              ['lblElemento', 'selElemento', 10, 2, -15],
                              ['lblCFOP', 'selCFOP', 30, 2],
                              ['lblGrupo', 'selGrupo', 6, 2],
                              ['lblExcecao', 'chkExcecao', 3, 2],
                              ['lblObservacao', 'txtObservacao', 20, 2],
                              ['lblVigInicio', 'txtVigInicio', 10, 2],
                              ['lblVigFim', 'txtVigFim', 10, 2],
                              //['btnIncluir', 'btn', nBtnsWidth, 2, 122],
                              ['lblRodapeVigFim', 'txtRodapeVigFim', 10, 10, 300, 113],
                              ['btnListar', 'btn', nBtnsWidth, 2, 910]], null, null, true);

    }
    else if (selElemento.value == 971) // Finalidade
    {
        divFG.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblCFOP.style.visibility = 'hidden';
        selCFOP.style.visibility = 'hidden';
        lblFinalidade.style.visibility = 'inherit';
        selFinalidade.style.visibility = 'inherit';
        lblNCM.style.visibility = 'hidden';
        txtNCM.style.visibility = 'hidden';
        txtCNAE.style.visibility = 'hidden';
        lblCNAE.style.visibility = 'hidden';
        lblUFOrigem.style.visibility = 'hidden';
        selOrigem.style.visibility = 'hidden';
        lblUFDestino.style.visibility = 'hidden';
        selDestino.style.visibility = 'hidden';
        lblExcecao.style.visibility = 'inherit';
        chkExcecao.style.visibility = 'inherit';
        lblProdutoID.style.visibility = 'hidden';
        txtProdutoID.style.visibility = 'hidden';
        lblDescricao.style.visibility = 'hidden';
        txtDescricao.style.visibility = 'hidden';
        btnPesquisar.style.visibility = 'hidden';
        lblComboProdutos.style.visibility = 'hidden';
        selProdutoID.style.visibility = 'hidden';
        lblPessoaID.style.visibility = 'hidden';
        txtPessoaID.style.visibility = 'hidden';
        lblDocFederal.style.visibility = 'hidden';
        txtDocFederal.style.visibility = 'hidden';
        lblFantasia.style.visibility = 'hidden';
        txtFantasia.style.visibility = 'hidden';
        lblPessoas.style.visibility = 'hidden';
        selPessoas.style.visibility = 'hidden';
        lblUF.style.visibility = 'hidden';
        chkUF.style.visibility = 'hidden';
        btnVoltar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        txtRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnFinalzar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnGravar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnDeletar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnAvancar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        chkExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnIncluir.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnListar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');

        adjustElementsInForm([['lblDicas', 'chkDicas', 3, 2, 0, -23],
                              ['lblVisao', 'selVisao', 5, 2, -5],
                              ['lblInc', 'chkInc', 3, 2],
                              ['lblElemento', 'selElemento', 15, 2, -15],
                              ['lblFinalidade', 'selFinalidade', 30, 2],
                              ['lblGrupo', 'selGrupo', 5, 2],
                              ['lblExcecao', 'chkExcecao', 3, 2],
                              ['lblObservacao', 'txtObservacao', 20, 2],
                              ['lblVigInicio', 'txtVigInicio', 10, 2],
                              ['lblVigFim', 'txtVigFim', 10, 2],
                              //['btnIncluir', 'btn', nBtnsWidth, 2, 100],
                              ['lblRodapeVigFim', 'txtRodapeVigFim', 10, 10, 300, 113],
                              ['btnListar', 'btn', nBtnsWidth, 2, 910]], null, null, true);
    }
    else if (selElemento.value == 972)// Localidade
    {
        divFG.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblCFOP.style.visibility = 'hidden';
        selCFOP.style.visibility = 'hidden';
        lblFinalidade.style.visibility = 'hidden';
        selFinalidade.style.visibility = 'hidden';
        lblNCM.style.visibility = 'hidden';
        txtNCM.style.visibility = 'hidden';
        txtCNAE.style.visibility = 'hidden';
        lblCNAE.style.visibility = 'hidden';
        lblUFOrigem.style.visibility = 'inherit';
        selOrigem.style.visibility = 'inherit';
        lblUFDestino.style.visibility = 'inherit';
        selDestino.style.visibility = 'inherit';
        lblProdutoID.style.visibility = 'hidden';
        txtProdutoID.style.visibility = 'hidden';
        lblDescricao.style.visibility = 'hidden';
        txtDescricao.style.visibility = 'hidden';
        btnPesquisar.style.visibility = 'hidden';
        lblComboProdutos.style.visibility = 'hidden';
        selProdutoID.style.visibility = 'hidden';
        lblPessoaID.style.visibility = 'hidden';
        txtPessoaID.style.visibility = 'hidden';
        lblDocFederal.style.visibility = 'hidden';
        txtDocFederal.style.visibility = 'hidden';
        lblFantasia.style.visibility = 'hidden';
        txtFantasia.style.visibility = 'hidden';
        lblPessoas.style.visibility = 'hidden';
        selPessoas.style.visibility = 'hidden';
        btnVoltar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        txtRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnFinalzar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnGravar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnDeletar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnAvancar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        chkExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblUF.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        chkUF.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnIncluir.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnListar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');


        adjustElementsInForm([['lblDicas', 'chkDicas', 3, 2, 0, -23],
                              ['lblVisao', 'selVisao', 5, 2, -5],
                              ['lblInc', 'chkInc', 3, 2],
                              ['lblElemento', 'selElemento', 11, 2, -15],
                              ['lblUFOrigem', 'selOrigem', 6, 2],
                              ['lblUFDestino', 'selDestino', 6, 2],
                              ['lblUF','chkUF',3,2],
                              ['lblGrupo', 'selGrupo', 5, 2],
                              ['lblExcecao', 'chkExcecao', 2, 2],
                              ['lblObservacao', 'txtObservacao', 20, 2],
                              ['lblVigInicio', 'txtVigInicio', 10, 2],
                              ['lblVigFim', 'txtVigFim', 10, 2],
                              //['btnIncluir', 'btn', nBtnsWidth, 2, 215],
                              ['lblRodapeVigFim', 'txtRodapeVigFim', 10, 10, 300, 113],
                              ['btnListar', 'btn', nBtnsWidth, 2, 910]], null, null, true);

        chkUF.checked = false;                              

    }
    else if (selElemento.value == 973) //NCM
    {
        divFG.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblCFOP.style.visibility = 'hidden';
        selCFOP.style.visibility = 'hidden';
        lblFinalidade.style.visibility = 'hidden';
        selFinalidade.style.visibility = 'hidden';
        lblNCM.style.visibility = 'inherit';
        txtNCM.style.visibility = 'inherit';
        txtCNAE.style.visibility = 'hidden';
        lblCNAE.style.visibility = 'hidden';
        lblUFOrigem.style.visibility = 'hidden';
        selOrigem.style.visibility = 'hidden';
        lblUFDestino.style.visibility = 'hidden';
        selDestino.style.visibility = 'hidden';
        lblProdutoID.style.visibility = 'hidden';
        txtProdutoID.style.visibility = 'hidden';
        lblDescricao.style.visibility = 'hidden';
        txtDescricao.style.visibility = 'hidden';
        btnPesquisar.style.visibility = 'hidden';
        lblComboProdutos.style.visibility = 'hidden';
        selProdutoID.style.visibility = 'hidden';
        lblPessoaID.style.visibility = 'hidden';
        txtPessoaID.style.visibility = 'hidden';
        lblDocFederal.style.visibility = 'hidden';
        txtDocFederal.style.visibility = 'hidden';
        lblFantasia.style.visibility = 'hidden';
        txtFantasia.style.visibility = 'hidden';
        lblPessoas.style.visibility = 'hidden';
        selPessoas.style.visibility = 'hidden';
        lblUF.style.visibility = 'hidden';
        chkUF.style.visibility = 'hidden';
        btnVoltar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        txtRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnFinalzar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnGravar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnDeletar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnAvancar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        chkExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnIncluir.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnListar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');


        adjustElementsInForm([['lblDicas', 'chkDicas', 3, 2, 0, -23],
                              ['lblVisao', 'selVisao', 5, 2, -5],
                              ['lblInc', 'chkInc', 3, 2],
                              ['lblElemento', 'selElemento', 10, 2, -15],
                              ['lblNCM', 'txtNCM', 10, 2],
                              ['lblGrupo', 'selGrupo', 5, 2],
                              ['lblExcecao', 'chkExcecao', 3, 2],
                              ['lblObservacao', 'txtObservacao', 20, 2],
                              ['lblVigInicio', 'txtVigInicio', 10, 2],
                              ['lblVigFim', 'txtVigFim', 10, 2],
                              //['btnIncluir', 'btn', nBtnsWidth, 2, 290],
                              ['lblRodapeVigFim', 'txtRodapeVigFim', 10, 10, 300, 113],
                              ['btnListar', 'btn', nBtnsWidth, 2, 910]], null, null, true);

    }
    else if (selElemento.value == 974)// CNAE 
    {

        divFG.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblCFOP.style.visibility = 'hidden';
        selCFOP.style.visibility = 'hidden';
        lblFinalidade.style.visibility = 'hidden';
        selFinalidade.style.visibility = 'hidden';
        lblNCM.style.visibility = 'hidden';
        txtNCM.style.visibility = 'hidden';
        txtCNAE.style.visibility = 'inherit';
        lblCNAE.style.visibility = 'inherit';
        lblUFOrigem.style.visibility = 'hidden';
        selOrigem.style.visibility = 'hidden';
        lblUFDestino.style.visibility = 'hidden';
        selDestino.style.visibility = 'hidden';
        lblProdutoID.style.visibility = 'hidden';
        txtProdutoID.style.visibility = 'hidden';
        lblDescricao.style.visibility = 'hidden';
        txtDescricao.style.visibility = 'hidden';
        btnPesquisar.style.visibility = 'hidden';
        lblComboProdutos.style.visibility = 'hidden';
        selProdutoID.style.visibility = 'hidden';
        lblPessoaID.style.visibility = 'hidden';
        txtPessoaID.style.visibility = 'hidden';
        lblDocFederal.style.visibility = 'hidden';
        txtDocFederal.style.visibility = 'hidden';
        lblFantasia.style.visibility = 'hidden';
        txtFantasia.style.visibility = 'hidden';
        lblPessoas.style.visibility = 'hidden';
        selPessoas.style.visibility = 'hidden';
        lblUF.style.visibility = 'hidden';
        chkUF.style.visibility = 'hidden';
        btnVoltar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        txtRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnFinalzar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnGravar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnDeletar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnAvancar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        chkExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnIncluir.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnListar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');

        adjustElementsInForm([['lblDicas', 'chkDicas', 3, 2, 0, -23],
                              ['lblVisao', 'selVisao', 5, 2, -5],
                              ['lblInc', 'chkInc', 3, 2],
                              ['lblElemento', 'selElemento', 10, 2, -15],
                              ['lblCNAE', 'txtCNAE', 10, 2],
                              ['lblGrupo', 'selGrupo', 5, 2],
                              ['lblExcecao', 'chkExcecao', 3, 2],
                              ['lblObservacao', 'txtObservacao', 20, 2],
                              ['lblVigInicio', 'txtVigInicio', 10, 2],
                              ['lblVigFim', 'txtVigFim', 10, 2],
                              //['btnIncluir', 'btn', nBtnsWidth, 2, 290 ],
                              ['lblRodapeVigFim', 'txtRodapeVigFim', 10, 10, 300, 113],
                              ['btnListar', 'btn', nBtnsWidth, 2, 910]], null, null, true);
    }
    else if (selElemento.value == 975)// produto
    {

        divFG.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblCFOP.style.visibility = 'hidden';
        selCFOP.style.visibility = 'hidden';
        lblFinalidade.style.visibility = 'hidden';
        selFinalidade.style.visibility = 'hidden';
        lblNCM.style.visibility = 'hidden';
        txtNCM.style.visibility = 'hidden';
        txtCNAE.style.visibility = 'hidden';
        lblCNAE.style.visibility = 'hidden';
        lblUFOrigem.style.visibility = 'hidden';
        selOrigem.style.visibility = 'hidden';
        lblUFDestino.style.visibility = 'hidden';
        selDestino.style.visibility = 'hidden';
        lblProdutoID.style.visibility = 'inherit';
        txtProdutoID.style.visibility = 'inherit';
        lblDescricao.style.visibility = 'inherit';
        txtDescricao.style.visibility = 'inherit';
        btnPesquisar.style.visibility = 'inherit';
        lblComboProdutos.style.visibility = 'inherit';
        selProdutoID.style.visibility = 'inherit';
        lblPessoaID.style.visibility = 'hidden';
        txtPessoaID.style.visibility = 'hidden';
        lblDocFederal.style.visibility = 'hidden';
        txtDocFederal.style.visibility = 'hidden';
        lblFantasia.style.visibility = 'hidden';
        txtFantasia.style.visibility = 'hidden';
        lblPessoas.style.visibility = 'hidden';
        selPessoas.style.visibility = 'hidden';
        lblUF.style.visibility = 'hidden';
        chkUF.style.visibility = 'hidden';
        btnVoltar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        txtRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnFinalzar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnGravar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnDeletar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnAvancar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        chkExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnIncluir.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnListar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');

        adjustElementsInForm([['lblDicas', 'chkDicas', 3, 2, 0, -23],
                              ['lblVisao', 'selVisao', 5, 2, -5],
                              ['lblInc', 'chkInc', 3, 2],
                              ['lblElemento', 'selElemento', 10, 2, -15],
                              ['lblProdutoID', 'txtProdutoID', 8, 2],
                              ['lblDescricao', 'txtDescricao', 30, 2],
                              ['btnPesquisar', 'btn', 15, 2],
                              ['lblComboProdutos', 'selProdutoID', 35, 2],
                              ['lblGrupo', 'selGrupo', 5, 2],
                              ['lblExcecao', 'chkExcecao', 3, 3, 2],
                              ['lblObservacao', 'txtObservacao', 20, 3],
                              ['lblVigInicio', 'txtVigInicio', 10, 3],
                              ['lblVigFim', 'txtVigFim', 10, 3],
                              //['btnIncluir', 'btn', nBtnsWidth, 2, 910],
                              ['lblRodapeVigFim', 'txtRodapeVigFim', 10, 10, 300, 113],
                              ['btnListar', 'btn', nBtnsWidth, 2, 910]], null, null, true);
    }
    else if (selElemento.value == 976)// Pessoa
    {

        divFG.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblCFOP.style.visibility = 'hidden';
        selCFOP.style.visibility = 'hidden';
        lblFinalidade.style.visibility = 'hidden';
        selFinalidade.style.visibility = 'hidden';
        lblNCM.style.visibility = 'hidden';
        txtNCM.style.visibility = 'hidden';
        txtCNAE.style.visibility = 'hidden';
        lblCNAE.style.visibility = 'hidden';
        lblUFOrigem.style.visibility = 'hidden';
        selOrigem.style.visibility = 'hidden';
        lblUFDestino.style.visibility = 'hidden';
        selDestino.style.visibility = 'hidden';
        lblProdutoID.style.visibility = 'hidden';
        txtProdutoID.style.visibility = 'hidden';
        lblDescricao.style.visibility = 'hidden';
        txtDescricao.style.visibility = 'hidden';
        btnPesquisar.style.visibility = 'inherit';
        lblComboProdutos.style.visibility = 'hidden';
        selProdutoID.style.visibility = 'hidden';
        lblPessoaID.style.visibility = 'inherit';
        txtPessoaID.style.visibility = 'inherit';
        lblDocFederal.style.visibility = 'inherit';
        txtDocFederal.style.visibility = 'inherit';
        lblFantasia.style.visibility = 'inherit';
        txtFantasia.style.visibility = 'inherit';
        lblPessoas.style.visibility = 'inherit';
        selPessoas.style.visibility = 'inherit';
        lblUF.style.visibility = 'hidden';
        chkUF.style.visibility = 'hidden';
        btnVoltar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        txtRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblRodapeVigFim.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnFinalzar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnGravar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnDeletar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        btnAvancar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');
        lblExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        chkExcecao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtObservacao.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigInicio.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        lblVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        txtVigFim.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnIncluir.style.visibility = (chkInc.checked ? 'inherit' : 'hidden');
        btnListar.style.visibility = (chkInc.checked ? 'hidden' : 'inherit');

        adjustElementsInForm([['lblDicas', 'chkDicas', 3, 2, 0, -23],
                              ['lblVisao', 'selVisao', 5, 2, -5],
                              ['lblInc', 'chkInc', 3, 2],
                              ['lblElemento', 'selElemento', 10, 2, -15],
                              ['lblPessoaID', 'txtPessoaID', 8, 2],
                              ['lblDocFederal', 'txtDocFederal', 15, 2],
                              ['lblFantasia', 'txtFantasia', 15, 2],
                              ['btnPesquisar', 'btn', 15, 2],
                              ['lblPessoas', 'selPessoas', 35, 2],
                              ['lblGrupo', 'selGrupo', 5, 2],
                              ['lblExcecao', 'chkExcecao', 3, 3, 2],
                              ['lblObservacao', 'txtObservacao', 20, 3],
                              ['lblVigInicio', 'txtVigInicio', 10, 3],
                              ['lblVigFim', 'txtVigFim', 10, 3],
                              //['btnIncluir', 'btn', nBtnsWidth, 2, 910],
                              ['lblRodapeVigFim', 'txtRodapeVigFim', 10, 10, 300, 113],
                              ['btnListar', 'btn', nBtnsWidth, 2, 910]], null, null, true);
    }

    btnIncluir.style.width = FONT_WIDTH * 7 + 20;
    btnListar.style.width = FONT_WIDTH * 7 + 20;
    
}
/********************************************************************
Preenche o grid de Mensagens
********************************************************************/
function fillGridElementos(sWhere) {


    var aHiddenColls = new Array();
    var sSQL = ' SELECT DISTINCT 0 AS OK , aa.RegElementoID, aa.RegraFiscalID, b.ItemMasculino AS ElementoID, ISNULL(CONVERT(VARCHAR(10), c.OperacaoID) + \' \' + c.Operacao, SPACE(1)) AS CFOPID, ISNULL(d.ItemMasculino, SPACE(1)) AS FinalidadeID,' +
                ' ISNULL(e.CodigoLocalidade2, SPACE(1)) AS UFOrigem, ISNULL(f.CodigoLocalidade2, SPACE(1)) AS UFDestino, ISNULL(aa.NCM, SPACE(1)) AS NCM, ISNULL(aa.CNAE, SPACE(1)) AS CNAE, ISNULL(aa.ProdutoID, 0) AS ProdutoID,  ISNULL(g.Conceito, SPACE(1)) AS Produto, ISNULL(h.PessoaID, 0) AS PessoaID, ISNULL(h.Fantasia, SPACE(1)) AS Pessoa,' +
                ' ISNULL(i.Numero, SPACE(1)) AS Numero, aa.Grupo, aa.Observacao ,aa.Excecao, ISNULL(aa.Deletado,0)AS Deletado, CONVERT (VARCHAR(10), Vigencia.dtVigenciaInicio, 103) AS VigInicio, CONVERT (VARCHAR(10) ,Vigencia.dtVigenciaFim, 103) AS VigFim ,' +
                ' Vigencia.RegVigenciaInicioID, Vigencia.RegVigenciaFimID, Vigencia.RegraFiscalFimID ' +
                ' FROM  RegrasFiscais a WITH ( NOLOCK ) ' +
                    ' INNER JOIN dbo.fn_RegraFiscalElemento_Vigencia_tbl(NULL) Vigencia ON ((Vigencia.RegraFiscalID = a.RegraFiscalID) OR (Vigencia.RegraFiscalFimID = a.RegraFiscalID)) ' +
                    ' LEFT OUTER JOIN RegrasFiscais_Elementos aa WITH ( NOLOCK ) ON (Vigencia.RegElementoID = aa.RegElementoID) ' +
                    ' LEFT OUTER JOIN TiposAuxiliares_Itens b WITH ( NOLOCK ) ON aa.ElementoID = b.ItemID ' +
                    ' LEFT OUTER JOIN Operacoes c WITH ( NOLOCK ) ON aa.CFOPID = c.OperacaoID ' +
                    ' LEFT OUTER JOIN TiposAuxiliares_Itens d WITH ( NOLOCK ) ON aa.FinalidadeID = d.ItemID' +
                    ' LEFT OUTER JOIN Localidades e WITH ( NOLOCK ) ON aa.UFOrigemID = e.LocalidadeID ' +
                    ' LEFT OUTER JOIN Localidades f WITH ( NOLOCK ) ON aa.UFDestinoID = f.LocalidadeID ' +
                    ' LEFT OUTER JOIN Conceitos g WITH ( NOLOCK ) ON aa.ProdutoID = g.ConceitoID' +
                    ' LEFT OUTER JOIN Pessoas h WITH ( NOLOCK ) ON aa.PessoaID = h.PessoaID' +
                    ' LEFT OUTER JOIN Pessoas_Documentos i WITH ( NOLOCK ) ON (h.PessoaID = i.PessoaID) AND (I.TipoDocumentoID IN (111,101)) ' +
                    ' INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) j ON (j.RegraFiscalID = a.RegraFiscalID) ' +
                    ' INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') k ON (k.AgrupadorID = j.AgrupadorID) ' +
                    /*' LEFT OUTER JOIN RegrasFiscais_Vigencias r WITH(NOLOCK) ON (r.RegElementoID = aa.RegElementoID) AND (r.Vigente = 1) ' +
                    ' LEFT OUTER JOIN RegrasFiscais_Vigencias s WITH(NOLOCK) ON (s.RegElementoID = aa.RegElementoID) AND (s.Vigente = 0) ' +*/
                ' WHERE ' + sWhere + ' ' +
                ' ORDER BY aa.RegraFiscalID, ElementoID, CFOPID, FinalidadeID, UFOrigem,UFDestino,NCM, CNAE, ProdutoID,Produto, PessoaID,Pessoa';
/*
                 + ' AND  aa.RegraFiscalID IN ( ' + glb_RegraFiscalID + ' ,' + glb_RegraFiscalMaeID + ' ) ' +
		            ' OR ( ( RegraFiscalMaeID = ' + glb_RegraFiscalMaeID + ' ) AND ( ( SELECT aa.RegraFiscalMaeID FROM   RegrasFiscais aa WITH ( NOLOCK )' +
		                ' WHERE  ( aa.RegraFiscalID = ' + glb_RegraFiscalMaeID + ' ))IS NOT NULL )  AND ' + sWhere + ' ) ' +
*/
/*		        
		        ' UNION ALL' +
		        ' SELECT DISTINCT 0 AS OK , aa.RegElementoID, aa.RegraFiscalID, b.ItemMasculino AS ElementoID, ISNULL(CONVERT(VARCHAR(10), c.OperacaoID) + \' \' + c.Operacao, SPACE(1)) AS CFOPID, ISNULL(d.ItemMasculino, SPACE(1)) AS FinalidadeID,' +
                    ' ISNULL(e.CodigoLocalidade2, SPACE(1)) AS UFOrigem, ISNULL(f.CodigoLocalidade2, SPACE(1)) AS UFDestino, ISNULL(aa.NCM, SPACE(1)) AS NCM, ISNULL(aa.CNAE, SPACE(1)) AS CNAE, ISNULL(aa.ProdutoID, SPACE(1)) AS ProdutoID,  ISNULL(g.Conceito, SPACE(1)) AS Produto, ISNULL(h.PessoaID, 0) AS PessoaID, ISNULL(h.Fantasia, SPACE(1)) AS Pessoa,' +
                    ' ISNULL(i.Numero, SPACE(1)) AS Numero, aa.Grupo, aa.Observacao ,aa.Excecao,  CONVERT (VARCHAR(10),r.dtVigencia,103) AS VigInicio, CONVERT (VARCHAR(10) ,s.dtVigencia,103) AS VigFim,' +
                    ' r.RegVigenciaID AS RegVigenciaInicioID, s.RegVigenciaID AS RegVigenciaFimID ' +
                ' FROM  RegrasFiscais a WITH ( NOLOCK ) 	' +
                    ' LEFT OUTER JOIN RegrasFiscais_Elementos aa WITH ( NOLOCK ) ON a.RegraFiscalID = aa.RegraFiscalID ' +
                    ' LEFT OUTER JOIN TiposAuxiliares_Itens b WITH ( NOLOCK ) ON aa.ElementoID = b.ItemID ' +
                    ' LEFT OUTER JOIN Operacoes c WITH ( NOLOCK ) ON aa.CFOPID = c.OperacaoID ' +
                    ' LEFT OUTER JOIN TiposAuxiliares_Itens d WITH ( NOLOCK ) ON aa.FinalidadeID = d.ItemID' +
                    ' LEFT OUTER JOIN Localidades e WITH ( NOLOCK ) ON aa.UFOrigemID = e.LocalidadeID ' +
                    ' LEFT OUTER JOIN Localidades f WITH ( NOLOCK ) ON aa.UFDestinoID = f.LocalidadeID ' +
                    ' LEFT OUTER JOIN Conceitos g WITH ( NOLOCK ) ON aa.ProdutoID = g.ConceitoID' +
                    ' LEFT OUTER JOIN Pessoas h WITH ( NOLOCK ) ON aa.PessoaID = h.PessoaID' +
                    ' LEFT OUTER JOIN Pessoas_Documentos i WITH ( NOLOCK ) ON (h.PessoaID = i.PessoaID) AND (I.TipoDocumentoID IN (111,101)) ' +
                    ' LEFT OUTER JOIN RegrasFiscais_Vigencias r WITH(NOLOCK) ON (r.RegElementoID = aa.RegElementoID) AND (r.Vigente = 1) ' +
                    ' LEFT OUTER JOIN RegrasFiscais_Vigencias s WITH(NOLOCK) ON (s.RegElementoID = aa.RegElementoID) AND (s.Vigente = 0) ' +
		        ' WHERE  ' + sWhere + ' AND  a.RegraFiscalID = ( SELECT    RegraFiscalMaeID FROM RegrasFiscais WITH ( NOLOCK ) WHERE RegraFiscalID =  ' + glb_RegraFiscalMaeID + ' AND ' + sWhere + ' ) ' +
		        ' ORDER BY aa.RegraFiscalID, ElementoID, CFOPID, FinalidadeID, UFOrigem,UFDestino,NCM, CNAE, ProdutoID,Produto, PessoaID,Pessoa';
		        // ' ORDER BY aa.RegraFiscalID, ElementoID, CFOPID, FinalidadeID, UFOrigem,UFDestino,aa.NCM, aa.CNAE, aa.ProdutoID,Produto, h.PessoaID,Pessoa';
*/

    dsoElementos.SQL = sSQL;
    dsoElementos.ondatasetcomplete = eval(fillGridElementos_DSC);
    dsoElementos.refresh();
}
/********************************************************************
Ajuste dos grids 
********************************************************************/
function fillGridElementos_DSC() {
    startGridInterface(fg);
    aHiddenColls = [];

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    // Grid Com os Campos Ocultos
    if (selElemento.value == 970) // CFOP
    {
        aHiddenColls = [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 21, 22, 23];
    }
    else if (selElemento.value == 971) // Finalidade
    {
        aHiddenColls = [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 20, 21, 22];
    }
    else if (selElemento.value == 972) //Localidade
    {
        aHiddenColls = [1, 2, 3, 6, 7, 8, 9, 10, 11, 12, 21, 22, 23];
    }
    else if (selElemento.value == 973) // NCM
    {
        aHiddenColls = [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 21, 22, 23];
    }
    else if (selElemento.value == 974) // CNAE
    {
        aHiddenColls = [1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 21, 22, 23];
    }
    else if (selElemento.value == 975) // Produto 
    {
        aHiddenColls = [1, 2, 3, 4, 5, 6, 7, 10, 11, 12, 21, 22, 23];
    }
    else if (selElemento.value == 976) //Pessoa
    {
        aHiddenColls = [1, 2, 3, 4, 5, 6, 7, 8, 9, 21, 22, 23];
    }

    headerGrid(fg, ['RegraID', 'ElementoID', 'CFOP', 'Finalidade', 'UF Origem', 'UF Destino', 'NCM',
                   'CNAE', 'ID', 'Produto', 'ID', 'Pessoa', 'Numero', 'OK', 'Grupo', 'Exce��o', 'Observa��o', 'Deletado', 'Vig�ncia Inicio', 'Vig�ncia Fim', 'RegraID',
                   'RegVigenciaInicioID', 'RegVigenciaFimID', 'RegElementoID'], aHiddenColls);

    fillGridMask(fg, dsoElementos, ['RegraFiscalID*', 'ElementoID*', 'CFOPID*', 'FinalidadeID*', 'UFOrigem*',
                                   'UFDestino*', 'NCM*', 'CNAE*', 'ProdutoID*', 'Produto*', 'PessoaID*', 'Pessoa*', 'Numero*', 'OK', 'Grupo', 'Excecao', 'Observacao',
                                   'Deletado', 'VigInicio*', 'VigFim*', 'RegraFiscalFimID*', 'RegVigenciaInicioID*', 'RegVigenciaFimID*', 'RegElementoID*'],
                                   ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '99/99/9999', '99/99/9999', '', '', '', ''],
                                   ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', dTFormat, dTFormat, '', '', '', '']);

    
    insertcomboData(fg, getColIndexByColKey(fg, 'Grupo'), dsoComboGrupo, 'fldName', 'fldID');

    alignColsInGrid(fg, [22]);
    
    fg.FrozenCols = 15;
    
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;
    fg.MergeCol(5) = true;
    fg.MergeCol(6) = true;
    fg.MergeCol(7) = true;
    fg.MergeCol(8) = true;
    fg.MergeCol(9) = true;
    fg.MergeCol(10) = true;
    fg.MergeCol(11) = true;
    fg.MergeCol(12) = true;
    //fg.MergeCol(13) = true;

    //  GridLinhaTotal(fg);
    gridHasTotalLine(fg, '', 0xC0C0C0, null, true,
                      [[getColIndexByColKey(fg, 'RegraFiscalID*'), '######', 'C']]);
      paintReadOnlyRows();

    //coloca o tipo das colunas
      with (fg) 
      {
        ColDataType(13) = 11;
        ColDataType(15) = 11;
        ColDataType(17) = 11;
      }

    fg.Editable = true;
    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.Redraw = 2;

    fg.ExplorerBar = 5;
    
    lockControlsInModalWin(false);
    


}
/********************************************************************
A�oes do combo
********************************************************************/
function cmbOnChange(cmb) 
{
    if ((cmb.id).toUpperCase() == 'SELELEMENTO') 
    {
        CarregaSegundoCombo();
        clearComboEx(['selGrupo']);
        mostraEscondeDicas(); //OcultaMostraCampos();
        Limpar();
        txtVigInicio.value = glb_Data;
    }
    if ((cmb.id).toUpperCase() == 'SELVISAO')
        Listar();
}
/********************************************************************
Select combo Estatico 
********************************************************************/
function CarregaCombo() 
{

    var sSQL = '';
    var sWhere = '';    
    //primeiro combo
    glb_CounterCmbsStatics = 1;

    if (!(chkInc.checked)) 
    {
        sSQL = 'SELECT DISTINCT b.ItemID AS fldID, b.ItemMasculino AS fldName ' +
	                'FROM RegrasFiscais_Elementos a WITH(NOLOCK) ' +
	                'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.ElementoID) ' +
	                'INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) c ON (c.RegraFiscalID = a.RegraFiscalID) ' +
		            'INNER JOIN dbo.RegrasFiscais d WITH ( NOLOCK ) ON (d.RegraFiscalID = c.RegraFiscalID) ' +
		            'INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') e ON (e.AgrupadorID = c.AgrupadorID) ' +
		            'WHERE (d.Nivel <= ' + glb_Nivel + ')';
    }
    else 
    {
        sSQL = ' SELECT DISTINCT  a.ItemID AS fldID, a.ItemMasculino AS fldName' +
                   ' FROM    TiposAuxiliares_Itens a    WITH ( NOLOCK ) ' +
                   ' INNER JOIN RegrasFiscais_Mapeamento b WITH ( NOLOCK ) ON ( b.ElementoID = a.ItemID ) ' +
                   ' INNER JOIN RegrasFiscais c WITH ( NOLOCK ) ON ( c.RegraFiscalID = b.RegraFiscalID ) ' +
                   ' INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) d ON ( d.RegraFiscalID = c.RegraFiscalID ) ' +
                   ' INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') e ON (e.AgrupadorID = d.AgrupadorID) ' +
                   ' WHERE (d.Nivel <= ' + glb_Nivel + ')';
                
    if (glb_Nivel != 0) 
    {
        sWhere =  ' AND (CadastroNivel12 = 1)';
    }
    else 
    {
        sWhere = '  AND (CadastroNivel0 = 1)';

   }   
   }

   setConnection(dsoCombosElem);
   dsoCombosElem.SQL = sSQL + sWhere;
   dsoCombosElem.ondatasetcomplete = CarregaCombo_DSC;
   dsoCombosElem.Refresh();
}
/********************************************************************
Select combo Estatico 
********************************************************************/
/*function CarregaComboFabricantes() 
{

glb_CounterCmbsStatics = 1;

setConnection(dsoCombosFabricante);
dsoCombosFabricante.SQL = ' SELECT 0 AS fldID, \' \' AS fldName'  +
' UNION ALL' +
' SELECT DISTINCT a.PessoaID AS fldID, a.Fantasia AS fldName' +
' FROM    Pessoas a WITH ( NOLOCK ) ' +
' INNER JOIN RelacoesPesCon b WITH ( NOLOCK ) ON a.PessoaID = b.SujeitoID' +
' WHERE   (( b.TipoRelacaoID = 62 ) AND ( a.ClassificacaoID = 59 ) AND ( a.EstadoID = 2 ) AND ( b.EstadoID = 2 ))' +
' ORDER BY fldName ';

dsoCombosFabricante.ondatasetcomplete = CarregaFabricanteMarca_DSC;
dsoCombosFabricante.Refresh();
}*/

/*function CarregaComboMarcas() {

glb_CounterCmbsStatics = 1;

setConnection(dsoCombosMarcas);
dsoCombosMarcas.SQL = ' SELECT 0 AS fldID, \' \' AS fldName'  +
' UNION ALL' +
' SELECT DISTINCT a.ConceitoID AS fldID , a.Conceito AS fldName ' +
' FROM    Conceitos a WITH ( NOLOCK ) ' +
' INNER JOIN RelacoesPesCon b WITH ( NOLOCK ) ON A.ConceitoID = b.ObjetoID' +
' WHERE   ( ( b.TipoRelacaoID = 63 ) AND ( a.TipoConceitoID = 304 ) AND ( a.EstadoID = 2 ) AND ( b.EstadoID = 2 )' +
' AND ( b.SujeitoID = ' + selFabricante.value + ' )) ' +
' ORDER BY fldName ';

dsoCombosMarcas.ondatasetcomplete = CarregaFabricanteMarca_DSC;
dsoCombosMarcas.Refresh();
}*/

function CarregaComboProdutos() {
    var sql = '';
    var txtsProduto = '';
    glb_CounterCmbsStatics = 1;


    clearComboEx(['selProdutoID']);
    setConnection(dsoCombosProdutos);


    if (txtProdutoID.value != '')
        txtsProduto = ' AND (a.ConceitoID  =' + txtProdutoID.value + ')';

    if ((txtProdutoID.value.length > 0) || (txtProdutoID.value != '') || ((txtDescricao.value.length > 0)/* && (selFabricante.value == 0)*/)) {
        sql = ' SELECT DISTINCT a.fldID, a.fldName ' +
            ' FROM ( SELECT TOP 200 a.ConceitoID AS fldID, CONVERT( VARCHAR, a.ConceitoID)+ \'  \' + a.Conceito AS fldName ' +
            ' FROM    Conceitos a WITH ( NOLOCK ) ' +
            ' INNER JOIN RelacoesPesCon b WITH ( NOLOCK ) ON ( a.ConceitoID = b.ObjetoID ) ' +
            ' INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = a.MarcaID) ' +
            ' WHERE   ( ( a.EstadoID <> 5 ) AND ( b.EstadoID <> 5 ) AND ( a.TipoConceitoID = 303 )' + txtsProduto +
            ' AND ((a.Conceito + c.Conceito + a.Modelo + a.Descricao) LIKE \'%' + txtDescricao.value + '%\' ))' +
            ' ORDER BY  a.Conceito) a ';
    }
    else {
        sql = ' SELECT DISTINCT a.fldID, a.fldName ' +
            ' FROM (SELECT TOP 200 a.ConceitoID AS fldID,   CONVERT( VARCHAR, a.ConceitoID) + \'  \' + a.Conceito AS fldName ' +
            ' FROM    Conceitos a WITH ( NOLOCK ) ' +
            ' INNER JOIN RelacoesPesCon b WITH ( NOLOCK ) ON ( a.ConceitoID = b.ObjetoID ) ' +
            ' INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = a.MarcaID) ' +
            ' WHERE   ( ( a.EstadoID <> 5 )AND ( b.EstadoID <> 5 )  AND ( a.TipoConceitoID = 303 )' +
        /*' AND  (( 0 = ' + selMarca.value + ' ) OR (a.MarcaID = ' + selMarca.value + ' ))' +
        ' AND  (( 0 = ' + selFabricante.value + ' ) OR (a.FabricanteID = ' + selFabricante.value + ' ))' +*/
            ' AND ((a.Conceito + c.Conceito + a.Modelo + a.Descricao) LIKE \'%' + txtDescricao.value + '%\' ))' +
            ' ORDER BY  a.Conceito) a ';
    }
    dsoCombosProdutos.SQL = sql;
    dsoCombosProdutos.ondatasetcomplete = CarregaComboProdutos_DSC;
    dsoCombosProdutos.Refresh();
}
/********************************************************************
Select  do Segundo combo da modal (dinamico)
********************************************************************/
function CarregaSegundoCombo() {

    glb_CounterCmbsStatics = 1;
    setConnection(dsoCombosGru);

    if (chkInc.checked) {
        dsoCombosGru.SQL = 'SELECT DISTINCT a.Grupo AS fldName, a.Grupo AS fldID ' +
                        ' FROM    RegrasFiscais_Elementos a  WITH ( NOLOCK ) ' +
                        ' WHERE   a.RegraFiscalID = ' + glb_RegraFiscalID + ' AND a.ElementoID = ' + selElemento.value + ' AND a.Grupo IS NOT NULL ' +
                        ' UNION ALL ' +
                        ' SELECT TOP 1  ISNULL (MAX ( a.Grupo),0) + 1, ISNULL (MAX ( a.Grupo),0) + 1' +
                        ' FROM    RegrasFiscais_Elementos a  WITH(NOLOCK) ' +
                        ' WHERE  ( a.RegraFiscalID = ' + glb_RegraFiscalID + 'AND a.ElementoID = ' + selElemento.value + ' ) ';
    }
    else {
        dsoCombosGru.SQL = ' SELECT  DISTINCT a.Grupo AS fldID, a.Grupo AS fldName ' +
                        ' FROM    (SELECT DISTINCT a.Grupo  ' +
                        ' FROM    RegrasFiscais_Elementos a WITH ( NOLOCK ) ' +
                        ' INNER JOIN dbo.RegrasFiscais b WITH ( NOLOCK ) ON a.RegraFiscalID = b.RegraFiscalID ' +
                        ' WHERE   a.RegraFiscalID IN (  ' + glb_RegraFiscalID + ',  ' + glb_RegraFiscalMaeID + ' )  AND a.ElementoID =  ' + selElemento.value +
                        ' OR ( ( RegraFiscalMaeID = ' + glb_RegraFiscalMaeID + ' ) AND ( ( SELECT aa.RegraFiscalMaeID  FROM   RegrasFiscais aa WITH ( NOLOCK )' +
                        ' WHERE  ( aa.RegraFiscalID = ' + glb_RegraFiscalID + ' )) IS NOT NULL AND a.ElementoID = ' + selElemento.value + ' ))' +
                        ' UNION ALL ' +
                        ' SELECT DISTINCT a.Grupo  ' +
                        ' FROM    dbo.RegrasFiscais_Elementos a WITH ( NOLOCK ) ' +
                        ' INNER JOIN dbo.RegrasFiscais b WITH ( NOLOCK ) ON a.RegraFiscalID = b.RegraFiscalID ' +
                        ' WHERE   a.RegraFiscalID = ( SELECT  RegraFiscalMaeID FROM RegrasFiscais WITH ( NOLOCK )' +
                        ' WHERE   RegraFiscalID = ' + glb_RegraFiscalID + ' AND a.ElementoID = ' + selElemento.value + '  ))a ' +
                        ' WHERE (a.Grupo IS NOT NULL) ';
    }
    dsoCombosGru.ondatasetcomplete = CarregaComboGrupo_DSC;
    dsoCombosGru.Refresh();

    lockControlsInModalWin(false);
}

/********************************************************************
Select  do Segundo combo da modal (dinamico)
********************************************************************/
function CarregaComboPessoa() {
    var PessoaID = (txtPessoaID.value == '' ? 0 : txtPessoaID.value + '');

    setConnection(dsoCombosPessoas);
    dsoCombosPessoas.SQL = 'SELECT DISTINCT a.fldID, a.fldName ' +
                          ' FROM (SELECT TOP 200 a.PessoaID AS fldID, CONVERT (VARCHAR, b.Numero) + \'\  \'\ + CONVERT (VARCHAR, a.pessoaID) + \'\  \'\+  a.Nome  AS fldName' +
                          ' FROM    Pessoas a WITH ( NOLOCK )' +
                          ' INNER JOIN Pessoas_Documentos b WITH ( NOLOCK ) ON a.PessoaID = b.PessoaID ' +
                          ' WHERE   ( ( ' + PessoaID + ' = 0 ) OR ( a.PessoaID =' + PessoaID + ' ))' +
                          ' AND ( b.Numero LIKE \'\%' + txtDocFederal.value + '%\' )' +
                          ' AND ( a.Fantasia LIKE \'%' + txtFantasia.value + '%\' )' +
                          ' AND (a.EstadoID = 2) AND (b.TipoDocumentoID IN (111,101))  ORDER BY a.Fantasia) a';

    dsoCombosPessoas.ondatasetcomplete = CarregaComboPessoas_DSC;
    dsoCombosPessoas.Refresh();
}
/********************************************************************
Carrega os Combos Estaticos ou Dinamicos
********************************************************************/
function CarregaCombo_DSC() {
    var optionStr, optionValue;

    clearComboEx(['selElemento']);
    while (!dsoCombosElem.recordset.EOF) {
        optionStr = dsoCombosElem.recordset['fldName'].value;
        optionValue = dsoCombosElem.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selElemento.add(oOption);
        dsoCombosElem.recordset.MoveNext();
    }

    lockControlsInModalWin(false);
    
    selElemento.disabled = !(selElemento.length > 0);
    // CarregaSegundoCombo();

    if (selElemento.length > 0) {
        if (chkInc.checked) {
            btnIncluir.disabled = false;
        }
        else {
            btnListar.disabled = false;
            selVisao.disabled = false;
        }

        selElemento.disabled = false;

        lockControlsInModalWin(true);
        
        mostraEscondeDicas();
        CarregaSegundoCombo();
    }
    else {
        clearComboEx(['selGrupo']);

        selElemento.disabled = true;
        selGrupo.disabled = true;
        
        if (glb_Inicio) {
            chkInc.checked = true;
            glb_Inicio = false;
            lockControlsInModalWin(true);
            CarregaCombo();
            return null;
        }

        mostraEscondeDicas(970);

        if (chkInc.checked) {
            lockControlsInModalWin(false);
            btnIncluir.disabled = true;
        }
        else {
            lockControlsInModalWin(false);
            btnListar.disabled = true;
            selVisao.disabled = true;
        }
        //window.top.overflyGen.Alert('N�o h� elementos d�sponiveis para cadastro/pesquisa.');
    }

    glb_Inicio = false;

    if (!chkInc.checked)
        ComboGrupo();
}

function CarregaComboPessoas_DSC() {
    var optionStr, optionValue;


    clearComboEx(['selPessoas']);

    while (!dsoCombosPessoas.recordset.EOF) {
        optionStr = dsoCombosPessoas.recordset['fldName'].value;
        optionValue = dsoCombosPessoas.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selPessoas.add(oOption);
        dsoCombosPessoas.recordset.MoveNext();
    }

    selPessoas.disabled = !(selPessoas.length > 0);
}
/********************************************************************
Carrega os Combos Estaticos ou Dinamicos
********************************************************************/
function CarregaComboGrupo_DSC() {
    var optionStr, optionValue, oOption;


    clearComboEx(['selGrupo']);


    optionStr = '';
    optionValue = 0;
    oOption = document.createElement("OPTION");
    oOption.text = optionStr;
    oOption.value = optionValue;
    selGrupo.add(oOption);

    while (!dsoCombosGru.recordset.EOF) {
        optionStr = dsoCombosGru.recordset['fldName'].value;
        optionValue = dsoCombosGru.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selGrupo.add(oOption);
        dsoCombosGru.recordset.MoveNext();
        //selGrupo.selectedIndex = 1;

    }

    selGrupo.disabled = !(selGrupo.length > 0);

    if (!chkInc.checked)
        Listar();
}

/********************************************************************
Carrega os Combos Estaticos ou Dinamicos
********************************************************************/
function CarregaComboProdutos_DSC() {
    var optionStr, optionValue;


    if (dsoCombosProdutos.recordset.EOF) {
        window.top.overflyGen.Alert('N�o Existem Produtos');
        lockControlsInModalWin(false);
        return null;
    }

    while (!dsoCombosProdutos.recordset.EOF) {
        optionStr = dsoCombosProdutos.recordset['fldName'].value;
        optionValue = dsoCombosProdutos.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selProdutoID.add(oOption);
        dsoCombosProdutos.recordset.MoveNext();
    }
    
    lockControlsInModalWin(false);

}
/********************************************************************
Carrega os Combos Estaticos ou Dinamicos
********************************************************************/
/*
function CarregaFabricanteMarca_DSC() 
{
var optionStr, optionValue;
var aCmbsStatics = [selFabricante, selMarca];
var aDSOsStatics = [dsoCombosFabricante, dsoCombosMarcas];
var carregaComboProduto = (dsoCombosFabricante.recordset.EOF);
   
var i;
var lFirstRecord;
var nQtdCmbs = 2;

glb_CounterCmbsStatics--;
    
if (glb_CounterCmbsStatics == 0) {
for (i = 0; i < nQtdCmbs; i++) {
lFirstRecord = true;
while (!aDSOsStatics[i].recordset.EOF) {
optionStr = aDSOsStatics[i].recordset['fldName'].value;
optionValue = aDSOsStatics[i].recordset['fldID'].value;
var oOption = document.createElement("OPTION");
oOption.text = optionStr;
oOption.value = optionValue;
aCmbsStatics[i].add(oOption);
aDSOsStatics[i].recordset.MoveNext();
lFirstRecord = false;
aCmbsStatics[i].disabled = !(aCmbsStatics[i].length > 0);
}
aCmbsStatics[i].disabled = !(aCmbsStatics[i].length > 0);
}
}

if (carregaComboProduto)
CarregaComboProdutos();
}
*/

/********************************************************************
Fun��o Para inserir um novo elemento
********************************************************************/
function Incluir(Tipo) {
    lockControlsInModalWin(true);
    var strPars = new String();
    var nRegraFiscalID = glb_RegraFiscalID;
    var nElementoID = selElemento.value;
    var nCFOPID = (selCFOP.value != '' || '0' ? selCFOP.value : null);
    var nFinalidade = (selFinalidade.value != '' || '0' ? selFinalidade.value : null);
    var sNCM = txtNCM.value;
    var sCNAE = txtCNAE.value;
    var nUFOrigem = selOrigem.value;
    var nUFDestino = selDestino.value;
    var nProdutoID = selProdutoID.value;
    var nPessoaID = selPessoas.value;
    var bExcecao = (chkExcecao.checked ? 1 : 0);
    var bPermiteMesmaUF = (chkUF.checked ? 1 : 0);
    var nGrupo = selGrupo.value;
    var sVigInicio = txtVigInicio.value;
    var sVigFim = txtVigFim.value;
    var sObservacao = txtObservacao.value;
    var nTipoID = Tipo;
    var sMensagem = '';
    var sMensagemConfirm = '';

    if ((sVigInicio == '') && (sVigFim == ''))
        sMensagem += 'Informe Data In�cio ou Data Fim.\n';

    if ((sVigInicio != '') && (compareDatas(sVigInicio, glb_Data) == glb_Data)) //(sVigInicio < glb_Data))
        sMensagemConfirm = 'Data inferior ao in�cio da regra. \n' +
            'Tem certeza?';

    if ((selElemento.value == 970) && (nCFOPID == 0))
        sMensagem += 'Selecionar CFOP\n';

    if ((selElemento.value == 971) && (nFinalidade == 0))
        sMensagem += 'Selecionar Finalidade\n';

    if ((selElemento.value == 972) && (nUFOrigem == 0))
        sMensagem += 'Selecionar UF Origem\n';

    if ((selElemento.value == 973) && (sNCM == ''))
        sMensagem += 'Selecionar NCM\n';

    if ((selElemento.value == 974) && (sCNAE == ''))
        sMensagem += 'Selecionar CNAE\n';

    if ((selElemento.value == 975) && (nProdutoID == 0))
        sMensagem += 'Selecionar Produto\n';

    if ((selElemento.value == 976) && (nPessoaID == 0))
        sMensagem += 'Selecionar Pessoa\n';

    if (sMensagem.length > 0) 
    {
        window.top.overflyGen.Alert(sMensagem);
        lockControlsInModalWin(false);
        return null;
    }

    if (sMensagemConfirm.length > 0) 
    {
        var _retConf = window.top.overflyGen.Confirm(sMensagemConfirm);

        if (_retConf != 1) 
        {
            lockControlsInModalWin(false);
            return null;
        }
    }
    
    strPars = '';

    if (nUFOrigem != '') 
    { 
        nUFOrigem = '' ;
        nUFDestino = '' ;

       for (i = 0; i < selOrigem.length; i++) {
            if (selOrigem.options[i].selected == true)
                nUFOrigem += (nUFOrigem == '' ? '/' : '/') + selOrigem.options[i].value;
        }

        for (i = 0; i < selDestino.length; i++) {
            if (selDestino.options[i].selected == true)
                nUFDestino += (nUFDestino == '' ? '/' : '/') + selDestino.options[i].value;
        }
            nUFOrigem += (nUFOrigem.length > 0 ? '/' : '');
            nUFDestino += (nUFDestino.length > 0 ? '/' : '');
     }

    if (nProdutoID != '') {

        nProdutoID = '';

        if (selProdutoID.value != 0) {
            for (i = 0; i < selProdutoID.length; i++) {
                if (selProdutoID.options[i].selected == true)
                    nProdutoID += (nProdutoID == '' ? '/' : '/') + selProdutoID.options[i].value;
            }
            nProdutoID += (nProdutoID.length > 0 ? '/' : '');
        }
    }
    if (nPessoaID != '') {
        nPessoaID = '';
        if (selPessoas.value != 0) {
            for (i = 0; i < selPessoas.length; i++) {
                if (selPessoas.options[i].selected == true)
                    nPessoaID += (nPessoaID == '' ? '/' : '/') + selPessoas.options[i].value;
            }
            nPessoaID += (nPessoaID.length > 0 ? '/' : '');
        }
    }

    if (nGrupo == 0) {
        nGrupo = null;
    }
    
    strPars += '?nRegraFiscalID=' + escape(nRegraFiscalID);
    strPars += '&nElementoID=' + escape(nElementoID);
    strPars += '&nCFOPID=' + escape(nCFOPID);
    strPars += '&nFinalidade=' + escape(nFinalidade);
    strPars += '&sNCM=' + escape(sNCM);
    strPars += '&sCNAE=' + escape(sCNAE);
    strPars += '&sUFOrigem=' + escape(nUFOrigem);
    strPars += '&sUFDestino=' + escape(nUFDestino);
    strPars += '&sProduto=' + escape(nProdutoID);
    strPars += '&sPessoa=' + escape(nPessoaID);
    strPars += '&bExcecao=' + escape(bExcecao);
    strPars += '&bPermiteMesmaUF=' + escape(bPermiteMesmaUF);    
    strPars += '&nGrupo=' + escape(nGrupo);
    strPars += '&sVigInicio=' + escape(dateFormatToSearch(sVigInicio));
    strPars += '&sVigFim=' + escape(dateFormatToSearch(sVigFim));

    sObservacao = replaceStr(sObservacao, ',', '^');
    strPars += '&sObservacao=' + escape(sObservacao);
    
    strPars += '&nTipoID=' + escape(nTipoID);

    dsoIncluirElementos.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/serverside/gravarelementos.aspx' + strPars;
    dsoIncluirElementos.ondatasetcomplete = Incluir_DSC;
    dsoIncluirElementos.refresh();

}
/********************************************************************
Funcao Retorno da gravacao
********************************************************************/
function Incluir_DSC() {
    if (!(dsoIncluirElementos.recordset.BOF && dsoIncluirElementos.recordset.EOF)) {
        if (dsoIncluirElementos.recordset['Mensagem'].value != null && dsoIncluirElementos.recordset['Mensagem'].value.length > 0) {
           if (window.top.overflyGen.Alert(dsoIncluirElementos.recordset['Mensagem'].value) == 0) {
                
                return null;
            }
        }
        else {
            lockControlsInModalWin(false);
            Limpar();
            CarregaSegundoCombo();
        }
    }
    lockControlsInModalWin(false);
}

/********************************************************************
Funcao botao Finalizar
********************************************************************/
function Finalizar() 
{
     if (txtRodapeVigFim.value.length > 0)
        Gravar(4);
    else
    {
        if (window.top.overflyGen.Alert('Informe uma data de vig�ncia fim') == 0)
            return null;

        lockControlsInModalWin(false);
    }
}

/********************************************************************
Clique botao Listar Elementos
********************************************************************/
function Listar() 
{   
    txtVigInicio.value = glb_Data;
    var Elemento = selElemento.value;
    var sWhere = '';

    lockControlsInModalWin(true);

    sWhere = '  aa.ElementoID = ' + Elemento;

    if (Elemento == 970)
    {
        if (selCFOP.value != 0)
            sWhere += ' AND aa.CFOPID = ' + selCFOP.value;
    }
    else if (Elemento == 971) 
    {
        if (selFinalidade.value != 0)
            sWhere += ' AND aa.FinalidadeID = ' + selFinalidade.value;
    }
    else if (Elemento == 972) 
    {
        var UFOrigem = '';
        var UFDestino = '';

        for (i = 0; i < selOrigem.length; i++) 
        {
            if (selOrigem.options[i].selected == true)
                UFOrigem += ((UFOrigem == '') ? ' AND aa.UFOrigemID IN (' + selOrigem.options[i].value : ' , ' + selOrigem.options[i].value);
        }

        for (i = 0; i < selDestino.length; i++) 
        {
            if (selDestino.options[i].selected == true)
                UFDestino += ((UFDestino == '') ? ' AND aa.UFDestinoID IN (' + selDestino.options[i].value : ' , ' + selDestino.options[i].value);
        }

        UFOrigem += (UFOrigem.length > 0 ? ')' : '');
        UFDestino += (UFDestino.length > 0 ? ')' : '');

        sWhere += ' ' + UFOrigem + ' ' + UFDestino;
    }
    else if (Elemento == 973) 
    {
        if (txtNCM.value.length > 0) 
            sWhere += ' AND aa.NCM = ' + txtNCM.value;
    }
    else if (selElemento.value == 974) 
    {
        if (txtCNAE.value.length > 0)
            sWhere = ' AND aa.CNAE = ' + txtCNAE.value;
    }
    else if (Elemento == 975) 
    {
        if (selGrupo.value != 0) 
        {
            var Produto = '';

            for (i = 0; i < selProdutoID.length; i++) 
            {
                if (selProdutoID.options[i].selected == true)
                    Produto += (Produto == '' ? ' AND aa.ProdutoID IN (' + selProdutoID.options[i].value : ' , ') + selProdutoID.options[i].value;
            }

            Produto += (Produto.length > 0 ? ')' : '');
            sWhere += Produto;
        }
    }
    else if (Elemento == 976) 
    {
        if (selGrupo.value != 0) 
        {
            var PessoaID = '';

            for (i = 0; i < selPessoas.length; i++)
            {
                if (selPessoas.options[i].selected)
                    PessoaID += (PessoaID == '' ? ' AND aa.PessoaID IN (' + selPessoas.options[i].value : ' , ') + selPessoas.options[i].value;
            }

            PessoaID += (PessoaID.length > 0 ? ')' : '');
            sWhere += PessoaID;
        }
    }
    
    if (selGrupo.selectedIndex >= 1)
        sWhere += ' AND aa.Grupo = ' + selGrupo.value;

    if (selVisao.value == 0)
        sWhere += 'AND ((Vigencia.RegraFiscalID = ' + glb_RegraFiscalID + ') OR (Vigencia.RegraFiscalFimID = ' + glb_RegraFiscalID + '))';
    else if (selVisao.value == 1)
        sWhere += 'AND (((Vigencia.RegraFiscalID = ' + glb_RegraFiscalID + ') OR (Vigencia.RegraFiscalFimID = ' + glb_RegraFiscalID + ')) ' +
            'OR ((a.Nivel <> ' + glb_Nivel + ') AND (a.Nivel - ' + glb_Nivel + ' BETWEEN -1 AND 1))) ';
        
    fillGridElementos(sWhere); //CarregaGrid

}
/********************************************************************
Fun��o para gravar a data finalizar da regra fiscal 
    nTipoID=1 : Gravar
    nTipoID=2 : Deletar
    nTipoID=3 : Finalizar um elemento
********************************************************************/
function Gravar(nTipoID) 
{
    lockControlsInModalWin(true);

    var sMensagem = '';
    var strPars = new String();
    var nCurrRegraFiscalID;
    var nRegElementoID;
    var DataVigFim;
    var nRegVigenciaInicioID;
    var nRegVigenciaFimID = '';
    var sObservacao;
    var bExcecao;
    var nGrupo;
    var nRegraFiscalID;
    var nBytesAcum = 0;
    var nDataLen = 0;
    
    strPars = '';


    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_sResultado = '';

    if ((nTipoID == 1) || (nTipoID == 4))
    {
        for (var i = 1; i < fg.Rows; i++)
        {
            bOK = fg.textMatrix(i, getColIndexByColKey(fg, 'OK'));

            if (bOK != 0) 
            {

                nBytesAcum = strPars.length;
                nCurrRegraFiscalID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegraFiscalID*'));
                nRegVigenciaFimID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegVigenciaFimID*'));

                if ((nCurrRegraFiscalID != glb_RegraFiscalID) && (nTipoID != 4))
                    break;

                if ((nRegVigenciaFimID != '') && (nTipoID == 4))
                    break;

                if (nTipoID == 4)
                    fg.textMatrix(i, getColIndexByColKey(fg, 'VigFim*')) = txtRodapeVigFim.value;

                nRegElementoID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegElementoID*'));
                DataVigFim = dateFormatToSearch(fg.textMatrix(i, getColIndexByColKey(fg, 'VigFim*')));
                nRegVigenciaInicioID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegVigenciaInicioID*'));
                nRegVigenciaFimID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegVigenciaFimID*'));
                sObservacao = fg.textMatrix(i, getColIndexByColKey(fg, 'Observacao'));
                bExcecao = fg.textMatrix(i, getColIndexByColKey(fg, 'Excecao'));
                nGrupo = fg.textMatrix(i, getColIndexByColKey(fg, 'Grupo'));
                nRegraFiscalID = glb_RegraFiscalID; // fg.textMatrix(i, getColIndexByColKey(fg, 'RegraFiscalID*'));

                if (nBytesAcum >= glb_nMaxStringSize) 
                {
                    nBytesAcum = 0;

                    if (strPars != '') 
                    {
                        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                        strPars = '';
                        nDataLen = 0;
                    }
                }
                
                if (strPars == '')
                    strPars += '?nRegElementoID=' + escape(nRegElementoID);
                else
                    strPars += '&nRegElementoID=' + escape(nRegElementoID);

                strPars += '&nRegraFiscalID=' + escape(nRegraFiscalID);
                strPars += '&sDataVigFim=' + escape(DataVigFim);
                strPars += '&nRegVigenciaInicioID=' + escape(nRegVigenciaInicioID);
                strPars += '&nRegVigenciaFimID=' + escape(nRegVigenciaFimID);

                sObservacao = replaceStr(sObservacao, ',', '^');
                strPars += '&sObservacao=' + escape(sObservacao);
                
                strPars += '&bExcecao=' + escape((bExcecao != 0) ? 1 : 0);
                strPars += '&nGrupo=' + escape(nGrupo);

                //if (((nRegVigenciaFimID == '') || (nRegVigenciaFimID == null)) && (nTipoID == 1))
                  //  nTipoID = 4;

                strPars += '&nTipoID=' + escape(nTipoID);

                nDataLen++;
            }
        }

        if (nDataLen > 0)
            glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
    }
    else if (nTipoID == 2) 
    {
        for (var i = 1; i < fg.Rows; i++) 
        {
            var bOK = fg.textMatrix(i, getColIndexByColKey(fg, 'OK'));

            if (bOK != 0)
            {
                nCurrRegraFiscalID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegraFiscalID*'));

                if (nCurrRegraFiscalID != glb_RegraFiscalID)
                    break;
            
                nRegElementoID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegElementoID*'));
                nRegVigenciaInicioID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegVigenciaInicioID*'));
                nRegVigenciaFimID = fg.textMatrix(i, getColIndexByColKey(fg, 'RegVigenciaFimID*'));

                if (nBytesAcum >= glb_nMaxStringSize) 
                {
                    nBytesAcum = 0;

                    if (strPars != '') 
                    {
                        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                        strPars = '';
                        nDataLen = 0;
                    }
                }
                
                if (strPars == '')
                    strPars += '?nRegElementoID=' + escape(nRegElementoID);
                else
                    strPars += '&nRegElementoID=' + escape(nRegElementoID);

                strPars += '&nRegraFiscalID=' + escape(glb_RegraFiscalID);
                strPars += '&nRegVigenciaInicioID=' + escape(nRegVigenciaInicioID);
                strPars += '&nRegVigenciaFimID=' + escape(nRegVigenciaFimID);
                strPars += '&nTipoID=' + escape(nTipoID);

                nDataLen++;
            }
        }

        if (nDataLen > 0)
            glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
    }

    if ((nCurrRegraFiscalID != null) && (nCurrRegraFiscalID != glb_RegraFiscalID) && (nTipoID != 4))
    {
        window.top.overflyGen.Alert('Selecione somente elementos desta regra');
        lockControlsInModalWin(false);
    }
    else if ((nTipoID == 4) && (nRegVigenciaFimID != '')) 
    {
        window.top.overflyGen.Alert('Selecione somente elementos que ainda n�o est�o finalizados');
        lockControlsInModalWin(false);
    }
    else if (glb_aSendDataToServer.length > 0)
    {
        if (nTipoID == 4)
            txtRodapeVigFim.value = '';
    
        var _retConf = 1;
        
        if (nTipoID == 2)
            _retConf = window.top.overflyGen.Confirm('Voc� tem certeza?');

        if (_retConf == 1) 
        {
            sendDataToServer();
            /*
            dsoGravaElementos.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/serverside/gravarelementos.aspx' + strPars;
            dsoGravaElementos.ondatasetcomplete = GravaElementos_DSC;
            dsoGravaElementos.refresh();*/
        }
        else
            lockControlsInModalWin(false);
    }
    else 
    {
        window.top.overflyGen.Alert('Selecione algum elemento desta regra');
        lockControlsInModalWin(false);
    }
}

function sendDataToServer() 
{
    if (glb_nTimerSendDataToServer != null) 
    {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try 
    {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) 
        {
            dsoGravaElementos.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/serverside/gravarelementos.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGravaElementos.ondatasetcomplete = GravaElementos_DSC;
            dsoGravaElementos.refresh();
        }
        else 
        {
            if (glb_sMensagem != '') 
            {
                if (window.top.overflyGen.Alert(glb_sMensagem) == 0)
                    return null;

                glb_sMensagem = '';
                    
                lockControlsInModalWin(false);
            }
            else
            {
                lockControlsInModalWin(false);
                glb_ListarTimerInt = window.setInterval('Listagem()', 10, 'JavaScript');
            }
        }
    }
    catch (e) 
    {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_ListarTimerInt = window.setInterval('Listagem()', 10, 'JavaScript');
    }
}

function GravaElementos_DSC() 
{
    if (!(dsoGravaElementos.recordset.BOF && dsoGravaElementos.recordset.EOF)) 
    {
        if (dsoGravaElementos.recordset['Mensagem'].value != null && dsoGravaElementos.recordset['Mensagem'].value.length > 0) 
            glb_sMensagem = dsoGravaElementos.recordset['Mensagem'].value;
        else
        {
            glb_nPointToaSendDataToServer++;
            glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
        }
    }
}

function Listagem()
{
    if (glb_ListarTimerInt != null) 
    {
        window.clearInterval(glb_ListarTimerInt);
        glb_ListarTimerInt = null;
    }
    
    ComboGrupo();
    Limpar();
    Listar();
}

/********************************************************************
Fun��o para limpar os campos 
********************************************************************/
function Limpar() {
    selCFOP.selectedIndex = -1;
    selOrigem.selectedIndex = -1;
    selDestino.selectedIndex = -1;
    selGrupo.selectedIndex = -1;
    selFinalidade.selectedIndex = -1;
    chkExcecao.checked = false;
    txtNCM.value = '';
    txtCNAE.value = '';
    txtProdutoID.value = '';
    txtDescricao.value = '';
    selProdutoID.selectedIndex = -1;
    txtPessoaID.value = '';
    txtDocFederal.value = '';
    txtFantasia.value = '';
    selPessoas.selectedIndex = -1;
    txtRodapeVigFim.value = '';
    txtObservacao.value = '';
    //txtVigInicio.value = glb_Data;
    //txtVigFim.value = '';
}
/********************************************************************
Clique botao Voltar
********************************************************************/
function Voltar() {
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.visibility = 'hidden';

    var strPars = new String();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)

    strPars = '?sCaller=' + escape('S');
    strPars += '&nRegraFiscalID=' + escape(glb_RegraFiscalID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/modalpages/modalmensagens.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 539));
}
/********************************************************************
Clique botao avancar
********************************************************************/
function avancarModal() {
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.visibility = 'hidden';

    var nRegraFiscalID = glb_RegraFiscalID;
    var strPars = new String();

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');
    strPars += '&nRegraFiscalID=' + escape(nRegraFiscalID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/regrasfiscais/modalpages/modalimpostos.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 539));
}
/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) 
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S', null);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) 
    {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null);
    }
    else if (ctl.id == btnDeletar.id) 
    {
        if (glb_EstadoID == 1) 
        {
            Gravar(2);
        }
        else
        {
            window.top.overflyGen.Alert("Opera��o indispon�vel para Regras Fiscais no estado Ativo.");

            lockControlsInModalWin(false);
        }
    }
    else if (ctl.id == btnAvancar.id) 
    {
        avancarModal();
    }
    else if (ctl.id == btnVoltar.id) 
    {
        Voltar();
    }
    else if (ctl.id == btnFinalzar.id) 
    {
        Finalizar();
    }
    else if (ctl.id == btnGravar.id) 
    {
        Gravar(1);
    }
    else if (ctl.id == btnIncluir.id) 
    {
        Incluir(3);
    }
    else if (ctl.id == btnListar.id) 
    {
        Listar();
    }
    else if ((selElemento.value == 975) && (ctl.id == btnPesquisar.id)) 
    {
        CarregaComboProdutos();
    }
    else if ((selElemento.value == 976) && (ctl.id == btnPesquisar.id)) 
    {
        CarregaComboPessoa();
        lockControlsInModalWin(false);
    }

}
/********************************************************************
Ajustar os elementos de acordo com o comando dicas
********************************************************************/

function mostraEscondeDicas(elementoDefault) 
{
    if ((elementoDefault == undefined) && (selElemento.length <= 0)) 
        elementoDefault = 970;
    
    if (chkDicas.checked) 
    {
        divFG.style.visibility = 'hidden';
        btnVoltar.style.visibility = 'hidden';
        txtRodapeVigFim.style.visibility = 'hidden';
        lblRodapeVigFim.style.visibility = 'hidden';
        btnFinalzar.style.visibility = 'hidden';
        btnGravar.style.visibility = 'hidden';
        btnDeletar.style.visibility = 'hidden';
        btnAvancar.style.visibility = 'hidden';
        escondeCamposDicas();
        divDicas.style.visibility = 'inherit';

        lblVisao.style.visibility = 'hidden';
        selVisao.style.visibility = 'hidden';                
    }
    else {
        divDicas.style.visibility = 'hidden';
        lblInc.style.visibility = 'inherit';
        chkInc.style.visibility = 'inherit';
        lblElemento.style.visibility = 'inherit';
        selElemento.style.visibility = 'inherit';
        lblGrupo.style.visibility = 'inherit';
        selGrupo.style.visibility = 'inherit';

        lblVisao.style.visibility = 'inherit';
        selVisao.style.visibility = 'inherit';

        OcultaMostraCampos(elementoDefault);
    }
}
/********************************************************************
Dicas
********************************************************************/

function dicas() {
    var sSQL = '';
    var sWHERE = '';

    if (glb_Nivel == 0)
        sWHERE = 'WHERE a.RegraFiscalID = ' + glb_RegraFiscalID;
    else if (glb_Nivel == 1)
        sWHERE = 'WHERE a.RegraFiscalID = ' + glb_RegraFiscalMaeID;
    else if (glb_Nivel == 2)
        sWHERE = 'WHERE a.RegraFiscalID IN ' +
                    '(SELECT aa.RegraFiscalMaeID FROM RegrasFiscais aa WITH(NOLOCK) WHERE RegraFiscalID = ' + glb_RegraFiscalMaeID + ')';

    sSQL = 'SELECT ISNULL(a.Observacoes, \'\') AS Observacoes ' +
                'FROM RegrasFiscais a WITH(NOLOCK) ' +
                sWHERE;

    dsoDicas.SQL = sSQL;
    dsoDicas.ondatasetcomplete = eval(dicas_DSC);
    dsoDicas.refresh();
}

function dicas_DSC() {
    if ((!dsoDicas.recordset.EOF) && (!dsoDicas.recordset.BOF))
        divDicas.innerHTML = dsoDicas.recordset['Observacoes'].value;

    //Listar();
}

/********************************************************************
Esconder campos Dicas
********************************************************************/
function escondeCamposDicas() {
    lblInc.style.visibility = 'hidden';
    chkInc.style.visibility = 'hidden';
    lblElemento.style.visibility = 'hidden';
    selElemento.style.visibility = 'hidden';
    lblUFOrigem.style.visibility = 'hidden';
    selOrigem.style.visibility = 'hidden';
    lblUFDestino.style.visibility = 'hidden';
    SelDestino.style.visibility = 'hidden';
    lblGrupo.style.visibility = 'hidden';
    selGrupo.style.visibility = 'hidden';
    lblExcecao.style.visibility = 'hidden';
    chkExcecao.style.visibility = 'hidden';
    lblObservacao.style.visibility = 'hidden';
    txtObservacao.style.visibility = 'hidden';
    lblCFOP.style.visibility = 'hidden';
    selCFOP.style.visibility = 'hidden';
    lblFinalidade.style.visibility = 'hidden';
    selFinalidade.style.visibility = 'hidden';
    lblNCM.style.visibility = 'hidden';
    txtNCM.style.visibility = 'hidden';
    lblCNAE.style.visibility = 'hidden';
    txtCNAE.style.visibility = 'hidden';
    lblProdutoID.style.visibility = 'hidden';
    txtProdutoID.style.visibility = 'hidden';
    lblDescricao.style.visibility = 'hidden';
    txtDescricao.style.visibility = 'hidden';
    btnPesquisar.style.visibility = 'hidden';
    lblComboProdutos.style.visibility = 'hidden';
    selProdutoID.style.visibility = 'hidden';
    lblPessoaID.style.visibility = 'hidden';
    txtPessoaID.style.visibility = 'hidden';
    lblDocFederal.style.visibility = 'hidden';
    txtDocFederal.style.visibility = 'hidden';
    lblFantasia.style.visibility = 'hidden';
    txtFantasia.style.visibility = 'hidden';
    lblPessoas.style.visibility = 'hidden';
    selPessoas.style.visibility = 'hidden';
    btnListar.style.visibility = 'hidden';
    btnIncluir.style.visibility = 'hidden';    
    lblVigInicio.style.visibility = 'hidden';
    txtVigInicio.style.visibility = 'hidden';
    lblVigFim.style.visibility = 'hidden';
    txtVigFim.style.visibility = 'hidden';
}

function ComboGrupo()
{
    if (selElemento.length <= 0) {
        lockControlsInModalWin(false);
        return null;
    }
    
    var sSQL = '';

    sSQL = 'SELECT DISTINCT a.Grupo AS fldName, a.Grupo AS fldID ' +
                'FROM RegrasFiscais_Elementos a  WITH(NOLOCK) ' +
                    'INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) b ON (b.RegraFiscalID = a.RegraFiscalID) ' +
                    'INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') c ON (b.AgrupadorID = c.AgrupadorID) ' +
                'WHERE a.ElementoID = ' + selElemento.value + ' ' +
            'UNION ALL ' +
            'SELECT TOP 1 ISNULL (MAX ( a.Grupo), 0) + 1, ISNULL (MAX(a.Grupo), 0) + 1 ' +
                'FROM RegrasFiscais_Elementos a  WITH(NOLOCK) ' +
                    'INNER JOIN dbo.fn_RegraFiscal_Niveis_tbl(NULL) b ON (b.RegraFiscalID = a.RegraFiscalID) ' +
                    'INNER JOIN dbo.fn_RegraFiscal_Agrupador_tbl(' + glb_RegraFiscalID + ') c ON (b.AgrupadorID = c.AgrupadorID) ' +
                'WHERE a.ElementoID = ' + selElemento.value + ' ';

    dsoComboGrupo.SQL = sSQL;
    dsoComboGrupo.ondatasetcomplete = ComboGrupo_DSC;
    dsoComboGrupo.refresh();
}

function ComboGrupo_DSC()
{
    lockControlsInModalWin(false);
}

function js_fg_modalElementos_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    //Forca celula read-only    
    if (!glb_GridIsBuilding) {
        if (glb_FreezeRolColChangeEvents)
            return true;

        if (cellIsLocked(NewRow, NewCol)) {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
    }
}

function cellIsLocked(nRow, nCol) {
    var retVal = false;
    
    if (nCol == getColIndexByColKey(fg, 'OK'))
        return retVal;
        
    if (((fg.textMatrix(nRow, getColIndexByColKey(fg, 'RegraFiscalID*')) != glb_RegraFiscalID) && (nRow > 1)) || (glb_EstadoID != 1))
    {
        retVal = true;
    }
    
    return retVal;
}


function js_modalElementos_AfterEdit(Row, Col) 
{
    if (fg.Editable) 
    {
        if ((Col != getColIndexByColKey(fg, 'OK')) && (fg.textMatrix(Row, getColIndexByColKey(fg, 'RegraFiscalID*')) == glb_RegraFiscalID))
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;

        if ((fg.textMatrix(Row, getColIndexByColKey(fg, 'RegraFiscalID*')) != glb_RegraFiscalID) && (fg.ColDataType(Col) == 11) && (Col != getColIndexByColKey(fg, 'OK')))
        {
            if (fg.textMatrix(Row, Col) != 0)
                fg.textMatrix(Row, Col) = 0;
            else
                fg.textMatrix(Row, Col) = 1;
        }
    }
   GridLinhaTotal(fg);

}

function js_modalElemento_BeforeEdit(grid, Row, Col) {

   // return null;
    var nomeColuna = grid.ColKey(Col);

    var lastPos = nomeColuna.length - 1;

    // retira o asterisco do nome do campo
    if (nomeColuna.substr(lastPos, 1) == '*') {
        nomeColuna = nomeColuna.substr(0, lastPos);
    }

    if ((dsoElementos.recordset[nomeColuna].type == 200) || (dsoElementos.recordset[nomeColuna].type == 129))
        fieldMaxLength(fg, Col, dsoElementos);
}

function paintReadOnlyRows() 
{
    for (var i = 2; i < fg.Rows; i++) 
    {
        if ((fg.textMatrix(i, getColIndexByColKey(fg, 'RegraFiscalID*')) != glb_RegraFiscalID)  || (glb_EstadoID != 1))
        {
            fg.Cell(6, i, 0, i, getColIndexByColKey(fg, 'OK') - 1) = 0XDCDCDC;
            fg.Cell(6, i, getColIndexByColKey(fg, 'OK') + 1, i, fg.Cols - 1) = 0XDCDCDC;
        }
    }
}


function GridLinhaTotal(grid) {
    var nContador = 0;

    for (i = 2; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, getColIndexByColKey(grid, 'OK')) != 0)
            nContador++;
    }

    if (grid.Rows > 1)
        grid.TextMatrix(1, getColIndexByColKey(grid, 'RegraFiscalID*')) = nContador;
}

function js_fg_modalElementosDblClick(grid, Row, Col)
{
    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if ((Col != getColIndexByColKey(grid, 'OK')))
        return true;

    // trap so header
    if (Row != 1)
        return true;

    var i;
    var nRegraFiscalID = 0;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);

    // limpa coluna se tem um check box checado
    for (i = 2; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 2; i < grid.Rows; i++) 
    {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    window.focus();
    grid.focus();

    GridLinhaTotal(grid);
}

function compareDatas(dataInicio, dataFim) 
{
    if (dataFim == '')
        return dataInicio;
    else if (dataInicio == '')
        return dataFim;
        
    dtInicio = dataInicio.split("/");
    dtFim = dataFim.split("/");

    var dtInicio = parseInt(dataInicio.split("/")[2].toString() + dataInicio.split("/")[1].toString() + dataInicio.split("/")[0].toString());
    var dtFim = parseInt(dataFim.split("/")[2].toString() + dataFim.split("/")[1].toString() + dataFim.split("/")[0].toString());

    if (dtFim > dtInicio)
        return dataFim;
    else if (dtFim == dtFim)
        return null;
    else
        return dataInicio;
}

function carregaVisao()
{
    var hint = new Array();

    hint[0] = 'Somente registros da pr�pria regra';
    hint[1] = 'Registros da pr�pria regra, regras de nivel superior e inferior';
    hint[2] = 'Registros da pr�pria regra, regras do mesmo nivel, superior e inferior';

    clearComboEx(['selVisao']);

    for (var i = 0; (i <= (glb_Nivel == 0 ? 1 : glb_Nivel)); i++) {
        var oOption = document.createElement("OPTION");
        oOption.text = i.toString();
        oOption.value = i;
        oOption.title = hint[i];
        selVisao.add(oOption);
    }
}