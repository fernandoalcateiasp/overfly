﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.cartascorrecao.serverside
{
    public partial class verificacaocartacarrecao : System.Web.UI.OverflyPage
    {
        public string Mensagem = "";
        public string Resultado = "";

        private Integer CartaCorrecaoID;

        public Integer nCartaCorrecaoID
        {
            get { return CartaCorrecaoID; }
            set { CartaCorrecaoID = value; }
        }
        private Integer EstadoDeID;

        public Integer nEstadoDeID
        {
            get { return EstadoDeID; }
            set { EstadoDeID = value; }
        }
        private Integer EstadoParaID;

        public Integer nEstadoParaID
        {
            get { return EstadoParaID; }
            set { EstadoParaID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }

        public void Verificacao()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[6];
            procParams[0] = new ProcedureParameters(
                "@CartaCorrecaoID",
                System.Data.SqlDbType.Int,
                CartaCorrecaoID);

            procParams[1] = new ProcedureParameters(
                "@EstadoDeID",
                System.Data.SqlDbType.Int,
                EstadoDeID);

            procParams[2] = new ProcedureParameters(
                "@EstadoParaID",
                System.Data.SqlDbType.Int,
                EstadoParaID);

            procParams[3] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                 UsuarioID);

            procParams[4] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.Output);

            procParams[5] = new ProcedureParameters(
                "@Mensagem",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[5].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_CartaCorrecao_Verifica",
                procParams);

            Resultado += procParams[4].Data.ToString();
            Mensagem += procParams[5].Data.ToString();
        }



        protected override void PageLoad(object sender, EventArgs e)
        {
            Verificacao();

            WriteResultXML(DataInterfaceObj.getRemoteData(
              "select " +
              (Resultado != null ? "'" + Resultado + "' " : "NULL") +
              " as Resultado , " +
              (Mensagem != null ? "'" + Mensagem + "' " : "NULL") +
              " as Mensagem  "));
        }
    }
}