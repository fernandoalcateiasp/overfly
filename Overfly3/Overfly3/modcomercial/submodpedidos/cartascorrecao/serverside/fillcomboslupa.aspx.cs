﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.cartascorrecao.serverside
{
    public partial class fillcomboslupa : System.Web.UI.OverflyPage
    {
        private string SParam1;

        public string sParam1
        {
            get { return SParam1; }
            set { SParam1 = value; }
        }
        private string SParam2;

        public string sParam2
        {
            get { return SParam2; }
            set { SParam2 = value; }
        }
        private string SText;

        public string sText
        {
            get { return SText; }
            set { SText = value; }
        }
        private string Date_Sql_Param;

        public string DATE_SQL_PARAM
        {
            get { return Date_Sql_Param; }
            set { Date_Sql_Param = value; }
        }
        private Integer Contexto;

        public Integer nContexto
        {
            get { return Contexto; }
            set { Contexto = value; }
        }

        protected string GetSql()
        {
            string SQL = "";


            SQL += "SELECT TOP 100 a.NotaFiscalID as fldID, a.NotaFiscal as fldName , " +
             "CONVERT(VARCHAR, a.dtNotaFiscal, " + Date_Sql_Param + ") as V_dtNotaFiscal, a.PedidoID, c.PessoaID, c.Fantasia as Pessoa, " +
             "ISNULL((SELECT TOP 1 (ISNULL(aa.SequencialEvento,0)+1) FROM CartasCorrecao aa WITH(NOLOCK) WHERE aa.NotaFiscalID = a.NotaFiscalID AND aa.EstadoID = 74 ORDER BY aa.dtCartaCorrecao DESC),1) AS SequencialEvento " +
             "FROM NotasFiscais a WITH(NOLOCK) " +
             "INNER JOIN NotasFiscais_Pessoas b WITH(NOLOCK) ON (b.NotaFiscalID = a.NotaFiscalID) " +
                    "AND ((" + Contexto.ToString() + " = 5131 AND (a.Emissor = 1) AND (b.TipoID = 791)) OR (" + Contexto.ToString() + " = 5132 AND (a.Emissor = 0) AND (b.TipoID = 790))) " +
             "INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = b.PessoaID) " +
             "WHERE a.EmpresaID = " + (sParam2) + " AND a.NotaFiscal >= " + (SText) + " AND a.EstadoID=67 " +
             "ORDER BY a.NotaFiscal";


            return SQL;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(
                  DataInterfaceObj.getRemoteData(GetSql()));
        }
    }
}