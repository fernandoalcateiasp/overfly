/********************************************************************
modalloadxml.js

Library javascript para o modalloadxml.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nUserID = 0;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() 
{
    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (viewcartacorrecaoBody) 
    {
        style.backgroundColor = 'transparent';
        style.top = 10;
        style.width = '100%';
        style.height = '100%';
        style.visibility = 'inherit';
        style.border = 'none';
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() 
{
    ;
}

function imprimirBoleto() 
{
    window.focus();
    viewcartacorrecaoBody.focus();
    window.print();
}
function carregaXML() 
{
    //var sCaminhoEstado = "CartaCorrecao";
    var sTipo = "Emitidas";
    var sEmpresaID = new String();

    sEmpresaID = glb_EmpresaID.toString();
    if (sEmpresaID.length < 2)
    {
	sEmpresaID = '0' + sEmpresaID;
	}

    //Nova estrutura de arquivamentos dos XML's
    var fullPath = PAGES_URL_ROOT + "/NFe/" + sEmpresaID + "/DocumentoEletronico/CC-e/" + glb_Tipo + "/" + glb_AnoCarta + "/" + glb_MesCarta + "/" + glb_DiaCarta + "/" + glb_NomeArquivo;
    //var fullPath = PAGES_URL_ROOT + "/NFe/" + glb_EmpresaID + "/DocumentoEletronico/CC-e/" + sTipo + "/" + sCaminhoEstado + "/" + glb_AnoCarta + "/" + glb_MesCarta + "/" + glb_DiaCarta + "/" + glb_NomeArquivo; ARF 05/04/2013
    window.open(fullPath);
}