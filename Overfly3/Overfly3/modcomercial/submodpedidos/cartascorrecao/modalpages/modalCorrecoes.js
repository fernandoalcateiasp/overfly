/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    var elem = document.getElementById('modalCorrecoesBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    if (! txtQuery.readOnly )
        txtQuery.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	var sQuery = '';
	
    // texto da secao01
    secText('Campo - ' + glb_sConta, 1);

    // reajusta dimensoes e reposiciona a janela
    (getFrameInHtmlTop( getExtFrameID(window))).style.top = (getFrameInHtmlTop( 'frameSup01')).offsetTop;

    // reajusta dimensoes e reposiciona a janela
	//redimAndReposicionModalWin(740, 490, false);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    var nMemoHeight = 375;
    
    // ajusta o divObservacoes
    elem = window.document.getElementById('divObservacoes');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = MAX_FRAMEWIDTH - (ELEM_GAP * 29);
        temp = parseInt(width);
        height = (nMemoHeight * 2) + (ELEM_GAP) + (16 * 2);
    }

    elem = document.getElementById('txtQuery');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = MAX_FRAMEWIDTH - (ELEM_GAP * 29);
        height = nMemoHeight;
    }

	sQuery = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL ,
                               glb_gridID + '.TextMatrix(' + glb_gridID + '.Row,' +
                               glb_col + ')');

	if (sQuery == null)
		sQuery = '';

    btnOK.disabled = (glb_btnOKState == 'D');
    
    txtQuery.maxLength = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL ,
                              glb_dso + '.recordset[' + '\'' + glb_fieldName + '\'' + '].definedSize');
    txtQuery.onkeyup = ismaxlength;       
    txtQuery.onkeydown= txtQuery_onkeyDown;
    txtQuery.readOnly = btnOK.disabled;
    txtQuery.value = sQuery;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
	var tempStr = '';
	var match = 0;
	var sTexto = '';
	var nLength = 0;
	
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) 
    {
        sTexto = trimStr(txtQuery.value);
        nLength = sTexto.length;

        if (nLength > 0) 
        {
            if (sTexto.substring(nLength - 1, nLength) != '.')
                sTexto += '.';
        }

        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', sTexto);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );

}

function ismaxlength() 
{
    var mlength = this.getAttribute ? parseInt(this.getAttribute("maxlength")) : "";

    if (this.getAttribute && this.value.length > mlength)
        this.value = this.value.substring(0, mlength);
}

function txtQuery_onkeyDown()
{
    if (event.keyCode == 13) 
    {
        event.returnValue = false;
        event.cancel = true;
    }
}