/********************************************************************
modalpessoa.js

Library javascript para o modalpessoa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoPesq = new CDatatransport('dsoPesq');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modalpessoaBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtPessoa').disabled == false )
        txtPessoa.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Pessoa', 1);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    
    // ajusta o divPessoa
    elem = window.document.getElementById('divPessoa');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;
        
    }

    adjustElementsInForm([['lblEhCliente', 'selEhCliente', 12, 1, -10, -10],
                          ['lblParceiro', 'chkParceiro', 3, 1],
                          ['lblPessoa', 'txtPessoa', 30, 1, -10]],
                          null,null,true);

    // btnFindPesquisa
    btnFindPesquisa.disabled = true;
    with (btnFindPesquisa.style)
    {
        top = parseInt(txtPessoa.style.top, 10);
        left = parseInt(txtPessoa.style.left, 10) + parseInt(txtPessoa.style.width, 10) + 2;
        width = 80;
        height = 24;
    }

    selEhCliente.onchange = selEhCliente_onChange;    
    
    var aOptions = [[1,'Cliente'],
                    [0,'Fornecedor']];
    var i;
    
    for (i=0; i<=1; i++)
    {
        var oOption = document.createElement("OPTION");
        oOption.value = aOptions[i][0];
        oOption.text = aOptions[i][1];
        selEhCliente.add(oOption);
    }
    // Contexto de Entrada (Fornecedor)
    if ( glb_nContextoID == 5111 )
    {
        selEhCliente.selectedIndex = 1;
        chkParceiro.style.visibility = 'hidden';
        lblParceiro.style.visibility = 'hidden';
    }
    // Contexto de Saida (Cliente)
    else        
    {
        selEhCliente.selectedIndex = 0;
        chkParceiro.style.visibility = 'inherit';
        lblParceiro.style.visibility = 'inherit';
        // Checa o ckeckbox
        chkParceiro.checked = true;
    }
        
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divPessoa').style.top) + parseInt(document.getElementById('divPessoa').style.height) + ELEM_GAP;
        width = temp + 221;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);

    headerGrid(fg,['Nome',
                   'ID',
                   'Fantasia',
                   'Doc',
                   'N�mero',
                   'Cidade',
                   'UF',
                   'Pa�s',
                   'Parceiro'], [2, 8]);
    
    fg.Redraw = 2;
}

function txtPessoa_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    txtPessoa.value = trimStr(txtPessoa.value);
    
    changeBtnState(txtPessoa.value);

    if (btnFindPesquisa.disabled)
        return;
    
    startPesq(txtPessoa.value);
}


function fg_DblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S' , new Array(
                      fg.TextMatrix(fg.Row, 2), fg.TextMatrix(fg.Row, 1), selEhCliente.value, fg.TextMatrix(fg.Row, 8)));
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null );    

}

function startPesq(strPesquisa)
{
    lockControlsInModalWin(true);
    var nEhCliente = selEhCliente.value;
    var strSQL = '';
    var strSQLOrder = '';
    var strSQLClientes = '';
    var strSQLUsuarios = '';
    var strSQLFornecedores = '';
    
    strSQLOrder =  'ORDER BY fldName';

    // Clientes
    if ( nEhCliente == 1 )
    {                
        strSQLClientes = 'SELECT TOP 100 b.Nome AS fldName,b.PessoaID AS fldID,b.Fantasia AS Fantasia, ' +
                         'd.ItemAbreviado AS Doc, c.Numero AS Numero, f.Localidade ' +
                         'AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais, ' +
                         '(CASE WHEN (SELECT COUNT (cc.PessoaID) ' +
                                  'FROM RelacoesPessoas aa WITH(NOLOCK), RelacoesPessoas bb WITH(NOLOCK), Pessoas cc WITH(NOLOCK) ' +
                                  'WHERE b.PessoaID=aa.SujeitoID AND aa.TipoRelacaoID=21 AND ' +
                                  'aa.EstadoID=2 AND aa.ObjetoID=bb.SujeitoID AND ' +
                                  'bb.TipoRelacaoID=21 AND bb.EstadoID=2 AND bb.ObjetoID=' + glb_nEmpresaID + ' ' +
                                  'AND bb.SujeitoID=cc.PessoaID AND cc.EstadoID=2 )>0 THEN 2 ELSE 1 END ) AS Parceiro ' +
                         'FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                         'LEFT OUTER JOIN Pessoas_Documentos c WITH(NOLOCK) ON (b.PessoaID = c.PessoaID AND (c.TipoDocumentoID=101 OR c.TipoDocumentoID=111)) ' +
                         'LEFT OUTER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON c.TipoDocumentoID = d.ItemID ' +
                         'LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (b.PessoaID = e.PessoaID AND (e.Ordem=1)) ' +
                         'LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID ' +
                         'LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID ' +
                         'LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID ' +
                         'WHERE a.ObjetoID = ' + glb_nEmpresaID + ' AND a.TipoRelacaoID = 21 ' +
                         'AND a.EstadoID = 2 AND a.SujeitoID = b.PessoaID ' +
                         'AND b.EstadoID = 2 AND b.Nome >= ' + '\'' + strPesquisa + '\'' + ' ';

        // Parceiros e Usuarios
        if ( !chkParceiro.checked )
        {
            strSQLUsuarios = 'UNION ALL ' +
                            'SELECT DISTINCT TOP 100 d.Nome AS fldName,d.PessoaID AS fldID,d.Fantasia AS Fantasia, ' +
                            'f.ItemAbreviado AS Doc, e.Numero AS Numero, h.Localidade ' +
                            'AS Cidade, i.CodigoLocalidade2 AS UF, j.CodigoLocalidade2 AS Pais, 0 AS Parceiro ' +
                            'FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK), RelacoesPessoas c WITH(NOLOCK), Pessoas d WITH(NOLOCK) ' +
                            'LEFT OUTER JOIN Pessoas_Documentos e WITH(NOLOCK) ON (d.PessoaID = e.PessoaID AND (e.TipoDocumentoID=101 OR e.TipoDocumentoID=111)) ' +
                            'LEFT OUTER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON e.TipoDocumentoID = f.ItemID ' +
                            'LEFT OUTER JOIN Pessoas_Enderecos g WITH(NOLOCK) ON (d.PessoaID = g.PessoaID AND (g.Ordem=1)) ' +
                            'LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON g.CidadeID = h.LocalidadeID ' +
                            'LEFT OUTER JOIN Localidades i WITH(NOLOCK) ON g.UFID = i.LocalidadeID ' +
                            'LEFT OUTER JOIN Localidades j WITH(NOLOCK) ON g.PaisID = j.LocalidadeID ' +
                            'WHERE a.ObjetoID = ' + glb_nEmpresaID + ' AND a.TipoRelacaoID = 21 ' +
                            'AND a.EstadoID = 2 AND a.SujeitoID = b.PessoaID ' +
                            'AND b.EstadoID = 2 AND b.PessoaID=c.ObjetoID AND c.TipoRelacaoID = 21 AND c.EstadoID=2 ' +
                            'AND c.SujeitoID=d.PessoaID AND d.EstadoID=2 ' +
                            'AND b.Nome >= ' + '\'' + strPesquisa + '\'' + ' ' +
                            'AND d.PessoaID NOT IN (SELECT SujeitoID FROM RelacoesPessoas WITH(NOLOCK) WHERE ObjetoID= ' + glb_nEmpresaID + ' AND TipoRelacaoID=21 AND EstadoID=2) ';
         }             

    }
    // Fornecedores
    else
    {
        strSQLFornecedores = 'SELECT TOP 100 b.Nome AS fldName,b.PessoaID AS fldID,b.Fantasia AS Fantasia, ' +
                             'd.ItemAbreviado AS Doc, c.Numero AS Numero, f.Localidade ' +
                             'AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais, ' +
                             '1 AS Parceiro ' +
                             'FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                             'LEFT OUTER JOIN Pessoas_Documentos c WITH(NOLOCK) ON (b.PessoaID = c.PessoaID AND (c.TipoDocumentoID=101 OR c.TipoDocumentoID=111)) ' +
                             'LEFT OUTER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON c.TipoDocumentoID = d.ItemID ' +
                             'LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (b.PessoaID = e.PessoaID AND (e.Ordem=1)) ' +
                             'LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID ' +
                             'LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID ' +
                             'LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID ' +
                             'WHERE a.SujeitoID = ' + glb_nEmpresaID + ' AND ( a.TipoRelacaoID BETWEEN 21 AND 22) ' +
                             'AND a.EstadoID = 2 AND a.ObjetoID = b.PessoaID ' +
                             'AND b.EstadoID = 2 AND b.Nome >= ' + '\'' + strPesquisa + '\'' + ' ';
    }

    strSQL = strSQLClientes + strSQLUsuarios + strSQLFornecedores +  strSQLOrder;

    setConnection(dsoPesq);
    dsoPesq.SQL = strSQL;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC()
{
    startGridInterface(fg);
    fg.FrozenCols = 0;
    headerGrid(fg,['Nome',
                   'ID',
                   'Fantasia',
                   'Doc',
                   'N�mero',
                   'Cidade',
                   'UF',
                   'Pa�s',
                   'Parceiro'], [2, 8]);

    fillGridMask(fg,dsoPesq,['fldName',
                             'fldID',
                             'Fantasia',
                             'Doc',
                             'Numero',
                             'Cidade',
                             'UF',
                             'Pais',
                             'Parceiro'],['','','','','','','','','']);
    fg.FrozenCols = 1;

    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
        btnOK.disabled = false;
    else
        btnOK.disabled = true;    
        
    // writeInStatusBar('child', 'cellMode', 'XXXXXXXX');    
    
}
// Funcao disparada no on change do combo selEhCliente
function selEhCliente_onChange ()
{
    if (this.value == 0)
    {
        chkParceiro.style.visibility = 'hidden';
        lblParceiro.style.visibility = 'hidden';
    }
    else        
    {
        chkParceiro.style.visibility = 'inherit';
        lblParceiro.style.visibility = 'inherit';
    }
    fg.Rows = 1;
    btnOK.disabled = true;
}
