
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Dim sURLRoot
    If (Mid(pagesURLRoot, 1, 10) <> "http://www") Then
		sURLRoot = "http://localhost/overfly3"
	Else
		sURLRoot = pagesURLRoot
	End If

    Set objSvrCfg = Nothing
%>

<%
Function ReplaceInText(baseStr, patrn, replStr)
    Dim regEx                         ' Create variables.
    Set regEx = New RegExp            ' Create regular expression.
    regEx.Pattern = patrn             ' Set pattern.
    regEx.IgnoreCase = True           ' Make case insensitive.
    regEx.Global = True               ' Replace globally
    ReplaceInText = regEx.Replace(baseStr, replStr)   ' Make replacement.
End Function
%>

<html id="viewcartacorrecaoHtml" name="viewcartacorrecaoHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/cartascorrecao/modalpages/viewcartacorrecao.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/cartascorrecao/modalpages/viewcartacorrecao.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i
Dim nCartaCorrecaoID

For i = 1 To Request.QueryString("nCartaCorrecaoID").Count    
	nCartaCorrecaoID = Request.QueryString("nCartaCorrecaoID")(i)
Next

Response.Write "var PAGES_URL_ROOT = " & Chr(39) & CStr(pagesURLRoot) & Chr(39) & ";"
Response.Write "var glb_CartaCorrecaoID = " & CStr(nCartaCorrecaoID) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

Dim sNomeEmpresa, sNomeCliente, sdtCartaCorrecao, sSequencialEvento, sCorrecao, sNotaFiscal, sdtNotaFiscal, sChaveAcesso
Dim sProtocolo, sdtProtocolo, sEmpresaCNPJ, sEmpresaIE, sClienteCNPJ, sClienteIE, sClienteEndereco1, sClienteEndereco2
Dim sEmpresaEndereco1, sEmpresaEndereco2, strSQL, rsData, sAno, sMes, sDia, sNomeArquivo, sClienteID, sEmpresaID, sTipo

sNomeEmpresa = ""
sNomeCliente = ""
sdtCartaCorrecao = ""
sSequencialEvento = ""
sCorrecao = ""
sNotaFiscal = ""
sdtNotaFiscal = ""
sChaveAcesso = ""
sProtocolo = ""
sdtProtocolo = ""
sEmpresaCNPJ = ""
sEmpresaIE = ""
sClienteCNPJ = ""
sClienteIE = ""
sClienteEndereco1 = ""
sClienteEndereco2 = ""
sEmpresaEndereco1 = ""
sEmpresaEndereco2 = ""
sAno = ""
sMes = ""
sDia = ""
sNomeArquivo = ""
sClienteID = ""
sEmpresaID = ""
sTipo = ""

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT TOP 1 a.CartaCorrecaoID, CONVERT(VARCHAR(10), a.dtCartaCorrecao, 103) + ' ' + CONVERT(VARCHAR(10), a.dtCartaCorrecao, 108) AS dtCartaCorrecao, " & _
    "a.SequencialEvento,a.Correcao, b.NotaFiscal, CONVERT(VARCHAR(10), b.dtNotaFiscal,103) dtNotaFiscal, " & _ 
    "b.ChaveAcesso, b.Protocolo, CONVERT(VARCHAR(10),b.dtProtocolo,103) + ' ' + CONVERT(VARCHAR(10),b.dtProtocolo,108) AS dtProtocolo, " & _ 
    "dbo.fn_Pessoa_Nome(a.EmpresaID) AS NomeEmpresa, dbo.fn_Pessoa_Nome(c.PessoaID) AS NomeCliente, " & _
    "(CASE WHEN c.TipoPessoaID = 51 THEN 'CPF ' + dbo.fn_Pessoa_Documento(c.PessoaID,d.PaisID,NULL,101,101,0) ELSE 'CNPJ ' + dbo.fn_Pessoa_Documento(c.PessoaID,d.PaisID,NULL,111,111,0) END) AS ClienteDocumentoFederal, " & _
    "(CASE WHEN c.TipoPessoaID = 51 THEN 'RG ' + dbo.fn_Pessoa_Documento(c.PessoaID,d.PaisID,NULL,102,102,0) ELSE 'IE ' + dbo.fn_Pessoa_Documento(c.PessoaID,d.PaisID,NULL,112,112,0) END) AS ClienteDocumentoEstadual, " & _
    "dbo.fn_Pessoa_Documento(a.EmpresaID,h.PaisID,NULL,111,111,0) AS EmpresaCNPJ, dbo.fn_Pessoa_Documento(a.EmpresaID,h.PaisID,NULL,112,112,0) AS EmpresaIE," & _
    "ISNULL(d.Endereco + space(1), SPACE(0)) + ISNULL(d.Numero + space(1), SPACE(0)) + ISNULL(d.Bairro, SPACE(0)) AS ClienteEndereco1, " & _
    "ISNULL(d.CEP + space(1), SPACE(0)) + ISNULL(e.Localidade + space(1), SPACE(0)) + ISNULL(f.CodigoLocalidade2 + space(1), SPACE(0)) + ISNULL(g.CodigoLocalidade2, SPACE(0)) AS ClienteEndereco2, " & _
    "ISNULL(h.Endereco + space(1), SPACE(0)) + ISNULL(h.Numero + space(1), SPACE(0)) + ISNULL(h.Bairro, SPACE(0)) AS EmpresaEndereco1, " & _
    "ISNULL(h.CEP + space(1), SPACE(0)) + ISNULL(i.Localidade + space(1), SPACE(0)) + ISNULL(j.CodigoLocalidade2 + space(1), SPACE(0)) + ISNULL(k.CodigoLocalidade2, SPACE(0)) AS EmpresaEndereco2, " & _               
    "dbo.fn_Pad(DAY(dtCartaCorrecao), 2, '0', 'L') AS DiaCarta, dbo.fn_Pad(MONTH(dtCartaCorrecao), 2, '0', 'L') AS MesCarta, YEAR(dtCartaCorrecao) AS AnoCarta, ISNULL(a.ArquivoXMLCCe,'') AS NomeArquivo, c.PessoaID AS ClienteID, a.EmpresaID, dbo.fn_TipoAuxiliar_Item (TipoCartaCorrecaoID, 0) AS TipoCartaCorrecaoID " & _
    "FROM CartasCorrecao a WITH(NOLOCK) " & _
	        "INNER JOIN NotasFiscais b WITH(NOLOCK) ON a.NotaFiscalID = b.NotaFiscalID " & _
	        "INNER JOIN NotasFiscais_Pessoas c WITH(NOLOCK) ON b.NotaFiscalID = c.NotaFiscalID " & _
	        "INNER JOIN Pessoas_Enderecos d WITH(NOLOCK) ON c.PessoaID = d.PessoaID " & _
	        "INNER JOIN Localidades e WITH(NOLOCK) ON d.CidadeID = e.LocalidadeID " & _
	        "INNER JOIN Localidades f WITH(NOLOCK) ON d.UFID = f.LocalidadeID " & _
	        "INNER JOIN Localidades g WITH(NOLOCK) ON d.PaisID = g.LocalidadeID " & _
	        "INNER JOIN Pessoas_Enderecos h WITH(NOLOCK) ON b.EmpresaID = h.PessoaID " & _
	        "INNER JOIN Localidades i WITH(NOLOCK) ON h.CidadeID = i.LocalidadeID " & _
	        "INNER JOIN Localidades j WITH(NOLOCK) ON h.UFID = j.LocalidadeID " & _
	        "INNER JOIN Localidades k WITH(NOLOCK) ON h.PaisID = k.LocalidadeID " & _
    "WHERE a.CartaCorrecaoID = " & CStr(nCartaCorrecaoID) & " " & _
	        "AND c.TipoID = 791 " & _
    "ORDER BY d.EndFaturamento DESC, d.Ordem "

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

If (Not (rsData.BOF AND rsData.EOF) ) Then    
    sNomeEmpresa = rsData.Fields("NomeEmpresa").Value
    sNomeCliente = rsData.Fields("NomeCliente").Value
    sdtCartaCorrecao = rsData.Fields("dtCartaCorrecao").Value
    sSequencialEvento = rsData.Fields("SequencialEvento").Value
    sCorrecao = rsData.Fields("Correcao").Value
    sNotaFiscal = rsData.Fields("NotaFiscal").Value
    sdtNotaFiscal = rsData.Fields("dtNotaFiscal").Value
    sChaveAcesso = rsData.Fields("ChaveAcesso").Value
    sProtocolo = rsData.Fields("Protocolo").Value
    sdtProtocolo = rsData.Fields("dtProtocolo").Value
    sEmpresaCNPJ = rsData.Fields("EmpresaCNPJ").Value
    sEmpresaIE = rsData.Fields("EmpresaIE").Value
    sClienteCNPJ = rsData.Fields("ClienteDocumentoFederal").Value
    sClienteIE = rsData.Fields("ClienteDocumentoEstadual").Value
    sClienteEndereco1 = rsData.Fields("ClienteEndereco1").Value
    sClienteEndereco2 = rsData.Fields("ClienteEndereco2").Value
    sEmpresaEndereco1 = rsData.Fields("EmpresaEndereco1").Value
    sEmpresaEndereco2 = rsData.Fields("EmpresaEndereco2").Value
    sDia = rsData.Fields("DiaCarta").Value
    sMes = rsData.Fields("MesCarta").Value
    sAno = rsData.Fields("AnoCarta").Value
    sNomeArquivo = rsData.Fields("NomeArquivo").Value
    sClienteID = rsData.Fields("ClienteID").Value
    sEmpresaID = rsData.Fields("EmpresaID").Value
    sTipo = rsData.Fields("TipoCartaCorrecaoID").Value
    
End If    

rsData.Close
Set rsData = Nothing


Response.Write "<script ID=" & Chr(34) & "serverSideVars1"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "var glb_AnoCarta = '" & CStr(sAno) & "';"
Response.Write "var glb_MesCarta = '" & CStr(sMes) & "';"
Response.Write "var glb_DiaCarta = '" & CStr(sDia) & "';"
Response.Write "var glb_NomeArquivo = '" & CStr(sNomeArquivo) & "';"
Response.Write "var glb_ClienteID = " & CStr(sClienteID) & ";"
Response.Write "var glb_EmpresaID = " & CStr(sEmpresaID) & ";"
Response.Write "var glb_Tipo = '" & CStr(sTipo) & "';"

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>
</head>
    <body id="viewcartacorrecaoBody" name="viewcartacorrecaoBody" LANGUAGE="javascript" onload="return window_onload()">
        <!-- Div relatorio -->
        <div id="divReportBorder" name="divReportBorder">
            <table id="tbl1" cellspacing="0" width="900px" style="font-size:12pt; border-bottom-style: solid; border-bottom-color: black; border-bottom-width: 1px;">
	            <tr >
		            <th align=center colspan="2">CARTA DE CORRE��O - CCe<br/><br/><br/><br/></th>				
	            </tr>
            </table>
            <table id="tbl2" cellspacing="0" width="900px" style="font-size:12pt; border-bottom-style: solid; border-bottom-color: black; border-bottom-width: 1px;">
                <tr>
		            <td colspan="2"> 
			            Emitente <br/><br/>
			            <strong><% Response.Write(sNomeEmpresa) %></strong><br/>         
                        <% Response.Write(sEmpresaEndereco1) %> <br/>       
                        <% Response.Write(sEmpresaEndereco2) %> <br/>       
                        <% Response.Write(sEmpresaCNPJ) %><br/>        
                        <% Response.Write(sEmpresaIE) %><br/><br/>
			        </td>				
		        </tr>				
            </table>	
            <table id="tbl3" cellspacing="0" width="900px" style="font-size:12pt; border-bottom-style: solid; border-bottom-color: black; border-bottom-width: 1px;">
                <tr>
		            <td colspan="2"> 
		            Destinat�rio <br/><br/>
		            <strong><% Response.Write(sNomeCliente) %></strong><br/>         
                    <% Response.Write(sClienteEndereco1) %> <br/>       
                    <% Response.Write(sClienteEndereco2) %> <br/>       
                    <% Response.Write(sClienteCNPJ) %><br/>        
                    <% Response.Write(sClienteIE) %><br/><br/>
		            </td>				
	            </tr>				
            </table>
            <table id="tbl4" cellspacing="0" width="900px" style="font-size:12pt; border-bottom-style: solid; border-bottom-color: black; border-bottom-width: 1px;">
    		        <tr>
			            <td>
                            Dados da Carta de Corre��o <br/><br/>
			                <img alt="" src="<%Response.Write(sURLRoot)%>/serversidegen/barcodeimage.asp?nBarCodeType=6&nBarCodeNumber=<%Response.Write(sChaveAcesso)%>&nNarrowBarsWidth=2&nNarrowBarsHeight=70"/><br/><br/>
			                Carta de Corre��o: <% Response.Write(CStr(nCartaCorrecaoID) & " " & sdtCartaCorrecao) %><br/>
                            <strong>Nota Fiscal: <% Response.Write(sNotaFiscal & " " & sdtNotaFiscal) %></strong><br/>
                            Chave de acesso: <% Response.Write(sChaveAcesso) %>  <br/>
                               
                            Protocolo de autoriza��o de uso da NFe: <% Response.Write(sProtocolo & " " & sdtProtocolo) %><br/>
                            Sequencia: <% Response.Write(sSequencialEvento) %><br/><br/>
			            </td>
			            <td style="font-size:16pt">
				            
			            </td>
		            </tr>
            </table>
            <table id="tbl5" cellspacing="0" height="630px" width="900px" style="font-size:12pt; border-bottom-style: solid; border-bottom-color: black; border-bottom-width: 1px;">
                    <tr  valign=top>
			            <td colspan="2"> 
			            Corre��es <br/><br/>
			            <% Response.Write(sCorrecao) %><br/><br/>              
			            </td>				
		            </tr>
            </table>				       
            <table id="tbl6" cellspacing="0" width="900px" style="font-size:10pt; border: none;">
			        <tr>
			            <td colspan="2"> 
			            Condi��es de uso da Carta de Corre��o <br/><br/>
			            
                        A Carta de Corre��o � disciplinada pelo � 1�-A do art. 7� do Conv�nio S/N, de 15 de dezembro de 1970 e pode ser utilizada <br/>
                        para regulariza��o de erro ocorrido na emiss�o de documento fiscal, desde que o erro n�o esteja relacionado com:<br/> 
                        I - as vari�veis que determinam o valor do imposto tais como: base de c�lculo, al�quota, diferen�a de pre�o, quantidade, valor da opera��o ou da presta��o;<br/> 
                        II - a corre��o de dados cadastrais que implique mudan�a do remetente ou do destinat�rio;<br/> 
                        III - a data de emiss�o ou de sa�da.
			            </td>				
		            </tr>
            </table>				       
        </div>  
    </body>
</html>
