/********************************************************************
modalloadxml.js

Library javascript para o modalloadxml.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nUserID = 0;

var dsoXMLFileNFe = new CDatatransport('dsoXMLFileNFe');
var dsoEnviaNFe = new CDatatransport('dsoEnviaNFe');
var dsoModeloNF = new CDatatransport('dsoModeloNF');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
adjustFrame()
setupPage()
chks_onclick()
btn_onclick(ctl)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() 
{
    window_onload_1stPart();

    // trava a interface
    lockControlsInModalWin(true);

    setupPage();

    loadPageFromServerInFrame();

    lockControlsInModalWin(false);

    showExtFrame(window, true);
    window.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() 
{
    var elem, elemBase;
    var elemToFocus;
    var aEmpresaData;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;

    // ajusta o body do html
    with (modalviewcartacorrecaoBody) 
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    secText('Carta de Corre��o - CCe', 1);
    
    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) 
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
        frameRect[1] += 3;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    with (divFrame.style) 
    {
        border = 'none';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.top, 10) +
				      parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = parseInt(divMod01.currentStyle.width, 10) - (2 * ELEM_GAP);
        height = parseInt(btnOk.currentStyle.top, 10) - parseInt(btnOk.currentStyle.height, 10) - (ELEM_GAP * 2);
    }

    // ajusta o frame de documento
    adjustFrame();

    btnCanc.style.visibility = 'hidden';
    btnXML.style.top = parseInt(btnCanc.currentStyle.top, 10);
    btnXML.style.left = parseInt(btnCanc.currentStyle.left, 10);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) 
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    lockControlsInModalWin(true);

    // Imprime o documento exibido na modal.
    if (controlID == 'btnOk') 
    {
        imprimirConteudo();
    }
    else if (controlID == 'btnXML') 
    {
        carregaXML();
    }
    else if (controlID == 'btnCanc') 
    {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_SUP', null);
    }
}

/********************************************************************
Ajusta o frame que carrega as paginas e carrega a primeira pagina
********************************************************************/
function adjustFrame() 
{
    var oFrame = null;
    var hGap = ELEM_GAP;
    var vGap = ELEM_GAP;
    var i;
    var coll;

    // todos os iframes
    coll = document.all.tags('IFRAME');

    for (i = 0; i < coll.length; i++) 
    {
        if ((coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO') 
        {
            oFrame = coll.item(i);

            with (oFrame.style) 
            {
                left = hGap;
                top = parseInt(divMod01.currentStyle.top, 10) +
				      parseInt(divMod01.currentStyle.height, 10) + vGap;
                width = '100%'; //parseInt(divMod01.currentStyle.width, 10) - (2 * hGap);
                height = '100%';//(parseInt(btnOk.currentStyle.top, 10) -
						   //parseInt(oFrame.currentStyle.top, 10) - vGap);

                backgroundColor = 'blue';

                borderStyle = 'inset';
                borderWidth = 2;
            }

            break;
        }
    }

}

/********************************************************************
Mostra ou esconde o frame que carrega o documento
********************************************************************/
function showDocumentFrame(action) 
{
    var coll, i, oFrame;

    // todos os iframes
    coll = document.all.tags('IFRAME');

    for (i = 0; i < coll.length; i++) 
    {
        if ((coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO') 
        {
            oFrame = coll.item(i);

            with (oFrame.style) 
            {
                if (action)
                    visibility = 'inherit';
                else
                    visibility = 'hidden';
            }

            break;
        }
    }
}

/********************************************************************
Carrega o boleto do servidor.

Parametros
- btnBoleto
- chkReciboEntrega
********************************************************************/
function loadPageFromServerInFrame() 
{
    var i, coll, oFrame;
    var strPars = '';

    // todos os iframes
    coll = document.all.tags('IFRAME');

    // Carrega novo documento se for o caso
    for (i = 0; i < coll.length; i++) 
    {
        if ((coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO') 
        {
            oFrame = coll.item(i);
            oFrame.tabIndex = -1;
            strPars = '?nCartaCorrecaoID=' + escape(glb_CartaCorrecaoID);

            oFrame.src = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/cartascorrecao/modalpages/viewcartacorrecao.asp' + strPars;

            break;
        }
    }
}

function imprimirConteudo() 
{
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'EstadoID\'].value');

    if (nEstadoID == 74)
        window.frames(0).imprimirBoleto();
    else 
    {
        if (window.top.overflyGen.Alert('A CCe deve estar Vinculada (V) para ser impressa.') == 0)
            return null;
    }            

    // esta funcao destrava o html contido na janela modal
    lockControlsInModalWin(false);
}

function carregaXML() 
{
    lockControlsInModalWin(false);
    window.frames(0).carregaXML();
}