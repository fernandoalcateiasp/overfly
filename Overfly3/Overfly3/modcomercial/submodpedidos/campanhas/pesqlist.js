/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form dpa
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoListData01 = new CDatatransport('dsoListData01');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoPropsPL = new CDatatransport('dsoPropsPL');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();

    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Emiss�o', 'Campanha', 'C�digo', 'Fornecedor', 'Contato', 
		'Cliente', 'Identificador', 'In�cio', 'Fim', 'Moeda', 'Valor Total', 'Saldo', 'Ped', 'Int', 'Apr', 'Custo', 'DPA', 'Observa��o');

    //@@ Formatacao das colunas do grid de pesquisa, definicao opcional
    // padrao do framework = null se variavel nao for definida aqui
    var dTFormat = '';
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    glb_aCOLPESQFORMAT = new Array('', '', dTFormat, '', '', '', '', '', '', dTFormat, dTFormat, '', 
		'###,###,###,###.00', '###,###,###,###.00', '#,###,###', '', '', '', '', '');

    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
	//especBtnIsPrintBtn('sup', 0);
	showBtnsEspecControlBar('sup', true, [1,1,1,1,1,0,0]);
	tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'DPA Pendentes', 'Associa Financeiros', '', '']);
	setupEspecBtnsControlBar('sup', 'HHHHHDD');
    
    alignColsInGrid(fg,[0,12,13,14]);
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    // Usuario clicou botao documentos
    if (btnClicked == 1) {
        if (fg.Rows > 1) {
            __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
        }
        else {
            window.top.overflyGen.Alert('Selecione um registro.');
        }
    }
    else if ( controlBar == 'SUP' )
    {
        // usuario clicou botao imprimir
        if ( btnClicked == 4 )
            openModalCampanhasPendentes();
        // usuario clicou botao imprimir
        if (btnClicked == 5)
            openModalAssociaFinanciero();
    }    
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Modal de impressao
    if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }    
    else if ( idElement.toUpperCase() == 'MODALCAMPANHASPENDENTESHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
			
			if (param2 != 0)
				sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWDETAIL', 
							[param2, null, null]);	

            // nao mexer
            return 0;
        }
    }
    else if ( idElement.toUpperCase() == 'MODALDPAPENDENTESHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
			
			if (param2 != 0)
				sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWDETAIL', 
							[param2, null, null]);	

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALASSOCIARFINANCEIROHTML')
    {
        if (param1 == 'OK')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalCampanhasPendentes()
{
	var htmlPath;
	var strPars = new String();

	strPars = '?sCaller=' + escape('PL');
	strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);

	// carregar modal - faz operacao de banco no carregamento
	htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/campanhas/modalpages/modalcampanhaspendentes.asp' + strPars;
	showModalWin(htmlPath, new Array(775,460));
}

function openModalAssociaFinanciero()
{
    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/campanhas/modalpages/modalassociarfinanceiro.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 540));
}