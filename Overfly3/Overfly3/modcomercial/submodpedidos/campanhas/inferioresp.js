/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de campanhas
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.campanhaID, a.ProprietarioID,a.AlternativoID,a.Observacoes FROM campanhas a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.campanhaID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24151) // Empresas
    {
        var nTipoCampanhaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,
                                  'dsoSup01.recordset[' + '\'' + 'TipoCampanhaID' + '\'' + '].value');

		var sOrderBy = '';

		if (nTipoCampanhaID == 1321)
		    sOrderBy = 'ORDER BY a.EhDefault DESC, (SELECT b.Fantasia FROM Pessoas b WITH(NOLOCK) WHERE (a.EmpresaID = b.PessoaID))';
		else
		    sOrderBy = 'ORDER BY (SELECT b.Fantasia FROM Pessoas b WITH(NOLOCK) WHERE (a.EmpresaID = b.PessoaID))';

        dso.SQL = 'SELECT * ' +
                  'FROM Campanhas_Empresas a WITH(NOLOCK) ' + 
                  'WHERE a.CampanhaID = '+ idToFind + ' ' + sOrderBy;
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24152) // Produtos
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Campanhas_Produtos a WITH(NOLOCK) ' + 
                  'WHERE a.CampanhaID = '+ idToFind + ' ' +
                  'ORDER BY ISNULL(a.Classe, SPACE(0)), a.ProdutoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24153) // Bundle
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Campanhas_Bundles a WITH(NOLOCK) ' + 
                  'WHERE a.CampanhaID = '+ idToFind + ' ' +
                  'ORDER BY Bundle, Classe';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24154) // Pedidos
    {
        var nMoedaSistemaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,
                                  'dsoSup01.recordset[' + '\'' + 'MoedaSistemaID' + '\'' + '].value');

		dso.SQL = 'SELECT j.Fantasia, d.PedidoID, f.RecursoAbreviado, dbo.fn_Data_Zero(d.dtMovimento) AS Data, l.NotaFiscal, g.Fantasia AS Parceiro, h.Fantasia AS Vendedor, ' +
				'i.ConceitoID AS ProdutoID, i.Conceito AS Produto, i.Modelo AS Modelo, (d.Quantidade * POWER(-1, e.TipoPedidoID)) AS Quantidade, ' +
				'CONVERT(NUMERIC(11,2), dbo.fn_DivideZero((CASE WHEN a.MoedaID = ' + nMoedaSistemaID + ' THEN 1 WHEN a.MoedaID = e.MoedaID THEN e.TaxaMoeda ELSE dbo.fn_Preco_Cotacao(' + nMoedaSistemaID + ', a.MoedaID, NULL, GETDATE()) END) * c.Valor, d.Quantidade)) AS ValorUnitario, ' +
				'CONVERT(NUMERIC(11,2), (CASE WHEN a.MoedaID = ' + nMoedaSistemaID + ' THEN 1 WHEN a.MoedaID = e.MoedaID THEN e.TaxaMoeda ELSE dbo.fn_Preco_Cotacao(' + nMoedaSistemaID + ', a.MoedaID, NULL, GETDATE()) END) * c.Valor * POWER(-1, e.TipoPedidoID)) AS ValorTotal, ' +
				'm.RelacaoID ' +
			'FROM Campanhas a WITH(NOLOCK) ' +
			    'INNER JOIN Campanhas_Produtos b WITH(NOLOCK) ON (a.CampanhaID = b.CampanhaID) ' +
			    'INNER JOIN Pedidos_Itens_Campanhas c WITH(NOLOCK) ON (b.CamProdutoID = c.CamProdutoID) ' +
			    'INNER JOIN Pedidos_Itens d WITH(NOLOCK) ON (c.PedItemID = d.PedItemID) ' +
			    'INNER JOIN Pedidos e WITH(NOLOCK) ON (d.PedidoID = e.PedidoID) ' +
			    'INNER JOIN Recursos f WITH(NOLOCK) ON (e.EstadoID = f.RecursoID) ' +
			    'INNER JOIN Pessoas g WITH(NOLOCK) ON (e.ParceiroID = g.PessoaID) ' +
			    'INNER JOIN Pessoas h WITH(NOLOCK) ON (e.ProprietarioID = h.PessoaID) ' +
			    'INNER JOIN Conceitos i WITH(NOLOCK) ON (d.ProdutoID = i.ConceitoID) ' +
			    'INNER JOIN Pessoas j WITH(NOLOCK) ON (e.EmpresaID = j.PessoaID) ' +
			    'LEFT OUTER JOIN NotasFiscais l WITH(NOLOCK) ON (e.NotaFiscalID = l.NotaFiscalID AND l.EstadoID=67) ' +
			    'INNER JOIN RelacoesPesCon m WITH(NOLOCK) ON (i.ConceitoID = m.ObjetoID AND e.EmpresaID = m.SujeitoID) ' +
			'WHERE (a.CampanhaID = ' + idToFind + ' AND m.TipoRelacaoID = 61) ' +
			'ORDER BY j.PessoaID, dbo.fn_Data_Zero(d.dtMovimento), i.ConceitoID';		

        return 'dso01Grid_DSC';
    }
    else if (folderID == 24155) // Pedidos Externos
    {
        dso.SQL = 'SELECT * ' + 
                  'FROM Campanhas_PedidosExternos a WITH(NOLOCK) ' + 
                  'WHERE a.CampanhaID = '+ idToFind + ' ' +
                  'ORDER BY (SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE (PessoaID = a.ParceiroID)), a.dtPedido, a.Pedido, a.NotaFiscal';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24156) // Financeiros Campanhas
    {
        dso.SQL = 'SELECT a.CampanhaID, e.PedidoID, h.Conceito, f.FinanceiroID, f.EstadoID, c.Valor, g.RecursoAbreviado ' +
                    'FROM Campanhas a ' +
                        'INNER JOIN Campanhas_Produtos b WITH(NOLOCK) ON (b.CampanhaID = a.CampanhaID) ' +
                        'INNER JOIN Pedidos_Itens_Campanhas c WITH(NOLOCK) ON (c.CamProdutoID = b.CamProdutoID) ' +
                        'INNER JOIN Financeiro_PedidosItensCampanhas d WITH(NOLOCK) ON (d.PedIteCampanhaID = c.PedIteCampanhaID) ' +
                        'INNER JOIN Pedidos_Itens e WITH(NOLOCK) ON (c.PedItemID = e.PedItemID) ' +
                        'INNER JOIN Financeiro f WITH(NOLOCK) ON (d.FinanceiroID = f.FinanceiroID) ' +
                        'INNER JOIN Recursos g WITH(NOLOCK) ON (f.EstadoID = g.RecursoID) ' +
                        'INNER JOIN Conceitos h WITH(NOLOCK) ON (b.ProdutoID = h.ConceitoID) ' +
                    'WHERE (a.CampanhaID = ' + idToFind + ') ';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Empresas
    else if ( pastaID == 24151 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT a.PessoaID AS PessoaID, a.Fantasia AS Fantasia ' +
                      'FROM Pessoas a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK) ' +
                      'WHERE b.EstadoID = 2 AND b.TipoRelacaoID = 12 ' +
                      'AND b.ObjetoID = 999 AND b.SujeitoID = a.PessoaID ' +
                      'ORDER BY Fantasia';
        }
    }
    // Produtos
    else if ( pastaID == 24152 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'EXEC sp_Campanhas_Combos ' + nRegistroID + ', 1';
        }
        else if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.ConceitoID, b.Conceito, b.Modelo ' +
                      'FROM Campanhas_Produtos a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
                      'WHERE (a.CampanhaID=' + nRegistroID + ' AND a.ProdutoID = b.ConceitoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT a.CamProdutoID, ' +
						'dbo.fn_CampanhaProduto_Totais(CamProdutoID, NULL, 7) AS Saldo, ' +
						'dbo.fn_CampanhaProduto_Totais(CamProdutoID, NULL, 8) AS SaldoValor, ' +
						'dbo.fn_CampanhaProduto_Totais(CamProdutoID, NULL, 2) AS Custo, ' +
						'dbo.fn_CampanhaProduto_Totais(CamProdutoID, NULL, 3) AS Diferenca, ' +
						'dbo.fn_CampanhaProduto_Totais(CamProdutoID, NULL, 9) AS Percentual ' +
                      'FROM Campanhas_Produtos a WITH(NOLOCK) ' +
                      'WHERE (a.CampanhaID=' + nRegistroID + ')';
        }
    }
    // Bundle
    else if ( pastaID == 24153 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'EXEC sp_Campanhas_Combos ' + nRegistroID + ', 2';
        }
        else if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT DISTINCT Classe ' +
				'FROM Campanhas_Produtos WITH(NOLOCK) ' +
				'WHERE CampanhaID = ' + nRegistroID;
        }
	}    
    // Pedidos Externos
    else if ( pastaID == 24155 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT DISTINCT b.ConceitoID, b.Conceito ' +
						'FROM Campanhas_Produtos a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
                      'WHERE(a.CampanhaID = ' + nRegistroID + ' AND a.ProdutoID = b.ConceitoID) ' +
                      'ORDER BY b.Conceito';
        }
        else if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT a.CamPedExternoID, b.Fantasia, c.ConceitoID AS ProdutoID, c.Modelo, ' +
						'CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(a.ValorTotal, a.Quantidade)) AS ValorUnitario ' +
                      'FROM Campanhas_PedidosExternos a WITH(NOLOCK), Pessoas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
                      'WHERE (a.CampanhaID=' + nRegistroID + ' AND a.ParceiroID = b.PessoaID AND ' +
						'a.ProdutoID = c.ConceitoID)';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
    var nTipoCampanhaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,
                                'dsoSup01.recordset[' + '\'' + 'TipoCampanhaID' + '\'' + '].value');

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(24151);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 24151); //Empresas
 	
    vPastaName = window.top.getPastaName(24152);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 24152); //Produtos

    if (nTipoCampanhaID==1323)
    {
		vPastaName = window.top.getPastaName(24153);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 24153); //Bundle
	}

    vPastaName = window.top.getPastaName(24154);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 24154); //Pedidos

    if (nTipoCampanhaID==1321)
    {
		vPastaName = window.top.getPastaName(24155);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 24155); //Pedidos Externos
    }

    vPastaName = window.top.getPastaName(24156);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24156); //Financeiro Campanhas

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
	
	var nFiltroContextoID = getCmbCurrDataInControlBar('sup', 1)[1];
    var currLine = 0;
    
    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 24151) // Empresas
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,fg.Row,[0], ['CampanhaID', registroID]);
        }
        else if (folderID == 24152) // Produtos
        {
			// Se bundle
			if (nFiltroContextoID == 5163)
				currLine = criticLineGridAndFillDsoEx(fg, currDSO,fg.Row,[0, 6, 12], ['CampanhaID', registroID]);
			else
				currLine = criticLineGridAndFillDsoEx(fg, currDSO,fg.Row,[0, 6], ['CampanhaID', registroID]);
        }
        else if (folderID == 24153) // Bundle
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,fg.Row,[0, 1, 2], ['CampanhaID', registroID]);
        }
        else if (folderID == 24155) // Pedidos Externos
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,fg.Row,[6, 8, 10, 11], ['CampanhaID', registroID], [11]);
        }
        
        if (currLine < 0)
        {
            if (fg.disabled == false)
            {
				try
                {
                    fg.Editable = true;
					window.focus();
					fg.focus();
				}	
                catch(e)
                {
					;
                }
            }    
        
            return false;
        }    
        
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    var aHoldCols = null;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 24151) // Empresas
    {
        var nTipoCampanhaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,
                                'dsoSup01.recordset[' + '\'' + 'TipoCampanhaID' + '\'' + '].value');
    
		if (nTipoCampanhaID != 1321)
			aHoldCols = [1,2,3];
		else
			aHoldCols = [3];
		
        headerGrid(fg,['Empresa','Default','Auto','CamEmpresaID'], aHoldCols);
        
        glb_aCelHint = [[0,2,'DPA ser� incluido automaticamente em pedidos de venda desta empresa?']];
        
        fillGridMask(fg,currDSO,['EmpresaID','EhDefault', 'EhAutomatico','CamEmpresaID'],['','','','']);

        // alinha todas as colunas a esquerda
        alignColsInGrid(fg, []);
                                    
    }
    else if (folderID == 24152) // Produtos
    {
        var bCusto = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,
                                'dsoSup01.recordset[' + '\'' + 'Custo' + '\'' + '].value');
                                
        var nTipoCampanhaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,
                                'dsoSup01.recordset[' + '\'' + 'TipoCampanhaID' + '\'' + '].value');

		aHoldCols = [13];
        
        // DPA
        if (nTipoCampanhaID == 1321)
        {
			if (!bCusto)
				aHoldCols = [7, 8, 11, 12, 13];
			else
				aHoldCols = [12, 13];
        }
        // Rebate
        else if (nTipoCampanhaID == 1322)
			aHoldCols = [7, 8, 11, 12, 13];
        // Bundle
        else if (nTipoCampanhaID == 1323)
        {
			aHoldCols = [7, 8, 11, 13];
        }


        headerGrid(fg,['ID',
					   'Produto',
					   'Modelo',
					   'In�cio',
					   'Fim',
					   'Quant',
					   'Valor Unit�rio', 
					   'Custo',
					   'Diferen�a',
					   'Saldo', 
					   'Saldo Valor',
					   'Perc',
					   'Classe',
					   'CamProdutoID'], aHoldCols);

        glb_aCelHint = [[0,09,'Saldo em quantidade'],
						[0,10,'Saldo em valor'],
						[0,11,'Percentual de Desconto sobre o custo']];
        
        fillGridMask(fg,currDSO,['ProdutoID*',
								 '^ProdutoID^dso01GridLkp^ConceitoID^Conceito*',
								 '^ProdutoID^dso01GridLkp^ConceitoID^Modelo*',
								 'dtInicio',
								 'dtFim',
								 'Quantidade',
								 'ValorUnitario',
								 '^CamProdutoID^dso02GridLkp^CamProdutoID^Custo*',
								 '^CamProdutoID^dso02GridLkp^CamProdutoID^Diferenca*',
								 '^CamProdutoID^dso02GridLkp^CamProdutoID^Saldo*', 
								 '^CamProdutoID^dso02GridLkp^CamProdutoID^SaldoValor*', 
								 '^CamProdutoID^dso02GridLkp^CamProdutoID^Percentual*', 
								 'Classe',
								 'CamProdutoID'],
								 ['','','','99/99/9999','99/99/9999','999999','999999999.99','999999999.99',
								 '999999999.99','999999','999999999.99','999.99', '', ''],
								 ['','','',dTFormat,dTFormat,'######','###,###,##0.00','###,###,##0.00',
								 '###,###,##0.00','######','###,###,##0.00','##,##0.00', '', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1,'######','C'],[5,'######','S'],[8,'###,###,###.00','S'],[9,'######','S'],[10,'###,###,###.00','S']]);

        alignColsInGrid(fg, [0,5,6,7,8,9,10,11]);
                                    
    }
    else if (folderID == 24153) // Bundle
    {
        headerGrid(fg,['Bundle',
					   'Classe',
					   'Quant',
					   'CamBundleID'], [3]);

        fillGridMask(fg,currDSO,['Bundle',
								 'Classe',
								 'Quantidade',
								 'CamBundleID'],
								 ['','','999999',''],
								 ['','','######','']);

        alignColsInGrid(fg, [2]);
        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 24154) // Pedidos
    {
        headerGrid(fg,['Empresa',
					   'Pedido',
					   'Est',
					   'Data',
					   'NF',
					   'Parceiro',
					   'Vendedor',
					   'ID',
					   'Produto',
					   'Modelo',
					   'Quant',
					   'Valor Unit',
					   'Valor Total',
					   'RelacaoID'], [13]);

        fillGridMask(fg,currDSO,['Fantasia',
								 'PedidoID',
								 'RecursoAbreviado',
								 'Data',
								 'NotaFiscal',
								 'Parceiro',
								 'Vendedor',
								 'ProdutoID',
								 'Produto',
								 'Modelo',
								 'Quantidade',
								 'ValorUnitario',
								 'ValorTotal',
								 'RelacaoID'],
								 ['','','','','','','','','','','','','',''],
								 ['','','',dTFormat,'','','','','','','(##########)','###,###,##0.00','(###,###,##0.00)','']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1,'#####','C'],[10,'#####','S'],[12,'###,###,###.00','S']]);
        
        
        for ( i=1; i<fg.Rows; i++ )
		{
			if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorTotal')) < 0 )
			{
				fg.Select(i, getColIndexByColKey(fg, 'ValorTotal'), i, getColIndexByColKey(fg, 'ValorTotal'));
				fg.FillStyle = 1;
		        fg.CellForeColor = 0X0000FF;

				//fg.Select(i, 9, i, 9);
				//fg.FillStyle = 1;
		        //fg.CellForeColor = 0X0000FF;

		    }
		    if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade')) < 0 )
		    {
				fg.Select(i, getColIndexByColKey(fg, 'Quantidade'), i, getColIndexByColKey(fg, 'Quantidade'));
				fg.FillStyle = 1;
		        fg.CellForeColor = 0X0000FF;
		    }
		}

        // alinha todas as colunas a esquerda
        alignColsInGrid(fg, [1,4,7,10,11,12]);
        
        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 24155) // Pedidos Externos
    {
        headerGrid(fg,['Parceiro',
					   'Data',
					   'Pedido',
					   'NF',
					   'Cliente',
					   'ID',
					   'Produto',
					   'Modelo',
					   'Quant',
					   'Valor Unit',
					   'Valor Total',
					   'ParceiroID',
					   'CamPedExternoID'], [11,12]);

        fillGridMask(fg,currDSO,['^CamPedExternoID^dso01GridLkp^CamPedExternoID^Fantasia*',
								 'dtPedido',
								 'Pedido',
								 'NotaFiscal',
								 'Cliente',
								 '^CamPedExternoID^dso01GridLkp^CamPedExternoID^ProdutoID*',
								 'ProdutoID',
								 '^CamPedExternoID^dso01GridLkp^CamPedExternoID^Modelo*',
								 'Quantidade',
								 '^CamPedExternoID^dso01GridLkp^CamPedExternoID^ValorUnitario*',
								 'ValorTotal',
								 'ParceiroID',
								 'CamPedExternoID'],
								 ['','99/99/9999','9999999999','9999999999','','','','','999999','','999999999.99','',''],
								 ['',dTFormat,'','','','','','','','###,###,##0.00','###,###,##0.00','','']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[3,'######','C'],[8,'######','S'],[10,'###,###,###.00','S']]);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;

        // alinha todas as colunas a esquerda
        alignColsInGrid(fg, [2,3,5,8,9,10]);
    }
    else if (folderID == 24156) // Financeiros Campanhas
    {
        headerGrid(fg, ['Pedido',
                        'Produto',
                        'Financeiro',
                        'Est',
                        'Valor'], []);

        fillGridMask(fg, currDSO, ['PedidoID',
                                   'Conceito',
                                   'FinanceiroID',
                                   'RecursoAbreviado',
                                   'Valor'],
								 ['', '', '', '', '999999999.99'],
								 ['', '', '', '', '###,###,##0.00']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
}
