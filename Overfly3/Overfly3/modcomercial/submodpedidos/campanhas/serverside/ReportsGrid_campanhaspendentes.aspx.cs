﻿using System;
using System.Web;
using System.Data;

namespace Overfly3.PrintJet
{
    public partial class ReportsGrid_campanhaspendentes : System.Web.UI.OverflyPage
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;

        private int RelatorioID = Convert.ToInt32(HttpContext.Current.Request.Params["RelatorioID"]);
        private int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["formato"]);
        private bool bToExcel = Convert.ToBoolean(HttpContext.Current.Request.Params["bToExcel"]);
        private int sLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["sLinguaLogada"]);
        private string glb_sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);

        int Datateste = 0;
        bool nulo = false;

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "1":
                        relatorioCampanhasPendentes();
                        break;
                }
            }
        }

        public void relatorioCampanhasPendentes()
        {
            //Parametros da procedure
            bool chkPendentes = Convert.ToBoolean(HttpContext.Current.Request.Params["chkPendentes"]);
            int selTipoCampanhaID = Convert.ToInt32(HttpContext.Current.Request.Params["selTipoCampanhaID"]);
            int selFornecedorID = Convert.ToInt32(HttpContext.Current.Request.Params["selFornecedorID"]);
            int selClienteID = Convert.ToInt32(HttpContext.Current.Request.Params["selClienteID"]);
            string sProduto = Convert.ToString(HttpContext.Current.Request.Params["sProduto"]);
            string sdtFim = Convert.ToString(HttpContext.Current.Request.Params["sdtFim"]);

            string sFiltro = "";

            //Define formato de data de acordo com a lingua logada
            string param = "dd/MM/yyy";
            if (sLinguaLogada != 246)
                param = "MM/dd/yyy";

            //Recebe data atual para colocar no titulo do Excel
            DateTime DataHoje = DateTime.Now;
            string _data = DataHoje.ToString(param);

            param = "<Format>" + param + "</Format>";

            string Title = "Campanhas - " + glb_sEmpresaFantasia + " - " + _data;

            if (chkPendentes)
	            sFiltro += " AND b.EstadoID = 41 AND ISNULL(dbo.fn_CampanhaProduto_Datas(a.CamProdutoID, 2), dbo.fn_Data_Zero(GETDATE())) >= dbo.fn_Data_Zero(GETDATE()) ";
            else		
	            sFiltro += " AND b.EstadoID IN(41,48) ";

            if (selTipoCampanhaID > 0)
                sFiltro += " AND b.TipoCampanhaID = " + selTipoCampanhaID + " ";

            if (selFornecedorID > 0)
                sFiltro += " AND b.FornecedorID = " + selFornecedorID + " ";

            if (selClienteID > 0)
                sFiltro += " AND b.ClienteID = " + selClienteID + " ";

            if (sProduto != "")
            {
                sFiltro += " AND (ISNULL(CONVERT(VARCHAR(10), e.ConceitoID), SPACE(0)) + CHAR(95) + " +
                                 "ISNULL(e.Conceito, SPACE(0)) + CHAR(95) + " +
                                 "ISNULL(e.Modelo, SPACE(0)) + CHAR(95) + " +
                                 "ISNULL(e.Descricao, SPACE(0)) + CHAR(95) + " +
                                 "ISNULL(e.PartNumber, SPACE(0))) LIKE '" + "%" + sProduto + "%'" + " ";
            }

            if (sdtFim != "")
            {
                sFiltro += " AND ISNULL(dbo.fn_CampanhaProduto_Datas(a.CamProdutoID, 2), dbo.fn_Data_Zero(GETDATE())) <= '" + sdtFim + "' ";
            }
            
            string strSQL = "SELECT g.ItemMasculino AS TipoCampanha, c.Fantasia AS Fornecedor, ISNULL(d.Fantasia, SPACE(1)) AS Cliente, " +
                                "dbo.fn_ProgramaMarketing_Identificador(b.FornecedorID, b.ClienteID) AS Identificador,  " +
                                "a.ProdutoID AS [ID], e.Conceito AS Produto, e.Modelo AS Modelo, a.Quantidade AS Quant,  " +
                                "f.SimboloMoeda AS [Valor], a.ValorUnitario AS ValorUnit,  " +
                                "dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 2) AS Custo,  " +
                                "dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 3) AS [Diferenca],  " +
                                "dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 1) AS ValorTotal, " +
                                "CONVERT(NUMERIC(6), dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 6)) AS Vendido, " +
                                "CONVERT(NUMERIC(6), dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 7)) AS Saldo, " +
                                "dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 8) AS SaldoValor, " +
                                "b.Campanha, b.Codigo AS [Codigo], a.dtInicio AS [Inicio], a.dtFim AS Fim, b.CampanhaID " +
                            "FROM Campanhas_Produtos a WITH(NOLOCK) " +
                                "INNER JOIN Campanhas b WITH(NOLOCK) ON (a.CampanhaID = b.CampanhaID) " +
                                "LEFT OUTER JOIN Pessoas c WITH(NOLOCK) ON (b.FornecedorID = c.PessoaID) " +
                                "LEFT OUTER JOIN Pessoas d WITH(NOLOCK) ON (b.ClienteID = d.PessoaID) " +
                                "INNER JOIN Conceitos e WITH(NOLOCK) ON (a.ProdutoID = e.ConceitoID) " +
                                "INNER JOIN Conceitos f WITH(NOLOCK) ON (b.MoedaID = f.ConceitoID) " +
                                "INNER JOIN TiposAuxiliares_Itens g WITH(NOLOCK) ON (b.TipoCampanhaID = g.ItemID) " +
                            "WHERE ((1 = 1) " + sFiltro + ") " +
                            "ORDER BY TipoCampanha, Fornecedor, Cliente, Produto";

            //Se Lista btnListar
            if (!bToExcel)
            {
                //Cria DataSet com retorno da procedure
                DataSet dsLista = DataInterfaceObj.getRemoteData(strSQL);

                //Escreve XML com o DataSet
                WriteResultXML(dsLista);
            }

            //SE EXCEL btnExcel
            else if (bToExcel)
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");


                DataSet dsFin = new DataSet();
                dsFin = (DataSet)HttpContext.Current.Session["Query"];
                Datateste = dsFin.Tables.Count;

                //Verifica se a Query está vazia e retorna mensagem de erro
                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    return;
                }
                else
                {
                    if (Formato == 1) //Se formato PDF
                    {
                        ;
                    }

                    else if (Formato == 2) //Se formato Excel
                    {
                        Relatorio.CriarObjTabela("0.00", "0.00", "Query1", false, false, false);

                            Relatorio.CriarObjColunaNaTabela("TipoCampanha", "TipoCampanha", true, "TipoCampanha");
                            Relatorio.CriarObjCelulaInColuna("Query", "TipoCampanha", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Fornecedor", "Fornecedor", true, "Fornecedor");
                            Relatorio.CriarObjCelulaInColuna("Query", "Fornecedor", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Cliente", "Cliente", true, "Cliente");
                            Relatorio.CriarObjCelulaInColuna("Query", "Cliente", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Identificador", "Identificador", true, "Identificador");
                            Relatorio.CriarObjCelulaInColuna("Query", "Identificador", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ID", "ID", true, "ID");
                            Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Produto", "Produto", true, "Produto");
                            Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Modelo", "Modelo", true, "Modelo");
                            Relatorio.CriarObjCelulaInColuna("Query", "Modelo", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Quant", "Quant", true, "Quant");
                            Relatorio.CriarObjCelulaInColuna("Query", "Quant", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Valor", "Valor", true, "Valor");
                            Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ValorUnit", "ValorUnit", true, "ValorUnit");
                            Relatorio.CriarObjCelulaInColuna("Query", "ValorUnit", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Custo", "Custo", true, "Custo");
                            Relatorio.CriarObjCelulaInColuna("Query", "Custo", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Diferenca", "Diferenca", true, "Diferenca");
                            Relatorio.CriarObjCelulaInColuna("Query", "Diferenca", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ValorTotal", "ValorTotal", true, "ValorTotal");
                            Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Vendido", "Vendido", true, "Vendido");
                            Relatorio.CriarObjCelulaInColuna("Query", "Vendido", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Saldo", "Saldo", true, "Saldo");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("SaldoValor", "SaldoValor", true, "SaldoValor");
                            Relatorio.CriarObjCelulaInColuna("Query", "SaldoValor", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Campanha", "Campanha", true, "Campanha");
                            Relatorio.CriarObjCelulaInColuna("Query", "Campanha", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Codigo", "Codigo", true, "Codigo");
                            Relatorio.CriarObjCelulaInColuna("Query", "Codigo", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Inicio", "Inicio", true, "Inicio");
                            Relatorio.CriarObjCelulaInColuna("Query", "Inicio", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("Fim", "Fim", true, "Fim");
                            Relatorio.CriarObjCelulaInColuna("Query", "Fim", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("CampanhaID", "CampanhaID", true, "CampanhaID");
                            Relatorio.CriarObjCelulaInColuna("Query", "CampanhaID", "DetailGroup");

                        Relatorio.TabelaEnd();
                    }
                    Relatorio.CriarPDF_Excel(Title, Formato);
                }
            }
        }
    }
}