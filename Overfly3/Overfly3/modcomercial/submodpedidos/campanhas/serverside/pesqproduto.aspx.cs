﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.campanhas.serverside
{
    public partial class pesqproduto : System.Web.UI.OverflyPage
    {
        private string SQL = "";
        private string sFiltroFornecedor = "";        

        private string toFind;

        public string strToFind
        {
            get { return toFind; }
            set { toFind = value; }
        }
        private Integer CampanhaID;

        public Integer nCampanhaID
        {
            get { return CampanhaID; }
            set { CampanhaID = value; }
        }
        private Integer FornecedorID;

        public Integer nFornecedorID
        {
            get { return FornecedorID; }
            set { FornecedorID = value; }
        }

        private string ListarProdutos()
        {
            if (FornecedorID.intValue() != 0)
            {
                sFiltroFornecedor = " AND c.FornecedorID = " + (FornecedorID) + " ";
            }

            toFind = "'%" + toFind + "%'";
            SQL = "SELECT DISTINCT TOP 100 a.Conceito AS fldName, a.ConceitoID AS fldID, " +
                "a.Modelo AS Modelo, a.PartNumber AS PartNumber, a.Descricao AS Descricao " +
            "FROM Conceitos a WITH(NOLOCK), RelacoesPesCon b WITH(NOLOCK), RelacoesPesCon_Fornecedores c WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), " +
                "RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), " +
                "RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos1 WITH(NOLOCK) " +
            "WHERE (a.EstadoID IN (2, 11) AND a.TipoConceitoID = 303 AND " +
                "a.ConceitoID = b.ObjetoID AND b.TipoRelacaoID = 61 AND b.EstadoID NOT IN(1,4,5) AND " +
                "b.RelacaoID = c.RelacaoID " + sFiltroFornecedor + " AND a.MarcaID = Marcas.ConceitoID AND a.ProdutoID = Familias.ConceitoID AND " +
                "Familias.EstadoID = 2 AND Familias.ConceitoID = RelConceitos2.SujeitoID AND " +
                "RelConceitos2.TipoRelacaoID = 41 AND RelConceitos2.EstadoID = 2 AND " +
                "RelConceitos2.ObjetoID = Conceitos2.ConceitoID AND Conceitos2.EstadoID = 2 AND " +
                "Conceitos2.ConceitoID = RelConceitos1.SujeitoID AND RelConceitos1.TipoRelacaoID = 41 AND " +
                "RelConceitos1.EstadoID = 2 AND RelConceitos1.ObjetoID = Conceitos1.ConceitoID AND " +
                "Conceitos1.EstadoID IN (2, 11) AND " +
                "((a.Conceito LIKE " + toFind + " ) OR " +
                "(Familias.Conceito LIKE " + toFind + " ) OR " +
                "(Conceitos1.Conceito LIKE " + toFind + " ) OR " +
                "(Conceitos2.Conceito LIKE " + toFind + " ) OR " +
                "(Marcas.Conceito LIKE " + toFind + " ) OR " +
                "(a.Modelo LIKE " + toFind + " ) OR " +
                "(a.Descricao LIKE " + toFind + " ) OR " +
                "(a.PartNumber LIKE " + toFind + " ) OR " +
                "(b.ObjetoID LIKE " + toFind + " ) ) AND " +
                "(SELECT COUNT(*) FROM Campanhas_Empresas WITH(NOLOCK) WHERE (CampanhaID = " + CampanhaID + " AND EmpresaID = b.SujeitoID)) > 0 AND " +
                "(SELECT COUNT(*) FROM Campanhas_Produtos WITH(NOLOCK) WHERE (CampanhaID = " + CampanhaID + " AND ProdutoID = a.ConceitoID)) = 0) " +
            "ORDER BY fldName ";
            return SQL;
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(DataInterfaceObj.getRemoteData(ListarProdutos()));
        }
    }
}