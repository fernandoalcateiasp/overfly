using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Overfly3.modcomercial.submodpedidos.campanhas.serverside
{
	public partial class pesqparceiro : System.Web.UI.OverflyPage
	{
		private string operador = " >= ";
		private string toFind =	"";
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"SELECT TOP 100 b.Nome AS fldName,b.PessoaID AS fldID,b.Fantasia AS Fantasia, " +
					"dbo.fn_Pessoa_Documento(b.PessoaID, NULL, NULL, 111, 111, 0) AS Documento, f.Localidade " +
					"AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " +
                    "FROM Pessoas b WITH(NOLOCK) " +
                    "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (b.PessoaID = e.PessoaID AND (e.Ordem=1)) " +
                    "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " +
                    "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " +
                    "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " +
					"WHERE b.EstadoID = 2 AND b.TipoPessoaID IN (51,52) AND b.Nome " + operador + " '" + toFind + "' " +
					"ORDER BY fldName "
				)
			);
		}
		
		public string strToFind
		{
			set { 
				toFind = value != null ? value : "";
				
				if(toFind.StartsWith("%") || toFind.EndsWith("%") ) {
					operador = " LIKE ";
				}
				
			}
		}
	}
}
