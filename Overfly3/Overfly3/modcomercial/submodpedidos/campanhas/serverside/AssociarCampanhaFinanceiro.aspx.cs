﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;


namespace Overfly3.modcomercial.submodpedidos.campanhas.serverside
{
    public partial class AssociarCampanhaFinanceiro : System.Web.UI.OverflyPage
    {
        private string Resultado;
        private string FinanceiroID;
        private string sql = "";

        public string nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value; }
        }
        private string Duplicata;

        public string nDuplicata
        {
            get { return Duplicata; }
            set { Duplicata = value; }
        }
        private string ValorFinanceiro;

        public string nValorFinanceiro
        {
            get { return ValorFinanceiro; }
            set { ValorFinanceiro = value; }
        }
        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private Integer UserID;

        public Integer nUserID
        {
            get { return UserID; }
            set { UserID = value; }
        }

        private Integer[] PedIteCampanhaID;

        public Integer[] nPedIteCampanhaID
        {
            get { return PedIteCampanhaID; }
            set { PedIteCampanhaID = value; }
        }
        private string[] Valor;

        public string[] nValor
        {
            get { return Valor; }
            set { Valor = value; }
        }
        private Integer DataLen;

        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }

        private void CampanhaGerador()
        {
            if ((FinanceiroID.ToString() == "") || (FinanceiroID.ToString() == "0"))
            {
                FinanceiroID = null;
            }

            ProcGerarFinanceiro();
        }

        private void ProcGerarFinanceiro()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[9];
            
            for (int i = 0; i < DataLen.intValue(); i++)
            {


                procParams[0] = new ProcedureParameters(
                    "@PedIteCampanhaID",
                    System.Data.SqlDbType.Int,
                    PedIteCampanhaID[i]);

                procParams[1] = new ProcedureParameters(
                    "@Valor",
                    System.Data.SqlDbType.Decimal,
                    Valor[i]);

                procParams[2] = new ProcedureParameters(
                    "@FinanceiroID",
                    System.Data.SqlDbType.Int,
                    (FinanceiroID == null ? DBNull.Value : (Object)FinanceiroID.ToString()));

                procParams[3] = new ProcedureParameters(
                    "@Duplicata",
                    System.Data.SqlDbType.VarChar,
                     (Duplicata == null ? DBNull.Value : (Object)Duplicata.ToString()));

                procParams[4] = new ProcedureParameters(
                    "@ValorFinanceiro",
                    System.Data.SqlDbType.Decimal,                    
                    (ValorFinanceiro == null ? DBNull.Value : (Object)ValorFinanceiro.ToString()));


                procParams[5] = new ProcedureParameters(
                     "@EmpresaFinanceiroID",
                     System.Data.SqlDbType.Int,
                    (EmpresaID == null ? DBNull.Value : (Object)EmpresaID.ToString()));

                procParams[6] = new ProcedureParameters(
                     "@UsuarioID",
                     System.Data.SqlDbType.Int,
                     UserID);

                procParams[7] = new ProcedureParameters(
                    "@Financeiro2ID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value,
                    ParameterDirection.Output);

                procParams[8] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.Output);

                procParams[8].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_Campanha_GeraFinanceiro",
                    procParams);

                Resultado += procParams[8].Data.ToString();
                FinanceiroID = procParams[7].Data.ToString();
   
            }

            sql = "DECLARE @Obs VARCHAR(MAX), @FinanceiroID INT " +
                    "SET @FinanceiroID = " + FinanceiroID + " " +
                    "SELECT DISTINCT @Obs = ISNULL(@Obs, '') + ' ' + ISNULL(d.Campanha, '') + ' ' + ISNULL(d.Codigo, '') + ' ' + ISNULL(d.Observacao, '') + ' ' + " +
                            "ISNULL(CONVERT(VARCHAR(10), d.dtInicio, 103), SPACE(0)) + ' ' + ISNULL(CONVERT(VARCHAR(10), d.dtFim, 103), SPACE(0)) " +
                    "FROM Financeiro_PedidosItensCampanhas a WITH(NOLOCK) " +
                            "INNER JOIN Pedidos_Itens_Campanhas b WITH(NOLOCK) ON (b.PedIteCampanhaID = a.PedIteCampanhaID) " +
                            "INNER JOIN Campanhas_Produtos c WITH(NOLOCK) ON (c.CamProdutoID = b.CamProdutoID) " +
                            "INNER JOIN Campanhas d WITH(NOLOCK) ON (d.CampanhaID = c.CampanhaID) " +
                        "WHERE (a.FinanceiroID = @FinanceiroID) " +
                    "UPDATE Financeiro SET Observacoes = ISNULL(CONVERT(VARCHAR(MAX), Observacoes), '') + @Obs " +
                    "WHERE ((FinanceiroID = @FinanceiroID) AND (ISNULL(CONVERT(VARCHAR(MAX), Observacoes), '') NOT LIKE '%' + @Obs + '%')) ";
            DataInterfaceObj.ExecuteSQLCommand(sql);
            

        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            CampanhaGerador();


            WriteResultXML(DataInterfaceObj.getRemoteData(
              "select " +
              (Resultado != null ? "'" + Resultado + "' " : "NULL") +
              " as Resultado , " +
              (FinanceiroID != null ? "'" + FinanceiroID + "' " : "NULL") +
              " as FinanceiroID  "));

        }
    }
}