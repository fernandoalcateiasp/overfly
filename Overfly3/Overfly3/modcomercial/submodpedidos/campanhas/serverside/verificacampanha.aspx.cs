﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;
namespace Overfly3.modcomercial.submodpedidos.campanhas.serverside
{
    public partial class verificacampanha : System.Web.UI.OverflyPage
    {

        private string Resultado;

        private Integer CampanhaID;

        public Integer nCampanhaID
        {
            get { return CampanhaID; }
            set { CampanhaID = value; }
        }
        private Integer CurrEstadoID;

        public Integer nCurrEstadoID
        {
            get { return CurrEstadoID; }
            set { CurrEstadoID = value; }
        }
        private Integer NewEstadoID;

        public Integer nNewEstadoID
        {
            get { return NewEstadoID; }
            set { NewEstadoID = value; }
        }
        private Integer UserID;

        public Integer nUserID
        {
            get { return UserID; }
            set { UserID = value; }
        }

        private void VerificarCampanha()
        {

            ProcedureParameters[] procParams = new ProcedureParameters[6];
            procParams[0] = new ProcedureParameters(
                "@CampanhaID",
                System.Data.SqlDbType.Int,
                CampanhaID);

            procParams[1] = new ProcedureParameters(
                "@EstadoDeID",
                System.Data.SqlDbType.Int,
                CurrEstadoID);

            procParams[2] = new ProcedureParameters(
                "@EstadoParaID",
                System.Data.SqlDbType.Int,
                NewEstadoID);

            procParams[3] = new ProcedureParameters(
                "@Data",
                System.Data.SqlDbType.DateTime,
                 DBNull.Value);

            procParams[4] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                UserID);

            procParams[5] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.Output);
            procParams[5].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Campanha_Verifica",
                procParams);

            Resultado += procParams[5].Data.ToString();
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            VerificarCampanha();
            WriteResultXML(DataInterfaceObj.getRemoteData(
             "select " +
             (Resultado != null ? "'" + Resultado + "' " : "NULL") +
             " as Resultado "));
        }
    }
}