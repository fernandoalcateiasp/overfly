/********************************************************************
modalprodutos.js

Library javascript para o modalprodutos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoPesq = new CDatatransport('dsoPesq');
// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    var elem = document.getElementById('modalprodutosBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtProduto').disabled == false )
        txtProduto.focus();

    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Produtos');

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Produto', 1);

    // ajusta elementos da janela
    var elem;
    var temp;
    
    // ajusta o divProdutos
    elem = window.document.getElementById('divProdutos');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        temp = parseInt(width);
        height = 40;
        
    }
    
    // txtProduto
    elem = window.document.getElementById('txtProduto');
    elem.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (elem.style)
    {
        left = 0;
        top = 16;
        width = (elem.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }
    
    txtProduto.onkeypress = txtProduto_onKeyPress;
    
    // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');
    elem.disabled = true;

    with (elem.style)
    {
        top = parseInt(document.getElementById('txtProduto').style.top);
        left = parseInt(document.getElementById('txtProduto').style.left) + parseInt(document.getElementById('txtProduto').style.width) + 2;
        width = 80;
        height = 24;
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divProdutos').style.top) + parseInt(document.getElementById('divProdutos').style.height) + ELEM_GAP;
        width = temp + 221;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);
    
    headerGrid(fg,['Produto',
                   'ID',
                   'Modelo',
                   'PartNumber',
                   'Descri��o'], [2]);

    fg.Redraw = 2;
}

function txtProduto_onKeyPress()
{
    if ( event.keyCode == 13 )
        btnFindPesquisa_onclick();
}

function txtProduto_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    txtProduto.value = trimStr(txtProduto.value);
    
    changeBtnState(txtProduto.value);

    if (btnFindPesquisa.disabled)
        return;
    
    startPesq(txtProduto.value);
}

function fg_DblClick()
{
    // Se tem Produto selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , new Array(
                      fg.TextMatrix(fg.Row, 1), 
					  fg.TextMatrix(fg.Row, 0), 
					  fg.TextMatrix(fg.Row, 2)));
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

function startPesq(strProduto)
{
    lockControlsInModalWin(true);
    
    writeInStatusBar('child', 'Listando', 'cellMode' , true);
        
    var strPas = '?';
    strPas += 'strToFind='+escape(strProduto);
    strPas += '&nCampanhaID='+escape(glb_nCampanhaID);
    strPas += '&nFornecedorID='+escape(glb_nFornecedorID);
    
    dsoPesq.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/campanhas/serverside/pesqproduto.aspx' + strPas;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC()
{
    startGridInterface(fg);

    headerGrid(fg,['Produto',
                   'ID',
                   'Modelo',
                   'PartNumber',
                   'Descri��o'], []);
    
    fillGridMask(fg,dsoPesq,['fldName',
                             'fldID',
                             'Modelo',
							 'PartNumber',
                             'Descricao'],['','','','','']);
                           
    alignColsInGrid(fg,[1]);

    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
    }    
    else
        btnOK.disabled = true;    
        
    writeInStatusBar('child', 'cellMode', 'Produtos');    
}
