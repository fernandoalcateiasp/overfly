/********************************************************************
modalcampanhaspendentes.js

Library javascript para o modalcampanhaspendentes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_CampanhasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_nCampanhaID = 0;

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_excel.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_excel_dis.gif';

var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');
var dsoCmbFornecedor = new CDatatransport('dsoCmbFornecedor');
var dsoCmbCliente = new CDatatransport('dsoCmbCliente');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
paintCellsSpecialyReadOnly()

js_fg_modalcampanhaspendentesBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalcampanhaspendentesDblClick(grid, Row, Col)
js_modalcampanhaspendentesKeyPress(KeyAscii)
js_modalcampanhaspendentes_ValidateEdit()
js_modalcampanhaspendentes_BeforeEdit(grid, row, col)
js_modalcampanhaspendentes_AfterEdit(Row, Col)
js_fg_AfterRowColmodalcampanhaspendentes (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalcampanhaspendentesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	glb_bFirstFill = true;
    // mostra a modal

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
	fillFornecedor();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Campanhas Pendentes', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK
    btnOK.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblPendentes','chkPendentes',3,1,-10,-10],
					      ['lblTipoCampanhaID','selTipoCampanhaID',9,1,-3],
					      ['lblFornecedorID','selFornecedorID',18,1,-1],
					      ['lblClienteID','selClienteID',18,1,-1],
						  ['lblProduto','txtProduto',15,1,-1],
						  ['lbldtFim','txtdtFim',10,1,-1],
						  ['btnExcel', 'btn', 21, 1, 100]], null, null, true);

	chkPendentes.checked = true;
	chkPendentes.onclick = fillFornecedor;
	selTipoCampanhaID.onchange = selCmbs_onchange;
	selFornecedorID.onchange = selCmbs_onchange;
	selClienteID.onchange = selCmbs_onchange;
	
	txtProduto.onfocus = selFieldContent;
	txtProduto.maxLength = 20;
	txtProduto.onkeypress = txtProdutoFiltro_onKeyPress;

	txtdtFim.onfocus = selFieldContent;
	txtdtFim.maxLength = 10;
	txtdtFim.onkeypress = txtProdutoFiltro_onKeyPress;

    btnExcel.src = glb_LUPA_IMAGES[0].src;

	btnExcel.style.height = 21;

	adjustLabelsCombos();

    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
			  btnFillGrid.offsetTop + btnFillGrid.offsetHeight + 2;
        width = txtdtFim.offsetLeft + txtdtFim.offsetWidth + ELEM_GAP;    
        height = txtdtFim.offsetTop + txtdtFim.offsetHeight;    
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + txtdtFim.offsetTop;
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		style.visibility = 'hidden';
		value = 'Baixar';
		title = 'Baixar Financeiros';
    }
    
    with (btnFillGrid)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		style.top = btnOK.currentStyle.top;
		style.left = parseInt(txtdtFim.currentStyle.left, 10) + parseInt(txtdtFim.currentStyle.width, 10) + (ELEM_GAP * 2);
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtProdutoFiltro_onKeyPress()
{
    if ( event.keyCode == 13 )
		listar();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnFillGrid')
    {
		listar();
    }
    else if (controlID == 'btnExcel')
    {
		//sendToExcel();
	}
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, glb_nCampanhaID ); 
    }    
}

/********************************************************************
Usuario cliclou o botao esquerdo do mouse
********************************************************************/
function btn_onmousedownExcel()
{
	if (event.button == 1)
		sendToExcel();
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    lockControlsInModalWin(true); //marco
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
	;
}
/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function buildStrSQL(bToExcel)
{
	var strSQL = '';

	var sProduto = trimStr(txtProduto.value);
	var sdtFim = trimStr(txtdtFim.value);
	var sFiltro = '';

	if (sdtFim != '')
		sdtFim = normalizeDate_DateTime(sdtFim, 1);
	
	var geraExcelID = 1;
	var formato = 2; //Excel
	var sLinguaLogada = getDicCurrLang();

	var strParameters = "RelatorioID=" + geraExcelID + "&Formato=" + formato + "&bToExcel=" + (bToExcel ? true : false) + "&sLinguaLogada=" + sLinguaLogada + "&glb_sEmpresaFantasia=" + glb_aEmpresaData[3] +
                        "&chkPendentes=" + (chkPendentes.checked ? true : false) + "&selTipoCampanhaID=" + selTipoCampanhaID.value + "&selFornecedorID=" + selFornecedorID.value + "&selClienteID=" + selClienteID.value +
                        "&sProduto=" + sProduto + "&sdtFim=" + sdtFim;

	return SYS_PAGESURLROOT + '/modcomercial/submodpedidos/campanhas/serverside/ReportsGrid_campanhaspendentes.aspx?' + strParameters;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_CampanhasTimerInt != null)
    {
        window.clearInterval(glb_CampanhasTimerInt);
        glb_CampanhasTimerInt = null;
    }
    
	var aGrid = null;
	var i = 0;
	var sProduto = trimStr(txtProduto.value);
	var sdtFim = trimStr(txtdtFim.value);
	var sFiltro = '';
	var nValor = 0;

	lockControlsInModalWin(true); //marco
	
	glb_nDSOs = 1;
    
    // zera o grid
    fg.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	dsoGrid.URL = buildStrSQL(false);

	dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
    try
    {
		dsoGrid.Refresh();
	}	
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Filtro inv�lido.') == 0 )
            return null;

		lockControlsInModalWin(false);        
        window.focus();
		txtdtFim.focus();		
	}
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

	if (glb_bFirstFill)
	{
		glb_bFirstFill = false;
	}

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

    headerGrid(fg,['Tipo',
					'Fornecedor',
					'Cliente',
					'ID',
					'Produto',
					'Modelo',
					'Quant',
					'$',
					'Valor',
					'Custo',
					'Diferen�a',
					'Vendido',
					'Saldo',
					'Campanha',
					'C�digo',
					'In�cio',
					'Fim',
					'CampanhaID'], [17]);

    fillGridMask(fg,dsoGrid,['TipoCampanha',
							 'Fornecedor',
							 'Cliente',
							 'ID',
							 'Produto',
							 'Modelo',
							 'Quant',
							 'Valor',
							 'ValorUnit',
							 'Custo',
							 'Diferenca',
							 'Vendido',
							 'Saldo',
							 'Campanha',
							 'Codigo',
						     'Inicio',
						     'Fim',
						     'CampanhaID'],
							 ['','','','9999999999','','','9999999999','','999.999.999,99','999.999.999,99',
							 '999.999.999,99','9999999999','9999999999','','',
						     '99/99/9999','99/99/9999',''],
						     ['','','','##########','','','##########','','###,###,##0.00','###,###,##0.00',
							 '###,###,##0.00','##########','##########','','',
						     dTFormat,dTFormat,'']);
    fg.Redraw = 0;

    alignColsInGrid(fg,[3,6,8,9,10,11,12]);

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[3,'############','C'], 
                                                          [6,'############','S'],
                                                          [11,'############','S'],
                                                          [12,'############','S']]);

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
	paintCellsSpecialyReadOnly();
	    
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    fg.Redraw = 2;
    fg.FrozenCols = 5;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;

	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;

    lockControlsInModalWin(true);
    
    for (i=2; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'OK', i) != 0)
		{
			nBytesAcum=strPars.length;
			
			if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
			{
				nBytesAcum = 0;
				if (strPars != '')
				{
					glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
					strPars = '';
					nDataLen = 0;
				}
				
				strPars += '?nUsuarioID=' + escape(glb_nUserID);
				strPars += '&bDesconto=1';
			}
		
			nDataLen++;
			strPars += '&nFinanceiroID=' + escape(getCellValueByColKey(fg, 'FinanceiroID*', i));
		}	
	}
	
	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
		
	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGrava.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/financeiro/serverside/baixarfinanceiros.asp' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_CampanhasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_CampanhasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
	}
}

function sendDataToServer_DSC()
{
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

function adjustLabelsCombos()
{
    setLabelOfControl(lblTipoCampanhaID, selTipoCampanhaID.value);
    setLabelOfControl(lblFornecedorID, selFornecedorID.value);
    setLabelOfControl(lblClienteID, selClienteID.value);
}
function listar()
{
	lockControlsInModalWin(true);
	
	if (!verificaData())
		return null;
		
	refreshParamsAndDataAndShowModalWin(false);
}

function verificaData()
{
	var sData2 = trimStr(txtdtFim.value);
	var bDataIsValid2 = true;

	if (sData2 != '')
		bDataIsValid2 = chkDataEx(txtdtFim.value);

	if (bDataIsValid2 != true)
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
    
		lockControlsInModalWin(false);    
        window.focus();
		txtdtFim.focus();
		
		return false;
	}

	return true;
}

function selCmbs_onchange()
{
	fg.Rows = 1;
	
	if (this == selTipoCampanhaID)
		fillFornecedor();
	else if (this == selFornecedorID)
		fillCliente();
	else
		adjustLabelsCombos();		
}

function fillFornecedor()
{
	if (glb_CampanhasTimerInt != null)
    {
        window.clearInterval(glb_CampanhasTimerInt);
        glb_CampanhasTimerInt = null;
    }

	clearComboEx([selFornecedorID.id]);
	clearComboEx([selClienteID.id]);

	var sFiltro = '';

	lockControlsInModalWin(true);

	if (chkPendentes.checked)
		sFiltro += ' AND a.EstadoID = 41 AND ISNULL(a.dtFim, dbo.fn_Data_Zero(GETDATE())) >= dbo.fn_Data_Zero(GETDATE()) ';
	else
		sFiltro += ' AND a.EstadoID IN (41,48) ';

	if (selTipoCampanhaID.value > 0)
		sFiltro += ' AND a.TipoCampanhaID = ' + selTipoCampanhaID.value + ' ';
	
    setConnection(dsoCmbFornecedor);

    dsoCmbFornecedor.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
		'UNION ALL ' +
		'SELECT DISTINCT b.PessoaID AS fldID, b.Fantasia AS fldName ' +
		'FROM Campanhas a, Pessoas b ' +
		'WHERE (a.FornecedorID = b.PessoaID ' + sFiltro + ') ' +
		'ORDER BY fldName ';

    dsoCmbFornecedor.ondatasetcomplete = dsoCmbFornecedor_DSC;
    dsoCmbFornecedor.Refresh();
}

function dsoCmbFornecedor_DSC()
{
    var optionStr, optionValue;

    while (!dsoCmbFornecedor.recordset.EOF)
    {
        optionStr = dsoCmbFornecedor.recordset['fldName'].value;
	    optionValue = dsoCmbFornecedor.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selFornecedorID.add(oOption);
        dsoCmbFornecedor.recordset.MoveNext();
    }

	lockControlsInModalWin(false);
	
	selFornecedorID.disabled = (selFornecedorID.options.length == 0);
	selClienteID.disabled = (selClienteID.options.length == 0);

	glb_CampanhasTimerInt = window.setInterval('fillCliente()', 10, 'JavaScript');
}

function fillCliente()
{
	if (glb_CampanhasTimerInt != null)
    {
        window.clearInterval(glb_CampanhasTimerInt);
        glb_CampanhasTimerInt = null;
    }

	clearComboEx([selClienteID.id]);

	var sFiltro = '';

	lockControlsInModalWin(true);

	if (chkPendentes.checked)
		sFiltro += ' AND a.EstadoID = 41 AND ISNULL(a.dtFim, dbo.fn_Data_Zero(GETDATE())) >= dbo.fn_Data_Zero(GETDATE()) ';
	else
		sFiltro += ' AND a.EstadoID IN (41,48) ';
		
	if (selTipoCampanhaID.value > 0)
		sFiltro += ' AND a.TipoCampanhaID = ' + selTipoCampanhaID.value + ' ';

	if (selFornecedorID.value > 0)		
		sFiltro += ' AND a.FornecedorID = ' + selFornecedorID.value + ' ';

    setConnection(dsoCmbCliente);

    dsoCmbCliente.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
		'UNION ALL ' +
		'SELECT DISTINCT b.PessoaID AS fldID, b.Fantasia AS fldName ' +
		'FROM Campanhas a, Pessoas b ' +
		'WHERE (a.ClienteID = b.PessoaID ' + sFiltro + ') ' +
		'ORDER BY fldName ';

    dsoCmbCliente.ondatasetcomplete = dsoCmbCliente_DSC;
    dsoCmbCliente.Refresh();
}

function dsoCmbCliente_DSC()
{
    var optionStr, optionValue;

    while (!dsoCmbCliente.recordset.EOF)
    {
        optionStr = dsoCmbCliente.recordset['fldName'].value;
        optionValue = dsoCmbCliente.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selClienteID.add(oOption);
        dsoCmbCliente.recordset.MoveNext();
    }

	lockControlsInModalWin(false);
	
	selClienteID.disabled = (selClienteID.options.length == 0);

	if (glb_bFirstFill)
	{
		glb_bFirstFill = false;
		showExtFrame(window, true);
		window.focus();

		if ( !selClienteID.disabled )
			selClienteID.focus();
	}

	adjustLabelsCombos();
	// ajusta estado dos botoes
	setupBtnsFromGridState();
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

function sendToExcel()
{
	if (btnExcel.src == glb_LUPA_IMAGES[1].src)
		return null;
		
	lockControlsInModalWin(true);

	var frameReport = document.getElementById("frmReport");
	frameReport.onreadystatechange = reports_onreadystatechange;

	frameReport.contentWindow.location = buildStrSQL(true);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalcampanhaspendentesBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalcampanhaspendentesDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;

    glb_PassedOneInDblClick = true;
    
	glb_nCampanhaID = getCellValueByColKey(fg, 'CampanhaID', fg.Row);

	btn_onclick(btnCanc);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcampanhaspendentesKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcampanhaspendentes_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcampanhaspendentes_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcampanhaspendentes_AfterEdit(Row, Col)
{
	;
}

function gridTotais(sColKeyTotal)
{
	var retVal = 0;
	var i = 0;

	if (fg.Rows < 2)
		return 0;
		
	for (i=2; i<fg.Rows; i++)
	{
		if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0)
		{
			if (sColKeyTotal == 'OK')
				retVal++;
			else	
				retVal += fg.ValueMatrix(i, getColIndexByColKey(fg, sColKeyTotal));
		}	
	}
	
	return retVal;
}


/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalcampanhaspendentes(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return true;

	setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************

function reports_onreadystatechange() {
    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
    (frameReport.contentWindow.document.readyState == 'interactive') ||
    (frameReport.contentWindow.document.readyState == 'complete')) {
        lockControlsInModalWin(false);
    }
}
