/********************************************************************
modalassociarfinanceiro.js

Library javascript para o modalassociarfinanceiro.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nDSOCounter = 0;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;

var glbGridOk = false;
var glb_Inicio = true;
var glb_nEmpresaID;
var glb_nContador = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************


var dsoGridCampanhas = new CDatatransport('dsoGridCampanhas');
var dsoGridFinanceiros = new CDatatransport('dsoGridFinanceiros');
var dsoMatriz = new CDatatransport('dsoMatriz');
var dsoAssociarCampanhaFinanceiro = new CDatatransport('dsoAssociarCampanhaFinanceiro');
var dsoFornecedores = new CDatatransport('dsoFornecedores');
var dsoCmbGrid01 = new CDatatransport('dsoCmbGrid01');
var dsoTipoCampanha = new CDatatransport('dsoTipoCampanha');
// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 49;
    modalFrame.style.left = 0;

    with (ModalAssociarFinanceiroBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    dsoCombosGrid();
    comboTipoCampanha();
}

/********************************************************************
Clique botao OK ou Cancela ou botoes 
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        ;
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_PL', null);

    else if (ctl.id == btnAssociar.id) 
    {
        AssociarCampanhaFinanceiro();
    }

    else if (ctl.id == btnBuscar.id) 
    {
        getDataInServerCampanhas();
        getDataInServerFinanceiros();
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Associar Financeiros');
    
    // texto da secao01
    secText('Associar Financeiros', 1);
    
    loadDataAndTreatInterface();
    // btnOK nao e usado nesta modal

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    with (btnOK)
    {
        disabled = true;
        style.visibility = 'hidden';
    }

    // btnCanc nao e usado nesta modal
    with (btnCanc)
    {
        style.visibility = 'hidden';
    }
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;
        
    // Ajusta botoes OK/Cancel para esta janela
    with (btnOK.style)
    {
        disabled = true;
        left = (widthFree - 2 * parseInt(width, 10) - ELEM_GAP) / 2;
    }
    with (btnCanc.style)
    {
        left = (parseInt(btnOK.currentStyle.left,10) +  parseInt(btnOK.currentStyle.width,10) + ELEM_GAP);
    }    
    
/*
    btnAssociar.style.top = topFree + 10 ;
    btnAssociar.style.left = 900 + ELEM_GAP;
    btnAssociar.style.width = FONT_WIDTH * 7 + 20;
*/
    lblCampanhas.style.top = topFree + 70;
    lblCampanhas.style.left = ELEM_GAP;
    lblCampanhas.style.width = FONT_WIDTH * 7 + 20;

    lblFinanceiros.style.top = topFree + 70;
    lblFinanceiros.style.left = 620 + ELEM_GAP;
    lblFinanceiros.style.width = FONT_WIDTH * 7 + 20;

    lblTipoCampanhaID.style.top = topFree + 10;
    lblTipoCampanhaID.style.left = ELEM_GAP;
    lblTipoCampanhaID.style.width = FONT_WIDTH * 7 + 20;

    selTipoCampanhaID.style.top = topFree + 25;
    selTipoCampanhaID.style.left = ELEM_GAP;
    selTipoCampanhaID.style.width = FONT_WIDTH * 7 + 20;

    lblFornecedor.style.top = topFree + 10;
    lblFornecedor.style.left = ELEM_GAP + parseInt(lblTipoCampanhaID.style.left, 10) + 75;
    lblFornecedor.style.width = FONT_WIDTH * 7 + 20;

    selFornecedor.style.top = topFree + 25;
    selFornecedor.style.left = ELEM_GAP + parseInt(selTipoCampanhaID.style.left, 10) + 75;
    selFornecedor.style.width = FONT_WIDTH * 7 + 80;

    lblEmpresa.style.top = topFree + 10;
    lblEmpresa.style.left = ELEM_GAP + parseInt(lblFornecedor.style.left, 10) + 135;
    lblEmpresa.style.width = FONT_WIDTH * 7 + 20;

    selEmpresa.style.top = topFree + 25;
    selEmpresa.style.left = ELEM_GAP + parseInt(selFornecedor.style.left, 10) + 135;
    selEmpresa.style.width = FONT_WIDTH * 7 + 80;

    lblCampanhaID.style.top = topFree + 10;
    lblCampanhaID.style.left = parseInt(selEmpresa.style.left, 10) + 135 + ELEM_GAP;
    lblCampanhaID.style.width = FONT_WIDTH * 7 + 20;

    txtCampanhaID.style.top = topFree + 25;
    txtCampanhaID.style.left = parseInt(selEmpresa.style.left, 10) + 133 + ELEM_GAP;
    txtCampanhaID.style.width = FONT_WIDTH * 7 + 25;

    lblFiltro.style.top = topFree + 10;
    lblFiltro.style.left = parseInt(lblCampanhaID.style.left, 10) + 80 + ELEM_GAP;
    lblFiltro.style.width = FONT_WIDTH * 7 + 20;

    txtFiltro.style.top = topFree + 25;
    txtFiltro.style.left = parseInt(txtCampanhaID.style.left, 10) + 78 + ELEM_GAP;
    txtFiltro.style.width = FONT_WIDTH * 7 + 80;

    lblCampanhaInterna.style.top = topFree + 10;
    lblCampanhaInterna.style.left = parseInt(txtFiltro.style.left, 10) + 133 + ELEM_GAP;
    lblCampanhaInterna.style.width = FONT_WIDTH * 7 + 20;

    chkCampanhaInterna.style.top = topFree + 24;
    chkCampanhaInterna.style.left = parseInt(txtFiltro.style.left, 10) + 129 + ELEM_GAP;
    chkCampanhaInterna.style.width = 23;
    chkCampanhaInterna.style.height = 23;

    lbldtInicio.style.top = topFree + 10;
    lbldtInicio.style.left = parseInt(lblCampanhaInterna.style.left, 10) + 18 + ELEM_GAP;
    lbldtInicio.style.width = FONT_WIDTH * 7 + 25;

    txtdtInicio.style.top = topFree + 25;
    txtdtInicio.style.left = parseInt(chkCampanhaInterna.style.left, 10) + 18 + ELEM_GAP;
    txtdtInicio.style.width = FONT_WIDTH * 7 + 25;

    lbldtFim.style.top = topFree + 10;
    lbldtFim.style.left = parseInt(lbldtInicio.style.left, 10) + 78 + ELEM_GAP;
    lbldtFim.style.width = FONT_WIDTH * 7 + 25;

    txtdtFim.style.top = topFree + 25;
    txtdtFim.style.left = parseInt(txtdtInicio.style.left, 10) + 78 + ELEM_GAP;
    txtdtFim.style.width = FONT_WIDTH * 7 + 25;

    btnBuscar.style.top = topFree + 25;
    btnBuscar.style.left = 802  + ELEM_GAP;
    btnBuscar.style.width = FONT_WIDTH * 7;

    btnAssociar.style.top = topFree + 25;
    btnAssociar.style.left = 900 + ELEM_GAP;
    btnAssociar.style.width = FONT_WIDTH * 7 + 10;

    
    // ajusta o divFG
    elem = window.document.getElementById('divFGCampanhas');

    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(btnAssociar.currentStyle.top, 10) + ELEM_GAP + 55;
        width = 595 + ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) -
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10) + 55;
    }
        
    elem = document.getElementById('fgCampanhas');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFGCampanhas').currentStyle.width);
        height = parseInt(document.getElementById('divFGCampanhas').currentStyle.height);
    }

    startGridInterface(fgCampanhas);
    fgCampanhas.Cols = 1;
    fgCampanhas.ColWidth(0) = parseInt(divFGCampanhas.currentStyle.width, 10) * 18;

    elemFinanceiros = window.document.getElementById('divFGFinanceiro');
    with (elemFinanceiros.style) 
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = parseInt(btnAssociar.currentStyle.left, 10) + ELEM_GAP - 295;
        top = parseInt(btnAssociar.currentStyle.top, 10) + ELEM_GAP + 55;
        width = 350 + ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) -
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10) + 55;
    }

    elemFinanceiros = document.getElementById('fgFinanceiro');
    with (elemFinanceiros.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFGFinanceiro').currentStyle.width);
        height = parseInt(document.getElementById('divFGFinanceiro').currentStyle.height);
    }

    startGridInterface(fgFinanceiro);
    fgFinanceiro.Cols = 1;
    fgFinanceiro.ColWidth(0) = parseInt(divFGFinanceiro.currentStyle.width, 10) * 18;

    preencheFornecedor();
}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServerCampanhas()
{
    var aEmpresa = getCurrEmpresaData();
    var nFornecedorID = selFornecedor.value;
    var nTipoCampanhaID = selTipoCampanhaID.value;
    var nCampanhaID = txtCampanhaID.value;
    var sFiltros = txtFiltro.value;
    var nEmpresaID = selEmpresa.value;
    var sFornecedorWhere = '';
    var sInternoWhere = '';
    var sCampanhaWhere = '';
    var sTipoCampanhaWhere = '';
    var sEmpresaWhere = '';
    var dtInicio = '';
    var dtFim = '';

    if ((!verificaData(txtdtInicio.value)) || (txtdtInicio.value == ''))
    {
        window.top.overflyGen.Alert('Data inicio invalida.');
        fgCampanhas.Rows = 1;
        lockControlsInModalWin(false);
        return null;
    }

    if ((!verificaData(txtdtFim.value)) || (txtdtFim.value == ''))
    {
        window.top.overflyGen.Alert('Data fim invalida.');
        fgCampanhas.Rows = 1;
        lockControlsInModalWin(false);
        return null;
    }

    if (verificaDatas(txtdtInicio.value, txtdtFim.value))
    {
        window.top.overflyGen.Alert('O limite para pesquisa � de 90 dias.');
        fgCampanhas.Rows = 1;
        lockControlsInModalWin(false);
        return null;
    }

    dtInicio = dateFormatToSearch(txtdtInicio.value);
    dtFim = dateFormatToSearch(txtdtFim.value);

    if (dtFim != '')
        dtFim = dtFim + ' 23:59:59';

    // DPAs
    if (nTipoCampanhaID > 0)
        sTipoCampanhaWhere = 'AND (b.TipoCampanhaID = ' + nTipoCampanhaID + ') ';

    if (nCampanhaID > 0)
        sCampanhaWhere = 'AND (a.CampanhaID = ' + nCampanhaID + ') ';
    
    if (nFornecedorID > 0)
        sFornecedorWhere = 'AND (b.FornecedorID = ' + nFornecedorID + ') ';
    else
        sFornecedorWhere = 'AND (b.FornecedorID IS NULL) ';

    if (nEmpresaID > 0)
        sEmpresaWhere = 'AND (a.EmpresaID = ' + nEmpresaID + ') ';

    if (chkCampanhaInterna.checked)
        sInternoWhere = 'AND (ISNULL(b.CampanhaInterna, 0) = 1) ';
    else
        sInternoWhere = 'AND (ISNULL(b.CampanhaInterna, 0) = 0) ';

    if (sFiltros != '')
        sFiltros = 'AND ' + sFiltros + ' ';

    var sSQL = 'SELECT DISTINCT a.CampanhaID, a.PedidoID, 0 AS OK, b.Campanha, d.Fantasia AS Empresa, b.Codigo, (SELECT TOP 1 aa.dtMovimento ' +
																						  'FROM Pedidos_Itens aa WITH(NOLOCK) ' +
																						  'WHERE (aa.PedidoID = a.PedidoID) AND (aa.dtMovimento IS NOT NULL)) AS dtPedido, ' +
			                'a.Saldo, 0 AS ValorAssociar, a.PedIteCampanhaID, e.ItemMasculino AS TipoCampanha ' +
	                'FROM vw_Campanha_PedidoItensSaldos a ' +
		                'INNER JOIN Campanhas b WITH(NOLOCK) ON (b.CampanhaID = a.CampanhaID) ' +
		                'INNER JOIN Pessoas_Enderecos c WITH(NOLOCK) ON (c.PessoaID = a.EmpresaID) ' +
		                'INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = c.PessoaID) ' +
                        'INNER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (e.ItemID = b.TipoCampanhaID) ' +
	                'WHERE (c.PaisID = ' + aEmpresa[1] + ') ' +
                            'AND ((SELECT TOP 1 aa.dtMovimento ' +
                                        'FROM Pedidos_Itens aa WITH(NOLOCK)  ' +
                                        'WHERE (aa.PedidoID = a.PedidoID) AND (aa.dtMovimento IS NOT NULL)) BETWEEN \'' + dtInicio + '\' AND \'' + dtFim + '\') ' +
                        sFornecedorWhere + sInternoWhere + sCampanhaWhere + sEmpresaWhere + sTipoCampanhaWhere + sFiltros +
	                'ORDER BY d.Fantasia, dtPedido, a.CampanhaID ';

    setConnection(dsoGridCampanhas);
    dsoGridCampanhas.SQL = sSQL;
    dsoGridCampanhas.ondatasetcomplete = eval(fillModalGrid_DSC);
    dsoGridCampanhas.refresh();
}
/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServerFinanceiros()
{
    var nFornecedorID = selFornecedor.value;
    var aEmpresa = getCurrEmpresaData();
    var sHistoricoPadraoWhere = '';

    if (nFornecedorID = 0)
        nFornecedorID = aEmpresa[0];

    if (chkCampanhaInterna.checked)
        sHistoricoPadraoWhere = 'AND (a.HistoricoPadraoID = 4) ';
    else
        sHistoricoPadraoWhere = 'AND (a.HistoricoPadraoID = 3604) ';
    
    var sSQL = 'SELECT NULL AS FinanceiroID, 0 AS OK, NULL AS EmpresaID, NULL AS Duplicata, NULL AS Valor UNION ALL ' +
                'SELECT a.FinanceiroID AS FinanceiroID, 0 AS OK, a.EmpresaID, a.Duplicata AS Duplicata, ' +
	                    '(a.Valor - ((SUM(ISNULL(b.Valor, 0)) + ' +
		                    'ISNULL((SELECT TOP 1 SUM(dd.Valor * (CASE WHEN aa.TipoFinanceiroID = dd.TipoFinanceiroID THEN -1 ELSE 1 END)) ' +
			                    'FROM Financeiro aa WITH(NOLOCK) ' +
				                    'INNER JOIN Financeiro_Ocorrencias bb WITH(NOLOCK) ON (aa.FinanceiroID = bb.FinanceiroID) ' +
				                    'INNER JOIN Financeiro_Ocorrencias cc WITH(NOLOCK) ON (cc.FinOcorrenciaReferenciaID = bb.FinOcorrenciaID) ' +
				                    'INNER JOIN Financeiro dd WITH(NOLOCK) ON (dd.FinanceiroID = cc.FinanceiroID) ' +
			                    'WHERE (aa.FinanceiroID = a.FinanceiroID) AND (bb.TipoOcorrenciaID = 1051) AND (cc.TipoOcorrenciaID = 1051)), 0)))) AS Valor ' +
                    'FROM Financeiro a WITH(NOLOCK) ' +
	                    'LEFT OUTER JOIN Financeiro_PedidosItensCampanhas b WITH(NOLOCK) ON (b.FinanceiroID = a.FinanceiroID) ' +
                    'WHERE (a.EstadoID IN (41, 48)) AND (a.PedidoID IS NULL) AND (a.PessoaID = ' + nFornecedorID + ') ' + sHistoricoPadraoWhere +
                    'GROUP BY a.FinanceiroID, a.EmpresaID, a.Duplicata, a.Valor';

    setConnection(dsoGridFinanceiros);
    dsoGridFinanceiros.SQL = sSQL;
    dsoGridFinanceiros.ondatasetcomplete = eval(fillModalGridFinanceiros_DSC);
    dsoGridFinanceiros.refresh();
}
/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGrid_DSC()
{
    glb_GridIsBuilding = true;

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    if (glb_Inicio)
        startGridInterface(fgCampanhas);
        
    aHiddenColls = [0, 11];

    headerGrid(fgCampanhas, ['CampanhaID',
                           'OK',
                           'ID',
                           'Tipo Campanha',
                           'Campanha',
                           'Empresa',
                           'Codigo',
                           'Pedido',
                           'Data Pedido',
                           'Saldo',
                           'Valor Associar',
                           'PedIteCampanhaID'], aHiddenColls);

    fillGridMask(fgCampanhas, dsoGridCampanhas, ['CampanhaID',
                                          'OK',
                                          'CampanhaID*',
                                          'TipoCampanha*',
							              'Campanha*',
							              'Empresa*',
							              'Codigo*',
							              'PedidoID*',
                                          'dtPedido*',
                                          'Saldo*',
                                          'ValorAssociar',
                                          'PedIteCampanhaID'],
                                          ['', '', '', '', '', '', '', '', '99/99/9999', '9999999999.99', '9999999999.99', ''],
                                          ['', '', '', '', '', '', '', '', dTFormat, '###,###,##0.00', '###,###,##0.00', '']);

    gridHasTotalLine(fgCampanhas, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fgCampanhas, 'PedidoID*'), '###', 'C'],
															  [getColIndexByColKey(fgCampanhas, 'ValorAssociar'), '###,###,###,###.00', 'S']]);

    with (fgCampanhas)
    {
        ColDataType(1) = 11;
    }

    fgCampanhas.Editable = true;
    fgCampanhas.ExplorerBar = 5;
    fgCampanhas.FontSize = '8';
    fgCampanhas.Redraw = 0;
    fgCampanhas.AutoSizeMode = 0;
    fgCampanhas.AutoSize(0, fgCampanhas.Cols - 1, false, 0);
    fgCampanhas.Redraw = 2;

    calculaTotais();

    glbGridOk = false;

    glb_GridIsBuilding = false;

    glb_nContador++;

    habilitaInterface();
} 
/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGridFinanceiros_DSC()
{
    glb_GridIsBuilding = true;
    
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';
    
    if (glb_Inicio)
        startGridInterface(fgFinanceiro);
        
    aHiddenColls = [0];

    headerGrid(fgFinanceiro, ['FinanceiroID',
                          'OK',
                          'ID',
                          'Empresa',
				          'Duplicata',
                          'Valor'], aHiddenColls);

    fillGridMask(fgFinanceiro, dsoGridFinanceiros, ['FinanceiroID',
                                          'OK',
                                          'FinanceiroID*',
                                          'EmpresaID*',
							              'Duplicata*',
                                          'Valor*'],
                                          ['', '', '', '', '', '9999999999.99'],
                                          ['', '', '', '', '', '###,###,##0.00']);

    //alignColsInGrid(fg,[0,1,5,6,7,8,9]);

    insertcomboData(fgFinanceiro, getColIndexByColKey(fgFinanceiro, 'EmpresaID*'), dsoCmbGrid01, 'fldName', 'fldID');

    fgFinanceiro.Editable = true;
    fgFinanceiro.FontSize = '8';
    fgFinanceiro.Redraw = 0;
    fgFinanceiro.AutoSizeMode = 0;
    fgFinanceiro.AutoSize(0, fgFinanceiro.Cols - 1, false, 0);
    fgFinanceiro.Redraw = 2;

    with (fgFinanceiro)
    {
        ColDataType(1) = 11;
    }

    if (fgFinanceiro.Rows > 1) 
    {
        fgFinanceiro.Cell(6, 1, getColIndexByColKey(fgFinanceiro, 'Duplicata*'), 1, getColIndexByColKey(fgFinanceiro, 'Duplicata*')) = 0xFFFFFF;
        fgFinanceiro.Cell(6, 1, getColIndexByColKey(fgFinanceiro, 'EmpresaID*'), 1, getColIndexByColKey(fgFinanceiro, 'EmpresaID*')) = 0xFFFFFF;
    }

    glbGridOk = false;

    glb_GridIsBuilding = false;

    glb_Inicio = false;

    glb_nContador++;

    habilitaInterface();
}

function js_ModalAssociarFinanceiro_AfterEdit(fg, Row, Col) 
{
    var nValorAssociar;
    var nValorSaldo;
    var bOk = false;
    
    if ((glb_GridIsBuilding) || (Row == 0))
        return null;

    if ((fg.Editable)) 
    {
        if (Col == getColIndexByColKey(fg, 'OK'))
            bOk = true;

        if (bOk)
        {
            fg.Row = Row;

            if (fg.id == 'fgFinanceiro') 
            {
                if ((fg.textMatrix(Row, getColIndexByColKey(fg, 'OK')) != 0) && (!glbGridOk))
                {
                    glbGridOk = true;
                    fg.textMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;
                }
                else if ((fg.textMatrix(Row, getColIndexByColKey(fg, 'OK')) != 0) && (glbGridOk))
                    fg.textMatrix(Row, getColIndexByColKey(fg, 'OK')) = 0;
                else if ((fg.textMatrix(Row, getColIndexByColKey(fg, 'OK')) == 0) && (glbGridOk))
                    glbGridOk = false;
            }
        }

        if (fg.id == 'fgCampanhas')
        {
            if (Col == getColIndexByColKey(fg, 'ValorAssociar'))
            {
                fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
                
                nValorAssociar = fg.valueMatrix(Row, Col);
                nValorSaldo = fg.valueMatrix(Row, getColIndexByColKey(fg, 'Saldo*'));

                if ((nValorAssociar > 0) && (nValorSaldo > 0))
                {
                    fg.textMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;
                    
                    if (nValorAssociar > nValorSaldo)
                        fg.textMatrix(Row, Col) = nValorSaldo;
                }
                else
                {
                    fg.textMatrix(Row, getColIndexByColKey(fg, 'OK')) = 0;
                    fg.textMatrix(Row, Col) = 0;
                }
            }
            else //if (Col == getColIndexByColKey(fg, 'OK')) 
            {
                nValorSaldo = fg.valueMatrix(Row, getColIndexByColKey(fg, 'Saldo*'));
                
                if (fg.textMatrix(Row, getColIndexByColKey(fg, 'OK')) != 0)
                    fg.textMatrix(Row, getColIndexByColKey(fg, 'ValorAssociar')) = nValorSaldo;
                else
                    fg.textMatrix(Row, getColIndexByColKey(fg, 'ValorAssociar')) = 0;
            }
            
            calculaTotais();
        }
    }
}

function js_BeforeRowColChangeModalAssociarFinanceiro(grid, OldRow, OldCol, NewRow, NewCol, Cancel)
{
    //Forca celula read-only
    
    //js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);

    glb_HasGridButIsNotFormPage = true;

    if (!glb_GridIsBuilding)
    {
        if (glb_FreezeRolColChangeEvents)
            return true;

        if (cellIsLocked(grid, NewRow, NewCol))
        {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
        {
            glb_validRow = NewRow;
            glb_validCol = NewCol;
        }
    }
}

function cellIsLocked(grid, nRow, nCol) 
{
    var retVal = true;

    if ((grid.id == 'fgFinanceiro') && (((nRow == 1) && (nCol == getColIndexByColKey(grid, 'Duplicata*'))) || (nCol == getColIndexByColKey(grid, 'EmpresaID*')) || 
            (nCol == getColIndexByColKey(grid, 'OK'))))
        retVal = false;

    if ((grid.id == 'fgCampanhas') && ((nCol == getColIndexByColKey(grid, 'OK')) || (nCol == getColIndexByColKey(grid, 'ValorAssociar'))))
        retVal = false;
        
    return retVal;
}

function fg_EnterCell_Prg() 
{
    return null;
}

function js_fg_ModalAssociarFinanceiroDblClick(grid, Row, Col) 
{
    if ((glb_GridIsBuilding) || (grid.id == 'fgFinanceiro'))
        return null;

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if (Col != getColIndexByColKey(grid, 'OK'))
        return true;

    // trap so header
    if (Row != 1)
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);

    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) 
    {
        if (grid.ValueMatrix(i, Col) != 0) 
        {
            bFill = false;
            break;
        }
    }

    for (i = 1; i < grid.Rows; i++) 
    {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    window.focus();
    grid.focus();
}

function js_ModalAssociarFinanceiro_BeforeEdit(grid, Row, Col) 
{
    if (glb_GridIsBuilding)
        return null;
}

function AssociarCampanhaFinanceiro()
{
    var strPars = '';
    var nBytesAcum = 0;
    var nFinanceiro = getFinanceiro();
    var nFinanceiroID = ((nFinanceiro.length > 0) ? nFinanceiro[0] : 0);
    var sDuplicata = ((nFinanceiro.length > 0) ? nFinanceiro[1] : 0);
    var nValorFinanceiro = ((nFinanceiro.length > 0) ? nFinanceiro[2] : 0);
    var nEmpresaID = ((nFinanceiro.length > 0) ? nFinanceiro[3] : 0);
    var nPedIteCampanhaID = getPedIteCampanha();
    var nUserID = glb_USERID;
    var nValor = 0;
    var sMsgErro = '';
    var nDataLen = 0;
    var i;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    
    glb_nEmpresaID = nEmpresaID;

    for (i = 0; i < nPedIteCampanhaID.length; i++)
    {
        nValor = (parseFloat(nValor) + parseFloat(nPedIteCampanhaID[i][1].toString().replace(",", "."))).toFixed(2);
    }

    if (nFinanceiroID == 0)
    {
        nValorFinanceiro = nValor;
    }
    else
    {
        if (nValor > nValorFinanceiro.replace(",", "."))
            sMsgErro = 'Valor do financeiro menor que o valor das campanhas a associar';
    }

    if (sMsgErro == '')
    {
        strPars = '?nFinanceiroID=' + escape(nFinanceiroID);
        strPars += '&sDuplicata=' + escape(sDuplicata);
        strPars += '&nValorFinanceiro=' + escape(nValorFinanceiro);
        strPars += '&nEmpresaID=' + escape(nEmpresaID);
        strPars += '&nUserID=' + escape(nUserID);

        for (i = 0; i < nPedIteCampanhaID.length; i++)
        {
            nBytesAcum = strPars.length;

            if (nBytesAcum >= glb_nMaxStringSize)
            {
                nBytesAcum = 0;

                if (strPars != '')
                {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                    strPars = '';
                    nDataLen = 0;
                }
            }

            if (strPars != '')
                strPars += '&nPedIteCampanhaID=' + escape(nPedIteCampanhaID[i][0].toString());
            else
                strPars = '&nPedIteCampanhaID=' + escape(nPedIteCampanhaID[i][0].toString());
            
            strPars += '&nValor=' + escape(nPedIteCampanhaID[i][1].toString());

            nDataLen++;
        }

        strPars += '&nDataLen=' + escape(nDataLen);

        if (strPars != '') 
        {
            glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
            strPars = '';
            nDataLen = 0;
        }

        sendDataToServer();
    }
    else
    {
        window.top.overflyGen.Alert(sMsgErro);
        
        lockControlsInModalWin(false);
    }
    
}

function AssociarCampanhaFinanceiro_DSC()
{
    if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }
    
    var sResultado = "";
    var nFinanceiroID = 0;
    var _retMsg;

    if (!((dsoAssociarCampanhaFinanceiro.recordset.BOF) || (dsoAssociarCampanhaFinanceiro.recordset.EOF)))
    {
        sResultado = dsoAssociarCampanhaFinanceiro.recordset['Resultado'].value;
        nFinanceiroID = dsoAssociarCampanhaFinanceiro.recordset['FinanceiroID'].value;

        if ((sResultado != null) && (sResultado != ""))
            window.top.overflyGen.Alert(sResultado);
        else if ((nFinanceiroID != null) && (nFinanceiroID != 0))
        {
            _retMsg = window.top.overflyGen.Confirm('Campanhas relacionadas ao Financeiro ' + nFinanceiroID + '. Deseja detalhar?');
            
            if (_retMsg == 1)
                sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(glb_nEmpresaID, nFinanceiroID));
        }
    }

    getDataInServerCampanhas();
    getDataInServerFinanceiros();
}

function preencheFornecedor()
{
    setConnection(dsoFornecedores);
    dsoFornecedores.SQL = 'SELECT 0 AS fldID, SPACE(1) AS fldName ' +
		                    'UNION ALL ' +
		                    'SELECT DISTINCT c.PessoaID AS fldID, c.Fantasia AS fldName ' +
		                        'FROM RelacoesPesCon a WITH(NOLOCK) ' +
		                            'INNER JOIN RelacoesPesCon_Fornecedores b WITH(NOLOCK) ON (b.RelacaoID = a.RelacaoID) ' +
		                                'INNER JOIN Pessoas c WITH (NOLOCK) ON (c.PessoaID = b.FornecedorID) ' +
		                        'WHERE (a.EstadoID NOT IN (4,5)) ' +
                                'GROUP BY c.PessoaID, c.Fantasia ' +
		                    'ORDER BY fldName';

    dsoFornecedores.ondatasetcomplete = preencheFornecedor_DSC;
    dsoFornecedores.Refresh();
}

function preencheFornecedor_DSC()
{
    var i;
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selFornecedor];
    var aDSOsDunamics = [dsoFornecedores];

    clearComboEx(['selFornecedor']);

    for (i = 0; i < aCmbsDynamics.length; i++) {
        aDSOsDunamics[i].recordset.moveFirst();
        while (!aDSOsDunamics[i].recordset.EOF) {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }

        aCmbsDynamics[i].disabled = (aCmbsDynamics[i].options.length == 0);
    }

    return null;
}

/*
function selFornecedor_onchange()
{
    ;
}
*/

function getFinanceiro()
{
    var nFinanceiro = new Array();

    for (i = 1; i < fgFinanceiro.Rows; i++)
    {
        if (fgFinanceiro.textMatrix(i, getColIndexByColKey(fgFinanceiro, 'OK')) != 0)
        {
            nFinanceiro = [fgFinanceiro.textMatrix(i, getColIndexByColKey(fgFinanceiro, 'FinanceiroID*')),
                            fgFinanceiro.textMatrix(i, getColIndexByColKey(fgFinanceiro, 'Duplicata*')),
                            fgFinanceiro.valueMatrix(i, getColIndexByColKey(fgFinanceiro, 'Valor*')),
                            fgFinanceiro.textMatrix(i, getColIndexByColKey(fgFinanceiro, 'EmpresaID*'))];
            break;
        }
    }

    return nFinanceiro;
}

function getPedIteCampanha()
{
    var nPedIteCampanhaID = new Array();

    for (i = 1; i < fgCampanhas.Rows; i++)
    {
        if (fgCampanhas.textMatrix(i, getColIndexByColKey(fgCampanhas, 'OK')) != 0)
        {
            nPedIteCampanhaID[nPedIteCampanhaID.length] = [[fgCampanhas.textMatrix(i, getColIndexByColKey(fgCampanhas, 'PedIteCampanhaID'))], 
                                                           [fgCampanhas.valueMatrix(i, getColIndexByColKey(fgCampanhas, 'ValorAssociar'))]];
        }
    }

    return nPedIteCampanhaID;
}

function dsoCombosGrid()
{
    lockControlsInModalWin(true);
    
    var aEmpresa = getCurrEmpresaData();
    // parametrizacao do dso dsoGrid
    setConnection(dsoCmbGrid01);
    dsoCmbGrid01.SQL = 'SELECT 0 AS fldID, \'\' AS fldName UNION ALL ' +
                        'SELECT a.PessoaID AS fldID, a.Fantasia AS fldName ' +
	                        'FROM Pessoas a WITH(NOLOCK) ' +
		                        'INNER JOIN RelacoesPesRec b WITH(NOLOCK) ON ((b.SujeitoID = a.PessoaID) AND (b.ObjetoID = 999)) ' +
	                        'WHERE ((a.TipoPessoaID = 52) AND (a.ClassificacaoID = 56) ' +
			                        'AND (a.EstadoID = 2) AND (b.EstadoID = 2) ' +
			                        'AND (b.TipoRelacaoID = 12) AND (dbo.fn_Empresa_Sistema(a.PessoaID) = 1) ' +
			                        'AND (dbo.fn_Pessoa_Localidade(a.PessoaID, 1, NULL, NULL) = dbo.fn_Pessoa_Localidade(' + aEmpresa[0] + ', 1, NULL, NULL)) ' +
			                        'AND (a.PessoaID NOT IN (3, 4, 12, 15))) '; //-- DURO - Retirando empresas que est�o em A mas n�o est�o mais em atividade.
    dsoCmbGrid01.ondatasetcomplete = fillGridData_DSC;
    dsoCmbGrid01.Refresh();
}

function fillGridData_DSC()
{
    preencheComboEmpresa();
}

function preencheComboEmpresa()
{
    var i;
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selEmpresa];
    var aDSOsDunamics = [dsoCmbGrid01];

    clearComboEx(['selEmpresa']);

    for (i = 0; i < aCmbsDynamics.length; i++) 
    {
        aDSOsDunamics[i].recordset.moveFirst();
        while (!aDSOsDunamics[i].recordset.EOF) 
        {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }

        aCmbsDynamics[i].disabled = (aCmbsDynamics[i].options.length == 0);
    }

    lockControlsInModalWin(false);
    
    return null;
}

/********************************************************************
Calcula Totais
********************************************************************/
function calculaTotais()
{
	var i = 0;
	var nCounter = 0;
	var nValorAssociar = 0;

	for (i = 2; i < fgCampanhas.Rows; i++)
	{
		if (fgCampanhas.ValueMatrix(i, getColIndexByColKey(fgCampanhas, 'OK')) != 0)
		{
			nCounter++;
			nValorAssociar += fgCampanhas.ValueMatrix(i, getColIndexByColKey(fgCampanhas, 'ValorAssociar'));
		}
	}

	if (fgCampanhas.Rows > 1)
	{
		fgCampanhas.TextMatrix(1, getColIndexByColKey(fgCampanhas, 'PedidoID*')) = nCounter;
		fgCampanhas.TextMatrix(1, getColIndexByColKey(fgCampanhas, 'ValorAssociar')) = nValorAssociar;
	}
}

function habilitaInterface()
{
    if (glb_nContador == 2)
    {
        lockControlsInModalWin(false);
        glb_nContador = 0;
    }
}

function sendDataToServer(nFinanceiroID)
{
    var strPars = '';
    var nUserID = glb_USERID;
    
    if (glb_nTimerSendDataToServer != null)
    {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try
    {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
        {
            if (nFinanceiroID > 0)
            {
                strPars = '?nFinanceiroID=' + escape(nFinanceiroID) + '&nUserID=' + escape(nUserID) + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            }
            else
                strPars = glb_aSendDataToServer[glb_nPointToaSendDataToServer];

            dsoAssociarCampanhaFinanceiro.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/campanhas/serverside/AssociarCampanhaFinanceiro.aspx' + strPars;
            dsoAssociarCampanhaFinanceiro.ondatasetcomplete = sendDataToServer_DSC;
            dsoAssociarCampanhaFinanceiro.Refresh();
        }
        else
        {
            glb_OcorrenciasTimerInt = window.setInterval('AssociarCampanhaFinanceiro_DSC()', 10, 'JavaScript');
        }
    }
    catch (e)
    {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        //getDataInServer();
    }
}

function sendDataToServer_DSC()
{
    var nFinanceiroID = 0;
 
    glb_nPointToaSendDataToServer++;

    if (!((dsoAssociarCampanhaFinanceiro.recordset.BOF) || (dsoAssociarCampanhaFinanceiro.recordset.EOF)))
    {
        nFinanceiroID = dsoAssociarCampanhaFinanceiro.recordset['FinanceiroID'].value;
        glb_nTimerSendDataToServer = window.setInterval('sendDataToServer(' + nFinanceiroID + ')', INTERVAL_BATCH_SAVE, 'JavaScript');
    }
    else
        glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

//Combo Tipo Campanhas. BJBN
function comboTipoCampanha()
{
    lockControlsInModalWin(true);

    var aEmpresa = getCurrEmpresaData();
    // parametrizacao do dso dsoGrid
    setConnection(dsoTipoCampanha);
    dsoTipoCampanha.SQL = 'SELECT 0 AS fldID, \'\' AS fldName UNION ALL ' +
                        'SELECT c.ItemID AS fldID, c.ItemMasculino AS fldName ' +
                            'FROM Recursos a WITH(NOLOCK) ' +
                                'INNER JOIN RelacoesRecursos b WITH(NOLOCK) ON (b.ObjetoID = a.RecursoID) ' +
                                'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.Filtro LIKE \'%\' + CONVERT(VARCHAR(10), a.RecursoID) + \'%\') ' +
                            'WHERE ((b.SujeitoID = 24150) AND (c.TipoID = 812))';
    dsoTipoCampanha.ondatasetcomplete = comboTipoCampanha_DSC;
    dsoTipoCampanha.Refresh();
}

function comboTipoCampanha_DSC()
{
    preencheComboTipoCampanha();
}

function preencheComboTipoCampanha() {
    var i;
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selTipoCampanhaID];
    var aDSOsDunamics = [dsoTipoCampanha];

    clearComboEx(['selTipoCampanhaID']);

    for (i = 0; i < aCmbsDynamics.length; i++) {
        aDSOsDunamics[i].recordset.moveFirst();
        while (!aDSOsDunamics[i].recordset.EOF) {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }

        aCmbsDynamics[i].disabled = (aCmbsDynamics[i].options.length == 0);
    }

    lockControlsInModalWin(false);

    return null;
}

function verificaData(sData)
{
    var sData = trimStr(sData);
    var bDataIsValid = true;

    if (sData != '')
        bDataIsValid = chkDataEx(sData);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        return false;
    }
    return true;
}

function verificaDatas(dtInicio, dtFim)
{
    var dias = 0;
    var date1 = new String(dtInicio);
    var date2 = new String(dtFim);

    date1 = date1.split("/");
    date2 = date2.split("/");

    if (DATE_FORMAT == "DD/MM/YYYY")
    {
        var sDate = new Date(date1[1] + "/" + date1[0] + "/" + date1[2]);
        var eDate = new Date(date2[1] + "/" + date2[0] + "/" + date2[2]);
    }
    else if (DATE_FORMAT == "MM/DD/YYYY")
    {
        var sDate = new Date(date1[0] + "/" + date1[1] + "/" + date1[2]);
        var eDate = new Date(date2[0] + "/" + date2[1] + "/" + date2[2]);
    }

    dias = Math.round((eDate - sDate) / 86400000);

    if (dias > 150)
        return true;
    else
        return false;
}