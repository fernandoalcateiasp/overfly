/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// controla o retorno do servidor dos dados de combos dinamicos
var glb_CounterCmbsDynamics = 0;
// guarda o botao clicado enviado pelo frameword
var glb_BtnFromFrameWork;
var glb_cmbLupa = null;
var glb_bInclusao = false;
var glb_bContato = false;
var dsoSup01 = new CDatatransport('dsoSup01');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoCmbDynamic02 = new CDatatransport('dsoCmbDynamic02');
var dsoCmbDynamic03 = new CDatatransport('dsoCmbDynamic03');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoVerificacao = new CDatatransport('dsoVerificacao');
var dsoCmbsLupa = new CDatatransport('dsoCmbsLupa');
var dsoCmbsLupa2 = new CDatatransport('dsoCmbsLupa2');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload() {
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);

    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selTipoRegistroID', '1'],
						  ['selMoedaID', '2'],
						  ['selDesconsiderar', '3']]);

    windowOnLoad_1stPart();

    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modcomercial/submodpedidos/campanhas/inferior.asp',
                              SYS_PAGESURLROOT + '/modcomercial/submodpedidos/campanhas/pesquisa.asp');

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);

    windowOnLoad_2ndPart();

    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'CampanhaID';

    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoRegistroID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoCampanhaID';
}

function setupPage() {
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup', [[1, 'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
                          ['lblEstadoID', 'txtEstadoID', 2, 1],
                          ['lblTipoRegistroID', 'selTipoRegistroID', 9, 1],
                          ['lbldtEmissao', 'txtdtEmissao', 10, 1],
                          ['lblCampanha', 'txtCampanha', 39, 1],
                          ['lblCodigo', 'txtCodigo', 20, 1],
                          ['lblFornecedorID', 'selFornecedorID', 22, 2],
                          ['btnFindFornecedor', 'btn', 21, 2],
                          ['lblContatoID', 'selContatoID', 20, 2],
                          ['lblClienteID', 'selClienteID', 22, 2],
                          ['btnFindCliente', 'btn', 21, 2],
                          ['lblIdentificador', 'txtIdentificador', 10, 2],
                          ['lbldtInicio', 'txtdtInicio', 10, 3],
                          ['lbldtFim', 'txtdtFim', 10, 3],
                          ['lblDesconsiderar', 'selDesconsiderar', 10, 3],
                          ['lblPrazo', 'txtPrazo', 5, 3],
                          ['lblMoedaID', 'selMoedaID', 8, 3],
                          ['lblValorTotal', 'txtValorTotal', 10, 3],
                          ['lblSaldo', 'txtSaldo', 10, 3],
                          ['lblPedidosPorCliente', 'txtPedidosPorCliente', 6, 3],
                          ['lblCampanhaInterna', 'chkCampanhaInterna', 3, 3],
                          ['lblSolicitaAprovacao', 'chkSolicitaAprovacao', 3, 3],
                          ['lblCusto', 'chkCusto', 3, 3],
                          ['lblRebatePermiteDPA', 'chkRebatePermiteDPA', 3, 3],
                          ['lblObservacao', 'txtObservacao', 30, 4]], null, null, true);
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2) {
    if (param1 == 'SHOWCAMPANHA') {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if (__currFormState() != 0)
            return null;

        // param2 - traz array:
        // o id da empresa do form que mandou
        // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();

        if (param2[0] != empresa[0])
            return null;

        window.top.focus();

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);

        return new Array(getHtmlId(), null, null);
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2) {

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@

    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.

}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked) {
    //@@
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)

    glb_BtnFromFramWork = btnClicked;

    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR')) {
        adjustLabelsCombos(true);
        setFieldsVisibility();
        startDynamicCmbs();
    }
    else
        // volta para a automacao    
        finalOfSupCascade(glb_BtnFromFramWork);
}

function setFieldsVisibility() {
    var aItemSelected = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = aItemSelected[1];

    lblClienteID.style.visibility = (nContextoID == 5161 ? 'inherit' : 'hidden');
    selClienteID.style.visibility = (nContextoID == 5161 ? 'inherit' : 'hidden');
    btnFindCliente.style.visibility = (nContextoID == 5161 ? 'inherit' : 'hidden');
    lblIdentificador.style.visibility = (nContextoID == 5161 ? 'inherit' : 'hidden');
    txtIdentificador.style.visibility = (nContextoID == 5161 ? 'inherit' : 'hidden');
    lblCusto.style.visibility = (nContextoID == 5161 ? 'inherit' : 'hidden');
    chkCusto.style.visibility = (nContextoID == 5161 ? 'inherit' : 'hidden');
    lblRebatePermiteDPA.style.visibility = (nContextoID == 5162 ? 'inherit' : 'hidden');
    chkRebatePermiteDPA.style.visibility = (nContextoID == 5162 ? 'inherit' : 'hidden');
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked) {
    //@@

    // campos read-only
    setReadOnlyFields();

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    txtIdentificador.innerText = '';

    if (dsoSup01.recordset.Fields.count > 0)
        if (!((dsoSup01.recordset.BOF) && (dsoSup01.recordset.EOF)))
            if (dsoSup01.recordset['Identificador'].value != null)
                txtIdentificador.innerText = dsoSup01.recordset['Identificador'].value;

    // campos read-only
    setReadOnlyFields();

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

function setTipoRegistro() {
    adjustLabelsCombos(true);
    setFieldsVisibility();

    if (selTipoRegistroID.options.length == 1) {
        if (dsoSup01.recordset.fields.count > 0)
            if (!((dsoSup01.recordset.BOF) && (dsoSup01.recordset.EOF)))
                dsoSup01.recordset[glb_sFldTipoRegistroName].value = selTipoRegistroID.options(0).value;
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selTipoRegistroID')
        adjustSupInterface();

    if (cmbID == 'selFornecedorID') {
        glb_bContato = true;
        startDynamicCmbs();
    }

    adjustLabelsCombos(false);

    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK(currEstadoID, newEstadoID) {
    verifyCampanhaInServer(currEstadoID, newEstadoID);
    return true;
    //@@ padrao do frame work
    //return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID, bSavedID) {
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {
    //@@
    var cmbID = '';
    var nTipoRelacaoID = 21;
    var nRegExcluidoID = '';

    if (btnClicked.id == 'btnFindFornecedor') {
        showModalRelacoes(window.top.formID, 'S', 'selFornecedorID', 'OBJ', getLabelNumStriped(lblFornecedorID.innerText), nTipoRelacaoID, nRegExcluidoID);
        return null;
    }
    else if (btnClicked.id == 'btnFindCliente') {
        nRegExcluidoID = selFornecedorID.value;

        showModalRelacoes(window.top.formID, 'S', 'selClienteID', 'SUJ', getLabelNumStriped(lblClienteID.innerText), nTipoRelacaoID, nRegExcluidoID);
        return null;
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    // Documentos
    if ((controlBar == 'SUP') && (btnClicked == 1)) {
        __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
    }
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    var nEmpresaID = getCurrEmpresaData();

    glb_BtnFromFramWork = btnClicked;

    if (btnClicked == 'SUPOK') {
        // Se e um novo registro
        if (dsoSup01.recordset[glb_sFldIDName].value == null) {
            dsoSup01.recordset['EmpresaID'].value = nEmpresaID[0];
        }
    }

    if (btnClicked == 'SUPALT') {
        selFornecedorID.disabled = selFornecedorID.options.length <= 0;
        selClienteID.disabled = selClienteID.options.length <= 0;
        selContatoID.disabled = selContatoID.options.length <= 0;
    }

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Modal de relacoes
    if (idElement.toUpperCase() == 'MODALRELACOESHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            fillFieldsByRelationModal(param2);

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
        // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode() {
    // campos read-only
    setReadOnlyFields();
    setFieldsVisibility();
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad() {
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 0, 0, 0]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Processo', '', '', '']);
}

/********************************************************************
Funcao do programador
Seta o campos read-only do sup
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setReadOnlyFields() {
    txtIdentificador.readOnly = true;
    txtSaldo.readOnly = true;
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs() {
    var nCampanhaID = 0;
    var aEmpresa = getCurrEmpresaData();
    var nEmpresaID = 0;
    var nFornecedorID = 0;
    var nClienteID = 0;
    var nContatoID = 0;

    if (glb_bInclusao || glb_bContato) {
        lockInterface(true);

        if (glb_bContato) {
            nEmpresaID = aEmpresa[0];
            nFornecedorID = selFornecedorID.value;
        }
    }
    else {
        nCampanhaID = dsoSup01.recordset['CampanhaID'].value;
        nFornecedorID = dsoSup01.recordset['FornecedorID'].value;
        nClienteID = dsoSup01.recordset['ClienteID'].value;
        nContatoID = dsoSup01.recordset['ContatoID'].value;
    }

    if (!glb_bContato)
        glb_CounterCmbsDynamics = 3;
    else
        glb_CounterCmbsDynamics = 1;

    if (!glb_bContato) {
        setConnection(dsoCmbDynamic01);

        dsoCmbDynamic01.SQL = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +
							  'UNION ALL SELECT 1 AS Indice, a.PessoaID AS fldID, a.Fantasia AS fldName ' +
		                      'FROM Pessoas a WITH(NOLOCK) ' +
		                      'WHERE (a.PessoaID = ' + nFornecedorID + ') ' +
		                      'UNION ALL SELECT DISTINCT 2 AS Indice, b.PessoaID AS fldID, b.Fantasia AS fldName ' +
								'FROM Campanhas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) WHERE (a.EstadoID <> 5 AND a.FornecedorID = b.PessoaID AND b.EstadoID = 2) ' +
							  'ORDER BY Indice, fldName ';

        dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
        dsoCmbDynamic01.Refresh();

        setConnection(dsoCmbDynamic02);

        dsoCmbDynamic02.SQL = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName UNION ALL ' +
							  'SELECT 1 AS Indice, a.PessoaID AS fldID, a.Fantasia AS fldName ' +
		                      'FROM Pessoas a WITH(NOLOCK) ' +
		                      'WHERE (a.PessoaID=' + nClienteID + ') ' +
		                      'UNION ALL SELECT DISTINCT 2 AS Indice, b.PessoaID AS fldID, b.Fantasia AS fldName ' +
								'FROM Campanhas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) WHERE (a.EstadoID <> 5 AND a.ClienteID = b.PessoaID AND b.EstadoID = 2) ' +
							  'ORDER BY Indice, fldName ';

        dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
        dsoCmbDynamic02.Refresh();
    }

    setConnection(dsoCmbDynamic03);

    if (glb_bContato) {
        dsoCmbDynamic03.SQL = 'SELECT DISTINCT 0 AS Indice, c.PessoaID AS fldID, c.Fantasia AS fldName ' +
								'FROM RelacoesPessoas a WITH(NOLOCK), RelacoesPessoas_Contatos b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
								'WHERE (a.SujeitoID = ' + nEmpresaID + ' AND ' +
									'a.EstadoID = 2 AND a.TipoRelacaoID = 21 AND a.ObjetoID = ' + nFornecedorID + ' AND a.RelacaoID = b.RelacaoID AND ' +
									'b.Comercial = 1 AND b.ContatoID = c.PessoaID AND c.EstadoID = 2) ' +
							  'ORDER BY Indice, fldName ';
    }
    else {
        dsoCmbDynamic03.SQL = 'SELECT 0 AS Indice, a.PessoaID AS fldID, a.Fantasia AS fldName ' +
		                      'FROM Pessoas a WITH(NOLOCK) ' +
		                      'WHERE (a.PessoaID=' + nContatoID + ') ' +
		                      'UNION ALL SELECT DISTINCT 1 AS Indice, d.PessoaID AS fldID, d.Fantasia AS fldName ' +
								'FROM Campanhas_Empresas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), RelacoesPessoas_Contatos c WITH(NOLOCK), Pessoas d WITH(NOLOCK) ' +
								'WHERE (a.CampanhaID = ' + nCampanhaID + ' AND a.EhDefault = 1 AND a.EmpresaID = b.SujeitoID AND ' +
									'b.EstadoID = 2 AND b.TipoRelacaoID = 21 AND b.ObjetoID = ' + nFornecedorID + ' AND b.RelacaoID = c.RelacaoID AND ' +
									'c.Comercial = 1 AND c.ContatoID = d.PessoaID AND d.EstadoID = 2) ' +
							  'ORDER BY Indice, fldName ';
    }

    dsoCmbDynamic03.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic03.Refresh();
}

/********************************************************************
Funcao do programador
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics != 0)
        return null;

    var i;
    var pOptionStr;
    var pOptionValue;
    var oldDataSrc;
    var oldDataFld;
    var lastIDFilled;
    var aCmbsDynamics = [selFornecedorID, selClienteID, selContatoID];
    var aDSOsDunamics = [dsoCmbDynamic01, dsoCmbDynamic02, dsoCmbDynamic03];

    if (glb_bInclusao || glb_bContato)
        lockInterface(false);

    // Inicia o carregamento de combos dinamicos (selFornecedorID )
    if (!glb_bContato)
        clearComboEx(['selFornecedorID', 'selClienteID', 'selContatoID']);
    else
        clearComboEx(['selContatoID']);

    for (i = 0; i <= 2; i++) {
        aDSOsDunamics[i].recordset.MoveFirst();
        oldDataSrc = aCmbsDynamics[i].dataSrc;
        oldDataFld = aCmbsDynamics[i].dataFld;
        aCmbsDynamics[i].dataSrc = '';
        aCmbsDynamics[i].dataFld = '';
        lastIDFilled = null;

        if (!(aDSOsDunamics[i].recordset.BOF && aDSOsDunamics[i].recordset.EOF)) {
            pOptionStr = aDSOsDunamics[i].recordset['fldName'];
            pOptionValue = aDSOsDunamics[i].recordset['fldID'];
        }

        while (!aDSOsDunamics[i].recordset.EOF) {
            if (lastIDFilled == pOptionValue.value) {
                aDSOsDunamics[i].recordset.MoveNext();
                continue;
            }

            var oOption = document.createElement("OPTION");
            oOption.text = pOptionStr.value;
            oOption.value = pOptionValue.value;

            aCmbsDynamics[i].add(oOption);

            lastIDFilled = pOptionValue.value;

            aDSOsDunamics[i].recordset.MoveNext();
        }

        aCmbsDynamics[i].dataSrc = oldDataSrc;
        aCmbsDynamics[i].dataFld = oldDataFld;

        if (glb_bInclusao || glb_bContato)
            aCmbsDynamics[i].disabled = aCmbsDynamics[i].options.length <= 0;
    }

    if (glb_bInclusao || glb_bContato) {
        // funcao da automacao que mostra o div secundario coerente
        adjustSupInterface();

        txtCampanha.focus();

        // escreve na barra de status
        writeInStatusBar('child', 'cellMode', 'Detalhe');

        glb_bInclusao = false;
        glb_bContato = false;

        // nao mexer
        return 0;
    }
    else {
        // volta para a automacao
        finalOfSupCascade(glb_BtnFromFramWork);
    }

    return null;
}

/********************************************************************
Funcao criada pelo programador.
Preenche os campos do subform corrente, com os dados vindos da janela
modal de relacoes

Parametro:
aData   -   array de dados
            aData[0] - ID do combo
            aData[1] - ID selecionado no grid da modal
            aData[2] - texto correspondente ao ID selecionado no grid da modal
            
            os demais elementos sao opcionais

Retorno:
nenhum
********************************************************************/
function fillFieldsByRelationModal(aData) {
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var elem;
    var addSexOrServ = null;

    elem = window.document.getElementById(aData[0]);

    if (elem == null)
        return;

    clearComboEx([elem.id]);

    oldDataSrc = elem.dataSrc;
    oldDataFld = elem.dataFld;
    elem.dataSrc = '';
    elem.dataFld = '';

    // Preencher o combo selClienteID
    if ((aData[0] == 'selClienteID') || (aData[0] == "selFornecedorID")) {
        // Insere um item em branco
        oOption = document.createElement("OPTION");
        oOption.text = '';
        oOption.value = 0;
        elem.add(oOption);
    }

    oOption = document.createElement("OPTION");
    oOption.text = aData[2];
    oOption.value = aData[1];
    elem.add(oOption);

    if (aData[0] == 'selClienteID')
        elem.selectedIndex = 1;
    else
        elem.selectedIndex = 0;

    // Preencher o combo selFornecedorID
    if (aData[0] == 'selFornecedorID') {
        dsoSup01.recordset.Fields['FornecedorID'].value = aData[1];
    }
        // Preencher o combo selClienteID
    else if (aData[0] == 'selClienteID') {
        dsoSup01.recordset.Fields['ClienteID'].value = aData[1];
        txtIdentificador.innerText = aData[3];
    }

    elem.dataSrc = oldDataSrc;
    elem.dataFld = oldDataFld;
    elem.disabled = elem.options.length <= 0;
    adjustLabelsCombos(false);

    if (aData[0] == 'selFornecedorID') {
        glb_bContato = true;
        startDynamicCmbs();
    }
}

function adjustLabelsCombos(bPaging) {
    if (bPaging) {
        setLabelOfControl(lblFornecedorID, dsoSup01.recordset['FornecedorID'].value);
        setLabelOfControl(lblClienteID, dsoSup01.recordset['ClienteID'].value);
        setLabelOfControl(lblContatoID, dsoSup01.recordset['ContatoID'].value);
        setLabelOfControl(lblMoedaID, dsoSup01.recordset['MoedaID'].value);
        setLabelOfControl(lblTipoRegistroID, dsoSup01.recordset['TipoCampanhaID'].value);
    }
    else {
        setLabelOfControl(lblFornecedorID, selFornecedorID.value);
        setLabelOfControl(lblClienteID, selClienteID.value);
        setLabelOfControl(lblContatoID, selContatoID.value);
        setLabelOfControl(lblMoedaID, selMoedaID.value);
        setLabelOfControl(lblTipoRegistroID, selTipoRegistroID.value);
    }
}
/********************************************************************
Verificacao da Campanha
********************************************************************/
function verifyCampanhaInServer(nCurrEstadoID, nNewEstadoID) {
    var nUserID = getCurrUserID();
    var nCampanhaID = dsoSup01.recordset['CampanhaID'].value;
    var strPars = '';

    strPars = '?nCampanhaID=' + escape(nCampanhaID);
    strPars += '&nCurrEstadoID=' + escape(nCurrEstadoID);
    strPars += '&nNewEstadoID=' + escape(nNewEstadoID);
    strPars += '&nUserID=' + escape(nUserID);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/campanhas/serverside/verificacampanha.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyCampanhaInServer_DSC;
    dsoVerificacao.refresh();
}
/********************************************************************
Retorno do servidor - Verificacao da Campanha
********************************************************************/
function verifyCampanhaInServer_DSC() {
    // NULL - Verificacao OK
    var sResultado = dsoVerificacao.recordset.Fields['Resultado'].value;

    if ((sResultado == null) || (sResultado == "")) {
        stateMachSupExec('OK');
    }
    else {
        if (window.top.overflyGen.Alert(sResultado) == 0)
            return null;

        stateMachSupExec('CANC');
    }
}
