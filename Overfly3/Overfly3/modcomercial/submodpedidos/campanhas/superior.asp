<%@  language="VBSCRIPT" enablesessionstate="False" %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="campanhassup01Html" name="campanhassup01Html">

<head>

    <title></title>

    <%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf  
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/campanhas/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/campanhas/superioresp.js" & Chr(34) & "></script>" & vbCrLf

    %>

    <%
Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf

Response.Write "var glb_dCurrDate103 = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';"
Response.Write "var glb_dCurrDate101 = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';"
Response.Write "</script>"
    %>

    <script id="wndJSProc" language="javascript">
<!--

    //-->
    </script>
</head>
<!-- //@@ -->
<body id="campanhassup01Body" name="campanhassup01Body" language="javascript" onload="return window_onload()">
    <!-- //@@ Os dsos sao definidos de acordo com o form -->

    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">

    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->

    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" datasrc="#dsoStateMachine" datafld="Estado" name="txtEstadoID" class="fldGeneral">
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo</p>
        <select id="selTipoRegistroID" name="selTipoRegistroID" class="fldGeneral" datasrc="#dsoSup01" datafld="TipoCampanhaID"></select>
        <p id="lbldtEmissao" name="lbldtEmissao" class="lblGeneral">Emiss�o</p>
        <input type="text" id="txtdtEmissao" name="txtdtEmissao" class="fldGeneral" datasrc="#dsoSup01" datafld="V_dtEmissao">
        <p id="lblCampanha" name="lblCampanha" class="lblGeneral">Campanha</p>
        <input type="text" id="txtCampanha" name="txtCampanha" datasrc="#dsoSup01" datafld="Campanha" class="fldGeneral">
        <p id="lblCodigo" name="lblCodigo" class="lblGeneral">C�digo</p>
        <input type="text" id="txtCodigo" name="txtCodigo" datasrc="#dsoSup01" datafld="Codigo" class="fldGeneral" title="C�digo da campanha dado pelo fornecedor">
        <p id="lblFornecedorID" name="lblFornecedorID" class="lblGeneral">Fornecedor</p>
        <select id="selFornecedorID" name="selFornecedorID" class="fldGeneral" datasrc="#dsoSup01" datafld="FornecedorID"></select>
        <input type="image" id="btnFindFornecedor" name="btnFindFornecedor" class="fldGeneral" title="Preencher combo">
        <p id="lblContatoID" name="lblContatoID" class="lblGeneral">Contato</p>
        <select id="selContatoID" name="selContatoID" class="fldGeneral" datasrc="#dsoSup01" datafld="ContatoID"></select>
        <p id="lblClienteID" name="lblClienteID" class="lblGeneral">Cliente</p>
        <select id="selClienteID" name="selClienteID" class="fldGeneral" datasrc="#dsoSup01" datafld="ClienteID"></select>
        <input type="image" id="btnFindCliente" name="btnFindCliente" class="fldGeneral" title="Preencher combo">
        <p id="lblIdentificador" name="lblIdentificador" class="lblGeneral">Identificador</p>
        <input type="text" id="txtIdentificador" name="txtIdentificador" class="fldGeneral">
        <p id="lbldtInicio" name="lbldtInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtdtInicio" name="txtdtInicio" class="fldGeneral" datasrc="#dsoSup01" datafld="V_dtInicio">
        <p id="lbldtFim" name="lbldtFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtdtFim" name="txtdtFim" class="fldGeneral" datasrc="#dsoSup01" datafld="V_dtFim">
        <p id="lblDesconsiderar" name="lblDesconsiderar" class="lblGeneral">Desconsiderar</p>
        <select id="selDesconsiderar" name="selDesconsiderar" class="fldGeneral" datasrc="#dsoSup01" datafld="Desconsiderar"></select>
        <p id="lblPrazo" name="lblPrazo" class="lblGeneral">Prazo</p>
        <input type="text" id="txtPrazo" name="txtPrazo" class="fldGeneral" datasrc="#dsoSup01" datafld="Prazo">
        <p id="lblMoedaID" name="lblMoedaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaID" name="selMoedaID" class="fldGeneral" datasrc="#dsoSup01" datafld="MoedaID"></select>
        <p id="lblValorTotal" name="lblValorTotal" class="lblGeneral">Valor Total</p>
        <input type="text" id="txtValorTotal" name="txtValorTotal" class="fldGeneral" datasrc="#dsoSup01" datafld="ValorTotal" title="Esta campanha n�o permite exceder este valor total?">
        <p id="lblSaldo" name="lblSaldo" class="lblGeneral">Saldo</p>
        <input type="text" id="txtSaldo" name="txtSaldo" class="fldGeneral" datasrc="#dsoSup01" datafld="Saldo">
        <p id="lblPedidosPorCliente" name="lblPedidosPorCliente" class="lblGeneral" title="N�mero m�ximo de pedidos por cliente">Ped</p>
        <input type="text" id="txtPedidosPorCliente" name="txtPedidosPorCliente" class="fldGeneral" datasrc="#dsoSup01" datafld="PedidosPorCliente" title="N�mero m�ximo de pedidos por cliente">
        <p id="lblCampanhaInterna" name="lblCampanhaInterna" class="lblGeneral">Int</p>
        <input type="checkbox" id="chkCampanhaInterna" name="chkCampanhaInterna" datasrc="#dsoSup01" datafld="CampanhaInterna" class="fldGeneral" title="� campanha interna?">
        <p id="lblSolicitaAprovacao" name="lblSolicitaAprovacao" class="lblGeneral">Apr</p>
        <input type="checkbox" id="chkSolicitaAprovacao" name="chkSolicitaAprovacao" datasrc="#dsoSup01" datafld="SolicitaAprovacao" class="fldGeneral" title="Solicita aprova��o pelo GP?">
        <p id="lblCusto" name="lblCusto" class="lblGeneral">Custo</p>
        <input type="checkbox" id="chkCusto" name="chkCusto" datasrc="#dsoSup01" datafld="Custo" class="fldGeneral" title="DPA depende do custo de reposicao do produto?">
        <p id="lblRebatePermiteDPA" name="lblRebatePermiteDPA" class="lblGeneral">DPA</p>
        <input type="checkbox" id="chkRebatePermiteDPA" name="chkRebatePermiteDPA" datasrc="#dsoSup01" datafld="RebatePermiteDPA" class="fldGeneral" title="Rebate permite DPA no mesmo item do pedido?">
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" datasrc="#dsoSup01" datafld="Observacao" class="fldGeneral">
    </div>

</body>

</html>
