/********************************************************************
pedidosinf01.js

Library javascript para o pedidosinf01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// controla o retorno do servidor dos dados de combos dinamicos
var glb_CounterCmbsDynamics = 0;
var glb_BtnFromFramWork = null;
// controla a geracao das parcelas de um pedido 
var glb_GerarParcelas = false;
// variavel de window setInterval
var __winTimerHandler = null;
// variavel de controle do Alert de Resultado
var glb_AlertResultado = 0;
// guarda o id da nota para enviar para a impressao
var glb_nNotaFiscalID = null;
// guarda o numero de dsos usados na empressao da nota
var glb_ndsosNota = 0;
// controla o foco do grid de itens
var glb_bFocus = null;
// variavel que controla o emissor da NF
var glb_bEmissorNF = false;
// variavel que controla a impressao da NF
var glb_bImprimeNF = false;
// variavel que forca refresh no inf apos refresh no sup
var glb_bForceRefrInf = false;
// variavel que controla botao especifico clicado
var glb_nBtnNotEspecific = 0;
// variavel que controla se o Pedido esta Fechado
var glb_bFechado = false;
// variavel controla se usuario pediu impressao de nota
var glb_bPrintingNF = false;
// vari�vel de controle do carregamento dinamico do printjet
var glb_nCallerPrintJet;
// variavel minuta
var glb_sMinuta;
// variavel de direitos
var glb_bA1;
var glb_bA2;
//variavel pegar empresa
var aEmpresa = getCurrEmpresaData();
// variavel que controle se devera imprimir boleto de cobranca
var glb_PrintListaOrOP_Timer = null;
var glb_CallOpenModalPrint_Timer = null;
var glb_CallOpenModalVL_Timer = null;
var glb_CallPrintNotaFiscal_Timer = null;
var glb_bOpenModalGVL = false;

var glb_winModalPrint2_Timer = null;
var glb_callPrintListaEmbalagem_Timer = null;
var glb_Rntc = null;
var glb_bPedidosAntecipadosPendentes = false;


// Ativo e Consumo - SGP
var glb_bNCM = null;

var dso01JoinSup = new CDatatransport('dso01JoinSup');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoDynamicCmbs = new CDatatransport('dsoDynamicCmbs');
var dsoCurrRelation = new CDatatransport('dsoCurrRelation');
var dsoExtra01 = new CDatatransport('dsoExtra01');
var dsoDissociar = new CDatatransport('dsoDissociar');
var dso01Grid = new CDatatransport('dso01Grid');
var dso02Grid = new CDatatransport('dso02Grid');
var dsoCmb01Grid_01 = new CDatatransport('dsoCmb01Grid_01');
var dsoCmb01Grid_02 = new CDatatransport('dsoCmb01Grid_02');
var dsoCmb01Grid_03 = new CDatatransport('dsoCmb01Grid_03');
var dso01GridLkp = new CDatatransport('dso01GridLkp');
var dso02GridLkp = new CDatatransport('dso02GridLkp');
var dso03GridLkp = new CDatatransport('dso03GridLkp');
var dso04GridLkp = new CDatatransport('dso04GridLkp');
var dso05GridLkp = new CDatatransport('dso05GridLkp');
var dso06GridLkp = new CDatatransport('dso06GridLkp');
var dso07GridLkp = new CDatatransport('dso07GridLkp');
var dsoProdutoData = new CDatatransport('dsoProdutoData');
var dsoParcelas = new CDatatransport('dsoParcelas');
var dsoAsstec = new CDatatransport('dsoAsstec');
var dsoAprovacaoEspecial = new CDatatransport('dsoAprovacaoEspecial');
var dsoParallelGrid = new CDatatransport('dsoParallelGrid');
var dsoGeraOrdemProducao = new CDatatransport('dsoGeraOrdemProducao');
var dsoDPA = new CDatatransport('dsoDPA');
var dsoLiberarComissao = new CDatatransport('dsoLiberarComissao');
var dsoHeader = new CDatatransport('dsoHeader');
var dsoShipTo = new CDatatransport('dsoShipTo');
var dsoDetail01 = new CDatatransport('dsoDetail01');
var dsoDetail02 = new CDatatransport('dsoDetail02');
var dsoDetail03 = new CDatatransport('dsoDetail03');
var dsoDetail04 = new CDatatransport('dsoDetail04');
var dso01PgGrid = new CDatatransport('dso01PgGrid');
var dsoStateMachineLkp = new CDatatransport('dsoStateMachineLkp');
var dsoFiltroInf = new CDatatransport('dsoFiltroInf');
var dsoCmbsPropAlt = new CDatatransport('dsoCmbsPropAlt');
var dsoGen01 = new CDatatransport('dsoGen01');
var dsoEMail = new CDatatransport('dsoEMail');
var dsoDataChegadaPortador = new CDatatransport('dsoDataChegadaPortador');
var dsoVerificaInvoice = new CDatatransport('dsoVerificaInvoice');
var dsoPedidosVinculados = new CDatatransport('dsoPedidosVinculados');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
adjustElementsInDivsNonSTD()
putSpecialAttributesInControls()
optChangedInCmb(cmb)
prgServerInf(btnClicked)
prgInterfaceInf(btnClicked, pastaID, dso)
finalOfInfCascade(btnClicked)
treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, currDSO)
btnLupaClicked(btnClicked)
addedLineInGrid(folderID, grid, nLineInGrid, dso)
btnBarClicked(controlBar, btnClicked)
btnBarNotEspecClicked(controlBar, btnClicked)
modalInformForm(idElement, param1, param2)
btnAltPressedInGrid( folderID )
btnAltPressedInNotGrid( folderID )
fillCmbFiltsInfAutomatic(folderID)
selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
frameIsAboutToApplyRights(folderID)
    
FUNCOES DA MAQUINA DE ESTADO:
stateMachOpened( currEstadoID )
stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG
fg_AfterRowColChange_Prg()
fg_DblClick_Prg()
fg_ChangeEdit_Prg()
fg_ValidateEdit_Prg()
fg_BeforeRowColChange_Prg()
fg_EnterCell_Prg()
fg_MouseUp_Prg()
fg_MouseDown_Prg()
fg_BeforeEdit_Prg()
    

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload() {
    // Ze em 17/03/08
    dealWithGrid_Load();

    __adjustFramePrintJet();

    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array dos ids dos dsos que tenham campos linkados
    // a controles HTMLs.
    // Estes controles HTMls estao em divs e sao linkados a campos do dso
    // que sejam: obrigatorios, numericos ou de datas, nos dsos.
    // Se nao aplicavel ao form este array deve ser null
    glb_aDsosToCloneFieldsEtc = ['dso01JoinSup'];

    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;
    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 

    glb_aGridStaticCombos = [[20010, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', ''],
                                       [0, 'dso04GridLkp', '', '']]],
                              [24002, [[4, 'dsoCmb01Grid_02', 'Codigo|*DescricaoLocal', 'ConServicoID'], //dsoCmb01Grid_02
                                       [0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', ''],
                                       [0, 'dso04GridLkp', '', ''],
                                       [0, 'dso05GridLkp', '', ''],
                                       [0, 'dso06GridLkp', '', ''],
                                       [0, 'dso07GridLkp', '', '']]],
                              [24003, [[1, 'dsoCmb01Grid_01', 'HistoricoAbreviado', 'HistoricoPadraoID'],
									   [8, 'dsoCmb01Grid_02', 'ItemAbreviado', 'ItemID'],
                                       [12, 'dsoCmb01Grid_03', 'SimboloMoeda', 'ConceitoID'],
                                       [0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', ''],
                                       [0, 'dso04GridLkp', '', ''],
                                       [0, 'dso05GridLkp', '', ''],
                                       [0, 'dso06GridLkp', '', '']]],
                              [24011, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID'],
                                       [0, 'dso01GridLkp', '', '']]],
                              [24009, [[6, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino', 'ItemID'],
									   [7, 'dsoCmb01Grid_02', '*ItemAbreviado|ItemMasculino', 'ItemID'],
									   [0, 'dso01GridLkp', '', ''],
									   [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', ''],
                                       [0, 'dso04GridLkp', '', '']]],
                              [21027, [[1, 'dsoCmb01Grid_01', 'Conceito|ProdutoID', 'PedItemID'],
                                       [0, 'dso01GridLkp', '', '']]]];

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03',
                               'divInf01_04',
                               'divInf01_05'],
                               [20008, 20009, 24001, 24014,
                               [20010, 24002, 24003, 24011, 24012, 24004, 24005, 24006, 24008, 24009, 24010, 24013, 21027]]);

    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();
}

function setupPage() {
    //@@ Ajustar os divs
    adjustDivs('inf', [[1, 'divInf01_01'],
                      [1, 'divInf01_02'],
                      [1, 'divInf01_03'],
                      [1, 'divInf01_04'],
                      [1, 'divInf01_05']]);

    adjustElementsInForm([['lblTotalPesoBruto', 'txtTotalPesoBruto', 9, 1],
                          ['lblTotalPesoLiquido', 'txtTotalPesoLiquido', 9, 1],
                          ['lblTotalCubagem', 'txtTotalCubagem', 12, 1],
                          ['lblNumeroVolumes', 'txtNumeroVolumes', 5, 1],
                          ['lblExpedicao', 'hrExpedicao', 80, 2],
                          ['lblConhecimento', 'txtConhecimento', 10, 3],
                          ['lblPortador', 'txtPortador', 20, 3, -3],
                          ['lblDocumentoPortador', 'txtDocumentoPortador', 15, 3, -3],
                          ['lblDocumentoFederalPortador', 'txtDocumentoFederalPortador', 15, 3, -4],
                          ['lblPlacaVeiculo', 'txtPlacaVeiculo', 8, 3, -4],
                          ['lblUFVeiculo', 'txtUFVeiculo', 3, 3, -16],
                          ['lblRNTC', 'txtRNTC', 20, 3, -2],
						  ['lbldtChegadaPortador', 'txtdtChegadaPortador', 17, 4],
						  ['lbldtEntradaSaida', 'txtdtEntradaSaida', 17, 4],
						  ['lblTempoExpedicao', 'txtTempoExpedicao', 11, 4],
						  ['lblMinutaID', 'txtMinutaID', 8, 4]], null, null, true);

    adjustElementsInForm([['lblQuoteID', 'txtQuoteID', 20, 1],
                          ['lblPessoaDealID', 'selPessoaDealID', 20, 1],
                          ['btnFindPessoa', 'btn', 21, 1]], null, null, true);
}

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg() {

}

function fg_DblClick_Prg() {
    var empresa = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransacaoID' + '\'' + '].value');

    // Itens (N�o chama o carrier para transa��es de ativo e consumo)
    if ((keepCurrFolder() == 24002) && (fg.Editable == false) &&
         (fg.Rows > 1) && (nTransacaoID != 711) && (nTransacaoID != 712) && (nTransacaoID != 715) && (nTransacaoID != 716) && (nTransacaoID != 741) && (nTransacaoID != 745))
        callCarrier();

        // Parcelas
    else if ((keepCurrFolder() == 24003) && (fg.Editable == false) &&
         (fg.Rows > 1))
        callCarrier();

        // Pedidos
    else if ((keepCurrFolder() == 24006) && (fg.Editable == false) &&
         (fg.Rows > 1))
        callCarrier();

    else if ((keepCurrFolder() == 24009) && (fg.Editable == false) && (fg.Rows > 1) &&
			  (btnIsEnableInCtrlBar('inf', 12)))
        btnBarClicked('INF', 1);

    else if ((keepCurrFolder() == 24010) && (!fg.Editable))
        sendJSCarrier(getHtmlId(), 'SHOWLANCAMENTO', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));
        // Captura o PedidoID na coluna 0 do grid
        // Manda mensagem
    else if (keepCurrFolder() == 24013)
        sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL', [fg.TextMatrix(keepCurrLineInGrid(), 0), null, null]);
}

function fg_ChangeEdit_Prg() {

}

function fg_ValidateEdit_Prg() {
    var nVariacao99999 = 0;
    // Rotina de C�lculo dos Itens do Pedido
    if ((fg.Editable) &&
         (keepCurrFolder() == 24002) &&
         (fg.EditText != '')) {
        var nVariacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'Variacao' + '\'' + '].value');

        var nPercentualSUP = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PercentualSUP' + '\'' + '].value');

        nVariacao = (nVariacao == null) ? 0 : nVariacao;
        nPercentualSUP = (nPercentualSUP == null) ? 0 : nPercentualSUP;

        if (fg.col == 1) // Campo de ProdutoID
        {
            nProdutoID = parseFloat(fg.EditText);

            // Combo de Produto
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ProdutoID*')) = '';

            // Varia��o
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'VariacaoItem')) = '0.00';

            // Valor Unit�rio
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorUnitario')) = '';

            if (!isNaN(nProdutoID)) {
                if (!((dsoCmb01Grid_01.recordset.BOF) && (dsoCmb01Grid_01.recordset.EOF))) {
                    dsoCmb01Grid_01.recordset.MoveFirst();
                    dsoCmb01Grid_01.recordset.Find('ProdutoID', nProdutoID);
                    if (!dsoCmb01Grid_01.recordset.EOF) {
                        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ProdutoID*')) = nProdutoID;
                        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorUnitario')) = roundNumber((1 + (nVariacao / 100)) * (1 + (nPercentualSUP / 100)) * (dsoCmb01Grid_01.recordset['ValorTabela'].value), 2);
                    }
                }
            }
            calcTotItem();
        }
        else if (fg.Col == 3) // Combo de Produto
        {
            // ID do Produto
            fg.TextMatrix(fg.Row, 2) = '';

            // Varia��o
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'VariacaoItem')) = '0.00';

            // Valor Unit�rio
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorUnitario')) = '';

            if (!((dsoCmb01Grid_01.recordset.BOF) && (dsoCmb01Grid_01.recordset.EOF))) {
                var sSelectedProduto = fg.ComboData;
                if (sSelectedProduto != '') {
                    dsoCmb01Grid_01.recordset.MoveFirst();
                    dsoCmb01Grid_01.recordset.Find('ProdutoID', sSelectedProduto);
                    if (!dsoCmb01Grid_01.recordset.EOF) {
                        fg.TextMatrix(fg.Row, 1) = sSelectedProduto;
                        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '_CALC_Total_11*')) = roundNumber((1 + (nVariacao / 100)) * (1 + (nPercentualSUP / 100)) * (dsoCmb01Grid_01.recordset['ValorTabela'].value), 2);
                    }
                }
            }
            calcTotItem();
        }
        else if (fg.Col == getColIndexByColKey(fg, 'Quantidade')) // Campo Quantidade
        {
            calcTotItem();
        }
        else if (fg.Col == getColIndexByColKey(fg, 'VariacaoItem')) // Campo Variacao Item (Calcula Valor Unitario)
        {
            nValorTabela = parseFloat(replaceStr(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorTabela*')), ',', '.'));
            nVariacaoItem = parseFloat(replaceStr(fg.EditText, ',', '.'));

            if ((!isNaN(nValorTabela)) &&
                 (!isNaN(nVariacaoItem)))
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorUnitario')) = roundNumber(nValorTabela * (1 + (nVariacaoItem / 100)) * (1 + (nVariacao / 100)) * (1 + (nPercentualSUP / 100)), 2);
            else {
                fg.EditText = '0.00';
                if (!isNaN(nValorTabela))
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorUnitario')) = roundNumber(nValorTabela * (1 + (nVariacao / 100)) * (1 + (nPercentualSUP / 100)), 2);
                else
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorUnitario')) = '';
            }
            calcTotItem();
        }
        else if (fg.Col == getColIndexByColKey(fg, 'ValorUnitario')) // Campo Valor Unitario (Calcula Variacao)
        {
            nValorUnitario = parseFloat(replaceStr(fg.EditText, ',', '.'));
            nValorTabela = parseFloat(replaceStr(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorTabela*')), ',', '.'));

            if ((!isNaN(nValorUnitario)) &&
                 (!isNaN(nValorTabela))) {
                nVariacao99999 = roundNumber(((nValorUnitario / (nValorTabela * (1 + (nVariacao / 100)) * (1 + (nPercentualSUP / 100)))) - 1) * 100, 2);
                if (nVariacao99999 > 999.99)
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'VariacaoItem')) = 999.99;
                else if (nVariacao99999 < -999.99)
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'VariacaoItem')) = -999.99;
                else
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'VariacaoItem')) = nVariacao99999;
            }

            else {
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'VariacaoItem')) = '0.00';
                if (!isNaN(nValorTabela))
                    fg.EditText = roundNumber(nValorTabela * (1 + (nVariacao / 100)) * (1 + (nPercentualSUP / 100)), 2);
                else
                    fg.EditText = '';
            }
            calcTotItem();
        }
    }
}

function fg_BeforeRowColChange_Prg() {

}

function fg_EnterCell_Prg() {

}

function fg_MouseUp_Prg() {

}

function fg_MouseDown_Prg() {
}

function fg_BeforeEdit_Prg() {

}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_PedidosInf_AfterEdit(Row, Col) {

    var nValorInterno = 0;
    var nValorRevenda = 0;
    var nValorUnitario = 0;
    var nColdtVigenciaServicoInicio = getColIndexByColKey(fg, 'dtVigenciaServicoInicio');
    var nColdtVigenciaServicoFim = getColIndexByColKey(fg, 'dtVigenciaServicoFim');
    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'Resultado\'].value');


    if (fg.Editable) {
        if (keepCurrFolder() == 24002) // Itens
        {
            if (Col == getColIndexByColKey(fg, 'ValorUnitario')) {
                fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

                nValorInterno = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorInterno'));
                nValorRevenda = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorRevenda'));
                nValorUnitario = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorUnitario'));


                if ((nValorUnitario < nValorRevenda) || (bResultado == false)) {
                    fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorRevenda')) = nValorUnitario;

                    if ((nValorUnitario < nValorInterno) || (bResultado == false))
                        fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorInterno')) = nValorUnitario;
                }
            }
            else if (Col == getColIndexByColKey(fg, 'ValorRevenda')) {
                fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

                nValorRevenda = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorRevenda'));
                nValorInterno = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorInterno'));
                nValorUnitario = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorUnitario'));

                if (nValorRevenda < nValorInterno)
                    fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorInterno')) = nValorRevenda;

                if (nValorUnitario < nValorRevenda)
                    fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorUnitario')) = nValorRevenda;
            }
            else if (Col == getColIndexByColKey(fg, 'ValorInterno')) {
                fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

                nValorRevenda = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorRevenda'));
                nValorInterno = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorInterno'));
                nValorUnitario = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorUnitario'));

                if (nValorRevenda < nValorInterno)
                    fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorRevenda')) = nValorInterno;

                if (nValorUnitario < nValorInterno)
                    fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorUnitario')) = nValorInterno;
            }

            if ((Col == getColIndexByColKey(fg, 'ValorUnitario')) ||
			    (Col == getColIndexByColKey(fg, 'ValorRevenda')) ||
			    (Col == getColIndexByColKey(fg, 'Quantidade'))) {
                fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorComissaoRevenda*')) = '';
                fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorComissaoRevenda*')) = '';
            }

            if ((Col == getColIndexByColKey(fg, 'ValorInterno')) ||
			    (Col == getColIndexByColKey(fg, 'ValorRevenda')) ||
			    (Col == getColIndexByColKey(fg, 'Quantidade'))) {
                fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorComissaoInterna*')) = '';
                fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorComissaoInterna*')) = '';
            }

            if (fg.col == nColdtVigenciaServicoInicio) {

                if ((verificaData(fg.TextMatrix(Row, nColdtVigenciaServicoInicio)) == false)) {
                    fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                }

                else if ((new Date(normalizeDate_DateTime(dateAdd('d', '31', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) < (new Date())) {

                    fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                    if (window.top.overflyGen.Alert('Vig�ncia in�cio n�o deve ser anterior aos �ltimos 30 dias.') == 0)
                        return null;

                    return false;
                }

                else if (fg.TextMatrix(Row, nColdtVigenciaServicoFim).length > 0) {

                    if ((new Date(normalizeDate_DateTime(dateAdd('d', '31', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) >
                            (new Date(normalizeDate_DateTime(fg.TextMatrix(Row, nColdtVigenciaServicoFim), (DATE_FORMAT == "DD/MM/YYYY" ? true : false))))) {

                        fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                        if (window.top.overflyGen.Alert('Per�odo de vig�ncia dever� ser de pelo menos 30 dias.') == 0)
                            return null;

                        return false;
                    }

                    else if ((new Date(normalizeDate_DateTime(dateAdd('yyyy', '5', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) <
                            (new Date(normalizeDate_DateTime(fg.TextMatrix(Row, nColdtVigenciaServicoFim), (DATE_FORMAT == "DD/MM/YYYY" ? true : false))))) {

                        fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                        if (window.top.overflyGen.Alert('Per�odo de vig�ncia m�xima s�o 5 anos.') == 0)
                            return null;

                        return false;
                    }
                }
            }

            if (fg.col == nColdtVigenciaServicoFim  ) {

                if ((verificaData(fg.TextMatrix(Row, nColdtVigenciaServicoFim)) == false)) {
                    fg.TextMatrix(Row, nColdtVigenciaServicoFim) = '';
                    
                }

                else if ((new Date(normalizeDate_DateTime(dateAdd('d', '1', fg.TextMatrix(Row, nColdtVigenciaServicoFim)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) < (new Date())) {

                    fg.TextMatrix(Row, nColdtVigenciaServicoFim) = '';

                    if (window.top.overflyGen.Alert('Vig�ncia fim deve ser maior que a data atual.') == 0)
                        return null;

                    return false;
                }

                else if (fg.TextMatrix(Row, nColdtVigenciaServicoInicio).length > 0) {

                    if ((new Date(normalizeDate_DateTime(dateAdd('d', '31', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) > 
                            (new Date(normalizeDate_DateTime(fg.TextMatrix(Row, nColdtVigenciaServicoFim), (DATE_FORMAT == "DD/MM/YYYY" ? true : false))))) {

                        fg.TextMatrix(Row, nColdtVigenciaServicoFim) = '';

                        if (window.top.overflyGen.Alert('Per�odo de vig�ncia dever� ser de pelo menos 30 dias.') == 0)
                            return null;

                        return false;
                    }

                    else if ((new Date(normalizeDate_DateTime(dateAdd('yyyy', '5', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) <
                            (new Date(normalizeDate_DateTime(fg.TextMatrix(Row, nColdtVigenciaServicoFim), (DATE_FORMAT == "DD/MM/YYYY" ? true : false))))) {

                        fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                        if (window.top.overflyGen.Alert('Per�odo de vig�ncia m�xima s�o 5 anos.') == 0)
                            return null;

                        return false;
                    }
                }
            }
        }
    }
}

// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD() {
    //@@
}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum 
********************************************************************/
function putSpecialAttributesInControls() {
    //@@

    // troca a pasta default para itens
    glb_pastaDefault = 24002;
}

// FINAL DE EVENTOS DO PRINTJET **************************************

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;


    if (keepCurrFolder() == 24014)
        adjustLabelsCombos(false);

    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div

    glb_BtnFromFramWork = btnClicked;

    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    setConnection(dsoCmbDynamic01);

    dsoCmbDynamic01.SQL = 'SELECT a.PessoaDealID, b.Fantasia AS PessoaDeal ' +
                            'FROM Pedidos a WITH(NOLOCK) ' +
                                'INNER JOIN Pessoas b WITH(NOLOCK) ON b.PessoaID = a.PessoaDealID ' +
                            'WHERE a.PedidoID = ' + nPedidoID;


    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic01_DSC;
    dsoCmbDynamic01.Refresh();

    
}

function dsoCmbDynamic01_DSC()
{
    clearComboEx(['selPessoaDealID']);

    var oOption = document.createElement("OPTION");
    oOption.value = 0;
    oOption.text = "";
    selPessoaDealID.add(oOption);

    if (dsoCmbDynamic01.recordset.RecordCount() > 0)
    {
        var oOption = document.createElement("OPTION");
        oOption.value = dsoCmbDynamic01.recordset['PessoaDealID'].value;
        oOption.text = dsoCmbDynamic01.recordset['PessoaDeal'].value;
        selPessoaDealID.add(oOption);

        selPessoaDealID.selectedIndex = 1;

        setLabelOfControl(lblPessoaDealID, selPessoaDealID.value);
     }

    finalOfInfCascade(glb_BtnFromFramWork);
    
}


/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    // esconde botoes especificos
    showBtnsEspecControlBar('inf', false, [1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('inf', ['', '', '', '', '', '', '', '']);

    // Embalagem/Expedicao
    if (pastaID == 24001) {
        // mostra dois botoes especificos desabilitados
        showBtnsEspecControlBar('inf', true, [1, 1, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Transportadora', 'Portador chegou', '', '']);
    }
        // Itens
    else if (pastaID == 24002) {
        // mostra quatro botoes especificos desabilitados
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Produto', 'Incluir Itens', 'Alterar itens', 'Impostos', 'Gerar Parcelas', 'Campanhas', 'Lotes', 'Fabricantes', 'Despesas', 'Dados Fiscais', 'Antecipar Entrega']);
    }
        // Parcelas
    else if (pastaID == 24003) {
        // mostra quatro botoes especificos desabilitados
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
        tipsBtnsEspecControlBar('inf', ['Atualizar Pedido', 'Recalcular Valores', 'Fechar Pedido', 'Detalhar Financeiro', 'Gerar Valores a Localizar', 'Setar forma de pagamento default', 'Incluir despesas', 'Demonstrativo de comiss�o', 'Liberar comiss�es', 'Demonstrativo de baixa de comiss�es']);
    }
        // Aprovacoes Especiais
    else if (pastaID == 24011) {
        // mostra quatro botoes especificos desabilitados
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0, 0, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Aprovar/Desaprovar', '', '', '', '', '', '', '']);
    }
        // Comiss�es
    else if (pastaID == 24012) {
        // mostra quatro botoes especificos desabilitados
        showBtnsEspecControlBar('inf', true, [0, 0, 0, 0, 0, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['', '', '', '', '', '', '', '']);
    }
        // Numeros de serie
    else if (pastaID == 24005) {
        // mostra quatro botoes especificos
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 1, 1]);
        tipsBtnsEspecControlBar('inf', ['N�meros de S�rie', 'Pesquisa', 'Vincular Asstec', 'Material com defeito', 'Detalhar Asstec']);
    }
        // Pedidos
    else if (pastaID == 24006) {
        // mostra quatro botoes especificos
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Pedido', '', '', '']);
    }
        // OPs
    else if (pastaID == 24008) {
        // mostra quatro botoes especificos
        showBtnsEspecControlBar('inf', true, [1, 1, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Gerar/sincronizar OPs', 'Detalha Ordem de Produ��o', '', '']);
    }
    else if (pastaID == 24009) // Inspecao Recebimento
    {
        // mostra quatro botoes especificos
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 0]);
        tipsBtnsEspecControlBar('inf', ['Amostra da Inspe��o', 'Procedimento', 'Detalhar Produto', '']);
    }
    else if (pastaID == 24010) // Eventos Contabeis
    {
        showBtnsEspecControlBar('inf', true, [1, 1, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Lan�amento', 'Detalhar Contas', '', '']);
    }
    else if (pastaID == 24013) //Pedidos Vinculados
    {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Pedido', '', '', '']);
    }


    // campos read-only
    var elem;
    elem = txtTotalPesoBruto;
    if (elem != null)
        elem.disabled = true;

    elem = txtTotalPesoLiquido;
    if (elem != null)
        elem.disabled = true;

    elem = txtTotalCubagem;
    if (elem != null)
        elem.disabled = true;


    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked) {
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ((btnClicked.toUpperCase()).indexOf('SUP') >= 0)
        // usario clicou botao no control bar sup
        sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
    else
        // usario clicou botao no control bar inf
        sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================

}

function adjustTemNF() {
    if (__winTimerHandler != null) {
        window.clearInterval(__winTimerHandler);
        __winTimerHandler = null;
    }

    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'chkTemNF.checked = true');
    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'chkBoxTemNFClicked()');
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID) {
    //@@
    var bTemProducao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TemProducao' + '\'' + '].value');
    var bTomacaoServico = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Servico' + '\'' + '].value');
    var bImportacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EhImportacao' + '\'' + '].value');
    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Resultado' + '\'' + '].value');
    var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransacaoID' + '\'' + '].value');

    var btn11 = 'D';
    if ((bImportacao == 1) && ((nTransacaoID == 211) || (nTransacaoID == 111)) && (sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ValorTotalPedido' + '\'' + '].value') > 0))
        btn11 = 'H';

    var btn8 = 'D';
    if (bResultado == 1)
        btn8 = 'H';


    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'margemContribuicao()');

    //Adicionado IF para ajustar campos da interface no caso de toma��o de servi�o. BJBN
    //if (bTomacaoServico)
        //sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'ajustaCampos_Tomacao()');

    var nEstadoID;
    nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

    if (btnClicked == 'SUPINCL') {
        //__winTimerHandler = window.setInterval('adjustTemNF()', 250, 'JavaScript');

        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'unlockPedidoID()');

        return null;
    }

    if (btnClicked == 'SUPOK')
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'setLabelFields()');

    // novo Estado ID foi ou nao gravado, pelo retorno da modal de
    // dados da nota fiscal, invocada no sup
    if (btnClicked == 'SUPEST') {
        var nTipoPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoPedidoID' + '\'' + '].value');
        var bTemNF = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TemNF' + '\'' + '].value');

        if (bTemNF) {
            if (((nTipoPedidoID == 601) && (nEstadoID == 27)) || ((nTipoPedidoID == 602) && (nEstadoID == 29)))
                // gravou novo EstadoID    
                if (sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_EstadoIDUpdated') == true) {
                    // seta a variavel que controla a impressao da NF
                    if (glb_bEmissorNF == true)
                        glb_bImprimeNF = true;
                    __winTimerHandler = window.setInterval('refreshSupAfterModalClose(false)', 10, 'JavaScript');
                }
        }
        // Valor a Localizar
        if (nEstadoID == 24) {
            sCor = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'CorEstado' + '\'' + '].value');

            if ((sCor != null) && (glb_bOpenModalGVL)) {
                glb_CallOpenModalVL_Timer = window.setInterval('openModalGerarValoresLocalizar()', 300, 'JavaScript');
            }

            glb_bOpenModalGVL = false;
        }
    }

    // nao altera Estado se o Pedido esta Suspenso
    var bSuspenso = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Suspenso' + '\'' + '].value');

    if (bSuspenso) {
        var strBtns = currentBtnsCtrlBarString('sup');
        var aStrBtns = strBtns.split('');
        aStrBtns[4] = 'D';
        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window, 'sup', strBtns);
    }

    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR')
          || (btnClicked == 'SUPEST')
          || (btnClicked == 'SUPOK') || (btnClicked == 'INFREFR')
          || (btnClicked == 'INFOK') || (btnClicked == 'INFCANC')) {
        if (sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selNotaFiscalID.value') == '')
            setupEspecBtnsControlBar('sup', 'HHHHHDH' + btn8 + 'DD' + btn11 + 'DDDD');

        else
            setupEspecBtnsControlBar('sup', 'HHHHHDH' + btn8 + 'DH' + btn11 + 'DDDD');
        //setupEspecBtnsControlBar('sup', 'HHHHHHDD');

        if (glb_bForceRefrInf == true) {
            glb_bForceRefrInf = false;
            __btn_REFR('inf');
        }
    }
    else
        setupEspecBtnsControlBar('sup', 'HDDDDDDDDDDDD');

    glb_bA1 = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoDireitos.recordset[' + '\'' + 'A1' + '\'' + '].value');
    glb_bA2 = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoDireitos.recordset[' + '\'' + 'A2' + '\'' + '].value');

    //Observacoes
    if (folderID == 20008) {

        if ((nEstadoID >= 24) && (glb_bA1 == 0) && (glb_bA2 == 0) && (aEmpresa[1] == 167)) {

            txtObservacoes.disabled = true;
        }
    }

    //Proprietarios/Alternativos
    else if (folderID == 20009) {

        if ((nEstadoID >= 24) && (glb_bA1 == 0) && (glb_bA2 == 0) && (aEmpresa[1] == 167)) {
            lblProprietario.disabled = true;
            lblAlternativo.disabled = true;
            selProprietario.readOnly = true;
            selAlternativo.readOnly = true;
            btnFindProprietario.disabled = true;
            btnFindAlternativo.disabled = true;
        }
    }

        // Embalagem/Expedicao
    else if (folderID == 24001) {
        var nTransportadoraID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransportadoraID' + '\'' + '].value');

        var sBtnPortador = 'D';

        if (nEstadoID <= 29)
            sBtnPortador = 'H';

        if ((nTransportadoraID != null) && (nTransportadoraID > 0))
            setupEspecBtnsControlBar('inf', 'H' + sBtnPortador + 'D');
        else if ((nTransportadoraID != null) && (nTransportadoraID > 0))
            setupEspecBtnsControlBar('inf', 'H' + sBtnPortador + 'D');
        else
            setupEspecBtnsControlBar('inf', 'D' + sBtnPortador + 'D');

        if ((nEstadoID >= 24) && (glb_bA1 == 0) && (glb_bA2 == 0) && (aEmpresa[1] == 167)) {

            txtNumeroVolumes.disabled = true;
            txtConhecimento.disabled = true;
            txtPortador.disabled = true;
            txtDocumentoPortador.disabled = true;
            txtDocumentoFederalPortador.disabled = true;
            txtPlacaVeiculo.disabled = true;
            txtUFVeiculo.disabled = true;
            txtRNTC.disabled = true;
            txtdtChegadaPortador.disabled = true;
            txtdtEntradaSaida.disabled = true;
            txtTempoExpedicao.disabled = true;
            txtMinutaID.disabled = true;
               
        }

    }
        // Botao gera parcelas
    else if ((folderID == 24002) && (btnClicked == 'SUPREFR') && (glb_GerarParcelas == true)) {
        glb_GerarParcelas = false;

        var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransacaoID' + '\'' + '].value');
        var bEhImportacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EhImportacao' + '\'' + '].value');

        if ((nTransacaoID == 211) && (bEhImportacao))
            verificaInvoice();
        else
        __winTimerHandler = window.setInterval('confirmParcelas()', 30, 'JavaScript');
    }
        // Botao fecha pedido
    else if ((folderID == 24003) && (btnClicked == 'SUPREFR') && (glb_AlertResultado > 0)) {
        
        // Verifica se o Pedido gera Resultado
        bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Resultado' + '\'' + '].value');
        if ((bResultado) && !(((glb_AlertResultado == 4) || (glb_AlertResultado == 5)) && (glb_bA1 == 0) && (glb_bA2 == 0)))
            __winTimerHandler = window.setInterval('showAlertFechaPedidos()', 30, 'JavaScript');

        // Comentado a pedido do Marcio da Gogeek        
        //else
        //__winTimerHandler = window.setInterval('showFolderItens()', 30, 'JavaScript');        
    }

    // Itens
    if (folderID == 24002) {
        enableDisableBtnsItens();
        if (glb_bFocus != null) {
            glb_bFocus = null;
            window.focus();
            fg.focus();
        }
    }
        // Parcelas
    else if (folderID == 24003) {
        enableDisableBtnsParcelas();
    }
        // Aprovacoes Especiais
    else if (folderID == 24011) {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }
        // Comiss�es
    else if (folderID == 24012) {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'DDDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }
        // Mensagens
    else if (folderID == 24004) {
        enableDisableBtnsMensagens();
    }

        // Numero de serie 
    else if (folderID == 24005) {
        // Contexto
        var cmbData = getCmbCurrDataInControlBar('sup', 1);

        // Asstec
        var bAsstec = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Asstec' + '\'' + '].value');

        // Resultado
        var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Resultado' + '\'' + '].value');

        // EhCliente
        var bEhCliente = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EhCliente' + '\'' + '].value');

        // EstadoID do Pedido
        var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		                  'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

        var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
									'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

        var sModalPesquisa = 'H';
        var sVincularAsstec = 'D';
        var sModalDefeito = 'D';
        var sDetalharAsstec = 'D';


        // Contexto == 5111 - entrada
        // botao 1 so habilita para Estado de Inspe��o (28)
        if (cmbData[1] == 5111) {
            if (nEstadoID == 28) {
                // Pedido de entrada s� habilita o botao 3 se
                // Pedido de Asstec e Retorno de Fornecedor
                if ((bAsstec == true) && (bEhCliente == false) && (fg.Rows > 1)) {
                    //setupEspecBtnsControlBar('inf', 'H' + sModalPesquisa + 'HDH');
                    sVincularAsstec = 'H';
                }
                // Devolucao de Venda ou retorno de fornecedor
                if ((bAsstec == true) && (fg.Rows > 1))// && ((bEhCliente == false) || (bResultado == true))) {
                {
                    //setupEspecBtnsControlBar('inf', 'H' + sModalPesquisa + 'DHH');
                    sModalDefeito = 'H';
                }
                // Aumento/Redu��o de Quantidade Estoque Asstec
                if ((bAsstec == true) && ((nTransacaoID == 831) || (nTransacaoID == 835))) {
                    sVincularAsstec = 'H';
                }

                if (bAsstec == true) {
                    //setupEspecBtnsControlBar('inf', 'H' + sModalPesquisa + 'DDH');
                    sDetalharAsstec = 'H';
                }

                setupEspecBtnsControlBar('inf', 'H' + sModalPesquisa + sVincularAsstec + sModalDefeito + sDetalharAsstec);

            }
            else if ((bAsstec == true) && (fg.Rows > 1))
                setupEspecBtnsControlBar('inf', 'H' + sModalPesquisa + 'DDH');
            else
                setupEspecBtnsControlBar('inf', 'H' + sModalPesquisa + 'DDD');
        }
            // Contexto == 5112 - saida
            // botao 1 so habilita para Estado de Embalagem (25)
        else if (cmbData[1] == 5112) {
            if (nEstadoID == 25) {
                // Pedido de Saida s� habilita o botao 3 se Pedido de Asstec 
                if ((bAsstec == true) && (fg.Rows > 1)) {
                    setupEspecBtnsControlBar('inf', 'H' + sModalPesquisa + 'HDH');
                }
                else {
                    setupEspecBtnsControlBar('inf', 'H' + sModalPesquisa + 'DDD');
                }
            }
            else if ((nEstadoID >= 25) && (bAsstec == true) && (fg.Rows > 1))
                setupEspecBtnsControlBar('inf', 'H' + sModalPesquisa + 'DDH');
            else
                setupEspecBtnsControlBar('inf', 'H' + sModalPesquisa + 'DDD');
        }
    }

        // Pedidos
    else if (folderID == 24006) {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }
        // OP
    else if (folderID == 24008) {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HHDD');
        else
            setupEspecBtnsControlBar('inf', 'HDDD');
    }
        // Inspecao Recebimento
    else if (folderID == 24009) {
        enableDisableBtnsInspecaoRecebimento();
    }
    else if ((folderID == 24010) && (fg.Rows > 1)) {
        setupEspecBtnsControlBar('inf', 'HHDD');
    }
    else if ((folderID == 24013) && (fg.Rows > 1)) {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    else if (folderID == 24014) {

        if ((nEstadoID >= 25) && (nEstadoID != 33)) {
            var strBtns = currentBtnsCtrlBarString('inf');
            var aStrBtns = strBtns.split('');
            aStrBtns[3] = 'D';
            strBtns = aStrBtns.toString();
            re = /,/g;
            strBtns = strBtns.replace(re, '');
            adjustControlBarEx(window, 'inf', strBtns);
        }
        adjustLabelsCombos(true);
    }

    // Impressao da NF
    var bNFeFSDA;
    bNFeFSDA = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'NFeFSDA\'].value');

    if ((btnClicked == 'SUPREFR') && (glb_bImprimeNF)) {
        // A funcao de impressao da nota fiscal se encontra no arquivo notafiscal.js
        // no diretorio modalpages do form de notas fiscais
        glb_bPrintingNF = false;

        if (bNFeFSDA > 0) {
            var _retConf = window.top.overflyGen.Confirm('Inserir 2 formul�rios de seguran�a na impressora (alimenta��o manual) para impress�o do FS-DA.\n\nImprimir FS-DA?');

            if (_retConf == 0)
                return null;
            else if (_retConf == 1) {
                glb_CallPrintNotaFiscal_Timer = window.setInterval('callPrintNFe()', 500, 'JavaScript');
            }

        }
        else {
            glb_bImprimeNF = false;
            var _retConf = window.top.overflyGen.Confirm('Imprime Nota Fiscal?');

            if (_retConf == 0 || _retConf == 2) {              
                return null;             
            }
            else if (_retConf == 1) {
                glb_bPrintingNF = true;
                glb_CallPrintNotaFiscal_Timer = window.setInterval('callPrintNF()', 500, 'JavaScript');
            }
            /* Desabilitado impress�o da Lista de Embalagem no avan�o do estado de Faturamento para Expedi��o. Verificado com Camilo Rodrigues e Eder Silva que
               essa fun��o n�o � mais utilizada - MTH 07/12/2017
            else if (_retConf == 2) {
                if (callPrintBoleto() == false)
                    glb_PrintListaOrOP_Timer = window.setInterval('printListaEmbalagemOrOP()', 300, 'JavaScript');
            }*/
        }
    }

    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'changeTransacaoCmbTitle()');
}

function callOpenModalPrint() {
    if (glb_CallOpenModalPrint_Timer != null) {
        window.clearInterval(glb_CallOpenModalPrint_Timer);
        glb_CallOpenModalPrint_Timer = null;
    }

    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bCotacaoToProposta = false;');
    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'openModalPrint(true);');
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {
    if (btnClicked.id == 'btnFindPessoa')
        openModalPessoa();

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso) {
    if (folderID == 24003) {
        if (!((dso02GridLkp.recordset.BOF) && (dso02GridLkp.recordset.EOF))) {
            dso02GridLkp.recordset.MoveFirst();
            fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'FormaPagamentoID')) = dso02GridLkp.recordset['FormaPagamentoID'].value;
            // coloca foco no campo Prazo
            fg.Col = getColIndexByColKey(fg, 'PrazoPagamento');
        }
        else
            // coloca foco no campo Forma Pagamento
            fg.Col = getColIndexByColKey(fg, 'FormaPagamentoID');
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {

    var strBtns = currentBtnsCtrlBarString('inf');

    if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 24013)) {
        sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL', [fg.TextMatrix(keepCurrLineInGrid(), 0), null, null]);
    }

    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 24011)) {
        aprovacaoEspecial();
    } else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 24010)) {
        var empresa = getCurrEmpresaData();
        sendJSCarrier(getHtmlId(), 'SHOWLANCAMENTO', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));
    }
    else if ((controlBar == 'INF') && (btnClicked == 2) && (keepCurrFolder() == 24010)) {
        openModalDetalharContas();
    }
        // Demonstrativo de baixa de comiss�es - DMC 04/08/2011
    else if ((controlBar == 'INF') && (btnClicked == 10) && (keepCurrFolder() == 24003)) {
        openModalBaixaComissoes();
    }
    // Antecipar Entrega
    else if ((controlBar == 'INF') && (btnClicked == 11) && (keepCurrFolder() == 24002))
    {
        var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
        var bPermitirAnteciparEntrega = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PermitirAnteciparEntrega' + '\'' + '].value');

        var sError = '';


        if((nEstadoID != 33) && (nEstadoID != 24))
            sError += 'Pedido precisa estar nos estados A ou M.\n';

        if (bPermitirAnteciparEntrega != 1)
            sError += 'Transa��o n�o permite antecipar entrega.';

        if (sError == '')
            verificaPedidosVinculados();
        else
            if (window.top.overflyGen.Alert(sError) == 0)
                return null;
    }
    else {
        glb_nBtnNotEspecific = btnClicked;
        verifyCurrEstadoID();
    }
}

/********************************************************************
Funcao criada pelo programador.
Continuacao da funcao btnBarClicked - Usuario clicou botao especifico da barra.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function btnBarClicked_Continue() {
    var btnClicked = glb_nBtnNotEspecific;
    var empresaData = getCurrEmpresaData();

    //Alimenta variavel de toma��o de servico e referencia
    var bTomacaoServico = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Servico' + '\'' + '].value');
    var bProdutoServico = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ProdutoServico' + '\'' + '].value'); //Joao
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
    var bProdutoServicoEstadual = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ServicoEstadual' + '\'' + '].value'); //FRG 22/05/2015

    glb_bNCM = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'NCM' + '\'' + '].value'); //SGP - Ativo e Consumo

    // Embalagem/Expedicao
    if (keepCurrFolder() == 24001) {
        detalhaTransportadora(btnClicked);
    }
        // Itens
    else if (keepCurrFolder() == 24002) {
        // Produtos
        if (btnClicked == 2) {

            if (sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Pedido_EhCotador' + '\'' + '].value') == 1) {
                if (window.top.overflyGen.Alert('N�o � permitido incluir itens em um pedido gerado pelo cotador.') == 0)
                    return null;
            }
            else {
                //Verifica se abre janela de incluir itens ou servi�os.
                if ((bTomacaoServico && !bProdutoServico) || (glb_bNCM) || (bProdutoServicoEstadual))
                    openModalIncluiServicos();
                else
                    openModalIncluiItens();
            }
        }
            // Impostos
        else if (btnClicked == 4)
            openModalImpostos();
            // Gera Parcelas
        else if (btnClicked == 5) {
            setupEspecBtnsControlBar('inf', 'DDDD');
            glb_GerarParcelas = true;
            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                      '__btn_REFR(\'sup\')');
        }
            // Detalhar Produto
        else if (btnClicked == 1) {
            callCarrier();
        }
            // Altera Itens
        else if (btnClicked == 3)
            openModalItens();
            // Associar Campanhas
        else if (btnClicked == 6)
            openModalCampanhas();
            // Fabricantes
        else if (btnClicked == 8)
            openModalFabricantes();
            // Altera Itens
        else if (btnClicked == 7) {
            if (nEstadoID == 21) {
                openModalAssociarLotes();
            }
            else
                if (window.top.overflyGen.Alert('Associa��o de lotes somente em C.') == 0)
                    return null;
        }   // Despesas
        else if (btnClicked == 9)
            openModalDespesas();
    }
        // Parcelas
    else if ((keepCurrFolder() == 24003)) {
        // Atualizar Pedido
        if (btnClicked == 1) {
            fechaPedidoParcelas(5);
        }
            // Recalcular Valores
        else if (btnClicked == 2) {
            fechaPedidoParcelas(3);
        }
            // Fecha Pedido
        else if (btnClicked == 3) {
            fechaPedidoParcelas(4);
        }
            // Detalhar Financeiro
        else if (btnClicked == 4) {
            callCarrier();
        }
        else if (btnClicked == 5) {
            openModalGerarValoresLocalizar();
        }
        else if (btnClicked == 6) {
            openModalSetarFormaPagamento();
        }
        else if (btnClicked == 7) {
            openModalParcelasDiversos();
        }
        else if (btnClicked == 8) {
            openModalComissaoVendas();
        }
        else if (btnClicked == 9) {
            liberarComissao();
        }
    }
        // Numero de Serie
    else if (keepCurrFolder() == 24005) {
        // Carrega modal de entrada ou saida, de codigo de barras
        if (btnClicked == 1) {
            openModalNumSerie();
        }
            // Carrega modal de pesquisa de pasta
        else if (btnClicked == 2)
            openModalPesqNumSerie();
            // Carrega modal de Vincular Asstec ou executa a procedure de
            // atualizacao de Asstec
        else if (btnClicked == 3)
            execSP_Pedido_AsstecNumeroSerie();
        else if (btnClicked == 4)
            openModalMaterialDefeito();
        else if (btnClicked == 5)
            callCarrier();
    }
        // Pedidos
    else if (keepCurrFolder() == 24006)
        callCarrier();
        // Ordens de Producao    
    else if (keepCurrFolder() == 24008) {
        if (btnClicked == 1)
            geraOrdemProducao();
            // Detalha OP	
        else if (btnClicked == 2)
            callCarrier();
    }
        // Inspecao Recebimento
    else if (keepCurrFolder() == 24009) {
        if (btnClicked == 1)
            openModalInspecaoRecebimento();
        else if (btnClicked == 2)
            window.top.openModalControleDocumento('I', '', 743, null, null, 'T');
        else if (btnClicked == 3)
            callCarrier();
    }
}

/********************************************************************
Funcao criada pelo programador.
abre modal lotes
********************************************************************/
function openModalAssociarLotes() {

    var htmlPath;
    var nTotalPedido = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'ValorTotalPedido' + '\'' + '].value');

    var strPars = new String();

    // mandar os parametros para o servidor
    strPars = '?nPedidoID=' + escape(getCurrDataInControl('sup', 'txtRegistroID'));
    strPars += '&nTotalPedido=' + escape(nTotalPedido);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalassociarlotes.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 540));
}
/********************************************************************
Funcao criada pelo programador.
Abre modal para pesquisa inclusao de itens no grid de itens

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalInspecaoRecebimento() {
    var nWidth = 671 + 80 + 6 + 3 + 11;
    var nHeight = 484 - 24;

    var htmlPath;
    var strPars = new String();
    var nPedRIRID = getCellValueByColKey(fg, 'PedRIRID*', fg.Row);
    var nProdutoID = getCellValueByColKey(fg, 'ProdutoID*', fg.Row);
    var sEstadoProduto = getCellValueByColKey(fg, '^PedRIRID^dso02GridLkp^PedRIRID^CurrEst*', fg.Row);
    var sProduto = getCellValueByColKey(fg, '^PedRIRID^dso03GridLkp^PedRIRID^Produto*', fg.Row);
    var nPedidoEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
    var nQuantidade = getCellValueByColKey(fg, 'QuantidadeAmostra*', fg.Row);

    // mandar os parametros para o servidor
    strPars = '?nPedRIRID=' + escape(nPedRIRID);
    strPars += '&nProdutoID=' + escape(nProdutoID);
    strPars += '&sEstadoProduto=' + escape(sEstadoProduto);
    strPars += '&sProduto=' + escape(sProduto);
    strPars += '&nPedidoEstadoID=' + escape(nPedidoEstadoID);
    strPars += '&nQuantidade=' + escape(nQuantidade);

    // carregar modal - nao faz operacao de banco no carregamento           
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalamostrainspecao.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
- Avancar, retroagir, incluir, alterar, estado, 
excluir, OK, cancelar refresh, anterior e proximo. 
(Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    if ((btnClicked == 'INFALT') && keepCurrFolder() == 24001) {
        glb_Rntc = txtRNTC.value;

        if (glb_Rntc == '')
            glb_Rntc = null;
    }

    if ((btnClicked == 'INFOK') && keepCurrFolder() == 24001) {
        var sObservacao;

        sObservacao = dso01JoinSup.recordset['Observacao'].value;

        if (((txtRNTC.value != '') && (txtRNTC.value != null)) || ((sObservacao != '') && (sObservacao != null)))
            replicaRNTC();

        txtUFVeiculo.value = (txtUFVeiculo.value).toUpperCase();
    }
    // btn OK e btn EXCL, para a automacao para verificar
    // se estadoID ainda nao foi mudado e permite a operacao
    if ((btnClicked == 'INFOK') || (btnClicked == 'INFEXCL')) {
        verifyCurrEstadoID();
        return true;
    }


    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    if (idElement.toUpperCase() == 'MODALCOMISSAOVENDASHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALDESPESASHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALANTECIPARENTREGAHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, '__btn_REFR(\'sup\')');

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALIMPORTACAOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, '__btn_REFR(\'sup\')');

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALPARCELASDIVERSASHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, '__btn_REFR(\'sup\')');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALSETARFORMAPAGAMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALGERAVALORLOCALIZARHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    else if (idElement.toUpperCase() == 'MODALMATERIALDEFEITOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status

            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');

            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

        // Janelas de pesquisa para entrada e saida de numeros de series
    else if (idElement.toUpperCase() == 'MODALNUMSERIEHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
            // Botao nao ativo, funcao aqui apenas para compatibilidade
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    else if (idElement.toUpperCase() == 'MODALIMPOSTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            window.focus();
            fg.focus();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
            // Botao nao ativo, funcao aqui apenas para compatibilidade
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            window.focus();
            fg.focus();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    else if (idElement.toUpperCase() == 'MODALINCLUIITENSHTML') {
        // Botao nao ativo, funcao aqui apenas para compatibilidade
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
            // Botao ativo
            // se param2 == true, refrescar o grid de itens
            // pois o pedido foi alterado
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            window.focus();
            fg.focus();

            if (param2)
                __winTimerHandler = window.setInterval('refreshSupAfterModalClose(true)', 10, 'JavaScript');

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

        // Janelas de pesquisa para entrada e saida de numeros de series
    else if (idElement.toUpperCase() == 'MODALVINCULAASSTECHTML') {
        if (param1 == 'OK') {
            // funcao aqui declarada apenas para compatibilidade
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
            // Botao nao ativo, funcao aqui apenas para compatibilidade
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

        // Janelas de pesquisa para entrada e saida de numeros de series
    else if (idElement.toUpperCase() == 'MODALAMOSTRAINSPECAOHTML') {
        if (param1 == 'OK') {
            // funcao aqui declarada apenas para compatibilidade
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
            // Botao nao ativo, funcao aqui apenas para compatibilidade
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

        // Modal bloqueto de cobranca
    else if (idElement.toUpperCase() == 'MODALBLOQUETOCOBRANCAHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
        }

        //glb_PrintListaOrOP_Timer = window.setInterval('printListaEmbalagemOrOP()', 300, 'JavaScript');
        glb_bImprimeNF = false;
        return 0;
    }

        // Modal Campanhas
    else if (idElement.toUpperCase() == 'MODALCAMPANHASHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
        // Modal Fabricantes
    else if (idElement.toUpperCase() == 'MODALFABRICANTESHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
        // Modal Itens
    else if (idElement.toUpperCase() == 'MODALITENSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');
            return 0;
        }
    }
        // Modal Pesquisa de Num Serie
    else if (idElement.toUpperCase() == 'MODALPESQNUMSERIEHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
    }
        // Modal Eventos Contabeis
    else if (idElement.toUpperCase() == 'MODALEVENTOSCONTABEIS_CONTASHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
    }
        // Modal demonstrat. de baixa de comiss�es DMC 04/08/2011
    else if (idElement.toUpperCase() == 'MODALBAIXACOMISSOESHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
        // Modal Inclui Servicos Tomacao. BJBN
    else if (idElement.toUpperCase() == 'MODALINCLUISERVICOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
    }
        //Modal Lotes
    else if (idElement.toUpperCase() == 'MODALASSOCIARLOTESHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
        // Modal Inclui Referencia. RRF
    else if (idElement.toUpperCase() == 'MODALREFERENCIAHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALPESSOAHTML') {
        if (param1 == 'OK') {

            clearComboEx(['selPessoaDealID']);

            var oOption = document.createElement("OPTION");
            oOption.value = 0;
            oOption.text = "";
            selPessoaDealID.add(oOption);


            var oOption = document.createElement("OPTION");
                oOption.value = param2[0];
                oOption.text = param2[1];
                selPessoaDealID.add(oOption);
                
                selPessoaDealID.selectedIndex = 1;

                restoreInterfaceFromModal();

                setLabelOfControl(lblPessoaDealID, selPessoaDealID.value);

                return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }



}

/********************************************************************
Funcao criada pelo programador.
Executa um Refresh no INF
           
Parametros:     
nenhum

Retorno:
nenhum
********************************************************************/
function refreshAfterModalClose() {
    if (__winTimerHandler != null) {
        window.clearInterval(__winTimerHandler);
        __winTimerHandler = null;
    }

    __btn_REFR('inf');
}

/********************************************************************
Funcao criada pelo programador.
Executa um Refresh no SUP e/ou INF
           
Parametros:     
bRefreshInf - false- executa Refresh no SUP
- true - executa refresh no SUP e no INF  

Retorno:
nenhum
********************************************************************/
function refreshSupAfterModalClose(bRefreshInf) {
    if (__winTimerHandler != null) {
        window.clearInterval(__winTimerHandler);
        __winTimerHandler = null;
    }

    glb_bFocus = true;

    if (bRefreshInf) {
        glb_bForceRefrInf = true;
    }

    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                          '__btn_REFR(\'sup\')');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid(folderID) {
    if (keepCurrFolder() == 24008)
        setupEspecBtnsControlBar('inf', 'DDDD');
    else if (keepCurrFolder() == 24009)
        setupEspecBtnsControlBar('inf', 'DHDD');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid(folderID) {
    // Embalagem/Expedicao
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
    if (folderID == 24001);
    {
        txtTotalPesoBruto.disabled = true;
        txtTotalPesoLiquido.disabled = true;
        txtTotalCubagem.disabled = true;
        txtdtChegadaPortador.disabled = true;
        txtdtEntradaSaida.disabled = true;
        txtTempoExpedicao.disabled = true;
        txtMinutaID.disabled = true;

        if ((nEstadoID == 29) || (nEstadoID == 31) || (nEstadoID == 32))
            txtRNTC.readOnly = false;
        else txtRNTC.readOnly = true;

    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID) {
    // Numero de Serie
    if (folderID == 24005) {
        glb_scmbKeyPesqSel = 'Produto';
        glb_sArgument = '';
        glb_sFilterPesq = '';
    }
    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder) {
    //@@ retornar null para prosseguir a automacao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra infeior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID) {
    // Pasta de LOG, Numeros de Serie, Comissoes Read Only
    if ((folderID == 20010) || (folderID == 24005) ||
		 (folderID == 24006) || (folderID == 24008) || (folderID == 24010)
		 || (folderID == 24013)) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
        // Pasta Itens
    else if (folderID == 24002) {
        // botao Incluir desabilitado
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    else {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}

function calcTotItem(nLine) {
    var nQtd, nValUnit;
    var lIsEditing = (nLine == null);
    nLine = (nLine == null ? fg.Row : nLine);

    if ((fg.Col == getColIndexByColKey(fg, 'Quantidade')) && (lIsEditing))
        nQtd = parseFloat(fg.EditText);
    else
        nQtd = parseFloat(fg.ValueMatrix(nLine, getColIndexByColKey(fg, 'Quantidade')));

    if ((fg.Col == getColIndexByColKey(fg, 'ValorUnitario')) && (lIsEditing))
        nValUnit = parseFloat(replaceStr(fg.EditText, ',', '.'));
    else
        nValUnit = parseFloat(replaceStr(fg.TextMatrix(nLine, getColIndexByColKey(fg, 'ValorUnitario')), ',', '.'));

    if ((!isNaN(nQtd)) && (!isNaN(nValUnit)))
        fg.TextMatrix(nLine, getColIndexByColKey(fg, '_CALC_Total_11*')) = roundNumber(nQtd * nValUnit, 2);
    else
        fg.TextMatrix(nLine, getColIndexByColKey(fg, '_CALC_Total_11*')) = '';
}

/********************************************************************
Funcao criada pelo programador.
Habilita desabilita botoes da pasta Itens
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function enableDisableBtnsItens() {
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'Resultado' + '\'' + '].value');
    var bImportacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'EhImportacao' + '\'' + '].value');
    var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'TransacaoID' + '\'' + '].value');

    var btnImportacao = 'D';
    if (bImportacao)
        btnImportacao = 'H';

    // Pasta de Itens e Read Only de Pedido n�o estiver em cotacao
    if (nEstadoID != 21) {
        var strBtns = defaultGridModeEx(fg);
        var aStrBtns = strBtns.split('');

        aStrBtns[3] = 'D';
        aStrBtns[4] = 'D';
        aStrBtns[5] = 'D';
        aStrBtns[6] = 'D';
        aStrBtns[8] = 'H';
        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window, 'inf', strBtns);

        // Desabilita os botoes especificos inferiores
        if ((fg.rows > 1) && (fg.Row > 0)) {
            setupEspecBtnsControlBar('inf', 'HDDHDHHHHHD'); //DHDHHHHHD

            if (nEstadoID > 21)
                setupEspecBtnsControlBar('inf', 'HDDHDHHHHHH');

            if ((nEstadoID == 25 || nEstadoID == 33) && (nTransacaoID == 215))
                setupEspecBtnsControlBar('inf', 'HDHHDHHHHHH');

            if ((nEstadoID >= 26) && (nEstadoID != 33))
                setupEspecBtnsControlBar('inf', 'HDDHDHDHHHD');
        }
        else
            setupEspecBtnsControlBar('inf', 'DDDDDDDDDDD');
    }
    else {
        var nNumeroMaxItens = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoEstaticCmbs.recordset[' + '\'' + 'fldID' + '\'' + '].value');
        var bTemNF = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'chkTemNF.checked');

        // N�o deixa colocar mais do que o numero maximo de itens    
        if (bTemNF) {
            if (fg.rows >= (nNumeroMaxItens + 2)) {
                var strBtns = defaultGridModeEx(fg);
                var aStrBtns = strBtns.split('');
                aStrBtns[4] = 'D';
                strBtns = aStrBtns.toString();
                re = /,/g;
                strBtns = strBtns.replace(re, '');
                adjustControlBarEx(window, 'inf', strBtns);
            }
        }

        var nValorTotalPedido = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ValorTotalPedido' + '\'' + '].value');


        if (nValorTotalPedido == null)
            nValorTotalPedido = 0;

        // Habilita os botoes especificos inferiores
        if ((fg.rows > 1) && (fg.Row > 0)) {
            if (bResultado)
                setupEspecBtnsControlBar('inf', 'HHHHHHHHHDH');
            else
                setupEspecBtnsControlBar('inf', 'HHHHHHHHHDH');
        }
        else
            setupEspecBtnsControlBar('inf', 'DHDDDDDDDDD');
    }
}

/********************************************************************
Funcao criada pelo programador.
Habilita desabilita botoes da pasta de Parcelas
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function enableDisableBtnsParcelas() {
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
    var nTipoPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoPedidoID' + '\'' + '].value');
    var sBtnValoresaLocalizar = 'D';
    var sBtnLiberaComissao = 'D';
    var sBtnBaixaComissoes = 'H';
    var sBtnSetFormaPagamento = (fg.Row > 0 ? 'H' : 'D');

    sBtnValoresaLocalizar = (fg.Row > 0 ? 'H' : 'D');
    sBtnBaixaComissoes = (fg.Row > 0 ? 'H' : 'D');

    sBtnLiberaComissao = (fg.Row > 0 && nEstadoID > 26 ? 'H' : 'D');

    // Pasta de Parcelas � Read Only de Pedido n�o estiver em cotacao
    if (nEstadoID != 21) {
        // Desabilita os botoes inferiores
        if ((fg.rows > 1)) {
            if (nEstadoID >= 22 && nEstadoID <= 26)
                setupEspecBtnsControlBar('inf', 'HDDH' + sBtnValoresaLocalizar + sBtnSetFormaPagamento + 'DH' + sBtnLiberaComissao + sBtnBaixaComissoes);
            else
                setupEspecBtnsControlBar('inf', 'DDDH' + sBtnValoresaLocalizar + sBtnSetFormaPagamento + 'DH' + sBtnLiberaComissao + sBtnBaixaComissoes);
        }
        else {
            setupEspecBtnsControlBar('inf', 'DDDD' + sBtnValoresaLocalizar + sBtnSetFormaPagamento + 'DD' + sBtnLiberaComissao + sBtnBaixaComissoes);
        }
    }
    else {
        // Botao Fecha Pedido
        var nValorTotalPedido = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ValorTotalPedido' + '\'' + '].value');

        // se acabou de ocorrer uma alteracao no grid de Parcelas ou de Pedidos
        // e o campo do superior esta desatualizado

        if (nValorTotalPedido == null)
            nValorTotalPedido = 0;


        if ((nValorTotalPedido > 0) && (fg.rows > 1)) {
            if (glb_bFechado == false)
                setupEspecBtnsControlBar('inf', 'HHHD' + sBtnValoresaLocalizar + sBtnSetFormaPagamento + 'HH' + sBtnLiberaComissao + sBtnBaixaComissoes);
            else
                setupEspecBtnsControlBar('inf', 'HHDH' + sBtnValoresaLocalizar + sBtnSetFormaPagamento + 'HH' + sBtnLiberaComissao + sBtnBaixaComissoes);
        }
        else {
            setupEspecBtnsControlBar('inf', 'DDDD' + sBtnValoresaLocalizar + sBtnSetFormaPagamento + 'DD' + sBtnLiberaComissao + sBtnBaixaComissoes);
        }
    }
}

/********************************************************************
Funcao criada pelo programador.
Habilita desabilita botoes da pasta de Parcelas
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function enableDisableBtnsInspecaoRecebimento() {
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

    // Pasta de Parcelas � Read Only de Pedido n�o estiver em cotacao
    if (nEstadoID != 28) {
        var strBtns = defaultGridModeEx(fg);
        var aStrBtns = strBtns.split('');
        aStrBtns[2] = 'D';
        aStrBtns[3] = 'D';
        aStrBtns[5] = 'D';
        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window, 'inf', strBtns);

    }

    if (fg.Rows > 1)
        setupEspecBtnsControlBar('inf', 'HHHD');
    else
        setupEspecBtnsControlBar('inf', 'DHDD');
}

function enableDisableBtnsMensagens() {
    var bTemNotaFiscal = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TemNF' + '\'' + '].value');
    var nNotaFiscal = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'NotaFiscalID' + '\'' + '].value');

    // Pasta de Parcelas � Read Only de Pedido n�o estiver em cotacao
    if (!(bTemNotaFiscal && (nNotaFiscal == null))) {
        var strBtns = defaultGridModeEx(fg);
        var aStrBtns = strBtns.split('');
        aStrBtns[2] = 'D';
        aStrBtns[3] = 'D';
        aStrBtns[5] = 'D';
        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window, 'inf', strBtns);
    }
}

/********************************************************************
Funcao criada pelo programador.
Mostra o confirm para gerar as parcelas
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function confirmParcelas() {
    if (__winTimerHandler != null) {
        window.clearInterval(__winTimerHandler);
        __winTimerHandler = null;
    }

    openConfirmParcelas();
}

/********************************************************************
Funcao criada pelo programador.
Abre confirm de Gerar Parcelas

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openConfirmParcelas() {
    var nTotalPedido = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'ValorTotalPedido' + '\'' + '].value');

    var nParcelas = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'NumeroParcelas' + '\'' + '].value');

    var sPrestacao = padNumReturningStr(roundNumber((nTotalPedido / nParcelas), 2), 2);

    var _retConf = window.top.overflyGen.Confirm('Gerar ' + nParcelas + ' parcela(s) de $' + sPrestacao + '?');

    if (_retConf == 0)
        return null;
    else if (_retConf == 1)
        fillParcelas();
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de entrada de numero de serie

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalNumSerie() {
    var htmlPath;
    var strPars = new String();
    // Contexto
    var cmbData = getCmbCurrDataInControlBar('sup', 1);
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
    var bTemProducao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TemProducao' + '\'' + '].value');

    // mandar os parametros para o servidor
    strPars = '?nPedidoID=' + escape(getCurrDataInControl('sup', 'txtRegistroID'));
    strPars += '&nOrdemProducaoID=' + escape(0);
    strPars += '&nContextoID=' + escape(cmbData[1]);
    strPars += '&nEstadoID=' + escape(nEstadoID);
    strPars += '&nTemProducao=' + escape(bTemProducao ? '1' : '0');
    strPars += '&sDATE_SQL_PARAM=' + escape(DATE_SQL_PARAM);

    strPars += '&nCurrUserID=' + escape(getCurrUserID());

    // carregar modal - nao faz operacao de banco no carregamento              
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalnumserie.asp' + strPars;
    showModalWin(htmlPath, new Array(780, 460));
}

/********************************************************************
Funcao criada pelo programador.
Preenche o grid de Parcelas

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function fillParcelas() {
    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');
    var bPedidoComplemento = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoComplemento' + '\'' + '].value');
    var nFormaPagamentoID = null;

    if (bPedidoComplemento == 1) {
        var _retConf = window.top.overflyGen.Confirm('Imposto pago pelo vendedor?');

        if (_retConf == 2)
            nFormaPagamentoID = 1037;
        else if (_retConf == 1)
            nFormaPagamentoID = 1038;
    }
    lockInterface(true);

    var strPars = '?';
    strPars += 'nPedidoID=' + escape(nPedidoID);
    strPars += '&nFormaPagamentoID=' + escape(nFormaPagamentoID);

    dsoParcelas.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/fillparcelas.aspx' + strPars;
    dsoParcelas.ondatasetcomplete = fillParcelas_DSC;
    dsoParcelas.Refresh();
}

/********************************************************************
Funcao criada pelo programador.
Retorno da funcao que preenche as parcelas

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function fillParcelas_DSC() {
    //destrava a Interface
    lockInterface(false);

    var folderChanged = false;

    // Executa refresh no inf
    showBtnsEspecControlBar('inf', false, [0, 0, 1, 0]);

    folderChanged = selOptByValueOfSelInControlBar('inf', 1, 24003);

    if (folderChanged) {
        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      'fillCmbsFiltInf_DSC(' + 24003 + ')');
    }

    __btn_REFR('inf');
}

/********************************************************************
Funcao criada pelo programador.
Abre modal com impostos do pedido

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalImpostos() {
    var htmlPath;
    var nTotalPedido = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'ValorTotalPedido' + '\'' + '].value');

    var strPars = new String();

    // mandar os parametros para o servidor
    strPars = '?nPedidoID=' + escape(getCurrDataInControl('sup', 'txtRegistroID'));
    strPars += '&nTotalPedido=' + escape(nTotalPedido);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalimpostos.asp' + strPars;
    //showModalWin(htmlPath, new Array(998, 575));
    showModalWin(htmlPath, new Array(1000, 539));

    //770, 450
}

/********************************************************************
Funcao criada pelo programador.
Abre modal com impostos do pedido

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalComissaoVendas() {
    var htmlPath;
    var strPars = new String();

    // mandar os parametros para o servidor
    strPars = '?nPedidoID=' + escape(getCurrDataInControl('sup', 'txtRegistroID'));

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalcomissaovendas.asp' + strPars;
    showModalWin(htmlPath, new Array(440, 370));
}


/********************************************************************
Funcao criada pelo programador.
Libera comiss�o da revenda ou interna

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function liberarComissao() {
    var nTipoParcelaID = getCellValueByColKey(fg, 'TipoParcelaID', fg.Row);
    var nLiberada = 0;
    var nPedParcelaID = 0;

    if ((fg.Rows <= 2) || (fg.Row <= 1) || !((nTipoParcelaID == 641) || (nTipoParcelaID == 642))) {
        if (window.top.overflyGen.Alert('Selecione uma parcela de comiss�o') == 0)
            return null;

        return null;
    }

    lockInterface(true);

    nLiberada = getCellValueByColKey(fg, 'Liberada*', fg.Row);
    nPedParcelaID = getCellValueByColKey(fg, 'PedParcelaID', fg.Row);

    nLiberada = (nLiberada == 1 ? 0 : 1);

    setConnection(dsoLiberarComissao);
    dsoLiberarComissao.SQL = 'SET NOCOUNT ON DECLARE @PodeLiberar BIT SET @PodeLiberar = 1 ' +
                                'IF EXISTS(SELECT 1 FROM Financeiro_Comissoes WITH(NOLOCK) WHERE PedParcelaID = ' + nPedParcelaID + ') ' +
                                '   SET @PodeLiberar = 0 ' +
                                'ELSE ' +
                                    'UPDATE Pedidos_Parcelas SET Liberada = ' + nLiberada + ' WHERE PedParcelaID = ' + nPedParcelaID + ' ' +
                                'SELECT @PodeLiberar AS PodeLiberar ' +
                                'SET NOCOUNT OFF';
    dsoLiberarComissao.ondatasetcomplete = liberarComissao_DSC;
    dsoLiberarComissao.Refresh();
}

function liberarComissao_DSC() {
    var nPodeLiberar = 1;

    if (!((dsoLiberarComissao.recordset.BOF) && (dsoLiberarComissao.recordset.EOF))) {
        if (dsoLiberarComissao.recordset['PodeLiberar'].value == false)
            nPodeLiberar = 0;
    }

    lockInterface(false);

    if (nPodeLiberar == 0) {
        if (window.top.overflyGen.Alert('Comiss�o j� esta associada a um pedido/financeiro.') == 0)
            return null;
    }
    else
        __btn_REFR('inf');
}

/********************************************************************
Funcao criada pelo programador.
Abre modal com impostos do pedido

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalDespesas() {
    if (fg.Row <= 0)
        return;

    var htmlPath;
    var nTotalPedido = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'ValorTotalPedido' + '\'' + '].value');
    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    var strPars = new String();

    // mandar os parametros para o servidor
    strPars = '?nPedidoID=' + escape(nPedidoID);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modaldespesas.asp' + strPars;
    showModalWin(htmlPath, new Array(690, 450));
}

/********************************************************************
Funcao criada pelo programador.
Abre modal com itens do pedido para serem antecipados

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalAnteciparEntrega() {
    if (fg.Row <= 0)
        return;

    var htmlPath;

    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    var strPars = new String();

    // mandar os parametros para o servidor
    strPars = '?nPedidoID=' + escape(nPedidoID);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalanteciparentrega.asp' + strPars;
    showModalWin(htmlPath, new Array(980, 542));
}

function openModalIncluiServicos() {
    var strPars = new String();
    //var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset(' + '\'' + 'TransacaoID' + '\'' + ').value');

    //strPars = '?&nTransacaoID=' + escape(nTransacaoID);

    if ((fg.Rows <= 2) || (glb_bNCM)) {
        htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalincluiservicos.asp'; // + strPars;
        showModalWin(htmlPath, new Array(980, 570));
    }
    else {
        if (window.top.overflyGen.Alert('J� existe um servi�o em seu pedido.') == 0)
            return null;
    }
}
/********************************************************************
Funcao criada pelo programador.
Abre modal para pesquisa refinada de produtos

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalIncluiItens() {
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];

    var nTipoPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoPedidoID' + '\'' + '].value');

    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Resultado' + '\'' + '].value');

    var bTemNF = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TemNF' + '\'' + '].value');

    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    var nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');

    var nTipoPessoa = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'TipoPessoa' + '\'' + '].value');

    var nVariacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'Variacao' + '\'' + '].value');

    var nPercentualSUP = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PercentualSUP' + '\'' + '].value');

    var bReferencia = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'Referencia' + '\'' + '].value');

    var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransacaoID' + '\'' + '].value');

    var nReferencia = 0;

    if (bReferencia)
        nReferencia = 1;

    var nQuantidadeItens = 0;

    for (j = 2; j < fg.Rows; j++) {
        if ((getCellValueByColKey(fg, 'TemPPB*', j) == 1) && (nTipoPedidoID == '602') && (bResultado) && (bTemNF))
            nQuantidadeItens += 3;
        else
            nQuantidadeItens++;
    }

    //var nNumeroMaxItens = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoEstaticCmbs.recordset[' + '\'' + 'fldID' + '\'' + '].value');
    var nNumeroMaxItens = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_NumeroMaxItens');
    var bTemNF = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'chkTemNF.checked');

    var bArredondaValor = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'ArredondaPreco' + '\'' + '].value');

    var bPedidoImportacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'EhImportacao' + '\'' + '].value');

    var nArredondaValor = 0;

    if (bArredondaValor)
        nArredondaValor = 1;

    // mandar os parametros para o servidor
    strPars = '?';
    strPars += 'nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nPedidoID=' + escape(nPedidoID);
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nTipoPessoa=' + escape(nTipoPessoa);
    strPars += '&nVariacao=' + escape(nVariacao);
    strPars += '&nPercentualSUP=' + escape(nPercentualSUP);
    strPars += '&nReferencia=' + escape(nReferencia);
    strPars += '&nQuantidadeItens=' + escape(nQuantidadeItens);
    if (bTemNF)
        strPars += '&nNumeroMaxItens=' + escape(nNumeroMaxItens);
    else
        strPars += '&nNumeroMaxItens=' + escape(999);

    strPars += '&nArredondaValor=' + escape(nArredondaValor);
    strPars += '&bPedidoImportacao=' + escape(bPedidoImportacao ? 1 : 0);
    strPars += '&nTransacaoID=' + escape(nTransacaoID);

    //Envia se a transa��o permite produtos desativos: LFS 17/05/2011
    var glb_ProdutoDesativo = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_ProdutoDesativo');
    strPars += '&produtoDesativo=' + escape(glb_ProdutoDesativo);

    // carregar modal - nao faz operacao de banco no carregamento
    if (bReferencia) {
        htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalreferencia.asp' + strPars;
        showModalWin(htmlPath, new Array(980, 570));
    }
    else {
        htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalincluiitens.asp' + strPars;
        showModalWin(htmlPath, new Array(995, 532));
    }
}

/********************************************************************
Funcao criada pelo programador.
Executa a Procedure sp_Pedido_Fecha

Parametro:
nTipoFechamento: 3- Recalcula Valores
4- Fecha Pedido
5- Atualiza Pedido

Retorno:
nenhum
********************************************************************/
function fechaPedidoParcelas(nTipoFechamento) {
    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    glb_AlertResultado = nTipoFechamento;

    lockInterface(true);

    var strPars = '?';
    strPars += 'nPedidoID=' + escape(nPedidoID);
    strPars += '&nTipoFechamento=' + escape(nTipoFechamento);
    dsoParcelas.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/fechapedidoparcelas.aspx' + strPars;
    dsoParcelas.ondatasetcomplete = fechaPedidoParcelas_DSC;
    dsoParcelas.Refresh();
}

/********************************************************************
Funcao criada pelo programador.
Retorno da funcao fechaPedidoParcelas

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function fechaPedidoParcelas_DSC() {
    //destrava a Interface
    lockInterface(false);

    // Executa refresh no sup
    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, '__btn_REFR(' + '\'' + 'sup' + '\'' + ')');
}

/********************************************************************
Funcao criada pelo programador.
Monta o alert de Resultado e muda para a pasta de Itens

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function showAlertFechaPedidos() {
    if (__winTimerHandler != null) {
        window.clearInterval(__winTimerHandler);
        __winTimerHandler = null;
    }

    glb_AlertResultado = 0;

    var folderChanged = false;

    var nContribuicaoAjustada = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'ValorTotalContrAjustada' + '\'' + '].value');

    var nMargemContribuicao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'MargemContribuicao' + '\'' + '].value');

    if ((nContribuicaoAjustada != null) && (nMargemContribuicao != null)) {
        var sContribuicaoAjustada = padNumReturningStr(roundNumber(nContribuicaoAjustada, 2), 2);
        var sMargemContribuicao = padNumReturningStr(roundNumber(nMargemContribuicao, 2), 2);

        // Alert de Resultado
        if (window.top.overflyGen.Alert('Resultados do Pedido ' + '\n\n' +
                                         'Contribui��o:' + ' ' + '$' + sContribuicaoAjustada + '     ' + sMargemContribuicao + '%' + '\n') == 0)
            return null;

        /*  Comentado a pedido do Marcio da Gogeek        
        if (glb_AlertResultado >= 4)
        {
        // Executa refresh no inf
        showBtnsEspecControlBar('inf', true, [0, 0, 0, 1]);
            
        folderChanged = selOptByValueOfSelInControlBar('inf', 1, 24002);
            
        if ( folderChanged )
        {
        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
        'fillCmbsFiltInf_DSC(' + 24002 + ')');
        }    
            
        __btn_REFR('inf');
        }
        */
    }
    
}

/********************************************************************
Funcao criada pelo programador.
Muda para a pasta de Itens no caso de Pedidos que nao geram resultados

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function showFolderItens() {
    if (__winTimerHandler != null) {
        window.clearInterval(__winTimerHandler);
        __winTimerHandler = null;
    }

    var folderChanged = false;

    // Executa refresh no inf
    showBtnsEspecControlBar('inf', true, [0, 0, 0, 1]);

    folderChanged = selOptByValueOfSelInControlBar('inf', 1, 24002);

    if (folderChanged) {
        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      'fillCmbsFiltInf_DSC(' + 24002 + ')');
    }

    __btn_REFR('inf');

    glb_AlertResultado = 0;
}

function callCarrier() {
    // Manda mensagem
    var empresa = getCurrEmpresaData();

    if ((keepCurrFolder() == 24002) && (fg.Editable == false) &&
         (fg.Rows > 1)) {
        openRelPesCon(fg.TextMatrix(fg.Row, 1));
    }

    else if ((keepCurrFolder() == 24003) && (fg.Editable == false) &&
         (fg.Rows > 1) && (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FinanceiroID*')) != '')) {
        sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(empresa[0], fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FinanceiroID*'))));
    }
    else if ((keepCurrFolder() == 24005) && (fg.Editable == false) &&
         (fg.Rows > 1) && (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'AsstecID')) != '')) {
        sendJSCarrier(getHtmlId(), 'SHOWASSTEC', new Array(empresa[0], fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'AsstecID'))));
    }
    else if ((keepCurrFolder() == 24006) && (fg.Editable == false) &&
         (fg.Rows > 1) && (fg.TextMatrix(fg.Row, 9) != '')) {
        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));
    }
    else if ((keepCurrFolder() == 24008) && (fg.Editable == false) &&
         (fg.Rows > 1)) {
        sendJSCarrier(getHtmlId(), 'SHOWORDEMPRODUCAO', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));
    }
    else if ((keepCurrFolder() == 24009) && (fg.Editable == false) &&
         (fg.Rows > 1)) {
        sendJSCarrier(getHtmlId(), 'SHOWCONCEITO', new Array(empresa[0], fg.TextMatrix(fg.Row, 3)));
    }
}

function geraOrdemProducao() {
    lockInterface(true);

    var strPars = new String();
    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
            'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');
    var nUsuarioID = getCurrUserID();

    strPars = '?nPedidoID=' + escape(nPedidoID);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);

    dsoGeraOrdemProducao.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/gerarordemproducao.aspx' + strPars;

    dsoGeraOrdemProducao.ondatasetcomplete = dsoGeraOrdemProducao_DSC;
    dsoGeraOrdemProducao.refresh();
}

/********************************************************************
Funcao do programador, retorno do servidor apos executar a procedure 
sp_xxxx
           
Parametros: Nenhum

Retorno: Nenhum
       
********************************************************************/
function dsoGeraOrdemProducao_DSC() {
    if (!((dsoGeraOrdemProducao.recordset.BOF) && (dsoGeraOrdemProducao.recordset.EOF))) {
        if (dsoGeraOrdemProducao.recordset['Mensagem'].value != null) {
            if (window.top.overflyGen.Alert(dsoGeraOrdemProducao.recordset['Mensagem'].value) == 0)
                return null;
        }
    }

    lockInterface(false);

    __btn_REFR('sup');
}

function detalhaTransportadora(btnClicked) {
    var empresa = getCurrEmpresaData();
    var nPessoaID = 0;

    if (btnClicked == 1)
        nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                               'dsoSup01.recordset[' + '\'' + 'TransportadoraID' + '\'' + '].value');
    else if (btnClicked == 2)
        gravaDataChegadaPortador();

    if ((nPessoaID != null) && (nPessoaID > 0))
        sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], nPessoaID));
}

function gravaDataChegadaPortador() {
    var strPars;

    if (dso01JoinSup.recordset['V_dtChegadaPortador'].value != null) {
        strPars = '?bGravaNulo=' + escape(1);

        var _retConf = window.top.overflyGen.Confirm('Apagar data de chegada do portador?');

        if (_retConf == 0)
            return null;
        else if (_retConf == 2) {
            return null;
        }
    }
    else
        strPars = '?bGravaNulo=' + escape(0);

    strPars += '&nPedidoID=' + escape(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value'));

    dsoDataChegadaPortador.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/gravachegadaportador.aspx' + strPars;
    dsoDataChegadaPortador.ondatasetcomplete = gravaDataChegadaPortador_DSC;
    dsoDataChegadaPortador.Refresh();
}

function gravaDataChegadaPortador_DSC() {
    __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');
}

/********************************************************************
Funcao criada pelo programador
Vai ao servidor obter o ID da relacao do Produto com a empresa.

Parametro:
nProdutoID - ID do Produto

Retorno:
nenhum
********************************************************************/
function openRelPesCon(nProdutoID) {
    lockInterface(true);

    var strPars = new String();
    var empresaID = getCurrEmpresaData();

    strPars = '?nProdutoID=' + escape(nProdutoID);
    strPars += '&nEmpresaID=' + escape(empresaID[0]);
    strPars += '&nTipo=' + escape(2);

    dsoCurrRelation.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/currrelation.aspx' + strPars;
    dsoCurrRelation.ondatasetcomplete = openRelPesCon_DSC;
    dsoCurrRelation.refresh();
}

/********************************************************************
Funcao criada pelo programador
Retorno do servidor da funcao openRelPesCon()

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openRelPesCon_DSC() {
    var empresa = getCurrEmpresaData();

    // Manda o id da relacao entre o produto e a empresa a detalhar
    if (dsoCurrRelation.recordset.Fields.Count > 0)
        sendJSCarrier(getHtmlId(), 'SHOWRELPESCON',
                      new Array(empresa[0], dsoCurrRelation.recordset.Fields['fldID'].value));


    lockInterface(false);
}

/********************************************************************
Criado pelo programador
Informa ao sistema o termino da impressa da Nota Fiscal

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function oPrinter_OnPrintFinish() {
    glb_bPrintingNF = false;
    if (callPrintBoleto() == false) {
        glb_bImprimeNF = false;
        return null;
    }
    //Desabilitado impress�o da Lista de Embalagem no avan�o do estado de Faturamento para Expedi��o. Verificado com Camilo Rodrigues e Eder Silva que
    //essa fun��o n�o � mais utilizada - MTH 07/12/2017
    //glb_PrintListaOrOP_Timer = window.setInterval('printListaEmbalagemOrOP()', 300, 'JavaScript');
}

/********************************************************************
Criado pelo programador
Verifica o corrente estadoID do pedido no servidor

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function verifyCurrEstadoID() {
    lockInterface(true);

    setConnection(dsoGen01);


    dsoGen01.SQL = 'SELECT EstadoID ' +
                   'FROM Pedidos WITH(NOLOCK) ' +
                   'WHERE PedidoID = ' +
                    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    dsoGen01.ondatasetcomplete = verifyCurrEstadoID_DSC;
    dsoGen01.Refresh();
}

/********************************************************************
Criado pelo programador
Retorno do servidor da verificacao do corrente estadoID do pedido no servidor

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function verifyCurrEstadoID_DSC() {
    lockInterface(false);
    var nEstadoIDInterface, nEstadoIDBanco;
    var sMsg = '';

    if (!((dsoGen01.recordset.BOF) && (dsoGen01.recordset.EOF))) {
        nEstadoIDInterface = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                      'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

        nEstadoIDBanco = dsoGen01.recordset['EstadoID'].value;

        if (nEstadoIDInterface != nEstadoIDBanco)
            sMsg = 'O estado corrente deste pedido n�o permite esta opera��o.';
    }
    else
        sMsg = 'O estado corrente deste pedido n�o permite esta opera��o.';

    if (sMsg != '') {
        if (window.top.overflyGen.Alert(sMsg) == 0)
            return null;

        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                      '__btn_REFR(' + '\'' + 'sup' + '\'' + ')');
    }
    else {
        if (glb_btnCtlSupInf == 'INFEXCL')
            deleteConfirmLineInGrid();
        else if (glb_btnCtlSupInf == 'INFOK')
            saveRegister_1stPart();
        else if ((glb_btnCtlSupInf == 'INFUM') ||
                  (glb_btnCtlSupInf == 'INFDOIS') ||
                  (glb_btnCtlSupInf == 'INFTRES') ||
                  (glb_btnCtlSupInf == 'INFQUATRO') ||
                  (glb_btnCtlSupInf == 'INFCINCO') ||
                  (glb_btnCtlSupInf == 'INFSEIS') ||
                  (glb_btnCtlSupInf == 'INFSETE') ||
                  (glb_btnCtlSupInf == 'INFOITO') ||
                  (glb_btnCtlSupInf == 'INFNOVE'))
            btnBarClicked_Continue();
    }

}

/********************************************************************
Criado pelo programador
Executa a Procedure de vinculacao de Asstec

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function execSP_Pedido_AsstecNumeroSerie() {
    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');
    var nNumMovimentoID = 0;
    var nPedProdutoID = 0;
    var nAsstecID = 0;
    var nTipoResultado = 2;

    nAsstecID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'AsstecID'));
    nNumMovimentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'NumMovimentoID'));
    nPedProdutoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PedProdutoID'));

    if ((nNumMovimentoID != 0) || (nPedProdutoID != 0)) {
        lockInterface(true);

        var strPars = '?';

        strPars += 'nPedidoID=' + escape(nPedidoID);

        if (nAsstecID != '') {
            nTipoResultado = 3;
            strPars += '&nAsstecID=' + escape(nAsstecID);
            strPars += '&nNumMovimentoID=' + escape(nNumMovimentoID);
            strPars += '&nPedProdutoID=' + escape(nPedProdutoID);
        }

        strPars += '&nTipoResultado=' + escape(nTipoResultado);

        dsoAsstec.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/vinculaasstec.aspx' + strPars;
        dsoAsstec.ondatasetcomplete = execSP_Pedido_AsstecNumeroSerie_DSC;
        dsoAsstec.Refresh();
    }
}

/********************************************************************
Criado pelo programador
Retorno da funcao que Executa a Procedure de vinculacao de Asstec
chama a funcao que abre a JM Vincula Asstec: se nao for Pedido de Saida e bEhCliente = false
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function execSP_Pedido_AsstecNumeroSerie_DSC() {
    //destrava a Interface
    lockInterface(false);

    var sErro = dsoAsstec.recordset['fldResponse'].value;

    if ((sErro.length) > 2)
    {
        if (window.top.overflyGen.Alert(dsoAsstec.recordset['fldResponse'].value) == 1)
            return null;
    }

    // EhCliente
    var bEhCliente = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'EhCliente\'].value');

    // Tipo de Pedido
    var nTipoPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TipoPedidoID\'].value');

    if (dsoAsstec.recordset['bStartModal'].value == "true")
        openModalVinculaAsstec();
    else {
        __btn_REFR('inf');
    }
}

/********************************************************************
Criado pelo programador
Verifica o corrente estadoID do pedido no servidor

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function verificaPedidosVinculados() {
    lockInterface(true);

    setConnection(dsoPedidosVinculados);

    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    dsoPedidosVinculados.SQL = 'SELECT CONVERT(BIT, COUNT(1)) AS PedidosAntecipadosPendentes ' +
                                    'FROM Pedidos_Vinculados a WITH(NOLOCK) ' +
                                        'INNER JOIN Pedidos_Vinculados b WITH(NOLOCK) ON ((b.VinculadoID = a.VinculadoID) AND (b.PedidoID <> a.PedidoID)) ' +
                                        'INNER JOIN Pedidos c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) ' +
                                    'WHERE ((a.PedidoID = ' + nPedidoID + ') AND (a.Origem = 1) AND (a.TipoVinculoID = 4401) AND (c.EstadoID IN (21,22,23,24,33,25,26))) AND (c.TipoPedidoID <> 601)';

    dsoPedidosVinculados.ondatasetcomplete = verificaPedidosVinculados_DSC;
    dsoPedidosVinculados.Refresh();
}

/********************************************************************
Criado pelo programador
Retorno do servidor da verificacao do corrente estadoID do pedido no servidor

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function verificaPedidosVinculados_DSC() {
    lockInterface(false);

    var bPedidosAntecipadosPendentes = dsoPedidosVinculados.recordset['PedidosAntecipadosPendentes'].value;

    if (bPedidosAntecipadosPendentes != 0) 
        glb_bPedidosAntecipadosPendentes = true;
    else
        glb_bPedidosAntecipadosPendentes = false;

    if (glb_bPedidosAntecipadosPendentes) {
        if (window.top.overflyGen.Alert('Existe pedidos vinculados pendentes. Avan�ar pedidos para liberar nova antecipa��o de entrega.') == 0)
            return null;
    }
    else
        openModalAnteciparEntrega();
}

/********************************************************************
Criado pelo programador
Abre a JM de vinculacao de Asstec
    
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalVinculaAsstec() {
    var htmlPath;
    var strPars = new String();
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];
    var nContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nTipoEmpresaID = aEmpresa[5];
    var nEmpresaPaisID = aEmpresa[1];
    var nEmpresaUFID = aEmpresa[4];
    var nEhCliente = 0;

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa);
    strPars += '&nContextoID=' + escape(nContextoID[1]);
    strPars += '&nTipoEmpresaID=' + escape(nTipoEmpresaID);
    strPars += '&nEmpresaPaisID=' + escape(nEmpresaPaisID);
    strPars += '&nEmpresaUFID=' + escape(nEmpresaUFID);

    nEhCliente = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'EhCliente' + '\'' + '].value');
    if (nEhCliente == false)
        strPars += '&nEhCliente=' + escape('0');
    else
        strPars += '&nEhCliente=' + escape('1');

    strPars += '&sCaller=' + escape('I');

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalvinculaasstec.asp' + strPars;
    showModalWin(htmlPath, new Array(672, 386));
}

/********************************************************************
Criado pelo programador
Abre a JM de Material com Defeito

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalMaterialDefeito() {
    var htmlPath;
    var strPars = new String();
    var bResultado;
    var bEhCliente;

    bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'Resultado' + '\'' + '].value');

    bEhCliente = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'EhCliente' + '\'' + '].value');

    if (bResultado == false)
        strPars += '?nResultado=' + escape('0');
    else
        strPars += '?nResultado=' + escape('1');

    if (bEhCliente == false)
        strPars += '&nEhCliente=' + escape('0');
    else
        strPars += '&nEhCliente=' + escape('1');

    strPars += '&sCaller=' + escape('I');

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalmaterialdefeito.asp' + strPars;
    showModalWin(htmlPath, new Array(400, 386));
}

function openModalPesqNumSerie() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 770;
    var nHeight = 470;

    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('I');
    strPars += '&nPedidoID=' + escape(nPedidoID);

    // carregar a modal
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalpesqnumserie.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

/********************************************************************
Criado pelo programador
Abre a JM de Gerar Valores a Localizar

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalGerarValoresLocalizar() {
    if (glb_CallOpenModalVL_Timer != null) {
        window.clearInterval(glb_CallOpenModalVL_Timer);
        glb_CallOpenModalVL_Timer = null;
    }

    var htmlPath;
    var strPars = new String();

    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'EmpresaID' + '\'' + '].value');

    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

    var nTipoPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'TipoPedidoID' + '\'' + '].value');

    var nTipoValorID;

    if (nTipoPedidoID == 601)
        nTipoValorID = 1001;
    else
        nTipoValorID = 1002;

    strPars = '?sCaller=' + escape('I');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nPedidoID=' + escape(nPedidoID);
    strPars += '&nEstadoID=' + escape(nEstadoID);
    strPars += '&nTipoValorID=' + escape(nTipoValorID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalgeravalorlocalizar.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}

/********************************************************************
Criado pelo programador
Abre a JM de Setar Forma de Pagamento

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalSetarFormaPagamento() {
    var htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalsetarformapagamento.asp';
    var nPedParcelaID = getCellValueByColKey(fg, 'PedParcelaID', fg.Row);

    var strPars = new String();
    strPars = '?sCaller=' + escape('I');
    strPars += '&nPedParcelaID=' + escape(nPedParcelaID);

    showModalWin((htmlPath + strPars), new Array(0, 0));
}

/********************************************************************
Chama a rotina de impressao de nota que esta na Biblioteca notafiscal.js

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function callPrintNF() {
    if (glb_CallPrintNotaFiscal_Timer != null) {
        window.clearInterval(glb_CallPrintNotaFiscal_Timer);
        glb_CallPrintNotaFiscal_Timer = null;
    }

    // As duas linhas abaixo ocorrem quando a chamada da nf
    // vier da janela de impressao
    // esta funcao fecha a janela modal e destrava a interface
    restoreInterfaceFromModal();
    // escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Emitindo Nota Fiscal');

    glb_bEmissorNF = false;
    glb_bImprimeNF = false;

    glb_nNotaFiscalID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
    'dsoSup01.recordset[' + '\'' + 'NotaFiscalID' + '\'' + '].value');

    imprimeNotaFiscal();

    //printListaEmbalagemOrOP();
    // escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Detalhe');

}

function callPrintNFe() {
    if (glb_CallPrintNotaFiscal_Timer != null) {
        window.clearInterval(glb_CallPrintNotaFiscal_Timer);
        glb_CallPrintNotaFiscal_Timer = null;
    }

    // As duas linhas abaixo ocorrem quando a chamada da nf
    // vier da janela de impressao
    // esta funcao fecha a janela modal e destrava a interface
    restoreInterfaceFromModal();
    // escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Detalhe');

    glb_bEmissorNF = false;
    glb_bImprimeNF = false;

    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'ImprimirNFe()');
}

/********************************************************************
Chama a rotina de impressao de nota que esta na Biblioteca notafiscal.js

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
/* Desabilitado impress�o da Lista de Embalagem no avan�o do estado de Faturamento para Expedi��o. Verificado com Camilo Rodrigues e Eder Silva que
   essa fun��o n�o � mais utilizada - MTH 07/12/2017
function callPrintListaEmbalagem() {
    glb_callPrintListaEmbalagem_Timer = window.setInterval('printListaEmbalagem()', 100, 'JavaScript');
}
*/
function callPrintOrdemProducao() {
    glb_callPrintListaEmbalagem_Timer = window.setInterval('printOrdemProducao()', 100, 'JavaScript');
}

function callPrintBoleto() {
    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');
    var bPrintBloqueto = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'TemBloqueto' + '\'' + '].value');

    if (bPrintBloqueto == false)
        return false;

    /****************
    var _retConf = window.top.overflyGen.Confirm('Imprime Boleto?');
                    
    if ( _retConf == 0 )
    return null;
    else if ( _retConf == 2 )
    return false;
    ****************/

    var strPars = new String();

    var htmlPath;

    strPars = '?nPedidoID=' + escape(nPedidoID);
    strPars += '&sTitulo=' + escape('Bloqueto de cobran�a');
    strPars += '&sCaller=' + escape('I');

    // carregar modal de documentos
    htmlPath = SYS_PAGESURLROOT + '/modalgen/modalbloquetocobranca.asp' + strPars;

    showModalWin(htmlPath, new Array(1000, 543));
    return true;
}
/* Desabilitado impress�o da Lista de Embalagem no avan�o do estado de Faturamento para Expedi��o. Verificado com Camilo Rodrigues e Eder Silva que
   essa fun��o n�o � mais utilizada - MTH 07/12/2017
function printListaEmbalagem() {
    if (glb_callPrintListaEmbalagem_Timer != null) {
        window.clearInterval(glb_callPrintListaEmbalagem_Timer);
        glb_callPrintListaEmbalagem_Timer = null;
    }

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'listaEmbalagem(' +
                  '\'' + 'Lista de Embalagem' + '\'' + ',' +
                  nPedidoID + ', ' + '\'' + aEmpresaData[3] + '\'' + ', false)');
}
*/
function printOrdemProducao() {
    if (glb_callPrintListaEmbalagem_Timer != null) {
        window.clearInterval(glb_callPrintListaEmbalagem_Timer);
        glb_callPrintListaEmbalagem_Timer = null;
    }

    sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'relatorioOrdemProducao(1, false)');
}

function openModalCampanhas() {
    var strPars = new String();
    var htmlPath;
    var nPedidoID, nEstadoID, nParceiroID;

    strPars = '?nUserID=' + escape(getCurrUserID());

    nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

    nParceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'ParceiroID' + '\'' + '].value');

    strPars += '&nPedidoID=' + escape(nPedidoID);
    strPars += '&nEstadoID=' + escape(nEstadoID);
    strPars += '&nParceiroID=' + escape(nParceiroID);
    strPars += '&sCaller=' + escape('I');


    // carregar modal de documentos
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalCampanhas.asp' + strPars;

    showModalWin(htmlPath, new Array(780, 460));
    return true;
}

function openModalFabricantes() {
    var strPars = new String();
    var htmlPath;
    var nPedidoID, nEstadoID, nParceiroID;

    strPars = '?nUserID=' + escape(getCurrUserID());

    nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

    nParceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'ParceiroID' + '\'' + '].value');

    strPars += '&nPedidoID=' + escape(nPedidoID);
    strPars += '&nEstadoID=' + escape(nEstadoID);
    strPars += '&nParceiroID=' + escape(nParceiroID);
    strPars += '&sCaller=' + escape('I');

    // carregar modal de documentos
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalfabricantes.asp' + strPars;

    showModalWin(htmlPath, new Array(780, 380));
    return true;
}

function openModalItens() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 995;
    var nHeight = 532;
    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                               ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));
    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                               ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
    var nE1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                           ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'E1' + '\'' + ')'));
    var nE2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                               ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'E2' + '\'' + ')'));

    var bDireitoAlterar = ((nA1 && nA2) ? 1 : 0);
    var bDireitoExcluir = ((nE1 && nE2) ? 1 : 0);

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)    
    strPars = '?sCaller=' + escape('I');
    strPars += '&bDireitoAlterar=' + escape(bDireitoAlterar);
    strPars += '&bDireitoExcluir=' + escape(bDireitoExcluir);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalitens.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

function __adjustFramePrintJet() {
    var i, elem, coll;
    var dimArray = new Array();
    // todos os iframes
    coll = document.all.tags('IFRAME');

    // posi��o e dimens�es  dos frames
    dimArray[0] = 0;
    dimArray[1] = 0;
    dimArray[2] = 0;
    dimArray[3] = 0;

    for (i = 0; i < coll.length; i++) {
        coll.item(i).style.visibility = 'hidden';
        coll.item(i).scrolling = 'no';
        coll.item(i).tabIndex = -1;
        moveFrameInHtmlTop(coll.item(i).id, dimArray);
        // originally no backgroundColor
        coll.item(i).style.backgroundColor = 'transparent';
    }
}

function __loadPrintJetPagesInFrame(urlPath) {
    if ((urlPath == null) || (urlPath == ''))
        return null;

    var frameToFill;

    frameToFill = window.document.all.tags("IFRAME").item(0);
    frameToFill.src = urlPath;
}

// Abre modal com demonstrativo de baixa de comiss�es - DMC 08/08/2011
function openModalBaixaComissoes() {
    var nTipoParcelaID = getCellValueByColKey(fg, 'TipoParcelaID', fg.Row);

    if (!((nTipoParcelaID == 641) || (nTipoParcelaID == 642))) {
        if (window.top.overflyGen.Alert('Selecione uma parcela de comiss�o') == 0)
            return null;

        return null;
    }

    var htmlPath;
    var strPars = new String();
    var nPedParcelaID = getCellValueByColKey(fg, 'PedParcelaID', fg.Row);

    strPars = '?nPedParcelaID=' + escape(nPedParcelaID);

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalbaixacomissoes.asp' + strPars;
    showModalWin(htmlPath, new Array(950, 380));
}

function openModalDetalharContas() {
    if (fg.Row <= 0)
        return null;

    var htmlPath;
    var strPars = new String();
    var nEventoID = getCellValueByColKey(fg, 'EventoID', fg.Row);

    // mandar os parametros para o servidor
    strPars = '?nEventoID=' + escape(nEventoID);

    // carregar modal - nao faz operacao de banco no carregamento           
    htmlPath = SYS_PAGESURLROOT + '/modalgen/modaleventoscontabeis_contas.asp' + strPars;
    showModalWin(htmlPath, new Array(765, 460));
}

function openModalParcelasDiversos() {
    var htmlPath, nPedidoID, nValorTotalPedido, nTipoPedidoID;

    var nEmpresaData = getCurrEmpresaData();
    var nIdiomaDeID = nEmpresaData[7];
    var nIdiomaParaID = nEmpresaData[8];

    nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');
    nValorTotalPedido = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ValorTotalPedido' + '\'' + '].value');
    nTipoPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoPedidoID' + '\'' + '].value');

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalparcelasdiversas.asp';

    // Passar parametro se veio do inf ou do sup
    var strPars = new String();
    strPars = '?sCaller=' + escape('I');
    strPars += '&nEmpresaID=' + escape(getCurrEmpresaData()[0]);
    strPars += '&nPedidoID=' + escape(nPedidoID);
    strPars += '&nValorTotalPedido=' + escape(nValorTotalPedido);
    strPars += '&nTipoPedidoID=' + escape(nTipoPedidoID);
    strPars += '&nIdiomaDeID=' + escape(nIdiomaDeID);
    strPars += '&nIdiomaParaID=' + escape(nIdiomaParaID);

    showModalWin((htmlPath + strPars), new Array(470, 240));
}
function aprovacaoEspecial() {
    var nUsuarioID;
    var nPedidoID;
    var nTipoAprovacaoID;
    var sUsuarioAprovador;
    var nTipoResultado = 2;
    var nPedAprovacaoID;

    nUsuarioID = getCurrUserID();
    nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    nTipoAprovacaoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoAprovacaoID*'));
    sUsuarioAprovador = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '^PedAprovacaoID^dso01GridLkp^PedAprovacaoID^Fantasia*'));
    nPedAprovacaoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PedAprovacaoID'));

    if (sUsuarioAprovador != '')
        nTipoResultado = 3;

    if ((nPedAprovacaoID != 0) && (nTipoAprovacaoID != 0)) {
        lockInterface(true);

        var strPars = '?';

        strPars += 'nPedidoID=' + escape(nPedidoID);
        strPars += '&nTipoAprovacaoID=' + escape(nTipoAprovacaoID);
        strPars += '&nUsuarioID=' + escape(nUsuarioID);
        strPars += '&nTipoResultado=' + escape(nTipoResultado);
        strPars += '&nPedAprovacaoID=' + escape(nPedAprovacaoID);

        dsoAprovacaoEspecial.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/aprovacaoespecial.aspx' + strPars;
        dsoAprovacaoEspecial.ondatasetcomplete = aprovacaoEspecial_DSC;
        dsoAprovacaoEspecial.Refresh();
    }

}
function aprovacaoEspecial_DSC() {
    //destrava a Interface
    lockInterface(false);

    if (!((dsoAprovacaoEspecial.recordset.BOF) && (dsoAprovacaoEspecial.recordset.EOF))) {
        if ((dsoAprovacaoEspecial.recordset['Mensagem'].value != null) && (dsoAprovacaoEspecial.recordset['Mensagem'].value != '')) {
            if (window.top.overflyGen.Alert(dsoAprovacaoEspecial.recordset['Mensagem'].value) == 0)
                return null;
        }
    }

    __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');
}

function replicaRNTC() {
    var sObservacao = dso01JoinSup.recordset['Observacao'].value;
    var sRNTC = txtRNTC.value;
    var sReplaceRNTC = '';

    _retMsg = window.top.overflyGen.Confirm("A informa��o ser� replicada no campo de Observa��o. Deseja continuar?");

    if (_retMsg == 1) {
        if ((sRNTC != '' && sRNTC != null) && (sObservacao == '' || sObservacao == null)) {
            if (sRNTC.length <= 40)
                dso01JoinSup.recordset['Observacao'].value = sRNTC;
            else
                window.top.overflyGen.Alert('Mensagem campo observa��o maior que o permitido');
        }

        else if ((sObservacao != '' && sObservacao != null) && (sRNTC != '' && sRNTC != null)) {
            if (sObservacao.indexOf(glb_Rntc) >= 0)
                sReplaceRNTC = sObservacao.replace(glb_Rntc, txtRNTC.value);
            else
                sReplaceRNTC = sObservacao + ' ' + sRNTC;

            if (sReplaceRNTC.length <= 40)
                dso01JoinSup.recordset['Observacao'].value = sReplaceRNTC;
            else
                window.top.overflyGen.Alert('Mensagem campo observa��o maior que o permitido');
        }
        else if (sRNTC == '' || sRNTC == null) {
            if (sObservacao.indexOf(glb_Rntc) >= 0)
                sReplaceRNTC = sObservacao.replace(glb_Rntc, txtRNTC.value);
            else
                sReplaceRNTC = sObservacao + ' ' + sRNTC;

            if (sReplaceRNTC.length <= 40)
                dso01JoinSup.recordset['Observacao'].value = sReplaceRNTC;
            else
                window.top.overflyGen.Alert('Mensagem campo observa��o maior que o permitido');
        }
    }
    else
        return null;
}
function verificaInvoice()
{
    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    setConnection(dsoVerificaInvoice);

    dsoVerificaInvoice.SQL = 'SELECT dbo.fn_Pedido_Invoice(' + nPedidoID + ', 2) AS Invoice';

    dsoVerificaInvoice.ondatasetcomplete = verificaInvoice_DSC;
    dsoVerificaInvoice.Refresh();
}

function verificaInvoice_DSC()
{
    if (dsoVerificaInvoice.recordset.Fields['Invoice'].value == null)
        window.top.overflyGen.Alert('Deve-se inserir a invoice pelo bot�o 11 antes de gerar parcela');
    else
        __winTimerHandler = window.setInterval('confirmParcelas()', 30, 'JavaScript');
}
/********************************************************************
Funcao criada pelo programador.
Abre janela modal de parceiro 

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPessoa() {
    var htmlPath;
    var strPars = new String();
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];
    var nContextoID = getCmbCurrDataInControlBar('sup', 1);

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa);
    strPars += '&nContextoID=' + escape(nContextoID[1]);
    strPars += '&sCaller=' + escape('INF');


    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalpessoa.asp' + strPars;
    showModalWin(htmlPath, new Array(671, 284));
}

function adjustLabelsCombos(bPaging) {
    if (bPaging) {
        setLabelOfControl(lblPessoaDealID, dso01JoinSup.recordset['PessoaDealID'].value);
    }
    else {
        setLabelOfControl(lblPessoaDealID, selPessoaDealID.value);
    }
}

function dateAdd(p_Interval, p_Number, p_Date) {

    /*
    //Ajuste string para formato americano.
    if (aEmpresa[1] == 130) {
        var dia = p_Date.substring(0, 2)
        var mes = p_Date.substring(3, 5)
        var ano = p_Date.substring(6, 10)

        p_Date = mes + '/' + dia + '/' + ano
    }
    */

    /*if (DATE_FORMAT == "DD/MM/YYYY")
        putDateInMMDDYYYY2(p_Date);*/

    p_Date = normalizeDate_DateTime(p_Date, (DATE_FORMAT == "DD/MM/YYYY" ? true : false));

    if (!new Date(p_Date)) { return "invalid date: '" + p_Date + "'"; }
    if (isNaN(p_Number)) { return "invalid number: '" + p_Number + "'"; }

    p_Number = new Number(p_Number);
    var dt = new Date(p_Date);

    switch (p_Interval.toLowerCase()) {
        case "yyyy": {
            dt.setFullYear(dt.getFullYear() + p_Number);
            break;
        }
        case "q": {
            dt.setMonth(dt.getMonth() + (p_Number * 3));
            break;
        }
        case "m": {
            dt.setMonth(dt.getMonth() + p_Number);
            break;
        }
        case "y":			// day of year
        case "d":			// day
        case "w": {		// weekday
            dt.setDate(dt.getDate() + p_Number);
            break;
        }
        case "ww": {	// week of year
            dt.setDate(dt.getDate() + (p_Number * 7));
            break;
        }
        case "h": {
            dt.setHours(dt.getHours() + p_Number);
            break;
        }
        case "n": {		// minute
            dt.setMinutes(dt.getMinutes() + p_Number);
            break;
        }
        case "s": {
            dt.setSeconds(dt.getSeconds() + p_Number);
            break;
        }
        case "ms": {	// JS extension
            dt.setMilliseconds(dt.getMilliseconds() + p_Number);
            break;
        }
        default: {
            return "invalid interval: '" + p_Interval + "'";
        }
    }

    if (DATE_FORMAT == "DD/MM/YYYY")
        return dataFormatada(dt, 1);
    else
        return dataFormatada(dt, 2);
}

function dataFormatada(data, tipo) {
    var dia = data.getDate();
    if (dia.toString().length == 1)
        dia = "0" + dia;
    var mes = data.getMonth() + 1;
    if (mes.toString().length == 1)
        mes = "0" + mes;
    var ano = data.getFullYear();

    if (tipo == 1)
        return dia + "/" + mes + "/" + ano;
    else if (tipo == 2)
        return mes + "/" + dia + "/" + ano;
}

//Valida datas
function verificaData(sData) {
    var sData = trimStr(sData);
    var bDataIsValid = true;

    if (sData != '')
        bDataIsValid = chkDataEx(sData);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        return false;
    }
    return true;
}

//BETWEEN entre datas
function beweenDate(sdtStart, sdtEnd, sType)
{
    sdtStart = new Date(sdtStart);
    sdtEnd = new Date(sdtEnd);
    var dr = moment.range(sdtStart, sdtEnd);

    if (sType == 1)
        return dr.diff('months');
    else if (sType == 2)
        return dr.diff('days');
}