/*********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de Pedidos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************   
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
-> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
registro o filtro deve ser por proprietarioID e nao pelo identity
do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister) {
    var aEmpresaData = getCurrEmpresaData();
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 *,' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=Pedidos.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'dbo.fn_Pedido_Totais(PedidoID,11) AS ValorTotalServicos,' +
               'dbo.fn_Pedido_Totais(PedidoID,12) AS ValorTotalProdutos,' +
               'dbo.fn_Pedido_Totais(PedidoID,13) AS ValorTotalImpostos,' +
               //'ISNULL(ValorFrete, 0) AS ValorFrete, ' +
                'ISNULL(ValorSeguro, 0) AS ValorSeguro, ' +
                //'ISNULL(ValorOutrasDespesas, 0) AS ValorOutrasDespesas, ' +
               'CONVERT(VARCHAR, dtPedido, ' + DATE_SQL_PARAM + ') AS V_dtPedido, ' +
               'dtNotaFiscal = (SELECT CONVERT(VARCHAR, dtNotaFiscal, ' + DATE_SQL_PARAM + ') FROM NotasFiscais WITH(NOLOCK) WHERE NotaFiscalID = Pedidos.NotaFiscalID), ' +
               '(SELECT TOP 1 Transacoes.PermitirAnteciparEntrega FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS PermitirAnteciparEntrega, ' +
               'dbo.fn_Pedido_PrevisaoEntregaContratual(PedidoID) AS PrevisaoEntrega, ' +
               'dbo.fn_Pedido_PrevisaoEntrega(PedidoID) AS PrevisaoEntregaItem, ' +
               'dbo.fn_Pedido_Cor(PedidoID,' + nUserID + ', GETDATE(), 9) AS CorPrevisaoEntrega, ' +
               'CONVERT(VARCHAR, dtPrevisaoEntrega, ' + DATE_SQL_PARAM + ') AS V_dtPrevisaoEntrega, ' +
               'dbo.fn_Pedido_Tipo(PedidoID, NULL) AS TipoPessoa, ' +
			   '(SELECT TOP 1 dbo.fn_Operacoes_Referencia(Transacoes.OperacaoID, NULL, 721, NULL) FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS Referencia, ' +
			   '(SELECT TOP 1 Transacoes.Resultado FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS Resultado, ' +
			   '(SELECT TOP 1 Transacoes.MetodoCustoID FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS MetodoCustoID, ' +
			   '(SELECT TOP 1 Transacoes.Asstec FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS Asstec, ' +
               'ISNULL((SELECT TOP 1 Transacoes.MatrizFilial FROM Operacoes Transacoes WITH(NOLOCK) ' +
		 	        'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)), 0) AS MatrizFilial, ' +
			   '(SELECT TOP 1 dbo.fn_Tradutor(Transacoes.Operacao, ' + aEmpresaData[7] + ', ' + aEmpresaData[8] + ', NULL) FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS Transacao, ' +
               'dbo.fn_Pedido_TemProducao(PedidoID) AS TemProducao, ' +
               'dbo.fn_Pedido_TemRIR(PedidoID) AS TemRIR, ' +
			   'dbo.fn_Financeiro_TemBoleto(-PedidoID) AS TemBloqueto, ' +
               'NULL AS DT_PrevisaoEntrega, ' +
               'dbo.fn_Pedido_Cor(PedidoID, ' + nUserID + ', GETDATE(), 2) AS CorEstado, ' +
               'dbo.fn_Pedido_NFe(PedidoID) AS PedidoEmitiraNFe, ' +
               'dbo.fn_Pessoa_Fantasia(ProgramaMarketingID, 0) AS ProgramaMarketing, ' +
               '(SELECT ModeloNF FROM NotasFiscais WITH(NOLOCK) WHERE NotaFiscalID = Pedidos.NotaFiscalID) AS ModeloNF, ' +
               '(SELECT COUNT(*) FROM NotasFiscais a WITH(NOLOCK) WHERE a.NotaFiscalID = Pedidos.NotaFiscalID AND a.TipoEmissaoID = 893) AS NFeFSDA, ' +               
               'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 303, 311), 0)  AS Produto, ' +
               'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 303, 315), 0)  AS ProdutoServico, ' +
               'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 320, 315), 0) AS Servico, ' +
               'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 309, NULL), 0) AS NCM, ' +
               'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 318, 315), 0) AS ServicoEstadual, ' +
               'ISNULL((SELECT TOP 1 Transacoes.ServicoProduto FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)), 0) AS ServicoProduto, ' +
               'dbo.fn_Estado_Pessoa_Parceiro(Pedidos.PessoaId,Pedidos.ParceiroID) as regAtivo, ' +
               '(SELECT TOP 1 clienteDesativo FROM Operacoes Transacoes WITH(NOLOCK) WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS produtoDesativo, ' +
               '(SELECT TOP 1 ISNULL(EhRetorno, 0) FROM Operacoes Transacoes WITH(NOLOCK) WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS EhRetorno, ' +
               'dbo.fn_Pedido_Importacao(PedidoID) AS EhImportacao, ' +
			   'dbo.fn_Pedido_Totais(PedidoID, 29) AS TotalDesconto, ' +
			   'dbo.fn_Fopag_Empresas(NULL,NULL,NULL) AS FopagEmpresas, ' +
			   'CONVERT(BIT,CASE WHEN PedidoReferenciaID IS NOT NULL THEN dbo.fn_Pedido_Complementar(PedidoID) ELSE NULL END) AS PedidoComplemento, ' +
			   '(SELECT PermiteOutroProduto FROM Operacoes Troca WITH(NOLOCK) WHERE (Pedidos.TransacaoID = Troca.OperacaoID)) AS PermiteOutroProduto, ' +
			   '(SELECT Fornecedor FROM Operacoes Troca2 WITH(NOLOCK) WHERE (Pedidos.TransacaoID = Troca2.OperacaoID)) AS Fornecedor, ' +
			   'dbo.fn_Pessoa_Localidade(Pedidos.PessoaID, 1, NULL, NULL)[PaisPessoaID], ' +
			   '(SELECT TOP 1 a.TipoConceitoID ' +
			        'FROM Operacoes_Conceitos a WITH(NOLOCK) ' +
			        'WHERE OperacaoID = Pedidos.TransacaoID) [TipoConceitoID], ' +
			   'dbo.fn_Pedido_EhComissao(PedidoID) AS PedidoEhComissao, ' +
			   '0 AS Vinculado, ' +               
			   '(SELECT DocumentoFiscalID FROM NotasFiscais WITH(NOLOCK) WHERE NotaFiscalID = Pedidos.NotaFiscalID) AS DocumentoFiscalID, ' +
               '(SELECT ArquivoDanfePDF FROM NotasFiscais WITH(NOLOCK) WHERE NotaFiscalID = Pedidos.NotaFiscalID) AS ArquivoDanfePDF, ' +
               'dbo.fn_Pedido_Invoice(PedidoID, 2) AS Invoice, ' +
               'CONVERT(VARCHAR, dbo.fn_Pedido_dtInvoice(PedidoID), 103) AS dtInvoice, ' +
               '(SELECT dbo.fn_TipoAuxiliar_Item(EnquadramentoEmpresaID, 0) ' +
                    'FROM Pessoas WITH(NOLOCK) ' +
                    'WHERE PessoaID = Pedidos.PessoaID) AS EnquadramentoEmpresa, ' +
               '(SELECT dbo.fn_TipoAuxiliar_Item(RegimeTributarioID, 0) ' +
                    'FROM Pessoas WITH(NOLOCK) ' +
                    'WHERE PessoaID = Pedidos.PessoaID) AS RegimeTributario, ' +
               'dbo.fn_Pedido_Vinculado(PedidoID, NULL, NULL, NULL, 9) AS PermiteEstornoAntecipacao, ' +
               'dbo.fn_Pedido_EhCotador(PedidoID) AS Pedido_EhCotador ' +
			   'FROM Pedidos WITH(NOLOCK) ' +
               'WHERE ProprietarioID = ' + nID + 'ORDER BY PedidoID DESC';
    else
        sSQL = 'SELECT *,' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=Pedidos.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'dbo.fn_Pedido_Totais(PedidoID,11) AS ValorTotalServicos,' +
               'dbo.fn_Pedido_Totais(PedidoID,12) AS ValorTotalProdutos,' +
               'dbo.fn_Pedido_Totais(PedidoID,13) AS ValorTotalImpostos,' +
                //'ISNULL(ValorFrete, 0) AS ValorFrete, ' +
                'ISNULL(ValorSeguro, 0) AS ValorSeguro, ' +
                //'ISNULL(ValorOutrasDespesas, 0) AS ValorOutrasDespesas, ' +
               'CONVERT(VARCHAR, dtPedido, ' + DATE_SQL_PARAM + ') AS V_dtPedido, ' +
               'dtNotaFiscal = (SELECT CONVERT(VARCHAR, dtNotaFiscal, ' + DATE_SQL_PARAM + ') FROM NotasFiscais WITH(NOLOCK) WHERE NotaFiscalID = Pedidos.NotaFiscalID), ' +
               '(SELECT TOP 1 Transacoes.PermitirAnteciparEntrega FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS PermitirAnteciparEntrega, ' +
               'dbo.fn_Pedido_Tipo(PedidoID, NULL) AS TipoPessoa, ' +
			   '(SELECT TOP 1 dbo.fn_Operacoes_Referencia(Transacoes.OperacaoID, NULL, 721, NULL) FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS Referencia, ' +
			   '(SELECT TOP 1 Transacoes.Resultado FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS Resultado, ' +
			   '(SELECT TOP 1 Transacoes.MetodoCustoID FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS MetodoCustoID, ' +
			   '(SELECT TOP 1 Transacoes.Asstec FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS Asstec, ' +
               'ISNULL((SELECT TOP 1 Transacoes.MatrizFilial FROM Operacoes Transacoes WITH(NOLOCK) ' +
		 	        'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)), 0) AS MatrizFilial, ' +
			   '(SELECT TOP 1 dbo.fn_Tradutor(Transacoes.Operacao, ' + aEmpresaData[7] + ', ' + aEmpresaData[8] + ', NULL) FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS Transacao, ' +
               'dbo.fn_Pedido_TemProducao(PedidoID) AS TemProducao, ' +
               'dbo.fn_Pedido_TemRIR(PedidoID) AS TemRIR, ' +
			   'dbo.fn_Financeiro_TemBoleto(-PedidoID) AS TemBloqueto, ' +
               'dbo.fn_Pedido_PrevisaoEntregaContratual(PedidoID) AS PrevisaoEntrega, ' +
               'dbo.fn_Pedido_PrevisaoEntrega(PedidoID) AS PrevisaoEntregaItem, ' +
               'dbo.fn_Pedido_Cor(PedidoID,' + nUserID + ', GETDATE(), 9) AS CorPrevisaoEntrega, ' +
               '(SELECT CONVERT(VARCHAR, a.dtPrevisaoEntrega , ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
					'CONVERT(VARCHAR, a.dtPrevisaoEntrega, 108) ' +
					'FROM Pedidos a WITH(NOLOCK) ' +
					'WHERE a.PedidoID=Pedidos.PedidoID) AS V_dtPrevisaoEntrega, ' +
               'dbo.fn_Pedido_Cor(PedidoID, ' + nUserID + ', GETDATE(), 2) AS CorEstado, ' +
               '(SELECT TOP 1 a.Nome FROM Pessoas a WITH(NOLOCK) WHERE a.PessoaID=pedidos.PessoaID) AS NomePessoa, ' +
               'dbo.fn_Pedido_NFe(PedidoID) AS PedidoEmitiraNFe, ' +
               'dbo.fn_Pessoa_Fantasia(ProgramaMarketingID, 0) AS ProgramaMarketing, ' +
               '(SELECT ModeloNF FROM NotasFiscais WITH(NOLOCK) WHERE NotaFiscalID = Pedidos.NotaFiscalID) AS ModeloNF, ' +
               '(SELECT COUNT(*) FROM NotasFiscais a WITH(NOLOCK) WHERE a.NotaFiscalID = Pedidos.NotaFiscalID AND a.TipoEmissaoID = 893) AS NFeFSDA, ' +
               'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 303, 311), 0)  AS Produto, ' +
               'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 303, 315), 0)  AS ProdutoServico, ' +
               'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 320, 315), 0) AS Servico, ' +
               'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 309, NULL), 0) AS NCM, ' +
               'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 318, 315), 0) AS ServicoEstadual, ' +
               'isnull((SELECT TOP 1 Transacoes.ServicoProduto FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)), 0) AS ServicoProduto, ' +
               'dbo.fn_Estado_Pessoa_Parceiro(Pedidos.PessoaId,Pedidos.ParceiroID) as regAtivo, ' +
               '(SELECT TOP 1 clienteDesativo FROM Operacoes Transacoes WITH(NOLOCK) WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS produtoDesativo, ' +
               '(SELECT TOP 1 ISNULL(EhRetorno, 0) FROM Operacoes Transacoes WITH(NOLOCK) WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS EhRetorno, ' +
               'dbo.fn_Pedido_Importacao(PedidoID) AS EhImportacao, ' +
			   'dbo.fn_Pedido_Totais(PedidoID, 29) AS TotalDesconto, ' +
			   'CONVERT(BIT,CASE WHEN PedidoReferenciaID IS NOT NULL THEN dbo.fn_Pedido_Complementar(PedidoID) ELSE NULL END) AS PedidoComplemento, ' +
			   'dbo.fn_Fopag_Empresas(NULL,NULL,NULL) AS FopagEmpresas, ' +
			   '(SELECT PermiteOutroProduto FROM Operacoes Troca WITH(NOLOCK) WHERE (Pedidos.TransacaoID = Troca.OperacaoID)) AS PermiteOutroProduto,  ' +
			   '(SELECT Fornecedor FROM Operacoes Troca2 WITH(NOLOCK) WHERE (Pedidos.TransacaoID = Troca2.OperacaoID)) AS Fornecedor, ' +
			   'dbo.fn_Pessoa_Localidade(Pedidos.PessoaID, 1, NULL, NULL)[PaisPessoaID], ' +
			   	'(SELECT TOP 1 a.TipoConceitoID ' +
			        'FROM Operacoes_Conceitos a WITH(NOLOCK) ' +
			        'WHERE OperacaoID = Pedidos.TransacaoID) [TipoConceitoID], ' +
			   'dbo.fn_Pedido_EhComissao(PedidoID) AS PedidoEhComissao, ' +               
               'dbo.fn_Pedido_Vinculado(PedidoID, NULL, NULL, NULL, 1) AS Vinculado, ' +
               '(SELECT DocumentoFiscalID FROM NotasFiscais WITH(NOLOCK) WHERE NotaFiscalID = Pedidos.NotaFiscalID) AS DocumentoFiscalID, ' +
               '(SELECT ArquivoDanfePDF FROM NotasFiscais WITH(NOLOCK) WHERE NotaFiscalID = Pedidos.NotaFiscalID) AS ArquivoDanfePDF, ' +
               'dbo.fn_Pedido_Invoice(PedidoID, 2) AS Invoice, ' +
               'CONVERT(VARCHAR, dbo.fn_Pedido_dtInvoice(PedidoID), 103) AS dtInvoice, ' +
               '(SELECT dbo.fn_TipoAuxiliar_Item(EnquadramentoEmpresaID, 0) ' +
                    'FROM Pessoas WITH(NOLOCK) ' +
                    'WHERE PessoaID = Pedidos.PessoaID)  AS EnquadramentoEmpresa, ' +
               '(SELECT dbo.fn_TipoAuxiliar_Item(RegimeTributarioID, 0) ' +
                    'FROM Pessoas WITH(NOLOCK) ' +
                    'WHERE PessoaID = Pedidos.PessoaID) AS RegimeTributario, ' +
               'dbo.fn_Pedido_Vinculado(PedidoID, NULL, NULL, NULL, 9) AS PermiteEstornoAntecipacao, ' +
               'dbo.fn_Pedido_EhCotador(PedidoID) AS Pedido_EhCotador, ' +
               'dbo.fn_Pessoa_Localidade(Pedidos.PessoaID, 3, NULL, NULL) AS CidadePessoa ' +
			   'FROM Pedidos WITH(NOLOCK) WHERE PedidoID = ' + nID + ' ORDER BY PedidoID DESC';

    setConnection(dso);

    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    //               'CONVERT(VARCHAR, dtPrevisaoEntrega, '+DATE_SQL_PARAM+') AS V_dtPrevisaoEntrega, ' +               

    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso) {
    var aEmpresaData = getCurrEmpresaData();
    var nUserID = getCurrUserID();
    setConnection(dso);

    var sql;

    sql = 'SELECT *,' +
          'dbo.fn_Pedido_Totais(PedidoID,11) AS ValorTotalServicos,' +
          'dbo.fn_Pedido_Totais(PedidoID,12) AS ValorTotalProdutos,' +
          'dbo.fn_Pedido_Totais(PedidoID,13) AS ValorTotalImpostos,' +
          //'ISNULL(ValorFrete, 0) AS ValorFrete, ' +
          'ISNULL(ValorSeguro, 0) AS ValorSeguro, ' +
          //'ISNULL(ValorOutrasDespesas, 0) AS ValorOutrasDespesas, ' +
          'CONVERT(VARCHAR, dtPedido, ' + DATE_SQL_PARAM + ') as V_dtPedido, ' +
          'dtNotaFiscal = (SELECT CONVERT(VARCHAR, dtNotaFiscal, ' + DATE_SQL_PARAM + ') FROM NotasFiscais WITH(NOLOCK) WHERE NotaFiscalID = Pedidos.NotaFiscalID), ' +
          'dbo.fn_Pedido_PrevisaoEntregaContratual(PedidoID) AS PrevisaoEntrega, ' +
          'dbo.fn_Pedido_PrevisaoEntrega(PedidoID) AS PrevisaoEntregaItem, ' +
          'dbo.fn_Pedido_Cor(PedidoID, ' + nUserID + ', GETDATE(), 9) AS CorPrevisaoEntrega, ' +
          'CONVERT(VARCHAR, dtPrevisaoEntrega, ' + DATE_SQL_PARAM + ') as V_dtPrevisaoEntrega, ' +
          'dbo.fn_Pedido_Tipo(PedidoID, NULL) AS TipoPessoa, ' +
		  '(SELECT TOP 1 dbo.fn_Operacoes_Referencia(Transacoes.OperacaoID, NULL, 721, NULL) FROM Operacoes Transacoes WITH(NOLOCK) ' +
		 	  'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS Referencia, ' +
		  '(SELECT TOP 1 Transacoes.Resultado FROM Operacoes Transacoes WITH(NOLOCK) ' +
		 	  'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS Resultado, ' +
          '(SELECT TOP 1 Transacoes.MetodoCustoID FROM Operacoes Transacoes WITH(NOLOCK) ' +
              'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS MetodoCustoID, ' +
		  '(SELECT TOP 1 Transacoes.Asstec FROM Operacoes Transacoes WITH(NOLOCK) ' +
		 	  'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS Asstec, ' +
          'ISNULL((SELECT TOP 1 Transacoes.MatrizFilial FROM Operacoes Transacoes WITH(NOLOCK) ' +
		 	  'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)), 0) AS MatrizFilial, ' +
          '(SELECT TOP 1 dbo.fn_Tradutor(Transacoes.Operacao, ' + aEmpresaData[7] + ', ' + aEmpresaData[8] + ', NULL) FROM Operacoes Transacoes WITH(NOLOCK) ' +
					'WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS Transacao, ' +
          'dbo.fn_Pedido_TemProducao(PedidoID) AS TemProducao, ' +
          'dbo.fn_Pedido_TemRIR(PedidoID) AS TemRIR, ' +          
		  'dbo.fn_Financeiro_TemBoleto(-PedidoID) AS TemBloqueto, ' +
          'dbo.fn_Pedido_NFe(PedidoID) AS PedidoEmitiraNFe, ' +
          'dbo.fn_Pessoa_Fantasia(ProgramaMarketingID, 0) AS ProgramaMarketing, ' +
          '0 AS Prop1, 0 AS Prop2, ' +
          'NULL AS DT_PrevisaoEntrega, ' +
          'dbo.fn_Pedido_Cor(PedidoID, ' + nUserID + ', GETDATE(), 2) AS CorEstado, ' +
          '(SELECT ModeloNF FROM NotasFiscais WITH(NOLOCK) WHERE NotaFiscalID = Pedidos.NotaFiscalID) AS ModeloNF, ' +
          '(SELECT COUNT(*) FROM NotasFiscais a WITH(NOLOCK) WHERE a.NotaFiscalID = Pedidos.NotaFiscalID AND a.TipoEmissaoID = 893) AS NFeFSDA, ' +          
          'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 303, 311), 0)  AS Produto, ' +
          'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 303, 315), 0)  AS ProdutoServico, ' +
          'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 320, 315), 0) AS Servico, ' +
          'ISNULL(dbo.fn_Transacao_PermiteConceito(Pedidos.EmpresaID, Pedidos.TransacaoID, 309, NULL), 0) AS NCM, ' +
          '(SELECT ModeloNF FROM NotasFiscais WITH(NOLOCK) WHERE NotaFiscalID = Pedidos.NotaFiscalID) AS ModeloNF, ' +
		  'dbo.fn_Estado_Pessoa_Parceiro(Pedidos.PessoaId,Pedidos.ParceiroID) as regAtivo, ' +
		  '(SELECT TOP 1 clienteDesativo FROM Operacoes Transacoes WITH(NOLOCK) WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS produtoDesativo, ' +
		  '(SELECT TOP 1 ISNULL(EhRetorno, 0) FROM Operacoes Transacoes WITH(NOLOCK) WHERE (Pedidos.TransacaoID = Transacoes.OperacaoID)) AS EhRetorno, ' +
		  'dbo.fn_Pedido_Importacao(PedidoID) AS EhImportacao, ' +
		  'NULL AS TotalDesconto, ' +
		  'dbo.fn_Fopag_Empresas(NULL,NULL,NULL) AS FopagEmpresas, ' +
		  '0 AS PedidoComplemento, ' +
		  'dbo.fn_Pedido_Vinculado(PedidoID, NULL, NULL, NULL, 1) AS Vinculado, ' +
          '0 AS Pedido_EhCotador ' +
		  'FROM Pedidos WITH(NOLOCK) WHERE PedidoID = 0';

    dso.SQL = sql;
}