<%@  language="VBSCRIPT" enablesessionstate="False" %>
<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>
<!-- //@@ ID e name do arquivo -->
<html id="pedidossup01Html" name="pedidossup01Html">
<head>
    <title></title>
    <%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
        
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/pedidossup01.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/especificsup.js" & Chr(34) & "></script>" & vbCrLf
    %>
    <script id="wndJSProc" language="javascript">
<!--

    //-->
    </script>
</head>
<!-- //@@ -->
<body id="pedidossup01Body" name="pedidossup01Body" language="javascript" onload="return window_onload()">

    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <!-- Campos Gerais 1 -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" datasrc="#dsoStateMachine" datafld="Estado"
            name="txtEstadoID" class="fldGeneral">
        <p id="lbldtPedido" name="lbldtPedido" class="lblGeneral">Data</p>
        <input type="text" id="txtdtPedido" name="txtdtPedido" class="fldGeneral" datasrc="#dsoSup01"
            datafld="V_dtPedido">
        <p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">Pessoa</p>
        <select id="selPessoaID" name="selPessoaID" class="fldGeneral" datasrc="#dsoSup01" datafld="PessoaID">
        </select>
        <input type="image" id="btnFindPessoa" name="btnFindPessoa" class="fldGeneral" title="Preencher Combo"
            width="24" height="23">
        <p id="lblParceiroID" name="lblParceiroID" class="lblGeneral">Parceiro</p>
        <select id="selParceiroID" name="selParceiroID" class="fldGeneral" datasrc="#dsoSup01" datafld="ParceiroID">
        </select>
        <p id="lblTransacaoID" name="lblTransacaoID" class="lblGeneral">Transa��o</p>
        <select id="selTransacaoID" name="selTransacaoID" class="fldGeneral" datasrc="#dsoSup01" datafld="TransacaoID">
        </select>
        <input type="image" id="btnFindTransacao" name="btnFindTransacao" class="fldGeneral" title="Preencher Combo"
            width="24" height="23">
        <!-- Final de Campos Gerais 1 -->
        <!-- Pedido Referencia -->
        <p id="lblPedidoReferenciaID" name="lblPedidoReferenciaID" class="lblGeneral">Pedido Ref</p>
        <input type="text" id="txtPedidoReferenciaID" name="txtPedidoReferenciaID" class="fldGeneral"
            datasrc="#dsoSup01" datafld="PedidoReferenciaID" title="N�mero do Pedido de Refer�ncia">

        <p id="lblPedidoComplemento" name="lblPedidoComplemento" class="lblGeneral">PC</p>
        <input type="checkbox" id="chkPedidoComplemento" name="chkPedidoComplemento" class="fldGeneral" title="� Pedido de Complemento?">
        <!-- Final de Pedido Anterior -->
        <!-- Campos Gerais 2 -->
        <!-- Travado dependendo de implantacao -->
        <p id="lblPedidoMae" name="lblPedidoMae" class="lblGeneral">M�e</p>
        <input type="checkbox" id="chkPedidoMae" name="chkPedidoMae" class="fldGeneral" datasrc="#dsoSup01"
            datafld="EhPedidoMae" title="Pedido � M�e?">
        <!-- Final de Travado dependendo de implantacao -->
        <p id="lblSuspenso" name="lblSuspenso" class="lblGeneral">S</p>
        <input type="checkbox" id="chkSuspenso" name="chkSuspenso" class="fldGeneral" datasrc="#dsoSup01"
            datafld="Suspenso" title="Pedido est� suspenso?">
        <p id="lblPermiteReserva" name="lblPermiteReserva" class="lblGeneral">PR</p>
        <input type="checkbox" id="chkPermiteReserva" name="chkPermiteReserva" class="fldGeneral" datasrc="#dsoSup01"
            datafld="PermiteReservaLongoPrazo" title="Permite reserva a longo prazo?">
        <p id="lblAjustesRejeicaoNFePendentes" name="lblAjustesRejeicaoNFePendentes" class="lblGeneral">ANP</p>
        <input type="checkbox" id="chkAjustesRejeicaoNFePendentes" name="chkAjustesRejeicaoNFePendentes" class="fldGeneral" datasrc="#dsoSup01"
            datafld="AjustesRejeicaoNFePendentes" title="Ajustes da Rejei��o da NFe est�o pendentes?">
        <!-- Final de Campos Gerais 2 -->
        <!-- Nota Fiscal -->
        <p id="lblTemNF" name="lblTemNF" class="lblGeneral">NF</p>
        <input type="checkbox" id="chkTemNF" name="chkTemNF" class="fldGeneral" datasrc="#dsoSup01"
            datafld="TemNF" title="Pedido ter� NF?">
        <p id="lblNotaFiscalID" name="lblNotaFiscalID" class="lblGeneral">Nota Fiscal</p>
        <select id="selNotaFiscalID" name="selNotaFiscalID" class="fldGeneral" datasrc="#dsoSup01"
            datafld="NotaFiscalID">
        </select>
        <p id="lblInvoiceID" name="lblInvoiceID" class="lblGeneral">Invoice</p>
        <input type="text" id="txtInvoiceID" name="txtInvoiceID" class="fldGeneral" datasrc="#dsoSup01"
            datafld="Invoice" title="N�mero da Invoice">
        <p id="lblDataNF" name="lblDataNF" class="lblGeneral">Data NF</p>
        <input type="text" id="txtDataNF" name="DataNF" class="fldGeneral" datasrc="#dsoSup01"
            datafld="dtNotaFiscal">
        <p id="lblDataInvoice" name="lblDataInvoice" class="lblGeneral">Data Invoice</p>
        <input type="text" id="txtDataInvoice" name="DataInvoice" class="fldGeneral" datasrc="#dsoSup01"
            datafld="dtInvoice">
        <!-- Final de Nota Fiscal -->
        <!-- Campos Gerais 3 -->
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" class="fldGeneral" datasrc="#dsoSup01"
            datafld="Observacao">
        <p id="lblNumeroProjeto" name="lblNumeroProjeto" class="lblGeneral">N�mero de Projeto</p>
        <input type="text" id="txtNumeroProjeto" name="txtNumeroProjeto" class="fldGeneral" datasrc="#dsoSup01"
            datafld="NumeroProjeto" title="N�mero do projeto do fornecedor">
        <p id="lblSeuPedido" name="lblSeuPedido" class="lblGeneral">Seu Pedido</p>
        <input type="text" id="txtSeuPedido" name="txtSeuPedido" class="fldGeneral" datasrc="#dsoSup01"
            datafld="SeuPedido" title="N�mero do Pedido do Parceiro">
        <p id="lblOrigemPedidoID" name="lblOrigemPedidoID" class="lblGeneral">Origem Pedido</p>
        <select id="selOrigemPedidoID" name="selOrigemPedidoID" class="fldGeneral" datasrc="#dsoSup01"
            datafld="OrigemPedidoID">
        </select>
        <p id="lblProgramaMarketing" name="lblProgramaMarketing" class="lblGeneral">Marketplace</p>
        <input type="text" id="txtProgramaMarketing" name="txtProgramaMarketing" class="fldGeneral" datasrc="#dsoSup01" datafld="ProgramaMarketing" title="Marketplace origem do pedido.">

        <!-- Final de Campos Gerais 3 -->
        <!-- Valores -->
        <!-- Travado dependendo de implantacao -->
        <p id="lblImpostosIncidencia" name="lblImpostosIncidencia" class="lblGeneral">Inc</p>
        <input type="checkbox" id="chkImpostosIncidencia" name="chkImpostosIncidencia" class="fldGeneral"
            datasrc="#dsoSup01" datafld="ImpostosIncidencia" title="Incide imposto (ICMS/Tax) sobre a nota?">
        <p id="lblImpostosEsquema" name="lblImpostosEsquema" class="lblGeneral">Esq</p>
        <input type="checkbox" id="chkImpostosEsquema" name="chkImpostosEsquema" class="fldGeneral"
            datasrc="#dsoSup01" datafld="ImpostosEsquema" title="Imposto (IPI) tem esquema de d�bito/cr�dito?">
        <p id="lblAtualizaPrecos" name="lblAtualizaPrecos" class="lblGeneral">At</p>
        <input type="checkbox" id="chkAtualizaPrecos" name="chkAtualizaPrecos" class="fldGeneral"
            datasrc="#dsoSup01" datafld="AtualizaPrecos" title="Atualiza os Pre�os do Pedido em fun��o da cota��o da Moeda?">
        <!-- Final de Travado dependendo de implantacao -->
        <p id="lblMoedaID" name="lblMoedaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaID" name="selMoedaID" class="fldGeneral" datasrc="#dsoSup01" datafld="MoedaID">
        </select>
        <p id="lblArredondaPreco" name="lblArredondaPreco" class="lblGeneral">Arr</p>
        <input type="checkbox" id="chkArredondaPreco" name="chkArredondaPreco" class="fldGeneral"
            datasrc="#dsoSup01" datafld="ArredondaPreco" title="Arredonda pre�o?">
        <p id="lblGeraParcelaGare" name="lblGeraParcelaGare" class="lblGeneral">GARE</p>
        <input type="checkbox" id="chkGeraParcelaGare" name="chkGeraParcelaGare" class="fldGeneral"
            datasrc="#dsoSup01" datafld="GeraParcelaGARE" title="Gera parcela GARE?">

        <!-- Campos Read Only -->
        <p id="lblTaxaMoeda" name="lblTaxaMoeda" class="lblGeneral">Taxa</p>
        <input type="text" id="txtTaxaMoeda" name="txtTaxaMoeda" class="fldGeneral" datasrc="#dsoSup01"
            datafld="TaxaMoeda" title="Taxa de convers�o dos valores do pedido para moeda comum">
        <!-- Final de Campos Read Only -->

        <!-- Campos Read Only -->
        <p id="lblNumeroItens" name="lblNumeroItens" class="lblGeneral">Itens</p>
        <input type="text" id="txtNumeroItens" name="txtNumeroItens" class="fldGeneral" datasrc="#dsoSup01"
            datafld="NumeroItens" title="N�mero de �tens do pedido">
        <!-- Final de Campos Read Only -->
        <p id="lblFinanciamentoID" name="lblFinanciamentoID" class="lblGeneral">Financiamento</p>
        <select id="selFinanciamentoID" name="selFinanciamentoID" class="fldGeneral" datasrc="#dsoSup01"
            datafld="FinanciamentoID">
        </select>
        <p id="lblNumeroParcelas" name="lblNumeroParcelas" class="lblGeneral">Parcelas</p>
        <select id="selNumeroParcelas" name="selNumeroParcelas" class="fldGeneral" datasrc="#dsoSup01"
            datafld="NumeroParcelas" title="N�mero de parcelas do pedido">
        </select>
        <p id="lblPrazo1" name="lblPrazo1" class="lblGeneral">Prazo1</p>
        <input type="text" id="txtPrazo1" name="txtPrazo1" class="fldGeneral" datasrc="#dsoSup01"
            datafld="Prazo1" title="Prazo da primeira parcela">
        <p id="lblIncremento" name="lblIncremento" class="lblGeneral">Incr</p>
        <input type="text" id="txtIncremento" name="txtIncremento" class="fldGeneral" datasrc="#dsoSup01"
            datafld="Incremento" title="Incremento das parcelas seguintes">
        <!-- Campos Read Only -->
        <p id="lblPrazoMedioPagamento" name="lblPrazoMedioPagamento" class="lblGeneral">PMP</p>
        <input type="text" id="txtPrazoMedioPagamento" name="txtPrazoMedioPagamento" class="fldGeneral"
            datasrc="#dsoSup01" datafld="PrazoMedioPagamento" title="Prazo M�dio de Pagamento">
        <p id="lblVariacao" name="lblVariacao" class="lblGeneral">Varia��o</p>
        <input type="text" id="txtVariacao" name="txtVariacao" class="fldGeneral" datasrc="#dsoSup01"
            datafld="Variacao" title="Percentual de varia��o dos pre�os">
        <!-- Final de Campos Read Only -->
        <p id="lblPercentualSUP" name="lblPercentualSUP" class="lblGeneral">SUP</p>
        <input type="text" id="txtPercentualSUP" name="txtPercentualSUP" class="fldGeneral" datasrc="#dsoSup01"
            datafld="PercentualSUP" title="Percentual de SUP">
        <!-- Campos Read Only -->
        <p id="lblValorFrete" name="lblValorFrete" class="lblGeneral">Valor Frete</p>
        <input type="text" id="txtValorFrete" name="txtValorFrete" class="fldGeneral" datasrc="#dsoSup01"
            datafld="ValorFrete">
        <p id="lblValorSeguro" name="lblValorSeguro" class="lblGeneral">Valor Seguro</p>
        <input type="text" id="txtValorSeguro" name="txtValorSeguro" class="fldGeneral" datasrc="#dsoSup01"
            datafld="ValorSeguro">
        <p id="lblValorTotalServicos" name="lblValorTotalServicos" class="lblGeneral">
            Total 
				Servi�os
        </p>
        <input type="text" id="txtValorTotalServicos" name="txtValorTotalServicos" class="fldGeneral"
            datasrc="#dsoSup01" datafld="ValorTotalServicos">
        <p id="lblValorTotalProdutos" name="lblValorTotalProdutos" class="lblGeneral">
            Total 
				Produtos
        </p>
        <input type="text" id="txtValorTotalProdutos" name="txtValorTotalProdutos" class="fldGeneral"
            datasrc="#dsoSup01" datafld="ValorTotalProdutos">
        <p id="lblValorTotalImpostos" name="lblValorTotalImpostos" class="lblGeneral">
            Total 
				Impostos
        </p>
        <input type="text" id="txtValorTotalImpostos" name="txtValorTotalImpostos" class="fldGeneral"
            datasrc="#dsoSup01" datafld="ValorTotalImpostos" title="IPI">
        <p id="lblValorTotalPedido" name="lblValorTotalPedido" class="lblGeneral">
            Total 
				Pedido
        </p>
        <input type="text" id="txtValorTotalPedido" name="txtValorTotalPedido" class="fldGeneral"
            datasrc="#dsoSup01" datafld="ValorTotalPedido">
        <p id="lblMargemContribuicao" name="lblMargemContribuicao" class="lblGeneral">MC</p>
        <input type="text" id="txtMargemContribuicao" name="txtMargemContribuicao" class="fldGeneral"
            datasrc="#dsoSup01" datafld="MargemContribuicao">
        <p id="lblValorOutrasDespesas" name="lblValorOutrasDespesas" class="lblGeneral">
            Outras 
				Despesas
        </p>
        <input type="text" id="txtValorOutrasDespesas" name="txtValorOutrasDespesas" class="fldGeneral"
            datasrc="#dsoSup01" datafld="ValorOutrasDespesas">
        <p id="lblFrete" name="lblFrete" class="lblGeneral">Frete</p>
        <input type="checkbox" id="chkFrete" name="chkFrete" datasrc="#dsoSup01" datafld="Frete" class="fldGeneral" title="� frete pago?"></input>
        <!-- Campo Total Desconto (casos Microsoft) -->
        <p id="lblTotalDesconto" name="lblTotalDesconto" class="lblGeneral">Total Desconto</p>
        <input type="text" id="txtTotalDesconto" name="txtTotalDesconto" class="fldGeneral"
            datasrc="#dsoSup01" datafld="TotalDesconto">
        <!-- Final de Campos Read Only -->
        <!-- Final de Valores -->
        <!-- Transporte -->
        <p id="lblTransportadoraID" name="lblTransportadoraID" class="lblGeneral">Transportadora</p>
        <select id="selTransportadoraID" name="selTransportadoraID" class="fldGeneral" datasrc="#dsoSup01"
            datafld="TransportadoraID">
        </select>
        <p id="lblMeioTransporteID" name="lblMeioTransporteID" class="lblGeneral">Meio Transporte</p>
        <select id="selMeioTransporteID" name="selMeioTransporteID" class="fldGeneral" datasrc="#dsoSup01"
            datafld="MeioTransporteID">
        </select>
        <p id="lblModalidadeTransporteID" name="lblModalidadeTransporteID" class="lblGeneral">Modalidade</p>
        <select id="selModalidadeTransporteID" name="selModalidadeTransporteID" class="fldGeneral"
            datasrc="#dsoSup01" datafld="ModalidadeTransporteID">
        </select>

        <p id="lbldtPrevisaoEntrega" name="lbldtPrevisaoEntrega" class="lblGeneral">Previs�o Entrega</p>
        <input type="text" id="txtdtPrevisaoEntrega" name="txtdtPrevisaoEntrega" datasrc="#dsoSup01" datafld="V_dtPrevisaoEntrega" class="fldGeneral"></input>

        <p id="lblPrevisaoEntrega" name="lblPrevisaoEntrega" class="lblGeneral">Previs�o Entrega</p>
        <input type="text" id="txtPrevisaoEntrega" name="txtPrevisaoEntrega" class="fldGeneral"></input>

        <p id="lblAtrasoExterno" name="lblAtrasoExterno" class="lblGeneral">Ext</p>
        <input type="checkbox" id="chkAtrasoExterno" name="chkAtrasoExterno" class="fldGeneral" datasrc="#dsoSup01" datafld="AtrasoExterno" title="� problema de atraso externo?"></input>

        <p id="lblDepositoID" name="lblDepositoID" class="lblGeneral">Dep�sito</p>
        <select id="selDepositoID" name="selDepositoID" class="fldGeneral" datasrc="#dsoSup01" datafld="DepositoID"></select>

        <p id="lblContaID" name="lblContaID" class="lblGeneral">Conta</p>
        <select id="selContaID" name="selContaID" class="fldGeneral" datasrc="#dsoSup01" datafld="ContaID"></select>

        <p id="lblLocalEntregaID" name="lblLocalEntregaID" class="lblGeneral">Local de Entrega</p>
        <select id="selLocalEntregaID" name="selLocalEntregaID" class="fldGeneral" datasrc="#dsoSup01" datafld="LocalEntregaID"></select>

        <p id="lblShipTo" name="lblShipTo" class="lblGeneral">Ship to</p>
        <select id="selShipTo" name="selShipTo" class="fldGeneral" datasrc="#dsoSup01" datafld="ShipToID"></select>
        <input type="image" id="btnFindShipTo" name="btnFindShipTo" class="fldGeneral" title="Preencher Combo"
            width="24" height="23">
        <!-- Final de Transporte -->
    </div>
</body>
</html>
