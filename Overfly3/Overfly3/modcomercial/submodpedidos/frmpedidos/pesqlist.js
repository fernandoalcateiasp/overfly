/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form pedidos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_FirstLoad = true;
var glb_QtdImpressoras = 0;
var empresaData = getCurrEmpresaData();

var dsoListData01 = new CDatatransport('dsoListData01');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoPropsPL = new CDatatransport('dsoPropsPL');
var dsoImpressorasNFe = new CDatatransport('dsoImpressorasNFe');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
pesqlistIsVisibleAndUnlocked()
btnBarClicked(controlBar, btnClicked)
modalInformForm(idElement, param1, param2)
specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload() {

    var empresaID = empresaData[0];
    //window.top.showAlert_AddDate('ida windowOnLoad_1stPart');
    windowOnLoad_1stPart(); //aqui
    //window.top.showAlert_AddDate('volta windowOnLoad_1stPart');
    //window.top.showAlert();

    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('Pedido', 'E', 'Data', 'Pessoa', 'Tran', 'NF', 'Data NF', 'Total', 'Transportadora',
		'Peso', 'Vol', 'Observa��o', 'Dep�sito', 'Colaborador', 'Previs�o Entrega', 'Libera��o', 'Chegada Portador', 'Localiza��o',
		'CorPedido', 'CorEstado', 'CorPessoa', 'CorValorTotal', 'CorTransportadora', 'CorPesoBruto', 'CorVolumes', 'CorColaborador',
		'CorPrevisaoEntrega', 'CorDeposito', 'CorLiberacao');

    //@@ Formatacao das colunas do grid de pesquisa, definicao opcional
    // padrao do framework = null se variavel nao for definida aqui
    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    glb_aCOLPESQFORMAT = new Array('', '', dTFormat, '', '', '', dTFormat, '###,###,###,###.00', '', '####.00', '####', '', '', '',
		'', dTFormat + ' hh:mm', dTFormat + ' hh:mm', '', '', '', '', '', '', '', '', '', '');

    //window.top.showAlert_AddDate('ida windowOnLoad_2ndPart');
    windowOnLoad_2ndPart();
    //window.top.showAlert_AddDate('volta windowOnLoad_2stPart');
    //window.top.showAlert();

    // Totaliza as impressoras de NFe da Empresa
    setConnection(dsoImpressorasNFe);
    sSQL = "SELECT COUNT(1) AS QtdImpressoras " +
                "FROM RelacoesPesRec a WITH (NOLOCK) " +
                    "INNER JOIN RelacoesPesRec_Impressoras b WITH (NOLOCK) ON a.RelacaoID = b.RelacaoID " +
                "WHERE (a.ObjetoID = 999 AND a.TipoRelacaoID = 12 AND a.EstadoID = 2 AND a.SujeitoID = " + empresaID + ") ";

    dsoImpressorasNFe.SQL = sSQL;
    dsoImpressorasNFe.ondatasetcomplete = dsoImpressorasNFe_DSC;
    dsoImpressorasNFe.refresh();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked() {
    window.top.showAlert_AddDate('Inicio Visible');
    var nColCorPedido = getColIndexByColKey(fg, 'CorPedido');
    var nColCorEstado = getColIndexByColKey(fg, 'CorEstado');
    var nColCorPessoa = getColIndexByColKey(fg, 'CorPessoa');
    var nColCorValorTotal = getColIndexByColKey(fg, 'CorValorTotal');
    var nColCorTransportadora = getColIndexByColKey(fg, 'CorTransportadora');
    var nColCorPesoBruto = getColIndexByColKey(fg, 'CorPesoBruto');
    var nColCorVolumes = getColIndexByColKey(fg, 'CorVolumes');
    var nColCorColaborador = getColIndexByColKey(fg, 'CorColaborador');
    var nColCorPrevisaoEntrega = getColIndexByColKey(fg, 'CorPrevisaoEntrega');
    var nColCorDeposito = getColIndexByColKey(fg, 'CorDeposito');
    var nColCorLiberacao = getColIndexByColKey(fg, 'CorLiberacao');
    var empresaID = empresaData[0];

    fg.ColDataType(getColIndexByColKey(fg, 'LiberacaoPedido')) = 7; // format data

    if ((fg.Cols >= 4) && (fg.Rows > 1))
        fg.FrozenCols = 4;

    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Pessoa', 'Detalhar Rela��o', 'Pedido Clone', 'Kardex', 'Resultados',
		'Aprovar margem m�nima', 'Imprimir DANFEs', 'Dados Importa��o', 'Despacho', 'Apura��o de Impostos', 'Pagamento de comiss�es', 'Pesquisa NFe']);

    var nContexto = getCmbCurrDataInControlBar('sup', 1);

    if (nContexto[1] == 5112) {
        setupEspecBtnsControlBar('sup', 'HHHDDDHHHHDHHHH');
    }

    if (nContexto[1] == 5111) {
        setupEspecBtnsControlBar('sup', 'HHHDDHHHHHDDHHH');
    }

    var contexto = getCmbCurrDataInControlBar('sup', 2);

    if (((contexto[1] == 41025) || (contexto[1] == 41037)) && (empresaID != 7))
        alignColsInGrid(fg, [1, 2, 7, 9, 11, 12]);
    else
        alignColsInGrid(fg, [0, 5, 7, 9, 10]);

    fg.Redraw = 0;
    fg.FontSize = '8';

    var i, j, lastRow;

    if (fg.Cols > 1) {
        fg.ColHidden(nColCorPedido) = true;
        fg.ColHidden(nColCorEstado) = true;
        fg.ColHidden(nColCorPessoa) = true;
        fg.ColHidden(nColCorValorTotal) = true;
        fg.ColHidden(nColCorTransportadora) = true;
        fg.ColHidden(nColCorPesoBruto) = true;
        fg.ColHidden(nColCorVolumes) = true;
        fg.ColHidden(nColCorColaborador) = true;
        fg.ColHidden(nColCorPrevisaoEntrega) = true;
        fg.ColHidden(nColCorDeposito) = true;
        fg.ColHidden(nColCorLiberacao) = true;

        // Se for Embalagem ou Embalagem (filial) para empresa do Brasil, exibe vinculados
        if (((contexto[1] == 41025) || (contexto[1] == 41037)) && (empresaID != 7)) {
            var nColPedidoID2 = getColIndexByColKey(fg, 'PedidoID2');
            var nColCorVinculado = getColIndexByColKey(fg, 'CorVinculado');
            fg.ColHidden(nColPedidoID2) = true;
            fg.ColHidden(nColCorVinculado) = true;
            fg.MergeCells = 1;
            fg.MergeCol(1) = true;
        }
    }

    glb_GridIsBuilding = true;

    lastRow = fg.Row;

    FillStyle = 1;
    for (i = 1; i < fg.Rows; i++) {
        // Pedido
        if (fg.TextMatrix(i, nColCorPedido) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'PedidoID'), i, getColIndexByColKey(fg, 'PedidoID')) = eval(fg.TextMatrix(i, nColCorPedido));

        // Estado
        if (fg.TextMatrix(i, nColCorEstado) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'Estado'), i, getColIndexByColKey(fg, 'Estado')) = eval(fg.TextMatrix(i, nColCorEstado));

        // Pessoa
        if (fg.TextMatrix(i, nColCorPessoa) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'Fantasia'), i, getColIndexByColKey(fg, 'Fantasia')) = eval(fg.TextMatrix(i, nColCorPessoa));

        // Valor total
        if (fg.TextMatrix(i, nColCorValorTotal) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'ValorTotalPedido'), i, getColIndexByColKey(fg, 'ValorTotalPedido')) = eval(fg.TextMatrix(i, nColCorValorTotal));

        // Transportadora
        if (fg.TextMatrix(i, nColCorTransportadora) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'Transportadora'), i, getColIndexByColKey(fg, 'Transportadora')) = eval(fg.TextMatrix(i, nColCorTransportadora));

        // Total Peso Bruto
        if (fg.TextMatrix(i, nColCorPesoBruto) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'TotalPesoBruto'), i, getColIndexByColKey(fg, 'TotalPesoBruto')) = eval(fg.TextMatrix(i, nColCorPesoBruto));

        // Numero Volumes
        if (fg.TextMatrix(i, nColCorVolumes) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'NumeroVolumes'), i, getColIndexByColKey(fg, 'NumeroVolumes')) = eval(fg.TextMatrix(i, nColCorVolumes));

        // Colaborador
        if (fg.TextMatrix(i, nColCorColaborador) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'Colaborador'), i, getColIndexByColKey(fg, 'Colaborador')) = eval(fg.TextMatrix(i, nColCorColaborador));

        // Previsao Entrega
        if (fg.TextMatrix(i, nColCorPrevisaoEntrega) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'PrevisaoEntrega'), i, getColIndexByColKey(fg, 'PrevisaoEntrega')) = eval(fg.TextMatrix(i, nColCorPrevisaoEntrega));

        // Deposito
        if (fg.TextMatrix(i, nColCorDeposito) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'Deposito'), i, getColIndexByColKey(fg, 'Deposito')) = eval(fg.TextMatrix(i, nColCorDeposito));

        // LiberacaoPedido
        if (fg.TextMatrix(i, nColCorLiberacao) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'LiberacaoPedido'), i, getColIndexByColKey(fg, 'LiberacaoPedido')) = eval(fg.TextMatrix(i, nColCorLiberacao));

        // Se for Embalagem ou Embalagem (filial) para empresa do Brasil, pinta celula da respectiva cor
        if (((contexto[1] == 41025) || (contexto[1] == 41037)) && (empresaID != 7)) {
            // Vinculado
            if (fg.TextMatrix(i, nColCorVinculado) != '')
                fg.Cell(6, i, getColIndexByColKey(fg, 'VinculadoID'), i, getColIndexByColKey(fg, 'VinculadoID')) = eval(fg.TextMatrix(i, nColCorVinculado));
        }
    }
    FillStyle = 0;

    if (lastRow >= 1)
        fg.Row = lastRow;

    glb_GridIsBuilding = false;

    fg.Redraw = 1;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    if (glb_FirstLoad) {
        selRegistros.selectedIndex = 1;
        glb_FirstLoad = false;
    }
    window.top.showAlert_AddDate('fim Visible');
    window.top.showAlert();

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    var contexto = 0;
    var nProcedimentoID = 0;
    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];
    var strPars = new String();
    var sSQL;

    if (controlBar == 'SUP') {
        if (btnClicked == 1) {
            if (fg.Rows > 1) {
                __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
            }
            else {
                window.top.overflyGen.Alert('Selecione um registro.');
            }
        }
        // usuario clicou botao imprimir
        else if (btnClicked == 2)
            openModalPrint();
        else if (btnClicked == 3) {
            var nAncoraID;

            contexto = getCmbCurrDataInControlBar('sup', 1);

            if (contexto[1] == 5111)
            {
                nProcedimentoID = 740;
                nAncoraID = 431;
            }

            else
            {
                nProcedimentoID = 720;
                nAncoraID = 431;
            }

            window.top.openModalControleDocumento('PL', '', nProcedimentoID, null, nAncoraID, 'T');
        }
        else if (btnClicked == 10) {
            // Verifica se existe(m) impressora(s) para NFe.  HM - 20/03/2010
            // Se existir exibe modal
            if (glb_QtdImpressoras > 0) {
                strPars = '?sCaller=' + escape('PL');
                strPars += '&nEmpresaID=' + escape(nEmpresaID);
                htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalnfe.asp' + strPars;
                showModalWin(htmlPath, new Array(1000, 560));
                //showModalWin(htmlPath, new Array(775,480));   
            }
            // Exibe alerta se n�o existir impressora para NFe.
            else {
                if (window.top.overflyGen.Alert('Empresa sem impressora de NFe cadastrada.') == 0)
                    return null;
            }
        }
        else if (btnClicked == 7) {
            if (fg.Rows > 1) {
                openModalKardex(getCellValueByColKey(fg, 'PedidoID', fg.Row));
            }
            else {
                window.top.overflyGen.Alert('Selecione um registro.');
            }
        }
        else if (btnClicked == 8) {
            if (fg.Rows > 1) {
                openModalResultados(getCellValueByColKey(fg, 'PedidoID', fg.Row));
            }
            else {
                window.top.overflyGen.Alert('Selecione um registro.');
            }
        }
        else if (btnClicked == 12) {
            openModalDespacho();
        }
        else if (btnClicked == 13) {
            openModalApuracaoImpostos();
        }
        else if (btnClicked == 9) {
            openModalAprovarMargemMinima();
        }
        else if (btnClicked == 14) {
            openModalPagamentoComissoes();
        }
        else if (btnClicked == 15) //Pequisar XML
            openModalPesquisaXML();
        else if (btnClicked == 6) //Pedido Logistica
            openmodalpedidoclone(nEmpresaID);
    }
}

function openModalDespacho() {
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var sEmpresaFantasia = empresaData[3];
    var nEmpresaID = empresaData[0];
    var nUserID = getCurrUserID();

    strPars = '?sCaller=' + escape('PL');
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nUserID=' + escape(nUserID);
    strPars += '&sEmpresaFantasia=' + escape(sEmpresaFantasia);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modaldespacho.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 560));
}

function openModalApuracaoImpostos() {
    var htmlPath;
    var strPars = new String();
    var nEmpresaData = getCurrEmpresaData();

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nContextoID=' + escape(0);
    strPars += '&nPaisEmpresaID=' + escape(nEmpresaData[1]);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalapuracaoimpostos.asp' + strPars;
    showModalWin(htmlPath, new Array(810, 465));
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Modal de pagamento de comiss�es
    if (idElement.toUpperCase() == 'MODALPAGAMENTOCOMISSOESHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // param2 != null e o id da pessoa existente no cadaqstro
            // com este documento
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // Simula cancelamento de inclusao
            if (param2 == null)
                __btn_CANC('SUP');
            else if (param2 == false) {
                return 0;
            }
            else {
                glb_localTimerInt = window.setInterval('showOrderByID(' + param2 + ')', 10, 'JavaScript');
            }

            // nao mexer
            return 0;
        }
    }

    // Modal de Pesquisa de Arquivos Nfe-XML
    if (idElement.toUpperCase() == 'MODALPESQUISAXMLHTML') {
        //Cliclou o botao X (fechar) da Modal
        if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }

    // Modal de Pedidos Clone
    if (idElement.toUpperCase() == 'MODALPEDIDOCLONEHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }


    // Modal de aprovacao de margem minima
    if (idElement.toUpperCase() == 'MODALAPROVARMARGEMMINIMAHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }

    // Modal de impressao
    else if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALDESPACHOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALAPURACAOIMPOSTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALPROGRAMACAOENTREGAHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALKARDEXHTML') {
        if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALRESULTADOSHTML') {
        if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Criada pelo programador
Na inclusao, retorno da modal de numero de documento, esta funcao
abre o detalhe de um usuario ja cadastrado
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function showOrderByID(PedidoID) {
    if (glb_localTimerInt != null) {
        window.clearInterval(glb_localTimerInt);
        glb_localTimerInt = null;
    }

    sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL',
      [PedidoID, null, null]);
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch() {
    var contexto = getCmbCurrDataInControlBar('sup', 2);
    var nEmpresaID = empresaData[0];

    // Se for Embalagem ou Embalagem (filial) para empresa do Brasil, exibe vinculados
    if (((contexto[1] == 41025) || (contexto[1] == 41037)) && (nEmpresaID != 7)) {
        glb_COLPESQORDER = new Array('Pedido2', 'Vinc', 'Pedido', 'E', 'Data', 'Pessoa', 'Tran', 'NF', 'Data NF', 'Total', 'Transportadora',
		    'Peso', 'Vol', 'Observa��o', 'Dep�sito', 'Colaborador', 'Previs�o Entrega', 'Libera��o', 'Chegada Portador', 'Localiza��o',
		    'CorPedido', 'CorEstado', 'CorPessoa', 'CorValorTotal', 'CorTransportadora', 'CorPesoBruto', 'CorVolumes', 'CorColaborador',
		    'CorPrevisaoEntrega', 'CorDeposito', 'CorVinculado', 'CorLiberacao');

        //@@ Formatacao das colunas do grid de pesquisa, definicao opcional
        // padrao do framework = null se variavel nao for definida aqui
        var dTFormat = '';
        if (DATE_FORMAT == "DD/MM/YYYY")
            dTFormat = 'dd/mm/yyyy';
        else if (DATE_FORMAT == "MM/DD/YYYY")
            dTFormat = 'mm/dd/yyyy';

        glb_aCOLPESQFORMAT = new Array('', '', '', '', dTFormat, '', '', '', dTFormat, '###,###,###,###.00', '', '####.00', '####', '', '', '',
                                        '', dTFormat + ' hh:mm', dTFormat + ' hh:mm', '', '', '', '', '', '', '', '', '', '', '');
    }
    else {
        //@@ Ordem e titulos das colunas do grid de pesquisa
        glb_COLPESQORDER = new Array('Pedido', 'E', 'Data', 'Pessoa', 'Tran', 'NF', 'Data NF', 'Total', 'Transportadora',
		    'Peso', 'Vol', 'Observa��o', 'Dep�sito', 'Colaborador', 'Previs�o Entrega', 'Libera��o', 'Chegada Portador', 'Localiza��o',
		    'CorPedido', 'CorEstado', 'CorPessoa', 'CorValorTotal', 'CorTransportadora', 'CorPesoBruto', 'CorVolumes', 'CorColaborador',
		    'CorPrevisaoEntrega', 'CorDeposito', 'CorLiberacao');

        //@@ Formatacao das colunas do grid de pesquisa, definicao opcional
        // padrao do framework = null se variavel nao for definida aqui
        var dTFormat = '';
        if (DATE_FORMAT == "DD/MM/YYYY")
            dTFormat = 'dd/mm/yyyy';
        else if (DATE_FORMAT == "MM/DD/YYYY")
            dTFormat = 'mm/dd/yyyy';

        glb_aCOLPESQFORMAT = new Array('', '', dTFormat, '', '', '', dTFormat, '###,###,###,###.00', '', '####.00', '####', '', '', '',
                                        '', dTFormat + ' hh:mm', dTFormat + ' hh:mm', '', '', '', '', '', '', '', '', '', '');
    }

    return selPesquisa.options[selPesquisa.selectedIndex].value;

    //@@ da automacao -> retorno padrao
    // return null;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    // por questoes esteticas, coloca foco no grid de pesquisa
    if (fg.disabled == false)
        fg.focus();

    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&currDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(428, 400));
}

function adjustOptionsNumRegsInCombo(numOptions) {
    var i, cmb;

    cmb = selRegistros;

    for (i = cmb.length - 1; i >= numOptions; i--) {
        cmb.remove(i);
    }
}

function openModalAprovarMargemMinima() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 770;
    var nHeight = 462;
    var empresaData = getCurrEmpresaData();

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nUsuarioID=' + escape(getCurrUserID());
    strPars += '&nEmpresaID=' + escape(empresaData[0]);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalaprovarmargemminima.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}


function openModalPagamentoComissoes() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 996;
    var nHeight = 540;
    var empresaData = getCurrEmpresaData();

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalpagamentocomissoes.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}


// Retorno
function dsoImpressorasNFe_DSC() {
    if (!((dsoImpressorasNFe.recordset.BOF) && (dsoImpressorasNFe.recordset.EOF))) {
        glb_QtdImpressoras = dsoImpressorasNFe.recordset['QtdImpressoras'].value;
    }
}

function openModalPesquisaXML() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 650;
    var nHeight = 150;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalpesquisaXML.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}
function openModalKardex(PedidoID) {
    var htmlPath;


    var strPars = new String();

    // mandar os parametros para o servidor
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nPedidoID=' + escape(PedidoID);

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalKardex.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 543));
}
function openModalResultados(PedidoID) {
    var htmlPath;


    var strPars = new String();

    // mandar os parametros para o servidor
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nPedidoID=' + escape(PedidoID);

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalresultados.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 543));
}

function openmodalpedidoclone(empresaID) {
    var htmlPath;
    var strPars = new String();
    var nWidth = 999;
    var nHeight = 400;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&nUserID=' + escape(getCurrUserID());

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalpedidoclone.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}
