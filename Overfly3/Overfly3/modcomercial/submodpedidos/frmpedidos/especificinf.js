/********************************************************************
especificinf.js
frmpedidos 

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Pedidos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;
var glb_bModoNumeroSerieFull = false;
var glb_MAX_NUMSERIE = 200;
var glb_duplicaGridRows = null;
var nEmpresaData = getCurrEmpresaData();
// FINAL DE VARIAVEIS GLOBAIS ***************************************
/********************************************************************

INDICE DAS FUNCOES:

setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
fillCmbPastasInCtlBarInf(nTipoRegistroID)    
criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID) {
    
    var bAsstec = false;
    var sSql = '';

    setConnection(dso);

    //@@


    if (folderID == 20008) // Observacoes
    {
        dso.SQL = 'SELECT a.PedidoID, a.Observacoes ' +
                  'FROM Pedidos a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.PedidoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
        //@@
    else if (folderID == 20009) // Prop/Altern
    {
        dso.SQL = 'SELECT a.PedidoID, a.ProprietarioID, a.AlternativoID ' +
                  'FROM Pedidos a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.PedidoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 24001) // Embalagem/Expedicao
    {
        dso.SQL = 'SELECT PedidoID, TotalPesoBruto as TotalPesoBruto, TotalPesoLiquido, dbo.fn_Pedido_Cubagem(PedidoID) AS TotalCubagem, ' +
            'NumeroVolumes, Conhecimento, Portador, DocumentoPortador, DocumentoFederalPortador, PlacaVeiculo,UFVeiculo, RNTC, dtChegadaPortador, ' +
			'CONVERT(VARCHAR(10), dtChegadaPortador, ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
				'CONVERT(VARCHAR(10), dtChegadaPortador, 108) AS V_dtChegadaPortador, ' +
			'CONVERT(VARCHAR(10), dbo.fn_Pedido_Datas(PedidoID, 2), ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
				'CONVERT(VARCHAR(10), dbo.fn_Pedido_Datas(PedidoID, 2), 108) AS dtEntradaSaida, ' +
            'CONVERT(NUMERIC(11), DATEDIFF(mi, dtChegadaPortador, dbo.fn_Pedido_Datas(PedidoID, 2))) AS TempoExpedicao, MinutaID, Observacao ' +
            'FROM Pedidos WITH(NOLOCK) ' +
            'WHERE ' + sFiltro + 'PedidoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.RegistroID = ' + idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24002) // Itens
    {
        dso.SQL = 'SELECT a.*, dbo.fn_Produto_PPB(a.ProdutoID, ' + nEmpresaData[4] + ', NULL) AS TemPPB, dbo.fn_PedItem_PrevisaoEntrega(a.PedItemID) AS PrevisaoEntrega ' +
                        'FROM Pedidos_Itens a WITH(NOLOCK) WHERE ' +
            sFiltro + 'a.PedidoID = ' + idToFind + ' ORDER BY a.Ordem';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24003) // Pedidos Parcelas
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Pedidos_Parcelas WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'PedidoID = ' + idToFind + ' ' +
                  'ORDER BY Ordem';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24011) // Pedidos Aprova��es Especiais
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Pedidos_AprovacoesEspeciais WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'PedidoID = ' + idToFind + ' ' +
                  'ORDER BY TipoAprovacaoID, PedAprovacaoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24012) // Comiss�es
    {
        dso.SQL = 'SELECT a.FinComissaoID, a.FinanceiroID, d.Fantasia AS Cliente, ' +
	                        'e.Fantasia AS Parceiro, a.Valor, h.Fantasia, c.PedidoID, f.NotaFiscal AS NF, f.dtNotaFiscal AS DataNF, ' +
	                        'g.Fantasia AS Usuario, a.dtAssociacao AS DataAssociacao ' +
                    'FROM Financeiro_Comissoes a WITH(NOLOCK) ' +
	                    'INNER JOIN Pedidos_Parcelas b WITH(NOLOCK) ON(a.PedParcelaID = b.PedParcelaID) ' +
	                    'LEFT OUTER JOIN Pedidos c WITH(NOLOCK) ON(c.PedidoID = b.PedidoID) ' +
	                    'LEFT OUTER JOIN Pessoas d WITH(NOLOCK) ON(d.PessoaID = c.PessoaID) ' +
                        'LEFT OUTER JOIN Pessoas e WITH(NOLOCK) ON(e.PessoaID = c.ParceiroID) ' +
	                    'LEFT OUTER JOIN NotasFiscais f WITH(NOLOCK) ON(c.PedidoID = f.PedidoID) AND (c.NotaFiscalID = f.NotaFiscalID) ' +
	                    'LEFT OUTER JOIN Pessoas g WITH(NOLOCK) ON(g.PessoaID = a.UsuarioID) ' +
	                    'LEFT OUTER JOIN Pessoas h WITH(NOLOCK) ON(c.EmpresaID = h.PessoaID) ' +
					'WHERE (a.PedidoID = ' + idToFind + ') ';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24004) // Mensagens
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Pedidos_Mensagens WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'PedidoID = ' + idToFind + ' ' +
                  'ORDER BY Ordem';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24005) // Numero de S�rie
    {
        bAsstec = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
			'dsoSup01.recordset[' + '\'' + 'Asstec' + '\'' + '].value');

        //dso.SQL = 'EXEC sp_Produto_NumerosSerie NULL, NULL, NULL, ' + idToFind + ', ' + (bAsstec ? 'NULL' : glb_MAX_NUMSERIE) + ', NULL';

        sSql = 'SET NOCOUNT ON ' +
                'DECLARE @TempTable TABLE (RegistroID INT IDENTITY PRIMARY KEY, Produto VARCHAR(25), ProdutoID INT, QuantidadeItem INT, ' +
		                'QuantidadePorCaixa INT, QuantidadeCaixas INT, QuantidadeUnidades INT, CaixaID INT, Caixa INT, ' +
		                'QuantidadeGatilhada INT, NumeroSerie VARCHAR(25), Estoque VARCHAR(10), OrdemProducaoID INT, AsstecID INT, ' +
		                'Colaborador VARCHAR(20), NumeroSerieID INT, NumMovimentoID INT, PedProdutoID INT) ' +
                'DECLARE @PedidoID INT, @Registros INT, @MaxLinha INT ' +
                'SET @PedidoID = ' + idToFind + ' ' +
                'SET @MaxLinha = 200 ' +
                'INSERT INTO @TempTable  ' +
                'SELECT Produtos.Conceito AS Produto, Produtos.ConceitoID AS ProdutoID, ' +
		                'dbo.fn_Pedido_NumeroSerieQuantidade(PedNS.PedidoID, NULL, Produtos.ConceitoID, 1) AS QuantidadeItem, ' +
		                'ISNULL(dbo.fn_Produto_PesosMedidas(Produtos.ConceitoID, NULL, 10), 1) AS QuantidadePorCaixa, ' +
		                'dbo.fn_Pedido_NumeroSerieQuantidade(PedNS.PedidoID, NULL, Produtos.ConceitoID, 4) AS QuantidadeCaixas, ' +
		                'dbo.fn_Pedido_NumeroSerieQuantidade(PedNS.PedidoID, NULL, Produtos.ConceitoID, 5) AS QuantidadeUnidades, ' +
		                'PedNS.CaixaID, PedNS.Caixa, PedNS.Quantidade AS QuantidadeGatilhada, PedNS.NumeroSerie, Estoque.ItemAbreviado AS Estoque, ' +
		                'PedNS.OrdemProducaoID, PedNS.AsstecID, Pessoas.Fantasia AS Colaborador, PedNs.NumeroSerieID, PedNS.NumMovimentoID, ' +
		                'PedNS.PedProdutoID ' +
                'FROM (SELECT a.PedidoID, a.ProdutoID, a.OrdemProducaoID, a.Quantidade, a.AsstecID, a.EstoqueID, a.ColaboradorID, NULL As NumMovimentoID, ' +
		                'a.PedProdutoID, NULL AS Caixa, NULL AS CaixaID, NULL AS NumeroSerieID, NULL AS NumeroSerie ' +
			                'FROM Pedidos_ProdutosSeparados a WITH(NOLOCK) ' +
			                'WHERE a.PedidoID = @PedidoID ' +
		                'UNION ' +
		                'SELECT a.PedidoID, a.ProdutoID, a.OrdemProducaoID, a.Quantidade, a.AsstecID, a.EstoqueID, a.ColaboradorID, NULL As NumMovimentoID, ' +
			                'a.PedProdutoID, NULL AS Caixa, NULL AS CaixaID, NULL AS NumeroSerieID, a.Lote AS NumeroSerie ' +
			                'FROM Pedidos_ProdutosLote a WITH(NOLOCK) ' +
			                'WHERE a.PedidoID = @PedidoID ' +
		                'UNION ' +
		                'SELECT a.PedidoID, b.ProdutoID, a.OrdemProducaoID, ' +
				                '(CASE WHEN b.CaixaID IS NULL THEN NULL ELSE (SELECT aa.Quantidade FROM vw_NumSerie_CxID aa WITH(NOEXPAND) WHERE (aa.CaixaID = b.CaixaID)) END) AS Quantidade, ' +
				                'a.AsstecID, a.EstoqueID, a.ColaboradorID, a.NumMovimentoID, NULL PedProdutoID, ' +
				                'dbo.fn_NumeroSerie_Caixa(b.CaixaID, a.PedidoID, b.ProdutoID) AS Caixa, ' +
				                'b.CaixaID AS CaixaID, b.NumeroSerieID, b.NumeroSerie ' +
			                'FROM NumerosSerie_Movimento a WITH(NOLOCK) ' +
					                'INNER JOIN NumerosSerie b WITH(NOLOCK) ON a.NumeroSerieID = b.NumeroSerieID ' +
			                'WHERE a.PedidoID = @PedidoID) PedNS ' +
		                'INNER JOIN Conceitos Produtos WITH(NOLOCK) ON PedNS.ProdutoID = Produtos.ConceitoID ' +
		                'INNER JOIN Pessoas WITH(NOLOCK) ON PedNS.ColaboradorID = Pessoas.PessoaID ' +
		                'LEFT JOIN TiposAuxiliares_Itens Estoque WITH(NOLOCK) ON Estoque.ItemID = PedNS.EstoqueID ' +
                'ORDER BY Produtos.Conceito, ISNULL(Caixa, 999999999), PedNS.Quantidade, PedNS.NumeroSerie ';

        if (bAsstec)
            sSql += 'SELECT * FROM @TempTable ORDER BY RegistroID ';
        else
            sSql += 'SELECT @Registros = COUNT(*) FROM @TempTable ' +
                        'IF (@MaxLinha < @Registros) ' +
	                        'SELECT Produto, ProdutoID, QuantidadeItem, QuantidadePorCaixa, QuantidadeCaixas, QuantidadeUnidades, ' +
				                        'NULL AS CaixaID, COUNT(DISTINCT CaixaID) AS Caixa, COUNT(*) AS QuantidadeGatilhada, NULL AS NumeroSerie, NULL AS Estoque, NULL AS OrdemProducaoID, ' +
				                        'NULL AS AsstecID, NULL AS Colaborador, NULL AS NumeroSerieID, NULL AS NumMovimentoID, NULL AS PedProdutoID ' +
			                        'FROM @TempTable ' +
			                        'GROUP BY Produto, ProdutoID, QuantidadeItem, QuantidadePorCaixa, QuantidadeCaixas, QuantidadeUnidades ' +
			                        'ORDER BY Produto ' +
                        'ELSE ' +
	                        'SELECT * FROM @TempTable ';
        sSql += 'SET NOCOUNT OFF';

        dso.SQL = sSql;

        return 'dso01Grid_DSC';
    }
    else if (folderID == 24006) // Pedidos
    {
        dso.SQL = 'SELECT a.PedidoID, c.RecursoAbreviado AS Estado, b.dtPedido, ' +
						'd.NotaFiscal, d.dtNotaFiscal, g.Codigo AS Codigo, f.SimboloMoeda, ' +
						'a.ProdutoID, e.Conceito AS Produto, a.Quantidade, ' +
						'dbo.fn_PedidoItem_Totais2(a.PedItemID, 7) AS ValorUnitario, ' +
						'dbo.fn_PedidoItem_Totais2(a.PedItemID, 8) AS ValorTotal ' +
                  'FROM Pedidos_Itens z WITH(NOLOCK) ' +
                    'INNER JOIN Pedidos_Itens a WITH(NOLOCK) ON (z.PedItemID = a.PedItemReferenciaID) ' +
                    'INNER JOIN Pedidos b WITH(NOLOCK) ON (a.PedidoID = b.PedidoID) ' +
                    'INNER JOIN Recursos c WITH(NOLOCK) ON (b.EstadoID = c.RecursoID) ' +
                    'LEFT OUTER JOIN NotasFiscais d WITH(NOLOCK) ON (b.NotaFiscalID = d.NotaFiscalID AND d.EstadoID=67) ' +
                    'INNER JOIN Conceitos e WITH(NOLOCK) ON (a.ProdutoID = e.ConceitoID) ' +
                    'INNER JOIN Conceitos f WITH(NOLOCK) ON (b.MoedaID = f.ConceitoID) ' +
                    'LEFT OUTER JOIN Operacoes g WITH(NOLOCK) ON (a.CFOPID = g.OperacaoID) ' +
                  'WHERE (' + sFiltro + 'z.PedidoID = ' + idToFind + ' ) ' +
                  'ORDER BY b.dtPedido, a.Ordem';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24008) // Ordens de Producao
    {
        dso.SQL = 'SELECT a.*, b.RecursoAbreviado AS Est, dbo.fn_OrdemProducao_Sincronizada(a.OrdemProducaoID) AS Sincronizado, ' +
					'NULL AS dtPrevisaoSistema ' +
                  'FROM OrdensProducao a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                  'WHERE (' + sFiltro + ' a.PedidoID = ' + idToFind + ' AND ' +
					'a.EstadoID=b.RecursoID) ' +
                  'ORDER BY a.OrdemProducaoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24009) // Inspecao Recebimento
    {
        dso.SQL = 'SELECT * FROM Pedidos_RIR  WITH(NOLOCK) WHERE PedidoID = ' + idToFind + ' ' +
			'ORDER BY PedRIRID';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 24010) // Lancamentos
    {
        dso.SQL = 'EXEC sp_EventosContabeis_Lancamentos ' + glb_nFormID + ', NULL, ' + idToFind;
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24013) // Pedidos Vinculados
    {
        dso.SQL = 'SELECT b.PedidoID, ' +
		                'd.RecursoAbreviado AS Estado, ' +
		                'e.OperacaoID AS TransacaoID, ' +
		                'c.ValorTotalPedido AS ValorTotal, ' +
		                'b.Obrigatorio ' +
	                'FROM Pedidos_Vinculados a WITH(NOLOCK) ' +
		                'INNER JOIN Pedidos_Vinculados b WITH(NOLOCK) ON (b.VinculadoID = a.VinculadoID) ' +
		                'INNER JOIN Pedidos c WITH(NOLOCK) ON (b.PedidoID = c.PedidoID) ' +
		                'INNER JOIN Recursos d WITH(NOLOCK) ON (c.EstadoID = d.RecursoID) ' +
		                'INNER JOIN Operacoes e WITH(NOLOCK) ON (c.TransacaoID = e.OperacaoID) ' +
	                    'WHERE (a.PedidoID = ' + idToFind + ' AND  b.PedidoID <> ' + idToFind + ') ' +
	                    'ORDER BY b.PedidoID ';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 21027) // Dados Fiscais
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Pedidos_Itens_DadosFiscais a WITH(NOLOCK) ' +
                  'WHERE (a.PedItemID IN  (SELECT PedItemID FROM dbo.Pedidos_Itens aa WITH(NOLOCK) WHERE aa.PedidoID = ' + idToFind + ' ))';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24014) // Report Fabricante
    {
        dso.SQL = 'SELECT a.PedidoID, a.QuoteID, a.PessoaDealID ' +
                  'FROM Pedidos a WITH(NOLOCK) ' +
                  'WHERE a.PedidoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID) {
    setConnection(dso);

    var nEmpresaID = nEmpresaData[0];

    // LOG
    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    else if (pastaID == 24002) // Itens
    {
                        
        var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

        var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

        var nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');

        var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'TransacaoID' + '\'' + '].value');
    
         /*var nClassificacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'ClassificacaoID' + '\'' + '].value');*/

        //Alterado para pedidos de toma��o de servico
        var bTomacaoServico = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'Servico' + '\'' + '].value');

        var nTipoConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'TipoConceitoID' + '\'' + '].value');

        //Alterado para verificar se pedido � de toma��o de servico.
        var bProdutoServico = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ProdutoServico' + '\'' + '].value');

        if (dso.id == 'dsoCmb01Grid_01') 
        {
            //Pedido em cotacao, select retorna todos os produtos disponiveis do cadastro
            if (nEstadoID == 21) {
                //if ((!bTomacaoServico) || (bTomacaoServico && bProdutoServico))
                //{
                dso.SQL = 'SELECT dbo.fn_Produto_Descricao2(Itens.PedItemID, NULL, 20) AS Produto, Produtos.ConceitoID AS ProdutoID, ' +
		         			    '(CONVERT(NUMERIC(11,2), dbo.fn_Pedido_Item(' + nPedidoID + ', Produtos.ConceitoID, GETDATE(), NULL, 1))) AS ValorTabela ' +
   	                         'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Pedidos_Itens Itens WITH(NOLOCK) ' +
                             'WHERE (ProdutosEmpresa.SujeitoID= ' + nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
                             'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND ' +
                             'Itens.PedidoID=' + nRegistroID + ' AND Itens.ProdutoID=ProdutosEmpresa.ObjetoID) ' +
                             'ORDER BY Produto';
                /*}
                else
                {
                dso.SQL = 'SELECT dbo.fn_Produto_Descricao2(Itens.PedItemID, NULL, 20) AS Produto, Produtos.ConceitoID AS ProdutoID, ' +
                '(CONVERT(NUMERIC(11,2), dbo.fn_Pedido_Item(' + nPedidoID + ', Produtos.ConceitoID, GETDATE(), NULL, 1))) AS ValorTabela ' +
                'FROM Conceitos Produtos WITH(NOLOCK), Pedidos_Itens Itens WITH(NOLOCK) ' +
                'WHERE (Itens.PedidoID=' + nRegistroID + ' AND Itens.ProdutoID=Produtos.ConceitoID) ' +
                'ORDER BY Produto';
                }*/
            }
            // Pedido nao esta em cotacao, select retorna apenas os produtos do pedido
            else {
                dso.SQL = 'SELECT dbo.fn_Produto_Descricao2(Itens.PedItemID, NULL, 20) AS Produto, Produtos.ConceitoID AS ProdutoID, ' +
                          '0 AS ValorTabela ' +
                          'FROM Pedidos_Itens Itens WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK) ' +
                          'WHERE (Itens.PedidoID=' + nRegistroID + ' AND Itens.ProdutoID=Produtos.ConceitoID) ' +
                          'ORDER BY Produto';
            }
        }
        else if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT DISTINCT b.ConceitoID AS MoedaID,b.SimboloMoeda AS Moeda ' +
                      'FROM Pedidos_Itens a  WITH(NOLOCK), Conceitos b  WITH(NOLOCK) ' +
                      'WHERE (a.PedidoID = ' + nRegistroID + ' AND a.MoedaID = b.ConceitoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT a.PedItemID, dbo.fn_PedidoItem_Totais(a.PedItemID, 9) AS PercMargemContribuicao, dbo.fn_Produto_Descricao2(a.PedItemID, NULL, 20) AS Produto ' +
                      'FROM Pedidos_Itens a WITH(NOLOCK) ' +
                      'WHERE (a.PedidoID = ' + nRegistroID + ')';
        }  
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT Recursos.RecursoID AS EstadoID, Recursos.RecursoAbreviado ' +
 			          'FROM Recursos WITH(NOLOCK), Pedidos_Itens Itens WITH(NOLOCK) ' +
			          'WHERE (Itens.PedidoID=' + nRegistroID + ' AND Itens.EstadoID=Recursos.RecursoID) ' +
				      'ORDER BY RecursoAbreviado';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT Pessoas.PessoaID AS AprovadorID, Pessoas.Fantasia AS Aprovador ' +
						'FROM Pessoas WITH(NOLOCK) ' +
						'INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON Pessoas.PessoaID= Itens.AprovadorID ' +
						'WHERE (Itens.PedidoID=' + nRegistroID + ') ';
        }
        else if (dso.id == 'dso05GridLkp') {
            dso.SQL = 'SELECT a.PedItemID, b.PedidoID, d.RecursoAbreviado AS Estado ' +
			        'FROM Pedidos_Itens a WITH(NOLOCK) ' +
			               'INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON a.PedItemEncomendaID = b.PedItemID ' +
			               'INNER JOIN Pedidos c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) ' +
			               'INNER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = c.EstadoID) ' +
					'WHERE (a.PedidoID=' + nRegistroID + ') ';
        }
        else if (dso.id == 'dso06GridLkp') {
            dso.SQL = 'SELECT TipoItem.ItemID AS FinalidadeID, TipoItem.ItemMasculino AS Finalidade ' +
						'FROM TiposAuxiliares_Itens TipoItem WITH(NOLOCK) ' +
						'INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (TipoItem.ItemID= Itens.FinalidadeID) ' +
						'WHERE (Itens.PedidoID=' + nRegistroID + ') ';
        }
        else if (dso.id == 'dso07GridLkp') {
            dso.SQL = 'SELECT a.PedItemID, b.PedidoID AS PedidoReferencia ' +
						'FROM Pedidos_Itens a WITH(NOLOCK) ' +
						'INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (a.PedItemReferenciaID= b.PedItemID) ' +
						'WHERE (a.PedidoID=' + nRegistroID + ') ';
        }
        //Inicio - Altera��o para inlcuir combo de servi�o. BJBN
        if (dso.id == 'dsoCmb01Grid_02') {
            //Pedido em cotacao, select retorna todos os produtos disponiveis do cadastro
            if (nEstadoID == 21) {
                dso.SQL = 'SELECT (CASE WHEN (LEN(a.DescricaoLocal) < 80) THEN a.DescricaoLocal ELSE LEFT(a.DescricaoLocal, 80) + \'...\' END) AS DescricaoLocal, ' +
                                'a.ConServicoID, a.Codigo AS Codigo ' +
	                        'FROM Conceitos_Servicos a WITH(NOLOCK) ' +
		                        'INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.ProdutoID = a.ConceitoID) ' +
		                        'INNER JOIN Pedidos c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) ' +
	                        'WHERE (b.PedidoID = ' + nRegistroID + ') AND ' +
	                            '(dbo.fn_Pessoa_Localidade(c.PessoaID, dbo.fn_Localidade_Tipo(a.LocalidadeID), NULL, NULL) = a.LocalidadeID)';
            }
            // Pedido nao esta em cotacao, select retorna apenas os produtos do pedido
            else {
                dso.SQL = 'SELECT (CASE WHEN (LEN(a.DescricaoLocal) < 80) THEN a.DescricaoLocal ELSE LEFT(a.DescricaoLocal, 80) + \'...\' END) AS DescricaoLocal, ' +
                                'a.ConServicoID, a.Codigo AS Codigo ' +
	                        'FROM Conceitos_Servicos a WITH(NOLOCK) ' +
		                        'INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.ProdutoID = a.ConceitoID) ' +
	                        'WHERE (b.PedidoID = ' + nRegistroID + ') AND (a.ConServicoID = b.ServicoID)';
            }
        }
        //Fim - Altera��o para inlcuir combo de servi�o. BJBN
    }
    else if (pastaID == 24003) // Parcelas
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT 0 AS HistoricoPadraoID, SPACE(0) AS HistoricoAbreviado ' +
					  'UNION ALL SELECT DISTINCT a.HistoricoPadraoID, a.HistoricoAbreviado AS HistoricoAbreviado ' +
					  'FROM HistoricosPadrao a  WITH(NOLOCK), HistoricosPadrao_Contas b WITH(NOLOCK) ' +
                      'WHERE (a.EstadoID = 2 AND a.HistoricoPadraoID = b.HistoricoPadraoID) ' +
                      'ORDER BY HistoricoAbreviado';
        }
        else if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT ItemAbreviado,ItemID FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE (EstadoID = 2 AND TipoID = 804) ' +
                      'ORDER BY Ordem';
        }
        else if (dso.id == 'dsoCmb01Grid_03') {
            dso.SQL = 'SELECT Moedas.SimboloMoeda, Moedas.ConceitoID ' +
					  'FROM RelacoesPesRec Empresas  WITH(NOLOCK), RelacoesPesRec_Moedas MoedasEmpresa WITH(NOLOCK), ' +
					  'Conceitos Moedas WITH(NOLOCK) ' +
                      'WHERE Empresas.SujeitoID = ' + nEmpresaID + ' AND ' +
					  'Empresas.ObjetoID = 999 AND Empresas.TipoRelacaoID = 12 AND ' +
					  'Empresas.RelacaoID = MoedasEmpresa.RelacaoID AND ' +
					  'MoedasEmpresa.MoedaID = Moedas.ConceitoID ' +
                      'ORDER BY MoedasEmpresa.Ordem';
        }
        else if (dso.id == 'dso01GridLkp') {

            dso.SQL = 'SELECT DISTINCT a.PedParcelaID, b.FinanceiroID, c.RecursoAbreviado, b.Duplicata, b.dtVencimento, ' +
                                '(CASE WHEN ((a.TipoParcelaID IS NULL) OR (a.TipoParcelaID IN (643,644,645))) ' +
                                  'THEN dbo.fn_Financeiro_Posicao(b.FinanceiroID, NULL, NULL, GETDATE(), NULL, NULL) ' +
                                  'ELSE ISNULL(dbo.fn_PedParcela_ValoresComissao(a.PedParcelaID, 1), 0) END) AS SaldoDevedor ' +
                      'FROM Pedidos_Parcelas a WITH(NOLOCK) ' +
                        'LEFT OUTER JOIN Financeiro b WITH(NOLOCK) ON (a.FinanceiroID = b.FinanceiroID) ' +
                        'LEFT OUTER JOIN Recursos c WITH(NOLOCK) ON (b.EstadoID = c.RecursoID) ' +
                      'WHERE (a.PedidoID = ' + nRegistroID + ') ';

        }
        else if (dso.id == 'dso02GridLkp') {
            var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                      'dsoSup01.recordset[' + '\'' + 'TransacaoID' + '\'' + '].value');

            dso.SQL = 'SELECT TOP 1 OpeFormasPagamento.FormaPagamentoID AS FormaPagamentoID ' +
				'FROM Operacoes Transacoes  WITH(NOLOCK), Operacoes_FormasPagamento OpeFormasPagamento WITH(NOLOCK), ' +
					'TiposAuxiliares_Itens FormasPagamento WITH(NOLOCK) ' +
				'WHERE (Transacoes.OperacaoID = ' + nTransacaoID + ' AND ' +
					'Transacoes.OperacaoID = OpeFormasPagamento.OperacaoID AND ' +
					'OpeFormasPagamento.EhDefault = 1 AND ' +
					'OpeFormasPagamento.FormaPagamentoID = FormasPagamento.ItemID AND ' +
					'FormasPagamento.EstadoID = 2) ';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT dbo.fn_Pedido_Totais(PedidoID, 33) AS ValorTotalParcelas, Fechado AS Fechado ' +
							'FROM Pedidos WITH(NOLOCK) WHERE (PedidoID = ' + nRegistroID + ')';
        }
        else if (dso.id == 'dso04GridLkp') {
            var pais = getCurrEmpresaData();

            if (pais[1] != 130) {

                dso.SQL = 'SELECT DISTINCT a.PedParcelaID, convert(varchar,(b.dtPedido + a.PrazoPagamento),101) AS dtVencimento ' +
                          'FROM Pedidos_Parcelas a WITH(NOLOCK), Pedidos b  WITH(NOLOCK) ' +
                          'WHERE (a.PedidoID = ' + nRegistroID + ' AND a.PedidoID = b.PedidoID)';
            }
            else {

                dso.SQL = 'SELECT DISTINCT a.PedParcelaID, convert(varchar,(b.dtPedido + a.PrazoPagamento),103) AS dtVencimento ' +
                          'FROM Pedidos_Parcelas a WITH(NOLOCK), Pedidos b  WITH(NOLOCK) ' +
                          'WHERE (a.PedidoID = ' + nRegistroID + ' AND a.PedidoID = b.PedidoID)';
            }
        }
        else if (dso.id == 'dso05GridLkp') {
            dso.SQL = 'SELECT DISTINCT a.PedParcelaID, b.Fantasia ' +
                      'FROM Pedidos_Parcelas a WITH(NOLOCK), Pessoas b  WITH(NOLOCK) ' +
                      'WHERE (a.PedidoID = ' + nRegistroID + ' AND a.PessoaID = b.PessoaID)';
        }
        else if (dso.id == 'dso06GridLkp') {
            dso.SQL = 'SELECT a.ItemID, a.ItemMasculino AS TipoParcela ' +
                      'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                      'WHERE (a.TipoID = 406 AND a.EstadoID = 2) ' +
                      'ORDER BY a.Ordem';
        }
    }
    else if (pastaID == 24011) // Aprova��es Especiais
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemMasculino,ItemID FROM TiposAuxiliares_Itens  WITH(NOLOCK) ' +
                      'WHERE (EstadoID = 2 AND TipoID = 417) ' +
                      'ORDER BY Ordem';
        }
        else if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT DISTINCT a.PedAprovacaoID, b.Fantasia ' +
                      'FROM Pedidos_AprovacoesEspeciais a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.PedidoID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
    }
    else if (pastaID == 24009) // Ispecao Recebimento
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemAbreviado,ItemMasculino,ItemID ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoID = 413 ' +
                      'ORDER BY Ordem';
        }
        else if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT ItemAbreviado,ItemMasculino,ItemID ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoID = 414 ' +
                      'ORDER BY Ordem';
        }
        else if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT dbo.fn_PedidoRIR_Aprovado(PedRIRID) AS Aprovado, ' +
					'(CONVERT(BIT, CONVERT(BIT, dbo.fn_PedidoRIR_Totais(PedRIRID, 6)) ^ 1)) AS Concluido, PedRIRID ' +
				'FROM Pedidos_RIR WITH(NOLOCK) ' +
				'WHERE (PedidoID=' + nRegistroID + ' )';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT a.RecursoAbreviado AS CurrEst, c.PedRIRID ' +
				'FROM Recursos a WITH(NOLOCK), Pedidos_Itens b WITH(NOLOCK), Pedidos_RIR c WITH(NOLOCK) ' +
				'WHERE (a.RecursoID=b.EstadoID AND b.PedidoID=c.PedidoID AND b.ProdutoID=c.ProdutoID AND ' +
					'c.PedidoID=' + nRegistroID + ' )';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT a.Conceito AS Produto, b.PedRIRID ' +
				'FROM Pedidos_RIR b WITH(NOLOCK), Conceitos a  WITH(NOLOCK) ' +
				'WHERE (a.ConceitoID=b.ProdutoID AND b.PedidoID=' + nRegistroID + ' )';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.PedRIRID, CONVERT(NUMERIC(6), SUM(a.Quantidade)) AS Quantidade ' +
				'FROM Pedidos_Itens a WITH(NOLOCK), Pedidos_RIR b WITH(NOLOCK) ' +
				'WHERE (a.PedidoID=b.PedidoID AND a.ProdutoID=b.ProdutoID AND b.PedidoID=' + nRegistroID + ' ) ' +
				'GROUP BY b.PedRIRID';
        }
    }
    else if (pastaID == 21027) // Dados Fiscais
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT DISTINCT a.PedItemID, a.ProdutoID, c.Conceito ' +
	                  'FROM dbo.Pedidos_Itens a WITH(NOLOCK) ' +
		                    'INNER JOIN dbo.Pedidos b WITH(NOLOCK) ON (a.PedidoID = b.PedidoID)  ' +
		                    'INNER JOIN dbo.Conceitos c WITH(NOLOCK) ON (a.ProdutoID = c.ConceitoID) ' +
	                  'WHERE a.PedidoID = ' + nRegistroID;
        }
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.PedItemID, a.ProdutoID , d.ItemAbreviado ' +
	                  'FROM dbo.Pedidos_Itens a WITH(NOLOCK) ' +
                            'INNER JOIN dbo.Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) ' +
                            'LEFT OUTER JOIN dbo.RelacoesPesCon c WITH(NOLOCK) ON (c.SujeitoID = b.EmpresaID) AND (c.ObjetoID = a.ProdutoID) ' +
                            'LEFT OUTER JOIN dbo.TiposAuxiliares_Itens d WITH(NOLOCK) ON (d.ItemID = c.OrigemID) ' +
	                  'WHERE a.PedidoID = ' + nRegistroID;
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID) {
    var i = 0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
    var bTomacaoServicoVerifica = '';

    //Verifica se pedido � de toma��o de servico.
    var bTomacaoServico = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Servico' + '\'' + '].value');

    if (bTomacaoServico) {
        bTomacaoServicoVerifica = 'true';
    }
    else {
        bTomacaoServicoVerifica = 'false';
    }

    var aFolders = [[20008, 'true'],  /* Observacoes   */
                    [20009, 'true'],  /* Proprietarios */
                    [20010, 'true'],  /* LOG */
                    [24001, 'true'],  /* Embalagem/Expedicao */
                    [24002, 'true'],  /* Itens         */
                    [24003, 'true'],  /* Parcelas      */
                    [24011, 'true'],  /* Aprovacoes Especiais */
                    [24012, bTomacaoServicoVerifica],  /* Comiss�es */
                    [24004, 'true'],  /* Mensagens      */
                    [24005, 'true'],  /* Numeros de Serie     */
                    [24008, 'true'],  /* Ordens de Producao     */
                    [24009, 'true'],  /* Inspecao Recebimento     */
                    [24006, 'true'],  /* Pedidos     */
                    [24013, 'true'], /*Pedidos Vinculados*/
                    [24010, 'true'], /* Lancamentos Contabeis     */
                    [21027, 'true'], /*Dados Fiscais*/
                    [24014, 'true']]; /*Report Fabricante*/

    var bTemProducao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TemProducao' + '\'' + '].value');
    var bTemRIR = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TemRIR' + '\'' + '].value');

    if (!bTemProducao) {
        aFolders[10][1] = 'false';
    }

    if (!bTemRIR) {
        aFolders[11][1] = 'false';
    }

    cleanupSelInControlBar('inf', 1);

    for (i = 0; i < aFolders.length; i++) {
        if (eval(aFolders[i][1])) {
            vPastaName = window.top.getPastaName(aFolders[i][0]);
            if (vPastaName != '')
                addOptionToSelInControlBar('inf', 1, vPastaName, aFolders[i][0]);
        }
    }

    // Nao mexer - Inicio de automacao ============================== 	
    // Tenta selecionar novamente a pasta anterior selecionada no combo
    // Se nao mais existe a pasta anteriormente selecionada no combo
    // seleciona a pasta default

    stripFoldersByRight(currSelection);

    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID) {
    //@@ return true se tudo ok, caso contrario return false;

    var currLine = 0;
    var vlrTabela = 0;
    var vlrUnitario = 0;
    var vlrPercentual = 0;
    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'Resultado\'].value');

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 24002) // Itens
        {
            vlrTabela = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ValorTabela*'));
            vlrUnitario = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ValorUnitario'));

            if (vlrUnitario == null || vlrUnitario <= 0) {
                if (window.top.overflyGen.Alert('Unit�rio n�o aceito.') == 0)
                    return null;

                fg.Col = getColIndexByColKey(fg, 'ValorUnitario');

                return false;
            }

            var bTemNF = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
									'dsoSup01.recordset[' + '\'' + 'TemNF' + '\'' + '].value');
            if (!bTemNF)
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'VariacaoItem')) = '0.00';

            var aHiddenCols = new Array();

            if (bResultado)
                aHiddenCols = [getColIndexByColKey(fg, '_CALC_Holdkey04_1')];
            else
                aHiddenCols = [getColIndexByColKey(fg, 'ValorRevenda'), getColIndexByColKey(fg, '_CALC_Holdkey04_1')];



            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [getColIndexByColKey(fg, '^EstadoID^dso03GridLkp^EstadoID^RecursoAbreviado*'),
                                                                                    getColIndexByColKey(fg, 'ProdutoID*'),
                                                                                    getColIndexByColKey(fg, 'Quantidade'),
                                                                                    getColIndexByColKey(fg, 'ValorTabela*'),
                                                                                    getColIndexByColKey(fg, 'VariacaoItem'),
                                                                                    getColIndexByColKey(fg, '^FinalidadeID^dso06GridLkp^FinalidadeID^Finalidade*'),
                                                                                    getColIndexByColKey(fg, '^MoedaID^dso01GridLkp^MoedaID^Moeda*'),
                                                                                    getColIndexByColKey(fg, '_CALC_Holdkey04_1')], ['PedidoID', registroID], aHiddenCols);
        }
        else if (folderID == 24003) // Parcelas
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [getColIndexByColKey(fg, 'FormaPagamentoID'),
                                                                                        getColIndexByColKey(fg, 'PrazoPagamento'),
                                                                                        getColIndexByColKey(fg, 'ValorParcela')], ['PedidoID', registroID]);
        }
        else if (folderID == 24011) // Aprovacoes Especiais
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['PedidoID', registroID]);
        }
        else if (folderID == 24004) // Mensagens
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1], ['PedidoID', registroID]);
        }
        else if (folderID == 24009) // Inspecao Recebimento
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['PedidoID', registroID]);
        }
        else if (folderID == 21027) // Dados Fiscais
        {
            //Ticket#2017052698000531 - Setor fiscal solicitou que o campo "Dados Fiscais Fabricante", somente deve ser preenchido se a origem do produto for igual a 4. - FRG - 02/06/2017.
            var aCampos = [1];

            if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '^PedItemID^dso01GridLkp^PedItemID^ItemAbreviado*')) == '4')
                aCampos = [1, 2];
            
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), aCampos, ['PedidoID', registroID]);
        }

        if (currLine < 0)
            return false;
    }
    else {
        if (folderID == 24014) {
            if (isNaN(txtQuoteID.value)) {
                if (window.top.overflyGen.Alert('Digite apenas n�meros.') == 0)
                    return null;

                return false;
            }
            else if (parseFloat(txtQuoteID.value) < 10000000) {
                if (window.top.overflyGen.Alert('QuoteID inv�lido.') == 0)
                    return null;

                    return false;

            }
        }
    }
 
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO) {
    var i;
    var dTFormat = '';
    var bVisualizaMargem = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_VisualizaMargem;');

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.FrozenCols = 0;

    if (folderID == 20010) // LOG
    {
        headerGrid(fg, ['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);

        fillGridMask(fg, currDSO, ['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['', '99/99/9999', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm:ss', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 24002) // Itens
    {
        // Variavel que Controla se o Parceiro e Cliente ou Fornecedor.
        // Caso seja Cliente no grid deve aparecer as colunas posteriores a coluna pedido mae
        var aHoldCols = new Array();

        var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Resultado' + '\'' + '].value');

        //Alterado para verificar se pedido � de toma��o de servico.
        var bTomacaoServico = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Servico' + '\'' + '].value');

        //Alterado para verificar se pedido � de toma��o de servico.
        var bProdutoServico = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ProdutoServico' + '\'' + '].value');

        // Asstec
        var bAsstec = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Asstec' + '\'' + '].value');

        // Tipo de Pedido
        var nTipoPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoPedidoID' + '\'' + '].value');

        var nTipoConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoConceitoID' + '\'' + '].value');

        // EhCliente
        var bEhCliente = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EhCliente' + '\'' + '].value');

        // PrevisaoEntregaItem
        var PrevisaoEntregaItem = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PrevisaoEntregaItem' + '\'' + '].value');

        // CorPrevisaoEntrega
        var CorPrevisaoEntrega = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'CorPrevisaoEntrega' + '\'' + '].value');

        var sReadOnly = '';

        var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransacaoID' + '\'' + '].value');

        /*var nClassificacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ClassificacaoID' + '\'' + '].value');*/

       if (nTransacaoID != 115 && nTransacaoID != 325 && nTransacaoID != 215)
            sReadOnly = '*';
        /*
        Array para facilitar a manuten��o do grid
        indice 0: titulos do grid
        indice 1: colunas do dso
        indice 2: mascaras de campos para digita��o
        indice 3: mascaras de campos para visualiza��o
        indice 4: esconde a coluna
        */

        if (nTipoConceitoID == 303) //Produto
            var nArray = [['Ordem', 'Ordem*', '99', '', false],
                           ['ID', 'ProdutoID*', '', '', false],
                           ['E', '^EstadoID^dso03GridLkp^EstadoID^RecursoAbreviado*', '', '', !bVisualizaMargem],
        //['Descri��o do Item', 'ProdutoID*', '', '', false],
                           ['Descri��o do Item', '^PedItemID^dso02GridLkp^PedItemID^Produto*', '', '', false],
                           ['Servi�o', 'ServicoID', '', '', true],
                           ['Quant', 'Quantidade', '99999', '#####', false],
                           ['Tabela', 'ValorTabela*', '9999999999.99', '###,###,##0.00', false],
                           ['Var', 'VariacaoItem', '9999999999.99', '###,###,##0.00', !bVisualizaMargem],
                           ['Valor Int', 'ValorInterno', '9999999999.9999', '###,###,##0.0000', false],
                           ['Valor Rev', 'ValorRevenda', '9999999999.9999', '###,###,##0.0000', false],
                           ['Valor Unit', 'ValorUnitario', '9999999999.9999', '###,###,##0.0000', false],
                           ['Valor Total', '_CALC_Total_11*', '9999999999.99', '###,###,##0.00', false],
                           ['Total Desconto', 'ValorDesconto' + ((nTransacaoID == 115 || nTransacaoID == 325) ? '*' : ''), '9999999999.9999', '#,###,###,##0.0000', false],
                           ['Previs�o Entrega', 'PrevisaoEntrega*', '', '', false],
                           ['Valor CI', 'ValorComissaoInterna*', '9999999999.99', '###,###,##0.00', false],
                           ['Valor CR', 'ValorComissaoRevenda*', '9999999999.99', '###,###,##0.00', false],
                           ['Finalidade', '^FinalidadeID^dso06GridLkp^FinalidadeID^Finalidade*', '', '', false],
                           ['CFOP', 'CFOPID*', '', '', false],
                           ['Enc', 'EhEncomenda' + sReadOnly, '', '', false],
                           ['Pedido Enc', '^PedItemID^dso05GridLkp^PedItemID^PedidoID*', '', '', false],
                           ['E', '^PedItemID^dso05GridLkp^PedItemID^Estado*', '', '', false],
                           ['Ref', '^PedItemID^dso07GridLkp^PedItemID^PedidoReferencia*', '', '', false],
                           ['_CALC_Holdkey01_1', '_CALC_Holdkey01_1', '9999999999.99', '###,###,##0.00', true],
                           ['M�e', 'PedidoMae*', '', '', false],
                           ['Moeda', '^MoedaID^dso01GridLkp^MoedaID^Moeda*', '', '', false],
                           ['_CALC_Holdkey02_1', '_CALC_Holdkey02_1', '', '', true],
                           ['Taxa', 'TaxaMoeda*', '9999999999.999999', '###,###,##0.00000', false],
                           ['Faturamento', 'ValorFaturamento*', '9999999999.99', '###,###,##0.00', false],
                           ['Fat Base', 'ValorFaturamentoBase*', '9999999999.99', '###,###,##0.00', false],
                           ['Financeiro', 'ValorFinanceiro*', '9999999999.99', '###,###,##0.00', false],
                           ['_CALC_Holdkey03_1', '_CALC_Holdkey03_1', '9999999999.99', '###,###,##0.00', true],
                           ['Contrib', 'ValorContribuicao*', '9999999999.99', '###,###,##0.00', !bVisualizaMargem],
                           ['Cont Ajust', 'ValorContribuicaoAjustada*', '9999999999.99', '###,###,##0.00', !bVisualizaMargem],
                           ['MC', '^PedItemID^dso02GridLkp^PedItemID^PercMargemContribuicao*', '9999999999.99', '###,###,##0.00', !bVisualizaMargem],
                           ['In�cio Vig�ncia', 'dtVigenciaServicoInicio', '99/99/9999', dTFormat, (nTransacaoID == 215) ? false : true],
                           ['Fim Vig�ncia', 'dtVigenciaServicoFim', '99/99/9999', dTFormat, (nTransacaoID == 215) ? false : true],
                           ['Aprovador', '^AprovadorID^dso04GridLkp^AprovadorID^Aprovador*', '', '', false],
                           ['_CALC_Holdkey04_1', '_CALC_Holdkey04_1', '9999999999.99', '###,###,##0.00', true],
                           ['_CALC_Holdkey05_1', '_CALC_Holdkey05_1', '9999999999.99', '###,###,##0.00', true],
                           ['MoedaID', 'MoedaID', '', '', true],
                           ['TemPPB', 'TemPPB*', '', '', true],
                           ['PedItemID', 'PedItemID', '', '', true]];

        else if (nTipoConceitoID == 309) //NCM
            var nArray = [['Ordem', 'Ordem*', '99', '', false],
                           ['ID', 'ProdutoID*', '', '', false],
                           ['E', '^EstadoID^dso03GridLkp^EstadoID^RecursoAbreviado*', '', '', false],
                           ['Descri��o do Item', '^PedItemID^dso02GridLkp^PedItemID^Produto*', '', '', false],
                           ['Servi�o', 'ServicoID', '', '', true],
                           ['Quant', 'Quantidade', '999999.9999', '######.0000', false],
                           ['Tabela', 'ValorTabela*', '9999999999.99', '###,###,##0.00', true],
                           ['Var', 'VariacaoItem', '9999999999.99', '###,###,##0.00', true],
                           ['Valor Int', 'ValorInterno', '9999999999.9999', '###,###,##0.0000', true],
                           ['Valor Rev', 'ValorRevenda', '9999999999.9999', '###,###,##0.0000', true],
                           ['Valor Unit', 'ValorUnitario', '9999999999.9999', '###,###,##0.0000', false],
                           ['Valor Total', '_CALC_Total_11*', '9999999999.99', '###,###,##0.00', false],
                           ['Total Desconto', 'ValorDesconto' + ((nTransacaoID == 115 || nTransacaoID == 325) ? '*' : ''), '9999999999.9999', '#,###,###,##0.0000', false],
                           ['Previs�o Entrega', 'PrevisaoEntrega*', '', '', true],
                           ['Valor CI', 'ValorComissaoInterna*', '9999999999.99', '###,###,##0.00', true],
                           ['Valor CR', 'ValorComissaoRevenda*', '9999999999.99', '###,###,##0.00', true],
                           ['Finalidade', '^FinalidadeID^dso06GridLkp^FinalidadeID^Finalidade*', '', '', true],
                           ['CFOP', 'CFOPID*', '', '', true],
                           ['Enc', 'EhEncomenda' + sReadOnly, '', '', true],
                           ['Pedido Enc', '^PedItemID^dso05GridLkp^PedItemID^PedidoID*', '', '', true],
                           ['E', '^PedItemID^dso05GridLkp^PedItemID^Estado*', '', '', true],
                           ['Ref', '^PedItemID^dso07GridLkp^PedItemID^PedidoReferencia*', '', '', true],
                           ['_CALC_Holdkey01_1', '_CALC_Holdkey01_1', '9999999999.99', '###,###,##0.00', true],
                           ['M�e', 'PedidoMae*', '', '', true],
                           ['Moeda', '^MoedaID^dso01GridLkp^MoedaID^Moeda*', '', '', true],
                           ['_CALC_Holdkey02_1', '_CALC_Holdkey02_1', '', '', true],
                           ['Taxa', 'TaxaMoeda*', '9999999999.999999', '###,###,##0.00000', true],
                           ['Faturamento', 'ValorFaturamento*', '9999999999.99', '###,###,##0.00', true],
                           ['Fat Base', 'ValorFaturamentoBase*', '9999999999.99', '###,###,##0.00', true],
                           ['Financeiro', 'ValorFinanceiro*', '9999999999.99', '###,###,##0.00', true],
                           ['_CALC_Holdkey03_1', '_CALC_Holdkey03_1', '9999999999.99', '###,###,##0.00', true],
                           ['Contrib', 'ValorContribuicao*', '9999999999.99', '###,###,##0.00', true],
                           ['Cont Ajust', 'ValorContribuicaoAjustada*', '9999999999.99', '###,###,##0.00', true],
                           ['MC', '^PedItemID^dso02GridLkp^PedItemID^PercMargemContribuicao*', '9999999999.99', '###,###,##0.00', true],
                           ['Aprovador', '^AprovadorID^dso04GridLkp^AprovadorID^Aprovador*', '', '', true],
                           ['_CALC_Holdkey04_1', '_CALC_Holdkey04_1', '9999999999.99', '###,###,##0.00', true],
                           ['_CALC_Holdkey05_1', '_CALC_Holdkey05_1', '9999999999.99', '###,###,##0.00', true],
                           ['MoedaID', 'MoedaID', '', '', true],
                           ['TemPPB', 'TemPPB*', '', '', true],
                           ['PedItemID', 'PedItemID', '', '', true]];

        else if (nTipoConceitoID == 318 || nTipoConceitoID == 320) //Servi�o Estadual / Servi�o Federal
            var nArray = [['Ordem', 'Ordem*', '99', '', false],
                           ['ID', 'ProdutoID*', '', '', false],
                           ['E', '^EstadoID^dso03GridLkp^EstadoID^RecursoAbreviado*', '', '', false],
                           ['Descri��o do Item', '^PedItemID^dso02GridLkp^PedItemID^Produto*', '', '', false],
                           ['Servi�o', 'ServicoID', '', '', true],
                           ['Quant', 'Quantidade', '99999', '#####', true],
                           ['Tabela', 'ValorTabela*', '9999999999.99', '###,###,##0.00', true],
                           ['Var', 'VariacaoItem', '9999999999.99', '###,###,##0.00', true],
                           ['Valor Int', 'ValorInterno', '9999999999.9999', '###,###,##0.0000', true],
                           ['Valor Rev', 'ValorRevenda', '9999999999.9999', '###,###,##0.0000', true],
                           ['Valor Unit', 'ValorUnitario', '9999999999.9999', '###,###,##0.0000', false],
                           ['Valor Total', '_CALC_Total_11*', '9999999999.99', '###,###,##0.00', true],
                           ['Total Desconto', 'ValorDesconto' + ((nTransacaoID == 115 || nTransacaoID == 325) ? '*' : ''), '9999999999.9999', '#,###,###,##0.0000', true],
                           ['Previs�o Entrega', 'PrevisaoEntrega*', '', '', true],
                           ['Valor CI', 'ValorComissaoInterna*', '9999999999.99', '###,###,##0.00', true],
                           ['Valor CR', 'ValorComissaoRevenda*', '9999999999.99', '###,###,##0.00', true],
                           ['Finalidade', '^FinalidadeID^dso06GridLkp^FinalidadeID^Finalidade*', '', '', true],
                           ['CFOP', 'CFOPID*', '', '', true],
                           ['Enc', 'EhEncomenda' + sReadOnly, '', '', true],
                           ['Pedido Enc', '^PedItemID^dso05GridLkp^PedItemID^PedidoID*', '', '', true],
                           ['E', '^PedItemID^dso05GridLkp^PedItemID^Estado*', '', '', true],
                           ['Ref', '^PedItemID^dso07GridLkp^PedItemID^PedidoReferencia*', '', '', true],
                           ['_CALC_Holdkey01_1', '_CALC_Holdkey01_1', '9999999999.99', '###,###,##0.00', true],
                           ['M�e', 'PedidoMae*', '', '', true],
                           ['Moeda', '^MoedaID^dso01GridLkp^MoedaID^Moeda*', '', '', true],
                           ['_CALC_Holdkey02_1', '_CALC_Holdkey02_1', '', '', true],
                           ['Taxa', 'TaxaMoeda*', '9999999999.999999', '###,###,##0.00000', true],
                           ['Faturamento', 'ValorFaturamento*', '9999999999.99', '###,###,##0.00', true],
                           ['Fat Base', 'ValorFaturamentoBase*', '9999999999.99', '###,###,##0.00', true],
                           ['Financeiro', 'ValorFinanceiro*', '9999999999.99', '###,###,##0.00', true],
                           ['_CALC_Holdkey03_1', '_CALC_Holdkey03_1', '9999999999.99', '###,###,##0.00', true],
                           ['Contrib', 'ValorContribuicao*', '9999999999.99', '###,###,##0.00', true],
                           ['Cont Ajust', 'ValorContribuicaoAjustada*', '9999999999.99', '###,###,##0.00', true],
                           ['MC', '^PedItemID^dso02GridLkp^PedItemID^PercMargemContribuicao*', '9999999999.99', '###,###,##0.00', true],
                           ['Aprovador', '^AprovadorID^dso04GridLkp^AprovadorID^Aprovador*', '', '', true],
                           ['_CALC_Holdkey04_1', '_CALC_Holdkey04_1', '9999999999.99', '###,###,##0.00', true],
                           ['_CALC_Holdkey05_1', '_CALC_Holdkey05_1', '9999999999.99', '###,###,##0.00', true],
                           ['MoedaID', 'MoedaID', '', '', true],
                           ['TemPPB', 'TemPPB*', '', '', true],
                           ['PedItemID', 'PedItemID', '', '', true]];

        headerGrid(fg, aReturnFieldsToGrid(nArray, 0), aReturnFieldsToGrid(nArray, 4));

        // array de celulas com hint
        // glb_aCelHint = [[Row,Col,'Hint'], [Row,Col,'Hint'], ...]
        // glb_aCelHint = [[0,0,'Zero zero'], [1,2,'Hum dois']];
        glb_aCelHint = [[0, 2, 'Estado do Produto'],
                        [0, 3, 'CP: C�digo do Prestador, CF: C�digo Federal, CT: C�digo do Tomador'],
                        [0, 8, 'Valor Interno'],
                        [0, 9, 'Valor Revenda'],
                        [0, 10, 'Valor Unitario'],
                        [0, 13, 'Valor Comiss�o Interna'],
                        [0, 14, 'Valor Comiss�o Revenda']];

        fillGridMask(fg, currDSO, aReturnFieldsToGrid(nArray, 1), aReturnFieldsToGrid(nArray, 2), aReturnFieldsToGrid(nArray, 3));

        alignColsInGrid(fg, [getColIndexByColKey(fg, 'Ordem*'),
                            getColIndexByColKey(fg, 'ProdutoID*'),
                            getColIndexByColKey(fg, 'Quantidade'),
                            getColIndexByColKey(fg, 'ValorTabela*'),
                            getColIndexByColKey(fg, 'VariacaoItem'),
                            getColIndexByColKey(fg, 'ValorInterno'),
                            getColIndexByColKey(fg, 'ValorRevenda'),
                            getColIndexByColKey(fg, 'ValorUnitario'),
                            getColIndexByColKey(fg, '_CALC_Total_11*'),
                            getColIndexByColKey(fg, 'ValorDesconto' + ((nTransacaoID == 115 || nTransacaoID == 325) ? '*' : '')),
                            getColIndexByColKey(fg, 'ValorComissaoInterna*'),
                            getColIndexByColKey(fg, 'ValorComissaoRevenda*'),
                            getColIndexByColKey(fg, 'PedidoMae'),
                            getColIndexByColKey(fg, '_CALC_Holdkey02_1'),
                            getColIndexByColKey(fg, 'TaxaMoeda*'),
                            getColIndexByColKey(fg, 'ValorFaturamentoBase'),
                            getColIndexByColKey(fg, 'ValorFinanceiro*'),
                            getColIndexByColKey(fg, 'ValorContribuicao*'),
                            getColIndexByColKey(fg, 'ValorContribuicaoAjustada*'),
                            getColIndexByColKey(fg, 'dtVigenciaServicoInicio'),
                            getColIndexByColKey(fg, 'dtVigenciaServicoFim'),
                            getColIndexByColKey(fg, '^PedItemID^dso02GridLkp^PedItemID^PercMargemContribuicao*')]);

        fg.ColDataType(getColIndexByColKey(fg, 'EhEncomenda' + sReadOnly)) = 11;

        // escreve nas celulas de campos calculados
        for (i = 1; i < fg.Rows; i++)
            calcTotItem(i);

        fg.FrozenCols = 2;

        /********************************************************************
        Prepara grid para linha de totalizacao
        Parametros:
        grid            referencia ao grid
        lineLabel       label da linha de titulos
        lineBkColor     cor de fundo da linha de totais (usar hexa invertido 0X000000)
        ou null
        lineFColor      cor dos caracteres da linha de totais  (usar hexa invertido 0X000000)
        ou null
        useBold         caracteres em bold (usar false ou true)
        aTotLine        Array de parametros para linha de totalizacao
        1 elemento do array - label da linha
        2 em diante - trio coluna, mascara e operacao a usar na coluna    
        As operacoes possiveis sao 'S' soma e 'M' media
        ********************************************************************/
        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'Quantidade'), (nTipoConceitoID == 309 ? '######.0000': '#########'), 'S'],
                                                              [getColIndexByColKey(fg, '_CALC_Total_11*'), '###,###,###,###.00', 'S'],
                                                              [getColIndexByColKey(fg, 'ValorComissaoInterna*'), '###,###,###,###.00', 'S'],
                                                              [getColIndexByColKey(fg, 'ValorComissaoRevenda*'), '###,###,###,###.00', 'S'],
                                                              [getColIndexByColKey(fg, 'ValorFaturamento*'), '###,###,###,###.00', 'S'],
                                                              [getColIndexByColKey(fg, 'ValorFaturamentoBase*'), '###,###,###,###.00', 'S'],
                                                              [getColIndexByColKey(fg, 'ValorFinanceiro*'), '###,###,###,###.00', 'S'],

                                                              [getColIndexByColKey(fg, 'ValorContribuicao*'), '###,###,###,###.00', 'S'],
                                                              [getColIndexByColKey(fg, '_CALC_Holdkey03_1'), '###,###,###,###.00', 'S'],
                                                              [getColIndexByColKey(fg, 'ValorContribuicaoAjustada*'), '###,###,###,###.00', 'S'],
                                                              [getColIndexByColKey(fg, 'ValorDesconto' + ((nTransacaoID == 115 || nTransacaoID == 325) ? '*' : '')), '###,###,###,###.0000', 'S']]);

        if ((PrevisaoEntregaItem != null) && (fg.Rows > 1))
        {
            fg.TextMatrix(1, getColIndexByColKey(fg, 'PrevisaoEntrega*')) = PrevisaoEntregaItem;

            if ((CorPrevisaoEntrega != '') && (CorPrevisaoEntrega != null))
                fg.Cell(6, 1, getColIndexByColKey(fg, 'PrevisaoEntrega*'), 1, getColIndexByColKey(fg, 'PrevisaoEntrega*')) = eval(CorPrevisaoEntrega);
        }

        if (nTipoConceitoID == 320)
            glb_duplicaGridRows = window.setInterval('duplicaGridRows()', 50, 'JavaScript');

        // Calcula a Margem e Comissao de Vendas do Pedido
        if (fg.Rows > 2) {

            var nValorTotalTabela = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                'dsoSup01.recordset[' + '\'' + 'ValorTotalTabela' + '\'' + '].value');

            var nVariacaoItem = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                'dsoSup01.recordset[' + '\'' + 'VariacaoItem' + '\'' + '].value');

            var nMargemContribuicao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                'dsoSup01.recordset[' + '\'' + 'MargemContribuicao' + '\'' + '].value');

            if (nValorTotalTabela != null)
                fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorTabela*')) = nValorTotalTabela;

            if (nVariacaoItem != null)
                fg.TextMatrix(1, getColIndexByColKey(fg, 'VariacaoItem')) = nVariacaoItem;

            if (nMargemContribuicao != null)
                fg.TextMatrix(1, getColIndexByColKey(fg, '^PedItemID^dso02GridLkp^PedItemID^PercMargemContribuicao*')) = nMargemContribuicao;
        }
    }
    else if (folderID == 24003) // Parcelas
    {
        var glb_bA1 = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoDireitos.recordset[' + '\'' + 'A1' + '\'' + '].value');
        var glb_bA2 = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoDireitos.recordset[' + '\'' + 'A2' + '\'' + '].value');

        var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
        var sReadOnly = '';
       
        if ((nEstadoID >= 24) && (glb_bA1 == 0) && (glb_bA2 == 0) && (nEmpresaData[1] == 167)) {

            sReadOnly = '*';
        }

        headerGrid(fg, ['Ordem',
                       'Hist�rico',
                       'Tipo Parcela',
                       'ID',
                       'Pessoa',
                       'Financeiro',
                       'E',
                       'Duplicata',
                       'Forma',
                       'Prazo',
                       'Vencimento',
                       'Valor',
					   '$',
                       'Saldo Devedor',
                       'Liberada',
                       'Observa��o',
                       'Vencimento2',
                       'TipoParcelaID',
                       'PedParcelaID'], [16, 17, 18]);

        glb_aCelHint = [[0, 6, 'Estado do financeiro'],
					    [0, 14, 'Parcela de comiss�o esta liberada?']];

        fillGridMask(fg, currDSO, ['Ordem*',
                                 'HistoricoPadraoID*',
                                 '^TipoParcelaID^dso06GridLkp^ItemID^TipoParcela*',
                                 'PessoaID*',
                                 '^PedParcelaID^dso05GridLkp^PedParcelaID^Fantasia*',
                                 'FinanceiroID*',
                                 '^PedParcelaID^dso01GridLkp^PedParcelaID^RecursoAbreviado*',
                                 '^PedParcelaID^dso01GridLkp^PedParcelaID^Duplicata*',
                                 'FormaPagamentoID' + sReadOnly,
                                 'PrazoPagamento',
                                 '^PedParcelaID^dso01GridLkp^PedParcelaID^dtVencimento*',
                                 'ValorParcela',
                                 'MoedaOcorrenciaID*',
                                 '^PedParcelaID^dso01GridLkp^PedParcelaID^SaldoDevedor*',
                                 'Liberada*',
                                 'Observacao',
                                 '^PedParcelaID^dso04GridLkp^PedParcelaID^dtVencimento*',
                                 'TipoParcelaID',
                                 'PedParcelaID'],
                                 ['', '', '', '', '', '', '', '', '', '999', '99/99/999', '999999999.99', '', '999999999.99', '', '', '', '', ''],
                                 ['', '', '', '', '', '', '', '', '', '999', dTFormat, '###,###,###,###.00', '', '###,###,###,###.00', '', '', '', '', '']);


        alignColsInGrid(fg, [0, 3, 5, 9, 11, 13]);

        fg.FrozenCols = 1;

        /********************************************************************
        Prepara grid para linha de totalizacao
        Parametros:
        grid            referencia ao grid
        lineLabel       label da linha de titulos
        lineBkColor     cor de fundo da linha de totais (usar hexa invertido 0X000000)
        ou null
        lineFColor      cor dos caracteres da linha de totais  (usar hexa invertido 0X000000)
        ou null
        useBold         caracteres em bold (usar false ou true)
        aTotLine        Array de parametros para linha de totalizacao
        1 elemento do array - label da linha
        2 em diante - trio coluna, mascara e operacao a usar na coluna    
        As operacoes possiveis sao 'S' soma e 'M' media
        ********************************************************************/
        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[13, '###,###,###,###.00', 'S']]);

        var nColdtVencimentoFinanceiro = getColIndexByColKey(fg, '^PedParcelaID^dso01GridLkp^PedParcelaID^dtVencimento*');
        var nColdtVencimentoPedido = getColIndexByColKey(fg, '^PedParcelaID^dso04GridLkp^PedParcelaID^dtVencimento*');

        // Atualiza o valor total das parcelas e o Vencimento caso Pedido ainda nao tenha financeiro
        if (fg.Rows > 2) {
            if (!((dso03GridLkp.recordset.BOF) && (dso03GridLkp.recordset.EOF))) {
                dso03GridLkp.recordset.MoveFirst();
                fg.TextMatrix(1, 11) = dso03GridLkp.recordset['ValorTotalParcelas'].value;
                glb_bFechado = dso03GridLkp.recordset['Fechado'].value;
            }

            // Atualiza o Vencimento se o Financeiro nao foi criado
            for (i = 1; i < fg.Rows; i++) {
                if (fg.TextMatrix(i, nColdtVencimentoFinanceiro) == '')
                    fg.TextMatrix(i, nColdtVencimentoFinanceiro) = fg.TextMatrix(i, nColdtVencimentoPedido);
            }
        }
    }
    else if (folderID == 24011) // Aprovacoes Especiais 
    {
        headerGrid(fg, ['Tipo',
                       'Usu�rio',
                       'Data',
                       'Observa��o',
                       'PedAprovacaoID'], [4]);

        fillGridMask(fg, currDSO, ['TipoAprovacaoID*',
                                 '^PedAprovacaoID^dso01GridLkp^PedAprovacaoID^Fantasia*',
                                 'dtAprovacao*',
                                 'Observacao',
                                 'PedAprovacaoID'],
                                 ['', '', '99/99/999', '', ''],
                                 ['', '', dTFormat + ' hh:mm:ss', '', '']);


        alignColsInGrid(fg, [4]);

        fg.FrozenCols = 1;

    }
    else if (folderID == 24012) // Comiss�es
    {
        headerGrid(fg, ['Cliente',
                        'FinanceiroID',
                        'Parceiro',
                        'Valor',
                        'Empresa',
                        'Pedido',
                        'Nota Fiscal',
                        'Data NF',
                        'Usu�rio',
                        'Associa��o',
                        'FinComissaoID'], [1, 10]);

        fillGridMask(fg, currDSO, ['Cliente',
								  'FinanceiroID',
								  'Parceiro',
								  'Valor',
								  'Fantasia',
								  'PedidoID',
								  'NF',
								  'DataNF',
								  'Usuario',
								  'DataAssociacao',
								  'FinComissaoID'],
								  ['', '', '', '999999999.99', '', '', '', '99/99/9999', '', '99/99/9999', ''],
								  ['', '', '', '###,###,###.00', '', '', '', '', '', dTFormat + ' hh:mm:ss', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[3, '###,###,###.00', 'S']]);

        alignColsInGrid(fg, [3, 5, 6]);
    }
    else if (folderID == 24004)  // Mensagens
    {
        headerGrid(fg, ['Ordem',
                       'Mensagem',
                       'Nota',
                       'PedMensagemID'], [3]);

        glb_aCelHint = [[0, 2, 'Imprime mensagem na venda do produto?']];

        fillGridMask(fg, currDSO, ['Ordem',
								 'Mensagem',
								 'NotaMensagem',
								 'PedMensagemID'],
                                 ['99', '', '', ''],
                                 ['##', '', '', '']);

        alignColsInGrid(fg, [0]);
    }
    else if (folderID == 24005) // Numero de Serie
    {
        var aHoldCols = new Array();
        var bProducao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TemProducao' + '\'' + '].value');
        var bAsstec = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Asstec' + '\'' + '].value');
        var nTipoPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoPedidoID' + '\'' + '].value');

        aHoldCols = [10, 14, 15, 16];

        if (nTipoPedidoID == 602)
            aHoldCols[aHoldCols.length] = 6;

        if (!bProducao)
            aHoldCols[aHoldCols.length] = 11;

        if (!bAsstec)
            aHoldCols[aHoldCols.length] = 12;

        glb_bModoNumeroSerieFull = false;
        if (!((currDSO.recordset.BOF) && (currDSO.recordset.EOF))) {
            currDSO.recordset.moveFirst();
            if ((currDSO.recordset['NumMovimentoID'].value == null) &&
				(currDSO.recordset['PedProdutoID'].value == null)) {
                glb_bModoNumeroSerieFull = true;
                //aHoldCols[aHoldCols.length] = 7;
                aHoldCols[aHoldCols.length] = 8;
                aHoldCols[aHoldCols.length] = 9;
                aHoldCols[aHoldCols.length] = 10;
                aHoldCols[aHoldCols.length] = 11;
                aHoldCols[aHoldCols.length] = 12;
                aHoldCols[aHoldCols.length] = 13;
            }
        }

        headerGrid(fg, ['Produto',
                       'ID',
                       'Quant',
                       'Q/Cx',
                       'Cx',
                       'Un',
                       'Caixa',
                       'Qt Gat',
                       'N�mero S�rie   ',
                       'Ordem',
					   'Estoque',
					   'OP',
                       'Asstec',
                       'Colaborador',
                       'NumeroSerieID',
                       'NumMovimentoID',
                       'PedProdutoID'], aHoldCols);

        fillGridMask(fg, currDSO, ['Produto',
                                 'ProdutoID',
                                 'QuantidadeItem',
                                 'QuantidadePorCaixa',
                                 'QuantidadeCaixas',
                                 'QuantidadeUnidades',
                                 'Caixa',
                                 'QuantidadeGatilhada',
                                 'NumeroSerie',
                                 '_calc_Ordem_5',
                                 'Estoque',
								 'OrdemProducaoID',
                                 'AsstecID',
                                 'Colaborador',
                                 'NumeroSerieID',
                                 'NumMovimentoID',
                                 'PedProdutoID'],
                                 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                                 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);

        glb_aCelHint = [[0, 2, 'Quantidade do �tem'],
						[0, 7, 'Quantidade gatilhada']];

        alignColsInGrid(fg, [1, 2, 3, 4, 5, 6, 7, 9, 11, 12]);

        // Calcula a Ordem
        var nOrdem, nProdutoID, nCaixa, nNumeroSerieID;
        var nItens, nQuantidade, nCaixas, nUnidades, nCaixasGatilhadas;

        nOrdem = 0;
        nItens = 0;
        nQuantidade = 0;
        nCaixas = 0;
        nUnidades = 0;
        nQuantidadeGatilhada = 0;
        nCaixasGatilhadas = 0;


        if (fg.Rows > 1) {
            nProdutoID = fg.TextMatrix(1, getColIndexByColKey(fg, 'ProdutoID'));
            nCaixa = fg.TextMatrix(1, getColIndexByColKey(fg, 'Caixa'));
            for (i = 1; i < fg.Rows; i++) {
                if (fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoID')) != nProdutoID)
                    nOrdem = 1;
                else
                    nOrdem++;

                if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoID')) != nProdutoID) || (i == 1)) {
                    nItens++;
                    nQuantidade += fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeItem'));
                    nCaixas += fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeCaixas'));
                    nUnidades += fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeUnidades'));
                    nQuantidadeGatilhada += fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeGatilhada'));
                }

                if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'Caixa')) != nCaixa) || (i == 1)) {
                    if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Caixa')) != '')
                        nCaixasGatilhadas++;
                }

                fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_Ordem_5')) = nOrdem;
                nProdutoID = fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoID'));
                nCaixa = fg.TextMatrix(i, getColIndexByColKey(fg, 'Caixa'));
            }


            gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true,
				[[getColIndexByColKey(fg, '_calc_Ordem_5'), '#########', 'C'],
				 [getColIndexByColKey(fg, 'Caixa'), '#########', 'S'],
				 [getColIndexByColKey(fg, 'QuantidadeGatilhada'), '#########', 'C']]);

            fg.TextMatrix(1, getColIndexByColKey(fg, 'ProdutoID')) = nItens;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'QuantidadeItem')) = nQuantidade;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'QuantidadeCaixas')) = nCaixas;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'QuantidadeUnidades')) = nUnidades;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'QuantidadeGatilhada')) = nQuantidadeGatilhada;

            if (!glb_bModoNumeroSerieFull)
                fg.TextMatrix(1, getColIndexByColKey(fg, 'Caixa')) = nCaixasGatilhadas;
        }

        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 24006)  // Pedidos
    {
        headerGrid(fg, ['Pedido',
                       'Est',
                       'Data',
                       'NF',
                       'Data NF',
                       'CFOP',
                       'ID',
                       'Produto',
                       'Quant',
                       'Moeda',
                       'Valor Unit',
                       'Valor Total'], [9]);

        fillGridMask(fg, currDSO, ['PedidoID',
								 'Estado',
								 'dtPedido',
								 'NotaFiscal',
								 'dtNotaFiscal',
								 'Codigo',
								 'ProdutoID',
								 'Produto',
								 'Quantidade',
								 'SimboloMoeda',
								 'ValorUnitario',
								 'ValorTotal'],
                                 ['', '', '', '', '', '', '', '', '', '', '', ''],
                                 ['', '', dTFormat, '', dTFormat, '', '', '', '', '', '###,###,##0.0000', '###,###,###,###.00']);

        alignColsInGrid(fg, [0, 3, 5, 6, 8, 10, 11]);
        fg.FrozenCols = 1;

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[8, '####', 'S'], [11, '###,###,###,###.00', 'S']]);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 24008)  // Ordens de Producao
    {
        headerGrid(fg, ['OP',
                       'Est',
                       'S',
                       'PI',
                       'OK',
                       'Data',
                       'Previs�o Sistema'], []);

        fillGridMask(fg, currDSO, ['OrdemProducaoID',
								 'Est',
								 'Suspenso',
								 'ProcessoIniciado',
								 'Sincronizado',
								 'dtOP',
								 'dtPrevisaoSistema'],
                                 ['', '', '', '', '', '', ''],
                                 ['##########', '', '', '', '', dTFormat, dTFormat]);

        alignColsInGrid(fg, [0]);
        fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 24009)  // Inspecao Recebimento
    {
        headerGrid(fg, ['RIR',
					   'Con',
					   'Apr',
					   'ID',
					   'Est',
					   'Produto',
					   'TI',
					   'RI',
					   'Quant',
					   'Amostra',
					   'Ac',
					   'Rej',
					   'Rejeitado',
					   'Observa��o',
					   'PedRIRID'], [14]);

        glb_aCelHint = [[0, 1, 'RIR concluido?'],
						[0, 2, 'RIR aprovado?'],
						[0, 6, 'Tipo de Inspe��o'],
						[0, 7, 'Regime de Inspe��o']];

        fillGridMask(fg, currDSO, ['PedRIRID*',
								 '^PedRIRID^dso01GridLkp^PedRIRID^Concluido*',
								 '^PedRIRID^dso01GridLkp^PedRIRID^Aprovado*',
								 'ProdutoID*',
								 '^PedRIRID^dso02GridLkp^PedRIRID^CurrEst*',
								 '^PedRIRID^dso03GridLkp^PedRIRID^Produto*',
								 'TipoInspecaoID*',
								 'RegimeInspecaoID*',
								 '^PedRIRID^dso04GridLkp^PedRIRID^Quantidade*',
								 'QuantidadeAmostra*',
								 'QuantidadeAceitacao*',
								 'QuantidadeRejeicao*',
								 'QuantidadeRejeitada*',
								 'Observacao',
								 'PedRIRID'],
                                 ['9999999999', '', '', '9999999999', '', '', '', '', '999999', '999999', '999999', '999999', '999999', '', ''],
                                 ['##########', '', '', '##########', '', '', '', '', '######', '######', '######', '######', '######', '', '']);

        alignColsInGrid(fg, [0, 3, 8, 9, 10, 11, 12]);
        fg.FrozenCols = 4;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 24010) // Lancamentos contabeis
    {
        headerGrid(fg, ['Lan�amento',
                        'Est',
                        'Data',
                        'Balancete',
                        'Est',
                        'Tipo',
                        'Quant',
                        'Hist�rico Padr�o',
                        'Hist�rico Complementar',
                        'Detalhe',
                        'D',
                        'C',
                        'Moeda',
                        'Total Lan�amento',
                        'Taxa',
                        'Total Convertido',
                        'Observa��o',
                        'EventoID'], [17]);

        glb_aCelHint = [[0, 4, '� estorno?'],
						[0, 6, 'Quantidade de detalhes'],
						[0, 10, 'N�mero de D�bitos'],
						[0, 11, 'N�mero de Cr�ditos'],
						[0, 11, 'Taxa de convers�o para moeda comum']];

        fillGridMask(fg, currDSO, ['LancamentoID',
                                 'Estado',
                                 'dtLancamento',
                                 'dtBalancete',
                                 'EhEstorno',
                                 'TipoLancamento',
                                 'Quantidade',
                                 'HistoricoPadrao',
                                 'HistoricoComplementar',
                                 'Detalhe',
                                 'NumeroDebitos',
                                 'NumeroCreditos',
                                 'Moeda',
                                 'ValorTotalLancamento',
                                 'TaxaMoeda',
                                 'ValorTotalConvertido',
                                 'Observacao',
                                 'EventoID'],
                                 ['9999999999', '', '99/99/9999', '99/99/9999', '', '', '999999', '', '', '', '99', '99', '',
									'999999999.99', '9999999999999.99', '999999999.99', '', ''],
								 ['##########', '', dTFormat, dTFormat, '', '', '######', '', '', '', '##', '##', '',
									'###,###,##0.00', '#,###,###,###,##0.0000000', '###,###,##0.00', '', '']);

        alignColsInGrid(fg, [0, 6, 10, 11, 13, 14, 15]);
        fg.FrozenCols = 2;
    }
    else if (folderID == 24013)  // Pedidos Vinculados
    {
        headerGrid(fg, ['Pedido',
                       'Est',
                       'Transa��o',
                       'Total',
                       'Obrigatorio'], []);

        fillGridMask(fg, currDSO, ['PedidoID',
								 'Estado',
								 'TransacaoID',
								 'ValorTotal',
								 'Obrigatorio'],
                                 ['', '', '', '', ''],
                                 ['', '', '', '###,###,###,###.00', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[3, '###,###,###,###.00', 'S']]);

        alignColsInGrid(fg, [getColIndexByColKey(fg, 'PedidoID'),
                             getColIndexByColKey(fg, 'ValorTotal')]);

        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
    }

    else if (folderID == 21027)  //Dados Fiscais
    {
        headerGrid(fg, ['ID',
                        'Produto',
					    'Dados Fiscais Fabricante',
					    'FCI',
                        'Origem',
					    'PedItemDadoFiscalID'], [5]);

        glb_aCelHint = [
                         [0, 1, 'Numero e Data da NF Fabricante'],
                         [0, 2, 'C�digo da Ficha de Conte�do de Importa��o'],
                         [0, 3, 'Percentual do Conte�do de Importa��o']
                       ];

        fillGridMask(fg, currDSO, ['^PedItemID^dso01GridLkp^PedItemID^ProdutoID*',
                                   'PedItemID',
								   'NotaFiscalFabricante',
								   'CodigoFCI',
                                   '^PedItemID^dso01GridLkp^PedItemID^ItemAbreviado*',
								   'PedItemDadoFiscalID'],
                                 ['', '', '', '', '',''],
                                 ['', '', '', '', '','']);

        alignColsInGrid(fg, [0]);
        fg.FrozenCols = 4;

        // Merge de Colunas
        fg.MergeCells = 0;
        fg.MergeCol(-1) = true;
    }
}

// Duplica a altura da linha para comportar a descricao do Servico Federal
function duplicaGridRows() {
    var i = 0;
    if (glb_duplicaGridRows != null) {
        window.clearInterval(glb_duplicaGridRows);
        glb_duplicaGridRows = null;
    }

    for (i = 2; i < fg.Rows; i++)
        fg.RowHeight(i) = 600;

    return null;
}

function aReturnFieldsToGrid(nArray, nindex) {
    var i = 0;
    var aNewArray = new Array();

    for (i = 0; i < nArray.length; i++) {
        if (nindex != 4)
            aNewArray[aNewArray.length] = nArray[i][nindex];
        else {
            if (nArray[i][4] == true)
                aNewArray[aNewArray.length] = i;
        }

    }

    return aNewArray;
}
