using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class associarcampanha : System.Web.UI.OverflyPage
	{
		private Integer pedItemID;
		private Integer camProdutoID;
		private string valor;
		private java.lang.Boolean dissociar;
		private java.lang.Boolean atualiza = new java.lang.Boolean(true);
		private string resultado;

		protected override void PageLoad(object sender, EventArgs e)
		{
			PedidoItemCampanhaAssociar();

			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select '" + resultado + "' as Resultado"
				)
			);
		}

		protected void PedidoItemCampanhaAssociar()
		{
			// Roda a procedure sp_PedidoItem_CampanhaAssociar
			ProcedureParameters[] procParams = new ProcedureParameters[6];

			procParams[0] = new ProcedureParameters(
				"@PedItemID",
				System.Data.SqlDbType.Int,
				pedItemID.ToString());

			procParams[1] = new ProcedureParameters(
				"@CamProdutoID",
				System.Data.SqlDbType.Int,
				camProdutoID.ToString());

			procParams[2] = new ProcedureParameters(
				"@Valor",
				System.Data.SqlDbType.Decimal,
				valor);                

			procParams[3] = new ProcedureParameters(
				"@Dissociar",
				System.Data.SqlDbType.Bit,
                dissociar.booleanValue() ? 1 : 0);

			procParams[4] = new ProcedureParameters(
				"@Atualiza",
                System.Data.SqlDbType.Bit,
                atualiza.booleanValue() ? 1 : 0);

            procParams[5] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[5].Length = 8000;

			DataInterfaceObj.execNonQueryProcedure(
				"sp_PedidoItem_CampanhaAssociar",
				procParams);

            if (procParams[5].Data != DBNull.Value)
                resultado = procParams[5].Data.ToString();
		}

        protected Integer nPedItemID
		{
			get { return pedItemID; }
			set { pedItemID = (value != null ? value : new Integer(0)); }
		}

        protected Integer nCamProdutoID
		{
			get { return camProdutoID; }
			set { camProdutoID = (value != null ? value : new Integer(0)); }
		}

        protected string nValor
		{
			get { return valor; }
			set { valor = (value != null ? value : "0"); }
		}

        protected java.lang.Boolean nDissociar
		{
			get { return dissociar; }
			set { dissociar = (value != null ? value : new java.lang.Boolean(false)); }
		}
	}
}
