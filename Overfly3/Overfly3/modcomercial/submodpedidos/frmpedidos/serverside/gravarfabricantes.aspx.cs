using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class gravarfabricantes : System.Web.UI.OverflyPage
    {
        protected static Integer Zero = new Integer(0);
        protected static Integer[] Zer = new Integer[0];
        protected static Integer Um = new Integer(1);

        private Integer dataLen = Um;
        private Integer[] pedidoId = Zer;
        private Integer[] produtoId = Zer;
        private Integer[] quantidade = Zer;
        private Integer[] fabricanteId = Zer;
        private Integer tipoResultado;
        private string resultado;

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(PedidoFabricantes());
        }

        // Roda a procedure sp_Pedido_Fabricantes
        protected DataSet PedidoFabricantes()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[6];
            for (int i = 0; i < dataLen.intValue(); i++)
            {

                procParams[0] = new ProcedureParameters(
                    "@PedidoID",
                    System.Data.SqlDbType.Int,
                    (pedidoId.Length > 0) ? (Object)pedidoId[i].ToString() : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                    "@ProdutoID",
                    System.Data.SqlDbType.Int,
                    (produtoId.Length > 0) ? (Object)produtoId[i].ToString() : DBNull.Value);

                procParams[2] = new ProcedureParameters(
                    "@Quantidade",
                    System.Data.SqlDbType.Int,
                    (quantidade.Length > 0) ? (Object)quantidade[i].ToString() : DBNull.Value);

                procParams[3] = new ProcedureParameters(
                    "@FabricanteID",
                    System.Data.SqlDbType.Int,
                     (fabricanteId.Length > 0) ? (Object)fabricanteId[i].ToString() : DBNull.Value);

                procParams[4] = new ProcedureParameters(
                    "@TipoResultado",
                    System.Data.SqlDbType.Int,
                    (tipoResultado.intValue() != null) ? (Object)tipoResultado.ToString() : DBNull.Value);

                procParams[5] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[5].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_Pedido_Fabricantes",
                    procParams);

                resultado += procParams[5].Data.ToString();
            }

            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (resultado != null ? "'" + resultado + "' " : "NULL") +
                 " as Resultado ");
        }

        protected Integer nDataLen
        {
            get { return dataLen; }
            set { dataLen = value != null ? value : new Integer(1); }
        }

        protected Integer[] nPedidoID
        {
            set
            {
                pedidoId = value;

                for (int i = 0; i < pedidoId.Length; i++)
                {
                    if (pedidoId[i] == null)
                        pedidoId[i] = Zero;
                }
            }
        }

        protected Integer[] nProdutoID
        {
            set
            {
                produtoId = value;

                for (int i = 0; i < produtoId.Length; i++)
                {
                    if (produtoId[i] == null)
                        produtoId[i] = Zero;
                }
            }
        }

        protected Integer[] nQuantidade
        {
            set
            {
                quantidade = value;

                for (int i = 0; i < quantidade.Length; i++)
                {
                    if (quantidade[i] == null)
                        quantidade[i] = Zero;
                }
            }
        }

        protected Integer[] nFabricanteID
        {
            set
            {
                fabricanteId = value;

                for (int i = 0; i < fabricanteId.Length; i++)
                {
                    if (fabricanteId[i] == null)
                        fabricanteId[i] = Zero;
                }
            }
        }

        protected Integer nTipoResultado
        {
            set { tipoResultado = value != null ? value : Zero; }
        }
    }
}

