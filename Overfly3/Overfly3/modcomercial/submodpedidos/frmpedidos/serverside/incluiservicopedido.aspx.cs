using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class incluiservicopedido : System.Web.UI.OverflyPage
    {
        private static Integer zero = new Integer(0);

        private Integer pedidoId;
        private Integer servicoID;
        private Integer produtoID;
        private string valor;
        private string quantidade;
        private Integer finalidade;
        private Integer unidade;
        private string descricao;
        private string sql;
        private int pedItemID;
        public Integer nProdutoID
        {
            get { return produtoID; }
            set { produtoID = value; }
        }

        public Integer nServicoID
        {
            get { return servicoID; }
            set { servicoID = value; }
        }
        protected Integer nPedidoID
        {
            get { return pedidoId; }
            set { pedidoId = value != null ? value : zero; }
        }


        protected string nValor
        {
            get { return valor; }
            set { valor = value != null ? value : "0"; }
        }

        public string nQuantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }
        public Integer nFinalidadeID
        {
            get { return finalidade; }
            set { finalidade = value; }
        }

        public Integer nUnidadeID
        {
            get { return unidade; }
            set { unidade = value; }
        }

        protected string sDescricao
        {
            get { return descricao; }
            set { descricao = value != null ? value : null; }
        }

        // Pega os parāmetros para a procedure sp_PedidoItem_Gerador
        protected void PedidoItemGerador()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[15];

            procParams[0] = new ProcedureParameters(
                "@PedidoID",
                System.Data.SqlDbType.Int,
                pedidoId);

            procParams[1] = new ProcedureParameters(
                "@ProdutoID",
                System.Data.SqlDbType.Int,
                ((produtoID.intValue() == 0) ? System.DBNull.Value : (Object)produtoID));

            procParams[2] = new ProcedureParameters(
                "@ServicoID",
                System.Data.SqlDbType.Int,
                ((servicoID == null) ? System.DBNull.Value : (Object)servicoID));

            procParams[3] = new ProcedureParameters(
                "@Quantidade",
                System.Data.SqlDbType.Decimal,
                quantidade);

            procParams[4] = new ProcedureParameters(
                "@Valorinterno",
                System.Data.SqlDbType.Money,
               System.DBNull.Value);

            procParams[5] = new ProcedureParameters(
                "@Valoricmsstinterno",
                System.Data.SqlDbType.Money,
                System.DBNull.Value);

            procParams[6] = new ProcedureParameters(
                "@ValorRevenda",
                System.Data.SqlDbType.Money,
                System.DBNull.Value);

            procParams[7] = new ProcedureParameters(
                "@ValorICMSSTRevenda",
                System.Data.SqlDbType.Money,
                System.DBNull.Value);

            procParams[8] = new ProcedureParameters(
                "@ValorUnitario",
                System.Data.SqlDbType.Money,
                valor);

            procParams[9] = new ProcedureParameters(
                "@FinalidadeID",
                System.Data.SqlDbType.Int,
                ((finalidade == null) ? System.DBNull.Value : (Object)finalidade));

            procParams[10] = new ProcedureParameters(
                "@CFOPID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[11] = new ProcedureParameters(
                "@PedItemReferenciaID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[12] = new ProcedureParameters(
                "@PedItemEncomendaID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[13] = new ProcedureParameters(
                "@TipoGerador",
                System.Data.SqlDbType.Int,
                1);

            procParams[14] = new ProcedureParameters(
                "@NovoPedItemID",
                System.Data.SqlDbType.Int,
                null,
                ParameterDirection.Output);

            DataInterfaceObj.execNonQueryProcedure(
                "sp_PedidoItem_Gerador",
                procParams);
            if (procParams[14].Data != DBNull.Value)
            {
                pedItemID = (int)procParams[14].Data;
            }

        }

        protected void UpdateDescricaoItem()
        {
            if (pedItemID > 0)
                sql = sql + "UPDATE Pedidos_Itens SET DescricaoItem = '" + descricao + "', EstadoID = 2 " +
                      "WHERE (PedItemID = " + pedItemID + ") ";

            if (sql != "")
            {
                DataInterfaceObj.ExecuteSQLCommand(sql);
            }
        }

        protected void UpdateUnidade()
        {
            if (unidade != null)
            {
                if (unidade.intValue() > 0)
                    sql = sql + "UPDATE Pedidos_Itens SET UnidadeID = " + unidade.intValue() + " " +
                          "WHERE (PedItemID = " + pedItemID + ") ";

            }
            if (sql != "")
            {
                DataInterfaceObj.ExecuteSQLCommand(sql);
            }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            PedidoItemGerador();
            UpdateDescricaoItem();
            UpdateUnidade();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select 0 as PedidoID, 0 as fldErrorNumber, space(0) as fldErrorText "
                )
            );
        }
    }
}
