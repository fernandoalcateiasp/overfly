using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class gravaparceladiversos : System.Web.UI.OverflyPage
	{
		private static Integer zero = new Integer(0);
		
		private Integer pedidoId;
		private Integer pessoaId;
		private Integer historicoPadraoId;
		private Integer tipocomissao;
        private Integer formaPagamentoId;
		private Integer prazo;
		private string valor;

		protected override void PageLoad(object sender, EventArgs e)
		{
			int fldresp;

            fldresp = DataInterfaceObj.ExecuteSQLCommand(
                "INSERT INTO Pedidos_Parcelas (FormaPagamentoID, HistoricoPadraoID, TipoParcelaID, PedidoID, PessoaID, PrazoPagamento, ValorParcela, Ordem) " +
                "VALUES(" + nFormaPagamentoID + ", " + historicoPadraoId + ", " + (tipocomissao.intValue() == 0 ? "NULL " : (Object)tipocomissao.ToString())
                                            + ", " + pedidoId + ", " + pessoaId + ", " + prazo + ", '" + valor + "', 99)");
			
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select " + fldresp + " as fldresp"
				)
			);
		}

        protected Integer nPedidoID
		{
			get { return pedidoId; }
			set { pedidoId = value != null ? value : zero; }
		}

        protected Integer nPessoaID
		{
			get { return pessoaId; }
			set { pessoaId = value != null ? value : zero; }
		}

        protected Integer nHistoricoPadraoID
		{
			get { return historicoPadraoId; }
			set { historicoPadraoId = value != null ? value : zero; }
		}

        protected Integer nTipoComissao
        {
            get { return tipocomissao; }
            set { tipocomissao = value != null ? value : zero; }
        }

        protected Integer nFormaPagamentoID
		{
			get { return formaPagamentoId; }
			set { formaPagamentoId = value != null ? value : zero; }
		}

        protected Integer Prazo
		{
			get { return prazo; }
			set { prazo = value != null ? value : zero; }
		}

        protected string Valor
		{
			get { return valor; }
			set { valor = value != null ? value : "0"; }
		}
	}
}
