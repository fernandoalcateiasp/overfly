using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class listaasstec : System.Web.UI.OverflyPage
	{
		private static Integer zero = new Integer(0);
		
		private Integer pedidoId;
		private Integer numMovimentoId;
		private Integer pedProdutoId;
		private Integer pedLoteId;

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(PedidoAsstecNumeroSerie());
		}

		// Roda a procedure sp_Pedido_AsstecNumeroSerie
		private DataSet PedidoAsstecNumeroSerie()
		{
			ProcedureParameters[] procParams = new ProcedureParameters[6];

			procParams[0] = new ProcedureParameters(
				"@PedidoID",
				System.Data.SqlDbType.Int,
				pedidoId.ToString());

			procParams[1] = new ProcedureParameters(
				"@NumMovimentoID",
				System.Data.SqlDbType.Int,
				numMovimentoId.intValue() != 0 ? (Object)numMovimentoId.ToString() : DBNull.Value);

			procParams[2] = new ProcedureParameters(
				"@PedProdutoID",
				System.Data.SqlDbType.Int,
				pedProdutoId.intValue() != 0 ? (Object)pedProdutoId.ToString() : DBNull.Value);

			procParams[3] = new ProcedureParameters(
				"@PedLoteID",
				System.Data.SqlDbType.Int,
				pedLoteId.intValue() != 0 ? (Object)pedLoteId.ToString() : DBNull.Value);

			procParams[4] = new ProcedureParameters(
				"@AsstecID",
				System.Data.SqlDbType.Int,
				DBNull.Value);

			procParams[5] = new ProcedureParameters(
				"@TipoResultado",
				System.Data.SqlDbType.Int,
				"1");

			return DataInterfaceObj.execQueryProcedure(
				"sp_Pedido_AsstecNumeroSerie",
				procParams);
		}

		public Integer nPedidoID
		{
			get { return pedidoId; }
			set { pedidoId = value != null ? value : zero; }
		}
		
		public Integer nNumMovimentoID
		{
			get { return numMovimentoId; }
			set { numMovimentoId = value != null ? value : zero; }
		}
		
		public Integer nPedProdutoID
		{
			get { return pedProdutoId; }
			set { pedProdutoId = value != null ? value : zero; }
		}

		public Integer nPedLoteID
		{
			get { return pedLoteId; }
			set { pedLoteId = value != null ? value : zero; }
		}
	}
}
