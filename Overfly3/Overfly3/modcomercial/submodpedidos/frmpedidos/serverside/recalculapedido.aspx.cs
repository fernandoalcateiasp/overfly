using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class recalculapedido : System.Web.UI.OverflyPage
	{
		private Integer pedidoId;    
		private java.lang.Boolean recalcula;

		protected override void PageLoad(object sender, EventArgs e)
		{
			string sql = 
				"UPDATE Pedidos SET Recalcula = " + (recalcula.booleanValue() ? "1" : "0") + " WHERE (PedidoID = " + pedidoId + ")";

			if(recalcula.booleanValue())
			{
                sql +=
                    " EXEC sp_Pedido_Atualiza " + pedidoId + ", NULL, 2";
			}
			
			DataInterfaceObj.ExecuteSQLCommand(sql);
			
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select 0 as fldErrorNumber, '' as fldErrorText"
				)
			);
		}

		protected Integer nPedidoID
		{
			get { return pedidoId; }
			set { pedidoId = value != null ? value : new Integer(0); }
		}

		protected java.lang.Boolean bRecalcula
		{
			get { return recalcula; }
			set { recalcula = value != null ? value : new java.lang.Boolean(false); }
		}
	}
}
