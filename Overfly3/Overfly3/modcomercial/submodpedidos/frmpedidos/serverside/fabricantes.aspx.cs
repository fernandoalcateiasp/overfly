using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class fabricantes : System.Web.UI.OverflyPage
	{
        protected static Integer Zero = new Integer(0);
        
        private Integer dataLen;
		private Integer[] pedidoId;
		private Integer[] produtoId;
		private Integer[] quantidade;
		private Integer[] fabricanteId;
		private Integer[] tipoResultado;
		private string[] resultado;

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(PedidoFabricantes());
		}

		// Roda a procedure sp_Pedido_Fabricantes
        protected DataSet PedidoFabricantes()
        {
            for (int i = 0; i < dataLen.intValue(); i++)
            {
                ProcedureParameters[] procParams = new ProcedureParameters[6];
                procParams[0] = new ProcedureParameters(
                    "@PedidoID",
                    System.Data.SqlDbType.Int,
                    pedidoId != null ? (Object)pedidoId.ToString() : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                    "@ProdutoID",
                    System.Data.SqlDbType.Int,
                    produtoId != null ? (Object)produtoId.ToString() : DBNull.Value);

                procParams[2] = new ProcedureParameters(
                    "@Quantidade",
                    System.Data.SqlDbType.Int,
                    quantidade != null ? (Object)quantidade : DBNull.Value);

                procParams[3] = new ProcedureParameters(
                    "@FabricanteID",
                    System.Data.SqlDbType.Int,
                    fabricanteId != null ? (Object)fabricanteId : DBNull.Value);

                procParams[4] = new ProcedureParameters(
                    "@TipoResultado",
                    System.Data.SqlDbType.Int,
                    tipoResultado.ToString());

                procParams[5] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[5].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_Pedido_Fabricantes",
                    procParams);

                if (procParams[5].Data != DBNull.Value)
                {
                    resultado[i] = procParams[5].Data.ToString();
                }

            }

            return DataInterfaceObj.getRemoteData(
                   "select " +
                   resultado +
                   " as Resultado ");
        }

		protected Integer nDataLen
		{
			get { return dataLen; }
			set { dataLen = value != null ? value : new Integer(1); }
		}

        protected Integer[] nPedidoID
		{
            set
            {
                pedidoId = value;

                for (int i = 0; i < pedidoId.Length; i++)
                {
                    if (pedidoId[i] == null)
                        pedidoId[i] = Zero;
                }
            }
		}
		
		protected Integer[] nProdutoID
		{
            set
            {
                produtoId = value;

                for (int i = 0; i < produtoId.Length; i++)
                {
                    if (produtoId[i] == null)
                        produtoId[i] = Zero;
                }
            }
		}

        protected Integer[] nQuantidade
		{
            set
            {
                quantidade = value;

                for (int i = 0; i < quantidade.Length; i++)
                {
                    if (quantidade[i] == null)
                        quantidade[i] = Zero;
                }
            }
		}

        protected Integer[] nFabricanteID
		{
            set
            {
                fabricanteId = value;

                for (int i = 0; i < fabricanteId.Length; i++)
                {
                    if (fabricanteId[i] == null)
                        fabricanteId[i] = Zero;
                }
            }
		}

        protected Integer[] nTipoResultado
		{
            set
            {
                tipoResultado = value;

                for (int i = 0; i < tipoResultado.Length; i++)
                {
                    if (tipoResultado[i] == null)
                        tipoResultado[i] = Zero;
                }
            }
		}

        protected string[] sResultado
		{
            set
            {
                resultado = value;

                for (int i = 0; i < resultado.Length; i++)
                {
                    if (resultado[i] == null || resultado[i].Length == 0)
                        resultado[i] = "NULL";
                }
            }
		}
	}
}
