using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class listarfabricantes : System.Web.UI.OverflyPage
    {
        protected static Integer Zero = new Integer(0);
        private Integer pedidoId;
        private Integer tipoResultado;
        private string resultado;

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(PedidoFabricantes());
        }

        // Roda a procedure sp_Pedido_Fabricantes
        protected DataSet PedidoFabricantes()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[6];

            procParams[0] = new ProcedureParameters(
                "@PedidoID",
                System.Data.SqlDbType.Int,
                (pedidoId.intValue() > 0) ? (Object)pedidoId.ToString() : DBNull.Value);

            procParams[1] = new ProcedureParameters(
                "@ProdutoID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[2] = new ProcedureParameters(
                "@Quantidade",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[3] = new ProcedureParameters(
                "@FabricanteID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[4] = new ProcedureParameters(
                "@TipoResultado",
                System.Data.SqlDbType.Int,
                (tipoResultado.intValue() != null) ? (Object)tipoResultado.ToString() : DBNull.Value);

            procParams[5] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[5].Length = 8000;

            return DataInterfaceObj.execQueryProcedure(
                   "sp_Pedido_Fabricantes",
                   procParams);
        }

        protected Integer nPedidoID
        {
            set { pedidoId = value != null ? value : Zero; }
        }

        protected Integer nTipoResultado
        {
            set { tipoResultado = value != null ? value : Zero; }
        }
    }
}
