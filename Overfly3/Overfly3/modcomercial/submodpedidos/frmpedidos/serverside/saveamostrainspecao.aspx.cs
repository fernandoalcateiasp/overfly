using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class saveamostrainspecao : System.Web.UI.OverflyPage
	{
		private static Integer zero = new Integer(0);
		
		private Integer dataLen;
		private Integer dataElemLen;
		private Object[][] data;
		
		public Integer nDataLen
		{
			get { return dataLen; }
			set { dataLen = value != null ? value : zero; }
		}
		
		public Integer nDataElemLen
		{
			get { return dataElemLen; }
			set { dataElemLen = value != null ? value : zero; }
		}
		
		public Object[] aData
		{
			set
			{
				if(value != null)
				{
					int element = 0;
					
					data = new Object[dataLen.intValue()][];
					for (int j = 0; j < (dataLen.intValue() - 1); j++)
					{
						data[j] = new Object[dataElemLen.intValue()];
						for (int i = 0; i < (dataElemLen.intValue() - 1); i++)
						{
							data[j][i] = value[element++];
						}
					}
				}
				else
				{
					data = new Object[0][];
				}
			}
		}
		
		protected string GetSql()
		{
			string sql = "";
			
			for(int i = 0; i < dataLen.intValue(); i++)
			{
				sql += 
					" UPDATE Pedidos_RIR_Itens SET " +
						" ResultadoID=" + data[i][1].ToString() + ", " +
						" Observacao=" + data[i][2].ToString() + " " +
					" WHERE RIRNumeroSerieID = " + data[i][0].ToString();
			}
			
			return sql;
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.ExecuteSQLCommand(GetSql());
			
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select 0 as fldErrorNumber, '' as fldErrorText"
				)
			);
		}
	}
}
