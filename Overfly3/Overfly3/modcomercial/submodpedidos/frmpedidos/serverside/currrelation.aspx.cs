using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class currrelation : System.Web.UI.OverflyPage
	{
		private static Integer zero = new Integer(0);
		private static Integer um = new Integer(0);
		private static java.lang.Boolean False = new java.lang.Boolean(false);
		private Integer pessoaId;
		private Integer empresaId;
		private Integer tipo;
		private Integer produtoId;
		private java.lang.Boolean ehCliente;

        protected Integer nPessoaID
		{
			get { return pessoaId; }
			set { pessoaId = value != null ? value : zero; }
		}

        protected Integer nEmpresaID
		{
			get { return empresaId; }
			set { empresaId = value != null ? value : zero; }
		}

        protected Integer nTipo
		{
			get { return tipo; }
			set { tipo = value != null ? value : um; }
		}

        protected Integer nProdutoID
		{
			get { return produtoId; }
			set { produtoId = value != null ? value : zero; }
		}

        protected java.lang.Boolean bEhCliente
		{
			get { return ehCliente; }
			set { ehCliente = value != null ? value : False; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			Integer temp;

			if (tipo.intValue() == 1)
			{
				if (!ehCliente.booleanValue())
				{
					temp = pessoaId;
					pessoaId = empresaId;
					empresaId = pessoaId;
				}

				WriteResultXML(
					DataInterfaceObj.getRemoteData(
						"SELECT TOP 1 RelacaoID AS fldID " +
								 "FROM RelacoesPessoas WITH(NOLOCK) " +
								 "WHERE TipoRelacaoID=21 " +
								 "AND SujeitoID = " + pessoaId + " " +
								 "AND ObjetoID = " + empresaId
					)
				);
			}
			else
			{
				WriteResultXML(
					DataInterfaceObj.getRemoteData(
						"SELECT TOP 1 RelacaoID AS fldID " +
                                 "FROM RelacoesPesCon WITH(NOLOCK) " +
								 "WHERE TipoRelacaoID=61 " +
								 "AND SujeitoID = " + empresaId + " " +
								 "AND ObjetoID = " + produtoId
					)
				);
			}
		}
	}
}
