using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class comissoespedidos : System.Web.UI.OverflyPage
    {
        private Integer empresaID;
        private Integer pessoaID;
        private Integer parceiroID;
        private Integer produtoID;
        private Integer servicoID;
        private string totais;
        private Integer userID;
        private string discriminacao;
        private string observacoes;
        private string pedparcelaid;
        private string resultado;
        private int pedidoID;

        protected override void PageLoad(object sender, EventArgs e)
        {
            GeraPedidoComissoes();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                "select " + 
                 (pedidoID != null ? "'" + (pedidoID + " ' as PedidoID ") : "") + 

                 (resultado != null ? "'," + (resultado + "' as Resultado ") : ",null as Resultado")));
        }

        protected void GeraPedidoComissoes()
        {
            // Roda a procedure sp_pedido_comissaoGerador
            ProcedureParameters[] procParams = new ProcedureParameters[13];

            procParams[0] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                empresaID.ToString());

            procParams[1] = new ProcedureParameters(
                "@PessoaID",
                System.Data.SqlDbType.Int,
                pessoaID.ToString());

            procParams[2] = new ProcedureParameters(
                "@ParceiroID",
                System.Data.SqlDbType.Int,
                parceiroID.ToString());

            procParams[3] = new ProcedureParameters(
                "@ProdutoID",
                System.Data.SqlDbType.Int,
                produtoID != null ? (Object)produtoID.ToString() : System.DBNull.Value);

            procParams[4] = new ProcedureParameters(
                "@ServicoID",
                System.Data.SqlDbType.Int,
                servicoID !=  null ? (Object)servicoID.ToString() : System.DBNull.Value);

            procParams[5] = new ProcedureParameters(
                "@Discriminacao",
                System.Data.SqlDbType.VarChar,
                discriminacao.ToString() != "NULL" ? (Object)discriminacao.ToString() : System.DBNull.Value);

            procParams[6] = new ProcedureParameters(
               "@Observacoes",
               System.Data.SqlDbType.VarChar,
               observacoes.ToString() != "NULL" ? (Object)observacoes.ToString() : System.DBNull.Value);

            procParams[7] = new ProcedureParameters(
                "@ParcelasAssociar",
                System.Data.SqlDbType.VarChar,
                pedparcelaid != null ? (Object)pedparcelaid.ToString() : System.DBNull.Value);

            procParams[8] = new ProcedureParameters(
                "@ValorTotalPedido",
                System.Data.SqlDbType.Money,
                totais != null ? (Object)totais.ToString() : System.DBNull.Value);

            procParams[9] = new ProcedureParameters(
               "@PesDadosBancariosID",
               System.Data.SqlDbType.VarChar,
               System.DBNull.Value);

            procParams[10] = new ProcedureParameters(
               "@UsuarioID",
               System.Data.SqlDbType.Int,
               userID != null ? (Object)userID.ToString() : System.DBNull.Value);

            procParams[11] = new ProcedureParameters(
               "@PedidoID",
               System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);

            procParams[12] = new ProcedureParameters(
               "@Resultado",
               System.Data.SqlDbType.VarChar,
               DBNull.Value,
               ParameterDirection.InputOutput);
            procParams[12].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_pedido_comissaoGerador",
                procParams);

            if (procParams[11].Data != DBNull.Value)
                pedidoID = (int)procParams[11].Data;

            if (procParams[12].Data != DBNull.Value)
                resultado = procParams[12].Data.ToString();
        }

        protected Integer nEmpresaID
        {
            get { return empresaID; }
            set { empresaID = (value != null ? value : new Integer(0)); }
        }

        protected Integer nPessoaID
        {
            get { return pessoaID; }
            set { pessoaID = (value != null ? value : new Integer(0)); }
        }

        protected Integer nParceiroID
        {
            get { return parceiroID; }
            set { parceiroID = (value != null ? value : new Integer(0)); }
        }

        protected Integer nProdutoID
        {
            get { return produtoID; }
            set { produtoID = (value != null ? value : new Integer(0)); }
        }

        protected Integer nServicoID
        {
            get { return servicoID; }
            set { servicoID = (value != null ? value : null); }
        }

        protected string nTotais
        {
            get { return totais; }
            set { totais = (value != null ? value : ""); }
        }

        protected Integer nUser
        {
            get { return userID; }
            set { userID = (value != null ? value : new Integer(0)); }
        }

        protected string strDiscriminacao
        {
            get { return discriminacao; }
            set { discriminacao = value != null ? value : ""; }
        }

        protected string strObservacoes
        {
            get { return observacoes; }
            set { observacoes = value != null ? value : ""; }
        }

        protected string strPedParcelaID
        {
            get { return pedparcelaid; }
            set { pedparcelaid = value != null ? value : ""; }
        }
    }
}
