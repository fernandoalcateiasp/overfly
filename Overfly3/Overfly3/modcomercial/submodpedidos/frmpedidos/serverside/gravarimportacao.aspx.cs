using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class gravarimportacao : System.Web.UI.OverflyPage
    {
        private static Integer[] empty = new Integer[0];
        
        private string resultado;

        private Integer dataLen;
        private Integer[] pedItemImpID;
        private Integer[] pedItemID;
        private string[] invoice;
        protected java.lang.Double[] taxaMoedaImportacao;
        private string[] dtInvoice;

        protected Integer nDataLen
        {
            get { return dataLen; }
            set { dataLen = value; }
        }
        protected Integer[] nPedItemImpID
        {
            get { return pedItemImpID; }
            set { pedItemImpID = value != null ? value : empty; }
        }
        protected Integer[] nPedItemID
        {
            get { return pedItemID; }
            set { pedItemID = value != null ? value : empty; }
        }
        protected string[] nInvoice
        {
            get { return invoice; }
            set { invoice = value != null ? value : null; }
        }
        protected java.lang.Double[] nTaxaMoedaImportacao
        {
            get { return taxaMoedaImportacao; }
            set { taxaMoedaImportacao = value; }
        }

        protected string[] dDtInvoice
        {
            get { return dtInvoice; }
            set { dtInvoice = value != null ? value : null; }
        }


        protected string GetSql()
        {
            string sql = "";

            for (int i = 0; i < dataLen.intValue(); i++)
            {
                if (pedItemImpID[i].intValue() == 0)
                {
                    sql = "INSERT INTO Pedidos_Itens_Importacao(PedItemID, Invoice, DtInvoice, TaxaMoedaImportacao) " +
                     "VALUES(" + pedItemID[i] + "," + invoice[i] + ",'" + dtInvoice[i] + "', " + ((taxaMoedaImportacao[i] != null && taxaMoedaImportacao[i].doubleValue() != 0.0) ? (Object)taxaMoedaImportacao[i].doubleValue() : DBNull.Value) + ")";
                }
                else
                {
                    sql = "UPDATE Pedidos_Itens_Importacao SET Invoice = " + invoice[i] + ", DtInvoice='" + dtInvoice[i] +
                         "', TaxaMoedaImportacao=" + ((taxaMoedaImportacao[i] != null && taxaMoedaImportacao[i].doubleValue() != 0.0) ? (Object)taxaMoedaImportacao[i].doubleValue() : DBNull.Value) + " WHERE PedItemImpID=" + pedItemImpID[i];
                }

                try
                {
                    DataInterfaceObj.ExecuteSQLCommand(sql);
                }
                 catch (System.Exception exception)
                {

                    resultado += exception.Message + " \r\n ";
                }

            }

            return resultado;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            resultado = GetSql();

            if (resultado == null)
                resultado = " Erro";

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select '" + resultado + "' as Resultado"
                )
            );
        }
    }
}
