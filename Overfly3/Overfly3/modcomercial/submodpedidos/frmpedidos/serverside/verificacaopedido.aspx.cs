using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class verificacaopedido : System.Web.UI.OverflyPage
	{
		private static Integer zero = new Integer(0);

		private Integer pedidoId = zero;
		private Integer currEstadoId = zero;
		private Integer newEstadoID = zero;
		private Integer depositoId = zero;
		private Integer userId = zero;
		private java.lang.Boolean limiteMaximo;
        private java.lang.Boolean simulacao = new java.lang.Boolean(false);
		private Integer tipoModal = zero;
		private Integer numeroVolumes = zero;
		private string conhecimento;
		private string portador;
		private string documentoPortador;
		private string documentoFederalPortador;
		private string placaVeiculo;
		private string ufVeiculo;
		
		private int response;
		private string mensagem;
		
		protected Integer nPedidoID
		{
			set { pedidoId = value != null ? value : zero; }
		}
		
		protected Integer nCurrEstadoID
		{
			set { currEstadoId = value != null ? value : zero; }
		}

		protected Integer nNewEstadoID
		{
			set { newEstadoID = value != null ? value : zero; }
		}

		protected Integer nDepositoID
		{
			set { depositoId = value != null ? value : zero; }
		}
		
		protected Integer nUserID
		{
			set { userId = value != null ? value : zero; }
		}
		
		protected java.lang.Boolean bLimiteMaximo
		{
			set { limiteMaximo = value != null ? value : new java.lang.Boolean(false); }
		}
		
		protected Integer nTipoModal
		{
			set { tipoModal = value != null ? value : zero; }
		}

        protected java.lang.Boolean bSimulacao
        {
            set { simulacao = value != null ? value : new java.lang.Boolean(false); }
        }		
		
		protected Integer nNumeroVolumes
		{
			set { numeroVolumes = value != null ? value : zero; }
		}
		
		protected string sConhecimento
		{
			set { conhecimento = value != null ? value : ""; }
		}
		
		protected string sPortador
		{
			set { portador = value != null ? value : ""; }
		}

		protected string sDocumentoPortador
		{
			set { documentoPortador = value != null ? value : ""; }
		}

		protected string sDocumentoFederalPortador
		{
			set { documentoFederalPortador = value != null ? value : ""; }
		}

		protected string sPlacaVeiculo
		{
			set { placaVeiculo = value != null ? value : ""; }
		}

		protected string sUFVeiculo
		{
			set { ufVeiculo = value != null ? value : ""; }
		}

		protected void PedidoVerifica()
		{
			ProcedureParameters[] procparam = new ProcedureParameters[9];
			
			procparam[0] = new ProcedureParameters(
				"@PedidoID", 
				SqlDbType.Int, 
				pedidoId.ToString());
			procparam[1] = new ProcedureParameters(
				"@EstadoDeID",
				SqlDbType.Int,
				currEstadoId.ToString()); 
			procparam[2] = new ProcedureParameters(
				"@EstadoParaID",
				SqlDbType.Int,
				newEstadoID.ToString()); 
			procparam[3] = new ProcedureParameters(
				"@UsuarioID", 
				SqlDbType.Int, 
				userId.ToString()); 
			procparam[4] = new ProcedureParameters(
				"@LimiteMaximo",
				SqlDbType.Bit,
				limiteMaximo.booleanValue() ? 1 : 0); 
			procparam[5] = new ProcedureParameters(
				"@dtData", 
				SqlDbType.DateTime,
				DBNull.Value);
            procparam[6] = new ProcedureParameters(
                "@Simulacao",
                SqlDbType.Bit,
                simulacao.booleanValue() ? 1 : 0); 
			procparam[7] = new ProcedureParameters(
				"@Resultado",
				SqlDbType.Int,
				DBNull.Value,
				ParameterDirection.InputOutput); 
			procparam[8] = new ProcedureParameters(
				"@Mensagem",
				SqlDbType.VarChar,
				DBNull.Value,
				ParameterDirection.InputOutput);
			procparam[8].Length = 8000;
			
			// Executa a procedure.
			DataInterfaceObj.execNonQueryProcedure(
				"sp_Pedido_Verifica",
				procparam
			);
			
			// Obt�m o resultado da execu��o.
			response = int.Parse(procparam[7].Data.ToString());
			mensagem = procparam[8].Data.ToString();
		}
		
		protected int updatePedidos()
		{
			string sql = "";
			
			// Modal de Embalagem
			if(tipoModal.intValue() == 3)
			{
				if(response == 0 || response == 1)
				{
					sql = 
						" UPDATE Pedidos SET " +
							" NumeroVolumes = " + numeroVolumes + ", " +
							" TotalPesoBruto = dbo.fn_Pedido_Totais(" + pedidoId + ", 5), " +
							" TotalPesoLiquido = dbo.fn_Pedido_Totais(" + pedidoId +  ", 6) " +
						" WHERE PedidoID= " + pedidoId;
				}			         
			}
			// Modal de Despacho
			else if(tipoModal.intValue() == 5)
			{
				if(response == 0)
				{
                    sql =
                        " UPDATE Pedidos SET " +
                            " Conhecimento = '" + conhecimento + "'," +
                            " Portador = '" + portador + "'," +
                            " DocumentoPortador = '" + documentoPortador + "'," +
                            " DocumentoFederalPortador = '" + documentoFederalPortador + "'," +
                            " PlacaVeiculo = '" + placaVeiculo + "'," +
                            " UFVeiculo = '" + ufVeiculo + "'" +
                        " WHERE PedidoID= " + pedidoId;
						
                        /*
						" UPDATE NotasFiscais SET" +
							" PlacaVeiculo = '" + placaVeiculo + "'," +
							" UFVeiculo = '" + ufVeiculo + "'" +
						" WHERE NotaFiscalID = " +
							"(SELECT NotaFiscalID FROM Pedidos WITH(NOLOCK) WHERE PedidoID = "
								+ pedidoId + ")";
                          */

				}
			}
            else if (tipoModal.intValue() == 6)
            {
                if (response == 2)
                {
                    sql = "EXEC sp_Pedido_LogErro " + pedidoId + ", " + currEstadoId + ", " + newEstadoID + ", '" + mensagem + "', 0, 1, " + userId + "";
                }
            }

			// Retorna o n�mero de linhas afetadas.
			return DataInterfaceObj.ExecuteSQLCommand(sql);
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			PedidoVerifica();
			updatePedidos();
			
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select " + response + " as Resultado, '" +
						mensagem + "' as Mensagem"
				)
			);
		}
	}
}
