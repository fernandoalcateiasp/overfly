using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;
using Overfly3.systemEx.serverside;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class sincronizavalorlocalizar : System.Web.UI.OverflyPage
    {
        private static Integer zero = new Integer(0);
        private string Valor2ID;
        private int valorid;

        private static java.lang.Double[] emptyDbl = new java.lang.Double[0];
        //private static java.lang.Double emptyDbls = Constants.DBL_EMPTY_SET;

        private ProcedureParameters[] procparam = new ProcedureParameters[] {
		/*  0 */	new ProcedureParameters("@ValorID", SqlDbType.Int, DBNull.Value), 
		/*  1 */	new ProcedureParameters("@FinanceiroID", SqlDbType.Int, DBNull.Value), 
		/*  2 */	new ProcedureParameters("@FormaPagamentoID", SqlDbType.Int, DBNull.Value), 
		/*  3 */	new ProcedureParameters("@RelPesContaID", SqlDbType.Int, DBNull.Value), 
		/*  4 */	new ProcedureParameters("@Documento", SqlDbType.VarChar, DBNull.Value), 
		/*  5 */	new ProcedureParameters("@Emitente", SqlDbType.VarChar, DBNull.Value), 
		/*  6 */	new ProcedureParameters("@dtEmissao", SqlDbType.DateTime, DBNull.Value), 
		/*  7 */	new ProcedureParameters("@BancoAgencia", SqlDbType.VarChar, DBNull.Value), 
		/*  8 */	new ProcedureParameters("@Conta", SqlDbType.VarChar, DBNull.Value), 
		/*  9 */	new ProcedureParameters("@NumeroDocumento", SqlDbType.VarChar, DBNull.Value), 
		/* 10 */	new ProcedureParameters("@Valor", SqlDbType.Money, DBNull.Value), 
		/* 11 */	new ProcedureParameters("@ValorEncargos", SqlDbType.Money, DBNull.Value),  // Paramentro novo. BJBN 12/09/2016
        /* 12 */	new ProcedureParameters("@ValorAplicar", SqlDbType.Money, DBNull.Value), 
		/* 13 */	new ProcedureParameters("@Observacao", SqlDbType.VarChar, DBNull.Value), 
		/* 14 */	new ProcedureParameters("@UsuarioID", SqlDbType.Int, DBNull.Value), 
		/* 15 */	new ProcedureParameters("@Incluir", SqlDbType.Bit, DBNull.Value), 
		/* 16 */	new ProcedureParameters("@Valor2ID", SqlDbType.Int, DBNull.Value, ParameterDirection.InputOutput), 
		/* 17 */	new ProcedureParameters("@Resultado", SqlDbType.VarChar, DBNull.Value, ParameterDirection.InputOutput)
		};

        private Integer userId;
        private Integer valorId;
        private Integer[] financeiroId;
        private Integer[] formaPagamentoId;
        private Integer relPesContaId;
        private string documento;
        private string emitente;
        private string dtEmissao;
        private string bancoAgencia;
        private string conta;
        private string numeroDocumento;
        protected string[] valor;
        protected string[] valorEncargos;
        protected string[] valorAplicar;
        private string[] observacao;
        private Integer incluir;

        protected Integer nUserID
        {
            get { return userId; }
            set { userId = value != null ? value : zero; }
        }

        protected Integer nValorID
        {
            get { return valorId; }
            set { valorId = value != null ? value : zero; }
        }

        protected Integer[] nFinanceiroID
        {
            get { return financeiroId; }
            set { financeiroId = value != null ? value : new Integer[0]; }
        }

        protected Integer[] nFormaPagamentoID
        {
            get { return formaPagamentoId; }
            set { formaPagamentoId = value != null ? value : new  Integer[0]; }
        }

        protected Integer nRelPesContaID
        {
            get { return relPesContaId; }
            set { relPesContaId = value != null ? value : zero; }
        }

        protected string sDocumento
        {
            get { return documento; }
            set { documento = value != null ? value : ""; }
        }

        protected string sEmitente
        {
            get { return emitente; }
            set { emitente = value != null ? value : ""; }
        }

        protected string sdtEmissao
        {
            get { return dtEmissao; }
            set { dtEmissao = value != null ? value : ""; }
        }

        protected string sBancoAgencia
        {
            get { return bancoAgencia; }
            set { bancoAgencia = value != null ? value : ""; }
        }

        protected string sConta
        {
            get { return conta; }
            set { conta = value != null ? value : ""; }
        }

        protected string sNumeroDocumento
        {
            get { return numeroDocumento; }
            set { numeroDocumento = value != null ? value : ""; }
        }

        protected string[] nValor
        {
            get { return valor; }
            set { valor = value; }
        }

        protected string[] nValorEncargos
        {
            get { return valorEncargos; }
            set { valorEncargos = value; }
        }
        
        protected string[] nValorAplicar
        {
            get { return valorAplicar; }
            set { valorAplicar = value; }
        }
        protected string[] sObservacao
        {
            set { observacao = value != null ? value : new string[0]; }
        }

        protected Integer nIncluir
        {
            set { incluir = value != null ? value : zero; }
        }

        protected void ValLocalizarSincroniza(int i)
        {
            int Valor2 = 0;
            if (i == 0)
            {
                if ((valorId.intValue() == 0) && (valorId == null))
                {
                    Valor2 = 0;
                }
                else
                    Valor2 = Convert.ToInt32(valorId);
            }
            else
                Valor2 = valorid;


            procparam[0].Data = (Valor2 != 0 ? (Object)Valor2 : DBNull.Value);
            procparam[1].Data = (financeiroId[i] != null && financeiroId[i].intValue() != 0) ? (Object)financeiroId[i].ToString() : DBNull.Value;
            procparam[2].Data = (formaPagamentoId[0] != null && formaPagamentoId[0].intValue() != 0) ? (Object)formaPagamentoId[0].ToString() : DBNull.Value;
            procparam[3].Data = (relPesContaId != null && relPesContaId.intValue() != 0) ? (Object)relPesContaId.ToString() : DBNull.Value;
            procparam[4].Data = (documento != null && documento != "") ? (Object)documento : DBNull.Value;
            procparam[5].Data = (emitente != null && emitente != "") ? (Object)emitente : DBNull.Value;
            procparam[6].Data = (dtEmissao != null && dtEmissao != "") ? (Object)dtEmissao.ToString() : DBNull.Value;
            procparam[7].Data = (bancoAgencia != null && bancoAgencia != "") ? (Object)bancoAgencia.ToString() : DBNull.Value;
            procparam[8].Data = (conta != null && conta != "") ? (Object)conta : DBNull.Value;
            procparam[9].Data = (numeroDocumento != null && numeroDocumento != "") ? (Object)numeroDocumento : DBNull.Value;
            procparam[10].Data = (valor[i] != null && valor[i].ToString() != "0.0") ? (Object)Convert.ToDouble(valor[i]) : DBNull.Value;
            procparam[11].Data = (valorEncargos[i] != null && valorEncargos[i].ToString() != "0.0") ? (Object)Convert.ToDouble(valorEncargos[i]) : DBNull.Value;
            procparam[12].Data = (valorAplicar[i] != null && valorAplicar[i].ToString() != "0.0") ? (Object)Convert.ToDouble(valorAplicar[i]) : DBNull.Value;
            procparam[13].Data = (observacao[i] != null && observacao[i] != "") ? (Object)observacao[i] : DBNull.Value;
            procparam[14].Data = (userId != null && userId.intValue() != 0) ? (Object)userId.ToString() : DBNull.Value;
            procparam[15].Data = (incluir != null) ? (Object)(incluir.intValue() == 1 ? 1 : 0) : DBNull.Value;
            procparam[16].Data = DBNull.Value;
            procparam[17].Data = DBNull.Value;

            // Roda a procedure
            DataInterfaceObj.execNonQueryProcedure(
                "sp_ValLocalizar_Sincroniza",
                procparam);
        }

        protected DataSet ProcessaFinanceiros()
        {
            string resultado = "";
            // Acerta o tamanho dos parāmetros.
            procparam[5].Length = 20; // @emitente
            procparam[6].Length = 8; // @dtEmissao
            procparam[7].Length = 9; // @bancoAgencia            
            procparam[8].Length = 19; // @Conta
            procparam[9].Length = 8; // @NumeroDocumento
            procparam[13].Length = 30; // @Observacao
            procparam[17].Length = 8000; // @Resultado

            // Chama a procedure para financeiro recebido.
            for (int i = 0; i < financeiroId.Length; i++)
            {
                // Roda a procedure.
                ValLocalizarSincroniza(i);

                // Acumula o resultado
                resultado += procparam[17].Data;
                Valor2ID = procparam[16].Data.ToString();

                if ((Valor2ID != null) && (Valor2ID != "0") && (Valor2ID != ""))
                    valorid = Convert.ToInt32(Valor2ID);
              }

            return DataInterfaceObj.getRemoteData(
                "select '" + resultado + "' as Resultado, " + valorid.ToString() + " as Valor2ID"
            );
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(ProcessaFinanceiros());
        }
    }
}
