using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class fechapedidoparcelas : System.Web.UI.OverflyPage
	{
		private Integer pedidoId; 
		private Integer tipoFechamento;

		private static Integer zero = new Integer(0);
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select " + execUpdate() + " as fldresp"
				)
			);
		}

		protected int execUpdate() 
		{
			return DataInterfaceObj.ExecuteSQLCommand(
				"UPDATE Pedidos SET TipoAtualizacaoID = " +
					tipoFechamento +
				" WHERE PedidoID = " +
					pedidoId
			);
		}
		
		public Integer nPedidoID
		{
			get { return pedidoId; }
			set { pedidoId = value != null ? value : zero; }
		}

		public Integer nTipoFechamento
		{
			get { return tipoFechamento; }
			set { tipoFechamento = value != null ? value : zero; }
		}
	}
}
