﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using OVERFLYSVRCFGLib;
using WSData;
using System.Drawing.Printing;
using System.Drawing;

namespace Overfly3.PrintJet
{
    public partial class Invoice : System.Web.UI.Page
    {
        string nNotaCorrente = HttpContext.Current.Request.Params["glb_nNotaFiscalID"];
        string nEmpresaData1 = HttpContext.Current.Request.Params["nEmpresaData1"];
        string nEmpresaData2 = HttpContext.Current.Request.Params["nEmpresaData2"];
        string DATE_SQL_PARAM = HttpContext.Current.Request.Params["DATE_SQL_PARAM"];

        string SQLHeader = string.Empty;
        string SQLDetail1 = string.Empty;
        string SQLDetail2 = string.Empty;
        string SQLDetail3 = string.Empty;
        string SQLDetail4 = string.Empty;
        string SQLExtra01 = string.Empty;
        string SQLShipTo = string.Empty;

        bool _Copy = false;

        string glb_nNotaFiscalID = HttpContext.Current.Request.Params["glb_nNotaFiscalID"];


        protected void Page_Load(object sender, EventArgs e)
        {
            ImprimeNotaFiscal("false");

        }

        private void ImprimeNotaFiscal(string Copy)
        {

            if (Copy == "false")
            {
                _Copy = false;
            }
            else
            {
                _Copy = true;
            }

            //Header - Nota Fiscal  

            SQLHeader = "SELECT " +
                        "NF.NotaFiscalID, Imagens.Arquivo AS FotoEmpresa, " +
                        "(CASE NF.TipoNotaFiscalID WHEN 601 THEN 'X' ELSE SPACE(0) END) AS Entrada, " +
                        "(CASE NF.TipoNotaFiscalID WHEN 602 THEN 'X' ELSE SPACE(0) END) AS Saida, " +
                        "STR(NF.NotaFiscal) AS NotaFiscal, CONVERT(VARCHAR, NF.dtNotaFiscal, " + DATE_SQL_PARAM + ") AS dtNotaFiscal, " +
                        "ISNULL(CONVERT(VARCHAR, NF.dtEntradaSaida, " + DATE_SQL_PARAM + "), SPACE(0)) AS dtEntradaSaida, " +
                        "ISNULL(CONVERT(VARCHAR(2), dbo.fn_Pad(DATEPART(hh, NF.dtEntradaSaida), 2, CHAR(48), CHAR(76))) + CHAR(58) + CONVERT(VARCHAR(2), dbo.fn_Pad(DATEPART(mi, NF.dtEntradaSaida), 2, CHAR(48), CHAR(76))), SPACE(0)) AS HoraEntradaSaida, " +
                        "NF.CFOPs, NF.Natureza, NF.TransacaoID AS TransacaoID, STR(NF.PedidoID) AS PedidoID, STR(NF.Formulario) AS Formulario, NFDest.Nome, " +
                        "'ID:' + SPACE(1) + CONVERT(VARCHAR(10), NFDest.PessoaID) + SPACE(1) + (CASE NFDest.PaisID WHEN 130 THEN 'CNPJ:' ELSE 'Doc:' END) + SPACE(1) + ISNULL(NFDest.DocumentoFederal, SPACE(0)) AS CampoEnd2 , ISNULL(STR(NFTransp.PessoaID), SPACE(0)) AS TransportadoraID, " +
                        "ISNULL(NFDest.DocumentoMunicipal, SPACE(0)) AS DocumentoMunicipal, STR(NFDest.PessoaID) AS PessoaID, ISNULL(NFDest.DocumentoFederal, SPACE(0)) AS Document1," +
                        "(CASE ISNULL(PessoaEnderecoPais.EnderecoInvertido, 0) WHEN 1 " +
                        "THEN (ISNULL(NFDest.Numero, SPACE(0)) + \' \' + ISNULL(NFDest.Endereco, SPACE(0)) +  \' \' ) " + //+ ISNULL(NFDest.Complemento, SPACE(0))
                        "ELSE (ISNULL(NFDest.Endereco, SPACE(0)) +  \' \' + ISNULL(NFDest.Numero, SPACE(0)) + \' \' ) END) +  \' \' +  ISNULL(NFDest.Bairro, SPACE(0)) AS Endereco,  ISNULL(NFDest.Complemento, SPACE(0)) as Complemento, " +
                        "ISNULL(NFDest.Bairro, SPACE(0)) AS Bairro, NFDest.Cidade + SPACE(1) + NFDest.UF + SPACE(1) + NFDest.CEP  + SPACE(1) + NFDest.Pais as CampoEnd1, NFDest.Pais, UPPER(LEFT(NFDest.Pais, 2)) AS PaisAbreviado, " +
                        "(ISNULL(NFDest.DDI, SPACE(0)) + ISNULL(NFDest.DDD, SPACE(0)) + ' ' + ISNULL(NFDest.Telefone, SPACE(0))) AS Telefone, " +
                        "ISNULL(NFDest.DocumentoEstadual, SPACE(0)) AS Documento2, " +
                        "NFISS.ValorImposto AS ValorTotalImposto3, NFISS.BaseCalculo AS BaseCalculoImposto3, LEFT(ROUND(((NFISS.ValorImposto/NFISS.BaseCalculo)*100),2),4) as AliquotaISS, NF.ValorTotalServicos, NFICMS.BaseCalculo AS BaseCalculoImposto1, NFICMS.ValorImposto AS ValorTotalImposto1, " +
                        "NFICMSST.BaseCalculo AS BaseCalculoImposto1Subst, NFICMSST.ValorImposto AS ValorTotalImposto1Subst, NF.ValorTotalProdutos, ISNULL(NF.ValorFrete, 0) AS ValorFrete, " +
                        "ISNULL(NF.ValorSeguro, 0) AS ValorSeguro, ISNULL(NF.OutrasDespesas, 0) AS OutrasDespesas, " +
                        "(CASE NF.EmpresaID WHEN 7 THEN ISNULL(NFTAX.ValorImposto, 0) ELSE ISNULL(NFIPI.ValorImposto, 0) END) AS ValorTotalImposto2, NF.ValorTotalNota, " +
                        "ISNULL(NFTransp.Nome,SPACE(0)) AS Transportadora, ISNULL(NF.Frete, SPACE(0)) AS Frete, " +
                        "(CASE ISNULL(PessoaEnderecoPais2.EnderecoInvertido, 0) WHEN 1 " +
                            "THEN (ISNULL(NFTransp.Numero, SPACE(0)) + ' ' + ISNULL(NFTransp.Endereco, SPACE(0)) +  ' ' + ISNULL(NFTransp.Complemento, SPACE(0))) " +
                            "ELSE (ISNULL(NFTransp.Endereco, SPACE(0)) +  ' ' + ISNULL(NFTransp.Numero, SPACE(0)) + ' ' + ISNULL(NFTransp.Complemento, SPACE(0))) END)  AS TranspEndereco, " +
                        "ISNULL(NFTransp.Cidade, SPACE(0)) AS TranspCidade, " +
                        "ISNULL(NFTransp.Bairro,SPACE(0)) AS TranspBairro, ISNULL(NFTransp.CEP,SPACE(0)) AS TranspCEP, " +
                        "ISNULL(NFTransp.UF,SPACE(0)) AS TranspUF, ISNULL(NFTransp.Pais,SPACE(0)) AS TranspPais, " +
                        "(ISNULL(NFTransp.DDI, SPACE(0)) + ISNULL(NFTransp.DDD, SPACE(0)) + ' ' + ISNULL(NFTransp.Telefone, SPACE(0))) AS TranspTelefone, " +
                        "ISNULL(NFTransp.DocumentoFederal,SPACE(0)) AS TranspDocumento1, ISNULL(NFTransp.DocumentoEstadual,SPACE(0)) AS TranspDocumento2, " +
                        "ISNULL(NF.PlacaVeiculo, SPACE(0)) AS PlacaVeiculo, ISNULL(NF.UFVeiculo, SPACE(0)) AS UFVeiculo, " +
                        "STR(ISNULL(NF.Quantidade, 0)) AS Quantidade, " +
                        "NF.Especie, ISNULL(dbo.fn_Tradutor(NF.MeioTransporte, " + nEmpresaData1 + ", " + nEmpresaData2 + ", NULL),SPACE(0)) AS MeioTransporte, " +
                        "NF.PesoBruto, NF.PesoLiquido, NF.Vendedor, " +
                        "ISNULL(NF.SeuPedido, SPACE(1)) AS SeuPedido, " +
                        "(SELECT ISNULL(CONVERT(VARCHAR(5), CONVERT(NUMERIC(3),PrazoMedioPagamento)) + ' Days', SPACE(0)) FROM Pedidos WITH(NOLOCK) WHERE Pedidos.PedidoID=NF.PedidoID) AS PMP, " +
                        "(SELECT (CASE SUBSTRING(Observacao,1,1) WHEN '/' THEN SUBSTRING(Observacao,2,39) ELSE 'Miami' END) " +
                        "FROM Pedidos WITH(NOLOCK) WHERE Pedidos.PedidoID=NF.PedidoID) AS CidadeEmpresa, " +
                        "ISNULL((SELECT dbo.fn_Tradutor(bb.ItemAbreviado, " + nEmpresaData1 + ", " + nEmpresaData2 + ", NULL) FROM " +
                            "Pedidos aa, TiposAuxiliares_Itens bb " +
                            "WHERE (aa.NotaFiscalID = NF.NotaFiscalID AND ISNULL(aa.ModalidadeTransporteID, 621) = bb.ItemID)), SPACE(0)) AS Incoterm, " +
                            "(SELECT TOP 1 ISNULL(LTRIM(RTRIM(Observacao)), SPACE(0)) FROM Pedidos WITH(NOLOCK) WHERE Pedidos.PedidoID=NF.PedidoID) AS ObservacoesInvoice " +
                        "FROM NotasFiscais NF WITH(NOLOCK) " +
                        "LEFT OUTER JOIN [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] Imagens WITH(NOLOCK) ON (NF.EmpresaID = Imagens.RegistroID AND Imagens.FormID=1210 AND Imagens.SubFormID=20100 AND Imagens.TipoArquivoID = 1451) " +
                        "INNER JOIN NotasFiscais_Pessoas NFDest WITH(NOLOCK) ON (NFDest.NotaFiscalID = NF.NotaFiscalID) AND (NFDest.TipoID = 791) " + //Destinatario
                        "LEFT OUTER JOIN NotasFiscais_Pessoas NFTransp WITH(NOLOCK) ON (NFTransp.NotaFiscalID = NF.NotaFiscalID) AND (NFTransp.TipoID = 793) " +
                        "LEFT OUTER JOIN NotasFiscais_Impostos NFIPI WITH(NOLOCK) ON (NFIPI.NotaFiscalID = NF.NotaFiscalID) AND (NFIPI.ImpostoID = 8) " +
                        "LEFT OUTER JOIN NotasFiscais_Impostos NFICMS WITH(NOLOCK) ON (NFICMS.NotaFiscalID = NF.NotaFiscalID) AND (NFICMS.ImpostoID = 9) " +
                        "LEFT OUTER JOIN NotasFiscais_Impostos NFISS WITH(NOLOCK) ON (NFISS.NotaFiscalID = NF.NotaFiscalID) AND (NFISS.ImpostoID = 10) " +
                        "LEFT OUTER JOIN NotasFiscais_Impostos NFICMSST WITH(NOLOCK) ON (NFICMSST.NotaFiscalID = NF.NotaFiscalID) AND (NFICMSST.ImpostoID = 25) " +
                        "LEFT OUTER JOIN NotasFiscais_Impostos NFTAX WITH(NOLOCK) ON (NFTAX.NotaFiscalID = NF.NotaFiscalID) AND (NFTAX.ImpostoID = 22) " +
                        "LEFT OUTER JOIN Localidades PessoaEnderecoPais WITH(NOLOCK) ON (NFDest.PaisID = PessoaEnderecoPais.LocalidadeID) " +
                        "LEFT OUTER JOIN Localidades PessoaEnderecoPais2 WITH(NOLOCK) ON (NFTransp.PaisID = PessoaEnderecoPais2.LocalidadeID) " +
                        "WHERE NF.NotaFiscalID = " + nNotaCorrente;

            // Detalhe 1 - Fatura

            SQLDetail1 = "SELECT a.NotaFiscalID, a.Duplicata, " +
                                "CONVERT(VARCHAR, a.Vencimento, " + DATE_SQL_PARAM + ") AS Vencimento, " +
                                "a.Valor, " +
                                "CONVERT(VARCHAR(3), DATEDIFF(dd, b.dtNotaFiscal, a.Vencimento)) AS Prazo " +
                                "FROM NotasFiscais_Duplicatas a WITH(NOLOCK), NotasFiscais b WITH(NOLOCK) " +
                                "WHERE (a.NotaFiscalID = " + nNotaCorrente + " AND a.NotaFiscalID = b.NotaFiscalID AND b.EstadoID=67) " +
                                "ORDER BY a.Vencimento ";

            // Detalhe 2 - Produto
            SQLDetail2 = "SELECT a.NotaFiscalID, STR(a.ProdutoID) AS ItemID, " +
                                " (a.Familia  + CHAR(13) +  CHAR(10)+ ISNULL(LEFT(a.Descricao,28), SPACE(1)))AS Produto, " +
                                "a.Marca, " +
                                "(a.Modelo + CHAR(13) +  CHAR(10)+ ISNULL(a.InformacoesAdicionais, ISNULL(dbo.fn_NotaFiscalItem_Mensagem(a.PedItemID), SPACE(0)))) AS Modelo, " +
                                "(ISNULL(dbo.fn_Produto_Descricao2(a.NFItemID, 245, 31) + CHAR(13) +  CHAR(10)+ ISNULL(a.InformacoesAdicionais, ISNULL(dbo.fn_NotaFiscalItem_Mensagem(a.PedItemID), SPACE(0)))  , SPACE(1))) AS DescricaoItem, " +
                                "ISNULL(a.CFOP, SPACE(1)) AS CFOP, " +
                                "ISNULL(LEFT(a.Descricao,20), SPACE(1)) AS DescricaoReduzida, " +
                                "ISNULL(a.InformacoesAdicionais, ISNULL(dbo.fn_NotaFiscalItem_Mensagem(a.PedItemID), SPACE(0))) AS PaisOrigem, " +
                                "(CASE WHEN (dbo.fn_Produto_PesosMedidas(a.ProdutoID, 0, 1)) > 0 THEN 'Net(Kg): ' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),a.Quantidade * dbo.fn_Produto_PesosMedidas(a.ProdutoID, 0, 1))) ELSE SPACE(0) END) AS PesoLiquido, " +
                                "(CASE WHEN (dbo.fn_Produto_PesosMedidas(a.ProdutoID, 1, 1)) > 0 THEN 'Gross(Kg): ' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),a.Quantidade * dbo.fn_Produto_PesosMedidas(a.ProdutoID, 1, 1))) ELSE SPACE(0) END) AS PesoBruto, " +
                                "ISNULL(a.NCM, SPACE(1)) AS ClassificacaoFiscal, (ISNULL(a.Origem, 0) + ISNULL(d.CodigoTributacao, 0)) AS SituacaoTributaria, a.Unidade, STR(a.Quantidade) AS Quantidade, " +
                                "(CONVERT (VARCHAR(MAX), dbo.fn_Numero_Formata(a.ValorUnitario, 2, 1, 101)) + CHAR(13) +  CHAR(10) + (CASE WHEN (dbo.fn_Produto_PesosMedidas(a.ProdutoID, 0, 1)) > 0 THEN 'Net(Kg): ' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),a.Quantidade * dbo.fn_Produto_PesosMedidas(a.ProdutoID, 0, 1))) ELSE SPACE(0) END)) AS ValorUnitario, " +
                                "(CONVERT(VARCHAR(MAX),dbo.fn_Numero_Formata(a.ValorTotal,2,1,101)) + CHAR(13) +  CHAR(10)+ (CASE WHEN (dbo.fn_Produto_PesosMedidas(a.ProdutoID, 1, 1)) > 0 THEN 'Gross(Kg): ' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),a.Quantidade * dbo.fn_Produto_PesosMedidas(a.ProdutoID, 1, 1))) ELSE SPACE(0) END)) AS ValorTotal, " +
                                 "STR(d.Aliquota) AS AliquotaImposto1, STR(e.Aliquota) AS AliquotaImposto2, " +
                                "d.Aliquota AS AliquotaImposto1Modelo2, " +
                                "e.Aliquota AS AliquotaImposto2Modelo2, e.ValorImposto AS ValorImposto2, CONVERT(BIT, ISNULL(c.EhMicro, 0)) AS EhMicro, " +
                                "ISNULL(a.InformacoesAdicionais, ISNULL(dbo.fn_NotaFiscalItem_Mensagem(a.PedItemID), SPACE(0))) AS MensagemItem, " +
                                "f.PartNumber AS PartNumber " +
                                "FROM NotasFiscais_Itens a WITH(NOLOCK) " +
                                    "INNER JOIN Conceitos b WITH(NOLOCK) ON a.ProdutoID = b.ConceitoID " +
                                    "INNER JOIN Conceitos c WITH(NOLOCK) ON b.ProdutoID = c.ConceitoID " +
                                    "LEFT  JOIN Conceitos f WITH(NOLOCK) ON a.ProdutoID = f.ConceitoID " +
                                    "LEFT OUTER JOIN NotasFiscais_Itens_Impostos d WITH(NOLOCK) ON d.NFItemID = a.NFItemID AND d.ImpostoID = 9" + //ICMS
                                    "LEFT OUTER JOIN NotasFiscais_Itens_Impostos e WITH(NOLOCK) ON e.NFItemID = a.NFItemID AND e.ImpostoID = 8 " + //IPI
                                "WHERE a.Tipo = 'P'" + " AND a.NotaFiscalID = " + nNotaCorrente;

            // Detalhe 3 - Servico
            SQLDetail3 = "SELECT Itens.NotaFiscalID, STR(Itens.ProdutoID) AS ItemID, Itens.Familia AS Produto, Itens.Marca, Itens.Modelo, " +
                                "(CASE WHEN NF.EmpresaID = 4 THEN ISNULL(LEFT(Itens.Descricao,35), SPACE(1)) ELSE ISNULL(LEFT(Itens.Descricao,28), SPACE(1)) END) AS Descricao, " +
                                "ISNULL(Itens.CFOP, SPACE(1)) AS CFOP, ISNULL(Itens.CodigoListaServico, SPACE(1)) AS CodigoListaServico, " +
                                "Itens.Unidade, STR(Itens.Quantidade) AS Quantidade, " +
                                "ISNULL(Itens.InformacoesAdicionais, ISNULL(dbo.fn_NotaFiscalItem_Mensagem(Itens.PedItemID), SPACE(0))) AS PaisOrigem, " +
                                "(CASE WHEN (dbo.fn_Produto_PesosMedidas(Itens.ProdutoID, 0, 1)) > 0 THEN 'Net(Kg): ' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),Itens.Quantidade * dbo.fn_Produto_PesosMedidas(Itens.ProdutoID, 0, 1))) ELSE SPACE(0) END) AS PesoLiquido, " +
                                "(CASE WHEN (dbo.fn_Produto_PesosMedidas(Itens.ProdutoID, 1, 1)) > 0 THEN 'Gross(Kg): ' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),Itens.Quantidade * dbo.fn_Produto_PesosMedidas(Itens.ProdutoID, 1, 1))) ELSE SPACE(0) END) AS PesoBruto, " +
                                "Itens.ValorUnitario, Itens.ValorTotal AS ValorTotal " +
                                "FROM NotasFiscais_Itens Itens WITH(NOLOCK) " +
                                "INNER JOIN NotasFiscais NF WITH(NOLOCK) ON (NF.NotaFiscalID = Itens.NotaFiscalID AND NF.EstadoID=67) " +
                                "WHERE Tipo = 'S'" + " AND Itens.NotaFiscalID = " + nNotaCorrente;

            // Detalhe 4 - Mensagem

            SQLDetail4 = "SELECT dbo.fn_NotaFiscal_Mensagem ( " + nNotaCorrente + " ) as Mensagem ";
            printInvoice();

        }

        private void printInvoice()
        {

            SQLExtra01 = "SELECT UPPER(Empresa.Nome) AS Nome, " +
                            "ISNULL(Empresa.Numero + SPACE(1), SPACE(0)) + ISNULL(Empresa.Endereco + SPACE(1), SPACE(0)) + " +
                            "ISNULL(Empresa.Complemento + SPACE(1), SPACE(0)) + ISNULL(Empresa.Bairro + SPACE(1), SPACE(0)) AS End1, " +
                            "ISNULL(Empresa.Cidade + SPACE(1), SPACE(0)) + ISNULL(Empresa.UF + SPACE(1), SPACE(0)) + ISNULL(SUBSTRING(Empresa.CEP,0,6) + '-' + SUBSTRING(Empresa.CEP,6,9) + SPACE(1), SPACE(0)) +  ISNULL(Empresa.Pais + SPACE(1), SPACE(0)) AS End2, " +
                            "ISNULL(Empresa.CEP, SPACE(0)) AS CEP, " +
                            "dbo.fn_Pessoa_Telefone(Empresa.PessoaID, 119, 119, 6, 0, NULL) AS Telefone, " +
                            "dbo.fn_Pessoa_Telefone(Empresa.PessoaID, 122, 122, 6, 0, NULL) AS nFax, " +
                            "SUBSTRING(dbo.fn_Pessoa_URL(Empresa.PessoaID, 125, NULL), 8, 72) AS URL, " +
                            "dbo.fn_Pessoa_URL(Empresa.PessoaID, 124, NULL) AS Email " +
                          "FROM NotasFiscais_Pessoas Empresa WITH(NOLOCK) " +
                          "WHERE (Empresa.TipoID = 790 AND Empresa.NotaFiscalID = " + glb_nNotaFiscalID + ") ";


            printInvoice_ShipTo();
        }

        private void printInvoice_ShipTo()
        {
            /*SQLShipTo = "SELECT NFPessoa.PessoaID, NFPessoa.Nome, NFPessoa.DocumentoFederal AS Documento1, NFPessoa.DocumentoEstadual AS Documento2, NFPessoa.Pais, NFPessoa.CEP, NFPessoa.UF, NFPessoa.Cidade, " +
                       "NFPessoa.Bairro, " +
                       "(CASE ISNULL(Localidades.EnderecoInvertido, 0) WHEN 1 " +
                           "THEN (ISNULL(NFPessoa.Numero, SPACE(0)) + ' ' + ISNULL(NFPessoa.Endereco, SPACE(0)) +  ' ' + ISNULL(NFPessoa.Complemento, SPACE(0))) " +
                           "ELSE (ISNULL(NFPessoa.Endereco, SPACE(0)) +  ' ' + ISNULL(NFPessoa.Numero, SPACE(0)) + ' ' + ISNULL(NFPessoa.Complemento, SPACE(0))) END)  AS Endereco, " +
                       "(NFPessoa.DDI + NFPessoa.DDD + ' ' + NFPessoa.Telefone) AS Telefone " +
                       "FROM NotasFiscais_Pessoas NFPessoa WITH(NOLOCK) " +
                           "INNER JOIN Localidades Localidades WITH(NOLOCK) ON (NFPessoa.PaisID = Localidades.LocalidadeID) " +
                       "WHERE NFPessoa.NotaFiscalID =" + glb_nNotaFiscalID + " AND NFPessoa.TipoID = 792";*/




            SQLShipTo = " SELECT STR(NFPessoa.PessoaID) AS PessoaID, NFPessoa.Nome, ISNULL(NFPessoa.DocumentoFederal, SPACE(0)) AS Document1, " +
                " (CASE ISNULL(Localidades.EnderecoInvertido, 0) WHEN 1 THEN (ISNULL(NFPessoa.Numero, SPACE(0)) + ' ' + ISNULL(NFPessoa.Endereco, SPACE(0)) +  ' ' ) ELSE (ISNULL(NFPessoa.Endereco, SPACE(0)) +  ' ' + ISNULL(NFPessoa.Numero, SPACE(0)) + ' ' ) END) +  \' \' +  ISNULL(NFPessoa.Bairro, SPACE(0)) AS Endereco, " +
                " ISNULL(NFPessoa.Bairro, SPACE(0)) AS Bairro, NFPessoa.Cidade + SPACE(1) + NFPessoa.UF + SPACE(1) + NFPessoa.CEP + SPACE(1) + NFPessoa.Pais as CampoEnd1, NFPessoa.Pais, UPPER(LEFT(NFPessoa.Pais, 2)) AS PaisAbreviado ,  ISNULL(NFPessoa.Complemento, SPACE(0)) as Complemento, " +
                " (ISNULL(NFPessoa.DDI, SPACE(0)) + ISNULL(NFPessoa.DDD, SPACE(0)) + ' ' + ISNULL(NFPessoa.Telefone, SPACE(0))) AS Telefone, ISNULL(NFPessoa.DocumentoEstadual, SPACE(0)) AS Documento2, " +
                  "(NFPessoa.DDI + NFPessoa.DDD + ' ' + NFPessoa.Telefone) AS Telefone, 'ID:' + SPACE(1) + CONVERT(VARCHAR(10), NFPessoa.PessoaID) + SPACE(1) + (CASE NFPessoa.PaisID WHEN 130 THEN 'CNPJ:' ELSE 'Doc:' END) + SPACE(1) + ISNULL(NFPessoa.DocumentoFederal, SPACE(0)) AS CampoEnd2  " +
                       "FROM NotasFiscais_Pessoas NFPessoa WITH(NOLOCK) " +
                           "INNER JOIN Localidades Localidades WITH(NOLOCK) ON (NFPessoa.PaisID = Localidades.LocalidadeID) " +
                       "WHERE NFPessoa.NotaFiscalID =" + glb_nNotaFiscalID + " AND NFPessoa.TipoID = 792";




            printInvoice_DSC();
        }

        private void printInvoice_DSC()
        {
            int nTransacaoID;
            bool bCustomsWarehouse = false;
            string term_Acquire_SoldTo = "Sold to:";
            string mensagem;
            string MensagemInvoice;
            string mensagem2;
            string Title;
            string sCEP;
            string sMensagemNota;
            string sTelefone;
            string sFax;

            // Variáveis de posicionamento

            /*
                        double Bloco1_Left = 0.02;
                        double Bloco1_Top = 0.313888;
                        double Bloco1_TopC = 5.219788;

                        double Bloco2_Left = 1.23825;
                        double Bloco2_Top = 0.313888;
                        double Bloco2_TopC = 5.219788;

                        double Bloco3_Left = 0.02;
                        double Bloco3_Top = 0.578;
                        double Bloco3_TopC = 5.460087;

                        double Bloco4_Left = 1.04775;
                        double Bloco4_Top = 0.578;
                        double Bloco4_TopC = 5.460087;

                        double Bloco5_Left = 0.02;
                        double Bloco5_Top = 0.911049;
                        double Bloco5_TopC = 5.781116;

                        double Bloco6_Left = 0.02;
                        double Bloco6_Top = 1.019583;
                        double Bloco6_TopC = 5.88342;

                        double Bloco7_Left = 0.02;
                        double Bloco7_Top = 1.965299;
                        double Bloco7_TopC = 6.828864;

                        double Bloco8_Left = 1.660695;
                        double Bloco8_Top = 1.965299;
                        double Bloco8_Top2 = 2.010278;
                        double Bloco8_TopC = 6.828864;
                        double Bloco8_Top2C = 6.872081;

                        double Bloco9_Left = 0.02;
                        double Bloco9_Top = 2.404063;
                        double Bloco9_TopC = 7.273816;

                        double TermCon_Letf = 0.0;
                        double TermCon_Top = 2.850;
                        double TermCon_TopC = 7.750;
                        double TermCon_Espacamento = 0.03175;
                        double TamFont = 7;

                        double TamLinha = 19.8;
            */

            double Bloco1_Left = 0.2;
            double Bloco1_Top = 3.13888;
            double Bloco1_TopC = 52.19788;

            double Bloco2_Left = 12.3825;
            double Bloco2_Top = 03.13888;
            double Bloco2_TopC = 52.19788;

            double Bloco3_Left = 0.2;
            double Bloco3_Top = 05.78;
            double Bloco3_TopC = 54.60087;

            double Bloco4_Left = 10.4775;
            double Bloco4_Top = 05.78;
            double Bloco4_TopC = 54.60087;

            double Bloco5_Left = 0.2;
            double Bloco5_Top = 9.11049;
            double Bloco5_TopC = 57.81116;

            double Bloco6_Left = 0.2;
            double Bloco6_Top = 10.19583;
            double Bloco6_TopC = 58.8342;

            double Bloco7_Left = 0.2;
            double Bloco7_Top = 19.65299;
            double Bloco7_TopC = 68.28864;

            double Bloco8_Left = 16.60695;
            double Bloco8_Top = 19.65299;
            double Bloco8_Top2 = 20.10278;
            double Bloco8_TopC = 68.28864;
            double Bloco8_Top2C = 68.72081;

            double Bloco9_Left = 0.2;
            double Bloco9_Top = 24.04063;
            double Bloco9_TopC = 72.73816;

            double TermCon_Letf = 0.0;
            double TermCon_Top = 28.50;
            double TermCon_TopC = 77.50;
            double TermCon_Espacamento = 0.3175;
            double TamFont = 7;

            double TamLinha = 19.8;
            ClsReport Relatorio = new ClsReport();

            DataSet Ds = new DataSet();

            string[,] arrQuerys;

            arrQuerys = new string[,] {
                                        {"sqlHeader", SQLHeader},
                                        {"sqlDetail1", SQLDetail1},
                                        {"sqlDetail2", SQLDetail2},
                                        {"sqlDetail3", SQLDetail3},
                                        {"sqlDetail4", SQLDetail4},
                                        {"sqlExtra01", SQLExtra01},
                                        {"sqlShipTo", SQLShipTo}
                                      };

            // var paramLeft1 = "0.02";
            var paramLeft1 = "0.2";

            Relatorio.CriarRelatório("Invoice", arrQuerys, "USA", "Portrait");

            // Relatorio.CriarObjImage("Query", "sqlHeader", "FotoEmpresa", paramLeft1, "0.160841", "Body", "5.09104", "1.74215");
            Relatorio.CriarObjImage("Query", "sqlHeader", "FotoEmpresa", paramLeft1, "1.60841", "Body", "5.09104", "1.74215");

            Ds.Tables.Add(RequestDatatable(SQLHeader, "sqlHeader").Copy());

            nTransacaoID = (Int32)Ds.Tables["sqlHeader"].Rows[0]["TransacaoID"];

            // Proforma Invoice (Customs Warehouse)
            if ((nTransacaoID == 461) || (nTransacaoID == 462))
                bCustomsWarehouse = true;

            if (bCustomsWarehouse)
                // Relatorio.CriarObjLabel("Fixo", "", "PROFORMA INVOICE", "1.176347", "1.156576", "16", "black", "B", "Body", "8.22792", "");
                Relatorio.CriarObjLabel("Fixo", "", "PROFORMA INVOICE", "11.76347", "11.56576", "16", "black", "B", "Body", "8.22792", "");
            else if (nTransacaoID == 112)
                // Relatorio.CriarObjLabel("Fixo", "", "CREDIT MEMO", "1.176347", "0.226576", "16", "black", "B", "Body", "8.22792", "Right");
                Relatorio.CriarObjLabel("Fixo", "", "CREDIT MEMO", "11.76347", "2.26576", "16", "black", "B", "Body", "8.22792", "Right");
            else
                // Relatorio.CriarObjLabel("Fixo", "", "COMMERCIAL INVOICE", "1.176347", "0.226576", "16", "black", "B", "Body", "8.22792", "Right");
                Relatorio.CriarObjLabel("Fixo", "", "COMMERCIAL INVOICE", "11.76347", "2.26576", "16", "black", "B", "Body", "8.22792", "Right");

            // Titulo 1
            Ds.Tables.Add(RequestDatatable(SQLExtra01, "sqlExtra01").Copy());

            sCEP = Ds.Tables["sqlExtra01"].Rows[0]["CEP"].ToString().Trim();

            sCEP = sCEP.Substring(0, 5) + "-" + sCEP.Substring(5);

            sTelefone = Ds.Tables["sqlExtra01"].Rows[0]["Telefone"].ToString().Trim();

            if (Ds.Tables["sqlExtra01"].Rows[0]["nFax"] != null || Ds.Tables["sqlExtra01"].Rows[0]["nFax"].ToString() != "")
            {
                sFax = Ds.Tables["sqlExtra01"].Rows[0]["nFax"].ToString().Trim();

                sFax = "Fax " + sFax;
            }
            else
            {
                sFax = "";
            }

            sTelefone = "Phone " + sTelefone;

            //Relatorio.CriarObjLinha(paramLeft1, "0.303305", TamLinha.ToString(), "black", "Body");
            Relatorio.CriarObjLinha(paramLeft1, "3.03305", TamLinha.ToString(), "black", "Body");

            Relatorio.CriarObjLabel("Query", "sqlExtra01", "Nome", Bloco1_Left.ToString(), Bloco1_Top.ToString(), "12", "black", "B", "Body", "8.83646", "");
            //Relatorio.CriarObjLabel("Query", "sqlExtra01", "End1", Bloco1_Left.ToString(), (Bloco1_Top += 0.055687).ToString(), "8", "black", "B", "Body", "8.83646", "");
            Relatorio.CriarObjLabel("Query", "sqlExtra01", "End1", Bloco1_Left.ToString(), (Bloco1_Top += 0.55687).ToString(), "8", "black", "B", "Body", "8.83646", "");
            //Relatorio.CriarObjLabel("Query", "sqlExtra01", "End2", Bloco1_Left.ToString(), (Bloco1_Top += 0.039687).ToString(), "8", "black", "B", "Body", "8.83646", "");
            Relatorio.CriarObjLabel("Query", "sqlExtra01", "End2", Bloco1_Left.ToString(), (Bloco1_Top += 0.39687).ToString(), "8", "black", "B", "Body", "8.83646", "");
            // Relatorio.CriarObjLabel("Fixo", "", sTelefone + "     " + sFax, Bloco1_Left.ToString(), (Bloco1_Top += 0.039687).ToString(), "8", "black", "B", "Body", "8.83646", "");
            Relatorio.CriarObjLabel("Fixo", "", sTelefone + "     " + sFax, Bloco1_Left.ToString(), (Bloco1_Top += 0.39687).ToString(), "8", "black", "B", "Body", "8.83646", "");
            //Relatorio.CriarObjLabel("Query", "sqlExtra01", "URL", Bloco1_Left.ToString(), (Bloco1_Top += 0.039687).ToString(), "8", "black", "B", "Body", "4.10042", "");
            Relatorio.CriarObjLabel("Query", "sqlExtra01", "URL", Bloco1_Left.ToString(), (Bloco1_Top += 0.39687).ToString(), "8", "black", "B", "Body", "4.10042", "");
            // Relatorio.CriarObjLabel("Query", "sqlExtra01", "Email", (Bloco1_Left + 0.325535).ToString(), (Bloco1_Top).ToString(), "8", "black", "B", "Body", "4.73604", "");
            Relatorio.CriarObjLabel("Query", "sqlExtra01", "Email", (Bloco1_Left + 3.25535).ToString(), (Bloco1_Top).ToString(), "8", "black", "B", "Body", "4.73604", "");

            if (bCustomsWarehouse)
                Relatorio.CriarObjLabel("Fixo", "", "Proforma Invoice:", Bloco2_Left.ToString(), Bloco2_Top.ToString(), "12", "black", "B", "Body", "3.58889", "");
            else
                Relatorio.CriarObjLabel("Fixo", "", "Invoice:", Bloco2_Left.ToString(), Bloco2_Top.ToString(), "12", "black", "B", "Body", "3.58889", "");

            // Relatorio.CriarObjLabel("Query", "sqlHeader", "NotaFiscal", (Bloco2_Left + 0.365945).ToString(), Bloco2_Top.ToString(), "12", "black", "B", "Body", "3.06854", "Right");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "NotaFiscal", (Bloco2_Left + 3.65945).ToString(), Bloco2_Top.ToString(), "12", "black", "B", "Body", "3.06854", "Right");
            // Relatorio.CriarObjLabel("Fixo", "", "Date:", Bloco2_Left.ToString(), (Bloco2_Top += 0.051979).ToString(), "10", "black", "", "Body", "3.58889", "");
            Relatorio.CriarObjLabel("Fixo", "", "Date:", Bloco2_Left.ToString(), (Bloco2_Top += 0.51979).ToString(), "10", "black", "", "Body", "3.58889", "");
            // Relatorio.CriarObjLabelData("Query", "sqlHeader", "dtNotaFiscal", (Bloco2_Left + 0.365945).ToString(), Bloco2_Top.ToString(), "10", "black", "", "Body", "3.06854", "", "Right");
            Relatorio.CriarObjLabelData("Query", "sqlHeader", "dtNotaFiscal", (Bloco2_Left + 3.65945).ToString(), Bloco2_Top.ToString(), "10", "black", "", "Body", "3.06854", "", "Right");
            //Relatorio.CriarObjLabel("Fixo", "", "Order:", Bloco2_Left.ToString(), (Bloco2_Top += 0.044979).ToString(), "10", "black", "", "Body", "3.58889", "");
            Relatorio.CriarObjLabel("Fixo", "", "Order:", Bloco2_Left.ToString(), (Bloco2_Top += 0.44979).ToString(), "10", "black", "", "Body", "3.58889", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "PedidoID", (Bloco2_Left + 0.365945).ToString(), Bloco2_Top.ToString(), "10", "black", "", "Body", "3.06854", "Right");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "PedidoID", (Bloco2_Left + 3.65945).ToString(), Bloco2_Top.ToString(), "10", "black", "", "Body", "3.06854", "Right");
            //Relatorio.CriarObjLabel("Fixo", "", "Salesperson:", Bloco2_Left.ToString(), (Bloco2_Top += 0.044979).ToString(), "10", "black", "", "Body", "2.34409", "");
            Relatorio.CriarObjLabel("Fixo", "", "Salesperson:", Bloco2_Left.ToString(), (Bloco2_Top += 0.44979).ToString(), "10", "black", "", "Body", "2.34409", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "Vendedor", (Bloco2_Left + 0.213).ToString(), Bloco2_Top.ToString(), "10", "black", "", "Body", "4.59557", "Right");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "Vendedor", (Bloco2_Left + 2.13).ToString(), Bloco2_Top.ToString(), "10", "black", "", "Body", "4.59557", "Right");
            //Relatorio.CriarObjLabel("Fixo", "", "Transaction:", Bloco2_Left.ToString(), (Bloco2_Top += 0.044979).ToString(), "10", "black", "", "Body", "3.58889", "");
            Relatorio.CriarObjLabel("Fixo", "", "Transaction:", Bloco2_Left.ToString(), (Bloco2_Top += 0.44979).ToString(), "10", "black", "", "Body", "3.58889", "");

            if (bCustomsWarehouse)
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "Natureza", (Bloco2_Left + 0.365945).ToString(), Bloco2_Top.ToString(), "10", "black", "", "Body", "3.06854", "Right");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Natureza", (Bloco2_Left + 3.65945).ToString(), Bloco2_Top.ToString(), "10", "black", "", "Body", "3.06854", "Right");
            else
                // Relatorio.CriarObjLabel("Query", "sqlHeader", "Natureza", (Bloco2_Left + 0.365945).ToString(), Bloco2_Top.ToString(), "10", "black", "", "Body", "3.06854", "Right");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Natureza", (Bloco2_Left + 3.65945).ToString(), Bloco2_Top.ToString(), "10", "black", "", "Body", "3.06854", "Right");

            // Relatorio.CriarObjLinha(paramLeft1, "0.5744", TamLinha.ToString(), "black", "Body");
            Relatorio.CriarObjLinha(paramLeft1, "5.744", TamLinha.ToString(), "black", "Body");

            if (bCustomsWarehouse)
            {
                term_Acquire_SoldTo = "Mecadoria, sem cobertura cambial, destinada ao EADI:";
            }
            else
            {
                term_Acquire_SoldTo = "Sold to:";

                if (Ds.Tables["sqlHeader"].Rows[0]["PessoaID"].ToString() == "2" ||
                    Ds.Tables["sqlHeader"].Rows[0]["PessoaID"].ToString() == "4" ||
                    Ds.Tables["sqlHeader"].Rows[0]["PessoaID"].ToString() == "10" ||
                    Ds.Tables["sqlHeader"].Rows[0]["PessoaID"].ToString() == "27682" ||
                    Ds.Tables["sqlHeader"].Rows[0]["PessoaID"].ToString() == "32078" ||
                    Ds.Tables["sqlHeader"].Rows[0]["PessoaID"].ToString() == "14121" ||
                    Ds.Tables["sqlHeader"].Rows[0]["PessoaID"].ToString() == "42896" ||
                    Ds.Tables["sqlHeader"].Rows[0]["PessoaID"].ToString() == "57723" ||
                    Ds.Tables["sqlHeader"].Rows[0]["PessoaID"].ToString() == "57725" ||
                    Ds.Tables["sqlHeader"].Rows[0]["PessoaID"].ToString() == "64006")

                    term_Acquire_SoldTo = "Sold to (Acquire):";
            }

            //SOLD TO    

            Relatorio.CriarObjLabel("Fixo", "", term_Acquire_SoldTo, Bloco3_Left.ToString(), Bloco3_Top.ToString(), "8", "black", "", "Body", "5", "");

            // Relatorio.CriarObjLabel("Query", "sqlHeader", "Nome", Bloco3_Left.ToString(), (Bloco3_Top += 0.044979).ToString(), "10", "black", "B", "Body", "9.70139", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "Nome", Bloco3_Left.ToString(), (Bloco3_Top += 0.44979).ToString(), "10", "black", "B", "Body", "9.70139", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "Endereco", Bloco3_Left.ToString(), (Bloco3_Top += 0.044979).ToString(), "10", "black", "", "Body", "10.70139", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "Endereco", Bloco3_Left.ToString(), (Bloco3_Top += 0.44979).ToString(), "10", "black", "", "Body", "10.70139", "");
            // Relatorio.CriarObjLabel("Query", "sqlHeader", "Complemento", Bloco3_Left.ToString(), (Bloco3_Top += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "Complemento", Bloco3_Left.ToString(), (Bloco3_Top += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd1", Bloco3_Left.ToString(), (Bloco3_Top += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd1", Bloco3_Left.ToString(), (Bloco3_Top += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");


            if (!bCustomsWarehouse)
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "Telefone", Bloco3_Left.ToString(), (Bloco3_Top += 0.044979).ToString(), "10", "black", "", "Body", "5", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Telefone", Bloco3_Left.ToString(), (Bloco3_Top += 0.44979).ToString(), "10", "black", "", "Body", "5", "");

            // Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd2", Bloco3_Left.ToString(), (Bloco3_Top += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd2", Bloco3_Left.ToString(), (Bloco3_Top += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");

            if (bCustomsWarehouse)
                Relatorio.CriarObjLabel("Fixo", "", "De acordo com a Instrução Normativa 241 de 6 de novembro de 2002.", "0", "0", "10", "black", "B", "Body", "5", "");

            // SHIP TO	

            Relatorio.CriarObjLabel("Fixo", "", "Ship to:", Bloco4_Left.ToString(), Bloco4_Top.ToString(), "8", "black", "", "Body", "5", "");

            // Se nao tem ShipTo na tabela NotasFiscais_ShipTo
            // usa o Pessoa do Pedido

            Ds.Tables.Add(RequestDatatable(SQLShipTo, "sqlShipTo").Copy());

            if (Ds.Tables["sqlShipTo"].Rows.Count == 0)
            {
                // Relatorio.CriarObjLabel("Query", "sqlHeader", "Nome", Bloco4_Left.ToString(), (Bloco4_Top += 0.044979).ToString(), "10", "black", "B", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Nome", Bloco4_Left.ToString(), (Bloco4_Top += 0.44979).ToString(), "10", "black", "B", "Body", "9.70139", "");
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "Endereco", Bloco4_Left.ToString(), (Bloco4_Top += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Endereco", Bloco4_Left.ToString(), (Bloco4_Top += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "Complemento", Bloco4_Left.ToString(), (Bloco4_Top += 0.044979).ToString(), "10", "black", "", "Body", "5", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Complemento", Bloco4_Left.ToString(), (Bloco4_Top += 0.44979).ToString(), "10", "black", "", "Body", "5", "");
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd1", Bloco4_Left.ToString(), (Bloco4_Top += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd1", Bloco4_Left.ToString(), (Bloco4_Top += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");
                // Relatorio.CriarObjLabel("Query", "sqlHeader", "Telefone", Bloco4_Left.ToString(), (Bloco4_Top += 0.044979).ToString(), "10", "black", "", "Body", "5", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Telefone", Bloco4_Left.ToString(), (Bloco4_Top += 0.44979).ToString(), "10", "black", "", "Body", "5", "");
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd2", Bloco4_Left.ToString(), (Bloco4_Top += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd2", Bloco4_Left.ToString(), (Bloco4_Top += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");
            }
            else
            {
                //Relatorio.CriarObjLabel("Query", "sqlShipTo", "Nome", Bloco4_Left.ToString(), (Bloco4_Top += 0.044979).ToString(), "10", "black", "B", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlShipTo", "Nome", Bloco4_Left.ToString(), (Bloco4_Top += 0.44979).ToString(), "10", "black", "B", "Body", "9.70139", "");
                //Relatorio.CriarObjLabel("Query", "sqlShipTo", "Endereco", Bloco4_Left.ToString(), (Bloco4_Top += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlShipTo", "Endereco", Bloco4_Left.ToString(), (Bloco4_Top += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");
                //Relatorio.CriarObjLabel("Query", "sqlShipTo", "Complemento", Bloco4_Left.ToString(), (Bloco4_Top += 0.044979).ToString(), "10", "black", "", "Body", "5", "");
                Relatorio.CriarObjLabel("Query", "sqlShipTo", "Complemento", Bloco4_Left.ToString(), (Bloco4_Top += 0.44979).ToString(), "10", "black", "", "Body", "5", "");
                //Relatorio.CriarObjLabel("Query", "sqlShipTo", "CampoEnd1", Bloco4_Left.ToString(), (Bloco4_Top += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlShipTo", "CampoEnd1", Bloco4_Left.ToString(), (Bloco4_Top += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");
                // Relatorio.CriarObjLabel("Query", "sqlShipTo", "Telefone", Bloco4_Left.ToString(), (Bloco4_Top += 0.044979).ToString(), "10", "black", "", "Body", "5", "");
                Relatorio.CriarObjLabel("Query", "sqlShipTo", "Telefone", Bloco4_Left.ToString(), (Bloco4_Top += 0.44979).ToString(), "10", "black", "", "Body", "5", "");
                // Relatorio.CriarObjLabel("Query", "sqlShipTo", "CampoEnd2", Bloco4_Left.ToString(), (Bloco4_Top += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlShipTo", "CampoEnd2", Bloco4_Left.ToString(), (Bloco4_Top += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");
            }

            //Relatorio.CriarObjLinha(paramLeft1, "0.910493", TamLinha.ToString(), "black", "Body");
            Relatorio.CriarObjLinha(paramLeft1, "9.10493", TamLinha.ToString(), "black", "Body");

            Relatorio.CriarObjLabel("Fixo", "", "Customer PO", Bloco5_Left.ToString(), Bloco5_Top.ToString(), "8", "black", "", "Body", "5", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "SeuPedido", Bloco5_Left.ToString(), (Bloco5_Top + 0.048505).ToString(), "10", "black", "", "Body", "5", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "SeuPedido", Bloco5_Left.ToString(), (Bloco5_Top + 0.48505).ToString(), "10", "black", "", "Body", "5", "");

            //Relatorio.CriarObjLabel("Fixo", "", "Terms (Day)", "0.2999", Bloco5_Top.ToString(), "8", "black", "", "Body", "5", "");
            Relatorio.CriarObjLabel("Fixo", "", "Terms (Day)", "2.999", Bloco5_Top.ToString(), "8", "black", "", "Body", "5", "");

            Ds.Tables.Add(RequestDatatable(SQLDetail1, "sqlDetail1").Copy());

            if (bCustomsWarehouse)
            {
                //Relatorio.CriarObjLabel("Fixo", "", "sem cobertura cambial", "0.345535", "0.892528", "10", "black", "", "Body", "5", "");
                Relatorio.CriarObjLabel("Fixo", "", "sem cobertura cambial", "3.45535", "8.92528", "10", "black", "", "Body", "5", "");
            }
            else
            {
                if (Ds.Tables["sqlDetail1"].Rows.Count != 0)
                {
                    // Relatorio.CriarObjLabel("Query", "sqlDetail1", "Prazo", "0.345535", (Bloco5_Top + 0.048505).ToString(), "10", "black", "", "Body", "5", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "Prazo", "3.45535", (Bloco5_Top + 0.48505).ToString(), "10", "black", "", "Body", "5", "");
                }
            }

            // Relatorio.CriarObjLabel("Fixo", "", "Due Date", "0.491118", Bloco5_Top.ToString(), "8", "black", "", "Body", "5", "");
            Relatorio.CriarObjLabel("Fixo", "", "Due Date", "4.91118", Bloco5_Top.ToString(), "8", "black", "", "Body", "5", "");

            if (!bCustomsWarehouse)
            {
                if (Ds.Tables["sqlDetail1"].Rows.Count != 0)
                {
                    //Relatorio.CriarObjLabel("Query", "sqlDetail1", "Vencimento", "0.491118", (Bloco5_Top + 0.048505).ToString(), "10", "black", "", "Body", "5", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "Vencimento", "4.91118", (Bloco5_Top + 0.48505).ToString(), "10", "black", "", "Body", "5", "");
                }
            }

            //Relatorio.CriarObjLabel("Fixo", "", "Freight Forwarder", "0.686909", Bloco5_Top.ToString(), "8", "black", "", "Body", "5", "");
            Relatorio.CriarObjLabel("Fixo", "", "Freight Forwarder", "6.86909", Bloco5_Top.ToString(), "8", "black", "", "Body", "5", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "Transportadora", "0.686909", (Bloco5_Top + 0.048505).ToString(), "10", "black", "", "Body", "7.7964", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "Transportadora", "6.86909", (Bloco5_Top + 0.48505).ToString(), "10", "black", "", "Body", "7.7964", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Ship Via", "1.72143", Bloco5_Top.ToString(), "8", "black", "", "Body", "1.63979", "");
            Relatorio.CriarObjLabel("Fixo", "", "Ship Via", "17.2143", Bloco5_Top.ToString(), "8", "black", "", "Body", "1.63979", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "MeioTransporte", "1.72143", (Bloco5_Top + 0.048505).ToString(), "10", "black", "", "Body", "3.19139", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "MeioTransporte", "17.2143", (Bloco5_Top + 0.48505).ToString(), "10", "black", "", "Body", "3.19139", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Incoterm", "1.863549", Bloco5_Top.ToString(), "8", "black", "", "Body", "1.37521", "");
            Relatorio.CriarObjLabel("Fixo", "", "Incoterm", "18.63549", Bloco5_Top.ToString(), "8", "black", "", "Body", "1.37521", "");

            if (!bCustomsWarehouse)
            {
                // Relatorio.CriarObjLabel("Query", "sqlHeader", "Incoterm", "1.863549", (Bloco5_Top + 0.048505).ToString(), "10", "black", "", "Body", "1.37521", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Incoterm", "18.63549", (Bloco5_Top + 0.48505).ToString(), "10", "black", "", "Body", "1.37521", "");
            }

            // Relatorio.CriarObjLinha(paramLeft1, " 1.014562", TamLinha.ToString(), "black", "Body");
            Relatorio.CriarObjLinha(paramLeft1, " 10.14562", TamLinha.ToString(), "black", "Body");

            Relatorio.CriarObjTabela(Bloco6_Left.ToString(), Bloco6_Top.ToString(), "sqlDetail2", false, false);

            Relatorio.CriarObjColunaNaTabela("ID", "ItemID", true, "0.7999", "Left");
            Relatorio.CriarObjCelulaInColuna("Query", "ItemID", "DetailGroup", null, null, 0, false, "7");

            Relatorio.CriarObjColunaNaTabela("Product Description (Model ~ Part Number)", "DescricaoItem", false, "12.702");
            //Relatorio.CriarObjColunaNaTabela("Product Description (Model ~ Part Number)", "DescricaoItem", false, "13.002");
            Relatorio.CriarObjCelulaInColuna("Query", "DescricaoItem", "DetailGroup", null, null, 0, false, "7");

            Relatorio.CriarObjColunaNaTabela("Qty", "Quantidade", true, "0.7999", "Center");
            //Relatorio.CriarObjColunaNaTabela("Qty", "Quantidade", true, "0.1999", "Center");
            Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup", null, null, 0, false, "7");

            Relatorio.CriarObjColunaNaTabela("Price (US$)", "ValorUnitario", false, "2.05343", "Right");
            //Relatorio.CriarObjColunaNaTabela("Price (US$)", "ValorUnitario", false, "1.75343", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ValorUnitario", "DetailGroup", null, null, 0, false, "7", " <Format>n2</Format>");

            Relatorio.CriarObjColunaNaTabela("Extended (US$)", "ValorTotal", false, "2.45343", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup", null, null, 0, false, "7", " <Format>n2</Format>");

            Relatorio.TabelaEnd();

            decimal nSubTotalInvoice = 0;

            if ((Ds.Tables["sqlHeader"].Rows[0]["ValorTotalProdutos"].ToString().Trim() != "") &&
               (Ds.Tables["sqlHeader"].Rows[0]["ValorTotalServicos"].ToString().Trim() != ""))
            {
                decimal vl1 = (decimal)Ds.Tables["sqlHeader"].Rows[0]["ValorTotalProdutos"];
                decimal vl2 = (decimal)Ds.Tables["sqlHeader"].Rows[0]["ValorTotalServicos"];

                nSubTotalInvoice = vl1 + vl2;
            }
            else if (Ds.Tables["sqlHeader"].Rows[0]["ValorTotalProdutos"].ToString().Trim() != "")
            {
                nSubTotalInvoice = (decimal)Ds.Tables["sqlHeader"].Rows[0]["ValorTotalProdutos"];
            }
            else if (Ds.Tables["sqlHeader"].Rows[0]["ValorTotalServicos"].ToString().Trim() != "")
            {
                nSubTotalInvoice = (decimal)Ds.Tables["sqlHeader"].Rows[0]["ValorTotalServicos"];
            }

            // Relatorio.CriarObjLinha(paramLeft1, "1.958243", TamLinha.ToString(), "black", "Body");
            Relatorio.CriarObjLinha(paramLeft1, "19.58243", TamLinha.ToString(), "black", "Body");

            if (!bCustomsWarehouse)
                Relatorio.CriarObjLabel("Fixo", "", "Customer agrees to the following:", Bloco7_Left.ToString(), Bloco7_Top.ToString(), "10", "black", "B", "Body", "6.49333", "");

            if (!bCustomsWarehouse)
                // Relatorio.CriarObjLabel("Fixo", "", "- 25% restoking fee on all returned items. NO CASH REFUNDS. Fee of $25.00 for all returned checks.", Bloco7_Left.ToString(), (Bloco7_Top += 0.042333).ToString(), "8", "black", "", "Body", "13.06979", "");
                Relatorio.CriarObjLabel("Fixo", "", "- 25% restoking fee on all returned items. NO CASH REFUNDS. Fee of $25.00 for all returned checks.", Bloco7_Left.ToString(), (Bloco7_Top += 0.42333).ToString(), "8", "black", "", "Body", "13.06979", "");

            if (!bCustomsWarehouse)
            {
                //Relatorio.CriarObjLabel("Fixo", "", "- All return items must be in original packaging and with invoice only.", Bloco7_Left.ToString(), (Bloco7_Top += 0.034396).ToString(), "8", "black", "", "Body", "13.06979", "");
                Relatorio.CriarObjLabel("Fixo", "", "- All return items must be in original packaging and with invoice only.", Bloco7_Left.ToString(), (Bloco7_Top += 0.34396).ToString(), "8", "black", "", "Body", "13.06979", "");
                //Relatorio.CriarObjLabel("Fixo", "", "- All merchandise carries manufacturer’s warranty only.", Bloco7_Left.ToString(), (Bloco7_Top += 0.034396).ToString(), "8", "black", "", "Body", "13.06979", "");
                Relatorio.CriarObjLabel("Fixo", "", "- All merchandise carries manufacturer’s warranty only.", Bloco7_Left.ToString(), (Bloco7_Top += 0.34396).ToString(), "8", "black", "", "Body", "13.06979", "");
                //Relatorio.CriarObjLabel("Fixo", "", "- Terms and Conditions printed on the back of this page.", Bloco7_Left.ToString(), (Bloco7_Top += 0.034396).ToString(), "8", "black", "", "Body", "13.06979", "");
                Relatorio.CriarObjLabel("Fixo", "", "- Terms and Conditions printed on the back of this page.", Bloco7_Left.ToString(), (Bloco7_Top += 0.34396).ToString(), "8", "black", "", "Body", "13.06979", "");
            }

            Ds.Tables.Add(RequestDatatable(SQLDetail4, "SQLDetail4").Copy());

            sMensagemNota = Ds.Tables["SQLDetail4"].Rows[0]["Mensagem"].ToString().Trim();

            // Relatorio.CriarObjLabel("Fixo", "", sMensagemNota, Bloco7_Left.ToString(), (Bloco7_Top += 0.054396).ToString(), "8", "black", "", "Body", "16.06979", "");
            Relatorio.CriarObjLabel("Fixo", "", sMensagemNota, Bloco7_Left.ToString(), (Bloco7_Top += 0.54396).ToString(), "8", "black", "", "Body", "16.06979", "");

            Relatorio.CriarObjLabel("Fixo", "", "Sub Total (US$)", Bloco8_Left.ToString(), Bloco8_Top.ToString(), "9", "black", "", "Body", "2.54756", "");
            //Relatorio.CriarObjLabel("Fixo", "", nSubTotalInvoice.ToString("#,0.00"), (Bloco8_Left + 0.138943).ToString(), Bloco8_Top2.ToString(), "9", "black", "", "Body", "1.92925", "Right");
            Relatorio.CriarObjLabel("Fixo", "", nSubTotalInvoice.ToString("#,0.00"), (Bloco8_Left + 1.38943).ToString(), Bloco8_Top2.ToString(), "9", "black", "", "Body", "1.92925", "Right");

            //Relatorio.CriarObjLabel("Fixo", "", "Tax (US$)", Bloco8_Left.ToString(), (Bloco8_Top += 0.111125).ToString(), "9", "black", "", "Body", "2.54756", "");
            Relatorio.CriarObjLabel("Fixo", "", "Tax (US$)", Bloco8_Left.ToString(), (Bloco8_Top += 1.11125).ToString(), "9", "black", "", "Body", "2.54756", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "ValorTotalImposto2", (Bloco8_Left + 0.138943).ToString(), (Bloco8_Top2 += 0.11).ToString(), "9", "black", "", "Body", "1.92925", "Right");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "ValorTotalImposto2", (Bloco8_Left + 1.38943).ToString(), (Bloco8_Top2 += 1.1).ToString(), "9", "black", "", "Body", "1.92925", "Right");

            // Relatorio.CriarObjLabel("Fixo", "", "Shipping (US$)", Bloco8_Left.ToString(), (Bloco8_Top += 0.111125).ToString(), "9", "black", "", "Body", "2.54756", "");
            Relatorio.CriarObjLabel("Fixo", "", "Shipping (US$)", Bloco8_Left.ToString(), (Bloco8_Top += 1.11125).ToString(), "9", "black", "", "Body", "2.54756", "");
            // Relatorio.CriarObjLabel("Query", "sqlHeader", "ValorFrete", (Bloco8_Left + 0.138943).ToString(), (Bloco8_Top2 += 0.11).ToString(), "9", "black", "", "Body", "1.92925", "Right");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "ValorFrete", (Bloco8_Left + 1.38943).ToString(), (Bloco8_Top2 += 1.1).ToString(), "9", "black", "", "Body", "1.92925", "Right");

            if (bCustomsWarehouse)
            {
                //Relatorio.CriarObjLabel("Fixo", "", "Total Proforma Invoice (US$)", Bloco8_Left.ToString(), (Bloco8_Top += 0.111125).ToString(), "9", "black", "", "Body", "2.54756", "");
                Relatorio.CriarObjLabel("Fixo", "", "Total Proforma Invoice (US$)", Bloco8_Left.ToString(), (Bloco8_Top += 1.11125).ToString(), "9", "black", "", "Body", "2.54756", "");
            }
            else
            {
                //Relatorio.CriarObjLabel("Fixo", "", "Total Invoice (US$)", Bloco8_Left.ToString(), (Bloco8_Top += 0.111125).ToString(), "9", "black", "B", "Body", "3.54756", "");
                Relatorio.CriarObjLabel("Fixo", "", "Total Invoice (US$)", Bloco8_Left.ToString(), (Bloco8_Top += 1.11125).ToString(), "9", "black", "B", "Body", "3.54756", "");
            }

            //Relatorio.CriarObjLabel("Query", "sqlHeader", "ValorTotalNota", (Bloco8_Left + 0.138943).ToString(), (Bloco8_Top2 += 0.11).ToString(), "9", "black", "B", "Body", "1.92925", "Right");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "ValorTotalNota", (Bloco8_Left + 1.38943).ToString(), (Bloco8_Top2 += 1.1).ToString(), "9", "black", "B", "Body", "1.92925", "Right");

            //Relatorio.CriarObjLinha(paramLeft1, "2.386424", TamLinha.ToString(), "black", "Body");
            Relatorio.CriarObjLinha(paramLeft1, "23.86424", TamLinha.ToString(), "black", "Body");


            if (_Copy == true)
            {
                if (bCustomsWarehouse)
                    Relatorio.CriarObjLabel("Fixo", "", "I declare that all the information contained the proforma invoice is true and correct:", Bloco9_Left.ToString(), Bloco9_Top.ToString(), "9", "black", "", "Body", "9.98235", "");

                else
                    Relatorio.CriarObjLabel("Fixo", "", "I declare that all the information contained the invoice is true and correct:", Bloco9_Left.ToString(), Bloco9_Top.ToString(), "9", "black", "", "Body", "9.98235", "");
            }
            else
            {
                Relatorio.CriarObjLabel("Fixo", "", "Received above merchandise in good condition:", Bloco9_Left.ToString(), Bloco9_Top.ToString(), "9", "black", "", "Body", "9.98235", "");
            }

            //Relatorio.CriarObjLabel("Fixo", "", "Date: ___/___/______    Signature: _________________________________    Name: ___________________________________", Bloco9_Left.ToString(), (Bloco9_Top + 0.092604).ToString(), "9", "black", "", "Body", "20.16063", "");
            Relatorio.CriarObjLabel("Fixo", "", "Date: ___/___/______    Signature: _________________________________    Name: ___________________________________", Bloco9_Left.ToString(), (Bloco9_Top + 0.92604).ToString(), "9", "black", "", "Body", "20.16063", "");

            //Relatorio.CriarObjLabel("Fixo", "", "_________________________________________________________________________________________________________________", Bloco9_Left.ToString(), (Bloco9_Top + 0.167657).ToString(), "9", "black", "", "Body", "20.16063", "");
            Relatorio.CriarObjLabel("Fixo", "", "_________________________________________________________________________________________________________________", Bloco9_Left.ToString(), (Bloco9_Top + 1.67657).ToString(), "9", "black", "", "Body", "20.16063", "");

            //Relatorio.CriarObjLabel("Fixo", "", "Issued by electronic process", Bloco9_Left.ToString(), (Bloco9_Top + 0.241565).ToString(), "9", "black", "", "Body", "5.59027", "");
            Relatorio.CriarObjLabel("Fixo", "", "Issued by electronic process", Bloco9_Left.ToString(), (Bloco9_Top + 2.41565).ToString(), "9", "black", "", "Body", "5.59027", "");

            if (_Copy == true)
            {
                //Relatorio.CriarObjLabel("Fixo", "", "CUSTOMER", (Bloco9_Left + 0.500).ToString(), (Bloco9_Top + 0.241565).ToString(), "9", "black", "B", "Body", "1.63201", "");
                Relatorio.CriarObjLabel("Fixo", "", "CUSTOMER", (Bloco9_Left + 5.00).ToString(), (Bloco9_Top + 2.41565).ToString(), "9", "black", "B", "Body", "1.63201", "");
            }
            else
            {
                //Relatorio.CriarObjLabel("Fixo", "", "ORIGINAL", (Bloco9_Left + 0.903167).ToString(), (Bloco9_Top + 0.241565).ToString(), "9", "black", "B", "Body", "1.63201", "");
                Relatorio.CriarObjLabel("Fixo", "", "ORIGINAL", (Bloco9_Left + 9.03167).ToString(), (Bloco9_Top + 2.41565).ToString(), "9", "black", "B", "Body", "1.63201", "");
            }

            // Relatorio.CriarObjLabel("Fixo", "", "TERMS & CONDITIONS", "0.87725", TermCon_Top.ToString(), TamFont.ToString(), "black", "B", "Body", "3.14792", "", true);
            Relatorio.CriarObjLabel("Fixo", "", "TERMS & CONDITIONS", "8.7725", TermCon_Top.ToString(), TamFont.ToString(), "black", "B", "Body", "3.14792", "", true);

            var paramLeftText = TermCon_Letf.ToString();
            var paramTopText = TermCon_Espacamento.ToString();

            Relatorio.CriarObjLabel("Fixo", "", "1. ACCEPTANCE", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "* This Agreement constitutes the entire and exclusive contract between ALLPLUS COMPUTER SYSTEMS CORP. (ALLPLUS), and the Purchaser and governs the rights and obligations of ALLPLUS and the Purchaser not withstanding any prior course of dealing, " +
                                                "custom of usage of trade or course of performance, prior invoicing terms and conditions, purchase orders, contract and agreements. "
                                   , paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "* By placing an order with ALLPLUS or by accepting shipment of merchandise from ALLPLUS, the Purchaser accepts and consents to all the terms and conditions stated in this Agreement. The ALLPLUS sales terms, conditions, and policies are subject to change without prior notice."
                                   , paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "* THE ACCEPTANCE OF THIS ACCOUNT BY ALLPLUS WILL CREATE A BINDING CONTRACT BETWEEN ALLPLUS, YOUR COMPANY AND THE GUARANTOR and such contract will create the following binding and irrevocable obligations of the parties to this this contract: Collection: In the event that Allplus Computer Systems Corp., or related company, is required to " +
                                                "bring any action to collect the sums due under this contract the parties agree that such a collection action may be brought solely and exclusively in the Circuit or County Court of Miami Dade County, Florida in the United States of America. Other Disputes – Jurisdiction – Venue – Conflict Of Laws:  All disputes of any nature arising in connection with or related to this contract may be brought solely and exclusively " +
                                                "in the Circuit or County Court of Miami Dade County in the United States of America. This contract shall be considered for all purposes a Florida document and shall be interpreted and enforced pursuant to the laws of the State of Florida regardless of principles of conflict of laws. Attorney's Fees and Costs. In the event any dispute arises under this contract, the prevailing party shall be entitled to reasonable costs and attorney's fees, at pre-trial, trial and all appellate levels and litigation expenses from the non-prevailing party."
                                   , paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "2. LIABILITY LIMITATIONS / INDEMNIFICATION", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "* ALLPLUS disclaims all liability for, and Purchaser specifically waives all rights to, any special, incidental or consequential damages, including any damages to properties or that resulting from loss of data, sale, profits, or goodwill, which Purchaser or its customer might suffer directly or indirectly as a result of any breach of any warranty, representation or covenant by ALLPLUS or its supplier / manufacturer.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Purchaser agrees to indemnify and hold ALLPLUS harmless against any claim or liability arising from any cause from any third party relating to subsequent sale or use of merchandise.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* ALLPLUS is not responsible for the integrity of data on storage or drive products of returning serviced items. ", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "3. PAYMENT TERMS", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "* \"Past Due\" accounts will be charged 1.5% (one e a half percent) simple interest per month. It will also cause ALLPLUS to suspend/void/withhold order and/or RMA and other services.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Purchaser not established on \"Company Checks\" or \"Net Term\" status with ALLPLUS must either prepay or pay by \"C.O.D. Cashier’s Check\" upon receipt of goods.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Purchaser approved for \"Company Check\" with ALLPLUS may pay by check drawn only on the company checking account already approved by ALLPLUS.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* \"Net Term\" Purchasers must make full payment by invoice due date.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Payment must be made by Cash or Cashier’s Check when credit limit is exceeded. ", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "4. RETURNED CHECKS (NSF/Stop-payment) AND PAST DUE ACCOUNTS", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "ALLPLUS will charge a $25 (twenty-five) service charge for every check returned. If full amount of the returned check plus service charge is not paid within 15 (fifteen) days from the payment due date, the account will be placed as \"Past Due\" and will revert to \"C.O.D. Cash\" or \"Prepayment\" basis. Further payment delay will cause ALLPLUS to assign outside collection efforts.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "5. RETURNING MERCHANDISE AUTHORIZATION (RMA)", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "* All return (replacement/credit/refund) must be pre-approved. Purchaser’s customer order cancellation is not a valid reason for return.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* A pre-approved RMA number must be marked clearly visible outside the shipping box and on the packing list.All items returned for credit/refund must be in original resellable packaging with all accessories. Otherwise ship RMA items without manuals / accessories.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Purchaser’s accounts must be current and have all checks cleared.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* RMA number expires 30 days after the date of its issuance.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* It is the Purchaser’s responsibility to make sure that returning items were purchased from ALLPLUS by matching all items with that on an ALLPLUS invoice or prior RMA records. Both the serial number and quantity of shipment must match that of RMA request or else is subject to rejection.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Purchasers are advised to test the RMA items prior to shipping. If the items returned are found to be out-of-warranty, not defective, or not belonging to ALLPLUS, they will be returned to Purchasers freight-collect, and this will cause delay on processing of remaining RMA items.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* ALLPLUS is not responsible for items that are damaged during shipment or misused/abused by the Purchaser.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Purchaser is responsible for all charges related to customs and overseas and express shipping for RMA.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "5.1 RMA PROCEDURES", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "5.1.1 MERCHANDISE TO BE RETURNED BY FREIGHT", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "(i) Use ALLPLUS RMA Request Form (please keep blank copies for future use); (ii) Fill out Sections 1 e 2 on the RMA Request form and fax or e-mail it along with supporting invoice/RMA record to ALLPLUS RMA Department to obtain an RMA number; (iii) ALLPLUS will fax or e-mail to Purchaser a RMA number issuance form, and the RMA Request form; (iv) Shipping -Enclose with the shipment our RMA Number Issuance Form and the RMA Request form.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "5.1.2 MERCHANDISE TO BE RETURNED IN PERSON", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "Follow steps (i) through (iii) of Section 5.1.1. Bring in items and our RMA issuance form plus copies of invoices.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "5.2 REFUND/CREDIT (Must be Pre-approved)", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "* Return within 15 (fifteen) days of invoice - credit at current market price (CMP). ", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Return between 15 (fifteen) to 30 (thirty) days of invoice - credit based on CMP less a restocking charge of 25% (twenty-five percent).", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* No credit or refund past 30 (thirty) days of invoice.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* No credit or refund on special order items (not on price list), opened software/hardware, freight, insurance, customs charges, and labor.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Refund (vs. credit) will be issued in the form of a company check and must be pre-approved during RMA number request.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Allow 2 (two) or more business weeks for processing of credit, 2 (two) weeks for refund, after ALLPLUS receives goods.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "5.3 REPAIR/REPLACEMENT", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "* No cross-shipping ", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* All inquiries on RMA status must be made within 30 (thirty) days of RMA number issuance and must be supported by proof of delivery.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* All claims on RMA discrepancies must be made within 1 (one) day of receipt.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* No claim on local delivery.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Past due balances will delay processing of items.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Replacement RMA items will be available for pickup by Purchasers at ALLPLUS facility ONLY.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "6. WARRANTY LIMITATIONS", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "* Warranty sticker, serial number, or component tampering, misuse, abuse, physical damage by Purchaser automatically void all warranty.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Repaired or replacement RMA items carry a warranty for only the remainder of warranty period from the date of original invoice.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Warranty and technical services cannot be extended to Purchaser’s customers.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* ALLPLUS makes no warranty of fitness for use or purpose of all goods sold. Product specifications are subject to change without prior notification.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* All functionally Dead-On-Arrival (DOA) items must be received at ALLPLUS’ warehouse within either 30 (thirty) days of ALLPLUS invoice or of date of receipt by Purchaser to receive a refund / credit/ speedy new replacements. Defective items returned subsequently will be repaired or replaced and are not eligible for credit or refund.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* The manufacturer warranty limitations apply to all products sold by ALLPLUS. ALLPLUS reserves the rights to repair or replace items with that of equivalent specifications.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* All individual parts sold by ALLPLUS have a limited warranty by manufacture.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* If the manufacturers warranty period for the product is longer than that of ALLPLUS’, the manufacturer’s RMA department will handle the service beyond ALLPLUS’ warranty period. ", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "7. QUALITY CONTROL (QC)", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "ALLPLUS relies on manufacturers’ quality control and does not individually test all products in our warehouse; hence ALLPLUS does not custom-configure motherboard or other product, jumper/software settings, including that for CPU’s. Purchaser is presumed to have qualified and knowledgeable technicians able to diagnose and pinpoint problem source. ALLPLUS’ technical support will provide product-specific installation update information but are discouraged to teach Purchaser basic diagnosis and installation step-by-step. A list of manufacturers providing direct technical and RMA service is available from ALLPLUS.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "8. SHIPMENT REFUSAL", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "After Purchaser orders merchandise from ALLPLUS, refusal to accept the shipment from the freight carrier for any reasons beyond the control of ALLPLUS will incur credit of CMP less a restocking charge of: 25% (twenty-five percent) of the invoice price and all freight charges (including loss or damage charges.) Subsequent purchases will require a written purchase order. ", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "9. DELIVERY", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "Shipment errors or discrepancies must be claimed in writing within 1 (one) day upon receipt of goods.Consignee (Recipient/Purchaser) upon receipt of goods must report damaged boxes or goods to freight carrier.Risk of loss passes to purchaser upon delivery by ALLPLUS to freight carrier.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "10. GOVERNING LAW AND FORUM", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "* All reasonable costs and expenses (including attorney’s and collection fees) incurred by ALLPLUS in collecting past due accounts shall be paid by the Purchaser.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Any dispute regarding the interpretation or validity hereof shall be governed by the laws of the state of Florida for sales out of Florida office respectively. The parties (both Seller and Purchaser) hereby agree that any dispute relating to the terms and conditions of sale furnished hereunder shall be subject to the jurisdiction of the courts of Miami, Florida, respectively.", paramLeftText, (TermCon_Top += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");



            // COPY 2
            // #############################################################################################################################################################################################################################################################################################################################################


            _Copy = true;

            // Relatorio.CriarObjImage("Query", "sqlHeader", "FotoEmpresa", paramLeft1, "5.065573", "Body", "5.09104", "1.74215");
            Relatorio.CriarObjImage("Query", "sqlHeader", "FotoEmpresa", paramLeft1, "50.65573", "Body", "5.09104", "1.74215");

            if (bCustomsWarehouse)
                // Relatorio.CriarObjLabel("Fixo", "", "PROFORMA INVOICE", "1.176347", "5.139532", "16", "black", "B", "Body", "8.22792", "");
                Relatorio.CriarObjLabel("Fixo", "", "PROFORMA INVOICE", "11.76347", "51.39532", "16", "black", "B", "Body", "8.22792", "");
            else if (nTransacaoID == 112)
                // Relatorio.CriarObjLabel("Fixo", "", "CREDIT MEMO", "1.176347", "5.139532", "16", "black", "B", "Body", "8.22792", "Right");
                Relatorio.CriarObjLabel("Fixo", "", "CREDIT MEMO", "11.76347", "51.39532", "16", "black", "B", "Body", "8.22792", "Right");
            else
                // Relatorio.CriarObjLabel("Fixo", "", "COMMERCIAL INVOICE", "1.176347", "5.139532", "16", "black", "B", "Body", "8.22792", "Right");
                Relatorio.CriarObjLabel("Fixo", "", "COMMERCIAL INVOICE", "11.76347", "51.39532", "16", "black", "B", "Body", "8.22792", "Right");

            // Relatorio.CriarObjLinha(paramLeft1, "5.209205", TamLinha.ToString(), "black", "Body");
            Relatorio.CriarObjLinha(paramLeft1, "52.09205", TamLinha.ToString(), "black", "Body");

            Relatorio.CriarObjLabel("Query", "sqlExtra01", "Nome", Bloco1_Left.ToString(), Bloco1_TopC.ToString(), "12", "black", "B", "Body", "8.83646", "");
            // Relatorio.CriarObjLabel("Query", "sqlExtra01", "End1", Bloco1_Left.ToString(), (Bloco1_TopC += 0.055687).ToString(), "8", "black", "B", "Body", "8.83646", "");
            Relatorio.CriarObjLabel("Query", "sqlExtra01", "End1", Bloco1_Left.ToString(), (Bloco1_TopC += 0.55687).ToString(), "8", "black", "B", "Body", "8.83646", "");
            //Relatorio.CriarObjLabel("Query", "sqlExtra01", "End2", Bloco1_Left.ToString(), (Bloco1_TopC += 0.039687).ToString(), "8", "black", "B", "Body", "8.83646", "");
            Relatorio.CriarObjLabel("Query", "sqlExtra01", "End2", Bloco1_Left.ToString(), (Bloco1_TopC += 0.39687).ToString(), "8", "black", "B", "Body", "8.83646", "");
            //Relatorio.CriarObjLabel("Fixo", "", sTelefone + "     " + sFax, Bloco1_Left.ToString(), (Bloco1_TopC += 0.039687).ToString(), "8", "black", "B", "Body", "8.83646", "");
            Relatorio.CriarObjLabel("Fixo", "", sTelefone + "     " + sFax, Bloco1_Left.ToString(), (Bloco1_TopC += 0.39687).ToString(), "8", "black", "B", "Body", "8.83646", "");
            //Relatorio.CriarObjLabel("Query", "sqlExtra01", "URL", Bloco1_Left.ToString(), (Bloco1_TopC += 0.039687).ToString(), "8", "black", "B", "Body", "4.10042", "");
            Relatorio.CriarObjLabel("Query", "sqlExtra01", "URL", Bloco1_Left.ToString(), (Bloco1_TopC += 0.39687).ToString(), "8", "black", "B", "Body", "4.10042", "");
            //Relatorio.CriarObjLabel("Query", "sqlExtra01", "Email", (Bloco1_Left + 0.325535).ToString(), (Bloco1_TopC).ToString(), "8", "black", "B", "Body", "4.73604", "");
            Relatorio.CriarObjLabel("Query", "sqlExtra01", "Email", (Bloco1_Left + 3.25535).ToString(), (Bloco1_TopC).ToString(), "8", "black", "B", "Body", "4.73604", "");

            if (bCustomsWarehouse)
                Relatorio.CriarObjLabel("Fixo", "", "Proforma Invoice:", Bloco2_Left.ToString(), Bloco2_TopC.ToString(), "12", "black", "B", "Body", "3.58889", "");
            else
                Relatorio.CriarObjLabel("Fixo", "", "Invoice:", Bloco2_Left.ToString(), Bloco2_TopC.ToString(), "12", "black", "B", "Body", "3.58889", "");

            //Relatorio.CriarObjLabel("Query", "sqlHeader", "NotaFiscal", (Bloco2_Left + 0.365945).ToString(), Bloco2_TopC.ToString(), "12", "black", "B", "Body", "3.06854", "Right");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "NotaFiscal", (Bloco2_Left + 3.65945).ToString(), Bloco2_TopC.ToString(), "12", "black", "B", "Body", "3.06854", "Right");
            //Relatorio.CriarObjLabel("Fixo", "", "Date:", Bloco2_Left.ToString(), (Bloco2_TopC += 0.051979).ToString(), "10", "black", "", "Body", "3.58889", "");
            Relatorio.CriarObjLabel("Fixo", "", "Date:", Bloco2_Left.ToString(), (Bloco2_TopC += 0.51979).ToString(), "10", "black", "", "Body", "3.58889", "");
            //Relatorio.CriarObjLabelData("Query", "sqlHeader", "dtNotaFiscal", (Bloco2_Left + 0.365945).ToString(), Bloco2_TopC.ToString(), "10", "black", "", "Body", "3.06854", "", "Right");
            Relatorio.CriarObjLabelData("Query", "sqlHeader", "dtNotaFiscal", (Bloco2_Left + 3.65945).ToString(), Bloco2_TopC.ToString(), "10", "black", "", "Body", "3.06854", "", "Right");
            //Relatorio.CriarObjLabel("Fixo", "", "Order:", Bloco2_Left.ToString(), (Bloco2_TopC += 0.044979).ToString(), "10", "black", "", "Body", "3.58889", "");
            Relatorio.CriarObjLabel("Fixo", "", "Order:", Bloco2_Left.ToString(), (Bloco2_TopC += 0.44979).ToString(), "10", "black", "", "Body", "3.58889", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "PedidoID", (Bloco2_Left + 0.365945).ToString(), Bloco2_TopC.ToString(), "10", "black", "", "Body", "3.06854", "Right");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "PedidoID", (Bloco2_Left + 3.65945).ToString(), Bloco2_TopC.ToString(), "10", "black", "", "Body", "3.06854", "Right");
            //Relatorio.CriarObjLabel("Fixo", "", "Salesperson:", Bloco2_Left.ToString(), (Bloco2_TopC += 0.044979).ToString(), "10", "black", "", "Body", "2.34409", "");
            Relatorio.CriarObjLabel("Fixo", "", "Salesperson:", Bloco2_Left.ToString(), (Bloco2_TopC += 0.44979).ToString(), "10", "black", "", "Body", "2.34409", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "Vendedor", (Bloco2_Left + 0.213).ToString(), Bloco2_TopC.ToString(), "10", "black", "", "Body", "4.59557", "Right");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "Vendedor", (Bloco2_Left + 2.13).ToString(), Bloco2_TopC.ToString(), "10", "black", "", "Body", "4.59557", "Right");
            //Relatorio.CriarObjLabel("Fixo", "", "Transaction:", Bloco2_Left.ToString(), (Bloco2_TopC += 0.044979).ToString(), "10", "black", "", "Body", "3.58889", "");
            Relatorio.CriarObjLabel("Fixo", "", "Transaction:", Bloco2_Left.ToString(), (Bloco2_TopC += 0.44979).ToString(), "10", "black", "", "Body", "3.58889", "");

            if (bCustomsWarehouse)
                // Relatorio.CriarObjLabel("Query", "sqlHeader", "Natureza", (Bloco2_Left + 0.365945).ToString(), Bloco2_TopC.ToString(), "10", "black", "", "Body", "3.06854", "Right");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Natureza", (Bloco2_Left + 3.65945).ToString(), Bloco2_TopC.ToString(), "10", "black", "", "Body", "3.06854", "Right");
            else
                // Relatorio.CriarObjLabel("Query", "sqlHeader", "Natureza", (Bloco2_Left + 0.365945).ToString(), Bloco2_TopC.ToString(), "10", "black", "", "Body", "3.06854", "Right");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Natureza", (Bloco2_Left + 3.65945).ToString(), Bloco2_TopC.ToString(), "10", "black", "", "Body", "3.06854", "Right");

            // Relatorio.CriarObjLinha(paramLeft1, "5.458739", TamLinha.ToString(), "black", "Body");
            Relatorio.CriarObjLinha(paramLeft1, "54.58739", TamLinha.ToString(), "black", "Body");

            //SOLD TO    

            Relatorio.CriarObjLabel("Fixo", "", term_Acquire_SoldTo, Bloco3_Left.ToString(), Bloco3_TopC.ToString(), "8", "black", "", "Body", "5", "");

            //Relatorio.CriarObjLabel("Query", "sqlHeader", "Nome", Bloco3_Left.ToString(), (Bloco3_TopC += 0.044979).ToString(), "10", "black", "B", "Body", "9.70139", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "Nome", Bloco3_Left.ToString(), (Bloco3_TopC += 0.44979).ToString(), "10", "black", "B", "Body", "9.70139", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "Endereco", Bloco3_Left.ToString(), (Bloco3_TopC += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "Endereco", Bloco3_Left.ToString(), (Bloco3_TopC += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");

            //Relatorio.CriarObjLabel("Query", "sqlHeader", "Complemento", Bloco3_Left.ToString(), (Bloco3_TopC += 0.044979).ToString(), "10", "black", "", "Body", "5", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "Complemento", Bloco3_Left.ToString(), (Bloco3_TopC += 0.44979).ToString(), "10", "black", "", "Body", "5", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd1", Bloco3_Left.ToString(), (Bloco3_TopC += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd1", Bloco3_Left.ToString(), (Bloco3_TopC += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");

            if (!bCustomsWarehouse)
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "Telefone", Bloco3_Left.ToString(), (Bloco3_TopC += 0.044979).ToString(), "10", "black", "", "Body", "5", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Telefone", Bloco3_Left.ToString(), (Bloco3_TopC += 0.44979).ToString(), "10", "black", "", "Body", "5", "");

            //Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd2", Bloco3_Left.ToString(), (Bloco3_TopC += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd2", Bloco3_Left.ToString(), (Bloco3_TopC += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");

            if (bCustomsWarehouse)
                Relatorio.CriarObjLabel("Fixo", "", "De acordo com a Instrução Normativa 241 de 6 de novembro de 2002.", "0", "0", "10", "black", "B", "Body", "5", "");

            // SHIP TO	

            Relatorio.CriarObjLabel("Fixo", "", "Ship to:", Bloco4_Left.ToString(), Bloco4_TopC.ToString(), "8", "black", "", "Body", "5", "");

            // Se nao tem ShipTo na tabela NotasFiscais_ShipTo
            // usa o Pessoa do Pedido

            if (Ds.Tables["sqlShipTo"].Rows.Count == 0)
            {
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "Nome", Bloco4_Left.ToString(), (Bloco4_TopC += 0.044979).ToString(), "10", "black", "B", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Nome", Bloco4_Left.ToString(), (Bloco4_TopC += 0.44979).ToString(), "10", "black", "B", "Body", "9.70139", "");
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "Endereco", Bloco4_Left.ToString(), (Bloco4_TopC += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Endereco", Bloco4_Left.ToString(), (Bloco4_TopC += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "Complemento", Bloco4_Left.ToString(), (Bloco4_TopC += 0.044979).ToString(), "10", "black", "", "Body", "5", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Complemento", Bloco4_Left.ToString(), (Bloco4_TopC += 0.44979).ToString(), "10", "black", "", "Body", "5", "");
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd1", Bloco4_Left.ToString(), (Bloco4_TopC += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd1", Bloco4_Left.ToString(), (Bloco4_TopC += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "Telefone", Bloco4_Left.ToString(), (Bloco4_TopC += 0.044979).ToString(), "10", "black", "", "Body", "5", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Telefone", Bloco4_Left.ToString(), (Bloco4_TopC += 0.44979).ToString(), "10", "black", "", "Body", "5", "");
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd2", Bloco4_Left.ToString(), (Bloco4_TopC += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "CampoEnd2", Bloco4_Left.ToString(), (Bloco4_TopC += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");
            }
            else
            {
                //Relatorio.CriarObjLabel("Query", "sqlShipTo", "Nome", Bloco4_Left.ToString(), (Bloco4_TopC += 0.044979).ToString(), "10", "black", "B", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlShipTo", "Nome", Bloco4_Left.ToString(), (Bloco4_TopC += 0.44979).ToString(), "10", "black", "B", "Body", "9.70139", "");
                //Relatorio.CriarObjLabel("Query", "sqlShipTo", "Endereco", Bloco4_Left.ToString(), (Bloco4_TopC += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlShipTo", "Endereco", Bloco4_Left.ToString(), (Bloco4_TopC += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");
                //Relatorio.CriarObjLabel("Query", "sqlShipTo", "Complemento", Bloco4_Left.ToString(), (Bloco4_TopC += 0.044979).ToString(), "10", "black", "", "Body", "5", "");
                Relatorio.CriarObjLabel("Query", "sqlShipTo", "Complemento", Bloco4_Left.ToString(), (Bloco4_TopC += 0.44979).ToString(), "10", "black", "", "Body", "5", "");
                //Relatorio.CriarObjLabel("Query", "sqlShipTo", "CampoEnd1", Bloco4_Left.ToString(), (Bloco4_TopC += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlShipTo", "CampoEnd1", Bloco4_Left.ToString(), (Bloco4_TopC += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");
                //Relatorio.CriarObjLabel("Query", "sqlShipTo", "Telefone", Bloco4_Left.ToString(), (Bloco4_TopC += 0.044979).ToString(), "10", "black", "", "Body", "5", "");
                Relatorio.CriarObjLabel("Query", "sqlShipTo", "Telefone", Bloco4_Left.ToString(), (Bloco4_TopC += 0.44979).ToString(), "10", "black", "", "Body", "5", "");
                //Relatorio.CriarObjLabel("Query", "sqlShipTo", "CampoEnd2", Bloco4_Left.ToString(), (Bloco4_TopC += 0.044979).ToString(), "10", "black", "", "Body", "9.70139", "");
                Relatorio.CriarObjLabel("Query", "sqlShipTo", "CampoEnd2", Bloco4_Left.ToString(), (Bloco4_TopC += 0.44979).ToString(), "10", "black", "", "Body", "9.70139", "");
            }

            //Relatorio.CriarObjLinha(paramLeft1, "5.7809", TamLinha.ToString(), "black", "Body");
            Relatorio.CriarObjLinha(paramLeft1, "57.809", TamLinha.ToString(), "black", "Body");

            Relatorio.CriarObjLabel("Fixo", "", "Customer PO", Bloco5_Left.ToString(), Bloco5_TopC.ToString(), "8", "black", "", "Body", "5", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "SeuPedido", Bloco5_Left.ToString(), (Bloco5_TopC + 0.048505).ToString(), "10", "black", "", "Body", "5", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "SeuPedido", Bloco5_Left.ToString(), (Bloco5_TopC + 0.48505).ToString(), "10", "black", "", "Body", "5", "");
            // Relatorio.CriarObjLabel("Fixo", "", "Terms (Day)", "0.2999", Bloco5_TopC.ToString(), "8", "black", "", "Body", "5", "");
            Relatorio.CriarObjLabel("Fixo", "", "Terms (Day)", "2.999", Bloco5_TopC.ToString(), "8", "black", "", "Body", "5", "");

            if (bCustomsWarehouse)
            {
                //Relatorio.CriarObjLabel("Fixo", "", "sem cobertura cambial", "0.345535", "0.892528", "10", "black", "", "Body", "5", "");
                Relatorio.CriarObjLabel("Fixo", "", "sem cobertura cambial", "3.45535", "8.92528", "10", "black", "", "Body", "5", "");
            }
            else
            {
                if (Ds.Tables["sqlDetail1"].Rows.Count != 0)
                {
                    //Relatorio.CriarObjLabel("Query", "sqlDetail1", "Prazo", "0.345535", (Bloco5_TopC + 0.048505).ToString(), "10", "black", "", "Body", "5", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "Prazo", "3.45535", (Bloco5_TopC + 0.48505).ToString(), "10", "black", "", "Body", "5", "");
                }
            }

            //Relatorio.CriarObjLabel("Fixo", "", "Due Date", "0.491118", Bloco5_TopC.ToString(), "8", "black", "", "Body", "5", "");
            Relatorio.CriarObjLabel("Fixo", "", "Due Date", "4.91118", Bloco5_TopC.ToString(), "8", "black", "", "Body", "5", "");

            if (!bCustomsWarehouse)
            {
                if (Ds.Tables["sqlDetail1"].Rows.Count != 0)
                {
                    // Relatorio.CriarObjLabel("Query", "sqlDetail1", "Vencimento", "0.491118", (Bloco5_TopC + 0.048505).ToString(), "10", "black", "", "Body", "5", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "Vencimento", "4.91118", (Bloco5_TopC + 0.48505).ToString(), "10", "black", "", "Body", "5", "");
                }
            }

            //Relatorio.CriarObjLabel("Fixo", "", "Freight Forwarder", "0.686909", Bloco5_TopC.ToString(), "8", "black", "", "Body", "5", "");
            Relatorio.CriarObjLabel("Fixo", "", "Freight Forwarder", "6.86909", Bloco5_TopC.ToString(), "8", "black", "", "Body", "5", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "Transportadora", "0.686909", (Bloco5_TopC + 0.048505).ToString(), "10", "black", "", "Body", "7.7964", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "Transportadora", "6.86909", (Bloco5_TopC + 0.48505).ToString(), "10", "black", "", "Body", "7.7964", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Ship Via", "1.72143", Bloco5_TopC.ToString(), "8", "black", "", "Body", "1.63979", "");
            Relatorio.CriarObjLabel("Fixo", "", "Ship Via", "17.2143", Bloco5_TopC.ToString(), "8", "black", "", "Body", "1.63979", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "MeioTransporte", "1.72143", (Bloco5_TopC + 0.048505).ToString(), "10", "black", "", "Body", "3.19139", "");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "MeioTransporte", "17.2143", (Bloco5_TopC + 0.48505).ToString(), "10", "black", "", "Body", "3.19139", "");
            // Relatorio.CriarObjLabel("Fixo", "", "Incoterm", "1.863549", Bloco5_TopC.ToString(), "8", "black", "", "Body", "1.37521", "");
            Relatorio.CriarObjLabel("Fixo", "", "Incoterm", "18.63549", Bloco5_TopC.ToString(), "8", "black", "", "Body", "1.37521", "");

            if (!bCustomsWarehouse)
            {
                //Relatorio.CriarObjLabel("Query", "sqlHeader", "Incoterm", "1.863549", (Bloco5_TopC + 0.048505).ToString(), "10", "black", "", "Body", "1.37521", "");
                Relatorio.CriarObjLabel("Query", "sqlHeader", "Incoterm", "18.63549", (Bloco5_TopC + 0.48505).ToString(), "10", "black", "", "Body", "1.37521", "");
            }

            // Relatorio.CriarObjLinha(paramLeft1, " 5.8746", TamLinha.ToString(), "black", "Body");
            Relatorio.CriarObjLinha(paramLeft1, " 58.746", TamLinha.ToString(), "black", "Body");

            Relatorio.CriarObjTabela(Bloco6_Left.ToString(), Bloco6_TopC.ToString(), "sqlDetail2", false);

            Relatorio.CriarObjColunaNaTabela("ID", "ItemID", true, "0.8", "Left");
            Relatorio.CriarObjCelulaInColuna("Query", "ItemID", "DetailGroup", null, null, 0, false, "7");

            Relatorio.CriarObjColunaNaTabela("Product Description (Model ~ Part Number)", "DescricaoItem", false, "12.702");
            //Relatorio.CriarObjColunaNaTabela("Product Description (Model ~ Part Number)", "DescricaoItem", false, "13.002");
            Relatorio.CriarObjCelulaInColuna("Query", "DescricaoItem", "DetailGroup", null, null, 0, false, "7");

            Relatorio.CriarObjColunaNaTabela("Qty", "Quantidade", true, "0.7999", "Center");
            //Relatorio.CriarObjColunaNaTabela("Qty", "Quantidade", true, "0.1999", "Center");
            Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup", null, null, 0, false, "7");

            Relatorio.CriarObjColunaNaTabela("Price (US$)", "ValorUnitario", false, "2.05343", "Right");
            //Relatorio.CriarObjColunaNaTabela("Price (US$)", "ValorUnitario", false, "1.75343", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ValorUnitario", "DetailGroup", null, null, 0, false, "7", " <Format>n2</Format>");

            Relatorio.CriarObjColunaNaTabela("Extended (US$)", "ValorTotal", false, "2.45343", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup", null, null, 0, false, "7", " <Format>n2</Format>");


            Relatorio.TabelaEnd();

            //Relatorio.CriarObjLinha(paramLeft1, "6.821809", TamLinha.ToString(), "black", "Body");
            Relatorio.CriarObjLinha(paramLeft1, "68.21809", TamLinha.ToString(), "black", "Body");

            if (!bCustomsWarehouse)
                Relatorio.CriarObjLabel("Fixo", "", "Customer agrees to the following:", Bloco7_Left.ToString(), Bloco7_TopC.ToString(), "10", "black", "B", "Body", "6.49333", "");

            if (!bCustomsWarehouse)
                //Relatorio.CriarObjLabel("Fixo", "", "- 25% restoking fee on all returned items. NO CASH REFUNDS. Fee of $25.00 for all returned checks.", Bloco7_Left.ToString(), 
                //(Bloco7_TopC += 0.042333).ToString(), "8", "black", "", "Body", "13.06979", "");
                Relatorio.CriarObjLabel("Fixo", "", "- 25% restoking fee on all returned items. NO CASH REFUNDS. Fee of $25.00 for all returned checks.", Bloco7_Left.ToString(), 
                    (Bloco7_TopC += 0.42333).ToString(), "8", "black", "", "Body", "13.06979", "");


                if (!bCustomsWarehouse)
            {
                //Relatorio.CriarObjLabel("Fixo", "", "- All return items must be in original packaging and with invoice only.", Bloco7_Left.ToString(), (Bloco7_TopC += 0.034396).ToString(), "8", "black", "", "Body", "13.06979", "");
                Relatorio.CriarObjLabel("Fixo", "", "- All return items must be in original packaging and with invoice only.", Bloco7_Left.ToString(), (Bloco7_TopC += 0.34396).ToString(), "8", "black", "", "Body", "13.06979", "");
                //Relatorio.CriarObjLabel("Fixo", "", "- All merchandise carries manufacturer’s warranty only.", Bloco7_Left.ToString(), (Bloco7_TopC += 0.034396).ToString(), "8", "black", "", "Body", "13.06979", "");
                Relatorio.CriarObjLabel("Fixo", "", "- All merchandise carries manufacturer’s warranty only.", Bloco7_Left.ToString(), (Bloco7_TopC += 0.34396).ToString(), "8", "black", "", "Body", "13.06979", "");
                //Relatorio.CriarObjLabel("Fixo", "", "- Terms and Conditions printed on the back of this page.", Bloco7_Left.ToString(), (Bloco7_TopC += 0.034396).ToString(), "8", "black", "", "Body", "13.06979", "");
                Relatorio.CriarObjLabel("Fixo", "", "- Terms and Conditions printed on the back of this page.", Bloco7_Left.ToString(), (Bloco7_TopC += 0.34396).ToString(), "8", "black", "", "Body", "13.06979", "");
            }

            //Relatorio.CriarObjLabel("Fixo", "", sMensagemNota, Bloco7_Left.ToString(), (Bloco7_TopC += 0.054396).ToString(), "8", "black", "", "Body", "16.06979", "");
            Relatorio.CriarObjLabel("Fixo", "", sMensagemNota, Bloco7_Left.ToString(), (Bloco7_TopC += 0.54396).ToString(), "8", "black", "", "Body", "16.06979", "");

            Relatorio.CriarObjLabel("Fixo", "", "Sub Total (US$)", Bloco8_Left.ToString(), Bloco8_TopC.ToString(), "9", "black", "", "Body", "2.54756", "");
            //Relatorio.CriarObjLabel("Fixo", "", nSubTotalInvoice.ToString("#,0.00"), (Bloco8_Left + 0.138943).ToString(), Bloco8_Top2C.ToString(), "9", "black", "", "Body", "1.92925", "Right");
            Relatorio.CriarObjLabel("Fixo", "", nSubTotalInvoice.ToString("#,0.00"), (Bloco8_Left + 1.38943).ToString(), Bloco8_Top2C.ToString(), "9", "black", "", "Body", "1.92925", "Right");

            //Relatorio.CriarObjLabel("Fixo", "", "Tax (US$)", Bloco8_Left.ToString(), (Bloco8_TopC += 0.111125).ToString(), "9", "black", "", "Body", "2.54756", "");
            Relatorio.CriarObjLabel("Fixo", "", "Tax (US$)", Bloco8_Left.ToString(), (Bloco8_TopC += 1.11125).ToString(), "9", "black", "", "Body", "2.54756", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "ValorTotalImposto2", (Bloco8_Left + 0.138943).ToString(), (Bloco8_Top2C += 0.11).ToString(), "9", "black", "", "Body", "1.92925", "Right");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "ValorTotalImposto2", (Bloco8_Left + 1.38943).ToString(), (Bloco8_Top2C += 1.1).ToString(), "9", "black", "", "Body", "1.92925", "Right");

            //Relatorio.CriarObjLabel("Fixo", "", "Shipping (US$)", Bloco8_Left.ToString(), (Bloco8_TopC += 0.111125).ToString(), "9", "black", "", "Body", "2.54756", "");
            Relatorio.CriarObjLabel("Fixo", "", "Shipping (US$)", Bloco8_Left.ToString(), (Bloco8_TopC += 1.11125).ToString(), "9", "black", "", "Body", "2.54756", "");
            //Relatorio.CriarObjLabel("Query", "sqlHeader", "ValorFrete", (Bloco8_Left + 0.138943).ToString(), (Bloco8_Top2C += 0.11).ToString(), "9", "black", "", "Body", "1.92925", "Right");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "ValorFrete", (Bloco8_Left + 1.38943).ToString(), (Bloco8_Top2C += 1.1).ToString(), "9", "black", "", "Body", "1.92925", "Right");

            if (bCustomsWarehouse)
            {
                //Relatorio.CriarObjLabel("Fixo", "", "Total Proforma Invoice (US$)", Bloco8_Left.ToString(), (Bloco8_TopC += 0.111125).ToString(), "9", "black", "", "Body", "2.54756", "");
                Relatorio.CriarObjLabel("Fixo", "", "Total Proforma Invoice (US$)", Bloco8_Left.ToString(), (Bloco8_TopC += 1.11125).ToString(), "9", "black", "", "Body", "2.54756", "");
            }
            else
            {
                //Relatorio.CriarObjLabel("Fixo", "", "Total Invoice (US$)", Bloco8_Left.ToString(), (Bloco8_TopC += 0.111125).ToString(), "9", "black", "B", "Body", "3.54756", "");
                Relatorio.CriarObjLabel("Fixo", "", "Total Invoice (US$)", Bloco8_Left.ToString(), (Bloco8_TopC += 1.11125).ToString(), "9", "black", "B", "Body", "3.54756", "");
            }

            //Relatorio.CriarObjLabel("Query", "sqlHeader", "ValorTotalNota", (Bloco8_Left + 0.138943).ToString(), (Bloco8_Top2C += 0.11).ToString(), "9", "black", "B", "Body", "1.92925", "Right");
            Relatorio.CriarObjLabel("Query", "sqlHeader", "ValorTotalNota", (Bloco8_Left + 1.38943).ToString(), (Bloco8_Top2C += 1.1).ToString(), "9", "black", "B", "Body", "1.92925", "Right");

            //Relatorio.CriarObjLinha(paramLeft1, "7.245297", TamLinha.ToString(), "black", "Body");
            Relatorio.CriarObjLinha(paramLeft1, "72.45297", TamLinha.ToString(), "black", "Body");


            if (_Copy == true)
            {
                if (bCustomsWarehouse)
                    Relatorio.CriarObjLabel("Fixo", "", "I declare that all the information contained the proforma invoice is true and correct:", Bloco9_Left.ToString(), Bloco9_TopC.ToString(), "9", "black", "", "Body", "11.66909", "");

                else
                    Relatorio.CriarObjLabel("Fixo", "", "I declare that all the information contained the invoice is true and correct:", Bloco9_Left.ToString(), Bloco9_TopC.ToString(), "9", "black", "", "Body", "11.66909", "");
            }
            else
            {
                Relatorio.CriarObjLabel("Fixo", "", "Received above merchandise in good condition:", Bloco9_Left.ToString(), Bloco9_TopC.ToString(), "9", "black", "", "Body", "11.66909", "");
            }

            //Relatorio.CriarObjLabel("Fixo", "", "Date: ___/___/______    Signature: _________________________________    Name: ___________________________________", Bloco9_Left.ToString(), (Bloco9_TopC + 0.092604).ToString(), "9", "black", "", "Body", "20.16063", "");
            Relatorio.CriarObjLabel("Fixo", "", "Date: ___/___/______    Signature: _________________________________    Name: ___________________________________", Bloco9_Left.ToString(), (Bloco9_TopC + 0.92604).ToString(), "9", "black", "", "Body", "20.16063", "");

            //Relatorio.CriarObjLabel("Fixo", "", "_________________________________________________________________________________________________________________", Bloco9_Left.ToString(), (Bloco9_TopC + 0.167657).ToString(), "9", "black", "", "Body", "20.16063", "");
            Relatorio.CriarObjLabel("Fixo", "", "_________________________________________________________________________________________________________________", Bloco9_Left.ToString(), (Bloco9_TopC + 1.67657).ToString(), "9", "black", "", "Body", "20.16063", "");

            //Relatorio.CriarObjLabel("Fixo", "", "Issued by electronic process", Bloco9_Left.ToString(), (Bloco9_TopC + 0.241565).ToString(), "9", "black", "", "Body", "5.59027", "");
            Relatorio.CriarObjLabel("Fixo", "", "Issued by electronic process", Bloco9_Left.ToString(), (Bloco9_TopC + 2.41565).ToString(), "9", "black", "", "Body", "5.59027", "");

            if (_Copy == true)
            {
                //Relatorio.CriarObjLabel("Fixo", "", "CUSTOMER", (Bloco9_Left + 0.903167).ToString(), (Bloco9_TopC + 0.241565).ToString(), "9", "black", "B", "Body", "2.63201", "");
                Relatorio.CriarObjLabel("Fixo", "", "CUSTOMER", (Bloco9_Left + 9.03167).ToString(), (Bloco9_TopC + 2.41565).ToString(), "9", "black", "B", "Body", "2.63201", "");
            }
            else
            {
                //Relatorio.CriarObjLabel("Fixo", "", "ORIGINAL", (Bloco9_Left + 0.903167).ToString(), (Bloco9_TopC + 0.241565).ToString(), "9", "black", "B", "Body", "1.63201", "");
                Relatorio.CriarObjLabel("Fixo", "", "ORIGINAL", (Bloco9_Left + 9.03167).ToString(), (Bloco9_TopC + 2.41565).ToString(), "9", "black", "B", "Body", "1.63201", "");
            }

            // Relatorio.CriarObjLabel("Fixo", "", "TERMS & CONDITIONS", "0.87725", TermCon_TopC.ToString(), TamFont.ToString(), "black", "B", "Body", "3.14792", "", true);
            Relatorio.CriarObjLabel("Fixo", "", "TERMS & CONDITIONS", "8.7725", TermCon_TopC.ToString(), TamFont.ToString(), "black", "B", "Body", "3.14792", "", true);

            var paramLeftTextC = TermCon_Letf.ToString();
            var paramTopTextC = TermCon_Espacamento.ToString();

            Relatorio.CriarObjLabel("Fixo", "", "1. ACCEPTANCE", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "* This Agreement constitutes the entire and exclusive contract between ALLPLUS COMPUTER SYSTEMS CORP. (ALLPLUS), and the Purchaser and governs the rights and obligations of ALLPLUS and the Purchaser not withstanding any prior course of dealing, " +
                                                "custom of usage of trade or course of performance, prior invoicing terms and conditions, purchase orders, contract and agreements. "
                                   , paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "* By placing an order with ALLPLUS or by accepting shipment of merchandise from ALLPLUS, the Purchaser accepts and consents to all the terms and conditions stated in this Agreement. The ALLPLUS sales terms, conditions, and policies are subject to change without prior notice."
                                   , paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "* THE ACCEPTANCE OF THIS ACCOUNT BY ALLPLUS WILL CREATE A BINDING CONTRACT BETWEEN ALLPLUS, YOUR COMPANY AND THE GUARANTOR and such contract will create the following binding and irrevocable obligations of the parties to this this contract: Collection: In the event that Allplus Computer Systems Corp., or related company, is required to " +
                                                "bring any action to collect the sums due under this contract the parties agree that such a collection action may be brought solely and exclusively in the Circuit or County Court of Miami Dade County, Florida in the United States of America. Other Disputes – Jurisdiction – Venue – Conflict Of Laws:  All disputes of any nature arising in connection with or related to this contract may be brought solely and exclusively " +
                                                "in the Circuit or County Court of Miami Dade County in the United States of America. This contract shall be considered for all purposes a Florida document and shall be interpreted and enforced pursuant to the laws of the State of Florida regardless of principles of conflict of laws. Attorney's Fees and Costs. In the event any dispute arises under this contract, the prevailing party shall be entitled to reasonable costs and attorney's fees, at pre-trial, trial and all appellate levels and litigation expenses from the non-prevailing party."
                                   , paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "2. LIABILITY LIMITATIONS / INDEMNIFICATION", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "* ALLPLUS disclaims all liability for, and Purchaser specifically waives all rights to, any special, incidental or consequential damages, including any damages to properties or that resulting from loss of data, sale, profits, or goodwill, which Purchaser or its customer might suffer directly or indirectly as a result of any breach of any warranty, representation or covenant by ALLPLUS or its supplier / manufacturer.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Purchaser agrees to indemnify and hold ALLPLUS harmless against any claim or liability arising from any cause from any third party relating to subsequent sale or use of merchandise.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* ALLPLUS is not responsible for the integrity of data on storage or drive products of returning serviced items. ", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "3. PAYMENT TERMS", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "* \"Past Due\" accounts will be charged 1.5% (one e a half percent) simple interest per month. It will also cause ALLPLUS to suspend/void/withhold order and/or RMA and other services.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Purchaser not established on \"Company Checks\" or \"Net Term\" status with ALLPLUS must either prepay or pay by \"C.O.D. Cashier’s Check\" upon receipt of goods.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Purchaser approved for \"Company Check\" with ALLPLUS may pay by check drawn only on the company checking account already approved by ALLPLUS.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* \"Net Term\" Purchasers must make full payment by invoice due date.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Payment must be made by Cash or Cashier’s Check when credit limit is exceeded. ", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "4. RETURNED CHECKS (NSF/Stop-payment) AND PAST DUE ACCOUNTS", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "ALLPLUS will charge a $25 (twenty-five) service charge for every check returned. If full amount of the returned check plus service charge is not paid within 15 (fifteen) days from the payment due date, the account will be placed as \"Past Due\" and will revert to \"C.O.D. Cash\" or \"Prepayment\" basis. Further payment delay will cause ALLPLUS to assign outside collection efforts.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "5. RETURNING MERCHANDISE AUTHORIZATION (RMA)", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "* All return (replacement/credit/refund) must be pre-approved. Purchaser’s customer order cancellation is not a valid reason for return.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* A pre-approved RMA number must be marked clearly visible outside the shipping box and on the packing list.All items returned for credit/refund must be in original resellable packaging with all accessories. Otherwise ship RMA items without manuals / accessories.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Purchaser’s accounts must be current and have all checks cleared.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* RMA number expires 30 days after the date of its issuance.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* It is the Purchaser’s responsibility to make sure that returning items were purchased from ALLPLUS by matching all items with that on an ALLPLUS invoice or prior RMA records. Both the serial number and quantity of shipment must match that of RMA request or else is subject to rejection.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Purchasers are advised to test the RMA items prior to shipping. If the items returned are found to be out-of-warranty, not defective, or not belonging to ALLPLUS, they will be returned to Purchasers freight-collect, and this will cause delay on processing of remaining RMA items.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* ALLPLUS is not responsible for items that are damaged during shipment or misused/abused by the Purchaser.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Purchaser is responsible for all charges related to customs and overseas and express shipping for RMA.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "5.1 RMA PROCEDURES", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "5.1.1 MERCHANDISE TO BE RETURNED BY FREIGHT", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "(i) Use ALLPLUS RMA Request Form (please keep blank copies for future use); (ii) Fill out Sections 1 e 2 on the RMA Request form and fax or e-mail it along with supporting invoice/RMA record to ALLPLUS RMA Department to obtain an RMA number; (iii) ALLPLUS will fax or e-mail to Purchaser a RMA number issuance form, and the RMA Request form; (iv) Shipping -Enclose with the shipment our RMA Number Issuance Form and the RMA Request form.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "5.1.2 MERCHANDISE TO BE RETURNED IN PERSON", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "Follow steps (i) through (iii) of Section 5.1.1. Bring in items and our RMA issuance form plus copies of invoices.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "5.2 REFUND/CREDIT (Must be Pre-approved)", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "* Return within 15 (fifteen) days of invoice - credit at current market price (CMP). ", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Return between 15 (fifteen) to 30 (thirty) days of invoice - credit based on CMP less a restocking charge of 25% (twenty-five percent).", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* No credit or refund past 30 (thirty) days of invoice.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* No credit or refund on special order items (not on price list), opened software/hardware, freight, insurance, customs charges, and labor.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Refund (vs. credit) will be issued in the form of a company check and must be pre-approved during RMA number request.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Allow 2 (two) or more business weeks for processing of credit, 2 (two) weeks for refund, after ALLPLUS receives goods.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "5.3 REPAIR/REPLACEMENT", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "* No cross-shipping ", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* All inquiries on RMA status must be made within 30 (thirty) days of RMA number issuance and must be supported by proof of delivery.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* All claims on RMA discrepancies must be made within 1 (one) day of receipt.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* No claim on local delivery.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Past due balances will delay processing of items.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Replacement RMA items will be available for pickup by Purchasers at ALLPLUS facility ONLY.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "6. WARRANTY LIMITATIONS", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "* Warranty sticker, serial number, or component tampering, misuse, abuse, physical damage by Purchaser automatically void all warranty.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Repaired or replacement RMA items carry a warranty for only the remainder of warranty period from the date of original invoice.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Warranty and technical services cannot be extended to Purchaser’s customers.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* ALLPLUS makes no warranty of fitness for use or purpose of all goods sold. Product specifications are subject to change without prior notification.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio. CriarObjLabel("Fixo", "", "* All functionally Dead-On-Arrival (DOA) items must be received at ALLPLUS’ warehouse within either 30 (thirty) days of ALLPLUS invoice or of date of receipt by Purchaser to receive a refund / credit/ speedy new replacements. Defective items returned subsequently will be repaired or replaced and are not eligible for credit or refund.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* The manufacturer warranty limitations apply to all products sold by ALLPLUS. ALLPLUS reserves the rights to repair or replace items with that of equivalent specifications.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* All individual parts sold by ALLPLUS have a limited warranty by manufacture.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* If the manufacturers warranty period for the product is longer than that of ALLPLUS’, the manufacturer’s RMA department will handle the service beyond ALLPLUS’ warranty period. ", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "7. QUALITY CONTROL (QC)", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "ALLPLUS relies on manufacturers’ quality control and does not individually test all products in our warehouse; hence ALLPLUS does not custom-configure motherboard or other product, jumper/software settings, including that for CPU’s. Purchaser is presumed to have qualified and knowledgeable technicians able to diagnose and pinpoint problem source. ALLPLUS’ technical support will provide product-specific installation update information but are discouraged to teach Purchaser basic diagnosis and installation step-by-step. A list of manufacturers providing direct technical and RMA service is available from ALLPLUS.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "8. SHIPMENT REFUSAL", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "After Purchaser orders merchandise from ALLPLUS, refusal to accept the shipment from the freight carrier for any reasons beyond the control of ALLPLUS will incur credit of CMP less a restocking charge of: 25% (twenty-five percent) of the invoice price and all freight charges (including loss or damage charges.) Subsequent purchases will require a written purchase order. ", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "9. DELIVERY", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "Shipment errors or discrepancies must be claimed in writing within 1 (one) day upon receipt of goods.Consignee (Recipient/Purchaser) upon receipt of goods must report damaged boxes or goods to freight carrier.Risk of loss passes to purchaser upon delivery by ALLPLUS to freight carrier.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.CriarObjLabel("Fixo", "", "10. GOVERNING LAW AND FORUM", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "B", "Body", "20.29292", "", true);

            Relatorio.CriarObjLabel("Fixo", "", "* All reasonable costs and expenses (including attorney’s and collection fees) incurred by ALLPLUS in collecting past due accounts shall be paid by the Purchaser.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");
            Relatorio.CriarObjLabel("Fixo", "", "* Any dispute regarding the interpretation or validity hereof shall be governed by the laws of the state of Florida for sales out of Florida office respectively. The parties (both Seller and Purchaser) hereby agree that any dispute relating to the terms and conditions of sale furnished hereunder shall be subject to the jurisdiction of the courts of Miami, Florida, respectively.", paramLeftTextC, (TermCon_TopC += TermCon_Espacamento).ToString(), TamFont.ToString(), "black", "", "Body", "20.29292", "");

            Relatorio.Imprimir("Invoice");
        }

        protected dataInterface DataInterfaceObj = new dataInterface(
        System.Configuration.ConfigurationManager.AppSettings["application"]);

        private DataTable RequestDatatable(string SQLQuery, string NomeQuery)
        {
            string strConn = "";

            Object oOverflySvrCfg = null;

            DataTable Dt = new DataTable();

            DataSet DsContainer = new DataSet();

            oOverflySvrCfg = comCreateObject("OverflySvrCfg.OverflyMTS");
            strConn = ((OVERFLYSVRCFGLib.OverflyMTS)oOverflySvrCfg).DotNetDataBaseStrConn(DataInterfaceObj.ApplicationName);

            SqlCommand cmd = new SqlCommand(SQLQuery);
            using (SqlConnection con = new SqlConnection(strConn))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    cmd.CommandTimeout = 0;
                    sda.SelectCommand = cmd;

                    sda.Fill(DsContainer, NomeQuery);
                }
            }

            Dt = DsContainer.Tables[NomeQuery];

            return Dt;
        }

        private static Object comCreateObject(string sProgID)
        {
            Type oType = Type.GetTypeFromProgID(sProgID);
            if (oType != null)
            {
                return Activator.CreateInstance(oType);
            }
            return null;
        }
    }
}