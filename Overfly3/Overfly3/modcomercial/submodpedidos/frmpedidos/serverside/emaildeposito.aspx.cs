using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class emaildeposito : System.Web.UI.OverflyPage
	{
		private Integer pedidoID;
		private Integer currEstadoID;
		private Integer newEstadoID;
		private Integer depositoID;

		private Integer zero = new Integer(0);
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(EDIArquivoOperadorLogistico());
		}
		
		protected DataSet EDIArquivoOperadorLogistico()
		{
            // Roda a procedure sp_EDI_Logistica_EnviarArquivo
			ProcedureParameters[] procParams = new ProcedureParameters[10];

			procParams[0] = new ProcedureParameters(
				"@PedidoID",
				System.Data.SqlDbType.Int,
				pedidoID.ToString());

			procParams[1] = new ProcedureParameters(
				"@EstadoDeID",
				System.Data.SqlDbType.Int,
				currEstadoID.ToString());

			procParams[2] = new ProcedureParameters(
				"@EstadoParaID",
				System.Data.SqlDbType.Int,
				depositoID.ToString());

			// Executa a procedure.
			DataInterfaceObj.execNonQueryProcedure(
                "sp_EDI_Logistica_EnviarArquivo",
				procParams);

			// Gera o resultado;
			return DataInterfaceObj.getRemoteData("select 'OK' as Ocorrencia");
		}
		
		protected Integer nPedidoID
		{
			get { return pedidoID; }
			set { pedidoID = value != null ? value : zero; }
		}
		
		protected Integer nCurrEstadoID
		{
			get { return currEstadoID; }
			set { currEstadoID = value != null ? value : zero; }
		}
		
		protected Integer nNewEstadoID
		{
			get { return newEstadoID; }
			set { newEstadoID = value != null ? value : zero; }
		}

		protected Integer nDepositoID
		{
			get { return depositoID; }
			set { depositoID = value != null ? value : zero; }
		}
	}
}
