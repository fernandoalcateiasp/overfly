﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;
using OVERFLYSVRCFGLib;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class EnviaEmailProposta : System.Web.UI.OverflyPage
    {
        protected Integer relatorioId;
        protected Integer usuarioId;
        protected Integer idiomaDeId;
        protected Integer idiomaParaId;
        protected Integer pedidoId;
        protected Integer validade;
        protected Integer contatoId;
        protected string contatoCargo;
        protected Integer fichaTecnica;
        protected java.lang.Boolean micro;
        protected Integer empresaId;
        protected string empresa;
        protected string site;
        protected Integer translateLen;
        protected Integer translateDim;
        protected Integer transacaoId;
        protected string dateSQLParam;
        protected java.lang.Boolean sup;
        protected java.lang.Boolean marca;
        protected java.lang.Boolean modelo;
        protected java.lang.Boolean ncm;
        protected java.lang.Boolean haObservacoes;
        protected java.lang.Boolean informacoesAdicionais;
        protected string urlRoot = null;
        protected string webPath = null;
        protected string mailBody;
        protected string report = "";
        protected string report2 = "";
        protected bool temPagamentoCCC = false;
        protected int colSpanItens = 0;
        protected int itensCount = 0;
        protected string modeloMicro;
        protected string prazoGarantia = "";
        protected string aprovarPropostaURL;
        protected string htmlCode = "";

        protected Integer zero = new Integer(0);
        protected java.lang.Boolean verdade = new java.lang.Boolean(true);
        protected java.lang.Boolean falso = new java.lang.Boolean(false);

 
        protected Integer nRelatorioID
        {
            get { return relatorioId; }
            set
            {
                relatorioId = value != null ? value : zero;

                if (relatorioId.intValue() == 40132)
                {
                    report = "Solicitação de Compra";
                    report2 = "Solicitação";
                }
                else
                {
                    report = "Proposta";
                    report2 = "Proposta";
                }
            }
        }

        protected Integer nUsuarioID
        {
            get { return usuarioId; }
            set { usuarioId = value != null ? value : zero; }
        }
           protected Integer nIdiomaDeID
        {
            get { return idiomaDeId; }
            set { idiomaDeId = value != null ? value : zero; }
        }

      
        protected Integer nIdiomaParaID
        {
            get { return idiomaParaId; }
            set
            {
                idiomaParaId = value != null ? value : zero;

                colSpanItens = (idiomaParaId.intValue() == 246) ? 2 : 0;
            }
        }

        protected int PedidoID
        {
            get { return pedidoId.intValue(); }
        }
        protected Integer nPedidoID
        {
            get { return pedidoId; }
            set { pedidoId = value != null ? value : zero; }
        }

        protected int Validade
        {
            get { return validade.intValue(); }
        }
        protected Integer nValidade
        {
            get { return validade; }
            set { validade = value != null ? value : zero; }
        }

        protected int ContatoID
        {
            get { return contatoId.intValue(); }
        }
        protected Integer nContatoID
        {
            get { return contatoId; }
            set { contatoId = value != null ? value : zero; }
        }

        protected string sContatoCargo
        {
            get { return contatoCargo; }
            set { contatoCargo = value != null ? value : ""; }
        }

        protected int FichaTecnica
        {
            get { return fichaTecnica.intValue(); }
        }
        protected Integer nFichaTecnica
        {
            get { return fichaTecnica; }
            set { fichaTecnica = value != null ? value : zero; }
        }

        protected bool EhMicro
        {
            get { return micro.booleanValue(); }
        }
        protected java.lang.Boolean nEhMicro
        {
            get { return micro; }
            set { micro = value != null ? value : falso; }
        }

        protected int EmpresaID
        {
            get { return empresaId.intValue(); }
        }
        protected Integer nEmpresaID
        {
            get { return empresaId; }
            set { empresaId = value != null ? value : zero; }
        }

        protected string sEmpresa
        {
            get { return empresa; }
            set { empresa = value != null ? value : ""; }
        }

        protected string sSite
        {
            get { return site; }
            set { site = value != null ? value : ""; }
        }

        protected Integer nTranslateLen
        {
            get { return translateLen; }
            set { translateLen = value != null ? value : zero; }
        }

        protected Integer nTranslateDim
        {
            get { return translateDim; }
            set { translateDim = value != null ? value : zero; }
        }

        protected string DATE_SQL_PARAM
        {
            get { return dateSQLParam; }
            set { dateSQLParam = value != null ? value : ""; }
        }

        protected bool SUP
        {
            get { return sup.booleanValue(); }
        }
        protected java.lang.Boolean bSUP
        {
            get { return sup; }
            set { sup = value != null ? value : verdade; }
        }

        protected string sModeloMicro
        {
            get { return modeloMicro; }
            set { modeloMicro = value != null ? value : ""; }
        }

        protected bool ExibeMarca
        {
            get { return bMarca.booleanValue(); }
        }
        protected java.lang.Boolean bMarca
        {
            get { return marca; }
            set { marca = value != null ? value : falso; }
        }

        protected bool ExibeModelo
        {
            get { return bModelo.booleanValue(); }
        }
        protected java.lang.Boolean bModelo
        {
            get { return modelo; }
            set { modelo = value != null ? value : falso; }
        }

        protected java.lang.Boolean bNcm
        {
            get { return ncm; }
            set { ncm = value != null ? value : falso; }
        }

        protected bool ExibeNcm
        {
            get { return bNcm.booleanValue(); }
        }

        protected java.lang.Boolean bObservacoes
        {
            get { return haObservacoes; }
            set { haObservacoes = value != null ? value : falso; }
        }

 
        protected java.lang.Boolean bInformacoesAdicionais
        {
            get { return informacoesAdicionais; }
            set { informacoesAdicionais = value != null ? value : falso; }
        }
        protected int TransacaoID
        {
            get { return transacaoId.intValue(); }
        }
        protected Integer nTransacaoID
        {
            get { return transacaoId; }
            set { transacaoId = value != null ? value : zero; }
        }

        private string MailFrom;

        public string sMailFrom
        {
            get { return MailFrom; }
            set { MailFrom = value; }
        }
        private string MailTo;

        public string sMailTo
        {
            get { return MailTo; }
            set { MailTo = value; }
        }
        private string MailCC;

        public string sMailCC
        {
            get { return MailCC; }
            set { MailCC = value; }
        }
        private string MailBCC;

        public string sMailBCC
        {
            get { return MailBCC; }
            set { MailBCC = value; }
        }
        private string Subject;

        public string sSubject
        {
            get { return Subject; }
            set { Subject = value; }
        }
        private string MailBody;

        public string sMailBody
        {
            get { return MailBody; }
            set { MailBody = value; }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            try
            {
                teste();
            }
            catch (System.Exception)
            {
                
                throw;
            }

            //alteração realizada neste ponto para duplicar apóstrofe para não dar erro quando passar para o SQL. Fernando Santos(TESTE)
            htmlCode = htmlCode.Replace("'", "''");

            WriteResultXML(DataInterfaceObj.getRemoteData(
                 "	DECLARE @TableEmail TABLE (EmailID INT) " +
             " INSERT INTO EmailsTemp (RelatorioID, Data, UserID, MailFrom, MailTo, MailCc, MailBcc, Subject, MailBody, TipoEmail) " +
                 " OUTPUT INSERTED.EmailID INTO @TableEmail" +
                 " SELECT " + relatorioId + ", getdate() , " + usuarioId.ToString() + ", '" + MailFrom + "','" + MailTo + "','" + MailCC + "','" + MailBCC + "','" + Subject + "','" + htmlCode.ToString() + "' , 16"
                 + "  SELECT EmailID FROM @TableEmail"));
        }

        private void teste()
        {
            using (WebClient client = new WebClient())
            {
                client.QueryString.Add("nUsuarioID", usuarioId.ToString()); //add parameters
                client.QueryString.Add("nIdiomaDeID", idiomaDeId.ToString()); //add parameters
                client.QueryString.Add("nIdiomaParaID", idiomaParaId.ToString()); //add parameters
                client.QueryString.Add("nPedidoID", pedidoId.ToString()); //add parameters
                client.QueryString.Add("nValidade", validade.ToString()); //add parameters
                client.QueryString.Add("nContatoID", contatoId.ToString()); //add parameters
                client.QueryString.Add("sContatoCargo", contatoCargo); //add parameters
                client.QueryString.Add("nFichaTecnica", fichaTecnica.ToString()); //add parameters
                client.QueryString.Add("nEhMicro", micro.ToString()); //add parameters
                client.QueryString.Add("nEmpresaID", empresaId.ToString()); //add parameters
                client.QueryString.Add("sEmpresa", empresa.ToString()); //add parameters
                client.QueryString.Add("sSite", sSite.ToString()); //add parameters
                client.QueryString.Add("DATE_SQL_PARAM", dateSQLParam.ToString()); //add parameters
                client.QueryString.Add("bMarca", marca.ToString()); //add parameters
                client.QueryString.Add("bModelo", modelo.ToString()); //add parameters
                client.QueryString.Add("bNcm", ncm.ToString()); //add parameters
                client.QueryString.Add("bSUP", sup.ToString()); //add parameters
                client.QueryString.Add("bObservacoes", haObservacoes.ToString()); //add parameters
                client.QueryString.Add("nRelatorioID", relatorioId.ToString()); //add parameters
                client.QueryString.Add("bInformacoesAdicionais", informacoesAdicionais.ToString()); //add parameters
                client.QueryString.Add("nTransacaoID", transacaoId.ToString()); //add parameters
                client.QueryString.Add("nTranslateLen",translateLen.ToString()); //add parameters
                client.QueryString.Add("nTranslateDim", translateDim.ToString()); //add parameters

                htmlCode = client.DownloadString(WebPath);
                                   
            }
        }
        public string URLRoot
        {
            get
            {
                if (urlRoot == null)
                {
                    OverflyMTS svrCfg = new OverflyMTS();

                    urlRoot = svrCfg.PagesURLRoot(
                        System.Configuration.ConfigurationManager.AppSettings["application"]
                    );

                    if (!urlRoot.StartsWith("http://www"))
                    {
                        urlRoot = "http://localhost/overfly3";
                    }

                }

                return urlRoot;
            }
        }
        public string WebPath
        {
            get
            {
                if (webPath == null)
                {
                    webPath = URLRoot + "/modcomercial/submodpedidos/frmpedidos/serverside/emailpropostacomercial.aspx";
                }

                return webPath;
            }
        }


    }
}