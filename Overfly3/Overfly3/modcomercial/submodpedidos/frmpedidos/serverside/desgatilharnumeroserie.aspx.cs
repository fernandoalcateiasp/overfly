using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class desgatilharnumeroserie : System.Web.UI.OverflyPage
	{
		private Integer pedidoId;
		private Integer ordemProducaoId;
		private Integer caixaId;
		private Integer produtoId;
		private Integer userId;

		private Integer zero = new Integer(0);
		
		protected DataSet PedidoDesgatilha()
		{
			// Roda a procedure sp_Pedido_Desgatilha
			ProcedureParameters[] procParams = new ProcedureParameters[10];

			procParams[0] = new ProcedureParameters(
				"@PedidoID",
				System.Data.SqlDbType.Int,
				pedidoId);

			procParams[1] = new ProcedureParameters(
				"@OrdemProducaoID",
				System.Data.SqlDbType.Int,
                ordemProducaoId.intValue() == 0 ? DBNull.Value : (Object)ordemProducaoId.ToString());

			procParams[2] = new ProcedureParameters(
				"@Quantidade",
				System.Data.SqlDbType.Int,
				DBNull.Value);

			procParams[3] = new ProcedureParameters(
				"@Identificador",
				System.Data.SqlDbType.Int,
				DBNull.Value);

			procParams[4] = new ProcedureParameters(
				"@EP",
				System.Data.SqlDbType.Int,
				DBNull.Value);

			procParams[5] = new ProcedureParameters(
				"@NumeroSerie",
				System.Data.SqlDbType.VarChar,
				DBNull.Value);
			procParams[5].Length = 25;

			procParams[6] = new ProcedureParameters(
				"@ProdutoID",
				System.Data.SqlDbType.Int,
				produtoId);

			procParams[7] = new ProcedureParameters(
				"@CaixaID",
				System.Data.SqlDbType.Int,
				caixaId.intValue() == -1 ? DBNull.Value : (Object)caixaId.ToString(),
				ParameterDirection.InputOutput);

			procParams[8] = new ProcedureParameters(
				"@QuantidadeDesgatilhada",
				System.Data.SqlDbType.Int,
				DBNull.Value,
				ParameterDirection.InputOutput);

			procParams[9] = new ProcedureParameters(
				"@StringErro",
				System.Data.SqlDbType.VarChar,
				DBNull.Value,
				ParameterDirection.InputOutput);
			procParams[9].Length = 8000;

			DataInterfaceObj.execNonQueryProcedure(
				"sp_Pedido_Desgatilha",
				procParams);
				
			return DataInterfaceObj.getRemoteData(
				"select " +
					((procParams[9].Data != DBNull.Value) ? 
						("'" + procParams[9].Data.ToString()) + "'" : "''") + 
				" as Resultado"
			);
		}
		
		public Integer nPedidoID
		{
			set { pedidoId = value != null ? value : zero; }
		}
		
		public Integer nOrdemProducaoID
		{
			set { ordemProducaoId = value != null ? value : zero; }
		}
		
		public Integer nCaixaID
		{
			set { caixaId = value != null ? value : zero; }
		}
		
		public Integer nProdutoID
		{
			set { produtoId = value != null ? value : zero; }
		}
		
		public Integer nUserID
		{
			set { userId = value != null ? value : zero; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(PedidoDesgatilha());
		}
	}
}
