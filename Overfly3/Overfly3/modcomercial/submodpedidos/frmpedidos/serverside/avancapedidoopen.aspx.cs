using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class avancapedidoopen : System.Web.UI.OverflyPage
    {
        private Integer pedidoID;
        private Integer estadoFimID;
        private Integer usuarioID;
        private string mensagem;
        private string Resultado;

        protected override void PageLoad(object sender, EventArgs e)
        {
            PedidoAvancaOpen();
            WriteResultXML(DataInterfaceObj.getRemoteData(
                    "select '" + mensagem + "' as Mensagem , '" + Resultado + "' as Resultado "));
        }

        protected void PedidoAvancaOpen()
        {
            // Roda a procedure sp_Empresa_Depositos
            ProcedureParameters[] procParams = new ProcedureParameters[5];

            procParams[0] = new ProcedureParameters(
                "@PedidoID",
                System.Data.SqlDbType.Int,
                pedidoID.ToString());

            procParams[1] = new ProcedureParameters(
                "@EstadoDeFimID",
                System.Data.SqlDbType.Int,
                estadoFimID);

            procParams[2] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                usuarioID);

            procParams[3] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.Int,
                System.DBNull.Value,
                ParameterDirection.Output);
            procParams[3].Length = 8000;

            procParams[4] = new ProcedureParameters(
                "@Mensagem",
                System.Data.SqlDbType.VarChar,
                System.DBNull.Value,
                ParameterDirection.Output);
            procParams[4].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Pedido_AvancaEstado",
                procParams);

            if (procParams[3].Data != DBNull.Value)
                Resultado = procParams[3].Data.ToString();
            if (procParams[4].Data != DBNull.Value)
                mensagem = procParams[4].Data.ToString();
        }

        protected Integer nPedidoID
        {
            get { return pedidoID; }
            set { pedidoID = (value != null ? value : new Integer(0)); }
        }

        protected Integer nEstadoFimID
        {
            get { return estadoFimID; }
            set { estadoFimID = (value != null ? value : new Integer(0)); }
        }

        protected Integer nUsuarioID
        {
            get { return usuarioID; }
            set { usuarioID = (value != null ? value : new Integer(0)); }
        }
    }
}
