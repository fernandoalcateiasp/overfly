using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class gerarordemproducao : System.Web.UI.OverflyPage
	{
		private Integer pedidoId;
		private Integer usuarioId;

		private Integer zero = new Integer(0);
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(PedidoParcelas());
		}

		// Roda a procedure sp_Pedido_OrdemProducao
		private DataSet PedidoParcelas()
		{
			ProcedureParameters[] procParams = new ProcedureParameters[2];

			procParams[0] = new ProcedureParameters(
				"@PedidoID",
				System.Data.SqlDbType.Int,
				pedidoId.ToString());

			procParams[1] = new ProcedureParameters(
				"@UsuarioID",
				System.Data.SqlDbType.Int,
				usuarioId.ToString());

			procParams[2] = new ProcedureParameters(
				"@Ocorrencia",
				System.Data.SqlDbType.Int,
				"0");

			procParams[3] = new ProcedureParameters(
				"@Resultado",
				System.Data.SqlDbType.VarChar,
				DBNull.Value,
				ParameterDirection.InputOutput);
			procParams[3].Length = 8000;

			DataInterfaceObj.execNonQueryProcedure(
				"sp_Pedido_OrdemProducao",
				procParams);

			return DataInterfaceObj.getRemoteData(
				"select " + procParams[3].Data + " as Mensagem"
			);
		}

		protected Integer nPedidoID
		{
			get { return pedidoId; }
			set { pedidoId = value != null ? value : zero; }
		}

        protected Integer nUsuarioID
		{
			get { return usuarioId; }
			set { usuarioId = value != null ? value : zero; }
		}
	}
}
