using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class incluiDadosFiscaisItens : System.Web.UI.OverflyPage
    {
        private Integer dataLen = Constants.INT_ZERO;
        public Integer nDataLen
        {
            get { return dataLen; }
            set { dataLen = value; }
        }

        private Integer[] pedidoID = Constants.INT_EMPTY_SET;
        public Integer[] nPedidoID
        {
            get { return pedidoID; }
            set { pedidoID = value; }
        }

        private Integer[] pedItemID = Constants.INT_EMPTY_SET;
        public Integer[] nPedItemID
        {
            get { return pedItemID; }
            set { pedItemID = value; }
        }

        private Integer[] origem = Constants.INT_EMPTY_SET;
        public Integer[] nOrigem
        {
            get { return origem; }
            set { origem = value; }
        }

        private Integer[] baseCalculoST = Constants.INT_EMPTY_SET;
        public Integer[] nBaseCalculoST
        {
            get { return baseCalculoST; }
            set { baseCalculoST = value; }
        }

        private Integer[] valorST = Constants.INT_EMPTY_SET;
        public Integer[] nValorST
        {
            get { return valorST; }
            set { valorST = value; }
        }

        private Integer[] CNPJFabricante = Constants.INT_EMPTY_SET;
        public Integer[] nCNPJFabricante
        {
            get { return CNPJFabricante; }
            set { CNPJFabricante = value; }
        }

        private Integer[] ehProducao = Constants.INT_EMPTY_SET;
        public Integer[] nEhProducao
        {
            get { return ehProducao; }
            set { ehProducao = value; }

        }

        private string[] operacao = Constants.STR_EMPTY_SET;
        public string[] sOperacao
        {
            get { return operacao; }
            set { operacao = value; }
        }

        private string[] NCM = Constants.STR_EMPTY_SET;
        public string[] sNCM
        {
            get { return NCM; }
            set { NCM = value; }
        }

        private string[] nfFabricante = Constants.STR_EMPTY_SET;
        public string[] sNFFabricante
        {
            get { return nfFabricante; }
            set { nfFabricante = value; }
        }

        private string[] FCI = Constants.STR_EMPTY_SET;
        public string[] sFCI
        {
            get { return FCI; }
            set { FCI = value; }
        }

        private string[] observacao = Constants.STR_EMPTY_SET;
        public string[] Observacao
        {
            get { return observacao; }
            set { observacao = value; }
        }

        protected string SQL
        {
            get
            {               
               string sql = "";
               for (int i = 0; i < nDataLen.intValue(); i++)
                {
                    if (operacao[i].ToString() == "Insert")
                    {
                        sql = sql +
                            "INSERT INTO dbo.Pedidos_DadosFiscais (PedidoID, PedItemID, NCM, Origem, BaseCalculoST, ValorST, EhProducao, CNPJFabricante, NotasFabricantes, dtControle, CodigoFCI) " +
                            "SELECT " + (pedidoID[i] != null ? (Object)pedidoID[i].ToString() : "NULL") + ", " + (pedItemID[i] != null ? (Object)pedItemID[i].ToString() : "NULL") + ", " +
                                        (sNCM[i] != null ? (Object)pedidoID[i].ToString() : "NULL") + ", " + (origem[i] != null ? (Object)origem[i].ToString() : "NULL") + ", " +
                                        (baseCalculoST[i] != null ? (Object)baseCalculoST[i].ToString() : "NULL") + ", " + (valorST[i] != null ? (Object)valorST[i].ToString() : "NULL") + ", " +
                                        (ehProducao[i] != null ? (Object)ehProducao[i].ToString() : "NULL") + ", " + (CNPJFabricante[i] != null ? (Object)CNPJFabricante[i].ToString() : "NULL") + ", "
                                        + (nfFabricante[i] != null ? (Object)nfFabricante[i].ToString() : "NULL") + ", GETDATE(), " + (FCI[i] != null ? (Object)FCI[i].ToString() : "NULL") + " ";
                    }
                   
                    else if  (operacao[i].ToString() == "Update") 
                    {
                         sql = sql +               
                           "UPDATE dbo.Pedidos_DadosFiscais " +
                                "SET NCM = " + (sNCM[i] != null ? (Object)pedidoID[i].ToString() : "NULL") + ", Origem = " + (origem[i] != null ? (Object)origem[i].ToString() : "NULL") +
                                  ", BaseCalculoST = " + (baseCalculoST[i] != null ? (Object)baseCalculoST[i].ToString() : "NULL") + ", " + "ValorST = " + (valorST[i] != null ? (Object)valorST[i].ToString() : "NULL") +
                                  ", EhProducao = " + (ehProducao[i] != null ? (Object)ehProducao[i].ToString() : "NULL") + ", CNPJFabricante = " + (CNPJFabricante[i] != null ? (Object)CNPJFabricante[i].ToString() : "NULL") +
                                ", NotasFabricantes = " + (nfFabricante[i] != null ? (Object)nfFabricante[i].ToString() : "NULL") + ", CodigoFCI = " + (FCI[i] != null ? (Object)FCI[i].ToString() : "NULL") +
                                " WHERE PedItemID = " + (pedItemID[i] != null ? (Object)pedItemID[i].ToString() : "NULL") + " ";
                    }
                    else if  (operacao[i].ToString() ==  "Delete") 
                    {
                        sql = sql +
                            "DELETE FROM dbo.Pedidos_DadosFiscais WHERE PedItemID = " + (pedItemID[i].intValue()) + " ";
                    }
               }
                
              return sql;
            }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            DataInterfaceObj.ExecuteSQLCommand(SQL);

            // DataInterfaceObj.getRemoteData("select '' as Resultado");
            WriteResultXML(DataInterfaceObj.getRemoteData(
               "SELECT 1 AS fldresp"));
        }
    }
}
