using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class listarpessoa : System.Web.UI.OverflyPage
	{
		private static Integer zero = new Integer(0);
		
		private Integer empresaId;
		private java.lang.Boolean ehCliente;
        private java.lang.Boolean cliente;
        private java.lang.Boolean parceiroId;
        private java.lang.Boolean entrada;
        private Integer usuarioId;
        private string pesquisa;
        private string argumento;
        

		protected override void PageLoad(object sender, EventArgs e)
		{
            WriteResultXML(sp_Pedido_ListarPessoa());
		}

        // Roda a procedure sp_Pedido_ListarPessoa
        private DataSet sp_Pedido_ListarPessoa()
		{
            ProcedureParameters[] procParams = new ProcedureParameters[8];

			procParams[0] = new ProcedureParameters(
                "@EmpresaID",
				System.Data.SqlDbType.Int,
				empresaId);

			procParams[1] = new ProcedureParameters(
                "@EhCliente",
				System.Data.SqlDbType.Bit,
                ehCliente.booleanValue() ? 1 : 0);

			procParams[2] = new ProcedureParameters(
                "@Cliente",
				System.Data.SqlDbType.Bit,
                cliente.booleanValue() ? 1 : 0);

			procParams[3] = new ProcedureParameters(
                "@Parceiro",
				System.Data.SqlDbType.Bit,
                parceiroId.booleanValue() ? 1 : 0);

            procParams[4] = new ProcedureParameters(
                "@EhEntrada",
                System.Data.SqlDbType.Bit,
                entrada.booleanValue() ? 1 : 0);

            procParams[5] = new ProcedureParameters(
                "@Pesquisa",
                System.Data.SqlDbType.VarChar,
                pesquisa);

            procParams[6] = new ProcedureParameters(
                "@Argumento",
                System.Data.SqlDbType.VarChar,
                argumento);

			procParams[7] = new ProcedureParameters(
                "@UsuarioID",
				System.Data.SqlDbType.Int,
				usuarioId);

            return DataInterfaceObj.execQueryProcedure(
                "sp_Pedido_ListarPessoa",
				procParams);
      
		}

        protected Integer nEmpresaID
		{
			get { return empresaId; }
            set { empresaId = value != null ? value : zero; }
		}

        protected java.lang.Boolean nEhCliente
		{
            get { return ehCliente; }
            set { ehCliente = (value != null ? value : new java.lang.Boolean(false)); }
		}

        protected java.lang.Boolean nCliente
		{
            get { return cliente; }
            set { cliente = (value != null ? value : new java.lang.Boolean(false)); }
		}

        protected java.lang.Boolean nParceiro
		{
            get { return parceiroId; }
            set { parceiroId = (value != null ? value : new java.lang.Boolean(false)); }
		}
        protected java.lang.Boolean nEntrada
        {
            get { return entrada; }
            set { entrada = (value != null ? value : new java.lang.Boolean(false)); }
        }
        protected Integer nUsuarioID
        {
            get { return usuarioId; }
            set { usuarioId = value != null ? value : zero; }
        }
        protected string sPesquisa
        {
            get { return pesquisa; }
            set { pesquisa = (value != null ? value : ""); }
        }
        protected string sArgumento
        {
            get { return argumento; }
            set { argumento = (value != null ? value : ""); }
        }


	}
}
