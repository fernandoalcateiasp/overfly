using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class enviaNFe : System.Web.UI.OverflyPage
    {
        protected static Integer Zero = new Integer(0);
        private Integer pedidoId;

        protected override void PageLoad(object sender, EventArgs e)
        {
            EmailFabricante();
            WriteResultXML(DataInterfaceObj.getRemoteData("select NULL as Resultado"));
        }

        // Roda a procedure sp_email_faturamento
        protected void EmailFabricante()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[1];

            procParams[0] = new ProcedureParameters(
                "@PedidoID",
                System.Data.SqlDbType.Int,
                (pedidoId.intValue() > 0) ? (Object)pedidoId.ToString() : DBNull.Value);

            DataInterfaceObj.execNonQueryProcedure(
                   "sp_email_faturamento",
                   procParams);
        }

        protected Integer nPedidoID
        {
            set { pedidoId = value != null ? value : Zero; }
        }
    }
}
