using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serversideEx
{
	public partial class cmbdepositos : System.Web.UI.OverflyPage
	{
		private static Integer Zero = new Integer(0);
		private Integer empresaId = Zero;
		private Integer parceiroId = Zero;
		private Integer transacaoId = Zero;

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(EmpresaDepositos());
		}

		protected DataSet EmpresaDepositos()
		{
			// Roda a procedure sp_Empresa_Depositos
			ProcedureParameters[] procParams = new ProcedureParameters[3];

			procParams[0] = new ProcedureParameters(
				"@EmpresaID",
				System.Data.SqlDbType.Int,
				empresaId.ToString());
				
			procParams[1] = new ProcedureParameters(
				"@ParceiroID",
				System.Data.SqlDbType.Int,
				parceiroId.ToString());

			procParams[2] = new ProcedureParameters(
				"@TransacaoID",
				System.Data.SqlDbType.Int,
				transacaoId.ToString());

			return DataInterfaceObj.execQueryProcedure(
				"sp_Empresa_Depositos", 
				procParams);
		}

		protected Integer EmpresaID
		{
			set { empresaId = (value != null ? value : Zero); }
		}

		protected Integer TransacaoID
		{
			set { transacaoId = (value != null ? value : Zero); }
		}

		protected Integer ParceiroID
		{
			set { parceiroId = (value != null ? value : Zero); }
		}
	}
}
