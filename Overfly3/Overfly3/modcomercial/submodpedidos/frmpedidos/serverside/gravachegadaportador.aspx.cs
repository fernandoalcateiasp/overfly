using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class gravachegadaportador : System.Web.UI.OverflyPage
	{
		public Integer pedidoId;
		public java.lang.Boolean gravaNulo;
	
		protected override void PageLoad(object sender, EventArgs e)
		{
			int fldresp;
						
			if(!gravaNulo.booleanValue())
			{
				fldresp = DataInterfaceObj.ExecuteSQLCommand(
					"UPDATE Pedidos SET dtChegadaPortador = GETDATE() WHERE PedidoID =" +
					pedidoId);
			}
			else
			{
				fldresp = DataInterfaceObj.ExecuteSQLCommand(
					"UPDATE Pedidos SET dtChegadaPortador = NULL WHERE PedidoID =" +
					pedidoId);
			}

			WriteResultXML(			
				DataInterfaceObj.getRemoteData(
					"select " + fldresp + " as fldresp"
				)
			);
		}

        protected Integer nPedidoID
		{
			get { return pedidoId; }
			set { pedidoId = value != null ? value : new Integer(0); }
		}

        protected java.lang.Boolean bGravaNulo
		{
			get { return gravaNulo; }
			set { gravaNulo = value != null ? value : new java.lang.Boolean(false); }
		}
	}
}
