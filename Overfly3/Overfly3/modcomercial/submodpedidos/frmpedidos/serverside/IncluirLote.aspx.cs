using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class IncluirLote : System.Web.UI.OverflyPage
	{
        private Integer empresaID;
        private Integer proprietarioID;
        private Integer usuarioID;
        private string DtFim;
        private string descricao;
        private string observacao;
        
		private static Integer zero = new Integer(0);
		
 		protected override void PageLoad(object sender, EventArgs e)
		{
            WriteResultXML(LoteGerador());
		}

        // Roda a procedure sp_Lote_Gerador
		private DataSet LoteGerador()
		{
			ProcedureParameters[] procParams = new ProcedureParameters[9];

			procParams[0] = new ProcedureParameters(
                "@EmpresaID",
				System.Data.SqlDbType.Int,
                empresaID.ToString());

			procParams[1] = new ProcedureParameters(
                "@Descricao",
				System.Data.SqlDbType.VarChar,
                descricao.ToString());			

			procParams[2] = new ProcedureParameters(
                "@dtEmissao",
				System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[3] = new ProcedureParameters(
                "@dtInicio",
                System.Data.SqlDbType.VarChar,
                System.DBNull.Value);
            
            procParams[4] = new ProcedureParameters(
                "@DtFim",
                System.Data.SqlDbType.VarChar,
                dtFim.ToString());            

            procParams[5] = new ProcedureParameters(
                "@Observacao",
                System.Data.SqlDbType.VarChar,
                observacao.ToString());

            procParams[6] = new ProcedureParameters(
                "@ProprietarioID",
                System.Data.SqlDbType.Int,
                proprietarioID.ToString());
   
            procParams[7] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                usuarioID.ToString());
   
            procParams[8] = new ProcedureParameters(
                "@LoteID",
				System.Data.SqlDbType.Int,
				DBNull.Value,
				ParameterDirection.InputOutput);			

			DataInterfaceObj.execNonQueryProcedure(
                "sp_Lote_Gerador",
				procParams);

			return DataInterfaceObj.getRemoteData(
                "select " + procParams[8].Data + " as LoteID"
			);
		}

        protected Integer nEmpresaID
		{
			get { return empresaID; }
            set { empresaID = value != null ? value : zero; }
		}

        protected Integer nProprietarioID
		{
			get { return proprietarioID; }
            set { proprietarioID = value != null ? value : zero; }
		}

        protected Integer nUsuarioID
		{
			get { return usuarioID; }
			set { usuarioID = value != null ? value : zero; }
		}

        protected string sDescricao
        {
            get { return descricao; }
            set { descricao = value != null ? value : ""; }
        }

        protected string dtFim
        {
            get { return DtFim; }
            set { DtFim = value != null ? value : ""; }
        }

        protected string sObservacao
        {
            get { return observacao; }
            set { observacao = value != null ? value : ""; }
        }
	}
}
