using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serversideEx
{
    public partial class cmblocaisentrega : System.Web.UI.OverflyPage
	{
		private static Integer Zero = new Integer(0);

		private Integer pessoaID = Zero;

		protected override void PageLoad(object sender, EventArgs e)
		{
            WriteResultXML(LocalEntrega());
		}

		protected DataSet LocalEntrega()
		{
			// Roda a procedure sp_Empresa_Depositos
			ProcedureParameters[] procParams = new ProcedureParameters[1];
				
			procParams[0] = new ProcedureParameters(
                "@PessoaID",
				System.Data.SqlDbType.Int,
				pessoaID.ToString());

			return DataInterfaceObj.execQueryProcedure(
                "sp_Pessoa_LocaisEntrega", 
				procParams);
		}

		protected Integer PessoaID
		{
            set { pessoaID = (value != null ? value : Zero); }
		}
	}
}
