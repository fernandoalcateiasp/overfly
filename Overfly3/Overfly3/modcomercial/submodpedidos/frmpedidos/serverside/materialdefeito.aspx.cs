using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class materialdefeito : System.Web.UI.OverflyPage
	{
		private static Integer zero = new Integer(0);
		private static Integer[] emptInt = new Integer[0];
		
		private Integer dataLen;
		private Integer[] numMovimentoId;
		private Integer[] pedProdutoId;
		private Integer[] pedLoteId;
		private string [] defeito;
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.ExecuteSQLCommand(GetSql());

			WriteResultXML(DataInterfaceObj.getRemoteData("select 0 as Ocorrencia"));
		}
		
		private string GetSql()
		{
			string sql = "";
			
			for(int i = 0; i < dataLen.intValue(); i++)
			{
                if (numMovimentoId[i].intValue() != 0)
                {
                    sql += " UPDATE NumerosSerie_Movimento SET Defeito = '" + defeito[i] + "' " +
                                    "WHERE (NumMovimentoID = " + numMovimentoId[i] + " ) ";
                }
                else if (pedProdutoId[i].intValue() != 0)
                {
                    sql += " UPDATE Pedidos_ProdutosSeparados SET Defeito = '" + defeito[i] + "' " +
                                    "WHERE (PedProdutoID = " + pedProdutoId[i] + " ) ";
                }
                // INICIO DE NOVO NS ---------------------------------------------------------------
                else if (pedLoteId[i].intValue() != 0)
                {
                    sql += " UPDATE Pedidos_ProdutosLote SET Defeito = '" + defeito[i] + "' " +
                                    "WHERE (PedProdutoID = " + pedLoteId[i] + " ) ";
                }
				// FIM DE NOVO NS ---------------------------------------------------------------
			}
			
			return sql;
		}
		
		protected Integer nDataLen
		{
			get { return dataLen; }
			set { dataLen = value != null ? value : zero; }
		}

        protected Integer[] nNMID
		{
			get { return numMovimentoId; }
			set { numMovimentoId = value != null ? value : emptInt; }
		}

        protected Integer[] nPPID
		{
			get { return pedProdutoId; }
			set { pedProdutoId = value != null ? value : emptInt; }
		}

        protected Integer[] nPLID
		{
			get { return pedLoteId; }
			set { pedLoteId = value != null ? value : emptInt; }
		}

        protected string[] nDefeito
		{
            get { return defeito; }
            set { if (value != null) defeito = value; }
		}
	}
}
