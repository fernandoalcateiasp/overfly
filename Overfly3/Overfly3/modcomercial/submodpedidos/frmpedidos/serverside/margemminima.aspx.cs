using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class margemminima : System.Web.UI.OverflyPage
	{
		private static Integer zeroInt = new Integer(0);
		private static Integer[] emptyInt = new Integer[0];        

		private static java.lang.Boolean falso = new java.lang.Boolean(false);
		
		
		private java.lang.Boolean aprovar;
		private java.lang.Boolean alterarMargem;
		private Integer usuarioId;
		private Integer[] registroId;
        private Integer[] margemMinima;
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.ExecuteSQLCommand(GetSql());
            WriteResultXML(
              DataInterfaceObj.getRemoteData("select 0 as Ocorrencia")
          );		
		}
		
		private string GetSql()
		{
			string sql = "";
			
			if(alterarMargem.booleanValue())
			{
				for(int i = 0; i < registroId.Length; i++)
				{
					sql += 
						" UPDATE RelacoesPesCon SET " + 
							" MargemMinima=" + margemMinima[i] + "," + 
							" UsuarioID=" + usuarioId +
						" WHERE RelacaoID = " + registroId[i];
				}
			}
			else
			{
				for (int i = 0; i < registroId.Length; i++)
				{
					if(aprovar.booleanValue())
					{
						sql +=
							" UPDATE Pedidos_Itens SET " + 
								" AprovadorID = " + usuarioId +
                            " WHERE PedItemID = " + registroId[i];							
					}
					else
					{
						sql +=
							" UPDATE Pedidos_Itens SET AprovadorID = NULL " +
                            " WHERE PedItemID = " + registroId[i];
					}
				}
			}
			
			return sql;
		}

		public java.lang.Boolean Aprovar
		{
			get { return aprovar; }
			set { aprovar = value != null ? value : falso; }
		}
		
		public java.lang.Boolean AlterarMargem
		{
			get { return alterarMargem; }
			set { alterarMargem = value != null ? value : falso; }
		}
		
		public Integer UsuarioID
		{
			get { return usuarioId; }
			set { usuarioId = value != null ? value : zeroInt; }
		}
		
		public Integer[] RegistroID
		{
			get { return registroId; }
			set { registroId = value != null ? value : emptyInt; }
		}

        public Integer[] MargemMinima
		{
			get { return margemMinima; }
			set { margemMinima = value != null ? value : emptyInt; }
		}
	}
}
