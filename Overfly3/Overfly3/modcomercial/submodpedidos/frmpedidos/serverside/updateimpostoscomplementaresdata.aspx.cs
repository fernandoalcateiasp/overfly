using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class updateimpostoscomplementaresdata : System.Web.UI.OverflyPage
	{
		private Integer[] pedImpostoID;
		private java.lang.Double[] valorImposto;
		private java.lang.Double[] baseCalculo;
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			int resultado = DataInterfaceObj.ExecuteSQLCommand(GetSql);
			
			WriteResultXML(
				DataInterfaceObj.getRemoteData(" SELECT " + resultado + " AS Resultado")
			);
		}
		
		protected Integer[] nPedImpostoID
		{
			set { pedImpostoID = value; }
		}
		protected java.lang.Double[] nValorImposto
		{
			set { valorImposto = value; }
		}
		protected java.lang.Double[] nBaseCalculo
		{
			set { baseCalculo = value; }
		}
		
		protected string GetSql
		{
			get
			{
				string result = "";

				for (int i = 0;
					pedImpostoID != null && 
						valorImposto != null && 
						baseCalculo != null && 
						i < pedImpostoID.Length; 
					i++)
				{
					result +=
						"UPDATE Pedidos_Itens_ImpostosComplementares SET " +
                            "ValorImposto=" + valorImposto[i].doubleValue() +", " +
							"BaseCalculo=" + baseCalculo[i].doubleValue() + " " + 
						"FROM Pedidos_Itens_ImpostosComplementares a " +
							"INNER JOIN Pedidos_Itens b ON (b.PedItemID = a.PedItemID) " +
							"INNER JOIN Pedidos c ON (c.PedidoID = b.PedidoID) " +
						"WHERE a.PedImpostoID=" + pedImpostoID[i] + " AND " +
							"c.EstadoID <= 26";
				}
				
				return result;
			}
		}
	}
}
