<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="emailpropostacomercial.aspx.cs" Inherits="Overfly3.modcomercial.submodpedidos.frmpedidos.serverside.emailpropostacomercial" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
	<head runat="server">
	</head>

	<body>
		<%
			if (IdiomaParaID == 246)
			{
				Response.Write("Segue a Proposta Comercial solicitada:");
			}
			else 
			{
				Response.Write("Attached is The Quotation Requested:");
			}
		%>
		<br/><br/>
		
		<%
		if((bool) Capa.Tables[1].Rows[0]["ImgParceiro"]) {
		%>
			<img src="<%Response.Write(WebPath);%>?nFormID=1210&nSubFormID=20100&nRegistroID=<%Response.Write(Capa.Tables[1].Rows[0]["ParceiroID"]);%>&nTamanho=0&nOrdem=1"/>
			
			<br/><br/>
		<%
		}
		%>
		
		<b>
			<font color="<%Response.Write(TitlesColor);%>">
				<%
					if (RelatorioID == 40133)
					{
						Response.Write(TranslateTherm("Cliente", IdiomaDeID, IdiomaParaID));
					} else {
						Response.Write(TranslateTherm("Faturar Para", IdiomaDeID, IdiomaParaID));
					}
				%>
			</font>
		</b>
		<br/>
		
		<table border="1" bordercolor="#DCDCDC" cellspacing="0" style="font-size:12pt">
			<tr>
				<td>
					<b><%Response.Write(Capa.Tables[1].Rows[0]["PessoaNome"]);%></b>
					<br/>
					
					<% Response.Write(Capa.Tables[1].Rows[0]["PessoaEndereco"]); %>
					<br/>
					
					<%
					if(Capa.Tables[1].Rows[0]["PessoaComplementoBairro"].ToString() != "") {
						Response.Write(Capa.Tables[1].Rows[0]["PessoaComplementoBairro"].ToString());
					}
					%>
					<br/>

					<%Response.Write(Capa.Tables[1].Rows[0]["PessoaCEPCidadeUF"].ToString());%>
					<br/>
					<%Response.Write(Capa.Tables[1].Rows[0]["PessoaPais"].ToString());%>
					<br/>
					<%Response.Write(Capa.Tables[1].Rows[0]["PessoaDocumento1"].ToString());%>
					&nbsp&nbsp&nbsp&nbsp
					<%Response.Write(Capa.Tables[1].Rows[0]["PessoaDocumento2"].ToString());%>
					<br/>
				</td>
			</tr>
		</table>
		<br/>

		<b>
			<font color="<%Response.Write(TitlesColor);%>">
				<%Response.Write(TranslateTherm(Report, IdiomaDeID, IdiomaParaID));%>
			</font>
		</b>
		<br/>
		
		<table border="1" bordercolor="#DCDCDC" cellspacing="0" style="font-size:12pt">
			<tr>
				<th> <%Response.Write(TranslateTherm(Report2, IdiomaDeID, IdiomaParaID));%></th>
				<th> <%Response.Write(TranslateTherm("Data", IdiomaDeID, IdiomaParaID));%></th>
				<th>
					<%Response.Write(TranslateTherm("Valor", IdiomaDeID, IdiomaParaID));%>
					&nbsp
					<%Response.Write(Capa.Tables[1].Rows[0]["MoedaPedido"].ToString());%>
				</th>
				
				<% if (IdiomaParaID == 246) { %>
				<th>
					<%Response.Write(TranslateTherm("Valor", IdiomaDeID, IdiomaParaID));%>
					&nbsp
					<%Response.Write(Capa.Tables[1].Rows[0]["MoedaConversao"].ToString());%>
				</th>
				<%}%>				

				<th> <%Response.Write(TranslateTherm("Previs�o Entrega", IdiomaDeID, IdiomaParaID));%> </th>
				<th> <%Response.Write(TranslateTherm("Validade", IdiomaDeID, IdiomaParaID));%> </th>
				<th> <%Response.Write(TranslateTherm("Frete", IdiomaDeID, IdiomaParaID));%> </th>

				<%if (RelatorioID == 40133) {%>
				<th>
					<%Response.Write(TranslateTherm("Seu Pedido", IdiomaDeID, IdiomaParaID));%>
				</th>
				<%}%>
			</tr>
			
			<tr>
				<td>
					<b> <%Response.Write(Capa.Tables[1].Rows[0]["PedidoID"].ToString());%> </b>
				</td>
				<td>
					<b> <%Response.Write(Capa.Tables[1].Rows[0]["dtPedido"].ToString());%> </b>
				</td>
				<td align="right">
					<b> <%Response.Write(Capa.Tables[1].Rows[0]["ValorTotalPedido"].ToString());%> </b>
				</td>

				<%if (IdiomaParaID == 246) {%>
					<th align="right">
						<%Response.Write(Capa.Tables[1].Rows[0]["ValorConvertido"].ToString());%>
					</th>
				<%}%>

				<td>
					<%Response.Write(Capa.Tables[1].Rows[0]["dtPrevisaoEntrega"].ToString());%>
				</td>
				<td>
					<%Response.Write(Validade);%> &nbsp 
					<%Response.Write(TranslateTherm("dias �teis", IdiomaDeID, IdiomaParaID));%>
				</td>
				<td>
					<%Response.Write(Capa.Tables[1].Rows[0]["Frete"].ToString());%>
				</td>
		
				<%if (RelatorioID == 40133) {%>
					<td>
						<%Response.Write(Capa.Tables[1].Rows[0]["SeuPedido"].ToString());%>
					</td>
				<%}%>
			</tr>
		</table>
		<br/>
		<b/>

		
		<% if (EhMicro) { %>
			<% for(int item = 0; item < Itens.Tables[1].Rows.Count; item++) { %>
				<% if ((bool) Itens.Tables[1].Rows[0]["EhGabinete"]) { %>

					<img src="<% Response.Write(WebPath);%>?nFormID=2110&nSubFormID=21000&nRegistroID=<% Response.Write(Itens.Tables[1].Rows[0]["ProdutoID"]); %>&nTamanho=0&nOrdem=1"/>
					<br>
					<font>
						&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
						<% Response.Write(Itens.Tables[1].Rows[0]["Modelo"]); %>
					</font><br/><br/>

				<% } %>
			<% } %>
		<% } %>

		<% if (EhMicro) { %>
			<% for(int item = 0; item < Itens.Tables[1].Rows.Count; item++) { %>
				<% if ((bool) Itens.Tables[1].Rows[0]["EhGabinete"]) { %>

					<img src="<% Response.Write(WebPath);%>?nFormID=2110&nSubFormID=21000&nRegistroID=<% Response.Write(Itens.Tables[1].Rows[0]["ProdutoID"]); %>&nTamanho=0&nOrdem=1"/>
					<br>
					<font>
						&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
						<% Response.Write(Itens.Tables[1].Rows[0]["Ncm"]); %>
					</font><br/><br/>

				<% } %>
			<% } %>
		<% } %>

		<font color="<% Response.Write(TitlesColor); %>">
			<% Response.Write(TranslateTherm("�tens", IdiomaDeID, IdiomaParaID)); %>
		</font>

		<table border="1" bordercolor="#DCDCDC" cellspacing="0" style="font-size:12pt">
			<tr>
				<th> # </th>
				<th> <% Response.Write(TranslateTherm("ID", IdiomaDeID, IdiomaParaID)); %> </th>
				<th> <% Response.Write(TranslateTherm("Produto", IdiomaDeID, IdiomaParaID)); %> </th>
				<%--<th> <% if (ExibeMarca) { Response.Write(TranslateTherm("Marca", IdiomaDeID, IdiomaParaID)); } %> </th>--%>
                <%--<th> <% if (EmpresaID == 7) { Response.Write(TranslateTherm("Serie", IdiomaDeID, IdiomaParaID)); } %> </th>--%>
				<%--<th> <% if (ExibeModelo) { Response.Write(TranslateTherm("Modelo", IdiomaDeID, IdiomaParaID)); } %> </th>--%>
				<th> <% if ((ExibeNcm) && (EmpresaID != 7)) { Response.Write(TranslateTherm("Ncm", IdiomaDeID, IdiomaParaID)); } %> </th>
				<%--<th> <% if (RelatorioID == 40132 || (RelatorioID == 40133 && EmpresaID == 7)) { Response.Write("Part Number"); } %> </th>--%>
				
				<th> <% Response.Write(TranslateTherm("Quant", IdiomaDeID, IdiomaParaID)); %> </th>
				<th> <% Response.Write(TranslateTherm("Unit�rio", IdiomaDeID, IdiomaParaID) + " " + Capa.Tables[1].Rows[0]["MoedaPedido"]); %> </th>
				<th> <% Response.Write(TranslateTherm("Total", IdiomaDeID, IdiomaParaID) + " " + Capa.Tables[1].Rows[0]["MoedaPedido"]); %> </th>

				<% if(IdiomaParaID == 246) { %>
						<% if(TransacaoID == 211 || TransacaoID == 215) { %>
						    <th> <% Response.Write(TranslateTherm("ISS", IdiomaDeID, IdiomaParaID)); %> </th>
                        
                        <% 
                            }
                        %>
                          
                        <%  else { %>
                                <th> <% Response.Write(TranslateTherm("ICMS", IdiomaDeID, IdiomaParaID)); %> </th>
                        <%
                            }
                        %>

						<th> <% Response.Write(TranslateTherm("IPI", IdiomaDeID, IdiomaParaID)); %> </th>
				<% 
						colSpanItens = 2;
					}
				%>
			</tr>

			<% for (itensCount = 0; itensCount < Itens.Tables[1].Rows.Count; itensCount++) { %>
				<tr>
					<td align="right"> <% Response.Write(Itens.Tables[1].Rows[itensCount]["Ordem"]); %> </td>
					
					<td align="right"> 
						<% Response.Write(Itens.Tables[1].Rows[itensCount]["ProdutoID"]); %> 
						
						<% if (InformacoesAdicionais) { %>
							<br/> <% Response.Write(Itens.Tables[1].Rows[itensCount]["CodigoFornecedor"]); %>
						<% } %>	
					</td>
					
					<td> <% Response.Write(Itens.Tables[1].Rows[itensCount]["Produto"]); %> </td>
					<%--<td> <% if (ExibeMarca) Response.Write(Itens.Tables[1].Rows[itensCount]["Marca"]); %> </td>--%>
                    <%--<td> <% if (EmpresaID == 7) Response.Write(Itens.Tables[1].Rows[itensCount]["Serie"]); %> </td>--%>
					<%--<td> <% if (ExibeModelo) Response.Write(Itens.Tables[1].Rows[itensCount]["Modelo"]); %> </td>--%>
					<td> <% if ((ExibeNcm) && (EmpresaID != 7)) Response.Write(Itens.Tables[1].Rows[itensCount]["Ncm"]); %> </td>
					<%--<td> <% if (RelatorioID == 40132 || (RelatorioID == 40133 && EmpresaID == 7)) Response.Write(Itens.Tables[1].Rows[itensCount]["PartNumber"]); %> </td>--%>
					
					<% if (!EhMicro || itensCount == 1) { %>
						<td align="right"> <% Response.Write(Itens.Tables[1].Rows[itensCount]["Quantidade"]); %> </td>
						<td align="right"> <% Response.Write(Itens.Tables[1].Rows[itensCount]["ValorUnitario"]); %> </td>
						<td align="right"> <% Response.Write(Itens.Tables[1].Rows[itensCount]["ValorTotal"]); %> </td>

						<%	if(IdiomaParaID == 246) { %>
								<td align="right"> <% Response.Write(Itens.Tables[1].Rows[itensCount]["ICMSISS"]); %> </td>
								<td align="right"> <% Response.Write(Itens.Tables[1].Rows[itensCount]["IPI"]); %> </td>
						<% 
								prazoGarantia = Itens.Tables[1].Rows[itensCount]["PrazoGarantia"].ToString();
							}
						} else {
					%>
						<td align="right"> 
							<% Response.Write((int) Itens.Tables[1].Rows[itensCount]["Quantidade"] / totMicros); %> )
						</td>
				
						<td colspan="<% Response.Write(2 + colSpanItens); %>" align=left>
							<% Response.Write(Itens.Tables[1].Rows[itensCount]["Descricao"]); %>
						</td>
					<% } %>
				</tr>
			<% } %>
			</table>
			<br/>

			<%
			if(Gare.Tables[1].Rows.Count > 0)
			{
			%>
			<b>
				<font color=" <% Response.Write(TitlesColor); %>">
					GARE (<% Response.Write(Mensagens.Tables[1].Rows[0]["ICMSSTMensagem2"]); %>)
				</font>
			</b>
			<br/>
			
			<table border="1" bordercolor="#DCDCDC" cellspacing="0" style="font-size:10pt">
				<tr>
					<th> <% Response.Write(TranslateTherm("ID", IdiomaDeID, IdiomaParaID)); %> </th>
					<th> <% Response.Write(TranslateTherm("Produto", IdiomaDeID, IdiomaParaID)); %> </th>
					<th> NCM </th>
					<th> IVA-ST </th>
					<th> <% Response.Write(TranslateTherm("Base", IdiomaDeID, IdiomaParaID)); %> </th>
					<th> ALQ-Inter </th>
					<th> ALQ-Intra </th>
					<th> IVA-ST Aj </th>
					<th> CFOP </th>
					<th> Quant </th>
					<th> <% Response.Write(TranslateTherm("Vl Total", IdiomaDeID, IdiomaParaID)); %> </th>
					<th> <% Response.Write(TranslateTherm("Vl Total c/ IPI", IdiomaDeID, IdiomaParaID)); %> </th>
					<th> BC ICMS </th>
					<th> ICMS </th>
					<th> BC ICMS-ST </th>
					<th> ICMS-ST </th>
				</tr>
				
				<%
				for (int st = 0; st < Gare.Tables[1].Rows.Count; st++) 
				{
				%>
					<tr>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["ID"].ToString()); %> </td>
						<td> <% Response.Write(Gare.Tables[1].Rows[st]["Produto"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["NCM"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["IVAST"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["BaseCalculo"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["ALQInter"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["ALQIntra"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["IVASTAj"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["CFOP"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["Quantidade"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["ValorTotal"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["ValorTotalComIPI"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["BCICMS"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["ICMS"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["BCICMSST"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gare.Tables[1].Rows[st]["ICMSST"].ToString()); %> </td>
					</tr>
				<%
				}
				%>
			</table>
			<br/>
				
			<table border="1" bordercolor="#DCDCDC" cellspacing="0" style="font-size:10pt">
				<td align="left"> &nbsp </td>
				<td align="left"> Legenda: </td>
				<td align="left"> IVA-ST Aj = [(1 + IVA-ST) * (1 - (ALQ-Inter * Base)) / (1 - (ALQ-Intra * Base)] - 1 </td>
				<td align="left"> BC ICMS-ST = [(Vl Total c/ IPI) * Base * (1 + IVA-ST Aj)] </td>
				<td align="left"> ICMS-ST = [(BC ICMS-ST) * (ALQ-Intra)] - ICMS </td>
			</table>
			<br/>
			<%
			}
			%>
			
			<%
			if(Gnre.Tables[1].Rows.Count > 0)
			{
			%>
			<b>
				<font color=" <% Response.Write(TitlesColor); %>">
					GNRE (<% Response.Write(Mensagens.Tables[1].Rows[0]["ICMSSTMensagem3"]); %>)
				</font>
			</b>
			<br/>
			
			<table border="1" bordercolor="#DCDCDC" cellspacing="0" style="font-size:10pt">
				<tr>
					<th> <% Response.Write(TranslateTherm("ID", IdiomaDeID, IdiomaParaID)); %> </th>
					<th> <% Response.Write(TranslateTherm("Produto", IdiomaDeID, IdiomaParaID)); %> </th>
					<th> NCM </th>
					<th> IVA-ST </th>
					<th> <% Response.Write(TranslateTherm("Base", IdiomaDeID, IdiomaParaID)); %> </th>
					<th> ALQ-Inter </th>
					<th> ALQ-Intra </th>
					<th> IVA-ST Aj </th>
					<th> CFOP </th>
					<th> Quant </th>
					<th> <% Response.Write(TranslateTherm("Vl Total", IdiomaDeID, IdiomaParaID)); %> </th>
					<th> <% Response.Write(TranslateTherm("Vl Total c/ IPI", IdiomaDeID, IdiomaParaID)); %> </th>
					<th> BC ICMS </th>
					<th> ICMS </th>
					<th> BC ICMS-ST </th>
					<th> ICMS-ST </th>
				</tr>
				
				<%
					for (int st = 0; st < Gnre.Tables[1].Rows.Count; st++) 
				{
				%>
					<tr>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["ID"].ToString()); %> </td>
						<td> <% Response.Write(Gnre.Tables[1].Rows[st]["Produto"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["NCM"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["IVAST"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["BaseCalculo"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["ALQInter"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["ALQIntra"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["IVASTAj"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["CFOP"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["Quantidade"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["ValorTotal"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["ValorTotalComIPI"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["BCICMS"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["ICMS"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["BCICMSST"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Gnre.Tables[1].Rows[st]["ICMSST"].ToString()); %> </td>
					</tr>
				<%
				}
				%>
			</table>
			<br/>
				
			<table border="1" bordercolor="#DCDCDC" cellspacing="0" style="font-size:10pt">
				<td align="left"> &nbsp </td>
				<td align="left"> Legenda: </td>
				<td align="left"> IVA-ST Aj = [(1 + IVA-ST) * (1 - (ALQ-Inter * Base)) / (1 - (ALQ-Intra * Base)] - 1 </td>
				<td align="left"> BC ICMS-ST = [(Vl Total c/ IPI) * Base * (1 + IVA-ST Aj)] </td>
				<td align="left"> ICMS-ST = [(BC ICMS-ST) * (ALQ-Intra)] - ICMS </td>
			</table>
			<br/>
			<%
			}
			%>

			<b>
				<font color=" <% Response.Write(TitlesColor); %>">
					<% Response.Write(TranslateTherm("Pagamento", IdiomaDeID, IdiomaParaID)); %>
				</font>
			</b>
			<br/>
			
			<table border="1" bordercolor="#DCDCDC" cellspacing="0" style="font-size:12pt">
				<tr>
					<th>#</th>
					<% if (SUP) { %> <th>Tipo</th> <% } %>

					<th> <% Response.Write(TranslateTherm("Forma", IdiomaDeID, IdiomaParaID)); %> </th>
					<th> <% Response.Write(TranslateTherm("Prazo", IdiomaDeID, IdiomaParaID)); %> </th>
					<th>
						<%
							Response.Write(TranslateTherm("Valor", IdiomaDeID, IdiomaParaID));
							Response.Write(Capa.Tables[1].Rows[0]["MoedaPedido"]);
						%>
					</th>
				</tr>
				
				<tr>
				<% for (int parcelas = 0; parcelas < Parcelas.Tables[1].Rows.Count; parcelas++) { %>
					<td align="right"> <% Response.Write(Parcelas.Tables[1].Rows[parcelas]["Ordem"].ToString()); %> </td>

					<% if(SUP) { %>
						<td align=center> <% Response.Write(Parcelas.Tables[1].Rows[parcelas]["Tipo"].ToString()); %> </td>
					<% } %>

						<td> <% Response.Write(Parcelas.Tables[1].Rows[parcelas]["Forma"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Parcelas.Tables[1].Rows[parcelas]["Prazo"].ToString()); %> </td>
						<td align="right"> <% Response.Write(Parcelas.Tables[1].Rows[parcelas]["Valor"].ToString()); %> </td>
					</tr>
					
					<%
					if ((int) Parcelas.Tables[1].Rows[parcelas]["FormaPagamentoID"] == 1033) {
						temPagamentoCCC = true;
					}
					%>
				<% } %>
			</table>
			<br/>

			<% if (Contas.Tables[1].Rows.Count > 0) { %>
				<b>
				<font color=" <% Response.Write(TitlesColor); %>"> <% Response.Write(ContasBancarias); %> </font></b><br>
				<table border="1" bordercolor="#DCDCDC" cellspacing="0" style="font-size:12pt">
					<% if(IdiomaParaID == 246) { %>
						<tr>
							<th>&nbsp</th>
							<th> Banco </th>
							<th> C�digo </th>
							<th> Ag�ncia </th>
							<th> Conta </th>
							<th> Correntista </th>
							<th> CNPJ </th></tr>
					<% } else { %>
						<tr>
							<th>&nbsp</th>
							<th> Bank </th>
							<th> Code </th>
							<th> Branch </th>
							<th> Account </th>
							<th> Holder </th></tr>
					<% } %>

					<% for(int cc = 0; cc < Contas.Tables[1].Rows.Count; cc++) { %>
						<tr>
							<td>
								<% if((bool) Contas.Tables[1].Rows[cc]["TemImagem"]) { %>
									<img 
										width="18" 
										height="18"
										alt=""  
										src="<%Response.Write(WebPath);%>?nFormID=9220&nSubFormID=28230&nRegistroID=<%Response.Write(Contas.Tables[1].Rows[cc]["BancoID"]);%>&nTamanho=0&nOrdem=1"/>
								<% } else { %>
									&nbsp
								<% } %>
							</td>
						
							<td> <%Response.Write(Contas.Tables[1].Rows[cc]["Banco"]); %> </td>
							<td> <%Response.Write(Contas.Tables[1].Rows[cc]["CodigoBanco"]); %> </td>
							<td> <%Response.Write(Contas.Tables[1].Rows[cc]["CodigoAgencia"]); %> </td>
							<td> <%Response.Write(Contas.Tables[1].Rows[cc]["ContaCorrente"]); %> </td>
							<td> <%Response.Write(Contas.Tables[1].Rows[cc]["Correntista"]); %> </td>
							<%if (IdiomaParaID == 246) {%>
                                <td> <%Response.Write(Contas.Tables[1].Rows[cc]["CNPJ"]); %> </td></tr>
		                    <%}%>
					<% } %>
				</table>
			<br/>
		<% } %>
		
		<% if (!EhMicro && Observacoes.Equals("1")) { %>
			<b>
				<font color="<%Response.Write(TitlesColor);%>">
					<% Response.Write(TranslateTherm("Outras Condi��es", IdiomaDeID, IdiomaParaID)); %>
				</font>
			</b>
			
			<table border="1" bordercolor="#DCDCDC" cellspacing="0" style="font-size:12pt">
				<tr>
					<td>
						<% Response.Write(Observacoes.Replace("\n", "<br>")); %>
					</td>
				</tr>
			</table>
			<br/>
		<% } else if(prazoGarantia != null && !prazoGarantia.Equals("")) { %>
			<b>
				<font color="<%Response.Write(TitlesColor);%>"> 
					<% Response.Write(TranslateTherm("Outras Condi��es", IdiomaDeID, IdiomaParaID)); %>
				</font>
			</b>
			
			<br/>
			<table border="1" bordercolor="#DCDCDC" cellspacing="0" style="font-size:12pt">
				<tr>
					<td>
						<br/>
						<%
						Response.Write(prazoGarantia);

						if(Observacoes.Equals("1"))
						{
							Observacoes.Replace("\n", "<BR>");
						}
						%>
					</td>
				</tr>
			</table>
			<br/>
		<% } %>
		
		<b>
			<font color="<%Response.Write(TitlesColor);%>">
				<%Response.Write(TranslateTherm("Ficha T�cnica", IdiomaDeID, IdiomaParaID));%>
			</font>
		</b>
		
		<br>
		<table border="1" bordercolor="#DCDCDC" cellspacing="0" style="font-size:12pt">
			<tr>
				<th>#</th>
				<th><% Response.Write(TranslateTherm("ID", IdiomaDeID, IdiomaParaID));%></th>
				<th colspan="3"><% Response.Write(TranslateTherm("Produto", IdiomaDeID, IdiomaParaID));%></th>

<%--
				<th><% Response.Write(TranslateTherm("Descri��o", IdiomaDeID, IdiomaParaID));%></th>
				<th><% Response.Write(TranslateTherm("Part Number", IdiomaDeID, IdiomaParaID));%></th>
--%>

			</tr>
			
			<% for (int ficha1 = 0; FichaTecnica1 != null && ficha1 < FichaTecnica1.Tables[1].Rows.Count; ficha1++) { %>
				<tr>
					<td><b> <% Response.Write(FichaTecnica1.Tables[1].Rows[ficha1]["Ordem"]); %> </b></td>
					<td><b> <% Response.Write(FichaTecnica1.Tables[1].Rows[ficha1]["ProdutoID"]); %> </b></td>
					<td colspan="3"><b> <% Response.Write(FichaTecnica1.Tables[1].Rows[ficha1]["Produto"]); %> </b></td>
<%--
					<td><b> <% Response.Write(FichaTecnica1.Tables[1].Rows[ficha1]["Descricao"]); %> </b></td>
					<td><b> <% Response.Write(FichaTecnica1.Tables[1].Rows[ficha1]["PartNumber"]); %> </b></td>
--%>
				</tr>
				
				<tr style="font-size:10pt">
					<td colspan="3">
						<% if(FichaTecnica1.Tables[1].Rows[ficha1]["Pesos"] != null) Response.Write(Pesos); %> <br/>
						<% if (FichaTecnica1.Tables[1].Rows[ficha1]["Dimensoes"] != null) Response.Write(Dimensoes); %> <br/>
						<% if (FichaTecnica1.Tables[1].Rows[ficha1]["Prazos"] != null) Response.Write(Prazos); %> <br/>
						
						<%
							for(int ficha2 = 0; ficha2 < FichaTecnica2.Tables[1].Rows.Count; ficha2++)
							{
                                string prod1 = FichaTecnica2.Tables[1].Rows[ficha2]["ProdutoID"].ToString();
                                string prod2 = FichaTecnica2.Tables[1].Rows[ficha2]["ProdutoID"].ToString();
                                string prod3 = FichaTecnica1.Tables[1].Rows[ficha1]["ProdutoID"].ToString();

                                if ((!prod2.Equals(prod1)) || (!prod1.Equals(prod3))) continue;

                                Response.Write(FichaTecnica2.Tables[1].Rows[ficha2]["Caracteristica"] + "<br/>");                                
							}
						%>
					</td>
					<td>
						<% if (FichaTecnica1.Tables[1].Rows[ficha1]["Pesos"] != null) Response.Write(FichaTecnica1.Tables[1].Rows[ficha1]["Pesos"]); %> <br/>
						<% if (FichaTecnica1.Tables[1].Rows[ficha1]["Dimensoes"] != null) Response.Write(FichaTecnica1.Tables[1].Rows[ficha1]["Dimensoes"]); %> <br/>
						<% if (FichaTecnica1.Tables[1].Rows[ficha1]["Prazos"] != null) Response.Write(FichaTecnica1.Tables[1].Rows[ficha1]["Prazos"]); %> <br/>
						
						<%
							for(int ficha2 = 0; ficha2 < FichaTecnica2.Tables[1].Rows.Count; ficha2++)
							{
								string prod1 = FichaTecnica2.Tables[1].Rows[ficha2]["ProdutoID"].ToString();
                                string prod2 = FichaTecnica1.Tables[1].Rows[ficha1]["ProdutoID"].ToString();

                                if ((FichaTecnica2.Tables[1].Rows[ficha2]["Valor"] != null) && (prod1.Equals(prod2)))
								{
									Response.Write(FichaTecnica2.Tables[1].Rows[ficha2]["Valor"] + "<br/>");
								}
							}
						%>
					</td>
					<td>
						<%
						if ((bool)FichaTecnica1.Tables[1].Rows[ficha1]["ImgProduto"])
						{
						%>
							<img alt="" src="<%Response.Write(WebPath);%>?nFormID=2110&nSubFormID=21000&nRegistroID=<%Response.Write(FichaTecnica1.Tables[1].Rows[ficha1]["ProdutoID"]);%>&nTamanho=0&nOrdem=1"/>
						<%
						}
						else if ((bool)FichaTecnica1.Tables[1].Rows[ficha1]["ImgMarca"])
						{
						%>
							<img alt="" src="<%Response.Write(WebPath);%>?nFormID=2110&nSubFormID=21000&nRegistroID=<%Response.Write(FichaTecnica1.Tables[1].Rows[ficha1]["MarcaID"]);%>&nTamanho=0&nOrdem=1"/>
						<%
						}
						else
						{
						%>
							&nbsp
						<%
						}
						%>
					</td>
				</tr>
			<% } %>
		</table>
		<br/>

		<%
		if(RelatorioID == 40133) {
		%>		
			<b>
				<font color="<%Response.Write(TitlesColor);%>">Internet</FONT>
			</b>
			<br/>
			
			<table border="1" bordercolor="#DCDCDC" cellspacing="0" style="font-size:12pt">
				<tr>
					<td>
						� <a href="<%Response.Write(site);%>/alcateiav3/web/aspx/PoliticaEmpresa.aspx"> Confira as Pol�ticas da Empresa </a>.
					</td>
				</tr>
			</table>
			<br/>		
		<%
		}
		%>

		<br/>
		<%
			if (IdiomaParaID == 246)
			{
                Response.Write(" � Validade da proposta : 1 dia ");
			}
		%>


        <%
			if (IdiomaParaID == 246)
			{
        %>
            <br/>

              <%Response.Write(" � Prazo de pagamento : mediante a an�lise de cr�dito");%>
        
            <br/><br/>
        <%
			}
		%>
        
		<%Response.Write(TranslateTherm("Atenciosamente", IdiomaDeID, IdiomaParaID));%>,
		<br/><br/>
		<b><%Response.Write(Capa.Tables[1].Rows[0]["ColaboradorFantasia"]);%></b>
		<br/>
		<b><%Response.Write(Capa.Tables[1].Rows[0]["ColaboradorTelefones"]);%></b>
		<br/>

		<%
			if((bool) Capa.Tables[1].Rows[0]["ImgEmpresa"])
			{
		%>
				<a href="<%Response.Write(Site);%>"> 
					<img src="<%Response.Write(WebPath);%>?nFormID=1210&nSubFormID=20100&nRegistroID=<%Response.Write(EmpresaID);%>&nTamanho=0&nOrdem=1" border="0"/>
				</a>
				<br/><br/>
		<%
			}
		%>
	</body>
</html>