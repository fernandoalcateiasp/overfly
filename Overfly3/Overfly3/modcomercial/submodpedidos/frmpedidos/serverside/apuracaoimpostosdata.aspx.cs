using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.Attributes;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class apuracaoimpostosdata : System.Web.UI.OverflyPage
	{
		private Integer empresaID;
		private Integer impostoID;
        private string diferencialAliquotas;
		private Integer CFOPID;
		private string dataInicio;
		private string dataFim;
		private string filtro;
        private string uf;
		private Integer tipoResultado;

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(PedidoApuracaoImpostos());
		}

		protected DataSet PedidoApuracaoImpostos()
		{
			// Roda a procedure sp_Pedido_ApuracaoImpostos
			ProcedureParameters[] procParams = new ProcedureParameters[9];

			procParams[0] = new ProcedureParameters(
				"@EmpresaID",
				System.Data.SqlDbType.Int,
                empresaID != null ? (Object)empresaID.ToString() : System.DBNull.Value);

            procParams[1] = new ProcedureParameters(
				"@ImpostoID",
				System.Data.SqlDbType.Int,
                impostoID.intValue() != 0 ? (Object)impostoID.ToString() : System.DBNull.Value);

            procParams[2] = new ProcedureParameters(
                "@DiferencialAliquotas",
				System.Data.SqlDbType.Bit,
                diferencialAliquotas == null || diferencialAliquotas.Length == 0 ?
                false : (Object)(diferencialAliquotas == "1"));            

			procParams[3] = new ProcedureParameters(
				"@CFOPID",
				System.Data.SqlDbType.Int,
                CFOPID.intValue() != 0 ? (Object)CFOPID.ToString() : System.DBNull.Value);

            procParams[4] = new ProcedureParameters(
                "@UF",
                 System.Data.SqlDbType.VarChar,
                 uf != "" ? (Object)uf.ToString() : System.DBNull.Value);

            procParams[5] = new ProcedureParameters(
                "@dtInicio",
                System.Data.SqlDbType.DateTime,
                dataInicio != null ? (Object)dataInicio.ToString() : System.DBNull.Value);

			procParams[6] = new ProcedureParameters(
				"@dtFim",
				System.Data.SqlDbType.DateTime,
                dataFim != null ? (Object)dataFim.ToString() : System.DBNull.Value);

			procParams[7] = new ProcedureParameters(
				"@Filtro",
				System.Data.SqlDbType.VarChar,
				filtro != "" ? (Object) filtro.ToString(): System.DBNull.Value);

            procParams[8] = new ProcedureParameters(
                "@TipoResultado",
                System.Data.SqlDbType.Int,
                tipoResultado.intValue() != 0 ? (Object)tipoResultado.ToString() : System.DBNull.Value);

			return DataInterfaceObj.execQueryProcedure(
				"sp_Pedido_ApuracaoImpostos",
				procParams);
		}

		protected Integer nEmpresaID
		{
			get { return empresaID; }
			set { empresaID = (value != null ? value : new Integer(0)); }
		}

        protected Integer nImpostoID
		{
			get { return impostoID; }
            set { impostoID = (value != null ? value : new Integer(0)); }
		}

        protected string sDiferencialAliquotas
		{
			get { return diferencialAliquotas; }
			set { diferencialAliquotas = (value != null ? value : "0"); }
		}

        protected Integer nCFOPID
		{
			get { return CFOPID; }
            set { CFOPID = (value != null ? value : new Integer(0)); }
		}

        protected string sDataInicio
		{
			get { return dataInicio; }
			set { dataInicio = (value != null ? value : ""); }
		}

        protected string sDataFim
		{
			get { return dataFim; }
			set { dataFim = (value != null ? value : ""); }
		}
        protected string sUF
        {
            get { return uf; }
            set { uf = (value != null ? value : ""); }
        }

        protected string sFiltro
		{
			get { return filtro; }
            set { filtro = (value != null ? value : ""); }
		}

        protected Integer nTipoResultado
		{
			get { return tipoResultado; }
			set { tipoResultado = (value != null ? value : new Integer(0)); }
		}
	}
}
