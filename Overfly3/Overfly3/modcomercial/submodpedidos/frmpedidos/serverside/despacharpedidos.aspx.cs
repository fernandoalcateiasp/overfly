using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class despacharpedidos : System.Web.UI.OverflyPage
	{
		private Integer zero = new Integer(0);
		private java.lang.Boolean False = new java.lang.Boolean(false);
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(PedidoDespacho());
		}

		protected DataSet PedidoDespacho()
		{
			// Roda a procedure sp_Pedido_Despacho
			ProcedureParameters[] procParams = new ProcedureParameters[10];

			procParams[0] = new ProcedureParameters(
				"@Pedidos",
				System.Data.SqlDbType.VarChar,
                pedidoID != null ? (Object)pedidoID.ToString() : System.DBNull.Value);

			procParams[1] = new ProcedureParameters(
				"@UsuarioID",
				System.Data.SqlDbType.Int,
                userID != null ? (Object)userID.ToString() : System.DBNull.Value);

			procParams[2] = new ProcedureParameters(
				"@EhTransportadora",
				System.Data.SqlDbType.Bit,
                transportadora.booleanValue() ? 1 : 0);

			procParams[3] = new ProcedureParameters(
				"@EhDespacho",
                System.Data.SqlDbType.Bit,
				despacho.booleanValue() ? 1 : 0);

            procParams[4] = new ProcedureParameters(
                "@Portador",
                System.Data.SqlDbType.VarChar,
                portador != null ? (Object)portador.ToString() : System.DBNull.Value);

			procParams[5] = new ProcedureParameters(
				"@DocumentoPortador",
				System.Data.SqlDbType.VarChar,
				documentoEstadualPortador != null ? (Object)documentoEstadualPortador.ToString() : System.DBNull.Value);

            procParams[6] = new ProcedureParameters(
                "@DocumentoFederalPortador",
                System.Data.SqlDbType.VarChar,
                documentoFederalPortador != null ? (Object)documentoFederalPortador.ToString() : System.DBNull.Value);
		
			procParams[7] = new ProcedureParameters(
				"@Conhecimento",
				System.Data.SqlDbType.VarChar,
				conhecimento != null ? (Object)conhecimento.ToString() : System.DBNull.Value);

			procParams[8] = new ProcedureParameters(
				"@MinutaID",
				System.Data.SqlDbType.VarChar,
				DBNull.Value,
				ParameterDirection.InputOutput);
            procParams[8].Length = 8000;

			procParams[9] = new ProcedureParameters(
				"@Resultado",
				System.Data.SqlDbType.VarChar,
				DBNull.Value,
				ParameterDirection.InputOutput);
			procParams[9].Length = 8000;

			DataInterfaceObj.execNonQueryProcedure(
                "sp_Pedido_Despacho",
				procParams);

			return DataInterfaceObj.getRemoteData(
				"select " +
				((procParams[8].Data != DBNull.Value) ? "'" + procParams[8].Data.ToString() + "'" : "''") +
					" as Minuta, " +
				((procParams[9].Data != DBNull.Value) ? "'" + procParams[9].Data.ToString() + "'" : "''") +
					" as Resultado"
			);
		}		
	
        private Integer userID;
        protected Integer nUserID
        {
            get { return userID; }
            set { userID = value != null ? value : zero; }
        }

		private java.lang.Boolean despacho;
		protected java.lang.Boolean nDespacho
		{
			get { return despacho; }
			set { despacho = value != null ? value : False; }
		}
		
		private java.lang.Boolean transportadora;
		protected java.lang.Boolean nTransportadora
		{
			get { return transportadora; }
			set { transportadora = value != null ? value : False; }
		}
		
		private string portador;
		protected string sPortador
		{
			get { return portador; }
			set { portador = value != null ? value : ""; }
		}

		private string documentoEstadualPortador;
		protected string sDocumentoEstadualPortador
		{
			get { return documentoEstadualPortador; }
			set { documentoEstadualPortador = value != null ? value : ""; }
		}
		
		private string documentoFederalPortador;
		protected string sDocumentoFederalPortador
		{
			get { return documentoFederalPortador; }
			set { documentoFederalPortador = value != null ? value : ""; }
		}
		
		private string conhecimento;
		protected string sConhecimento
		{
			get { return conhecimento; }
			set { conhecimento = value != null ? value : ""; }
		}
		
		private string pedidoID;
		protected string nPedidoID
		{
			get { return pedidoID; }
			set { pedidoID = value != null ? value : ""; }
		}
		
	}
}
