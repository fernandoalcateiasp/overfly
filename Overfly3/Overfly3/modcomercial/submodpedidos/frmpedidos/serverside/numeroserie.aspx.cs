using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class numeroserie : System.Web.UI.OverflyPage
	{
		private static Integer zero = new Integer(0);
		private static Integer[] emptyInt = new Integer[0];		
		private static string[] emptyStr = new string[0];		
		private static java.lang.Boolean falso = new java.lang.Boolean(false);
		
		private Integer[] recNo;
		private Integer[] pedidoId;
        private Integer dataLen;
		private Integer ordemProducaoId;
		private Integer[] etiquetaPropria;
		private Integer[] quantidade;
		private string[] produto;
		private java.lang.Boolean desgatilha;
		private Integer[] userID;
		private Integer caixaGeradaId;
		private Integer[] produtoId;
		private Integer[] caixaId;
		private string[] numeroSerie;
        private string QuantidadeGatilhada;
        private string QuantidadeDesGatilhada;
        private string StringErro;
        private string Numeroserie;
        private string ProdutoID;
        private string CaixaID;
		
		public Integer[] nRN
		{
			get { return recNo; }
			set { recNo = value != null ? value : emptyInt; }
		}
		
		public Integer[] nPedID
		{
			get { return pedidoId; }
            set { pedidoId = value != null ? value : emptyInt; }
		}
		
		public Integer nOPID
		{
			get { return ordemProducaoId; }
			set
			{
				if(value == null || value.intValue() == 0)
				{
					ordemProducaoId = null;
				}
				else
				{
					ordemProducaoId = value;
				}
			}
		}

		public Integer[] nTN
		{
			get { return etiquetaPropria; }
			set { etiquetaPropria = value != null ? value : emptyInt; }
		}

		public Integer[] nQ
		{
			get { return quantidade; }
			set { quantidade = value != null ? value : emptyInt; }
		}

		public string[] sP
		{
			get { return produto; }
			set { produto = value != null ? value : emptyStr; }
		}
		
		public java.lang.Boolean nD
		{
			get { return desgatilha; }
			set { desgatilha = value != null ? value : falso; }
		}

		public Integer[] nUID
		{
			get { return userID; }
            set { userID = value != null ? value : emptyInt; }
		}

		public Integer nCaixaGeradaID
		{
			get { return caixaGeradaId; }
			set { caixaGeradaId = value != null ? value : zero; }
		}

		public Integer[] nPID
		{
			get { return produtoId; }
			set { produtoId = value != null ? value : emptyInt; }
		}

		public Integer[] nCID
		{
			get { return caixaId; }
			set { caixaId = value != null ? value : emptyInt; }
		}

        protected Integer nDataLen
        {
            get { return dataLen; }
            set { dataLen = value != null ? value : zero; }
        }

		public string[] sNS
		{
			get { return numeroSerie; }
			set { numeroSerie = value != null ? value : emptyStr; }
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					GetSql()
				)
			);
		}

        protected string GetSql()
        {
            string sql = "";            
            int Desga = (desgatilha.booleanValue() ? 1 : 0);
            
            if (Desga == 0)
            {
                ProcedureParameters[] procGatilha = new ProcedureParameters[11];
                for (int i = 0; i < dataLen.intValue(); i++)
                {

                    procGatilha[0] = new ProcedureParameters(
                        "@PedidoID",
                        System.Data.SqlDbType.Int,
                        pedidoId[i]);

                    procGatilha[1] = new ProcedureParameters(
                        "@OrdemProducaoID",
                        System.Data.SqlDbType.Int,
                        ordemProducaoId == null ? System.DBNull.Value : (Object)ordemProducaoId.ToString());

                    procGatilha[2] = new ProcedureParameters(
                        "@Quantidade",
                        System.Data.SqlDbType.Int,
                        quantidade[i]);

                    procGatilha[3] = new ProcedureParameters(
                        "@Identificador",
                        System.Data.SqlDbType.VarChar,
                        produto[i]);

                    procGatilha[4] = new ProcedureParameters(
                        "@EP",
                        System.Data.SqlDbType.Bit,
                        etiquetaPropria[i]);

                    procGatilha[5] = new ProcedureParameters(   
                        "@UsuarioID",
                        System.Data.SqlDbType.Int,
                        userID[i]);

                    procGatilha[6] = new ProcedureParameters(
                        "@NumeroSerie",
                        System.Data.SqlDbType.VarChar,
                        numeroSerie[i],
                        ParameterDirection.InputOutput);

                    procGatilha[7] = new ProcedureParameters(
                      "@ProdutoID",
                      System.Data.SqlDbType.Int,
                      produtoId[i],
                      ParameterDirection.InputOutput);

                    procGatilha[8] = new ProcedureParameters(
                      "@CaixaID",
                      System.Data.SqlDbType.Int,
                      caixaGeradaId != null ? caixaGeradaId.ToString() : (caixaId[i].intValue() != -1 ? (Object)caixaId[i].ToString() : System.DBNull.Value),
                        //System.DBNull.Value,
                      ParameterDirection.InputOutput);

                    procGatilha[9] = new ProcedureParameters(
                      "@QuantidadeGatilhada",
                      System.Data.SqlDbType.Int,
                      DBNull.Value,
                      ParameterDirection.InputOutput);

                    procGatilha[10] = new ProcedureParameters(
                        "@StringErro",
                        System.Data.SqlDbType.VarChar,
                        DBNull.Value,
                        ParameterDirection.InputOutput);
                    procGatilha[10].Length = 8000;

                    DataInterfaceObj.execNonQueryProcedure(
                        "sp_Pedido_Gatilha",
                        procGatilha);

                    if (procGatilha[6].Data != DBNull.Value)
                        Numeroserie = procGatilha[6].Data.ToString();

                    if (procGatilha[7].Data != DBNull.Value)
                        ProdutoID = procGatilha[7].Data.ToString();

                    if (procGatilha[8].Data != DBNull.Value)
                    {
                        CaixaID = procGatilha[8].Data.ToString();                        
                        caixaGeradaId = new Integer(int.Parse(procGatilha[8].Data.ToString()));
                    }
                    if (procGatilha[9].Data != DBNull.Value)
                        QuantidadeGatilhada = procGatilha[9].Data.ToString();

                    if (procGatilha[10].Data != DBNull.Value)
                        StringErro = procGatilha[10].Data.ToString();


                    sql += (sql == "" ? "SELECT" : "UNION SELECT") + " " +
                        (QuantidadeGatilhada != null ? ( "" + QuantidadeGatilhada + " as QuantidadeGatilhada, ") : " ") +
                        (StringErro != null ? ("'" + StringErro + "' as Resultado, ") : " ") +
                        (recNo[i] != null ? ("' " + recNo[i] + "' as RecNo, ") : " ") +
                        (Numeroserie != null ? ("' " + Numeroserie + "' as NumeroSerie, ") : " ") +
                        (CaixaID != null ? ("' " + CaixaID + "' as CaixaID, ") : " NULL as CaixaID, ") +
                        (ProdutoID != null ? ("' " + ProdutoID + "' as ProdutoID ") : " NULL as ProdutoID ");

                }
            }
            else
            {
                ProcedureParameters[] procDesGatilha = new ProcedureParameters[10];
                for (int i = 0; i < dataLen.intValue(); i++)
                {

                    procDesGatilha[0] = new ProcedureParameters(
                        "@PedidoID",
                        System.Data.SqlDbType.Int,
                        pedidoId[i]);

                    procDesGatilha[1] = new ProcedureParameters(
                        "@OrdemProducaoID",
                        System.Data.SqlDbType.Int,
                        ordemProducaoId == null ? System.DBNull.Value : (Object)ordemProducaoId.ToString());

                    procDesGatilha[2] = new ProcedureParameters(
                        "@Quantidade",
                        System.Data.SqlDbType.Int,
                        quantidade[i]);

                    procDesGatilha[3] = new ProcedureParameters(
                        "@Identificador",
                        System.Data.SqlDbType.VarChar,
                        produto[i]);

                    procDesGatilha[4] = new ProcedureParameters(
                        "@EP",
                        System.Data.SqlDbType.Bit,
                        etiquetaPropria[i]);

                    procDesGatilha[5] = new ProcedureParameters(
                        "@NumeroSerie",
                        System.Data.SqlDbType.VarChar,
                        numeroSerie[i]);

                    procDesGatilha[6] = new ProcedureParameters(
                        "@ProdutoID",
                        System.Data.SqlDbType.Int,
                        produtoId[i]);

                    procDesGatilha[7] = new ProcedureParameters(
                        "@CaixaID",
                        System.Data.SqlDbType.Int,
                        caixaGeradaId != null ? caixaGeradaId.ToString() : (caixaId[i].intValue() != -1 ? (Object)caixaId[i].ToString() : System.DBNull.Value));

                    procDesGatilha[8] = new ProcedureParameters(
                        "@QuantidadeDesgatilhada",
                        System.Data.SqlDbType.Int,
                        DBNull.Value);

                    procDesGatilha[9] = new ProcedureParameters(
                        "@StringErro",
                        System.Data.SqlDbType.VarChar,
                        DBNull.Value,
                        ParameterDirection.InputOutput);
                    procDesGatilha[9].Length = 8000;

                    DataInterfaceObj.execNonQueryProcedure(
                        "sp_Pedido_Desgatilha",
                        procDesGatilha);

                     if (procDesGatilha[5].Data != DBNull.Value)
                        Numeroserie = procDesGatilha[5].Data.ToString();

                    if (procDesGatilha[6].Data != DBNull.Value)
                        ProdutoID = procDesGatilha[6].Data.ToString();

                    if (procDesGatilha[9].Data != DBNull.Value)
                        StringErro = procDesGatilha[9].Data.ToString();

                    sql += (sql == "" ? "SELECT" : "UNION SELECT") + " " +
                        (" NULL as QuantidadeGatilhada, ") +
                        (StringErro != null ? ("NULL as Resultado, ") : " ") +
                        ("NULL as RecNo, ") +
                          (Numeroserie != null ? ("' " + Numeroserie + "' as NumeroSerie, ") : " ") +
                        ("NULL as CaixaID, ") +
                        (ProdutoID != null ? ("' " + ProdutoID + "' as ProdutoID ") : " ") + ", '' AS CaixaID ";

                }

            }
            return sql;
        }
	}
}
