using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class fillparcelas : System.Web.UI.OverflyPage
	{
		private Integer pedidoId;
        private Integer formapagamentoID;
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(PedidoParcelas());
		}

		// Roda a procedure sp_Pedido_Parcelas
		private DataSet PedidoParcelas()
		{
			ProcedureParameters[] procParams = new ProcedureParameters[2];

			procParams[0] = new ProcedureParameters(
				"@PedidoID",
				System.Data.SqlDbType.Int,
				pedidoId != null ? (Object) pedidoId.ToString() : new Integer(0));

            procParams[1] = new ProcedureParameters(
                "@FormaPagamentoID",
                System.Data.SqlDbType.Int,
                (formapagamentoID == null || formapagamentoID.ToString() == "NULL" || formapagamentoID.intValue() == 0) ? System.DBNull.Value : (Object)formapagamentoID.ToString());

			DataInterfaceObj.execNonQueryProcedure(
				"sp_Pedido_Parcelas",
				procParams);

			return DataInterfaceObj.getRemoteData(
				"select 0 as fldresp"
			);
		}
		
		protected Integer nPedidoID
		{
			get { return pedidoId; }
			set { pedidoId = value != null ? value : new Integer(0); }
		}

        protected Integer nFormaPagamentoID
        {
            get { return formapagamentoID; }
            set { formapagamentoID = value != null ? value : new Integer(0); }
        }
	}
}
