using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class dadosnotafiscal : System.Web.UI.OverflyPage
	{
		private Integer pedidoID;
		private Integer emissor;
		private Integer atualizaData;
        private Integer documentoFiscalID;
		private string notaFiscal; 
		private Integer numeroVolumes;
		private string dtFaturamento;
        private string chaveAcesso;
        private string serie;
        private string strSql;

		private Integer zero = new Integer(0);
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(Ocorrencias());
		}

		protected DataSet Ocorrencias()
		{
			if (numeroVolumes.intValue() > 0)
            {
                strSql = "UPDATE Pedidos SET NumeroVolumes = " + (numeroVolumes != null ? (Object)numeroVolumes.ToString() : System.DBNull.Value) + " " +
                            "WHERE PedidoID = " + (pedidoID != null ? (Object)pedidoID.ToString() : System.DBNull.Value);
            }
            if (emissor.intValue() == 1)
                dtFaturamento = "" ;

            strSql = strSql + " DELETE Temporaria WHERE (FormID = 5110 AND RegistroID = " + (pedidoID != null ? (Object)pedidoID.ToString() : System.DBNull.Value) + ") " +
                        "INSERT INTO Temporaria (FormID, RegistroID, Logico1, Logico2, Valor1, Valor2, Valor3, Valor4, Valor5, dtData1) " +
                        "SELECT 5110, " + (pedidoID != null ? (Object)pedidoID.ToString() : System.DBNull.Value) + ", " +
                                          (emissor != null ? (Object)emissor.ToString() : System.DBNull.Value) + ", " +
                                          (atualizaData != null ? (Object)atualizaData.ToString() : System.DBNull.Value) + ", " +
                                          (documentoFiscalID != null ? (Object)documentoFiscalID.ToString() : System.DBNull.Value) + ", 0, " +
                                          (notaFiscal != null ? (Object)notaFiscal.ToString() : System.DBNull.Value) + "," +
                                          (serie != "" ? (Object)serie.ToString() : "NULL ") + ", " +
                                          (chaveAcesso.ToString() != "" ? ("'" + (Object)chaveAcesso.ToString() + "'") : "NULL") + ", ' "
                                          + sDtFaturamento + "' ";

            int Ocorrencia = DataInterfaceObj.ExecuteSQLCommand(strSql);
            
            return DataInterfaceObj.getRemoteData(
				"select " + Ocorrencia + " as Ocorrencia");
		}
		
		protected Integer nPedidoID
		{
			get { return pedidoID; }
			set { pedidoID = value != null ? value : zero; }
		}

        protected Integer nEmissor
		{
			get { return emissor; }
			set { emissor = value != null ? value : zero; }
		}

        protected Integer nAtualizaData
		{
			get { return atualizaData; }
			set { atualizaData = value != null ? value : zero; }
		}

        protected Integer nDocumentoFiscalID
        {
            get { return documentoFiscalID; }
            set { documentoFiscalID = value != null ? value : zero; }
        }

        protected string sSerie
        {
            get { return serie; }
            set { serie = value != null ? value : ""; }
        }

        protected string nNotaFiscal
		{
			get { return notaFiscal; }
            set { notaFiscal = value != null ? value : ""; }
		}
        
        protected Integer nNumeroVolumes
		{
			get { return numeroVolumes; }
			set { numeroVolumes = value != null ? value : zero; }
		}

        protected string sChaveAcesso
        {
            get { return chaveAcesso; }
            set { chaveAcesso = value != null ? value : ""; }
        }

        protected string sDtFaturamento
		{
			get { return dtFaturamento; }
			set { dtFaturamento = value != null ? value : ""; }
		}
	}
}
