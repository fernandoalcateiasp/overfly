using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class dadospedido : System.Web.UI.OverflyPage
	{
        private Integer pedidoID;

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(GetDadosPedidos());
        }

        protected DataSet GetDadosPedidos()
        {
            return DataInterfaceObj.getRemoteData(
               "SELECT NumeroVolumes,Conhecimento,Portador,DocumentoPortador,DocumentoFederalPortador,PlacaVeiculo,UFVeiculo, " +
         "ISNULL(dbo.fn_Pessoa_Documento(Pedidos.PessoaID, NULL, NULL, 111, 101, 0),0) As Documento, " +
         "ValorTotalPedido, ISNULL(dbo.fn_Pedido_Totais(Pedidos.PedidoID, 53),0) AS ValorIPI, " +
         "ISNULL(dbo.fn_Pedido_Totais(Pedidos.PedidoID, 14),0) AS BaseICMS, " +
         "ISNULL(dbo.fn_Pedido_Totais(Pedidos.PedidoID, 15),0) AS ValorICMS, " +
         "ISNULL(dbo.fn_Pedido_Totais(Pedidos.PedidoID, 55),0) AS BaseICMSST, " +
         "ISNULL(dbo.fn_Pedido_Totais(Pedidos.PedidoID, 54),0) AS ValorICMSST, " +
         "ISNULL(dbo.fn_Pedido_Totais(Pedidos.PedidoID, 57),0) AS BaseICMSSTComp, " +
         "ISNULL(dbo.fn_Pedido_Totais(Pedidos.PedidoID, 58),0) AS ValorICMSSTComp, " +
         "ISNULL(dbo.fn_Pedido_Totais(Pedidos.PedidoID, 16),0) AS ValorISS, " +
         "ISNULL(Transacao.MatrizFilial,0) AS MatrizFilial, " +
         "(SELECT COUNT(1) FROM RelacoesPessoas Filial WITH(NOLOCK) " +
            "WHERE (Filial.EstadoID = 2 AND Filial.TipoRelacaoID = 30  AND Filial.TransferePedidos = 1 AND " +
                        "((Filial.SujeitoID = Pedidos.PessoaID AND Filial.ObjetoID = Pedidos.EmpresaID) OR " +
                            "(Filial.SujeitoID = Pedidos.EmpresaID AND Filial.ObjetoID = Pedidos.PessoaID)))) AS TransferePedidos, " +
         "(SELECT COUNT(1) " +
                "FROM Pedidos_Itens Itens WITH(NOLOCK) " +
                        "INNER JOIN Operacoes CFOPs WITH(NOLOCK) ON Itens.CFOPID = CFOPs.OperacaoID " +
                 "WHERE (Itens.PedidoID = Pedidos.PedidoID AND CFOPs.SubstituicaoTributariaID = 3001 AND CFOPs.TransacaoBaixaID IN (112,116) AND " +
                            "Itens.CFOPID NOT IN (12029,14111,14113,22029,54111))) AS DevolucaoIncideICMST " +
         "FROM Pedidos WITH(NOLOCK) " +
            "INNER JOIN Operacoes Transacao WITH(NOLOCK) ON Pedidos.TransacaoID = Transacao.OperacaoID " +
         "WHERE PedidoID=" + pedidoID.ToString()
            );
        }

        protected Integer nPedidoID
        {
            get { return pedidoID; }
            set { pedidoID = value != null ? value : new Integer(0); }
        }

	}
}
