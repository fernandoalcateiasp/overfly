using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class impressaoNFSe : System.Web.UI.OverflyPage
    {
        private static Integer zero = new Integer(0);

        private Integer notaFiscalID;
        private Integer impressoraID;
        private Integer numeroVias;                
        private string msgRetorno;


        protected Integer nNotaFiscalID
        {
            get { return notaFiscalID; }
            set { notaFiscalID = value != null ? value : zero; }
        }

        protected Integer nImpressoraID
        {
            get { return impressoraID; }
            set { impressoraID = value != null ? value : zero; }
        }

        protected Integer nNumeroVias
        {
            get { return numeroVias; }
            set { numeroVias = value != null ? value : zero; }
        }


        // Pega os parāmetros para a procedure sp_G2KA_Imprime
        protected void Imprime()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[4];

            procParams[0] = new ProcedureParameters(
                "@NotaFiscalID",
                System.Data.SqlDbType.Int,
                ((notaFiscalID == null) ? System.DBNull.Value : (Object)notaFiscalID));

            procParams[1] = new ProcedureParameters(
                "@ImpressoraID",
                System.Data.SqlDbType.Int,
                ((impressoraID == null) ? System.DBNull.Value : (Object)impressoraID));

            procParams[2] = new ProcedureParameters(
                "@NumeroVias",
                System.Data.SqlDbType.Int,
                ((numeroVias == null) ? System.DBNull.Value : (Object)numeroVias));

            procParams[3] = new ProcedureParameters(
                "@Mensagem",
                SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[3].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_G2KA_Imprime",
                procParams);

            if (procParams[3].Data != DBNull.Value)
            {
                msgRetorno = procParams[3].Data.ToString();
            }

        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            Imprime();  

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select '" + msgRetorno +"' AS Mensagem "
                )
            );
        }
    }
}
