using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serversideEx
{
    public partial class cmbmoedas : System.Web.UI.OverflyPage
	{
		private static Integer Zero = new Integer(0);

		private Integer empresaID = Zero;
        protected Integer EmpresaID
        {
            set { empresaID = (value != null ? value : Zero); }
        }

        private Integer transacaoID = Zero;
        protected Integer TransacaoID
        {
            set { transacaoID = (value != null ? value : Zero); }
        }

		protected override void PageLoad(object sender, EventArgs e)
		{
            string strSQL = "SELECT c.ConceitoID as fldID, c.SimboloMoeda as fldName, 0 AS EhDefault, b.Ordem as Ordem2, " +
                                    "dbo.fn_Preco_Cotacao(d.MoedaID,c.ConceitoID,NULL,GETDATE()) AS Cotacao, e.TaxaVenda AS TaxaVenda, ' ' AS Filtro " +
                                "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec_Financ e WITH(NOLOCK) " +
                                "WHERE (a.SujeitoID IN (" + ((transacaoID.ToString() == "113") ? (empresaID.ToString() + ", 7") : empresaID.ToString()) + ") AND a.ObjetoID=999 AND a.TipoRelacaoID=12 " +
                                "AND a.RelacaoID = b.RelacaoID AND b.Faturamento=1 AND b.MoedaID = c.ConceitoID " +
                                "AND d.RecursoID=999 AND a.RelacaoID=e.RelacaoID AND b.MoedaID=e.MoedaID) ";

            WriteResultXML(DataInterfaceObj.getRemoteData(strSQL));
		}
	}
}
