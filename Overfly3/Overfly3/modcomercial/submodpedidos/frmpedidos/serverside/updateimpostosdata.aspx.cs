using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class updateimpostosdata : System.Web.UI.OverflyPage
	{
		private static Integer[] empty = new Integer[0];
		
		private Integer[] pedImpostoId;
        private string[] valorImposto;
        private string[] baseCalculo;
		private Integer[] tipoImposto;
        private string[] aliquota;

		protected Integer[] nPedImpostoID
		{
			get { return pedImpostoId; }
			set { pedImpostoId = value != null ? value : empty; }
		}

        protected string[] nValorImposto
        {
            set
            {
                valorImposto = value;

                for (int i = 0; i < valorImposto.Length; i++)
                {
                    if (valorImposto[i] == null || valorImposto[i].Length == 0)
                        valorImposto[i] = "NULL";
                }
            }
        }
        
        protected string[] nBaseCalculo
        {
            set
            {
                baseCalculo = value;

                for (int i = 0; i < baseCalculo.Length; i++)
                {
                    if (baseCalculo[i] == null || baseCalculo[i].Length == 0)
                        baseCalculo[i] = "NULL";
                }
            }
        }

		protected Integer[] nTipoImposto
		{
			get { return tipoImposto; }
			set { tipoImposto = value != null ? value : empty; }
		}

        protected string[] nAliquota
        {
            set
            {
                aliquota = value;

                for (int i = 0; i < aliquota.Length; i++)
                {
                    if (aliquota[i] == null || aliquota[i].Length == 0)
                        aliquota[i] = "NULL";
                }
            }
        }

		protected string GetSql()
		{
			string sql = "";

            for (int i = 0; i < pedImpostoId.Length; i++)
            {
                if (tipoImposto[i].intValue() == 1)
                {
                    sql += "UPDATE Pedidos_Impostos SET ValorImposto=" + valorImposto[i] + ", BaseCalculo=" + baseCalculo[i] + " " +
                        "FROM 	Pedidos_Impostos WITH(NOLOCK) INNER JOIN Pedidos WITH(NOLOCK) ON Pedidos_Impostos.PedidoID = Pedidos.PedidoID " +
                        "WHERE  Pedidos_Impostos.PedImpostoID=" + pedImpostoId[i] + " AND (Pedidos.Fechado = 1 OR Pedidos.TransacaoID IN (221, 741)) AND Pedidos.EstadoID = 21 ";
                }
                else
                {
                    sql += "UPDATE Pedidos_Itens_Impostos SET ValorImposto=" + valorImposto[i] + ", BaseCalculo=" + baseCalculo[i] + ", Aliquota=" + aliquota[i] + " " +
                    "FROM 	Pedidos_Itens_Impostos a WITH(NOLOCK) INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.PedItemID = a.PedItemID) " +
                    "INNER JOIN Pedidos c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) " +
                    "WHERE  a.PedImpostoID=" + pedImpostoId[i] + " AND (c.Fechado = 1 OR c.TransacaoID IN (221, 741)) AND c.EstadoID = 21 ";
                }
            }
			
			return sql;
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			int resultado = DataInterfaceObj.ExecuteSQLCommand(GetSql());
			
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select " + resultado + " as Resultado"
				)
			);
		}
	}
}
