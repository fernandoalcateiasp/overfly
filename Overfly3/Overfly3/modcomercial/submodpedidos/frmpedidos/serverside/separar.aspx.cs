using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class separar : System.Web.UI.OverflyPage
	{
		private static Integer zero = new Integer(0);
		private java.lang.Boolean falso = new java.lang.Boolean(false);

		private Integer pedidoId;
		private Integer usuarioId;
		private Integer produtoId;
		private java.lang.Boolean estorno;
		
		public Integer nPedidoID
		{
			get { return pedidoId; }
			set { pedidoId = value != null ? value : zero; }
		}
		
		public Integer nUsuarioID
		{
			get { return usuarioId; }
			set { usuarioId = value != null ? value : zero; }
		}

		public Integer nProdutoID
		{
			get { return produtoId; }
			set { produtoId = value != null ? value : zero; }
		}

		public java.lang.Boolean bEstorno
		{
			get { return estorno; }
			set { estorno = value != null ? value : falso; }
		}

		// Roda a procedure sp_PedidoItem_Separar;
		protected DataSet PedidoItemSeparar()
		{
			ProcedureParameters[] procParams = new ProcedureParameters[5];

			procParams[0] = new ProcedureParameters(
				"@PedidoID",
				System.Data.SqlDbType.Int,
				pedidoId.ToString());

			procParams[1] = new ProcedureParameters(
				"@ProdutoID",
				System.Data.SqlDbType.Int,
				produtoId.intValue() == 0 ? DBNull.Value : (Object)produtoId.ToString());

			procParams[2] = new ProcedureParameters(
				"@Estornar",
				System.Data.SqlDbType.Bit,
				estorno.booleanValue() ? 1 : 0);

			procParams[3] = new ProcedureParameters(
				"@UsuarioID",
				System.Data.SqlDbType.Int,
				usuarioId.ToString());

			procParams[4] = new ProcedureParameters(
				"@Resultado",
				System.Data.SqlDbType.VarChar,
				DBNull.Value,
				ParameterDirection.InputOutput);
			procParams[4].Length = 8000;

			DataInterfaceObj.execNonQueryProcedure(
				"sp_PedidoItem_Separar",
				procParams);

			return DataInterfaceObj.getRemoteData(
				"select " +
				((procParams[4].Data != DBNull.Value) ? "'" + procParams[4].Data.ToString() + "'" : " NULL ") +
				" as Resultado"
			);
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(PedidoItemSeparar());
		}
	}
}
