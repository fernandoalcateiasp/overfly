using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class aprovacaoespecial : System.Web.UI.OverflyPage
    {
        private Integer pedidoID;
        private Integer tipoAprovacaoID;
        private Integer usuarioID;
        private Integer tipoResultado;
        private Integer pedAprovacaoID;
        private string mensagem;

        protected override void PageLoad(object sender, EventArgs e)
        {
            PedidoAprovacaoEspecial();
            WriteResultXML(DataInterfaceObj.getRemoteData(
                    "select '" + mensagem + "' as Mensagem "));
        }

        protected void PedidoAprovacaoEspecial()
        {
            // Roda a procedure sp_Empresa_Depositos
            ProcedureParameters[] procParams = new ProcedureParameters[6];

            procParams[0] = new ProcedureParameters(
                "@PedidoID",
                System.Data.SqlDbType.Int,
                pedidoID.ToString());

            procParams[1] = new ProcedureParameters(
                "@TipoAprovacaoID",
                System.Data.SqlDbType.Int,
                tipoAprovacaoID);

            procParams[2] = new ProcedureParameters(
                "@TipoResultado",
                System.Data.SqlDbType.Int,
                tipoResultado);

            procParams[3] = new ProcedureParameters(
                "@PedAprovacaoID",
                System.Data.SqlDbType.Int,
                pedAprovacaoID,
                ParameterDirection.InputOutput);

            procParams[4] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                usuarioID,
                ParameterDirection.InputOutput);

            procParams[5] = new ProcedureParameters(
                "@Mensagem",
                System.Data.SqlDbType.VarChar,
                System.DBNull.Value,
                ParameterDirection.Output);
            procParams[5].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Pedido_AprovacaoEspecial",
                procParams);

            if (procParams[5].Data != DBNull.Value)
                mensagem = procParams[5].Data.ToString();
        }

        protected Integer nPedidoID
        {
            get { return pedidoID; }
            set { pedidoID = (value != null ? value : new Integer(0)); }
        }

        protected Integer nTipoAprovacaoID
        {
            get { return tipoAprovacaoID; }
            set { tipoAprovacaoID = (value != null ? value : new Integer(0)); }
        }

        protected Integer nUsuarioID
        {
            get { return usuarioID; }
            set { usuarioID = (value != null ? value : new Integer(0)); }
        }

        protected Integer nTipoResultado
        {
            get { return tipoResultado; }
            set { tipoResultado = (value != null ? value : new Integer(0)); }
        }

        protected Integer nPedAprovacaoID
        {
            get { return pedAprovacaoID; }
            set { pedAprovacaoID = (value != null ? value : new Integer(0)); }
        }       
    }
}
