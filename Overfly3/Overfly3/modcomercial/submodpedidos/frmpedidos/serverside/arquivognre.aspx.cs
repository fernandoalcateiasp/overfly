using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using OVERFLYSVRCFGLib;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class arquivognre : System.Web.UI.OverflyPage
	{
		protected string pedidosID;
		protected string empresaID;
		protected string uf;
		
		protected string urlBase;
		
		protected string fileName;
		protected string dataType;
		protected string dataFile;
		
		protected string nEmpresaID {
			set { empresaID = value != null ? value : ""; }
		}
		
		protected string UF {
			set {
				uf = value != null ? value : "";
				
				dataType = uf.Equals("SP") ? "xml" : "txt";
			}
		}
		
		protected string nPedidosID {
			set { pedidosID = value != null ? value : ""; }
		}

		protected string UrlFile {
			get {
				if(urlBase == null) {
					OverflyMTS overflyMTS = new OverflyMTS();
					urlBase = overflyMTS.PagesURLRoot(DataInterfaceObj.ApplicationName);
				}
				
				return urlBase + FileName;
			}
		}

		protected string FileName {
			get {
				if (fileName == null)
				{
					string NF = "";

					// Obt�m o n�mero da nota fiscal para compor o nome do arquivo TXT.
					// A NF � o quarto campo nos dados do arquivo.
					if (dataFile != null && dataType.Equals("txt"))
					{
						string[] campos = dataFile.Split(new char[] {'\t', '\n'} );
						
						NF = campos[4];
					}
					// Obt�m o n�mero da nota fiscal para compor o nome do arquivo XML.
					// Recupera o n�mero do atributo Nf da tag <Dados>
					else if (dataFile != null && dataType.Equals("xml"))
					{
						int start;
						int length;

						start = dataFile.IndexOf("Nf=\"") + "Nf=\"".Length;
						length = dataFile.Substring(start).IndexOf("\"");

						NF = dataFile.Substring(start, length);
					}
					// Se n�o h� um meio de definir um nome,coloca um fixo.
					else {
						DateTime now = new DateTime(0);
						
						NF = now.Year.ToString() + now.Month.ToString() + now.Day.ToString() + 
							now.Hour.ToString() + now.Minute.ToString() + now.Second.ToString();
					}

					// Monta o nome do arquivo.
					fileName = "/GNRE/" + empresaID + "_" + NF + "." + dataType;
				}
				
				return fileName;
			}
		}
		
		protected void GeraArquivo()
		{
			FileStream file = new FileStream(
				Request.PhysicalApplicationPath + FileName,
				FileMode.Create,
				FileAccess.ReadWrite);

			StreamWriter sw = new StreamWriter(file);

			sw.WriteLine(dataFile);

			sw.Close();
		}
		
		// Executa a procedure sp_ArquivoGNRE
		protected void PedidoGNRE()
		{
			// Roda a procedure sp_Empresa_Depositos
			ProcedureParameters[] procParams = new ProcedureParameters[3];

			procParams[0] = new ProcedureParameters(
				"@PedidosID",
				System.Data.SqlDbType.VarChar,
				pedidosID);
			procParams[0].Length = 8000;

			procParams[1] = new ProcedureParameters(
				"@UF",
				System.Data.SqlDbType.Char,
				uf);
			procParams[1].Length = 2;

			procParams[2] = new ProcedureParameters(
				"@Resultado",
				System.Data.SqlDbType.VarChar,
				DBNull.Value,
				ParameterDirection.InputOutput);
			procParams[2].Length = 8000;

			DataInterfaceObj.execNonQueryProcedure("sp_Pedido_GNRE", procParams);

			dataFile = procParams[2].Data.ToString();
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			// Chama a procedure para obter os dados e o tipo de retorno.
			PedidoGNRE();
			// Gera o arquivo em disco no servidor.
			GeraArquivo();


			responseXML = 
				"<html xmlns=\"http://www.w3.org/1999/xhtml\" > " + 
					"<head id=\"head1\" runat=\"server\"> " + 
						"<title></title> " + 
					"</head> " + 
					"<body> " + 
						"<br/> " + 
						"<table border=\"0\"> " + 
							"<tr> " + 
								"<td> " + 
									"Click <a href=\"" + UrlFile + "\">aqui</a> para exibir o arquivo ou selecione Salvar como... no menu suspensso. " + 
								"</td> " + 
							"</tr> " + 
						"</table> " + 
					"</body> " + 
				"</html>";
			
			Response.Write(responseXML);
		}
	}
}
