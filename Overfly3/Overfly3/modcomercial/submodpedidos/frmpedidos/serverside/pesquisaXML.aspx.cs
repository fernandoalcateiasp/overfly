using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class pesquisaXML : System.Web.UI.OverflyPage
	{
		private string chaveAcesso;	
	
		protected string nChaveAcesso
		{
			get { return chaveAcesso; }
			set { chaveAcesso = value != null ? value : "null"; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					GetSql()
				)
			);
		}
	
		protected string GetSql()
		{
			string sql = "";

            sql = "SET NOCOUNT ON DECLARE @XML XML, @CaminhoArquivo VARCHAR(512), @CNPJ VARCHAR(14), @Emitente VARCHAR(512), @NotaFiscal INT, @ChaveAcesso VARCHAR(44) " +
                    "DECLARE @Table TABLE (ChaveAcesso VARCHAR(44), Emitente VARCHAR(512), CNPJ VARCHAR(14), NotaFiscal INT) " +

                    "SET @ChaveAcesso = '" + chaveAcesso + "' " +

                    "SELECT @CaminhoArquivo = Caminho FROM [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[XMLRecebidas] a WITH(NOLOCK) WHERE ChaveAcesso = @ChaveAcesso " +

                    "IF (@CaminhoArquivo IS NOT NULL) " +
                    "BEGIN " +
                        "CREATE TABLE #TmpArquivo(Arquivo XML) " +

                        "EXEC ('INSERT INTO #TmpArquivo SELECT CAST(BulkColumn AS XML) FROM OPENROWSET(BULK N''' + @CaminhoArquivo + ''', SINGLE_BLOB) AS Arquivo') " +

                        "SELECT @XML = Arquivo FROM #TmpArquivo " +

                        "DROP TABLE #TmpArquivo " +

                        "SET @XML = REPLACE(CONVERT(VARCHAR(MAX),@XML),'xmlns=' + CHAR(34) + 'http://www.portalfiscal.inf.br/nfe' + CHAR(34) + '','') " +

                        "SELECT @CNPJ = NFe.Prot.value('CNPJ[1]', 'VARCHAR(14)') FROM @XML.nodes('/nfeProc/NFe/infNFe/emit') AS NFe(Prot) " +

                        "SELECT @Emitente = NFe.Prot.value('xNome[1]', 'VARCHAR(512)') FROM @XML.nodes('/nfeProc/NFe/infNFe/emit') AS NFe(Prot) " +

                        "SELECT @NotaFiscal = NFe.Prot.value('nNF[1]', 'INT') FROM @XML.nodes('/nfeProc/NFe/infNFe/ide') AS NFe(Prot) " +

                        "INSERT INTO @Table " +
                            "SELECT @ChaveAcesso, @Emitente, @CNPJ, @NotaFiscal " +
                    "END " +

                    "SELECT *FROM @Table SET NOCOUNT OFF";
               return sql;
		}
	}
}
