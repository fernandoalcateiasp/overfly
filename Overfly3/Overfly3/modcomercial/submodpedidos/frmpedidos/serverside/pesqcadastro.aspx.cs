using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class pesqcadastro : System.Web.UI.OverflyPage
	{
		private static Integer zero = new Integer(0);
		private static java.lang.Boolean verdade = new java.lang.Boolean(true);
		
		private Integer contextoId;
		private Integer pedidoId;
		private Integer usuarioId;
		private Integer empresaId;
		private java.lang.Boolean cliente;
		private Integer parceiroId;
		private Integer tipo;
		private string filtro;
		private string dateSqlParam;
        private string paisID;

        public Integer nContextoID
		{
			get { return contextoId; }
			set { contextoId = value != null ? value : zero; }
		}
		
		public Integer nPedidoID
		{
			get { return pedidoId; }
			set { pedidoId = value != null ? value : zero; }
		}
		
		public Integer nUsuarioID
		{
			get { return usuarioId; }
			set { usuarioId = value != null ? value : zero; }
		}
		
		public Integer nEmpresaID
		{
			get { return empresaId; }
			set { empresaId = value != null ? value : zero; }
		}
		
		public java.lang.Boolean nEhCliente
		{
			get { return cliente; }
			set { cliente = value != null ? value : verdade; }
		}

		public Integer nParceiroID
		{
			get { return parceiroId; }
			set { parceiroId = value != null ? value : zero; }
		}
		
		public Integer nTipo
		{
			get { return tipo; }
			set { tipo = value != null ? value : zero; }
		}
		
		public string sFiltro
		{
			get { return filtro; }
			set { filtro = value != null ? value : ""; }
		}
		
		public string DATE_SQL_PARAM
		{
			get { return dateSqlParam; }
			set { dateSqlParam = value != null ? value : "103"; }
		}

        public string nPaisID
        {
            get { return paisID; }
            set { paisID = value != null ? value : ""; }
        }

        protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					GetSql()
				)
			);
		}
	
		protected string GetSql()
		{
			string sql = "";
			
			if(tipo.intValue() == 1)
			{
				if(cliente.booleanValue())
				{
					filtro = " AND b.SujeitoID = a.PessoaID AND b.ObjetoID = " + empresaId + " AND b.TipoRelacaoID = 21 ";
				}
				else
				{
					filtro = " AND b.ObjetoID = a.PessoaID AND b.SujeitoID = " + empresaId + " AND b.TipoRelacaoID = 21 ";
				}

                sql = "DECLARE @dtValidadeCredito DATETIME " +
                        
                        "SELECT @dtValidadeCredito = MAX(dtInicio) " +
                            "FROM Creditos WITH(NOLOCK) " +
                            "WHERE ParceiroID = " + parceiroId + " AND EstadoID = 75 " +

                        "SELECT TOP 1 CONVERT(VARCHAR, a.dtNascimento, " + dateSqlParam + ") AS dtFundacao, " +
                                "DATEDIFF(yy, a.dtNascimento, GETDATE()) AS AnosFundacao, " +
                                "a.CNAE AS CNAE, " +
                                "c.AtividadeEconomica AS DescricaoCNAE, " +
                                "ISNULL(d.ItemMasculino, SPACE(0)) AS Classificacao, " +
                                "ISNULL(e.ItemMasculino, SPACE(0)) AS ClassificacaoABC, " +
                                "ISNULL(f.ItemMasculino, SPACE(0)) AS Conceito, " +
                                "CONVERT(VARCHAR, @dtValidadeCredito, " + dateSqlParam + ") AS dtValidadeCredito, " +
                                "(ISNULL(g.Bloquear, 0)) AS Bloqueado, " +
                                "dbo.fn_Formata_Numero(a.CapitalSocial,2,103) AS CapitalSocial, " +
                                "dbo.fn_Formata_Numero(a.PatrimonioLiquido,2,103) AS PatrimonioLiquido, " +
                                "dbo.fn_Formata_Numero(a.FaturamentoAnual,2,103) AS FaturamentoAnual, " +
                                "dbo.fn_Formata_Numero(dbo.fn_Pessoa_Credito(a.PessoaID, 1, " + paisID + "), 2, 103) AS LimiteCreditoEmpresa, " +
                                "dbo.fn_Formata_Numero(dbo.fn_Pessoa_Credito(a.PessoaID, 2, " + paisID + "), 2, 103)  AS SeguroCredito, " +
                                "dbo.fn_Formata_Numero(dbo.fn_Pessoa_Credito(a.PessoaID, 3, " + paisID + "), 2, 103) AS LimiteCredito, " +
                                "dbo.fn_Formata_Numero(dbo.fn_Pessoa_Posicao(a.PessoaID, -" + empresaId + ", 10, GETDATE(), 0, NULL, NULL, NULL, NULL),2,103) AS SaldoDevedor, " +
                                "dbo.fn_Formata_Numero(dbo.fn_Pessoa_Credito(a.PessoaID, 4, " + paisID + "), 2, 103) AS SaldoCredito, " +
                                "dbo.fn_Formata_Numero(dbo.fn_Pessoa_Credito(a.PessoaID, 6, " + paisID + "), 2, 103) AS SaldoCreditoGrupo " +
                            "FROM Pessoas a WITH(NOLOCK) " +
                                "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (a.PessoaID = b.SujeitoID) " +
                                "LEFT OUTER JOIN CNAEs c WITH(NOLOCK) ON (a.CNAE = c.CNAE) " +
                                "LEFT OUTER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (a.ClassificacaoID = d.ItemID) " +
                                "LEFT OUTER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (b.ClassificacaoID = e.ItemID) " +
                                "LEFT OUTER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (b.ConceitoID = f.ItemID) " +
                                "LEFT OUTER JOIN Pessoas_Creditos g WITH(NOLOCK) ON (g.PessoaID = a.PessoaID) " +
                            "WHERE (a.PessoaID = " + parceiroId + " AND b.ObjetoID = " + empresaId + " AND b.EstadoID = 2 AND b.TipoRelacaoID = 21 " + filtro + ")";

			}     
			// Atributos
			else if (tipo.intValue() == 2)
			{
				sql = "EXEC sp_Pedido_Atributos " + pedidoId + ", " + usuarioId;
			}
			// InfFinanceiras
			else if(tipo.intValue() == 3)
			{
				sql = "SELECT 1 AS Indice, a.Observacoes AS Observacoes, NULL AS dtReferencia, " +
					"NULL AS Empresa, NULL AS dtInicio, NULL AS dtUltimaCompra, NULL AS ValorUltimaCompra, " +
					"NULL AS dtMaiorCompra, NULL AS ValorMaiorCompra, NULL AS LimiteCredito, NULL AS Prazo, NULL AS Atraso, NULL AS Conceito, NULL AS Ordem2 " +
					"FROM Pessoas a WITH(NOLOCK) " +
					"WHERE (a.PessoaID = " + parceiroId + ") " +
					"UNION ALL " +
					"SELECT TOP 5 2 AS Indice, NULL AS Observacoes, a.dtReferencia, " +
					"b.Fantasia AS Empresa, a.dtInicio, a.dtUltimaCompra, a.ValorUltimaCompra, " +
                    "a.dtMaiorCompra, a.ValorMaiorCompra, dbo.fn_Pessoa_Credito(a.PessoaID, 3, " + paisID + "), a.Prazo, a.Atraso, c.ItemMasculino AS Conceito, a.dtReferencia AS Ordem2 " +
					"FROM Pessoas_RefComerciais a WITH(NOLOCK) " +
						"INNER JOIN Pessoas b WITH(NOLOCK) ON (a.EmpresaID = b.PessoaID) " +
						"LEFT OUTER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (a.ConceitoID = c.ItemID) " +
					"WHERE (a.PessoaID = " + parceiroId + ") " +
					"ORDER BY Indice, Ordem2 DESC";
			}
			// Financeiros
            else if (tipo.intValue() == 4)
            {
                sql = "SELECT a.dtVencimento, CONVERT(NUMERIC(11), dbo.fn_Financeiro_Totais(a.FinanceiroID, GETDATE(), 40)) AS Atraso, " +
                        "a.FinanceiroID,c.RecursoAbreviado AS Estado, " +
                        "a.PedidoID AS PedidoID, j.OperacaoID AS Codigo, e.Fantasia AS Pessoa, a.Duplicata, a.dtEmissao, " +
                        "g.ItemAbreviado AS FormaPagamento, a.PrazoPagamento, h.SimboloMoeda AS Moeda, " +
                        "(a.Valor * POWER(-1, a.TipoFinanceiroID)) AS Valor, " +
                        "(dbo.fn_Financeiro_Posicao(a.FinanceiroID, 2, NULL, GETDATE(), NULL, NULL) * POWER(-1, a.TipoFinanceiroID)) AS SaldoDevedor, " +
                        "(dbo.fn_Financeiro_Posicao(a.FinanceiroID, NULL, NULL, GETDATE(), NULL, NULL) * POWER(-1, a.TipoFinanceiroID)) AS SaldoAtualizado, " +
                        "i.Fantasia AS Empresa " +
                      "FROM Financeiro a WITH(NOLOCK) " +
                        "LEFT OUTER JOIN Pedidos b WITH(NOLOCK) ON (a.PedidoID = b.PedidoID) " +
                        "LEFT OUTER JOIN Operacoes j WITH(NOLOCK) ON (b.TransacaoID = j.OperacaoID), Recursos c WITH(NOLOCK), Pessoas e WITH(NOLOCK), " +
                        "TiposAuxiliares_Itens g WITH(NOLOCK), Conceitos h WITH(NOLOCK), Pessoas i WITH(NOLOCK) " +
                      "WHERE (a.ParceiroID = " + parceiroId + " AND a.EstadoID = c.RecursoID AND " +
                        "a.EstadoID NOT IN(48,5,1) AND a.PessoaID = e.PessoaID AND " +
                        "a.FormaPagamentoID = g.ItemID AND a.MoedaID = h.ConceitoID AND " +
                        "a.EmpresaID = i.PessoaID) " +
                        "ORDER BY dtVencimento";
            }
			
			return sql;
		}
	}
}
