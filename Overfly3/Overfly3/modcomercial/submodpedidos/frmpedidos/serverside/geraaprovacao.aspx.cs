using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class geraaprovacao : System.Web.UI.OverflyPage
	{
		private Integer pedidoId;
        private Integer tipoAprovacaoID;
        private Integer tipoResultado;
        private string sql;
        private int EstadoID;
		private Integer zero = new Integer(0);
		
		protected override void PageLoad(object sender, EventArgs e)
		{
            Estado();
            WriteResultXML(AprovacoesEspeciais());


		}

        protected int Estado()
        {
            sql = "SELECT EstadoID FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND ItemID = " + (tipoAprovacaoID);

            EstadoID = DataInterfaceObj.ExecuteSQLCommand(sql);

            return EstadoID;            
        }

        // Roda a procedure sp_Pedido_AprovacaoEspecial
		private DataSet AprovacoesEspeciais()
		{
			ProcedureParameters[] procParams = new ProcedureParameters[6];

			procParams[0] = new ProcedureParameters(
				"@PedidoID",
				System.Data.SqlDbType.Int,
				pedidoId.ToString());

			procParams[1] = new ProcedureParameters(
                "@TipoAprovacaoID",
				System.Data.SqlDbType.Int,
				tipoAprovacaoID.ToString());

			procParams[2] = new ProcedureParameters(
                "@TipoResultado",
				System.Data.SqlDbType.Int,
                tipoResultado);

			procParams[3] = new ProcedureParameters(
                "@PedAprovacaoID",
				System.Data.SqlDbType.Int,
				DBNull.Value,
				ParameterDirection.InputOutput);
		
            procParams[4] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);
            
            procParams[5] = new ProcedureParameters(
                "@Mensagem",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[5].Length = 8000;

			DataInterfaceObj.execNonQueryProcedure(
                "sp_Pedido_AprovacaoEspecial",
				procParams);

			return DataInterfaceObj.getRemoteData(
                "select '" + procParams[4].Data + "' as Resultado"
			);
		}

		protected Integer nPedidoID
		{
			get { return pedidoId; }
			set { pedidoId = value != null ? value : zero; }
		}

        protected Integer nTipoAprovacaoID
		{
			get { return tipoAprovacaoID; }
			set { tipoAprovacaoID= value != null ? value : zero; }
		}

        protected Integer nTipoResultado
        {
            get { return tipoResultado; }
            set { tipoResultado = value != null ? value : zero; }
        }
	}
}
