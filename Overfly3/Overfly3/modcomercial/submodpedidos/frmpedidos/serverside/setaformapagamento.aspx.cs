using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class setaformapagamento : System.Web.UI.OverflyPage
	{
		private string formaPagamentoId;
		private Integer relacaoId;
		
		protected string sFormaPagamentoID
		{
			get { return formaPagamentoId; }
			set { formaPagamentoId = value != null ? value : ""; }
		}
		
		protected Integer nRelacaoID
		{
			get { return relacaoId; }
			set { relacaoId = value != null ? value : new Integer(0); }
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.ExecuteSQLCommand(
				" UPDATE RelacoesPessoas SET FormaPagamentoID = " + formaPagamentoId +
				" WHERE RelacaoID = " + relacaoId
			);
			
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select 'Forma de Pagamento setada como default' as Verificacao"
				)
			);
		}
	}
}
