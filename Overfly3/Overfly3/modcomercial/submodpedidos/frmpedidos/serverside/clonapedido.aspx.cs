using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class clonapedido : System.Web.UI.OverflyPage
    {
        protected static Integer Zero = new Integer(0);
        protected static Integer[] Zer = new Integer[0];
        protected static Integer Um = new Integer(1);

        private Integer dataLen = Um;
        private Integer[] PedidoID = Zer;
        private Integer userID;
        private Integer tipoPedido;
        private Integer empresaID;
        private string pedidoGeradoID;
        private string resultado;

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(PedidoClone());
        }

        // Roda a procedure sp_Pedido_Clone
        protected DataSet PedidoClone()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[6];
            for (int i = 0; i < dataLen.intValue(); i++)
            {

                procParams[0] = new ProcedureParameters(
                    "@EmpresaID",
                    System.Data.SqlDbType.Int,
                    (empresaID.intValue() != null) ? (Object)empresaID.ToString() : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                    "@PedidoID",
                    System.Data.SqlDbType.Int,
                    (PedidoID.Length > 0) ? (Object)PedidoID[i].ToString() : DBNull.Value);

                procParams[2] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                    (userID.intValue() != null) ? (Object)userID.ToString() : DBNull.Value);
                    

                procParams[3] = new ProcedureParameters(
                    "@TipoPedido",
                    System.Data.SqlDbType.Int,
                    (tipoPedido.intValue() != null) ? (Object)tipoPedido.ToString() : DBNull.Value);

                procParams[4] = new ProcedureParameters(
                    "@PedidoGeradoID",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[4].Length = 256;

                procParams[5] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.Output);
                procParams[5].Length = 8000;
               

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_Pedido_Clone",
                    procParams);

                pedidoGeradoID += procParams[4].Data.ToString();

                resultado += procParams[5].Data.ToString();
            }

            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (pedidoGeradoID != null ? "'" + pedidoGeradoID + "' " : "NULL") +
                 " as PedidoGeradoID " +
                 (resultado != null ? ", '" + resultado + "' " : ", NULL") +
                 " as Resultado ");
        }

        protected Integer nDataLen
        {
            get { return dataLen; }
            set { dataLen = value != null ? value : Um; }
        }

        protected Integer nEmpresaID
        {
            get { return empresaID; }
            set { empresaID = value != null ? value : Um; }
        }

        protected Integer nUserID
        {
            get { return userID; }
            set { userID = value != null ? value : Zero; }
        }

        protected Integer nTipoPedido
        {
            get { return tipoPedido; }
            set { tipoPedido = value != null ? value : Um; }
        }

        protected Integer[] nPedidoID
        {
            set
            {
                PedidoID = value;

                for (int i = 0; i < PedidoID.Length; i++)
                {
                    if (PedidoID[i] == null)
                        PedidoID[i] = Zero;
                }
            }
        }
    }
}
