using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class impressaoNFe : System.Web.UI.OverflyPage
    {
        private Integer notaFiscalID;
        private Integer impressoraID;
        private java.lang.Boolean danfe;
        private java.lang.Boolean gnre;
        private java.lang.Boolean comprovantePagtoGNRE;
        private java.lang.Boolean listaEmbalagem;


        protected override void PageLoad(object sender, EventArgs e)
        {
            ImpressaoNotaFiscalEletronica();
            WriteResultXML(
                DataInterfaceObj.getRemoteData("select 0 as Resultado, space(0) as Mensagem")
            );
        }

        public Integer nNotaFiscalID
        {
            get { return notaFiscalID; }
            set { notaFiscalID = value != null ? value : new Integer(0); }
        }
        protected java.lang.Boolean bDanfe
        {
            set { danfe = value != null ? value : new java.lang.Boolean(false); }
        }   
        protected Integer nImpressoraID
        {
            set { impressoraID = value != null ? value : new Integer(-1); }
        }

        protected void ImpressaoNotaFiscalEletronica()
        {
            ProcedureParameters[] procparam = new ProcedureParameters[3];

            procparam[0] = new ProcedureParameters(
                "@NotaFiscalID",
                SqlDbType.Int,
                notaFiscalID);
            procparam[1] = new ProcedureParameters(
                "@Danfe",
                SqlDbType.Bit,
                danfe.booleanValue() ? 1 : 0);
            procparam[2] = new ProcedureParameters(
                "@ImpressoraID",
                SqlDbType.Int,
                impressoraID);

            // Executa a procedure.
            DataInterfaceObj.execNonQueryProcedure(
                "sp_NotaFiscal_Inventti_Imprime",
                procparam);
        }
    }
}