using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{

    public partial class crianovainvoice : System.Web.UI.OverflyPage
    {
        public string Resultado = "";

        private string pedidoID;
        private string empresaID;
        private string estadoID;
        private string invoice;
        private string dtEmissao;
        private string exportadorID;
        private string taxaMoeda;
        private string valorTotalInvoice;

        protected string nPedidoID { set { pedidoID = value == "" ? null : value; } }
        protected string nEmpresaID { set { empresaID = value == "" ? null : value; } }
        protected string nEstadoID { set { estadoID = value == "" ? null : value; } }
        protected string sInvoice { set { invoice = value != null ? value : ""; } }
        protected string sDtEmissao { set { dtEmissao = value != null ? value : ""; } }
        protected string nExportadorID { set { exportadorID = value != null ? value : ""; } }
        protected string nTaxaMoeda { set { taxaMoeda = ((value == "") || (value == "0")) ? null : value; } }
        protected string nValorTotalInvoice { set { valorTotalInvoice = ((value == "") || (value == "0")) ? null : value; } }

        protected string Importacao_Servico()
        {
            // Manta os parametros da ProcedureParameters.
            ProcedureParameters[] Importacao_ServicoProcParams = new ProcedureParameters[9];

            Importacao_ServicoProcParams[0] = new ProcedureParameters(
                "@PedidoID",
                System.Data.SqlDbType.Int,
                (pedidoID != null ? (Object)int.Parse(pedidoID) : DBNull.Value));

            Importacao_ServicoProcParams[1] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                (empresaID != null ? (Object)int.Parse(empresaID) : DBNull.Value));

            Importacao_ServicoProcParams[2] = new ProcedureParameters(
                "@EstadoID",
                System.Data.SqlDbType.Int,
                estadoID != null ? (Object)int.Parse(estadoID) : DBNull.Value);

            Importacao_ServicoProcParams[3] = new ProcedureParameters(
                "@Invoice",
                System.Data.SqlDbType.VarChar,
                (!(invoice == null || invoice.Length == 0)) ? (Object)invoice : DBNull.Value);
            Importacao_ServicoProcParams[3].Length = 15;

            Importacao_ServicoProcParams[4] = new ProcedureParameters(
                "@dtEmissao", SqlDbType.DateTime,
                dtEmissao.Equals("") ? System.DBNull.Value : (Object)dtEmissao.ToString());

            Importacao_ServicoProcParams[5] = new ProcedureParameters(
                "@ExportadorID",
                System.Data.SqlDbType.Int,
                exportadorID != null ? (Object)int.Parse(exportadorID) : DBNull.Value);

            Importacao_ServicoProcParams[6] = new ProcedureParameters(
                "@TaxaMoeda",
                System.Data.SqlDbType.Money,
                taxaMoeda != null ? (Object)float.Parse(taxaMoeda) : DBNull.Value);

            Importacao_ServicoProcParams[7] = new ProcedureParameters(
                "@ValorTotalInvoice",
                System.Data.SqlDbType.Money,
                valorTotalInvoice != null ? (Object)float.Parse(valorTotalInvoice) : DBNull.Value);

            Importacao_ServicoProcParams[8] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.Int,
                System.DBNull.Value,
                ParameterDirection.Output);

            // DataInterfaceObj.execQueryProcedure("sp_Importacao_Servico", Importacao_ServicoProcParams);
            DataInterfaceObj.execNonQueryProcedure("sp_Importacao_Servico", Importacao_ServicoProcParams);

            Resultado += Importacao_ServicoProcParams[8].Data.ToString();

            return Resultado;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(DataInterfaceObj.getRemoteData("select '" + Importacao_Servico() + "' as Resultado"));
        }
    }
}