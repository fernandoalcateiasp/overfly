using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class AssociarItemLote : System.Web.UI.OverflyPage
    {
        private Integer[] loteID;

        public Integer[] nLoteID
        {
            get { return loteID; }
            set { loteID = value; }
        }
        private Integer[] peditemID;

        public Integer[] nPedItemID
        {
            get { return peditemID; }
            set { peditemID = value; }
        }
        private Integer[] quantidade;

        public Integer[] nQuantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }
        private int LotPedItemID;
        private string Resultado;

        private int nDataLen;
        private bool[] criarProduto;

        public bool[] bCriarProduto
        {
            get { return criarProduto; }
            set { criarProduto = value; }
        }
        private Integer[] zero = new Integer[0];

        protected override void PageLoad(object sender, EventArgs e)
        {
            LoteAssociar();
            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select '" + Resultado + " ' as Resultado, " +
                                LotPedItemID + "  as LotPedItemID"));
        }

        // Roda a procedure sp_Lote_ItemAssociar
        protected void LoteAssociar()
        {

            nDataLen = peditemID.Length;

            for (int i = 0; i < nDataLen; i++)
            {
                ProcedureParameters[] procParams = new ProcedureParameters[6];
                procParams[0] = new ProcedureParameters(
                    "@LoteID",
                    System.Data.SqlDbType.Int,
                    loteID[i]);

                procParams[1] = new ProcedureParameters(
                    "@PedItemID",
                    System.Data.SqlDbType.Int,
                    peditemID[i]);

                procParams[2] = new ProcedureParameters(
                    "@Quantidade",
                    System.Data.SqlDbType.Int,
                    quantidade[i]);

                procParams[3] = new ProcedureParameters(
                    "@CriarProduto",
                    System.Data.SqlDbType.VarChar,
                    criarProduto[i].ToString());

                procParams[4] = new ProcedureParameters(
                    "@LotPedItemID",
                    System.Data.SqlDbType.Int,
                    System.DBNull.Value,
                    ParameterDirection.InputOutput);

                procParams[5] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    System.DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[5].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_Lote_ItemAssociar",
                    procParams);

                if (procParams[4].Data != DBNull.Value)
                    LotPedItemID = int.Parse(procParams[4].Data.ToString());

                if (procParams[5].Data != DBNull.Value)
                    Resultado = procParams[5].Data.ToString();
            }
        }
    }
}
