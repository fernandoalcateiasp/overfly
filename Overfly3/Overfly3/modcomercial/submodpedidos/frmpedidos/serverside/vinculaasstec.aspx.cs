using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class vinculaasstec : System.Web.UI.OverflyPage
    {
        private static Integer zero = new Integer(0);

        private Integer pedidoId = zero;
        private Integer numMovimentoId = zero;
        private Integer pedProdutoId = zero;
        private Integer pedLoteId = zero;
        private Integer asstecId = zero;
        private Integer tipoResultado = zero;
        protected int AsstecDuplicada;
        private java.lang.Boolean bStartModal;
        private string sql;
        private string erros = "";
        protected int sqlMovimentacao;


        protected Integer nPedidoID
        {
            get { return pedidoId; }
            set { pedidoId = value != null ? value : zero; }
        }

        protected Integer nNumMovimentoID
        {
            get { return numMovimentoId; }
            set { numMovimentoId = value != null ? value : zero; }
        }

        protected Integer nPedProdutoID
        {
            get { return pedProdutoId; }
            set { pedProdutoId = value != null ? value : zero; }
        }

        protected Integer nPedLoteID
        {
            get { return pedLoteId; }
            set { pedLoteId = value != null ? value : zero; }
        }

        protected Integer nAsstecID
        {
            get { return asstecId; }
            set { asstecId = value != null ? value : zero; }
        }

        protected Integer nTipoResultado
        {
            get { return tipoResultado; }
            set { tipoResultado = value != null ? value : new Integer(1); }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            VincularAsstec();

            WriteResultXML(DataInterfaceObj.getRemoteData("select '" + bStartModal + "' as bStartModal, ' " + AsstecDuplicada + " ' as nAsstecDuplicada, ' "  + erros + " ' as fldResponse"));

        }

        protected void VincularAsstec()
        {

            try
            {
                ProcedureParameters[] procParams = new ProcedureParameters[6];
                procParams[0] = new ProcedureParameters(
                    "@PedidoID",
                    System.Data.SqlDbType.Int,
                    pedidoId.intValue() == 0 ? DBNull.Value : (Object)pedidoId.ToString());

                procParams[1] = new ProcedureParameters(
                    "@NumMovimentoID",
                    System.Data.SqlDbType.Int,
                    numMovimentoId.intValue() == 0 ? DBNull.Value : (Object)numMovimentoId.ToString());

                procParams[2] = new ProcedureParameters(
                    "@PedProdutoID",
                    System.Data.SqlDbType.Int,
                    pedProdutoId.intValue() == 0 ? DBNull.Value : (Object)pedProdutoId.ToString());

                procParams[3] = new ProcedureParameters(
                    "@PedLoteID",
                    System.Data.SqlDbType.Int,
                    pedLoteId.intValue() == 0 ? DBNull.Value : (Object)pedLoteId.ToString());

                procParams[4] = new ProcedureParameters(
                    "@AsstecID",
                    System.Data.SqlDbType.Int,
                    asstecId.intValue() == 0 ? DBNull.Value : (Object)asstecId.ToString());

                procParams[5] = new ProcedureParameters(
                    "@TipoResultado",
                    System.Data.SqlDbType.Int,
                    tipoResultado);

                DataInterfaceObj.execNonQueryProcedure(
                      "sp_Pedido_AsstecNumeroSerie",
                      procParams);
            }

            catch (System.Exception exception)
            {

                erros += exception.Message + " " + " \r\n ";
            }

            sql = "";


            sql = "DECLARE @TransacaoID INT, @PedidoID INT, @TipoPedidoID INT, @AsstecDuplicada INT, @AsstecID INT " +
                   "SELECT @TransacaoID = b.OperacaoID, @TipoPedidoID = a.TipoPedidoID, @AsstecID = e.AsstecID " +
                        "FROM Pedidos a WITH(NOLOCK) " +
                        "INNER JOIN   Operacoes b WITH(NOLOCK) ON (a.TransacaoID = b.OperacaoID) " +
                        "INNER JOIN   NumerosSerie_Movimento c WITH(NOLOCK) ON (a.PedidoID = c.PedidoID) " +
                        "INNER JOIN NumerosSerie d WITH(NOLOCK) ON (c.NumeroSerieID = d.NumeroSerieID) " +
                        "INNER JOIN Asstec e WITH(NOLOCK) ON (d.NumeroSerie = e.NumeroSerie) " +
                        "WHERE (a.PedidoID =" + pedidoId + ") " +
                   "IF (@TransacaoID IN (511, 512 , 515, 516, 521, 523, 525, 527)  AND @TipoPedidoID IN (601, 602)) " +
                        "AND (((SELECT COUNT(*) " +
                                    "FROM NumerosSerie_Movimento a WITH(NOLOCK) " +
                                        "INNER JOIN Pedidos b WITH(NOLOCK) ON (a.PedidoID = b.PedidoID) " +
                                    "WHERE AsstecID = @AsstecID AND b.PedidoID <> " + pedidoId + " AND (b.EstadoID < 31 AND b.TipoPedidoID = 602)) > 0) " +
                        "AND ((SELECT COUNT(*) " +
                                    "FROM NumerosSerie_Movimento a WITH(NOLOCK) " +
                                        "INNER JOIN Pedidos b WITH(NOLOCK) ON (a.PedidoID = b.PedidoID) " +
                                    "WHERE AsstecID = @AsstecID AND b.TipoPedidoID = 602) > 0)) " +
                   "SELECT COUNT(b.NumeroSerie) AS AsstecDuplicada " +
                       "FROM NumerosSerie_Movimento a WITH(NOLOCK) " +
                            "INNER JOIN NumerosSerie b WITH(NOLOCK) ON (a.NumeroSerieID = b.NumeroSerieID) " +
                            "INNER JOIN Pedidos c ON (a.PedidoID = c.PedidoID) " +
                       "WHERE c.TipoPedidoID = 602 AND a.PedidoID = @PedidoID " +
                            "AND b.NumeroSerie NOT IN (SELECT a.NumeroSerie " +
                                                            "FROM Asstec a WITH(NOLOCK) " +
                                                                "INNER JOIN NumerosSerie_Movimento c WITH(NOLOCK) ON (b.NumeroSerieID = c.NumeroSerieID) " +
                                                                "INNER JOIN Pedidos d ON (c.PedidoID = d.PedidoID) " +
                                                            "WHERE (c.PedidoID <> @PedidoID AND d.EstadoID < 31 AND a.AsstecID IS NOT NULL AND d.TipoPedidoID = 602) " +
                                                                    "OR (c.PedidoID <> @PedidoID AND d.EstadoID < 32 AND a.AsstecID IS NOT NULL AND d.TipoPedidoID = 601)) " +
                    "ELSE " +
                    "SELECT 1 AS AsstecDuplicada ";


            AsstecDuplicada = DataInterfaceObj.ExecuteSQLCommand(sql);

            bStartModal = new java.lang.Boolean(true);

            if (tipoResultado.intValue() == 2)
            {
                sql = "DECLARE @Temp TABLE (fldID INT) "+
                        " INSERT INTO @Temp SELECT Movimentos.NumMovimentoID AS fldId " +
                        "FROM NumerosSerie_Movimento Movimentos WITH(NOLOCK) " +
                        "WHERE (Movimentos.PedidoID = " + pedidoId + " AND Movimentos.AsstecID IS NULL) " +
                    "UNION ALL " +
                    "SELECT Movimentos.PedProdutoID AS fldID " +
                        "FROM Pedidos_ProdutosSeparados Movimentos WITH(NOLOCK) " +
                        "WHERE (Movimentos.PedidoID =  " + pedidoId + " AND Movimentos.AsstecID IS NULL) " +
                    "UNION ALL " +
                    "SELECT Movimentos.PedProdutoID AS fldID " +
                        "FROM Pedidos_ProdutosLote Movimentos WITH(NOLOCK) " +
                        "WHERE (Movimentos.PedidoID =  " + pedidoId + " AND Movimentos.AsstecID IS NULL) " +
                        " SELECT COUNT (*) AS fldID FROM @Temp ";

                sqlMovimentacao = DataInterfaceObj.ExecuteSQLCommand(sql);

                if (sqlMovimentacao == 0)
                {
                    bStartModal = new java.lang.Boolean(false);
                }

            }
        }
    }
}