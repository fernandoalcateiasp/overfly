<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="emailfabricantes.aspx.cs" Inherits="Overfly3.modcomercial.submodpedidos.frmpedidos.serverside.emailfabricantes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
	<head runat="server">
			<title>Packing List</title>
	</head>

	<body>
		<p style='font-size:8pt;font-family:arial'>
			<table border="0" cellpadding="0" cellspacing="0" style="font-size:8pt;font-family:arial">
				<tr>
					<td align='center' valign='bottom' style='font-size:16pt;font-family:arial;width=650'>
						<% Response.Write(Replicate(" ", 67)); %>
						<b>packing list</b>
					</td>
				</tr>
			</table>
				
			<% Response.Write(Replicate("_", 109)); %>
			<b>
				Order <% Response.Write(nPedidoID); %>
				&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
				Customer: <% Response.Write(sNomePessoa); %>
				<% if(nInvoiceID != null && !nInvoiceID.intValue() == 0) { %>
					&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
					Invoice <% Response.Write(nInvoiceID); %>
					&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
					<% Response.Write(sInvoiceDate); %>
				<%}%>

			</b>
			<b>
				Country of purchase: <% Response.Write(sPaisEmpresa); %>
				&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCountry 
				originated from: <% Response.Write(sPaisEmpresa); %> 
			</b>
				
		    <% Response.Write(Replicate("_", 109)); %> 
			<table border=0 cellpadding=1 cellspacing=4 style='font-size:8pt;font-family:arial'>
				<tr>
					<td align='right'><b>id</b></td>
					<td><b>Product</b></td>
					<td><b>Model</b></td>
					<td><b>Qty</b></td>
					<td align='right'><b>Net Kg</b></td>
					<td align='right'><b>Gross Kg</b></td>
					<td><b>ID</b></td>
					<td><b>Manufactorer</b></td>
				</tr>

				<% 
				if(Detail01.Tables[0].Rows.Count > 0)
				{
					for(int i = 0; i < Detail01.Tables[0].Rows.Count; i++)
					{
				%>
				<tr>
					<td align='right'>
						<% Response.Write(Detail01.Tables[0].Rows[i]["ProdutoID"]); %> 
					</td>
					<td>
						<% Response.Write(Detail01.Tables[0].Rows[i]["Produto"]); %> 
					</td>
					<td>
						<% Response.Write(Detail01.Tables[0].Rows[i]["Modelo"]); %> 
					</td>
					<td align='right'>
						<% Response.Write(Detail01.Tables[0].Rows[i]["Quantidade"]); %> 
					</td>
					<td align='right'>
						<% Response.Write(Detail01.Tables[0].Rows[i]["PesoLiquido"]); %> 
					</td>
					<td align='right'>
						<% Response.Write(Detail01.Tables[0].Rows[i]["PesoBruto"]); %> 
					</td>
					<td align='right'>
						<% Response.Write(Detail01.Tables[0].Rows[i]["PesoBruto"]); %> 
					</td>
					<td align='right'>
						<% Response.Write(Detail01.Tables[0].Rows[i]["FabricanteID"]); %> 
					</td>
					<td>
						<% Response.Write(Detail01.Tables[0].Rows[i]["Fabricante"]); %> 
					</td>
				</tr>
				<%			
					}
				}
				%>
			</table>
	
			<% Response.Write(Replicate("_", 109)); %> 
			<p style='font-size:12pt;font-family:arial;font-weight:bold'>
				Manufactorer Details
			</p>
					
			<table border='0' cellpadding='1' cellspacing='4' style='font-size:8pt;font-family:arial'>
				<tr>
					<td align='right'><b>ID</b></td>
					<td><b>Manufactorer</b></td>
					<td><b>Address</b></td>
					<td><b>City</b></td>
					<td><b>Country</b></td>
				</tr>
    
				<% 
				if(Detail02.Tables[0].Rows.Count > 0)
				{
					for(int i = 0; i < Detail02.Tables[0].Rows.Count; i++)
					{
				%>
				<tr>
					<td align='right'>
						<% Response.Write(Detail02.Tables[0].Rows[i]["FabricanteID"]); %> 
					</td>
					<td>
						<% Response.Write(Detail02.Tables[0].Rows[i]["Fabricante"]); %> 
					</td>
					<td>
						<% Response.Write(Detail02.Tables[0].Rows[i]["Endereco"]); %> 
					</td>
					<td>
						<% Response.Write(Detail02.Tables[0].Rows[i]["Cidade"]); %> 
					</td>
					<td>
						<% Response.Write(Detail02.Tables[0].Rows[i]["Pais"]); %> 
					</td>
				</tr>
				<%			
					}
				}
				%>
			</table>
		</p>
	</body>
</html>
