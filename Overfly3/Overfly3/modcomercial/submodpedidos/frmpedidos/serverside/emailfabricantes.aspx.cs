using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using OVERFLYSVRCFGLib;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class emailfabricantes : System.Web.UI.OverflyPage
	{
		private Integer empresaID;
		private Integer pedidoID;
		private Integer invoiceID;
		private string invoiceDate;
		private string paisEmpresa;
		private string nomePessoa;
		private string mailBody;

		private DataSet detalhe1 = null;
		private DataSet detalhe2 = null;
		private DataSet empresa = null;
		private string pagesURLRoot;
		private string sURLRoot;
		
		private Integer zero = new Integer(0);

		
		protected override void PageLoad(object sender, EventArgs e)
		{
			OverflyMTS objSvrCfg = new OverflyMTS();
			
			pagesURLRoot = objSvrCfg.PagesURLRoot(
				System.Configuration.ConfigurationManager.AppSettings["application"]);
			
			if(!pagesURLRoot.StartsWith("http://www"))
			{
				sURLRoot = "http://localhost/overfly3";
			}
			else
			{
				sURLRoot = pagesURLRoot;
			}
		}
		
		
		public string PagesURLRoot
		{
			get { return pagesURLRoot; }
		}

		private string URLRoot
		{
			get { return sURLRoot; }
		}
		
		// Propriedade Empresa
		public DataSet Empresa
		{
			get
			{
				if(empresa == null)
				{
					empresa = DataInterfaceObj.getRemoteData(
						"SELECT TOP 1 " +
							"b.NomeInternacional, " +
							"(" +
								"SELECT TOP 1 " +
									"c.Nome " +
								"FROM Pessoas c WITH(NOLOCK), " +
									"Pedidos d WITH(NOLOCK) " +
								"WHERE c.PessoaID=d.PessoaID AND " +
									"d.PedidoID=" + pedidoID + 
							") AS NomePessoa " +
						"FROM Pessoas_Enderecos a WITH(NOLOCK), " +
							"Localidades b WITH(NOLOCK) " +
						"WHERE a.PessoaID = " + empresaID + " AND " +
							"a.PaisID=b.LocalidadeID " +
						"ORDER BY a.EndFaturamento DESC, Ordem"
					);
				}
				
				return empresa;
			}
		}
		
		// Propriedade Detalhe 1 - Fatura
		public DataSet Detalhe1
		{
			get
			{
				if(detalhe1 == null)
				{
					// Roda a procedure sp_Pedido_Fabricantes
					ProcedureParameters[] procParams = new ProcedureParameters[6];

					procParams[0] = new ProcedureParameters(
						"@PedidoID",
						System.Data.SqlDbType.Int,
						pedidoID.ToString());

					procParams[1] = new ProcedureParameters(
						"@ProdutoID",
						System.Data.SqlDbType.Int,
						DBNull.Value);
					
					procParams[2] = new ProcedureParameters(
						"@Quantidade",
						System.Data.SqlDbType.Int,
						DBNull.Value);
					
					procParams[3] = new ProcedureParameters(
						"@FabricanteID",
						System.Data.SqlDbType.Int,
						DBNull.Value);
					
					procParams[4] = new ProcedureParameters(
						"@TipoResultado",
						System.Data.SqlDbType.Int,
						"1");
					
					procParams[5] = new ProcedureParameters(
						"@Resultado",
						System.Data.SqlDbType.VarChar,
						DBNull.Value);
					procParams[5].Length = 8000;
					
					
					detalhe1 = DataInterfaceObj.execQueryProcedure(
						"sp_Pedido_Fabricantes",
						procParams
					);
				}				
				
				return detalhe1;
			}
		}

		// Propriedade Detalhe 2
		public DataSet Detalhe2
		{
			get
			{
				if(detalhe2 == null)
				{
					detalhe2 = DataInterfaceObj.getRemoteData(
						"SELECT " + pedidoID + " AS _PedidoID, b.PessoaID AS FabricanteID, " + 
							"b.Nome AS Fabricante, " +
							"(ISNULL(c.Numero + SPACE(1), SPACE(0)) + " +
								"ISNULL(c.Endereco + SPACE(1), SPACE(0)) + " +
								"ISNULL(c.Complemento + SPACE(1), SPACE(0)) + " +
								"ISNULL(c.Bairro + SPACE(1), SPACE(0)) + " +
								"ISNULL(c.CEP + SPACE(1), SPACE(0))) AS Endereco, " +
								"ISNULL(d.NomeInternacional, d.Localidade) " +
							"AS Cidade, " +
							"e.NomeInternacional AS Pais " +
						"FROM Pedidos_Fabricantes a WITH(NOLOCK), Pessoas b WITH(NOLOCK), " +
							"Pessoas_Enderecos c WITH(NOLOCK), Localidades d WITH(NOLOCK), " +
							"Localidades e WITH(NOLOCK) " +
						"WHERE (a.PedidoID = " + pedidoID + " AND a.FabricanteID = b.PessoaID AND " +
							"b.PessoaID = c.PessoaID AND c.EndFaturamento = 1 AND c.Ordem = 1 AND " +
							"c.CidadeID = d.LocalidadeID AND c.PaisID = e.LocalidadeID) " +
						"GROUP BY b.PessoaID, b.Nome, c.Numero, c.Endereco, c.Complemento, " +
							"c.Bairro, c.CEP, d.NomeInternacional, d.Localidade, e.NomeInternacional " +
						"ORDER BY b.PessoaID"
					);
				}
				
				return detalhe2;
			}
		}
				
		public Integer nEmpresaID
		{
			get { return empresaID; }
			set { empresaID = value != null ? value : zero; }
		}
		
		public Integer nPedidoID
		{
			get { return pedidoID; }
			set { pedidoID = value != null ? value : zero; }
		}
		
		public Integer nInvoiceID
		{
			get { return invoiceID; }
			set { invoiceID = value != null ? value : zero; }
		}
		
		public string sInvoiceDate
		{
			get { return invoiceDate; }
			set { invoiceDate = value != null ? value : ""; }
		}
		
		public string sPaisEmpresa
		{
			get { return paisEmpresa; }
			set { paisEmpresa = value != null ? value : ""; }
		}
		
		public string sNomePessoa
		{
			get { return nomePessoa; }
			set { nomePessoa = value != null ? value : ""; }
		}

		public string sMailBody
		{
			get { return mailBody; }
			set { mailBody = value != null ? value : ""; }
		}
		
		// Fun��o de repeti��o de caracter
		public string Replicate(string val, int n)
		{
			string result = "";
		    
			if(val == null)
				return "";

			for(int i = 0; i < n; i++)
				result += val;

			return result;
		}
		
	}
}
