using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.Attributes;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class anteciparentrega : System.Web.UI.OverflyPage
    {
        private string mensagem;
        private string resultado;

        private Integer usuarioID;
        protected Integer nUsuarioID
        {
            get { return usuarioID; }
            set { usuarioID = value; }
        }

        private Integer[] pedItemID;
        protected Integer[] nPedItemID
        {
            get { return pedItemID; }
            set { pedItemID = value; }
        }

        private Integer[] quantidadeAntecipar;
        protected Integer[] nQuantidadeAntecipar
        {
            get { return quantidadeAntecipar; }
            set { quantidadeAntecipar = value; }
        }

        private Integer pedidoID;
        protected Integer nPedidoID
        {
            get { return pedidoID; }
            set { pedidoID = value; }
        }

        private Integer tipoResultado;
        protected Integer nTipoResultado
        {
            get { return tipoResultado; }
            set { tipoResultado = value; }
        }

        protected void AnteciparEntrega()
        {
            String TypeName = "dbo.tpPedidosItensAntecipar";

            DataTable dt = new DataTable();
            //dt.Columns.Add("PedItemAnteciparID", typeof(int));
            dt.Columns.Add("PedItemID", typeof(int));
            dt.Columns.Add("Quantidade", typeof(int));
            
            if (pedItemID != null)
            {
                for (int i = 0; i < pedItemID.Length; i++)
                {
                    dt.Rows.Add(pedItemID[i], quantidadeAntecipar[i]);
                }
            }

            ProcedureParameters[] procParams = new ProcedureParameters[6];
            procParams[0] = new ProcedureParameters("@PedidosItensAntecipar", SqlDbType.Structured, dt);
            
            procParams[1] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                usuarioID.ToString());

            if (pedidoID != null) {
		         procParams[2] = new ProcedureParameters(
                    "@PedidoID",
                    System.Data.SqlDbType.Int,
                    pedidoID.ToString());
	        }
            else {
		         procParams[2] = new ProcedureParameters(
                    "@PedidoID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value);
	        }
            
            procParams[3] = new ProcedureParameters(
                "@TipoResultado",
                System.Data.SqlDbType.Int,
                tipoResultado.ToString());

            procParams[4] = new ProcedureParameters(
                "@Mensagem",
                SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.Output, 8000);

            procParams[5] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.Output);

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Pedido_Antecipar",
                procParams,
                TypeName);

            mensagem = ((procParams[4].Data != DBNull.Value) ? (string)procParams[4].Data : null);
            resultado = ((procParams[5].Data != DBNull.Value) ? procParams[5].Data.ToString() : null);
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            AnteciparEntrega();

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "SELECT " + (mensagem != null ? ("'" + mensagem + "'") : "NULL") + " AS Mensagem, " +
                            (resultado != null ? resultado : "NULL")             + " AS Resultado "
            ));
        }
    }
}
