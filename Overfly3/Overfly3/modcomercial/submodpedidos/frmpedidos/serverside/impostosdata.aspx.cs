using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class impostosdata : System.Web.UI.OverflyPage
    {
        private Integer pedidoID;
        private Integer modo;
        private Integer impostoID;
        private string impostosql;

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(
                DataInterfaceObj.getRemoteData(GetSQL)
            );
        }

        public Integer nPedidoID
        {
            get { return pedidoID; }
            set { pedidoID = value != null ? value : new Integer(0); }
        }
        protected Integer nModo
        {
            set { modo = value != null ? value : new Integer(0); }
        }
        protected Integer nImpostoID
        {
            set { impostoID = value != null ? value : new Integer(-1); }
        }

        protected string GetSQL
        {
            get
            {
                string sql = "";

                if (impostoID.intValue() >= 0)
                {
                    impostosql = "AND c.ImpostoID =" + impostoID + "  ";
                }
                else
                    impostosql = "";

                if (modo.intValue() == 0)
                {
                    sql = "SELECT " +
                           "' ' AS Ordem, " +
                           "' ' AS ProdutoID, " +
                           "'Impostos do Pedido' AS Conceito, " +
                           "' ' AS NCM, " +
                           "c.ImpostoID, " +
                           "d.Imposto, " +
                           "0 AS Carga, " +
                           "CONVERT(NUMERIC(5,2), " +
                           "(dbo.fn_DivideZero(c.ValorImposto,c.BaseCalculo)*100)) AS Aliquota, " +
                           "0 AS Base, " +
                           "c.ValorImposto, " +
                           "c.BaseCalculo, " +
                           "1 AS TipoImposto, " +
                           "(CASE WHEN c.ImpostoID IN (9,25) THEN ABS(d.AlteraAliquota) ELSE ABS(~d.AlteraAliquota)END) AS AlteraAliquota, " +
                           "ABS(d.EsquemaCredito) AS EsquemaCredito, " +
                           "ABS(d.InclusoPreco) AS InclusoPreco, " +
                           "c.PedImpostoID, " +
                           "0 AS OK, " +
                           "' ' AS CFOPID " +
                        "FROM " +
                           "Pedidos_Impostos c WITH(NOLOCK), " +
                           "Conceitos d WITH(NOLOCK) " +
                        "WHERE " +
                           "c.PedidoID="  + (nPedidoID) + " AND " +
                           "c.ImpostoID = d.ConceitoID " +
                        "UNION ALL " +
                         "SELECT " +
                           "STR(a.Ordem, 3) AS Ordem, " +
                           "STR(a.ProdutoID, 7) AS ProdutoID, " +
                           "b.Conceito, " +
                           "Classificacao.Conceito AS NCM, " +
                           "c.ImpostoID, " +
                           "d.Imposto, " +
                           "dbo.fn_PedidoItem_PercentuaisImpostos(c.PedItemID, c.ImpostoID, 1) AS Carga, " +
                           "dbo.fn_PedidoItem_PercentuaisImpostos(c.PedItemID, c.ImpostoID, 2) AS Aliquota, " +
                           "dbo.fn_PedidoItem_PercentuaisImpostos(c.PedItemID, c.ImpostoID, 3) AS Base, " +
                           "c.ValorImposto, " +
                           "c.BaseCalculo, " +
                           "2 AS TipoImposto, " +
                           "(CASE WHEN c.ImpostoID IN (9,25) THEN ABS(d.AlteraAliquota) ELSE ABS(~d.AlteraAliquota)END) AS AlteraAliquota, " +
                           "ABS(d.EsquemaCredito) AS EsquemaCredito, " +
                           "ABS(d.InclusoPreco) AS InclusoPreco, " +
                           "c.PedImpostoID AS PedImpostoID, " +
                           "0 AS OK, " +
                           "a.CFOPID AS CFOPID " +
                         "FROM " +
                           "Pedidos_Itens a WITH(NOLOCK) " +
                           "INNER JOIN " +
                           "Conceitos b WITH(NOLOCK) " +
                           "ON a.ProdutoID = b.ConceitoID " +
                           "INNER JOIN " +
                           "Pedidos WITH(NOLOCK) " +
                           "ON a.PedidoID = Pedidos.PedidoID " +
                           "INNER JOIN " +
                           "Pedidos_Itens_Impostos c WITH(NOLOCK) " +
                           "ON a.PedItemID = c.PedItemID " +
                           "INNER JOIN " +
                           "Conceitos d WITH(NOLOCK) " +
                           "ON c.ImpostoID=d.ConceitoID " +
                           "LEFT JOIN " +
                           "RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) " +
                           "ON ProdutosEmpresa.SujeitoID = Pedidos.EmpresaID AND " +
                           "  (ProdutosEmpresa.ObjetoID = a.ProdutoID) AND (ProdutosEmpresa.TipoRelacaoID = 61) " +
                           "LEFT JOIN " +
                           "Conceitos Classificacao WITH(NOLOCK) " +
                           "ON ProdutosEmpresa.ClassificacaoFiscalID = Classificacao.ConceitoID " +
                         "WHERE " +
                           "a.PedidoID=" + pedidoID +
                           impostosql +
                         " ORDER BY " +
                            "TipoImposto, Ordem, ProdutoID, c.ImpostoID";
                }

                else if (modo.intValue() == 1)
                {
                    sql = "SELECT " +
                            "STR(a.Ordem, 3) AS Ordem, " +
                            "STR(a.ProdutoID, 7) AS ProdutoID, " +
                            "b.Conceito, " +
                            "Classificacao.Conceito AS NCM, " +
                            "c.ImpostoID, " +
                            "d.Imposto, " +
                            "0 AS Aliquota, " +
                            "c.ValorImposto, " +
                            "c.BaseCalculo, " +
                            "2 AS TipoImposto, " +
                            "a.CFOPID, " +
                            "CFOP.AlteraImpostosComplementares AS CFOPAIC, " +
                            "Impostos.AlteraImpostosComplementares AS ImpostoAIC, " +
                            "c.PedImpostoID AS PedImpostoID, " +
                            "0 AS OK " +
                        "FROM " +
                            "Pedidos_Itens a WITH(NOLOCK), " +
                            "Conceitos b WITH(NOLOCK), " +
                            "Pedidos WITH(NOLOCK), " +
                            "Pedidos_Itens_ImpostosComplementares c WITH(NOLOCK), " +
                            "Conceitos d WITH(NOLOCK), " +
                            "RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), " +
                            "Conceitos Classificacao WITH(NOLOCK), " +
                            "Operacoes CFOP WITH(NOLOCK), " +
                            "Conceitos Impostos WITH(NOLOCK) " +
                        "WHERE " +
                            "a.PedidoID=" + (pedidoID) +
                            impostosql + " AND " +
                            "a.ProdutoID=b.ConceitoID AND " +
                            "a.PedItemID=c.PedItemID AND " +
                            "a.PedidoID = Pedidos.PedidoID AND " +
                            "ProdutosEmpresa.SujeitoID = Pedidos.EmpresaID AND " +
                            "ProdutosEmpresa.ObjetoID = a.ProdutoID AND " +
                            "ProdutosEmpresa.TipoRelacaoID = 61 AND " +
                            "ProdutosEmpresa.ClassificacaoFiscalID = Classificacao.ConceitoID AND " +
                            "c.ImpostoID=d.ConceitoID AND " +
                            "a.CFOPID = CFOP.OperacaoID AND " +
                            "c.ImpostoID = Impostos.ConceitoID " +
                        "ORDER BY " +
                            "TipoImposto, Ordem, ProdutoID, c.ImpostoID";
                }

                else if (modo.intValue() == 2)
                {
                    sql = "SELECT " +
                           "' ' AS Ordem, " +
                           "' ' AS ProdutoID, " +
                           "'Impostos do Pedido' AS Conceito, " +
                           "' ' AS NCM," +
                           "' ' AS CST," +
                           "' ' AS DescricaoCST, " +
                           "' ' AS CFOP, " +
                           "c.ImpostoID," +
                           "d.Imposto," +
                           "0 AS Carga," +
                           "CONVERT(NUMERIC(5,2)," +
                           "(dbo.fn_DivideZero(SUM(c.ValorImposto),SUM(c.BaseCalculo))*100)) AS Aliquota, " +
                           "0 AS Base," +
                           "SUM(c.ValorImposto) AS ValorImposto, " +
                           "SUM(c.BaseCalculo) AS BaseCalculo, " +
                           "1 AS TipoImposto,  " +
                           "(CASE WHEN c.ImpostoID IN (9,25) THEN ABS(d.AlteraAliquota) ELSE ABS(~d.AlteraAliquota)END) AS AlteraAliquota, " +
                           "ABS(d.EsquemaCredito) AS EsquemaCredito," +
                           "ABS(d.InclusoPreco) AS InclusoPreco," +
                           "NULL AS PedImpostoID," +
                           "0 AS OK," +
                           "' ' AS CFOPID, " +
                           "c.ImpostoApuracao AS ImpostoApuracao " +
                        "FROM " +
                           "Pedidos_Itens aa  WITH(NOLOCK), " +
                           "Pedidos_Itens_Impostos c WITH(NOLOCK), " +
                           "Conceitos d WITH(NOLOCK) " +
                        "WHERE " +
                           "aa.PedidoID =" + (pedidoID) + " AND " +
                            "c.PedItemID = aa.PedItemID AND " +
                           "c.ImpostoID = d.ConceitoID " +
                        "GROUP BY c.ImpostoID, d.ConceitoID, d.Imposto, d.AlteraAliquota, d.EsquemaCredito, d.InclusoPreco,  c.ImpostoApuracao " +
                        "UNION ALL " +
                         "SELECT " +
                           "STR(a.Ordem, 3) AS Ordem, " +
                           "STR(a.ProdutoID, 7) AS ProdutoID, " +
                           "b.Conceito, " +
                           "Classificacao.Conceito AS NCM, " +
                           "CONVERT(VARCHAR, dbo.fn_TipoAuxiliar_Item(c.CSTID, 2)) AS CST, " +
                           "dbo.fn_TipoAuxiliar_Item(c.CSTID, 0) AS DescricaoCST, " +
                           "Ope.Operacao AS CFOP, " +
                           "c.ImpostoID, " +
                           "d.Imposto, " +
                           "dbo.fn_PedidoItem_PercentuaisImpostos(c.PedItemID, c.ImpostoID, 1) AS Carga, " +
                           "dbo.fn_PedidoItem_PercentuaisImpostos(c.PedItemID, c.ImpostoID, 2) AS Aliquota, " +
                           "dbo.fn_PedidoItem_PercentuaisImpostos(c.PedItemID, c.ImpostoID, 3) AS Base, " +
                           "c.ValorImposto, " +
                           "c.BaseCalculo, " +
                           "2 AS TipoImposto, " +
                           "(CASE WHEN c.ImpostoID IN (9,25) THEN ABS(d.AlteraAliquota) ELSE ABS(~d.AlteraAliquota)END) AS AlteraAliquota, " +
                           "ABS(d.EsquemaCredito) AS EsquemaCredito, " +
                           "ABS(d.InclusoPreco) AS InclusoPreco, " +
                           "c.PedImpostoID AS PedImpostoID, " +
                           "0 AS OK, " +
                           "CONVERT(VARCHAR(30),a.CFOPID) AS CFOPID, " +
                           "c.ImpostoApuracao AS ImpostoApuracao " +
                         "FROM " +
                           "Pedidos_Itens a WITH(NOLOCK) " +
                           "INNER JOIN " +
                           "Conceitos b WITH(NOLOCK) " +
                           "ON a.ProdutoID = b.ConceitoID " +
                           "INNER JOIN " +
                           "Pedidos WITH(NOLOCK) " +
                           "ON a.PedidoID = Pedidos.PedidoID " +
                           "INNER JOIN " +
                           "Pedidos_Itens_Impostos c WITH(NOLOCK) " +
                           "ON a.PedItemID = c.PedItemID " +
                           "INNER JOIN " +
                           "Conceitos d WITH(NOLOCK) " +
                           "ON c.ImpostoID=d.ConceitoID " +
                           "LEFT JOIN " +
                           "RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) " +
                           "ON ProdutosEmpresa.SujeitoID = Pedidos.EmpresaID AND " +
                           "  (ProdutosEmpresa.ObjetoID = a.ProdutoID) AND (ProdutosEmpresa.TipoRelacaoID = 61) " +
                           "LEFT JOIN " +
                           "Conceitos Classificacao WITH(NOLOCK) " +
                           "ON ProdutosEmpresa.ClassificacaoFiscalID = Classificacao.ConceitoID " +
                           "LEFT JOIN Operacoes Ope WITH(NOLOCK)" +
                           "ON a.CFOPID = Ope.OperacaoID " +
                         "WHERE " +
                           "a.PedidoID=" + (pedidoID) +
                           impostosql +
                         " ORDER BY " +
                "TipoImposto, Ordem, ProdutoID, c.ImpostoID";
                }
                return sql;
            }
        }
    }
}
