using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class gravatotalgross : System.Web.UI.OverflyPage
	{
		private static Integer zero = new Integer(0);
		
		private Integer pedidoId;
		private string totalGross;

		protected override void PageLoad(object sender, EventArgs e)
		{
			int fldresp;

			fldresp = DataInterfaceObj.ExecuteSQLCommand(
				"UPDATE Pedidos SET TotalGrossSKID = " + totalGross + " WHERE PedidoID =" + pedidoId
			);

			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select " + fldresp + " as fldresp"
				)
			);
		}

		public Integer PedidoID
		{
			get { return pedidoId; }
			set { pedidoId = value != null ? value : zero; }
		}
		
		public string TotalGross
		{
			get { return totalGross; }
            set { totalGross = value; }
		}
	}
}
