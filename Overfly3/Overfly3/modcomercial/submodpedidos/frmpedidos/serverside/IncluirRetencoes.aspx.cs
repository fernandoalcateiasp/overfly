using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class IncluirRetencoes : System.Web.UI.OverflyPage
    {
        
        protected static Integer Zero = new Integer(0);

        private Integer[] pedidoId;
        private Integer[] impostoID;

        protected Integer[] nPedidoID
        {
            set
            {
                pedidoId = value;

                for (int i = 0; i < pedidoId.Length; i++)
                {
                    if (pedidoId[i] == null)
                        pedidoId[i] = Zero;
                }
            }
        }
        protected Integer[] nImpostoID
        {
            set
            {
                impostoID = value;

                for (int i = 0; i < impostoID.Length; i++)
                {
                    if (impostoID[i] == null)
                        impostoID[i] = Zero;
                }
            }
        }
       

        // Pega os parāmetros para a procedure sp_Pedido_Financeiro_Retencao
        protected void PedidoFinanceiroRetencao()
        {
            
            ProcedureParameters[] procParams = new ProcedureParameters[2];
            for (int i = 0; i < impostoID.Length; i++)
            {
                procParams[0] = new ProcedureParameters(
                    "@PedidoID",
                    System.Data.SqlDbType.Int,
                   ((pedidoId[i].intValue() == 0) ? System.DBNull.Value : (Object)pedidoId[i].intValue()));

                procParams[1] = new ProcedureParameters(
                    "@ImpostoID",
                    System.Data.SqlDbType.Int,
                    ((impostoID[i].intValue() == 0) ? System.DBNull.Value : (Object)impostoID[i].intValue()));

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_Pedido_Financeiro_Retencao",
                    procParams);
            }
        }        
        protected override void PageLoad(object sender, EventArgs e)
        {
            PedidoFinanceiroRetencao();            

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select 0 as fldresp"
                )
            );
        }
    }
}
