using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.Attributes;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class aprovarcampanha : System.Web.UI.OverflyPage
    {
        private Integer pedIteCampanhaID;
        private Integer usuarioID;
        private java.lang.Boolean aprovar;
        private java.lang.Boolean direito;
        private string resultado;

        protected override void PageLoad(object sender, EventArgs e)
        {
            PedidoItemCampanhaAprovar();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select ' " + resultado + "' as Resultado"
                )
            );
        }

        protected void PedidoItemCampanhaAprovar()
        {
            // Roda a procedure sp_PedidoItem_CampanhaAprovar
            ProcedureParameters[] procParams = new ProcedureParameters[5];

            procParams[0] = new ProcedureParameters(
                "@PedIteCampanhaID",
                System.Data.SqlDbType.Int,
                pedIteCampanhaID.ToString());

            procParams[1] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                usuarioID.ToString());

            procParams[2] = new ProcedureParameters(
                "@Aprovar",
                System.Data.SqlDbType.Bit,
                aprovar.booleanValue() ? 1 : 0);

            procParams[3] = new ProcedureParameters(
                "@Direito",
                System.Data.SqlDbType.Bit,
                direito.booleanValue() ? 1 : 0);

            procParams[4] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[4].Length = 8000;
            
            DataInterfaceObj.execNonQueryProcedure(
                "sp_PedidoItem_CampanhaAprovar",
                procParams);

            if (procParams[4].Data != DBNull.Value)
                resultado = procParams[4].Data.ToString();
        }

        protected Integer nPedIteCampanhaID
        {
            get { return pedIteCampanhaID; }
            set { pedIteCampanhaID = (value != null ? value : new Integer(0)); }
        }

        protected Integer nUsuarioID
        {
            get { return usuarioID; }
            set { usuarioID = (value != null ? value : new Integer(0)); }
        }

        protected java.lang.Boolean bAprovar
        {
            get { return aprovar; }
            set { aprovar = (value != null ? value : new java.lang.Boolean(false)); }
        }

        protected java.lang.Boolean bDireito
        {
            get { return direito; }
            set { direito = (value != null ? value : new java.lang.Boolean(false)); }
        }       
    }
}
