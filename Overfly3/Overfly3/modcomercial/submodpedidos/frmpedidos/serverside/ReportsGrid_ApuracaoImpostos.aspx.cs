using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.PrintJet
{
	public partial class ApuracaoImpostos : System.Web.UI.OverflyPage
	{
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey; 
        private int nLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["nLinguaLogada"]);
        private string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["sEmpresaFantasia"]);
        private string controle = Convert.ToString(HttpContext.Current.Request.Params["controle"]);
        private int nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["nEmpresaID"]);
        private string selImpostoID = Convert.ToString(HttpContext.Current.Request.Params["selImpostoID"]);
        private string chkDiferencialAliquota = Convert.ToString(HttpContext.Current.Request.Params["chkDiferencialAliquota"]);
        private string sDataInicio = Convert.ToString(HttpContext.Current.Request.Params["sDataInicio"]);
        private string sDataFim = Convert.ToString(HttpContext.Current.Request.Params["sDataFim"]);
        private string txtFiltro = Convert.ToString(HttpContext.Current.Request.Params["txtFiltro"]);

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                switch (controle)
                {
                    case "Excel":
                        listaExcelRateios();
                        break;
                }
            }
        }
        public void listaExcelRateios()
        {
            DateTime DataHoje = DateTime.Now;
             string _data = DataHoje.ToString("dd_MM_yyyy");

            if (nLinguaLogada != 246)
            {
                _data = DataHoje.ToString("MM_dd_yyyy");
            }

            string Title = "";

            Title = "Apura��o de Impostos_" + sEmpresaFantasia + "_" + _data;

            string strSQL = "EXEC sp_Pedido_ApuracaoImpostos " + nEmpresaID + ", " +
                           selImpostoID + ", " + chkDiferencialAliquota + ", NULL, NULL, '" + sDataInicio + "', '" +
                            sDataFim + "', " + txtFiltro + ", 2";


            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            Relatorio.CriarRelat�rio(Title.ToString(), arrSqlQuery, "BRA", "Excel");

            int Datateste = 0;
            bool nulo = false;

            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query est� vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            if (dsFin.Tables["Query1"].Rows.Count == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {
                Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, true,true);
                Relatorio.TabelaEnd();

                Relatorio.CriarPDF_Excel(Title, 2);
            }
        }
	}
}
