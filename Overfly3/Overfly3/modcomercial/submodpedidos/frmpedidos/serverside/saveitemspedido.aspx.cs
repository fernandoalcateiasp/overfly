using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class saveitemspedido : System.Web.UI.OverflyPage
    {
        private static Integer zero = new Integer(0);

        private Integer dataLen;
        private Integer pedidoId;
        private Integer estadoId;
        private Integer variacao;   
        private Integer dataElemLen;
        private string resultado;
        private Object[][] data;
        private char[] marks = new char[] { (char) 47 };
        private string[] values;
        private string dtAgendamento;
        string UrlDeOrigem;
        bool temDtVigencia;

        protected Integer nDataLen
        {
            get { return dataLen; }
            set { dataLen = value != null ? value : zero; }
        }

        protected Integer nPedidoID
        {
            get { return pedidoId; }
            set { pedidoId = value != null ? value : zero; }
        }

        protected Integer nEstadoID
        {
            get { return estadoId; }
            set { estadoId = value != null ? value : zero; }
        }

        protected Integer nVariacao
        {
            get { return variacao; }
            set { variacao = value != null ? value : zero; }
        }

        protected Integer nDataElemLen
        {
            get { return dataElemLen; }
            set { dataElemLen = value != null ? value : zero; }
        }

        protected Object[] aData
        {
            set
            {
                UrlDeOrigem = Request.UrlReferrer.AbsolutePath;

                if (UrlDeOrigem.IndexOf("modalincluiitens") > 0)
                    temDtVigencia = true;
                else
                    temDtVigencia = false;

                if (value != null)
                {
                    int i=0;
                    data = new Object[dataLen.intValue()][];
                    for (int j = 0; j < (dataLen.intValue()); j++)
                    {
                        data[j] = new Object[dataElemLen.intValue()];
                        int l = 0;
                        while(l < dataElemLen.intValue())
                        {
                            //values = value[j].ToString().Split(marks);

                            if ((temDtVigencia) && ((i == 15) || (i == 16)))
                            {
                                if (i == 15) //dtVigenciaServicoInicio
                                {
                                    if (value[i].ToString().Split(marks)[1] == "NULL")
                                        data[j][l] = value[i].ToString().Split(marks)[1]; //values[j][i];
                                    else
                                        data[j][l] = value[i].ToString().Split(marks)[1] + "/" + value[i].ToString().Split(marks)[2] + "/" + value[i].ToString().Split(marks)[3]; //values[j][i];
                                }
                                else if (i == 16) //dtVigenciaServicoFim
                                {
                                    if (value[i].ToString().Split(marks)[1] == "NULL")
                                        data[j][l] = value[i].ToString().Split(marks)[1]; //values[j][i];
                                    else
                                        data[j][l] = value[i].ToString().Split(marks)[1] + "/" + value[i].ToString().Split(marks)[2] + "/" + value[i].ToString().Split(marks)[3]; //values[j][i];
                                }
                            }
                            else
                                data[j][l] = value[i].ToString().Split(marks)[1]; //values[j][i];
                            i++;
                            l++;
                        }                        
                    }
                }
                else
                {
                    data = new Object[0][];
                }
            }
        }

        // Pega os parāmetros para a procedure sp_PedidoItem_Gerador
        protected void PedidoItemGerador()
        {   
            for (int i = 0; i <= (dataLen.intValue() - 1); i++)
            {
                int produtoId = Convert.ToInt32(data[i][0].ToString());
                int servicoId = Convert.ToInt32(data[i][14].ToString());
                int quantidade = Convert.ToInt32(data[i][3].ToString());
                           

                string ValorInterno = (data[i][6].ToString());
                if (ValorInterno.ToString() == "0")
                {
                    ValorInterno = "NULL";
                }

                string ValorRevenda = (data[i][7].ToString());
                if (ValorRevenda.ToString() == "0")
                {
                    ValorRevenda = "NULL";
                }
                
                string ValorUnitario= (data[i][8].ToString());
                if (ValorUnitario.ToString() == "0")
                {
                    ValorUnitario = "NULL";
                }

                int FinalidadeID = Convert.ToInt32(data[i][1].ToString());
                
                int PedItemReferenciaID = Convert.ToInt32(data[i][9].ToString());               

                int  PedItemEncomendaID = Convert.ToInt32(data[i][12].ToString());

                ProcedureParameters[] procParams = new ProcedureParameters[15];
                
                procParams[0] = new ProcedureParameters(
                    "@PedidoID",
                    System.Data.SqlDbType.Int,
                    pedidoId);

                procParams[1] = new ProcedureParameters(
                    "@ProdutoID",
                    System.Data.SqlDbType.Int,
                    ((produtoId == 0) ? System.DBNull.Value : (Object)produtoId));

                procParams[2] = new ProcedureParameters(
                    "@ServicoID",
                    System.Data.SqlDbType.Int,
                    ((servicoId == 0) ? System.DBNull.Value : (Object)servicoId));

                procParams[3] = new ProcedureParameters(
                    "@Quantidade",
                    System.Data.SqlDbType.Int,
                    quantidade);

                procParams[4] = new ProcedureParameters(
                    "@Valorinterno",
                    System.Data.SqlDbType.Money,
                    //  (object)ValorInterno.intValue());
                   ((ValorInterno.ToString() == "NULL") ? System.DBNull.Value : (Object)ValorInterno));         

                procParams[5] = new ProcedureParameters(
                    "@Valoricmsstinterno",
                    System.Data.SqlDbType.Money,
                    System.DBNull.Value);

                procParams[6] = new ProcedureParameters(
                    "@ValorRevenda",
                    System.Data.SqlDbType.Money,
                    ((ValorRevenda.ToString() == "NULL") ? System.DBNull.Value : (Object)ValorRevenda));

                procParams[7] = new ProcedureParameters(
                    "@ValorICMSSTRevenda",
                    System.Data.SqlDbType.Money,
                    System.DBNull.Value);

                procParams[8] = new ProcedureParameters(
                    "@ValorUnitario",
                    System.Data.SqlDbType.Money,
                    ((ValorUnitario.ToString() == "NULL") ? System.DBNull.Value : (Object)ValorUnitario));

                procParams[9] = new ProcedureParameters(
                    "@FinalidadeID",
                    System.Data.SqlDbType.Int,
                   ((FinalidadeID == 0) ? System.DBNull.Value : (Object)FinalidadeID));

                procParams[10] = new ProcedureParameters(
                    "@CFOPID",
                    System.Data.SqlDbType.Int,
                    System.DBNull.Value);

                procParams[11] = new ProcedureParameters(
                    "@PedItemReferenciaID",
                    System.Data.SqlDbType.Int,
                    ((PedItemReferenciaID== 0) ? System.DBNull.Value : (Object)PedItemReferenciaID));

                procParams[12] = new ProcedureParameters(
                    "@PedItemEncomendaID",
                    System.Data.SqlDbType.Int,
                   ((PedItemEncomendaID== 0) ? System.DBNull.Value : (Object)PedItemEncomendaID));

                procParams[13] = new ProcedureParameters(
                    "@TipoGerador",
                    System.Data.SqlDbType.Int,
                    1);

                procParams[14] = new ProcedureParameters(
                    "@NovoPedItemID",
                    System.Data.SqlDbType.Int,
                    null,
                    ParameterDirection.Output);

                try
                {
                    DataInterfaceObj.execNonQueryProcedure(
                        "sp_PedidoItem_Gerador",
                        procParams);

                    resultado = procParams[14].Data.ToString();
                }
                catch (System.Exception exception)
                {
                    resultado += exception.Message + " \r\n ";

                }
            }

        }

        /** Atualiza a tabela ValidacoesDados_Detalhes. */
        private void atualizaVigenciaServico(string pedItemID)
        {
            string sql = "";

            for (int i = 0; i <= (dataLen.intValue() - 1); i++)
            {
                string dtVigenciaServicoInicio = (data[i][15].ToString());
                if (dtVigenciaServicoInicio != "NULL")
                    dtVigenciaServicoInicio = "'" + dtVigenciaServicoInicio + "'";

                string dtVigenciaServicoFim = (data[i][16].ToString());
                if (dtVigenciaServicoFim != "NULL")
                    dtVigenciaServicoFim = "'" + dtVigenciaServicoFim + "'";

                sql =
                    " UPDATE Pedidos_Itens SET " +
                        " dtVigenciaServicoInicio = " + dtVigenciaServicoInicio + ", "  +
                        " dtVigenciaServicoFim = " + dtVigenciaServicoFim + " " +
                    " WHERE (PeditemID = " + pedItemID + ")";
            }

            DataInterfaceObj.ExecuteSQLCommand(sql);
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            PedidoItemGerador();

            if (temDtVigencia)
                atualizaVigenciaServico(resultado);
            
            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select " + ((resultado != null) ? (resultado.Length > 0 ? ("'" + resultado + "'") : "NULL") : " NULL ") + " as Resultado, 0 as fldErrorNumber, space(0) as fldErrorText " 
                )
            );
        }
    }
}