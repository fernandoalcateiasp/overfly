using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class consultaChaveAcesso : System.Web.UI.OverflyPage
	{
		private static Integer zero = new Integer(0);
		private java.lang.Boolean falso = new java.lang.Boolean(false);

		private string Numero;
        private string ChaveAcesso;
        private string UF;
        private string PedidoID;
        private string ChaveAcessoCNPJ;

        public string sNumero
		{
			get { return Numero; }
            set { Numero = value != null ? value : ""; }
		}

        public string sChaveAcesso
        {
            get { return ChaveAcesso; }
            set { ChaveAcesso = value != null ? value : ""; }
        }

        public string sChaveAcessoCNPJ
        {
            get { return ChaveAcessoCNPJ; }
            set { ChaveAcessoCNPJ = value != null ? value : ""; }
        }

        public string sUF
        {
            get { return UF; }
            set { UF = value != null ? value : ""; }
        }

        public string sPedidoID
        {
            get { return PedidoID; }
            set { PedidoID = value != null ? value : ""; }
        }
        
		protected DataSet consultarChaveNFe()
		{
            ProcedureParameters[] procParams = new ProcedureParameters[3];

			procParams[0] = new ProcedureParameters(
				"@PedidoID",
				System.Data.SqlDbType.Int,
				int.Parse(sPedidoID));

			procParams[1] = new ProcedureParameters(
				"@ChaveAcesso",
				System.Data.SqlDbType.VarChar,
				sChaveAcesso, 44);

			procParams[2] = new ProcedureParameters(
				"@Mensagem",
				System.Data.SqlDbType.VarChar,
				DBNull.Value,
				ParameterDirection.InputOutput);
			procParams[2].Length = 8000;

			DataInterfaceObj.execNonQueryProcedure(
				"sp_Pedido_ConfereXML",
				procParams);

			return DataInterfaceObj.getRemoteData(
				"SELECT " +
                    ((procParams[2].Data != DBNull.Value) ? "'" + procParams[2].Data.ToString() + "'" : " NULL ") + " AS NFeValidacao, " +
                    "dbo.fn_Digito_Calcula('" + sNumero + "',NULL,1,11,2,2,9,0,0,1,0,0,11,0) AS cDV, " +
					"'" + sNumero + "'" + " AS Numero, " +
					"'" + sChaveAcesso + "'" + " AS ChaveAcesso, " +
					"dbo.fn_Digito_Verifica(" + "'" + sChaveAcesso + "'" + ",NULL,44,1,11,2,2,9,0,0,1,0,0,11,0) AS ChaveAcessoOK, " +
							"dbo.fn_ExisteChaveDeAcesso('" + sChaveAcesso + "') as ChegouNota, " +
		            "dbo.fn_DocumentoFederal_SefazUF('" + sUF + "', '" + sChaveAcessoCNPJ + "') AS EhCNPJSefaz"
			);
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(consultarChaveNFe());
		}
	}
}
