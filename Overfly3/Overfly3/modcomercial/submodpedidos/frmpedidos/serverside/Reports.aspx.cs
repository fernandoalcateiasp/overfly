﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;

        public string[,] glb_aTranslate_EtiquetaEmbalagem;
        public string[,] glb_aTranslate_Proposta;
        public string[,] glb_ListaEmbalagens;

        private object[,] glb_aKey;
        private object[] glb_aFields;
        private object[] glb_aGroupBy;
        private object[,] glb_aOrderBy;

        private object[] glb_aDetailIndex;

        private object glb_strSQL_Select;
        private object glb_strSQL_From;
        private object glb_strSQL_Where1;
        private object glb_strSQL_Where2;

        private int RelatorioID;

        private int glb_nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);


        bool chkPedidosWeb = Convert.ToBoolean(HttpContext.Current.Request.Params["bchkPedidosWeb"]);
        bool chkPedidosPb = Convert.ToBoolean(HttpContext.Current.Request.Params["bchkPedidosPb"]);

        string sDataInicio = HttpContext.Current.Request.Params["sDataInicio"];
        string sDataFim = HttpContext.Current.Request.Params["sDataFim"];
        string sFiltro = HttpContext.Current.Request.Params["sFiltro"];
        string sMarca = HttpContext.Current.Request.Params["sMarca"];
        string glb_sEmpresaFantasia = HttpContext.Current.Request.Params["glb_sEmpresaFantasia"];
        string glb_sEmpresaRazao = HttpContext.Current.Request.Params["glb_sEmpresaRazao"];
        string nEmpresaLogadaIdioma = HttpContext.Current.Request.Params["nEmpresaLogadaIdioma"];
        int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        int DATE_SQL_PARAM = Convert.ToInt32(HttpContext.Current.Request.Params["DATE_SQL_PARAM"]);
        string sLinguaLogada = HttpContext.Current.Request.Params["sLinguaLogada"];
        string nPedidoID = HttpContext.Current.Request.Params["nPedidoID"];
        string sMinuta = HttpContext.Current.Request.Params["nMinuta"];
        string strDataInicio = HttpContext.Current.Request.Params["strDataInicio"];
        string strDataFim = HttpContext.Current.Request.Params["strDataFim"];
        string sTitle = HttpContext.Current.Request.Params["sTitle"];
        int Datateste = 0;


        int glb_nTipoPerfilComercialID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nTipoPerfilComercialID"]);

        bool dirA1 = Convert.ToBoolean(HttpContext.Current.Request.Params["dirA1"]);
        bool dirA2 = Convert.ToBoolean(HttpContext.Current.Request.Params["dirA2"]);

        string glb_USERID = HttpContext.Current.Request.QueryString["glb_USERID"];

        protected void Page_Load(object sender, EventArgs e)
        {
            CarregaGlobais();

            if (!IsPostBack)
            {
                string nomeRel = Request.Params["relatorioID"].ToString();
                RelatorioID = Convert.ToInt32(Request.Params["relatorioID"].ToString());

                switch (nomeRel)
                {
                    case "40131":
                        RelatorioPedido1();
                        break;

                    case "40133":
                        propostaSolicitacao();
                        break;

                    case "40132":
                        propostaSolicitacao();
                        break;
                    
                    case "40139":
                        RelatorioVendas();
                        break;

                    case "40134":
                        ListaEmbalagens();
                        break;

                    case "PedidoPrint": //Relatório não finalizado
                        PedidoPrint();
                        break;

                    case "40146":
                        minutaTransporte();
                        break;

                    case "40147":
                        call_DemonstrativoCalculoICMSST();
                        break;

                    // Pedidos->Totalizacao de Vendas
                    case "40140":
                        TotalizaVendas();
                        break;

                    case "40142":
                        relatorioCompras();
                        break;

                    case "40148":
                        relatorioComissao();
                        break;

                    case "40143":
                        relatorioTransportadoras();
                        break;

                    case "40137":
                        prtMercadoria();
                        break;

                    case "40145":
                        relatorioFabricantes();
                        break;
                }
            }
        }

        public void PedidoPrint()
        {
            int formato = 1;

            Relatorio.CriarPDF_Excel("Pedido", formato);
        }

        public void ListaEmbalagens() {
            int iQLinhas = 0;
            int iQLinhas2 = 0;

            string SQLDetail1 = string.Empty;
            string SQLDetail2 = string.Empty;
            string SQLDetail3 = string.Empty;
            bool nulo = false;
            string paramData = "103";
            string paramLingua = "BRA";
            int yTraducao = 0;

            if (sLinguaLogada != "246")
            {
                paramData = "101";
                paramLingua = "USA";
                yTraducao = 1;
            }

            //Dados Gerais
            SQLDetail1 = "SELECT CONVERT(VARCHAR(10),ROW_NUMBER() OVER(ORDER BY Produtos.ConceitoID ASC)) AS Item, Produtos.ConceitoID AS ProdutoID, Produtos.Conceito, " +
                            "CONVERT(VARCHAR(14),(SELECT dbo.fn_Formata_Numero(SUM(Quantidade),0,103) FROM Pedidos_Itens WITH(NOLOCK) WHERE(Pedidos.PedidoID = PedidoID AND Produtos.ConceitoID = ProdutoID))) AS Quantidade, " +
                                "ISNULL(dbo.fn_Produto_MensagemBeneficio (Itens.ProdutoID, dbo.fn_Pessoa_Localidade(Pedidos.PessoaID, 2,NULL,NULL), GETDATE()), SPACE(0)) AS NumeroSerie, " +
                                "ISNULL(CONVERT(VARCHAR(10),dbo.fn_Pad(NotasFiscais.NotaFiscal,6,'0','L')),'') AS NotaFiscal, ISNULL(CONVERT(VARCHAR(10),NotasFiscais.dtNotaFiscal," + paramData + "),'') AS Data, " +
                                "'' AS Fornecedor, '' AS Documento " +
                            "FROM Pedidos WITH(NOLOCK) " +
                                "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                                "INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (Pedidos.PedidoID=Itens.PedidoID) " +
                                "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID = ProdutosEmpresa.ObjetoID AND Pedidos.EmpresaID = ProdutosEmpresa.SujeitoID) " +
                                "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (ProdutosEmpresa.ObjetoID = Produtos.ConceitoID) " +
                            "WHERE (Pedidos.PedidoID = " + nPedidoID + " AND " +
                                "ProdutosEmpresa.TipoRelacaoID = 61 AND Produtos.ProdutoSeparavel = 1) " +
                            "GROUP BY Pedidos.PedidoID, Produtos.ConceitoID, Produtos.Conceito, Produtos.LegislacaoAplicavel, " +
                                "NotasFiscais.NotaFiscal, NotasFiscais.dtNotaFiscal, Pedidos.PessoaID, Itens.ProdutoID " +
                            "ORDER BY Produtos.ConceitoID";

            //Detalhe
            SQLDetail2 = "SELECT '' AS Item, Itens.ProdutoID, '' AS Conceito, '' AS Quantidade, NumerosSerie.NumeroSerie AS NumeroSerie, " +
                                    "dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, NumerosSerie.NumeroSerie, 1) AS NotaFiscal, " +
                                    "CONVERT(VARCHAR(10),CONVERT(DATETIME,dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, NumerosSerie.NumeroSerie, 2),102)," + paramData + ") AS Data, " +
                                    "dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, NumerosSerie.NumeroSerie, 3) AS Fornecedor, " +
                                    "dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, NumerosSerie.NumeroSerie, 4) AS Documento " +
                            "FROM Pedidos WITH(NOLOCK) " +
                                "INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (Pedidos.PedidoID = Itens.PedidoID) " +
                                "INNER JOIN NumerosSerie_Movimento Movimentos WITH(NOLOCK) ON (Itens.PedidoID = Movimentos.PedidoID) " +
                                "INNER JOIN NumerosSerie WITH(NOLOCK) ON (Movimentos.NumeroSerieID = NumerosSerie.NumeroSerieID AND Itens.ProdutoID = NumerosSerie.ProdutoID) " +
                            "WHERE (Pedidos.PedidoID = " + nPedidoID + ") " +
                                "GROUP BY Pedidos.EmpresaID, Pedidos.PedidoID, Itens.ProdutoID, NumeroSerie " +
                            "UNION ALL " +
                            "SELECT '' AS Item, Itens.ProdutoID, '' AS Conceito, '' AS Quantidade, Movimentos.Lote AS NumeroSerie, " +
                                "dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, Movimentos.Lote, 1) AS NotaFiscal, " +
                                "CONVERT(VARCHAR(10),CONVERT(DATETIME,dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, Movimentos.Lote, 2),102)," + paramData + ") AS Data, " +
                                "dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, Movimentos.Lote, 3) AS Fornecedor, " +
                                "dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, Movimentos.Lote, 4) AS Documento " +
                            "FROM Pedidos WITH(NOLOCK), Pedidos_Itens Itens WITH(NOLOCK), Pedidos_ProdutosLote Movimentos WITH(NOLOCK) " +
                            "WHERE (Pedidos.PedidoID = " + nPedidoID + " AND Pedidos.PedidoID = Itens.PedidoID AND Pedidos.PedidoID = Movimentos.PedidoID AND Movimentos.ProdutoID = itens.ProdutoID) " +
                            "GROUP BY Pedidos.EmpresaID, Pedidos.PedidoID, Itens.ProdutoID, Movimentos.Lote " +
                            "UNION ALL " +
                            "SELECT ' ' AS Item, ' ' AS ProdutoID, ' ' AS Conceito, ' ' AS Quantidade, ' ' AS NumeroSerie, " +
                                "' ' AS NotaFiscal, " +
                                "' ' AS Data, " +
                                "' ' AS Fornecedor, " +
                                "' ' AS Documento "+
                            "ORDER BY Itens.ProdutoID, NumeroSerie ";

            //Totais
            SQLDetail3 = "SELECT dbo.fn_Formata_Numero(SUM(Quantidade),0,103) AS Quantidade FROM Pedidos_Itens WITH(NOLOCK) WHERE(PedidoID = " + nPedidoID +")" ;

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            ClsReport Relatorio = new ClsReport();

            string[,] arrQuerys;
            string[,] traducao;
            string tabelaContar = "SQLDetail1";
            string tabelaContar2 = "SQLDetail2";

            arrQuerys = new string[,] { { "SQLDetail1", SQLDetail1 },
                                    { "SQLDetail2", SQLDetail2 },
                                    { "SQLDetail3", SQLDetail3 }};

            traducao = new string[,] { { "Lista de Embalagem", "Packing List" },
                                        { "Pedido", "Order" },
                                        { "NF", "Invoice" },
                                        { "Produto", "Product" },
                                        { "Qtd", "Qty" },
                                        { "Número Série", "Serial Number" },
                                        { "Data", "Date" },
                                        { "Fornecedor", "Supplier" },
                                        { "Documento", "Document"},
                                        { "Total Itens", "Total Items" } };

            string[,] arrIndexKey = new string[,] { { "SQLDetail2", "ProdutoID", "ProdutoID" } };

            string Title = traducao[0, yTraducao];


            //Cria o relatório
            if (Formato == 1)
            {
                Relatorio.CriarRelatório(Title, arrQuerys, paramLingua, "Portrait", "1.8", "Default", "Default", true);
            }
            else
            {
                Relatorio.CriarRelatório(Title, arrQuerys, paramLingua, "Excel");
            }



            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            iQLinhas = dsFin.Tables[tabelaContar].Rows.Count;
            iQLinhas2 = dsFin.Tables[tabelaContar2].Rows.Count;

            if (iQLinhas == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {

                if (Formato == 1)
                {
                    double headerX = 0.2;
                    double headerY = 0.4;
                    double tabelaTitulo = 1.40;

                    double tabelaX = 0.2;
                    double tabelaY = 0;


                    //Labels Cabeçalho
                    Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "12", "black", "B", "Header", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 8.14).ToString(), headerY.ToString(), "12", "#0113a0", "B", "Header", "5", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 15.4).ToString(), headerY.ToString(), "10", "black", "B", "Header", "3.72187", "Horas");

                    Relatorio.CriarNumeroPagina((headerX + 18.2).ToString(), (headerY).ToString(), "10", "Black", "B", false);

                    //Parametros(Filtros) selecionados
                    Relatorio.CriarObjLabel("Fixo", "", traducao[1, yTraducao], (headerX).ToString(), (headerY + 0.5).ToString(), "8", "black", "B", "Header", "1", "");
                    Relatorio.CriarObjLabel("Fixo", "", nPedidoID, (headerX + 1).ToString(), (headerY + 0.5).ToString(), "8", "black", "B", "Header", "1.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", traducao[2, yTraducao], (headerX + 2.5).ToString(), (headerY + 0.5).ToString(), "8", "black", "B", "Header", "1", "");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "NotaFiscal", (headerX + 3.5).ToString(), (headerY + 0.5).ToString(), "8", "black", "B", "Header", "1.5", "");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "Data", (headerX + 5).ToString(), (headerY + 0.5).ToString(), "8", "black", "B", "Header", "1.5", "");

                    //Linha
                    Relatorio.CriarObjLinha((headerX).ToString(), (headerY + 0.9).ToString(), "20.8", "", "Header");
                    
                    //Cabeçalho
                    Relatorio.CriarObjLabel("Fixo", "", "Item", (headerX).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1", "");
                    Relatorio.CriarObjLabel("Fixo", "", "ID", (headerX += 1).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1", "");
                    Relatorio.CriarObjLabel("Fixo", "", traducao[3, yTraducao], (headerX += 1).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "2.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", traducao[4, yTraducao], (headerX += 2.5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", traducao[5, yTraducao], (headerX += 1.5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "4", "");
                    Relatorio.CriarObjLabel("Fixo", "", traducao[2, yTraducao], (headerX += 4).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", traducao[6, yTraducao], (headerX += 1.5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", traducao[7, yTraducao], (headerX += 1.5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", traducao[8, yTraducao], (headerX += 5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "2", "");


                    //Tabela

                    if (iQLinhas2 == 0)
                    {
                        Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "SQLDetail1", false);
                    }
                    else
                    {       
                        Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), arrIndexKey, false);
                    }
                        

                    Relatorio.CriarObjColunaNaTabela(" ", "Item", false, "1", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Item", "DetailGroup", "Null", "Null", 0, false, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "ProdutoID", false, "1", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup", "Null", "Null", 0, false, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "Conceito", false, "2.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Conceito", "DetailGroup", "Null", "Null", 0, false, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "Quantidade", false, "1.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup", "Null", "Null", 0, false, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "NumeroSerie", false, "4", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "NumeroSerie", "DetailGroup", "Null", "Null", 0, false, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "NotaFiscal", false, "1.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "NotaFiscal", "DetailGroup", "Null", "Null", 0, false, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "Data", false, "1.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup", "Null", "Null", 0, false, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "Fornecedor", false, "5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Fornecedor", "DetailGroup", "Null", "Null", 0, false, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "Documento", false, "2", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Documento", "DetailGroup", "Null", "Null", 0, false, "6");

                    Relatorio.TabelaEnd();

                    Relatorio.CriarObjLinha(tabelaX.ToString(), (tabelaY+0.1).ToString(), "20.8", "", "Body");

                    Relatorio.CriarObjLabel("Fixo", "", traducao[9, yTraducao], (tabelaX).ToString(), (tabelaY + 0.15).ToString(), "8", "black", "B", "Body", "1.5", "");
                    Relatorio.CriarObjLabel("Query", "SQLDetail3", "Quantidade", "4.7", (tabelaY + 0.15).ToString(), "8", "black", "B", "Body", "1.5", "");
                }

                else if (Formato == 2)
                {

                    if (iQLinhas2 == 0)
                    {
                        Relatorio.CriarObjTabela("0", "0", "SQLDetail1", true, false);
                    }
                    else
                    {                       
                        Relatorio.CriarObjTabela("0", "0", arrIndexKey, true, false);
                    }
                  
                    Relatorio.CriarObjColunaNaTabela("ID", "ProdutoID", true, "ProdutoID");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(traducao[3, yTraducao], "Conceito", true, "Conceito");
                    Relatorio.CriarObjCelulaInColuna("Query", "Conceito", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(traducao[4, yTraducao], "Quantidade", true, "Quantidade");
                    Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(traducao[5, yTraducao], "NumeroSerie", true, "NumeroSerie");
                    Relatorio.CriarObjCelulaInColuna("Query", "NumeroSerie", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(traducao[2, yTraducao], "NotaFiscal", true, "NotaFiscal");
                    Relatorio.CriarObjCelulaInColuna("Query", "NotaFiscal", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(traducao[6, yTraducao], "Data", true, "Data");
                    Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(traducao[7, yTraducao], "Fornecedor", true, "Fornecedor");
                    Relatorio.CriarObjCelulaInColuna("Query", "Fornecedor", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(traducao[8, yTraducao], "Documento", true, "Documento");
                    Relatorio.CriarObjCelulaInColuna("Query", "Documento", "DetailGroup");

                    Relatorio.TabelaEnd();
                }

                Relatorio.CriarPDF_Excel(Title, Formato);
            }
        }

        public void minutaTransporte()
        {
            string strParams = Convert.ToString(HttpContext.Current.Request.Params["strParams"]);
            int iQLinhas = 0;

            string SQLDetail1 = string.Empty;
            string SQLDetail2 = string.Empty;
            bool nulo = false;


            //Dados Gerais
            SQLDetail1 = "SELECT  TOP 1 COUNT(a.NotaFiscalID) AS Quant_NF,dbo.fn_Numero_Formata(SUM(NF.PesoBruto),2,1,103) AS TotalPB ,dbo.fn_Numero_Formata(SUM(NF.ValorTotalNota),2,1,103) AS ValorTotalNota,  " +
                                "SUM(a.NumeroVolumes) AS Total_Vol, a.TransportadoraID AS TransportadoraID, Empresa.Nome AS EmpresaNome,  +   " +
                                "ISNULL('CNPJ ' + dbo.fn_Pessoa_Documento( Empresa.PessoaID, NULL, NULL, 111, NULL, NULL) + ' ',SPACE(0)) +   " +
                                "ISNULL('IE ' + dbo.fn_Pessoa_Documento( Empresa.PessoaID, NULL, NULL, 112, NULL, NULL),SPACE(0)) AS EmpresaDocumentos, " +
                                "(ISNULL(Emp_End.Endereco + SPACE(1), SPACE(0)) + ISNULL(Emp_End.Numero + SPACE(1), SPACE(0)) + ISNULL(Emp_End.Complemento, SPACE(0))) AS EmpresaEndereco, " +
                                "ISNULL(Emp_End.Bairro + ' ' ,SPACE(0)) + ISNULL(Emp_End.CEP + ' ',SPACE(0)) +   " +
                                "ISNULL(Emp_Cid.Localidade + ' ',SPACE(0)) + ISNULL(Emp_UF.CodigoLocalidade2,SPACE(0))  AS EmpresaEndereco2,   " +
                                "Transport.Nome AS TransportNome,   " +
                                "ISNULL('CNPJ ' + dbo.fn_Pessoa_Documento( Transport.PessoaID, NULL, NULL, 111, NULL, NULL) + ' ',SPACE(0)) +  " +
                                "ISNULL('IE ' + dbo.fn_Pessoa_Documento( Transport.PessoaID, NULL, NULL, 112, NULL, NULL),SPACE(0)) AS TransportDocumentos,   " +
                                "dbo.fn_Pessoa_Endereco(Transport.PessoaID, 1) AS TransportEndereco,   " +
                                "ISNULL(Transport_End.Bairro + ' ' ,SPACE(0))  + ISNULL(Transport_End.CEP + ' ',SPACE(0)) +   " +
                                "ISNULL(Transport_Cid.Localidade + ' ',SPACE(0))  + ISNULL(Transport_UF.CodigoLocalidade2,SPACE(0))  AS TransportEndereco2   " +
                                ", Emp_End.Ordem " +
                            "FROM vw_Pedidos_MinutasTransporte a WITH(NOLOCK) " +
                                "INNER JOIN Pessoas Empresa WITH(NOLOCK)  ON a.EmpresaID = Empresa.PessoaID " +
                                "INNER JOIN Pessoas_Enderecos Emp_End WITH(NOLOCK) ON Empresa.PessoaID = Emp_End.PessoaID  " +
                                "LEFT JOIN Localidades Emp_pais WITH(NOLOCK) ON Emp_End.PaisID = Emp_pais.LocalidadeID " +
                                "LEFT JOIN Localidades Emp_UF WITH(NOLOCK) ON Emp_End.UFID = Emp_UF.LocalidadeID   " +
                                "LEFT JOIN Localidades Emp_Cid WITH(NOLOCK) ON Emp_End.CidadeID = Emp_Cid.LocalidadeID  " +
                                "INNER JOIN Pessoas Transport  WITH(NOLOCK) ON a.TransportadoraID = Transport.PessoaID  " +
                                "INNER JOIN Pessoas_Enderecos Transport_End WITH(NOLOCK) ON Transport.PessoaID = Transport_End.PessoaID  " +
                                "LEFT JOIN Localidades Transport_pais WITH(NOLOCK) ON Transport_End.PaisID = Transport_pais.LocalidadeID   " +
                                "LEFT JOIN Localidades Transport_UF WITH(NOLOCK) ON Transport_End.UFID = Transport_UF.LocalidadeID  " +
                                "LEFT JOIN Localidades Transport_Cid WITH(NOLOCK) ON Transport_End.CidadeID = Transport_Cid.LocalidadeID  " +
                                "INNER JOIN NotasFiscais NF WITH(NOLOCK) ON a.NotaFiscalID = NF.NotaFiscalID AND NF.EstadoID=67 " +
                                "WHERE a.MinutaID = " + sMinuta + " AND Emp_End.EndFaturamento = 1 " +
                            "GROUP BY a.TransportadoraID, Empresa.Nome, Empresa.PessoaID, Empresa.PessoaID, Empresa.PessoaID, Emp_End.Bairro, " +
                                "Emp_End.CEP, Emp_Cid.Localidade, Emp_UF.CodigoLocalidade2, Transport.Nome, Transport.PessoaID, Transport.PessoaID, " +
                                "Transport.PessoaID,Transport_End.Bairro,Transport_End.CEP,Transport_Cid.Localidade, Transport_UF.CodigoLocalidade2" +
                                ", Emp_End.Ordem, Emp_End.Endereco, Emp_End.Numero,Emp_End.Complemento " +
                            "ORDER BY Emp_End.Ordem ";

            //Detalhe
            SQLDetail2 = "SELECT DISTINCT a.MinutaID AS MinutaID, 1 AS Count, a.TransportadoraID AS TransportadoraID, b.NotaFiscal, a.NumeroVolumes AS Volumes, " +
                                "dbo.fn_Numero_Formata(b.ValorTotalNota,2,1,103) AS Valor, dbo.fn_Numero_Formata(b.PesoBruto,2,1,103) AS PesoKg, d.Nome AS Cliente, d.DocumentoFederal AS CNPJ, " +
                                "ISNULL(d.Endereco,SPACE(0)) + ' ' + ISNULL(d.Numero,SPACE(0)) + ' ' + ISNULL(d.Complemento,SPACE(0)) + '-'+ ISNULL(d.bairro,SPACE(0)) + ' ' + ISNULL(d.CEP,SPACE(0)) + ' / ' + ISNULL(d.Cidade,SPACE(0)) + ' ' + ISNULL(d.UF,SPACE(0)) AS Endereco1, " +
                                "d.Endereco, d.Numero, d.Complemento, d.Bairro, d.CEP, " +
                                "(d.DDI + d.DDD + ' ' + d.Telefone) AS Telefone, " +
                                "(CASE b.Frete WHEN 1 THEN 'Emitente' WHEN 2 THEN 'Destinatário' ELSE '' END) AS Frete, d.Cidade, d.UF, dbo.fn_Localidade_Zona(d.CidadeID, d.CEP) AS Zona, " +
                                "(SELECT TOP 1 dbo.fn_Pessoa_Fantasia(Contato.ContatoID,0) " +
                                    "FROM RelacoesPessoas RelPes WITH(NOLOCK) " +
                                        "LEFT OUTER JOIN RelacoesPessoas_Contatos Contato WITH(NOLOCK) ON RelPes.RelacaoID = Contato.RelacaoID " +
                                    "WHERE (RelPes.ObjetoID = b.EmpresaID  AND RelPes.SujeitoID = d.PessoaID) " +
                                    "ORDER BY Contato.Ordem) AS Contato " +

                            "FROM vw_Pedidos_MinutasTransporte a WITH(NOLOCK) " +
                                "LEFT OUTER JOIN NotasFiscais b WITH(NOLOCK) ON (a.NotaFiscalID = b.NotaFiscalID AND b.EstadoID=67) " +
                                "LEFT OUTER JOIN NotasFiscais_Pessoas d WITH(NOLOCK) ON (d.NotaFiscalID = b.NotaFiscalID) " +
                                        "AND (((b.Emissor = 1) AND (d.TipoID = 791)) OR ((b.Emissor = 0) AND (d.TipoID = 790))) " +
                            "WHERE a.MinutaID = " + sMinuta + " " + strDataInicio + strDataFim +
                            "ORDER BY b.NotaFiscal";

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            ClsReport Relatorio = new ClsReport();

            string[,] arrQuerys;
            string tabelaContar = "SQLDetail1";
            

            arrQuerys = new string[,] { { "SQLDetail1", SQLDetail1 },
                                    { "SQLDetail2", SQLDetail2 }};


            string Title = sTitle;


            //Cria o relatório
            if (Formato == 1)
            {
                Relatorio.CriarRelatório(Title, arrQuerys, "BRA", "Landscape", "1.3", "Default", "Default", true);
            }
            else
            {
                Relatorio.CriarRelatório(Title, arrQuerys, "BRA", "Excel");
            }



            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            double qtdeLinhasTabela = Convert.ToDouble(dsFin.Tables["SQLDetail1"].Rows[0]["Quant_NF"]);

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            iQLinhas = dsFin.Tables[tabelaContar].Rows.Count;

            if (iQLinhas == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {

                if (Formato == 1)
                {
                    double headerX = 0.2;
                    double headerY = 0.4;

                    double bodyX = 0.2;
                    double bodyY = 0;

                    double footerY = 0;

                    //Labels Cabeçalho
                    Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "12", "black", "B", "Header", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 12.5).ToString(), headerY.ToString(), "12", "#0113a0", "B", "Header", "5", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 23).ToString(), headerY.ToString(), "10", "black", "B", "Header", "3.72187", "Horas");

                    Relatorio.CriarNumeroPagina((headerX + 27).ToString(), (headerY).ToString(), "10", "Black", "B", false);

                    //Parametros(Filtros) selecionados
                    Relatorio.CriarObjLabel("Fixo", "", strParams, (headerX).ToString(), (headerY + 0.5).ToString(), "9", "black", "B", "Header", "29", "");

                    //Dados da Empresa e Transportadora
                    Relatorio.CriarObjLabel("Fixo", "", "Empresa", (bodyX).ToString(), (bodyY + 0.2).ToString(), "10", "blue", "B", "Body", "2", "");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "EmpresaNome", (bodyX).ToString(), (bodyY + 0.6).ToString(), "9", "black", "B", "Body", "9", "");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "EmpresaDocumentos", (bodyX).ToString(), (bodyY + 1).ToString(), "9", "black", "B", "Body", "9", "");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "EmpresaEndereco", (bodyX).ToString(), (bodyY + 1.4).ToString(), "9", "black", "B", "Body", "9", "");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "EmpresaEndereco2", (bodyX).ToString(), (bodyY + 1.8).ToString(), "9", "black", "B", "Body", "9", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Transportadora", (bodyX + 14.5).ToString(), (bodyY + 0.2).ToString(), "10", "blue", "B", "Body", "9", "");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "TransportNome", (bodyX + 14.5).ToString(), (bodyY + 0.6).ToString(), "9", "black", "B", "Body", "9", "");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "TransportDocumentos", (bodyX + 14.5).ToString(), (bodyY + 1).ToString(), "9", "black", "B", "Body", "9", "");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "TransportEndereco", (bodyX + 14.5).ToString(), (bodyY + 1.4).ToString(), "9", "black", "B", "Body", "9", "");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "TransportEndereco2", (bodyX + 14.5).ToString(), (bodyY + 1.8).ToString(), "9", "black", "B", "Body", "9", "");

                    //Dados para assinatura
                    Relatorio.CriarObjLinha(bodyX.ToString(), (bodyY + 2.4).ToString(), "28.5", "", "Body");
                    Relatorio.CriarObjLabel("Fixo", "", "Totais", bodyX.ToString(), (bodyY + 2.7).ToString(), "9", "Black", "B", "Body", "1");
                    Relatorio.CriarObjLabel("Fixo", "", "NF:", (bodyX + 0.2).ToString(), (bodyY + 3.1).ToString(), "9", "Black", "B", "Body", "1");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "Quant_NF", (bodyX + 1).ToString(), (bodyY + 3.1).ToString(), "9", "Black", "B", "Body", "1.5");
                    Relatorio.CriarObjLabel("Fixo", "", "Vol", (bodyX + 2).ToString(), (bodyY + 3.1).ToString(), "9", "Black", "B", "Body", "1");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "Total_Vol", (bodyX + 2.8).ToString(), (bodyY + 3.1).ToString(), "9", "Black", "B", "Body", "1.5");
                    Relatorio.CriarObjLabel("Fixo", "", "Valor", (bodyX + 4).ToString(), (bodyY + 3.1).ToString(), "9", "Black", "B", "Body", "1.5");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "ValorTotalNota", (bodyX + 4.9).ToString(), (bodyY + 3.1).ToString(), "9", "Black", "B", "Body", "2");
                    Relatorio.CriarObjLabel("Fixo", "", "Peso Kg", (bodyX + 7.1).ToString(), (bodyY + 3.1).ToString(), "9", "Black", "B", "Body", "1.5");
                    Relatorio.CriarObjLabel("Query", "SQLDetail1", "TotalPB", (bodyX + 8.6).ToString(), (bodyY + 3.1).ToString(), "9", "Black", "B", "Body", "2");

                    Relatorio.CriarObjLabel("Fixo", "", "Data: ___ /___ /______  Nome:______________________________________________ Assinatura:______________________ RG:______________ CPF:___________________", bodyX.ToString(), (bodyY + 4.1).ToString(), "10", "Black", "B", "Body", "29");

                    Relatorio.CriarObjLinha(bodyX.ToString(), (bodyY + 4.8).ToString(), "28.5", "", "Body");

                    //Tabela
                    Relatorio.CriarObjTabela(bodyX.ToString(), (bodyY + 5.2).ToString(), "SQLDetail2", false);

                    Relatorio.CriarObjColunaNaTabela("NF", "NotaFiscal", true, "NF", "Right", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "NotaFiscal", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Vol", "Volumes", true, "Vol", "Right", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Volumes", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Valor", "Valor", true, "Valor", "Right", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Peso Kg", "PesoKg", true, "Peso Kg", "Right", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "PesoKg", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Cliente", "Cliente", false, "6", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Cliente", "DetailGroup", "Null", "Null", 0, false, "7");

                    Relatorio.CriarObjColunaNaTabela("CNPJ", "CNPJ", true, "CNPJ", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "CNPJ", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Endereco", "Endereco1", false, "10", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Endereco1", "DetailGroup", "Null", "Null", 0, false, "7.5");

                    Relatorio.CriarObjColunaNaTabela("Telefone", "Telefone", true, "Telefone", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Telefone", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Frete", "Frete", true, "Frete", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Frete", "DetailGroup", "Null", "Null", 0, false, "8.5");

                    Relatorio.TabelaEnd();

 
                    //Relatorio.CriarObjLabel("Fixo", "", "teste", (headerX).ToString(), (headerY + 0.5).ToString(), "9", "black", "B", "Footer", "2", "");
                }

                else if (Formato == 2)
                {

                    Relatorio.CriarObjTabela("0", "0", "SQLDetail2", false);

                    Relatorio.CriarObjColunaNaTabela("MinutaID", "MinutaID", true, "MinutaID", "Right", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "MinutaID", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("NF", "NotaFiscal", true, "NF", "Right", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "NotaFiscal", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Vol", "Volumes", true, "Vol", "Right", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Volumes", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Valor", "Valor", true, "Valor", "Right", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Peso Kg", "PesoKg", true, "Peso Kg", "Right", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "PesoKg", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Cliente", "Cliente", false, "6", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Cliente", "DetailGroup", "Null", "Null", 0, false, "7.5");

                    Relatorio.CriarObjColunaNaTabela("CNPJ", "CNPJ", true, "CNPJ", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "CNPJ", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Endereco", "Endereco", true, "Endereco", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Endereco", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Numero", "Numero", true, "Numero", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Numero", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Complemento", "Complemento", true, "Complemento", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Complemento", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Bairro", "Bairro", true, "Bairro", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Bairro", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("CEP", "CEP", true, "CEP", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "CEP", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Telefone", "Telefone", true, "Telefone", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Telefone", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Frete", "Frete", true, "Frete", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Frete", "DetailGroup", "Null", "Null", 0, false, "8.5");

                    Relatorio.CriarObjColunaNaTabela("Cidade", "Cidade", true, "Cidade", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Cidade", "DetailGroup", "Null", "Null", 0, false, "8.5");

                    Relatorio.CriarObjColunaNaTabela("UF", "UF", true, "UF", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "UF", "DetailGroup", "Null", "Null", 0, false, "8.5");

                    Relatorio.CriarObjColunaNaTabela("Zona", "Zona", true, "Zona", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Zona", "DetailGroup", "Null", "Null", 0, false, "8.5");

                    Relatorio.CriarObjColunaNaTabela("Contato", "Contato", true, "Contato", "Default", "9", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Contato", "DetailGroup", "Null", "Null", 0, false, "8.5");

                    Relatorio.TabelaEnd();
                }

                Relatorio.CriarPDF_Excel(Title, Formato);
            }
        }

        public void RelatorioVendas()
        {
            string i;
            int nItensSelected;

            bool chkReportaFabricante = Convert.ToBoolean(HttpContext.Current.Request.Params["ReportaFabricante"]);
            bool chkPedidosWeb2 = Convert.ToBoolean(HttpContext.Current.Request.Params["PedidosWeb"]);
            bool chkMoedaLocal2 = Convert.ToBoolean(HttpContext.Current.Request.Params["MoedaLocal"]);
            bool chkSoResultado = Convert.ToBoolean(HttpContext.Current.Request.Params["SoResultado"]);
            bool chkDadosProduto = Convert.ToBoolean(HttpContext.Current.Request.Params["DadosProduto"]);
            bool chkDadosParceiro = Convert.ToBoolean(HttpContext.Current.Request.Params["DadosParceiro"]);
            bool chkDadosResultado = Convert.ToBoolean(HttpContext.Current.Request.Params["DadosResultado"]);
            bool chkComissaoVendas = Convert.ToBoolean(HttpContext.Current.Request.Params["ComissaoVendas"]);

            int selFabricante = Convert.ToInt32(HttpContext.Current.Request.Params["selFabricante"]);
            int selPadrao = Convert.ToInt32(HttpContext.Current.Request.Params["selPadrao"]);
            int nIdiomaDeID = Convert.ToInt32(HttpContext.Current.Request.Params["IdiomaDeID"]);
            int nIdiomaParaID = Convert.ToInt32(HttpContext.Current.Request.Params["IdiomaParaID"]);
            int EmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["EmpresaID"]);
            int selMicrosoftTipoRelatorio = Convert.ToInt32(HttpContext.Current.Request.Params["MicrosoftTipoRelatorio"]);
            int glb_USERID = Convert.ToInt32(HttpContext.Current.Request.Params["UserID"]);
            int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);

            string sDataInicio = Convert.ToString(HttpContext.Current.Request.Params["sDataInicio"]);
            string sDataFim = Convert.ToString(HttpContext.Current.Request.Params["sDataFim"]);
            string DATE_SQL_PARAM = HttpContext.Current.Request.Params["DATE_SQL_PARAM"];
            string Familias = HttpContext.Current.Request.Params["Familias"];
            string EmpresaNome = HttpContext.Current.Request.Params["EmpresaNome"];
            string txtFiltro1 = HttpContext.Current.Request.Params["Filtro"];
            string EmpresasIn = HttpContext.Current.Request.Params["EmpresasIn"];
            string Mesano = HttpContext.Current.Request.Params["MesAno"];
            string Title = "Relatorio de Vendas";


            glb_nEmpresaID = EmpresaID;

            string strSQL = "";
            string sReportaFabricante = "";
            string sPedidosWeb = "";
            string sDistinct = "";

            if (chkReportaFabricante)
                sReportaFabricante = " AND ProdutosEmpresa.ReportaFabricante = 1 ";

            if (chkPedidosWeb2)
                sPedidosWeb = " AND Pedidos.OrigemPedidoID = 606";
            else
                sPedidosWeb = "";

            string sTaxaMoeda = " * 1 * POWER(-1, Pedidos.TipoPedidoID) ";

            if (chkMoedaLocal2)
                sTaxaMoeda = " * Pedidos.TaxaMoeda * POWER(-1, Pedidos.TipoPedidoID) ";

            string sFiltroFabricante = (selFabricante == 0 ? "" : " AND Produtos.FabricanteID=" + selFabricante);

            string sFiltroFamilia = "";

            nItensSelected = 0;

            string sFiltro = ((txtFiltro1.Trim()).Length > 1 ? " AND (" + (txtFiltro1.Trim()) + ") " : "");

            if (!(dirA1 && dirA2))
            {
                sFiltro += " AND Pedidos.ProprietarioID = " + glb_USERID + " ";
            }

            var sMarca = "";

            // Retirado Filtro que permite GP"s olharem apenas para suas marcas, solicitado pelo Luciano Quarterolo. TSA.: 05/05/2015
            // Os GP"s devem ter acesso apenas as linhas que gerenciam, solicitado pelo Roberto Silva. TSA.: 08/10/2015
            if (glb_nEmpresaID != 7)
            {
                if (glb_nTipoPerfilComercialID == 263)//Gerente de produto
                {
                    sMarca = " AND ProdutosEmpresa.ProprietarioID = " + glb_USERID + " ";
                }
                else if (glb_nTipoPerfilComercialID == 264)//Assistente de Produto
                {
                    sMarca = " AND ProdutosEmpresa.AlternativoID = " + glb_USERID + " ";
                }
                else if (glb_nTipoPerfilComercialID == 265)//Vendedor e Televendas
                {
                    sMarca = " AND Pedidos.ProprietarioID = " + glb_USERID + " ";
                }
                if ((!(dirA1 && dirA2)) && (glb_nTipoPerfilComercialID == 265))
                {
                    sMarca = "";
                }
            }

            if (selPadrao == 708)
                sDistinct = " DISTINCT ";


            var strSQL_Select1 = "SELECT " + sDistinct +
                        "CONVERT(VARCHAR(10), Itens.dtMovimento, " + DATE_SQL_PARAM + ") AS Data_, Pedidos.PedidoID AS Pedido_, NotasFiscais.NotaFiscal AS NF_, " +
                        "Transacoes.Operacao AS Transacao_, Vendedores.PessoaID AS VendedorID_ , Vendedores.Fantasia AS Vendedor_, " +
                        "Pedidos.ParceiroID AS ParceiroID_, Parceiros.Fantasia AS Parceiro_, " +
                        "Cidades.Localidade AS Cidade_, Estados.CodigoLocalidade2 AS UF_, Paises.CodigoLocalidade2 AS [País_], " +
                        "Itens.ProdutoID AS ProdutoID_, Produtos.Conceito AS Produto_, " +
                        "CONVERT(INT, CASE Pedidos.TipoPedidoID WHEN 602 THEN Itens.Quantidade ELSE Itens.Quantidade * (-1) END) AS Quant_," +
                        "Itens.ValorUnitario AS [Preço_], ";

            var strSQL_Select2 = "";
            var strSQL_Select3 = "";
            var strSohResultado = (chkSoResultado ? "Transacoes.Resultado=1 AND " : "");

            var sFiltroEmpresa = "";
            var strSQL_OrderBy = "";

            // Padrao Generico
            if (selPadrao == 701)
            {
                var sDadosProduto = "Caracteristicas.Valor AS [Característica], " +
                    "dbo.fn_Pessoa_Fantasia(ProdutosEmpresa.ProprietarioID, 0) AS [GP], " +
                    "CONVERT(INT, (CASE Pedidos.TipoPedidoID WHEN 602 THEN Itens.Quantidade ELSE Itens.Quantidade * (-1) END)) AS Quant, " +
                    "Pedidos.MargemContribuicao AS [MC Pedido], " +
                    "Itens.ValorUnitario AS [Preço], " +
                    "(SELECT TOP 1 MoedaRebate.SimboloMoeda " +
                    "FROM Recursos Sistemas WITH(NOLOCK), Conceitos MoedaRebate WITH(NOLOCK) " +
                    "WHERE (Sistemas.RecursoID = 999 AND Sistemas.MoedaID = MoedaRebate.ConceitoID)) AS [Moeda], " +
                    "dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1321, 1, 1) AS [Total DPA], " +
                    "dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1322, 1, 1) AS [Total Rebate], ";

                if (chkDadosProduto)
                    sDadosProduto = "Marcas.Conceito AS Marca, Produtos.Modelo AS Modelo, " +
                        "Produtos.Descricao AS [Descrição], Produtos.PartNumber AS [Part Number], " +
                        "LinhasProduto.Conceito AS [Linha de Produto], ISNULL(Identificadores.Identificador, SPACE(0)) AS [GTIN],  " +
                        sDadosProduto;

                var sDadosParceiro = "Cidades.Localidade AS Cidade, Estados.CodigoLocalidade2 AS UF, (SELECT ItemMasculino FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE ItemID=Estados.TipoRegiaoID) AS [Região], " +
                    "Paises.CodigoLocalidade2 AS [País]";

                if (chkDadosParceiro)
                    sDadosParceiro = "Classificacoes.ItemMasculino AS Classificacao, dbo.fn_Pessoa_Documento(Pedidos.ParceiroID, NULL, NULL, 101, 111, 0) AS [CNPJ/CPF], " +
                        "Fone = (SELECT TOP 1 (ISNULL(DDI, SPACE(0)) + ISNULL(DDD, SPACE(0)) + " + "'" + "-" + "'" + " + Numero) AS Fone FROM Pessoas_Telefones " +
                            "WHERE TipoTelefoneID BETWEEN 119 AND 121 AND Ordem=1 AND PessoaID=Pedidos.ParceiroID ORDER BY TipoTelefoneID), " +
                        "(ISNULL(Faxes.DDI, SPACE(0)) + ISNULL(Faxes.DDD, SPACE(0)) + " + "'" + "-" + "'" + " + ISNULL(Faxes.Numero, SPACE(0))) AS Fax, URLs.URL AS [e-Mail], Enderecos.CEP AS CEP, " +
                        "Enderecos.Endereco AS [Endereço], Enderecos.Numero AS [Número], Enderecos.Complemento AS Complemento, " +
                        "Enderecos.Bairro AS Bairro, " + sDadosParceiro + ", Clientes.Nome AS Cliente, " +
                        "CASE WHEN Pedidos.TransportadoraID = Pedidos.ParceiroID THEN " + "'" + "O Mesmo" + "'" + " " +
                        "WHEN Pedidos.TransportadoraID = Pedidos.EmpresaID THEN " + "'" + "Empresa" + "'" + " " +
                        "ELSE Transportadoras.Nome END AS Transportadora ";

                var sDadosFaturamento = "";

                if (chkDadosResultado)
                {
                    sDadosFaturamento = " ,CONVERT(NUMERIC(10,5), Pedidos.TaxaMoeda) AS [Taxa Moeda]," +
                        ((glb_nEmpresaID != 7) ? "Parcelas.valorparcela AS [Valor Frete]," : "") +
                        "(Itens.ValorFaturamento " + sTaxaMoeda + ") AS Faturamento, " +
                        "(Itens.ValorFaturamentoBase " + sTaxaMoeda + ") AS [Fatur Base], " +
                        "(Itens.ValorFinanceiro " + sTaxaMoeda + ") AS Financeiro, " +
                        "(Itens.ValorContribuicao " + sTaxaMoeda + ") AS [Contribuição], " +
                        "(Itens.ValorContribuicaoAjustada " + sTaxaMoeda + ") AS [Contr Ajustada], " +
                        "CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(Itens.ValorContribuicaoAjustada, Itens.ValorFaturamentoBase) * 100) AS MC, " +
                        "(Pedidos.PrazoMedioPagamento) AS PMP, " +
                        "(SELECT TOP 1 MoedaFornecedor.SimboloMoeda FROM RelacoesPesCon_Fornecedores Fornec WITH(NOLOCK), Conceitos MoedaFornecedor WITH(NOLOCK) WHERE (ProdutosEmpresa.RelacaoID = Fornec.RelacaoID AND Fornec.MoedaID = MoedaFornecedor.ConceitoID) ORDER BY Fornec.Ordem) AS MoedaFOB, " +
                        "(SELECT TOP 1 CustoFOB FROM RelacoesPesCon_Fornecedores WITH(NOLOCK) WHERE (ProdutosEmpresa.RelacaoID = RelacaoID) ORDER BY Ordem) AS CustoFOB, " +
                        "CONVERT(NUMERIC(11,2), (SELECT TOP 1 CustoFOB * Itens.Quantidade FROM RelacoesPesCon_Fornecedores WITH(NOLOCK) WHERE (ProdutosEmpresa.RelacaoID = RelacaoID) ORDER BY Ordem)) AS TotalCustoFOB ";
                }

                strSQL_Select2 = "dbo.fn_Pessoa_Fantasia(Pedidos.EmpresaID, 0) AS Empresa, CONVERT(VARCHAR(10), Itens.dtMovimento, " + DATE_SQL_PARAM + ") AS Data, Pedidos.PedidoID AS Pedido, NotasFiscais.NotaFiscal AS NF, " +
                    "Transacoes.Operacao AS Transacao, Vendedores.PessoaID AS VendedorID , Vendedores.Fantasia AS Vendedor, " +
                    "dbo.fn_RelPessoa_Classificacao(Pedidos.EmpresaID, Vendedores.PessoaID, 2, GETDATE(), 2) AS [Clas vendedor], " +
                    "Pedidos.ParceiroID AS ParceiroID, Parceiros.Nome AS Parceiro, Parceiros.Fantasia AS [Fantasia], " +
                    "dbo.fn_RelPessoa_Classificacao(Pedidos.EmpresaID, Pedidos.ParceiroID, 2, GETDATE(), 1) AS [Clas parceiro], " + sDadosParceiro + ", " +
                    "Itens.ProdutoID AS ProdutoID, Produtos.Conceito AS Produto, Familias.Conceito AS [Família], " + sDadosProduto +
                    "Itens.ValorUnitario AS [Preço] " + sDadosFaturamento + ", Itens.dtMovimento AS Data1, Pedidos.SeuPedido, Pedidos.Observacao, " +
                    "dbo.fn_Produto_Descricao2(Itens.produtoid , NULL, 14) AS [Descrição Fiscal] ";
            }

            // Padrão Cisco
            // Solicitação dia 23/03/2011, criado em em 24/03/2011 - DMC
            else if (selPadrao == 704)
            {
                strSQL_Select2 = "SELECT (CASE Pedidos.EmpresaID WHEN 10 THEN " + "'" + "BR-RIO DE JANEIRO" + "'" + " WHEN 14 THEN " + "'" + "BR-RIO DE JANEIRO" + "'" + " ELSE Empresa.Fantasia END) AS " + "'" + " Distributor Warehouse Number" + "'" + ", Produtos.PartNumber AS " + "'" + " Cisco Standard Part Number " + "'" + ", SPACE(0) AS " + "'" + " Distributor Part Number " + "'" + ", " +
                    "SPACE(0) AS " + "'" + " Product Item Description" + "'" + ", PS.NumeroSerie AS " + "'" + " Product Serial Number" + "'" + ", CONVERT(VARCHAR(10), Itens.dtMovimento, " + DATE_SQL_PARAM + ") AS " + "'" + " Distributor to Reseller Invoice Date (Shipped Date) " + "'" + ", " +
                        "SPACE(0) AS " + "'" + " Distributor to Reseller Sales Order Date " + "'" + ", NotasFiscais.NotaFiscal AS " + "'" + " Distributor to Reseller Invoice Number " + "'" + ", SPACE(0) AS " + "'" + " Original Distributor to Reseller Invoice Number " + "'" + ", " +
                        "Itens.Ordem AS " + "'" + " Distributor Sales Order Number " + "'" + ", SPACE(0) AS " + "'" + " Distributor PO Number to Cisco " + "'" + ", Pedidos.PedidoID AS " + "'" + " Reseller to Distributor PO Number " + "'" + ", SPACE(0) AS " + "'" + " End Customer to Reseller PO Number " + "'" + ", " +
                        "SPACE(0) AS " + "'" + " SO Line Number (Order Line Item Number) " + "'" + ", SPACE(0) AS " + "'" + " (Original) Re-Bill Invoice Number " + "'" + ", SPACE(0) AS " + "'" + " Service Contract Number " + "'" + ", SPACE(0) AS " + "'" + " SCC Quote Number " + "'" + ", SPACE(0) AS " + "'" + " Drop Ship " + "'" + ", " +
                        "SPACE(0) AS " + "'" + " Ship-To Name " + "'" + ", SPACE(0) AS " + "'" + " Ship-To DUNS Number " + "'" + ", SPACE(0) AS " + "'" + " Ship-To Address1 " + "'" + ", SPACE(0) AS " + "'" + " Ship-To Address2 " + "'" + ", SPACE(0) AS " + "'" + " Ship-To City " + "'" + ", SPACE(0) AS " + "'" + " Ship-To State " + "'" + ", " +
                        "SPACE(0) AS " + "'" + " Ship-To Zip/Postal Code" + "'" + " , SPACE(0) AS " + "'" + " Ship-To Country " + "'" + ", Parceiros.Nome AS " + "'" + " Buyer/Reseller Name " + "'" + ", Parceiros.PessoaID AS " + "'" + " Buyer/Reseller Partner Identification " + "'" + ", SPACE(0) AS " + "'" + " Buyer/Reseller DUNS Number " + "'" + ", " +
                        "Enderecos.Endereco AS " + "'" + " Buyer/Reseller Address1 " + "'" + ", Enderecos.Numero AS " + "'" + " Buyer/Reseller Address2 " + "'" + ", Cidades.Localidade AS " + "'" + " Buyer/Reseller City " + "'" + ", Estados.CodigoLocalidade2 AS " + "'" + " Buyer/Reseller/State/Province/County/Region " + "'" + ", " +
                        "Enderecos.CEP AS " + "'" + " Buyer/Reseller / Zip Postal Code" + "'" + ", Paises.CodigoLocalidade2 AS " + "'" + " Buyer/Reseller Country" + "'" + ", SPACE(0) AS " + "'" + " Reseller Contact Name" + "'" + ", SPACE(0) AS " + "'" + " Reseller Contact Email" + "'" + ", SPACE(0) AS " + "'" + " Reseller Contact Telephone" + "'" + ", " +
                        "SPACE(0) AS " + "'" + " Bill To Name " + "'" + ", SPACE(0) AS " + "'" + " Bill To DUNS Number" + "'" + ", SPACE(0) AS " + "'" + " Bill To Address1" + "'" + ", SPACE(0) AS " + "'" + " Bill To Address2" + "'" + ", SPACE(0) AS " + "'" + " Bill-To City" + "'" + ", SPACE(0) AS " + "'" + " Bill-To/State/Province/County/Region" + "'" + ", " +
                        "SPACE(0) AS " + "'" + " Bill-To Zip/Postal Code " + "'" + ", SPACE(0) AS " + "'" + " Bill-To Country" + "'" + ", SPACE(0) AS " + "'" + " Service Provider Name" + "'" + ", SPACE(0) AS " + "'" + " Service Provider Country" + "'" + ", CASE Pedidos.TipoPedidoID WHEN 602 THEN CASE WHEN PS.NumeroSerie IS NULL THEN Itens.Quantidade ELSE 1 END ELSE Itens.Quantidade * (-1) END AS " + "'" + " Product Quantity" + "'" + ", " +
                        "SPACE(0) AS " + "'" + " Reported Product Unit Price - Local Currency " + "'" + ", SPACE(0) AS " + "'" + " Reported Product Unit Price - USD" + "'" + ", SPACE(0) AS " + "'" + " Reported Net POS Price" + "'" + ", Clientes.Nome AS " + "'" + " End Customer Name" + "'" + ", SPACE(0) AS " + "'" + " End Customer DUNS Number" + "'" + ", SPACE(0) AS " + "'" + " End Customer Address1" + "'" + ", " +
                        "SPACE(0) AS " + "'" + " End Customer Address2 " + "'" + ",	cliente_cidades.Localidade AS " + "'" + " End Customer City" + "'" + ", cliente_estados.CodigoLocalidade2 AS " + "'" + " End Customer State/Province/County/Region" + "'" + ", SPACE(0) AS " + "'" + " End Customer Zip / Postal Code" + "'" + ", Paises.CodigoLocalidade2 AS " + "'" + " End Customer Country " + "'" + ", " +
                        "SPACE(0) AS " + "'" + " End Customer Contact Name " + "'" + ", SPACE(0) AS " + "'" + " End Customer Contact Email" + "'" + ", SPACE(0) AS " + "'" + " End Customer Contact Telephone" + "'" + ",	SPACE(0) AS " + "'" + " Export Reference Number" + "'" + ", ISNULL(Pedidos.NumeroProjeto, SPACE(0)) AS " + "'" + " Reported Deal ID" + "'" + ", SPACE(0) AS " + "'" + " Promotion Authorization Number" + "'" + ", " +
                        "SPACE(0) AS " + "'" + " Claim ligibility Quantity " + "'" + ", SPACE(0) AS " + "'" + " Claim Per Unit " + "'" + ", SPACE(0) AS " + "'" + " Extended Claim Amount" + "'" + ", SPACE(0) AS " + "'" + "Claim Reference Number" + "'" + ", " +
                        "dbo.fn_Pessoa_Documento(Clientes.PessoaID, 130, NULL, 111, 101, NULL) AS 'CNPJClienteFinal'" + ", " +
                        "Vendedores.Fantasia AS 'Vendedor'" + ", " +
                        "LinhasProduto.Conceito AS 'LinhaProduto'" + ", " +
                        "dbo.fn_Pessoa_Documento(Parceiros.PessoaID, 130, NULL, 111, 101, NULL) AS 'CNPJRevenda'" + " ";

                var strSQL_From = "FROM Pedidos WITH(NOLOCK) " +
             "INNER JOIN " +
             "Pedidos_Itens Itens WITH(NOLOCK) " +
             "ON (Itens.PedidoID = Pedidos.PedidoID) " +
             "LEFT OUTER JOIN(SELECT P.PedidoID,P.ProdutoID,NumerosSerie.NumeroSerie AS NumeroSerie " +
                                "FROM NumerosSerie_Movimento Movimentos WITH(NOLOCK) " +
                                    "INNER JOIN NumerosSerie NumerosSerie WITH(NOLOCK) ON (Movimentos.NumeroSerieID = NumerosSerie.NumeroSerieID) " +
                                    "INNER JOIN Pedidos_Itens P WITH(NOLOCK) ON (P.PedidoID = Movimentos.PedidoID) AND (P.ProdutoID = NumerosSerie.ProdutoID) " +
                        "UNION ALL " +
                            "SELECT P.PedidoID, P.ProdutoID,PP.Lote AS NumeroSerie " +
                                "FROM Pedidos_ProdutosLote PP WITH(NOLOCK) " +
                                    "INNER JOIN Pedidos_Itens P WITH(NOLOCK) ON (P.PedidoID = PP.PedidoID) AND (P.ProdutoID = PP.ProdutoID) " +
                        "UNION ALL " +
                            "SELECT P.PedidoID, P.ProdutoID, NULL AS NumeroSerie " +
                                "FROM Pedidos_ProdutosSeparados PPS WITH(NOLOCK) " +
                                    "INNER JOIN Pedidos_Itens P WITH(NOLOCK) ON (P.PedidoID = PPS.PedidoID) AND (P.ProdutoID = PPS.ProdutoID) " +
                ") AS PS ON (Itens.PedidoID = PS.PedidoID) AND (Itens.ProdutoID = PS.ProdutoID) " +
            "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID = 67) " +
            "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID = Transacoes.OperacaoID) " +
            "INNER JOIN Pessoas Empresa WITH (NOLOCK) ON (Empresa.PessoaID = Pedidos.EmpresaID) " +
            "INNER JOIN Pessoas Vendedores WITH(NOLOCK) ON (Pedidos.ProprietarioID = Vendedores.PessoaID) " +
            "INNER JOIN Pessoas Parceiros WITH(NOLOCK) ON (Pedidos.ParceiroID = Parceiros.PessoaID) " +
            "LEFT OUTER JOIN TiposAuxiliares_Itens Classificacoes WITH(NOLOCK) ON (Parceiros.ClassificacaoID = Classificacoes.ItemID) " +
            "LEFT OUTER JOIN Pessoas_Telefones Faxes WITH(NOLOCK) ON (Parceiros.PessoaID = Faxes.PessoaID AND Faxes.TipoTelefoneID = 122 AND Faxes.Ordem = 1) " +
            "LEFT OUTER JOIN Pessoas_URLs URLs WITH(NOLOCK) ON (Parceiros.PessoaID = URLs.PessoaID AND URLs.TipoURLID = 124 AND URLs.Ordem=1) " +
            "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Parceiros.PessoaID = Enderecos.PessoaID) " +
            "LEFT OUTER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID = Cidades.LocalidadeID) " +
            "LEFT OUTER JOIN Localidades Estados WITH(NOLOCK) ON (Enderecos.UFID = Estados.LocalidadeID) " +
            "LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID = Paises.LocalidadeID) " +
            "INNER JOIN Pessoas Clientes WITH(NOLOCK) ON (Pedidos.PessoaID = Clientes.PessoaID) " +
            "INNER JOIN Pessoas_Enderecos Cliente_Enderecos WITH(NOLOCK) ON (Clientes.PessoaID = Cliente_Enderecos.PessoaID) " +
            "LEFT OUTER JOIN Localidades Cliente_Cidades WITH(NOLOCK) ON (Cliente_Enderecos.CidadeID = Cliente_Cidades.LocalidadeID) " +
            "LEFT OUTER JOIN Localidades Cliente_Estados WITH(NOLOCK) ON (Cliente_Enderecos.UFID = Cliente_Estados.LocalidadeID) " +
            "LEFT OUTER JOIN Localidades Cliente_Paises WITH(NOLOCK) ON (Cliente_Enderecos.PaisID = Cliente_Paises.LocalidadeID) " +
            "INNER JOIN Pessoas Transportadoras WITH(NOLOCK) ON (Pedidos.TransportadoraID = Transportadoras.PessoaID) " +
            "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID = Produtos.ConceitoID) " +
            "LEFT OUTER JOIN Conceitos_Caracteristicas Caracteristicas WITH(NOLOCK) ON (Produtos.ConceitoID = Caracteristicas.ProdutoID AND dbo.fn_Produto_CaracteristicaOrdem(Caracteristicas.ProdutoID, Caracteristicas.CaracteristicaID)=1) " +
            "LEFT OUTER JOIN Conceitos LinhasProduto WITH(NOLOCK) ON (LinhasProduto.ConceitoID = Produtos.LinhaProdutoID) " +
            "INNER JOIN Conceitos Familias WITH(NOLOCK) ON (Produtos.ProdutoID = Familias.ConceitoID) " +
            "INNER JOIN Conceitos Marcas WITH(NOLOCK) ON (Produtos.MarcaID = Marcas.ConceitoID) " +
            "INNER JOIN RelacoesPesCon ProdutosEmpresa  WITH(NOLOCK) ON (Itens.ProdutoID = ProdutosEmpresa.ObjetoID) AND (ProdutosEmpresa.SujeitoID = Pedidos.EmpresaID)";

                var strSQL_Where1 = "WHERE (Pedidos.ProprietarioID <> 1059 AND " +
                    "Enderecos.EndFaturamento=1 AND Enderecos.Ordem=1 AND ";

                var strSQL_Where2 = "Cliente_Enderecos.EndFaturamento=1 AND Cliente_Enderecos.Ordem=1 AND ";

                var strSQL_Where3 = "ProdutosEmpresa.TipoRelacaoID = 61 AND " + strSohResultado + " " +
                    "Pedidos.Suspenso=0 AND ((Pedidos.TipoPedidoID=601 AND Pedidos.EstadoID>=30) OR (Pedidos.TipoPedidoID=602 AND Pedidos.EstadoID>=29)) AND " +
                    "Itens.dtMovimento >=" + sDataInicio + " AND Itens.dtMovimento <=" + sDataFim + " " +
                    sMarca + sReportaFabricante + sPedidosWeb + sFiltroFabricante + sFiltroFamilia + sFiltro + ") ";

                strSQL_OrderBy = "ORDER BY " + "'" + " Distributor to Reseller Invoice Date (Shipped Date) " + "'" + ", " + "'" + " Reseller to Distributor PO Number " + "'" + "";

                strSQL = strSQL_Select2 + strSQL_From + strSQL_Where1 + strSQL_Where2 +
                    strSQL_Where3 + strSQL_OrderBy;
            }
            // Padrao Intel
            // Kingston ira usar este modelo temporariamente ateh a priscila definir o padrao deles
            else if ((selPadrao == 702) || (selPadrao == 710))
            {
                sFiltroEmpresa = "";
                strSQL_OrderBy = "ORDER BY EmpresaID, Data_, Pedido_";

                if (glb_nEmpresaID == 3)
                    sFiltroEmpresa = " Pedidos.EmpresaID in " + EmpresasIn + " AND ";
                else

                    strSQL_Select2 = "Pedidos.EmpresaID AS EmpresaID, LEFT(dbo.fn_String_Substituicao(Parceiros.Nome,1),35) AS B01, " +
                    "(CASE WHEN Clientes.Nome = Parceiros.Nome THEN SPACE(0) ELSE LEFT(dbo.fn_String_Substituicao(Clientes.Nome,1),35) END) AS B10, " +
                    "dbo.fn_Pessoa_Documento(Pedidos.ParceiroID, NULL, NULL, 101, 111, 0) AS B02, " +
                    "LEFT(dbo.fn_String_Substituicao(ISNULL(Enderecos.Endereco, SPACE(0)) + SPACE(1) + ISNULL(Enderecos.Numero, SPACE(0)),1),35) AS B03, " +
                    "LEFT(dbo.fn_String_Substituicao(ISNULL(Enderecos.Complemento,SPACE(0)),1), 35) AS B04, " +
                    "LEFT(dbo.fn_String_Substituicao(ISNULL(Enderecos.Bairro, SPACE(0)),1),35) AS B05, " +
                    "LEFT(dbo.fn_String_Substituicao(ISNULL(Cidades.Localidade, SPACE(0)),1),35) AS B06, " +
                    "LEFT(ISNULL(Estados.CodigoLocalidade2, SPACE(0)),3) AS B07, " +
                    "dbo.fn_String_Substituicao(Enderecos.CEP,1) AS B08, " +
                    "LEFT(ISNULL(Paises.CodigoLocalidade2, SPACE(0)),3) AS B09, " +
                    "(CASE WHEN Clientes.Nome = Parceiros.Nome THEN SPACE(0) ELSE dbo.fn_Pessoa_Documento(Clientes.PessoaID, NULL, NULL, 101, 111, 0) END) AS B11, " +
                    "(CASE WHEN Clientes.Nome = Parceiros.Nome THEN SPACE(0) ELSE LEFT(dbo.fn_String_Substituicao(ISNULL(Cliente_Enderecos.Endereco, SPACE(0)) + SPACE(1) + ISNULL(Cliente_Enderecos.Numero, SPACE(0)),1), 35) END) AS B12, " +
                    "(CASE WHEN Clientes.Nome = Parceiros.Nome THEN SPACE(0) ELSE LEFT(dbo.fn_String_Substituicao(ISNULL(Cliente_Enderecos.Complemento, SPACE(0)),1), 35) END) AS B13, " +
                    "(CASE WHEN Clientes.Nome = Parceiros.Nome THEN SPACE(0) ELSE LEFT(dbo.fn_String_Substituicao(ISNULL(Cliente_Enderecos.Bairro, SPACE(0)),1), 35) END) AS B14, " +
                    "(CASE WHEN Clientes.Nome = Parceiros.Nome THEN SPACE(0) ELSE LEFT(dbo.fn_String_Substituicao(ISNULL(Cliente_Cidades.Localidade, SPACE(0)),1), 35) END) AS B15, " +
                    "(CASE WHEN Clientes.Nome = Parceiros.Nome THEN SPACE(0) ELSE LEFT(ISNULL(Cliente_Estados.CodigoLocalidade2, SPACE(0)), 3) END) AS B16, " +
                    "(CASE WHEN Clientes.Nome = Parceiros.Nome THEN SPACE(0) ELSE LEFT(dbo.fn_String_Substituicao(ISNULL(Cliente_Enderecos.CEP, SPACE(0)),1), 10) END) AS B17, " +
                    "(CASE WHEN Clientes.Nome = Parceiros.Nome THEN SPACE(0) ELSE LEFT(ISNULL(Cliente_Paises.CodigoLocalidade2, SPACE(0)), 3) END) AS B18, " +
                    "REPLACE(CONVERT(VARCHAR(11), CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(dbo.fn_PedidoItem_Totais(Itens.PedItemID, 7), " +
                        "Pedidos.TaxaMoeda))), " + "'" + "," + "'" + ", " + "'" + "." + "'" + ") AS B22, dbo.fn_String_Substituicao(NotasFiscais.NotaFiscal,1) AS B24, ";

                // Intel
                if (selPadrao == 702)
                    strSQL_Select2 += "(CASE WHEN Pedidos.TipoPedidoID = 601 THEN CONVERT(VARCHAR(8),(SELECT TOP 1 e.dtNotaFiscal " +
                        "FROM Pedidos_Itens a WITH(NOLOCK), Pedidos_Itens_Campanhas b WITH(NOLOCK), Campanhas_Produtos c WITH(NOLOCK), " +
                                "Campanhas d WITH(NOLOCK), NotasFiscais e WITH(NOLOCK)  " +
                        "WHERE (a.PedidoiD = e.PedidoID AND e.EstadoID=67 AND c.CampanhaID = d.CampanhaID AND b.CamProdutoID = c.CamProdutoID  " +
                                   "AND a.PedItemID = b.PedItemID AND Itens.PedItemReferenciaID = a.PedItemID AND Itens.ProdutoID = a.ProdutoID  " +
                                    "AND d.TipoCampanhaID = 1321)),112) ELSE SPACE(0) END) AS B41, " +
                    "(CASE WHEN Pedidos.TipoPedidoID = 601 THEN (SELECT TOP 1 e.NotaFiscal " +
                        "FROM Pedidos_Itens a WITH(NOLOCK), Pedidos_Itens_Campanhas b WITH(NOLOCK), Campanhas_Produtos c WITH(NOLOCK), " +
                                "Campanhas d WITH(NOLOCK), NotasFiscais e WITH(NOLOCK)  " +
                        "WHERE (a.PedidoiD = e.PedidoID AND e.EstadoID = 67 AND c.CampanhaID = d.CampanhaID AND b.CamProdutoID = c.CamProdutoID  " +
                                    "AND a.PedItemID = b.PedItemID AND Itens.PedItemReferenciaID = a.PedItemID AND Itens.ProdutoID = a.ProdutoID  " +
                                    "AND d.TipoCampanhaID = 1321)) ELSE SPACE(0) END) AS B42, " +
                    "(CASE WHEN Pedidos.TipoPedidoID = 601 THEN (SELECT TOP 1 a.Ordem " +
                        "FROM Pedidos_Itens a WITH(NOLOCK), Pedidos_Itens_Campanhas b WITH(NOLOCK), Campanhas_Produtos c WITH(NOLOCK), " +
                                "Campanhas d WITH(NOLOCK) " +
                        "WHERE (c.CampanhaID = d.CampanhaID AND b.CamProdutoID = c.CamProdutoID  " +
                                    "AND a.PedItemID = b.PedItemID AND Itens.PedItemReferenciaID = a.PedItemID AND Itens.ProdutoID = a.ProdutoID " +
                                    "AND d.TipoCampanhaID = 1321)) ELSE SPACE(0) END) AS B43,";

                strSQL_Select2 += "ISNULL(CONVERT(VARCHAR, Itens.Ordem), SPACE(0)) AS B25,";

                strSQL_Select2 += "'" + "USD" + "'" + " AS B26, " +
                    "(SELECT TOP 1 c.Codigo FROM Pedidos_Itens_Campanhas a WITH(NOLOCK), Campanhas_Produtos b WITH(NOLOCK), Campanhas c WITH(NOLOCK) WHERE (Itens.PedItemID = a.PedItemID AND a.CamProdutoID = b.CamProdutoID AND b.CampanhaID = c.CampanhaID AND c.TipoCampanhaID IN (1321,1322))) AS B27, " +
                    "CONVERT(VARCHAR(10), dbo.fn_String_Substituicao(dbo.fn_ProgramaMarketing_Identificador(" + selFabricante + ", Parceiros.PessoaID), 1)) AS B32, " +
                    "Itens.ProdutoID AS B20, " +
                    "CONVERT(INT, CASE Pedidos.TipoPedidoID WHEN 602 THEN Itens.Quantidade ELSE Itens.Quantidade * (-1) END) AS B21," +
                    "CONVERT(VARCHAR(8), dbo.fn_Data_Zero(Itens.dtMovimento), 112) AS B23, " +
                    "(SELECT TOP 1 a.Codigo FROM RelacoesPessoas a WITH(NOLOCK) WHERE (a.EstadoID <> 5 AND a.TipoRelacaoID = 21 AND a.SujeitoID = Pedidos.EmpresaID AND a.ObjetoID = " + selFabricante + ")) AS B37, ";

                // Intel
                if (selPadrao == 702)
                    strSQL_Select2 += "dbo.fn_String_Substituicao(Produtos.Modelo,1) AS B19, ";
                else
                    strSQL_Select2 += "Produtos.PartNumber AS B19, ";

                strSQL_Select2 += "Produtos.PartNumber AS PartNumber, Vendedores.PessoaID as VendedorID, Vendedores.Fantasia AS Vendedor, Equipes.Fantasia AS Equipe, " +
                    "(SELECT TOP 1 bb.SimboloMoeda FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK), Conceitos bb WHERE (aa.RelacaoID = ProdutosEmpresa.RelacaoID AND aa.MoedaID = bb.ConceitoID) ORDER BY aa.Ordem) AS MoedaFOB, " +
                    "(SELECT TOP 1 CustoFOB FROM RelacoesPesCon_Fornecedores WITH(NOLOCK) WHERE (RelacaoID = ProdutosEmpresa.RelacaoID) ORDER BY Ordem) AS CustoFOB, " +
                    "CONVERT(NUMERIC(11,2), (SELECT TOP 1 CustoFOB FROM RelacoesPesCon_Fornecedores WITH(NOLOCK) WHERE (RelacaoID = ProdutosEmpresa.RelacaoID) ORDER BY Ordem) * Itens.Quantidade) AS TotalCustoFOB, " +
                    "Itens.PedidoID AS PedidoID, Pedidos.ParceiroID AS ParceiroID, Familias.Conceito AS Familia, Marcas.Conceito AS Marca, " +
                    "ISNULL(Caracteristicas.Valor, '') + ' ' + ISNULL(Caracteristicas2.Valor, '') AS Caracteristica, " +
                    "ISNULL(Produtos.Descricao, '') AS Descrição, " + //Solicitação TICKET#2017080398000211 para pegar a descrição do produto - MSO 11/08/2017
                    "dbo.fn_DivideZero(dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1321, 1, 1), Itens.Quantidade) AS ValorDPA, " +
                    "dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1321, 1, 1) AS TotalDPA," +
                    "dbo.fn_DivideZero(dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1322, 1, 1), Itens.Quantidade) AS ValorRebate, dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1322, 1, 1) AS TotalRebate ";

                var strSQL_From = "FROM Pedidos_Itens Itens WITH(NOLOCK) " +
                    "INNER JOIN Pedidos WITH(NOLOCK) ON (Itens.PedidoID=Pedidos.PedidoID) " +
                    "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                    "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID=Transacoes.OperacaoID) " +
                    "INNER JOIN Pessoas Vendedores WITH(NOLOCK) ON (Pedidos.ProprietarioID=Vendedores.PessoaID) " +
                    "INNER JOIN Pessoas Equipes WITH(NOLOCK) ON (Pedidos.AlternativoID = Equipes.PessoaID) " +
                    "INNER JOIN Pessoas Parceiros WITH(NOLOCK) ON (Pedidos.ParceiroID=Parceiros.PessoaID) " +
                    "LEFT OUTER JOIN TiposAuxiliares_Itens Classificacoes WITH(NOLOCK) ON (Parceiros.ClassificacaoID=Classificacoes.ItemID) " +
                    "LEFT OUTER JOIN Pessoas_Telefones Faxes WITH(NOLOCK) ON (Pedidos.ParceiroID=Faxes.PessoaID AND Faxes.TipoTelefoneID=122 AND Faxes.Ordem=1) " +
                    "LEFT OUTER JOIN Pessoas_URLs URLs WITH(NOLOCK) ON (Pedidos.ParceiroID=URLs.PessoaID AND URLs.TipoURLID=124 AND URLs.Ordem=1) " +
                    "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pedidos.ParceiroID=Enderecos.PessoaID) " +
                    "LEFT OUTER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades Estados WITH(NOLOCK) ON (Enderecos.UFID=Estados.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) " +
                    "INNER JOIN Pessoas Clientes WITH(NOLOCK) ON (Pedidos.PessoaID=Clientes.PessoaID) " +
                    "INNER JOIN Pessoas_Enderecos Cliente_Enderecos WITH(NOLOCK) ON (Pedidos.PessoaID=Cliente_Enderecos.PessoaID) " +
                    "LEFT OUTER JOIN Localidades Cliente_Cidades WITH(NOLOCK) ON (Cliente_Enderecos.CidadeID=Cliente_Cidades.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades Cliente_Estados WITH(NOLOCK) ON (Cliente_Enderecos.UFID=Cliente_Estados.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades Cliente_Paises WITH(NOLOCK) ON (Cliente_Enderecos.PaisID=Cliente_Paises.LocalidadeID) " +
                    "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID=Produtos.ConceitoID) " +
                    "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.SujeitoID=Pedidos.EmpresaID) " +
                    "INNER JOIN Conceitos Familias WITH(NOLOCK) ON (Produtos.ProdutoID=Familias.ConceitoID) " +
                    "INNER JOIN Conceitos Marcas WITH(NOLOCK) ON (Produtos.MarcaID=Marcas.ConceitoID) " +
                    "LEFT OUTER JOIN Conceitos_Caracteristicas Caracteristicas WITH(NOLOCK) ON (Produtos.ConceitoID=Caracteristicas.ProdutoID AND dbo.fn_Produto_CaracteristicaOrdem(Caracteristicas.ProdutoID, Caracteristicas.CaracteristicaID)=1) " +
                    //Inserido por solicitação da Ivana para pegar caracteristica do processador. BJBN 05/08/2011
                    "LEFT OUTER JOIN Conceitos_Caracteristicas Caracteristicas2 WITH(NOLOCK) ON (Produtos.ConceitoID=Caracteristicas2.ProdutoID AND Caracteristicas2.CaracteristicaID = 5706) ";

                var strSQL_Where1 = "WHERE ((" + sFiltroEmpresa +
                    "Pedidos.PessoaID <> Pedidos.EmpresaID) AND " +
                    "((Enderecos.EndFaturamento=1 AND Enderecos.Ordem=1) AND ";

                var strSQL_Where2 = "(Cliente_Enderecos.EndFaturamento=1 AND Cliente_Enderecos.Ordem=1)) AND ";

                var strSQL_Where3 = "((ProdutosEmpresa.TipoRelacaoID=61) AND " +
                    strSohResultado +
                    "(Pedidos.Suspenso=0 AND ((Pedidos.TipoPedidoID=601 AND Pedidos.EstadoID>=30) OR (Pedidos.TipoPedidoID=602 AND Pedidos.EstadoID>=29) )) AND " +
                    "(Itens.dtMovimento>=" + sDataInicio + " AND Itens.dtMovimento<=" + sDataFim + " " +
                    sMarca + sReportaFabricante + sPedidosWeb + sFiltroFabricante + sFiltroFamilia + sFiltro + "))) ";

                strSQL = strSQL_Select1 + strSQL_Select2 + strSQL_Select3 + strSQL_From + strSQL_Where1 + strSQL_Where2 +
                    strSQL_Where3 + strSQL_OrderBy;
            }
            // Padrao Microsoft
            else if (selPadrao == 703)
            {
                var sMicrosoft_Select = "";

                //Adicionado Fabricante 91575 Microsoft Corp quando o fabricante escolhido for Microsoft US. Solicitado pela Ivana. BJBN
                if (selMicrosoftTipoRelatorio == 1)
                {
                    sMicrosoft_Select = "SELECT " +
                        "Itens.ProdutoID AS ProdutoID, Empresas.Nome AS Empresa, RelPessoas.Codigo AS [Código], " +
                        "'" + "O" + "'" + " AS Status, CONVERT(DATETIME," + "'" + sDataFim + "'" + "," + DATE_SQL_PARAM + ") AS [Data Rel.], NULL AS V1, NULL AS V2, " +
                        "NULL AS V3, NULL AS V4, NULL AS V5, NULL AS V6, NULL AS V7, NULL AS V8, Pessoas.Nome AS Parceiro, Parceiros.Fantasia AS [Fantasia], " +
                        "dbo.fn_Pessoa_Documento(Pedidos.PessoaID, NULL, NULL, 101, 111, 0) AS [CNPJ/CPF], NULL AS V9, ISNULL(Enderecos.Endereco, SPACE(0)) + SPACE(1) + ISNULL(Enderecos.Numero,SPACE(0)) + SPACE(1) + " +
                        "ISNULL(Enderecos.Complemento,SPACE(0)) AS [Endereço], NULL AS V10, NULL AS V11, Cidades.Localidade AS  " +
                        "Cidade, Estados.CodigoLocalidade2 AS UF, Enderecos.CEP AS CEP, Paises.CodigoLocalidade3 AS [País], " +
                        "dbo.fn_Data_Zero(NotasFiscais.dtNotaFiscal) AS [Data Ven], " +
                        "NULL AS V12, Produtos.PartNumber AS [Part Number], NULL AS V13, NULL AS V14, " +
                        "Produtos.Modelo AS Modelo," +
                        "SUM(CASE Pedidos.TipoPedidoID WHEN 602 THEN Itens.Quantidade ELSE 0 END) AS [Qtd Vend]," +
                        "SUM(CASE Pedidos.TipoPedidoID WHEN 601 THEN Itens.Quantidade ELSE 0 END) AS [Qtd Devol]," +
                        "NULL AS V15, dbo.fn_Pedido_ValorTotal(Itens.PedidoID, Itens.ProdutoID) AS [Preço Total], NULL AS V16, " +
                        "Vendedores.Nome AS Vendedor ";
                }
                else if (selMicrosoftTipoRelatorio == 2)
                {
                    sMicrosoft_Select = "SELECT " +
                        "Empresas.Fantasia AS [Company], NotasFiscais.NotaFiscal AS [Invoice Number], convert(varchar, NotasFiscais.dtNotaFiscal, 112) AS [Invoice Date], " +
                        "Pessoas.Nome AS [SB Name], Pessoas.PessoaID AS [SB ID], " +
                        "ISNULL(Enderecos.Endereco, SPACE(0)) + SPACE(1) + ISNULL(Enderecos.Numero,SPACE(0)) + SPACE(1) + " +
                        "ISNULL(Enderecos.Complemento,SPACE(0)) AS [SB Adress Line 1], SPACE(0) AS [SB Adress Line 2], " +
                        "SPACE(0) AS [SB Adress Line 3], Cidades.Localidade AS [SB City], " +
                        "Estados.CodigoLocalidade2 AS [SB State/Provincy], Enderecos.CEP AS [SB Zip/Postal Code], " +
                        "Paises.CodigoLocalidade3 AS [SB Country Code], Produtos.PartNumber AS [MS part Number], " +
                        "Produtos.Modelo AS [MS Product Name], " +
                        "SUM(CASE Pedidos.TipoPedidoID WHEN 602 THEN Itens.Quantidade ELSE 0 END) AS [Quantity sold]," +
                        "SUM(CASE Pedidos.TipoPedidoID WHEN 601 THEN Itens.Quantidade ELSE 0 END) AS [Quantity Returned]," +
                        "5138120073 AS [Microsoft Agreement Number], SPACE(0) AS [Discount Price], SPACE(0) AS [Promotion Number], " +
                        "Vendedores.Nome AS Vendedor, " +
                        "ISNULL((SELECT TOP 1 Conceito FROM Conceitos Linhas WITH(NOLOCK) WHERE Linhas.ConceitoID = Produtos.LinhaProdutoID), SPACE(0)) As Linha, " +
                        "ISNULL((SELECT TOP 1 Conceito FROM Conceitos Marcas WITH(NOLOCK) WHERE Marcas.ConceitoID = Produtos.MarcaID),SPACE(0)) As Marca, Produtos.ConceitoID AS ID ";
                }
                else if (selMicrosoftTipoRelatorio == 3)
                {
                    sMicrosoft_Select = "SELECT " +
                        "NotasFiscais.NotaFiscal AS [Invoice Number], convert(varchar, NotasFiscais.dtNotaFiscal, 112) AS [Invoice Date], " +
                        "Pessoas.Nome AS [SB Name], Pessoas.PessoaID AS [SB ID], " +
                        "ISNULL(Enderecos.Endereco, SPACE(0)) + SPACE(1) + ISNULL(Enderecos.Numero,SPACE(0)) + SPACE(1) + " +
                        "ISNULL(Enderecos.Complemento,SPACE(0)) AS [SB Adress Line 1], SPACE(0) AS [SB Adress Line 2], " +
                        "SPACE(0) AS [SB Adress Line 3], Cidades.Localidade AS [SB City], " +
                        "Estados.CodigoLocalidade2 AS [SB State/Provincy], Enderecos.CEP AS [SB Zip/Postal Code], " +
                        "Paises.CodigoLocalidade3 AS [SB Country Code], Produtos.PartNumber AS [MS part Number], " +
                        "Produtos.Modelo AS [MS Product Name], " +
                        "SUM(CASE Pedidos.TipoPedidoID WHEN 602 THEN Itens.Quantidade ELSE 0 END) AS [Quantity sold]," +
                        "SUM(CASE Pedidos.TipoPedidoID WHEN 601 THEN Itens.Quantidade ELSE 0 END) AS [Quantity Returned]," +
                        "5138120073 AS [Microsoft Agreement Number], SPACE(0) AS [Discount Price], SPACE(0) AS [Promotion Number], " +
                        "Vendedores.Nome AS Vendedor ";
                }
                else if (selMicrosoftTipoRelatorio == 4)
                {
                    sMicrosoft_Select = "SELECT " +
                        "NotasFiscais.NotaFiscal AS [Invoice Number], convert(varchar, NotasFiscais.dtNotaFiscal, 112) AS [Invoice Date], " +
                        "Pessoas.Nome AS [SB Name], Pessoas.PessoaID AS [SB ID], " +
                        "ISNULL(Enderecos.Endereco, SPACE(0)) + SPACE(1) + ISNULL(Enderecos.Numero,SPACE(0)) + SPACE(1) + " +
                        "ISNULL(Enderecos.Complemento,SPACE(0)) AS [SB Adress Line 1], SPACE(0) AS [SB Adress Line 2], " +
                        "SPACE(0) AS [SB Adress Line 3], Cidades.Localidade AS [SB City], " +
                        "Estados.CodigoLocalidade2 AS [SB State/Provincy], Enderecos.CEP AS [SB Zip/Postal Code], " +
                        "Paises.CodigoLocalidade3 AS [SB Country Code], Produtos.PartNumber AS [MS part Number], " +
                        "Produtos.Modelo AS [MS Product Name], " +
                        "SUM(CASE Pedidos.TipoPedidoID WHEN 602 THEN Itens.Quantidade ELSE 0 END) AS [Quantity sold]," +
                        "SUM(CASE Pedidos.TipoPedidoID WHEN 601 THEN Itens.Quantidade ELSE 0 END) AS [Quantity Returned]," +
                        "5138120073 AS [Microsoft Agreement Number], SPACE(0) AS [Discount Price], SPACE(0) AS [Promotion Number], " +
                        "Vendedores.Nome AS Vendedor, " +
                        "dbo.fn_Pessoa_URL(Vendedores.PessoaID, 124, NULL) AS [Email Vendedor], " +
                        "dbo.fn_Pessoa_Documento(Pedidos.ParceiroID, NULL, NULL, 111, 111, 0) AS [CNPJ Parceiro]";
                }

                //BJBN - Tipo relatorio 1 deve considerar o parceiro do Pedido. solicitado pela Ivana Pereira. 27/10/2010.
                var sPessoa = ((selMicrosoftTipoRelatorio == 2 || selMicrosoftTipoRelatorio == 4 || selMicrosoftTipoRelatorio == 1) ? "ParceiroID" : "PessoaID");

                var sMicrosoft_From = "FROM " +
                    "Pedidos_Itens Itens WITH(NOLOCK) " +
                        "INNER JOIN Pedidos WITH(NOLOCK) ON (Itens.PedidoID=Pedidos.PedidoID) " +
                        "INNER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                        "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID=Transacoes.OperacaoID) " +
                        "INNER JOIN Pessoas Empresas WITH(NOLOCK) ON (Pedidos.EmpresaID=Empresas.PessoaID) " +
                        "LEFT OUTER JOIN RelacoesPessoas RelPessoas WITH(NOLOCK) ON (RelPessoas.EstadoID <> 5 AND RelPessoas.SujeitoID=Empresas.PessoaID) " +
                                                                                    "AND (RelPessoas.TipoRelacaoID=21 AND RelPessoas.ObjetoID = " + selFabricante + ") " +
                        "INNER JOIN Pessoas Pessoas WITH(NOLOCK) ON (Pedidos." + sPessoa + "=Pessoas.PessoaID) " +
                        "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pessoas.PessoaID=Enderecos.PessoaID) " +
                        "LEFT OUTER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) " +
                        "LEFT OUTER JOIN Localidades Estados WITH(NOLOCK) ON (Enderecos.UFID=Estados.LocalidadeID) " +
                        "LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) " +
                        "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID=Produtos.ConceitoID) " +
                        "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.SujeitoID=Pedidos.EmpresaID) " +
                        "INNER JOIN RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK) ON (ProdutosEmpresa.RelacaoID=Fornecedores.RelacaoID) " +
                        "INNER JOIN Pessoas Vendedores WITH(NOLOCK) ON (Pedidos.ProprietarioID = Vendedores.PessoaID) ";

                var sMicrosoft_Where1 = "WHERE " +
                    "Pedidos.EmpresaID IN " + EmpresasIn + " AND " +
                    "Pedidos.PessoaID <> Pedidos.EmpresaID AND " +
                    "Enderecos.EndFaturamento=1 AND Enderecos.Ordem=1 AND ";

                if (selFabricante > 0)
                    sMicrosoft_Where1 += "Produtos.FabricanteID = " + selFabricante + " AND ";

                var sMicrosoft_Where2 = "((ProdutosEmpresa.TipoRelacaoID=61 AND (Fornecedores.Ordem=1)) AND " +
                    "(" + strSohResultado +
                    // A pedido do marcel em 12/09/2005 deve-se considerar no relatorio o estado 27 (Recebimento) tambem
                    "(Pedidos.Suspenso=0 AND ((Pedidos.TipoPedidoID=601 AND (Pedidos.EstadoID=27 OR Pedidos.EstadoID>=30)) OR (Pedidos.TipoPedidoID=602 AND Pedidos.EstadoID>=29) )) AND " +
                    // A pedido do marcel em 23/08/2005, deve-se reportar pela data da NF e nao pela data
                    // do itens.dtMovimento
                    "(NotasFiscais.dtNotaFiscal>=" + sDataInicio + " AND NotasFiscais.dtNotaFiscal<=" + sDataFim + " " +
                    sMarca + sReportaFabricante + sPedidosWeb + sFiltroFamilia + sFiltro + " ))) ";

                var sMicrosoft_GroupBy = "";

                if (selMicrosoftTipoRelatorio == 1)
                {
                    sMicrosoft_GroupBy = "GROUP BY " +
                        "Empresas.Nome, RelPessoas.Codigo, Itens.ProdutoID, Pessoas.Nome, dbo.fn_Pessoa_Documento(Pedidos.PessoaID, NULL, NULL, 101, 111, 0), " +
                        "ISNULL(Enderecos.Endereco, SPACE(0)) + SPACE(1) + ISNULL(Enderecos.Numero,SPACE(0)) + SPACE(1) + " +
                        "ISNULL(Enderecos.Complemento,SPACE(0)), Cidades.Localidade, Estados.CodigoLocalidade2, Enderecos.CEP, " +
                        "Paises.CodigoLocalidade3, NotasFiscais.dtNotaFiscal, Produtos.PartNumber, Produtos.Modelo, " +
                        "Pedidos.TipoPedidoID, Itens.PedidoID, Vendedores.Fantasia, Vendedores.Nome ";
                }
                else if (selMicrosoftTipoRelatorio == 2)
                {
                    sMicrosoft_GroupBy = "GROUP BY " +
                        "Empresas.Fantasia, NotasFiscais.NotaFiscal, NotasFiscais.dtNotaFiscal, Pessoas.Nome, Pessoas.PessoaID," +
                        "ISNULL(Enderecos.Endereco, SPACE(0)) + SPACE(1) + ISNULL(Enderecos.Numero,SPACE(0)) + SPACE(1) + " +
                        "ISNULL(Enderecos.Complemento,SPACE(0)), Cidades.Localidade, Estados.CodigoLocalidade2, Enderecos.CEP, " +
                        "Paises.CodigoLocalidade3, Produtos.PartNumber, Produtos.Modelo, Vendedores.Nome,Produtos.LinhaProdutoID,Produtos.MarcaID,Produtos.ConceitoID ";
                }
                else if (selMicrosoftTipoRelatorio == 3)
                {
                    sMicrosoft_GroupBy = "GROUP BY " +
                        "NotasFiscais.NotaFiscal, NotasFiscais.dtNotaFiscal, Pessoas.Nome, Pessoas.PessoaID," +
                        "ISNULL(Enderecos.Endereco, SPACE(0)) + SPACE(1) + ISNULL(Enderecos.Numero,SPACE(0)) + SPACE(1) + " +
                        "ISNULL(Enderecos.Complemento,SPACE(0)), Cidades.Localidade, Estados.CodigoLocalidade2, Enderecos.CEP, " +
                        "Paises.CodigoLocalidade3, Produtos.PartNumber, Produtos.Modelo, Vendedores.Nome ";
                }
                else if (selMicrosoftTipoRelatorio == 4)
                {
                    sMicrosoft_GroupBy = "GROUP BY " +
                        "NotasFiscais.NotaFiscal, NotasFiscais.dtNotaFiscal, Pessoas.Nome, Pessoas.PessoaID," +
                        "ISNULL(Enderecos.Endereco, SPACE(0)) + SPACE(1) + ISNULL(Enderecos.Numero,SPACE(0)) + SPACE(1) + " +
                        "ISNULL(Enderecos.Complemento,SPACE(0)), Cidades.Localidade, Estados.CodigoLocalidade2, Enderecos.CEP, " +
                        "Paises.CodigoLocalidade3, Produtos.PartNumber, Produtos.Modelo, Vendedores.PessoaID, Vendedores.Nome, Pedidos.ParceiroID ";
                }

                var sMicrosoft_OrderBy = "";

                if (selMicrosoftTipoRelatorio == 1)
                {
                    sMicrosoft_OrderBy = "ORDER BY " +
                        "NotasFiscais.dtNotaFiscal, Itens.PedidoID";
                }
                else if (selMicrosoftTipoRelatorio == 2)
                {
                    sMicrosoft_OrderBy = "ORDER BY " +
                    "NotasFiscais.NotaFiscal";
                }
                else if (selMicrosoftTipoRelatorio == 3)
                {
                    sMicrosoft_OrderBy = "ORDER BY " +
                    "NotasFiscais.NotaFiscal";
                }
                else if (selMicrosoftTipoRelatorio == 4)
                {
                    sMicrosoft_OrderBy = "ORDER BY " +
                    "NotasFiscais.NotaFiscal";
                }

                strSQL = sMicrosoft_Select + sMicrosoft_From + sMicrosoft_Where1 + sMicrosoft_Where2 + sMicrosoft_GroupBy +
                    sMicrosoft_OrderBy;
            }

            // Padrao Proxim
            else if (selPadrao == 705)
            {
                var sProxim_Select = "SELECT " +
                    "Itens.PedidoID AS [order], " +
                    "NotasFiscais.NotaFiscal AS [invoice], " +
                    "dbo.fn_Data_Zero(Itens.dtMovimento) AS [date], " +
                    "Parceiros.Nome AS [sold to customer name], Paises.NomeInternacional AS [sold to country], " +
                    "Transportadoras.Nome AS [ship to customer name], TranspPaises.NomeInternacional AS [ship to country], " +
                    "Produtos.PartNumber AS [item number], Produtos.Modelo AS [Model], Produtos.Descricao AS [item description], " +
                    "CONVERT(INT, CASE Pedidos.TipoPedidoID WHEN 602 THEN Itens.Quantidade ELSE Itens.Quantidade * (-1) END) AS [unit sold], " +
                    "CONVERT(NUMERIC(11,2), ISNULL(dbo.fn_DivideZero(Itens.ValorCustoBase, Itens.Quantidade), 0)) AS [unit cogs], " +
                    "CONVERT(NUMERIC(11,2), ISNULL(Itens.ValorCustoBase * POWER(-1, Pedidos.TipoPedidoID), 0)) AS [extended cogs], " +
                    "(SELECT TOP 1 c.Campanha FROM Pedidos_Itens_Campanhas a WITH(NOLOCK), Campanhas_Produtos b WITH(NOLOCK), Campanhas c WITH(NOLOCK) WHERE (Itens.PedItemID = a.PedItemID AND a.CamProdutoID = b.CamProdutoID AND b.CampanhaID = c.CampanhaID AND c.TipoCampanhaID = 1321)) AS [DPA], " +
                    "CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1321, 1, 1), Itens.Quantidade)) AS [Valor DPA], " +
                    "dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1321, 1, 1) AS [Total DPA], " +
                    "(SELECT TOP 1 c.Campanha FROM Pedidos_Itens_Campanhas a WITH(NOLOCK), Campanhas_Produtos b WITH(NOLOCK), Campanhas c WITH(NOLOCK) WHERE (Itens.PedItemID = a.PedItemID AND a.CamProdutoID = b.CamProdutoID AND b.CampanhaID = c.CampanhaID AND c.TipoCampanhaID = 1322)) AS [Rebate], " +
                    "CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1322, 1, 1), Itens.Quantidade)) AS [Valor Rebate], " +
                    "dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1322, 1, 1) AS [Total Rebate], " +
                    "(SELECT TOP 1 c.Campanha FROM Pedidos_Itens_Campanhas a WITH(NOLOCK), Campanhas_Produtos b WITH(NOLOCK), Campanhas c WITH(NOLOCK) WHERE (Itens.PedItemID = a.PedItemID AND a.CamProdutoID = b.CamProdutoID AND b.CampanhaID = c.CampanhaID AND c.TipoCampanhaID = 1323)) AS [Bundle], " +
                    "CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1323, 1, 1), Itens.Quantidade)) AS [Valor Bundle], " +
                    "dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1323, 1, 1) AS [Total Bundle] ";

                var sProxim_From = "FROM " +
                    "Pedidos_Itens Itens WITH(NOLOCK) " +
                    "INNER JOIN Pedidos WITH(NOLOCK) ON (Itens.PedidoID=Pedidos.PedidoID) " +
                    "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                    "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID=Transacoes.OperacaoID) " +
                    "INNER JOIN Pessoas Empresas WITH(NOLOCK) ON (Pedidos.EmpresaID=Empresas.PessoaID) " +
                    "INNER JOIN Pessoas Parceiros WITH(NOLOCK) ON (Pedidos.ParceiroID=Parceiros.PessoaID) " +
                    "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pedidos.ParceiroID=Enderecos.PessoaID) " +
                    "INNER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) " +
                    "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID=Produtos.ConceitoID) " +
                    "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.SujeitoID= Pedidos.EmpresaID) " +
                    "INNER JOIN Pessoas Transportadoras WITH(NOLOCK) ON (Pedidos.TransportadoraID = Transportadoras.PessoaID) " +
                    "INNER JOIN Pessoas_Enderecos TranspEnderecos WITH(NOLOCK) ON (Pedidos.TransportadoraID = TranspEnderecos.PessoaID) " +
                    "INNER JOIN Localidades TranspPaises WITH(NOLOCK) ON (TranspEnderecos.PaisID = TranspPaises.LocalidadeID) ";

                var sProxim_Where1 = "WHERE " +
                    "((Pedidos.EmpresaID IN " + EmpresasIn + " AND " +
                    "Pedidos.PessoaID <> Pedidos.EmpresaID) AND " +
                    "((Enderecos.EndFaturamento=1 AND Enderecos.Ordem=1)) AND ";

                var sProxim_Where2 = "(TranspEnderecos.EndFaturamento = 1 AND TranspEnderecos.Ordem = 1) AND " +
                    "((ProdutosEmpresa.TipoRelacaoID=61 AND " +
                    "(" + strSohResultado +
                    "(Pedidos.Suspenso=0 AND ((Pedidos.TipoPedidoID=601 AND Pedidos.EstadoID>=30) OR (Pedidos.TipoPedidoID=602 AND Pedidos.EstadoID>=29) )) AND " +
                    "(Itens.dtMovimento>=" + sDataInicio + " AND Itens.dtMovimento<=" + sDataFim + " " +
                    sMarca + sReportaFabricante + sPedidosWeb + sFiltroFabricante + sFiltroFamilia + sFiltro + " ))))) ";

                var sProxim_GroupBy = "GROUP BY " +
                    "Itens.ProdutoID, NotasFiscais.NotaFiscal, " +
                    "Itens.Quantidade, ProdutosEmpresa.RelacaoID, Pedidos.TaxaMoeda, " +
                    "Itens.PedItemID, Itens.ValorCustoBase, Parceiros.PessoaID, Parceiros.Nome, Parceiros.Fantasia," +
                    "Paises.NomeInternacional, Itens.dtMovimento, Produtos.PartNumber, " +
                    "Produtos.Modelo, Produtos.Descricao, Pedidos.TipoPedidoID, Itens.PedidoID, " +
                    "Transportadoras.Nome, TranspPaises.NomeInternacional ";

                var sProxim_OrderBy = "ORDER BY " +
                    "Itens.dtMovimento, Itens.PedidoID";

                strSQL = sProxim_Select + sProxim_From + sProxim_Where1 + sProxim_Where2 + sProxim_GroupBy +
                    sProxim_OrderBy;
            }

            // Pedidos
            if (selPadrao == 706)
            {
                strSohResultado = (chkSoResultado ? "Transacoes.Resultado=1 AND " : "");

                var sDadosParceiro = "Cidades.Localidade AS Cidade, Estados.CodigoLocalidade2 AS UF, (SELECT ItemMasculino FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE ItemID=Estados.TipoRegiaoID) AS [Região], " +
                    "Paises.CodigoLocalidade2 AS [País]";

                if (chkDadosParceiro)
                    sDadosParceiro = "Classificacoes.ItemMasculino AS Classificacao, dbo.fn_Pessoa_Documento(Pedidos.ParceiroID, NULL, NULL, 101, 111, 0) AS [CNPJ/CPF], " +
                        "Enderecos.Endereco AS [Endereço], Enderecos.Numero AS [Número], Enderecos.Complemento AS Complemento, " +
                        "Enderecos.Bairro AS Bairro, " + sDadosParceiro + ", Clientes.Nome AS Cliente, " +
                        "CASE WHEN Pedidos.TransportadoraID = Pedidos.ParceiroID THEN " + "'" + "O Mesmo" + "'" + " " +
                        "WHEN Pedidos.TransportadoraID = Pedidos.EmpresaID THEN " + "'" + "Empresa" + "'" + " " +
                        "ELSE Transportadoras.Nome END AS Transportadora ";

                var sDadosFaturamento = "";

                if (chkDadosResultado)
                {
                    sDadosFaturamento = " ,CONVERT(NUMERIC(10,5), Pedidos.TaxaMoeda) AS [Taxa Moeda]," +
                        "SUM((Itens.ValorFaturamento " + sTaxaMoeda + ")) AS Faturamento, " +
                        "SUM((Itens.ValorFaturamentoBase " + sTaxaMoeda + ")) AS [Fatur Base], " +
                        "SUM((Itens.ValorFinanceiro " + sTaxaMoeda + ")) AS Financeiro, " +
                        "SUM((Itens.ValorContribuicao " + sTaxaMoeda + ")) AS [Contribuição], " +
                        "SUM((Itens.ValorContribuicaoAjustada " + sTaxaMoeda + ")) AS [Contr Ajustada], " +
                        "CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(SUM(Itens.ValorContribuicaoAjustada), SUM(Itens.ValorFaturamentoBase)) * 100) AS MC, " +
                        "(Pedidos.PrazoMedioPagamento) AS PMP ";
                }

                var sDadosComissao = "";

                if (chkComissaoVendas)
                {
                    if (chkMoedaLocal2)
                        sDadosComissao = " ,Pedidos.PercentualSup AS [Percentual SUP], (dbo.fn_Pedido_Totais(Pedidos.PedidoID, 32) * POWER(-1, Pedidos.TipoPedidoID)) AS [Parcela SUP] ";
                    else
                        sDadosComissao = " ,Pedidos.PercentualSup AS [Percentual SUP], ( dbo.fn_DivideZero(dbo.fn_Pedido_Totais(Pedidos.PedidoID, 32), Pedidos.TaxaMoeda) * POWER(-1, Pedidos.TipoPedidoID)) AS [Parcela SUP] ";
                }

                strSQL_Select1 = "SELECT " +
                    "MONTH(Itens.dtMovimento) AS Mes, CONVERT(VARCHAR(10), Itens.dtMovimento, " + DATE_SQL_PARAM + ") AS Data, " +
                    "Pedidos.PedidoID AS Pedido, NotasFiscais.NotaFiscal AS NF, " +
                    "Transacoes.Operacao AS Transacao, Vendedores.PessoaID AS VendedorID, Vendedores.Fantasia AS Vendedor, " +
                    "Pedidos.ParceiroID AS ParceiroID, Parceiros.Nome AS Parceiro, Parceiros.Fantasia AS [Fantasia], " + sDadosParceiro + sDadosFaturamento + sDadosComissao;

                var strSQL_From = "FROM Pedidos_Itens Itens WITH(NOLOCK) " +
                    "INNER JOIN Pedidos WITH(NOLOCK) ON (Itens.PedidoID=Pedidos.PedidoID) " +
                    "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                    "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID=Transacoes.OperacaoID) " +
                    "INNER JOIN Pessoas Vendedores WITH(NOLOCK) ON (Pedidos.ProprietarioID=Vendedores.PessoaID) " +
                    "INNER JOIN Pessoas Parceiros WITH(NOLOCK) ON (Pedidos.ParceiroID=Parceiros.PessoaID) " +
                    "LEFT OUTER JOIN TiposAuxiliares_Itens Classificacoes WITH(NOLOCK) ON (Parceiros.ClassificacaoID=Classificacoes.ItemID) " +
                    "LEFT OUTER JOIN Pessoas_Telefones Faxes WITH(NOLOCK) ON (Pedidos.ParceiroID=Faxes.PessoaID AND Faxes.TipoTelefoneID=122 AND Faxes.Ordem=1) " +
                    "LEFT OUTER JOIN Pessoas_URLs URLs WITH(NOLOCK) ON (Pedidos.ParceiroID=URLs.PessoaID AND URLs.TipoURLID=124 AND URLs.Ordem=1) " +
                    "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pedidos.ParceiroID=Enderecos.PessoaID) " +
                    "LEFT OUTER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades Estados WITH(NOLOCK) ON (Enderecos.UFID=Estados.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) " +
                    "INNER JOIN Pessoas Clientes WITH(NOLOCK) ON (Pedidos.PessoaID=Clientes.PessoaID) " +
                    "INNER JOIN Pessoas_Enderecos Cliente_Enderecos WITH(NOLOCK) ON (Pedidos.PessoaID=Cliente_Enderecos.PessoaID) " +
                    "LEFT OUTER JOIN Localidades Cliente_Cidades WITH(NOLOCK) ON (Cliente_Enderecos.CidadeID=Cliente_Cidades.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades Cliente_Estados WITH(NOLOCK) ON (Cliente_Enderecos.UFID=Cliente_Estados.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades Cliente_Paises WITH(NOLOCK) ON (Cliente_Enderecos.PaisID=Cliente_Paises.LocalidadeID) " +
                    "INNER JOIN Pessoas Transportadoras WITH(NOLOCK) ON (Pedidos.TransportadoraID = Transportadoras.PessoaID) " +
                    "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (ProdutosEmpresa.ObjetoID=Itens.ProdutoID) AND (ProdutosEmpresa.SujeitoID = Pedidos.EmpresaID) " +
                    "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID=Produtos.ConceitoID) " +
                    "INNER JOIN Conceitos Familias WITH(NOLOCK) ON (Produtos.ProdutoID=Familias.ConceitoID) " +
                    "INNER JOIN Conceitos Marcas WITH(NOLOCK) ON (Produtos.MarcaID=Marcas.ConceitoID) ";

                var strSQL_Where1 = "WHERE ((Pedidos.EmpresaID IN " + EmpresasIn + " AND " +
                    "(ProdutosEmpresa.TipoRelacaoID = 61) AND " +
                    "Pedidos.PessoaID <> Pedidos.EmpresaID) AND " +
                    "((Enderecos.EndFaturamento=1 AND Enderecos.Ordem=1) AND ";

                var strSQL_Where2 = "" +
                    "(Cliente_Enderecos.EndFaturamento=1 AND Cliente_Enderecos.Ordem=1)) AND ";

                var strSQL_Where3 = "((" + strSohResultado +
                    "(Pedidos.Suspenso=0 AND ((Pedidos.TipoPedidoID=601 AND Pedidos.EstadoID>=30) OR (Pedidos.TipoPedidoID=602 AND Pedidos.EstadoID>=29) )) AND " +
                    "(Itens.dtMovimento>=" + sDataInicio + " AND Itens.dtMovimento<=" + sDataFim + " " +
                    sMarca + sReportaFabricante + sPedidosWeb + sFiltroFabricante + sFiltroFamilia + sFiltro + ")))) ";

                var strSQL_GroupBy = "GROUP BY MONTH(Itens.dtMovimento), CONVERT(VARCHAR(10), Itens.dtMovimento, " + DATE_SQL_PARAM + "), Pedidos.PedidoID, NotasFiscais.NotaFiscal, Transacoes.Operacao, Vendedores.PessoaID, Vendedores.Fantasia, " +
                    "Pedidos.ParceiroID, Parceiros.Nome, Cidades.Localidade, Estados.codigoLocalidade2, Estados.TipoRegiaoID, Paises.Codigolocalidade2, " +
                    "Classificacoes.ItemMasculino, Enderecos.CEP, Enderecos.Endereco, Enderecos.Numero, Enderecos.Complemento, Enderecos.Bairro, " +
                    "Clientes.Nome, Pedidos.TransportadoraID, Transportadoras.Nome, Pedidos.EmpresaID, Pedidos.TaxaMoeda, Pedidos.PrazoMedioPagamento, Pedidos.PercentualSup, Pedidos.TipoPedidoID ";

                strSQL_OrderBy = "ORDER BY Mes, Data, Pedido";

                strSQL = strSQL_Select1 + strSQL_From + strSQL_Where1 + strSQL_Where2 +
                    strSQL_Where3 + strSQL_GroupBy + strSQL_OrderBy;
            }
            // Epson
            else if (selPadrao == 707)
            {
                strSQL_Select2 = "SELECT " + "'" + EmpresaNome + "'" + " + CHAR(9) + " + "'" + Mesano + "'" + " " +
                        "UNION ALL " +
                        "SELECT ISNULL( dbo.fn_Pessoa_Documento(" + EmpresaID + ", NULL, NULL, 101, 111, 0) , SPACE(0)) + CHAR(9) +  " +
                        "Parceiros.Nome + CHAR(9) + " +
                        "ISNULL(dbo.fn_Pessoa_Documento(Pedidos.ParceiroID, NULL, NULL, 101, 111, 0),SPACE(0)) + CHAR(9) + " +
                        "ISNULL( dbo.fn_Pessoa_Documento(Pedidos.ParceiroID, NULL, NULL, 102, 112, 0) , SPACE(0)) + CHAR(9) + " +
                        "ISNULL(dbo.fn_Pessoa_Endereco(Pedidos.ParceiroID, 1), SPACE(0)) + CHAR(9) + ISNULL(Enderecos.CEP,SPACE(0)) + CHAR(9) + " +
                        "SPACE(0) + CHAR(9) + Vendedores.Nome + CHAR(9) + ISNULL(dbo.fn_Pessoa_Documento(Vendedores.PessoaID, NULL, NULL, 101, 111, 0),SPACE(0)) + CHAR(9) + ISNULL(dbo.fn_Pessoa_URL(Vendedores.PessoaID, 124,NULL), SPACE(0)) + CHAR(9) + " +
                        "ISNULL(LTRIM(STR(NotasFiscais.NotaFiscal)), SPACE(0)) + CHAR(9) + ISNULL(CONVERT(VARCHAR(8), Itens.dtMovimento, 3), SPACE(0)) + CHAR(9) + " +
                        "ISNULL(Produtos.PartNumber, SPACE(0)) + CHAR(9) + " +
                        "ISNULL(LTRIM(STR(Itens.Quantidade * POWER(-1,Pedidos.TipoPedidoID))), SPACE(0)) + CHAR(9) + " +
                        "ISNULL(REPLACE(dbo.fn_Numero_Formata(dbo.fn_PedidoItem_Totais(Itens.PedItemID, 7),2,1,103)," + "'" + "." + "'" + "," + "'" + "'" + "), SPACE(0)) + CHAR(9) + " +
                        "ISNULL(STR(CEILING(dbo.fn_PedidoItem_Totais(Itens.PedItemID, 5))), SPACE(0)) + CHAR(9) + SPACE(0) + CHAR(9) + SPACE(0) + CHAR(9) + SPACE(0) ";

                var strSQL_From = "FROM Pedidos_Itens Itens WITH(NOLOCK) " +
                    "INNER JOIN Pedidos WITH(NOLOCK) ON (Itens.PedidoID=Pedidos.PedidoID) " +
                    "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                    "LEFT OUTER JOIN Pessoas_URLs URLs WITH(NOLOCK) ON (Pedidos.ParceiroID=URLs.PessoaID AND URLs.TipoURLID=124 AND URLs.Ordem=1) " +
                    "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID=Transacoes.OperacaoID) " +
                    "INNER JOIN Pessoas Vendedores WITH(NOLOCK) ON (Pedidos.ProprietarioID=Vendedores.PessoaID) " +
                    "INNER JOIN Pessoas Parceiros WITH(NOLOCK) ON (Pedidos.ParceiroID=Parceiros.PessoaID) " +
                    "INNER JOIN Pessoas Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID=Pessoas.PessoaID) " +
                    "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pedidos.ParceiroID=Enderecos.PessoaID) " +
                    "LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) " +
                    "INNER JOIN Pessoas Clientes WITH(NOLOCK) ON (Pedidos.PessoaID=Clientes.PessoaID) " +
                    "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID=Produtos.ConceitoID) " +
                    "LEFT OUTER JOIN Conceitos_Caracteristicas Caracteristicas WITH(NOLOCK) ON (Produtos.ConceitoID=Caracteristicas.ProdutoID AND dbo.fn_Produto_CaracteristicaOrdem(Caracteristicas.ProdutoID, Caracteristicas.CaracteristicaID)=1) " +
                    "INNER JOIN Conceitos Familias WITH(NOLOCK) ON (Produtos.ProdutoID=Familias.ConceitoID) " +
                    "INNER JOIN Conceitos Marcas WITH(NOLOCK) ON (Produtos.MarcaID=Marcas.ConceitoID) " +
                    "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.SujeitoID = Pedidos.EmpresaID) ";

                var strSQL_Where1 = "WHERE ((Pedidos.EmpresaID IN " + EmpresasIn + " AND " +
                    "Pedidos.PessoaID <> Pedidos.EmpresaID) AND " +
                    "(Pessoas.TipoPessoaID = 52) AND " +
                    "(Parceiros.TipoPessoaID<>51 AND " +
                    "(Enderecos.EndFaturamento=1 AND Enderecos.Ordem=1)) ";

                var strSQL_Where2 = " AND ";

                var strSQL_Where3 = "((ProdutosEmpresa.TipoRelacaoID=61) AND " +
                    strSohResultado +
                    "(Pedidos.Suspenso=0 AND Pedidos.TipoPedidoID=602 AND Pedidos.EstadoID>=29) AND " +
                    "(Itens.dtMovimento>=" + sDataInicio + " AND Itens.dtMovimento<=" + sDataFim + " " +
                    sMarca + sReportaFabricante + sPedidosWeb + sFiltroFabricante + sFiltroFamilia + sFiltro + "))) ";

                strSQL = strSQL_Select2 + strSQL_Select3 + strSQL_From + strSQL_Where1 + strSQL_Where2 +
                    strSQL_Where3;

            }
            // AMD
            else if (selPadrao == 708)
            {

                sFiltroEmpresa = "";
                strSQL_OrderBy = "ORDER BY EmpresaID, Data_, Pedido_";

                if (glb_nEmpresaID == 3)
                    sFiltroEmpresa = " Pedidos.EmpresaID IN " + EmpresasIn + " AND ";
                else
                    sFiltroEmpresa = " Pedidos.EmpresaID IN " + EmpresasIn + " AND ";

                strSQL_Select2 = "Pedidos.EmpresaID AS EmpresaID, (CASE Pedidos.EmpresaID WHEN 7 THEN '1714' ELSE '4035' END) AS [CUST NO], (CASE Pedidos.EmpresaID WHEN 2 THEN 'ALCATRJ' WHEN 10 THEN 'ALCATRJ' ELSE 'APRSLLAM'  END) AS [BRANCH CODE], " +
                    "(CASE Pedidos.TipoPedidoID WHEN 602 THEN " + "'" + "MSST" + "'" + " " +
                        "ELSE " + "'" + "MERT" + "'" + " END) AS [TRXN CODE]," +
                    "Produtos.PartNumber AS [MATERIAL], " +
                    "CONVERT(INT, CASE Pedidos.TipoPedidoID WHEN 602 THEN Itens.Quantidade " +
                        "ELSE Itens.Quantidade * (-1) END) AS [QUANTITY], " +
                    "dbo.fn_String_Substituicao(NotasFiscais.NotaFiscal,1) AS [RESALE INVOICE NO], Itens.Ordem AS [ITEM NO]," +
                    "dbo.fn_Numero_Formata((dbo.fn_DivideZero(Itens.ValorUnitario, Pedidos.TaxaMoeda)), 2, 1, 101) AS [RESALE PRICE], MoedasSistema.ConceitoAbreviado AS [RESALE CURRENCY], " +
                    "ISNULL(CONVERT(VARCHAR(4),YEAR(NotasFiscais.dtNotaFiscal)), SPACE(0)) + " +
                    "ISNULL(CONVERT(VARCHAR(2),dbo.fn_Pad(MONTH(NotasFiscais.dtNotaFiscal), 2, " + "'" + "0" + "'" + ", " + "'" + "L" + "'" + ")), SPACE(0)) + " +
                    "ISNULL(CONVERT(VARCHAR(2),dbo.fn_Pad(DAY(NotasFiscais.dtNotaFiscal), 2, " + "'" + "0" + "'" + ", " + "'" + "L" + "'" + ")), SPACE(0)) AS [RESALE INVOICE DATE], " +
                    "Parceiros.PessoaID AS [REPORTED RESALE CUST CODE], " +
                    "LEFT(dbo.fn_String_Substituicao(Parceiros.Nome,1),35) AS [REPORTED RESALE CUST NAME], " +
                    "Parceiros.PessoaID AS [REPORTED RESALE CUST DEALER ID], " +
                    "LEFT(dbo.fn_String_Substituicao(ISNULL(Enderecos.Endereco, SPACE(0)) + SPACE(1) + " +
                    "ISNULL(Enderecos.Numero, SPACE(0)),1) + SPACE(1) + ISNULL(dbo.fn_String_Substituicao(ISNULL(Enderecos.Complemento,SPACE(0)),1), SPACE(0)),35) AS [REPORTED RESALE CUST ADDRESS]," +
                    "dbo.fn_String_Substituicao(ISNULL(Cidades.Localidade, SPACE(0)),1) AS [REPORTED RESALE CUST CITY]," +
                    "dbo.fn_String_Substituicao(Enderecos.CEP,1) AS [REPORTED RESALE CUST POSTAL CODE]," +
                    "LEFT(ISNULL(Paises.CodigoLocalidade2, SPACE(0)),3) AS [REPORTED RESALE CUST COUNTRY], " +
                    "LEFT(ISNULL(Estados.CodigoLocalidade2, SPACE(0)),3) AS [REPORTED RESALE CUST STATE], " +
                    "(SELECT TOP 1 c.Codigo FROM Pedidos_Itens_Campanhas a WITH(NOLOCK), Campanhas_Produtos b WITH(NOLOCK), Campanhas c WITH(NOLOCK) WHERE (Itens.PedItemID = a.PedItemID AND a.CamProdutoID = b.CamProdutoID AND b.CampanhaID = c.CampanhaID AND c.TipoCampanhaID = 1321)) AS [CONTRACT NO], " +
                    "SPACE(0) AS [ITEM NO ], dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1321, 1, 1) AS [REPORTED CLAIM], MoedasSistema.ConceitoAbreviado AS [REPORT CLAIM CURRENCY], " +
                    "(SELECT TOP 1 c.CampanhaID " +
                        "FROM Pedidos_Itens_Campanhas a WITH(NOLOCK), Campanhas_Produtos b WITH(NOLOCK), Campanhas c WITH(NOLOCK) " +
                        "WHERE (Itens.PedItemID = a.PedItemID AND a.CamProdutoID = b.CamProdutoID AND b.CampanhaID = c.CampanhaID AND c.TipoCampanhaID = 1321)) AS [CLAIM REF NO], " +
                    "(CASE Pedidos.TipoPedidoID WHEN 602 THEN SPACE(0) ELSE CONVERT(VARCHAR(10),Itens.PedidoID) END) " +
                    "AS [RETURN MEMO NO]," +
                    "(CASE Pedidos.TipoPedidoID WHEN 602 THEN SPACE(0) ELSE CONVERT(VARCHAR(10),Itens.Ordem) END) " +
                    "AS [ITEM NO],dbo.fn_PedidoItem_Totais(Itens.PedItemID, 21) AS [INVENTORY COST], MoedasSistema.ConceitoAbreviado AS [INVENTORY COST CURRENCY], " +
                    "SPACE(0) AS [DESIGN REG NO], dbo.fn_PedidoItem_Totais(Itens.PedItemID, 22) AS [DPA CONTRACT PRICE], MoedasSistema.ConceitoAbreviado AS [DPA CONTRACT CURRENCY], " +
                    "dbo.fn_Pessoa_Documento(Vendedores.PessoaID, NULL, NULL, 101, 101, 0) AS CPFVendedor, " +
                    "dbo.fn_Pessoa_Documento(Parceiros.PessoaID, NULL, NULL, 111, 111, 0) AS CNPJParceiro, " +
                    "Vendedores.Nome AS NomeVendedor, Itens.ProdutoID AS ProdutoID, " +
                    "(SELECT TOP 1 Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID=dbo.fn_Pessoa_Grupo(Vendedores.PessoaID)) AS EquipeVendas, " +
                    "CONVERT(NUMERIC(11,2),(SELECT TOP 1 CustoFOB FROM RelacoesPesCon_Fornecedores WITH(NOLOCK) WHERE (RelacaoID = ProdutosEmpresa.RelacaoID) ORDER BY Ordem)) AS CustoFOB, " +
                    "dbo.fn_Pessoa_Telefone(Parceiros.PessoaID, 119, 120, 1, 0, NULL) AS [Telefone], " +
                    "(SELECT TOP 1 URL FROM Pessoas_URLs  WITH(NOLOCK) WHERE (Parceiros.PessoaID=PessoaID AND TipoURLID = 124) ORDER BY Ordem) AS [e-mail], " +
                    "ISNULL(CONVERT(VARCHAR,nf_referencia.NotaFiscal),SPACE(0)) AS [ORIGINAL RESALE INVOICE NO]," +
                    "ISNULL(CONVERT(VARCHAR,nf_referencia.dtNotaFiscal,101),SPACE(0)) AS [ORIGINAL RESALE INVOICE DATE], " +
                    "ISNULL(CONVERT(VARCHAR,rma.Numero),SPACE(0)) AS [RMA NO], " +
                    "ISNULL(CONVERT(VARCHAR,(SELECT TOP 1 b.NotaFiscal FROM Pedidos a " +
                                                "INNER JOIN NotasFiscais b ON b.NotaFiscalID = a.NotaFiscalID " +
                                                "INNER JOIN Pedidos_Itens c ON a.PedidoID = c.PedidoID " +
                                            "WHERE a.Observacao LIKE '" + "%<DROP_SHIP%" + "'" +
                                                    " AND c.PedItemID = Itens.PedItemID )), SPACE(0)) AS [DROP SHIP AMD INVOICE NO] ";

                var strSQL_From = "FROM Pedidos_Itens Itens WITH(NOLOCK) " +
                        "LEFT OUTER JOIN dbo.fn_Numeros_RMA_tbl(NULL, " + selFabricante + ", " + sDataInicio + ", " + sDataFim + ") AS rma ON (Itens.PedItemID = rma.PedItemID ) " +
                        "LEFT OUTER JOIN Pedidos_Itens referencia WITH(NOLOCK) ON (referencia.pedidoID = Itens.PedItemReferenciaID) " +
                        "LEFT OUTER JOIN Pedidos ped_referencia WITH(NOLOCK) ON (ped_referencia.PedidoID = referencia.PedidoID) " +
                        "LEFT OUTER JOIN NotasFiscais nf_referencia WITH(NOLOCK) ON (nf_referencia.NotaFiscalID = ped_referencia.NotaFiscalID) " +
                        "INNER JOIN Pedidos WITH(NOLOCK) ON (Itens.PedidoID=Pedidos.PedidoID) " +
                        "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                        "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID=Transacoes.OperacaoID) " +
                        "INNER JOIN Pessoas Vendedores WITH(NOLOCK) ON (Pedidos.ProprietarioID=Vendedores.PessoaID) " +
                        "INNER JOIN Pessoas Equipes WITH(NOLOCK) ON (Pedidos.AlternativoID = Equipes.PessoaID) " +
                        "INNER JOIN Pessoas Parceiros WITH(NOLOCK) ON (Pedidos.ParceiroID=Parceiros.PessoaID) " +
                        "LEFT OUTER JOIN TiposAuxiliares_Itens Classificacoes WITH(NOLOCK) ON (Parceiros.ClassificacaoID=Classificacoes.ItemID) " +
                        "LEFT OUTER JOIN Pessoas_Telefones Faxes WITH(NOLOCK) ON (Parceiros.PessoaID=Faxes.PessoaID AND Faxes.TipoTelefoneID=122 AND Faxes.Ordem=1) " +
                        "LEFT OUTER JOIN Pessoas_URLs URLs WITH(NOLOCK) ON (Parceiros.PessoaID=URLs.PessoaID AND URLs.TipoURLID=124 AND URLs.Ordem=1) " +
                        "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Parceiros.PessoaID=Enderecos.PessoaID) " +
                        "LEFT OUTER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) " +
                        "LEFT OUTER JOIN Localidades Estados WITH(NOLOCK) ON (Enderecos.UFID=Estados.LocalidadeID) " +
                        "LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) " +
                        "INNER JOIN Pessoas Clientes WITH(NOLOCK) ON (Pedidos.PessoaID=Clientes.PessoaID) " +
                        "INNER JOIN Pessoas_Enderecos Cliente_Enderecos WITH(NOLOCK) ON (Clientes.PessoaID=Cliente_Enderecos.PessoaID) " +
                        "LEFT OUTER JOIN Localidades Cliente_Cidades WITH(NOLOCK) ON (Cliente_Enderecos.CidadeID=Cliente_Cidades.LocalidadeID) " +
                        "LEFT OUTER JOIN Localidades Cliente_Estados WITH(NOLOCK) ON (Cliente_Enderecos.UFID=Cliente_Estados.LocalidadeID) " +
                        "LEFT OUTER JOIN Localidades Cliente_Paises WITH(NOLOCK) ON (Cliente_Enderecos.PaisID=Cliente_Paises.LocalidadeID) " +
                        "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID=Produtos.ConceitoID) " +
                        "LEFT OUTER JOIN Conceitos_Caracteristicas Caracteristicas WITH(NOLOCK) ON (Produtos.ConceitoID=Caracteristicas.ProdutoID AND dbo.fn_Produto_CaracteristicaOrdem(Caracteristicas.ProdutoID, Caracteristicas.CaracteristicaID)=1) " +
                        "INNER JOIN Conceitos Familias WITH(NOLOCK) ON (Produtos.ProdutoID=Familias.ConceitoID) " +
                        "INNER JOIN Conceitos Marcas WITH(NOLOCK) ON (Produtos.MarcaID=Marcas.ConceitoID) " +
                        "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.SujeitoID=Pedidos.EmpresaID), " +
                        "Recursos Sistemas WITH(NOLOCK) " +
                        "INNER JOIN Conceitos MoedasSistema WITH(NOLOCK) ON (Sistemas.MoedaID = MoedasSistema.ConceitoID) ";

                if (strSohResultado != "")
                    strSohResultado = "((" + strSohResultado + "(Pedidos.PessoaID <> Pedidos.EmpresaID)) OR (Transacoes.OperacaoID = 985)) AND ";
                else
                    strSohResultado = "Pedidos.PessoaID <> Pedidos.EmpresaID) AND ";


                var strSQL_Where1 = "WHERE ((" + sFiltroEmpresa +
                    "((Enderecos.EndFaturamento=1 AND Enderecos.Ordem=1) AND ";

                var strSQL_Where2 = "(Cliente_Enderecos.EndFaturamento=1 AND Cliente_Enderecos.Ordem=1)) AND ";

                var strSQL_Where3 = "((ProdutosEmpresa.TipoRelacaoID=61) AND " +
                    "(Sistemas.RecursoID = 999) AND " +
                    strSohResultado +
                    "(Pedidos.Suspenso=0 AND ((Pedidos.TipoPedidoID=601 AND Pedidos.EstadoID>=30) OR (Pedidos.TipoPedidoID=602 AND Pedidos.EstadoID>=29) )) AND " +
                    "(Itens.dtMovimento>=" + sDataInicio + " AND Itens.dtMovimento<=" + sDataFim + " " +
                    sMarca + sReportaFabricante + sPedidosWeb + sFiltroFabricante + sFiltroFamilia + sFiltro + ")))) ";

                strSQL = strSQL_Select1 + strSQL_Select2 + strSQL_Select3 + strSQL_From + strSQL_Where1 + strSQL_Where2 +
                    strSQL_Where3 + strSQL_OrderBy;
            }
            // Padrao Seagate
            else if (selPadrao == 709)
            {
                sFiltroEmpresa = "";

                if ((glb_nEmpresaID == 2) || (glb_nEmpresaID == 7))
                    sFiltroEmpresa = " Pedidos.EmpresaID IN (2,7) AND ";
                else
                    sFiltroEmpresa = " Pedidos.EmpresaID IN " + EmpresasIn + " AND ";

                var sSeagate_Select = "SELECT dbo.fn_String_Substituicao(Empresas.Nome, 1) AS [Distributor Name]," +
                    "CONVERT(VARCHAR(4),DATEPART(yyyy, GETDATE())) + " +
                        "dbo.fn_Pad(DATEPART(mm, GETDATE()), 2, " + "'" + "0" + "'" + ", " + "'" + "L" + "'" + ") + " +
                        "dbo.fn_Pad(DATEPART(dd, GETDATE()), 2, " + "'" + "0" + "'" + ", " + "'" + "L" + "'" + ") + " +
                        "dbo.fn_Pad(DATEPART(hh, GETDATE()), 2, " + "'" + "0" + "'" + ", " + "'" + "L" + "'" + ") + " +
                        "dbo.fn_Pad(DATEPART(mi, GETDATE()), 2, " + "'" + "0" + "'" + ", " + "'" + "L" + "'" + ") + " +
                        "dbo.fn_Pad(DATEPART(ss, GETDATE()), 2, " + "'" + "0" + "'" + ", " + "'" + "L" + "'" + ") AS [Report Date/Time], " +
                    "Empresas.PessoaID AS [Distributor Location Identifier], " +
                    "Parceiros.PessoaID AS [Bill-to Customer Number], " +
                    "'" + "0" + "'" + " AS [Bill-to Tax ID/VAT Number]," +
                    "CONVERT(VARCHAR(4),DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento))) + " +
                        "dbo.fn_Pad(DATEPART(mm, dbo.fn_Data_Zero(Itens.dtMovimento)), 2, " + "'" + "0" + "'" + ", " + "'" + "L" + "'" + ") + " +
                        "dbo.fn_Pad(DATEPART(dd, dbo.fn_Data_Zero(Itens.dtMovimento)), 2, " + "'" + "0" + "'" + ", " + "'" + "L" + "'" + ") AS [Distributor Invoice Date]," +
                    "NotasFiscais.NotaFiscal AS [Distributor Invoice Number]," +
                    "dbo.fn_String_Substituicao(Parceiros.Nome, 1) AS [Bill-to Customer Name]," +
                    "dbo.fn_String_Substituicao(Enderecos.Endereco, 1) AS [Bill-to Customer Address1]," +
                    "NULL AS [Bill-to Customer Address2]," +
                    "dbo.fn_String_Substituicao(Cidades.Localidade, 1) AS [Bill-to Customer City]," +
                    "dbo.fn_String_Substituicao(UFs.CodigoLocalidade2, 1) AS [Bill-to Customer State or Province or County]," +
                    "dbo.fn_String_Substituicao(Paises.CodigoLocalidade2, 1) AS [Bill-to Customer Country Code]," +
                    "LEFT(Enderecos.CEP, 5) AS [Bill-to Customer Zip Postal Code]," +
                    "dbo.fn_String_Substituicao(Parceiros.Nome, 1) AS [Ship-to Customer Name]," +
                    "dbo.fn_String_Substituicao(Enderecos.Endereco, 1) AS [Ship-to Customer Address1]," +
                    "NULL AS [Ship-to Customer Address2]," +
                    "dbo.fn_String_Substituicao(Cidades.Localidade, 1) AS [Ship-to Customer City]," +
                    "dbo.fn_String_Substituicao(UFs.CodigoLocalidade2, 1) AS [Ship-to Customer State or Province or County]," +
                    "dbo.fn_String_Substituicao(Paises.CodigoLocalidade2, 1) AS [Ship-to Customer Country Code]," +
                    "LEFT(Enderecos.CEP, 5) AS [Ship-to Customer Zip Postal Code]," +
                    "dbo.fn_String_Substituicao(Produtos.Modelo, 1) AS [Seagate Model Number]," +
                    "CONVERT(INT, CASE Pedidos.TipoPedidoID WHEN 602 THEN Itens.Quantidade ELSE Itens.Quantidade * (-1) END) AS [Quantity]," +
                    "CONVERT(VARCHAR(13), CONVERT(NUMERIC(11,2), dbo.fn_PedidoItem_Totais(Itens.PedItemID, 7))) AS [Resale Price]," +
                    "dbo.fn_String_Substituicao(Moedas.ConceitoAbreviado, 1) AS [Resale Price Currency] ";

                var sSeagate_From = "FROM Pedidos_Itens Itens WITH(NOLOCK) " +
                    "INNER JOIN Pedidos WITH(NOLOCK) ON (Itens.PedidoID=Pedidos.PedidoID) " +
                    "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                    "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID=Transacoes.OperacaoID) " +
                    "INNER JOIN Pessoas Empresas WITH(NOLOCK) ON (Pedidos.EmpresaID=Empresas.PessoaID) " +
                    "INNER JOIN Pessoas Parceiros WITH(NOLOCK) ON (Pedidos.ParceiroID=Parceiros.PessoaID) " +
                    "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pedidos.ParceiroID=Enderecos.PessoaID) " +
                    "LEFT OUTER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades UFs WITH(NOLOCK) ON (Enderecos.UFID=UFs.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) " +
                    "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID=Produtos.ConceitoID) " +
                    "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.SujeitoID=Pedidos.EmpresaID) " +
                    "INNER JOIN Conceitos Moedas WITH(NOLOCK) ON (Pedidos.MoedaID = Moedas.ConceitoID) ";

                var sSeagate_Where1 = "WHERE " +
                    "((" + sFiltroEmpresa + " " +
                    "dbo.fn_Empresa_Sistema(Pedidos.PessoaID) = 0 AND Pedidos.PessoaID NOT IN (14121,17441) AND " +
                    "Pedidos.PessoaID <> Pedidos.EmpresaID) AND " +
                    "((Enderecos.EndFaturamento=1 AND Enderecos.Ordem=1)) AND ";

                var sSeagate_Where2 = "((ProdutosEmpresa.TipoRelacaoID=61 AND " +
                    "(" + strSohResultado +
                    "(Pedidos.Suspenso=0 AND ((Pedidos.TipoPedidoID=601 AND Pedidos.EstadoID>=30) OR (Pedidos.TipoPedidoID=602 AND Pedidos.EstadoID>=29) )) AND " +
                    "(Itens.dtMovimento>=" + sDataInicio + " AND Itens.dtMovimento<=" + sDataFim + " " +
                    sMarca + sReportaFabricante + sPedidosWeb + sFiltroFabricante + sFiltroFamilia + sFiltro + " ))))) ";

                var sSeagate_OrderBy = "ORDER BY [Distributor Location Identifier], " +
                    "Itens.dtMovimento, Itens.PedidoID";

                strSQL = sSeagate_Select + sSeagate_From + sSeagate_Where1 + sSeagate_Where2 + sSeagate_OrderBy;
            }
            // Padrão Toshiba
            else if (selPadrao == 717)
            {
                var strSQL_Toshiba = "";
                var strSQL_FromToshiba = "";
                var strSQL_WhereToshiba = "";
                var strSQL_FiltrosToshiba = "";

                strSQL_Toshiba = "SELECT ISNULL(Campanhas.CampanhaID,SPACE(0)) [CampanhaID], Itens.ProdutoID [ProdutoID], Empresa.Fantasia [Distributor Name], 'T0B08U0001' [Distributor Number], CONVERT (VARCHAR(10),GETDATE(),101) [Report Date], " +
                                        "SPACE(0) [Toshiba Item Number], Produtos.Modelo [Distributor Item Number], Produtos.PartNumber [Local Generic Item], notasfiscais.notafiscal [Resales Invoice], " +
                                        "CONVERT(INT, CASE pedidos.tipopedidoid WHEN 602 THEN Itens.quantidade ELSE Itens.quantidade * ( -1 ) END) [Resales Quantity], " +
                                        "dbo.fn_Numero_Formata(Itens.valorunitario,2,0,101) [Resales Price], dbo.fn_Numero_Formata((Kardex.ValorCustoTotal/Kardex.Quantidade),2,0,103) [Unit Cost], " +
                                        "ISNULL(dbo.fn_Numero_Formata(((Kardex.ValorCustoTotal/Kardex.Quantidade) - CampanhaProdutos.ValorUnitario),2,0,103),SPACE(0)) [Aproved Spa], " +
                                        "ISNULL(dbo.fn_Numero_Formata(CampanhaProdutos.ValorUnitario,2,0,103), SPACE(0)) [Credit per unit], " +
                                        "ISNULL(dbo.fn_Numero_Formata((CampanhaProdutos.ValorUnitario*CampanhaProdutos.Quantidade),2,0,103), SPACE(0)) [Total credit], " +
                                        "CONVERT (VARCHAR(10),NotasFiscais.dtNotaFiscal,101) [Transaction Date], ISNULL(Campanhas.Codigo, SPACE(0)) [DPA MC Ref], " +
                                        "Clientes.PessoaID [End Customer Number], Clientes.Fantasia [End Customer Name], SPACE(0) [End Customer Address1], SPACE(0) [End Customer Address2], SPACE(0) [End Customer Address3], SPACE(0) [End Customer Address4], " +
                                        "Cliente_Cidades.Localidade [End Customer City], ISNULL(Cliente_Enderecos.Endereco, SPACE(0)) [End Customer State], Cliente_Enderecos.CEP [End Customer Postal Code], Cliente_Paises.Localidade [Country Code] ";
                strSQL_FromToshiba = "FROM Pedidos_Itens Itens WITH(NOLOCK) " +
                                        "INNER JOIN Pedidos WITH(NOLOCK) ON (Itens.PedidoID = Pedidos.PedidoID) " +
                                        "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID) " +
                                        "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID = Transacoes.OperacaoID) " +
                                        "INNER JOIN Pessoas Empresa WITH(NOLOCK) ON (Pedidos.EmpresaID = Empresa.PessoaID) " +
                                        "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pedidos.ParceiroID = Enderecos.PessoaID) " +
                                        "LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID = Paises.LocalidadeID) " +
                                        "INNER JOIN Pessoas Clientes WITH(NOLOCK) ON (Pedidos.pessoaid = Clientes.PessoaID) " +
                                        "INNER JOIN Pessoas_Enderecos Cliente_Enderecos WITH(NOLOCK) ON (Clientes.pessoaid = Cliente_Enderecos.pessoaid) " +
                                        "LEFT OUTER JOIN Localidades Cliente_Cidades WITH(NOLOCK) ON (Cliente_Enderecos.CidadeID = Cliente_Cidades.LocalidadeID) " +
                                        "LEFT OUTER JOIN Localidades Cliente_Paises WITH(NOLOCK) ON  (Cliente_Enderecos.PaisID = Cliente_Paises.LocalidadeID) " +
                                        "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID = Produtos.ConceitoID) " +
                                        "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON ((Itens.ProdutoID = ProdutosEmpresa.ObjetoID) AND (ProdutosEmpresa.SujeitoID = Pedidos.EmpresaID)) " +
                                        "LEFT OUTER JOIN Pedidos_Itens_Campanhas ItensCampanhas WITH(NOLOCK) ON (ItensCampanhas.PedItemID = Itens.PedItemID) " +
                                        "LEFT OUTER JOIN Campanhas_Produtos CampanhaProdutos WITH(NOLOCK) ON (CampanhaProdutos.CamProdutoID = ItensCampanhas.CamProdutoID) " +
                                        "LEFT OUTER JOIN Campanhas WITH(NOLOCK) ON (Campanhas.CampanhaID = CampanhaProdutos.CampanhaID) " +
                                        "INNER JOIN Pedidos_Itens_Kardex Kardex WITH(NOLOCK) ON (Kardex.PedItemID = Itens.PedItemID) ";
                strSQL_WhereToshiba = "WHERE (Pedidos.EmpresaID IN " + EmpresasIn + " AND Enderecos.endfaturamento = 1 AND Enderecos.ordem = 1 AND Cliente_Enderecos.endfaturamento = 1 AND Cliente_Enderecos.ordem = 1 " +
                                            "AND ProdutosEmpresa.TipoRelacaoID = 61 AND Transacoes.Resultado = 1 AND Pedidos.Suspenso = 0 AND Produtos.MarcaID IN (3212,3372) " +
                                            "AND ((Pedidos.TipoPedidoID = 601 AND Pedidos.EstadoID >= 30) OR (Pedidos.TipoPedidoID = 602 AND Pedidos.EstadoID >= 29)) " +
                                            "AND Itens.dtMovimento BETWEEN " + sDataInicio + " AND " + sDataFim + " AND NotasFiscais.EstadoID = 67 AND Kardex.TipoCustoID = 375) ";
                strSQL_FiltrosToshiba = sFiltroFabricante + sPedidosWeb + sFiltroFamilia + sFiltro + " ";

                strSQL = strSQL_Toshiba + strSQL_FromToshiba + strSQL_WhereToshiba + strSQL_FiltrosToshiba;
            }
            // Padrao Gigabyte
            else if (selPadrao == 950)
            {
                var sGigabyte_Select = "SELECT	n.Numero AS [CNPJ Distribuidor], " +
                    "i.Numero AS [CNPJ Revenda], " +
                    "g.Nome AS [RazaoSocial], " +
                    "h.Endereco + SPACE(1) + ISNULL(h.Numero, SPACE(0)) + SPACE(1) + ISNULL(h.Complemento, SPACE(0)) AS [Endereco/Complemento], " +
                    "h.Bairro AS [Bairro], " +
                    "h.CEP AS [CEP], " +
                    "j.Localidade AS [Cidade], " +
                    "k.CodigoLocalidade2 AS [UF],  " +
                    "ISNULL(q.Fantasia, SPACE(0)) AS [Contato], " +
                    "ISNULL(r.URL, SPACE(0)) AS [E-mail], " +
                    "CONVERT(VARCHAR(10), b.dtMovimento, 103) AS [Data Emissao NF], " +
                    "d.NotaFiscal AS [Numero NF], " +
                    "bb.Codigo AS [CFOP], " +
                    "Produtos.Modelo AS [Modelo], " +
                    "b.Quantidade * POWER(-1, Pedidos.TipoPedidoID) AS [Quantidade], " +
                    "b.ValorUnitario AS [Valor Unitario], " +
                    "b.ValorUnitario * b.Quantidade * POWER(-1, Pedidos.TipoPedidoID) AS [Valor Total], " +
                    "oo.Nome AS Vendedor, " +
                    "dbo.fn_Pessoa_Documento(oo.PessoaID, NULL, NULL, 101, 101, 0) AS [CPF Vendedor], " +
                    "dbo.fn_Pessoa_URL(oo.PessoaID, 124, NULL) AS [E-mail ], " +
                    "marca.Conceito [Marca] ";

                var sGigabyte_From = "FROM Pedidos Pedidos WITH(NOLOCK) " +
                    "INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (Pedidos.PedidoID = b.PedidoID) " +
                    "INNER JOIN Operacoes bb WITH(NOLOCK) ON (b.CFOPID = bb.OperacaoID) " +
                    "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (b.ProdutoID = Produtos.ConceitoID) " +
                    "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (ProdutosEmpresa.ObjetoID = b.ProdutoID) AND (ProdutosEmpresa.SujeitoID = Pedidos.EmpresaID) " +
                    "INNER JOIN NotasFiscais d WITH(NOLOCK) ON (Pedidos.NotaFiscalID = d.NotaFiscalID AND d.EstadoID=67) " +
                    "INNER JOIN Operacoes e WITH(NOLOCK) ON (Pedidos.TransacaoID = e.OperacaoID) " +
                    "INNER JOIN Pessoas g WITH(NOLOCK) ON (Pedidos.ParceiroID = g.PessoaID) " +
                    "INNER JOIN Pessoas_Enderecos h WITH(NOLOCK) ON (g.PessoaID = h.PessoaID) " +
                    "INNER JOIN Pessoas_Documentos i WITH(NOLOCK) ON (g.PessoaID = i.PessoaID) " +
                    "INNER JOIN Localidades j WITH(NOLOCK) ON (h.CidadeID = j.LocalidadeID) " +
                    "INNER JOIN Localidades k WITH(NOLOCK) ON (h.UFID = k.LocalidadeID) " +
                    "INNER JOIN Pessoas m WITH(NOLOCK) ON (Pedidos.EmpresaID = m.PessoaID) " +
                    "INNER JOIN Pessoas_Documentos n WITH(NOLOCK) ON (m.PessoaID = n.PessoaID) " +
                    "INNER JOIN RelacoesPessoas o WITH(NOLOCK) ON (o.SujeitoID = g.PessoaID) AND (o.ObjetoID = Pedidos.EmpresaID) AND (o.TipoRelacaoID = 21) AND o.EstadoID <> 5 " +
                    "INNER JOIN Pessoas oo WITH(NOLOCK) ON (Pedidos.ProprietarioID=oo.PessoaID) " +
                    "LEFT OUTER JOIN RelacoesPessoas_Contatos p WITH(NOLOCK) ON (o.RelacaoID = p.RelacaoID) AND (p.Ordem = 1) " +
                    "LEFT OUTER JOIN Pessoas q WITH(NOLOCK) ON (p.ContatoID = q.PessoaID) " +
                    "LEFT OUTER JOIN Pessoas_URLs r WITH(NOLOCK) ON (r.PessoaID = q.PessoaID) AND (r.TipoURLID = 124) AND (r.Ordem = 1) " +
                    "LEFT JOIN Conceitos marca WITH(NOLOCK) ON (marca.ConceitoID = produtos.MarcaID) ";

                var sGigabyte_Where = "WHERE (Pedidos.EmpresaID IN " + EmpresasIn + ") AND " +
                    "(ProdutosEmpresa.TipoRelacaoID = 61) AND " +
                    "(Pedidos.EstadoID >= 29) AND " +
                    "(Pedidos.TipoPedidoID = 602) AND " +
                    "(d.dtNotaFiscal BETWEEN " + sDataInicio + " AND " + sDataFim + ") AND " +
                    "(e.Resultado = 1) AND " +
                    "(h.Ordem = 1) AND " +
                    "(i.TipoDocumentoID = 111) AND " +
                    "(n.TipoDocumentoID = 111) " + sMarca + sReportaFabricante + sPedidosWeb + sFiltroFabricante + sFiltroFamilia + sFiltro;

                var sGigabyte_OrderBy = "ORDER BY Pedidos.PedidoID, Produtos.ConceitoID";

                strSQL = sGigabyte_Select + sGigabyte_From + sGigabyte_Where + sGigabyte_OrderBy;
            }

            // Padrao 3Com
            else if (selPadrao == 951)
            {
                var s3Com_variaveis = "DECLARE @Nome VARCHAR(35), @Ecam VARCHAR(8), @UF_Abreviado VARCHAR(5), @Cep VARCHAR(12), " +
                    "@Pais VARCHAR(5), @Contato VARCHAR(35), @Telefone VARCHAR(20), @UF VARCHAR(30), @Sql VARCHAR(8000), @dtFim DATETIME " +
                "SET NOCOUNT ON CREATE TABLE #table3Com (PedidoEmpresaID INT, PedidoID INT, ProdutoID INT, PessoaID INT, YourCompanyName VARCHAR(35), YourCompanyECAMBillTo VARCHAR(20), " +
                    "YourCompanyBillToName VARCHAR(35), StateCompany VARCHAR(30), YourCompanyBilltoZip VARCHAR(12), YourCompanyBilltoCountryCode VARCHAR(5), " +
                    "ContactNameCompany VARCHAR(35), ContactPhone VARCHAR(22), YourCompanyECAMShipTo VARCHAR(35), YourCompanyShipToName VARCHAR(50), " +
                    "StatecompanyShip VARCHAR(35), YourCompanyShipToZip VARCHAR(35), YourCompanyShipToCountryCode VARCHAR(35), ResellerNumber VARCHAR(35), " +
                    "ResellerBillto VARCHAR(35), ResellerBilltoName VARCHAR(50), ResellerBilltoAddress1 VARCHAR(35), ResellerBilltoAddress2 VARCHAR(35), " +
                    "City VARCHAR(35), StateParceiro VARCHAR(35), ResellerBilltoZip VARCHAR(35), ResellerBilltoCountryCode VARCHAR(35), ContactNameParceiro VARCHAR(35), " +
                    "ContactPhoneParceiro VARCHAR(35), ShiptoNumber VARCHAR(35), ResellerShiptoName VARCHAR(50), StatePessoa VARCHAR(35), " +
                    "ResellerShiptoZip VARCHAR(35), ResellerShiptoCountryCode VARCHAR(35), CountryCode VARCHAR(35), ProductNumber VARCHAR(35), UnitPrice VARCHAR(35), " +
                    "CurrencyCode VARCHAR(35), ShipDate VARCHAR(35), InvoiceDate VARCHAR(35), ReportEndDate VARCHAR(35), QtySold INT, QtyReturned INT, " +
                    "InvoiceCreditNoteNumber VARCHAR(35), EffectiveInventoryDate VARCHAR(35), QtyonHand NUMERIC(11,2), QtyonOrder NUMERIC(11,2), QtyStockTransfer NUMERIC(11,2)) ";

                var s3Com_EmpresaRelatorio = "SELECT @Nome = a.Nome, @Ecam = 63253858, @UF_Abreviado = c.CodigoLocalidade2, @Cep = b.CEP, @Pais = d.CodigoLocalidade2, @Contato = f.Fantasia, @Telefone = dbo.fn_Pessoa_Telefone(f.PessoaID, 119, 120, 1, 0, NULL),@UF = c.Localidade " +
                            "FROM	Pessoas a WITH(NOLOCK)  " +
                                "INNER JOIN Pessoas_Enderecos b WITH(NOLOCK) ON a.PessoaID = b.PessoaID " +
                                "INNER JOIN Localidades c WITH(NOLOCK) ON b.UFID = c.LocalidadeID " +
                                "INNER JOIN Localidades d WITH(NOLOCK) ON b.PaisID = d.LocalidadeID " +
                                "INNER JOIN RelacoesPessoas e WITH(NOLOCK) ON a.PessoaID = e.SujeitoID AND e.EstadoID <> 5 " +
                                "INNER JOIN Pessoas f WITH(NOLOCK) ON e.ProprietarioID = f.PessoaID " +
                            "WHERE	a.PessoaID = 7 AND b.EndFaturamento = 1 AND b.Ordem = 1 AND e.TipoRelacaoID = 21 AND e.ObjetoID = 15395 /*3Com*/ ";

                var s3Com_Pedidos = " SET @dtFim = " + sDataFim + " " +
                            "INSERT INTO #table3Com (PedidoEmpresaID, PedidoID, ProdutoID, PessoaID, ResellerNumber, ResellerBillto,	ProductNumber, UnitPrice, " +
                                            "CurrencyCode, ShipDate,	InvoiceDate, ReportEndDate, QtySold, QtyReturned, InvoiceCreditNoteNumber) " +
                            "SELECT  Pedidos.EmpresaID, Pedidos.PedidoID, Produtos.ConceitoID, Pedidos.PessoaID, " +
                                    "dbo.fn_Pessoa_Documento(Pedidos.ParceiroID, NULL, NULL, 111, 111, 0) AS [Reseller Number], " +
                                    "Pedidos.ParceiroID AS [Reseller Bill to], Produtos.PartNumber, dbo.fn_DivideZero(Itens.ValorUnitario, Pedidos.TaxaMoeda) , " +
                                    "(SELECT TOP 1 SimboloMoeda FROM Conceitos WITH(NOLOCK) WHERE ConceitoID = 541), " +
                                    "CONVERT(VARCHAR(8), (CASE WHEN Pedidos.TipoPedidoID = 601 THEN (dbo.fn_Pedido_Datas(Pedidos.PedidoID, 8)) ELSE NotasFiscais.dtNotafiscal END),112),	CONVERT(VARCHAR(8), NotasFiscais.dtNotafiscal,112), " +
                                    "CONVERT(VARCHAR(8), @dtFim,112), (CASE WHEN Pedidos.TipoPedidoID = 602 THEN Itens.Quantidade ELSE 0 END), " +
                                    "(CASE WHEN Pedidos.TipoPedidoID = 601 THEN Itens.Quantidade ELSE 0 END), NotasFiscais.NotaFiscal " +
                            "FROM	Pedidos WITH(NOLOCK) " +
                                    "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON Pedidos.TransacaoID=Transacoes.OperacaoID  " +
                                    "INNER JOIN NotasFiscais WITH(NOLOCK) ON Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67 " +
                                    "INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON Pedidos.PedidoID = Itens.PedidoID " +
                                    "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON Itens.ProdutoID=Produtos.ConceitoID " +
                                    "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON ProdutosEmpresa.ObjetoID = Itens.ProdutoID AND ProdutosEmpresa.SujeitoID = Pedidos.EmpresaID " +
                            "WHERE 	dbo.fn_Pessoa_Empresa(NULL, Pedidos.PessoaID, NULL, NULL) = 0 " +
                                    "AND Transacoes.Resultado = 1  " +
                                    "AND ProdutosEmpresa.TipoRelacaoID = 61 " +
                                    "AND Pedidos.EmpresaID IN " + EmpresasIn + " " +
                                    "AND Pedidos.Suspenso=0 " +
                                    "AND Pedidos.EstadoID >= 29 " +
                                    "AND ( (Pedidos.TipoPedidoID = 602 AND NotasFiscais.dtNotaFiscal Between " + sDataInicio + " AND " + sDataFim + ") " +
                                        "OR (Pedidos.TipoPedidoID = 601 AND (dbo.fn_Pedido_Datas(Pedidos.PedidoID, 8) Between " + sDataInicio + " AND " + sDataFim + "))) " +

                                    "AND Produtos.FabricanteID = 15395 " + sMarca + sReportaFabricante + sPedidosWeb + sFiltroFamilia + sFiltro;

                var s3Com_update1 = " UPDATE #Table3Com SET YourCompanyName = @Nome, YourCompanyECAMBillTo = @Ecam, YourCompanyBillToName = @Nome, StateCompany = @UF_Abreviado, " +
                                        "YourCompanyBilltoZip = @Cep, YourCompanyBilltoCountryCode = @Pais, ContactNameCompany = @Contato, ContactPhone = @Telefone, " +
                                        "YourCompanyECAMShipTo = @Ecam, YourCompanyShipToName = @Nome, StatecompanyShip = @UF, YourCompanyShipToZip = @CEP, " +
                                        "YourCompanyShipToCountryCode = @Pais ";

                //Insere nome e endereco do parceiro do pedido.
                var s3Com_update2 = " UPDATE #table3Com SET ResellerBilltoAddress1 = b.Endereco,ResellerBilltoAddress2 = b.Complemento, City = c.Localidade, StateParceiro = d.Localidade, " +
                                    "ResellerBilltoZip = b.CEP, ResellerBillToCountryCode = e.CodigoLocalidade2, ResellerBilltoName = Parceiros.Nome " +
                                "FROM #table3Com a " +
                                    "INNER JOIN Pessoas_Enderecos b WITH(NOLOCK) ON a.ResellerBillto = b.PessoaID " +
                                    "INNER JOIN Localidades c WITH(NOLOCK) ON b.Cidadeid = c.LocalidadeID " +
                                    "INNER JOIN Localidades d WITH(NOLOCK) ON b.UFID = d.LocalidadeID " +
                                    "INNER JOIN Localidades e WITH(NOLOCK) ON b.PaisID = e.LocalidadeID " +
                                    "INNER JOIN Pessoas Parceiros WITH(NOLOCK) ON a.ResellerBillto = Parceiros.PessoaID " +
                                "WHERE b.PesEnderecoID = (SELECT TOP 1 aa.PesEnderecoID " +
                                                                "FROM Pessoas_Enderecos aa WITH(NOLOCK) " +
                                                                "WHERE aa.PessoaID = a.ResellerBillto AND aa.EndFaturamento = 1 " +
                                                                "ORDER BY aa.Ordem) ";
                //Insere contato e phone do contato do parceiro.			
                var s3Com_update3 = " UPDATE #table3Com SET ContactnameParceiro = d.Fantasia, ContactPhoneParceiro = dbo.fn_Pessoa_Telefone(d.PessoaID, 119, 120, 1 ,0, NULL) " +
                                "FROM #table3Com a " +
                                    "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON a.ResellerBillto = b.SujeitoID AND b.EstadoID <> 5 " +
                                    "INNER JOIN RelacoesPessoas_Contatos c WITH(NOLOCK) ON b.RelacaoID = c.RelacaoID " +
                                    "INNER JOIN Pessoas d WITH(NOLOCK) ON c.ContatoID = d.PessoaID " +
                                "WHERE b.TipoRelacaoID = 21 AND b.ObjetoID = a.PedidoEmpresaID AND c.Ordem = 1 ";

                //Insere nome e endereco da pessoa do pedido.
                var s3Com_update4 = " UPDATE #table3Com SET ShiptoNumber = b.numero, ResellerShiptoName = f.Nome, StatePessoa = d.Localidade, " +
                                    "ResellerShiptoZip = b.CEP, ResellerShipToCountryCode = e.CodigoLocalidade2, CountryCode = e.CodigoLocalidade2 " +
                                "FROM #table3Com a " +
                                    "INNER JOIN Pessoas_Enderecos b WITH(NOLOCK) ON a.PessoaID = b.PessoaID " +
                                    "INNER JOIN Localidades d WITH(NOLOCK) ON b.UFID = d.LocalidadeID " +
                                    "INNER JOIN Localidades e WITH(NOLOCK) ON b.PaisID = e.LocalidadeID " +
                                    "INNER JOIN Pessoas f WITH(NOLOCK) ON b.PessoaID = f.PessoaID " +
                                    "WHERE b.PesEnderecoID = (SELECT TOP 1 aa.PesEnderecoID " +
                                                                        "FROM Pessoas_Enderecos aa WITH(NOLOCK) " +
                                                                        "WHERE aa.PessoaID = a.PessoaID AND aa.EndFaturamento = 1 " +
                                                                        "ORDER BY aa.Ordem) ";

                var s3com_selTable = "SELECT YourCompanyName AS [Your Company Name], " + "'" + "'" + " AS [Number], YourCompanyECAMBillTo AS [Your Company ECAM Bill To #], " +
                            "YourCompanyBillToName AS [Your Company Bill To Name], " + "'" + "'" + " AS [Address 1], " + "'" + "'" + " AS [Address 2], " + "'" + "'" + " AS [City], " +
                            "StateCompany AS [State], YourCompanyBilltoZip AS [Your Company Bill to Zip], " + "'" + "'" + " AS [Country], YourCompanyBilltoCountryCode AS [Your Company Bill to Country Code], " +
                            "ContactNameCompany AS [Contact Name], ContactPhone AS [Contact Phone], YourCompanyECAMShipTo AS [Your Company ECAM Ship To #], " +
                            "YourCompanyShipToName AS [Your Company Ship To Name], " + "'" + "'" + " AS [Ship to Address 1], " + "'" + "'" + " AS [Ship To Address 2], " + "'" + "'" + " AS [City], " +
                            "StatecompanyShip AS [State], YourCompanyShipToZip AS [Your Company Ship To Zip], " + "'" + "'" + " AS [Country Name], " +
                            "YourCompanyShipToCountryCode AS [Your Company Ship To Country Code], " + "'" + "'" + " AS [Contact Name], " + "'" + "'" + " AS [Contact Phone], " +
                            "ResellerNumber AS [Reseller Number], ResellerBillto AS [Reseller Bill to #], ResellerBilltoName AS [Reseller Bill to Name], " +
                            "ResellerBilltoAddress1 AS [Reseller Bill to Address 1], ResellerBilltoAddress2 AS [Reseller Bill to Address 2], City AS [City], " +
                            "StateParceiro AS [State], ResellerBilltoZip AS [Reseller Bill to Zip], ResellerBilltoCountryCode AS [Reseller Bill to Country Code], " +
                            "'" + "'" + " AS [Country], ContactNameParceiro AS [Contact Name], ContactPhoneParceiro AS [Contact Phone], ShiptoNumber AS [Ship to Number], " +
                            "ResellerShiptoName AS [Reseller Ship to Name], " + "'" + "'" + " AS [Ship to Address 1], " + "'" + "'" + " AS [Ship to Address 2], " + "'" + "'" + " AS [City], StatePessoa AS [State], " +
                            "ResellerShiptoZip AS [Reseller Ship to Zip], ResellerShiptoCountryCode AS [Reseller Ship to Country Code], " +
                            "'" + "'" + " AS [Country], " + "'" + "'" + " AS [Contact Name], " + "'" + "'" + " AS [Contact Phone], " + "'" + "'" + " AS [Sales Region], " + "'" + "'" + " AS [Sales Territory], " + "'" + "'" + " AS [End-User Bill to #], 	" + "'" + "'" + " AS [End-User Bill To Name], " +
                            "'" + "'" + " AS [Bill to Address 1], " + "'" + "'" + " AS [Bill to Address 2], " + "'" + "'" + " AS [City], " + "'" + "'" + " AS [State], " + "'" + "'" + " AS [Zip], CountryCode AS [Country Code], " +
                            "'" + "'" + " AS [Country], " + "'" + "'" + " AS [End-User Ship To #], " + "'" + "'" + " AS [Ship to Name], " + "'" + "'" + " AS [Ship to Address 1], " + "'" + "'" + " AS [Ship to Address 2], " +
                            "'" + "'" + " AS [City], " + "'" + "'" + " AS [State], " + "'" + "'" + " AS [Zip], " + "'" + "'" + " AS [Country Code], " + "'" + "'" + " AS [Country], " + "'" + "'" + " AS [Contact Name], " + "'" + "'" + " AS [Contact Phone], " +
                            "'" + "'" + " AS [SPQ/Promotion ID], " + "'" + "'" + " AS [Bundle ID], ProductNumber AS [3COM Product Number], " + "'" + "'" + " AS [Customer Product Number], " +
                            "(dbo.fn_Numero_Formata(UnitPrice, 2, 1, 101)) AS [Unit Price], CurrencyCode AS [Currency Code], " + "'" + "'" + " AS [UOM      1 = EA], " + "'" + "'" + " AS [Effective Inventory Date], " +
                            "'" + "'" + " AS [Qty on Hand], " + "'" + "'" + " AS [Qty on Order], " + "'" + "'" + " AS [Qty Committed], " + "'" + "'" + " AS [Qty in Float], " + "'" + "'" + " AS [Qty on Back Order], " +
                            "'" + "'" + " AS [Qty Stock Transfer], ShipDate AS [Ship Date], InvoiceDate AS [Invoice Date], ReportEndDate AS [Report End Date], " +
                            "QtySold AS [Qty Sold ], QtyReturned AS [Qty Returned], " + "'" + "'" + " AS [Qty Shipped], InvoiceCreditNoteNumber AS [Invoice/Credit Note Number] " +
                        "FROM #table3Com ORDER BY InvoiceDate " +
                    "DROP TABLE #table3Com SET NOCOUNT OFF ";

                strSQL = s3Com_variaveis + s3Com_EmpresaRelatorio + s3Com_Pedidos + s3Com_update1 + s3Com_update2 + s3Com_update3 + s3Com_update4 + s3com_selTable;
            }

            // Padrao NVidia
            else if (selPadrao == 952)
            {

                var sNVidia_Insert_1 = "";
                var sNVidia_Insert_2 = "";
                var sNVidia_variaveis = "";
                var sNVida_SelectTable = "";

                sNVidia_variaveis = "DECLARE @Separador VARCHAR(1) " +
                    "SET NOCOUNT ON CREATE TABLE #tempNVidia  (EmpresaID INT, ReporterID VARCHAR(10), Fabricante VARCHAR(20), Modelo VARCHAR(18), Descricao VARCHAR(256), Quant INT, " +
                        "DataNotaFiscal DATETIME, NotaFiscal INT, Ordem INT, ValorUnitario NUMERIC(11,2), MoedaID INT, ClienteID INT, Nome VARCHAR(20), Endereco VARCHAR(35), " +
                        "Numero VARCHAR(6), Complemento VARCHAR(20), Cidade VARCHAR(30), Estado VARCHAR(30), CEP VARCHAR(9), Pais VARCHAR(30), Vendedor VARCHAR(20)) " +
                        "SET @Separador = CHAR(44) ";

                if (glb_nEmpresaID != 7)
                {
                    sNVidia_Insert_1 = "INSERT INTO #tempNVidia " +
                                "SELECT  Pedidos.EmpresaID, '1242392', Fabricante.Fantasia, Produtos.Modelo, Produtos.Conceito + SPACE(1) + Produtos.Descricao, " +
                                "CASE WHEN Pedidos.TipoPedidoID = 602 THEN Itens.Quantidade ELSE -Itens.Quantidade END, " +
                                "Itens.dtMovimento, NotasFiscais.NotaFiscal, Itens.Ordem, Itens.ValorUnitario, Itens.MoedaID, Pessoas.PessoaID, Pessoas.Fantasia, " +
                                "Enderecos.Endereco, Enderecos.Numero, Enderecos.Complemento, Cidades.Localidade, UFs.CodigoLocalidade2, Enderecos.CEP, " +
                                "Pais.CodigoLocalidade2, Vendedor.Fantasia " +
                            "FROM Pedidos Pedidos WITH(NOLOCK) " +
                                    "INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (Pedidos.PedidoID = Itens.PedidoID) " +
                                    "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID = Produtos.ConceitoID) " +
                                    "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID = ProdutosEmpresa.ObjetoID) AND (ProdutosEmpresa.SujeitoID = Pedidos.EmpresaID) " +
                                    "INNER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                                    "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID = Transacoes.OperacaoID) " +
                                    "INNER JOIN Pessoas Vendedor WITH(NOLOCK) ON (Pedidos.ProprietarioID = Vendedor.PessoaID) " +
                                    "INNER JOIN Pessoas Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID = Pessoas.PessoaID) " +
                                    "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pessoas.PessoaID = Enderecos.PessoaID) " +
                                    "INNER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID = Cidades.LocalidadeID) " +
                                    "INNER JOIN Localidades UFs WITH(NOLOCK) ON (Enderecos.UFID = UFs.LocalidadeID) " +
                                    "INNER JOIN Localidades Pais WITH(NOLOCK) ON (Enderecos.PaisID = Pais.LocalidadeID) " +
                                    "INNER JOIN Pessoas Fabricante WITH(NOLOCK) ON (Produtos.FabricanteID = Fabricante.PessoaID) " +
                            "WHERE	(Pedidos.EmpresaID IN " + EmpresasIn + " ) AND " +
                                    "(ProdutosEmpresa.TipoRelacaoID = 61 ) AND " +
                                    "(Pedidos.EstadoID >= 29) AND " +
                                    "(Itens.dtMovimento BETWEEN " + sDataInicio + " AND " + sDataFim + ") AND " +
                                    "(Transacoes.Resultado = 1) AND " +
                                    "(Enderecos.Ordem = 1) " + sMarca + sReportaFabricante + sPedidosWeb + sFiltro + sFiltroFamilia + " " +
                        "UNION ALL  " +
                        " /*Vendas Allplus (FOB Brasil)*/ " +
                        "SELECT  Pedidos.EmpresaID, '1242392', Fabricante.Fantasia, Produtos.Modelo, Produtos.Conceito + SPACE(1) + Produtos.Descricao, " +
                                "CASE WHEN Pedidos.TipoPedidoID = 602 THEN Itens.Quantidade ELSE -Itens.Quantidade END, " +
                                "Itens.dtMovimento, NotasFiscais.NotaFiscal, Itens.Ordem, Itens.ValorUnitario, Itens.MoedaID, Pessoas.PessoaID, Pessoas.Fantasia, Enderecos.Endereco, Enderecos.Numero, Enderecos.Complemento, " +
                                "Cidades.Localidade, UFs.CodigoLocalidade2, Enderecos.CEP, Pais.CodigoLocalidade2, Vendedor.Fantasia " +
                            "FROM Pedidos Pedidos WITH(NOLOCK) " +
                                    "INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (Pedidos.PedidoID = Itens.PedidoID) " +
                                    "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID = Produtos.ConceitoID) " +
                                    "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID = ProdutosEmpresa.ObjetoID) AND (ProdutosEmpresa.SujeitoID = Pedidos.EmpresaID) " +
                                    "INNER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                                    "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID = Transacoes.OperacaoID) " +
                                    "INNER JOIN Pessoas Vendedor WITH(NOLOCK) ON (Pedidos.ProprietarioID = Vendedor.PessoaID) " +
                                    "INNER JOIN Pessoas Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID = Pessoas.PessoaID) " +
                                    "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pessoas.PessoaID = Enderecos.PessoaID) " +
                                    "INNER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID = Cidades.LocalidadeID) " +
                                    "INNER JOIN Localidades UFs WITH(NOLOCK) ON (Enderecos.UFID = UFs.LocalidadeID) " +
                                    "INNER JOIN Localidades Pais WITH(NOLOCK) ON (Enderecos.PaisID = Pais.LocalidadeID) " +
                                    "INNER JOIN Pessoas Fabricante WITH(NOLOCK) ON (Produtos.FabricanteID = Fabricante.PessoaID) " +
                            "WHERE	(Pedidos.EmpresaID = 7) AND " +
                                    "(ProdutosEmpresa.TipoRelacaoID = 61) AND " +
                                    "(Pedidos.EstadoID >= 29) AND " +
                                    "(Itens.dtMovimento BETWEEN " + sDataInicio + " AND " + sDataFim + ") AND " +
                                    "(Transacoes.Resultado = 1) AND " +
                                    "(Enderecos.Ordem = 1) AND " +
                                    "(UFs.LocalidadeID = 130) " + sReportaFabricante + sPedidosWeb + sFiltro + sFiltroFamilia + " ";

                }
                else
                {
                    sNVidia_Insert_2 = " /*Vendas Allplus (Exceto FOB Brasil)*/ " +
                    "INSERT INTO #tempNVidia " +
                        "SELECT  Pedidos.EmpresaID, '1368406', Fabricante.Fantasia, Produtos.Modelo, Produtos.Conceito + SPACE(1) + Produtos.Descricao, " +
                                "CASE WHEN Pedidos.TipoPedidoID = 602 THEN Itens.Quantidade ELSE -Itens.Quantidade END, " +
                                "Itens.dtMovimento, NotasFiscais.NotaFiscal, Itens.Ordem, Itens.ValorUnitario, Itens.MoedaID, Pessoas.PessoaID, Pessoas.Fantasia, Enderecos.Endereco, Enderecos.Numero, Enderecos.Complemento, " +
                                "Cidades.Localidade, UFs.CodigoLocalidade2, Enderecos.CEP, Pais.CodigoLocalidade2, Vendedor.Fantasia " +
                            "FROM Pedidos Pedidos WITH(NOLOCK) " +
                                    "INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (Pedidos.PedidoID = Itens.PedidoID) " +
                                    "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID = Produtos.ConceitoID) " +
                                    "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID = ProdutosEmpresa.ObjetoID) AND (ProdutosEmpresa.SujeitoID = Pedidos.EmpresaID) " +
                                    "INNER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                                    "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID = Transacoes.OperacaoID) " +
                                    "INNER JOIN Pessoas Vendedor WITH(NOLOCK) ON (Pedidos.ProprietarioID = Vendedor.PessoaID) " +
                                    "INNER JOIN Pessoas Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID = Pessoas.PessoaID) " +
                                    "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pessoas.PessoaID = Enderecos.PessoaID) " +
                                    "INNER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID = Cidades.LocalidadeID) " +
                                    "INNER JOIN Localidades UFs WITH(NOLOCK) ON (Enderecos.UFID = UFs.LocalidadeID) " +
                                    "INNER JOIN Localidades Pais WITH(NOLOCK) ON (Enderecos.PaisID = Pais.LocalidadeID) " +
                                    "INNER JOIN Pessoas Fabricante WITH(NOLOCK) ON (Produtos.FabricanteID = Fabricante.PessoaID) " +
                            "WHERE	(Pedidos.EmpresaID IN " + EmpresasIn + ") AND " +
                                    "(ProdutosEmpresa.TipoRelacaoID = 61) AND " +
                                    "(Pedidos.EstadoID >= 29) AND " +
                                    "(Itens.dtMovimento BETWEEN " + sDataInicio + " AND " + sDataFim + ") AND " +
                                    "(Transacoes.Resultado = 1) AND " +
                                    "(Enderecos.Ordem = 1) AND " +
                                    "(UFs.LocalidadeID <> 130) " + sReportaFabricante + sPedidosWeb + sFiltro + sFiltroFamilia + " " +
                            "ORDER BY Itens.dtMovimento ASC ";
                }

                sNVida_SelectTable = "DELETE #tempNVidia WHERE (ClienteID = 2) " +
                                "/*Header*/ " +
                                "SELECT	'ReporterID' + @Separador + " +
                                        "'Manufacturer' + @Separador + " +
                                        "'Product Number' + @Separador + " +
                                        "'Product Description' + @Separador + " +
                                        "'Quantity Sold/Returned' + @Separador + " +
                                        "'Sold/Invoice Date' + @Separador + " +
                                        "'Invoice Number' + @Separador + " +
                                        "'Invoice Line Item Number' + @Separador + " +
                                        "'Price Per Unit' + @Separador + " +
                                        "'Currency of Sale' + @Separador + " +
                                        "'Shipto Company Name' + @Separador + " +
                                        "'Shipto Street Address1' + @Separador + " +
                                        "'Shipto Street Address2' + @Separador + " +
                                        "'Shipto City' + @Separador + " +
                                        "'Shipto Stateprov' + @Separador + " +
                                        "'Shipto Postal Code' + @Separador + " +
                                        "'Shipto Country Code' + @Separador + " +
                                        "'Shipto SalesRepname' AS Registro " +
                                "UNION ALL " +
                                "/*Detalhe*/ " +
                                "SELECT a.ReporterID + @Separador + /*[ReporterID]*/ " +
                                        "a.Fabricante + @Separador + /*[Manufacturer]*/ " +
                                        "CONVERT(VARCHAR(10), a.Modelo) + @Separador + /*[Product Number]*/ " +
                                        "a.Descricao + @Separador + /*[Product Description]*/ " +
                                        "CONVERT(VARCHAR(10), a.Quant) + @Separador + /*[Quantity Sold/Returned]*/ " +
                                        "CONVERT(VARCHAR(4),DATEPART(yyyy, a.DataNotaFiscal)) +  " +
                                            "dbo.fn_Pad(CONVERT(VARCHAR(2),DATEPART(mm, a.DataNotaFiscal)), 2, '0', 'L') +  " +
                                            "dbo.fn_Pad(CONVERT(VARCHAR(2),DATEPART(dd, a.DataNotaFiscal)), 2, '0', 'L') + @Separador + /*[Sold/Invoice Date]*/ " +
                                        "CONVERT(VARCHAR(10), a.NotaFiscal) + @Separador + /*[Invoice Number]*/ " +
                                        "CONVERT(VARCHAR(10), a.Ordem) + @Separador + /*[Invoice Line Item Number]*/ " +
                                        "CONVERT(VARCHAR(10), a.ValorUnitario) + @Separador + /*[Price per Unit]*/ " +
                                        "CASE a.MoedaID  " +
                                            "WHEN 541 THEN 'USD' " +
                                            "WHEN 647 THEN 'BRL' END + @Separador + /*[Currency of Sale]*/ " +
                                        "a.Nome + @Separador + /*[Shipto Company Name]*/ " +
                                        "a.Endereco + @Separador + /*[Shipto Street Address1]*/ " +
                                        "REPLACE(ISNULL(a.Numero, SPACE(0)), SPACE(1), SPACE(0)) + " +
                                            "CASE WHEN a.Complemento IS NOT NULL THEN SPACE(1) ELSE SPACE(0) END + " +
                                            "ISNULL(a.Complemento, SPACE(0)) + @Separador + /*[Shipto Street Address2]*/ " +
                                        "a.Cidade + @Separador + /*[Shipto City]*/ " +
                                        "a.Estado + @Separador + /*[Shipto Stateprov]*/ " +
                                        "REPLACE(a.CEP, '.', SPACE(0)) + @Separador + /*[Shipto Postal Code]*/ " +
                                        "a.Pais + @Separador + /*[Shipto Country Code]*/ " +
                                        "a.Vendedor AS Registro /*[SalesRepName]*/ " +
                                    "FROM #tempNVidia a " +
                            "DROP TABLE #tempNVidia SET NOCOUNT OFF ";

                strSQL = sNVidia_variaveis + sNVidia_Insert_1 + sNVidia_Insert_2 + sNVida_SelectTable;
            }
            // Padrão HP
            else if (selPadrao == 953)
            {
                var strSQL_HP = "";

                strSQL_HP = "SELECT Campanhas.Codigo As [Circular] " +
                    ",Produtos.Modelo AS [Descrição] " +
                    ",Produtos.PartNumber AS [Cód HP] " +
                    ",CONVERT(NUMERIC(11,2),((ISNULL(PedItensCampanhas.Valor, 0) * Pedidos.TaxaMoeda) / Itens.Quantidade)) AS [Rebate] " +
                    ",Itens.Quantidade AS [Qtde.] " +
                    ",CONVERT(NUMERIC(11,2),(ISNULL(PedItensCampanhas.Valor, 0) * Pedidos.TaxaMoeda)) AS [Rebate Total] " +
                    ",'' AS [PL] " +
                    ",Campanhas.Campanha AS [ID/OPG] " +
                    ",CONVERT(VARCHAR(10),'" + sDataInicio + "'," + DATE_SQL_PARAM + ") + ' a ' + CONVERT(VARCHAR(10),'" + sDataFim + "'," + DATE_SQL_PARAM + ") AS [Data Inicia a Fim]" +
                "FROM Pedidos_Itens Itens WITH(NOLOCK) " +
                    "INNER JOIN Pedidos WITH(NOLOCK) ON Itens.PedidoID = Pedidos.PedidoID " +
                    "INNER JOIN Conceitos Produtos WITH(NOLOCK)ON Itens.ProdutoID = Produtos.ConceitoID " +
                    "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK)ON Itens.ProdutoID = ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.SujeitoID = Pedidos.EmpresaID " +
                    "LEFT JOIN Pedidos_Itens_Campanhas PedItensCampanhas WITH(NOLOCK) ON Itens.PedItemID = PedItensCampanhas.PedItemID " +
                    "LEFT JOIN Campanhas_Produtos CamProdutos WITH(NOLOCK) ON PedItensCampanhas.CamProdutoID = CamProdutos.CamProdutoID " +
                    "LEFT JOIN Campanhas WITH(NOLOCK) ON CamProdutos.CampanhaID = Campanhas.CampanhaID ";

                if (chkSoResultado)
                    strSQL_HP += "INNER JOIN Operacoes Transacao WITH(NOLOCK) ON (Pedidos.TransacaoID = Transacao.OperacaoID) AND (Transacao.Resultado = 1) ";

                strSQL_HP += "WHERE Pedidos.EmpresaID IN " + EmpresasIn + " AND " +
                    " ProdutosEmpresa.TipoRelacaoID = 61 AND " +
                    " Pedidos.Suspenso = 0 AND Pedidos.TipoPedidoID=602 AND Pedidos.EstadoID>=29 AND " +
                    "(Itens.dtMovimento BETWEEN " + sDataInicio + " AND " + sDataFim + " ) " +
                    sMarca + sReportaFabricante + sPedidosWeb + sFiltroFabricante + sFiltroFamilia + sFiltro + " ";

                strSQL = strSQL_HP;
            }
            // Padrão SanDisk
            else if (selPadrao == 954)
            {
                var sResultado = (chkSoResultado ? " AND Transacoes.Resultado = 1 " : "");


                strSQL = "SELECT Pedidos.EmpresaID AS [EmpID], " +
                    "Produtos.ConceitoID AS [Product Code], " +
                    "Produtos.PartNumber AS [Sandisk ID], " +
                    "Produtos.Descricao AS [Description], " +
                    "CONVERT(VARCHAR(10),NotasFiscais.dtNotaFiscal,112) AS [Date], " +
                    "NotasFiscais.NotaFiscal AS [Invoice#], " +
                    "Itens.Quantidade * (CASE WHEN Pedidos.TipoPedidoID = 602 THEN 1 ELSE -1 END) AS [Quantity], " +
                    "Pessoas.PessoaID AS [Customer ID], " +
                    "Pessoas.Nome AS [Customer], " +
                    "ISNULL(Enderecos.Endereco,SPACE(0)) + SPACE(1) + ISNULL(Enderecos.Numero,SPACE(0)) AS [Address1], " +
                    "Cidades.Localidade AS [City], " +
                    "Enderecos.CEP As [Postal Code], " +
                    "Paises.Localidade AS [Country] " +
                "FROM Pedidos WITH(NOLOCK) " +
                            "INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (Itens.PedidoID=Pedidos.PedidoID) " +
                            "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                            "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID=Transacoes.OperacaoID) " +
                            "INNER JOIN Pessoas WITH(NOLOCK) ON (Pedidos.ParceiroID=Pessoas.PessoaID) " +
                            "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pedidos.PessoaID=Enderecos.PessoaID) " +
                            "LEFT OUTER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) " +
                            "LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) " +
                            "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID=Produtos.ConceitoID) " +
                            "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.SujeitoID=Pedidos.EmpresaID) " +
                 "WHERE Pedidos.PessoaID <> Pedidos.EmpresaID " +
                        "AND Enderecos.EndFaturamento=1  " +
                        "AND Enderecos.Ordem=1 " +
                        "AND ProdutosEmpresa.TipoRelacaoID=61 " +
                        "AND Pedidos.Suspenso=0 " +
                        "AND ((Pedidos.TipoPedidoID=601 AND Pedidos.EstadoID>=30) " +
                                "OR (Pedidos.TipoPedidoID=602 AND Pedidos.EstadoID >=29)) " +
                        "AND Itens.dtMovimento>= " + sDataInicio + " " +
                        "AND Itens.dtMovimento<= " + sDataFim + " " +
                        sResultado + sMarca + sReportaFabricante + sPedidosWeb + sFiltroFabricante + sFiltroFamilia + sFiltro +
                "ORDER BY Pedidos.EmpresaID, NotasFiscais.NotaFiscal, Produtos.ConceitoID ";
            }

            // Generico
            if (selPadrao == 701)
            {
                var strSQL_From = "FROM Pedidos_Itens Itens WITH(NOLOCK) " +
                    "INNER JOIN Pedidos WITH(NOLOCK) ON (Itens.PedidoID=Pedidos.PedidoID) " +
                    ((glb_nEmpresaID != 7) ? "LEFT JOIN Pedidos_Parcelas Parcelas WITH(NOLOCK) ON (Parcelas.PedidoID=Pedidos.PedidoID AND Parcelas.historicopadraoid=3173 ) " : "") +
                    "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                    "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID=Transacoes.OperacaoID) " +
                    /*"INNER JOIN Pessoas Empresa WITH (NOLOCK) ON (Empresa.PessoaID=Pedidos.EmpresaID) " +*/
                    "INNER JOIN Pessoas Vendedores WITH(NOLOCK) ON (Pedidos.ProprietarioID=Vendedores.PessoaID) " +
                    "INNER JOIN Pessoas Parceiros WITH(NOLOCK) ON (Pedidos.ParceiroID=Parceiros.PessoaID) " +
                    "LEFT OUTER JOIN TiposAuxiliares_Itens Classificacoes WITH(NOLOCK) ON (Parceiros.ClassificacaoID=Classificacoes.ItemID) " +
                    "LEFT OUTER JOIN Pessoas_Telefones Faxes WITH(NOLOCK) ON (Parceiros.PessoaID=Faxes.PessoaID AND Faxes.TipoTelefoneID=122 AND Faxes.Ordem=1) " +
                    "LEFT OUTER JOIN Pessoas_URLs URLs WITH(NOLOCK) ON (Parceiros.PessoaID=URLs.PessoaID AND URLs.TipoURLID=124 AND URLs.Ordem=1) " +
                    "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Parceiros.PessoaID=Enderecos.PessoaID) " +
                    "LEFT OUTER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades Estados WITH(NOLOCK) ON (Enderecos.UFID=Estados.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) " +
                    "INNER JOIN Pessoas Clientes WITH(NOLOCK) ON (Pedidos.PessoaID=Clientes.PessoaID) " +
                    "INNER JOIN Pessoas_Enderecos Cliente_Enderecos WITH(NOLOCK) ON (Clientes.PessoaID=Cliente_Enderecos.PessoaID) " +
                    "LEFT OUTER JOIN Localidades Cliente_Cidades WITH(NOLOCK) ON (Cliente_Enderecos.CidadeID=Cliente_Cidades.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades Cliente_Estados WITH(NOLOCK) ON (Cliente_Enderecos.UFID=Cliente_Estados.LocalidadeID) " +
                    "LEFT OUTER JOIN Localidades Cliente_Paises WITH(NOLOCK) ON (Cliente_Enderecos.PaisID=Cliente_Paises.LocalidadeID) " +
                    "INNER JOIN Pessoas Transportadoras WITH(NOLOCK) ON (Pedidos.TransportadoraID = Transportadoras.PessoaID) " +
                    "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID=Produtos.ConceitoID) " +
                    "LEFT OUTER JOIN Conceitos_Caracteristicas Caracteristicas WITH(NOLOCK) ON (Produtos.ConceitoID=Caracteristicas.ProdutoID AND dbo.fn_Produto_CaracteristicaOrdem(Caracteristicas.ProdutoID, Caracteristicas.CaracteristicaID)=1) " +
                    "LEFT OUTER JOIN Conceitos LinhasProduto WITH(NOLOCK) ON (LinhasProduto.ConceitoID=Produtos.LinhaProdutoID) " +
                    "INNER JOIN Conceitos Familias WITH(NOLOCK) ON (Produtos.ProdutoID=Familias.ConceitoID) " +
                    "INNER JOIN Conceitos Marcas WITH(NOLOCK) ON (Produtos.MarcaID=Marcas.ConceitoID) " +
                    "INNER JOIN RelacoesPesCon ProdutosEmpresa  WITH(NOLOCK) ON (Itens.ProdutoID=ProdutosEmpresa.ObjetoID) AND (ProdutosEmpresa.SujeitoID=Pedidos.EmpresaID) " +
                    ((chkDadosProduto) ? "LEFT OUTER JOIN Conceitos_Identificadores Identificadores WITH(NOLOCK) ON ((Identificadores.ProdutoID = Itens.ProdutoID) AND (Identificadores.GTIN = 1))" : " ");

                var strSQL_Where1 = "WHERE (Pedidos.EmpresaID IN " + EmpresasIn + " AND " +
                    "Enderecos.EndFaturamento=1 AND Enderecos.Ordem=1 AND ";

                var strSQL_Where2 = "Cliente_Enderecos.EndFaturamento=1 AND Cliente_Enderecos.Ordem=1 AND ";

                var strSQL_Where3 = "ProdutosEmpresa.TipoRelacaoID = 61 AND " +
                    strSohResultado + " " +
                    "Pedidos.Suspenso=0 AND ((Pedidos.TipoPedidoID=601 AND Pedidos.EstadoID>=30) OR (Pedidos.TipoPedidoID=602 AND Pedidos.EstadoID>=29)) AND " +
                    "Itens.dtMovimento >=" + sDataInicio + " AND Itens.dtMovimento <=" + sDataFim + " " +
                    sMarca + sReportaFabricante + sPedidosWeb + sFiltroFabricante + sFiltroFamilia + sFiltro + ") ";

                strSQL_OrderBy = "ORDER BY Data_, Pedido_";

                strSQL = strSQL_Select1 + strSQL_Select2 + strSQL_Select3 + strSQL_From + strSQL_Where1 + strSQL_Where2 +
                    strSQL_Where3 + strSQL_OrderBy;

            }
            var Header_Body = (Formato == 1 ? "Header" : "Body");

            var posY = (Formato == 1 ? "0" : "0.04");
            //var posY = (Formato == 1 ? "0" : "0.004");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };


            if (Formato == 1)
            {

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Landscape", "0.80");

                Relatorio.CriarObjLabel("Fixo", "", Title, "10.13834", "0.1", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "1.013834", "0.01", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "19.795", "0.1", "11", "black", "B", Header_Body, "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "1.9795", "0.01", "11", "black", "B", Header_Body, "3.72187", "Horas");


                //Cria Tabela           
                Relatorio.CriarObjTabela("0.2", posY, "Query1", true, true, true);
                //Relatorio.CriarObjTabela("0.02", posY, "Query1", true, true, true);

                Relatorio.TabelaEnd();
            }

            else if (Formato == 2) // Se Excel
            {

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                //Cabeçalhos retirados devido ao Gsheet apresentar falha na conversão devido às celulas mescladas - MTH 19/10/2018
                //Relatorio.CriarObjLabel("Fixo", "", Title, "10.13834", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "1.013834", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabelData("Now", "", "", "16.795", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "1.6795", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.0001", "Query1", true, true, true);
                //Relatorio.CriarObjTabela("0.00", "0.00001", "Query1", true, true, true);

                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }
        public void propostaSolicitacao()
        {
            glb_nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["EmpresaID"]);
            int PedidoID = Convert.ToInt32(HttpContext.Current.Request.Params["PedidoID"]);
            int nIdiomaDeID = Convert.ToInt32(HttpContext.Current.Request.Params["nIdiomaDeID"]);
            int nIdiomaParaID = Convert.ToInt32(HttpContext.Current.Request.Params["nIdiomaParaID"]);
            int glb_nEhMicro = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEhMicro"]);
            int nTransacaoID = Convert.ToInt32(HttpContext.Current.Request.Params["nTransacaoID"]);
            int nValidade = Convert.ToInt32(HttpContext.Current.Request.Params["nValidade"]);
            string Moeda = Convert.ToString(HttpContext.Current.Request.Params["MoedaID"]);
            string nContatoID = Convert.ToString(HttpContext.Current.Request.Params["nContatoID"]);
            string sContatoCargo = Convert.ToString(HttpContext.Current.Request.Params["sContatoCargo"]);
            string DATE_SQL_PARAM = HttpContext.Current.Request.Params["DATE_SQL_PARAM"];
            bool bInformacoesAdicionais = Convert.ToBoolean(HttpContext.Current.Request.Params["bInformacoesAdicionais"]);
            int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
            string Title = Convert.ToString(HttpContext.Current.Request.Params["Title"]);
            bool bSup = Convert.ToBoolean(HttpContext.Current.Request.Params["bSup"]);
            bool bFichaTecnica = Convert.ToBoolean(HttpContext.Current.Request.Params["bFichaTecnica"]);


            string sPesos = "";
            string sDimensoes = "";
            string sPrazos = "";
            string sMeses = "";

            string sReport = "";
            string sReport2 = "";

            string strSQLEmpresa = "";


            sReport = (RelatorioID == 40132 ? "Solicitação de Compra" : "Proposta Comercial");
            sReport2 = (RelatorioID == 40132 ? "Solicitação" : "Proposta");

            nContatoID = "0";

            // QUERY MASTER
            string strSQLSelect1 = "SELECT TOP 1 " +
                "Pedidos.PedidoID AS PedidoIDLink, " +
                "RTRIM(LTRIM(STR(Pedidos.PedidoID))) AS PedidoID, CONVERT(VARCHAR, Pedidos.dtPedido, " + DATE_SQL_PARAM + ") as dtPedido, " +
                "Moedas.SimboloMoeda AS MoedaPedido, Pedidos.ValorTotalPedido as ValorTotalPedido, MoedasConvertidas.SimboloMoeda AS MoedaConversao, " +
                "CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(Pedidos.ValorTotalPedido, Pedidos.TaxaMoeda)) AS ValorConvertido, " +
                "(CASE WHEN Pedidos.dtPrevisaoEntrega <= GETDATE() THEN " + "'" + "Imediata" + "'" + " ELSE " +
                    "CONVERT(VARCHAR, Pedidos.dtPrevisaoEntrega , " + DATE_SQL_PARAM + ") + SPACE(1) + " +
                    "CONVERT(VARCHAR, Pedidos.dtPrevisaoEntrega, 108) END) AS dtPrevisaoEntrega, " +
                "dbo.fn_Tradutor((CASE Pedidos.Frete WHEN 0 THEN " + "'" + translateTherm("A Pagar", glb_aTranslate_Proposta) + "'" + "ELSE " + "'" + translateTherm("Pago", glb_aTranslate_Proposta) + "'" + " END), " + nIdiomaDeID + ", " + nIdiomaParaID + ", NULL) AS Frete, " +
                "Pedidos.SeuPedido as SeuPedido, Pedidos.Observacao as Observacao, " +
                "Colaboradores.Fantasia AS ColaboradorFantasia, " +
                "(ISNULL(dbo.fn_Pessoa_Telefone(Colaboradores.PessoaID, 119, 120,1,0, NULL) + SPACE(3), SPACE(0))) AS ColaboradorTelefones, " +
                "(SELECT URL FROM Pessoas_URLs WITH(NOLOCK) WHERE (PessoaID=Colaboradores.PessoaID AND TipoURLID=124 AND Ordem=1)) AS ColaboradorEmail, ";

            string strSQLSelect2 =
                "RTRIM(LTRIM(STR(Pedidos.PessoaID))) AS PessoaID, Pessoas.Nome AS PessoaNome,";

            if (nIdiomaParaID == 246)
            {
                strSQLSelect2 = strSQLSelect2 + "ISNULL(PessoaPaises.Localidade, SPACE(0)) AS PessoaPais, ";
            }
            else
            {
                strSQLSelect2 = strSQLSelect2 + "ISNULL(PessoaPaises.NomeInternacional, SPACE(0)) AS PessoaPais, ";
            }

            strSQLSelect2 = strSQLSelect2 + "(CASE ISNULL(PessoaPaises.EnderecoInvertido, 0) WHEN 1 " +
                                                        "THEN ISNULL(PessoaEnderecos.Numero, SPACE(0)) + ISNULL(SPACE(1) + PessoaEnderecos.Endereco, SPACE(0)) " +
                                                        "ELSE ISNULL(PessoaEnderecos.Endereco, SPACE(0)) + ISNULL(SPACE(1) + PessoaEnderecos.Numero, SPACE(0)) END) AS PessoaEndereco, " +
                "(ISNULL(PessoaEnderecos.Complemento + SPACE(1), SPACE(0)) + ISNULL(PessoaEnderecos.Bairro, SPACE(0))) AS PessoaComplementoBairro, " +
                "(PessoaCidades.Localidade + SPACE(1) + ISNULL(PessoaUFs.CodigoLocalidade2, SPACE(0)) + SPACE(1) + PessoaEnderecos.CEP) AS PessoaCEPCidadeUF, ";

            strSQLSelect2 = strSQLSelect2 + "ISNULL(dbo.fn_Pessoa_Documento(Pessoas.PessoaID, NULL, NULL, 101, 111, 0), SPACE(0)) + " +
                "ISNULL(SPACE(3) + dbo.fn_Pessoa_Documento(Pessoas.PessoaID, NULL, NULL, 102, 112, 0), SPACE(0)) AS PessoaDocumento1, " +
                "ContatoNome = (SELECT Nome FROM Pessoas WITH(NOLOCK) WHERE (PessoaID=" + nContatoID + ")), " +
                "ContatoCargo = " + "'" + sContatoCargo + "'" + ", " +
                "(ISNULL(dbo.fn_Pessoa_Telefone(" + nContatoID + ", 119, 120,1,0,NULL) + SPACE(3), SPACE(0)) + " +
                    "ISNULL(dbo.fn_Pessoa_Telefone(" + nContatoID + ", 122, 121,1,0,NULL), SPACE(0))) AS ContatoTelefones, " +
                "(SELECT URL FROM Pessoas_URLs WITH(NOLOCK) WHERE (PessoaID=" + nContatoID + " AND TipoURLID=124 AND Ordem=1)) AS ContatoEmail, " +
                "ImagensPessoa.Arquivo AS FotoPessoa, ImagensEmpresa.Arquivo AS FotoEmpresa ";

            string strSQLFrom = "FROM " +
                "Pedidos WITH(NOLOCK) " +
                    "INNER JOIN Conceitos Moedas WITH(NOLOCK) ON (Pedidos.MoedaID=Moedas.ConceitoID) " +
                    "INNER JOIN Pessoas Colaboradores WITH(NOLOCK) ON (Pedidos.ProprietarioID=Colaboradores.PessoaID) " +
                    "INNER JOIN Pessoas WITH(NOLOCK) ON (" + (RelatorioID == 40133 ? "Pedidos.PessoaID=Pessoas.PessoaID" : "Pedidos.EmpresaID=Pessoas.PessoaID") + ") " +
                    "INNER JOIN Pessoas_Enderecos PessoaEnderecos WITH(NOLOCK) ON (Pessoas.PessoaID=PessoaEnderecos.PessoaID) " +
                     "LEFT OUTER JOIN Localidades PessoaCidades WITH(NOLOCK) ON (PessoaEnderecos.CidadeID=PessoaCidades.LocalidadeID)  " +
                     "LEFT OUTER JOIN Localidades PessoaUFs WITH(NOLOCK) ON (PessoaEnderecos.UFID=PessoaUFs.LocalidadeID)  " +
                     "LEFT OUTER JOIN Localidades PessoaPaises WITH(NOLOCK) ON (PessoaEnderecos.PaisID=PessoaPaises.LocalidadeID)  " +
                     "LEFT OUTER JOIN [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] ImagensPessoa WITH(NOLOCK) ON (ImagensPessoa.FormID=1210 AND ImagensPessoa.SubFormID=20100 AND ImagensPessoa.RegistroID=Pedidos.ParceiroID AND ImagensPessoa.TipoArquivoID = 1451)  " +
                     "LEFT OUTER JOIN [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] ImagensEmpresa WITH(NOLOCK) ON (ImagensEmpresa.FormID=1210 AND ImagensEmpresa.SubFormID=20100 AND ImagensEmpresa.RegistroID=Pedidos.EmpresaID AND ImagensEmpresa.TipoArquivoID = 1451),  " +
                 "Recursos Sistema WITH(NOLOCK)  " +
                     "INNER JOIN Conceitos MoedasConvertidas WITH(NOLOCK) ON (Sistema.MoedaID=MoedasConvertidas.ConceitoID)  ";

            string strSQLWhere = "WHERE  " +
                 "(Pedidos.PedidoID= " + PedidoID + " AND  " +
                 "Sistema.RecursoID=999 AND  ";

            strSQLWhere += "(PessoaEnderecos.Ordem=1)) ";

            string strSQL = strSQLSelect1 + strSQLSelect2 + strSQLFrom + strSQLWhere;

            // QUERY DETAIL1
            if (glb_nEhMicro == 0)
            {
                strSQLSelect1 = "SELECT  " +
                     "Itens.PedidoID, Itens.Ordem AS Ordem, (RTRIM(LTRIM(STR(Itens.ProdutoID)))) AS ProdutoID,  " +
                     "Produtos.Conceito AS Produto, " +
                     "CONVERT(INT, Itens.Quantidade) AS Quantidade,  " +
                     "Moedas.SimboloMoeda AS Moeda,  " +
                     "dbo.fn_PedidoItem_Totais(Itens.PedItemID, 7) AS ValorUnitario, dbo.fn_PedidoItem_Totais(Itens.PedItemID, 8) AS ValorTotal,  " +
                     "ICMSISS = (SELECT CONVERT(VARCHAR(6), CONVERT(NUMERIC(5,2), ISNULL(SUM(ItensImpostos.Aliquota),0))) + CHAR(37)  " +
                         "FROM Pedidos_Itens_Impostos ItensImpostos WITH(NOLOCK), Conceitos Impostos WITH(NOLOCK)  " +
                         "WHERE (Itens.PedItemID=ItensImpostos.PedItemID AND ItensImpostos.ImpostoID=Impostos.ConceitoID AND  " +
                         "Impostos.IncidePreco=1 AND Impostos.InclusoPreco=1 AND Impostos.AlteraAliquota=1)),  ";

                strSQLSelect2 =
                     "IPI = CONVERT(VARCHAR, CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(Itens.PedidoID, Produtos.ConceitoID, GETDATE(), Itens.FinalidadeID, -6))) + CHAR(37), " +
                     "(SELECT TOP 1 Fornecedores.Codigo FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK)  " +
                     "WHERE (ProdutosEmpresa.SujeitoID =  " + glb_nEmpresaID + " AND ProdutosEmpresa.TipoRelacaoID = 61 AND  " +
                         "Produtos.ConceitoID = ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.RelacaoID = Fornecedores.RelacaoID AND  " +
                         "Fornecedores.FornecedorID = Pedidos.PessoaID)) AS CodigoFornecedor,  " +
                     "Produtos.Modelo AS Modelo,  " +
                     "ValorConvertido = CASE Itens.TaxaMoeda WHEN 1 THEN NULL ELSE CONVERT(NUMERIC(11,2), " +
                         "dbo.fn_DivideZero(Itens.ValorUnitario,Itens.TaxaMoeda)) END,  " +
                     "TaxaMoeda = CASE Itens.TaxaMoeda WHEN 1 THEN NULL ELSE CONVERT(NUMERIC(10,4), Itens.TaxaMoeda) END  ";
            }
            else
            {
                strSQLSelect1 = "SELECT  " +
                     "Itens.PedidoID, Itens.Ordem AS Ordem, (RTRIM(LTRIM(STR(Itens.ProdutoID)))) AS ProdutoID,  " +
                     "(CASE ISNULL(Familias.EhMicro, 0) WHEN 1 THEN (LEFT(Produtos.Conceito, 6) + SPACE(1) + ISNULL(dbo.fn_Pedido_ProdutoModelo(Pedidos.PedidoID, Produtos.ConceitoID), SPACE(0))) ELSE  " +
                     "Produtos.Conceito END) AS Produto,  " +
                     "CONVERT(INT, Itens.Quantidade) AS Quantidade,  " +
                     "(CASE ISNULL(Familias.EhMicro, 0) WHEN 1 THEN Moedas.SimboloMoeda ELSE SPACE(0) END) AS Moeda,  " +
                     "(CASE ISNULL(Familias.EhMicro, 0) WHEN 1 THEN CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(Pedidos.ValorTotalPedido, Itens.Quantidade)) END) AS ValorUnitario,  " +
                     "(CASE ISNULL(Familias.EhMicro, 0) WHEN 1 THEN Pedidos.ValorTotalPedido END) AS ValorTotal,  " +
                     "ICMSISS = (CASE ISNULL(Familias.EhMicro, 0) WHEN 1 THEN (SELECT CONVERT(VARCHAR(6),CONVERT(NUMERIC(5,2),ISNULL(SUM(ItensImpostos.Aliquota),0))) + CHAR(37)  " +
                         "FROM Pedidos_Itens_Impostos ItensImpostos WITH(NOLOCK), Conceitos Impostos WITH(NOLOCK)  " +
                         "WHERE (Itens.PedItemID=ItensImpostos.PedItemID AND ItensImpostos.ImpostoID=Impostos.ConceitoID AND  " +
                         "Impostos.IncidePreco=1 AND Impostos.InclusoPreco=1 AND Impostos.AlteraAliquota=1)) END),  ";

                strSQLSelect2 =
                     "IPI = (CASE ISNULL(Familias.EhMicro, 0) WHEN 1 THEN CONVERT(VARCHAR, CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(Itens.PedidoID, Produtos.ConceitoID, GETDATE(), Itens.FinalidadeID, -6))) + CHAR(37) END)  ";
            }

            strSQLFrom = "FROM  " +
                 "Pedidos_Itens Itens WITH(NOLOCK), Pedidos WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK), Conceitos Moedas WITH(NOLOCK)  ";

            strSQLWhere = "WHERE  " +
                 "(Itens.PedidoID= " + PedidoID + " AND Itens.ProdutoID=Produtos.ConceitoID AND  " +
                 "Pedidos.PedidoID=Itens.PedidoID AND Produtos.ProdutoID = Familias.ConceitoID AND  " +
                 "Produtos.MarcaID=Marcas.ConceitoID AND Pedidos.MoedaID=Moedas.ConceitoID)  ";

            string strSQLOrderBy = "ORDER BY  " +
                 "Itens.Ordem ";

            string strSQL2 = strSQLSelect1 + strSQLSelect2 + strSQLFrom + strSQLWhere + strSQLOrderBy;

            // QUERY DETAIL2
            var strSQL3 = "SELECT " +
                    "PedidosParcelas.PedidoID , PedidosParcelas.Ordem AS Ordem, " +
                    "Tipo = ISNULL(HP.HistoricoAbreviado, SPACE(0)), " +
                    "dbo.fn_Tradutor(FormasPagamento.ItemMasculino, " + nIdiomaDeID + ", " + nIdiomaParaID + ", NULL) AS Forma, " +
                    "PedidosParcelas.PrazoPagamento AS Prazo, PedidosParcelas.ValorParcela AS Valor, " +
                    "PedidosParcelas.Observacao AS Observacao " +
                "FROM Pedidos_Parcelas PedidosParcelas WITH(NOLOCK) " +
                    "INNER JOIN TiposAuxiliares_Itens FormasPagamento WITH(NOLOCK) ON (PedidosParcelas.FormaPagamentoID=FormasPagamento.ItemID) " +
                    "LEFT OUTER JOIN HistoricosPadrao HP WITH(NOLOCK) ON (PedidosParcelas.HistoricoPadraoID = HP.HistoricoPadraoID AND ISNULL(PedidosParcelas.HistoricoPadraoID, 0) <> 3173) " +
                "WHERE (PedidoID = " + PedidoID + " ) ";

            if (!bSup)
                strSQL3 = strSQL3 + " AND (PedidosParcelas.HistoricoPadraoID IS NULL) ";

            // QUERY DETAIL3
            strSQLSelect1 = "SELECT " +
             "Itens.PedidoID as PedidoID, (RTRIM(LTRIM(STR(Itens.ProdutoID)))) AS ProdutoID, (RTRIM(LTRIM(STR(Itens.ProdutoID)))) AS ProdutoID2, Itens.Ordem AS OrdemProduto,  Produtos.Conceito AS Produto, Produtos.Descricao as Descricao, Produtos.PartNumber ";

            strSQLSelect2 = "";

            strSQLFrom = "FROM " +
                "Pedidos_Itens Itens WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), RelacoesPesCon RelPesCon WITH(NOLOCK) ";

            strSQLWhere = "WHERE (" +
                "Itens.PedidoID= " + PedidoID + " AND Itens.ProdutoID=Produtos.ConceitoID AND Produtos.ProdutoID = Familias.ConceitoID " +
                "AND RelPesCon.SujeitoID=" + glb_nEmpresaID + " AND RelPesCon.ObjetoID=Itens.ProdutoID " +
                "AND RelPesCon.FichaTecnica=1)"; // AND Produtos.HomologacaoOK=1) ";

            strSQLOrderBy = "ORDER BY Itens.Ordem";

            var strSQL4 = strSQLSelect1 + strSQLSelect2 + strSQLFrom + strSQLWhere + strSQLOrderBy;

            // QUERY DETAIL3

            if (nIdiomaParaID == 246)
            {
                sPesos = "Pesos (Bruto/Líquido)";
                sDimensoes = "Dimensões (L/A/P)";
                sPrazos = "Prazos (Troca/Garantia/Asstec)";
                sMeses = "meses";
            }
            else
            {
                sPesos = "Weights (Gross/Net)";
                sDimensoes = "Dimensions (W/H/L)";
                sPrazos = "Terms (Exchange/Warranty/Repair)";
                sMeses = "months";
            }
            var strSQLTmp = "SELECT Itens.PedidoID ,Itens.ProdutoID AS ProdutoID, " +
                "3 AS Ordem, '" + sPesos + "' AS Caracteristica, " +
                "ISNULL(CONVERT(VARCHAR(7), CONVERT(NUMERIC(5,2),dbo.fn_Produto_PesosMedidas(Produtos.ConceitoID,1,1))) + CHAR(47), SPACE(0)) + " +
                "ISNULL(CONVERT(VARCHAR(7), CONVERT(NUMERIC(5,2),dbo.fn_Produto_PesosMedidas(Produtos.ConceitoID,0,1))), SPACE(0)) + ' Kg' AS Valor ";
            strSQLTmp += "FROM " +
                "Pedidos_Itens Itens WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), RelacoesPesCon RelPesCon WITH(NOLOCK) ";

            strSQLTmp += "WHERE (" +
                "Itens.PedidoID= " + PedidoID + " AND Itens.ProdutoID=Produtos.ConceitoID AND Produtos.ProdutoID = Familias.ConceitoID " +
                "AND RelPesCon.SujeitoID=" + glb_nEmpresaID + " AND RelPesCon.ObjetoID=Itens.ProdutoID " +
                "AND RelPesCon.FichaTecnica=1) ";

            strSQLTmp += "UNION ALL ";

            strSQLTmp += "SELECT  Itens.PedidoID, Itens.ProdutoID AS ProdutoID, " +
            "2 AS Ordem, '" + sDimensoes + "' AS Caracteristica, " +
                "ISNULL(CONVERT(VARCHAR(7), CONVERT(NUMERIC(7,2),dbo.fn_Produto_PesosMedidas(Produtos.ConceitoID,1,2))) + CHAR(120), SPACE(0)) + " +
                    "ISNULL(CONVERT(VARCHAR(7), CONVERT(NUMERIC(7,2),dbo.fn_Produto_PesosMedidas(Produtos.ConceitoID,1,3))) + CHAR(120), SPACE(0)) + " +
                    "ISNULL(CONVERT(VARCHAR(7), CONVERT(NUMERIC(7,2),dbo.fn_Produto_PesosMedidas(Produtos.ConceitoID,1,4))), SPACE(0)) + ' mm' AS Valor ";
            strSQLTmp += "FROM " +
                "Pedidos_Itens Itens WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), RelacoesPesCon RelPesCon WITH(NOLOCK) ";

            strSQLTmp += "WHERE (" +
                "Itens.PedidoID= " + PedidoID + " AND Itens.ProdutoID=Produtos.ConceitoID AND Produtos.ProdutoID = Familias.ConceitoID " +
                "AND RelPesCon.SujeitoID=" + glb_nEmpresaID + " AND RelPesCon.ObjetoID=Itens.ProdutoID " +
                "AND RelPesCon.FichaTecnica=1) ";

            strSQLTmp += "UNION ALL ";

            strSQLTmp += "SELECT " +
                " Itens.PedidoID,  Itens.ProdutoID AS ProdutoID, " +
                "1 AS Ordem, '" + sPrazos + "' AS Caracteristica, " +
                    "ISNULL(CONVERT(VARCHAR(7), RelPesCon.PrazoTroca) + CHAR(47), SPACE(0)) + " +
                        "ISNULL(CONVERT(VARCHAR(7), RelPesCon.PrazoGarantia) + CHAR(47), SPACE(0)) + " +
                        "ISNULL(CONVERT(VARCHAR(7), RelPesCon.PrazoAsstec), SPACE(0)) + ' " + sMeses + "' AS Valor ";

            strSQLTmp += "FROM " +
                "Pedidos_Itens Itens WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), RelacoesPesCon RelPesCon WITH(NOLOCK) ";

            strSQLTmp += "WHERE (" +
                "Itens.PedidoID= " + PedidoID + " AND Itens.ProdutoID=Produtos.ConceitoID AND Produtos.ProdutoID = Familias.ConceitoID " +
                "AND RelPesCon.SujeitoID=" + glb_nEmpresaID + " AND RelPesCon.ObjetoID=Itens.ProdutoID " +
                "AND RelPesCon.FichaTecnica=1) ";

            strSQLTmp += "UNION ALL ";

            // QUERY DETAIL4
            strSQLTmp +=
                "SELECT " +
                    "Itens.PedidoID as PedidoID, " +
                     "ItensCaracteristicas.ProdutoID as ProdutoID, " +
                     "dbo.fn_Produto_CaracteristicaOrdem(ItensCaracteristicas.ProdutoID, ItensCaracteristicas.CaracteristicaID) AS Ordem, " +
                     "dbo.fn_Tradutor(Caracteristicas.Conceito, " + nIdiomaDeID + ", " + nIdiomaParaID + ", NULL) AS Caracteristica, " +
                    "ItensCaracteristicas.Valor AS Valor ";

            strSQLTmp +=
                "FROM Conceitos_Caracteristicas ItensCaracteristicas WITH(NOLOCK) " +
                    "INNER JOIN Conceitos Caracteristicas WITH(NOLOCK) ON (Caracteristicas.ConceitoID = ItensCaracteristicas.CaracteristicaID) " +
                    "INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (Itens.ProdutoID = ItensCaracteristicas.ProdutoID) " +
                    "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Produtos.ConceitoID = Itens.ProdutoID) " +
                    "INNER JOIN RelacoesConceitos RelConceitos WITH(NOLOCK) " +
                        "ON ((RelConceitos.SujeitoID = ItensCaracteristicas.CaracteristicaID) AND (RelConceitos.ObjetoID = Produtos.ProdutoID)) ";

            strSQLTmp +=
                "WHERE ((Itens.PedidoID = " + PedidoID + ") " +
                    "AND (ItensCaracteristicas.Checado = 1) " +
                    "AND (RelConceitos.FichaTecnica = 1) )";
            

            strSQLTmp += "ORDER BY ProdutoID, Itens.PedidoID, Ordem";

            string strSQL5 = strSQLTmp;

            strSQLEmpresa = "SELECT * FROM Pessoas  " +
                                  "WHERE PessoaID = " + glb_nEmpresaID;

            string sqlImagensEmpresas = "SELECT TOP 1 ImagensEmpresa.Arquivo as ImagemEmpresa, ImagensPessoa.Arquivo as ImagemPessoa FROM Pedidos a with(nolock) " +
                               "LEFT OUTER JOIN [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] ImagensPessoa WITH(NOLOCK) ON (ImagensPessoa.FormID=1210 AND ImagensPessoa.SubFormID=20100 AND ImagensPessoa.RegistroID=a.ParceiroID AND ImagensPessoa.TipoArquivoID = 1451) " +
                               "LEFT OUTER JOIN [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] ImagensEmpresa WITH(NOLOCK) ON (ImagensEmpresa.FormID=1210 AND ImagensEmpresa.SubFormID=20100 AND ImagensEmpresa.RegistroID=a.EmpresaID AND ImagensEmpresa.TipoArquivoID = 1451),Recursos Sistema WITH(NOLOCK) " +
                               " where a.PedidoID = " + PedidoID + " and Sistema.RecursoID=999 ";

            string sqlImagensPessoas = "SELECT TOP 1 ImagensEmpresa.Arquivo as ImagemEmpresa, ImagensPessoa.Arquivo as ImagemPessoa FROM Pedidos a with(nolock) " +
                               "LEFT OUTER JOIN [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] ImagensPessoa WITH(NOLOCK) ON (ImagensPessoa.FormID=1210 AND ImagensPessoa.SubFormID=20100 AND ImagensPessoa.RegistroID=a.ParceiroID AND ImagensPessoa.TipoArquivoID = 1451) " +
                               "LEFT OUTER JOIN [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] ImagensEmpresa WITH(NOLOCK) ON (ImagensEmpresa.FormID=1210 AND ImagensEmpresa.SubFormID=20100 AND ImagensEmpresa.RegistroID=a.EmpresaID AND ImagensEmpresa.TipoArquivoID = 1451),Recursos Sistema WITH(NOLOCK) " +
                               " where a.PedidoID = " + PedidoID + " and Sistema.RecursoID=999 ";

            arrSqlQuery = new string[,] {
                                        {"Query11", strSQL},
                                        {"Query22", strSQL2},
                                        {"Query3", strSQL3},
                                        {"Query4", strSQL4},
                                        {"Query5", strSQL5},                                        
                                        {"strSQLEmpresa", strSQLEmpresa},
                                        {"SQLImagensEmpresas",sqlImagensEmpresas},
                                        {"SQLImagensPessoas",sqlImagensPessoas}
                                      };


            arrIndexKey = new string[,] { { "Query5", "ProdutoID;Ordem", "ProdutoID" } };

            Relatorio.CriarRelatório(translateTherm(sReport2.ToString(), glb_aTranslate_Proposta) + '-' + PedidoID, arrSqlQuery, "BRA", "Portrait", "0.60");

            
            //Labels Cabeçalho
            Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.2", "0.1", "11", "black", "B", "Header", "2.5", "");
            //Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.02", "0.01", "11", "black", "B", "Header", "2.5", "");
            Relatorio.CriarObjLabel("Fixo", "", translateTherm(sReport.ToString(), glb_aTranslate_Proposta), "9.32834", "0.1", "11", "#0113a0", "B", "Header", "4.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", translateTherm(sReport.ToString(), glb_aTranslate_Proposta), "0.932834", "0.01", "11", "#0113a0", "B", "Header", "4.5", "");
            Relatorio.CriarObjLabelData("Now", "", "", "16.2225", "0.1", "11", "black", "B", "Header", "3.72187", "Horas");
            //Relatorio.CriarObjLabelData("Now", "", "", "1.62225", "0.01", "11", "black", "B", "Header", "3.72187", "Horas");
            Relatorio.CriarObjLinha("0.2", "0.58", "20.6", "", "Header");
            //Relatorio.CriarObjLinha("0.02", "0.058", "20.6", "", "Header");

            if (RelatorioID == 40133)
            {
                Relatorio.CriarObjLabel("Fixo", "", translateTherm("Cliente", glb_aTranslate_Proposta), "0.2", "0.2", "10", "#0113a0", "B", "Body", "8", "");
                //Relatorio.CriarObjLabel("Fixo", "", translateTherm("Cliente", glb_aTranslate_Proposta), "0.02", "0.02", "10", "#0113a0", "B", "Body", "8", "");
            }
            else
            {
                Relatorio.CriarObjLabel("Fixo", "", translateTherm("Faturar Para", glb_aTranslate_Proposta), "0.2", "0.2", "10", "#0113a0", "B", "Body", "8", "");
                //Relatorio.CriarObjLabel("Fixo", "", translateTherm("Faturar Para", glb_aTranslate_Proposta), "0.02", "0.02", "10", "#0113a0", "B", "Body", "8", "");

            }

            Relatorio.CriarObjImage("Query", "SQLImagensPessoas", "ImagemPessoa", "18.5", "0.1", "Body", "2.29104", "2.14215");
            //Relatorio.CriarObjImage("Query", "SQLImagensPessoas", "ImagemPessoa", "1.85", "0.01", "Body", "2.29104", "2.14215");
            Relatorio.CriarObjImage("Query", "SQLImagensEmpresas", "ImagemEmpresa", "0.2", "11.5", "Body", "3.09104", "2.04215");
            //Relatorio.CriarObjImage("Query", "SQLImagensEmpresas", "ImagemEmpresa", "0.02", "1.15", "Body", "3.09104", "2.04215");

           
            Relatorio.CriarObjLabel("Query", "Query11", "PessoaNome", "0.2", "0.58979", "10", " ", "Bold", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query11", "PessoaNome", "0.02", "0.058979", "10", " ", "Bold", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query11", "PessoaEndereco", "0.2", "1.03958", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query11", "PessoaEndereco", "0.02", "0.103958", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query11", "PessoaComplementoBairro", "0.2", "1.4893", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query11", "PessoaComplementoBairro", "0.02", "0.14893", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query11", "PessoaCEPCidadeUF", "0.2", "1.93916", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query11", "PessoaCEPCidadeUF", "0.02", "0.193916", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query11", "PessoaPais", "0.2", "2.33874", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query11", "PessoaPais", "0.02", "0.233874", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query11", "PessoaDocumento1", "0.2", "2.73874", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query11", "PessoaDocumento1", "0.02", "0.273874", "10", " ", "L", "Body", "8", "");

            Relatorio.CriarObjLinha("0.2", "3.23874", "20.6", "", "Body");
            //Relatorio.CriarObjLinha("0.02", "0.323874", "20.6", "", "Body");

            Relatorio.CriarObjLabel("Fixo", "", translateTherm(sReport2.ToString(), glb_aTranslate_Proposta), "0.2", "3.43874", "10", "#0113a0", "B", "Body", "5.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", translateTherm(sReport2.ToString(), glb_aTranslate_Proposta), "0.02", "0.343874", "10", "#0113a0", "B", "Body", "5.5", "");
            Relatorio.CriarObjLabel("Fixo", "", translateTherm("Validade", glb_aTranslate_Proposta), "6.6", "3.43874", "8", "", "Bold", "Body", "2.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", translateTherm("Validade", glb_aTranslate_Proposta), "0.66", "0.343874", "8", "", "Bold", "Body", "2.5", "");
            Relatorio.CriarObjLabel("Fixo", "", nValidade.ToString() + ' ' + translateTherm("dias úteis", glb_aTranslate_Proposta), "8.2", "3.43874", "8", "", "L", "Body", "2.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", nValidade.ToString() + ' ' + translateTherm("dias úteis", glb_aTranslate_Proposta), "0.82", "0.343874", "8", "", "L", "Body", "2.5", "");


            Relatorio.CriarObjTabela("0.2", "3.99874", "Query11", true, false);
            //Relatorio.CriarObjTabela("0.02", "0.399874", "Query11", true, false);

            Relatorio.CriarObjColunaNaTabela(translateTherm(sReport2, glb_aTranslate_Proposta), "PedidoID", false, "2.0");
            Relatorio.CriarObjCelulaInColuna("Query", "PedidoID", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela(translateTherm("Data", glb_aTranslate_Proposta), "dtPedido", false, "3.0");
            Relatorio.CriarObjCelulaInColuna("Query", "dtPedido", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela(translateTherm("Valor R$", glb_aTranslate_Proposta), "ValorTotalPedido", false, "2.5", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ValorTotalPedido", "DetailGroup", null, null, 0, false, "0", " <Format>n2</Format>");

            Relatorio.CriarObjColunaNaTabela(translateTherm("Valor US$", glb_aTranslate_Proposta), "ValorConvertido", false, "2.5", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ValorConvertido", "DetailGroup", null, null, 0, false, "0", " <Format>n2</Format>");

            Relatorio.CriarObjColunaNaTabela(translateTherm("Previsão Entrega", glb_aTranslate_Proposta), "dtPrevisaoEntrega", false, "4.0");
            Relatorio.CriarObjCelulaInColuna("Query", "dtPrevisaoEntrega", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela(translateTherm("Frete", glb_aTranslate_Proposta), "Frete", false, "2.0");
            Relatorio.CriarObjCelulaInColuna("Query", "Frete", "DetailGroup");

            if (RelatorioID == 40133)
            {
                Relatorio.CriarObjColunaNaTabela(translateTherm("Seu Pedido", glb_aTranslate_Proposta), "SeuPedido", false, "4.5");
                Relatorio.CriarObjCelulaInColuna("Query", "SeuPedido", "DetailGroup");
            }

            Relatorio.TabelaEnd();


            Relatorio.CriarObjLinha("0.2", "4.23874", "20.6", "", "Body");
            //Relatorio.CriarObjLinha("0.02", "0.423874", "20.6", "", "Body");

            Relatorio.CriarObjLabel("Fixo", "", translateTherm("Ítens", glb_aTranslate_Proposta), "0.2", "4.3874", "10", "#0113a0", "B", "Body", "2.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", translateTherm("Ítens", glb_aTranslate_Proposta), "0.02", "0.43874", "10", "#0113a0", "B", "Body", "2.5", "");
            // Itens
            Relatorio.CriarObjTabela("0.2", "4.87931", "Query22", true, false);
            //Relatorio.CriarObjTabela("0.02", "0.487931", "Query22", true, false);

            Relatorio.CriarObjColunaNaTabela("#", "Ordem", true, "");
            Relatorio.CriarObjCelulaInColuna("Query", "Ordem", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela(translateTherm("ID", glb_aTranslate_Proposta), "ProdutoID", true, "");
            if (bInformacoesAdicionais)
                Relatorio.CriarObjCelulaInColuna("Query", "CodigoFornecedor", "DetailGroup");
            else
                Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela(translateTherm("Produto", glb_aTranslate_Proposta), "Produto", false, "5.50");
            if (bInformacoesAdicionais)
                Relatorio.CriarObjCelulaInColuna("Query", "Modelo", "DetailGroup");
            else
                Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela(translateTherm("Quant", glb_aTranslate_Proposta), "Quantidade", false, "3.5");
            Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela(translateTherm("Unitário", glb_aTranslate_Proposta), "ValorUnitario", false, "2.50", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ValorUnitario", "DetailGroup", null, null, 0, false, "0", " <Format>n4</Format>");

            Relatorio.CriarObjColunaNaTabela(translateTherm("Total", glb_aTranslate_Proposta), "ValorTotal", false, "2.50", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup", null, null, 0, false, "0", " <Format>n4</Format>");

            if (nIdiomaParaID == 246)
            {
                if (nTransacaoID == 211 || nTransacaoID == 215)
                {
                    Relatorio.CriarObjColunaNaTabela(translateTherm("ISS", glb_aTranslate_Proposta), "ICMSISS", true, "", "Right");
                    Relatorio.CriarObjCelulaInColuna("Query", "ICMSISS", "DetailGroup");
                }
                else
                {
                    Relatorio.CriarObjColunaNaTabela(translateTherm("ICMS", glb_aTranslate_Proposta), "ICMSISS", true, "", "Right");
                    Relatorio.CriarObjCelulaInColuna("Query", "ICMSISS", "DetailGroup");
                }

                Relatorio.CriarObjColunaNaTabela(translateTherm("IPI", glb_aTranslate_Proposta), "IPI", true, "", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "IPI", "DetailGroup");
            }

            Relatorio.TabelaEnd();

            // Dados do Pagamento
            Relatorio.CriarObjLinha("0.2", "5.00000", "20.6", "", "Body");
            //Relatorio.CriarObjLinha("0.02", "0.500000", "20.6", "", "Body");
            Relatorio.CriarObjLabel("Fixo", "", translateTherm("Pagamento", glb_aTranslate_Proposta), "0.2", "5.180", "10", "#0113a0", "B", "Body", "2.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", translateTherm("Pagamento", glb_aTranslate_Proposta), "0.02", "0.5180", "10", "#0113a0", "B", "Body", "2.5", "");

            Relatorio.CriarObjTabela("0.2", "5.5800", "Query3", true, false);
            //Relatorio.CriarObjTabela("0.02", "0.55800", "Query3", true, false);

            Relatorio.CriarObjColunaNaTabela("#", "Ordem", false, "1.0");
            Relatorio.CriarObjCelulaInColuna("Query", "Ordem", "DetailGroup");

            if (bSup)
            {
                Relatorio.CriarObjColunaNaTabela(translateTherm("Tipo", glb_aTranslate_Proposta), "Tipo", false, "3.0");
                Relatorio.CriarObjCelulaInColuna("Query", "Tipo", "DetailGroup");
            }

            Relatorio.CriarObjColunaNaTabela(translateTherm("Forma", glb_aTranslate_Proposta), "Forma", false, "3.0");
            Relatorio.CriarObjCelulaInColuna("Query", "Forma", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela(translateTherm("Prazo", glb_aTranslate_Proposta), "Prazo", false, "2.0", "Center");
            Relatorio.CriarObjCelulaInColuna("Query", "Prazo", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela(translateTherm("Valor", glb_aTranslate_Proposta), "Valor", false, "3.5", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup", null, null, 0, false, "0", " <Format>n4</Format>");

            Relatorio.TabelaEnd();

            if (bFichaTecnica)
            {
                Relatorio.CriarObjLinha("0.2", "7.00000", "20.6", "", "Body");
                //Relatorio.CriarObjLinha("0.02", "0.700000", "20.6", "", "Body");
                Relatorio.CriarObjLabel("Fixo", "", translateTherm("Ficha Técnica", glb_aTranslate_Proposta), "0.2", "7.180", "10", "#0113a0", "B", "Body", "5.5", "");
                //Relatorio.CriarObjLabel("Fixo", "", translateTherm("Ficha Técnica", glb_aTranslate_Proposta), "0.02", "0.7180", "10", "#0113a0", "B", "Body", "5.5", "");

                Relatorio.CriarObjTabela("0.2", "7.7800", arrIndexKey, true, false);
                //Relatorio.CriarObjTabela("0.02", "0.77800", arrIndexKey, true, false);
             

                Relatorio.CriarObjColunaNaTabela(translateTherm("Ordem", glb_aTranslate_Proposta), "OrdemProduto", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "OrdemProduto", "DetailGroup");


                Relatorio.CriarObjColunaNaTabela(translateTherm("ID", glb_aTranslate_Proposta), "ProdutoID", true, "");
                Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID2", "DetailGroup", "null", "null");


                Relatorio.CriarObjColunaNaTabela(translateTherm("Produto", glb_aTranslate_Proposta), "Produto", false, "4");
                Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup");


                Relatorio.CriarObjColunaNaTabela(translateTherm("Descrição", glb_aTranslate_Proposta), "Descricao", false, "4");
                Relatorio.CriarObjCelulaInColuna("Query", "Descricao", "DetailGroup");


                Relatorio.CriarObjColunaNaTabela(translateTherm("Caracteristica", glb_aTranslate_Proposta), "Caracteristica", false, "4");
                Relatorio.CriarObjCelulaInColuna("Query", "Caracteristica", "DetailGroup");


                Relatorio.CriarObjColunaNaTabela(" ", "Valor", false, "4");
                Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");


                Relatorio.CriarObjColunaNaTabela(translateTherm("Part Number", glb_aTranslate_Proposta), "PartNumber", false, "2.3");
                Relatorio.CriarObjCelulaInColuna("Query", "PartNumber", "DetailGroup");


                Relatorio.TabelaEnd();
            }

            Relatorio.CriarObjLinha("0.2", "9.0000", "20.6", "", "Body");
            //Relatorio.CriarObjLinha("0.02", "0.90000", "20.6", "", "Body");
            Relatorio.CriarObjLabel("Fixo", "", translateTherm("Atenciosamente", glb_aTranslate_Proposta), "0.2", "9.180", "10", "#0113a0", "B", "Body", "5.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", translateTherm("Atenciosamente", glb_aTranslate_Proposta), "0.02", "0.9180", "10", "#0113a0", "B", "Body", "5.5", "");
            Relatorio.CriarObjLabel("Query", "Query11", "ColaboradorFantasia", "0.2", "9.989", "10", " ", "Bold", "Body", "5.54792", "");
            //Relatorio.CriarObjLabel("Query", "Query11", "ColaboradorFantasia", "0.02", "0.9989", "10", " ", "Bold", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Nome", "0.2", "10.399", "10", " ", "L", "Body", "6.64792", "");
            //Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Nome", "0.02", "1.0399", "10", " ", "L", "Body", "6.64792", "");
            Relatorio.CriarObjLabel("Query", "Query11", "ColaboradorTelefones", "0.2", "10.799", "10", " ", "L", "Body", "7.54792", "");
            //Relatorio.CriarObjLabel("Query", "Query11", "ColaboradorTelefones", "0.02", "1.0799", "10", " ", "L", "Body", "7.54792", "");

            Relatorio.CriarPDF_Excel(Title + "-" + PedidoID, Formato);

        }
        public void RelatorioPedido1()
        {
            bool bEhCliente = Convert.ToBoolean(HttpContext.Current.Request.Params["EhCliente"]); ;
            int nIdiomaParaID = Convert.ToInt32(HttpContext.Current.Request.Params["IdiomaParaID"]);
            int EmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["EmpresaID"]);
            int PedidoID = Convert.ToInt32(HttpContext.Current.Request.Params["PedidoID"]);
            string DATE_SQL_PARAM = HttpContext.Current.Request.Params["DATE_SQL_PARAM"];
            string Moeda = Convert.ToString(HttpContext.Current.Request.Params["Moeda"]);
            string MoedaConversao = Convert.ToString(HttpContext.Current.Request.Params["MoedaConversao"]);

            string strSQLSelect1 = "";
            string strSQLSelect2 = "";
            string strSQLSelect3 = "";
            string strSQLFrom = "";
            string strSQL1 = "";
            string strSQL2 = "";
            string strSQL3 = "";
            string strSQLWhere = "";
            string strSQLWhere1 = "";
            string strSQLWhere2 = "";
            string strSQLOrderBy = "";
            string strSQLEmpresa = "";

            strSQLEmpresa = "SELECT * FROM Pessoas WITH(NOLOCK) " +
                                  "WHERE PessoaID =" + EmpresaID;

            // QUERY MASTER
            strSQLSelect1 = "SELECT TOP 1 " +
                            "Pedidos.PedidoID AS PedidoIDLink, " +
                            "(RTRIM(LTRIM(STR(Pedidos.PedidoID)))) AS PedidoID, " +
                            "CONVERT(VARCHAR(90), Pedidos.dtPedido, " + DATE_SQL_PARAM + ") AS DataPedido, " +
                            "NotasFiscais.NotaFiscal AS NotaFiscal, " +
                            "CONVERT(VARCHAR(90), NotasFiscais.dtNotaFiscal, " + DATE_SQL_PARAM + ") AS DataNotaFiscal, " +
                            "Transacoes.Operacao AS NaturezaOperacao, " +
                            "Colaboradores.Fantasia AS Colaborador, " +
                            "LTRIM(Moedas.SimboloMoeda) AS MoedaPedido, " +
                            "Pedidos.ValorTotalPedido, " +
                            "LTRIM(MoedasConvertidas.SimboloMoeda) AS MoedaConversao, Pedidos.TaxaMoeda AS TaxaConversao, " +
                            "CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(Pedidos.ValorTotalPedido, Pedidos.TaxaMoeda)) AS ValorConvertido, ";

            strSQLSelect2 = "(RTRIM(LTRIM(STR(Pessoas.PessoaID)))) AS PessoaID, Pessoas.Fantasia AS PessoaFantasia, Pessoas.Nome AS PessoaNome, ";

            // Portugues
            if (nIdiomaParaID == 246)
            {
                strSQLSelect2 += "(ISNULL(PessoaEnderecos.Endereco,SPACE(0)) + SPACE(1) + " +
                   "ISNULL(PessoaEnderecos.Numero,SPACE(0))) AS PessoaEnderecos, " +
                "(ISNULL(PessoaEnderecos.CEP,SPACE(0)) + SPACE(1) + ISNULL(PessoaCidades.Localidade,SPACE(0)) + " +
                    "SPACE(1) + ISNULL(PessoaUFs.CodigoLocalidade2,SPACE(0))) AS PessoaCEPCidadeUF, " +
                "(ISNULL(EmpresaEnderecos.CEP,SPACE(0)) + SPACE(1) + ISNULL(EmpresaCidades.Localidade,SPACE(0)) + " +
                    "SPACE(1) + ISNULL(EmpresaUFs.CodigoLocalidade2,SPACE(0))) AS EmpresaCEPCidadeUF, ";
            }
            else
            {
                strSQLSelect2 += "(ISNULL(PessoaEnderecos.Numero,SPACE(0)) + SPACE(1) + " +
                    "ISNULL(PessoaEnderecos.Endereco,SPACE(0))) AS PessoaEnderecos, " +
                    "(ISNULL(PessoaCidades.Localidade,SPACE(0)) + SPACE(1) + ISNULL(PessoaUFs.CodigoLocalidade2,SPACE(0)) + SPACE(1) + " +
                        "ISNULL(PessoaEnderecos.CEP,SPACE(0))) AS PessoaCEPCidadeUF, " +
                    "(ISNULL(EmpresaCidades.Localidade,SPACE(0)) + SPACE(1) + ISNULL(EmpresaUFs.CodigoLocalidade2,SPACE(0)) + SPACE(1) + " +
                    "ISNULL(EmpresaEnderecos.CEP,SPACE(0))) AS EmpresaCEPCidadeUF, ";
            }

            strSQLSelect2 += "(ISNULL(PessoaEnderecos.Complemento + SPACE(1), SPACE(0)) + " +
                    "ISNULL(PessoaEnderecos.Bairro,SPACE(0))) AS PessoaComplementoBairro, " +
                "Empresa.PessoaID AS EmpresaID, Empresa.Nome AS EmpresaNome, Empresa.Fantasia AS EmpresaFantasia, ";

            // Portugues
            if (nIdiomaParaID == 246)
                strSQLSelect2 += "(ISNULL(EmpresaEnderecos.Endereco,SPACE(0)) + SPACE(1) + " +
                    "ISNULL(EmpresaEnderecos.Numero,SPACE(0))) AS EmpresaEnderecos, ";
            else
                strSQLSelect2 += "(ISNULL(EmpresaEnderecos.Numero,SPACE(0)) + SPACE(1) + " +
                    "ISNULL(EmpresaEnderecos.Endereco,SPACE(0))) AS EmpresaEnderecos, ";

            strSQLSelect2 += "(ISNULL(EmpresaEnderecos.Complemento + SPACE(1), SPACE(0)) + " +
                    "ISNULL(EmpresaEnderecos.Bairro,SPACE(0))) AS EmpresaComplementoBairro, " +
                "(ISNULL(dbo.fn_Pessoa_Telefone(Pessoas.PessoaID, 119, 120,1,0,NULL) + SPACE(3), SPACE(0)) + SPACE(1) + " +
                    "ISNULL(dbo.fn_Pessoa_Telefone(Pessoas.PessoaID, 122, 121,1,0,NULL), SPACE(0))) AS PessoaTelefones, " +
                "ISNULL(dbo.fn_Pessoa_Documento(Pessoas.PessoaID, NULL, NULL, 101, 111, 0) + SPACE(3), SPACE(0)) + " +
                "ISNULL(dbo.fn_Pessoa_Documento(Pessoas.PessoaID, NULL, NULL, 102, 112, 0), SPACE(0)) AS PessoaDocumento1, ";

            strSQLSelect2 += "(ISNULL(dbo.fn_Pessoa_Telefone(Empresa.PessoaID, 119, 120,1,0,NULL) + SPACE(3), SPACE(0)) + SPACE(1) + " +
                    "ISNULL(dbo.fn_Pessoa_Telefone(Empresa.PessoaID, 122, 121,1,0,NULL), SPACE(0))) AS EmpresaTelefones, " +
                "ISNULL(dbo.fn_Pessoa_Documento(Empresa.PessoaID, NULL, NULL, 101, 111, 0) + SPACE(3), SPACE(0)) + " +
                "ISNULL(dbo.fn_Pessoa_Documento(Empresa.PessoaID, NULL, NULL, 102, 112, 0), SPACE(0)) AS EmpresaDocumento1, ";

            if (bEhCliente)
            {
                strSQLSelect2 += "(SELECT TOP 1 a.Fantasia " +
                    "FROM Pessoas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), RelacoesPessoas_Contatos c WITH(NOLOCK) " +
                    "WHERE b.ObjetoID=" + EmpresaID + " AND b.SujeitoID=Pedidos.ParceiroID AND c.RelacaoID=b.RelacaoID AND " +
                        "a.PessoaID=c.ContatoID " +
                    "ORDER BY c.Ordem) AS Contato, ";
            }
            else
            {
                strSQLSelect2 += "(SELECT TOP 1 a.Fantasia " +
                    "FROM Pessoas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), RelacoesPessoas_Contatos c WITH(NOLOCK) " +
                    "WHERE b.SujeitoID=" + EmpresaID + " AND b.ObjetoID=Pedidos.ParceiroID AND c.RelacaoID=b.RelacaoID AND " +
                        "a.PessoaID=c.ContatoID " +
                    "ORDER BY c.Ordem) AS Contato, ";
            }

            strSQLSelect2 += "ISNULL((SELECT TOP 1 Pessoas_URLs.URL " +
                    "FROM Pessoas_URLs WITH(NOLOCK), RelacoesPessoas_Contatos WITH(NOLOCK) " +
                    "WHERE PessoaID=RelacoesPessoas_Contatos.ContatoID AND " +
                        "Pessoas_URLs.TipoURLID=124 AND Pessoas_URLs.Ordem=1 AND RelacoesPessoas_Contatos.RelacaoID=" +
                        "RelPessoas.RelacaoID)," +
                        "(SELECT URL " +
                        "FROM Pessoas_URLs WITH(NOLOCK) " +
                        "WHERE PessoaID=Pedidos.PessoaID AND TipoURLID=124 AND Ordem=1))  AS ContatoEmail, ";

            strSQLSelect3 =
                 "(LTRIM(STR(ROUND(Pedidos.Variacao,2),5,2)) + CHAR(37)) AS Variacao, " +
                 "Pedidos.SeuPedido AS SeuPedido, " +
                 "Pedidos.Observacao AS Observacao, " +
                 "(CASE Pedidos.TransportadoraID WHEN Pedidos.ParceiroID THEN " + "'" + "O mesmo" + "'" + " " +
                     "WHEN Pedidos.EmpresaID THEN " + "'" + "Nosso carro" + "'" + " " +
                     "ELSE " +
                     "(SELECT Fantasia " +
                         "FROM Pessoas WITH(NOLOCK) " +
                         "WHERE PessoaID=Pedidos.TransportadoraID) " +
                 "END) AS Transportadora, " +
                 "(LTRIM(STR(ROUND(Pedidos.TotalPesoBruto,2),7,4)) + CHAR(107) + CHAR(103)) AS TotalPesoBruto, " +
                 "(LTRIM(STR(ROUND(Pedidos.TotalPesoLiquido,2),7,4)) + CHAR(107) + CHAR(103)) AS TotalPesoLiquido, " +
                 "Pedidos.NumeroVolumes AS NumeroVolumes, " +
                 "Pedidos.dtPrevisaoEntrega AS DataPrevisaoEntrega, " +
                 "Pedidos.Conhecimento AS Conhecimento, " +
                 "Pedidos.Portador AS Portador, " +
                 "Pedidos.DocumentoPortador AS PortadorDocumento, " +
                 "Pedidos.PlacaVeiculo AS PlacaVeiculo, " +
                 "Pedidos.Observacoes AS Observacoes ";

            strSQLFrom = "FROM " +
                "Pedidos WITH(NOLOCK) " +
                "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                "INNER JOIN Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID=Pessoas.PessoaID) " +
                "INNER JOIN Pessoas_Enderecos PessoaEnderecos WITH(NOLOCK) ON (Pessoas.PessoaID=PessoaEnderecos.PessoaID) " +
                "LEFT OUTER JOIN Localidades PessoaCidades WITH(NOLOCK) ON (PessoaEnderecos.CidadeID=PessoaCidades.LocalidadeID) " +
                "LEFT OUTER JOIN Localidades PessoaUFs WITH(NOLOCK) ON (PessoaEnderecos.UFID=PessoaUFs.LocalidadeID) " +
                "INNER JOIN RelacoesPessoas RelPessoas WITH(NOLOCK) ON (" +
                (bEhCliente ? "Pedidos.ParceiroID=RelPessoas.SujeitoID AND RelPessoas.ObjetoID = Pedidos.EmpresaID" : "Pedidos.ParceiroID=RelPessoas.ObjetoID AND RelPessoas.SujeitoID = Pedidos.EmpresaID") + ") " +
                "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID=Transacoes.OperacaoID) " +
                "INNER JOIN Pessoas Colaboradores WITH(NOLOCK) ON (Pedidos.ProprietarioID=Colaboradores.PessoaID) " +
                "INNER JOIN Conceitos Moedas WITH(NOLOCK) ON (Pedidos.MoedaID=Moedas.ConceitoID) " +
                "INNER JOIN Pessoas Empresa WITH(NOLOCK) ON (Pedidos.EmpresaID=Empresa.PessoaID) " +
                "INNER JOIN Pessoas_Enderecos EmpresaEnderecos WITH(NOLOCK) ON (Empresa.PessoaID=EmpresaEnderecos.PessoaID) " +
                "LEFT OUTER JOIN Localidades EmpresaCidades WITH(NOLOCK) ON (EmpresaEnderecos.CidadeID=EmpresaCidades.LocalidadeID) " +
                "LEFT OUTER JOIN Localidades EmpresaUFs WITH(NOLOCK) ON (EmpresaEnderecos.UFID=EmpresaUFs.LocalidadeID), " +
                "Recursos Sistema WITH(NOLOCK) " +
                "INNER JOIN Conceitos MoedasConvertidas WITH(NOLOCK) ON (Sistema.MoedaID=MoedasConvertidas.ConceitoID) ";

            strSQLWhere1 = "WHERE (" +
                "Pedidos.PedidoID= " + PedidoID + " AND RelPessoas.TipoRelacaoID=21 AND PessoaEnderecos.Ordem=1 ";

            strSQLWhere2 = " AND EmpresaEnderecos.Ordem=1 AND Sistema.RecursoID=999)  ";

            strSQL1 = strSQLSelect1 + strSQLSelect2 + strSQLSelect3 + strSQLFrom + strSQLWhere1 + strSQLWhere2;


            // QUERY DETAIL 1
            strSQLSelect1 = "SELECT " +
                    "Itens.PedidoID, Itens.Ordem AS Ordem, (RTRIM(LTRIM(STR(Itens.ProdutoID)))) AS ProdutoID, " +
                    "Produtos.Conceito AS Produto, Produtos.Modelo AS Modelo, Produtos.PartNumber, CONVERT(INT, Itens.Quantidade) AS Quantidade, " +
                    "Itens.ValorUnitario AS ValorUnitario, (Itens.ValorUnitario*Itens.Quantidade) AS ValorTotal, " +
                    "(SELECT CONVERT(VARCHAR(6), CONVERT(NUMERIC(5,2), ISNULL(SUM(ItensImpostos.Aliquota),0))) + CHAR(37) " +
                        "FROM Pedidos_Itens_Impostos ItensImpostos WITH(NOLOCK) " +
                                " INNER JOIN Conceitos Impostos WITH(NOLOCK) ON ItensImpostos.ImpostoID = Impostos.ConceitoID " +
                        "WHERE (Itens.PedItemID = ItensImpostos.PedItemID " +
                                "AND Impostos.IncidePreco = 1 " +
                                "AND Impostos.InclusoPreco = 1 " +
                                "AND Impostos.AlteraAliquota =1)) AS ICMSISS, ";
            strSQLSelect2 =
                "(SELECT SUM(ItensImpostos.ValorImposto) " +
                    "FROM Pedidos_Itens_Impostos ItensImpostos WITH(NOLOCK) " +
                            "INNER JOIN Conceitos Impostos WITH(NOLOCK) ON ItensImpostos.ImpostoID=Impostos.ConceitoID " +
                    "WHERE (Itens.PedItemID=ItensImpostos.PedItemID " +
                                "AND Impostos.IncidePreco=1 " +
                                "AND Impostos.InclusoPreco=0 " +
                                "AND Impostos.DestacaNota=1)) AS Impostos ";

            strSQLFrom = "FROM " +
                "Pedidos_Itens Itens WITH(NOLOCK) , Conceitos Produtos WITH(NOLOCK) ";

            strSQLWhere = "WHERE " +
                "(Itens.PedidoID= " + PedidoID + " AND Itens.ProdutoID=Produtos.ConceitoID) ";

            strSQLOrderBy = "ORDER BY Itens.Ordem";

            // QUERY DETAIL 2
            strSQL2 = strSQLSelect1 + strSQLSelect2 + strSQLFrom + strSQLWhere + strSQLOrderBy;

            strSQL3 = "SELECT " +
                    "Parcelas.PedidoID, Parcelas.Ordem, Financeiros.FinanceiroID, Financeiros.dtVencimento, " +
                    "Tipo = ISNULL(HP.HistoricoAbreviado, SPACE(0)), " +
                    "FormasPagamentos.ItemAbreviado AS FormaPagamento, " +
                    "Parcelas.PrazoPagamento, CONVERT(VARCHAR(90), Financeiros.dtVencimento," + DATE_SQL_PARAM + ") AS Vencimento, Parcelas.ValorParcela, " +
                    "Parcelas.Observacao " +
                "FROM Pedidos_Parcelas Parcelas WITH(NOLOCK) " +
                    "INNER JOIN TiposAuxiliares_Itens FormasPagamentos WITH(NOLOCK) ON (Parcelas.FormaPagamentoID=FormasPagamentos.ItemID) " +
                    "LEFT OUTER JOIN Financeiro Financeiros WITH(NOLOCK) ON (Parcelas.FinanceiroID=Financeiros.FinanceiroID) " +
                    "LEFT OUTER JOIN HistoricosPadrao HP WITH(NOLOCK) ON (Parcelas.HistoricoPadraoID = HP.HistoricoPadraoID) " +
                "WHERE (Parcelas.PedidoID= " + PedidoID + ") " +
                "ORDER BY Parcelas.Ordem";

            string strObservacoes = "SELECT REPLACE(SUBSTRING(Observacoes,0,5799),'&',SPACE(0)) as Observacoes FROM Pedidos WITH (NOLOCK)" +
                                    "WHERE PedidoID = " + PedidoID;

            arrSqlQuery = new string[,] {
                                        {"Query1", strSQL1},
                                        {"Query2", strSQL2},
                                        {"strSQLEmpresa", strSQLEmpresa},
                                        {"Query3", strSQL3},
                                        {"QueryObservacoes", strObservacoes}
                                      };

            Relatorio.CriarRelatório("RelPedido", arrSqlQuery, "BRA", "Portrait", "0.60");

            //Labels Cabeçalho
            Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.2", "0.1", "11", "black", "B", "Header", "2.5", "");
            //Relatorio.CriarObjLabel("Query", "strSQLEmpresa", "Fantasia", "0.02", "0.01", "11", "black", "B", "Header", "2.5", "");
            Relatorio.CriarObjLabel("Fixo", "", "Pedido", "9.32834", "0.1", "11", "#0113a0", "B", "Header", "2.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Pedido", "0.932834", "0.01", "11", "#0113a0", "B", "Header", "2.5", "");
            Relatorio.CriarObjLabelData("Now", "", "", "16.2225", "0.1", "11", "black", "B", "Header", "3.72187", "Horas");
            //Relatorio.CriarObjLabelData("Now", "", "", "1.62225", "0.01", "11", "black", "B", "Header", "3.72187", "Horas");
            Relatorio.CriarObjLinha("0.2", "0.58", "20.6", "", "Header");
            //Relatorio.CriarObjLinha("0.02", "0.058", "20.6", "", "Header");
            //Labels 
            Relatorio.CriarObjLabel("Fixo", "", "Pessoa", "0.2", "0.04", "10", "#0113a0", "B", "Body", "8", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Pessoa", "0.02", "0.004", "10", "#0113a0", "B", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaID", "0.2", "0.48979", "10", " ", "L", "Body", "2.45208", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "PessoaID", "0.02", "0.048979", "10", " ", "L", "Body", "2.45208", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaFantasia", "2.56958", "0.45451", "10", " ", "L", "Body", "5.54792", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "PessoaFantasia", "0.256958", "0.045451", "10", " ", "L", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaNome", "0.2", "0.93958", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "PessoaNome", "0.02", "0.093958", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaEnderecos", "0.2", "1.38937", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "PessoaEnderecos", "0.02", "0.138937", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaComplementoBairro", "0.2", "1.83916", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "PessoaComplementoBairro", "0.02", "0.183916", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaCEPCidadeUF", "0.2", "2.28895", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "PessoaCEPCidadeUF", "0.02", "0.228895", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaDocumento1", "0.2", "2.73874", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "PessoaDocumento1", "0.02", "0.273874", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "Contato", "0.2", "3.18854", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "Contato", "0.02", "0.318854", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaTelefones", "0.2", "3.63833", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "PessoaTelefones", "0.02", "0.363833", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "ContatoEmail", "0.2", "4.08812", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "ContatoEmail", "0.02", "0.408812", "10", " ", "L", "Body", "8", "");

            Relatorio.CriarObjLabel("Fixo", "", "Empresa", "9.32833", "0.04", "10", "#0113a0", "B", "Body", "2.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Empresa", "0.932833", "0.004", "10", "#0113a0", "B", "Body", "2.5", "");
            Relatorio.CriarObjLabel("Query", "Query1", "EmpresaID", "9.32833", "0.48979", "10", " ", "L", "Body", "1.89917", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "EmpresaID", "0.932833", "0.048979", "10", " ", "L", "Body", "1.89917", "");
            Relatorio.CriarObjLabel("Query", "Query1", "EmpresaFantasia", "11.40389", "0.48979", "10", " ", "L", "Body", "5.92444", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "EmpresaFantasia", "1.140389", "0.048979", "10", " ", "L", "Body", "5.92444", "");
            Relatorio.CriarObjLabel("Query", "Query1", "EmpresaNome", "9.32833", "0.93957", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "EmpresaNome", "0.932833", "0.093957", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "EmpresaEnderecos", "9.32833", "1.38937", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "EmpresaEnderecos", "0.932833", "0.138937", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "EmpresaComplementoBairro", "9.32833", "1.83915", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "EmpresaComplementoBairro", "0.932833", "0.183915", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "EmpresaCEPCidadeUF", "9.32833", "2.28894", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "EmpresaCEPCidadeUF", "0.932833", "0.228894", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "EmpresaDocumento1", "9.32833", "2.73873", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "EmpresaDocumento1", "0.932833", "0.273873", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "EmpresaTelefones", "9.32833", "3.18852", "10", " ", "L", "Body", "8", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "EmpresaTelefones", "0.932833", "0.318852", "10", " ", "L", "Body", "8", "");
            // pedidos
            Relatorio.CriarObjLinha("0.2", "4.82523", "20.6", "", "Body");
            //Relatorio.CriarObjLinha("0.02", "0.482523", "20.6", "", "Body");

            Relatorio.CriarObjLabel("Fixo", "", "Pedido", "0.2", "4.89064", "10", "#0113a0", "B", "Body", "2.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Pedido", "0.02", "0.489064", "10", "#0113a0", "B", "Body", "2.5", "");

            Relatorio.CriarObjTabela("0.2", "5.51682", "Query1", true);
            //Relatorio.CriarObjTabela("0.02", "0.551682", "Query1", true);

            Relatorio.CriarObjColunaNaTabela("Pedido", "PedidoID", false, "2.0");
            Relatorio.CriarObjCelulaInColuna("Query", "PedidoID", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Data", "DataPedido", false, "2.5");
            Relatorio.CriarObjCelulaInColuna("Query", "DataPedido", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Nota", "NotaFiscal", false, "2.0");
            Relatorio.CriarObjCelulaInColuna("Query", "NotaFiscal", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Data", "DataNotaFiscal", false, "2.0");
            Relatorio.CriarObjCelulaInColuna("Query", "DataNotaFiscal", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("CFOP", "NaturezaOperacao", false, "4.0");
            Relatorio.CriarObjCelulaInColuna("Query", "NaturezaOperacao", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Colaborador", "Colaborador", false, "3.5");
            Relatorio.CriarObjCelulaInColuna("Query", "Colaborador", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Valor " + Moeda, "ValorTotalPedido", false, "2.3", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ValorTotalPedido", "DetailGroup", null, null, 0, false, "0", " <Format>n2</Format>");

            Relatorio.CriarObjColunaNaTabela("Valor " + MoedaConversao, "ValorConvertido", false, "2.1", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ValorConvertido", "DetailGroup", null, null, 0, false, "0", " <Format>n2</Format>");

            Relatorio.TabelaEnd();

            Relatorio.CriarObjLinha("0.2", "7.02523", "20.6", "", "Body");
            //Relatorio.CriarObjLinha("0.02", "0.702523", "20.6", "", "Body");

            Relatorio.CriarObjLabel("Fixo", "", "Transporte", "0.2", "7.09577", "10", "#0113a0", "B", "Body", "2.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Transporte", "0.02", "0.709577", "10", "#0113a0", "B", "Body", "2.5", "");
            Relatorio.CriarObjLabel("Fixo", "", "Transportadora:", "0.2", "7.66883", "10", " ", "L", "Body", "2.62361", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Transportadora:", "0.02", "0.766883", "10", " ", "L", "Body", "2.62361", "");
            Relatorio.CriarObjLabel("Fixo", "", "Peso Bruto:", "0.2", "8.11863", "10", " ", "L", "Body", "2.62361", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Peso Bruto:", "0.02", "0.811863", "10", " ", "L", "Body", "2.62361", "");
            Relatorio.CriarObjLabel("Fixo", "", "Peso Líquido:", "0.2", "8.56842", "10", " ", "L", "Body", "2.62361", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Peso Líquido:", "0.02", "0.856842", "10", " ", "L", "Body", "2.62361", "");
            Relatorio.CriarObjLabel("Fixo", "", "Volumes:", "0.2", "9.05349", "10", " ", "L", "Body", "2.62361", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Volumes:", "0.02", "0.905349", "10", " ", "L", "Body", "2.62361", "");

            Relatorio.CriarObjLabel("Query", "Query1", "Transportadora", "3", "7.66883", "10", " ", "L", "Body", "3", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "Transportadora", "0.3", "0.766883", "10", " ", "L", "Body", "3", "");
            Relatorio.CriarObjLabel("Query", "Query1", "TotalPesoBruto", "3", "8.11863", "10", " ", "L", "Body", "3", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "TotalPesoBruto", "0.3", "0.811863", "10", " ", "L", "Body", "3", "");
            Relatorio.CriarObjLabel("Query", "Query1", "TotalPesoLiquido", "3", "8.56842", "10", " ", "L", "Body", "3", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "TotalPesoLiquido", "0.3", "0.856842", "10", " ", "L", "Body", "3", "");
            Relatorio.CriarObjLabel("Query", "Query1", "NumeroVolumes", "3", "9.05349", "10", " ", "L", "Body", "3.5", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "NumeroVolumes", "0.3", "0.905349", "10", " ", "L", "Body", "3.5", "");

            Relatorio.CriarObjLabel("Fixo", "", "Despacho", "9.32834", "7.09577", "10", "#0113a0", "B", "Body", "2.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Despacho", "0.932834", "0.709577", "10", "#0113a0", "B", "Body", "2.5", "");
            Relatorio.CriarObjLabel("Fixo", "", "Previsão de Entrega:", "9.32834", "7.66883", "10", " ", "L", "Body", "3.35457", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Previsão de Entrega:", "0.932834", "0.766883", "10", " ", "L", "Body", "3.35457", "");
            Relatorio.CriarObjLabel("Fixo", "", "Conhecimento:", "9.32834", "8.11863", "10", " ", "L", "Body", "3", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Conhecimento:", "0.932834", "0.811863", "10", " ", "L", "Body", "3", "");
            Relatorio.CriarObjLabel("Fixo", "", "Portador:", "9.32834", "8.56842", "10", " ", "L", "Body", "3", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Portador:", "0.932834", "0.856842", "10", " ", "L", "Body", "3", "");
            Relatorio.CriarObjLabel("Fixo", "", "Documento:", "9.32834", "9.05349", "10", " ", "L", "Body", "3", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Documento:", "0.932834", "0.905349", "10", " ", "L", "Body", "3", "");
            Relatorio.CriarObjLabel("Fixo", "", "Placa do Veiculo:", "9.32834", "9.468", "10", " ", "L", "Body", "3", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Placa do Veiculo:", "0.932834", "0.9468", "10", " ", "L", "Body", "3", "");

            Relatorio.CriarObjLabelData("Query", "Query1", "DataPrevisaoEntrega", "13.06215", "7.66883", "10", " ", "L", "Body", "5", "Horas");
            //Relatorio.CriarObjLabelData("Query", "Query1", "DataPrevisaoEntrega", "1.306215", "0.766883", "10", " ", "L", "Body", "5", "Horas");
            Relatorio.CriarObjLabel("Query", "Query1", "Conhecimento", "13.06215", "8.11863", "10", " ", "L", "Body", "3", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "Conhecimento", "1.306215", "0.811863", "10", " ", "L", "Body", "3", "");
            Relatorio.CriarObjLabel("Query", "Query1", "Portador", "13.06215", "8.56842", "10", " ", "L", "Body", "3.5", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "Portador", "1.306215", "0.856842", "10", " ", "L", "Body", "3.5", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PortadorDocumento", "13.06215", "9.05349", "10", " ", "L", "Body", "3", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "PortadorDocumento", "1.306215", "0.905349", "10", " ", "L", "Body", "3", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PlacaVeiculo", "13.06215", "9.50015", "10", " ", "L", "Body", "3", "");
            //Relatorio.CriarObjLabel("Query", "Query1", "PlacaVeiculo", "1.306215", "0.950015", "10", " ", "L", "Body", "3", "");

            Relatorio.CriarObjLinha("0.2", "10.06203", "20.6", "", "Body");
            //Relatorio.CriarObjLinha("0.02", "1.006203", "20.6", "", "Body");
            Relatorio.CriarObjLabel("Fixo", "", "Ítens", "0.2", "10.24496", "10", "#0113a0", "B", "Body", "2.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Ítens", "0.02", "1.024496", "10", "#0113a0", "B", "Body", "2.5", "");

            // Itens
            Relatorio.CriarObjTabela("0.2", "10.87931", "Query2", true);
            //Relatorio.CriarObjTabela("0.02", "1.087931", "Query2", true);
            Relatorio.CriarObjColunaNaTabela("#", "Ordem", true, "");
            Relatorio.CriarObjCelulaInColuna("Query", "Ordem", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("ID", "ProdutoID", true, "");
            Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Produto", "Produto", false, "4.50");
            Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Modelo", "Modelo", false, "3.5");
            Relatorio.CriarObjCelulaInColuna("Query", "Modelo", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Part Number", "PartNumber", false, "3.5");
            Relatorio.CriarObjCelulaInColuna("Query", "PartNumber", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Quant", "Quantidade", true, "");
            Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Unitário", "ValorUnitario", false, "2.50", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ValorUnitario", "DetailGroup", null, null, 0, false, "0", " <Format>n4</Format>");

            Relatorio.CriarObjColunaNaTabela("Total", "ValorTotal", false, "2.50", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup", null, null, 0, false, "0", " <Format>n4</Format>");

            Relatorio.CriarObjColunaNaTabela("ICMS", "ICMSISS", true, "", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ICMSISS", "DetailGroup");

            Relatorio.TabelaEnd();


            // Parcelas
            Relatorio.CriarObjLinha("0.2", "11.00000", "20.6", "", "Body");
            //Relatorio.CriarObjLinha("0.02", "1.100000", "20.6", "", "Body");
            Relatorio.CriarObjLabel("Fixo", "", "Parcelas", "0.2", "11.200000", "10", "#0113a0", "B", "Body", "2.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Parcelas", "0.02", "1.1200000", "10", "#0113a0", "B", "Body", "2.5", "");

            Relatorio.CriarObjTabela("0.2", "11.80000", "Query3", true);
            //Relatorio.CriarObjTabela("0.02", "1.180000", "Query3", true);

            Relatorio.CriarObjColunaNaTabela("#", "Ordem", false, "1.0");
            Relatorio.CriarObjCelulaInColuna("Query", "Ordem", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Fin", "FinanceiroID", false, "3.0");
            Relatorio.CriarObjCelulaInColuna("Query", "FinanceiroID", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Tipo", "Tipo", false, "3.0");
            Relatorio.CriarObjCelulaInColuna("Query", "Tipo", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Forma", "FormaPagamento", false, "3.0");
            Relatorio.CriarObjCelulaInColuna("Query", "FormaPagamento", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Prazo", "PrazoPagamento", false, "2.0", "Center");
            Relatorio.CriarObjCelulaInColuna("Query", "PrazoPagamento", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Vencimento", "Vencimento", false, "2.0");
            Relatorio.CriarObjCelulaInColuna("Query", "Vencimento", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Valor", "ValorParcela", false, "3.5", "Right");
            Relatorio.CriarObjCelulaInColuna("Query", "ValorParcela", "DetailGroup", null, null, 0, false, "0", " <Format>n4</Format>");

            Relatorio.CriarObjColunaNaTabela("Observação", "Observacao", false, "3.00", "Center");
            Relatorio.CriarObjCelulaInColuna("Query", "Observacao", "DetailGroup");

            Relatorio.TabelaEnd();

            Relatorio.CriarObjLinha("0.2", "12.06706", "20.6", "", "Body");
            //Relatorio.CriarObjLinha("0.02", "1.206706", "20.6", "", "Body");
            Relatorio.CriarObjLabel("Fixo", "", "Observações", "0.2", "12.06762", "10", "#0113a0", "B", "Body", "2.5", "");
            //Relatorio.CriarObjLabel("Fixo", "", "Observações", "0.02", "1.206762", "10", "#0113a0", "B", "Body", "2.5", "");
            Relatorio.CriarObjLabel("Query", "QueryObservacoes", "Observacoes", "0.2", "13.05796", "10", " ", "L", "Body", "20.5", "");
            //Relatorio.CriarObjLabel("Query", "QueryObservacoes", "Observacoes", "0.02", "1.305796", "10", " ", "L", "Body", "20.5", "");

            //Exporta 
            Relatorio.CriarPDF_Excel("RelPedido-" + PedidoID, 1);
        }


        public void SolicitDeComp()
        {

            string query1 = Request.Form["query1"].ToString();
            string query2 = Request.Form["query2"].ToString();
            string query3 = Request.Form["query3"].ToString();

            arrSqlQuery = new string[,] { { "Query1", query1 }, { "Query2", query2 }, { "Query3", query3 } };

            //Cria Relatorio
            Relatorio.CriarRelatório("SolicitDeComp", arrSqlQuery, "BRA", "Portrait");
            //Labels Cabeçalho
            Relatorio.CriarObjLabel("Fixo", "", "Alcateia SP", "0.02", "0", "11", "black", "B", "Header", "2.5", "");
            Relatorio.CriarObjLabel("Fixo", "", "Solicitação de Compra", "0.774084", "0", "11", "#0113a0", "B", "Header", "7.55355", "");
            Relatorio.CriarObjLabelData("Now", "", "", "1.62225", "0", "11", "black", "B", "Header", "3.72187", "Horas");
            Relatorio.CriarObjLinha("0.01", "0.047625", "20.6", "", "Header");
            //Body
            Relatorio.CriarObjImage("Query", "Query1", "FotoPessoa", "", "", "Body", "", "");
            Relatorio.CriarObjLabel("Fixo", "", "Faturar Para", "0.02", "0.123062", "10", "#0113a0", "B", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaNome", "0.02", "0.175097", "10", " ", "B", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaEndereco", "0.02", "0.220076", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaComplementoBairro", "0.02", "0.265056", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaCEPCidadeUF", "0.02", "0.310035", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaPais", "0.02", "0.355014", "10", " ", "L", "Body", "8", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaDocumento1", "0.02", "0.399993", "10", " ", "L", "Body", "8", "");

            Relatorio.CriarObjLabel("Fixo", "", "Solicitação de Compra", "0.02", "0.489069", "10", "#0113a0", "B", "Body", "8", "");

            Relatorio.CriarObjLabel("Fixo", "", "Solicitação", "0.02", "0.534049", "10", " ", "B", "Body", "2.45208", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PedidoID", "0.02", "0.579028", "10", " ", "L", "Body", "2.45208", "");

            Relatorio.CriarObjLabel("Fixo", "", "Data", "0.303417", "0.534049", "10", " ", "B", "Body", "2.45208", "");
            Relatorio.CriarObjLabelData("Query", "Query1", "dtPedido", "0.303417", "0.579028", "10", " ", "L", "Body", "2.45208", "");

            Relatorio.CriarObjLabel("Fixo", "", "Valor R$", "0.610021", "0.534049", "10", " ", "B", "Body", "2.45208", "");
            Relatorio.CriarObjLabel("Query", "Query1", "ValorTotalPedido", "0.610021", "0.579028", "10", " ", "L", "Body", "2.45208", "");

            Relatorio.CriarObjLabel("Fixo", "", "Valor US$", "0.893125", "0.534049", "10", " ", "B", "Body", "2.45208", "");
            Relatorio.CriarObjLabel("Query", "Query1", "ValorConvertido", "0.893125", "0.579028", "10", " ", "L", "Body", "2.45208", "");

            Relatorio.CriarObjLabel("Fixo", "", "Previsão Entrega", "1.171221", "0.534049", "10", " ", "B", "Body", "2.9164", "");
            Relatorio.CriarObjLabel("Query", "Query1", "dtPrevisaoEntrega", "1.171221", "0.579028", "10", " ", "L", "Body", "2.45208", "");

            Relatorio.CriarObjLabel("Fixo", "", "Validade", "1.4805", "0.534049", "10", " ", "B", "Body", "2.45208", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaID", "1.4805", "0.579028", "10", " ", "L", "Body", "2.45208", "");

            Relatorio.CriarObjLabel("Fixo", "", "Frete", "1.76625", "0.534049", "10", " ", "B", "Body", "2.45208", "");
            Relatorio.CriarObjLabel("Query", "Query1", "Frete", "1.76625", "0.579028", "10", " ", "L", "Body", "2.45208", "");

            Relatorio.CriarObjLabel("Fixo", "", "Ítens", "0.02", "0.641646", "10", "#0113a0", "B", "Body", "8", "");
            Relatorio.CriarObjLinha("0.01", "0.690563", "20.6", "", "Body");
            //Relatorio.CriarObjTabela("Query2", "0.02", "0.708202");

            Relatorio.CriarObjLinha("0.01", "0.810509", "20.6", "", "Body");
            Relatorio.CriarObjLabel("Fixo", "", "Pagamento", "0.02", "0.832146", "10", "#0113a0", "B", "Body", "8", "");
            //Relatorio.CriarObjTabela("Query3", "0.02", "0.897416");
            Relatorio.CriarObjLinha("0.01", "1.001009", "20.6", "", "Body");

            Relatorio.CriarObjLabel("Fixo", "", "Atenciosamente, ", "0.02", "1.018648", "10", " ", "L", "Body", "3.53687", "");

            Relatorio.CriarObjLabel("Query", "Query1", "ColaboradorFantasia", "0.02", "1.110406", "10", " ", "L", "Body", "6.27687", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PessoaNome", "0.02", "1.155385", "10", " ", "L", "Body", "6.27687", "");
            Relatorio.CriarObjLabel("Query", "Query1", "ColaboradorTelefones", "0.02", "1.200364", "10", " ", "L", "Body", "6.27687", "");

            //Relatorio.CriarObjImage("Query", "Query1", "FotoPessoa", "", "", "Body");
        }

        public void TotalizaVendas()
        {
            glb_aKey = new object[,]
            {
                {"", "DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)),", 1, "Ano"},  /* ANO */
                {"", "Enderecos.Bairro + SPACE(1) + Cidades.Localidade,", 1, "Bairro"},  /*  BAIRRO */
                {"", "CFOPs.OperacaoID,", 0, null},  /* CFOP */
                {"", "Enderecos.CidadeID,", 0, null},  /* CIDADE */
                {"", "Parceiros.ClassificacaoID,", 0, null},  /* CLASSIFICACAO */
                {"", "Parceiros.PessoaID,", 0, null},  /* CLIENTE */
                {"", "Pessoas.PessoaID,", 0, null},  /* PESSOA */
                {"", "dbo.fn_Data_Zero(Itens.dtMovimento),", 1, "Dia"},  /* DIA */
                {"", "Pedidos.AlternativoID,", 0, null},  /* EQUIPE */
                {"", "Enderecos.UFID,", 0, null},  /* ESTADO */
                {"", "Familias.ConceitoID,", 2, "Familia"},  /* FAMILIA */
                {"", "RelPessoas.ListaPreco,", 0, null},  /* LISTA */
                {"", "Marcas.ConceitoID,", 2, "Marca"},  /* MARCA */
                {"", "GPs.PessoaID,", 0, null},  /* GER PRODUTOS */
                {"", "LinhasProduto.ConceitoID,", 0, null},  /* LINHAS DE PRODUTO */
                {"", "LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + '/' + dbo.fn_Pad(LTRIM(STR(DATEPART(mm, dbo.fn_Data_Zero(Itens.dtMovimento)))),2,CHAR(48),'L'),", 1, "Mes"},  /* MES */
                {"", "Enderecos.PaisID,", 0, null},  /* PAIS */
                {"", "Produtos.ConceitoID,", 2, "Produto"},  /* PRODUTO */
                {"", "LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + '/' + LTRIM(STR(DATEPART(qq, dbo.fn_Data_Zero(Itens.dtMovimento)))),", 1, "Quarter"},  /* QUARTER */
                {"", "Estados.TipoRegiaoID,", 0, null},  /* REGIAO */
                {"", "LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + '/' + LTRIM(STR(DATEPART(wk, dbo.fn_Data_Zero(Itens.dtMovimento)))),", 1, "Semana"},  /* SEMANA */
                {"", "Transacoes.OperacaoID,", 0, null},  /* TRANSACAO */
                {"", "Pedidos.ProprietarioID,", 0, null},  /* VENDEDOR */
                {"", "Pedidos.OrigemPedidoID,", 0, null},  /* ORIGEM */
                {"", "Pedidos.ProgramaMarketingID,", 0, null}  /* PROGRAMA MARKETING */
            };

            glb_aFields = new object[]
            {
                "NULL AS ID, DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)) AS Nome,",  /* ANO */
                "NULL AS ID, Enderecos.Bairro + SPACE(1) + Cidades.Localidade AS Nome,",  /* BAIRRO */
                "CFOPs.OperacaoID AS ID, LEFT(CFOPs.Operacao,33) AS Nome,",  /* CFOP */
                "Enderecos.CidadeID AS ID, Cidades.Localidade + SPACE(1) + Estados.CodigoLocalidade2 AS Nome,",  /* CIDADE */
                "Parceiros.ClassificacaoID AS ID, Classificacoes.ItemMasculino AS Nome,",  /* CLASSIFICACAO */
                "Parceiros.PessoaID AS ID, Parceiros.Fantasia AS Nome,",  /* CLIENTE */
                "Pessoas.PessoaID AS ID, Pessoas.Fantasia AS Nome,",  /* PESSOA */
                "NULL AS ID, dbo.fn_Data_Zero(Itens.dtMovimento) AS Nome,",  /* DIA */
                "Pedidos.AlternativoID AS ID, Equipes.Fantasia AS Nome,",  /* EQUIPE */
                "Enderecos.UFID AS ID, Estados.Localidade + SPACE(1) + Paises.CodigoLocalidade2 AS Nome,",  /* ESTADO */
                "Familias.ConceitoID AS ID, " + (glb_nEmpresaID == 7 ? "dbo.fn_Tradutor(Familias.Conceito,246,245,NULL)" : "Familias.Conceito") + " AS Nome,",  /* FAMILIA */
                "RelPessoas.ListaPreco AS ID, RelPessoas.ListaPreco AS Nome,",  /* LISTA */
                "Marcas.ConceitoID AS ID, Marcas.Conceito AS Nome,",  /* MARCA */
                "GPs.PessoaID AS ID, GPs.Fantasia AS Nome,",  /* GER PRODUTOS */
                "LinhasProduto.ConceitoID AS ID, LinhasProduto.Conceito AS Nome,",  /* LINHAS DE PRODUTO */
                "NULL AS ID, LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + '/' + dbo.fn_Pad(LTRIM(STR(DATEPART(mm, dbo.fn_Data_Zero(Itens.dtMovimento)))),2,CHAR(48), 'L') AS Nome,",  /* MES */
                "Enderecos.PaisID AS ID, Paises.Localidade AS Nome,",  /* PAIS */
                "Produtos.ConceitoID AS ID, Produtos.Conceito AS Nome,",  /* PRODUTO */
                "NULL AS ID, LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + '/' + LTRIM(STR(DATEPART(qq, dbo.fn_Data_Zero(Itens.dtMovimento)))) AS Nome,",  /* QUARTER */
                "Estados.TipoRegiaoID AS ID, Regioes.ItemMasculino + SPACE(1) + Paises.CodigoLocalidade2 AS Nome,",  /* REGIAO */
                "NULL AS ID, LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + '/' + LTRIM(STR(DATEPART(wk, dbo.fn_Data_Zero(Itens.dtMovimento)))) AS Nome,",  /* SEMANA */
                "Transacoes.OperacaoID AS ID, Transacoes.Operacao AS Nome,",  /* TRANSACAO */
                "Pedidos.ProprietarioID AS ID, Vendedores.Fantasia AS Nome,",  /* VENDEDOR */
                "Pedidos.OrigemPedidoID AS ID, dbo.fn_TipoAuxiliar_Item(Pedidos.OrigemPedidoID, NULL) AS Nome,",  /* ORIGEM */
                "Pedidos.ProgramaMarketingID AS ID, dbo.fn_Pessoa_Fantasia(Pedidos.ProgramaMarketingID, 0) AS Nome,"  /* PROGRAMA MARKETING */
            };

            glb_aGroupBy = new object[]
            {
                "DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)) ",  /* ANO */
                "Enderecos.Bairro + SPACE(1) + Cidades.Localidade ",  /* BAIRRO */
                "CFOPs.OperacaoID, CFOPs.Operacao ",  /* CFOP */
                "Enderecos.CidadeID, Cidades.Localidade, Estados.CodigoLocalidade2 ",  /* CIDADE */
                "Parceiros.ClassificacaoID, Classificacoes.ItemMasculino ",  /* CLASSIFICACAO */
                "Parceiros.PessoaID, Parceiros.Fantasia ",  /* CLIENTE */
                "Pessoas.PessoaID, Pessoas.Fantasia ",  /* PESSOA */
                "dbo.fn_Data_Zero(Itens.dtMovimento) ",  /* DIA */
                "Pedidos.AlternativoID, Equipes.Fantasia ",  /* EQUIPE */
                "Enderecos.UFID, Estados.Localidade, Paises.CodigoLocalidade2 ",  /* ESTADO */
                "Familias.ConceitoID, Familias.Conceito ",  /* FAMILIA */
                "RelPessoas.ListaPreco, RelPessoas.ListaPreco ",  /* LISTA */
                "Marcas.ConceitoID, Marcas.Conceito ",  /* MARCA */
                "GPs.PessoaID, GPs.Fantasia ",  /* GER PRODUTOS */
                "LinhasProduto.ConceitoID, LinhasProduto.Conceito ",  /* LINHAS DE PRODUTO */
                "LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + '/' + dbo.fn_Pad(LTRIM(STR(DATEPART(mm, dbo.fn_Data_Zero(Itens.dtMovimento)))),2,CHAR(48), 'L') ",  /* MES */
                "Enderecos.PaisID, Paises.Localidade ",  /* PAIS */
                "Produtos.ConceitoID, Produtos.Conceito ",  /* PRODUTO */
                "LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + '/' + LTRIM(STR(DATEPART(qq, dbo.fn_Data_Zero(Itens.dtMovimento)))) ",  /* QUARTER */
                "Estados.TipoRegiaoID, Regioes.ItemMasculino, Paises.CodigoLocalidade2 ",  /* REGIAO */
                "LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + '/' + LTRIM(STR(DATEPART(wk, dbo.fn_Data_Zero(Itens.dtMovimento)))) ",  /* SEMANA */
                "Transacoes.OperacaoID, Transacoes.Operacao ",  /* TRANSACAO */
                "Pedidos.ProprietarioID, Vendedores.Fantasia ",  /* VENDEDOR */
                "Pedidos.OrigemPedidoID, dbo.fn_TipoAuxiliar_Item(Pedidos.OrigemPedidoID, NULL) ",  /* ORIGEM */
                "Pedidos.ProgramaMarketingID, dbo.fn_Pessoa_Fantasia(Pedidos.ProgramaMarketingID, 0) "  /* PROGRAMA MARKETING */
            };

            glb_aOrderBy = new object[,]
            {
                {"DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento))", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* ANO */
                {"Enderecos.Bairro + SPACE(1) + Cidades.Localidade", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* BAIRRO */
                {"CFOPs.Operacao", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* CFOP */
                {"Cidades.Localidade, Estados.CodigoLocalidade2", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* CIDADE */
                {"Classificacoes.ItemMasculino", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* CLASSIFICACAO */
                {"Parceiros.Fantasia", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* CLIENTE */
                {"Pessoas.Fantasia", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* PESSOA */
                {"dbo.fn_Data_Zero(Itens.dtMovimento)", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* DIA */
                {"Equipes.Fantasia", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* EQUIPE */
                {"Estados.Localidade, Paises.CodigoLocalidade2", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* ESTADO */
                {"Familias.Conceito", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* FAMILIA */
                {"RelPessoas.ListaPreco", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* LISTA */
                {"Marcas.Conceito", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* MARCA */
                {"GPs.Fantasia", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* GER PRODUTOS */
                {"LinhasProduto.Conceito", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* LINHAS DE PRODUTO */
                {"LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + '/' + dbo.fn_Pad(LTRIM(STR(DATEPART(mm, dbo.fn_Data_Zero(Itens.dtMovimento)))),2,CHAR(48), 'L')", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* MES */
                {"Paises.Localidade", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* PAIS */
                {"Produtos.Conceito", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* PRODUTO */
                {"LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + '/' + LTRIM(STR(DATEPART(qq, dbo.fn_Data_Zero(Itens.dtMovimento))))", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* QUARTER */
                {"Regioes.ItemMasculino", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* REGIAO */
                {"LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + '/' + LTRIM(STR(DATEPART(wk, dbo.fn_Data_Zero(Itens.dtMovimento))))", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* SEMANA */
                {"Transacoes.Operacao", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* TRANSACAO */
                {"Vendedores.Fantasia", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* VENDEDOR */
                {"Pedidos.OrigemPedidoID", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"}, /* ORIGEM */
                {"Pedidos.ProgramaMarketingID", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"} /* PROGRAMA MARKETING */
            };

            glb_aDetailIndex = new object[]
            {
                "Nome", "Contr_Cont DESC", "Contr_Ger DESC", "Faturamento DESC", "Preco_Base_Cont DESC", "Preco_Base DESC", "Financeiro DESC", "MC_Cont DESC", "MC DESC", "Pedidos DESC", "Quantidade DESC"
            };

            resetGlbsSQLVars();

            var nRegistros = "";

            int nSelTotal1 = Convert.ToInt32(HttpContext.Current.Request.Params["nSelTotal1"]);
            int nSelTotal2 = Convert.ToInt32(HttpContext.Current.Request.Params["nSelTotal2"]);
            int nSelTotal3 = Convert.ToInt32(HttpContext.Current.Request.Params["nSelTotal3"]);
            int nSelTotal4 = Convert.ToInt32(HttpContext.Current.Request.Params["nSelTotal4"]);
            int nSelTotal5 = Convert.ToInt32(HttpContext.Current.Request.Params["nSelTotal5"]);
            int nSelTotalOrdem1 = Convert.ToInt32(HttpContext.Current.Request.Params["nSelTotalOrdem1"]);
            int nSelTotalOrdem2 = Convert.ToInt32(HttpContext.Current.Request.Params["nSelTotalOrdem2"]);
            int nSelTotalOrdem3 = Convert.ToInt32(HttpContext.Current.Request.Params["nSelTotalOrdem3"]);
            int nSelTotalOrdem4 = Convert.ToInt32(HttpContext.Current.Request.Params["nSelTotalOrdem4"]);
            int nSelTotalOrdem5 = Convert.ToInt32(HttpContext.Current.Request.Params["nSelTotalOrdem5"]);

            int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["formato"]);

            var strSQL_Select = "SELECT " + nRegistros + glb_aFields[nSelTotal1] + glb_strSQL_Select;
            var strSQL_From = glb_strSQL_From;
            var strSQL_Where1 = glb_strSQL_Where1;
            var strSQL_Where2 = glb_strSQL_Where2 + "(dbo.fn_Data_Zero(Itens.dtMovimento)>=" + sDataInicio + " AND dbo.fn_Data_Zero(Itens.dtMovimento)<=" + sDataFim + ")" + sMarca + sFiltro + "))";
            var strSQL_GroupBy = "GROUP BY " + glb_aGroupBy[nSelTotal1];
            var strSQL_OrderBy = "ORDER BY " + glb_aOrderBy[nSelTotal1, nSelTotalOrdem1];
            var strSQLTotais = strSQL_Select + strSQL_From + strSQL_Where1 + strSQL_Where2 + strSQL_GroupBy + strSQL_OrderBy;

            if ((chkPedidosWeb == true) && (chkPedidosPb == true))
                sFiltro = sFiltro + " AND (Pedidos.OrigemPedidoID IN (606,608))";
            else if (chkPedidosWeb == true)
                sFiltro = sFiltro + " AND (Pedidos.OrigemPedidoID = 606) ";
            else if (chkPedidosPb == true)
                sFiltro = sFiltro + " AND (Pedidos.OrigemPedidoID = 608) ";

            if (!(dirA1 && dirA2))
                sFiltro += " AND Pedidos.ProprietarioID = " + glb_USERID + " ";

            if (glb_nEmpresaID != 7)
            {
                if (glb_nTipoPerfilComercialID == 263)//Gerente de produto
                    sMarca = " AND ProdutosEmpresa.ProprietarioID = " + glb_USERID + " ";
                else if (glb_nTipoPerfilComercialID == 264)//Assistente de Produto
                    sMarca = " AND ProdutosEmpresa.AlternativoID = " + glb_USERID + " ";
                else if (glb_nTipoPerfilComercialID == 265)//Vendedor e Televendas
                    sMarca = " AND Pedidos.ProprietarioID = " + glb_USERID + " ";

                if ((!(dirA1 && dirA2)) && (glb_nTipoPerfilComercialID == 265))
                    sMarca = "";
            }

            string[] strSQL = new string[5];
            var sExternalKey = "";
            var sSelectAnalise = "";
            var sGroupByAnalise = "";

            int Nivel = Convert.ToInt32(HttpContext.Current.Request.Params["Nivel"]);

            int[] nSelTotal = new int[5]
            {
                nSelTotal1, nSelTotal2, nSelTotal3, nSelTotal4, nSelTotal5
            };

            int[] nSelTotalOrdem = new int[5]
            {
                nSelTotalOrdem1, nSelTotalOrdem2, nSelTotalOrdem3, nSelTotalOrdem4, nSelTotalOrdem5
            };


            var sGroupBy = "GROUP BY " + glb_aKey[nSelTotal1, 0] + glb_aGroupBy[nSelTotal1] + sGroupByAnalise;
            var sOrderBy = "ORDER BY " + glb_aKey[nSelTotal1, 0];
            var ordemRepetida = 0;

            for (int i = 0; i < 5; i++)
            {

                sGroupBy += "," + glb_aGroupBy[nSelTotal[i]];

                sExternalKey += glb_aKey[(i == 0 ? nSelTotal1 : nSelTotal[i - 1]), (i == 0 ? 0 : 1)];

                if ((i > 0) && ((int)(glb_aKey[nSelTotal[i - 1], 2]) != 0))
                    sExternalKey += sExternalKey.Substring(0, sExternalKey.LastIndexOf(",")) + " AS " + glb_aKey[nSelTotal[i - 1], 3] + ", ";

                strSQL_Select = "SELECT " + (i == 0 ? nRegistros : "") + sExternalKey + glb_aFields[nSelTotal[i]] +
                    (i == 0 ? sSelectAnalise : "") + glb_strSQL_Select;

                strSQL_From = glb_strSQL_From.ToString();

                strSQL_Where1 = glb_strSQL_Where1.ToString();

                strSQL_Where2 = glb_strSQL_Where2 + "(dbo.fn_Data_Zero(Itens.dtMovimento)>=" + sDataInicio + " AND dbo.fn_Data_Zero(Itens.dtMovimento)<=" + sDataFim + ")" +
                    sMarca + sFiltro + "))";

                strSQL_GroupBy = sGroupBy;

                if (i == 0)
                    sOrderBy += glb_aOrderBy[nSelTotal[i], nSelTotalOrdem[i]];
                else
                    for (int j = 0; j < i; j++)
                    {
                        if (glb_aOrderBy[nSelTotal[j], nSelTotalOrdem[j]] == glb_aOrderBy[nSelTotal[i], nSelTotalOrdem[i]])
                            ordemRepetida = 1;
                    }
                if (ordemRepetida != 1 && i != 0)
                    sOrderBy += "," + glb_aOrderBy[nSelTotal[i], nSelTotalOrdem[i]];

                strSQL_OrderBy = sOrderBy;

                strSQL[i] = strSQL_Select + strSQL_From + strSQL_Where1 + strSQL_Where2 + strSQL_GroupBy + strSQL_OrderBy;
            }

            string query1 = strSQL[0].ToString();
            string query2 = strSQL[1].ToString();
            string query3 = strSQL[2].ToString();
            string query4 = strSQL[3].ToString();
            string query5 = strSQL[4].ToString();

            string inform1 = Request.Params["information1"].ToString();
            string inform2 = Request.Params["information2"].ToString();
            string inform3 = Request.Params["information3"].ToString();

            inform1 = inform1.Replace("MÃªs", "Mês");
            inform2 = inform2.Replace("PerÃ­odo", "Período");

            string ListColum = Request.Params["ListColum"].ToString();
            string ListLabels = Request.Params["ListLabels"].ToString();

            string TipoResult = Request.Params["TipoResult"].ToString();

            string indexKey2 = Request.Params["indexkey"].ToString();
            string indexKey3 = Request.Params["indexkey3"].ToString();
            string indexKey4 = Request.Params["indexkey4"].ToString();
            string indexKey5 = Request.Params["indexkey5"].ToString();

            string FieldsMae2 = Request.Params["keyAnt"].ToString();
            string FieldsMae3 = Request.Params["fieldsmae3"].ToString();
            string FieldsMae4 = Request.Params["fieldsmae4"].ToString();
            string FieldsMae5 = Request.Params["fieldsmae5"].ToString();

            char[] DelimitChar1 = { ':' };

            string[] ListColum1 = ListColum.Split(DelimitChar1);
            string[] ListLabels1 = ListLabels.Split(DelimitChar1);

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Nivel == 2)
            {
                arrSqlQuery = new string[,] { { "Query1", query1 }, { "Query2", query2 } };
                arrIndexKey = new string[,] { { "Query2", indexKey2, FieldsMae2 } };
            }
            else if (Nivel == 3)
            {
                arrSqlQuery = new string[,] { { "Query1", query1 }, { "Query2", query2 }, { "Query3", query3 } };
                arrIndexKey = new string[,] { { "Query2", indexKey2, FieldsMae2 }, { "Query3", indexKey3, FieldsMae3 } };
            }
            else if (Nivel == 4)
            {
                arrSqlQuery = new string[,] { { "Query1", query1 }, { "Query2", query2 }, { "Query3", query3 }, { "Query4", query4 } };
                arrIndexKey = new string[,] { { "Query2", indexKey2, FieldsMae2 }, { "Query3", indexKey3, FieldsMae3 }, { "Query4", indexKey4, FieldsMae4 } };
            }
            else if (Nivel == 5)
            {
                arrSqlQuery = new string[,] { { "Query1", query1 }, { "Query2", query2 }, { "Query3", query3 }, { "Query4", query4 }, { "Query5", query5 } };
                arrIndexKey = new string[,] { { "Query2", indexKey2, FieldsMae2 }, { "Query3", indexKey3, FieldsMae3 }, { "Query4", indexKey4, FieldsMae4 }, { "Query5", indexKey5, FieldsMae5 } };
            }
            else
            {
                arrSqlQuery = new string[,] { { "Query1", query1 } };
            }

            string format = "BRA";

            if (inform3.Contains("Allplus") == true)
            {
                format = "USA";
            }

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            var posY = (Formato == 1 ? "0" : "1.8");
            //var posY = (Formato == 1 ? "0" : "0.18");

            var x1 = 0;

            if (Formato == 2) // Se Excel
            {

                //Cria Relatorio
                Relatorio.CriarRelatório("TotalizaVendas", arrSqlQuery, format, "Excel");

                //Cabeçalhos retirados devido ao Gsheet apresentar falha na conversão devido às celulas mescladas - MTH 19/10/2018
                //Labels Cabeçalho
                //Relatorio.CriarObjLabel("Fixo", "", inform1.ToString(), "0", "0", "8", "", "B", Header_Body, "26.73584", "");
                //Relatorio.CriarObjLabel("Fixo", "", inform1.ToString(), "0", "0", "8", "", "B", Header_Body, "26.73584", "");
                //Relatorio.CriarObjLabel("Fixo", "", inform2.ToString(), "0", "0.34396", "8", "", "B", Header_Body, "12.27438", "");
                //Relatorio.CriarObjLabel("Fixo", "", inform2.ToString(), "0", "0.034396", "8", "", "B", Header_Body, "12.27438", "");

                //Relatorio.CriarObjLabel("Fixo", "", "Totalização de Vendas", "13.13834", "0.68792", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Totalização de Vendas", "1.313834", "0.068792", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabelData("Now", "", "", "20.5", "0.68792", "11", "black", "B", Header_Body, "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "2.05", "0.068792", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela
                Relatorio.CriarObjTabela("0", "1.19945", arrIndexKey, true, true);
                //Relatorio.CriarObjTabela("0", "0.119945", arrIndexKey, true, true);

                //Insere Colunas na tabela
                Relatorio.CriarObjColunaNaTabela("ID", "ID", true, "ID");
                Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Fixo", "Total", "TotalGroup", "", "DetailGroup", 2, true);

                Relatorio.CriarObjColunaNaTabela("Nome", "Nome", true, "Nome");
                Relatorio.CriarObjCelulaInColuna("Query", "Nome", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Pedidos", "Pedidos", true, "Pedidos");
                Relatorio.CriarObjCelulaInColuna("Query", "Pedidos", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Pedidos", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Quantidade", "Quantidade", true, "Quantidade");
                Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Faturamento", "Faturamento", true, "Faturamento");
                Relatorio.CriarObjCelulaInColuna("Query", "Faturamento", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Faturamento", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Impostos", "Impostos", true, "Impostos");
                Relatorio.CriarObjCelulaInColuna("Query", "Impostos", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Impostos", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Desp_Ger", "Desp_Ger", true, "Desp_Ger");
                Relatorio.CriarObjCelulaInColuna("Query", "Desp_Ger", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Desp_Ger", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Financeiro", "Financeiro", true, "Financeiro");
                Relatorio.CriarObjCelulaInColuna("Query", "Financeiro", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Financeiro", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Preco_Base", "Preco_Base", true, "Preco_Base");
                Relatorio.CriarObjCelulaInColuna("Query", "Preco_Base", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Preco_Base", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Preco_Base_Cont", "Preco_Base_Cont", true, "Preco_Base_Cont");
                Relatorio.CriarObjCelulaInColuna("Query", "Preco_Base_Cont", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Preco_Base_Cont", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Custos_Ger", "Custos_Ger", true, "Custos_Ger");
                Relatorio.CriarObjCelulaInColuna("Query", "Custos_Ger", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Custos_Ger", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Custo_Base", "Custo_Base", true, "Custo_Base");
                Relatorio.CriarObjCelulaInColuna("Query", "Custo_Base", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Custo_Base", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Custo_Base_Cont", "Custo_Base_Cont", true, "Custo_Base_Cont");
                Relatorio.CriarObjCelulaInColuna("Query", "Custo_Base_Cont", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Custo_Base_Cont", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Contr_Cont", "Contr_Cont", true, "Contr_Cont");
                Relatorio.CriarObjCelulaInColuna("Query", "Contr_Cont", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Contr_Cont", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Contr_Ger", "Contr_Ger", true, "Contr_Ger");
                Relatorio.CriarObjCelulaInColuna("Query", "Contr_Ger", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Contr_Ger", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("MC", "MC", true, "MC");
                Relatorio.CriarObjCelulaInColuna("Query", "MC", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "MC", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("MC_Cont", "MC_Cont", true, "MC_Cont");
                Relatorio.CriarObjCelulaInColuna("Query", "MC_Cont", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "MC_Cont", "TotalGroup", "", "DetailGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("CAP", "CAP", true, "CAP");
                Relatorio.CriarObjCelulaInColuna("Query", "CAP", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "CAP", "TotalGroup", "", "DetailGroup", 0, true);

                if (Nivel > 1)
                {
                    for (int cont = 1; cont < Nivel; cont++)
                    {
                        x1++;

                        Relatorio.CriarObjColunaNaTabela("C" + x1, "C" + x1, true, "C" + x1);
                        Relatorio.CriarObjCelulaInColuna("Query", "C" + x1, "DetailGroup");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);

                        x1++;

                        Relatorio.CriarObjColunaNaTabela("C" + x1, "C" + x1, true, "C" + x1);
                        Relatorio.CriarObjCelulaInColuna("Query", "C" + x1, "DetailGroup");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true);
                    }
                }
            }
            else  // Se PDF
            {

                //Cria Relatorio
                Relatorio.CriarRelatório("TotalizaVendas", arrSqlQuery, format, "Landscape");

                //Labels Cabeçalho
                Relatorio.CriarObjLabel("Fixo", "", "Totalização de Vendas", "13.13834", "0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Totalização de Vendas", "1.313834", "0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "24.795", "0", "11", "black", "B", Header_Body, "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "2.4795", "0", "11", "black", "B", Header_Body, "3.72187", "Horas");
                Relatorio.CriarObjLinha("0.1", "0.47625", "29", "Black", Header_Body);
                //Relatorio.CriarObjLinha("0.01", "0.047625", "29", "Black", Header_Body);
                Relatorio.CriarObjLabel("Fixo", "", inform1.ToString(), "0.2", "0.54681", "8", "", "B", Header_Body, "26.73584", "");
                //Relatorio.CriarObjLabel("Fixo", "", inform1.ToString(), "0.02", "0.054681", "8", "", "B", Header_Body, "26.73584", "");
                Relatorio.CriarObjLabel("Fixo", "", inform2.ToString(), "0.2", "0.89077", "8", "", "B", Header_Body, "12.27438", "");
                //Relatorio.CriarObjLabel("Fixo", "", inform2.ToString(), "0.02", "0.089077", "8", "", "B", Header_Body, "12.27438", "");
                Relatorio.CriarObjLabel("Fixo", "", "Tipo:" + TipoResult, "0.2", "1.26118", "8", "", "B", Header_Body, "6.43063", "");
                //Relatorio.CriarObjLabel("Fixo", "", "Tipo:" + TipoResult, "0.02", "0.126118", "8", "", "B", Header_Body, "6.43063", "");
                Relatorio.CriarObjLabel("Fixo", "", inform3.ToString(), "0.2", "1.60514", "8", "", "B", Header_Body, "20.22646", "");
                //Relatorio.CriarObjLabel("Fixo", "", inform3.ToString(), "0.02", "0.160514", "8", "", "B", Header_Body, "20.22646", "");

                //Cria Tabela
                Relatorio.CriarObjTabela("0.2", posY, arrIndexKey, true, true);
                //Relatorio.CriarObjTabela("0.02", posY, arrIndexKey, true, true);

                //Insere Colunas na tabela
                for (int i = 0; i < ListColum1.Length; i++)
                {
                    Relatorio.CriarObjColunaNaTabela(ListLabels1[i], ListColum1[i], true, ListColum1[i]);
                    Relatorio.CriarObjCelulaInColuna("Query", ListColum1[i], "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna((i == 0 || i == 1 ? "Fixo" : "Query"), (i == 0 ? "Total" : i == 1 ? "Merge" : ListColum1[i]), "TotalGroup", "", "DetailGroup", (i == 0 ? 2 : 0), true);
                }

            }


            //Finaliza tabela
            Relatorio.TabelaEnd();

            //Exporta 
            Relatorio.CriarPDF_Excel("TotalVendas", Formato);
        }

        public void relatorioCompras()
        {
            int nPadraoPedido = Convert.ToInt32(HttpContext.Current.Request.Params["nPadraoPedido"]);

            string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
            string sTaxaMoeda = Convert.ToString(HttpContext.Current.Request.Params["sTaxaMoeda"]);
            string sDataInicio = Convert.ToString(HttpContext.Current.Request.Params["sDataInicio"]);
            string sDataFim = Convert.ToString(HttpContext.Current.Request.Params["sDataFim"]);
            string sFiltro = Convert.ToString(HttpContext.Current.Request.Params["sFiltro"]);
            string sFiltroFabricante = Convert.ToString(HttpContext.Current.Request.Params["sFiltroFabricante"]);
            string sFiltroFamilia = Convert.ToString(HttpContext.Current.Request.Params["sFiltroFamilia"]);
            string strSQL;

            // Padrao Pedidos
            if (nPadraoPedido == 706)
            {
               strSQL = "SELECT CONVERT(VARCHAR, Itens.dtMovimento, 103) AS Data, Pedidos.PedidoID, NotasFiscais.NotaFiscal, " +
                                    "CONVERT(VARCHAR, NotasFiscais.dtNotaFiscal, 103) AS DataNF, CFOPs.Codigo AS CFOP, " +
                                    "Colaboradores.Fantasia AS Colaborador, Pessoas.PessoaID , Pessoas.Fantasia, Pedidos.Observacao, " +
                                    "CONVERT(NUMERIC(11,2), Pedidos.ValorTotalPedido " + sTaxaMoeda + ") AS ValorPedido, Pedidos.ValorFrete, Pedidos.ValorOutrasDespesas " +
                                    "FROM Pedidos WITH(NOLOCK) " +
                                    "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID = Transacoes.OperacaoID) " +
                                    "INNER JOIN Pessoas Colaboradores WITH(NOLOCK) ON (Pedidos.ProprietarioID = Colaboradores.PessoaID) " +
                                    "INNER JOIN Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID = Pessoas.PessoaID) " +
                                    "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID) " +
                                    "INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (Pedidos.PedidoID = Itens.PedidoID) " +
                                    "LEFT JOIN Operacoes CFOPs WITH(NOLOCK) ON (Itens.CFOPID = CFOPs.OperacaoID) " +
                                    "WHERE (Pedidos.EstadoID >= 30 AND " +
                                    "((Transacoes.MetodoCustoID = 372 AND Transacoes.OperacaoID IN (111, 211, 985, 121, 122, 143, 162, 171, 174, 311, 312, 316, 535, 611, 991)) OR Transacoes.OperacaoID = 116) AND " +
                                    "dbo.fn_Data_Zero(Itens.dtMovimento) BETWEEN " + sDataInicio + " AND " + sDataFim + " AND Pedidos.EmpresaID = " + glb_nEmpresaID + ") " + sFiltro +
                                    "GROUP BY Itens.dtMovimento, Pedidos.PedidoID, NotasFiscais.NotaFiscal, NotasFiscais.dtNotaFiscal, CFOPs.Codigo, " +
                                    "Colaboradores.Fantasia, Pessoas.PessoaID , Pessoas.Fantasia, Pedidos.Observacao, Pedidos.TipoPedidoID, Pedidos.ValorTotalPedido, " +
                                    "Pedidos.ValorFrete, Pedidos.ValorOutrasDespesas " +
                                    "ORDER BY Itens.dtMovimento";
            }
            // Padrao Generico
            else
            {
                strSQL = "SELECT CONVERT(VARCHAR, Itens.dtMovimento, 103) AS Data, Pedidos.PedidoID, NotasFiscais.NotaFiscal, " +
                                     "CONVERT(VARCHAR, NotasFiscais.dtNotaFiscal, 103) AS DataNF, CFOPs.OperacaoID AS CFOP, " +
                                     "Colaboradores.Fantasia AS Colaborador, Pessoas.PessoaID , Pessoas.Fantasia, Pedidos.Observacao, " +
                                     "Produtos.ConceitoID, (SELECT TOP 1 CodigoAnterior " +
                                                             "FROM RelacoesPesCon WITH(NOLOCK) " +
                                                             "WHERE (ObjetoID = Produtos.ConceitoID AND TipoRelacaoID = 61 AND " +
                                                                 "SujeitoID = Pedidos.EmpresaID)) AS CodigoAnterior, Produtos.Conceito," +
                                     "Produtos.Modelo, Produtos.PartNumber, Familias.Conceito AS Familia, " +
                                     "(SELECT TOP 1 Marca.Conceito FROM Conceitos Marca WITH(NOLOCK) WHERE Produtos.MarcaID = Marca.ConceitoID) AS Marca, " +
                                     "(SELECT TOP 1 dbo.fn_Pessoa_Fantasia(GP.ProprietarioID, 0) FROM RelacoesPesCon GP WITH(NOLOCK) WHERE Produtos.ConceitoID = GP.ObjetoID AND Pedidos.EmpresaID = GP.SujeitoID AND GP.TipoRelacaoID = 61) AS GP, " +
                                     "CONVERT(NUMERIC(11), (CASE Pedidos.TipoPedidoID WHEN 601 THEN Itens.Quantidade ELSE Itens.Quantidade * (-1) END)) AS Quantidade, " +
                                     "CONVERT(NUMERIC(11,2), dbo.fn_PedidoItem_Totais(Itens.PedItemID, 8) " + sTaxaMoeda + ") AS Valor " +
                                 "FROM Pedidos WITH(NOLOCK) " +
                                     "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID = Transacoes.OperacaoID) " +
                                     "INNER JOIN Pessoas Colaboradores WITH(NOLOCK) ON (Pedidos.ProprietarioID = Colaboradores.PessoaID) " +
                                     "INNER JOIN Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID = Pessoas.PessoaID) " +
                                     "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                                     "INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (Pedidos.PedidoID = Itens.PedidoID) " +
                                     "LEFT JOIN Operacoes CFOPs WITH(NOLOCK) ON (Itens.CFOPID = CFOPs.OperacaoID) " +
                                     "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID = Produtos.ConceitoID) " +
                                     "INNER JOIN Conceitos Familias  WITH(NOLOCK) ON (Produtos.ProdutoID = Familias.ConceitoID) " +
                                 "WHERE (Pedidos.EstadoID >= 30 AND " +
                                     "((Transacoes.MetodoCustoID = 372 AND Transacoes.OperacaoID IN (111, 211, 985, 121, 122, 143, 162, 171, 174, 311, 312, 316, 535, 611, 991)) OR Transacoes.OperacaoID = 116) AND " +
                                     "dbo.fn_Data_Zero(Itens.dtMovimento) BETWEEN " + sDataInicio + " AND " +
                                     sDataFim + " AND Pedidos.EmpresaID = " + glb_nEmpresaID + ") " +
                                     sFiltroFabricante + sFiltroFamilia + sFiltro +
                                 "ORDER BY Itens.dtMovimento";
            }


            // Excel
            int Formato = 2;
            string Title = "Relatório de Compras";

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "2.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "5.13834", "0.0", "11", "#0113a0", "B", Header_Body, "7.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "13.0", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }

        public void relatorioComissao()
        {
            string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
            string sDataInicio = Convert.ToString(HttpContext.Current.Request.Params["sDataInicio"]);
            string sDataFim = Convert.ToString(HttpContext.Current.Request.Params["sDataFim"]);
            string sFiltro = Convert.ToString(HttpContext.Current.Request.Params["sFiltro"]);
            string sParceiroID = Convert.ToString(HttpContext.Current.Request.Params["sParceiroID"]);
            string sEstadoID = Convert.ToString(HttpContext.Current.Request.Params["sEstadoID"]);
            string sTipoComissaoID = Convert.ToString(HttpContext.Current.Request.Params["sTipoComissaoID"]);
            string sFiltroEmpresas = Convert.ToString(HttpContext.Current.Request.Params["sFiltroEmpresas"]);
            string strParams = Convert.ToString(HttpContext.Current.Request.Params["strParams"]);
            string strSQL;

            string parametro = "103";

            if (sLinguaLogada != "246")
                parametro = "101";


            strSQL = "SELECT CONVERT(VARCHAR(10),Comissao.dtVencimento,"+ parametro + ") AS Vencimento, " +
                        "(CASE WHEN DATEDIFF(DD, Comissao.dtVencimento, GETDATE()) <= 0 THEN 0 ELSE DATEDIFF(DD, Comissao.dtVencimento, GETDATE()) END) AS Atraso, " +
                        "dbo.fn_TipoAuxiliar_Item(Comissao.EstadoID, 0) AS Estado, " +
                        "CONVERT(VARCHAR(10),Comissao.dtEmissao,"+ parametro+") AS Emissao, " +
                        "Comissao.PedidoID AS Pedido, " +
                        "NotasFiscais.NotaFiscal AS NotaFiscal, NotasFiscais.Natureza AS Transacao, dbo.fn_Formata_Numero(Comissao.ValorParcela,2," + parametro + ") AS ValorLiquido, " +
                        "dbo.fn_Pessoa_Fantasia(Pedidos.PessoaID, 0) As FaturadoPara, " +
                        "Pedidos.SeuPedido AS SeuPedido, " +
                        "dbo.fn_Pessoa_Fantasia(Pedidos.EmpresaID, 0) AS Distribuidor, " +
                        "PedParcelas.Observacao " +
                    "FROM fn_ParcelasComissoes_tbl(NULL, " + sParceiroID + ", " + sEstadoID + ") Comissao " +
                        "INNER JOIN Pedidos WITH(NOLOCK) ON Comissao.PedidoID = Pedidos.PedidoID " +
                        "INNER JOIN NotasFiscais WITH(NOLOCK) ON Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID " +
                        "INNER JOIN Pedidos_Parcelas PedParcelas WITH(NOLOCK) ON (PedParcelas.PedParcelaID = Comissao.PedParcelaID) " +
                    "WHERE Comissao.TipoParcelaID = " + sTipoComissaoID + " AND Comissao.dtVencimento BETWEEN " + sDataInicio + " AND " + sDataFim + " " + sFiltroEmpresas + sFiltro +
                    " ORDER BY Comissao.EstadoID, Pedidos.EmpresaID, Comissao.dtVencimento";

            string strSQL2;

            strSQL2 = " SELECT dbo.fn_Formata_Numero(SUM(Comissao.ValorParcela), 2, " + parametro + ") AS SUMValorLiquido " +
                            " FROM fn_ParcelasComissoes_tbl(NULL, " + sParceiroID + ", " + sEstadoID + ") Comissao " +
                                "INNER JOIN Pedidos WITH(NOLOCK) ON Comissao.PedidoID = Pedidos.PedidoID " +
                                "INNER JOIN NotasFiscais WITH(NOLOCK) ON Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID " +
                                "INNER JOIN Pedidos_Parcelas PedParcelas WITH(NOLOCK) ON (PedParcelas.PedParcelaID = Comissao.PedParcelaID) " +
                            " WHERE Comissao.TipoParcelaID = " + sTipoComissaoID + " AND Comissao.dtVencimento BETWEEN " + sDataInicio + " AND " + sDataFim + " " + sFiltroEmpresas + sFiltro;


            string Title = "Relatório de Comissão";

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL },
                                          { "Query2", strSQL2 } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1) // Se PDF
            {
                double headerX = 0.2;
                double headerY = 0.2;

                double tabelaX = 0.2;
                double tabelaY = 0.0;

                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Portrait", "1.45", Header_Color:true);

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                string sumTotal = dsFin.Tables["Query2"].Rows[0]["SUMValorLiquido"].ToString();

                //Header
                Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, headerX.ToString(), headerY.ToString(), "12", "black", "B", Header_Body, "2.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, (headerX+ 8.14).ToString(), headerY.ToString(), "12", "#0113a0", "B", Header_Body, "7.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", (headerX + 15).ToString(), headerY.ToString(), "11", "black", "B", Header_Body, "3.72187", "Horas");
                Relatorio.CriarNumeroPagina((headerX + 19.7).ToString(), headerY.ToString(), "11", "black", "B", false);
                
                //Parametros
                Relatorio.CriarObjLabel("Fixo", "", strParams, headerX.ToString(), (headerY+0.5).ToString(), "8", "black", "B", Header_Body, "15.51332", "");

                Relatorio.CriarObjLinha(headerX.ToString(), (headerY + 0.85).ToString(),"20.2","black", Header_Body);

                //Titulo tabela
                Relatorio.CriarObjLabel("Fixo", "", "Vencimento", (headerX).ToString(), (headerY + 0.88).ToString(), "8", "black", "B", Header_Body, "15.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", "Atraso", (headerX+1.95).ToString(), (headerY + 0.88).ToString(), "8", "black", "B", Header_Body, "15.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", "Estado", (headerX+3).ToString(), (headerY + 0.88).ToString(), "8", "black", "B", Header_Body, "15.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", "Emissão", (headerX+4.9).ToString(), (headerY + 0.88).ToString(), "8", "black", "B", Header_Body, "15.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", "Pedido", (headerX+6.5).ToString(), (headerY + 0.88).ToString(), "8", "black", "B", Header_Body, "15.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", "Nota fiscal", (headerX+7.93).ToString(), (headerY + 0.88).ToString(), "8", "black", "B", Header_Body, "15.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", "Transação", (headerX+9.5).ToString(), (headerY + 0.88).ToString(), "8", "black", "B", Header_Body, "15.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", "Valor líquido", (headerX+11.13).ToString(), (headerY + 0.88).ToString(), "8", "black", "B", Header_Body, "15.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", "Faturado Para", (headerX+13).ToString(), (headerY + 0.88).ToString(), "8", "black", "B", Header_Body, "15.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", "Seu Pedido", (headerX+15.6).ToString(), (headerY + 0.88).ToString(), "8", "black", "B", Header_Body, "15.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", "Distribuidor", (headerX+18.08).ToString(), (headerY + 0.88).ToString(), "8", "black", "B", Header_Body, "15.51332", "");


                //Cria Tabela           
                Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "Query1", false, false, false);

                Relatorio.CriarObjColunaNaTabela(" ", "Vencimento", false, "1.8", "Default","1","0");
                Relatorio.CriarObjCelulaInColuna("Query", "Vencimento", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "Total Geral", "TotalGroup", "", "DetailGroup", 1, true, "8");

                Relatorio.CriarObjColunaNaTabela(" ", "Atraso", false, "1.2", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "Atraso", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela(" ", "Estado", false, "1.9", "Default", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "Estado", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela(" ", "Emissao", false, "1.6", "Default", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "Emissao", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela(" ", "Pedido", false, "1.3", "Default", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "Pedido", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela(" ", "NotaFiscal", false, "1.7", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "NotaFiscal", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela(" ", "Transacao", false, "1.6", "Default", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "Transacao", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela(" ", "ValorLiquido", false, "1.9", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "ValorLiquido", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", sumTotal, "TotalGroup", "", "DetailGroup", 1, true, "8");

                Relatorio.CriarObjColunaNaTabela(" ", "FaturadoPara", false, "2.6", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "FaturadoPara", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela(" ", "SeuPedido", false, "2.48", "Default", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "SeuPedido", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela(" ", "Distribuidor", false, "2.1", "Default", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "Distribuidor", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.TabelaEnd();
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                DataSet dsFin = new DataSet();
                dsFin = (DataSet)HttpContext.Current.Session["Query"];
                string sumTotal = dsFin.Tables["Query2"].Rows[0]["SUMValorLiquido"].ToString();

                Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "2.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "5.13834", "0.0", "11", "#0113a0", "B", Header_Body, "7.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "13.0", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");
                Relatorio.CriarObjLabel("Fixo", "", strParams, "0.001", "0.4", "11", "#0113a0", "B", Header_Body, "15.51332", "");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.6", "Query1", true, false, false);

                Relatorio.CriarObjColunaNaTabela("Vencimento", "Vencimento", true, "1.8", "Default");
                Relatorio.CriarObjCelulaInColuna("Query", "Vencimento", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "Total Geral", "TotalGroup", "", "DetailGroup", 1, true, "8");

                Relatorio.CriarObjColunaNaTabela("Atraso", "Atraso", true, "1.2", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "Atraso", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("Estado", "Estado", true, "1.9", "Default");
                Relatorio.CriarObjCelulaInColuna("Query", "Estado", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("Emissão", "Emissao", true, "1.6", "Default");
                Relatorio.CriarObjCelulaInColuna("Query", "Emissao", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("Pedido", "Pedido", true, "1.3", "Default");
                Relatorio.CriarObjCelulaInColuna("Query", "Pedido", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("Nota fiscal", "NotaFiscal", true, "1.7", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "NotaFiscal", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("Transação", "Transacao", true, "1.6", "Default");
                Relatorio.CriarObjCelulaInColuna("Query", "Transacao", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("Valor líquido", "ValorLiquido", true, "1.9", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "ValorLiquido", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", sumTotal, "TotalGroup", "", "DetailGroup", 1, true, "8");

                Relatorio.CriarObjColunaNaTabela("Faturado Para", "FaturadoPara", true, "2.6");
                Relatorio.CriarObjCelulaInColuna("Query", "FaturadoPara", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("Seu Pedido", "SeuPedido", true, "2.48", "Default");
                Relatorio.CriarObjCelulaInColuna("Query", "SeuPedido", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("Distribuidor", "Distribuidor", true, "2.1", "Default");
                Relatorio.CriarObjCelulaInColuna("Query", "Distribuidor", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }

        public void relatorioTransportadoras()
        {
            string sEmpresaID = Convert.ToString(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
            string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
            string sDataInicio = Convert.ToString(HttpContext.Current.Request.Params["sDataInicio"]);
            string sDataFim = Convert.ToString(HttpContext.Current.Request.Params["sDataFim"]);
            string sFiltro = Convert.ToString(HttpContext.Current.Request.Params["sFiltro"]);
            string chkFretePago = Convert.ToString(HttpContext.Current.Request.Params["chkFretePago"]);
            string sTranspCondition = Convert.ToString(HttpContext.Current.Request.Params["sTranspCondition"]);
            string sDATE_SQL_PARAM = Convert.ToString(HttpContext.Current.Request.Params["sDATE_SQL_PARAM"]);
            string strSQL;

            if (sFiltro == "null")
            {
                sFiltro = "";
            }

            if (sTranspCondition == "null")
            {
                sTranspCondition = "";
            }

            strSQL = "SELECT CONVERT(VARCHAR(10),NotasFiscais.dtNotaFiscal,103) AS [Data NF], NotasFiscais.NotaFiscal AS NF, Transacoes.Operacao AS Transacao, Pedidos.PedidoID AS Pedido, Clientes.Nome AS Pessoa, " +
                            "UFs.CodigoLocalidade2 AS UF, Cidades.Localidade AS Cidade, Vendedores.Fantasia AS Vendedor, " +
                            "dbo.fn_Pedido_Marcas(Pedidos.PedidoID) AS Marcas, " +
                            "(CASE WHEN Pedidos.TransportadoraID = Pedidos.ParceiroID THEN 'O mesmo' ELSE Transportadoras.Fantasia END) AS Transportadora, " +
                            "Pedidos.ValorTotalPedido AS [Tot Pedido], " +
                            "(SELECT TOP 1 ValorParcela FROM Pedidos_Parcelas WITH(NOLOCK) WHERE (PedidoID = Pedidos.PedidoID AND HistoricoPadraoID = 3173)) AS [Val Frete], " +
                            "(SELECT TOP 1 Observacao FROM Pedidos_Parcelas WITH(NOLOCK) WHERE (PedidoID = Pedidos.PedidoID AND HistoricoPadraoID = 3173)) AS [Observação], " +
                            "TotalPesoBruto AS PesoBruto, TotalPesoLiquido AS PesoLiquido, NumeroVolumes AS Volumes, dbo.fn_Numero_Formata(dbo.fn_Pedido_Cubagem(Pedidos.PedidoID),8,1," + sDATE_SQL_PARAM + ") AS Cubagem,  pedidos.RNTC " +
                    "FROM Pedidos WITH(NOLOCK), NotasFiscais WITH(NOLOCK), Operacoes Transacoes WITH(NOLOCK), Pessoas Clientes WITH(NOLOCK), Pessoas Transportadoras WITH(NOLOCK), Pessoas Vendedores WITH(NOLOCK), " +
                        "Pessoas_Enderecos Enderecos WITH(NOLOCK), Localidades Paises WITH(NOLOCK), Localidades UFs WITH(NOLOCK), Localidades Cidades  WITH(NOLOCK) " +
                    "WHERE (Pedidos.EmpresaID = " + sEmpresaID + " AND Pedidos.TipoPedidoID = 602 AND " +
                        "Pedidos.Frete = " + chkFretePago + " AND Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID AND " +
                        "Pedidos.TransacaoID = Transacoes.OperacaoID AND Pedidos.PessoaID = Clientes.PessoaID AND Pedidos.TransportadoraID = Transportadoras.PessoaID " +
                        sTranspCondition + " AND NotasFiscais.EstadoID = 67 AND " +
                        "Pedidos.ProprietarioID = Vendedores.PessoaID AND Clientes.PessoaID = Enderecos.PessoaID AND " +
                        "Enderecos.EndFaturamento = 1 AND Enderecos.Ordem = 1 AND Enderecos.PaisID = Paises.LocalidadeID AND " +
                        "Enderecos.UFID = UFs.LocalidadeID AND Enderecos.CidadeID = Cidades.LocalidadeID AND " +
                        "dbo.fn_Data_Zero(NotasFiscais.dtNotaFiscal) BETWEEN " + sDataInicio + " AND " + sDataFim + sFiltro + ") " +
                    "ORDER BY NotasFiscais.dtNotaFiscal, NotasFiscais.NotaFiscal";

            // Excel
            int Formato = 2;
            string Title = "Relatório de Transportadoras";

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "2.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "5.13834", "0.0", "11", "#0113a0", "B", Header_Body, "6.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "13.0", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }

        public void prtMercadoria()
        {
            int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
            string pedidoID = Convert.ToString(HttpContext.Current.Request.Params["PedidoID"]);

            string strSQL1 = "";
            strSQL1 = " SELECT " +
                        " Pedidos.PedidoID, CONVERT(VARCHAR, Pedidos.dtPedido, 103) AS dtPedido, NF.NotaFiscal, CONVERT(VARCHAR, NF.dtNotaFiscal, 103) AS dtNotaFiscal, " +
                        " Transacoes.Operacao AS Transacao, Colaboradores.Fantasia AS Vendedor, CONVERT(INT, Pedidos.NumeroVolumes) AS NumeroVolumes, Pessoas.Fantasia AS Cliente, " +
                        " Portador = CASE WHEN Pedidos.Portador IS NULL THEN REPLICATE(CHAR(95), 20) WHEN Pedidos.Portador = SPACE(0) THEN REPLICATE(CHAR(95), 20) ELSE Pedidos.Portador END, " +
                        " DocumentoPortador = CASE WHEN Pedidos.DocumentoPortador IS NULL THEN REPLICATE(CHAR(95), 20) WHEN Pedidos.DocumentoPortador = SPACE(0) THEN REPLICATE(CHAR(95), 20) ELSE Pedidos.DocumentoPortador END, " +
                        " ' ' AS TEST " +
                      " FROM " +
                        " Pedidos WITH(NOLOCK) " +
                        " INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON(Transacoes.OperacaoID = Pedidos.TransacaoID) " +
                        " INNER JOIN Pessoas Colaboradores WITH(NOLOCK) ON(Colaboradores.PessoaID = Pedidos.ProprietarioID) " +
                        " INNER JOIN NotasFiscais NF WITH(NOLOCK) ON(NF.NotaFiscalID = Pedidos.NotaFiscalID) " +
                        " INNER JOIN Pessoas WITH(NOLOCK)ON(Pessoas.PessoaID = Pedidos.PessoaID) " +
                      " WHERE " +
                        " Pedidos.PedidoID IN ( " + pedidoID + " ) AND NF.EstadoID = 67 ";

            arrSqlQuery = new string[,] { { "Query", strSQL1 } };

            double TamLinha = 19;

            double header_top = 0;
            double header_left = 1.1;

            double bloco1_top = 1.5;
            double bloco1ask_left = 1.1;
            double bloco1ans_left = 4;

            double bloco2_top = 1.5;
            double bloco2_left = 6.75;

            double bloco3_top = 1.5;
            double bloco3ask_left = 10.5;
            double bloco3ans_left = 13.5;

            double bloco4_top = 1.5;
            double bloco4_left = 1.1;


            Relatorio.CriarRelatório("Protocolo de Retirada de Mercadoria", arrSqlQuery, "BRA", "Portrait");

            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            int countRow = dsFin.Tables["Query"].Rows.Count;
            int i = 0;
            double y = 0;
            
            while (i < countRow) {

                Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, header_left.ToString(), (header_top+y).ToString(), "12", "black", "B", "Body", "2.5", "");
                Relatorio.CriarObjLabel("Fixo", "", "Protocolo de Retirada de Mercadoria", (header_left + 5.12834).ToString(), (header_top + y).ToString(), "12", "#0113a0", "B", "Body", "8", "");
                Relatorio.CriarObjLabelData("Now", "", "", (header_left + 15.0225).ToString(), (header_top + y).ToString(), "10", "black", "B", "Body", "3.72187", "Horas");

                Relatorio.CriarObjLinha(header_left.ToString(), (header_top + 0.5 + y).ToString(), TamLinha.ToString(), "", "Body");

                //Bloco1  
                Relatorio.CriarObjLabel("Fixo", "", "Pedido", bloco1ask_left.ToString(), (bloco1_top+y).ToString(), "14", " ", "L", "Body", "5.54792", "");
                Relatorio.CriarObjLabel("Fixo", "", dsFin.Tables["Query"].Rows[i]["PedidoID"].ToString(), bloco1ans_left.ToString(), (bloco1_top+y).ToString(), "14", " ", "B", "Body", "5.54792", "");

                Relatorio.CriarObjLabel("Fixo", "", "Nota", bloco1ask_left.ToString(), (bloco1_top + 1.5 + y).ToString(), "14", " ", "L", "Body", "5.54792", "");
                Relatorio.CriarObjLabel("Fixo", "", dsFin.Tables["Query"].Rows[i]["NotaFiscal"].ToString(), bloco1ans_left.ToString(), (bloco1_top + 1.5 + y).ToString(), "14", " ", "B", "Body", "5.54792", "");

                Relatorio.CriarObjLabel("Fixo", "", "Transação", bloco1ask_left.ToString(), (bloco1_top + 3 + y).ToString(), "14", " ", "L", "Body", "5.54792", "");
                Relatorio.CriarObjLabel("Fixo", "", dsFin.Tables["Query"].Rows[i]["Transacao"].ToString(), bloco1ans_left.ToString(), (bloco1_top + 3 + y).ToString(), "14", " ", "B", "Body", "5.54792", "");

                Relatorio.CriarObjLabel("Fixo", "", "Volume(s)", bloco1ask_left.ToString(), (bloco1_top + 4.5 + y).ToString(), "14", " ", "L", "Body", "5.54792", "");
                Relatorio.CriarObjLabel("Fixo", "", dsFin.Tables["Query"].Rows[i]["NumeroVolumes"].ToString(), bloco1ans_left.ToString(), (bloco1_top + 4.5 + y).ToString(), "14", " ", "B", "Body", "5.54792", "");

                //Bloco2
                Relatorio.CriarObjLabel("Query", "Query", "dtPedido", bloco2_left.ToString(), (bloco2_top + y).ToString(), "14", " ", "B", "Body", "5.54792", "");
                Relatorio.CriarObjLabel("Fixo", "", dsFin.Tables["Query"].Rows[i]["dtNotaFiscal"].ToString(), bloco2_left.ToString(), (bloco2_top + 1.5 + y).ToString(), "14", " ", "B", "Body", "5.54792", "");

                //Bloco3  
                Relatorio.CriarObjLabel("Fixo", "", "Vendedor", bloco3ask_left.ToString(), (bloco3_top + y).ToString(), "14", " ", "L", "Body", "5.54792", "");
                Relatorio.CriarObjLabel("Fixo", "", dsFin.Tables["Query"].Rows[i]["Vendedor"].ToString(), bloco3ans_left.ToString(), (bloco3_top + y).ToString(), "14", " ", "B", "Body", "5.54792", "");

                Relatorio.CriarObjLabel("Fixo", "", "Cliente", bloco3ask_left.ToString(), (bloco3_top + 1.5 + y).ToString(), "14", " ", "L", "Body", "5.54792", "");
                Relatorio.CriarObjLabel("Fixo", "", dsFin.Tables["Query"].Rows[i]["Cliente"].ToString(), bloco3ans_left.ToString(), (bloco3_top + 1.5 + y).ToString(), "14", " ", "B", "Body", "5.54792", "");

                Relatorio.CriarObjLabel("Fixo", "", "Portador", bloco3ask_left.ToString(), (bloco3_top + 3 + y).ToString(), "14", " ", "L", "Body", "5.54792", "");
                Relatorio.CriarObjLabel("Fixo", "", dsFin.Tables["Query"].Rows[i]["Portador"].ToString(), bloco3ans_left.ToString(), (bloco3_top + 3 + y).ToString(), "14", " ", "B", "Body", "5.54792", "");

                Relatorio.CriarObjLabel("Fixo", "", "Documento", bloco3ask_left.ToString(), (bloco3_top + 4.5 + y).ToString(), "14", " ", "L", "Body", "5.54792", "");
                Relatorio.CriarObjLabel("Fixo", "", dsFin.Tables["Query"].Rows[i]["DocumentoPortador"].ToString(), bloco3ans_left.ToString(), (bloco3_top + 4.5 + y).ToString(), "14", " ", "B", "Body", "5.54792", "");
            
                //Bloco4
                Relatorio.CriarObjLabel("Fixo", "", "Favor retirar as mercadorias na expedição.", bloco4_left.ToString(), (bloco4_top + 6.5 + y).ToString(), "14", " ", "L", "Body", "15", "");
                Relatorio.CriarObjLinha(header_left.ToString(), (header_top + 9.9 + y).ToString(), TamLinha.ToString(), "", "Body");

                i++;
                y += 25.8;
            }

            Relatorio.CriarObjTabela("0.1", "10", "Query", false);
            //Coluna1
            Relatorio.CriarObjColunaNaTabela(" ", "TEST", false, "7", "Default", "10");
            Relatorio.CriarObjCelulaInColuna("Query", "TEST", "DetailGroup", "Null", "Null", 0, false, "10");
            Relatorio.TabelaEnd();                
            
            //Exporta 
            //Relatorio.CriarPDF_Excel("Protocolo de Retirada de Mercadoria_",1);
            Relatorio.Imprimir("Protocolo de Retirada de Mercadoria");
        }

        public void call_DemonstrativoCalculoICMSST()
        {
            string glb_nPedidoID = Convert.ToString(HttpContext.Current.Request.Params["glb_nPedidoID"]);
            string Title = "Demonstrativo de Cálculo de ICMS-ST";

            string strSQL1 = "SELECT " +
                            glb_nPedidoID + "AS 'PedidoID', " +
                            "Operacoes.Operacao AS Transacao, ISNULL(CONVERT(VARCHAR(11),NotasFiscais.NotaFiscal), 'não emitida') AS NotaFiscal, " +
                            "ISNULL(CONVERT(VARCHAR(10),NotasFiscais.dtNotaFiscal," + DATE_SQL_PARAM + "), SPACE(0)) AS Data, " +
                            "Pessoas.Nome, Pessoas.TipoPessoaID, CodigoLocalidade2 AS UF, " +
                            "(CASE Pessoas.TipoPessoaID WHEN 51 THEN dbo.fn_Pessoa_Documento(Pessoas.PessoaID, NULL, NULL, 101, 101, 0) " +
                                "WHEN 52 THEN dbo.fn_Pessoa_Documento(Pessoas.PessoaID, NULL, NULL, 111, 111, 0) ELSE SPACE(0) END) AS Documento, " +
                            "~dbo.fn_Pessoa_Incidencia(Pedidos.EmpresaID, -Pedidos.PessoaID) AS EhContribuinte " +
                        "FROM " +
                            "Pedidos WITH(NOLOCK) " +
                                "INNER JOIN Operacoes WITH(NOLOCK) ON Operacoes.OperacaoID = Pedidos.TransacaoID " +
                                "LEFT JOIN NotasFiscais WITH(NOLOCK) ON NotasFiscais.NotaFiscalID = Pedidos.NotaFiscalID AND NotasFiscais.EstadoID=67 " +
                                "INNER JOIN Pessoas WITH(NOLOCK) ON Pedidos.PessoaID = Pessoas.PessoaID " +
                                "INNER JOIN Pessoas_Enderecos WITH(NOLOCK) ON Pessoas_Enderecos.PessoaID = Pessoas.PessoaID " +
                                "INNER JOIN Localidades WITH(NOLOCK) ON Localidades.LocalidadeID = Pessoas_Enderecos.UFID " +
                        "WHERE " +
                            "PedidoS.PedidoID = " + glb_nPedidoID;

            string strSQL2 = "SELECT " +
                                    glb_nPedidoID + "AS 'PedidoID', " + 
                                    "(SELECT COUNT(1) FROM Pedidos_Itens WITH(NOLOCK) WHERE PedidoID = " + glb_nPedidoID + " AND dbo.fn_PedidoItem_STGuiaRecolhimento(PeditemID) = 0) AS QtdSemGareGNRE, " +
                                    "(SELECT COUNT(1) FROM Pedidos_Itens WITH(NOLOCK) WHERE PedidoID = " + glb_nPedidoID + " AND dbo.fn_PedidoItem_STGuiaRecolhimento(PeditemID) = 1) AS QtdComGare, " +
                                    "(SELECT COUNT(1) FROM Pedidos_Itens WITH(NOLOCK) WHERE PedidoID = " + glb_nPedidoID + " AND dbo.fn_PedidoItem_STGuiaRecolhimento(PeditemID) = 2) AS QtdComGNRE, " +
                                    "dbo.fn_Pedido_ICMSSTMensagem(" + glb_nPedidoID + ",0) as ICMSSTMensagem1, " +
                                    "dbo.fn_Pedido_ICMSSTMensagem(" + glb_nPedidoID + ",1) as ICMSSTMensagem2, " +
                                    "dbo.fn_Pedido_ICMSSTMensagem(" + glb_nPedidoID + ",2) as ICMSSTMensagem3 ";


            arrSqlQuery = new string[,] { { "strSQL1", strSQL1 },
                                          { "strSQL2", strSQL2 }};

            Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape");

            DataSet dsFin = new DataSet();
            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            int EhRES = 0;
            string tipoResultado = "0";
            string mensagem1 = "Produtos sem GARE/GNRE";
            string mensagemQuery = "ICMSSTMensagem1";

            string uf = dsFin.Tables["strSQL1"].Rows[0]["UF"].ToString();
            string ehContribuinte = dsFin.Tables["strSQL1"].Rows[0]["EhContribuinte"].ToString();

            if (uf == "MT" && ehContribuinte == "True")
                EhRES = 1;

            if (Convert.ToInt32(dsFin.Tables["strSQL2"].Rows[0]["QtdComGare"]) >= 1)
            {
                tipoResultado = "1";
                mensagem1 = "Produtos com GARE";
                mensagemQuery = "ICMSSTMensagem2";
            }

            if (Convert.ToInt32(dsFin.Tables["strSQL2"].Rows[0]["QtdComGNRE"]) >= 1)
            {
                tipoResultado = "2";

                if (EhRES == 0)
                    mensagem1 = "Produtos com GNRE";
                else
                    mensagem1 = "Produtos com DAR-1";

                mensagemQuery = "ICMSSTMensagem3";
            }

            string parametro = "103";

            if (sLinguaLogada != "246")
                parametro = "101";

            string strSQL3 = "";
            if (Formato == 1)
            {
                strSQL3 = " CREATE TABLE #Pedido_DemonstrativoCalculoICMSST " +
                                        " (PedidoID INT, ID INT, Produto VARCHAR(40), Finalidade VARCHAR(50), NCM VARCHAR(40), IVAST NUMERIC(11,2), BaseCalculo NUMERIC(5,2), ALQInter NUMERIC(11,2), " +
                                        " ALQIntra NUMERIC(11, 2), IVASTAj NUMERIC(11, 2), CFOP VARCHAR(6), Quantidade INT, ValorTotal NUMERIC(11, 2), ValorTotalComIPI NUMERIC(11, 2), " +
                                        " BCICMS NUMERIC(11, 2), ICMS NUMERIC(11, 2), BCICMSST NUMERIC(11, 2), ST_Unit NUMERIC(13, 4), ICMSST NUMERIC(11, 2), CargaMedia NUMERIC(5, 2)) " +

                                    " INSERT INTO #Pedido_DemonstrativoCalculoICMSST " +
                                        " EXEC sp_Pedido_DemonstrativoCalculoICMSST " + glb_nPedidoID + ", " + tipoResultado +

                                    " SELECT " +
                                        " PedidoID, ID, Produto, Finalidade, NCM, dbo.fn_Formata_Numero(IVAST, 2, " + parametro + ") AS IVAST, " +
                                        " dbo.fn_Formata_Numero(BaseCalculo, 2, " + parametro + ") AS BaseCalculo, dbo.fn_Formata_Numero(ALQInter, 2, " + parametro + ") AS ALQInter, " +
                                        " dbo.fn_Formata_Numero(ALQIntra, 2, " + parametro + ") AS ALQIntra, dbo.fn_Formata_Numero(IVASTAj, 2, " + parametro + ") AS IVASTAj, " +
                                        " CFOP, Quantidade, dbo.fn_Formata_Numero(ValorTotal, 2, " + parametro + ") AS ValorTotal, " +
                                        " dbo.fn_Formata_Numero(ValorTotalComIPI, 2, " + parametro + ") AS ValorTotalComIPI, dbo.fn_Formata_Numero(BCICMS, 2, " + parametro + ") AS BCICMS, " +
                                        " dbo.fn_Formata_Numero(ICMS, 2, " + parametro + ") AS ICMS, dbo.fn_Formata_Numero(BCICMSST, 2, " + parametro + ") AS BCICMSST, " +
                                        " dbo.fn_Formata_Numero(ST_Unit, 4, " + parametro + ") AS ST_Unit, dbo.fn_Formata_Numero(ICMSST, 2, " + parametro + ") AS ICMSST, " +
                                        " dbo.fn_Formata_Numero(CargaMedia, 2, " + parametro + ") AS CargaMedia" +
                                    " FROM " +
                                        " #Pedido_DemonstrativoCalculoICMSST " +

                                    " DROP TABLE #Pedido_DemonstrativoCalculoICMSST ";
            }
            else if(Formato == 2)
            {
                strSQL3 = " CREATE TABLE #Pedido_DemonstrativoCalculoICMSST " +
                                        " (PedidoID INT, ID INT, Produto VARCHAR(40), Finalidade VARCHAR(50), NCM VARCHAR(40), IVAST NUMERIC(11,2), BaseCalculo NUMERIC(5,2), ALQInter NUMERIC(11,2), " +
                                        " ALQIntra NUMERIC(11, 2), IVASTAj NUMERIC(11, 2), CFOP VARCHAR(6), Quantidade INT, ValorTotal NUMERIC(11, 2), ValorTotalComIPI NUMERIC(11, 2), " +
                                        " BCICMS NUMERIC(11, 2), ICMS NUMERIC(11, 2), BCICMSST NUMERIC(11, 2), ST_Unit NUMERIC(13, 4), ICMSST NUMERIC(11, 2), CargaMedia NUMERIC(5, 2)) " +

                                    " INSERT INTO #Pedido_DemonstrativoCalculoICMSST " +
                                        " EXEC sp_Pedido_DemonstrativoCalculoICMSST " + glb_nPedidoID + ", " + tipoResultado +

                                    " SELECT " +
                                        " PedidoID, ID, Produto, Finalidade, NCM, IVAST, BaseCalculo, ALQInter, ALQIntra, IVASTAj, " +
                                        " CFOP, Quantidade, ValorTotal, ValorTotalComIPI, BCICMS, ICMS, BCICMSST, ST_Unit, ICMSST, CargaMedia " +
                                    " FROM " +
                                        " #Pedido_DemonstrativoCalculoICMSST " +

                                    " DROP TABLE #Pedido_DemonstrativoCalculoICMSST ";
            }
            
            string strSQL4 = " CREATE TABLE #Pedido_DemonstrativoCalculoICMSST " +
                                    " (PedidoID INT, ID INT, Produto VARCHAR(40), Finalidade VARCHAR(50), NCM VARCHAR(40), IVAST NUMERIC(11,2), BaseCalculo NUMERIC(5,2), ALQInter NUMERIC(11,2), " +
                                    " ALQIntra NUMERIC(11, 2), IVASTAj NUMERIC(11, 2), CFOP VARCHAR(6), Quantidade INT, ValorTotal NUMERIC(11, 2), ValorTotalComIPI NUMERIC(11, 2), " +
                                    " BCICMS NUMERIC(11, 2), ICMS NUMERIC(11, 2), BCICMSST NUMERIC(11, 2), ST_Unit NUMERIC(13, 4), ICMSST NUMERIC(11, 2), CargaMedia NUMERIC(5, 2)) " +

                                " INSERT INTO #Pedido_DemonstrativoCalculoICMSST " +
                                    " EXEC sp_Pedido_DemonstrativoCalculoICMSST " + glb_nPedidoID + ", " + tipoResultado +

                                " SELECT " +
                                    " 'Totais' AS 'Totais', " +
                                    " SUM(Quantidade) AS 'Quantidade', dbo.fn_Formata_Numero(SUM(ValorTotal), 2, " + parametro + ") AS 'ValorTotal', " +
                                    " dbo.fn_Formata_Numero(SUM(ValorTotalComIPI), 2, " + parametro + ") AS 'ValorTotalComIPI', dbo.fn_Formata_Numero(SUM(BCICMS), 2, " + parametro + ") AS 'BCICMS', " +
                                    " dbo.fn_Formata_Numero(SUM(ICMS), 2, " + parametro + ") AS 'ICMS', dbo.fn_Formata_Numero(SUM(BCICMSST), 2, " + parametro + ") AS 'BCICMSST', " +
                                    " dbo.fn_Formata_Numero(SUM(ICMSST), 2, " + parametro + ") AS 'ICMSST' " +
                                " FROM " +
                                    " #Pedido_DemonstrativoCalculoICMSST " +

                                " DROP TABLE #Pedido_DemonstrativoCalculoICMSST ";

            arrSqlQuery = new string[,] { { "strSQL1", strSQL1 },
                                          { "strSQL2", strSQL2 },
                                          { "strSQL3", strSQL3 },
                                          { "strSQL4", strSQL4 }};                                                    


            Relatorio = new ClsReport();


            if (Formato == 1)
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape", "1.6", "Default", "Default", true);
            else
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape");


            dsFin = new DataSet();
            dsFin = (DataSet)HttpContext.Current.Session["Query"];
            

            string sParams = "Pedido " + glb_nPedidoID + "      " + dsFin.Tables["strSQL1"].Rows[0]["Transacao"] + "      NF " + dsFin.Tables["strSQL1"].Rows[0]["NotaFiscal"] + "      " +
                             dsFin.Tables["strSQL1"].Rows[0]["Data"] + "      " + dsFin.Tables["strSQL1"].Rows[0]["Nome"] + "      " + dsFin.Tables["strSQL1"].Rows[0]["UF"] +
                             "      CNPJ " + dsFin.Tables["strSQL1"].Rows[0]["Documento"];


            if (Formato == 1) // Se PDF
            {
                double headerX = 0.2;
                double headerY = 0.2;
                double tabelaTitulo = 1.2;

                double tabelaX = 0.2;
                double tabelaY = 0;

                double bodyX = 0.2;
                double bodyY = 0.0;

                //Header
                Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "12", "black", "B", "Header", "5", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 10).ToString(), headerY.ToString(), "12", "#0113a0", "B", "Header", "9", "");
                Relatorio.CriarObjLabelData("Now", "", "", (headerX + 23).ToString(), headerY.ToString(), "10", "black", "B", "Header", "3.72187", "Horas");
                Relatorio.CriarNumeroPagina((headerX + 27).ToString(), (headerY).ToString(), "10", "Black", "B", false);

                //Header Parametros
                Relatorio.CriarObjLabel("Fixo", "", sParams, (headerX).ToString(), (headerY+0.5).ToString(), "8", "black", "B", "Header", "29", "");

                Relatorio.CriarObjLinha((headerX).ToString(), (headerY + 0.9).ToString(), "29", "", "Header");

                //Header Titulo da Tabela
                Relatorio.CriarObjLabel("Fixo", "", "ID", (headerX).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "Produto", (headerX+1.2).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "Finalidade", (headerX+3.7).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "NCM", (headerX+6.2).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "IVA-ST", (headerX+8.15).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "Base", (headerX+9.65).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "ALQ-Inter", (headerX+10.74).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "ALQ-Intra", (headerX+12.35).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");

                if (tipoResultado == "2") // Tem GNRE
                {
                    if (EhRES == 1)
                    {
                        Relatorio.CriarObjLabel("Fixo", "", "C. Média", (headerX+13.93).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                    }
                    else
                    {
                        Relatorio.CriarObjLabel("Fixo", "", "IVA-ST Aj", (headerX+13.9).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                    }
                }
                else
                {
                    Relatorio.CriarObjLabel("Fixo", "", "IVA-ST Aj", (headerX + 13.9).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                }

                Relatorio.CriarObjLabel("Fixo", "", "CFOP", (headerX + 15.18).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "Quant", (headerX+16).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "Vl Total", (headerX+17.55).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "Vl Total c/ IPI", (headerX+18.95).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "BCICMS", (headerX+21).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "ICMS", (headerX+22.7).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "BCICMS-ST", (headerX+23.82).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "ICMS-ST Unit", (headerX+25.5).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");
                Relatorio.CriarObjLabel("Fixo", "", "ICMS-ST Tot", (headerX+27.5).ToString(), (tabelaTitulo).ToString(), "7", "black", "B", "Header", "15", "");

                //Mensagens
                Relatorio.CriarObjLabel("Fixo", "", mensagem1, (headerX).ToString(), (bodyY + 0.4).ToString(), "9", "black", "B", "Body", "29", "", false, true);
                Relatorio.CriarObjLabel("Query", "strSQL2", mensagemQuery, (headerX + 8).ToString(), (bodyY + 0.4).ToString(), "7", "black", "B", "Body", "21", "", false, true);

                //Tabela
                Relatorio.CriarObjTabela((tabelaX).ToString(), (tabelaY+1).ToString(), "strSQL3", false);

                Relatorio.CriarObjColunaNaTabela(" ", "ID", false, "1.2", "Default","1","0");
                Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup","Null","Null",0,false,"7");

                Relatorio.CriarObjColunaNaTabela(" ", "Produto", false, "2.5", "Default", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "Finalidade", false, "2.5", "Default", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "Finalidade", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "NCM", false, "1.6", "Default", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "NCM", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "IVAST", false, "1.3", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "IVAST", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "BaseCalculo", false, "1.3", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "BaseCalculo", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "ALQInter", false, "1.6", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "ALQInter", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "ALQIntra", false, "1.6", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "ALQIntra", "DetailGroup", "Null", "Null", 0, false, "7");

                if (tipoResultado == "2") // Tem GNRE
                {
                    if (EhRES == 1)
                    {
                        Relatorio.CriarObjColunaNaTabela(" ", "CargaMedia", false, "1.5", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "CargaMedia", "DetailGroup", "Null", "Null", 0, false, "7");
                    }
                    else
                    {
                        Relatorio.CriarObjColunaNaTabela(" ", "IVASTAj", false, "1.5", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "IVASTAj", "DetailGroup", "Null", "Null", 0, false, "7");
                    }
                }
                else
                {
                    Relatorio.CriarObjColunaNaTabela(" ", "IVASTAj", false, "1.5", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "IVASTAj", "DetailGroup", "Null", "Null", 0, false, "7");
                }

                Relatorio.CriarObjColunaNaTabela(" ", "CFOP", false, "0.9", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "CFOP", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "Quantidade", false, "0.9", "Center", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "ValorTotal", false, "1.7", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "ValorTotalComIPI", false, "2", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "ValorTotalComIPI", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "BCICMS", false, "1.5", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "BCICMS", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "ICMS", false, "1.4", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "ICMS", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "BCICMSST", false, "1.8", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "BCICMSST", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "ST_Unit", false, "1.8", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "ST_Unit", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "ICMSST", false, "2", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "ICMSST", "DetailGroup", "Null", "Null", 0, false, "7");

                Relatorio.TabelaEnd();


                //Linha com totais
                Relatorio.CriarObjTabela((tabelaX).ToString(), (tabelaY + 1.01).ToString(), "strSQL4", false);

                Relatorio.CriarObjColunaNaTabela(" ", "Totais", false, "16", "Default", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "Totais", "DetailGroup", "Null", "Null", 0, true, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "Quantidade", false, "0.9", "Center", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup", "Null", "Null", 0, true, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "ValorTotal", false, "1.7", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup", "Null", "Null", 0, true, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "ValorTotalComIPI", false, "2", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "ValorTotalComIPI", "DetailGroup", "Null", "Null", 0, true, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "BCICMS", false, "1.5", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "BCICMS", "DetailGroup", "Null", "Null", 0, true, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "ICMS", false, "1.4", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "ICMS", "DetailGroup", "Null", "Null", 0, true, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "BCICMSST", false, "1.8", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "BCICMSST", "DetailGroup", "Null", "Null", 0, true, "7");

                Relatorio.CriarObjColunaNaTabela(" ", "ICMSST", false, "3.8", "Right", "1", "0");
                Relatorio.CriarObjCelulaInColuna("Query", "ICMSST", "DetailGroup", "Null", "Null", 0, true, "7");

                Relatorio.TabelaEnd();


                //Demonstrativo
                Relatorio.CriarObjLinha((bodyX).ToString(), (bodyY + 1.51).ToString(), "29", "", "Body");

                Relatorio.CriarObjLabel("Fixo", "", "Legenda:", (bodyX).ToString(), (bodyY + 1.85).ToString(), "7", "black", "B", "Body", "15", "");

                if (EhRES != 1)
                {
                    Relatorio.CriarObjLabel("Fixo", "", "IVA - ST Aj = [(1 + IVA - ST) * (1 - (ALQ - Inter * Base)) / (1 - (ALQ - Intra * Base)] - 1", (bodyX).ToString(), (bodyY + 2.35).ToString(), "7", "black", "B", "Body", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "BC ICMS-ST = [((Vl Total * Base) + IPI)  * (1 + IVA-ST Aj)]", (bodyX).ToString(), (bodyY + 2.85).ToString(), "7", "black", "B", "Body", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "ICMS-ST = [(BC ICMS-ST) * (ALQ-Intra)] - ICMS", (bodyX).ToString(), (bodyY + 3.35).ToString(), "7", "black", "B", "Body", "15", "");
                }
                else
                {
                    Relatorio.CriarObjLabel("Fixo", "", "BC ICMS-ST = [(ICMS + ICMS-ST Tot) / (ALQ-Intra)]", (bodyX).ToString(), (bodyY + 2.35).ToString(), "7", "black", "B", "Body", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "ICMS-ST = [(Vl Total c/ IPI) * (C. Média)]", (bodyX).ToString(), (bodyY + 2.85).ToString(), "7", "black", "B", "Body", "15", "");
                }
            }

            
            else if (Formato == 2) // Se Excel
            {
                double headerX = 0;
                double headerY = 0;

                double tabelaX = 0;
                double tabelaY = 0.35;

                //Header
                Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "8", "black", "B", "Body", "3.5", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 8.5).ToString(), headerY.ToString(), "8", "#0113a0", "B", "Body", "6", "");
                Relatorio.CriarObjLabelData("Now", "", "", (headerX + 18.3).ToString(), headerY.ToString(), "8", "black", "B", "Body", "3.72187", "Horas");

                Relatorio.CriarObjLabel("Fixo", "", "PedidoID " + glb_nPedidoID, (headerX).ToString(), (headerY+0.01).ToString(), "8", "black", "L", "Body", "3.5", "");


                //Tabela
                Relatorio.CriarObjTabela((tabelaX).ToString(), (tabelaY).ToString(), "strSQL3", false);

                Relatorio.CriarObjColunaNaTabela("ID", "ID", false, "3.5", "Default","8");
                Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", "Totais", "TotalGroup", "", "DetailGroup", 1, true, "8");

                Relatorio.CriarObjColunaNaTabela("Produto", "Produto", false, "5", "Default");
                Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                Relatorio.CriarObjColunaNaTabela("Finalidade", "Finalidade", false, "6", "Default");
                Relatorio.CriarObjCelulaInColuna("Query", "Finalidade", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                Relatorio.CriarObjColunaNaTabela("NCM", "NCM", false, "2.5", "Default");
                Relatorio.CriarObjCelulaInColuna("Query", "NCM", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                Relatorio.CriarObjColunaNaTabela("IVAST", "IVAST", false, "1.3", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "IVAST", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                Relatorio.CriarObjColunaNaTabela("BaseCalculo", "BaseCalculo", false, "3.72187", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "BaseCalculo", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                Relatorio.CriarObjColunaNaTabela("ALQInter", "ALQInter", false, "2.6", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "ALQInter", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                Relatorio.CriarObjColunaNaTabela("ALQIntra", "ALQIntra", false, "2.6", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "ALQIntra", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                Relatorio.CriarObjColunaNaTabela("IVA-ST Aj", "IVASTAj", false, "2.5", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "IVASTAj", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                Relatorio.CriarObjColunaNaTabela("CFOP", "CFOP", false, "1.9", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "CFOP", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                Relatorio.CriarObjColunaNaTabela("Quantidade", "Quantidade", false, "3.5", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("ValorTotal", "ValorTotal", false, "3", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("ValorTotalComIPI", "ValorTotalComIPI", false, "3.8", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "ValorTotalComIPI", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Query", "ValorTotalComIPI", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("BCICMS", "BCICMS", false, "3", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "BCICMS", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Query", "BCICMS", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("ICMS", "ICMS", false, "3", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "ICMS", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Query", "ICMS", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("BCICMSST", "BCICMSST", false, "3", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "BCICMSST", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Query", "BCICMSST", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("ST_Unit", "ST_Unit", false, "3", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "ST_Unit", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                Relatorio.CriarObjColunaNaTabela("ICMSST", "ICMSST", false, "3", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "ICMSST", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Query", "ICMSST", "TotalGroup", "", "DetailGroup", 0, true, "8");

                Relatorio.CriarObjColunaNaTabela("C.Média ", "CargaMedia", false, "2.5", "Right");
                Relatorio.CriarObjCelulaInColuna("Query", "CargaMedia", "DetailGroup", "Null", "Null", 0, false, "8");
                Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);
                

                Relatorio.TabelaEnd();
            }


            Relatorio.CriarPDF_Excel(Title, Formato);
        }


        public void relatorioFabricantes()
        {
            string glb_nPedidoID = Convert.ToString(HttpContext.Current.Request.Params["glb_nPedidoID"]);
            int aEmpresaData0 = Convert.ToInt32(HttpContext.Current.Request.Params["aEmpresaData0"]);
            string sNomePessoa = Convert.ToString(HttpContext.Current.Request.Params["sNomePessoa"]);
            string nNotaFiscal = Convert.ToString(HttpContext.Current.Request.Params["nNotaFiscal"]);
            string sDataNF = Convert.ToString(HttpContext.Current.Request.Params["sDataNF"]);
            string glb_sPaisEmpresa = Convert.ToString(HttpContext.Current.Request.Params["glb_sPaisEmpresa"]);
            string strCustomerPO = Convert.ToString(HttpContext.Current.Request.Params["strCustomerPO"]);
            string glb_TotalGrossSKID = Convert.ToString(HttpContext.Current.Request.Params["glb_TotalGrossSKID"]);
            bool chkWS = Convert.ToBoolean(HttpContext.Current.Request.Params["chkWS"]);


            string Title = "PACKING LIST";

            string strSQL1 = " SELECT dbo.fn_Pedido_RelatorioFabricante(" + glb_nPedidoID + ") AS Imprime ";

            string strSQL2 = " SELECT " +
                                " UPPER(Empresa.Nome) AS Nome, " +
                                " ISNULL(Enderecos.Numero + SPACE(1), SPACE(0)) + ISNULL(Enderecos.Endereco + SPACE(1), SPACE(0)) + " +
                                " ISNULL(Enderecos.Complemento + SPACE(1), SPACE(0)) + ISNULL(Enderecos.Bairro + SPACE(1), SPACE(0)) AS End1, " +
                                " ISNULL(Cidades.Localidade + SPACE(1), SPACE(0)) + ISNULL(UFs.CodigoLocalidade2 + SPACE(1), SPACE(0)) + ISNULL(Paises.CodigoLocalidade3 + SPACE(1), SPACE(0)) AS End2, " +
                                " ISNULL(Enderecos.CEP, SPACE(0)) AS CEP, " +
                                " dbo.fn_Pessoa_Telefone(Empresa.PessoaID, 119, 119, 5, 0, NULL) AS Telefone, " +
                                " dbo.fn_Pessoa_Telefone(Empresa.PessoaID, 122, 122, 5, 0, NULL) AS Fax, " +
                                " SUBSTRING(dbo.fn_Pessoa_URL(Empresa.PessoaID, 125, NULL), 8, 72) AS URL, " +
                                " dbo.fn_Pessoa_URL(Empresa.PessoaID, 124, NULL) AS EMail " +
                            " FROM " +
                                " Pessoas Empresa WITH(NOLOCK) " +
                                    " INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON(Empresa.PessoaID = Enderecos.PessoaID) " +
                                    " LEFT OUTER JOIN Localidades Cidades WITH(NOLOCK) ON(Enderecos.CidadeID = Cidades.LocalidadeID) " +
                                    " LEFT OUTER JOIN Localidades UFs WITH(NOLOCK) ON(Enderecos.UFID = UFs.LocalidadeID) " +
                                    " LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON(Enderecos.PaisID = Paises.LocalidadeID) " +
                            " WHERE (Empresa.PessoaID = " + aEmpresaData0 + ") ";

            string strSQL3 = " SELECT " +
                                glb_nPedidoID + " AS _PedidoID, Imagens.Arquivo AS FotoEmpresa " +
                            " FROM " +
                                " [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] Imagens WITH(NOLOCK) " +
                            " WHERE (Imagens.FormID = 1210 AND Imagens.SubFormID = 20100 AND Imagens.TipoArquivoID = 1451 AND Imagens.RegistroID = " + glb_nEmpresaID + ") ";

            string strSQL4 = " DECLARE @Resultado VARCHAR(8000) " +
                             " CREATE TABLE #Pedido_Fabricantes(PedidoID_ INT, PedFabricanteID_ INT, ProdutoID INT, Produto VARCHAR(35), Modelo VARCHAR(18), QuantidadeTotal INT, Quantidade INT, " +
                                                    "FabricanteID INT, Fabricante VARCHAR(40), Pais VARCHAR(30), PesoBruto NUMERIC(9, 4), PesoLiquido NUMERIC(9, 4), TotalPesoLiquido NUMERIC(9, 4)) " +

                             " INSERT INTO #Pedido_Fabricantes(PedidoID_, PedFabricanteID_, ProdutoID, Produto, Modelo, QuantidadeTotal, " +
                                                               " Quantidade, FabricanteID, Fabricante, Pais, PesoBruto, PesoLiquido, TotalPesoLiquido) " +

                             " EXEC sp_Pedido_Fabricantes " + glb_nPedidoID + ", NULL, NULL, NULL, 1, @Resultado OUTPUT " +

                             " SELECT " +
                                " PedidoID_, PedFabricanteID_, ProdutoID, Produto, Modelo, QuantidadeTotal, Quantidade, FabricanteID, Fabricante, Pais, " +
                                " REPLACE(PesoBruto, '.', ',') AS PesoBruto, REPLACE(PesoLiquido,'.', ',') AS PesoLiquido, REPLACE(TotalPesoLiquido,'.', ',') AS TotalPesoLiquido, " +
                                " (SELECT REPLACE(SUM(TotalPesoLiquido), '.', ',') FROM #Pedido_Fabricantes WITH(NOLOCK)) AS TotalPesoLiquidoSUM " +
                             " FROM " +
                                " #Pedido_Fabricantes WITH(NOLOCK) " +
                             " GROUP BY " +
                                " PedidoID_, PedFabricanteID_, ProdutoID, Produto, Modelo, QuantidadeTotal, Quantidade, FabricanteID, Fabricante,Pais, PesoBruto, PesoLiquido, TotalPesoLiquido " +
                             " ORDER BY Produto " +

                            " DROP TABLE #Pedido_Fabricantes ";

            string strSQL5 = " SELECT " +
                                glb_nPedidoID + " AS PedidoID_, b.PessoaID AS FabricanteID, b.Nome AS Fabricante, " +
                                " (ISNULL(c.Numero + SPACE(1), SPACE(0)) + ISNULL(c.Endereco + SPACE(1), SPACE(0)) + " +
                                " ISNULL(c.Complemento + SPACE(1), SPACE(0)) + ISNULL(c.Bairro + SPACE(1), SPACE(0)) + ISNULL(c.CEP + SPACE(1), SPACE(0))) AS Endereco, " +
                                " ISNULL(d.NomeInternacional, d.Localidade) AS Cidade, e.NomeInternacional AS Pais " +
                            " FROM " +
                                " Pedidos_Fabricantes a WITH(NOLOCK) " +
                                    " INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.FabricanteID) " +
                                    " INNER JOIN Pessoas_Enderecos c WITH(NOLOCK) ON (c.PessoaID = b.PessoaID) " +
                                    " INNER JOIN Localidades d WITH(NOLOCK) ON (d.LocalidadeID = c.CidadeID) " +
                                    " INNER JOIN Localidades e WITH(NOLOCK) ON (e.LocalidadeID = c.PaisID) " +
                            " WHERE " +
                                " a.PedidoID = " + glb_nPedidoID + " AND c.EndFaturamento = 1 AND c.Ordem = 1 " +
                            " GROUP BY " +
                                " b.PessoaID, b.Nome, c.Numero, c.Endereco, c.Complemento, c.Bairro, c.CEP, d.NomeInternacional, d.Localidade, e.NomeInternacional " +
                            " ORDER BY b.PessoaID ";


            arrSqlQuery = new string[,] { { "strSQL1", strSQL1 },
                                          { "strSQL2", strSQL2 },
                                          { "strSQL3", strSQL3 },
                                          { "strSQL4", strSQL4 },
                                          { "strSQL5", strSQL5 }};

            Relatorio.CriarRelatório(Title, arrSqlQuery, "USA", "Portrait", "2.35","Default","Default",false,"2.4");

            DataSet dsFin = new DataSet();
            dsFin = (DataSet)HttpContext.Current.Session["Query"];
            
            bool bCanPrint = false;

            if (Convert.ToInt32(dsFin.Tables["strSQL1"].Rows[0]["Imprime"]) == 1)
                bCanPrint = true;

            if (!bCanPrint)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Dados dos fabricantes incompletos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {
                string End2 = dsFin.Tables["strSQL2"].Rows[0]["End2"].ToString();

                string sCEP = dsFin.Tables["strSQL2"].Rows[0]["CEP"].ToString();
                sCEP = sCEP.Substring(0, 5) + '-' + sCEP.Substring(5);


                string sTelefone = dsFin.Tables["strSQL2"].Rows[0]["Telefone"].ToString();
                string sFax = dsFin.Tables["strSQL2"].Rows[0]["Fax"].ToString();

                sTelefone = "Phone " + sTelefone.Substring(0, 3) + "-" + sTelefone.Substring(3, (sTelefone).Length - 7) + "-" + sTelefone.Substring(3 + (sTelefone).Length - 7);
                sFax = "Fax " + sFax.Substring(0, 3) + "-" + sFax.Substring(3, (sFax).Length - 7) + "-" + sFax.Substring(3 + (sFax).Length - 7);


                string sURL = dsFin.Tables["strSQL2"].Rows[0]["URL"].ToString();
                string sEMail = dsFin.Tables["strSQL2"].Rows[0]["EMail"].ToString();

                string strInvoice = " ";
                string strInvoiceDate = " ";
                if (nNotaFiscal != "")
                {
                    strInvoice = "Invoice: " + nNotaFiscal;
                    strInvoiceDate = sDataNF;
                }

                string TotalPesoLiquidoSUM = dsFin.Tables["strSQL4"].Rows[0]["TotalPesoLiquidoSUM"].ToString();


                if (Formato == 1)
                {
                    double headerX = 0.9;
                    double headerY = 0.75;

                    double bodyX = 0.9;
                    double bodyY = 0;

                    double footerX = 0.9;
                    double footerY = 0;

                    //Header
                    Relatorio.CriarObjImage("Query", "strSQL3", "FotoEmpresa", headerX.ToString(), headerY.ToString(), "Header", "5.265208", "1.322916");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 15.8).ToString(), (headerY+0.7).ToString(), "16", "black", "B", "Header", "10", "");
                    Relatorio.CriarObjLinha((headerX).ToString(), (headerY + 1.45).ToString(), "19.8", "", "Header");
                    
                    //Informações da Allplus
                    Relatorio.CriarObjLabel("Query", "strSQL2", "Nome", (bodyX).ToString(), (bodyY).ToString(), "12", "black", "B", "Body", "10", "");
                    Relatorio.CriarObjLabel("Query", "strSQL2", "End1", (bodyX).ToString(), (bodyY+0.5).ToString(), "8", "black", "B", "Body", "10", "");
                    Relatorio.CriarObjLabel("Fixo", "", End2 + " " + sCEP, (bodyX).ToString(), (bodyY + 0.9).ToString(), "8", "black", "B", "Body", "10", "");
                    Relatorio.CriarObjLabel("Fixo", "", sTelefone + "     " + sFax, (bodyX).ToString(), (bodyY + 1.3).ToString(), "8", "black", "B", "Body", "10", "");
                    Relatorio.CriarObjLabel("Fixo", "", sURL + "     " + sEMail, (bodyX).ToString(), (bodyY + 1.7).ToString(), "8", "black", "B", "Body", "10", "");


                    Relatorio.CriarObjLinha((bodyX).ToString(), (bodyY + 2.2).ToString(), "19.8", "", "Body");

                    //Informações do Pedido //Linha1
                    Relatorio.CriarObjLabel("Fixo", "", "Order: ", (bodyX).ToString(), (bodyY + 2.25).ToString(), "8", "black", "B", "Body", "1", "");
                    Relatorio.CriarObjLabel("Query", "strSQL5", "PedidoID_", (bodyX+0.97).ToString(), (bodyY+2.25).ToString(), "8", "black", "B", "Body", "5", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Customer: " + sNomePessoa, (bodyX+6).ToString(), (bodyY + 2.25).ToString(), "8", "black", "B", "Body", "9", "");

                    Relatorio.CriarObjLabel("Fixo", "", strInvoice, (bodyX+14).ToString(), (bodyY + 2.25).ToString(), "8", "black", "B", "Body", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", strInvoiceDate, (bodyX + 18.35).ToString(), (bodyY + 2.25).ToString(), "8", "black", "B", "Body", "1.5", "");

                    //Informações do Pedido //Linha2
                    Relatorio.CriarObjLabel("Fixo", "", "Country of purchase: " + glb_sPaisEmpresa, (bodyX).ToString(), (bodyY + 2.6).ToString(), "8", "black", "B", "Body", "6", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Country originated from: " + glb_sPaisEmpresa, (bodyX+6).ToString(), (bodyY + 2.6).ToString(), "8", "black", "B", "Body", "8", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Customer PO: " + strCustomerPO, (bodyX+14).ToString(), (bodyY + 2.6).ToString(), "8", "black", "B", "Body", "5.4", "");


                    Relatorio.CriarObjLinha((bodyX).ToString(), (bodyY + 3).ToString(), "19.8", "", "Body");

                    
                    //Tabela
                    Relatorio.CriarObjTabela((bodyX).ToString(), (bodyY + 3.1).ToString(), "strSQL4", false);

                        Relatorio.CriarObjColunaNaTabela("ID  ", "ProdutoID", true, "1.2", "Right");
                        Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("  Product", "Produto", false, "6.2", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Model", "Modelo", false, "3.1", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Modelo", "DetailGroup");
                    
                        Relatorio.CriarObjColunaNaTabela("Qty", "Quantidade", false, "1.2", "Right");
                        Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Net Kg", "PesoLiquido", true, "1.5", "Right");
                        Relatorio.CriarObjCelulaInColuna("Query", "PesoLiquido", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Gross Kg*", "PesoBruto", true, "2.1", "Right");
                        Relatorio.CriarObjCelulaInColuna("Query", "PesoBruto", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("ID  ", "FabricanteID", true, "1.2", "Right");
                        Relatorio.CriarObjCelulaInColuna("Query", "FabricanteID", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Manufactorer", "Fabricante", false, "4.4", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Fabricante", "DetailGroup");

                    Relatorio.TabelaEnd();

                    //Observação no fim da tabela
                    Relatorio.CriarObjLabel("Fixo", "", "*The weight is without SKID.", (bodyX).ToString(), (bodyY + 3.6).ToString(), "6", "black", "L", "Body", "5.4", "");


                    Relatorio.CriarObjLinha((bodyX).ToString(), (bodyY + 4).ToString(), "19.8", "", "Body");

                    //Totais
                    Relatorio.CriarObjLabel("Fixo", "", "Total Net Kg:  " + TotalPesoLiquidoSUM, (bodyX+1.8).ToString(), (bodyY + 4.15).ToString(), "8", "black", "B", "Body", "9", "");

                    if (chkWS)
                        Relatorio.CriarObjLabel("Fixo", "", "Total Gross Kg (with SKID):  " + glb_TotalGrossSKID, (bodyX + 11.25).ToString(), (bodyY + 4.15).ToString(), "8", "black", "B", "Body", "9", "");
                    else
                        Relatorio.CriarObjLabel("Fixo", "", "Total Gross Kg:  " + glb_TotalGrossSKID, (bodyX + 11.25).ToString(), (bodyY + 4.15).ToString(), "8", "black", "B", "Body", "9", "");

                    Relatorio.CriarObjLinha((bodyX).ToString(), (bodyY + 4.65).ToString(), "19.8", "", "Body");

                    //Manufactorer Details
                    Relatorio.CriarObjLabel("Fixo", "", "Manufactorer Details", (bodyX).ToString(), (bodyY + 5).ToString(), "12", "black", "B", "Body", "10", "");

                    //Tabela do Manufactorer Details
                    Relatorio.CriarObjTabela((bodyX).ToString(), (bodyY + 5.9).ToString(), "strSQL5", false);

                        Relatorio.CriarObjColunaNaTabela("ID  ", "FabricanteID", true, "1.2", "Right");
                        Relatorio.CriarObjCelulaInColuna("Query", "FabricanteID", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("  Manufactorer", "Fabricante", false, "5", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Fabricante", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Address", "Endereco", false, "9", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Endereco", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("City", "Cidade", false, "2.5", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Cidade", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Country", "Pais", false, "2.5", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pais", "DetailGroup");

                    Relatorio.TabelaEnd();

                    //Rodapé
                    Relatorio.CriarObjLinha((footerX).ToString(), (footerY).ToString(), "19.8", "", "Footer");
                    Relatorio.CriarObjLabel("Fixo", "", "Date: ___/___/______    Signature: _________________________________    Name: _________________________________", (footerX).ToString(), (footerY+0.3).ToString(), "9", "black", "L", "Footer", "19.8", "");
                    Relatorio.CriarObjLinha((footerX).ToString(), (footerY+1).ToString(), "19.8", "", "Footer");

                    Relatorio.CriarObjLabel("Fixo", "", "Issued by electronic process", (footerX).ToString(), (footerY + 1.4).ToString(), "9", "black", "L", "Footer", "19.8", "");
                    Relatorio.CriarObjLabel("Fixo", "", "ORIGINAL", (footerX+9).ToString(), (footerY + 1.4).ToString(), "9", "black", "B", "Footer", "19.8", "");
                }
            }

            Relatorio.CriarPDF_Excel(Title,Formato);
        }


        private void resetGlbsSQLVars()
        {
            var sTaxaMoeda = " * 1 * POWER(-1, Pedidos.TipoPedidoID) ";
            var sTaxaMoedaContabil = " / Pedidos.TaxaMoeda * POWER(-1, Pedidos.TipoPedidoID) ";
            var sDevolucao = " * POWER(-1, Pedidos.TipoPedidoID) ";

            bool chkMoedaLocal = Convert.ToBoolean(HttpContext.Current.Request.Params["bchkMoedaLocal"]);
            string EmpresasIn2 = HttpContext.Current.Request.Params["EmpresasIn2"];

            //var nSelTotal1 = selTotal1.value;

            if (chkMoedaLocal == true)
            {
                sTaxaMoeda = "* (CASE WHEN ResultadosGerenciais.MoedaID = dbo.fn_Moeda(Pedidos.EmpresaID, 2) THEN 1 ELSE Pedidos.TaxaMoeda END) * POWER(-1, Pedidos.TipoPedidoID) ";
                sTaxaMoedaContabil = "";
            }
            else
            {
                sTaxaMoeda = " / (CASE WHEN ResultadosGerenciais.MoedaID = dbo.fn_Moeda(Pedidos.EmpresaID, 2) THEN Pedidos.TaxaMoeda ELSE 1 END) * POWER(-1, Pedidos.TipoPedidoID) ";
                sTaxaMoedaContabil = " / (CASE WHEN dbo.fn_Moeda(Pedidos.EmpresaID, 2) = 647 THEN Pedidos.TaxaMoeda ELSE 1 END) * POWER(-1, Pedidos.TipoPedidoID)";
            }
            glb_strSQL_Select = " COUNT(DISTINCT Pedidos.PedidoID) AS Pedidos, " +
                                "SUM(CONVERT(INT, Itens.Quantidade)" + sDevolucao + ") AS Quantidade, CONVERT(NUMERIC(11,2), SUM(Itens.ValorFaturamento" + sTaxaMoeda + ")) AS Faturamento, " +
                                "CONVERT(NUMERIC(11,2), SUM(dbo.fn_PedidoItem_Totais(Itens.PedItemID,10)" + sTaxaMoedaContabil + ")) AS Impostos, " +
                                "CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosGerenciais.ValorDespesa, 0)" + sTaxaMoeda + ")) AS Desp_Ger, " +
                                "CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosGerenciais.ValorFinanceiro, Itens.ValorFinanceiro)" + sTaxaMoeda + ")) AS Financeiro, " +
                                "CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosGerenciais.PrecoBase, Itens.ValorFaturamentoBase)" + sTaxaMoeda + ")) AS Preco_Base, " +
                                "CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosContabil.PrecoBase, Itens.ValorFaturamentoBase)" + sTaxaMoedaContabil + ")) AS Preco_Base_Cont, " +
                                "CONVERT(NUMERIC(11,2), SUM((dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1321, 1, 1) + " +
                                    "dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1322, 1, 1) + " +
                                    "dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1323, 1, 1))" + sTaxaMoeda + ")) AS Custos_Ger, " +
                                "CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosGerenciais.CustoBase, Itens.ValorCustoBase) " + sTaxaMoeda + ")) AS Custo_Base, " +
                                "CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosContabil.CustoBase, Itens.ValorCustoBase) " + sTaxaMoedaContabil + ")) AS Custo_Base_Cont, " +
                                "CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosContabil.ValorContribuicao, Itens.ValorContribuicao)" + sTaxaMoedaContabil + ")) AS [Contr_Cont], " +
                                "CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosGerenciais.ValorContribuicao, Itens.ValorContribuicaoAjustada)" + sTaxaMoeda + ")) AS [Contr_Ger], " +
                                "CONVERT(NUMERIC(12,2), dbo.fn_DivideZero(SUM(ISNULL(ResultadosGerenciais.ValorContribuicao, Itens.ValorContribuicaoAjustada)" + sTaxaMoeda + ") ," +
                                    "SUM(ISNULL(ResultadosGerenciais.PrecoBase, Itens.ValorFaturamentoBase)" + sTaxaMoeda + ")) * 100) AS MC, " +
                                "CONVERT(NUMERIC(12,2), dbo.fn_DivideZero(SUM(ISNULL(ResultadosContabil.ValorContribuicao, Itens.ValorContribuicao)" + sTaxaMoedaContabil + ") ," +
                                    "SUM(ISNULL(ResultadosContabil.PrecoBase, Itens.ValorFaturamentoBase)" + sTaxaMoedaContabil + ")) * 100) AS MC_Cont, " +
                                "COUNT(DISTINCT Pedidos.ParceiroID) as CAP ";
            //'CONVERT(NUMERIC(11,2), SUM(Itens.ValorComissao' + sTaxaMoeda + ')) AS [Comissão], CONVERT(NUMERIC(10,2), dbo.fn_DivideZero(SUM(Itens.ValorComissao), SUM(Itens.ValorContribuicaoAjustada)) * 100) AS CV ';

            glb_strSQL_From = "FROM Pedidos_Itens Itens WITH(NOLOCK) " +
                                "LEFT OUTER JOIN Operacoes CFOPs WITH(NOLOCK) ON (Itens.CFOPID = CFOPs.OperacaoID) " +
                                "INNER JOIN Pedidos WITH(NOLOCK) ON (Itens.PedidoID=Pedidos.PedidoID) " +
                                "LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) " +
                                "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.SujeitoID=Pedidos.EmpresaID) " +
                                "INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID=Transacoes.OperacaoID) " +
                                "INNER JOIN Pessoas Vendedores WITH(NOLOCK) ON (Pedidos.ProprietarioID=Vendedores.PessoaID) " +
                                "INNER JOIN Pessoas Equipes WITH(NOLOCK) ON (Pedidos.AlternativoID=Equipes.PessoaID) " +
                                "INNER JOIN Pessoas Parceiros WITH(NOLOCK) ON (Pedidos.ParceiroID=Parceiros.PessoaID) " +
                                "LEFT OUTER JOIN TiposAuxiliares_Itens Classificacoes WITH(NOLOCK) ON (Parceiros.ClassificacaoID=Classificacoes.ItemID) " +
                                "INNER JOIN Pessoas Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID=Pessoas.PessoaID) " +
                                "INNER JOIN RelacoesPessoas RelPessoas WITH(NOLOCK) ON (Pedidos.ParceiroID=RelPessoas.SujeitoID) " +
                                "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pedidos.ParceiroID=Enderecos.PessoaID) " +
                                "LEFT OUTER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) " +
                                "LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) " +
                                "LEFT OUTER JOIN Localidades Estados WITH(NOLOCK) ON (Enderecos.UFID=Estados.LocalidadeID) " +
                                "LEFT OUTER JOIN TiposAuxiliares_Itens Regioes WITH(NOLOCK) ON (Estados.TipoRegiaoID=Regioes.ItemID) " +
                                "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID=Produtos.ConceitoID) " +
                                "INNER JOIN Conceitos Familias WITH(NOLOCK) ON (Produtos.ProdutoID=Familias.ConceitoID) " +
                                "INNER JOIN Conceitos Marcas WITH(NOLOCK) ON (Produtos.MarcaID=Marcas.ConceitoID) " +
                                "INNER JOIN Pessoas GPs WITH(NOLOCK) ON (GPs.PessoaID=ProdutosEmpresa.ProprietarioID) " +
                                "LEFT OUTER JOIN Conceitos LinhasProduto WITH(NOLOCK) ON (LinhasProduto.ConceitoID=Produtos.LinhaProdutoID) " +
                //Incluido pelo projeto Custo. BJBN
                //Custo Gerencial 471.
                                "LEFT OUTER JOIN Pedidos_Itens_Resultados ResultadosGerenciais WITH(NOLOCK) ON (ResultadosGerenciais.PedITemID = Itens.PedItemID) AND (ResultadosGerenciais.TipoResultadoID = 471) " +
                //Custo Gerencial 470.
                                "LEFT OUTER JOIN Pedidos_Itens_Resultados ResultadosContabil WITH(NOLOCK) ON (ResultadosContabil.PedITemID = Itens.PedItemID) AND (ResultadosContabil.TipoResultadoID = 470) ";

            glb_strSQL_Where1 = "WHERE ((Pedidos.EmpresaID in " + EmpresasIn2 + " AND " +
                                "Pedidos.PessoaID <> Pedidos.EmpresaID AND " +
                                "RelPessoas.ObjetoID = Pedidos.EmpresaID AND RelPessoas.TipoRelacaoID=21 AND " +
                                "RelPessoas.EstadoID <> 5 AND " +
                                "Enderecos.EndFaturamento=1 AND Enderecos.Ordem=1 AND " +
                                "(Itens.CFOPID IS NOT NULL OR (Itens.CFOPID IS NULL AND Pedidos.TransacaoID IN (211, 215))) AND ";

            glb_strSQL_Where2 = "ProdutosEmpresa.TipoRelacaoID=61 AND " +
                                "Transacoes.Resultado=1 AND Pedidos.Suspenso=0 AND " +
                                "((Pedidos.TipoPedidoID=601 AND Pedidos.EstadoID>=30) OR (Pedidos.TipoPedidoID=602 AND Pedidos.EstadoID>=29)) AND ";


        }

        protected dataInterface DataInterfaceObj = new dataInterface(
        System.Configuration.ConfigurationManager.AppSettings["application"]);

        private DataTable RequestDatatable(string SQLQuery, string NomeQuery)
        {
            string strConn = "";

            Object oOverflySvrCfg = null;

            DataTable Dt = new DataTable();

            DataSet DsContainer = new DataSet();

            oOverflySvrCfg = comCreateObject("OverflySvrCfg.OverflyMTS");
            strConn = ((OVERFLYSVRCFGLib.OverflyMTS)oOverflySvrCfg).DotNetDataBaseStrConn(DataInterfaceObj.ApplicationName);

            SqlCommand cmd = new SqlCommand(SQLQuery);
            using (SqlConnection con = new SqlConnection(strConn))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    cmd.CommandTimeout = 0;
                    sda.SelectCommand = cmd;

                    sda.Fill(DsContainer, NomeQuery);
                }
            }

            Dt = DsContainer.Tables[NomeQuery];

            return Dt;
        }


        public string translateTherm(string sTherm, string[,] aTranslate)
        {
            int nIdiomaDeID = Convert.ToInt32(HttpContext.Current.Request.Params["nIdiomaDeID"]);
            int nIdiomaParaID = Convert.ToInt32(HttpContext.Current.Request.Params["nIdiomaParaID"]);
            string retVal = "";
            int nSeek;

            if ((sTherm == null) || (sTherm == "") || (nIdiomaDeID == nIdiomaParaID) || (nIdiomaParaID == 246))
                return sTherm;

            nSeek = Assek(aTranslate, sTherm);

            if (nSeek >= 0)
                retVal = aTranslate[nSeek, 1];
            else
                retVal = "";

            return retVal;
        }


        public int Assek(string[,] aArray, string strToSeek)
        {
            int nFinal = (((aArray.Length) / 2) - 1);
            int retVal = -1;

            for (int i = 0; i <= nFinal; i++)
            {
                if (aArray[i, 0].CompareTo(strToSeek) == 0)
                {
                    retVal = i;
                    break;
                }
            }
            return retVal;
        }

        public void CarregaGlobais()
        {
            // Array de dicionario usado para traduzir proposta/pedido
            glb_aTranslate_Proposta = new string[,] {{"Proposta Comercial", "Quotation"},
                                                        {"Cliente", "Customer"},
                                                        {"Dados da Proposta", "Quotation"},
                                                        {"Proposta", "Quotation"},
                                                        {"Data", "Date"},
                                                        {"Valor", "Total Amount"},
                                                        {"Valor Convertido", ""},
                                                        {"Previsão Entrega", "Estimated Delivery"},
                                                        {"Frete", "Freight"},
                                                        {"Validade", "Expiration"},
                                                        {"dias úteis", "days"},
                                                        {"Seu Pedido", "Your PO"},
                                                        {"Ítens", "Items"},
                                                        {"ID", "ID"},
                                                        {"Produto", "Product"},
                                                        {"Quant", "Qty"},
                                                        {"Unitário", "Unitary"},
                                                        {"Total", "Total"},
                                                        {"Pagamento", "Payment"},
                                                        {"Tipo", "Type"},
                                                        {"Forma", "Form"},
                                                        {"Prazo", "Term"},
                                                        {"Valor", "Total"},
                                                        {"Observações", "Comments"},
                                                        {"Atenciosamente", "Best Regards"},
                                                        {"De acordo do cliente", "Agreed by customer"},
                                                        {"Aprovar esta proposta", "Approve this quotation"},
                                                        {"acesse", "access"},
                                                        {"área de Negócios", "business Area"},
                                                        {"página Pedidos", "Orders"},
                                                        {"proposta", "quotation"},
                                                        {"e", "and"},
                                                        {"Restart", "Menina Restart"},
                                                        {"aprove a proposta", "submit the quotation"},
                                                        {"Ficha Técnica", "Technical Especification"},
                                                        {"Descrição", "Description"},
                                                        {"Part Number", "Part Number"},
                                                        {"Peso", "Weight"},
                                                        {"Gar", "Warr"},
                                                        {"A Pagar", "To be collected"},
                                                        {"Pago", "Paid"},
                                                        {"Solicitação de Compra", "Purchase Order"},
                                                        {"Ordem", "Ordem"},
                                                        {"Valor R$","Valor R$"},
                                                        {"Valor US$","Valor US$"},
                                                        {"Caracteristica", "Characteristics"},
                                                        {"Solicitação", "PO"},
                                                        {"Faturar Para", "Invoice To"}};

            glb_aTranslate_EtiquetaEmbalagem = new string[,] {{"NF", "Invoice"},
                                                            {"Pedido", "Order"},
                                                            {"Volume", "Volume"},
                                                            {"Transp", "Carrier"},
                                                            {"Rem", ""},
                                                            {"Data NF", "Invoice Date"}};

            glb_ListaEmbalagens = new string[,] {{"Lista de Embalagem", "Packing List"},
    						{"Pedido", "Order"},
							{"NF", "Invoice"},
							{"Produto", "Product"},
							{"Qtd", "Qty"},
							{"Número Série", "Serial Number"},
							{"Data", "Date"},
							{"Fornecedor", "Supplier"},
							{"Documento", "Document"},
							{"Total Itens", "Total Items"}};

        }


        private static Object comCreateObject(string sProgID)
        {
            Type oType = Type.GetTypeFromProgID(sProgID);
            if (oType != null)
            {
                return Activator.CreateInstance(oType);
            }
            return null;
        }
    }
}