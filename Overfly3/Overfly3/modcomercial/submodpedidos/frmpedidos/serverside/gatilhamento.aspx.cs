using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class gatilhamento : System.Web.UI.OverflyPage
	{
		private Integer pedRIRId;
		private string result;
		private Integer numeroSerie;
		private Integer desgatilha;
	
		private static Integer zero = new Integer(0);
		
 		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(PedidoParcelas());
		}

		// Roda a procedure sp_Pedido_RIRNumeroSerie
		private DataSet PedidoParcelas()
		{
			ProcedureParameters[] procParams = new ProcedureParameters[4];

			procParams[0] = new ProcedureParameters(
				"@PedRIRID",
				System.Data.SqlDbType.Int,
				pedRIRId.ToString());

			procParams[1] = new ProcedureParameters(
				"@NumeroSerie",
				System.Data.SqlDbType.VarChar,
				numeroSerie.ToString());
			procParams[1].Length = 20;

			procParams[2] = new ProcedureParameters(
				"@Desgatilha",
				System.Data.SqlDbType.Int,
				desgatilha.ToString());

			procParams[3] = new ProcedureParameters(
				"@Resultado",
				System.Data.SqlDbType.VarChar,
				DBNull.Value,
				ParameterDirection.InputOutput);
			procParams[3].Length = 8000;

			DataInterfaceObj.execNonQueryProcedure(
				"sp_Pedido_RIRNumeroSerie",
				procParams);

			return DataInterfaceObj.getRemoteData(
				"select " + procParams[3].Data + " as Mensagem"
			);
		}

		public Integer nPedRIRID
		{
			get { return pedRIRId; }
			set { pedRIRId = value != null ? value : zero; }
		}
		
		public Integer sNumeroSerie
		{
			get { return numeroSerie; }
			set { numeroSerie = value != null ? value : zero; }
		}
		
		public Integer nDesgatilha
		{
			get { return desgatilha; }
			set { desgatilha = value != null ? value : zero; }
		}
	}
}
