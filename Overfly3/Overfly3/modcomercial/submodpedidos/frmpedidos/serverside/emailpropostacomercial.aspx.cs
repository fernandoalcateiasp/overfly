using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;
using OVERFLYSVRCFGLib;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class emailpropostacomercial : System.Web.UI.OverflyPage
	{
		///////////////////////////////////////////////////////////////////////
		///////// Atributos para os par�metros recebidos pela p�gina. /////////
		///////////////////////////////////////////////////////////////////////
		protected Integer relatorioId;
		protected Integer usuarioId;
		protected Integer idiomaDeId;
		protected Integer idiomaParaId;
		protected Integer pedidoId;
		protected Integer validade;
		protected Integer contatoId;
		protected string contatoCargo;
		protected Integer fichaTecnica;
		protected java.lang.Boolean micro;
		protected Integer empresaId;
		protected string empresa;
		protected string site;
		protected Integer translateLen;
		protected Integer translateDim;
        protected Integer transacaoId;
		protected string dateSQLParam;
		protected java.lang.Boolean sup;
		protected java.lang.Boolean marca;
		protected java.lang.Boolean modelo;
        protected java.lang.Boolean ncm;
		protected java.lang.Boolean haObservacoes;
		protected java.lang.Boolean informacoesAdicionais;

		protected string[][] translate = new string[][] {
			new string[] {"Proposta Comercial","Quotation"},
			new string[] {"Cliente","Customer"},
			new string[] {"Dados da Proposta","Quotation"},
			new string[] {"Proposta","Quotation"},
			new string[] {"Data","Date"},
			new string[] {"Valor","Total Amount"},
			new string[] {"Valor Convertido",""},
			new string[] {"Previs�o Entrega","Estimated Delivery"},
			new string[] {"Frete","Freight"},
			new string[] {"Validade","Expiration"},
			new string[] {"dias �teis","days"},
			new string[] {"Seu Pedido","Your PO"},
			new string[] {"�tens","Items"},
			new string[] {"ID","ID"},
			new string[] {"Produto","Product (Part Number)"},
			new string[] {"Quant","Qty"},
			new string[] {"Unit�rio","Unitary"},
			new string[] {"Total","Total"},
			new string[] {"Pagamento","Payment"},
			new string[] {"Forma","Form"},
			new string[] {"Prazo","Term"},
			new string[] {"Valor","Total"},
			new string[] {"Observa��es","Comments"},
			new string[] {"Atenciosamente","Best Regards"},
			new string[] {"De acordo do cliente","Agreed by customer"},
			new string[] {"Aprovar esta proposta","Approve this quotation"},
			new string[] {"acesse","access"},
			new string[] {"�rea de Neg�cios","business Area"},
			new string[] {"p�gina Pedidos","Orders"},
			new string[] {"proposta","quotation"},
			new string[] {"e","and"},
			new string[] {"aprove a proposta","submit the quotation"},
			new string[] {"Ficha T�cnica","Technical Especification"},
			new string[] {"Descri��o","Description"},
			new string[] {"Part Number","Part Number"},
			new string[] {"Peso","Weight"},
			new string[] {"Gar","Warr"},
			new string[] {"A Pagar","To be collected"},
			new string[] {"Pago","Paid"},
			new string[] {"Solicita��o de Compra","Purchase Order"},
			new string[] {"Solicita��o","PO"},
			new string[] {"Faturar Para","Invoice To"},
			new string[] {"Marca","Brand"},
            new string[] {"Serie","Series"},
			new string[] {"Modelo","Model"},
            new string[] {"Ncm","Ncm"},
			new string[] {"Outras Condi��es", "Other Conditions"}
		};
		///////////////////////////////////////////////////////////////////////

		protected string mailBody;
		protected string report = "";
		protected string report2 = "";
		protected bool temPagamentoCCC = false;
		protected int colSpanItens = 0;
		protected int itensCount = 0;
		protected string modeloMicro;
		protected string prazoGarantia = "";
		protected string aprovarPropostaURL;

		protected Integer zero = new Integer(0);
		protected java.lang.Boolean verdade = new java.lang.Boolean(true);
		protected java.lang.Boolean falso = new java.lang.Boolean(false);

		protected string urlRoot = null;
		protected string webPath = null;
		protected Integer totalMicros = null;

		protected DataSet capa = null;
		protected DataSet itens = null;
		protected DataSet parcelas = null;
		
		protected override void PageLoad(object sender, EventArgs e)
		{
		
		}
		
		public string TranslateTherm(string therm, int idiomaDeId, int idiomaParaId)
		{
			if (therm == null || therm.Equals("") || idiomaDeId == idiomaParaId || idiomaParaId == 246)
			{
				return therm;
			}

			int index;
			for (index = 0; index < translate.Length; index++)
			{
				if(translate[index][0].Equals(therm))
					break;
			}

			if (index >= translate.Length)
			{
				return therm;
			}
			return translate[index][1];
		}

		public string URLRoot
		{
			get
			{
				if(urlRoot == null)
				{
					OverflyMTS svrCfg = new OverflyMTS();
					
					urlRoot = svrCfg.PagesURLRoot(
						System.Configuration.ConfigurationManager.AppSettings["application"]
					);
					
					if(!urlRoot.StartsWith("http://www"))
					{
						urlRoot = "http://localhost/overfly3";
					}

				}
				
				return urlRoot;
			}
		}

		protected int totMicros {
			get { return totalMicros.intValue(); }
		}
		public Integer TotalMicros
		{
			get { return totalMicros; }
			set { totalMicros = (value == null || value.intValue() <= 0) ? new Integer(1) : value; }
		}

		public DataSet Capa
		{
			get
			{
				if(capa == null)
				{
					capa = DataInterfaceObj.getRemoteData(
						"SELECT TOP 1 " +
                            "CONVERT(BIT, dbo.fn_Imagem(1210, 20100," + empresaId + ", 2, NULL, NULL)) AS ImgEmpresa," +
                            "CONVERT(BIT, dbo.fn_Imagem(1210, 20100, Pedidos.ParceiroID, 2, NULL, NULL)) AS ImgParceiro," +
							"Pedidos.ParceiroID AS ParceiroID, " + 
							"RTRIM(LTRIM(STR(Pedidos.PedidoID))) AS PedidoID,  " +
							"CONVERT(VARCHAR, Pedidos.dtPedido, " + dateSQLParam + ") AS dtPedido," +
							"Moedas.SimboloMoeda AS MoedaPedido, " + 
							"CONVERT(VARCHAR(12), CONVERT(NUMERIC(11,2), Pedidos.ValorTotalPedido)) AS ValorTotalPedido, " + 
							"MoedasConvertidas.SimboloMoeda AS MoedaConversao, " +
							"CONVERT(VARCHAR(12), CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(Pedidos.ValorTotalPedido, Pedidos.TaxaMoeda))) AS ValorConvertido, " +
							"(CASE WHEN Pedidos.dtPrevisaoEntrega <= GETDATE() THEN '" + Imediata + 
								"' ELSE CONVERT(VARCHAR, Pedidos.dtPrevisaoEntrega, " + dateSQLParam + ") + SPACE(1) + " + "CONVERT(VARCHAR, Pedidos.dtPrevisaoEntrega, 108) END) " + 
							"AS dtPrevisaoEntrega, " +
							"(CASE Pedidos.Frete WHEN 0 THEN '" + TranslateTherm("A Pagar", idiomaDeId.intValue(), idiomaParaId.intValue()) +
								"' ELSE '" + TranslateTherm("Pago", idiomaDeId.intValue(), idiomaParaId.intValue()) + "' END) AS Frete," +
							"ISNULL(Pedidos.SeuPedido, '&nbsp') AS SeuPedido, " + 
							"Pedidos.Observacao, " + 
							"Pedidos.Observacoes, " +
							"Colaboradores.Fantasia AS ColaboradorFantasia, " +
                            "ISNULL(STUFF(STUFF(REPLACE(dbo.fn_Pessoa_Telefone(Colaboradores.PessoaID, 119, 120,1,0,NULL),'-',''),5,0,' '),3,0,' ') + SPACE(3), SPACE(0)) AS ColaboradorTelefones, " +
							"RTRIM(LTRIM(STR(Pedidos.PessoaID))) AS PessoaID, " + 
							"UPPER(Pessoas.Nome) AS PessoaNome, " +
							(idiomaParaId.intValue() == 246 ? "ISNULL(PessoaPaises.Localidade, SPACE(0)) AS PessoaPais, " : "ISNULL(PessoaPaises.NomeInternacional, SPACE(0)) AS PessoaPais, ") +
							"(CASE ISNULL(PessoaPaises.EnderecoInvertido, 0) WHEN 1 " +
								"THEN ISNULL(PessoaEnderecos.Numero, SPACE(0)) + ISNULL(SPACE(1) + PessoaEnderecos.Endereco, SPACE(0)) " +
								"ELSE ISNULL(PessoaEnderecos.Endereco, SPACE(0)) + ISNULL(SPACE(1) + PessoaEnderecos.Numero, SPACE(0)) END) AS PessoaEndereco, " +
							"(ISNULL(PessoaEnderecos.Complemento + SPACE(1), SPACE(0)) + ISNULL(PessoaEnderecos.Bairro, SPACE(0))) AS PessoaComplementoBairro, " +
                            "(REPLACE(PessoaCidades.Localidade, CHAR(39), '') + SPACE(1) + ISNULL(PessoaUFs.CodigoLocalidade2, SPACE(0)) + SPACE(1) + PessoaEnderecos.CEP) AS PessoaCEPCidadeUF, " +
                            "dbo.fn_Pessoa_Documento(Pessoas.PessoaID, NULL, NULL, 101, 111, 0) AS PessoaDocumento1, " +
                            "dbo.fn_Pessoa_Documento(Pessoas.PessoaID, NULL, NULL, 102, 112, 0) AS PessoaDocumento2, " +
							"dbo.fn_Pedido_Totais(Pedidos.PedidoID, 7) AS TotalMicros " +
						"FROM Pedidos WITH (NOLOCK) " +
                            "INNER JOIN Conceitos Moedas WITH (NOLOCK) ON (Pedidos.MoedaID=Moedas.ConceitoID) " +
                            "INNER JOIN Pessoas Colaboradores WITH (NOLOCK) ON (Pedidos.ProprietarioID=Colaboradores.PessoaID) " +
                            "INNER JOIN Pessoas WITH (NOLOCK) ON (" +
							(relatorioId.intValue() == 40133 ? "Pedidos.PessoaID=Pessoas.PessoaID) " : "Pedidos.EmpresaID=Pessoas.PessoaID) ") +
                            "INNER JOIN Pessoas_Enderecos PessoaEnderecos WITH (NOLOCK) ON (Pessoas.PessoaID=PessoaEnderecos.PessoaID) " +
                            "LEFT OUTER JOIN Localidades PessoaCidades WITH (NOLOCK) ON (PessoaEnderecos.CidadeID=PessoaCidades.LocalidadeID) " +
                            "LEFT OUTER JOIN Localidades PessoaUFs WITH (NOLOCK) ON (PessoaEnderecos.UFID=PessoaUFs.LocalidadeID) " +
                            "LEFT OUTER JOIN Localidades PessoaPaises WITH (NOLOCK) ON (PessoaEnderecos.PaisID=PessoaPaises.LocalidadeID), " +
                            "Recursos Sistema WITH (NOLOCK) " +
                            "INNER JOIN Conceitos MoedasConvertidas WITH (NOLOCK) ON (Sistema.MoedaID=MoedasConvertidas.ConceitoID) " +
						"WHERE (Pedidos.PedidoID= " + pedidoId + " AND Sistema.RecursoID=999 AND PessoaEnderecos.Ordem=1) "
					);

					if (capa.Tables[1].Rows[0]["TotalMicros"] != null || (int) capa.Tables[1].Rows[0]["TotalMicros"] <= 0)
					{
						TotalMicros = new Integer(1);
					}
					else
					{
						TotalMicros = new Integer((int) capa.Tables[1].Rows[0]["TotalMicros"]);
					}
				}
				
				return capa;
			}
		}

		public DataSet Itens
		{
			get
			{
				if(itens == null)
				{
					itens = DataInterfaceObj.getRemoteData(
						(!micro.booleanValue() ?
							"SELECT " +
								"Itens.PedidoID, Itens.Ordem AS Ordem, (RTRIM(LTRIM(STR(Itens.ProdutoID)))) AS ProdutoID, " +
                                "dbo.fn_Produto_Descricao2(Itens.PedItemID, " + idiomaParaId + ", 21) AS Produto, " +
                                "Marcas.Conceito AS Marca, ISNULL(Produtos.Serie, SPACE(0)) AS Serie, Produtos.Modelo AS Modelo, ISNULL(Classificacao.Conceito, SPACE(0)) AS Ncm, ISNULL(Produtos.PartNumber, '&nbsp') AS PartNumber, Familias.EhGabinete, " +
								"SPACE(0) AS PrazoGarantia, CONVERT(INT, Itens.Quantidade) AS Quantidade, Moedas.SimboloMoeda AS Moeda, " +
								"CONVERT(VARCHAR(12), CONVERT(NUMERIC(11,2), dbo.fn_PedidoItem_Totais(Itens.PedItemID, 7))) AS ValorUnitario, " +
								"CONVERT(VARCHAR(12), CONVERT(NUMERIC(11,2),dbo.fn_PedidoItem_Totais(Itens.PedItemID, 8))) AS ValorTotal, " +
								"ICMSISS = (SELECT CONVERT(VARCHAR(6),CONVERT(NUMERIC(5,2),ISNULL(SUM(ItensImpostos.Aliquota),0))) + CHAR(37) " +
                            "FROM Pedidos_Itens_Impostos ItensImpostos WITH(NOLOCK), Conceitos Impostos WITH(NOLOCK) " +
							"WHERE (Itens.PedItemID=ItensImpostos.PedItemID AND ItensImpostos.ImpostoID=Impostos.ConceitoID AND " +
                                "Impostos.IncidePreco=1 AND Impostos.InclusoPreco=1 AND Impostos.AlteraAliquota=1)), " +
								"IPI = CONVERT(VARCHAR, CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(Itens.PedidoID, Produtos.ConceitoID, GETDATE(), Itens.FinalidadeID, -6))) + CHAR(37), " +
								"(SELECT TOP 1 Fornecedores.Codigo " +
                                    "FROM RelacoesPesCon ProdutosEmpresa WITH (NOLOCK), RelacoesPesCon_Fornecedores Fornecedores WITH (NOLOCK) " +
									"WHERE (ProdutosEmpresa.SujeitoID = " + empresaId + " AND ProdutosEmpresa.TipoRelacaoID = 61 AND " +
										"Produtos.ConceitoID = ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.RelacaoID = Fornecedores.RelacaoID AND " +
										"Fornecedores.FornecedorID = Pedidos.PessoaID)) AS CodigoFornecedor, " +
                                        "ValorConvertido = CASE Itens.TaxaMoeda WHEN 0 THEN NULL WHEN 1 THEN NULL ELSE CONVERT(NUMERIC(11,2)," +
										"Itens.ValorUnitario/Itens.TaxaMoeda) END, " +
										"TaxaMoeda = CASE Itens.TaxaMoeda WHEN 1 THEN NULL ELSE CONVERT(NUMERIC(10,4), Itens.TaxaMoeda) END "
						:
							"SELECT " +
								"Itens.PedidoID, " + 
								"Itens.Ordem AS Ordem, " + 
								"(RTRIM(LTRIM(STR(Itens.ProdutoID)))) AS ProdutoID, " +
                                "dbo.fn_Produto_Descricao2(Itens.PedItemID, " + idiomaParaId + ", 21) AS Produto, " +
								"Marcas.Conceito AS Marca, " +
                                "ISNULL(Produtos.Serie, SPACE(0)) AS Serie, " +
								"dbo.fn_Pedido_ProdutoModelo(Pedidos.PedidoID, Produtos.ConceitoID) AS Modelo," +
                                "ISNULL(Classificacao.Conceito, SPACE(0)) AS Ncm," +
								"Produtos.Descricao AS Descricao, " + 
								"dbo.fn_PedidoItem_Garantia(Itens.PedItemID) AS PrazoGarantia, " + 
								"Familias.EhGabinete, " + 
								"CONVERT(INT, Itens.Quantidade) AS Quantidade, " +
								"(CASE ISNULL(Familias.EhMicro, 0) WHEN 1 THEN Moedas.SimboloMoeda ELSE '&nbsp' END) AS Moeda, " +
								"(CASE ISNULL(Familias.EhMicro, 0) WHEN 1 THEN CONVERT(VARCHAR(12), CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(Pedidos.ValorTotalPedido, Itens.Quantidade))) ELSE '&nbsp' END) AS ValorUnitario, " +
								"(CASE ISNULL(Familias.EhMicro, 0) WHEN 1 THEN CONVERT(VARCHAR(12), CONVERT(NUMERIC(11,2),Pedidos.ValorTotalPedido)) ELSE '&nbsp' END) AS ValorTotal, " +
								"ICMSISS = (CASE ISNULL(Familias.EhMicro, 0) WHEN 1 THEN (SELECT ( CONVERT(VARCHAR(6),CONVERT(NUMERIC(5,2),ISNULL(SUM(ItensImpostos.Aliquota),0))) + CHAR(37)) " +
                            "FROM Pedidos_Itens_Impostos ItensImpostos WITH(NOLOCK), Conceitos Impostos WITH(NOLOCK) " +
							"WHERE (Itens.PedItemID=ItensImpostos.PedItemID AND ItensImpostos.ImpostoID=Impostos.ConceitoID AND " +
                                "Impostos.IncidePreco=1 AND Impostos.InclusoPreco=1 AND Impostos.AlteraAliquota=1)) ELSE '&nbsp' END), " +
								"IPI = (CASE ISNULL(Familias.EhMicro, 0) WHEN 1 THEN CONVERT(VARCHAR, CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(Itens.PedidoID, Produtos.ConceitoID, GETDATE(), Itens.FinalidadeID, -6))) + CHAR(37) ELSE '&nbsp' END) "
						) +

                        "FROM Pedidos_Itens Itens WITH(NOLOCK) " +
	                        "INNER JOIN Pedidos WITH(NOLOCK) ON (Pedidos.PedidoID = Itens.PedidoID) " +
	                        "INNER JOIN  Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID=Produtos.ConceitoID) " +
	                        "INNER JOIN Conceitos Familias WITH(NOLOCK) ON (Produtos.ProdutoID = Familias.ConceitoID) " +
	                        "INNER JOIN Conceitos Moedas WITH(NOLOCK) ON (Pedidos.MoedaID=Moedas.ConceitoID) " +
	                        "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (ProdutosEmpresa.ObjetoID=Produtos.ConceitoID) " +
	                        "INNER JOIN Conceitos Marcas WITH(NOLOCK) ON (Produtos.MarcaID=Marcas.ConceitoID) " +
	                        "LEFT JOIN Conceitos Classificacao WITH(NOLOCK) ON (ProdutosEmpresa.ClassificacaoFiscalID = Classificacao.ConceitoID) " +

                        "WHERE " +
                            "(Itens.PedidoID=  " + pedidoId + " AND ProdutosEmpresa.SujeitoID = " + empresaId + " AND ProdutosEmpresa.TipoRelacaoID=61)" +
						"ORDER BY Itens.Ordem"
					);
				}
				
				return itens;
			}
		}
		
		public DataSet Parcelas
		{
			get
			{
				if(parcelas == null)
				{
					parcelas = DataInterfaceObj.getRemoteData(
						"SELECT " +
							"PedidosParcelas.PedidoID , PedidosParcelas.Ordem AS Ordem, " +
							"Tipo = ISNULL(CONVERT(VARCHAR(10), HP.HistoricoAbreviado), '&nbsp'), " +
							"dbo.fn_Tradutor(FormasPagamento.ItemMasculino, " + idiomaDeId + ", " + idiomaParaId + ", NULL) AS Forma, " +
							"PedidosParcelas.PrazoPagamento AS Prazo, CONVERT(VARCHAR(12), CONVERT(NUMERIC(11,2), PedidosParcelas.ValorParcela)) AS Valor, " +
							"PedidosParcelas.Observacao AS Observacao, PedidosParcelas.FormaPagamentoID " +
                        "FROM Pedidos_Parcelas PedidosParcelas WITH (NOLOCK) " +
                            "INNER JOIN TiposAuxiliares_Itens FormasPagamento WITH (NOLOCK) ON (PedidosParcelas.FormaPagamentoID=FormasPagamento.ItemID) " +
                            "LEFT OUTER JOIN HistoricosPadrao HP WITH (NOLOCK) ON (PedidosParcelas.HistoricoPadraoID = HP.HistoricoPadraoID AND ISNULL(PedidosParcelas.HistoricoPadraoID, 0) <> 3173) " +
						"WHERE (PedidoID = " + pedidoId + ") " +
							(!SUP ? " AND (PedidosParcelas.HistoricoPadraoID IS NULL)" : "")
					);
				}
				
				return parcelas;
			}
		}

		protected DataSet contas = null;
		public DataSet Contas
		{
			get
			{
				if (contas == null && temPagamentoCCC)
				{
                    contas = DataInterfaceObj.getRemoteData(
                        "SELECT " + (temPagamentoCCC ? "" : " top 0 ") +
                            "dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 5) AS Banco, " +
                            "dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 1) AS CodigoBanco, " +
                            "dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 21) AS CodigoAgencia, " +
                            "dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 31) AS ContaCorrente, " +
                            "c.Nome AS Correntista, CONVERT(BIT, dbo.fn_Imagem(9220, 28230, d.BancoID, 2, NULL, NULL)) AS TemImagem, " +
                            "d.BancoID " +
                            (idiomaParaId.intValue() == 246 ? ", dbo.fn_Pessoa_Documento(c.PessoaID, NULL, NULL, 111, NULL, 0) AS CNPJ " : " ") +
                        "FROM RelacoesPessoas a WITH (NOLOCK), RelacoesPessoas_Contas b WITH (NOLOCK), Pessoas c WITH (NOLOCK), Pessoas d WITH (NOLOCK) " +
                        "WHERE (a.TipoRelacaoID = 24 AND a.EstadoID = 2 AND " +
                            "a.SujeitoID = " + empresaId.ToString() + " AND ISNULL(a.Observacoes, SPACE(0)) NOT LIKE '<sem proposta>' AND " +
                            "a.RelacaoID = b.RelacaoID AND b.TipoContaID = 1506 AND b.EstadoID = 2 AND b.Publica = 1 AND a.SujeitoID = c.PessoaID AND " +
                            "a.ObjetoID=d.PessoaID) " +
                        "ORDER BY CodigoBanco, CodigoAgencia, ContaCorrente"
                    );
				}
				else if (contas == null && !temPagamentoCCC)
				{
                        contas = DataInterfaceObj.getRemoteData(
                            "SELECT  TOP 0 '' AS Banco, '' AS CodigoBanco, '' AS CodigoAgencia, '' AS ContaCorrente, " +
                                "'' AS Correntista, 0 AS TemImagem, 0 AS BancoID " + (idiomaParaId.intValue() == 246 ? ", '' AS CNPJ " : " ")
                        );
				}

				return contas;
			}
		}

		protected DataSet fichaTecnica1 = null;
		public DataSet FichaTecnica1
		{
			get
			{
				if (fichaTecnica1 == null)
				{
					fichaTecnica1 = DataInterfaceObj.getRemoteData(
						"SELECT " +
							"Itens.PedidoID, Itens.Ordem AS Ordem, (RTRIM(LTRIM(STR(Itens.ProdutoID)))) AS ProdutoID, " +
                            "CONVERT(BIT, dbo.fn_Imagem(2110, 21000, Itens.ProdutoID, 2, NULL, NULL)) AS ImgProduto," +
                            "CONVERT(BIT, dbo.fn_Imagem(2110, 21000, Produtos.MarcaID, 2, NULL, NULL)) AS ImgMarca," +
							"Itens.ProdutoID AS _ProdutoID, Produtos.MarcaID AS MarcaID," +
                            "dbo.fn_Produto_Descricao2(Itens.PedItemID, " + idiomaParaId.ToString() + ", 21) AS Produto, CONVERT(INT, Itens.Quantidade) AS Quantidade, Itens.ValorUnitario AS ValorUnitario, " +
							"(Itens.ValorUnitario*Itens.Quantidade) AS ValorTotal, " +
							"ICMSISS = (SELECT (RTRIM(LTRIM(STR(ItensImpostos.Aliquota))) + CHAR(37)) " +
											"FROM Pedidos_Itens_Impostos ItensImpostos, Conceitos Impostos " +
											"WHERE (Itens.PedItemID=ItensImpostos.PedItemID AND ItensImpostos.ImpostoID=Impostos.ConceitoID AND " +
                                                "Impostos.IncidePreco=1 AND Impostos.InclusoPreco=1 AND Impostos.AlteraAliquota=1)), " +
												"IPI = REPLACE(CONVERT(VARCHAR, CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(Itens.PedidoID, Produtos.ConceitoID, GETDATE(), Itens.FinalidadeID, -6))), CHAR(46), CHAR(44)) + CHAR(37), " +
												//"Produtos.Descricao, ISNULL(Produtos.PartNumber, '&nbsp') AS PartNumber, " +
							"ISNULL(CONVERT(VARCHAR(7), CONVERT(NUMERIC(5,2),dbo.fn_Produto_PesosMedidas(Produtos.ConceitoID,1,1))) + CHAR(47), SPACE(0)) + " +
							"ISNULL(CONVERT(VARCHAR(7), CONVERT(NUMERIC(5,2),dbo.fn_Produto_PesosMedidas(Produtos.ConceitoID,0,1))), SPACE(0)) + ' Kg' AS Pesos, " +
							"ISNULL(CONVERT(VARCHAR(7), CONVERT(NUMERIC(5),dbo.fn_Produto_PesosMedidas(Produtos.ConceitoID,1,2))) + CHAR(120), SPACE(0)) + " +
							"ISNULL(CONVERT(VARCHAR(7), CONVERT(NUMERIC(5),dbo.fn_Produto_PesosMedidas(Produtos.ConceitoID,1,3))) + CHAR(120), SPACE(0)) + " +
							"ISNULL(CONVERT(VARCHAR(7), CONVERT(NUMERIC(5),dbo.fn_Produto_PesosMedidas(Produtos.ConceitoID,1,4))), SPACE(0)) + ' mm' AS Dimensoes, " +

							(EhMicro ?
									"ISNULL(CONVERT(VARCHAR(7), RelPesCon.PrazoTroca) + CHAR(47), SPACE(0)) + " +
									"ISNULL(CONVERT(VARCHAR(7), RelPesCon.PrazoGarantia) + CHAR(47), SPACE(0)) + " +
									"ISNULL(CONVERT(VARCHAR(7), RelPesCon.PrazoAsstec), SPACE(0)) + ' " + Meses + "' AS Prazos "
							:
								"NULL AS Prazos ") +
						"FROM " +
                            "Pedidos_Itens Itens WITH (NOLOCK), Conceitos Produtos WITH (NOLOCK), Conceitos Familias WITH (NOLOCK), RelacoesPesCon RelPesCon WITH (NOLOCK) " +

						"WHERE (" +
							"Itens.PedidoID= " + pedidoId + " AND Itens.ProdutoID=Produtos.ConceitoID AND Produtos.ProdutoID = Familias.ConceitoID " +
							"AND RelPesCon.SujeitoID=" + empresaId + " AND RelPesCon.ObjetoID=Itens.ProdutoID " +
							"AND RelPesCon.FichaTecnica=1) " +
						"ORDER BY Itens.Ordem"
					);
				}

				return fichaTecnica1;
			}
		}

		protected DataSet fichaTecnica2 = null;
		public DataSet FichaTecnica2
		{
			get
			{
				if (fichaTecnica2 == null)
				{
					fichaTecnica2 = DataInterfaceObj.getRemoteData(
                        "SELECT dbo.fn_Produto_CaracteristicaOrdem(ItensCaracteristicas.ProdutoID, ItensCaracteristicas.CaracteristicaID) AS Ordem, " +
		                        "dbo.fn_Tradutor(Caracteristicas.Conceito, " + IdiomaDeID + ", " + IdiomaParaID + ", NULL) AS Caracteristica, " +
		                        "ItensCaracteristicas.Valor AS Valor, " +
		                        "RelConceitos.FichaTecnica, " +
		                        "ItensCaracteristicas.ProdutoID " +
	                        "FROM Conceitos_Caracteristicas ItensCaracteristicas WITH(NOLOCK) " +
		                        "INNER JOIN Conceitos Caracteristicas WITH(NOLOCK) ON (Caracteristicas.ConceitoID = ItensCaracteristicas.CaracteristicaID) " +
		                        "INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (Itens.ProdutoID = ItensCaracteristicas.ProdutoID) " +
		                        "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Produtos.ConceitoID = Itens.ProdutoID) " +
		                        "INNER JOIN RelacoesConceitos RelConceitos WITH(NOLOCK) " +
			                        "ON ((RelConceitos.SujeitoID = ItensCaracteristicas.CaracteristicaID) AND (RelConceitos.ObjetoID = Produtos.ProdutoID)) " +
	                        "WHERE ((Itens.PedidoID = " + pedidoId + ") " +
		                        "AND (ItensCaracteristicas.Checado = 1) " +
		                        "AND (RelConceitos.FichaTecnica = 1)) " +
	                        "ORDER BY Itens.PedidoID, Ordem"
					);
				}

				return fichaTecnica2;
			}
		}

		protected DataSet gare;
		protected DataSet Gare
		{
			get
			{
				ProcedureParameters[] procparam;
				
				if(gare == null) 
				{
					procparam = new ProcedureParameters[2];

					procparam[0] = new ProcedureParameters(
						"@PedidoID",
						SqlDbType.Int,
						pedidoId.ToString());

					procparam[1] = new ProcedureParameters(
						"@TipoResultado",
						SqlDbType.Int,
						"1");

					gare = DataInterfaceObj.execQueryProcedure("sp_Pedido_DemonstrativoCalculoICMSST", procparam);
				}
				
				return gare;
			}
		}

		protected DataSet gnre;
		protected DataSet Gnre
		{
			get
			{
				ProcedureParameters[] procparam;

				if (gnre == null)
				{
					procparam = new ProcedureParameters[2];

					procparam[0] = new ProcedureParameters(
						"@PedidoID",
						SqlDbType.Int,
						pedidoId.ToString());

					procparam[1] = new ProcedureParameters(
						"@TipoResultado",
						SqlDbType.Int,
						"2");

					gnre = DataInterfaceObj.execQueryProcedure("sp_Pedido_DemonstrativoCalculoICMSST", procparam);
				}

                return gnre;
			}
		}

		private DataSet mensagens;
		protected DataSet Mensagens
		{
			get
			{
				if(mensagens == null)
				{
					mensagens = DataInterfaceObj.getRemoteData(
						"SELECT dbo.fn_Pedido_ICMSSTMensagem(" + pedidoId + ",0) as ICMSSTMensagem1, " +
						"dbo.fn_Pedido_ICMSSTMensagem(" + pedidoId + ",1) as ICMSSTMensagem2, " +
						"dbo.fn_Pedido_ICMSSTMensagem(" + pedidoId + ",2) as ICMSSTMensagem3 "
					);
				}
				
				return mensagens;
			}
		}
		
		public string WebPath
		{
			get
			{
				if(webPath == null)
				{
					webPath = URLRoot + "/serversidegenEx/imageblob.aspx";
				}
				
				return webPath;
			}
		}


		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		public string Pesos
		{
			get
			{
				if (idiomaParaId != null && idiomaParaId.intValue() == 245)
					return "Weights (Gross/Net)";
				else
					return "Pesos (Bruto/L�quido)";
			}
		}

		public string Dimensoes
		{
			get
			{
				if (idiomaParaId != null && idiomaParaId.intValue() == 245)
					return "Dimensions (W/H/L)";
				else
					return "Dimens�es (L/A/P)";
			}
		}

		public string Prazos
		{
			get
			{
				if (idiomaParaId != null && idiomaParaId.intValue() == 245)
					return "Terms (Exchange/Warranty/Repair)";
				else
					return "Prazos (Troca/Garantia/Asstec)";
			}
		}
		
		public string Meses
		{
			get
			{
				if (idiomaParaId != null && idiomaParaId.intValue() == 245)
					return "months";
				else
					return "meses";
			}
		}

		public string Imediata
		{
			get
			{
				if (idiomaParaId != null && idiomaParaId.intValue() == 245)
					return "Immediate";
				else
					return "Imediata";
			}
		}

		public string ContasBancarias
		{
			get
			{
				if (idiomaParaId != null && idiomaParaId.intValue() == 245)
					return "Bank Accounts";
				else
					return "Contas Banc�rias";
			}
		}

		///////////////////////////////////////////////////////////////////////
		public string MailBody
		{
			get { return mailBody; }
			set { mailBody = value != null ? value : ""; }
		}

		public string TitlesColor { get { return "#000080"; } }
		public string Report { get { return report; } }
		public string Report2 { get { return report2; } }

		// Campo Observacoes da propriedade Capa
		public string Observacoes
		{
			get
			{
				return (Capa.Tables[1].Rows[0]["Observacoes"] != null) ?
					Capa.Tables[1].Rows[0]["Observacoes"].ToString() :
					"";
			}
		}

		public string AprovarPropostaURL
		{
			get { return aprovarPropostaURL; }
			set { aprovarPropostaURL = value != null ? value : ""; }
		}

		///////////////////////////////////////////////////////////////////////
		/////// Propriedades para os par�metros recebidos pela p�gina. ////////
		///////////////////////////////////////////////////////////////////////
		protected int RelatorioID 
		{
			get { return nRelatorioID.intValue(); }
		}
		protected Integer nRelatorioID
		{
			get { return relatorioId; }
			set
			{
				relatorioId = value != null ? value : zero;

				if (relatorioId.intValue() == 40132)
				{
					report = "Solicita��o de Compra";
					report2 = "Solicita��o";
				}
				else
				{
					report = "Proposta";
					report2 = "Proposta";
				}
			}
		}

		protected Integer nUsuarioID
		{
			get { return usuarioId; }
			set { usuarioId = value != null ? value : zero; }
		}

		protected int IdiomaDeID
		{
			get { return idiomaDeId.intValue(); }
		}
		protected Integer nIdiomaDeID
		{
			get { return idiomaDeId; }
			set { idiomaDeId = value != null ? value : zero; }
		}

		protected int IdiomaParaID
		{
			get { return nIdiomaParaID.intValue(); }
		}
		protected Integer nIdiomaParaID
		{
			get { return idiomaParaId; }
			set
			{
				idiomaParaId = value != null ? value : zero;

				colSpanItens = (idiomaParaId.intValue() == 246) ? 2 : 0;
			}
		}

		protected int PedidoID
		{
			get { return pedidoId.intValue(); }
		}
		protected Integer nPedidoID
		{
			get { return pedidoId; }
			set { pedidoId = value != null ? value : zero; }
		}

		protected int Validade
		{
			get { return validade.intValue(); }
		}
		protected Integer nValidade
		{
			get { return validade; }
			set { validade = value != null ? value : zero; }
		}

		protected int ContatoID
		{
			get { return contatoId.intValue(); }
		}
		protected Integer nContatoID
		{
			get { return contatoId; }
			set { contatoId = value != null ? value : zero; }
		}

		protected string sContatoCargo
		{
			get { return contatoCargo; }
			set { contatoCargo = value != null ? value : ""; }
		}

		protected int FichaTecnica
		{
			get { return fichaTecnica.intValue(); }
		}
		protected Integer nFichaTecnica
		{
			get { return fichaTecnica; }
			set { fichaTecnica = value != null ? value : zero; }
		}

		protected bool EhMicro {
			get { return micro.booleanValue(); }
		}
		protected java.lang.Boolean nEhMicro
		{
			get { return micro; }
			set { micro = value != null ? value : falso; }
		}

		protected int EmpresaID
		{
			get { return empresaId.intValue(); }
		}
		protected Integer nEmpresaID
		{
			get { return empresaId; }
			set { empresaId = value != null ? value : zero; }
		}

		protected string sEmpresa
		{
			get { return empresa; }
			set { empresa = value != null ? value : ""; }
		}

		protected string sSite
		{
			get { return site; }
			set { site = value != null ? value : ""; }
		}

		protected Integer nTranslateLen
		{
			get { return translateLen; }
			set { translateLen = value != null ? value : zero; }
		}

		protected Integer nTranslateDim
		{
			get { return translateDim; }
			set { translateDim = value != null ? value : zero; }
		}

		protected string DATE_SQL_PARAM
		{
			get { return dateSQLParam; }
			set { dateSQLParam = value != null ? value : ""; }
		}

		protected bool SUP {
			get { return sup.booleanValue(); }
		}
		protected java.lang.Boolean bSUP
		{
			get { return sup; }
			set { sup = value != null ? value : verdade; }
		}

		protected string sModeloMicro
		{
			get { return modeloMicro; }
			set { modeloMicro = value != null ? value : ""; }
		}

		protected bool ExibeMarca
		{
			get { return bMarca.booleanValue(); }
		}
		protected java.lang.Boolean bMarca
		{
			get { return marca; }
			set { marca = value != null ? value : falso; }
		}

		protected bool ExibeModelo
		{
			get { return bModelo.booleanValue(); }
		}
		protected java.lang.Boolean bModelo
		{
			get { return modelo; }
			set { modelo = value != null ? value : falso; }
		}

        protected java.lang.Boolean bNcm
        {
            get { return ncm; }
            set { ncm = value != null ? value : falso; }
        }

        protected bool ExibeNcm
        {
            get { return bNcm.booleanValue(); }
        }

        protected java.lang.Boolean bObservacoes
		{
			get { return haObservacoes; }
			set { haObservacoes = value != null ? value : falso; }
		}

		protected bool InformacoesAdicionais
		{
			get { return informacoesAdicionais.booleanValue(); }
		}
		protected java.lang.Boolean bInformacoesAdicionais
		{
			get { return informacoesAdicionais; }
			set { informacoesAdicionais = value != null ? value : falso; }
		}
        protected int TransacaoID
        {
            get { return transacaoId.intValue(); }
        }
        protected Integer nTransacaoID
        {
            get { return transacaoId; }
            set { transacaoId = value != null ? value : zero; }
        }
		///////////////////////////////////////////////////////////////////////
	}
}
