using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class ocorrenciadescontogerador : System.Web.UI.OverflyPage
	{
		private string associacaoID;
        private Integer pedParcelaID;
        private string valorAssociar;
        private Integer empresaID;
        private Integer userID;        
		private string tipoAssociacao;                
        private string resultado;
        private int pedidoID;
        private Integer tipoFinanceiro;
                
		protected override void PageLoad(object sender, EventArgs e)
		{
            ComissoesAssociar();

			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select 'OK' as Ocorrencia"
				)
			);
		}
       
        protected void ComissoesAssociar()
        {
            // Roda a procedure sp_FinaceiroComissoes_Associar
            ProcedureParameters[] procParams = new ProcedureParameters[7];

            procParams[0] = new ProcedureParameters(
                "@StrAssociacaoID",
                System.Data.SqlDbType.VarChar,
                associacaoID.ToString());

            procParams[1] = new ProcedureParameters(
                "@PedParcelaID",
                System.Data.SqlDbType.Int,
                pedParcelaID.ToString());

            procParams[2] = new ProcedureParameters(
                "@StrValorAssociar",
                System.Data.SqlDbType.VarChar,
                valorAssociar.ToString());

            procParams[3] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                empresaID.ToString());

            procParams[4] = new ProcedureParameters(
                "@TipoFinanceiro",
                System.Data.SqlDbType.Int,
                sTipoAssociacao == "Financeiros" ? (Object)nTipoFinanceiro.ToString() : System.DBNull.Value);

            procParams[5] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.VarChar,
                userID.ToString());

            procParams[6] = new ProcedureParameters(
               "@TipoAssociacao",
               System.Data.SqlDbType.VarChar,
               tipoAssociacao);

            
            DataInterfaceObj.execNonQueryProcedure(
                "sp_FinaceiroComissoes_Associar",
                procParams);           
        }

        protected Integer nEmpresaID
		{
			get { return empresaID; }
			set { empresaID = (value != null ? value : new Integer(0)); }
		}

        protected string nAssociacaoID
		{
			get { return associacaoID; }
			set { associacaoID = (value != null ? value : ""); }
		}

        protected Integer nPedParcelaID
        {
            get { return pedParcelaID; }
            set { pedParcelaID = (value != null ? value : new Integer(0)); }
        }

        protected string nValorAssociar
        {
            get { return valorAssociar; }
            set { valorAssociar = (value != null ? value : ""); }
        }

        protected Integer nUserID
        {
            get { return userID; }
            set { userID = (value != null ? value : new Integer(0)); }
        }

        protected Integer nTipoFinanceiro
       {
            get { return tipoFinanceiro; }
            set { tipoFinanceiro = (value != null ? value : new Integer(0)); }
       }

        protected string sTipoAssociacao
        {
            get { return tipoAssociacao; }
            set { tipoAssociacao = (value != null ? value : ""); }
        }       
	}
}
