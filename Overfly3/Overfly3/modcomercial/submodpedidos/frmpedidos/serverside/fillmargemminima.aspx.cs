using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.Attributes;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
    public partial class fillmargemminima : System.Web.UI.OverflyPage
	{
		private string empresas;
        private string estados;
		private string gerentesProduto;
		private string familias;
        private string marcas;
        private string pesquisa;
		private Integer usuarioID;
        private java.lang.Boolean aprovar;
        private java.lang.Boolean emailMarketing;


		protected override void PageLoad(object sender, EventArgs e)
		{
            WriteResultXML(ProdutoMargemMinima());
		}

		protected DataSet ProdutoMargemMinima()
		{
            // Roda a procedure sp_ProdutoMargemMinima_Pesquisar
			ProcedureParameters[] procParams = new ProcedureParameters[9];

			procParams[0] = new ProcedureParameters(
                "@Empresas",
				System.Data.SqlDbType.VarChar,
                empresas != "null" ? (Object)empresas : DBNull.Value);

			procParams[1] = new ProcedureParameters(
                "@Estados",
				System.Data.SqlDbType.VarChar,
                estados != "null" ? (Object)estados : DBNull.Value);
                
			procParams[2] = new ProcedureParameters(
                "@GerentesProduto",
				System.Data.SqlDbType.VarChar,
                gerentesProduto != "null" ? (Object)gerentesProduto : DBNull.Value);

            procParams[3] = new ProcedureParameters(
                "@Familias",
                 System.Data.SqlDbType.VarChar,
                 familias != "null" ? (Object)familias : DBNull.Value);

            procParams[4] = new ProcedureParameters(
                "@Marcas",
                System.Data.SqlDbType.VarChar,
                marcas != "null" ? (Object)marcas : DBNull.Value);

            procParams[5] = new ProcedureParameters(
                "@Pesquisa",
                System.Data.SqlDbType.VarChar,
                pesquisa != "null" ? (Object)pesquisa : DBNull.Value);

            procParams[6] = new ProcedureParameters(
                "@UsuarioID",
				System.Data.SqlDbType.Int,
                usuarioID != null ? (Object)usuarioID.ToString() : DBNull.Value);

            procParams[7] = new ProcedureParameters(
                "@Aprovar",
                System.Data.SqlDbType.Bit,
                aprovar != null ? (Object)aprovar.ToString() : DBNull.Value);

            procParams[8] = new ProcedureParameters(
                 "@EhEmailMarjeting",
                 System.Data.SqlDbType.Bit,
                 emailMarketing != null ? (Object)emailMarketing.ToString() : DBNull.Value);

			return DataInterfaceObj.execQueryProcedure(
                "sp_ProdutoMargemMinima_Pesquisar",
				procParams);
		}

		public string sEmpresas
		{
			get { return empresas; }
            set { empresas = (value != null ? value : ""); }
		}

        public string sEstados
        {
            get { return estados; }
            set { estados = (value != null ? value : ""); }
        }

        public string sGerentesProduto
        {
            get { return gerentesProduto; }
            set { gerentesProduto = (value != null ? value : ""); }
        }

        public string sFamilias
        {
            get { return familias; }
            set { familias = (value != null ? value : ""); }
        }

        public string sMarcas
        {
            get { return marcas; }
            set { marcas = (value != null ? value : ""); }
        }

        public string sPesquisa
        {
            get { return pesquisa; }
            set { pesquisa = (value != null ? value : ""); }
        }
        
        public Integer nUsuarioID
		{
			get { return usuarioID; }
			set { usuarioID = (value.intValue() == 0 ? null : value); }
		}

        public java.lang.Boolean bEmailMarketing
        {
            get { return emailMarketing; }
            set { emailMarketing = (value != null ? value : new java.lang.Boolean(false)); }
        }

        public java.lang.Boolean bAprovar
        {
            get { return aprovar; }
            set { aprovar = (value != null ? value : new java.lang.Boolean(false)); }
        }
	}
}
