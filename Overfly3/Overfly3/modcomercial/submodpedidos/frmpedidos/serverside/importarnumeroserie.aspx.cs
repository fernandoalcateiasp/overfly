using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmpedidos.serverside
{
	public partial class importarnumeroserie : System.Web.UI.OverflyPage
	{
		private Integer pedidoId;
        private Integer usuarioId;
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(PedidoImportaNumeroSerie());
		}

		// Roda a procedure sp_Pedido_ImportaNumeroSerie
		private DataSet PedidoImportaNumeroSerie()
		{
			ProcedureParameters[] procParams = new ProcedureParameters[3];

			procParams[0] = new ProcedureParameters(
				"@PedidoID",
				System.Data.SqlDbType.Int,
				pedidoId.ToString());

            procParams[1] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                usuarioId.ToString());
            
            procParams[2] = new ProcedureParameters(
				"@Resultado",
				System.Data.SqlDbType.VarChar,
				DBNull.Value,
				ParameterDirection.InputOutput);
			procParams[2].Length = 8000;

			DataInterfaceObj.execNonQueryProcedure(
				"sp_Pedido_ImportaNumeroSerie",
				procParams);

			return DataInterfaceObj.getRemoteData(
				"select " +
				((procParams[2].Data != DBNull.Value) ? "'" + procParams[2].Data.ToString() + "'" : " NULL ") + 
				" as Resultado"
			);
		}

		public Integer nPedidoID
		{
			get { return pedidoId; }
			set { pedidoId = value != null ? value : new Integer(0); }
		}

        public Integer nUsuarioID
        {
            get { return usuarioId; }
            set { usuarioId = value != null ? value : new Integer(0); }
        }
	}
}
