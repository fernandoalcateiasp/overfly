/********************************************************************
pedidossup01.js

Library javascript para o pedidossup01.asp
********************************************************************/

/********************************************************************
Descricao da interface pedidos supx

Campos Gerais 1
lblRegistroID - txtRegistroID
lblEstadoID - txtEstadoID - DATASRC = dsoStateMachine - DATAFLD = Estado
lbldtPedido - txtdtPedido - DATASRC = dsoSup01 - DATAFLD = V_dtPedido
lblPessoaID - selPessoaID - DATASRC = dsoSup01 - DATAFLD= PessoaID
btnFindPessoa
btnFindTransacao
Final de Campos Gerais 1

Pedido ReferenciaID - Visibilidade depende do tipo de Pedido
lblPedidoReferenciaID - txtPedidoReferenciaID - DATASRC = dsoSup01 - DATAFLD = PedidoReferenciaID
Final de Pedido ReferenciaID

Campos Gerais 2
Travado dependendo de implantacao
lblPedidoMae - chkPedidoMae - DATASRC = dsoSup01 - DATAFLD = EhPedidoMae
Final de Travado dependendo de implantacao

lblSuspenso - chkSuspenso - DATASRC = dsoSup01 - DATAFLD = Suspenso
Final de Campos Gerais 2

Nota Fiscal
lblTemNF - chkTemNF - DATASRC = dsoSup01 - DATAFLD = TemNF
lblNotaFiscalID - selNotaFiscalID - DATASRC = dsoSup01 - DATAFLD = NotaFiscalID
lblDataNF - txtDataNF - DATASRC = dsoSup01 - DATAFLD = dtNotaFiscal
Final de Nota Fiscal

Campos Gerais 3
lblObservacao - txtObservacao - DATASRC = dsoSup01 - DATAFLD = Observacao
lblSeuPedido - txtSeuPedido - DATASRC = dsoSup01 - DATAFLD = SeuPedido
lblOrigemPedidoID - selOrigemPedidoID - DATASRC = dsoSup01 - DATAFLD = OrigemPedidoID
Final de Campos Gerais 3

Valores
Travado dependendo de implantacao
lblImpostosIncidencia - chkImpostosIncidencia - DATASRC = dsoSup01 - DATAFLD = ImpostosIncidencia
lblImpostosEsquema - chkImpostosEsquema - DATASRC = dsoSup01 - DATAFLD = ImpostosEsquema
lblAtualizaPrecos - chkAtualizaPrecos - DATASRC = dsoSup01 - DATAFLD = AtualizaPrecos
Final de Travado dependendo de implantacao

lblMoedaID - selMoedaID - DATASRC = dsoSup01 - DATAFLD = MoedaID
lblArredondaPreco - chkArredondaPreco - DATASRC = dsoSup01 - DATAFLD = ArredondaPreco

Campos Read Only
lblTaxaMoeda - txtTaxaMoeda - DATASRC = dsoSup01 - DATAFLD = TaxaMoeda
lblNumeroItens - txtNumeroItens - DATASRC = dsoSup01 - DATAFLD = NumeroItens
Final de Campos Read Only

lblFinanciamentoID - selFinanciamentoID - DATASRC = dsoSup01 - DATAFLD = FinanciamentoID
lblNumeroParcelas - selNumeroParcelas - DATASRC = dsoSup01 - DATAFLD = NumeroParcelas
lblPrazo1 - txtPrazo1 - DATASRC = dsoSup01 - DATAFLD = Prazo1
lblIncremento - txtIncremento - DATASRC = dsoSup01 - DATAFLD = Incremento

Campos Read Only
lblPrazoMedioPagamento - txtPrazoMedioPagamento - DATASRC = dsoSup01 - DATAFLD = PrazoMedioPagamento
lblVariacao - txtVariacao - DATASRC = dsoSup01 - DATAFLD = Variacao
Final de Campos Read Only

lblPercentualSUP - txtPercentualSUP - DATASRC = dsoSup01 - DATAFLD = PercentualSUP

Campos Read Only
lblValorFrete - txtValorFrete - DATASRC = dsoSup01 - DATAFLD = ValorFrete
lblValorSeguro - txtValorSeguro - DATASRC = dsoSup01 - DATAFLD = ValorSeguro
lblValorTotalServicos - txtValorTotalServicos - DATASRC = dsoSup01 - DATAFLD = ValorTotalServicos
lblValorTotalProdutos - txtValorTotalProdutos - DATASRC = dsoSup01 - DATAFLD = ValorTotalProdutos
lblValorTotalImpostos - txtValorTotalImpostos - DATASRC = dsoSup01 - DATAFLD = ValorTotalImpostos
lblValorTotalPedido - txtValorTotalPedido - DATASRC = dsoSup01 - DATAFLD = ValorTotalPedido
Final de Campos Read Only

Margem de Contribuicao - Visibilidade depende se o Pedido gera ou nao resultado
lblMargemContribuicao - txtMargemContribuicao - DATASRC = dsoSup01 - DATAFLD = MargemContribuicao
Final de Margem de Contribuicao

Final de Valores

Transporte
lblTransportadoraID - selTransportadoraID - DATASRC = dsoSup01 - DATAFLD = TransportadoraID
lblMeioTransporteID - selMeioTransporteID - DATASRC = dsoSup01 - DATAFLD = MeioTransporteID
lblModalidadeTransporteID - selModalidadeTransporteID - DATASRC = dsoSup01 - DATAFLD = ModalidadeTransporteID
lblFrete - chkFrete - DATASRC = dsoSup01 - DATAFLD = Frete
lbldtPrevisaoEntrega - txtdtPrevisaoEntrega - DATASRC = dsoSup01 - DATAFLD = V_dtPrevisaoEntrega
lblAtrasoExterno - chkAtrasoExterno - DATASRC = dsoSup01 - DATAFLD = AtrasoExterno
Final de Transporte

********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// variavel de timer
var glb_TimerPedidoVar = null;
var glb_bCotacaoToProposta = false;
// controla o retorno do servidor dos dados de combos dinamicos
// selSujeitoID e selObjetoID
var glb_CounterCmbsDynamics = 0;
// guarda o botao clicado enviado pelo frameword
var glb_BtnFromFramWork = null;
// guarda o ID do Financiamento Padrao
var glb_defFinanPadraoID = 0;
// controle usado pela funcao atualizacaoPedido para saber
// se ela deve ou nao gerar os registros da Nota Fiscal do Pedido
var glb_bUnlockFromParceiro = false;
// variavel que contem o numero maximo de itens de um pedido
var glb_NumeroMaxItens = 0;
// variavel que contem o numero maximo de parcelas de um pedido
var glb_NumeroMaxParcelas = 0;
// variavel que contem o numero de Dias Base da empresa logada
var glb_DiasBase = 0;
// variavel que contem o numero maximo de PMP
var glb_PMPMaximo = 0;
// contem novo EstadoID
var glb_nNewEstadoIDPedido = null;
var glb_bLimiteMaximo = 0;
// contem atual EstadoID
var glb_nCurrEstadoIDPedido = null;
// Executa preenchimento de combos de transporte no on change do parceiro
var glb_bExecFillTransporte = false;
var glb_BloquetoSupTimer = null;

var glb_PessoaDesativa = false;
var glb_ParceiroDesativo = false;
var glb_ProdutoDesativo = false;
var glb_Importacao = false;

var glb_TomacaoServico = false;
var glb_Edit = false;

var glb_Empresa = false;
var glb_FirstLoad = true;
var glb_PaisPessoa = 0;

var glb_PerfilID = null;
var glb_VisualizaMargem = false;
var glb_OrigemPedidoID = null;
var glb_nUserID = getCurrUserID();
var glb_aEmpresa = getCurrEmpresaData();

var glb_bEstornaAntecipacao = false;

var dsoSup01 = new CDatatransport('dsoSup01');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoParceiroData = new CDatatransport('dsoParceiroData');
var dsoTransporte01 = new CDatatransport('dsoTransporte01');
var dsoTransporte02 = new CDatatransport('dsoTransporte02');
var dsoDynamicCmbs = new CDatatransport('dsoDynamicCmbs');
var dsoCmbDynamic05 = new CDatatransport('dsoCmbDynamic05');
var dsoCurrData = new CDatatransport('dsoCurrData');
var dsoImpressaoNFe = new CDatatransport('dsoImpressaoNFe');
var dsoCurrRelation = new CDatatransport('dsoCurrRelation');
var dsoVerificacao = new CDatatransport('dsoVerificacao');
var dsoVerificaGravaLog = new CDatatransport('dsoVerificaGravaLog');
var dsoProcessaPedido = new CDatatransport('dsoProcessaPedido');
var dsoClonaPedido = new CDatatransport('dsoClonaPedido');
var dsoMailDeposito = new CDatatransport('dsoMailDeposito');
var dsoPerfilUsuario = new CDatatransport('dsoPerfilUsuario');
var dsoPerfilVendedor = new CDatatransport('dsoPerfilVendedor');
var dsoDireitoReserva = new CDatatransport('dsoDireitoReserva');
var dsoEhClienteMagazine = new CDatatransport('dsoEhClienteMagazine');
var dsoEhCargoVarejo = new CDatatransport('dsoEhCargoVarejo');
var dsoCmbRecursoMae = new CDatatransport('dsoCmbRecursoMae');
var dsoDepositos = new CDatatransport('dsoDepositos');
var dsoLocaisEntrega = new CDatatransport('dsoLocaisEntrega');
//var dsoMoedas = new CDatatransport('dsoMoedas');
var dsoContas = new CDatatransport('dsoContas');
var dsoDireitos = new CDatatransport('dsoDireitos');
var dsoAnteciparEntrega = new CDatatransport('dsoAnteciparEntrega');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
putSpecialAttributesInControls()
prgServerSup(btnClicked)
prgInterfaceSup(btnClicked)
optChangedInCmb(cmb)
btnLupaClicked(btnClicked)
finalOfSupCascade(btnClicked)
btnBarClicked(controlBar, btnClicked)
btnBarNotEspecClicked(controlBar, btnClicked)
modalInformForm(idElement, param1, param2)
supInitEditMode()
formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
stateMachOpened( currEstadoID )
stateMachBtnOK( currEstadoID, newEstadoID )
stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
stateMachClosedEx(oldEstadoID, currEstadoID, bSavedID)
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
carrierArrived(idElement, idBrowser, param1, param2)
carrierReturning(idElement, idBrowser, param1, param2)    

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload() {
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);

    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    glb_aStaticCombos = ([['selOrigemPedidoID', '1'],
                          ['selMoedaID', '2']]);

    //glb_aStaticCombos = ([['selOrigemPedidoID', '1']]);

    windowOnLoad_1stPart();

    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/pedidosinf01.asp',
                              SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/pedidospesqlist.asp');

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);

    windowOnLoad_2ndPart();

    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'PedidoID';

    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';

    glb_Empresa = getCurrEmpresaData();

    var sSql = '';
    var nEmpresaID = getCurrEmpresaData()[0];

    //Direitos do lista de pre�o
    sSql = 'SELECT TOP 1 Direitos.Alterar1 AS A1, Direitos.Alterar2 AS A2 ' +
                'FROM RelacoesPesRec RelPesRec ' +
                    'INNER JOIN RelacoesPesRec_Perfis RelPesRecPerfis WITH(NOLOCK) ON (RelPesRec.RelacaoID = RelPesRecPerfis.RelacaoID) ' +
                    'INNER JOIN Recursos Perfis WITH(NOLOCK) ON (RelPesRecPerfis.PerfilID = Perfis.RecursoID) ' +
                    'INNER JOIN Recursos_Direitos Direitos WITH(NOLOCK) ON (Perfis.RecursoID = Direitos.PerfilID) ' +
            'WHERE (RelPesRec.SujeitoID IN ((SELECT ' + glb_nUserID + ' ' +
                                            'UNION ALL ' +
                                            'SELECT UsuarioDeID ' +
                                                'FROM DelegacaoDireitos WITH(NOLOCK) ' +
                                                'WHERE (UsuarioParaID = ' + glb_nUserID + ' AND GETDATE() BETWEEN dtInicio AND dtFim))) AND ' +
                   'RelPesRec.ObjetoID = 999 AND RelPesRec.TipoRelacaoID = 11 AND RelPesRecPerfis.EmpresaID = ' + nEmpresaID + '  AND Perfis.EstadoID = 2 AND ' +
                   'Direitos.RecursoMaeID = 5221 AND Direitos.RecursoID =  24230) ' +
            'ORDER BY Direitos.Alterar2 DESC, Direitos.Alterar1 DESC ';

    setConnection(dsoDireitos);
    dsoDireitos.SQL = sSql;
    dsoDireitos.ondatasetcomplete = dsoDireitos_DSC;
    dsoDireitos.Refresh();
}

function dsoDireitos_DSC()
{
    var nA1 = dsoDireitos.recordset['A1'].value;
    var nA2 = dsoDireitos.recordset['A2'].value;
    
    glb_VisualizaMargem = ((nA1 == 1) && (nA2 == 1));
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup', [[1, 'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
                            ['lblEstadoID', 'txtEstadoID', 2, 1, -2],
                            ['lbldtPedido', 'txtdtPedido', 10, 1, -3],
                            ['lblPessoaID', 'selPessoaID', 23, 1, -1],
                            ['btnFindPessoa', 'btn', 21, 1],
                            ['lblParceiroID', 'selParceiroID', 23, 1],
                            ['lblTransacaoID', 'selTransacaoID', 8, 1, -2],
                            ['btnFindTransacao', 'btn', 21, 1],
                            ['lblPedidoReferenciaID', 'txtPedidoReferenciaID', 8, 1],
                            ['lblPedidoComplemento', 'chkPedidoComplemento', 3, 1, -6],
                            ['lblPedidoMae', 'chkPedidoMae', 3, 2],
                            ['lblSuspenso', 'chkSuspenso', 3, 2, -6],
                            ['lblPermiteReserva', 'chkPermiteReserva', 3, 2, -6],
                            ['lblAjustesRejeicaoNFePendentes', 'chkAjustesRejeicaoNFePendentes', 3, 2, -6],
                            ['lblTemNF', 'chkTemNF', 3, 2, -3],
                            ['lblNotaFiscalID', 'selNotaFiscalID', 13, 2, -8],
                            ['lblDataNF', 'txtDataNF', 10, 2, -6],
                            ['lblObservacao', 'txtObservacao', 31, 2, -3],
                            ['lblNumeroProjeto', 'txtNumeroProjeto', 17, 2, -3],
                            ['lblSeuPedido', 'txtSeuPedido', 20, 2, -3],
                            ['lblImpostosIncidencia', 'chkImpostosIncidencia', 3, 3],
                            ['lblImpostosEsquema', 'chkImpostosEsquema', 3, 3, -5],
                            ['lblAtualizaPrecos', 'chkAtualizaPrecos', 3, 3, -6],
                            ['lblArredondaPreco', 'chkArredondaPreco', 3, 3, -6],
    //chkGeraParcelaGare Inserido a Pedido do Financeiro. SSO.:26/04/2010
                            ['lblGeraParcelaGare', 'chkGeraParcelaGare', 3, 3, -6],
    //Fim
                            ['lblMoedaID', 'selMoedaID', 8, 3],
                            ['lblTaxaMoeda', 'txtTaxaMoeda', 11, 3],
                            ['lblNumeroItens', 'txtNumeroItens', 3, 3],
                            ['lblFinanciamentoID', 'selFinanciamentoID', 15, 3],
                            ['lblNumeroParcelas', 'selNumeroParcelas', 5, 3],
                            ['lblPrazo1', 'txtPrazo1', 4, 3],
                            ['lblIncremento', 'txtIncremento', 4, 3],
                            ['lblPrazoMedioPagamento', 'txtPrazoMedioPagamento', 5, 3],
                            ['lblVariacao', 'txtVariacao', 5, 3],
                            ['lblPercentualSUP', 'txtPercentualSUP', 5, 3, -5],
                            ['lblValorFrete', 'txtValorFrete', 11, 4],
                            ['lblValorSeguro', 'txtValorSeguro', 11, 4],
                            ['lblValorTotalServicos', 'txtValorTotalServicos', 11, 4],
                            ['lblValorTotalProdutos', 'txtValorTotalProdutos', 11, 4],
                            ['lblValorTotalImpostos', 'txtValorTotalImpostos', 11, 4],
                            ['lblTotalDesconto', 'txtTotalDesconto', 11, 4],
                            ['lblValorTotalPedido', 'txtValorTotalPedido', 11, 4],
                            ['lblMargemContribuicao', 'txtMargemContribuicao', 6, 4],
                            ['lblValorOutrasDespesas', 'txtValorOutrasDespesas', 11, 4],
                            ['lblFrete', 'chkFrete', 3, 5, 0, -2],
                            ['lblTransportadoraID', 'selTransportadoraID', 15, 5, -8],
                            ['lblMeioTransporteID', 'selMeioTransporteID', 11, 5, -2],
                            ['lblModalidadeTransporteID', 'selModalidadeTransporteID', 5, 5, -2],
                            ['lblPrevisaoEntrega', 'txtPrevisaoEntrega', 16, 5, -9],
                            ['lblAtrasoExterno', 'chkAtrasoExterno', 3, 5, -2],
                            ['lblDepositoID', 'selDepositoID', 15, 5, -7],
                            ['lblLocalEntregaID', 'selLocalEntregaID', 17, 5],
                            ['lblOrigemPedidoID', 'selOrigemPedidoID', 12, 5, -3],
                            ['lblProgramaMarketing', 'txtProgramaMarketing', 15, 5, -3],
                            ['lblShipTo', 'selShipTo', 10, 5, -120],
                            ['btnFindShipTo', 'btn', 21, 5]], null, null, true);

    txtdtPrevisaoEntrega.style.width = parseInt(txtdtPrevisaoEntrega.currentStyle.width, 10) - 2;
    txtPrevisaoEntrega.style.width = parseInt(txtPrevisaoEntrega.currentStyle.width, 10) - 2;
    chkFrete.onclick = chkFrete_onclick;
}

function ajustaEntrada()
{
    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
                          ['lblEstadoID', 'txtEstadoID', 2, 1, -2],
                          ['lbldtPedido', 'txtdtPedido', 10, 1, -3],
                          ['lblPessoaID', 'selPessoaID', 23, 1, -1],
                          ['btnFindPessoa', 'btn', 21, 1],
                          ['lblParceiroID', 'selParceiroID', 23, 1],
                          ['lblTransacaoID', 'selTransacaoID', 8, 1, -2],
                          ['btnFindTransacao', 'btn', 21, 1],
                          ['lblPedidoReferenciaID', 'txtPedidoReferenciaID', 8, 1],
                          ['lblPedidoComplemento', 'chkPedidoComplemento', 3, 1, -6],
                          ['lblPedidoMae', 'chkPedidoMae', 3, 2],
                          ['lblSuspenso', 'chkSuspenso', 3, 2, -6],
                          ['lblPermiteReserva', 'chkPermiteReserva', 3, 2, -6],
                          ['lblAjustesRejeicaoNFePendentes', 'chkAjustesRejeicaoNFePendentes', 3, 2, -6],
                          ['lblTemNF', 'chkTemNF', 3, 2, -3],
                          ['lblNotaFiscalID', 'selNotaFiscalID', 13, 2, -8],
                          ['lblDataNF', 'txtDataNF', 10, 2, -6],
                          ['lblObservacao', 'txtObservacao', 31, 2, -3],
                          ['lblNumeroProjeto', 'txtNumeroProjeto', 17, 2, -3],
                          ['lblSeuPedido', 'txtSeuPedido', 20, 2, -3],
                          ['lblImpostosIncidencia', 'chkImpostosIncidencia', 3, 3],
                          ['lblImpostosEsquema', 'chkImpostosEsquema', 3, 3, -5],
                          ['lblAtualizaPrecos', 'chkAtualizaPrecos', 3, 3, -6],
                          ['lblArredondaPreco', 'chkArredondaPreco', 3, 3, -6],
    //chkGeraParcelaGare Inserido a Pedido do Financeiro. SSO.:26/04/2010
                          ['lblGeraParcelaGare', 'chkGeraParcelaGare', 3, 3, -6],
    //Fim
                          ['lblMoedaID', 'selMoedaID', 8, 3],
                          ['lblTaxaMoeda', 'txtTaxaMoeda', 11, 3],
                          ['lblNumeroItens', 'txtNumeroItens', 3, 3],
                          ['lblFinanciamentoID', 'selFinanciamentoID', 15, 3],
                          ['lblNumeroParcelas', 'selNumeroParcelas', 5, 3],
                          ['lblPrazo1', 'txtPrazo1', 4, 3],
                          ['lblIncremento', 'txtIncremento', 4, 3],
                          ['lblPrazoMedioPagamento', 'txtPrazoMedioPagamento', 5, 3],
                          ['lblVariacao', 'txtVariacao', 5, 3],
                          ['lblPercentualSUP', 'txtPercentualSUP', 5, 3, -5],
                          ['lblValorFrete', 'txtValorFrete', 11, 4],
                          ['lblValorSeguro', 'txtValorSeguro', 11, 4],
                          ['lblValorTotalServicos', 'txtValorTotalServicos', 11, 4],
                          ['lblValorTotalProdutos', 'txtValorTotalProdutos', 11, 4],
                          ['lblValorTotalImpostos', 'txtValorTotalImpostos', 11, 4],
                          ['lblTotalDesconto', 'txtTotalDesconto', 11, 4],
                          ['lblValorTotalPedido', 'txtValorTotalPedido', 11, 4],
                          ['lblMargemContribuicao', 'txtMargemContribuicao', 6, 4],
                          ['lblValorOutrasDespesas', 'txtValorOutrasDespesas', 11, 4],
                          ['lblFrete', 'chkFrete', 3, 5, 0, -2],
                          ['lblTransportadoraID', 'selTransportadoraID', 15, 5, -8],
                          ['lblMeioTransporteID', 'selMeioTransporteID', 11, 5, -2],
                          ['lblModalidadeTransporteID', 'selModalidadeTransporteID', 5, 5, -2],
                          ['lbldtPrevisaoEntrega', 'txtdtPrevisaoEntrega', 16, 5, -9],
                          ['lblAtrasoExterno', 'chkAtrasoExterno', 3, 5, -2],
                          ['lblDepositoID', 'selDepositoID', 15, 5, -7],
                          ['lblLocalEntregaID', 'selLocalEntregaID', 17, 5],
                          ['lblOrigemPedidoID', 'selOrigemPedidoID', 12, 5, -3],
                          ['lblProgramaMarketing', 'txtProgramaMarketing', 15, 5, -3],
                          ['lblShipTo', 'selShipTo', 10, 5, -120],
                          ['btnFindShipTo', 'btn', 21, 5]], null, null, true);
}

// CARRIER **********************************************************
/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/
/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2) {
    if (param1 == 'SHOWPEDIDO') {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if (__currFormState() != 0)
            return null;

        // param2 - traz array:
        // o id da empresa do form que mandou
        // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();

        if (param2[0] != empresa[0])
            return null;

        window.top.focus();

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);

        return new Array(getHtmlId(), null, null);
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2) {
    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.

    // labels coloridos
    lbldtPedido.style.color = 'green';
    lblPessoaID.style.color = 'blue';
    lblParceiroID.style.color = 'green';
    lblTransacaoID.style.color = 'green';
    lblSuspenso.style.color = 'green';
    lblAjustesRejeicaoNFePendentes.color = 'green';
    lblTemNF.style.color = 'green';
    lblOrigemPedidoID.style.color = 'green';
    lblProgramaMarketing.style.color = 'green';
    lblObservacao.style.color = 'green';
    lblSeuPedido.style.color = 'green';
    lblAtualizaPrecos.style.color = 'green';
    lblArredondaPreco.style.color = 'green';
    lblFinanciamentoID.style.color = 'green';
    lblNumeroParcelas.style.color = 'green';
    lblPrazo1.style.color = 'green';
    lblIncremento.style.color = 'green';
    lblValorFrete.style.color = 'green';
    lblValorSeguro.style.color = 'green';
    lblValorOutrasDespesas.style.color = 'green';
    lblTransportadoraID.style.color = 'green';
    lblModalidadeTransporteID.style.color = 'green';
    lblMeioTransporteID.style.color = 'green';
    lblFrete.style.color = 'green';
    lbldtPrevisaoEntrega.style.color = 'green';
    lblLocalEntregaID.style.color = 'green';

    // checkBox com funcao onclick
    chkTemNF.onclick = chkBoxTemNFClicked;

    // Campo para valor numerico que tem minimo e maximo
    txtPrazo1.setAttribute('minMax', new Array(0, 100), 1);
    txtIncremento.setAttribute('minMax', new Array(0, 100), 1);
    //txtPercentualSUP.setAttribute('minMax', new Array(0, 50), 1);
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad() {
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);
    // mostra quatro botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
    // os hints dos botoes especificos
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Pessoa', 'Kardex', 'Resultados', 'Detalhar Rela��o', 'Detalhar Nota Fiscal', 'Despacho', 'Apura��o de Impostos']);

    // rotina para setar as variaveis que contem o numero maximo de itens e parcelas
    // e o numero de Dias Base da empresa logada
    // de um pedido
    if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.Find('fldName', "NumeroItens");
        if (!dsoEstaticCmbs.recordset.EOF) {
            glb_NumeroMaxItens = dsoEstaticCmbs.recordset['fldID'].value;
        }
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.Find('fldName', "NumeroParcelas");
        if (!dsoEstaticCmbs.recordset.EOF) {
            glb_NumeroMaxParcelas = dsoEstaticCmbs.recordset['fldID'].value;
        }
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.Find('fldName', "DiasBase");
        if (!dsoEstaticCmbs.recordset.EOF) {
            glb_DiasBase = dsoEstaticCmbs.recordset['fldID'].value;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked) {
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;

    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR')) {
        startDynamicCmbsEx();
        finalOfSupCascade(glb_BtnFromFramWork);
    }
    else
        finalOfSupCascade(glb_BtnFromFramWork);

    adjustLabelsCombos(true);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked) {
    //@@

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    var nEstadoID;
    
    if (!(dsoSup01.recordset.BOF && dsoSup01.recordset.EOF))
    {
        nEstadoID = dsoSup01.recordset['EstadoID'].value;

        if (nEstadoID == null)
            preencheMoeda();
    }
        

    previsaoEntrega();

    if (!(dsoSup01.recordset.BOF && dsoSup01.recordset.EOF)) {
        chkPedidoComplemento.checked = (dsoSup01.recordset['PedidoComplemento'].value == true);
    }

    if ((dsoSup01.recordset['Servico'].value == 1) || (dsoSup01.recordset['NCM'].value == 1))
        ajustaCampos_Tomacao();
    else
        ajustaCampos();

    txtPrazo1.onchange = calculaPMP;
    txtIncremento.onchange = calculaPMP;

    if (btnClicked == 'SUPINCL') {
        var aItemSelected;
        var nContextoID;

        lockBtnLupa(btnFindTransacao, true);
        clearComboEx(['selParceiroID']);
        clearComboEx(['selShipTo']);
        clearComboEx(['selLocalEntregaID']);

        setLabelShipTo();
        clearCmbsTransporte();

        if (!((dsoCurrData.recordset.BOF) && (dsoCurrData.recordset.EOF))) {
            txtdtPedido.value = dsoCurrData.recordset['currDate'].value;

            // pega o value do contexto atual
            aItemSelected = getCmbCurrDataInControlBar('sup', 1);
            nContextoID = aItemSelected[1];
            // 5111->Pedido de Entrada, 5112 Pedido de Saida
            if (nContextoID == 5112)
            {
                txtdtPrevisaoEntrega.value = dsoCurrData.recordset['currDate'].value;
            }
        }

        setValueInControlLinked(chkTemNF, dsoSup01);
        setValueInControlLinked(selMoedaID, dsoSup01);

        clearComboEx(['selFinanciamentoID']);

        if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {

            dsoEstaticCmbs.recordset.MoveFirst();
            dsoEstaticCmbs.recordset.Find('fldID', dsoSup01.recordset['MoedaID'].value);

            if (!dsoEstaticCmbs.recordset.EOF) {
                txtTaxaMoeda.value = dsoEstaticCmbs.recordset['Cotacao'].value;
                if (selMoedaID.selectedIndex == -1)
                    selMoedaID.selectedIndex = 0;
            }
        }

        txtPercentualSUP.value = '0.00';

        setValueInControlLinked(selOrigemPedidoID, dsoSup01);

        // funcao que controla campos read-only
        controlReadOnlyFields(false);
        selParceiroID.disabled = true;
    }

    glb_PessoaDesativa = !dsoSup01.recordset['regAtivo'].value;
    glb_ProdutoDesativo = dsoSup01.recordset['produtoDesativo'].value;
    glb_OrigemPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'OrigemPedidoID' + '\'' + '].value');

    chkBoxTemNFClicked();

    if ((dsoSup01.recordset['TransacaoID'].value == 211) && (dsoSup01.recordset['EhImportacao'].value == 1))
        ajustaCampos_ImportacaoServico();

    pedidoReferenciaID(btnClicked);
    margemContribuicao(btnClicked);
    setShipToVisibility();

    //Caso a Margem esta negativa fica em vermelho.
    if (txtMargemContribuicao.value < 0)
        txtMargemContribuicao.style.color = 'red';
    else
        txtMargemContribuicao.style.color = 'black';

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;
    var cmbValue = cmb.value;

    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

    if (cmbID == 'selParceiroID') {
        setLabelParceiro(selParceiroID.options[selParceiroID.selectedIndex].value);

        clearComboEx(['selTransacaoID']);

        dsoParceiroData.recordset.setFilter('PessoaID=' + cmbValue);
        glb_ParceiroDesativo = !dsoParceiroData.recordset['regAtivo'].value;
        dsoParceiroData.recordset.setFilter('');

        glb_bUnlockFromParceiro = true;
        lockInterface(true);
        clearCmbsTransporte();
        glb_bExecFillTransporte = true;
        setupDSOFinanciamento('financiamento_DSC');
        fillComboDeposito();
    }
    else if (cmbID == 'selFinanciamentoID') {
        if (!((dsoCmbDynamic05.recordset.BOF) && (dsoCmbDynamic05.recordset.EOF))) {
            dsoCmbDynamic05.recordset.MoveFirst();
            dsoCmbDynamic05.recordset.Find('fldID', selFinanciamentoID.value);
            if (!dsoCmbDynamic05.recordset.EOF) {
                setValueInControlLinked(selNumeroParcelas, dsoSup01,
                                        dsoCmbDynamic05.recordset['NumeroParcelas'].value);
                txtPrazo1.value = dsoCmbDynamic05.recordset['Prazo1'].value;
                txtIncremento.value = dsoCmbDynamic05.recordset['Incremento'].value;
                txtPrazoMedioPagamento.value = dsoCmbDynamic05.recordset['PMP'].value;
                txtVariacao.value = dsoCmbDynamic05.recordset['Variacao'].value;
            }
        }
        enableDisableFieldsByFinanc();
    }
    else if (cmbID == 'selNumeroParcelas') {
        calculaPMP();
    }
    else if (cmbID == 'selMoedaID') {
        //if (!((dsoMoedas.recordset.BOF) && (dsoMoedas.recordset.EOF))) {
        //    dsoMoedas.recordset.MoveFirst();
        //    dsoMoedas.recordset.Find('fldID', cmbValue);
        //    if (!dsoMoedas.recordset.EOF) {
        //        txtTaxaMoeda.value = dsoMoedas.recordset['Cotacao'].value;
        //    }
        //}
        if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {
            dsoEstaticCmbs.recordset.MoveFirst();
            dsoEstaticCmbs.recordset.Find('fldID', cmbValue);
            if (!dsoEstaticCmbs.recordset.EOF) {
                txtTaxaMoeda.value = dsoEstaticCmbs.recordset['Cotacao'].value;
            }
        }
    }
    else if (cmbID == 'selTransportadoraID') {
        setLabelTransportadora(selTransportadoraID.options[selTransportadoraID.selectedIndex].value);
        fillCmbsSecondariesTransporte();
    }
    else if (cmbID == 'selShipTo') {
        setLabelShipTo();
    }
    else if (cmbID == 'selLocalEntregaID') {
        setLabelLocalEntrega(selLocalEntregaID.options[selLocalEntregaID.selectedIndex].value);
    }

    adjustLabelsCombos(false);

    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selTipoRegistroID')
        adjustSupInterface();
    // Final de Nao mexer - Inicio de automacao =====================

}

function adjustLabelsCombos(bPaging) {
    if (bPaging)
        setLabelOfControl(lblContaID, dsoSup01.recordset['ContaID'].value);
    else
        setLabelOfControl(lblContaID, selContaID.value);
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro
newEstadoID     - Novo EstadoID selecionado

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar true nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK(currEstadoID, newEstadoID) {
    // Primeiro parametro para funcao openModalVolAndDataTransp
    // 1 - Numero de volumes e Dados da Nota (Entrada)
    // 3 - Numero de volumes (Saida)
    // 4 - Dados da Nota Fiscal (Saida)
    // 5 - Dados de Transporte (Saida)

    // Alterado para fazer verifica��es tamb�m de C para A. BJBN 03/03/2010
    if (((currEstadoID == 21) || (currEstadoID == 22) || (currEstadoID == 23)) && (newEstadoID == 24)) {
        sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bOpenModalGVL = true');
    }

    // Pedido de Entrada
    if (dsoSup01.recordset['TipoPedidoID'].value == 601) {
        // Cotacao->Proposta 
        if ((currEstadoID == 21) && (newEstadoID == 22)) {
            verifyPedidoInServer(currEstadoID, newEstadoID);
            return true;
        }
        // Alterado para fazer verifica��es tamb�m de C para A. BJBN 03/03/2010
        // Proposta ->Reserva ou Proposta -> Aprovacao ou Cota��o -> Aprova��o
        if (((currEstadoID == 22) && ((newEstadoID == 23) || (newEstadoID == 24))) || ((currEstadoID == 21) && (newEstadoID == 24))) {
            verifyPedidoInServer(currEstadoID, newEstadoID);
            return true;
        }
            //Alterado para fazer verifica��es de A/P para C. SGP 06/03/2014
        else if (((currEstadoID == 22) && (newEstadoID == 21)) ||
                 ((currEstadoID == 23) && (newEstadoID == 21)) ||
				 ((currEstadoID == 24) && (newEstadoID == 21))) {
            verifyPedidoInServer(currEstadoID, newEstadoID);
            return true;
        }
            // Aprovacao->Faturamento 
        else if ((currEstadoID == 24) && (newEstadoID == 26)) {
            verifyPedidoInServer(currEstadoID, newEstadoID);
            return true;
        }
            // Faturamento - > Aprova��o
        else if ((currEstadoID == 26) && (newEstadoID == 24)) {
            verifyPedidoInServer(currEstadoID, newEstadoID);
            return true;
        }
            // Faturamento - > Recebimento (No caso de Pedido com Nota)
        else if ((currEstadoID == 26) && (newEstadoID == 27)) {

            if (!(dsoSup01.recordset['TransacaoID'].value == 211 && dsoSup01.recordset['EhImportacao'].value == 1))
            {
                if (dsoSup01.recordset['TemNF'].value == true)
                {
                    // esconde a modal de maquina de estado
                    showFrameInHtmlTop('frameModal', false);

                    openModalVolAndDataTransp(1, currEstadoID, newEstadoID); // abre a modal de volumes 
                }
                else
                    verifyPedidoInServer(currEstadoID, newEstadoID);
            }
            else
                verifyPedidoInServer(currEstadoID, newEstadoID);

            return true;
        }
            // Recebimento - > Faturamento
        else if ((currEstadoID == 27) && (newEstadoID == 26)) {
            verifyPedidoInServer(currEstadoID, newEstadoID);
            return true;
        }
            // Recebimento - > Inspecao
        else if ((currEstadoID == 27) && (newEstadoID == 28)) {
            // esconde a modal de maquina de estado
            showFrameInHtmlTop('frameModal', false);
            // abre a modal de dados de transporte
            openModalVolAndDataTransp(5, currEstadoID, newEstadoID);

            return true;
        }
            // Inspecao - > Estocado
        else if ((currEstadoID == 28) && (newEstadoID == 30)) {
            verifyPedidoInServer(currEstadoID, newEstadoID);
            return true;
        }
        else {
            //@@ prossegue a automacao
            return false;
        }
    }
        // Pedido de Saida
    else if (dsoSup01.recordset['TipoPedidoID'].value == 602) {
        // Alterado para fazer verifica��es tamb�m de C para A. BJBN 03/03/2010
        // Cotacao->Proposta ou Aprova��o
        if ((currEstadoID == 21) && ((newEstadoID == 22) || (newEstadoID == 24))) {
            //verifyPedidoInServer(currEstadoID, newEstadoID);
            // esconde a modal de maquina de estado
            showFrameInHtmlTop('frameModal', false);
            // abre a modal de volumes 
            openModalCadastro(currEstadoID, newEstadoID);
            return true;
        }
            //Alterado para fazer verifica��es de A/P para C. SGP 06/03/2014
        else if (((currEstadoID == 22) && (newEstadoID == 21)) ||
                 ((currEstadoID == 23) && (newEstadoID == 21)) ||
				 ((currEstadoID == 24) && (newEstadoID == 21))) {
            verifyPedidoInServer(currEstadoID, newEstadoID);
            return true;
        }
            // Alterado para fazer verifica��es tamb�m de C para A. BJBN 03/03/2010
            // Proposta->Reserva ou Reserva->Aprovacao ou Cota��o->Aprovacao ou Proposta->Aprovacao
        else if (((currEstadoID == 22) && (newEstadoID == 23)) ||
				 ((currEstadoID == 23) && (newEstadoID == 24)) ||
				 ((currEstadoID == 21) && (newEstadoID == 24)) ||
				 ((currEstadoID == 22) && (newEstadoID == 24))) {
            verifyPedidoInServer(currEstadoID, newEstadoID);
            return true;
        }
            // Aprovacao->Embalagem 
            // Material->Embalagem 
        else if (((currEstadoID == 24) && (newEstadoID == 25)) ||
                ((currEstadoID == 33) && (newEstadoID == 25))) {
            //verifyPedidoInServer(currEstadoID, newEstadoID);
            // esconde a modal de maquina de estado
            showFrameInHtmlTop('frameModal', false);
            // abre a modal de volumes 
            openModalCadastro(currEstadoID, newEstadoID);
            return true;
        }
            // Embalagem->Aprovacao
        else if ((currEstadoID == 25) && (newEstadoID == 24)) {
            verifyPedidoInServer(currEstadoID, newEstadoID);
            return true;
        }
            // Embalagem - > Faturamento
        else if ((currEstadoID == 25) && (newEstadoID == 26)) {
            // esconde a modal de maquina de estado
            showFrameInHtmlTop('frameModal', false);
            // abre a modal de volumes 
            openModalVolAndDataTransp(3, currEstadoID, newEstadoID);

            return true;
        }
            // Faturamento - > Embalagem
        else if ((currEstadoID == 26) && (newEstadoID == 25)) {
            verifyPedidoInServer(currEstadoID, newEstadoID);
            return true;
        }
            // Faturamento - > Expedicao
        else if ((currEstadoID == 26) && (newEstadoID == 29) && (dsoSup01.recordset['TemNF'].value == true)) {
            var nPedidoEmitiraNFe = dsoSup01.recordset['PedidoEmitiraNFe'].value;

            if (nPedidoEmitiraNFe == true) {
                verifyPedidoInServer(currEstadoID, newEstadoID);
                return true;
            }
            else {
                // esconde a modal de maquina de estado
                showFrameInHtmlTop('frameModal', false);
                // abre a modal de dados de Nota Fiscal
                openModalVolAndDataTransp(4, currEstadoID, newEstadoID);
            }
            return true;
        }
            // Expedicao - > Despachado
        else if ((currEstadoID == 29) && (newEstadoID == 31)) {
            // esconde a modal de maquina de estado
            showFrameInHtmlTop('frameModal', false);
            // abre a modal de dados de transporte
            openModalVolAndDataTransp(5, currEstadoID, newEstadoID);

            return true;
        }
            // Cota��o->Deletar
        else if ((currEstadoID == 21) && (newEstadoID == 5)) {

            var retorno = false;
            var bPermiteEstornoAntecipacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PermiteEstornoAntecipacao' + '\'' + '].value');

            bPermiteEstornoAntecipacao = Boolean(bPermiteEstornoAntecipacao);

            if (bPermiteEstornoAntecipacao) {
                var _retConf = window.top.overflyGen.Confirm('Estornar antecipa��o de entrega?');

                // Se sim, seta vari�vel global para executar procedure de antecipa��o
                if (_retConf == 1) {
                    glb_bEstornaAntecipacao = true;
                    verifyPedidoInServer(currEstadoID, newEstadoID);
                    return true;
                }
                else
                    retorno = true;
            }

            // esconde a modal de maquina de estado
            showFrameInHtmlTop('frameModal', false);
            lockInterface(false);
            return retorno;
        }
        else
            //@@ prossegue a automacao
            return false;
    }

    //@@ prossegue a automacao
    return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID, bSavedID) {
    // Apenas se a alteracao de estado foi feita
    if (bSavedID) {
        // Pedido de Entrada
        if (dsoSup01.recordset['TipoPedidoID'].value == 601) {
            // Opera��o (Proposta->Cotacao)
            if ((oldEstadoID == 22) && (currEstadoID == 21)) {
                glb_TimerPedidoVar = window.setInterval('refreshSup()', 10, 'JavaScript');
            }
                // Alterado para fazer verifica��es tamb�m de C para A. BJBN 03/03/2010
            else if ((oldEstadoID == 21) && ((currEstadoID == 22) || (currEstadoID == 24))) {
                glb_bCotacaoToProposta = true;
            }

                // Faturamento - > Recebimento (No caso de Pedido sem Nota)
            else if ((oldEstadoID == 26) && (currEstadoID == 27) && (dsoSup01.recordset['TemNF'].value == false))
                glb_TimerPedidoVar = window.setInterval('refreshSup()', 10, 'JavaScript');
        }
        // Pedido de Saida 
        if (dsoSup01.recordset['TipoPedidoID'].value == 602) {
            // Opera��o (Proposta->Cotacao)
            if ((oldEstadoID == 22) && (currEstadoID == 21)) {
                glb_TimerPedidoVar = window.setInterval('refreshSup()', 10, 'JavaScript');
            }
                // Opera��o Invertida (Expedi��o->Faturamento)
            else if ((oldEstadoID == 29) && (currEstadoID == 26)) {
                glb_TimerPedidoVar = window.setInterval('refreshSup()', 10, 'JavaScript');
            }
        }

    }
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Independente de fechar a janela da maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosedEx(oldEstadoID, currEstadoID, bSavedID) {
    // (PE F->F OR PS E->F) PedidoEmitiraNFe
    if ((((oldEstadoID == 26) && (currEstadoID == 26) && (bSavedID == false)) ||
         ((oldEstadoID == 25) && (currEstadoID == 26) && (bSavedID == true))) && (dsoSup01.recordset['PedidoEmitiraNFe'].value == true)) {
        if ((oldEstadoID == 26) && (currEstadoID == 26) && (bSavedID == false) && (dsoSup01.recordset['NotaFiscalID'].value > 0) && (dsoSup01.recordset['Servico'].value != 1)) {
            if (window.top.overflyGen.Alert('NFe esta em processamento.') == 0)
                return null;
        }

        glb_TimerPedidoVar = window.setInterval('refreshSup()', 10, 'JavaScript');
    }
}

function estornoAntecipacao() {

    lockInterface(true);
    
    glb_bEstornaAntecipacao = false;

    var strPars = '';
    var nPedidoID = dsoSup01.recordset['PedidoID'].value;
    
    strPars += '?nUsuarioID=' + glb_nUserID.toString();
    strPars += '&nPedidoID=' + nPedidoID;
    strPars += '&nTipoResultado=2';
    
    dsoAnteciparEntrega.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/anteciparentrega.aspx' + strPars;
    dsoAnteciparEntrega.ondatasetcomplete = estornoAntecipacao_DSC;
    dsoAnteciparEntrega.Refresh();
}

function estornoAntecipacao_DSC() {
    //destrava a Interface
    lockInterface(false);

    var sMensagem = (dsoAnteciparEntrega.recordset.Fields['Mensagem'].value == null ? '' : dsoAnteciparEntrega.recordset.Fields['Mensagem'].value);
    var nResultado = (dsoAnteciparEntrega.recordset.Fields['Resultado'].value == null ? 0 : dsoAnteciparEntrega.recordset.Fields['Resultado'].value);

    if (nResultado == 0) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }
        stateMachSupExec('OK');
    }
    else if (nResultado == 1) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }
        stateMachSupExec('CANC');
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {
    if (btnClicked.id == 'btnFindPessoa')
        openModalPessoa();
    else if (btnClicked.id == 'btnFindTransacao')
        openModalTransacao();
    else if (btnClicked.id == 'btnFindShipTo')
        openModalShipTo();
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    var contexto = 0;
    var nEstadoID = 0;
    var nProcedimentoID = 0;
    var sAnchor = '';

    var empresa = getCurrEmpresaData();
    if (controlBar == 'SUP') {
        // Documentos
        if ((controlBar == 'SUP') && (btnClicked == 1)) {
            __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
        }
        else if (btnClicked == 2)
            openModalPrint(false);
        else if (btnClicked == 3) {
            contexto = getCmbCurrDataInControlBar('sup', 1);
            nEstadoID = dsoSup01.recordset['EstadoID'].value;

            if (contexto[1] == 5111) {
                if (nEstadoID == 21) {
                    nProcedimentoID = 740;
                    sAnchor = '231';
                }
                else if (nEstadoID == 22) {
                    nProcedimentoID = 740;
                    sAnchor = '232';
                }
                else if (nEstadoID == 23) {
                    nProcedimentoID = 740;
                    sAnchor = '233';
                }
                else if (nEstadoID == 24) {
                    nProcedimentoID = 740;
                    sAnchor = '234';
                }
                else if (nEstadoID == 26) {
                    nProcedimentoID = 751;
                    sAnchor = '2151';
                }
                else if (nEstadoID == 27) {
                    nProcedimentoID = 743;
                    sAnchor = '21';
                }
                else if (nEstadoID == 28) {
                    nProcedimentoID = 743;
                    sAnchor = '22';
                }
                else if ((nEstadoID == 30) || (nEstadoID == 32) || (nEstadoID == 5)) {
                    nProcedimentoID = 740;
                    sAnchor = '23';
                }
            }
            else {
                if (nEstadoID == 21) {
                    nProcedimentoID = 720;
                    sAnchor = '221';
                }
                else if (nEstadoID == 22) {
                    nProcedimentoID = 720;
                    sAnchor = '222';
                }
                else if (nEstadoID == 23) {
                    nProcedimentoID = 720;
                    sAnchor = '223';
                }
                else if (nEstadoID == 24) {
                    nProcedimentoID = 720;
                    sAnchor = '224';
                }
                else if (nEstadoID == 25) {
                    nProcedimentoID = 751;
                    sAnchor = '2142';
                }
                else if (nEstadoID == 26) {
                    nProcedimentoID = 751;
                    sAnchor = '2152';
                }
                else if (nEstadoID == 29) {
                    nProcedimentoID = 751;
                    sAnchor = '216';
                }
                else if ((nEstadoID == 31) || (nEstadoID == 32) || (nEstadoID == 33) || (nEstadoID == 5)) {
                    nProcedimentoID = 720;
                    sAnchor = '22';
                }
            }
            window.top.openModalControleDocumento('S', '', nProcedimentoID, null, sAnchor, 'T');
        }
            // Detalhar Pessoa
        else if (btnClicked == 4) {
            // Manda o id da pessoa a detalhar 
            sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], selPessoaID.value));
        }
            // Detalhar relacao
        else if (btnClicked == 5) {
            // Manda o id da relacao entre a empresa e o parceiro
            // a detalhar indo antes ao servidor
            openRelationByCarrier();
        }
            // Abrir Modal Kardex
        else if (btnClicked == 7) {

            openModalKardex(txtRegistroID.value);
        }
            // Abrir Modal de Resultados
        else if (btnClicked == 8) {

            openModalResultados(txtRegistroID.value);
        }
            //Importa��o
        else if (btnClicked == 11)
            openModalImportacao();
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
- Avancar, retroagir, incluir, alterar, estado, 
excluir, OK, cancelar refresh, anterior e proximo. 
(Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    var aItemSelected;
    var nContextoID;
    var nEmpresaID = getCurrEmpresaData();
    var nPedidoID;
    var nTipoPedido;
    var fldIDName = null; //dsoSup01.recordset[glb_sFldIDName].value;

    //Alterado para o caso de toma��o de servi�o.
    var Hoje = new Date();
    Hoje.setDate(Hoje.getDate() + 1);

    var Dia = ((Hoje.getDate() < 10) ? '0' + Hoje.getDate().toString() : Hoje.getDate().toString());

    var Mes = Hoje.getMonth();
    Mes = Mes + 1;

    Mes = ((Mes < 10) ? '0' + Mes : Mes);

    glb_BtnFromFramWork = btnClicked;

    if (btnClicked == 'SUPOK')
    {
        if ((glb_OrigemPedidoID != 608) && (selOrigemPedidoID.value == 608)) {
            window.top.overflyGen.Alert('A origem do pedido n�o pode ser alterada para PoweredBy.');
            refreshSup();
        }

        //Trava para usuario nao alterar pedido para origem no marketplace
        if ((glb_OrigemPedidoID != 607) && (selOrigemPedidoID.value == 607)) {
            window.top.overflyGen.Alert('A origem do pedido n�o pode ser alterada para Marketplace.');
            refreshSup();
        }

        //Trava para usuario nao alterar pedido para origem no marketplace
        if ((glb_OrigemPedidoID == 607) && (selOrigemPedidoID.value != 607)) {
            window.top.overflyGen.Alert('A origem do pedido n�o pode ser alterada.');
            refreshSup();
        }


        //Preenche dados para o caso de toma��o de servi�o.
        if (((glb_TomacaoServico) || (dsoSup01.recordset['ProdutoServico'].value == 1)) &&
            ((dsoSup01.recordset['EstadoID'].value == 21) || ((typeof (dsoSup01.recordset[glb_sFldIDName].value)).toUpperCase() == 'UNDEFINED')) &&
            (dsoSup01.recordset['TransacaoID'].value != 832) && (dsoSup01.recordset['TransacaoID'].value != 836)) {
            Hoje = Dia + '/' + Mes + '/' + Hoje.getFullYear().toString();

            selTransportadoraID.value = selParceiroID.value; //Preenche Transportadora com o mesmo para o caso de toma��o de servi�o.

            selDepositoID.value = depositoEmpresa(nEmpresaID[0]); //Preenche deposito com a empresa no caso de toma��o de servi�o.

            txtdtPrevisaoEntrega.value = Hoje; //Preenche previs�o de entrega com a data do dia para o caso de toma��o de servi�o.
        }

        // Pedido esta em inclusao
        if (dsoSup01.recordset[glb_sFldIDName].value == null) {
            txtRegistroID.value = trimStr(txtRegistroID.value);
            nPedidoID = txtRegistroID.value;
            if (nPedidoID != '') {
                var _retConf = window.top.overflyGen.Confirm('Gerar Pedido a partir do Pedido: ' + nPedidoID + '?');

                if (_retConf == 0)
                    return null;
                else if (_retConf == 1) {

                    var _retConf2 = window.top.overflyGen.Confirm('Pedido para Nota Fiscal Complementar?');
                    if (_retConf2 == 1)
                        nTipoPedido = 3;
                    else
                        nTipoPedido = 1;

                    lockInterface(true);
                    dsoClonaPedido.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/clonapedido.aspx?nPedidoID=' +
					                     escape(txtRegistroID.value) +
					                     '&nEmpresaID=' + escape(nEmpresaID[0]) +
					                     '&nUserID=' + escape(glb_nUserID) + '&nTipoPedido=' + escape(nTipoPedido) + '&nDataLen=' + escape(1);

                    dsoClonaPedido.ondatasetcomplete = dsoClonaPedido_DSC;
                    dsoClonaPedido.refresh();

                    return true;
                }
                txtRegistroID.value = '';
            }
        }

        var sPrevisaoDate, sToday;

        if (!glb_TomacaoServico) {
            sPrevisaoDate = txtdtPrevisaoEntrega.value;
        }  

        sToday = txtdtPedido.value;

        if (!glb_TomacaoServico) {
            if (strToDate(sPrevisaoDate) != null) {
                if ((strToDate(sPrevisaoDate) < strToDate(sToday))) {
                    if (window.top.overflyGen.Alert('A data de Previs�o de Entrega n�o pode ser menor que a data do Pedido.') == 0)
                        return null;
                    return true;
                }
            }
        }

        // Se e um novo registro
        if (dsoSup01.recordset[glb_sFldIDName].value == null) {
            // pega o value do contexto atual
            aItemSelected = getCmbCurrDataInControlBar('sup', 1);
            nContextoID = aItemSelected[1];
            // 5111->Pedido de Entrada, 5112 Pedido de Saida
            if (nContextoID == 5111)
                dsoSup01.recordset['TipoPedidoID'].value = 601;
            else if (nContextoID == 5112)
                dsoSup01.recordset['TipoPedidoID'].value = 602;
            // Grava o ID da Empresa logada
            dsoSup01.recordset['EmpresaID'].value = nEmpresaID[0];
            setSelOrigemPedido();
            //dsoSup01.recordset['OrigemPedidoID'].value = 605;


        }

        if ((chkAjustesRejeicaoNFePendentes.checked == false) && (dsoSup01.recordset['AjustesRejeicaoNFePendentes'].value == true) &&
            (dsoSup01.recordset['EstadoID'].value == 26)) {
            var nPedidoID = dsoSup01.recordset['PedidoID'].value;
            var nDepositoID = dsoSup01.recordset['DepositoID'].value;
            var strPars = new String();

            strPars = '?nPedidoID=' + escape(nPedidoID);
            strPars += '&nCurrEstadoID=' + escape(26);
            strPars += '&nNewEstadoID=' + escape(29);
            strPars += '&nUserID=' + escape(glb_nUserID);
            strPars += '&bLimiteMaximo=' + escape(glb_bLimiteMaximo);
            strPars += '&nDepositoID=' + escape(nDepositoID == null ? 0 : nDepositoID);
            strPars += '&nTipoModal=' + escape(6);
            strPars += '&bSimulacao=' + escape(1);

            dsoVerificaGravaLog.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/verificacaopedido.aspx' + strPars;
            dsoVerificaGravaLog.ondatasetcomplete = dsoVerificaGravaLog_DSC;
            dsoVerificaGravaLog.refresh();

            return true;
        }
        if ((chkAjustesRejeicaoNFePendentes.checked == false) && (dsoSup01.recordset['AjustesRejeicaoNFePendentes'].value == true) &&
           ((dsoSup01.recordset['EstadoID'].value == 27) || (dsoSup01.recordset['EstadoID'].value == 28) || (dsoSup01.recordset['EstadoID'].value == 29) ||
            (dsoSup01.recordset['EstadoID'].value == 30) || (dsoSup01.recordset['EstadoID'].value == 31) || (dsoSup01.recordset['EstadoID'].value == 32))) {
            dsoVerificaGravaLog_DSC(true);
            return true;
        }

        if ((((selTransacaoID.value == 111) && (selMoedaID.value != 647)) || ((selTransacaoID.value == 113) && (selMoedaID.value != 541))) && (glb_aEmpresa[1] == 130)) {

            if (window.top.overflyGen.Alert('Moeda do pedido n�o condiz com transa��o escolhida.') == 0)
                return null;

            return true;
        }
    }
    else if (btnClicked == 'SUPEST') {
        // nao altera Estado se O Pedido esta Suspenso
        var bSuspenso = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Suspenso' + '\'' + '].value');

        // sempre que o estado do pedido e diferente de 21
        // o sup nao pode ser alterado
        if (bSuspenso) {
            if (window.top.overflyGen.Alert('Pedido n�o pode ser alterado neste Estado!') == 0)
                return null;
            return true;
        }
    }
    else if (btnClicked == 'SUPINCL') {
        setLabelParceiro();
        setLabelPessoa();
        setLabelShipTo();
        setLabelTransportadora();
        setLabelLocalEntrega();


        // Seta a Data
        dsoCurrData.URL = SYS_ASPURLROOT + '/serversidegenEx/currdate.aspx?currDateFormat=' + escape(DATE_FORMAT);
        dsoCurrData.ondatasetcomplete = dsoCurrDataComplete_DSC;
        dsoCurrData.refresh();
        // para a automacao, para prosseguir na funcao
        // dsoCurrDataComplete_DSC() - retorno da data do servidor
        return true;
    }
    else if (btnClicked == 'SUPALT')
    {

        // So preenche os combos de transporte se o estadoid por menor
        // que Faturamento
        if (dsoSup01.recordset['EstadoID'].value <= 26) {
            fillCmbsTransporte();
            return true;
        }
    }

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {


    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALVIEWXMLHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALVOLANDTRANSPDATAHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            glb_nCurrEstadoIDPedido = param2[2];
            glb_nNewEstadoIDPedido = param2[1];

            var nTipoPedidoID = dsoSup01.recordset['TipoPedidoID'].value;
            var nPedidoEmitiraNFe = dsoSup01.recordset['PedidoEmitiraNFe'].value;

            // Verifica pedido ao faturar NF: Pedido de entrada para E. Pedido de saida para X.            
            if (((nTipoPedidoID == 601) && (glb_nNewEstadoIDPedido == 27)) || ((nTipoPedidoID == 602) && (glb_nNewEstadoIDPedido == 29)))
                glb_TimerPedidoVar = window.setInterval('bef_verifyPedidoInServer()', 10, 'JavaScript');
            else
                glb_TimerPedidoVar = window.setInterval('saveStateIDAfterModal()', 10, 'JavaScript');

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCADASTROHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            glb_nCurrEstadoIDPedido = param2[0];
            glb_nNewEstadoIDPedido = param2[1];
            glb_bLimiteMaximo = param2[2];

            glb_TimerPedidoVar = window.setInterval('bef_verifyPedidoInServer()', 10, 'JavaScript');

            return 0;
        }
            // Desabilitado na modal, esta aqui para compatibilidade
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALPESSOAHTML') {
        if (param1 == 'OK') {
            // Seta o campo EhCliente
            dsoSup01.recordset['EhCliente'].value = param2[2];
            // Preenche o combo de parceiro
            fillComboPessoa(param2);

            glb_PessoaDesativa = param2[7] == 0;

            // Seleciona a transacao Default
            if (!glb_PessoaDesativa) {
                if (trimStr(param2[4]) != '') {
                    fillComboTransacao([param2[4], param2[5]], param2[8]);
                }
                else {
                    clearComboEx(['selTransacaoID']);
                }
            }
            else {
                clearComboEx(['selTransacaoID']);
            }

            // Seta o campo ImpostosIncidencia
            if ((param2[6] == '0') || (param2[6].toUpperCase() == 'FALSE'))
                chkImpostosIncidencia.checked = false;
            else
                chkImpostosIncidencia.checked = true;

            glb_PaisPessoa = param2[9];
            ajustaCampos();

            // Chamadas abaixo movidas para coerencia da interface
            // esta funcao fecha a janela modal e destrava a interface
            //restoreInterfaceFromModal();    
            // escreve na barra de status
            //writeInStatusBar('child', 'cellMode', 'Altera��o');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALTRANSACAOHTML') {
        if (param1 == 'OK') {
            // Preenche o combo de transacao
            fillComboTransacao(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            //Verfica se � toma��o de servi�o.
            if ((param2[3] == 1) || (param2[4] == 1)) {
                txtdtPrevisaoEntrega.style.visibility = 'hidden';
                ajustaCampos_Tomacao();
            }
            else
                ajustaCampos();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
        // Modal de impressao
    else if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            if (param2 == 'BLOQUETO') {
                glb_BloquetoSupTimer = window.setInterval('openModalPrintBloqueto_Timer()', 100, 'JavaScript');
            }
            else {
                // esta funcao fecha a janela modal e destrava a interface
                restoreInterfaceFromModal();
                // escreve na barra de status
                writeInStatusBar('child', 'cellMode', 'Detalhe');
            }

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
        // Modal bloqueto de cobranca
    else if (idElement.toUpperCase() == 'MODALBLOQUETOCOBRANCAHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
        // Modal find pessoa
    else if (idElement.toUpperCase() == 'MODALFINDPESSOAHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            fillCmbShipTo(param2);

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALNFEHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
    }

        // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

        // Modal de Custos
    else if (idElement.toUpperCase() == 'MODALKARDEXHTML') {
        if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALRESULTADOSHTML') {
        if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALAPURACAOIMPOSTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALEMAILHTML') {
        if (param1 == 'OK') {

            // esta funcao que fecha a janela modal e destrava a interface
            // restoreInterfaceFromModal() foi movida para final das funcoes
            // chamadas pela funcao fillComboParceiro;    
            // escreve na barra de status
            //  writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();            
            return 0;
        }
    }
}

/********************************************************************
Funcao criada pelo programador.
Aciona funcao que verifica pedido no servidor

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function bef_verifyPedidoInServer() {
    if (glb_TimerPedidoVar != null) {
        window.clearInterval(glb_TimerPedidoVar);
        glb_TimerPedidoVar = null;
    }

    verifyPedidoInServer(glb_nCurrEstadoIDPedido, glb_nNewEstadoIDPedido, true);
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de volumes e dados de transporte

Parametro:
nTipoModal:
1 - Dados da Nota (Entrada)  - chkBox
2 - Dados de Transporte (Entrada)
3 - Numero de volumes (Saida)
4 - Dados da Nota Fiscal (Saida)
5 - Dados de Transporte (Saida)

Retorno:
nenhum
********************************************************************/
function openModalVolAndDataTransp(tipoModal, currEstadoID, newEstadoID) {
    var htmlPath;
    var strPars = new String();
    var bTomacaoServico = 0;
    var bMatrizFilial = 0;
    var nModalWidth = 365;
    var nModalHeight = 452;
    var ntipoModalEx = 0;
    var nPedidoID = dsoSup01.recordset['PedidoID'].value;
    var nPedidoEmitiraNFe = dsoSup01.recordset['PedidoEmitiraNFe'].value;
    var aEmpresa = getCurrEmpresaData();

    try {
        bTomacaoServico = dsoSup01.recordset['Servico'].value || dsoSup01.recordset['ProdutoServico'].value;
        bMatrizFilial = dsoSup01.recordset['MatrizFilial'].value;
    }
    catch (e) {
        ;
    }

    //if ((bTomacaoServico == false) && (tipoModal == 1) && (aEmpresa[1] == 130) && (dsoSup01.recordset['TransacaoID'].value == 111))
    if ((bTomacaoServico == false) && (tipoModal == 1) && (aEmpresa[1] == 130) && (dsoSup01.recordset['EhCliente'].value == 0) && (bMatrizFilial == false)) {
        nModalWidth += 620;
        nModalHeight += 108;
        ntipoModalEx = 1;
    }

    // mandar os parametros para o servidor
    strPars = '?nPedidoID=' + escape(nPedidoID);
    strPars += '&nTipoModal=' + escape(tipoModal);

    if (ntipoModalEx == 1)
        strPars += '&nTipoModalEx=' + escape(ntipoModalEx);

    strPars += '&nCurrEstadoID=' + escape(currEstadoID);
    strPars += '&nNewEstadoID=' + escape(newEstadoID);
    strPars += '&nPedidoEmitiraNFe=' + escape(nPedidoEmitiraNFe);

    if ((tipoModal == 1) || (tipoModal == 4)) {
        var empresaData = getCurrEmpresaData();
        strPars += '&nEmpresaID=' + escape(empresaData[0]);
    }

    // carregar modal - faz operacao de banco no carregamento    
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalvolandtranspdata.asp' + strPars;

    //showModalWin(htmlPath, new Array(365, 400));
    showModalWin(htmlPath, new Array(nModalWidth, nModalHeight));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de cadastro

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalCadastro(currEstadoID, newEstadoID) {
    var htmlPath;
    var strPars = new String();

    var nPedidoID = dsoSup01.recordset['PedidoID'].value;
    var empresaData = getCurrEmpresaData();

    // mandar os parametros para o servidor
    strPars = '?sCaller=' + escape('S');
    strPars += '&nPedidoID=' + escape(dsoSup01.recordset.Fields['PedidoID'].value);
    strPars += '&nParceiroID=' + escape(dsoSup01.recordset.Fields['ParceiroID'].value);
    strPars += '&nEmpresaID=' + escape(empresaData[0]);
    strPars += '&nEhCliente=' + escape((dsoSup01.recordset.Fields['EhCliente'].value == true ? 1 : 0));
    strPars += '&nCurrEstadoID=' + escape(currEstadoID);
    strPars += '&nNewEstadoID=' + escape(newEstadoID);

    // carregar modal
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalcadastro.asp' + strPars;

    showModalWin(htmlPath, new Array((740 + 30), (486 + 40 - 4)));

    // Forca esta janela modal, excepcionalmente, a alinhar top ao browser filho
    var modalFrame = getFrameInHtmlTop('frameModal');

    if (modalFrame != null)
        modalFrame.style.top = 0;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de parceiro 

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPessoa() {
    var htmlPath;
    var strPars = new String();
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];
    var nContextoID = getCmbCurrDataInControlBar('sup', 1);

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa);
    strPars += '&nContextoID=' + escape(nContextoID[1]);
    strPars += '&sCaller=' + escape('SUP');

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalpessoa.asp' + strPars;
    showModalWin(htmlPath, new Array(671, 284));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de parceiro 

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalTransacao() {
    var htmlPath;
    var strPars = new String();
    var aEmpresa = glb_aEmpresa;
    var nCurrEmpresa = aEmpresa[0];
    var nContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nEhCliente = dsoSup01.recordset['EhCliente'].value;

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa);
    strPars += '&nEmpresaPaisID=' + escape(aEmpresa[1]);
    strPars += '&nContextoID=' + escape(nContextoID[1]);
    strPars += '&nParceiroID=' + escape(selPessoaID.value);
    strPars += '&nTransacaoID=' + escape(selTransacaoID.value);
    strPars += '&nEhCliente=' + escape(nEhCliente);
    strPars += '&clienteDesativo=' + escape(glb_PessoaDesativa || glb_ParceiroDesativo);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modaltransacao.asp' + strPars;
    showModalWin(htmlPath, new Array(514, 284));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint(bOpenInProposta) {
    var aPasta = getCmbCurrDataInControlBar('inf', 1);
    var pastaID = aPasta[1];

    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var nPedidoID = dsoSup01.recordset['PedidoID'].value;

    var nNumeroVolumes;

    if (sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,
        '( (dso01JoinSup.recordset.Fields.Count > 0) && (keepCurrFolder() == 24001) )'))
        nNumeroVolumes = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,
                                       'dso01JoinSup.recordset[' + '\'' + 'NumeroVolumes' + '\'' + '].value');
    else
        nNumeroVolumes = dsoSup01.recordset['NumeroVolumes'].value;

    if (nNumeroVolumes == null)
        nNumeroVolumes = 0;

    // Verifica se tem um numero de serie selecionado no grid de Numero de Serie
    var nNumeroSerieID = 0, nProdutoID = 0;
    if (pastaID == 24005) {
        var nRowSelected = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,
                          'fg.Row');
        if (nRowSelected > 0) {
            nNumeroSerieID = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,
                          'fg.TextMatrix(fg.Row, getColIndexByColKey(fg, ' + '\'' +
                          'NumeroSerieID' + '\'' + '))');
            nNumeroSerieID = parseInt(nNumeroSerieID, 10);
            //INICIO DE NS-------------------------------------------------------            
            if (isNaN(nNumeroSerieID)) {
                nNumeroSerieID = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,
                          'fg.TextMatrix(fg.Row, getColIndexByColKey(fg, ' + '\'' +
                          'PedProdutoID' + '\'' + '))');
                nNumeroSerieID = parseInt(nNumeroSerieID, 10);
            }

            if (isNaN(nNumeroSerieID))
                nNumeroSerieID = 0;
            //FIM DE NS-------------------------------------------------------            
            nProdutoID = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,
                          'fg.TextMatrix(fg.Row, getColIndexByColKey(fg, ' + '\'' +
                          'ProdutoID' + '\'' + '))');
            nProdutoID = parseInt(nProdutoID, 10);
        }
    }

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(glb_nUserID);
    strPars += '&nPedidoID=' + escape(nPedidoID);
    strPars += '&nNumeroVolumes=' + escape(nNumeroVolumes);
    strPars += '&nNumeroSerieID=' + escape(nNumeroSerieID);
    strPars += '&nProdutoID=' + escape(nProdutoID);
    strPars += '&currDateFormat=' + escape(DATE_FORMAT);
    strPars += '&bOpenInProposta=' + escape((bOpenInProposta == true ? 1 : 0));

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(585, 334));

}

// FUNCOES PARTICULARES DE SERVIDOR =================================

/********************************************************************
Funcao do programador
Preenche os combos do grupo Transporte
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function fillCmbsTransporte() {
    if (dsoSup01.recordset.Fields.Count == 0)
        return null;

    var nPedidoID = 0;
    var nParceiroID = 0;
    var nPessoaID = 0;
    var nEmpresaID = 0;

    nPedidoID = dsoSup01.recordset['PedidoID'].value;

    if (selParceiroID.selectedIndex >= 0) {
        lockInterface(true);

        nParceiroID = selParceiroID.value;
        nEmpresaID = getCurrEmpresaData();
        nPessoaID = selPessoaID.value;

        // parametrizacao do dso dsoTransporte01
        setConnection(dsoTransporte01);

        if (nPedidoID == null)
            dsoTransporte01.SQL = 'EXEC sp_CarrinhosCompras_Combos ' + nEmpresaID[0] + ', 0, ' + nPessoaID + ', ' + nParceiroID + ', NULL, 3';
        else
            dsoTransporte01.SQL = 'EXEC sp_CarrinhosCompras_Combos ' + -nPedidoID + ', NULL, NULL, NULL, NULL, 3';

        dsoTransporte01.ondatasetcomplete = dsoTransporte01_DSC;
        dsoTransporte01.Refresh();
    }
}

/********************************************************************
Funcao do programador
Retorno do servidor da funcao fillCmbsTransporte()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoTransporte01_DSC() {
    var sFiltroFrete = '';

    if (chkFrete.checked)
        sFiltroFrete = ' AND FretePago = 1 ';
    else
        sFiltroFrete = ' AND FretePago = 0 ';

    // Se alteracao guarda o ID da transportadora
    var currTranspID = 0;
    if ((lastBtnUsedInCtrlBar('sup') == 'ALT') && (selTransportadoraID.options.length > 0) && (selTransportadoraID.selectedIndex != -1))
        currTranspID = selTransportadoraID.options[selTransportadoraID.selectedIndex].value;

    var oldDataSrc = selTransportadoraID.dataSrc;
    var oldDataFld = selTransportadoraID.dataFld;
    selTransportadoraID.dataSrc = '';
    selTransportadoraID.dataFld = '';
    clearComboEx(['selTransportadoraID']);
    dsoSup01.recordset['TransportadoraID'].value = 0;

    dsoTransporte01.recordset.MoveFirst();
    dsoTransporte01.recordset.setFilter('Indice = 5' + sFiltroFrete);

    while (!dsoTransporte01.recordset.EOF) {
        optionStr = dsoTransporte01.recordset['Campo'].value;
        optionValue = dsoTransporte01.recordset['RegistroID'].value;

        if (lookUpValueInCmb(selTransportadoraID, optionValue) >= 0) {
            dsoTransporte01.recordset.MoveNext();
            continue;
        }

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        oOption.setAttribute('Rodoviario', dsoTransporte01.recordset['Rodoviario'].value, 1);
        oOption.setAttribute('Aereo', dsoTransporte01.recordset['Aereo'].value, 1);
        oOption.setAttribute('Maritimo', dsoTransporte01.recordset['Maritimo'].value, 1);
        oOption.setAttribute('PP', dsoTransporte01.recordset['PP'].value, 1);
        oOption.setAttribute('PT', dsoTransporte01.recordset['PT'].value, 1);
        oOption.setAttribute('PC', dsoTransporte01.recordset['PC'].value, 1);
        oOption.setAttribute('TP', dsoTransporte01.recordset['TP'].value, 1);
        oOption.setAttribute('TT', dsoTransporte01.recordset['TT'].value, 1);
        selTransportadoraID.add(oOption);
        dsoTransporte01.recordset.MoveNext();
    }

    selTransportadoraID.dataSrc = oldDataSrc;
    selTransportadoraID.dataFld = oldDataFld;

    if (selTransportadoraID.options.length > 0) {
        if (selTransportadoraID.options.length == 1) {
            dsoTransporte01.recordset.MoveFirst();
            dsoSup01.recordset['TransportadoraID'].value = dsoTransporte01.recordset['RegistroID'].value;
            selOptByValueInSelect(getHtmlId(), 'selTransportadoraID', dsoTransporte01.recordset['RegistroID'].value);
        }
        else if ((lastBtnUsedInCtrlBar('sup') == 'ALT') && (currTranspID != 0)) {
            if (selOptByValueInSelect(getHtmlId(), 'selTransportadoraID', currTranspID))
                dsoSup01.recordset['TransportadoraID'].value = currTranspID;
        }

        fillCmbsSecondariesTransporte();
    }
    else {
        lockInterface(false);

        // funcao que controla campos read-only
        controlReadOnlyFields(true);

        if (glb_BtnFromFramWork == 'SUPALT')
            editRegister();
    }

    dsoTransporte01.recordset.setFilter('');
}

// FINAL DE FUNCOES PARTICULARES DE SERVIDOR ========================

// FUNCOES PARTICULARES DE INTERFACE ================================

/********************************************************************
Funcao criada pelo programador.
Da refresh no sup

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function refreshSup() {
    if (glb_TimerPedidoVar != null) {
        window.clearInterval(glb_TimerPedidoVar);
        glb_TimerPedidoVar = null;
    }

    __btn_REFR('sup');
}

/********************************************************************
Funcao criada pelo programador.
Da refresh no inf

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function refreshInf() {
    if (glb_TimerPedidoVar != null) {
        window.clearInterval(glb_TimerPedidoVar);
        glb_TimerPedidoVar = null;
    }

    sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,
                          '__btn_REFR(' + '\'' + 'inf' + '\'' + ')');
}

/********************************************************************
Funcao criada pelo programador
Usuario clicou checkbox tem Nota Fiscal.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function chkBoxTemNFClicked() {
    if (chkTemNF.checked == true) {
        lblNotaFiscalID.style.visibility = 'inherit';
        selNotaFiscalID.style.visibility = 'inherit';
        lblDataNF.style.visibility = 'inherit';
        txtDataNF.style.visibility = 'inherit';
    }
    else {
        lblNotaFiscalID.style.visibility = 'hidden';
        selNotaFiscalID.style.visibility = 'hidden';
        lblDataNF.style.visibility = 'hidden';
        txtDataNF.style.visibility = 'hidden';
    }
}

function clearCmbsTransporte() {
    clearComboEx(['selTransportadoraID', 'selModalidadeTransporteID', 'selMeioTransporteID']);

    lockUnlockCmbsTransporte();
}

function lockUnlockCmbsTransporte() {
    var nEstadoID = dsoSup01.recordset['EstadoID'].value;
    var aEmpresa = getCurrEmpresaData();
    var nEmpresaID = aEmpresa[0];

    chkFrete.disabled = true;
    selTransportadoraID.disabled = true;
    selModalidadeTransporteID.disabled = true;
    selMeioTransporteID.disabled = true;

    if (((nEstadoID == null) || (nEstadoID == 21)))
        chkFrete.disabled = false;

    var bA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));
    var bA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));


    if (((!chkFrete.disabled) ||
            ((!chkFrete.checked) && (nEstadoID <= 26)) && (aEmpresa[1] != 167)) ||
	    ((aEmpresa[1] == 167) && ((nEstadoID < 24) || ((nEstadoID <= 26) && (bA1 && bA2))))) {

        if ((selTransportadoraID.options.length > 0) && (dsoSup01.recordset['Vinculado'].value != 2))
            selTransportadoraID.disabled = false;

        if (selModalidadeTransporteID.options.length > 0)
            selModalidadeTransporteID.disabled = false;

        if (selMeioTransporteID.options.length > 0)
            selMeioTransporteID.disabled = false;
    }

    // O mesmo
    if (selTransportadoraID.selectedIndex >= 0) {
        if (selTransportadoraID.options(selTransportadoraID.selectedIndex).innerText.toUpperCase() == 'O MESMO') {
            selModalidadeTransporteID.selectedIndex = -1;
            dsoSup01.recordset['ModalidadeTransporteID'].value = 0;
            selMeioTransporteID.selectedIndex = -1;
            dsoSup01.recordset['MeioTransporteID'].value = 0;   
        }
    }
}

function fillCmbsSecondariesTransporte() {
    clearComboEx(['selModalidadeTransporteID', 'selMeioTransporteID']);

    if ((selTransportadoraID.value != null) && (selTransportadoraID.value != '')) {
        dsoTransporte02_DSC();
    }
    else {
        selModalidadeTransporteID.disabled = true;
        selMeioTransporteID.disabled = true;

        lockInterface(false);

        // funcao que controla campos read-only
        controlReadOnlyFields(true);

        if (glb_BtnFromFramWork == 'SUPALT')
            editRegister();


    }
}

function dsoTransporte02_DSC() {
    var nTransportadoraID = selTransportadoraID.value;
    var nRodoviario = selTransportadoraID.options.item(selTransportadoraID.selectedIndex).getAttribute('Rodoviario', 1);
    var nAereo = selTransportadoraID.options.item(selTransportadoraID.selectedIndex).getAttribute('Aereo', 1);
    var nMaritimo = selTransportadoraID.options.item(selTransportadoraID.selectedIndex).getAttribute('Maritimo', 1);
    var nPP = selTransportadoraID.options.item(selTransportadoraID.selectedIndex).getAttribute('PP', 1);
    var nPT = selTransportadoraID.options.item(selTransportadoraID.selectedIndex).getAttribute('PT', 1);
    var nPC = selTransportadoraID.options.item(selTransportadoraID.selectedIndex).getAttribute('PC', 1);
    var nTP = selTransportadoraID.options.item(selTransportadoraID.selectedIndex).getAttribute('TP', 1);
    var nTT = selTransportadoraID.options.item(selTransportadoraID.selectedIndex).getAttribute('TT', 1);
    var oldDataSrc = selMeioTransporteID.dataSrc;
    var oldDataFld = selMeioTransporteID.dataFld;
    var filtro = '';


    if (!((dsoTransporte01.recordset.BOF) && (dsoTransporte01.recordset.EOF))) {
        // Meio de Transporte
        dsoTransporte01.recordset.setFilter('');

        selMeioTransporteID.dataSrc = '';
        selMeioTransporteID.dataFld = '';

        if (nRodoviario == 0)
            filtro = filtro + ' AND Rodoviario = 0';

        if (nAereo == 0)
            filtro = filtro + ' AND Aereo = 0';

        if (nMaritimo == 0)
            filtro = filtro + ' AND Maritimo = 0';

        dsoTransporte01.recordset.MoveFirst();
        dsoTransporte01.recordset.setFilter('Indice = 7' + filtro);

        while (!dsoTransporte01.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoTransporte01.recordset['Campo'].value;
            oOption.value = dsoTransporte01.recordset['RegistroID'].value;
            selMeioTransporteID.add(oOption);
            dsoTransporte01.recordset.MoveNext();

            if (oOption.value == dsoSup01.recordset['MeioTransporteID'].value)
                oOption.selected = true;

        }

        dsoTransporte01.recordset.setFilter('');

        selMeioTransporteID.dataSrc = oldDataSrc;
        selMeioTransporteID.dataFld = oldDataFld;

        // Modalidade de Transporte
        oldDataSrc = selModalidadeTransporteID.dataSrc;
        oldDataFld = selModalidadeTransporteID.dataFld;

        dsoTransporte01.recordset.MoveFirst();
        filtro = 'Indice = 8';

        if (nPP == 0)
            filtro = filtro + ' AND PP = 0';

        if (nPT == 0)
            filtro = filtro + ' AND PT = 0';

        if (nPC == 0)
            filtro = filtro + ' AND PC = 0';

        if (nTP == 0)
            filtro = filtro + ' AND TP = 0';

        if (nTT == 0)
            filtro = filtro + ' AND TT = 0';

        dsoTransporte01.recordset.setFilter(filtro);


        while (!dsoTransporte01.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoTransporte01.recordset['CampoAbreviado'].value;
            oOption.value = dsoTransporte01.recordset['RegistroID'].value;
            selModalidadeTransporteID.add(oOption);
            if (oOption.value == dsoSup01.recordset['ModalidadeTransporteID'].value)
                oOption.selected = true;

            dsoTransporte01.recordset.MoveNext();

        }

        dsoTransporte01.recordset.setFilter('');

        selModalidadeTransporteID.dataSrc = oldDataSrc;
        selModalidadeTransporteID.dataFld = oldDataFld;

    }

    if ((selMeioTransporteID.options.length > 0) && ((selMeioTransporteID.selectedIndex == -1) ||
            (dsoSup01.recordset['MeioTransporteID'].value == null))) {
        selMeioTransporteID.disabled = false;
        selMeioTransporteID.selectedIndex = 0;
        dsoSup01.recordset['MeioTransporteID'].value = selMeioTransporteID.value;
    }

    if ((selModalidadeTransporteID.options.length > 0) && ((selModalidadeTransporteID.selectedIndex == -1) ||
            (dsoSup01.recordset['ModalidadeTransporteID'].value == null))) {
        selModalidadeTransporteID.disabled = false;
        selModalidadeTransporteID.selectedIndex = 0;
        dsoSup01.recordset['ModalidadeTransporteID'].value = selModalidadeTransporteID.value;
    }

    lockInterface(false);

    // funcao que controla campos read-only
    //controlReadOnlyFields(true);

    if (glb_BtnFromFramWork == 'SUPALT')
        editRegister();
    else
        controlReadOnlyFields(true);

    if (dsoSup01.recordset['Servico'].value == 1) {
        txtdtPrevisaoEntrega.style.visibility = 'hidden';
    }

    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode() {
    // funcao que controla campos read-only
    controlReadOnlyFields(true);

    pedidoReferenciaID('SUPALT');
    margemContribuicao('SUPALT');

    if (!glb_TomacaoServico) {
        // Estes campos tem dd/mm/yyyy hh:mm:ss
        txtdtPrevisaoEntrega.maxLength = 19;
    }

    // Volta para a Automacao
    supInitEditMode_Continue();

    /*
    if ((glb_TomacaoServico) || (dsoSup01.recordset['NCM'].value == 1)) {
        glb_Edit = true;
        preencheContas();
    }
    */
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup(). Carrega combos dinamicos
usando fldID e fldName como campos padroes do dso
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbsEx()
{
    var strPars = new String();
    var empresaID = getCurrEmpresaData();

    glb_CounterCmbsDynamics = 5;
    //glb_CounterCmbsDynamics = 5;

    // Parametros fixos - sempre vao para o servidor
    strPars = '?nFormID=' + escape(window.top.formID);
    strPars += '&nUserID=' + escape(glb_nUserID);
    strPars += '&nEmpresaID=' + escape(empresaID[0]);
    // Parametros diversos - vao conforme o form
    strPars += '&param1=' + escape(dsoSup01.recordset['PessoaID'].value);
    strPars += '&param2=' + escape(dsoSup01.recordset['ParceiroID'].value);
    strPars += '&param3=' + escape(dsoSup01.recordset['TransacaoID'].value);
    strPars += '&param4=' + escape(dsoSup01.recordset['NotaFiscalID'].value);
    strPars += '&param5=' + escape(dsoSup01.recordset['ParceiroID'].value);
    strPars += '&param7=' + escape(dsoSup01.recordset['TransportadoraID'].value);
    strPars += '&param8=' + escape(dsoSup01.recordset['MeioTransporteID'].value);
    strPars += '&param9=' + escape(dsoSup01.recordset['ModalidadeTransporteID'].value);
    strPars += '&param10=' + escape(dsoSup01.recordset['ShipToID'].value);

    dsoDynamicCmbs.URL = SYS_ASPURLROOT + '/serversidegenEx/dynamiccmbsdatasup.aspx' + strPars;
    dsoDynamicCmbs.ondatasetcomplete = dsoCmbDynamicEx_DSC;
    dsoDynamicCmbs.refresh();

    dsoDepositos.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/cmbdepositos.aspx' +
		'?EmpresaID=' + escape(empresaID[0]) +
		'&ParceiroID=' + escape(dsoSup01.recordset['ParceiroID'].value) +
		'&TransacaoID=' + escape(dsoSup01.recordset['TransacaoID'].value);
    dsoDepositos.ondatasetcomplete = dsoCmbDynamicEx_DSC;
    dsoDepositos.refresh();

    dsoLocaisEntrega.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/cmblocaisentrega.aspx' +
		'?PessoaID=' + escape(dsoSup01.recordset['PessoaID'].value);
    dsoLocaisEntrega.ondatasetcomplete = dsoCmbDynamicEx_DSC;
    dsoLocaisEntrega.refresh();

    //dsoMoedas.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/cmbmoedas.aspx' +
	//	'?EmpresaID=' + escape(dsoSup01.recordset['EmpresaID'].value) +
    //    '&TransacaoID=' + escape(dsoSup01.recordset['TransacaoID'].value);
    //dsoMoedas.ondatasetcomplete = dsoCmbDynamicEx_DSC;
    //dsoMoedas.refresh();

    var nPedidoID = dsoSup01.recordset['PedidoID'].value;
    var nTransacaoID = dsoSup01.recordset['TransacaoID'].value;
    var bPedidoEhComissao = dsoSup01.recordset['PedidoEhComissao'].value;
    var bTomacaoServico = ((dsoSup01.recordset['Servico'].value == 1) || (dsoSup01.recordset['NCM'].value == 1));
    var npedidoReferenciaID = dsoSup01.recordset['PedidoReferenciaID'].value;
    var bPedidoReferencia = (parseInt(npedidoReferenciaID) > 0);
    var bPedidoComplemento = (dsoSup01.recordset['PedidoComplemento'].value == true);
    var sSql = '';

    sSql = 'SELECT SPACE(0) AS fldID, SPACE(0) AS fldName ' +
         'UNION ALL ' +
         'SELECT DISTINCT a.ContaID AS fldID, CONVERT(VARCHAR(10), a.ContaID) + \' - \' + a.Conta AS fldNam ' +
         'FROM PlanoContas a WITH (NOLOCK) ' +
             'INNER JOIN Conceitos_Contas b WITH(NOLOCK) ON b.ContaID = a.ContaID ' +
             'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON c.ProdutoID = b.ConceitoID ';

    if (bPedidoReferencia && bPedidoComplemento)
        sSql += 'WHERE (a.EstadoID = 2) AND (c.PedidoID = ' + npedidoReferenciaID + ')';
    // if (nTransacaoID = 185)
    else if (((bTomacaoServico) && (!bPedidoEhComissao)) || (dsoSup01.recordset['NCM'].value == 1)) 
        sSql += 'WHERE (a.EstadoID = 2) AND (c.PedidoID = ' + nPedidoID + ')';
    else
        sSql +=  'WHERE (a.EstadoID = 2) AND (c.PedidoID = ' + nPedidoID + ') AND (b.ComissaoVendas = 1)';
 

    dsoContas.SQL = sSql;
    dsoContas.ondatasetcomplete = dsoCmbDynamicEx_DSC;
    dsoContas.refresh();

    setupDSOFinanciamento('dsoCmbDynamicEx_DSC');
}

/********************************************************************
Funcao disparada pelo programador.
Funcao do ondatasetcomplete disparada pela funcao dsoCmbDynamicEx
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamicEx_DSC() {
    var optionStr, optionValue;
    var aCmbsDynamics = [selPessoaID, selParceiroID, selTransacaoID, selNotaFiscalID, selTransportadoraID,
		selMeioTransporteID, selModalidadeTransporteID, selShipTo, selFinanciamentoID, selDepositoID, selLocalEntregaID, selContaID];
    var aDSOsDynamics = [dsoDynamicCmbs, dsoDynamicCmbs, dsoDynamicCmbs, dsoDynamicCmbs, dsoDynamicCmbs,
		dsoDynamicCmbs, dsoDynamicCmbs, dsoDynamicCmbs, dsoCmbDynamic05, dsoDepositos, dsoLocaisEntrega, dsoContas];
    //var aCmbsDynamics = [selPessoaID, selParceiroID, selTransacaoID, selNotaFiscalID, selTransportadoraID,
	//	selMeioTransporteID, selModalidadeTransporteID, selShipTo, selFinanciamentoID, selDepositoID, selLocalEntregaID, selMoedaID];
    //var aDSOsDynamics = [dsoDynamicCmbs, dsoDynamicCmbs, dsoDynamicCmbs, dsoDynamicCmbs, dsoDynamicCmbs,
	//	dsoDynamicCmbs, dsoDynamicCmbs, dsoDynamicCmbs, dsoCmbDynamic05, dsoDepositos, dsoLocaisEntrega, dsoMoedas];
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var i;
    var lFirstRecord;
    var sFilter;

    // Inicia o carregamento de combos dinamicos
    clearComboEx(['selPessoaID', 'selParceiroID', 'selTransacaoID', 'selNotaFiscalID', 'selTransportadoraID',
        'selMeioTransporteID', 'selModalidadeTransporteID', 'selShipTo', 'selFinanciamentoID', 'selDepositoID', 'selLocalEntregaID', 'selContaID']);
    //clearComboEx(['selPessoaID', 'selParceiroID', 'selTransacaoID', 'selNotaFiscalID', 'selTransportadoraID',
    //    'selMeioTransporteID', 'selModalidadeTransporteID', 'selShipTo', 'selFinanciamentoID', 'selDepositoID', 'selLocalEntregaID', 'selMoedaID']);

    //glb_aStaticCombos = ([['selOrigemPedidoID', '1']]);

    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics == 0) {
        //for (i = 0; i <= 11; i++) {
        for (i = 0; i <= 11; i++) {
            oldDataSrc = aCmbsDynamics[i].dataSrc;
            oldDataFld = aCmbsDynamics[i].dataFld;
            aCmbsDynamics[i].dataSrc = '';
            aCmbsDynamics[i].dataFld = '';
            lFirstRecord = true;

            //dsoDynamicCmbs.recordset.Filter = 'Indice = ' + (i + 1).toString();
            sFilter = 'Indice = ' + (i + 1).toString();

            dsoDynamicCmbs.recordset.setFilter(sFilter);

            while (!aDSOsDynamics[i].recordset.EOF) {
                // se for o combo selFinanciamento
                // montar o combo selNumeroParcelas de acordo com o campo ParcelasMax
                if ((i == 8) && (lFirstRecord)) {
                    fillCmbNumeroParcelas();
                }
                optionStr = aDSOsDynamics[i].recordset['fldName'].value;
                optionValue = aDSOsDynamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[i].add(oOption);

                if ((i == 9) && (aDSOsDynamics[i].recordset['EhDefault'].value == 1))
                    aCmbsDynamics[i].selected = true;

                //fazer aqui o da moedaid
                //if ((i == 11) && (aDSOsDynamics[i].recordset['EhDefault'].value == 1))
                //    aCmbsDynamics[i].selected = true;

                aDSOsDynamics[i].recordset.MoveNext();
                lFirstRecord = false;
            }

            dsoDynamicCmbs.recordset.setFilter('');

            aCmbsDynamics[i].dataSrc = oldDataSrc;
            aCmbsDynamics[i].dataFld = oldDataFld;
        }

        setLabelParceiro(dsoSup01.recordset['ParceiroID'].value);
        setLabelPessoa(dsoSup01.recordset['PessoaID'].value);
        setLabelShipTo(dsoSup01.recordset['ShipToID'].value);
        setLabelTransportadora(dsoSup01.recordset['TransportadoraID'].value);
        setLabelLocalEntrega(dsoSup01.recordset['LocalEntregaID'].value);

        // volta para a automacao
        if (glb_BtnFromFramWork != 'SUPOK')
            finalOfSupCascade(glb_BtnFromFramWork);
    }
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Preenche o combo de Pessoa
Parametro:
aPessoa     Array: item 0->Nome
item 1->ID
item 2->0-Fornecedor
1-Cliente

Retorno:
nenhum
********************************************************************/
function fillComboPessoa(aPessoa) {
    clearComboEx(['selPessoaID', 'selParceiroID', 'selFinanciamentoID', 'selLocalEntregaID']);

    var oldDataSrc = selPessoaID.dataSrc;
    var oldDataFld = selPessoaID.dataFld;
    selPessoaID.dataSrc = '';
    selPessoaID.dataFld = '';
    var oOption = document.createElement("OPTION");
    oOption.text = aPessoa[0];
    oOption.value = aPessoa[1];
    selPessoaID.add(oOption);
    selPessoaID.dataSrc = oldDataSrc;
    selPessoaID.dataFld = oldDataFld;

    if (selPessoaID.options.length != 0) {
        setValueInControlLinked(selPessoaID, dsoSup01);

        setLabelPessoa(dsoSup01.recordset['PessoaID'].value);

        fillComboParceiro(aPessoa);
        fillComboLocalEntrega();
    }
    else {
        restoreInterfaceFromModal();
        // escreve na barra de status
        writeInStatusBar('child', 'cellMode', 'Altera��o');
    }

}

/********************************************************************
Funcao criada pelo programador.
Preenche o combo de parceiro 

Parametro:
aPessoa     Array: item 0->Nome
item 1->ID
item 2->0-Fornecedor
1-Cliente
item 3->0-Usuario
1-Cliente
2-Usuario/Cliente
Retorno:
nenhum
********************************************************************/
function fillComboParceiro(aPessoa) {
    var nValue, sText;

    // Fornecedor ou Cliente Direto
    if ((aPessoa[2] == 0) || ((aPessoa[2] == 1) && (aPessoa[3] == 1))) {
        nValue = aPessoa[1];
        sText = aPessoa[0];
        var oldDataSrc = selParceiroID.dataSrc;
        var oldDataFld = selParceiroID.dataFld;
        selParceiroID.dataSrc = '';
        selParceiroID.dataFld = '';
        var oOption = document.createElement("OPTION");
        oOption.text = sText;
        oOption.value = nValue;
        selParceiroID.add(oOption);
        selParceiroID.dataSrc = oldDataSrc;
        selParceiroID.dataFld = oldDataFld;
        if (selParceiroID.options.length != 0) {
            setValueInControlLinked(selParceiroID, dsoSup01);
            setLabelParceiro(dsoSup01.recordset['ParceiroID'].value);

            // Limpa do campo TransportadoraID do dsoSup01
            dsoSup01.recordset['TransportadoraID'].value = null;

            fillCmbsTransporte();
        }
        setupDSOFinanciamento('financiamento_DSC');
    }
        // Cliente Indireto
    else {
        var nEmpresaID = getCurrEmpresaData();

        strSQL = 'SELECT c.PessoaID, c.Fantasia, dbo.fn_Estado_Pessoa_Parceiro(a.SujeitoID ,a.ObjetoID) as regAtivo ' +
                 'FROM RelacoesPessoas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
                 'WHERE a.SujeitoID= ' + aPessoa[1] + ' AND a.TipoRelacaoID=21 ' +
                 'AND a.ObjetoID=b.SujeitoID AND b.TipoRelacaoID=21 ' +
                 'AND a.ObjetoID<>a.SujeitoID ' +
                 'AND b.ObjetoID=' + nEmpresaID[0] + ' ' +
                 'AND b.ObjetoID<>b.SujeitoID ' +
                 'AND b.SujeitoID=c.PessoaID ' +
                 'UNION ALL ' +
                 'SELECT b.PessoaID, b.Fantasia, dbo.fn_Estado_Pessoa_Parceiro(a.SujeitoID ,a.ObjetoID) as regAtivo ' +
                 'FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                 'WHERE a.SujeitoID= ' + aPessoa[1] + ' AND a.TipoRelacaoID=21 ' +
                 'AND a.ObjetoID= ' + nEmpresaID[0] + ' AND b.PessoaID=a.SujeitoID ' +
                 'ORDER BY Fantasia';

        setConnection(dsoParceiroData);
        dsoParceiroData.SQL = strSQL;
        dsoParceiroData.ondatasetcomplete = Parceiro_DSC;
        dsoParceiroData.Refresh();
    }
}

function Parceiro_DSC() {
    if (!((dsoParceiroData.recordset.BOF) && (dsoParceiroData.recordset.EOF))) {
        var oldDataSrc = selParceiroID.dataSrc;
        var oldDataFld = selParceiroID.dataFld;
        selParceiroID.dataSrc = '';
        selParceiroID.dataFld = '';
        while (!dsoParceiroData.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoParceiroData.recordset['Fantasia'].value;
            oOption.value = dsoParceiroData.recordset['PessoaID'].value;
            selParceiroID.add(oOption);
            dsoParceiroData.recordset.MoveNext();
        }
        dsoSup01.recordset['ParceiroID'].value = 0;
        selParceiroID.selectedIndex = -1;
        selParceiroID.dataSrc = oldDataSrc;
        selParceiroID.dataFld = oldDataFld;
    }

    if (selParceiroID.options.length == 1) {
        selParceiroID.selectedIndex = 0;

        clearComboEx(['selTransacaoID']);
        dsoParceiroData.recordset.setFilter('PessoaID=' + selParceiroID.options[0].value);
        glb_ParceiroDesativo = dsoParceiroData.recordset['regAtivo'] == 0;
        dsoParceiroData.recordset.setFilter('');

        dsoSup01.recordset['ParceiroID'].value = selParceiroID.options[0].value;
        setLabelParceiro(dsoSup01.recordset['ParceiroID'].value);

        // Limpa do campo TransportadoraID do dsoSup01
        dsoSup01.recordset['TransportadoraID'].value = null;

        fillCmbsTransporte();
    }

    if (selParceiroID.selectedIndex >= 0) {
        setupDSOFinanciamento('financiamento_DSC');
    }
    else {
        restoreInterfaceFromModal();
        // escreve na barra de status
        writeInStatusBar('child', 'cellMode', 'Altera��o');

        // destrava a Lupa de transacao
        glb_TimerPedidoVar = window.setInterval('enablebtnFindTransacao()', 50, 'JavaScript');
        return null;
    }
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de transacao 

Parametro:
aTransacao   Array: item 0->TransacaoID
item 1->Transacao

Retorno:
nenhum
********************************************************************/
function fillComboTransacao(aTransacao) {
    clearComboEx(['selTransacaoID']);
    var oldDataSrc = selTransacaoID.dataSrc;
    var oldDataFld = selTransacaoID.dataFld;
    selTransacaoID.dataSrc = '';
    selTransacaoID.dataFld = '';
    var oOption = document.createElement("OPTION");
    oOption.value = aTransacao[0];
    oOption.text = aTransacao[0];
    selTransacaoID.add(oOption);
    selTransacaoID.dataSrc = oldDataSrc;
    selTransacaoID.dataFld = oldDataFld;

    selTransacaoID.title = aTransacao[1];
    lblTransacaoID.title = aTransacao[1];

    //ALIMENTA A VARI�VEL QUE INDICA SE A TRANSA��O PERMITE PRODUTOS DESATIVOS
    glb_ProdutoDesativo = aTransacao[2] == 1;

    if (selTransacaoID.options.length != 0)
        setValueInControlLinked(selTransacaoID, dsoSup01);

    preencheMoeda();

    fillComboDeposito();
}

function fillComboDeposito() {
    var nEmpresaID = getCurrEmpresaData()[0];

    dsoDepositos.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/cmbdepositos.aspx' +
		'?EmpresaID=' + escape(nEmpresaID) +
		'&ParceiroID=' + escape(dsoSup01.recordset['ParceiroID'].value) +
		'&TransacaoID=' + escape(dsoSup01.recordset['TransacaoID'].value);
    dsoDepositos.ondatasetcomplete = dsoDepositos_DSC;
    dsoDepositos.refresh();
}

function dsoDepositos_DSC() {
    var oldDataSrc = selDepositoID.dataSrc;
    var oldDataFld = selDepositoID.dataFld;
    var valueSelected = null;
    selDepositoID.dataSrc = '';
    selDepositoID.dataFld = '';

    clearComboEx([selDepositoID.id]);

    while (!dsoDepositos.recordset.EOF) {
        optionStr = dsoDepositos.recordset['fldName'].value;
        optionValue = dsoDepositos.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selDepositoID.add(oOption);

        if (dsoDepositos.recordset['EhDefault'].value == 1)
            valueSelected = optionValue;

        dsoDepositos.recordset.MoveNext();
    }

    selDepositoID.dataSrc = oldDataSrc;
    selDepositoID.dataFld = oldDataFld;

    if (valueSelected != null)
        setValueInControlLinked(selDepositoID, dsoSup01, valueSelected);

    selDepositoID.disabled = (selDepositoID.options.length == 0);
}

function fillComboLocalEntrega() {
    dsoLocaisEntrega.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/cmblocaisentrega.aspx' +
		'?PessoaID=' + escape(dsoSup01.recordset['PessoaID'].value);
    dsoLocaisEntrega.ondatasetcomplete = dsoLocaisEntrega_DSC;
    dsoLocaisEntrega.refresh();
}

function dsoLocaisEntrega_DSC() {
    var oldDataSrc = selLocalEntregaID.dataSrc;
    var oldDataFld = selLocalEntregaID.dataFld;
    var valueSelected = null;
    selLocalEntregaID.dataSrc = '';
    selLocalEntregaID.dataFld = '';

    clearComboEx(['selLocalEntregaID']);

    while (!dsoLocaisEntrega.recordset.EOF) {
        optionStr = dsoLocaisEntrega.recordset['fldName'].value;
        optionValue = dsoLocaisEntrega.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selLocalEntregaID.add(oOption);

        if (dsoLocaisEntrega.recordset['EhDefault'].value == 1)
            valueSelected = optionValue;

        dsoLocaisEntrega.recordset.MoveNext();
    }

    selLocalEntregaID.dataSrc = oldDataSrc;
    selLocalEntregaID.dataFld = oldDataFld;

    if (valueSelected != null)
        setValueInControlLinked(selLocalEntregaID, dsoSup01, valueSelected);

    selLocalEntregaID.disabled = (selLocalEntregaID.options.length == 0);
    setLabelLocalEntrega(dsoSup01.recordset['LocalEntregaID'].value);
}

/********************************************************************
Funcao criada pelo programador.
Preenche o combo de parcelas

Parametro:
Nenhum

Retorno:
nenhum
********************************************************************/
function fillCmbNumeroParcelas() {
    var oldDataSrc_2;
    var oldDataFld_2;
    var nMaxParcelas = 0;
    var j;

    glb_defFinanPadraoID = dsoCmbDynamic05.recordset['Padrao'].value;
    nMaxParcelas = dsoCmbDynamic05.recordset['MaxParcelas'].value;
    // seta a variavel que contem o PMP maximo
    glb_PMPMaximo = dsoCmbDynamic05.recordset['PMPMaximo'].value;
    clearComboEx([selNumeroParcelas.id]);
    oldDataSrc_2 = selNumeroParcelas.dataSrc;
    oldDataFld_2 = selNumeroParcelas.dataFld;
    selNumeroParcelas.dataSrc = '';
    selNumeroParcelas.dataFld = '';
    for (j = 1; j <= nMaxParcelas; j++) {
        var oOption = document.createElement("OPTION");
        oOption.text = j;
        oOption.value = j;
        selNumeroParcelas.add(oOption);
    }
    selNumeroParcelas.dataSrc = oldDataSrc_2;
    selNumeroParcelas.dataFld = oldDataFld_2;
}

/********************************************************************
Funcao criada pelo programador
configura o dso dsoCmbDynamic05 para ser usado no preenchimento do
combo selFinanciamentoID

Parametro:
fComplete       : Funcao de ondatasetcomplete

Retorno:
nenhum
********************************************************************/
function setupDSOFinanciamento(fComplete) {
    var nEmpresaID = getCurrEmpresaData();
    var nEstadoID = dsoSup01.recordset['EstadoID'].value;

    txtPrazo1.value = '';
    txtIncremento.value = '';
    txtPrazoMedioPagamento.value = '';
    txtVariacao.value = '';
    clearComboEx(['selNumeroParcelas']);

    // parametrizacao do dso dsoCmbDynamic05 (designado para selFinanciamentoID)
    setConnection(dsoCmbDynamic05);

    var strSQLSelect = '';
    var strSQLFrom = '';
    var strSQLWhere = '';
    var strSQL = '';
    var nEhCliente;
    var ParceiroID = (selParceiroID.value == "" ? dsoSup01.recordset['ParceiroID'].value : selParceiroID.value);

    if ((nEstadoID == 21) || (nEstadoID == null)) {
        strSQLSelect = 'SELECT Financiamentos.FinanciamentoID AS fldID, ' +
				'(CONVERT(VARCHAR(50), CONVERT(INT, ROUND(dbo.fn_Financiamento_PMP(Financiamentos.FinanciamentoID,NULL,NULL,NULL),2))) + SPACE(1) + CHAR(40) + Financiamentos.Financiamento + CHAR(41)) AS fldName, ' +
				'Financiamentos.NumeroParcelas AS NumeroParcelas, ' +
				'Financiamentos.Prazo1 AS Prazo1, ' +
				'Financiamentos.Incremento AS Incremento, ' +
				'STR(ROUND(dbo.fn_Financiamento_PMP(Financiamentos.FinanciamentoID,NULL,NULL,NULL),2),5,2) AS PMP, ' +
				'STR(ROUND((TaxasEmpresa.TaxaVenda*(dbo.fn_Financiamento_PMP(Financiamentos.FinanciamentoID,null,null,null)/Empresas.DiasBase)),2),5,2) AS Variacao, ' +
				'ROUND(dbo.fn_Financiamento_PMP(FinancLimite.FinanciamentoID,NULL,NULL,NULL),2) AS PMPMaximo, ' +
				'Empresas.NumeroParcelas AS MaxParcelas, FinancPadrao.FinanciamentoID AS Padrao, RelPessoas.Frete AS Frete ';

        strSQLFrom = 'FROM Pessoas WITH(NOLOCK), Pessoas_Creditos PesCredito WITH(NOLOCK), RelacoesPessoas RelPessoas WITH(NOLOCK), FinanciamentosPadrao FinancPadrao WITH(NOLOCK), FinanciamentosPadrao FinancLimite WITH(NOLOCK), FinanciamentosPadrao Financiamentos WITH(NOLOCK), ' +
				'RelacoesPesRec Empresas WITH(NOLOCK), RelacoesPesRec_Financ TaxasEmpresa WITH(NOLOCK) ';

        nEhCliente = dsoSup01.recordset['EhCliente'].value;
        // Cliente
        if (nEhCliente == true) {
            strSQLWhere = 'WHERE (Pessoas.PessoaID = ' + ParceiroID + ' AND	Pessoas.PessoaID = RelPessoas.SujeitoID AND	Pessoas.PessoaID = PesCredito.PessoaID AND ' +
                'PesCredito.PaisID = ' + nEmpresaID[1] + ' AND RelPessoas.ObjetoID = ' + nEmpresaID[0] + ' AND RelPessoas.TipoRelacaoID = 21 AND ' +
				'FinancPadrao.FinanciamentoID = dbo.fn_RelacoesPessoas_FinanciamentoPadrao(RelPessoas.SujeitoID, RelPessoas.ObjetoID, 1) AND FinancPadrao.EstadoID = 2 AND ' +
				'PesCredito.FinanciamentoLimiteID = FinancLimite.FinanciamentoID AND ' +
				'Financiamentos.EstadoID = 2 AND ' +
				'(dbo.fn_Financiamento_PMP(Financiamentos.FinanciamentoID,NULL,NULL,NULL)) <= (dbo.fn_Financiamento_PMP(FinancLimite.FinanciamentoID,NULL,NULL,NULL)) AND ' +
				'Empresas.SujeitoID = ' + nEmpresaID[0] + ' AND Empresas.ObjetoID = 999 AND Empresas.TipoRelacaoID = 12 AND Empresas.RelacaoID = TaxasEmpresa.RelacaoID AND ' +
				'TaxasEmpresa.MoedaID = ' + dsoSup01.recordset['MoedaID'].value + ') ' +
			'ORDER BY Financiamentos.Ordem ';
        }
        else {
            strSQLWhere = 'WHERE (Pessoas.PessoaID = ' + nEmpresaID[0] + ' AND Pessoas.PessoaID = RelPessoas.SujeitoID AND	Pessoas.PessoaID = PesCredito.PessoaID AND ' +
                'PesCredito.PaisID = ' + nEmpresaID[1] + ' AND RelPessoas.ObjetoID = ' + ParceiroID + ' AND RelPessoas.TipoRelacaoID = 21 AND ' +
				'FinancPadrao.FinanciamentoID = dbo.fn_RelacoesPessoas_FinanciamentoPadrao(RelPessoas.ObjetoID, RelPessoas.SujeitoID, 0) AND FinancPadrao.EstadoID = 2 AND ' +
				'PesCredito.FinanciamentoLimiteID = FinancLimite.FinanciamentoID AND ' +
				'Financiamentos.EstadoID = 2 AND ' +
				'(dbo.fn_Financiamento_PMP(Financiamentos.FinanciamentoID,NULL,NULL,NULL)) <= (dbo.fn_Financiamento_PMP(FinancLimite.FinanciamentoID,NULL,NULL,NULL)) AND ' +
				'Empresas.SujeitoID = ' + nEmpresaID[0] + ' AND Empresas.ObjetoID = 999 AND Empresas.TipoRelacaoID = 12 AND Empresas.RelacaoID = TaxasEmpresa.RelacaoID AND ' +
				'TaxasEmpresa.MoedaID = ' + dsoSup01.recordset['MoedaID'].value + ') ' +
			'ORDER BY Financiamentos.Ordem ';
        }

    }
    else {
        strSQLSelect = 'SELECT Financiamentos.FinanciamentoID AS fldID, ' +
				'(CONVERT(VARCHAR(50), CONVERT(INT, ROUND(dbo.fn_Financiamento_PMP(Financiamentos.FinanciamentoID,NULL,NULL,NULL),2))) + SPACE(1) + CHAR(40) + Financiamentos.Financiamento + CHAR(41)) AS fldName, ' +
				'Pedidos.NumeroParcelas AS NumeroParcelas, Pedidos.Prazo1 AS Prazo1, ' +
				'Pedidos.Incremento AS Incremento, Pedidos.PrazoMedioPagamento AS PMP, ' +
				'Pedidos.Variacao AS Variacao, Pedidos.PrazoMedioPagamento AS PMPMaximo, ' +
				'Empresas.NumeroParcelas AS MaxParcelas, Financiamentos.FinanciamentoID AS Padrao, Pedidos.Frete AS Frete ';

        strSQLFrom = 'FROM Pedidos WITH(NOLOCK), FinanciamentosPadrao Financiamentos WITH(NOLOCK), RelacoesPesRec Empresas WITH(NOLOCK) ';

        strSQLWhere = 'WHERE (Pedidos.PedidoID = ' + dsoSup01.recordset['PedidoID'].value + ' AND ' +
				'Pedidos.FinanciamentoID = Financiamentos.FinanciamentoID AND ' +
				'Empresas.SujeitoID = ' + nEmpresaID[0] + ' AND Empresas.ObjetoID = 999 AND Empresas.TipoRelacaoID = 12) ';
    }

    strSQL = strSQLSelect + strSQLFrom + strSQLWhere;

    dsoCmbDynamic05.SQL = strSQL;
    dsoCmbDynamic05.ondatasetcomplete = eval(fComplete);
    dsoCmbDynamic05.Refresh();
}

/********************************************************************
Funcao criada pelo programador
Retorno da funcao fillComboParceiro que repreenche o combo de 
financiamento.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function financiamento_DSC() {
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var i;
    var lFirstRecord;

    // Inicia o carregamento de combos dinamicos (selPessoaID, selObjetoID )
    clearComboEx(['selFinanciamentoID']);

    selFinanciamentoID.disabled = true;
    oldDataSrc = selFinanciamentoID.dataSrc;
    oldDataFld = selFinanciamentoID.dataFld;
    selFinanciamentoID.dataSrc = '';
    selFinanciamentoID.dataFld = '';
    lFirstRecord = true;
    while (!dsoCmbDynamic05.recordset.EOF) {
        // se for o combo selFinanciamento
        // montar o combo selNumeroParcelas de acordo com o campo ParcelasMax
        if (lFirstRecord)
            fillCmbNumeroParcelas();

        optionStr = dsoCmbDynamic05.recordset['fldName'].value;
        optionValue = dsoCmbDynamic05.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selFinanciamentoID.add(oOption);
        dsoCmbDynamic05.recordset.MoveNext();
        lFirstRecord = false;
    }
    selFinanciamentoID.dataSrc = oldDataSrc;
    selFinanciamentoID.dataFld = oldDataFld;

    setValueInControlLinked(selFinanciamentoID, dsoSup01, glb_defFinanPadraoID);
    if (!((dsoCmbDynamic05.recordset.BOF) && (dsoCmbDynamic05.recordset.EOF))) {
        dsoCmbDynamic05.recordset.MoveFirst();
        dsoCmbDynamic05.recordset.Find('fldID', glb_defFinanPadraoID);
        if (!dsoCmbDynamic05.recordset.EOF)
            setValueInControlLinked(selNumeroParcelas, dsoSup01,
                                    dsoCmbDynamic05.recordset['NumeroParcelas'].value);
        else {
            dsoCmbDynamic05.recordset.MoveFirst();
            setValueInControlLinked(selNumeroParcelas, dsoSup01);
        }

        txtPrazo1.value = dsoCmbDynamic05.recordset['Prazo1'].value;
        txtIncremento.value = dsoCmbDynamic05.recordset['Incremento'].value;
        txtPrazoMedioPagamento.value = dsoCmbDynamic05.recordset['PMP'].value;
        txtVariacao.value = dsoCmbDynamic05.recordset['Variacao'].value;
        chkFrete.checked = dsoCmbDynamic05.recordset['Frete'].value;
    }

    if (glb_bUnlockFromParceiro) {
        glb_bUnlockFromParceiro = false;
        lockInterface(false);
    }
    else {
        restoreInterfaceFromModal();
        // escreve na barra de status
        writeInStatusBar('child', 'cellMode', 'Altera��o');

        // destrava a Lupa de transacao
        glb_TimerPedidoVar = window.setInterval('enablebtnFindTransacao()', 50, 'JavaScript');
    }

    if (selFinanciamentoID.options.length > 0)
        selFinanciamentoID.disabled = false;

    if (glb_bExecFillTransporte) {
        glb_bExecFillTransporte = false;
        fillCmbsTransporte();
    }

    return null;
}

// Funcao que destrava a Lupa de transacao
function enablebtnFindTransacao() {
    if (glb_TimerPedidoVar != null) {
        window.clearInterval(glb_TimerPedidoVar);
        glb_TimerPedidoVar = null;
    }

    lockBtnLupa(btnFindTransacao, false);
    selParceiroID.disabled = false;
}


/********************************************************************
Funcao criada pelo programador
Vai ao servidor obter o ID da relacao e no retorno dispara um carrier.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openRelationByCarrier() {
    lockInterface(true);

    var strPars = new String();
    var empresaID = getCurrEmpresaData();

    strPars = '?nPessoaID=' + escape(getCurrDataInControl('sup', 'selParceiroID'));
    strPars += '&nEmpresaID=' + escape(empresaID[0]);
    strPars += '&nTipo=' + escape(1);
    strPars += '&bEhCliente=' + escape(dsoSup01.recordset['EhCliente'].value);

    dsoCurrRelation.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/currrelation.aspx' + strPars;
    dsoCurrRelation.ondatasetcomplete = openRelationByCarrier_DSC;
    dsoCurrRelation.refresh();
}

/********************************************************************
Funcao criada pelo programador
Retorno do servidor da funcao openRelationByCarrier()

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openRelationByCarrier_DSC() {
    var empresa = getCurrEmpresaData();

    // Manda o id da relacao entre a empresa e o parceiro a detalhar 
    if (dsoCurrRelation.recordset.Fields.Count > 0)
        if (!(dsoCurrRelation.recordset.BOF && dsoCurrRelation.recordset.EOF))
            sendJSCarrier(getHtmlId(), 'SHOWRELPESSOAS',
                          new Array(empresa[0], dsoCurrRelation.recordset.Fields['fldID'].value));

    lockInterface(false);
}

function saveStateIDAfterModal() {
    if (glb_TimerPedidoVar != null) {
        window.clearInterval(glb_TimerPedidoVar);
        glb_TimerPedidoVar = null;
    }

    lockInterface(true);
    glb_btnCtlSup = 'SUPEST';

    // Alterado para fazer verifica��es tamb�m de C para A. BJBN 03/03/2010
    // Se Cotacao->Proposta
    if ((((glb_nNewEstadoIDPedido == 22) && (glb_nCurrEstadoIDPedido == 21)) || ((glb_nNewEstadoIDPedido == 22) && (glb_nCurrEstadoIDPedido == 21))) &&
		 ((dsoSup01.recordset['Resultado'].value == true) && (dsoSup01.recordset['TipoPedidoID'].value == 602)) ||
		 ((parseInt(dsoSup01.recordset['MetodoCustoID'].value, 10) == 372) && (dsoSup01.recordset['TipoPedidoID'].value == 601)))
        glb_bCotacaoToProposta = true;

    dsoSup01.recordset.Fields['EstadoID'].value = glb_nNewEstadoIDPedido;

    saveRegister_1stPart();

    if (dsoSup01.recordset['Servico'].value == 1) {
        txtdtPrevisaoEntrega.style.visibility = 'hidden';
    }
}

/********************************************************************
Funcao criada pelo programador.
Controla a habilitacao dos campos 
Numero de Parcelas, Prazo1,Incremento so podem ser alterados
caso o Financimento seja o Personalizado

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function enableDisableFieldsByFinanc() {
    var nFinanciamentoID = parseInt(selFinanciamentoID.value, 10);

    var nEstadoID = dsoSup01.recordset['EstadoID'].value;

    if ((nFinanciamentoID == 1) && ((nEstadoID == 21) || (nEstadoID == null))) {
        selNumeroParcelas.disabled = false;
        txtPrazo1.readOnly = false;
        txtIncremento.readOnly = false;
    }
    else {
        selNumeroParcelas.disabled = true;
        txtPrazo1.readOnly = true;
        txtIncremento.readOnly = true;
    }
}

/********************************************************************
Funcao criada pelo programador.
Calcula o PMP
caso o Financimento seja o Personalizado

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function calculaPMP() {
    if (this.readOnly)
        return true;

    var nTaxaVenda = null;
    var nNumeroParcelas = 0;
    var nIncremento = parseInt(txtIncremento.value, 10);
    var nPrazo1 = parseInt(txtPrazo1.value, 10);
    var nPrazon = 0;
    var nPMP = 0;
    var nVariacao = 0;

    if (selNumeroParcelas.selectedIndex == -1) {
        if (window.top.overflyGen.Alert('Numero de Parcelas em branco.') == 0)
            return null;
        return null;
    }
    else
        nNumeroParcelas = parseInt(selNumeroParcelas.value, 10);

    if (isNaN(nIncremento)) {
        if (window.top.overflyGen.Alert('Incremento inv�lido.') == 0)
            return null;
        txtPrazoMedioPagamento.value = '';
        txtVariacao.value = '';
        return null;
    }
    if (isNaN(nPrazo1)) {
        if (window.top.overflyGen.Alert('Prazo1 inv�lido.') == 0)
            return null;
        txtPrazoMedioPagamento.value = '';
        txtVariacao.value = '';
        return null;
    }

    if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.Find('fldID', dsoSup01.recordset['MoedaID'].value);
        if (!dsoEstaticCmbs.recordset.EOF) {
            nTaxaVenda = dsoEstaticCmbs.recordset['TaxaVenda'].value;
        }
    }

    nPrazon = (nPrazo1 + (nIncremento * (nNumeroParcelas - 1)));

    nPMP = ((nPrazo1 + nPrazon) / 2);

    nVariacao = ((nPMP * nTaxaVenda) / glb_DiasBase);

    txtPrazoMedioPagamento.value = padNumReturningStr(roundNumber(nPMP, 2), 2);

    txtVariacao.value = padNumReturningStr(roundNumber(nVariacao, 2), 2);
}

/********************************************************************
Verificacoes do Pedido
********************************************************************/
function verifyPedidoInServer(nCurrEstadoID, nNewEstadoID, bSaveState) {
    var nPedidoID = dsoSup01.recordset['PedidoID'].value;
    var nDepositoID = dsoSup01.recordset['DepositoID'].value;
    var strPars = new String();

    strPars = '?nPedidoID=' + escape(nPedidoID);
    strPars += '&nCurrEstadoID=' + escape(nCurrEstadoID);
    strPars += '&nNewEstadoID=' + escape(nNewEstadoID);
    strPars += '&nUserID=' + escape(glb_nUserID);
    strPars += '&bLimiteMaximo=' + escape(glb_bLimiteMaximo);
    strPars += '&nDepositoID=' + escape(nDepositoID == null ? 0 : nDepositoID);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/verificacaopedido.aspx' + strPars;

    if (bSaveState == null)
        dsoVerificacao.ondatasetcomplete = verifyPedidoInServer1_DSC;
    else {
        lockInterface(true);
        dsoVerificacao.ondatasetcomplete = verifyPedidoInServer2_DSC;
    }

    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes do Pedido
********************************************************************/
function verifyPedidoInServer1_DSC() {
    // NULL - Verificacao OK

    var sMensagem = (dsoVerificacao.recordset.Fields['Mensagem'].value == null ? '' : dsoVerificacao.recordset.Fields['Mensagem'].value);
    var nResultado = (dsoVerificacao.recordset.Fields['Resultado'].value == null ? 0 : dsoVerificacao.recordset.Fields['Resultado'].value);

    if (nResultado == 0) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }
        
        // Executa estorno de antecipa��o de pedido
        if (glb_bEstornaAntecipacao)
            estornoAntecipacao();
        // sen�o prossegue com mudan�a de estado
        else
            stateMachSupExec('OK');

    }
    else if (nResultado == 1) {
        var _retConf = window.top.overflyGen.Confirm(sMensagem);

        if (_retConf == 0)
            return null;
        else if (_retConf == 1)
        {
            // Executa estorno de antecipa��o de pedido
            if (glb_bEstornaAntecipacao)
                estornoAntecipacao();
            // sen�o prossegue com mudan�a de estado
            else
                stateMachSupExec('OK');
        }
        else
            stateMachSupExec('CANC');
    }

    else if (nResultado == 2) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }
        stateMachSupExec('CANC');
    }
}

/********************************************************************
Retorno do servidor - Verificacoes do Pedido - versao 2
********************************************************************/
function verifyPedidoInServer2_DSC() {
    lockInterface(false);

    var sMensagem = (dsoVerificacao.recordset.Fields['Mensagem'].value == null ? '' : dsoVerificacao.recordset.Fields['Mensagem'].value);
    var nResultado = (dsoVerificacao.recordset.Fields['Resultado'].value == null ? 0 : dsoVerificacao.recordset.Fields['Resultado'].value);

    if (nResultado == 0) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }
        saveStateIDAfterModal();
    } else if (nResultado == 1) {
        var _retConf = window.top.overflyGen.Confirm(sMensagem);

        if (_retConf == 0)
            return null;
        else if (_retConf == 1)
            saveStateIDAfterModal();
        else {
            doNotSaveNewStateSup();
            return null;
        }
    }

    else if (nResultado == 2) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }
        doNotSaveNewStateSup();
        return null;
    }
}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Read Only

Parametro:
bAlteracao - true se Alteracao
- false se Inclusao

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields(bAlteracao) {
    var nE1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E1' + '\'' + ')'));

    var nE2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E2' + '\'' + ')'));

    // pega o value do contexto atual
    var aItemSelected = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = aItemSelected[1];

    var bReadOnly;

    //DireitoEspecifico
    //Pedidos->Entradas/Sa�das->SUP
    //24000 SFS-Grupo Pedido-> E1&&E2 -> Libera edi��o do combo 'selOrigemPedidoID'
    // if ((nE1 == 1) && (nE2 == 1) && (!(bAlteracao)))
    //   bReadOnly = false;
    //else
    bReadOnly = true;

    txtPercentualSUP.readOnly = true;
    txtTaxaMoeda.readOnly = true;

    PerfilUsuario();
    PerfiVendedor();
    GeraParcelaGare();

    chkAjustesRejeicaoNFePendentes.disabled = (!chkAjustesRejeicaoNFePendentes.checked);

    chkPedidoMae.disabled = true;
    chkAtualizaPrecos.disabled = true;
    chkImpostosEsquema.disabled = true;
    chkImpostosIncidencia.disabled = true;
    selNotaFiscalID.disabled = true;


    txtDataNF.readOnly = true;

    selPessoaID.disabled = true;
    selTransacaoID.disabled = true;
    selShipTo.disabled = true;
    lockBtnLupa(btnFindShipTo, true);

    // Liberado somente para:
    // Jose Gracia / Eduardo / Tatiana / Genoma / Helio / Ana Souza /  Trans S�o Geraldo /Camilo
    if ((glb_nUserID == 1000) ||
		(glb_nUserID == 1004) ||
		(glb_nUserID == 1103) ||
		(glb_nUserID == 1059) ||
		(glb_nUserID == 1165) ||
		(glb_nUserID == 1545) ||
		(glb_nUserID == 64006) ||
		(glb_nUserID == 1069)) {
        selShipTo.disabled = (selShipTo.length < 2);
        lockBtnLupa(btnFindShipTo, false);
    }

    txtPrazoMedioPagamento.readOnly = true;
    txtVariacao.readOnly = true;
    txtValorFrete.readOnly = true;
    txtValorSeguro.readOnly = true;
    txtValorTotalServicos.readOnly = true;
    txtValorTotalProdutos.readOnly = true;
    txtValorTotalImpostos.readOnly = true;
    txtValorTotalPedido.readOnly = true;
    txtMargemContribuicao.readOnly = true;
    txtPedidoReferenciaID.readOnly = true;
    txtNumeroItens.readOnly = true;
    // Campos que dependendo do estado do Pedido sao travados
    txtdtPedido.readOnly = false;
    lockBtnLupa(btnFindPessoa, false);
    selParceiroID.disabled = false;
    lockBtnLupa(btnFindTransacao, true);
    chkTemNF.disabled = false;
    selMoedaID.disabled = false;
    chkArredondaPreco.disabled = false;

    /*if ( selPessoaID.options.length > 0 )
    selLocalEntregaID.disabled = false;
    else
    selLocalEntregaID.disabled = true;*/

    if (selFinanciamentoID.options.length > 0)
        selFinanciamentoID.disabled = false;
    else
        selFinanciamentoID.disabled = true;

    selNumeroParcelas.disabled = false;

    txtPrazo1.readOnly = false;
    txtIncremento.readOnly = false;
    if (!glb_TomacaoServico) {
        txtdtPrevisaoEntrega.readOnly = false;
    }

    if (selMeioTransporteID.options.length > 0)
        selMeioTransporteID.disabled = false;

    if (selModalidadeTransporteID.options.length > 0)
        selModalidadeTransporteID.disabled = false;

    selDepositoID.disabled = (selDepositoID.options.length <= 0);

    chkFrete.disabled = false;

    var bEhRetorno = dsoSup01.recordset['EhRetorno'].value;

    if ((nContextoID == 5111) || (bEhRetorno == true)) {
        txtValorOutrasDespesas.readOnly = false;
    }
    else {
        txtValorOutrasDespesas.readOnly = true;
    }

    /*if ((nContextoID == 5111) && (selTransacaoID.value == '111')) {
    txtTotalDesconto.readOnly = false;
    if (getCloneFromOriginal('txtTotalDesconto') != null)
    getCloneFromOriginal('txtTotalDesconto').readOnly = false;
    }
    else {*/
    txtTotalDesconto.readOnly = true;
    /*}*/

    /*if (getCloneFromOriginal('chkPedidoComplemento') != null)
        getCloneFromOriginal('chkPedidoComplemento').readOnly = true; atencao */

    var nEstadoID = dsoSup01.recordset['EstadoID'].value;
    var aEmpresa = getCurrEmpresaData();
    var nEmpresaID = aEmpresa[0];

    if ((((nEstadoID == null) || (nEstadoID == 21)) && (nEmpresaID >= 7 && nEmpresaID <= 9)) ||
        ((selTransacaoID.value == '741') || (selTransacaoID.value == '711') || (selTransacaoID.value == '992'))) {
        txtValorFrete.readOnly = false;
    }

    //if ((glb_TomacaoServico) && ((dsoSup01.recordset['TransacaoID'].value == 185) || (nEstadoID > 26)))
    if ((glb_TomacaoServico) && (nEstadoID > 26))
        //Desabilita combo de conta quanto o estado foi maior que faturamento ou quando a transacao for de Comiss�o. BJBN
        selContaID.disabled = true;

    // N�o permitir edi��o do n�mero de projeto no contexto de sa�da.
    if ((glb_aEmpresa[1] == 130) && ((nContextoID == 5112)))
        txtNumeroProjeto.disabled = true;

    if (bAlteracao) {
        if (!((nEstadoID == null) || (nEstadoID == 21))) {
            txtdtPedido.readOnly = true;
            lockBtnLupa(btnFindPessoa, true);
            selParceiroID.disabled = true;
            lockBtnLupa(btnFindTransacao, true);
            chkTemNF.disabled = true;
            selMoedaID.disabled = true;
            chkImpostosIncidencia.disabled = true;
            chkArredondaPreco.disabled = true;

            selFinanciamentoID.disabled = true;
            selNumeroParcelas.disabled = true;

            txtPrazo1.readOnly = true;
            txtIncremento.readOnly = true;

            if (nEstadoID > 21)
                selLocalEntregaID.disabled = true;

            if (nEstadoID > 22)
                selDepositoID.disabled = true;

            var bA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));
            var bA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));

            if ((nEstadoID >= 24) && (aEmpresa[1] == 167) && !(bA1 && bA2)) {
                txtObservacao.disabled = true;
                txtSeuPedido.disabled = true;
                chkAtrasoExterno.disabled = true;
                txtdtPrevisaoEntrega.disabled = true;
            }

            if (nEstadoID > 26) {
                if (!glb_TomacaoServico) {
                    txtdtPrevisaoEntrega.readOnly = true;
                }

                lockUnlockCmbsTransporte();

                // Pedidos de saida, estado maior q faturamento
                if ((nContextoID == 5112) && (nEstadoID > 26)) {
                    selShipTo.disabled = true;
                    lockBtnLupa(btnFindShipTo, selShipTo.disabled);
                }

                // Pedido de Entrada
                if ((nContextoID == 5111) && (nEstadoID >= 29)) {
                    txtValorOutrasDespesas.readOnly = true;
                }
            }
            else {
                // Campos de Transporte
                lockUnlockCmbsTransporte();

                // habilita e desabilita os campos Numero de Parcelas,Prazo1,Incremento
                enableDisableFieldsByFinanc();

            }

            /*if ((nContextoID == 5111) && (nEstadoID == 1)) {
            txtTotalDesconto.readOnly = false;
            if (getCloneFromOriginal('txtTotalDesconto') != null)
            getCloneFromOriginal('txtTotalDesconto').readOnly = false;
            }
            else {*/
            txtTotalDesconto.readOnly = true;
            /*}  */
            chkPedidoComplemento.readOnly = true;

        }
        else {
            lockBtnLupa(btnFindTransacao, false);
            chkImpostosIncidencia.disabled = false;

            // Campos de Transporte
            lockUnlockCmbsTransporte();

            // habilita e desabilita os campos Numero de Parcelas,Prazo1,Incremento
            enableDisableFieldsByFinanc();
        }
    }
    else {
        chkImpostosIncidencia.disabled = false;

        // Campos de Transporte
        lockUnlockCmbsTransporte();

        // habilita e desabilita os campos Numero de Parcelas,Prazo1,Incremento
        enableDisableFieldsByFinanc();

    }
}

function PerfilUsuario() {
    setConnection(dsoPerfilUsuario);

    dsoPerfilUsuario.SQL = 'SELECT COUNT(DISTINCT a.PerfilID) AS PerfilID ' +
                                'FROM RelacoesPesRec_Perfis a WITH(NOLOCK)' +
                                    'INNER JOIN RelacoesPesRec b ON (a.RelacaoID = b.RelacaoID) ' +
                                'WHERE (b.SujeitoID = ' + glb_nUserID + ' AND a.PerfilID = 500) ';

    dsoPerfilUsuario.ondatasetcomplete = perfilUsuario_DSC;
    dsoPerfilUsuario.Refresh();
}

function perfilUsuario_DSC() {
    glb_PerfilID = dsoPerfilUsuario.recordset['PerfilID'].value;

    direitoReserva();
}

function direitoReserva() {
    var empresa = getCurrEmpresaData();

    setConnection(dsoDireitoReserva);

    dsoDireitoReserva.SQL = 'SELECT COUNT(*) AS DireitoReserva, ' +
                            '(SELECT COUNT(*) ' +
                                'FROM RelacoesPesRec_Perfis aa WITH(NOLOCK) ' +
                                'WHERE (aa.RelacaoID = ' + glb_nUserID + ') AND (aa.PerfilID = 658) AND ' +
                                '(aa.Nivel = 3) AND (aa.EmpresaID = ' + empresa[0] + ')) AS DireitoReserva2, ' +
                            '(SELECT COUNT(*) ' +
                                'FROM RelacoesPesRec_Perfis aaa WITH(NOLOCK) ' +
                                'WHERE (aaa.RelacaoID = ' + glb_nUserID + ') AND (aaa.PerfilID = 664) AND ' +
                                '(aaa.Nivel = 3) AND (aaa.EmpresaID = ' + empresa[0] + ')) AS DireitoOrigemPedido ' +
	                    'FROM RelacoesPessoas a WITH(NOLOCK) ' +
	                    'WHERE ((a.SujeitoID = ' + glb_nUserID + ') AND (a.TipoRelacaoID = 31) AND ' +
			                   '((CONVERT(INT, dbo.fn_Funcionario_CargoID(a.SujeitoID, NULL)) IN (611, 616, 619, 620, 631, 632, 643, 684, 685, 686)))) ';

    dsoDireitoReserva.ondatasetcomplete = direitoReserva_DSC;
    dsoDireitoReserva.Refresh();
}

function direitoReserva_DSC() {
    var nDireitoReserva = dsoDireitoReserva.recordset['DireitoReserva'].value;
    var nDireitoReserva2 = dsoDireitoReserva.recordset['DireitoReserva2'].value;

    if ((nDireitoReserva == 0) && (nDireitoReserva2 == 0) && (glb_PerfilID != 1))
        chkPermiteReserva.disabled = true;
    else
        chkPermiteReserva.disabled = false;

    if (dsoSup01.recordset['EstadoID'].value == 21) {
        var nDireitoOrigemPedido = dsoDireitoReserva.recordset['DireitoOrigemPedido'].value;

        if ((nDireitoOrigemPedido == 1) || (glb_PerfilID == 1))
            selOrigemPedidoID.disabled = false;
        else
            selOrigemPedidoID.disabled = true;
    }
    else
        selOrigemPedidoID.disabled = true;
}

//Inserido para checkbox do gera parcela Gare SSO.26/04/2010
function GeraParcelaGare() {
    var nPessoaID = dsoSup01.recordset['PessoaID'].value;


    if ((nPessoaID == '') || (nPessoaID == null))
        return null;

    setConnection(dsoEhClienteMagazine);

    /* Corre��o. Pedido exemplo: 3428567. FRG - 02/03/2018. */
    dsoEhClienteMagazine.SQL = 'SELECT  COUNT(*) AS EhClienteMagazine ' +
				                'FROM    dbo.Pessoas WITH ( NOLOCK ) ' +
				                'WHERE   ClassificacaoID = 62 ' +
            			                'AND PessoaID = ' + nPessoaID + 
                                        'AND Observacoes LIKE \'%<Magazine%\'';


    /*Id do Marcio Caggiano DURO : Quando o cargo de varejo forem criados substituir por c�digo comentado
    */

    dsoEhClienteMagazine.ondatasetcomplete = GeraParcelaGare_DSC;
    dsoEhClienteMagazine.Refresh();
}

function GeraParcelaGare_DSC() {

    var empresa = getCurrEmpresaData();

    /* 
    SSO.:30/04/2010
    Tempor�rio: O ID 1236 (Marcio Caggiano) est� duro nesta fun��o, pois os cargos de varejo n�o existem e ser�o criados 
    pelo projeto de atualiza��o da fopag. Quando forem criados, trocar o esquema do getCurrUserID() pelo bEhCargoVarejo
    */


    /*  
    setConnection(dsoEhCargoVarejo);

    dsoEhCargoVarejo.SQL = 'SELECT  COUNT (*) AS EhCargoVarejo' +
    ' FROM    dbo.RelacoesPesRec a WITH ( NOLOCK )' +
    ' INNER JOIN dbo.RelacoesPesRec_Perfis b WITH ( NOLOCK ) ON ( a.RelacaoID = b.RelacaoID )' +
    ' WHERE   a.TipoRelacaoID = 11 ' +
    ' AND b.PerfilID IN (658)' +
    ' AND a.SujeitoID =' + userID +
    ' AND b.EmpresaID =' + empresa;

    var bEhCargoVarejo = dsoEhCargoVarejo('EhCargoVarejo'].value;
    */

    var bEhClienteMagazine = dsoEhClienteMagazine.recordset['EhClienteMagazine'].value;

    /*if ((bEhClienteMagazine == 1) && (bEhCargoVarejo == 1))*/
    if ((bEhClienteMagazine == 1) && ((glb_nUserID == 133520) || (glb_nUserID == 1336) ||
                                        (glb_nUserID == 44065) || (glb_nUserID == 2388)))
        chkGeraParcelaGare.disabled = false;
    else
        chkGeraParcelaGare.disabled = true;
}

function dsoVerificaGravaLog_DSC(bOrigem) {
    if (bOrigem == true) {
        saveRegister_1stPart();
        glb_TimerPedidoVar = window.setInterval('refreshSup()', 10, 'JavaScript');
    }
    else {
        var sMensagem = (dsoVerificaGravaLog.recordset.Fields['Mensagem'].value == null ? '' : dsoVerificaGravaLog.recordset.Fields['Mensagem'].value);
        var nResultado = (dsoVerificaGravaLog.recordset.Fields['Resultado'].value == null ? 0 : dsoVerificaGravaLog.recordset.Fields['Resultado'].value);

        if ((nResultado == 0) || (nResultado == 1)) {
            saveRegister_1stPart();
            glb_TimerPedidoVar = window.setInterval('refreshSup()', 10, 'JavaScript');

        }
        else {
            if (sMensagem != '') {
                if (window.top.overflyGen.Alert(sMensagem) == 0)
                    return null;
            }

            lockInterface(false);
        }
    }

    if (dsoSup01.recordset['TomacaoServico'].value == 1) {
        txtdtPrevisaoEntrega.style.visibility = 'hidden';
    }
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de parceiro 

Parametro:
aParceiro   Array: item 0->Nome
item 1->ID

Retorno:
nenhum
********************************************************************/
function dsoCurrDataComplete_DSC() {
    // Prossegue a automacao interrompida no botao de inclusao
    lockAndOrSvrSup_SYS();
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblPessoaID, de acordo com o ID da Pessoa selecionada

Parametro:
nPessoaID   ID da Pessoa

Retorno:
nenhum
********************************************************************/
function setLabelPessoa(nPessoaID) {
    if (nPessoaID == null)
        nPessoaID = '';

    var sPhoneNumber = getPhoneByID(nPessoaID);

    var sLabelPessoa = 'Pessoa ' + nPessoaID;
    lblPessoaID.style.width = sLabelPessoa.length * FONT_WIDTH;
    lblPessoaID.innerText = sLabelPessoa;
    lblPessoaID.title = sPhoneNumber;
    selPessoaID.title = sPhoneNumber;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblPessoaID, de acordo com o ID da Pessoa selecionada

Parametro:
nPessoaID   ID da Pessoa

Retorno:
nenhum
********************************************************************/
function setLabelShipTo(nID) {
    if ((nID == null) || (nID == '')) {
        if (selShipTo.selectedIndex > 0)
            nID = selShipTo.value;
        else
            nID = '';
    }

    var sLabelPessoa = 'Ship to ' + nID;
    lblShipTo.style.width = sLabelPessoa.length * FONT_WIDTH;
    lblShipTo.innerText = sLabelPessoa;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o title do controle lblTransportadoraID, de acordo com o ID da Transportadora selecionada

Parametro:
nTransportadoraID   ID da Transportadora

Retorno:
nenhum
********************************************************************/
function setLabelTransportadora(nTransportadoraID) {
    if (nTransportadoraID == null)
        nTransportadoraID = '';

    var sPhoneNumber = getPhoneByID(nTransportadoraID);

    lblTransportadoraID.title = sPhoneNumber;
    selTransportadoraID.title = sPhoneNumber;
}


/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblParceiroID, de acordo com o ID do Parceiro selecionado

Parametro:
nParceiroID   ID do Parceiro

Retorno:
nenhum
********************************************************************/
function setLabelParceiro(nParceiroID) {
    if (nParceiroID == null)
        nParceiroID = '';

    var sPhoneNumber = getPhoneByID(nParceiroID);

    var sLabelParceiro = 'Parceiro ' + nParceiroID;
    lblParceiroID.style.width = sLabelParceiro.length * FONT_WIDTH;
    lblParceiroID.innerText = sLabelParceiro;
    lblParceiroID.title = sPhoneNumber;
    selParceiroID.title = sPhoneNumber;
}

function setLabelFields() {
    setLabelParceiro(dsoSup01.recordset['ParceiroID'].value);
    setLabelPessoa(dsoSup01.recordset['PessoaID'].value);
    setLabelShipTo(dsoSup01.recordset['ShipToID'].value);
    setLabelTransportadora(dsoSup01.recordset['TransportadoraID'].value);
    setLabelLocalEntrega(dsoSup01.recordset['LocalEntregaID'].value);
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle selLocalEntregaID, de acordo com o ID da Local de entrega selecionada

Parametro:
nLocalEntregaID   ID da Pessoa

Retorno:
nenhum
********************************************************************/
function setLabelLocalEntrega(nLocalEntregaID) {
    if (nLocalEntregaID == null)
        nLocalEntregaID = '';

    //var sPhoneNumber = getPhoneByID(nPessoaID);

    var sLabelLocalEntrega = 'Local de entrega ' + nLocalEntregaID;
    lblLocalEntregaID.style.width = sLabelLocalEntrega.length * FONT_WIDTH;
    lblLocalEntregaID.innerText = sLabelLocalEntrega;
    //lblLocalEntregaID.title = sPhoneNumber;
    //selPessoaID.title = sPhoneNumber;
}

function getPhoneByID(nPessoaID) {
    var retVal = '';

    if ((nPessoaID == null) || (nPessoaID == ''))
        return retVal;
    if (dsoDynamicCmbs.recordset.Fields.Count == 0)
        return retVal;
    else if (dsoDynamicCmbs.recordset.BOF && dsoDynamicCmbs.recordset.EOF)
        return retVal;
    else {
        dsoDynamicCmbs.recordset.MoveFirst();
        while (!dsoDynamicCmbs.recordset.EOF) {
            if (dsoDynamicCmbs.recordset['fldID'].value == nPessoaID) {
                retVal += 'Telefone: ' + dsoDynamicCmbs.recordset['Telefone'].value;
                break;
            }

            dsoDynamicCmbs.recordset.MoveNext();
        }
    }

    return retVal;
}

/********************************************************************
Funcao criada pelo programador
Deixa o campo txtPedidoReferenciaID invis�vel se Pedido de Entrada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function pedidoReferenciaID(btnClicked) {
    /*if (dsoSup01.recordset['TipoPedidoID'].value == 601) {
    lblPedidoReferenciaID.style.visibility = 'inherit';
    chkPedidoComplemento.readOnly = true;

    if ((btnClicked == 'SUPINCL') || (btnClicked == 'SUPALT')) {
    if (getCloneFromOriginal('txtPedidoReferenciaID') != null)
    {
    getCloneFromOriginal('txtPedidoReferenciaID').style.visibility = 'inherit';
    chkPedidoComplemento.style.visibility = 'inherit';
    }
    }
    else
    {
    txtPedidoReferenciaID.style.visibility = 'inherit';
    chkPedidoComplemento.style.visibility = 'inherit';
    }
    }
    else {
    lblPedidoReferenciaID.style.visibility = 'hidden';
    lblPedidoComplemento.style.visibility = 'hidden';
    chkPedidoComplemento.style.visibility = 'hidden';
    txtPedidoReferenciaID.style.visibility = 'hidden';
    if (getCloneFromOriginal('txtPedidoReferenciaID') != null)
    getCloneFromOriginal('txtPedidoReferenciaID').style.visibility = 'hidden';
    }*/


    if (dsoSup01.recordset['PedidoReferenciaID'].value != null) {
        lblPedidoReferenciaID.style.visibility = 'inherit';
        lblPedidoComplemento.style.visibility = 'inherit';

        if ((btnClicked == 'SUPINCL') || (btnClicked == 'SUPALT')) {
            txtPedidoReferenciaID.style.visibility = 'inherit';
            chkPedidoComplemento.style.visibility = 'inherit';
        }
        else {
            txtPedidoReferenciaID.style.visibility = 'inherit';
            chkPedidoComplemento.style.visibility = 'inherit';
        }
    }
    else {
        lblPedidoReferenciaID.style.visibility = 'hidden';
        lblPedidoComplemento.style.visibility = 'hidden';
        chkPedidoComplemento.style.visibility = 'hidden';
        txtPedidoReferenciaID.style.visibility = 'hidden';
    }
}
/********************************************************************
Funcao criada pelo programador.
Mostra/Esconde o campo MargemContribuicao dependendo se o Pedido gera resultado

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function margemContribuicao(btnClicked) {

    var empresa = getCurrEmpresaData();

    if (dsoSup01.recordset.Fields.Count == 0)
        return null;

    if (dsoSup01.recordset.BOF || dsoSup01.recordset.EOF)
        return null;

    if (dsoSup01.recordset['Resultado'].value == true) {

        // Vendedores n�o podem visualizar a MC.
        if (!glb_VisualizaMargem)
        {
            lblMargemContribuicao.style.visibility = 'hidden';
            txtMargemContribuicao.style.visibility = 'hidden';
        }
        else
        {
            lblMargemContribuicao.style.visibility = 'inherit';
            txtMargemContribuicao.style.visibility = 'inherit';
        }
        //if ((btnClicked == 'SUPINCL') || (btnClicked == 'SUPALT')) 
        //  txtMargemContribuicao.style.visibility = 'inherit';
    }
    else {
        lblMargemContribuicao.style.visibility = 'hidden';
        txtMargemContribuicao.style.visibility = 'hidden';
    }
}
function changeTransacaoCmbTitle() {
    if (dsoSup01.recordset.Fields.Count != 0) {
        if (!(dsoSup01.recordset.BOF && dsoSup01.recordset.EOF)) {
            lblTransacaoID.title = dsoSup01.recordset['Transacao'].value;
            selTransacaoID.title = dsoSup01.recordset['Transacao'].value;
        }
    }
}

function unlockPedidoID() {
    // pega o value do contexto atual
    var aItemSelected = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = aItemSelected[1];

    var elem = txtRegistroID;

    //rrf nota fiscal complementar aqui
    /*if (nContextoID != 5111)
    return true;

    */
    // Marco 07/12/2004
    //txtRegistroID.disabled = false;
    txtRegistroID.readOnly = false;

    if (elem != null) {
        elem.disabled = false;

        elem.setAttribute('thePrecision', 10, 1);
        elem.setAttribute('theScale', 0, 1);
        elem.setAttribute('verifyNumPaste', 1);
        elem.onkeypress = verifyNumericEnterNotLinked;
        elem.setAttribute('minMax', new Array(0, 9999999999), 1);
        elem.onfocus = selFieldContent;
        elem.maxLenght = 10;
    }

    return true;
}

function dsoClonaPedido_DSC() {
    lockInterface(false);
    while (!dsoClonaPedido.recordset.EOF) {
        if (dsoClonaPedido.recordset.Fields.Count == 0) {
            if (window.top.overflyGen.Alert('Pedido n�o foi gerado.') == 0)
                return null;
            return null;
        }

        var msgAlert = dsoClonaPedido.recordset.Fields["Resultado"].value;

        if (dsoClonaPedido.recordset.Fields["PedidoGeradoID"].value == 0) {
            if (msgAlert == '')
                return null;

            if (window.top.overflyGen.Alert(msgAlert) == 0)
                return null;
            return null;
        }
        else {
            if (msgAlert != '') {
                if (window.top.overflyGen.Alert(msgAlert) == 0)
                    return null;
            }

            glb_TimerPedidoVar = window.setInterval('showPedidoByID(' + dsoClonaPedido.recordset.Fields["PedidoGeradoID"].value + ')', 10, 'JavaScript');
        }
        dsoClonaPedido.recordset.MoveNext();
    }
}

/********************************************************************
Criada pelo programador
Na inclusao, esta funcao abre o detalhe de um pedido
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function showPedidoByID(pedidoID) {
    if (glb_TimerPedidoVar != null) {
        window.clearInterval(glb_TimerPedidoVar);
        glb_TimerPedidoVar = null;
    }

    sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL',
      [pedidoID, null, null]);
}

function openModalPrintBloqueto_Timer() {
    if (glb_BloquetoSupTimer != null) {
        window.clearInterval(glb_BloquetoSupTimer);
        glb_BloquetoSupTimer = null;
    }

    // esta funcao fecha a janela modal e destrava a interface
    restoreInterfaceFromModal();
    // escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Detalhe');

    openModalPrintBloqueto();
}

function openModalPrintBloqueto() {
    var strPars = new String();
    var nPedidoID = dsoSup01.recordset['PedidoID'].value;
    var htmlPath;

    strPars = '?nPedidoID=' + escape(nPedidoID);
    strPars += '&sTitulo=' + escape('Bloqueto de cobran�a');
    strPars += '&sCaller=' + escape('S');

    // carregar modal de documentos
    htmlPath = SYS_PAGESURLROOT + '/modalgen/modalbloquetocobranca.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 543));
}

/*********************************************
Chama modal Visualizar XML
*********************************************/
function openModalViewXML() {
    var htmlPath;
    var nNotaFiscalID = dsoSup01.recordset['NotaFiscalID'].value;

    // Veio do sup ou do inf?
    var htmlId = (getHtmlId()).toUpperCase();
    //notaFiscalID = dsoSup01.recordset['NotaFiscalID'].value;

    // Passar o id do label do combo que vai preencher
    // para configurar a janela
    // e o id do combo que vai preencher, que volta com a mensagem
    // para saber o combo a preencher

    //  &nEmpresaID=' +nEmpresaID +  '&nNotaFiscal=' + nNotaFiscal ;

    var strPars = new String();

    strPars = '?sCaller=' + escape('S');
    strPars += '&NotaFiscalID=' + escape(nNotaFiscalID);

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalviewnfe.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 590));

    //TODO: criado apenas para quando converter o notafiscal, voltar a modal para la !!!
}

function showvalorlocalizar(nValorID) {
    var empresa = getCurrEmpresaData();

    // Manda o id da Nota Fiscal a detalhar 
    sendJSCarrier(getHtmlId(), 'SHOWVALORID', new Array(empresa[0], nValorID));
}

function chkFrete_onclick() {
    dsoTransporte01_DSC();
}

function setShipToVisibility() {
    var nEmpresaID = getCurrEmpresaData();
    var sVisibility = 'hidden';
    var aItemSelected = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = aItemSelected[1];


    // Combo de ship to, somente p/ allplus e pedido de saida
    if ((nEmpresaID[0] == 7) && (nContextoID == 5112))
        sVisibility = 'inherit';

    lblShipTo.style.visibility = sVisibility;
    selShipTo.style.visibility = sVisibility;
    btnFindShipTo.style.visibility = sVisibility;
}

function openModalShipTo() {
    var empresaData = getCurrEmpresaData();

    strPars = '?nFormID=';
    strPars += escape(window.top.formID);
    strPars += '&sCaller=';
    strPars += escape('S');
    strPars += '&nEmpresaID=';
    strPars += escape(empresaData[0]);
    strPars += '&sComboID=';
    strPars += escape('selShipTo');
    strPars += '&sLabel=';
    strPars += escape(getLabelNumStriped(lblShipTo.innerText));

    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/valoresalocalizar/modalpages/modalfindpessoa.asp' + strPars;
    showModalWin(htmlPath, new Array(571, 284));
}

/********************************************************************
Funcao criada pelo programador.
Preenche os campos do subform corrente, com os dados vindos da janela
modal de relacoes

Parametro:
aData   -   array de dados
aData[0] - ID do combo
aData[1] - ID da pessoa
aData[2] - Nome da Pessoa

os demais elementos sao opcionais

Retorno:
nenhum
********************************************************************/
function fillCmbShipTo(aData) {
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var elem;
    var addSexOrServ = null;

    elem = window.document.getElementById(aData[0]);

    if (elem == null)
        return;

    clearComboEx([elem.id]);

    oldDataSrc = elem.dataSrc;
    oldDataFld = elem.dataFld;
    elem.dataSrc = '';
    elem.dataFld = '';

    // Preencher o combo selSujeitoID
    dsoSup01.recordset.Fields['ShipToID'].value = aData[1];

    oOption = document.createElement("OPTION");
    oOption.text = '';
    oOption.value = 0;
    elem.add(oOption);

    oOption = document.createElement("OPTION");
    oOption.text = aData[2];
    oOption.value = aData[1];
    elem.add(oOption);

    elem.selectedIndex = 1;

    elem.dataSrc = oldDataSrc;
    elem.dataFld = oldDataFld;

    elem.disabled = false;
    setLabelShipTo();
}

function ImprimirNFe() {
    var nNotaFiscalID = dsoSup01.recordset['NotaFiscalID'].value;

    var strPars = new String();
    strPars = '?nNotaFiscalID=' + escape(nNotaFiscalID);
    strPars += '&bDanfe=1';
    strPars += '&bGNRE=1';
    strPars += '&bComprovantePagtoGNRE=1';
    strPars += '&bListaEmbalagem=1';
    dsoImpressaoNFe.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/impressaoNFe.aspx' + strPars;
    dsoImpressaoNFe.ondatasetcomplete = ImprimirNFe_DSC;
    dsoImpressaoNFe.refresh();
}

function ImprimirNFe_DSC() {
    return null;
}

function ajustaCampos() {
    glb_TomacaoServico = false;

    setupPage();

    selContaID.style.visibility = 'hidden';
    lblContaID.style.visibility = 'hidden';
    lblFrete.style.visibility = 'inherit';
    chkFrete.style.visibility = 'inherit';
    lblTransportadoraID.style.visibility = 'inherit';
    selTransportadoraID.style.visibility = 'inherit';
    lblMeioTransporteID.style.visibility = 'inherit';
    selMeioTransporteID.style.visibility = 'inherit';
    lblModalidadeTransporteID.style.visibility = 'inherit';
    selModalidadeTransporteID.style.visibility = 'inherit';
    // lbldtPrevisaoEntrega.style.visibility = 'inherit';
    // txtdtPrevisaoEntrega.style.visibility = 'inherit';
    lblAtrasoExterno.style.visibility = 'inherit';
    chkAtrasoExterno.style.visibility = 'inherit';
    lblDepositoID.style.visibility = 'inherit';
    selDepositoID.style.visibility = 'inherit';
    lblLocalEntregaID.style.visibility = 'inherit';
    selLocalEntregaID.style.visibility = 'inherit';
    lblShipTo.style.visibility = 'inherit';
    selShipTo.style.visibility = 'inherit';
    // txtdtPrevisaoEntrega.style.visibility = 'inherit';
    lblInvoiceID.style.visibility = 'hidden';
    txtInvoiceID.style.visibility = 'hidden';
    lblDataInvoice.style.visibility = 'hidden';
    txtDataInvoice.style.visibility = 'hidden';



    glb_OrigemPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'OrigemPedidoID' + '\'' + '].value');

    if (glb_OrigemPedidoID != 607) {
        lblProgramaMarketing.style.visibility = 'hidden';
        txtProgramaMarketing.style.visibility = 'hidden';
    }
    else {
        lblProgramaMarketing.style.visibility = 'inherit';
        txtProgramaMarketing.style.visibility = 'inherit';
        txtProgramaMarketing.style.disabled = true;
    }

}
function ajustaCampos_Tomacao() {
    glb_TomacaoServico = true;

    var Hoje = new Date();
    var Dia = ((Hoje.getDate() < 10) ? '0' + Hoje.getDate().toString() : Hoje.getDate().toString());
    var Mes = Hoje.getMonth();
    var nEmpresaID = getCurrEmpresaData();

    Mes = Mes + 1;
    Mes = ((Mes < 10) ? '0' + Mes : Mes);
    Hoje = Dia + '/' + Mes + '/' + Hoje.getFullYear().toString();

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
                          ['lblEstadoID', 'txtEstadoID', 2, 1, -2],
                          ['lbldtPedido', 'txtdtPedido', 10, 1, -3],
                          ['lblPessoaID', 'selPessoaID', 23, 1, -1],
                          ['btnFindPessoa', 'btn', 21, 1],
                          ['lblParceiroID', 'selParceiroID', 23, 1],
                          ['lblTransacaoID', 'selTransacaoID', 8, 1, -2],
                          ['btnFindTransacao', 'btn', 21, 1],
                          ['lblPedidoReferenciaID', 'txtPedidoReferenciaID', 8, 1],
						  ['lblPedidoComplemento', 'chkPedidoComplemento', 3, 1, -6],
                          ['lblPedidoMae', 'chkPedidoMae', 3, 2],
                          ['lblSuspenso', 'chkSuspenso', 3, 2, -6],
                          ['lblPermiteReserva', 'chkPermiteReserva', 3, 2, -6],
                          ['lblAjustesRejeicaoNFePendentes', 'chkAjustesRejeicaoNFePendentes', 3, 2, -6],
                          ['lblTemNF', 'chkTemNF', 3, 2, -3],
                          ['lblNotaFiscalID', 'selNotaFiscalID', 13, 2, -8],
                          ['lblDataNF', 'txtDataNF', 10, 2, -6],
                          ['lblObservacao', 'txtObservacao', 31, 2, -3],
                          ['lblNumeroProjeto', 'txtNumeroProjeto', 12, 2, -3],
                          ['lblSeuPedido', 'txtSeuPedido', 12, 2, -3],
                          ['lblImpostosIncidencia', 'chkImpostosIncidencia', 3, 3],
                          ['lblImpostosEsquema', 'chkImpostosEsquema', 3, 3, -5],
                          ['lblAtualizaPrecos', 'chkAtualizaPrecos', 3, 3, -6],
                          ['lblArredondaPreco', 'chkArredondaPreco', 3, 3, -6],
                          ['lblGeraParcelaGare', 'chkGeraParcelaGare', 3, 3, -6],
                          ['lblMoedaID', 'selMoedaID', 8, 3],
                          ['lblTaxaMoeda', 'txtTaxaMoeda', 11, 3],
                          ['lblNumeroItens', 'txtNumeroItens', 3, 3],
                          ['lblFinanciamentoID', 'selFinanciamentoID', 15, 3],
                          ['lblNumeroParcelas', 'selNumeroParcelas', 5, 3],
                          ['lblPrazo1', 'txtPrazo1', 4, 3],
                          ['lblIncremento', 'txtIncremento', 4, 3],
                          ['lblPrazoMedioPagamento', 'txtPrazoMedioPagamento', 5, 3],
                          ['lblVariacao', 'txtVariacao', 5, 3],
                          ['lblPercentualSUP', 'txtPercentualSUP', 5, 3, -5],
                          ['lblValorFrete', 'txtValorFrete', 11, 4],
                          ['lblValorSeguro', 'txtValorSeguro', 11, 4],
                          ['lblValorTotalServicos', 'txtValorTotalServicos', 11, 4],
                          ['lblValorTotalProdutos', 'txtValorTotalProdutos', 11, 4],
                          ['lblValorTotalImpostos', 'txtValorTotalImpostos', 11, 4],
                          ['lblTotalDesconto', 'txtTotalDesconto', 11, 4],
                          ['lblValorTotalPedido', 'txtValorTotalPedido', 11, 4],
                          ['lblMargemContribuicao', 'txtMargemContribuicao', 6, 4],
                          ['lblValorOutrasDespesas', 'txtValorOutrasDespesas', 11, 4],
                          ['lblContaID', 'selContaID', 50, 5],
                          ['lblFrete', 'chkFrete', 3, 5, 0, -2],
                          ['lblTransportadoraID', 'selTransportadoraID', 15, 5, -8],
                          ['lblMeioTransporteID', 'selMeioTransporteID', 11, 5, -2],
                          ['lblModalidadeTransporteID', 'selModalidadeTransporteID', 5, 5, -2],
                          ['lbldtPrevisaoEntrega', 'txtdtPrevisaoEntrega', 16, 5, -9],
                          ['lblAtrasoExterno', 'chkAtrasoExterno', 3, 5, -2],
                          ['lblDepositoID', 'selDepositoID', 15, 5, -7],
                          ['lblLocalEntregaID', 'selLocalEntregaID', 17, 5],
                          ['lblOrigemPedidoID', 'selOrigemPedidoID', 10, 5, -3],
                          ['lblProgramaMarketing', 'txtProgramaMarketing', 10, 5, -3],
                          ['lblShipTo', 'selShipTo', 19, 5, -4],
                          ['btnFindShipTo', 'btn', 21, 5]], null, null, true);


    //Faz altera��es necessarias nos campos somente se o pedido estiver em C ou se for um novo registro.
    if ((dsoSup01.recordset['EstadoID'].value == 21) || (dsoSup01.recordset[glb_sFldIDName].value == null)) {
        selTransportadoraID.value = selParceiroID.value; //Preenche Transportadora com o mesmo para o caso de toma��o de servi�o.
        selDepositoID.value = ((nEmpresaID[0] == 4) ? 12 : nEmpresaID[0]); //Preenche deposito com a empresa no caso de toma��o de servi�o.
        txtdtPrevisaoEntrega.value = Hoje; //Preenche previs�o de entrega com a data do dia para o caso de toma��o de servi�o.
    }

    txtdtPrevisaoEntrega.style.visibility = 'hidden';

    selContaID.style.visibility = 'inherit';
    lblContaID.style.visibility = 'inherit';
    lblFrete.style.visibility = 'hidden';
    chkFrete.style.visibility = 'hidden';
    lblTransportadoraID.style.visibility = 'hidden';
    selTransportadoraID.style.visibility = 'hidden';
    lblMeioTransporteID.style.visibility = 'hidden';
    selMeioTransporteID.style.visibility = 'hidden';
    lblModalidadeTransporteID.style.visibility = 'hidden';
    selModalidadeTransporteID.style.visibility = 'hidden';
    lbldtPrevisaoEntrega.style.visibility = 'hidden';
    txtdtPrevisaoEntrega.style.visibility = 'hidden';
    lblAtrasoExterno.style.visibility = 'hidden';
    chkAtrasoExterno.style.visibility = 'hidden';
    lblDepositoID.style.visibility = 'hidden';
    selDepositoID.style.visibility = 'hidden';
    lblLocalEntregaID.style.visibility = 'hidden';
    selLocalEntregaID.style.visibility = 'hidden';
    lblShipTo.style.visibility = 'hidden';
    selShipTo.style.visibility = 'hidden';
    lblInvoiceID.style.visibility = 'hidden';
    txtInvoiceID.style.visibility = 'hidden';
    lblDataInvoice.style.visibility = 'hidden';
    txtDataInvoice.style.visibility = 'hidden';

    // preencheContas();
}

/*
function preencheContas() {
    var nPedidoID = dsoSup01.recordset['PedidoID'].value;
    var nTransacaoID = dsoSup01.recordset['TransacaoID'].value;
    var bPedidoEhComissao = dsoSup01.recordset['PedidoEhComissao'].value;
    var sSql = '';

    // if (nTransacaoID = 185)
    if (((glb_TomacaoServico) && (!bPedidoEhComissao)) || (dsoSup01.recordset['NCM'].value == 1)) {
        sSql = 'SELECT SPACE(0) AS fldID, SPACE(0) AS fldNam ' +
            'UNION ALL ' +
            'SELECT DISTINCT a.ContaID AS fldID, CONVERT(VARCHAR(10), a.ContaID) + \' - \' + a.Conta AS fldNam ' +
            'FROM PlanoContas a WITH (NOLOCK) ' +
	            'INNER JOIN Conceitos_Contas b WITH(NOLOCK) ON b.ContaID = a.ContaID ' +
	            'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON c.ProdutoID = b.ConceitoID ' +
            'WHERE (a.EstadoID = 2) AND (c.PedidoID = ' + nPedidoID + ')';
    }
    else {
        sSql = 'SELECT SPACE(0) AS fldID, SPACE(0) AS fldNam ' +
            'UNION ALL ' +
                ' SELECT a.ContaID AS fldID, CONVERT(VARCHAR(10), a.ContaID) + \' - \' + a.Conta AS fldNam ' +
                    'FROM PlanoContas a WITH (NOLOCK) ' +
	                    'INNER JOIN Conceitos_Contas b WITH(NOLOCK) ON b.ContaID = a.ContaID ' +
	                    'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON c.ProdutoID = b.ConceitoID ' +
                    'WHERE (a.EstadoID = 2) AND (c.PedidoID = ' + nPedidoID + ') AND (b.ComissaoVendas = 1)';

    }

    setConnection(dsoContas);
    dsoContas.SQL = sSql;
    dsoContas.ondatasetcomplete = preencheContas_DSC;
    dsoContas.Refresh();
}

function preencheContas_DSC() {
    var nTransacaoID = dsoSup01.recordset['TransacaoID'].value;
    var nEstadoID = dsoSup01.recordset['EstadoID'].value;

    clearComboEx(['selContaID']);

    while ((!dsoContas.recordset.BOF) && (!dsoContas.recordset.EOF)) {
        optionStr = dsoContas.recordset['fldNam'].value;
        optionValue = dsoContas.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;

        if (optionValue == dsoSup01.recordset['ContaID'].value)
            oOption.selected = true;

        selContaID.add(oOption);

        dsoContas.recordset.MoveNext();
    }

    //if ((glb_Edit) && (nEstadoID <= 26) && (nTransacaoID != 185))
    if ((glb_Edit) && (nEstadoID <= 26) && (!glb_TomacaoServico))
        selContaID.disabled = (selContaID.options.length <= 1);
        //else if (!(glb_Edit) && (nEstadoID <= 26) && (nTransacaoID != 185)) {
    else if (!(glb_Edit) && (nEstadoID <= 26) && (!glb_TomacaoServico)) {
        selContaID.disabled = true;

        if (selContaID.options.length <= 1)
            dsoSup01.recordset['ContaID'].value = selContaID.options.value;
    }

    glb_Edit = false;
}
*/

function ajustaCampos_ImportacaoServico()
{
    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
                            ['lblEstadoID', 'txtEstadoID', 2, 1, -2],
                            ['lbldtPedido', 'txtdtPedido', 10, 1, -3],
                            ['lblPessoaID', 'selPessoaID', 23, 1, -1],
                            ['btnFindPessoa', 'btn', 21, 1],
                            ['lblParceiroID', 'selParceiroID', 23, 1],
                            ['lblTransacaoID', 'selTransacaoID', 8, 1, -2],
                            ['btnFindTransacao', 'btn', 21, 1],
                            ['lblPedidoReferenciaID', 'txtPedidoReferenciaID', 8, 1],
                            ['lblPedidoComplemento', 'chkPedidoComplemento', 3, 1, -6],
                            ['lblPedidoMae', 'chkPedidoMae', 3, 2],
                            ['lblSuspenso', 'chkSuspenso', 3, 2, -6],
                            ['lblPermiteReserva', 'chkPermiteReserva', 3, 2, -6],
                            ['lblAjustesRejeicaoNFePendentes', 'chkAjustesRejeicaoNFePendentes', 3, 2, -6],
                            ['lblTemNF', 'chkTemNF', 3, 2, -3],
                            ['lblInvoiceID', 'txtInvoiceID', 13, 2, -8],
                            ['lblDataInvoice', 'txtDataInvoice', 10, 2, -6],
                            ['lblObservacao', 'txtObservacao', 31, 2, -3],
                            ['lblNumeroProjeto', 'txtNumeroProjeto', 17, 2, -3],
                            ['lblSeuPedido', 'txtSeuPedido', 20, 2, -3],
                            ['lblImpostosIncidencia', 'chkImpostosIncidencia', 3, 3],
                            ['lblImpostosEsquema', 'chkImpostosEsquema', 3, 3, -5],
                            ['lblAtualizaPrecos', 'chkAtualizaPrecos', 3, 3, -6],
                            ['lblArredondaPreco', 'chkArredondaPreco', 3, 3, -6],
                            ['lblGeraParcelaGare', 'chkGeraParcelaGare', 3, 3, -6],
                            ['lblMoedaID', 'selMoedaID', 8, 3],
                            ['lblTaxaMoeda', 'txtTaxaMoeda', 11, 3],
                            ['lblNumeroItens', 'txtNumeroItens', 3, 3],
                            ['lblFinanciamentoID', 'selFinanciamentoID', 15, 3],
                            ['lblNumeroParcelas', 'selNumeroParcelas', 5, 3],
                            ['lblPrazo1', 'txtPrazo1', 4, 3],
                            ['lblIncremento', 'txtIncremento', 4, 3],
                            ['lblPrazoMedioPagamento', 'txtPrazoMedioPagamento', 5, 3],
                            ['lblVariacao', 'txtVariacao', 5, 3],
                            ['lblPercentualSUP', 'txtPercentualSUP', 5, 3, -5],
                            ['lblValorFrete', 'txtValorFrete', 11, 4],
                            ['lblValorSeguro', 'txtValorSeguro', 11, 4],
                            ['lblValorTotalServicos', 'txtValorTotalServicos', 11, 4],
                            ['lblValorTotalProdutos', 'txtValorTotalProdutos', 11, 4],
                            ['lblValorTotalImpostos', 'txtValorTotalImpostos', 11, 4],
                            ['lblTotalDesconto', 'txtTotalDesconto', 11, 4],
                            ['lblValorTotalPedido', 'txtValorTotalPedido', 11, 4],
                            ['lblMargemContribuicao', 'txtMargemContribuicao', 6, 4],
                            ['lblValorOutrasDespesas', 'txtValorOutrasDespesas', 11, 4],
                            ['lblFrete', 'chkFrete', 3, 5, 0, -2],
                            ['lblTransportadoraID', 'selTransportadoraID', 15, 5, -8],
                            ['lblMeioTransporteID', 'selMeioTransporteID', 11, 5, -2],
                            ['lblModalidadeTransporteID', 'selModalidadeTransporteID', 5, 5, -2],
                            ['lbldtPrevisaoEntrega', 'txtdtPrevisaoEntrega', 16, 5, -9],
                            ['lblAtrasoExterno', 'chkAtrasoExterno', 3, 5, -2],
                            ['lblDepositoID', 'selDepositoID', 15, 5, -7],
                            ['lblLocalEntregaID', 'selLocalEntregaID', 17, 5],
                            ['lblOrigemPedidoID', 'selOrigemPedidoID', 12, 5, -3],
                            ['lblProgramaMarketing', 'txtProgramaMarketing', 15, 5, -3],
                            ['lblShipTo', 'selShipTo', 10, 5, -120],
                            ['btnFindShipTo', 'btn', 21, 5]], null, null, true);

    lblNotaFiscalID.style.visibility = 'hidden';
    selNotaFiscalID.style.visibility = 'hidden';
    lblDataNF.style.visibility = 'hidden';
    txtDataNF.style.visibility = 'hidden';
    lblInvoiceID.style.visibility = 'inherit';
    txtInvoiceID.style.visibility = 'inherit';
    lblDataInvoice.style.visibility = 'inherit';
    txtDataInvoice.style.visibility = 'inherit';

}

function depositoEmpresa(nEmpresaID) {
    if (nEmpresaID == 4)
        return 12;
    else if (nEmpresaID == 16)
        return 19244;
    else if (nEmpresaID == 21)
        return 276273;
    else
        return nEmpresaID;
}
function openModalKardex(PedidoID) {
    var htmlPath;


    var strPars = new String();

    // mandar os parametros para o servidor
    strPars = '?sCaller=' + escape('S');
    strPars += '&nPedidoID=' + escape(PedidoID);

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalKardex.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 543));
}
function openModalResultados(PedidoID) {
    var htmlPath;


    var strPars = new String();

    // mandar os parametros para o servidor
    strPars = '?sCaller=' + escape('S');
    strPars += '&nPedidoID=' + escape(PedidoID);

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalresultados.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 543));

}

function openModalImportacao() {
    var htmlPath;
    var nPedidoID = dsoSup01.recordset['PedidoID'].value;
    var strPars = new String();

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalimportacao.asp';
    showModalWin(htmlPath, new Array(493, 100));
}


function openModalApuracaoImpostos() {
    var htmlPath;
    var strPars = new String();
    var nEmpresaData = getCurrEmpresaData();

    strPars = '?sCaller=' + escape('S');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nContextoID=' + escape(0);
    strPars += '&nPaisEmpresaID=' + escape(nEmpresaData[1]);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalapuracaoimpostos.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}

function setSelOrigemPedido() {

    var oOption = document.createElement("OPTION");
    oOption.text = "Normal";
    oOption.value = 605;
    oOption.selected = true;
    selOrigemPedidoID.add(oOption);

}

function PerfiVendedor() {
    setConnection(dsoPerfilVendedor);

    dsoPerfilVendedor.SQL = 'SELECT CONVERT(BIT, COUNT(DISTINCT a.PerfilID)) AS EhVendedor ' +
                                'FROM RelacoesPesRec_Perfis a WITH(NOLOCK)' +
                                    'INNER JOIN RelacoesPesRec b ON (a.RelacaoID = b.RelacaoID) ' +
                                'WHERE (b.SujeitoID = ' + glb_nUserID + ' AND a.PerfilID IN (651, 652, 653)) ';

    dsoPerfilVendedor.ondatasetcomplete = perfilVendedor_DSC;
    dsoPerfilVendedor.Refresh();
}

function perfilVendedor_DSC() {
    var bEhVendedor = dsoPerfilVendedor.recordset['EhVendedor'].value;
    var TemNF = dsoSup01.recordset['NotaFiscalID'].value;

    if ((TemNF > 0) || (bEhVendedor))
        chkSuspenso.disabled = true;
    else
        chkSuspenso.disabled = false;
}

function previsaoEntrega()
{
    var aItemSelected;
    var nContextoID;
    var nEmpresaID = getCurrEmpresaData()[0];

    // pega o value do contexto atual
    aItemSelected = getCmbCurrDataInControlBar('sup', 1);
    nContextoID = aItemSelected[1];

    txtPrevisaoEntrega.readOnly = true;

    // 5111->Pedido de Entrada, 5112 Pedido de Saida
    if ((nContextoID == 5112) && (nEmpresaID != 7))
    {
        if (glb_TomacaoServico == 0) {
            txtPrevisaoEntrega.style.visibility = 'inherit';
            lblPrevisaoEntrega.style.visibility = 'inherit';
        }
        else {
            txtPrevisaoEntrega.style.visibility = 'hidden';
            lblPrevisaoEntrega.style.visibility = 'hidden';
        }

        txtdtPrevisaoEntrega.style.visibility = 'hidden';
        lbldtPrevisaoEntrega.style.visibility = 'hidden';

        if (!(dsoSup01.recordset.BOF && dsoSup01.recordset.EOF))
        {
            txtPrevisaoEntrega.value = dsoSup01.recordset['PrevisaoEntrega'].value;
        }
    }
    else if ((nContextoID == 5111) || (nEmpresaID == 7))
    {
        txtPrevisaoEntrega.style.visibility = 'hidden';
        lblPrevisaoEntrega.style.visibility = 'hidden';

        txtdtPrevisaoEntrega.style.visibility = 'inherit';
        lbldtPrevisaoEntrega.style.visibility = 'inherit';

        ajustaEntrada();
    }
}

function preencheMoeda()
{
    var nEmpresaData = getCurrEmpresaData();
    var nPaisEmpresaID = nEmpresaData[1];

    if (nPaisEmpresaID == 130)
        selMoedaID.value = 647;
    else
        selMoedaID.value = 541;

    if (selTransacaoID.selectedIndex >= 0)
    {
        if (selTransacaoID.value == 113)
            selMoedaID.value = 541;
    }
}