/********************************************************************
modalassociarlotes.js

Library javascript para o modalassociarlotes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_PedidoFechado = 0;
var glb_PedidoID = 0;
var glb_ComplementoValor = false;
var glb_nDSOCounter = 0;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;

var glb_ColGrid = [];
var glb_ColGrid = null;
var glbLoteID = null;
var glbProdutoID = null;
var glbGridOk = false;
var glb_Inicio = true;
var glb_aGridOk = null;
var glb_nTipoSaldoLote = null;
// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoGridItens = new CDatatransport('dsoGridItens');
var dsoGridLotes = new CDatatransport('dsoGridLotes');
var dsoMatriz = new CDatatransport('dsoMatriz');
var dsoIncluirLote = new CDatatransport('dsoIncluirLote');
var dsoAssociarItemLote = new CDatatransport('dsoAssociarItemLote');
// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    glb_PedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');
    glb_PedidoFechado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Fechado' + '\'' + '].value');

    var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransacaoID' + '\'' + '].value');
    var nTipoPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoPedidoID' + '\'' + '].value');

    if ((nTransacaoID == 115) || (nTransacaoID == 215) || (nTransacaoID == 535))
        glb_nTipoSaldoLote = '5'; //Quantidade a vender
    else if (nTransacaoID == 112)
        glb_nTipoSaldoLote = '10'; //Quantidade vendida
    else if ((nTransacaoID == 111) || (nTransacaoID == 211))
        glb_nTipoSaldoLote = '3'; //Quantidade a comprar
    else if (nTransacaoID == 116)
        glb_nTipoSaldoLote = '11'; //Quantidade comprada
    else if ((nTransacaoID == 891) || (nTransacaoID == 895) || (nTransacaoID == 896) || (nTransacaoID == 811) || (nTransacaoID == 812) || (nTransacaoID == 816))
        glb_nTipoSaldoLote = '13'; //Quantidade comprada
    else if (nTipoPedidoID == 602)
        glb_nTipoSaldoLote = '5'; //Quantidade a vender
    else if (nTipoPedidoID == 601)
        glb_nTipoSaldoLote = '3'; //Quantidade a comprar

    matriz();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 49;
    modalFrame.style.left = 0;

}
function window_onload_DSC() {
    with (ModalAssociarLotesBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();
}





/********************************************************************
Clique botao OK ou Cancela ou botoes 
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        ;
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);

    else if (ctl.id == btnAssociar.id) {
        if (glb_aGridOk != null) {
            if ((glb_aGridOk[0] == 'fgLotes') && (fgLotes.row == 1) && (fgLotes.TextMatrix(1, getColIndexByColKey(fgLotes, 'LoteID*')) == 0))
                IncluirLote();
            else
                AssociarItemLote(getRegitro(), false);
        }
        else {
            window.top.overflyGen.Alert('N�o foram encontrados itens para serem associados ao Lote.');

            lockControlsInModalWin(false);
        }
        //window.top.overflyGen.Alert('AINDA NAO FUNFOU');
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Associar Lotes');

    // texto da secao01
    secText('Associar Lotes', 1);

    loadDataAndTreatInterface();
    // btnOK nao e usado nesta modal

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    with (btnOK) {
        disabled = true;
        style.visibility = 'hidden';
    }

    // btnCanc nao e usado nesta modal
    with (btnCanc) {
        style.visibility = 'hidden';
    }
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;

    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;

    // Ajusta botoes OK/Cancel para esta janela
    with (btnOK.style) {
        disabled = true;
        left = (widthFree - 2 * parseInt(width, 10) - ELEM_GAP) / 2;
    }
    with (btnCanc.style) {
        left = (parseInt(btnOK.currentStyle.left, 10) + parseInt(btnOK.currentStyle.width, 10) + ELEM_GAP);
    }


    btnAssociar.style.top = topFree + 10;
    btnAssociar.style.left = 900 + ELEM_GAP;
    btnAssociar.style.width = FONT_WIDTH * 7 + 20;

    lblItens.style.top = topFree + 30;
    lblItens.style.left = ELEM_GAP;
    lblItens.style.width = FONT_WIDTH * 7 + 20;

    lblLotes.style.top = topFree + 30;
    lblLotes.style.left = 500 + ELEM_GAP;
    lblLotes.style.width = FONT_WIDTH * 7 + 20;


    // ajusta o divFG
    elem = window.document.getElementById('divFGItens');

    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(btnAssociar.currentStyle.top, 10) + ELEM_GAP + 30;
        width = 480 + ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) -
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10) + 50;
    }

    elem = document.getElementById('fgItens');
    with (elem.style) {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFGItens').currentStyle.width);
        height = parseInt(document.getElementById('divFGItens').currentStyle.height);
    }

    startGridInterface(fgItens);
    fgItens.Cols = 1;
    fgItens.ColWidth(0) = parseInt(divFGItens.currentStyle.width, 10) * 18;

    elemLotes = window.document.getElementById('divFGLotes');
    with (elemLotes.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = parseInt(btnAssociar.currentStyle.left, 10) + ELEM_GAP - 415;
        top = parseInt(btnAssociar.currentStyle.top, 10) + ELEM_GAP + 30;
        width = 475 + ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) -
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10) + 50;
    }

    elemLotes = document.getElementById('fglotes');
    with (elemLotes.style) {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFGLotes').currentStyle.width);
        height = parseInt(document.getElementById('divFGLotes').currentStyle.height);
    }

    startGridInterface(fgLotes);
    fgLotes.Cols = 1;
    fgLotes.ColWidth(0) = parseInt(divFGLotes.currentStyle.width, 10) * 18;

    getDataInServerItens();
    getDataInServerLotes();
}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServerItens() {
    var sSQL = 'SELECT DISTINCT  0 AS OK, a.ProdutoID AS ProdutoID, b.Conceito AS Produto, a.Quantidade AS Quantidade, a.ValorUnitario as ValorUnitario, ' +
                            'dbo.fn_Lotes_ProdutosQuantidades(NULL, a.ProdutoID, 6, a.PedItemID) AS Saldo, ' +
                            '0 AS Lote, ' +
                            '0 AS SaldoLote, 0 AS Associar, 0 AS QtdAssociar, a.PedItemID AS PedItemID, a.PedidoID, 0 AS SaldoAssociado ' +
                    'FROM Pedidos_Itens a WITH(NOLOCK) ' +
                        'INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ProdutoID = b.ConceitoID) ' +
                    'WHERE (PedidoID = ' + glb_PedidoID + ') ' +
                    'ORDER BY a.ProdutoID';

    setConnection(dsoGridItens);
    dsoGridItens.SQL = sSQL;
    dsoGridItens.ondatasetcomplete = eval(fillModalGrid_DSC);
    dsoGridItens.refresh();
}
/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServerLotes() {
    var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransacaoID' + '\'' + '].value');
    var sSQL = '';

    /*if (nTransacaoID == 111) 
    {
        sSQL = 'SELECT 0 AS PedidoID, 0 AS OK, 0 AS LoteID, \'\' AS Descricao, GETDATE() AS dtFim, GETDATE() AS dtEmissao, 0 AS Saldo, 0 AS Lote, 0 AS SaldoLote, 0 AS Associar, ' +
                '0 AS QtdAssociar, 0 AS LotePeditemID, 0 AS LotProdutoID, 0 AS ProdutoID, 0 AS SaldoAssociado, 0 AS TipoLoteID UNION ALL ';
    }

    sSQL = sSQL + 'SELECT DISTINCT d.PedidoID, 0 AS OK, a.LoteID AS LoteID, a.Descricao AS Descricao, a.dtFim, a.dtEmissao, 0 AS Saldo, 0 AS Lote, ' +
                            '0  AS SaldoLote, ' +
                            '0 AS Associar, ' +
                            '0 AS QtdAssociar, 0 AS LotePeditemID, 0 AS LotProdutoID, 0 AS ProdutoID, 0 AS SaldoAssociado, ' +
                            '(CASE WHEN (e.PedidoID IS NOT NULL) THEN 1 WHEN ((e.PedidoID IS NULL) AND (c.ProdutoID IS NOT NULL)) THEN 2 ELSE 0 END) AS TipoLoteID ' +
                    'FROM Lotes a WITH(NOLOCK) ' +
                        'INNER JOIN Lotes_Produtos c WITH (NOLOCK) ON (a.LoteID = c.LoteID) ' +
                        'INNER JOIN Pedidos_Itens d WITH (NOLOCK) ON (c.ProdutoID = d.ProdutoID) ' +
                        'LEFT OUTER JOIN Lotes_Itens b WITH(NOLOCK) ON (b.LoteID = a.LoteID) AND (b.PedItemID = d.PedItemID) ' +
                        'LEFT OUTER JOIN Pedidos_Itens e WITH(NOLOCK) ON (e.PedItemID = b.PedItemID) AND (e.PedidoID = d.PedidoID) ' +
                    'WHERE d.PedidoID = ' + glb_PedidoID + ' AND (c.Quantidade > 0) ' +
                    'ORDER BY TipoLoteID, LoteID';
    */

    sSQL = 'exec sp_Lote_AssociarListagem ' + glb_PedidoID;

    setConnection(dsoGridLotes);
    dsoGridLotes.SQL = sSQL;
    dsoGridLotes.ondatasetcomplete = eval(fillModalGridLotes_DSC);
    dsoGridLotes.refresh();
}
/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGrid_DSC() {
    glb_GridIsBuilding = true;

    if (glb_Inicio)
        startGridInterface(fgItens);

    aHiddenColls = [];

    aHiddenColls = (glb_ColGrid == null ? OrderColGrid(null, fgItens, false) : glb_ColGrid);

    headerGrid(fgItens, ['PedidoID',
                           'OK',
                           'ID',
                           'Produto',
                           'Quant',
                           'Valor Unit',
                           'Saldo',
                           'Lote',
                           'Saldo',
                           'Associar',
                           'Associar',
                           'SaldoAssociado',
                           'PedItemID'], aHiddenColls);

    fillGridMask(fgItens, dsoGridItens, ['PedidoID',
                                          'OK',
                                          'ProdutoID*',
							              'Produto*',
                                          'Quantidade*',
                                          'ValorUnitario*',
                                          'Saldo*',
                                          'Lote*',
                                          'SaldoLote*',
                                          'Associar',
                                          'QtdAssociar',
                                          'SaldoAssociado',
                                          'PedItemID'],
                                          ['', '', '', '', '', '9999999999.99', '', '', '', '', '', ''],
                                          ['', '', '', '', '', '###,###,##0.00', '', '', '', '', '', '']);


    //alignColsInGrid(fg,[0,1,5,6,7,8,9]);
    //fg.MergeCells = 4;

    with (fgItens) {
        ColDataType(1) = 11;
        ColDataType(9) = 11;
    }

    fgItens.Editable = true;
    fgItens.FontSize = '8';
    fgItens.Redraw = 0;
    fgItens.AutoSizeMode = 0;
    fgItens.AutoSize(0, fgItens.Cols - 1, false, 0);
    fgItens.Redraw = 2;

    if (glbGridOk)
        FiltroMatriz(fgItens);

    glb_ColGrid = null;

    glb_GridIsBuilding = false;
}
/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGridLotes_DSC() {
    glb_GridIsBuilding = true;

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    if (glb_Inicio)
        startGridInterface(fgLotes);

    aHiddenColls = [];

    aHiddenColls = (glb_ColGrid == null ? OrderColGrid(null, fgLotes, false) : glb_ColGrid);

    headerGrid(fgLotes, ['PedidoID',
                          'OK',
                          'ID',
				          'Descricao',
                          'Fim',
                          'Emissao',
                          'Lote',
                          'Saldo',
                          'Associar',
                          'Associar',
                          'LotePeditemID',
                          'LotProdutoID',
                          'SaldoAssociado',
                          'ProdutoID',
                          'TipoLoteID'], aHiddenColls);

    fillGridMask(fgLotes, dsoGridLotes, ['PedidoID',
                                          'OK',
                                          'LoteID*',
							              'Descricao',
                                          'dtFim',
                                          'dtEmissao*',
                                          'Lote*',
                                          'SaldoLote*',
                                          'Associar',
                                          'QtdAssociar',
                                          'LotePeditemID*',
                                          'LotProdutoID*',
                                          'SaldoAssociado',
                                          'ProdutoID*',
                                          'TipoLoteID'],
                                          ['', '', '', '', '99/99/9999', '99/99/9999', '', '', '', ''],
                                          ['', '', '', '', dTFormat, dTFormat, '', '', '', '']);

    //alignColsInGrid(fg,[0,1,5,6,7,8,9]);

    fgLotes.Editable = true;
    fgLotes.FontSize = '8';
    fgLotes.Redraw = 0;
    fgLotes.AutoSizeMode = 0;
    fgLotes.AutoSize(0, fgLotes.Cols - 1, false, 0);
    fgLotes.Redraw = 2;

    with (fgLotes) {
        ColDataType(1) = 11;
        ColDataType(8) = 11;
    }

    if (glbGridOk)
        FiltroMatriz(fgLotes);
    else {
        for (i = 1; i < fgLotes.Rows; i++) {
            nTipoLoteID = fgLotes.TextMatrix(i, getColIndexByColKey(fgLotes, 'TipoLoteID'));

            if (nTipoLoteID == 1)
                fgLotes.Cell(6, i, 0, i, (fgLotes.Cols - 1)) = 0X90EE90; //0x32CD32; - Alterado para verde Padr�o. BJBN
            else if (nTipoLoteID == 2)
                fgLotes.Cell(6, i, 0, i, (fgLotes.Cols - 1)) = 0x00FFFF;
            else
                fgLotes.Cell(6, i, 0, i, (fgLotes.Cols - 1)) = 0xFFFFFF;
        }
    }

    glb_GridIsBuilding = false;
    glb_ColGrid = null;

    glb_Inicio = false;

    lockControlsInModalWin(false);
}

function js_ModalAssociarLotes_AfterEdit(fg, Row, Col) {
    var bOk = false;
    var bAssociar = false;
    var bQtdAssociar = false;
    var nLimiteValor;
    var nValorAssociar;
    var LotePedItemID = 0;
    var ProdutoID = 0;
    var sFiltro = '';
    var nSaldoItem = 0;
    var nSaldoLote = 0;

    if ((glb_GridIsBuilding) || (Row == 0))
        return null;

    if ((fg.Editable)) {
        if (Col == getColIndexByColKey(fg, 'OK'))
            bOk = true;

        if (Col == getColIndexByColKey(fg, 'Associar'))
            bAssociar = true;

        if (Col == getColIndexByColKey(fg, 'QtdAssociar'))
            bQtdAssociar = true;

        if (bOk) {
            fg.Row = Row;

            if ((fg.textMatrix(Row, getColIndexByColKey(fg, 'OK')) != 0) && (!glbGridOk)) {
                glb_aGridOk = [fg.id, true];

                glbGridOk = true;
                fg.textMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;

                if (fg.id == 'fgLotes') {
                    glbLoteID = fg.textMatrix(Row, getColIndexByColKey(fg, 'LoteID*'));

                    if (glbLoteID != 0)
                        dsoMatriz.recordset.setFilter('LoteID = ' + glbLoteID + ' OR PedidoID = ' + glb_PedidoID);

                    try {
                        dsoMatriz.recordset.MoveFirst();
                    }
                    catch (e) {
                        sFiltro = '';
                    }

                    while ((!dsoMatriz.recordset.EOF) && (!dsoMatriz.recordset.BOF)) {
                        nSaldoLote = dsoMatriz.recordset['SaldoLote'].value;

                        if (sFiltro == '') {
                            if (glb_nTipoSaldoLote == 5)
                                sFiltro = 'Saldo <= ' + nSaldoLote + ' AND ProdutoID = ' + dsoMatriz.recordset['ProdutoID'].value;
                            else
                                sFiltro = 'ProdutoID = ' + dsoMatriz.recordset['ProdutoID'].value;
                        }
                        else {
                            if (glb_nTipoSaldoLote == 5)
                                sFiltro += ' OR Saldo <= ' + nSaldoLote + ' AND ProdutoID = ' + dsoMatriz.recordset['ProdutoID'].value;
                            else
                                sFiltro += ' OR ProdutoID = ' + dsoMatriz.recordset['ProdutoID'].value;;
                        }

                        dsoMatriz.recordset.MoveNext();
                    }
                    
                    dsoMatriz.recordset.setFilter('');

                    if ((sFiltro != '') && (glbLoteID != 0)) {
                        dsoGridItens.recordset.setFilter(sFiltro);

                        if ((!dsoGridItens.recordset.BOF) && (!dsoGridItens.recordset.EOF))
                            dsoGridItens.recordset.MoveFirst();
                    }
                }
                else if (fg.id == 'fgItens') {
                    glbProdutoID = fg.textMatrix(Row, getColIndexByColKey(fg, 'ProdutoID*'));
                    dsoMatriz.recordset.setFilter('ProdutoID = ' + glbProdutoID + ' OR PedidoID = ' + glb_PedidoID);
                    try {
                        dsoMatriz.recordset.MoveFirst();
                    }
                    catch (e) {
                        sFiltro = 'ProdutoID = -1';
                    }

                    while ((!dsoMatriz.recordset.EOF) && (!dsoMatriz.recordset.BOF)) {
                        nSaldoItem = fg.textMatrix(Row, getColIndexByColKey(fg, 'Saldo*'));
                        nSaldoLote = dsoMatriz.recordset['SaldoLote'].value;

                        if (sFiltro == '') {
                            if (glb_nTipoSaldoLote == 5) {
                                if (nSaldoLote >= nSaldoItem)
                                    sFiltro = 'LoteID = ' + dsoMatriz.recordset['LoteID'].value;
                                else
                                    sFiltro = 'LoteID <> ' + dsoMatriz.recordset['LoteID'].value;
                            }
                            else
                                sFiltro = 'LoteID = ' + dsoMatriz.recordset['LoteID'].value;
                        }
                        else {
                            if (glb_nTipoSaldoLote == 5) {
                                if (nSaldoLote >= nSaldoItem)
                                    sFiltro += ' OR LoteID = ' + dsoMatriz.recordset['LoteID'].value;
                                else
                                    sFiltro += ' OR LoteID <> ' + dsoMatriz.recordset['LoteID'].value;
                            }
                            else
                                sFiltro += ' OR LoteID = ' + dsoMatriz.recordset['LoteID'].value;

                        }

                        dsoMatriz.recordset.MoveNext();
                    }

                    dsoMatriz.recordset.setFilter('');

                    if (sFiltro != '') {
                        dsoGridLotes.recordset.setFilter(sFiltro);

                        if ((!dsoGridLotes.recordset.BOF) && (!dsoGridLotes.recordset.EOF))
                            dsoGridLotes.recordset.MoveFirst();
                    }
                }
                OrderColGrid(bOk, fg, false);
            }
            else {
                if (fg.textMatrix(Row, getColIndexByColKey(fg, 'OK')) == 0) {
                    glb_ColGrid = null;
                    glbGridOk = false;

                    glb_aGridOk = null;

                    dsoGridItens.recordset.setFilter('');
                    dsoGridLotes.recordset.setFilter('');

                    dsoGridItens.recordset.MoveFirst();

                    if ((!dsoGridLotes.recordset.BOF) && (!dsoGridLotes.recordset.EOF))
                        dsoGridLotes.recordset.MoveFirst();

                    OrderColGrid(false, fg, false);
                }

                fg.textMatrix(Row, getColIndexByColKey(fg, 'OK')) = 0;
            }
        }
        if (bAssociar) {
            if (fg.textMatrix(Row, getColIndexByColKey(fg, 'Associar')) != 0)
                fg.textMatrix(Row, getColIndexByColKey(fg, 'Associar')) = 1;
            else
                fg.textMatrix(Row, getColIndexByColKey(fg, 'Associar')) = 0;
        }

        if (bQtdAssociar) {
            nLimiteValor = LimiteItemAssociar(Row);

            nValorAssociar = fg.valueMatrix(Row, getColIndexByColKey(fg, 'QtdAssociar'));

            if (nValorAssociar > nLimiteValor)
                fg.textMatrix(Row, getColIndexByColKey(fg, 'QtdAssociar')) = nLimiteValor;

            fg.textMatrix(Row, getColIndexByColKey(fg, 'Associar')) = 1;
        }
    }
}

function js_BeforeRowColChangeModalAssociarLotes(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    //Forca celula read-only

    //js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);

    glb_HasGridButIsNotFormPage = true;

    if (!glb_GridIsBuilding) {
        if (glb_FreezeRolColChangeEvents)
            return true;

        if (cellIsLocked(grid, NewRow, NewCol)) {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else if (glb_aGridOk != null) {
            if ((glb_aGridOk[0] == grid.id) && (glb_aGridOk[1])) {
                glb_validRow = OldRow;
                glb_validCol = NewCol;
            }
            else
                js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);

        //GridLinhaTotal(fg);
    }
    //return null;

}

function cellIsLocked(grid, nRow, nCol) {
    var retVal = true;

    //(glb_nTipoSaldoLote != '5') - Somente para venda
    if ((nCol == getColIndexByColKey(grid, 'OK')) || (nCol == getColIndexByColKey(grid, 'Associar')) || ((glb_nTipoSaldoLote != '5') && (nCol == getColIndexByColKey(grid, 'QtdAssociar'))))
        retVal = false;

    if ((grid.id == 'fgLotes') && (nRow == 1) && ((nCol == getColIndexByColKey(grid, 'Descricao')) || (nCol == getColIndexByColKey(grid, 'dtFim')))) {
        if (grid.TextMatrix(nRow, getColIndexByColKey(grid, 'LoteID*')) == 0)
            retVal = false;
    }

    return retVal;
}

function fg_EnterCell_Prg() {
    return null;
}

function js_fg_ModalAssociarLotesDblClick(grid, Row, Col) {
    if (glb_GridIsBuilding)
        return null;

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if ((Col != getColIndexByColKey(grid, 'OK')) || (Col != getColIndexByColKey(grid, 'Associar')))
        return true;

    // trap so header
    if (Row != 1)
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);

    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 1; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    window.focus();
    grid.focus();

    //    GridLinhaTotal(grid);
}
function js_modalassociarlotes_BeforeEdit(grid, Row, Col) {
    if (glb_GridIsBuilding)
        return null;
    /*
        var nomeColuna = grid.ColKey(Col);
    
        var lastPos = nomeColuna.length - 1;
    
        // retira o asterisco do nome do campo
        if (nomeColuna.substr(lastPos, 1) == '*') 
        {
            nomeColuna = nomeColuna.substr(0, lastPos);
        }
    */
    /*  if ((dsoImpostos.recordset[nomeColuna].type == 200) || (dsoImpostos.recordset[nomeColuna].type == 129))
         fieldMaxLength(fg, Col, dsoImpostos);*/

    //GridLinhaTotal(fg);
}

function OrderColGrid(bOk, fg, rec) {
    if (bOk == null && fg.id == 'fgItens')
        glb_ColGrid = [0, 7, 8, 9, 10, 11, 12];

    else if (bOk == null && fg.id == 'fgLotes')
        glb_ColGrid = [0, 6, 7, 8, 9, 10, 11, 12, 13, 14];

    else if (bOk == false && fg.id == 'fgItens')
        fillModalGridLotes_DSC();

    else if (bOk == false && fg.id == 'fgLotes')
        fillModalGrid_DSC();

    else if ((bOk) && (fg.id == 'fgItens')) {
        if (rec) {
            glb_ColGrid = [0, 1, 11, 12];
        }
        else {
            glb_ColGrid = OrderColGrid(true, fgLotes, true);
            fillModalGridLotes_DSC();
        }
    }

    else if ((bOk) && (fg.id == 'fgLotes')) {
        if (rec) {
            glb_ColGrid = [0, 1, 10, 11, 12, 13, 14];
        }
        else {
            glb_ColGrid = OrderColGrid(true, fgItens, true);
            fillModalGrid_DSC();
        }
    }

    return glb_ColGrid;
}

function FiltroMatriz(fg) {
    if (fg.Rows <= 1)
        return false;

    var nProdutoID = 0;
    var nLoteID = 0;
    var nQuantidadeItem = 0;
    var nQuantidadeLote = 0;
    var nSaldoLote = 0;
    var nQuantidadeAssociar = 0;
    var nQuantidadeAssociada = 0;
    var nPedItemID;
    var nPedItemAssociadoID;
    var iCount = 0;

    if (!(dsoMatriz.recordset.BOF && dsoMatriz.recordset.EOF))
        dsoMatriz.recordset.MoveFirst();

    if (fg.id == 'fgLotes')
        nPedItemID = fgItens.ValueMatrix(fgItens.Row, getColIndexByColKey(fgItens, 'PedItemID'));

    for (iCount = 1; iCount < fg.Rows; iCount++) {

        if ((fg.id == 'fgItens') && (fg.rows > 1)) {
            nProdutoID = fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'ProdutoID*'));

            nPedItemID = fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'PedItemID'));

            if (!(dsoMatriz.recordset.BOF && dsoMatriz.recordset.EOF))
                dsoMatriz.recordset.setFilter('ProdutoID = ' + nProdutoID + ' AND LoteID = ' + glbLoteID);

            nQuantidadeItem = fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'Quantidade*'));

            nQuantidadeAssociada = 0;

            nQuantidadeLote = 0;

            nSaldoLote = 0;

            if ((!dsoMatriz.recordset.BOF) && (!dsoMatriz.recordset.EOF))
                nQuantidadeLote = dsoMatriz.recordset['QuantidadeLote'].value;

            while ((!dsoMatriz.recordset.BOF) && (!dsoMatriz.recordset.EOF)) {
                nSaldoLote = nSaldoLote + dsoMatriz.recordset['SaldoLote'].value;

                nPedItemAssociadoID = dsoMatriz.recordset['PedItemID'].value;

                if (nPedItemAssociadoID > 0) {
                    if (nPedItemAssociadoID == nPedItemID)
                        nQuantidadeAssociada = nQuantidadeAssociada + dsoMatriz.recordset['SaldoAssociado'].value;
                }

                dsoMatriz.recordset.MoveNext();
            }

            dsoMatriz.recordset.setFilter('');

            fg.textMatrix(iCount, getColIndexByColKey(fg, 'Lote*')) = nQuantidadeLote;
            fg.textMatrix(iCount, getColIndexByColKey(fg, 'SaldoLote*')) = nSaldoLote;
            fg.textMatrix(iCount, getColIndexByColKey(fg, 'SaldoAssociado')) = nQuantidadeAssociada;

            //(glb_nTipoSaldoLote != 5) - Somente quando n�o for venda.
            if ((nQuantidadeAssociada > 0) && (glb_nTipoSaldoLote != 5))
                nQuantidadeAssociar = nQuantidadeAssociada;
            else
                nQuantidadeAssociar = LimiteItemAssociar(iCount);

            fg.textMatrix(iCount, getColIndexByColKey(fg, 'QtdAssociar')) = nQuantidadeAssociar;
        }

        else if ((fg.id == 'fgLotes') && (fg.rows > 1)) {
            nLoteID = fg.ValueMatrix(iCount, getColIndexByColKey(fg, 'LoteID*'));

            dsoMatriz.recordset.setFilter('ProdutoID = ' + glbProdutoID + ' AND LoteID = ' + nLoteID);

            nPedItemAssociadoID = 0;
            nQuantidadeAssociada = 0;

            if ((!dsoMatriz.recordset.BOF) && (!dsoMatriz.recordset.EOF)) {
                nQuantidadeLote = dsoMatriz.recordset['QuantidadeLote'].value;
                nSaldoLote = dsoMatriz.recordset['SaldoLote'].value;
                nPedItemAssociadoID = dsoMatriz.recordset['PedItemID'].value;

                if (nPedItemAssociadoID > 0) {
                    if (nPedItemAssociadoID == nPedItemID)
                        nQuantidadeAssociada = dsoMatriz.recordset['SaldoAssociado'].value;
                }
            }

            dsoMatriz.recordset.setFilter('');

            fg.textMatrix(iCount, getColIndexByColKey(fg, 'Lote*')) = nQuantidadeLote;
            fg.textMatrix(iCount, getColIndexByColKey(fg, 'SaldoLote*')) = nSaldoLote;
            fg.textMatrix(iCount, getColIndexByColKey(fg, 'SaldoAssociado')) = nQuantidadeAssociada;

            if (nQuantidadeAssociada > 0)
                fgLotes.Cell(6, iCount, 0, iCount, (fgLotes.Cols - 1)) = 0x32CD32;
            else if (nQuantidadeAssociada == 0)
                fgLotes.Cell(6, iCount, 0, iCount, (fgLotes.Cols - 1)) = 0x00FFFF;

            //(glb_nTipoSaldoLote != 5) - Somente quando n�o for venda.
            if ((nQuantidadeAssociada > 0) && (glb_nTipoSaldoLote != 5))
                nQuantidadeAssociar = nQuantidadeAssociada;
            else
                nQuantidadeAssociar = LimiteItemAssociar(iCount);

            fg.textMatrix(iCount, getColIndexByColKey(fg, 'QtdAssociar')) = nQuantidadeAssociar;
        }
    }

    dsoMatriz.recordset.setFilter('');
}

function LimiteItemAssociar(linha) {
    var linhaLote;
    var linhaItem;
    var nQuantidadeLote;
    var nSaldoLote;
    var nQuantidadeAssociada = 0;
    var bNovoLote = 0;

    if (glb_aGridOk != null) {
        if (glb_aGridOk[0] == 'fgItens') {
            linhaLote = linha;
            linhaItem = fgItens.Row;

            nQuantidadeLote = fgLotes.ValueMatrix(linhaLote, getColIndexByColKey(fgLotes, 'Lote*'));
            nSaldoLote = fgLotes.ValueMatrix(linhaLote, getColIndexByColKey(fgLotes, 'SaldoLote*'));
            nQuantidadeAssociada = fgLotes.ValueMatrix(linhaLote, getColIndexByColKey(fgLotes, 'SaldoAssociado'));
        }
        else if (glb_aGridOk[0] == 'fgLotes') {
            linhaItem = linha;
            linhaLote = fgLotes.Row;

            nQuantidadeLote = fgItens.ValueMatrix(linhaItem, getColIndexByColKey(fgItens, 'Lote*'));
            nSaldoLote = fgItens.ValueMatrix(linhaItem, getColIndexByColKey(fgItens, 'SaldoLote*'));
            nQuantidadeAssociada = fgItens.ValueMatrix(linhaItem, getColIndexByColKey(fgItens, 'SaldoAssociado'));

            if (fgLotes.TextMatrix(linhaLote, getColIndexByColKey(fgLotes, 'LoteID*')) == 0)
                bNovoLote = 1;
        }
    }

    if (nQuantidadeLote == 0)
        bNovoLote = 1;

    var nQuantidadeSaldoItem = fgItens.ValueMatrix(linhaItem, getColIndexByColKey(fgItens, 'Saldo*'));

    var nQuantidadeLimite = 0;

    if (glb_nTipoSaldoLote != 5) {
        if ((bNovoLote) || ((nQuantidadeAssociada + nSaldoLote) > (nQuantidadeAssociada + nQuantidadeSaldoItem))) {
            if ((nQuantidadeSaldoItem > 0) && (nQuantidadeAssociada > 0))
                nQuantidadeLimite = (nQuantidadeAssociada + nQuantidadeSaldoItem);
            else if ((nQuantidadeAssociada > 0) && (nQuantidadeSaldoItem == 0))
                nQuantidadeLimite = nQuantidadeAssociada;
            else
                nQuantidadeLimite = nQuantidadeSaldoItem;
        }
        else {
            if ((nSaldoLote > 0) && (nQuantidadeAssociada > 0))
                nQuantidadeLimite = (nQuantidadeAssociada + nSaldoLote);
            else if ((nQuantidadeAssociada > 0) && (nSaldoLote == 0))
                nQuantidadeLimite = nQuantidadeAssociada;
            else
                nQuantidadeLimite = nSaldoLote;
        }
    }
    else if (glb_nTipoSaldoLote == 5) {
        nQuantidadeLimite = nQuantidadeSaldoItem;
    }

    return nQuantidadeLimite;
}

function IncluirLote() {
    if (!VerificaQuantidadeAssociar()) {
        lockControlsInModalWin(false);

        return null;
    }

    var nProprietarioID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ProprietarioID' + '\'' + '].value');
    var nUsuarioID = getCurrUserID();
    var empresa = getCurrEmpresaData();
    var nEmpresaID = empresa[0];
    var dtFim = normalizeDate_DateTime(fgLotes.textMatrix(fgLotes.row, getColIndexByColKey(fgLotes, 'dtFim')), true);
    var sDescricao = fgLotes.textMatrix(fgLotes.row, getColIndexByColKey(fgLotes, 'Descricao'));
    var sObservacao = '';
    var strPars;

    strPars = '?nProprietarioID=' + escape(nProprietarioID);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&dtFim=' + escape(dtFim);
    strPars += '&sDescricao=' + escape(sDescricao);
    strPars += '&sObservacao=' + escape(sObservacao);

    setConnection(dsoIncluirLote);
    dsoIncluirLote.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/IncluirLote.aspx' + strPars;
    dsoIncluirLote.ondatasetcomplete = IncluirLote_DSC;
    dsoIncluirLote.Refresh();
}

function IncluirLote_DSC() {
    var nLoteID;

    if ((!dsoIncluirLote.recordset.BOF) && (!dsoIncluirLote.recordset.EOF)) {
        nLoteID = dsoIncluirLote.recordset['LoteID'].value;

        AssociarItemLote(nLoteID, true);
    }
}

function AssociarItemLote(nRegistroID, bNovoLote) {
    var nPedItemID;
    var nLoteID;
    var nQuantidade;
    var strPars = '';
    var bCriarProduto;
    var nProdutoID;

    bCriarProduto = bNovoLote;

    if (!VerificaQuantidadeAssociar()) {
        lockControlsInModalWin(false);

        return null;
    }

    if (glb_aGridOk[0] == 'fgLotes') {
        nLoteID = nRegistroID;

        for (var i = 1; (i < fgItens.Rows) ; i++) {
            if (fgItens.textMatrix(i, getColIndexByColKey(fgItens, 'Associar')) != 0) {
                nPedItemID = fgItens.textMatrix(i, getColIndexByColKey(fgItens, 'PedItemID'));
                nQuantidade = fgItens.textMatrix(i, getColIndexByColKey(fgItens, 'QtdAssociar'));
                nProdutoID = fgItens.ValueMatrix(i, getColIndexByColKey(fgItens, 'ProdutoID*'));
                sObservacao = '';

                if (!bNovoLote)
                    bCriarProduto = criaProduto(nLoteID, nProdutoID);

                if (strPars == '')
                    strPars = '?nLoteID=' + escape(nLoteID);
                else
                    strPars += '&nLoteID=' + escape(nLoteID);

                strPars += '&nPedItemID=' + escape(nPedItemID);
                strPars += '&nQuantidade=' + escape(nQuantidade);
                strPars += '&bCriarProduto=' + escape(bCriarProduto);

                if (!bNovoLote)
                    bCriarProduto = false;
            }
        }
    }
    else if (glb_aGridOk[0] == 'fgItens') {
        nPedItemID = nRegistroID;
        nProdutoID = fgItens.ValueMatrix(fgItens.Row, getColIndexByColKey(fgItens, 'ProdutoID*'));

        for (var i = 1; (i < fgLotes.Rows) ; i++) {
            if (fgLotes.textMatrix(i, getColIndexByColKey(fgLotes, 'Associar')) != 0) {
                nLoteID = fgLotes.textMatrix(i, getColIndexByColKey(fgLotes, 'LoteID*'));
                nQuantidade = fgLotes.textMatrix(i, getColIndexByColKey(fgLotes, 'QtdAssociar'));
                sObservacao = '';

                if (!bNovoLote)
                    bCriarProduto = criaProduto(nLoteID, nProdutoID);

                if (strPars == '')
                    strPars = '?nPedItemID=' + escape(nPedItemID);
                else
                    strPars += '&nPedItemID=' + escape(nPedItemID);

                strPars += '&nLoteID=' + escape(nLoteID);
                strPars += '&nQuantidade=' + escape(nQuantidade);
                strPars += '&bCriarProduto=' + escape(bCriarProduto);

                if (!bNovoLote)
                    bCriarProduto = false;
            }
        }
    }

    if (strPars != '') {
        dsoAssociarItemLote.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/AssociarItemLote.aspx' + strPars;
        dsoAssociarItemLote.ondatasetcomplete = AssociarItemLote_DSC;
        dsoAssociarItemLote.Refresh();
    }
    else {
        window.top.overflyGen.Alert('N�o foram encontrados itens para serem associados ao Lote.');

        lockControlsInModalWin(false);
    }

}

function AssociarItemLote_DSC() {
    glb_ColGrid = null;
    glb_aGridOk = null;
    glbGridOk = false;

    matriz();
    getDataInServerItens();
    getDataInServerLotes();
}

function VerificaQuantidadeAssociar() {
    var nQuantidade = 0;
    var nSaldoAssociado = 0;
    var bAssociarOK = true;
    var bAssociar = false;
    var nLimiteQuantidade = 0;
    var nQuantidadeAssociar = 0;
    var fg;

    if (glb_aGridOk != null) {
        if (glb_aGridOk[0] == 'fgItens') {
            fg = fgLotes;
            nLimiteQuantidade = fgItens.TextMatrix(fgItens.Row, getColIndexByColKey(fgItens, 'Saldo*'));
        }
        else if (glb_aGridOk[0] == 'fgLotes')
            fg = fgItens;
    }

    for (var i = 1; (i < fg.Rows) ; i++) {
        if (fg.textMatrix(i, getColIndexByColKey(fg, 'Associar')) != 0) {
            bAssociar = true;

            nQuantidade = fg.ValueMatrix(i, getColIndexByColKey(fg, 'QtdAssociar'));

            nSaldoAssociado = fg.ValueMatrix(i, getColIndexByColKey(fg, 'SaldoAssociado'));

            nQuantidadeAssociar = nQuantidadeAssociar + nQuantidade - nSaldoAssociado;


            if (fg.id == 'fgItens') {
                if (fgLotes.ValueMatrix(fgLotes.Row, getColIndexByColKey(fgLotes, 'LoteID*')) != 0)
                    nLimiteQuantidade = nLimiteQuantidade + fg.TextMatrix(i, getColIndexByColKey(fg, 'SaldoLote*'));
                else
                    nLimiteQuantidade = nQuantidadeAssociar;
            }

            if ((nQuantidade <= 0) && (nSaldoAssociado == 0))
                bAssociarOK = false;
        }
    }

    if ((!bAssociarOK) || (!bAssociar)) {
        window.top.overflyGen.Alert('N�o existem quantidades a serem associadas');

        return false;
    }
    else if (nLimiteQuantidade < nQuantidadeAssociar) {
        window.top.overflyGen.Alert('A quantidade a Associar � maior do que o saldo disponivel.');

        return false;
    }
    else
        return true;
}

function getRegitro() {
    if (glb_aGridOk[0] == 'fgLotes')
        return fgLotes.textMatrix(fgLotes.row, getColIndexByColKey(fgLotes, 'LoteID*'));
    else if (glb_aGridOk[0] == 'fgItens')
        return fgItens.textMatrix(fgItens.row, getColIndexByColKey(fgItens, 'PedItemID'));
}

function criaProduto(nLoteID, nProdutoID) {
    var bReturn = false;

    dsoMatriz.recordset.setFilter('LoteID = ' + nLoteID + ' AND ProdutoID = ' + nProdutoID);

    if ((dsoMatriz.recordset.BOF) && (dsoMatriz.recordset.EOF))
        bReturn = true;

    dsoMatriz.recordset.setFilter('');

    return bReturn;
}

function matriz() {
    setConnection(dsoMatriz);

    dsoMatriz.SQL = 'SELECT DISTINCT a.LoteID, b.ProdutoID AS ProdutoID, ISNULL(d.PedItemID, 0) AS PedItemID, ' +
                            '(SELECT SUM(aa.Quantidade) FROM Lotes_Produtos aa WITH(NOLOCK) WHERE (aa.LoteID = a.LoteID) AND (aa.ProdutoID = b.ProdutoID)) AS QuantidadeLote, ' +
                            'dbo.fn_Lotes_ProdutosQuantidades(a.LoteID, c.ProdutoID, ' + glb_nTipoSaldoLote + ', NULL) AS SaldoLote, ' +
                            'c.Quantidade AS QuantidadeItem,c.ValorUnitario as ValorUnitario ,dbo.fn_Lotes_ProdutosQuantidades(A.LoteID,b.ProdutoID,6, d.PedItemID) AS SaldoItem, ' +
                            'b.LotProdutoID, d.LotPedItemID, ' +
                            '(CASE WHEN ((d.PedItemID IS NOT NULL) AND (e.TransacaoID <> 115)) THEN c.PedidoID ELSE NULL END) AS PedidoID, ' +
                            'ISNULL(d.Quantidade, 0) AS SaldoAssociado ' +
                        'FROM Lotes a WITH(NOLOCK) ' +
                            'INNER JOIN Lotes_Produtos b WITH(NOLOCK) ON (b.LoteID = a.LoteID)' +
                            'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.ProdutoID = b.ProdutoID) ' +
                            'LEFT OUTER JOIN Lotes_Itens d WITH(NOLOCK) ON (d.LoteID = A.LoteID) AND (d.PedItemID = c.PedItemID) ' +
                            'INNER JOIN Pedidos e WITH(NOLOCK) ON (e.PedidoID = c.PedidoID) ' +
                        'WHERE c.PedidoID =' + glb_PedidoID + ' ' +
                        'ORDER BY b.ProdutoID';

    dsoMatriz.ondatasetcomplete = matriz_DSC;
    dsoMatriz.Refresh();
}

function matriz_DSC() {
    if (glb_Inicio)
        window_onload_DSC();
}