/********************************************************************
modaldespacho.js

Library javascript para o modaldespacho.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var dsoGrid = new CDatatransport('dsoGrid');
var dsoMinuta = new CDatatransport('dsoMinuta');
var dsoGrava = new CDatatransport('dsoGrava');
var dsoCombo = new CDatatransport('dsoCombo');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modaldespachoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modaldespachoDblClick(grid, Row, Col)
js_modaldespachoKeyPress(KeyAscii)
js_modaldespacho_ValidateEdit()
js_modaldespacho_BeforeEdit(grid, row, col)
js_modaldespacho_AfterEdit(Row, Col)
js_fg_AfterRowColmodaldespacho (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modaldespachoBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 50;
    modalFrame.style.left = 0;

    // desabilita o botao OK
    btnOK.disabled = true;
    btnFillGrid.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblDespacho', 'chkDespacho', 3, 1, -10, -10],
						  ['lblEstados', 'chkEstados', 3, 1, -13],
						  ['lblTransportadora', 'chkTransportadora', 3, 1, -13],
						  ['lblDepositoID', 'selDepositoID', 11, 1],
						  ['lblPessoaID', 'selPessoaID', 19, 2, -10],
						  ['lblPortador', 'txtPortador', 16, 2],
						  ['lblDocumentoEstadualPortador', 'txtDocumentoEstadualPortador', 13, 2],
						  ['lblDocumentoFederalPortador', 'txtDocumentoFederalPortador', 13, 2],
                            ['lblFiltro', 'txtFiltro', 35, 2],
						  ['lblConhecimento', 'txtConhecimento', 9, 2]], null, null, true);

    chkDespacho.onclick = chkDespacho_onclick;
    chkEstados.onclick = chkEstados_onclick;

    chkTransportadora.onclick = chkTransportadora_onclick;
    selPessoaID.onchange = selPessoaID_onchange;
    selDepositoID.onchange = selDepositoID_onchange;
    adjustLabelsCombos();

    txtDocumentoEstadualPortador.onfocus = selFieldContent;
    txtDocumentoEstadualPortador.maxLength = 15;
    txtDocumentoEstadualPortador.onkeypress = txtField_onKeyPress;

    txtDocumentoFederalPortador.onfocus = selFieldContent;
    txtDocumentoFederalPortador.maxLength = 15;
    txtDocumentoFederalPortador.onkeypress = txtField_onKeyPress;

    txtFiltro.onfocus = selFieldContent;
    txtFiltro.onkeypress = txtField_onKeyPress;

    txtPortador.onfocus = selFieldContent;
    txtPortador.maxLength = 20;
    txtPortador.onkeypress = txtField_onKeyPress;

    txtConhecimento.onfocus = selFieldContent;
    txtConhecimento.maxLength = 10;
    txtConhecimento.onkeypress = txtField_onKeyPress;

    // Ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        btnFillGrid.offsetTop + btnFillGrid.offsetHeight + 2;
        width = txtDocumentoEstadualPortador.offsetLeft + txtDocumentoEstadualPortador.offsetWidth + ELEM_GAP;
        height = txtConhecimento.offsetTop + txtConhecimento.offsetHeight;
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // Reposiciona botao OK
    with (btnOK) {
        style.top = divControls.offsetTop + txtConhecimento.offsetTop;
        style.width = parseInt(btnOK.currentStyle.width, 10) - 18;
        style.left = modWidth - parseInt(btnOK.currentStyle.width) - 16;
        value = 'Ativar';
        title = 'Ativar Financeiros';
    }

    with (btnFillGrid) {
        style.width = btnOK.currentStyle.width;
        style.height = btnOK.currentStyle.height;
        style.top = btnOK.currentStyle.top;
        style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnFillGrid.currentStyle.width, 10) - 4;
    }

    // Esconde botao cancel
    with (btnCanc) {
        style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtField_onKeyPress() {
    if (event.keyCode == 13) {
        fillGridData();
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        saveDataInGrid();
    }
    else if (controlID == 'btnFillGrid') {
        listar();
    }
    // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
- null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar interface aqui
    setupBtnsFromGridState();

    showExtFrame(window, true);

    selPessoaID.focus();

    chkDespacho_onclick();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState() {
    var i;
    var bHasRowsInGrid = fg.Rows > 1;

    btnOK.disabled = true;
    btnFillGrid.disabled = false;

    if ((chkDespacho.checked) && (selPessoaID.value <= 0))
        return null;

    //DireitoEspecifico
    //Pedidos->Saidas->Modal Despacho (B6)
    //S� libera bot�o despacho se o usuario tem alguma perfil que pode despachar.
    else if ((chkDespacho.checked) && (!glb_bDireitoDepacho))
        return null;

    if (bHasRowsInGrid) {
        for (i = 1; i < fg.Rows; i++) {
            if (getCellValueByColKey(fg, 'OK', i) != 0) {
                btnOK.disabled = false;
                break;
            }
        }
    }
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    lockControlsInModalWin(true);

    var nPessoaID = selPessoaID.value;
    var sPortador = trimStr(txtPortador.value);
    var sDeposito = '';
    var sDocumentoEstadualPortador = trimStr(txtDocumentoEstadualPortador.value);
    var sDocumentoFederalPortador = trimStr(txtDocumentoFederalPortador.value);
    var sFiltro = '';
    var sTop = '';
    var sOrderBy = '';

    if (txtFiltro.value != '') {
        sFiltro += ' AND (' + txtFiltro.value + ') ';
    }  


    if (nPessoaID < 1)
        sTop = ' TOP 50 ';

    if (chkDespacho.checked) {
        sFiltro += ' AND a.dtChegadaPortador IS NOT NULL ';

        if (chkEstados.checked) {
            sFiltro += ' AND a.EstadoID IN (21,22,23,24,25,26,29) ';
            sTop = ' TOP 50 ';
        }
        else
            sFiltro += ' AND a.EstadoID = (29) ';
    }
    else {
        sFiltro += ' AND a.dtChegadaPortador IS NULL ';

        if (chkEstados.checked) {
            sFiltro += ' AND a.EstadoID IN (21,22,23,24,25,26,29) ';
            sTop = ' TOP 50 ';
        }
        else
            sFiltro += ' AND a.EstadoID IN (24,25,26,29) ';
    }

    if (chkTransportadora.checked) {
        sFiltro += ' AND a.ParceiroID <> a.TransportadoraID ';
        sOrderBy += ' Transportadora, c.Fantasia, a.PedidoID ';
    }
    else {
        sFiltro += ' AND (a.ParceiroID = a.TransportadoraID OR a.TransportadoraID IS NULL) ';
        sOrderBy += ' c.Fantasia, a.PedidoID ';
    }

    if (selDepositoID.value != 0)
        sDeposito = ' AND a.DepositoID = ' + selDepositoID.value + ' ';

    if (nPessoaID > 0) {
        if (chkTransportadora.checked)
            sFiltro += ' AND a.TransportadoraID = ' + nPessoaID + ' ';
        else
            sFiltro += ' AND a.PessoaID = ' + nPessoaID + ' ';
    }

    if (sPortador != '')
        sFiltro += ' AND a.Portador LIKE ' + '\'' + '%' + sPortador + '%' + '\'' + ' ';

    if (sDocumentoEstadualPortador != '')
        sFiltro += ' AND a.DocumentoPortador LIKE ' + '\'' + '%' + sDocumentoEstadualPortador + '%' + '\'' + ' ';

    if (sDocumentoFederalPortador != '')
        sFiltro += ' AND a.DocumentoFederalPortador LIKE ' + '\'' + '%' + sDocumentoFederalPortador + '%' + '\'' + ' ';

    // zera o grid
    fg.Rows = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT ' + sTop + 'a.PedidoID, g.VinculadoID, b.RecursoAbreviado AS Estado, CONVERT(BIT, 0) AS OK, a.dtPedido, c.Fantasia AS Pessoa, ' +
			'd.OperacaoID AS Codigo, e.NotaFiscal, e.dtNotaFiscal, a.ValorTotalPedido, ' +
            '(CASE a.TransportadoraID WHEN a.ParceiroID THEN ' + '\'' + 'O mesmo' + '\'' + ' ' +
            'WHEN a.EmpresaID THEN ' + '\'' + 'Nosso carro' + '\'' + ' ' +
            'ELSE (SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID=a.TransportadoraID) END) AS Transportadora, ' +
            'dbo.fn_Pessoa_Fantasia(a.DepositoID, 0) AS Deposito, a.TotalPesoBruto, a.NumeroVolumes, dbo.fn_Pedido_Cubagem(a.PedidoID) AS Cubagem, a.Observacao, ' +
            'f.Fantasia AS Colaborador, a.dtPrevisaoEntrega, a.dtChegadaPortador, ' +
			'a.Portador, a.DocumentoPortador, a.DocumentoFederalPortador, a.Conhecimento, ' +
			'dbo.fn_Pedido_Cor(a.PedidoID, ' + glb_nUserID + ', GETDATE(), 1) AS CorPedido, ' +
			'dbo.fn_Pedido_Cor(a.PedidoID, ' + glb_nUserID + ', GETDATE(), 2) AS CorEstado, ' +
			'dbo.fn_Pedido_Cor(a.PedidoID, ' + glb_nUserID + ', GETDATE(), 3) AS CorPessoa, ' +
			'dbo.fn_Pedido_Cor(a.PedidoID, ' + glb_nUserID + ', GETDATE(), 5) AS CorTransportadora, ' +
			'dbo.fn_Pedido_Cor(a.PedidoID, ' + glb_nUserID + ', GETDATE(), 6) AS CorPesoBruto, ' +
			'dbo.fn_Pedido_Cor(a.PedidoID, ' + glb_nUserID + ', GETDATE(), 7) AS CorVolumes, ' +
			'dbo.fn_Pedido_Cor(a.PedidoID, ' + glb_nUserID + ', GETDATE(), 8) AS CorColaborador, ' +
			'dbo.fn_Pedido_Cor(a.PedidoID, ' + glb_nUserID + ', GETDATE(), 9) AS CorPrevisaoEntrega, ' +
			'dbo.fn_Pedido_Cor(a.PedidoID, ' + glb_nUserID + ', GETDATE(), 11) AS CorVinculado ' +
		'FROM Pedidos a WITH(NOLOCK) ' +
					'INNER JOIN Recursos b WITH(NOLOCK) ON a.EstadoID = b.RecursoID ' +
					'INNER JOIN Pessoas c WITH(NOLOCK) ON a.PessoaID = c.PessoaID ' +
					'INNER JOIN Operacoes d WITH(NOLOCK) ON a.TransacaoID = d.OperacaoID ' +
					'LEFT JOIN NotasFiscais e WITH(NOLOCK) ON a.PedidoID = e.PedidoID AND e.EstadoID=67 ' +
					'INNER JOIN Pessoas f WITH(NOLOCK) ON a.ProprietarioID = f.PessoaID ' +
					'LEFT JOIN Pedidos_Vinculados g WITH(NOLOCK) ON (g.PedidoID = a.PedidoID) ' +
		'WHERE (a.EmpresaID = ' + glb_nEmpresaID + ' AND a.TipoPedidoID = 602 ' + sDeposito + sFiltro + ') ' +
		'ORDER BY ' + sOrderBy;

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var i, lastRow, sColuna1, sColuna2, sDado1, sDado2;

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    if (chkTransportadora.checked) {
        sColuna1 = 'Transportadora';
        sColuna2 = 'Pessoa';
        sDado1 = 'Transportadora*';
        sDado2 = 'Pessoa*';
    }
    else {
        sColuna1 = 'Pessoa';
        sColuna2 = 'Transportadora';
        sDado1 = 'Pessoa*';
        sDado2 = 'Transportadora*';
    }

    var aHoldCols = new Array();

    if (glb_nEmpresaID != 7)
        aHoldCols = [22, 23, 24, 25, 26, 27, 28, 29, 30];
    else
        aHoldCols = [1, 22, 23, 24, 25, 26, 27, 28, 29, 30];

    headerGrid(fg, [sColuna1,
                   'Vinc',
				   sColuna2,
				   'Deposito',
				   'OK',
				   'Pedido',
				   'E',
				   'Data',
				   'Op',
				   'NF',
				   'Data NF',
				   'Total',
				   'Peso',
				   'Vol',
				   'Cubagem',
				   'Observa��o',
				   'Colaborador',
				   'Previs�o Entrega',
				   'Chegada Portador',
				   'Portador',
				   'Doc Estadual',
				   'Doc Federal',
				   'Conhecimento',
				   'CorPedido',
				   'CorEstado',
				   'CorPessoa',
				   'CorTransportadora',
				   'CorPesoBruto',
				   'CorVolumes',
				   'CorColaborador',
				   'CorPrevisaoEntrega',
				   'CorVinculado'], aHoldCols);

    fillGridMask(fg, dsoGrid, [sDado1,
                             'VinculadoID*',
							 sDado2,
							 'Deposito*',
							 'OK',
							 'PedidoID*',
							 'Estado*',
							 'dtPedido*',
							 'Codigo*',
							 'NotaFiscal*',
							 'dtNotaFiscal*',
							 'ValorTotalPedido*',
							 'TotalPesoBruto*',
							 'NumeroVolumes*',
							 'Cubagem*',
							 'Observacao*',
							 'Colaborador*',
							 'dtPrevisaoEntrega*',
							 'dtChegadaPortador*',
							 'Portador*',
							 'DocumentoPortador*',
							 'DocumentoFederalPortador*',
							 'Conhecimento*',
							 'CorPedido*',
							 'CorEstado*',
							 'CorPessoa*',
							 'CorTransportadora*',
							 'CorPesoBruto*',
							 'CorVolumes*',
							 'CorColaborador*',
							 'CorPrevisaoEntrega*',
							 'CorVinculado*'],
							 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
							 ['', '', '', '', '', '', '', dTFormat, '', '', dTFormat, '###,###,###,###.00', '####.00', '####', '#.00000000', '', '', dTFormat + ' hh:mm', dTFormat + ' hh:mm', '', '', '', '', '', '', '', '', '', '', '', '', '']);

    var nColCorPedido = getColIndexByColKey(fg, 'CorPedido*');
    var nColCorEstado = getColIndexByColKey(fg, 'CorEstado*');
    var nColCorPessoa = getColIndexByColKey(fg, 'CorPessoa*');
    var nColCorTransportadora = getColIndexByColKey(fg, 'CorTransportadora*');
    var nColCorPesoBruto = getColIndexByColKey(fg, 'CorPesoBruto*');
    var nColCorVolumes = getColIndexByColKey(fg, 'CorVolumes*');
    var nColCorColaborador = getColIndexByColKey(fg, 'CorColaborador*');
    var nColCorPrevisaoEntrega = getColIndexByColKey(fg, 'CorPrevisaoEntrega*');
    var nColCorVinculado = getColIndexByColKey(fg, 'CorVinculado*');

    fg.Redraw = 0;

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C']]);

    totalLine();

    alignColsInGrid(fg, [5, 9, 11, 12, 13]);

    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.FrozenCols = 6;

    paintCellsSpecialyReadOnly();

    lastRow = fg.Row;
    fg.FillStyle = 1;
    for (i = 1; i < fg.Rows; i++) {
        // Pedido
        if (fg.TextMatrix(i, nColCorPedido) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'PedidoID*'), i, getColIndexByColKey(fg, 'PedidoID*')) = eval(fg.TextMatrix(i, nColCorPedido));

        // Estado
        if (fg.TextMatrix(i, nColCorEstado) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'Estado*'), i, getColIndexByColKey(fg, 'Estado*')) = eval(fg.TextMatrix(i, nColCorEstado));

        // Pessoa
        if (fg.TextMatrix(i, nColCorPessoa) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'Pessoa*'), i, getColIndexByColKey(fg, 'Pessoa*')) = eval(fg.TextMatrix(i, nColCorPessoa));

        // Transportadora
        if (fg.TextMatrix(i, nColCorTransportadora) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'Transportadora*'), i, getColIndexByColKey(fg, 'Transportadora*')) = eval(fg.TextMatrix(i, nColCorTransportadora));

        // PesoBruto
        if (fg.TextMatrix(i, nColCorPesoBruto) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'TotalPesoBruto*'), i, getColIndexByColKey(fg, 'TotalPesoBruto*')) = eval(fg.TextMatrix(i, nColCorPesoBruto));

        // Volumes
        if (fg.TextMatrix(i, nColCorVolumes) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'NumeroVolumes*'), i, getColIndexByColKey(fg, 'NumeroVolumes*')) = eval(fg.TextMatrix(i, nColCorVolumes));

        // Colaborador
        if (fg.TextMatrix(i, nColCorColaborador) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'Colaborador*'), i, getColIndexByColKey(fg, 'Colaborador*')) = eval(fg.TextMatrix(i, nColCorColaborador));

        // PrevisaoEntrega
        if (fg.TextMatrix(i, nColCorPrevisaoEntrega) != '')
            fg.Cell(6, i, getColIndexByColKey(fg, 'dtPrevisaoEntrega*'), i, getColIndexByColKey(fg, 'dtPrevisaoEntrega*')) = eval(fg.TextMatrix(i, nColCorPrevisaoEntrega));

        // Vinculado
        if (glb_nEmpresaID != 7)
            if (fg.TextMatrix(i, nColCorVinculado) != '')
                fg.Cell(6, i, getColIndexByColKey(fg, 'VinculadoID*'), i, getColIndexByColKey(fg, 'VinculadoID*')) = eval(fg.TextMatrix(i, nColCorVinculado));
    }

    fg.FillStyle = 0;
    if (lastRow >= 1)
        fg.Row = lastRow;

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        fg.Editable = true;
        window.focus();
        fg.LeftCol = 5;
        fg.focus();
        fg.Col = getColIndexByColKey(fg, 'OK');
    }
    else {
        ;
    }

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.Redraw = 2;

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var sErro = '';
    var sConfirm = '';

    var nDespacho = (chkDespacho.checked ? 1 : 0);
    var sPortador = trimStr(txtPortador.value);
    var nTransportadora = (chkTransportadora.checked ? 1 : 0);
    var sDocumentoEstadualPortador = trimStr(txtDocumentoEstadualPortador.value);
    var sDocumentoFederalPortador = trimStr(txtDocumentoFederalPortador.value);
    var sConhecimento = trimStr(txtConhecimento.value);
    var sPedidoID = '';
    var sPortadorPedido = '';
    var sDocumentoEstadualPortadorPedido = '';
    var sDocumentoFederalPortadorPedido = '';
    var nMinutaID;
    var nNF = 0;
    var nVol = 0;
    var nValor = 0.00;
    var nPeso = 0.00;
    var _retConf;
    var fistTime = true;

    if (nDespacho == 1) {
        if (sPortador == '')
            sErro += 'Preencher portador.' + String.fromCharCode(13, 10);
        if (sDocumentoEstadualPortador == '')
            sErro += 'Preencher documento estadual do portador.' + String.fromCharCode(13, 10);
        if (sDocumentoFederalPortador == '')
            sErro += 'Preencher documento federal do portador.' + String.fromCharCode(13, 10);
    }

    if (sErro != '') {
        if (window.top.overflyGen.Alert(sErro) == 0)
            return null;
    }
    else {
        strPars = '?nUserID=' + escape(glb_nUserID);
        strPars += '&nDespacho=' + escape(nDespacho);
        strPars += '&nTransportadora=' + escape(nTransportadora);

        if (nDespacho == 1) {
            if (chkTransportadora.checked) {
                nNF = fg.ValueMatrix(1, getColIndexByColKey(fg, 'NotaFiscal*'));
                nVol = fg.ValueMatrix(1, getColIndexByColKey(fg, 'NumeroVolumes*'));
                nValor = fg.ValueMatrix(1, getColIndexByColKey(fg, 'ValorTotalPedido*'));
                nPeso = fg.ValueMatrix(1, getColIndexByColKey(fg, 'TotalPesoBruto*'));

                _retConf = window.top.overflyGen.Confirm('Gerar minuta com estes totais? \n ' + 'Notas Fiscais: ' + nNF +
						'    Volumes: ' + nVol + '\n Valor: ' + nValor + '    Peso Kg: ' + nPeso);

                if (_retConf != 1)
                    return null;
            }

            strPars += '&sPortador=' + escape(sPortador);
            strPars += '&sDocumentoEstadualPortador=' + escape(sDocumentoEstadualPortador);
            strPars += '&sDocumentoFederalPortador=' + escape(sDocumentoFederalPortador);
            strPars += '&sConhecimento=' + escape(sConhecimento);
        }

        for (i = 2; i < fg.Rows; i++) {
            if (getCellValueByColKey(fg, 'OK', i) != 0) {
                if ((nDespacho == 1) && (!chkTransportadora.checked)) {
                    sPedidoID = getCellValueByColKey(fg, 'PedidoID*', i);
                    sPortadorPedido = getCellValueByColKey(fg, 'Portador*', i);
                    sDocumentoEstadualPortadorPedido = getCellValueByColKey(fg, 'DocumentoPortador*', i);
                    sDocumentoFederalPortadorPedido = getCellValueByColKey(fg, 'DocumentoFederalPortador*', i);

                    if (sPortadorPedido != sPortador)
                        sConfirm += 'Pedido ' + sPedidoID + ' com portador diferente.' + String.fromCharCode(13, 10);
                    if (sDocumentoEstadualPortadorPedido != sDocumentoEstadualPortador)
                        sConfirm += 'Pedido ' + sPedidoID + ' com documento estadual do portador diferente.' + String.fromCharCode(13, 10);
                    if (sDocumentoFederalPortadorPedido != sDocumentoFederalPortador)
                        sConfirm += 'Pedido ' + sPedidoID + ' com documento estadual do portador diferente.' + String.fromCharCode(13, 10);
                }

                if (fistTime) {
                    strPars += '&nPedidoID=' + escape(',' + getCellValueByColKey(fg, 'PedidoID*', i) + ',');
                    fistTime = false;
                }
                else
                    strPars += escape(getCellValueByColKey(fg, 'PedidoID*', i) + ',');
            }
        }

        if (sConfirm != '') {
            _retConf = window.top.overflyGen.Confirm(sConfirm + '\n' + 'Despacha os pedidos assim mesmo?');

            if (_retConf != 1)
                return null;
        }

        lockControlsInModalWin(true);
        dsoGrava.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/despacharpedidos.aspx' + strPars;
        dsoGrava.ondatasetcomplete = saveDataInGrid_DSC;
        dsoGrava.refresh();
    }
    txtPortador.value = '';
    txtDocumentoEstadualPortador.value = '';
    txtDocumentoFederalPortador.value = '';
    txtFiltro.value = '';
    txtConhecimento.value = '';
}

function chkDespacho_onclick() {
    fg.Rows = 1;
    if (chkDespacho.checked) {
        secText('Despachar pedidos', 1);
        btnOK.value = 'Despachar';
        btnOK.title = 'Despachar pedidos';
    }
    else {
        secText('Chegada de portador', 1);
        btnOK.value = 'Portador';
        btnOK.title = 'Portador chegou';
    }

    showhideConhecimento();
    fillComboPessoa();
    setupBtnsFromGridState();
}

function chkEstados_onclick() {
    fg.Rows = 1;
    fillComboPessoa();
    setupBtnsFromGridState();
}

function chkTransportadora_onclick() {
    fg.Rows = 1;
    showhideConhecimento();
    fillComboPessoa();
    setupBtnsFromGridState();
}

function selPessoaID_onchange() {
    txtPortador.value = '';
    txtDocumentoEstadualPortador.value = '';
    txtDocumentoFederalPortador.value = '';
    txtFiltro.value = '';
    txtConhecimento.value = '';

    adjustLabelsCombos();
    fg.Rows = 1;
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

function selDepositoID_onchange() {
    fg.Rows = 1;
    fillComboPessoa();
}

function adjustLabelsCombos() {
    if (chkTransportadora.checked)
        lblPessoaID.innerText = 'Transportadora';
    else
        lblPessoaID.innerText = 'Pessoa';

    setLabelOfControl(lblPessoaID, selPessoaID.value);
}

function showhideConhecimento() {
    if ((chkDespacho.checked) && (chkTransportadora.checked)) {
        lblConhecimento.style.visibility = 'inherit';
        txtConhecimento.style.visibility = 'inherit';
    }
    else {
        lblConhecimento.style.visibility = 'hidden';
        txtConhecimento.style.visibility = 'hidden';
        txtConhecimento.value = '';
    }
}

function listar() {
    fillGridData();
}

/********************************************************************
Solicitar dados do combo ao servidor
********************************************************************/
function fillComboPessoa() {
    lockControlsInModalWin(true);
    var strSQL = '', sFiltro = '';
    var sDeposito = '';

    clearComboEx(['selPessoaID']);

    // parametrizacao do dso dsoCombo
    setConnection(dsoCombo);

    if (chkDespacho.checked) {
        sFiltro += ' AND a.dtChegadaPortador IS NOT NULL ';

        if (chkEstados.checked)
            sFiltro += ' AND a.EstadoID IN (21,22,23,24,25,26,29) ';
        else
            sFiltro += ' AND a.EstadoID IN (29) ';
    }
    else {
        sFiltro += ' AND a.dtChegadaPortador IS NULL ';

        if (chkEstados.checked)
            sFiltro += ' AND a.EstadoID IN (21,22,23,24,25,26,29) ';
        else
            sFiltro += ' AND a.EstadoID IN (24,25,26,29) ';
    }

    if (chkTransportadora.checked)
        sFiltro += ' AND a.ParceiroID <> a.TransportadoraID AND a.TransportadoraID = b.PessoaID ';
    else
        sFiltro += ' AND (a.TransportadoraID IS NULL OR a.ParceiroID = a.TransportadoraID) AND a.PessoaID = b.PessoaID ';

    if (selDepositoID.value != 0)
        sDeposito = ' AND a.DepositoID = ' + selDepositoID.value;

    strSQL = 'SELECT 0 AS PessoaID, SPACE(0) AS Fantasia ' +
		'UNION ALL SELECT DISTINCT b.PessoaID AS PessoaID, b.Fantasia AS Fantasia FROM Pedidos a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
		'WHERE (a.EmpresaID = ' + glb_nEmpresaID + sDeposito +
			' AND a.TipoPedidoID = 602 ' + sFiltro + ') ' +
		'ORDER BY Fantasia';

    dsoCombo.SQL = strSQL;

    dsoCombo.ondatasetcomplete = fillComboPessoa_DSC;
    dsoCombo.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do combo
********************************************************************/
function fillComboPessoa_DSC() {
    while (!dsoCombo.recordset.EOF) {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoCombo.recordset['Fantasia'].value;
        oOption.value = dsoCombo.recordset['PessoaID'].value;

        selPessoaID.add(oOption);
        dsoCombo.recordset.MoveNext();
    }

    adjustLabelsCombos();
    lockControlsInModalWin(false);
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    var sResultado = dsoGrava.recordset.Fields['Resultado'].value;
    var sMinuta = (dsoGrava.recordset.Fields['Minuta'].value == "") ? null : dsoGrava.recordset.Fields['Minuta'].value;
    var _retConf;

    if ((sResultado != null) && ((sResultado != ''))) {
        if (window.top.overflyGen.Alert(sResultado) == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
    if (sMinuta != null) {
        _retConf = window.top.overflyGen.Confirm('Gerada minuta ' + sMinuta + '. \n Deseja imprimir?');

        if (_retConf == 1)
            relatorioMinutaTransporte(sMinuta, 1, 2);
    }

    fg.Rows = 1;
    setupBtnsFromGridState();
    lockControlsInModalWin(false);
    fillComboPessoa();
}

/********************************************************************
Atualuza linha totais quando o bot�o ok � true
********************************************************************/

function totalLine() {
    var nNF = 0;
    var nVol = 0;
    var nValor = 0.00;
    var nPeso = 0.00;
    var nCubagem = 0.00;
    var i = 0;
    var OldStateGrid = fg.Editable;

    fg.Editable = false;

    //if ( (chkDespacho.checked) && (!chkTransportadora.checked) )
    for (i = 2; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0) {
            nNF += 1;
            nVol += fg.ValueMatrix(i, getColIndexByColKey(fg, 'NumeroVolumes*'));
            nValor += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorTotalPedido*'));
            nPeso += fg.ValueMatrix(i, getColIndexByColKey(fg, 'TotalPesoBruto*'));
            nCubagem += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Cubagem*'));
        }
    }

    if (fg.Rows > 2) {
        fg.TextMatrix(1, getColIndexByColKey(fg, 'NotaFiscal*')) = nNF;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'NumeroVolumes*')) = nVol;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorTotalPedido*')) = nValor;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'TotalPesoBruto*')) = nPeso;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Cubagem*')) = nCubagem;
    }
    fg.Editable = OldStateGrid;
}

/********************************************************************
Pega minutaid no banco
********************************************************************/
/*
function getMinuta()
{
setConnection(dsoMinuta);
dsoMinuta.SQL = 'EXEC sp_Minuta_ID 1';
dsoMinuta.ondatasetcomplete = getMinuta_DSC;
dsoMinuta.refresh();	
}

function getMinuta_DSC()
{
var nMinutaID=dsoMinuta.recordset['MinutaID'].value;
	
if (nMinutaID <= 0)
return false;
}
*/
/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly() {
    paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modaldespachoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modaldespachoDblClick(grid, Row, Col) {
    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if (getColIndexByColKey(grid, 'OK') != Col)
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);

    glb_PassedOneInDblClick = true;

    // limpa coluna se tem um check box checado
    for (i = 2; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 2; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    totalLine();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaldespachoKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaldespacho_ValidateEdit() {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaldespacho_BeforeEdit(grid, row, col) {
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if (fldDet == null)   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ((fldDet[0] == 200) || (fldDet[0] == 129))  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaldespacho_AfterEdit(Row, Col) {
    setupBtnsFromGridState();
    totalLine();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
grid
OldRow
OldCol
NewRow
NewCol
********************************************************************/
function js_fg_AfterRowColmodaldespacho(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return true;

    setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************