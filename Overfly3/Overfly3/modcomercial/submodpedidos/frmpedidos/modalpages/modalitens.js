/********************************************************************
modalitens.js

Library javascript para o modalitens.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_CaracteristicasTimerInt = null;
var glb_RecalculaTimer = null;
var glb_nDOS = 0;
var glbDireitoCFOP = 0;
var glb_nPedidoID = 0;
var glb_nPessoaID = 0;

var dsoPedido = new CDatatransport('dsoPedido');
var dsoGrid = new CDatatransport('dsoGrid');
var dsoLkp01 = new CDatatransport('dsoLkp01');
var dsoLkp02 = new CDatatransport('dsoLkp02');
var dsoFinalidade = new CDatatransport('dsoFinalidade');
var dsoCfop = new CDatatransport('dsoCfop');
var dsoUnidade = new CDatatransport('dsoUnidade');
var dsoValidaCfop = new CDatatransport('dsoValidaCfop');
var dsoDireitoCFOP = new CDatatransport('dsoDireitoCFOP');
var dsoValorICMSSTRevenda = new CDatatransport('dsoValorICMSSTRevenda');
var dsoCmbGrid01 = new CDatatransport('dsoCmbGrid01');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalitens.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalitens.ASP

js_fg_modalitensBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalitensDblClick( grid, Row, Col)
js_modalitensKeyPress(KeyAscii)
js_modalitens_AfterRowColChange
js_modalitens_ValidateEdit()
js_modalitens_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // ajusta o body do html
    with (modalitensBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Altera��o de itens', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));

    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 39;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    btnRefresh.style.top = btnOK.offsetTop;
    btnRefresh.style.left = ELEM_GAP;

    btnExcluir.style.top = btnOK.offsetTop;
    btnExcluir.style.left = btnRefresh.offsetLeft + btnRefresh.offsetWidth + ELEM_GAP;

    btnExcluirTodos.style.top = btnOK.offsetTop;
    btnExcluirTodos.style.left = btnExcluir.offsetLeft + btnExcluir.offsetWidth + ELEM_GAP;

    btnGravar.style.top = btnOK.offsetTop;
    btnGravar.style.left = btnExcluirTodos.offsetLeft + btnExcluirTodos.offsetWidth + ELEM_GAP + 620;

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;

    direitoCFOP();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        ;
    }
    // codigo privado desta janela
    else if (controlID == 'btnCanc') {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
    else if (controlID == 'btnRefresh') {
        fillGridData();
    }
    else if (controlID == 'btnGravar') {
        btnCloseWin.disabled = true;

        __INTERFACEISLOCKED = false;

        lockControlsInModalWin(true);
        var nTipoConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TipoConceitoID\'].value');

        if (nTipoConceitoID == 309) {
            var sMsg = '';

            var sConceito = 'NCM';

            if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'DescricaoItem')) <= 0)
                sMsg += '- Descri��o do Item\n';

            sMsg = (sMsg != '' ? 'Selecione o ' + sConceito + ' e informe:\n' : '') + sMsg;

            if (sMsg != '') {
                if (window.top.overflyGen.Alert(sMsg) == 1)
                    lockControlsInModalWin(false);
                return null;
            }
            else
                saveDataInGrid();
        }
        else
            saveDataInGrid();
    }
    else if (controlID == 'btnExcluir') {
        excluirLinha();
    }
    else if (controlID == 'btnExcluirTodos') {
        excluirTodos();
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
- null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    glb_nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'PedidoID\'].value');
    glb_nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'PessoaID\'].value');

    // @@ atualizar interface aqui
    setupBtnsFromGridState();

    // mostra a janela modal
    fillGridData();

}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState() {
    var bHasRowsInGrid = fg.Rows > 1;

    btnOK.disabled = !(bHasRowsInGrid && glb_dataGridWasChanged);
    btnRefresh.disabled = false;
    btnGravar.disabled = !glb_dataGridWasChanged;

    if (fg.rows <= 1)
        btnGravar.disabled = true;

    //btnExcluir.disabled = !bHasRowsInGrid;
    //btnExcluirTodos.disabled = !bHasRowsInGrid;
    //DireitoEspecifico
    //Deixa um usuario, que n�o pode excluir itens, excluir pela modal itens
    if (!glb_bDireitoAlterar)
        btnGravar.disabled = true;

    if (!glb_bDireitoExcluir) {
        btnExcluir.disabled = true;
        btnExcluirTodos.disabled = true;
    }
}


function direitoCFOP() {
    var nUserPerfil = glb_USERID;

    /*******************************************************
    Perfils que podem alterar finalidade e CFOP no Pedido.
    ********************************************************/
    setConnection(dsoDireitoCFOP);
    dsoDireitoCFOP.SQL = 'SELECT COUNT(1) AS DireitoCFOP ' +
	                        'FROM dbo.RelacoesPesRec_Perfis a WITH(NOLOCK) ' +
	                        'WHERE (a.RelacaoID = ' + nUserPerfil + ') ' +
	                            'AND (PerfilID IN (517,518,522,524, 582, 581, 571, 639, 677, 572, 583, 619, 500, 501, 632, 657, 658, 611, 631, 651))';

    dsoDireitoCFOP.ondatasetcomplete = DireitoCFOP_DSC;
    dsoDireitoCFOP.Refresh();
}

function DireitoCFOP_DSC() {

    glbDireitoCFOP = dsoDireitoCFOP.recordset['DireitoCFOP'].value;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    if (glb_CaracteristicasTimerInt != null) {
        window.clearInterval(glb_CaracteristicasTimerInt);
        glb_CaracteristicasTimerInt = null;
    }

    lockControlsInModalWin(true);

    var nTransacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TransacaoID\'].value');

    // zera o grid
    fg.Rows = 1;

    glb_dataGridWasChanged = false;
    glb_nDOS = 6;

    setConnection(dsoFinalidade);
    dsoFinalidade.SQL = 'SELECT a.ItemID AS FinalidadeID,  a.ItemMasculino AS Finalidade ' +
                                'FROM dbo.TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                                        'INNER JOIN dbo.Pessoas b WITH(NOLOCK) ON a.Filtro LIKE \'%(\' + CONVERT(VARCHAR(16), b.ClassificacaoID) +\')%\' ' +
                                        'INNER JOIN dbo.Pessoas_Enderecos c WITH(NOLOCK) ON (c.PessoaID = b.PessoaID) ' +
                                'WHERE (a.EstadoID = 2 AND a.TipoID = 418) ' +
                                        'AND b.PessoaID = ' + glb_nPessoaID + ' ' +
                                        'AND (a.Filtro LIKE \'%{\' + \'' + nTransacao + '\' + \'}%\') ' +
                                        'AND (((~dbo.fn_Pessoa_Incidencia(' + glb_aEmpresaData[0] + ',-b.PessoaID) = 1) AND ((a.Filtro LIKE \'%<CB1>%\') OR (a.Filtro LIKE \'%<CB@_>%\' ESCAPE \'@\')) AND ((130 = ' + glb_aEmpresaData[1] + 'AND c.PaisID = 130) OR (' + glb_aEmpresaData[1] + ' <> 130))) ' +
			                            'OR ((~dbo.fn_Pessoa_Incidencia(' + glb_aEmpresaData[0] + ',-b.PessoaID) = 0) AND ((a.Filtro LIKE \'%<CB0>%\') OR (a.Filtro LIKE \'%\<CB@_>%\' ESCAPE \'@\')) AND ((130 = ' + glb_aEmpresaData[1] + 'AND c.PaisID = 130) OR (' + glb_aEmpresaData[1] + ' <> 130))) ' +
                                        'OR ((130 = ' + glb_aEmpresaData[1] + 'AND c.PaisID <> 130) AND a.ItemID = 785)) ' +
                                'ORDER BY a.Ordem';

    dsoFinalidade.ondatasetcomplete = fillGridData_DSC;
    dsoFinalidade.Refresh();

    setConnection(dsoCfop);
    dsoCfop.SQL = 'SELECT DISTINCT TOP 10 OperacaoID AS CFOPID, convert(varchar,OperacaoID) + \' - \' +  Operacao AS CFOP  ' +
	                	'FROM dbo.Operacoes a WITH (NOLOCK) ' +
		                    'INNER JOIN dbo.Pedidos_Itens b WITH (NOLOCK) ON (b.PedidoID = ' + glb_nPedidoID + ') ' +
		                    'LEFT OUTER JOIN dbo.TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.Filtro LIKE \'%{\' + CONVERT(VARCHAR(3), a.TransacaoBaixaID) + \'}%\') ' +
	                    'WHERE (a.TipoOperacaoID = 651) AND ' +
			                '(a.EstadoID = 2) AND ' +
			                '(a.Nivel = 3) AND ' +
			                '(a.TransacaoBaixaID = ' + nTransacao + ') AND ' +
			                '((a.PaisID IS NULL) OR (a.PaisID = dbo.fn_Pessoa_Localidade(' + glb_aEmpresaData[0] + ', 1, 1, NULL))) AND ' +
			                '(a.OperacaoID = dbo.fn_EmpresaPessoaTransacao_CFOP(' + glb_aEmpresaData[0] + ', ' + glb_nPessoaID + ', NULL, NULL, NULL, NULL, NULL, ' + nTransacao + ', c.ItemID, b.ProdutoID, a.OperacaoID))';

    dsoCfop.ondatasetcomplete = fillGridData_DSC;
    dsoCfop.Refresh();

    setConnection(dsoUnidade);
    dsoUnidade.SQL =    'SELECT 0 AS UnidadeID, SPACE(0) AS Unidade ' +
                        'UNION ALL ' +
                        'SELECT ItemID AS UnidadeID, ItemMasculino AS Unidade ' +
                            'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                            'WHERE ((TipoID = 103) AND (EstadoID = 2)) ';

    dsoUnidade.ondatasetcomplete = fillGridData_DSC;
    dsoUnidade.Refresh();

    setConnection(dsoLkp01);
    dsoLkp01.SQL = 'SELECT Recursos.RecursoID AS EstadoID, Recursos.RecursoAbreviado ' +
 			        'FROM dbo.Recursos WITH(NOLOCK), dbo.Pedidos_Itens Itens WITH(NOLOCK) ' +
			        'WHERE (Itens.PedidoID=' + glb_nPedidoID + ' AND Itens.EstadoID=Recursos.RecursoID) ' +
				    'ORDER BY RecursoAbreviado';
    dsoLkp01.ondatasetcomplete = fillGridData_DSC;
    dsoLkp01.Refresh();


    setConnection(dsoLkp02);
    dsoLkp02.SQL = 'SELECT dbo.fn_Produto_Descricao2(Produtos.ConceitoID, NULL, 12) AS Produto, Produtos.ConceitoID AS ProdutoID, Produtos.PartNumber, ' +
	                'CONVERT(BIT, dbo.fn_Produto_Similares(Itens.ProdutoID, ' + glb_aEmpresaData[0] + ')) AS Similares ' +
		            'FROM dbo.Pedidos_Itens Itens WITH(NOLOCK), dbo.Conceitos Produtos WITH(NOLOCK) ' +
		            'WHERE (Itens.PedidoID=' + glb_nPedidoID + ' AND Itens.ProdutoID=Produtos.ConceitoID)';

    dsoLkp02.ondatasetcomplete = fillGridData_DSC;
    dsoLkp02.Refresh();

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);
    dsoGrid.SQL = 'SELECT a.*, 0.00 AS ValorTotal FROM dbo.Pedidos_Itens a WITH(NOLOCK) WHERE a.PedidoID = ' + glb_nPedidoID + ' ORDER BY a.Ordem';
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();

    // parametrizacao do dso dsoGrid
    setConnection(dsoCmbGrid01);
    dsoCmbGrid01.SQL = 'SELECT DISTINCT a.ConServicoID AS fldID, dbo.fn_Produto_Descricao2(a.ConServicoID, NULL, 13)  AS fldName ' +
	                        'FROM Conceitos_Servicos a WITH(NOLOCK) ' +
		                        'INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.ProdutoID = a.ConceitoID) ' +
		                        'INNER JOIN Pedidos c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) ' +
	                        'WHERE (b.PedidoID = ' + glb_nPedidoID + ') AND (dbo.fn_Pessoa_Localidade(c.PessoaID, dbo.fn_Localidade_Tipo(a.LocalidadeID), NULL, NULL) = a.LocalidadeID) ' +
                            'ORDER BY fldName';
    dsoCmbGrid01.ondatasetcomplete = fillGridData_DSC;
    dsoCmbGrid01.Refresh();

}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDOS--;

    if (glb_nDOS > 0)
        return;

    showExtFrame(window, true);
    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'Resultado\'].value');

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    aHidenCols = new Array();
    aHidenCols = [];

    /****************************************************************************
    Verifica se usuario logado tem direito de alterar CFOP e Finalidade do pedido
    *****************************************************************************/
    if (glbDireitoCFOP <= 0)
        aHidenCols = [5, 6];

    var nTipoConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TipoConceitoID\'].value');
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'EstadoID\'].value');
    var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TransacaoID\'].value');


    //Esconde - Ordem, PS, ValorTotal, PedItemID
    if (nTipoConceitoID == 303) {
        var sConceito = 'Produto';

        if ((nEstadoID == 25 || nEstadoID == 33) && (nTransacaoID == 215))
        {
            aHidenCols[aHidenCols.length] = 3;
            aHidenCols[aHidenCols.length] = 5;
            aHidenCols[aHidenCols.length] = 6;
            aHidenCols[aHidenCols.length] = 7;
            aHidenCols[aHidenCols.length] = 8;
            aHidenCols[aHidenCols.length] = 9;
            aHidenCols[aHidenCols.length] = 10;
            aHidenCols[aHidenCols.length] = 11;
            aHidenCols[aHidenCols.length] = 12;
            aHidenCols[aHidenCols.length] = 13;
            aHidenCols[aHidenCols.length] = 14;
            aHidenCols[aHidenCols.length] = 15;
            aHidenCols[aHidenCols.length] = 16;
            aHidenCols[aHidenCols.length] = 17;
            aHidenCols[aHidenCols.length] = 18;
            aHidenCols[aHidenCols.length] = 19;
            aHidenCols[aHidenCols.length] = 22;
            aHidenCols[aHidenCols.length] = 23;
        }
        else
        {
            aHidenCols[aHidenCols.length] = 0;
            aHidenCols[aHidenCols.length] = 3;
            aHidenCols[aHidenCols.length] = 8;
            aHidenCols[aHidenCols.length] = 10;
            aHidenCols[aHidenCols.length] = 22;
            aHidenCols[aHidenCols.length] = 23;
        }
    }
    else if (nTipoConceitoID == 309) {
        var sConceito = 'NCM';

        aHidenCols[aHidenCols.length] = 3;
        aHidenCols[aHidenCols.length] = 5;
        aHidenCols[aHidenCols.length] = 6;
        aHidenCols[aHidenCols.length] = 7;
        aHidenCols[aHidenCols.length] = 11;
        aHidenCols[aHidenCols.length] = 12;
        aHidenCols[aHidenCols.length] = 13;
        aHidenCols[aHidenCols.length] = 14;
        aHidenCols[aHidenCols.length] = 17;
        aHidenCols[aHidenCols.length] = 18;
        aHidenCols[aHidenCols.length] = 19;
        aHidenCols[aHidenCols.length] = 20;
        aHidenCols[aHidenCols.length] = 21;
        aHidenCols[aHidenCols.length] = 22;
        aHidenCols[aHidenCols.length] = 23;
    }
    else {
        var sConceito = ' Servi�o';

        aHidenCols[aHidenCols.length] = 4;
        aHidenCols[aHidenCols.length] = 5;
        aHidenCols[aHidenCols.length] = 6;
        aHidenCols[aHidenCols.length] = 7;
        aHidenCols[aHidenCols.length] = 9;
        aHidenCols[aHidenCols.length] = 10;
        aHidenCols[aHidenCols.length] = 11;
        aHidenCols[aHidenCols.length] = 12;
        aHidenCols[aHidenCols.length] = 13;
        aHidenCols[aHidenCols.length] = 14;
        aHidenCols[aHidenCols.length] = 16;
        aHidenCols[aHidenCols.length] = 17;
        aHidenCols[aHidenCols.length] = 18;
        aHidenCols[aHidenCols.length] = 19;
        aHidenCols[aHidenCols.length] = 20;
        aHidenCols[aHidenCols.length] = 21;
        aHidenCols[aHidenCols.length] = 22;
        aHidenCols[aHidenCols.length] = 23;
    }

    //Titulos das colunas do Grid
    headerGrid(fg, ['Ordem', //0
                    'ID', //1
                    'E', //2
                    'Servi�o' + replicate(' ', 50), //3
                    sConceito, //4
                    'Part Number', //5
                    'Finalidade', //6
                    'CFOP', //7
                    'Descri��o do Item', //8
                    'Quant', //9
                    'Unidade', //10
                    'Tabela', //11
                    'Var', //12
                    'Valor Int', //13
                    'Valor Rev', //14
                    'Valor Unit', //15
                    'Valor Total', //16
                    'Total Desconto', //17
                    'Valor CI', //18
                    'Valor CR', //19
                    'In�cio Vig�ncia', //20
                    'Fim Vig�ncia', //21
                    'PS', //22
                    'PedItemID'], aHidenCols); //23

    glb_aCelHint = [[0, 2, 'Estado do Produto'],
                    [0, 13, 'Valor Interno'],
                    [0, 14, 'Valor Revenda'],
                    [0, 15, 'Valor Unitario'],
                    [0, 18, 'Valor Comiss�o Interna'],
                    [0, 19, 'Valor Comiss�o Revenda']];

    //Campos das calunas do grid
    fillGridMask(fg, dsoGrid, ['Ordem*',
                             'ProdutoID*',
                             '^EstadoID^dsoLkp01^EstadoID^RecursoAbreviado*',
                             'ServicoID',
                             '^ProdutoID^dsoLkp02^ProdutoID^Produto*',
                             '^ProdutoID^dsoLkp02^ProdutoID^PartNumber*',
                             'FinalidadeID',
                             'CFOPID',
                             'DescricaoItem',
                             'Quantidade',
                             'UnidadeID',
                             'ValorTabela*',
                             'VariacaoItem',
                             'ValorInterno',
                             'ValorRevenda',
                             'ValorUnitario',
                             '_CALC_Total_11*',
                             'ValorDesconto',
                             'ValorComissaoInterna*',
                             'ValorComissaoRevenda*',
                             'dtVigenciaServicoInicio',
                             'dtVigenciaServicoFim',
                             '^ProdutoID^dsoLkp02^ProdutoID^Similares*',
                             'PedItemID'],
                             ['99', '', '', '', '', '', '', '', '', (nTipoConceitoID == 309 ? '999999.9999' : '99999'), '', '', '999.99', '999999999.9999', '999999999.9999', '999999999.9999', '999999999.99', '9999999999.99', '999999999.99', '999999999.99', '99/99/9999', '99/99/9999', '', ''],
                             ['##', '', '', '', '', '', '', '', '', (nTipoConceitoID == 309 ? '######.0000' : '######'), '', '###,###,##0.00', '##0.00', '###,###,##0.0000', '###.###.##0.00', '###,###,##0.0000', '###,###,##0.00', '#,###,###,##0.00', '###,###,##0.00', '###,###,##0.00', dTFormat, dTFormat, '', '']);

    alignColsInGrid(fg, [getColIndexByColKey(fg, 'Ordem*'),
                            getColIndexByColKey(fg, 'Quantidade'),
                            getColIndexByColKey(fg, 'ValorTabela*'),
                            getColIndexByColKey(fg, 'VariacaoItem'),
                            getColIndexByColKey(fg, 'ValorInterno'),
                            getColIndexByColKey(fg, 'ValorRevenda'),
                            getColIndexByColKey(fg, 'ValorUnitario'),
                            getColIndexByColKey(fg, '_CALC_Total_11*'),
                            getColIndexByColKey(fg, 'ValorDesconto'),
                            getColIndexByColKey(fg, 'ValorComissaoInterna*'),
                            getColIndexByColKey(fg, 'ValorComissaoRevenda*'),
                            getColIndexByColKey(fg, 'Ordem*')]);

    insertcomboData(fg, getColIndexByColKey(fg, 'FinalidadeID'), dsoFinalidade, 'Finalidade', 'FinalidadeID');

    insertcomboData(fg, getColIndexByColKey(fg, 'CFOPID'), dsoCfop, 'CFOP', 'CFOPID');

    insertcomboData(fg, getColIndexByColKey(fg, 'ServicoID'), dsoCmbGrid01, 'fldName', 'fldID');

    insertcomboData(fg, getColIndexByColKey(fg, 'UnidadeID'), dsoUnidade, 'Unidade', 'UnidadeID');

    for (i = 1; i < fg.Rows; i++)
        calcTotItem(i);

    totalLine();

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;

    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if (fg.Rows > 1)
        fg.Col = getColIndexByColKey(fg, 'Quantidade');

    if (fg.Rows > 1)
        fg.Editable = true;

    fg.FrozenCols = 5;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ((fg.Row < 1) && (fg.Rows > 1))
        fg.Row = 1;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
    }

    // ajusta estado dos botoes
    //setupBtnsFromGridState();

    fg.ColWidth(getColIndexByColKey(fg, '^ProdutoID^dsoLkp02^ProdutoID^Similares*')) = 400;
    btnCloseWin.disabled = false;
    lockControlsInModalWin(false);
}

/********************************************************************
Valida CFOP
********************************************************************/
function validaCfop() {
    var nTransacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TransacaoID\'].value');
    var nCfopInvalida = '';
    var i = 2;

    while (i < fg.Rows) {
        var nProdutoID = fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoID*'));
        var nFinalidade = fg.TextMatrix(i, getColIndexByColKey(fg, 'FinalidadeID'));
        var nCFOPID = fg.TextMatrix(i, getColIndexByColKey(fg, 'CFOPID'));

        setConnection(dsoValidaCfop);
        dsoValidaCfop.SQL = 'SELECT ' + nProdutoID + ' AS Produto, ' +
                                'ISNULL(dbo.fn_EmpresaPessoaTransacao_CFOP(' + glb_aEmpresaData[0] + ', ' + glb_nPessoaID + ', NULL, NULL, NULL, NULL, NULL, ' +
                                    nTransacao + ', ' + (nFinalidade != '' ? nFinalidade : 'NULL') + ', ' + nProdutoID + ', ' + nCFOPID + '), 0) AS CFOP ';
        dsoValidaCfop.Refresh();
        if (dsoValidaCfop.recordset['CFOP'].value == 0) {
            if ((nCfopInvalida == '') || (nCfopInvalida == undefined))
                nCfopInvalida = nProdutoID;
            else
                nCfopInvalida += ', ' + nProdutoID;
        }
        i++;
    }

    if ((nCfopInvalida != '') && (nCfopInvalida != undefined)) {
        window.top.overflyGen.Alert('Os produtos ' + nCfopInvalida + ' est�o com CFOP invalida.');
        nCfopInvalida = '';
        return false;
    }
    else
        return true;
}

/********************************************************************
Calcula valor IMCSST da revenda
********************************************************************/
function calculaValorICMSSTRevendaEInterno_AfterEdit(Row) {
    var nTransacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransacaoID' + '\'' + '].value');
    var nPedItemID = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'PedItemID'));
    var nProdutoID = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ProdutoID*'));
    var nValorRevenda = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorRevenda'));
    var nValorInterno = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorInterno'));
    var nFinalidade = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'FinalidadeID'));
    var nCFOPID = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'CFOPID'));

    setConnection(dsoValorICMSSTRevenda);
    dsoValorICMSSTRevenda.SQL =
        'SELECT dbo.fn_Produto_ValorICMSST(a.ProdutoID,' + glb_aEmpresaData[0] + ', ' + glb_nPessoaID + ', NULL,NULL,NULL,NULL,NULL, ' + nValorRevenda + ' * ' +
                '(1 + (dbo.fn_Pedido_Item(a.PedidoID, a.ProdutoID, GETDATE(),' + nFinalidade + ', -6)/100)), NULL, NULL , NULL, ' +
                'dbo.fn_Pedido_Item(' + glb_nPedidoID + ',a.ProdutoID,GETDATE(),' + nFinalidade + ',-6), ' + nFinalidade + ', ' + nCFOPID + ', 0, ' +
                'GETDATE(), 1) AS ValorICMSSTRevenda, ' +
                'dbo.fn_Produto_ValorICMSST(a.ProdutoID,' + glb_aEmpresaData[0] + ', ' + glb_nPessoaID + ', NULL,NULL,NULL,NULL,NULL, ' + nValorInterno + ' * ' +
                '(1 + (dbo.fn_Pedido_Item(a.PedidoID, a.ProdutoID, GETDATE(),' + nFinalidade + ', -6)/100)), NULL, NULL , NULL, ' +
                'dbo.fn_Pedido_Item(' + glb_nPedidoID + ',a.ProdutoID,GETDATE(),' + nFinalidade + ',-6), ' + nFinalidade + ', ' + nCFOPID + ', 0, ' +
                'GETDATE(), 1) AS ValorICMSSTInterno ' +
                'FROM dbo.Pedidos_Itens a WITH(NOLOCK) ' +
            'WHERE a.PedItemID = ' + nPedItemID;


    dsoValorICMSSTRevenda.ondatasetcomplete = calculaValorICMSSTRevendaEInterno_AfterEdit_DSC(Row);
    dsoValorICMSSTRevenda.Refresh();
}
function calculaValorICMSSTRevendaEInterno_AfterEdit_DSC(Row) 
{

    dsoValorICMSSTRevenda.recordset.MoveFirst();
    if (!(dsoValorICMSSTRevenda.recordset.BOF && dsoValorICMSSTRevenda.recordset.EOF)) 
    {
        dsoGrid.recordset['ValorICMSSTRevenda'].value = dsoValorICMSSTRevenda.recordset['ValorICMSSTRevenda'].value;
        dsoGrid.recordset['ValorICMSSTInterno'].value = dsoValorICMSSTRevenda.recordset['ValorICMSSTInterno'].value;
    }

    calcTotItem(Row);
    totalLine();
}



/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid() {
    /*if (!validaCfop())
    return null;*/

    glb_dataGridWasChanged = false;

    var strPars = '?nPedidoID=' + escape(glb_nPedidoID) +
		'&bRecalcula=' + escape(0);

    dsoPedido.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/recalculapedido.aspx' + strPars;
    dsoPedido.ondatasetcomplete = dsoPedido_DSC;
    dsoPedido.Refresh();
}

function dsoPedido_DSC() {
    try {
        dsoGrid.SubmitChanges();
    }
    catch (e) {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887) {
            if (window.top.overflyGen.Alert('Este registro acaba de ser alterado ou removido.') == 0)
                return null;
        }

        fg.Rows = 1;
        lockControlsInModalWin(false);

        glb_CaracteristicasTimerInt = window.setInterval('fillGridData()', 30, 'JavaScript');
        return null;
    }

    dsoGrid.ondatasetcomplete = saveDataInGrid_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    glb_RecalculaTimer = window.setInterval('recalculaPedido()', 30, 'JavaScript');
    return null;
}

function recalculaPedido() {
    if (glb_RecalculaTimer != null) {
        window.clearInterval(glb_RecalculaTimer);
        glb_RecalculaTimer = null;
    }

    var strPars = '?nPedidoID=' + escape(glb_nPedidoID) +
		'&bRecalcula=' + escape(1);

    dsoPedido.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/recalculapedido.aspx' + strPars;
    dsoPedido.ondatasetcomplete = recalculaPedido_DSC;
    dsoPedido.Refresh();
}

function recalculaPedido_DSC() {
    //glb_nDOS = 0;
    fillGridData();
}

function excluirLinha() {
    if (fg.Row > 0) {
        _retMsg = window.top.overflyGen.Confirm("Deseja excluir esta linha?");

        if ((_retMsg == 0) || (_retMsg == 2))
            return null;

        // Altera o campo do dso com o novo valor
        dsoGrid.recordset.MoveFirst();
        dsoGrid.recordset.Find(fg.ColKey(fg.Cols - 1) , fg.TextMatrix(fg.Row, fg.Cols - 1));

        if (!(dsoGrid.recordset.EOF)) {
            fg.RemoveItem(fg.Row);
            dsoGrid.recordset.Delete();
            glb_dataGridWasChanged = true;
            setupBtnsFromGridState();
        }
        saveDataInGrid();
    }
}

function excluirTodos() {
    if (fg.Row > 0) {
        _retMsg = window.top.overflyGen.Confirm("Deseja excluir todas as linhas?");

        if ((_retMsg == 0) || (_retMsg == 2))
            return null;

        // Altera o campo do dso com o novo valor
        dsoGrid.recordset.MoveFirst();

        while (true) {
            dsoGrid.recordset.Delete();

            if (!dsoGrid.recordset.BOF)
                dsoGrid.recordset.MoveFirst();
            else
                break;
        }

        fg.Rows = 1;
        glb_dataGridWasChanged = true;
        setupBtnsFromGridState();
        saveDataInGrid();
    }
}

/********************************************************************
Calcula Total Item
********************************************************************/
function calcTotItem(nLine) {
    var nQtd, nValUnit;
    var lIsEditing = (nLine == null);

    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade');
    var nColValorUnitario = getColIndexByColKey(fg, 'ValorUnitario');
    var nColCalcTotal = getColIndexByColKey(fg, '_CALC_Total_11*');
    var nColValorInterna = getColIndexByColKey(fg, 'ValorComissaoInterna*');
    var nColValorComissao = getColIndexByColKey(fg, 'ValorComissaoRevenda*');

    nLine = (nLine == null ? fg.Row : nLine);

    if ((fg.Col == nColQuantidade) && (lIsEditing))
        nQtd = parseFloat(fg.EditText);
    else
        nQtd = parseFloat(fg.ValueMatrix(nLine, nColQuantidade));

    if ((fg.Col == nColValorUnitario) && (lIsEditing))
        nValUnit = parseFloat(replaceStr(fg.EditText, ',', '.'));
    else
        nValUnit = parseFloat(replaceStr(fg.TextMatrix(nLine, nColValorUnitario), ',', '.'));

    if ((!isNaN(nQtd)) && (!isNaN(nValUnit)))
        fg.TextMatrix(nLine, nColCalcTotal) = roundNumber(nQtd * nValUnit, 2);
    else
        fg.TextMatrix(nLine, nColCalcTotal) = '';
}

function totalLine() {

    var nTipoConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TipoConceitoID\'].value');

    gridHasTotalLine(fg, '', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'Quantidade'), (nTipoConceitoID == 309 ? '######.0000' : '######.###'), 'S'],
                                                            [getColIndexByColKey(fg, '_CALC_Total_11*'), '###,###,###,###.00', 'S'],
                                                            [getColIndexByColKey(fg, 'ValorDesconto'), '###,###,###,###.00', 'S'],
                                                            [getColIndexByColKey(fg, 'ValorComissaoInterna*'), '###,###,###,###.00', 'S'],
                                                            [getColIndexByColKey(fg, 'ValorComissaoRevenda*'), '###,###,###,###.00', 'S']]);
}


// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalitensBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalitensDblClick(grid, Row, Col) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalitensKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalitens_ValidateEdit() {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalitens_AfterEdit(Row, Col) {
    var nTipoConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TipoConceitoID\'].value');

    var nVariacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                'dsoSup01.recordset[' + '\'' + 'Variacao' + '\'' + '].value');
    var nPercentualSUP = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                'dsoSup01.recordset[' + '\'' + 'PercentualSUP' + '\'' + '].value');

    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'Resultado\'].value');

    var nType;

    var nVariacaoItem99999 = 0;

    var nColProdutoID = getColIndexByColKey(fg, 'ProdutoID*');
    var nColValorInterno = getColIndexByColKey(fg, 'ValorInterno');
    var nColValorUnitario = getColIndexByColKey(fg, 'ValorUnitario');
    var nColValorRevenda = getColIndexByColKey(fg, 'ValorRevenda');
    var nColValorTabela = getColIndexByColKey(fg, 'ValorTabela*');
    var nColValorComissaoInterna = getColIndexByColKey(fg, 'ValorComissaoInterna*');
    var nColValorComissaoRevenda = getColIndexByColKey(fg, 'ValorComissaoRevenda*');
    var nColdtVigenciaServicoInicio = getColIndexByColKey(fg, 'dtVigenciaServicoInicio');
    var nColdtVigenciaServicoFim = getColIndexByColKey(fg, 'dtVigenciaServicoFim');
    var vlrFatMax = fg.ValueMatrix(Row, nColValorRevenda);

    if (fg.Editable) {
        // Altera o campo do dso com o novo valor
        dsoGrid.recordset.MoveFirst();

        if (fg.ColKey(Col) == '^ProdutoID^dsoLkp02^ProdutoID^Similares*') {
            if (fg.ValueMatrix(Row, getColIndexByColKey(fg, '^ProdutoID^dsoLkp02^ProdutoID^Similares*')) == 0)
                fg.TextMatrix(Row, getColIndexByColKey(fg, '^ProdutoID^dsoLkp02^ProdutoID^Similares*')) = -1;
            else
                fg.TextMatrix(Row, getColIndexByColKey(fg, '^ProdutoID^dsoLkp02^ProdutoID^Similares*')) = 0;

        }
        else {
            dsoGrid.recordset.Find(fg.ColKey(fg.Cols - 1) , fg.TextMatrix(Row, fg.Cols - 1));

            //Atualiza dso
            if (!(dsoGrid.recordset.EOF)) {
                nType = dsoGrid.recordset[fg.ColKey(Col)].type;

                // Se decimal , numerico , int, bigint ou boolean
                if ((nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20)) {
                    if ((nType == 131) || (nType == 14))
                        fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

                    dsoGrid.recordset[fg.ColKey(Col)].value = fg.ValueMatrix(Row, Col);
                }
                else {
                    if (fg.ColKey(Col) != 'ConServicoID')
                        dsoGrid.recordset[fg.ColKey(Col)].value = fg.TextMatrix(Row, Col);
                    else
                        dsoGrid.recordset[fg.ColKey(Col)].value = fg.ValueMatrix(Row, Col);
                }

                glb_dataGridWasChanged = true;
                setupBtnsFromGridState();
            }

            // Campo Variacao Item
            if (Col == getColIndexByColKey(fg, 'VariacaoItem')) {
                nValorTabela = parseFloat(replaceStr(fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorTabela*')), ',', '.'));
                nVariacaoItem = parseFloat(replaceStr(fg.TextMatrix(Row, getColIndexByColKey(fg, 'VariacaoItem')), ',', '.'));

                if ((!isNaN(nValorTabela)) && (!isNaN(nVariacaoItem)))
                    fg.TextMatrix(Row, nColValorUnitario) = roundNumber(nValorTabela * (1 + (nVariacaoItem / 100)) * (1 + (nVariacao / 100)) * (1 + (nPercentualSUP / 100)), 2);
                else {
                    if (!isNaN(nValorTabela))
                        fg.TextMatrix(Row, nColValorUnitario) = roundNumber(nValorTabela * (1 + (nVariacao / 100)) * (1 + (nPercentualSUP / 100)), 2);
                    else
                        fg.TextMatrix(Row, nColValorUnitario) = '0.00';
                }

                nValorUnitario = fg.ValueMatrix(Row, nColValorUnitario);
                nValorRevenda = fg.ValueMatrix(Row, nColValorRevenda);
                nValorInterno = fg.ValueMatrix(Row, nColValorInterno);

                if ((nValorUnitario < nValorRevenda) || (bResultado == false))
                    fg.TextMatrix(Row, nColValorRevenda) = nValorUnitario;

                if ((nValorUnitario < nValorInterno) || (bResultado == false))
                    fg.TextMatrix(Row, nColValorInterno) = nValorUnitario;
            }
            // Campo valor interno
            else if (Col == nColValorInterno) {
                nValorUnitario = fg.ValueMatrix(Row, nColValorUnitario);
                nValorRevenda = fg.ValueMatrix(Row, nColValorRevenda);
                nValorInterno = fg.ValueMatrix(Row, nColValorInterno);

                if ((nValorInterno > nValorRevenda) || (bResultado == false))
                    fg.TextMatrix(Row, nColValorRevenda) = nValorInterno;

                if ((nValorInterno > nValorUnitario) || (bResultado == false))
                    fg.TextMatrix(Row, nColValorUnitario) = nValorInterno;
            }
            // Campo valor revenda
            else if (Col == nColValorRevenda) {
                nValorUnitario = fg.ValueMatrix(Row, nColValorUnitario);
                nValorRevenda = fg.ValueMatrix(Row, nColValorRevenda);
                nValorInterno = fg.ValueMatrix(Row, nColValorInterno);

                if ((nValorRevenda < nValorInterno) || (bResultado == false))
                    fg.TextMatrix(Row, nColValorInterno) = nValorRevenda;

                if ((nValorRevenda > nValorUnitario) || (bResultado == false))
                    fg.TextMatrix(Row, nColValorUnitario) = nValorRevenda;
            }
            // Campo Valor Unitario
            else if (Col == nColValorUnitario) {
                nValorUnitario = fg.ValueMatrix(Row, nColValorUnitario);
                nValorRevenda = fg.ValueMatrix(Row, nColValorRevenda);
                nValorInterno = fg.ValueMatrix(Row, nColValorInterno);
                nValorTabela = parseFloat(replaceStr(fg.TextMatrix(Row, nColValorTabela), ',', '.'));

                if ((nValorUnitario < nValorRevenda) || (bResultado == false))
                    fg.TextMatrix(Row, nColValorRevenda) = nValorUnitario;

                if ((nValorUnitario < nValorInterno) || (bResultado == false))
                    fg.TextMatrix(Row, nColValorInterno) = nValorUnitario;

                if (!isNaN(nValorUnitario) && !isNaN(nValorTabela) && nValorTabela > 0) {
                    nVariacaoItem99999 = roundNumber(((nValorUnitario / (nValorTabela * (1 + (nVariacao / 100)) * (1 + (nPercentualSUP / 100)))) - 1) * 100, 2);

                    if (nVariacaoItem99999 > 999.99)
                        fg.TextMatrix(Row, getColIndexByColKey(fg, 'VariacaoItem')) = 999.99;
                    else if (nVariacaoItem99999 < -999.99)
                        fg.TextMatrix(Row, getColIndexByColKey(fg, 'VariacaoItem')) = -999.99;
                    else
                        fg.TextMatrix(Row, getColIndexByColKey(fg, 'VariacaoItem')) = nVariacaoItem99999;
                }
                else {
                    if (!isNaN(nValorTabela)) {
                        nVariacaoItem99999 = roundNumber(nValorTabela * (1 + (nVariacao / 100)) * (1 + (nPercentualSUP / 100)), 2);

                        if (nVariacaoItem99999 > 999.99)
                            fg.TextMatrix(Row, getColIndexByColKey(fg, 'VariacaoItem')) = 999.99;
                        else if (nVariacaoItem99999 < -999.99)
                            fg.TextMatrix(Row, getColIndexByColKey(fg, 'VariacaoItem')) = -999.99;
                        else
                            fg.TextMatrix(Row, getColIndexByColKey(fg, 'VariacaoItem')) = nVariacaoItem99999;
                    }
                    else
                        fg.TextMatrix(Row, getColIndexByColKey(fg, 'VariacaoItem')) = '0.00';
                }

            }

            else if (fg.col == nColdtVigenciaServicoInicio) {

                if ((verificaData(fg.TextMatrix(Row, nColdtVigenciaServicoInicio)) == false)) {
                    fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                }

                else if ((new Date(normalizeDate_DateTime(dateAdd('d', '31', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) < (new Date())) {

                    fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                    if (window.top.overflyGen.Alert('Vig�ncia in�cio n�o deve ser anterior aos �ltimos 30 dias.') == 0)
                        return null;

                    return false;
                }

                else if (fg.TextMatrix(Row, nColdtVigenciaServicoFim).length > 0) {

                    if ((new Date(normalizeDate_DateTime(dateAdd('d', '31', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) >
                            (new Date(normalizeDate_DateTime(fg.TextMatrix(Row, nColdtVigenciaServicoFim), (DATE_FORMAT == "DD/MM/YYYY" ? true : false))))) {

                        fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                        if (window.top.overflyGen.Alert('Per�odo de vig�ncia dever� ser de pelo menos 30 dias.') == 0)
                            return null;

                        return false;
                    }

                    else if ((new Date(normalizeDate_DateTime(dateAdd('yyyy', '5', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) <
                            (new Date(normalizeDate_DateTime(fg.TextMatrix(Row, nColdtVigenciaServicoFim), (DATE_FORMAT == "DD/MM/YYYY" ? true : false))))) {

                        fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                        if (window.top.overflyGen.Alert('Per�odo de vig�ncia m�xima s�o 5 anos.') == 0)
                            return null;

                        return false;
                    }
                }
            }

            else if (fg.col == nColdtVigenciaServicoFim) {

                if ((verificaData(fg.TextMatrix(Row, nColdtVigenciaServicoFim)) == false)) {
                    fg.TextMatrix(Row, nColdtVigenciaServicoFim) = '';

                }

                else if ((new Date(normalizeDate_DateTime(dateAdd('d', '1', fg.TextMatrix(Row, nColdtVigenciaServicoFim)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) < (new Date())) {

                    fg.TextMatrix(Row, nColdtVigenciaServicoFim) = '';

                    if (window.top.overflyGen.Alert('Vig�ncia fim deve ser maior que a data atual.') == 0)
                        return null;

                    return false;
                }

                else if (fg.TextMatrix(Row, nColdtVigenciaServicoInicio).length > 0) {

                    if ((new Date(normalizeDate_DateTime(dateAdd('d', '31', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) >
                            (new Date(normalizeDate_DateTime(fg.TextMatrix(Row, nColdtVigenciaServicoFim), (DATE_FORMAT == "DD/MM/YYYY" ? true : false))))) {

                        fg.TextMatrix(Row, nColdtVigenciaServicoFim) = '';

                        if (window.top.overflyGen.Alert('Per�odo de vig�ncia dever� ser de pelo menos 30 dias.') == 0)
                            return null;

                        return false;
                    }

                    else if ((new Date(normalizeDate_DateTime(dateAdd('yyyy', '5', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) <
                            (new Date(normalizeDate_DateTime(fg.TextMatrix(Row, nColdtVigenciaServicoFim), (DATE_FORMAT == "DD/MM/YYYY" ? true : false))))) {

                        fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                        if (window.top.overflyGen.Alert('Per�odo de vig�ncia m�xima s�o 5 anos.') == 0)
                            return null;

                        return false;
                    }
                }
            }


            dsoGrid.recordset['VariacaoItem'].value = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'VariacaoItem'));
            dsoGrid.recordset['ValorInterno'].value = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorInterno'));
            dsoGrid.recordset['ValorRevenda'].value = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorRevenda'));
            dsoGrid.recordset['ValorUnitario'].value = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorUnitario'));
            fg.TextMatrix(Row, nColValorComissaoInterna) = '';
            fg.TextMatrix(Row, nColValorComissaoRevenda) = '';

            calculaValorICMSSTRevendaEInterno_AfterEdit(Row);        

            fg.TextMatrix(1, nColValorComissaoInterna) = '';
            fg.TextMatrix(1, nColValorComissaoRevenda) = '';
        }
    }

    fg.Row = Row;
    fg.Col = Col;

}
// FINAL DE EVENTOS DE GRID *****************************************
function dateAdd(p_Interval, p_Number, p_Date) {

    /*
    //Ajuste string para formato americano.
    if (aEmpresa[1] == 130) {
        var dia = p_Date.substring(0, 2)
        var mes = p_Date.substring(3, 5)
        var ano = p_Date.substring(6, 10)

        p_Date = mes + '/' + dia + '/' + ano
    }
    */

    /*if (DATE_FORMAT == "DD/MM/YYYY")
        putDateInMMDDYYYY2(p_Date);*/

    p_Date = normalizeDate_DateTime(p_Date, (DATE_FORMAT == "DD/MM/YYYY" ? true : false));

    if (!new Date(p_Date)) { return "invalid date: '" + p_Date + "'"; }
    if (isNaN(p_Number)) { return "invalid number: '" + p_Number + "'"; }

    p_Number = new Number(p_Number);
    var dt = new Date(p_Date);

    switch (p_Interval.toLowerCase()) {
        case "yyyy": {
            dt.setFullYear(dt.getFullYear() + p_Number);
            break;
        }
        case "q": {
            dt.setMonth(dt.getMonth() + (p_Number * 3));
            break;
        }
        case "m": {
            dt.setMonth(dt.getMonth() + p_Number);
            break;
        }
        case "y":			// day of year
        case "d":			// day
        case "w": {		// weekday
            dt.setDate(dt.getDate() + p_Number);
            break;
        }
        case "ww": {	// week of year
            dt.setDate(dt.getDate() + (p_Number * 7));
            break;
        }
        case "h": {
            dt.setHours(dt.getHours() + p_Number);
            break;
        }
        case "n": {		// minute
            dt.setMinutes(dt.getMinutes() + p_Number);
            break;
        }
        case "s": {
            dt.setSeconds(dt.getSeconds() + p_Number);
            break;
        }
        case "ms": {	// JS extension
            dt.setMilliseconds(dt.getMilliseconds() + p_Number);
            break;
        }
        default: {
            return "invalid interval: '" + p_Interval + "'";
        }
    }

    if (DATE_FORMAT == "DD/MM/YYYY")
        return dataFormatada(dt, 1);
    else
        return dataFormatada(dt, 2);
}

function dataFormatada(data, tipo) {
    var dia = data.getDate();
    if (dia.toString().length == 1)
        dia = "0" + dia;
    var mes = data.getMonth() + 1;
    if (mes.toString().length == 1)
        mes = "0" + mes;
    var ano = data.getFullYear();

    if (tipo == 1)
        return dia + "/" + mes + "/" + ano;
    else if (tipo == 2)
        return mes + "/" + dia + "/" + ano;
}


//Valida datas
function verificaData(sData) {
    var sData = trimStr(sData);
    var bDataIsValid = true;

    if (sData != '')
        bDataIsValid = chkDataEx(sData);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        return false;
    }
    return true;
}

//BETWEEN entre datas
function beweenDate(sdtStart, sdtEnd, sType) {
    sdtStart = new Date(sdtStart);
    sdtEnd = new Date(sdtEnd);
    var dr = moment.range(sdtStart, sdtEnd);

    if (sType == 1)
        return dr.diff('months');
    else if (sType == 2)
        return dr.diff('days');
}