/********************************************************************
modalaprovarmargemminima.js

Library javascript para o modalaprovarmargemminima.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_LiberarPedidoTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_sVisibility = null;
var glb_nValue = 0;
var glb_nControlDiv = 0;
var glb_aContextoID = getCmbCurrDataInControlBar('sup', 1);
var glb_nContextoID = glb_aContextoID[1];

// DSO
var dsoGrid = new CDatatransport("dsoGrid");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoVendedores = new CDatatransport("dsoVendedores");
var dsoEstadoParaID = new CDatatransport("dsoEstadoParaID");
var dsoListaPedidos = new CDatatransport("dsoListaPedidos");
var dsoAvancaPedidos = new CDatatransport("dsoAvancaPedidos");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

//Novas Func�es Modal Hibrida.
    chkLiberar_onclick()
    chkLiberarPedido_onclick()
//Fun��o que alterna as Divs de acordo com o que o usuario clicou
    alteraDiv()
//Carrega combos dinamicos ao contexto da p�gina
    preencheVendedores()
    preencheVendedores_DSC()
    preencheEstadoFim()
    preencheEstadoFim_DSC()
//Listagem do Grid FG2
    fillPedidosLiberar()
    fillPedidosLiberar_DSC()
//Executa a sp_Pedido_AvancaEstado 
    liberaPedidoGrid()
    liberaPedidoGrid_DSC()



js_fg_modalaprovarmargemminimaBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalaprovarmargemminimaDblClick(grid, Row, Col)
js_modalaprovarmargemminimaKeyPress(KeyAscii)
js_modalaprovarmargemminima_ValidateEdit()
js_modalaprovarmargemminima_BeforeEdit(grid, row, col)
js_modalaprovarmargemminima_AfterEdit(Row, Col)
js_fg_AfterRowColmodalaprovarmargemminima (grid, OldRow, OldCol, NewRow, NewCol)
js_fg_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalaprovarmargemminimaBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    glb_bFirstFill = true;
    // mostra a modal

    showExtFrame(window, true);
    window.focus();

    if (!chkAprovar.disabled)
        chkAprovar.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Aprovar Pedidos', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // desabilita o botao OK
    btnOK.disabled = true;
    btnAlteraMMin.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblLiberar', 'chkLiberar', 3, 1, -10, -10],
        ['lblEmpresaID', 'selEmpresaID', 11, 1, -5],
        ['lblEstadoID', 'selEstadoID', 5, 1],
		['lblGPID', 'selGPID', 14, 1],
		['lblFamiliaID', 'selFamiliaID', 17, 1],
		['lblMarcaID', 'selMarcaID', 14, 1],
		['lblPesquisa', 'txtPesquisa', 10, 1, -2],
        ['lblAprovar', 'chkAprovar', 3, 2, -10, -3],
		['lblEmailMarketing', 'ChkEmailMarketing', 3, 3, -10, -5]], null, null, true);

    chkAprovar.checked = true;
    chkAprovar.onclick = chkAprovar_onclick;
    chkLiberar.onclick = chkLiberar_onclick;
    txtPesquisa.onkeypress = txtPesquisa_onkeypress;
    txtPesquisa.maxLength = 100;

    selEmpresaID.style.height = 80;
    selEstadoID.style.height = 80;
    selGPID.style.height = 80;
    selFamiliaID.style.height = 80;
    selMarcaID.style.height = 80;

    adjustLabelsCombos();

    // Ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        btnAlteraMMin.offsetTop + btnAlteraMMin.offsetHeight + 2;
        width = selMarcaID.offsetLeft + selMarcaID.offsetWidth + ELEM_GAP;
        height = selMarcaID.offsetTop + selMarcaID.offsetHeight;
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        //visibility = 'inherit'
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP + 20;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }

    with (fg.style) {
        //visibility = 'inherit';
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // Aqui ajusta os campos nos divs Painel (funcao adjustElementsInForm)	
    adjustElementsInForm([['lblLiberarPedido', 'chkLiberarPedido', 3, 1, -10, -10],
						  ['lblEmpresaLiberarID', 'selEmpresaLiberarID', 20, 1, -5],
                          ['lblEstadoLiberarID', 'selEstadoLiberarID', 5, 1],
                          ['lblEstadoFimID', 'selEstadoFimID', 5, 1],
                          ['lblVendedorID', 'selVendedorID', 20, 1]], null, null, true);

    chkLiberarPedido.onclick = chkLiberarPedido_onclick;
    selEmpresaLiberarID.style.height = 80;
    selEstadoLiberarID.style.height = 80;
    selEstadoFimID.style.height = 80;
    selVendedorID.style.height = 80;

    preencheVendedores();
    preencheEstadoFim();

    selEstadoLiberarID.onchange = selEstadoLiberarID_onchange;

    // Ajusta o divPainel
    with (divPainel.style) {
        border = 'none';
        backgroundColor = 'transparent';
        visibility = 'hidden';

        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = selVendedorID.offsetLeft + selVendedorID.offsetWidth + ELEM_GAP;
        height = selVendedorID.offsetTop + selVendedorID.offsetHeight;
    }

    // ajusta o divFG2
    with (divFG2.style) {
        border = 'none';
        backgroundColor = 'transparent';
        visibility = 'hidden';
        left = ELEM_GAP;
        top = parseInt(divPainel.currentStyle.top, 10) + parseInt(divPainel.currentStyle.height, 10) + ELEM_GAP - 10;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6) - 10;
    }

    with (fg2.style) {
        //visibility = 'inherit';
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10);
        height = parseInt(divFG2.style.height, 10);
    }

    // Reposiciona botao OK
    with (btnOK) {
        style.top = divControls.offsetTop + txtPesquisa.offsetTop + txtPesquisa.offsetHeight + 15;
        style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
        value = 'Aprovar';
    }


    with (btnAlteraMMin) {
        style.width = btnOK.currentStyle.width;
        style.height = btnOK.currentStyle.height;
        //style.top = btnOK.currentStyle.top;
        style.top = btnOK.offsetTop;
        style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnAlteraMMin.currentStyle.width, 10) - 4;
    }

    with (btnFillGrid) {
        style.width = btnOK.currentStyle.width;
        style.height = btnOK.currentStyle.height;
        //style.top = btnOK.currentStyle.top;
        style.top = divControls.offsetTop + txtPesquisa.offsetTop;
        style.left = btnOK.offsetLeft;
    }

    // Esconde botao cancel
    with (btnCanc) {
        style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;

    startGridInterface(fg2);
    fg2.Cols = 0;
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;
    fg2.Redraw = 2;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtDataInicioFiltro_onKeyPress() {
    if (event.keyCode == 13) {
        fg.Rows = 1;
        // ajusta estado dos botoes
        setupBtnsFromGridState();
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        if (glb_nControlDiv == 0)
            saveDataInGrid(false);
        else
            liberaPedidoGrid();
    }
    else if (controlID == 'btnFillGrid') {
        listar();
    }
    else if (controlID == 'btnAlteraMMin') {
        saveDataInGrid(true);
    }
        // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    lockControlsInModalWin(true);

    // @@ atualizar interface aqui
    setupBtnsFromGridState();

    // preenche o grid da janela modal
    if (glb_nControlDiv == 0)
        fillGridData();
    else
        fillPedidosLiberar();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState() {
    var i;
    var bHasRowsInGrid = fg.Rows > 1;

    btnOK.disabled = true;

    if (bHasRowsInGrid) {
        for (i = 1; i < fg.Rows; i++) {
            if (getCellValueByColKey(fg, 'OK', i) != 0) {
                btnOK.disabled = false;
                break;
            }
        }
    }
    if (!ChkEmailMarketing.checked)
        btnAlteraMMin.disabled = btnOK.disabled;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    var sEmpresas = '';
    var sEstados = '';
    var sGerentesProduto = '';
    var sFamilias = '';
    var sMarcas = '';
    var sPesquisa = trimStr(txtPesquisa.value);
    var bAprovar;
    var bEmailMarketing;
    var strPars = '';


    // Empresa
    for (i = 0; i < selEmpresaID.length; i++) {
        if (selEmpresaID.options[i].selected)
            sEmpresas += '/' + selEmpresaID.options[i].value;
    }

    if (sEmpresas == '')
        sEmpresas = null;
    else
        sEmpresas = sEmpresas + '/';
    //sEmpresas = '\'' + sEmpresas + '/' + '\'';

    // Estados
    for (i = 0; i < selEstadoID.length; i++) {
        if (selEstadoID.options[i].selected)
            sEstados += '/' + selEstadoID.options[i].value;
    }


    if (sEstados == '')
        sEstados = null;
    else
        sEstados = sEstados + '/';
    //sEstados = '\'' + sEstados + '/' + '\'';

    // Gerente Produto
    for (i = 0; i < selGPID.length; i++) {
        if (selGPID.options[i].selected)
            sGerentesProduto += '/' + selGPID.options[i].value;
    }

    if (sGerentesProduto == '')
        sGerentesProduto = null;
    else
        sGerentesProduto = sGerentesProduto + '/';
    //sGerentesProduto = '\'' + sGerentesProduto + '/' + '\'';


    // Familias
    for (i = 0; i < selFamiliaID.length; i++) {
        if (selFamiliaID.options[i].selected)
            sFamilias += '/' + selFamiliaID.options[i].value;
    }

    if (sFamilias == '')
        sFamilias = null;
    else
        sFamilias = sFamilias + '/';
    //sFamilias = '\'' + sFamilias + '/' + '\'';


    // Marcas
    for (i = 0; i < selMarcaID.length; i++) {
        if (selMarcaID.options[i].selected)
            sMarcas += '/' + selMarcaID.options[i].value;
    }

    if (sMarcas == '')
        sMarcas = null;
    else
        sMarcas = sMarcas + '/';
    //sMarcas = '\'' + sMarcas + '/' + '\'';


    if (sPesquisa == '')
        sPesquisa = null;
    /*else
		sPesquisa = '\'' + sPesquisa + '\'';
    */

    bAprovar = (chkAprovar.checked ? 1 : 0);
    bEmailMarketing = (ChkEmailMarketing.checked ? 1 : 0);

    strPars = '?sEmpresas=' + escape(sEmpresas);
    strPars += '&sEstados=' + escape(sEstados);
    strPars += '&sGerentesProduto=' + escape(sGerentesProduto);
    strPars += '&sFamilias=' + escape(sFamilias);
    strPars += '&sMarcas=' + escape(sMarcas);
    strPars += '&sPesquisa=' + escape(sPesquisa);
    strPars += '&nUsuarioID=' + escape(glb_nUsuarioID);
    strPars += '&bAprovar=' + escape(bAprovar);
    strPars += '&bEmailMarketing=' + escape(bEmailMarketing);

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    //Alterado para pagina ASP. BJBN 30/08/2013
    dsoGrid.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/fillmargemminima.aspx' + strPars;
    /*
	dsoGrid.SQL = 'EXEC sp_ProdutoMargemMinima_Pesquisar ' + sEmpresas + ', ' + sEstados + ', ' + sGerentesProduto + ', ' + sFamilias + ', ' +
		sMarcas + ', ' + sPesquisa + ',' + glb_nUsuarioID + ', ' + (chkAprovar.checked ? 1 : 0) + ', ' + (ChkEmailMarketing.checked ? 1 : 0);
    */
    dsoGrid.ondatasetcomplete = fillGridData_DSC;

    try {
        dsoGrid.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;

    if (glb_nDSOs > 0)
        return null;

    if (glb_bFirstFill) {
        glb_bFirstFill = false;
    }

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg, ['Produto',
					'ID',
					'E',
					'Qtd',
					'Email MKT',
					'Unit',
					'Total',
					'MC',
					'M M�n',
					'M M�n Cli',
					'M M�d',
					'OK',
					'Parceiro',
					'Int',
					'Ext',
					'Vendedor',
					'Vend 10',
					'Estq',
					'C/E',
					'TotalPedido',
					'MC',
					'PMP',
					'Pedido',
					'Est',
					'Empresa',
					'Fam�lia',
					'Marca',
					'GP',
					'RelacaoID',
					'PedItemID'], [28, 29]);

    fillGridMask(fg, dsoGrid, ['Produto*',
							'ProdutoID*',
							'EstadoProduto*',
							'Quantidade*',
							'PrecoEmailMarketing*',
							'ValorUnitarioItem*',
							'ValorTotalItem*',
							'MargemContribuicao*',
							'MargemMinima',
							'MargemMinimaCliente*',
							'MargemContribuicaoMedia*',
							'OK',
							'Parceiro*',
							'ClassificacaoInterna*',
							'ClassificacaoExterna*',
							'Vendedor*',
							'QuantidadeVendas*',
							'Estoque*',
							'ComprarExcesso*',
							'ValorTotalPedido*',
							'MargemContribuicaoPedido*',
							'PrazoMedioPagamento*',
							'PedidoID*',
							'EstadoPedido*',
							'Empresa*',
							'Familia*',
							'Marca*',
							'GerenteProduto*',
							'RelacaoID',
							'PedItemID'],
							 ['', '', '', '99999', '999999999.99', '999999999.99', '999.99', '#999.99', '999.99', '999.99', '', '', '', '', '', '99999', '999999', '9999999', '999999999.99', '999.99', '', '', '', '', '', '', '', '', ''],
							 ['', '', '', '#####', '###,###,##0.00', '###,###,##0.00', '##0.00', '##0.00', '##0.00', '##0.00', '', '', '', '', '', '#####', '######', '#######', '###,###,##0.00', '##0.00', '', '', '', '', '', '', '', '', '']);

    fg.Redraw = 0;

    alignColsInGrid(fg, [1, 3, 4, 5, 6, 7, 8, 9, 10, 16, 17, 18, 19, 20, 21, 22]);

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'ProdutoID*'), '###,###,###,###.00', 'S'],
                                                          [getColIndexByColKey(fg, 'Quantidade*'), '###,###,###,###.00', 'S'],
                                                          [getColIndexByColKey(fg, 'ValorTotalItem*'), '###,###,###,###.00', 'S']]);

    setTotalLine();
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    paintCellsSpecialyReadOnly();

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        //js_modalaprovarmargemminima_AfterEdit(2, getColIndexByColKey(fg, 'OK'));
        fg.Editable = true;
        window.focus();
        fg.focus();
    }
    else {
        ;
    }

    fg.Redraw = 2;
    fg.FrozenCols = 11;

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid(bAlterarMargem) {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    lockControlsInModalWin(true);

    for (i = 2; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0) {
            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                    strPars = '';
                    nDataLen = 0;
                }

                strPars = '?Aprovar=' + escape(chkAprovar.checked ? 1 : 0);
                strPars += '&AlterarMargem=' + escape(bAlterarMargem ? 1 : 0);
                strPars += '&UsuarioID=' + escape(glb_nUsuarioID);
            }

            nDataLen++;

            if (bAlterarMargem)
                strPars += '&RegistroID=' + escape(getCellValueByColKey(fg, 'RelacaoID', i));
            else
                strPars += '&RegistroID=' + escape(getCellValueByColKey(fg, 'PedItemID', i));

            strPars += '&MargemMinima=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'MargemMinima')));
        }
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/margemminima.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {            
            glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;        
        glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

function adjustLabelsCombos() {
    ;
}

function listar() {
    refreshParamsAndDataAndShowModalWin(false);
}

function txtPesquisa_onkeypress() {
    if (event.keyCode == 13) {
        listar();
    }
}

function selEstadoLiberarID_onchange() {
    preencheEstadoFim();
}


/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    lockControlsInModalWin(false);

    glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly() {
    paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalaprovarmargemminimaBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalaprovarmargemminimaDblClick(grid, Row, Col) {
    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if (getColIndexByColKey(grid, 'OK') != Col)
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);

    glb_PassedOneInDblClick = true;

    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 1; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    js_modalaprovarmargemminima_AfterEdit(fg.Row, fg.Col);

    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalaprovarmargemminimaKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalaprovarmargemminima_ValidateEdit() {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalaprovarmargemminima_BeforeEdit(grid, row, col) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalaprovarmargemminima_AfterEdit(Row, Col) {
    var nColValor = getColIndexByColKey(fg, 'MargemMinima');

    if (Col == nColValor) {
        fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;
        fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
    }

    setupBtnsFromGridState();
    setTotalLine();
}

function setTotalLine() {
    var nProdutoID = 0;
    var nQuantidade = 0;
    var nValorTotalItem = 0;

    for (i = 2; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
            nProdutoID++;
            nQuantidade += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade*'));
            nValorTotalItem += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorTotalItem*'));
        }
    }

    if (fg.Rows > 2) {
        fg.TextMatrix(1, getColIndexByColKey(fg, 'ProdutoID*')) = nProdutoID;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Quantidade*')) = nQuantidade;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorTotalItem*')) = nValorTotalItem;
    }
}

function chkAprovar_onclick() {
    if (chkAprovar.checked)
        btnOK.value = 'Aprovar';
    else
        btnOK.value = 'Desaprovar';

    fg.Rows = 1;
}


function chkLiberarPedido_onclick() {
    if (chkLiberarPedido.checked) {
        glb_sVisibility = 1;
    }
    else {
        glb_sVisibility = 0;
        chkLiberar.checked = false;
        glb_nControlDiv = 0;
        fg2.Row = 0;
        secText('Aprovar Pedidos', 1);
        chkAprovar_onclick();
    }

    alteraDiv();
}


function chkLiberar_onclick() {
    if (chkLiberar.checked) {
        glb_sVisibility = 1;
        chkLiberarPedido.checked = true;
        glb_nControlDiv = 1;
        secText('Liberar Pedidos OPEN', 1);
        btnOK.value = 'Liberar';
        fg.row = 0;
    }
    else {
        glb_sVisibility = 0;
    }

    alteraDiv();
}

function alteraDiv() {
    var div1;
    var div2;

    if (glb_sVisibility == 0) {
        div1 = 'inherit';
        div2 = 'hidden';
    }
    else {
        div1 = 'hidden';
        div2 = 'inherit';
    }

    divPainel.style.visibility = div2;
    divFG2.style.visibility = div2;
    fg2.style.visibility = div2;

    divControls.style.visibility = div1;
    divFG.style.visibility = div1;
    fg.style.visibility = div1;
    btnAlteraMMin.style.visbility = div1;


    glb_sVisibility = null;
}


function preencheVendedores() {
    glb_nValue = selVendedorID.value;

    setConnection(dsoVendedores);

    dsoVendedores.SQL = 'SELECT DISTINCT a.ProprietarioID AS VendedorID, b.Fantasia AS Vendedor ' +
    	    'FROM Pedidos a WITH(NOLOCK) ' +
                'INNER JOIN Pessoas b WITH(NOLOCK)  ON (b.PessoaID = a.ProprietarioID) ' +
                'WHERE (a.EstadoID IN (21,22,23,24)) ' +
                'AND b.EstadoID NOT IN (1, 5) ' +
                'AND a.TipoPedidoID = (CASE WHEN ' + escape(glb_nContextoID) + ' = 5111 THEN 601 ELSE 602 END) ' +
                ' AND a.TransacaoID IN (111,115,125,215) ' +
                ' AND a.dtPedido >= (GETDATE() - 30) ' +
            'ORDER BY Vendedor ASC';

    dsoVendedores.ondatasetcomplete = preencheVendedores_DSC;
    dsoVendedores.Refresh();
}

function preencheVendedores_DSC() {
    if (!((dsoVendedores.recordset.BOF) && (dsoVendedores.recordset.EOF)))
    {
        clearComboEx(['selVendedorID']);
        while (!dsoVendedores.recordset.EOF)
        {
            optionStr = dsoVendedores.recordset['Vendedor'].value;
            optionValue = dsoVendedores.recordset['VendedorID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            selVendedorID.add(oOption);
            dsoVendedores.recordset.MoveNext();
        }
    }
}

function preencheEstadoFim() {
    glb_nValue = selEstadoFimID.value;

    setConnection(dsoEstadoParaID);

    dsoEstadoParaID.SQL = ' SELECT a.RecursoID AS EstadoID, a.RecursoAbreviado AS Estado ' +
                            ' FROM Recursos a WITH(NOLOCK) ' +
                            ' WHERE a.RecursoID IN (24,25,26) ' +
                                ' AND a.RecursoID > ' + selEstadoLiberarID.value +
                                ' AND a.RecursoID <> (CASE WHEN ' + escape(glb_nContextoID) + ' = 5111 THEN 25 ELSE 26 END) ' +
                            ' ORDER BY EstadoID ';

    dsoEstadoParaID.ondatasetcomplete = preencheEstadoFim_DSC;
    dsoEstadoParaID.Refresh();
}

function preencheEstadoFim_DSC() {
    if (!((dsoEstadoParaID.recordset.BOF) && (dsoEstadoParaID.recordset.EOF))) {
        clearComboEx(['selEstadoFimID']);
        while (!dsoEstadoParaID.recordset.EOF) {
            optionStr = dsoEstadoParaID.recordset['Estado'].value;
            optionValue = dsoEstadoParaID.recordset['EstadoID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            selEstadoFimID.add(oOption);
            dsoEstadoParaID.recordset.MoveNext();
        }
    }

}

function fillPedidosLiberar() {
    if (glb_LiberarPedidoTimerInt != null) {
        window.clearInterval(glb_LiberarPedidoTimerInt);
        glb_LiberarPedidoTimerInt = null;
    }

    var sEmpresas = '';
    var sEstado = '';
    var sVendedor = '';
    var sWhere = '';

    // Empresa
    for (i = 0; i < selEmpresaLiberarID.length; i++) {
        if (selEmpresaLiberarID.options[i].selected)
            sEmpresas += ',' + selEmpresaLiberarID.options[i].value;
    }

    if (sEmpresas != '')
        sWhere = sWhere + ' AND (a.EmpresaID IN (0' + sEmpresas + ')) ';

    // Vendedor
    for (i = 0; i < selVendedorID.length; i++) {
        if (selVendedorID.options[i].selected)
            sVendedor += ',' + selVendedorID.options[i].value;
    }

    if (sVendedor != '')
        sWhere = sWhere + ' AND (a.ProprietarioID IN (0' + sVendedor + ')) ';

    sEstados = selEstadoLiberarID.value;

    setConnection(dsoListaPedidos);
    dsoListaPedidos.SQL = 'SELECT DISTINCT TOP 50 a.PedidoID, f.RecursoAbreviado as Estado, a.PessoaID, c.Fantasia AS Pessoa, a.ParceiroID, d.Fantasia AS Parceiro, a.ValorTotalPedido, ' +
                            ' a.MargemContribuicao, a.PercentualSUP, a.Observacao, a.TransacaoID, CONVERT(VARCHAR(MAX),a.Observacoes) AS Observacoes,  a.ProprietarioID, b.Fantasia AS Proprietario, \'\' AS DescricaoRodape ' +
                            'FROM Pedidos a WITH(NOLOCK) ' +
                            'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ProprietarioID) ' +
                            'INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.PessoaID)  ' +
                            'INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = a.ParceiroID) ' +
                            'INNER JOIN Pedidos_Itens e WITH(NOLOCK) ON (e.PedidoID = a.PedidoID) ' +
                            'INNER JOIN Recursos f WITH(NOLOCK) ON (f.RecursoID = a.EstadoID) ' +
                            'INNER JOIN RelacoesPesCon g WITH(NOLOCK) ON (g.ObjetoID = e.ProdutoID) ' +
                            'INNER JOIN RelacoesPesCon_Fornecedores h WITH(NOLOCK) ON (h.RelacaoID = g.RelacaoID) ' +
                            'WHERE (a.EstadoID = ' + sEstados + ') ' +
		                        sWhere +
		                        'AND a.TransacaoID IN (111,115,125,215) ' +
		                        'AND (a.dtPedido >= (GETDATE()-30)) ' +
                                'AND a.TipoPedidoID = (CASE WHEN ' + escape(glb_nContextoID) + ' = 5111 THEN 601 ELSE 602 END) ' +
		                        'AND ISNULL(e.EhEncomenda,0) = (CASE WHEN ' + escape(glb_nContextoID) + ' = 5111 THEN 0 ELSE 1 END)  ' +
                                //Inserido where para listar apenas pedidos de OPEN para libera��o.
                                'AND (g.TipoRelacaoID = 61) ' +
		                        'AND (h.FornecedorID = 10003) ' +
	                        'ORDER BY a.PedidoID DESC ';
    dsoListaPedidos.ondatasetcomplete = fillPedidosLiberar_DSC;
    dsoListaPedidos.Refresh();

}

function fillPedidosLiberar_DSC() {
    fg2.Redraw = 0;
    fg2.Editable = false;
    startGridInterface(fg2);


    fg2.FontSize = '8';
    fg2.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg2, ['PedidoID',
                     'E',
                     'PessoaID',
                     'Pessoa',
                     'ParceiroID',
                     'Parceiro',
                     'ValorPedido',
                     'MC',
                     'SUP',
                     'Observa��o',
                     'Observacoes',
                     'TransacaoID',
                     'ProprietarioID',
                     'Proprietario',
                     'DescricaoRodape'], [2, 4, 12, 14]);

    fillGridMask(fg2, dsoListaPedidos,
                            ['PedidoID*',
                                'Estado*',
                                'PessoaID*',
                                'Pessoa*',
                                'ParceiroID*',
                                'Parceiro*',
                                'ValorTotalPedido*',
                                'MargemContribuicao*',
                                'PercentualSUP*',
                                'Observacao*',
                                'Observacoes*',
                                'TransacaoID*',
                                'ProprietarioID*',
                                'Proprietario*',
                                'DescricaoRodape*'],
							 ['', '', '', '', '', '', '999999999.99', '999.99', '999.99', '', '', '', '', '', ''],
                             ['', '', '', '', '', '', '###,###,##0.00', '###.00', '###.00', '', '', '', '', '', '']);

    fg2.Redraw = 0;

    alignColsInGrid(fg2, [6, 7, 8]);

    gridHasTotalLine(fg2, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg2, 'ValorTotalPedido*'), '###,###,###,###.00', 'S']]);

    setTotalLine();
    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0, fg2.Cols - 1);

    paintCellsSpecialyReadOnly();

    fg2.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fg2.Rows > 1) {
        //js_modalaprovarmargemminima_AfterEdit(2, getColIndexByColKey(fg, 'OK'));
        //fg2.Editable = true;
        window.focus();
        fg2.focus();
    }
    else {
        ;
    }

    fg2.Redraw = 2;
    fg2.FrozenCols = 2;

    btnOK.disabled = false;

    // ajusta estado dos botoes
    //setupBtnsFromGridState();
}

function liberaPedidoGrid() {
    /*if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }
    */

    var nPedidoID = '';
    var nEstadoFimID = '';
    var nUsuarioID = '';
    var strPars = '';

    nPedidoID = fg2.ValueMatrix(fg2.Row, getColIndexByColKey(fg2, 'PedidoID*'));
    nEstadoFimID = selEstadoFimID.value;

    strPars = '?nPedidoID=' + escape(nPedidoID);
    strPars += '&nEstadoFimID=' + escape(nEstadoFimID);
    strPars += '&nUsuarioID=' + escape(glb_nUsuarioID);

    // parametrizacao do dso dsoAvancaPedidos
    setConnection(dsoAvancaPedidos);

    dsoAvancaPedidos.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/avancapedidoopen.aspx' + strPars;
    dsoAvancaPedidos.ondatasetcomplete = liberaPedidoGrid_DSC;
    dsoAvancaPedidos.Refresh();
}

function liberaPedidoGrid_DSC() {

    var sMensagem = (dsoAvancaPedidos.recordset.Fields['Mensagem'].value == null ? '' : dsoAvancaPedidos.recordset.Fields['Mensagem'].value);
    var nResultado = (dsoAvancaPedidos.recordset.Fields['Resultado'].value == null ? 0 : dsoAvancaPedidos.recordset.Fields['Resultado'].value);

    if (nResultado == 1) {
        if (sMensagem == '') {
            sMensagem = 'Erro ao Avan�ar o pedido. 1';
        }
    }
    else if (nResultado == 2) {
        if (sMensagem == '') {
            sMensagem = 'Pedido Avan�ado.';
        }
    }
    else {
        if (sMensagem == '') {
            sMensagem = 'Erro ao Avan�ar o pedido. 0';
        }
    }

    if (window.top.overflyGen.Alert(sMensagem) == 0)
        return null;

    lockControlsInModalWin(false);
    fillPedidosLiberar();
    //glb_LiberarPedidoTimerInt = window.setInterval('fillPedidosLiberar()', 10, 'JavaScript');
}


/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalaprovarmargemminima(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return true;

    setupBtnsFromGridState();
}


function js_fg_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {

}

// FINAL DE EVENTOS DE GRID *****************************************
