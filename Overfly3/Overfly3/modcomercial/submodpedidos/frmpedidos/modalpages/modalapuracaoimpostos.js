/********************************************************************
modalapuracaoimpostos.js

Library javascript para o modalapuracaoimpostos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
var glb_bDetalhe = false;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');
var dsoEMail = new CDatatransport('dsoEMail');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
listar()
paintCellsSpecialyReadOnly()

js_fg_modalapuracaoimpostosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalapuracaoimpostosDblClick(grid, Row, Col)
js_modalapuracaoimpostosKeyPress(KeyAscii)
js_modalapuracaoimpostos_ValidateEdit()
js_modalapuracaoimpostos_BeforeEdit(grid, row, col)
js_modalapuracaoimpostos_AfterEdit(Row, Col)
js_fg_AfterRowColmodalapuracaoimpostos (grid, OldRow, OldCol, NewRow, NewCol)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalapuracaoimpostosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a modal
	showExtFrame(window, true);
	window.focus();
	setupBtnsFromGridState();

	if ( !txtDataInicio.disabled )
		txtDataInicio.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Apura��o de Impostos', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
	btnFillGrid.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblImpostoID','selImpostoID',11,1,-10,-10],
                          ['lblDiferencialAliquota', 'chkDiferencialAliquota', 3, 1],
                          ['lblDataInicio', 'txtDataInicio', 10, 1, -8],
						  ['lblDataFim','txtDataFim',10,1],
						  ['lblFiltro','txtFiltro',38,1],
						  ['btnFillGrid','btn',52,1,5,-2],
						  ['btnImprimir','btn',52,1,3],
						  ['btnExcel','btn',52,1,3]], null, null, true);
  
	btnFillGrid.style.height = 25;
	btnImprimir.style.height = 25;
	btnExcel.style.height = 25;
  
	txtDataInicio.onfocus = selFieldContent;
	txtDataInicio.maxLength = 10;
	txtDataInicio.onkeypress = txtDataInicioFiltro_onKeyPress;
	txtDataInicio.value = glb_dCurrDate;
	
	txtDataFim.onfocus = selFieldContent;
	txtDataFim.maxLength = 10;
	txtDataFim.onkeypress = txtDataInicioFiltro_onKeyPress;
	txtDataFim.value = glb_dCurrDate;

	selImpostoID.onchange = selImpostoID_onchange;
	adjustLabelsCombos();

	txtFiltro.onfocus = selFieldContent;
	txtFiltro.maxLength = 1000;
	txtFiltro.onkeypress = txtDataInicioFiltro_onKeyPress;

	chkDiferencialAliquota.onclick = chkDiferencialAliquota_onclick;

	chkDiferencialAliquota.disabled = true;
		
    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
			  btnFillGrid.offsetTop + btnFillGrid.offsetHeight + 2;
        width = txtFiltro.offsetLeft + txtFiltro.offsetWidth + ELEM_GAP;    
        height = txtFiltro.offsetTop + txtFiltro.offsetHeight;    
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + txtFiltro.offsetTop;
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		style.visibility = 'hidden';
		value = 'Ativar';
		title = 'Ativar Financeiros';
    }
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtDataInicioFiltro_onKeyPress()
{
    if ( event.keyCode == 13 )
	{
		fg.Rows = 1;
		// ajusta estado dos botoes
		setupBtnsFromGridState();
		listar();
	}
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        ;
    }
    else if (controlID == 'btnFillGrid')
    {
		listar();
    }
    else if (controlID == 'btnImprimir')
    {
		imprime();
    }
    else if (controlID == 'btnExcel')
    {
		geraExcel();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    lockControlsInModalWin(true);
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var i;
	btnOK.disabled = true;
	btnFillGrid.disabled = false;
	btnImprimir.disabled = (fg.Rows <= 1);
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData(bDetalhe)
{
	if (bDetalhe==null)
	    bDetalhe = false;

	var sImposto = '?nImpostoID=' + escape(bDetalhe ? getCellValueByColKey(fg, 'ImpostoID', fg.Row) : selImpostoID.value);
	var sDiferencialAliquota = '&sDiferencialAliquotas=' + ((chkDiferencialAliquota.checked) ? '1' : '0');
	var sDataInicio = '&sDataInicio=' + escape(normalizeDate_DateTime(txtDataInicio.value, true));
	var sDataFim = '&sDataFim=' + escape(normalizeDate_DateTime(txtDataFim.value, true));
	var nEmpresaID = '&nEmpresaID=' + escape(glb_aEmpresaData[0]);
	var nCFOPID = '&nCFOPID=' + escape(bDetalhe ? getCellValueByColKey(fg, 'CFOPID', fg.Row) : 0);
	var sUF = '&sUF=' + escape(bDetalhe ? getCellValueByColKey(fg, 'UF', fg.Row) : '');
	var nTipoResultado = '&nTipoResultado=' + escape(bDetalhe ? 2 : 1);
	var sFiltro = '&sFiltro=' + escape(trimStr(txtFiltro.value));

	var strPars = sImposto + sDiferencialAliquota + sDataInicio + sDataFim + nEmpresaID + nCFOPID + sUF + nTipoResultado + sFiltro;
	
	lockControlsInModalWin(true);
	
    dsoGrid.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/apuracaoimpostosdata.aspx' + strPars;
    
    if (bDetalhe)
		dsoGrid.ondatasetcomplete = fillGridDataDetalhe_DSC;
    else
		dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.refresh();
}

function fillGridDataDetalhe_DSC()
{
    glb_bDetalhe = true;

	if (dsoGrid.recordset.Fields.Count == 1)
	{
		if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF))
		{
			if ( window.top.overflyGen.Alert(dsoGrid.recordset['Msg'].value) == 0 )
				return null;
		}
		
		lockControlsInModalWin(false);
		return null;
	}

	var nLine = fg.Row;
	var nLineOld = nLine;
	while (!(dsoGrid.recordset.BOF || dsoGrid.recordset.EOF))
	{
		addRowinGrid(dsoGrid, nLine);
		dsoGrid.recordset.MoveNext();
		nLine++;
	}
	
	fg.Row = nLineOld;
	fg.TopRow = fg.Row;
	lockControlsInModalWin(false);
	window.focus();
	fg.focus();
	glb_bDetalhe = false;
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	if (dsoGrid.recordset.Fields.Count == 1)
	{
		if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF))
		{
			if ( window.top.overflyGen.Alert(dsoGrid.recordset['Msg'].value) == 0 )
				return null;
		}
		
		lockControlsInModalWin(false);
		return null;
	}

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	var aHoldCols = new Array();


	headerGrid(fg,['Imposto',
				'Quant',
				'Base Cr�dito',
				'Valor Cr�dito',
				'Base D�bito',
				'Valor D�bito',
                'Valor FCP',
				'Saldo',
				'ImpostoID',
				'UF',
				'CFOPID',
				'Nivel'], [6,8,9,10,11]);

	fg.ColKey(0) = 'Label';
	fg.ColKey(1) = 'Quantidade';
	fg.ColKey(2) = 'BaseCredito';
	fg.ColKey(3) = 'ValorCredito';
	fg.ColKey(4) = 'BaseDebito';
	fg.ColKey(5) = 'ValorDebito';
	fg.ColKey(6) = 'ValorFCPDebito';
	fg.ColKey(7) = 'Saldo';
	fg.ColKey(8) = 'ImpostoID';
	fg.ColKey(9) = 'UF';
	fg.ColKey(10) = 'CFOPID';
	fg.ColKey(11) = 'Nivel';
	fg.ColHidden(6) = true;
	fg.ColHidden(8) = true;
	fg.ColHidden(9) = true;
	fg.ColHidden(10) = true;
	fg.ColHidden(11) = true;

    // Quando for diferencia de aliquotas mostra o valor do FCP. BJBN
	if (chkDiferencialAliquota.checked)
	    fg.ColHidden(6) = false;
				
	fg.Editable = false;
	while (!(dsoGrid.recordset.BOF || dsoGrid.recordset.EOF))
	{
		addRowinGrid(dsoGrid, fg.Rows - 1);
		dsoGrid.recordset.MoveNext();
	}
	
	alignColsInGrid(fg,[1, 2, 3, 4, 5, 6, 7]);																
	
	fg.AllowUserResizing = 1;
	fg.VirtualData = true;
	fg.Ellipsis = 1;
	fg.SelectionMode = 1;
	fg.AllowSelection = false;
	fg.FixedRows = 1;
	fg.FixedCols = 1;
	fg.ScrollBars = 3;
	fg.OutLineBar = 1;
    fg.GridLinesFixed = 13;
	fg.GridLines = 1;
	
	for (i=1; i<fg.Rows; i++)
	{
		//fg.IsSubTotal(i) = true;
		fg.RowOutlineLevel(i) = getCellValueByColKey(fg, 'Nivel', i);
		
        if ( fg.IsSubTotal(i) == true )
            fg.IsCollapsed(i) = 2;
	}
	
    fg.Redraw = 0;

    putMasksInGrid(fg, ['','','','','','','','','','','',''],
					   ['', '###,###,##0', '(###,###,##0.00)', '(###,###,##0.00)', '(###,###,##0.00)', '(###,###,##0.00)', '(###,###,##0.00)', '(###,###,##0.00)', '', '', '', '']);
					   
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

	paintCellsSpecialyReadOnly();
	    
	//fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.Redraw = 2;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
    
    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
		fg.Editable = false;
        window.focus();
        fg.focus();
        fg.Row = 1;
    }                
    else
    {
        ;
    }            
}

function addRowinGrid(odso, nLine)
{
	fg.Row = nLine;

	fg.AddItem('', fg.Row+1);
	
	if (fg.Row < (fg.Rows-1))
		fg.Row++;

	fg.ColDataType(getColIndexByColKey(fg, 'Label')) = 12;
	fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Label')) = odso.recordset['Label'].value;

	fg.ColDataType(getColIndexByColKey(fg, 'Quantidade')) = 12;
	fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Quantidade')) = odso.recordset['Quantidade'].value;
	
	fg.ColDataType(getColIndexByColKey(fg, 'BaseCredito')) = 12;
	fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'BaseCredito')) = odso.recordset['BaseCredito'].value;	

	fg.ColDataType(getColIndexByColKey(fg, 'ValorCredito')) = 12;
	fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorCredito')) = odso.recordset['ValorCredito'].value;	

	fg.ColDataType(getColIndexByColKey(fg, 'BaseDebito')) = 12;
	fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'BaseDebito')) = odso.recordset['BaseDebito'].value;	

	fg.ColDataType(getColIndexByColKey(fg, 'ValorDebito')) = 12;
	fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorDebito')) = odso.recordset['ValorDebito'].value;	

	if (!(glb_bDetalhe))
	{
	    fg.ColDataType(getColIndexByColKey(fg, 'ValorFCPDebito')) = 12;
	    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorFCPDebito')) = odso.recordset['ValorFCPDebito'].value;
	}

	fg.ColDataType(getColIndexByColKey(fg, 'Saldo')) = 12;
	fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Saldo')) = odso.recordset['Saldo'].value;	

	fg.ColDataType(getColIndexByColKey(fg, 'ImpostoID')) = 12;
	fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ImpostoID')) = odso.recordset['ImpostoID'].value;

	fg.ColDataType(getColIndexByColKey(fg, 'UF')) = 12;
	fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'UF')) = odso.recordset['UF'].value;

	fg.ColDataType(getColIndexByColKey(fg, 'CFOPID')) = 12;
	fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CFOPID')) = odso.recordset['CFOPID'].value;
	
	fg.ColDataType(getColIndexByColKey(fg, 'Nivel')) = 12;
	fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Nivel')) = odso.recordset['Nivel'].value;

	fg.IsSubTotal(fg.Row) = true;
	fg.RowOutlineLevel(fg.Row) = odso.recordset['Nivel'].value;
	
	fg.FillStyle = 1;

	if (((odso.recordset['Nivel'].value == 2) && ((selImpostoID.value != 25))) || ((selImpostoID.value == 25) && (odso.recordset['Nivel'].value == 3)))
	{
		fg.Select(fg.Row, getColIndexByColKey(fg, 'Label'), fg.Row, getColIndexByColKey(fg, 'Label'));
		fg.CellForeColor = 0XFF0000;
	}
	
	if (odso.recordset['BaseCredito'].value < 0)
	{
		fg.Select(fg.Row, getColIndexByColKey(fg, 'BaseCredito'), fg.Row, getColIndexByColKey(fg, 'BaseCredito'));
		fg.CellForeColor = 0X0000FF;
	}
	
	if (odso.recordset['ValorCredito'].value < 0)
	{
		fg.Select(fg.Row, getColIndexByColKey(fg, 'ValorCredito'), fg.Row, getColIndexByColKey(fg, 'ValorCredito'));
		fg.CellForeColor = 0X0000FF;
	}
	
	if (odso.recordset['BaseDebito'].value < 0)
	{
		fg.Select(fg.Row, getColIndexByColKey(fg, 'BaseDebito'), fg.Row, getColIndexByColKey(fg, 'BaseDebito'));
		fg.CellForeColor = 0X0000FF;
	}
	
	if (odso.recordset['ValorDebito'].value < 0)
	{
		fg.Select(fg.Row, getColIndexByColKey(fg, 'ValorDebito'), fg.Row, getColIndexByColKey(fg, 'ValorDebito'));
		fg.CellForeColor = 0X0000FF;
	}

	if (!(glb_bDetalhe))
	{
	    if (odso.recordset['ValorFCPDebito'].value < 0)
	    {
	        fg.Select(fg.Row, getColIndexByColKey(fg, 'ValorFCPDebito'), fg.Row, getColIndexByColKey(fg, 'ValorFCPDebito'));
	        fg.CellForeColor = 0X0000FF;
	    }

	}
	
	if (odso.recordset['Saldo'].value < 0)
	{
		fg.Select(fg.Row, getColIndexByColKey(fg, 'Saldo'), fg.Row, getColIndexByColKey(fg, 'Saldo'));
		fg.CellForeColor = 0X0000FF;
	}

}

function selImpostoID_onchange()
{
	adjustLabelsCombos();
	fg.Rows = 1;

    //ICMS
	if (selImpostoID.value == 9)
	    chkDiferencialAliquota.disabled = false;
	else
	{
	    chkDiferencialAliquota.checked = false;
	    chkDiferencialAliquota.disabled = true;
	}

	// ajusta estado dos botoes
	setupBtnsFromGridState();
}

function adjustLabelsCombos()
{
    setLabelOfControl(lblImpostoID, (selImpostoID.value == 0 ? '' : selImpostoID.value));
}
function listar()
{
	if (!verificaData())
		return null;
		
	refreshParamsAndDataAndShowModalWin(false);
}

function verificaData()
{
	var sData = trimStr(txtDataInicio.value);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(txtDataInicio.value);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        window.focus();
        
        if ( sData != '' )    
			txtDataInicio.focus();
		
		return false;
	}

	sData = trimStr(txtDataFim.value);
	if (sData != '')
		bDataIsValid = chkDataEx(txtDataFim.value);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        window.focus();
        
        if ( sData != '' )    
			txtDataFim.focus();
		
		return false;
	}
	return true;
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalapuracaoimpostosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalapuracaoimpostosDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;
        
	if (((getCellValueByColKey(fg, 'Nivel', fg.Row) != 2) && (selImpostoID.value != 25)) || ((getCellValueByColKey(fg, 'Nivel', fg.Row) != 3) && (selImpostoID.value == 25)))
		return true;

	deletaNivelFilho();

    // trap so colunas editaveis
    fillGridData(true);
    glb_PassedOneInDblClick = true;
}

function deletaNivelFilho()
{
	var nRow;
	var nNivel = fg.RowOutlineLevel(fg.Row);

	nRow = fg.Row;

	if (nRow <= fg.Rows - 2)
	{
		nRow ++;

		while ((nRow <= fg.Rows - 1) && (nNivel < fg.RowOutlineLevel(nRow)))
			fg.RemoveItem(nRow);
	}
}


/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalapuracaoimpostosKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalapuracaoimpostos_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalapuracaoimpostos_BeforeEdit(grid, row, col)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalapuracaoimpostos_AfterEdit(Row, Col)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalapuracaoimpostos(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return true;

	setupBtnsFromGridState();
}
// FINAL DE EVENTOS DE GRID *****************************************

function getCurrDate()
{
   var d, s = "";
   d = new Date();
   
   if (DATE_SQL_PARAM == 103)
   {
		s += padL(d.getDate().toString(),2,'0') + "/";
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
   }
   else
   {
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
		s += padL(d.getDate().toString(),2,'0') + "/";
   }
   
   s += d.getYear() + " ";
   s += padL(d.getHours().toString(),2,'0') + ":";
   s += padL(d.getMinutes().toString(),2,'0');

   return(s);
}

function imprime()
{
	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	var sMsg = aEmpresaData[6] + '     ' + translateTerm('Apura��o de Impostos', null) + '     ' + 
		txtDataInicio.value + ' - ' + txtDataFim.value + '     ' + getCurrDate();
	var gridLine = fg.gridLines;
	fg.gridLines = 2;

	fg.ColWidth(getColIndexByColKey(fg, 'Quantidade')) = fg.ColWidth(getColIndexByColKey(fg, 'Quantidade')) * 1.11;
	fg.ColWidth(getColIndexByColKey(fg, 'BaseCredito')) = fg.ColWidth(getColIndexByColKey(fg, 'BaseCredito')) * 1.11;
	fg.ColWidth(getColIndexByColKey(fg, 'ValorCredito')) = fg.ColWidth(getColIndexByColKey(fg, 'ValorCredito')) * 1.11;
	fg.ColWidth(getColIndexByColKey(fg, 'BaseDebito')) = fg.ColWidth(getColIndexByColKey(fg, 'BaseDebito')) * 1.11;
	fg.ColWidth(getColIndexByColKey(fg, 'ValorDebito')) = fg.ColWidth(getColIndexByColKey(fg, 'ValorDebito')) * 1.11;
	fg.ColWidth(getColIndexByColKey(fg, 'Saldo')) = fg.ColWidth(getColIndexByColKey(fg, 'Saldo')) * 1.11;
	
	fg.PrintGrid(sMsg, false, 1, 0, 450);
	fg.AutoSizeMode = 0;
	fg.AutoSize(0,fg.Cols-1);
	fg.gridLines = gridLine;
    fg.Redraw = 1;
}

function geraExcel()
{
	var sDataInicio = normalizeDate_DateTime(txtDataInicio.value, true);
	var sDataFim = normalizeDate_DateTime(txtDataFim.value, true);
	var nLinguaLogada = getDicCurrLang();

	strPars = '?nEmpresaID=' + glb_aEmpresaData[0];
	strPars += '&controle=' + 'Excel';
	strPars += '&sEmpresaFantasia=' + glb_aEmpresaData[3];
	strPars += '&nLinguaLogada=' + nLinguaLogada;
	strPars += '&selImpostoID=' + (selImpostoID.selectedIndex <= 0 ? 'NULL' : selImpostoID.value);
	strPars += '&chkDiferencialAliquota=' + ((chkDiferencialAliquota.checked) ? '1' : '0');
	strPars += '&sDataInicio=' + sDataInicio;
	strPars += '&sDataFim=' + sDataFim;
	strPars += '&txtFiltro=' + (trimStr(txtFiltro.value) == '' ? 'NULL' : '\'' + trimStr(txtFiltro.value) + '\'');

	lockControlsInModalWin(true);
	var frameReport = document.getElementById("frmReport");
	frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
	frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/ReportsGrid_ApuracaoImpostos.aspx' + strPars;
}

function chkDiferencialAliquota_onclick()
{
    adjustLabelsCombos();
    fg.Rows = 1;
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

function reports_onreadystatechange() {

    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
        (frameReport.contentWindow.document.readyState == 'interactive') ||
        (frameReport.contentWindow.document.readyState == 'complete')) {

        lockControlsInModalWin(false);
    }
}