/********************************************************************
modalincluiservicos.js

Library javascript para o modalincluiservicos.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************
var modPesqincluiservicos_Timer = null;
var glb_nTipoConceitoID = null;
var glb_nDOS = 0;
var glb_bAtivoConsumo = false;
var glb_nRow = 0;

var dsoLocalidades = new CDatatransport('dsoLocalidades');
var dsoServicos = new CDatatransport('dsoServicos');
var dsoIncluir = new CDatatransport('dsoIncluir');
var dsoAtivoConsumo = new CDatatransport('dsoAtivoConsumo');
var dsoTipoConceito = new CDatatransport('dsoTipoConceito');
var dsoFinalidade = new CDatatransport('dsoFinalidade');
var dsoUnidade = new CDatatransport('dsoUnidade');
// FINAL DE VARIAVEIS GLOBAIS ***************************************
/********************************************************************
RELACAO DAS FUNCOES

NOTA: Ver tambem o arquivo commonincluiservicos.js

FUNCOES GERAIS:
window_onload()
setupPage()
txtPesquisa_onKeyPress()
txtPesquisa_ondigit(ctl)
btnFindPesquisa_onclick(ctl)
btn_onclick(ctl)
startPesq(strPesquisa)
dataToReturn()

Eventos de grid particulares desta janela:
fg_ModPesqincluiservicosDblClick()
fg_ModPesqincluiservicosKeyPress(KeyAscii)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // ajusta o body do html

    with (modalincluiservicosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    tipoConceito();
    // configuracao inicial do html
    

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    //fg.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Incluir Itens ', 1);

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 10;
    modalFrame.style.left = 10;

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;


    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
    }

    // ajusta o divPessoa
    elem = window.document.getElementById('divControles');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP - 11;
        top = parseInt(document.getElementById('divMod01').style.height, 10) + ELEM_GAP - 10;
        width = parseInt(document.getElementById('divMod01').style.width);
        temp = parseInt(width);
        height = MAX_FRAMEHEIGHT_OLD - 70;
    }

    with (divFG.style) {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = (ELEM_GAP * 2) - 9;
        top = (ELEM_GAP * 3) + 51;
        width = modWidth - 23;
        height = MAX_FRAMEHEIGHT_OLD - 170;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10) - 2;
        height = parseInt(divFG.style.height, 10) - 2;
    }

    elem = window.document.getElementById('divRodape');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = (ELEM_GAP * 2) - 9;
        top = parseInt(document.getElementById('divFG').style.height, 10) + 90;
        width = parseInt(document.getElementById('divFG').style.width) - 10;
        temp = parseInt(width);
        height = 120;
    }

    with (txtDescricao.style) {
        left = 0;
        top = 0;
        width = parseInt(divRodape.style.width, 10);
        height = parseInt(divRodape.style.height, 10);
    }

    glb_nTipoConceitoID = dsoTipoConceito.recordset['TipoConceitoID'].value;

    if ((glb_nTipoConceitoID == 309) || (glb_nTipoConceitoID == 318))  //NCM - Ativo/Consumo -- FRG 22/05/2015

    {
        lblMunicipio.style.visibility = 'hidden';
        txtMunicipio.style.visibility = 'hidden';
        lblUF.style.visibility = 'hidden';
        txtUF.style.visibility = 'hidden';

        adjustElementsInForm([['lblArgumento', 'txtArgumento', 97, 1],
                            ['lblUF', 'txtUF', 4, 1],
                            ['lblMunicipio', 'txtMunicipio', 30, 1]],
                            null, null, true);
    }
    else //Servico Estadual
    {
        adjustElementsInForm([['lblUF', 'txtUF', 4, 1],
                            ['lblMunicipio', 'txtMunicipio', 30, 1],
                            ['lblArgumento', 'txtArgumento', 30, 1]],
                            null, null, true);
    }

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.style.width, 10) * 18;
    fg.Redraw = 2;

    txtMunicipio.disabled = true;
    txtUF.disabled = true;

    btnCanc.value = 'Cancelar';
    btnCanc.style.visibility = 'hidden';

    btnListar.onclick = btnListar_onclick;
    btnListar.style.top = txtArgumento.offsetTop + 25;
    if ((glb_nTipoConceitoID == 309) || (glb_nTipoConceitoID == 318))  //NCM - Ativo/Consumo
        btnListar.style.left = (txtArgumento.offsetLeft + lblArgumento.offsetWidth + 660); // 330;
    else // Servico Federal
        btnListar.style.left = (txtArgumento.offsetLeft + lblArgumento.offsetWidth + 195); // 330;

    btnCanc.style.left = (txtArgumento.offsetLeft + lblArgumento.offsetWidth + 720); // 330;

    btnOK.style.top = btnListar.offsetTop;
    btnOK.style.left = (btnListar.offsetLeft + btnListar.offsetWidth + 10);
    btnOK.disabled = true;
    btnOK.value = 'Incluir';
    //btnOK.style.visibility = 'hidden';

    txtArgumento.onkeypress = argumentoKeyPress;

    pessoaLocalidade();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqincluiservicosDblClick() {
    ;
}

function incluiServicos(nRow) {
    var strPars = '';

    if (isNaN(nRow))
        nRow = fg.Row;

    if ((glb_nTipoConceitoID == 309) || (glb_nTipoConceitoID == 318)) //NCM - Ativo/Consumo -- FRG 22/05/2015
    {
        var nServicoID = "";
        var nProdutoID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ConceitoID*'));
        var sDescricao = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'DescricaoItem'));
        var nValor = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ValorUnitario'));
        var nQuantidade = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'Quantidade'));
        var nFinalidadeID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'FinalidadeID'));
        var nUnidadeID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'UnidadeID'));
        var nPedidoID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['PedidoID'].value");

        nValor = nValor.replace(',', '.');

        strPars = '?nPedidoID=' + escape(nPedidoID);
        strPars += '&nProdutoID=' + escape(nProdutoID);
        strPars += '&sDescricao=' + escape(sDescricao);
        strPars += '&nServicoID=' + escape(nServicoID);
        if (nFinalidadeID == null) {
            strPars += '&nFinalidadeID=';
        }
        else {
            strPars += '&nFinalidadeID=' + escape(nFinalidadeID);
        }
        if (nUnidadeID.length == 0) {
            strPars += '&nUnidadeID=321';
        }
        else {
            strPars += '&nUnidadeID=' + escape(nUnidadeID);
        }
        strPars += '&nValor=' + escape(nValor);
        strPars += '&nQuantidade=' + escape(nQuantidade);
    }
    else //Servi�o Federal
    {
        var nServicoID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ConServicoID*'));
        var nProdutoID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ConceitoID*'));
        var sDescricao = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'DescricaoItem'));
        var nValor = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ValorUnitario'));
        var nPedidoID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['PedidoID'].value");
        var nTipoPedidoID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TipoPedidoID'].value");
        var nQuantidade = 1;
        var sCP = txtArgumento.value; //C�digo do Prestador
        var sCF = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Federal'));// C�digo Federal
        var sCT = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Municipal'));; //C�digo do Tomador

        var sDescricaoPessoa = '(CP:' + sCP + ')(CF:' + sCF + ')(CT:' + sCT + ')';

        // Se o pedido for de "saida", nao geraremos a nomenclatura acima. FRG - 17/01/2018.(E-mail: RES: Publica��o: EstadoID do servi�o. - qua 17/01/2018 08:40).
        if (nTipoPedidoID == 602) {
            sDescricaoPessoa = '';
        }

        nValor = nValor.replace(',', '.');

        sDescricao = sDescricao + sDescricaoPessoa;

        strPars = '?nPedidoID=' + escape(nPedidoID);
        strPars += '&nProdutoID=' + escape(nProdutoID);
        strPars += '&sDescricao=' + escape(sDescricao);
        strPars += '&nServicoID=' + escape(nServicoID);
        strPars += '&nFinalidadeID=';
        strPars += '&nValor=' + escape(nValor);
        strPars += '&nQuantidade=' + escape(nQuantidade);
    }

    dsoIncluir.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/incluiservicopedido.aspx' + strPars;
    dsoIncluir.ondatasetcomplete = incluiServicos_DSC;
    dsoIncluir.refresh();
}

function incluiServicos_DSC() {
    if ((glb_nTipoConceitoID == 309) || (glb_nTipoConceitoID == 318)) { //NCM - Ativo/Consumo - FRG 22/05/2015
        AtivoConsumo();
    }
    else {
        ;
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);
    var sMsg = '';

    if (ctl.id == btnOK.id) {

        var sConceito;

        if (glb_nTipoConceitoID == 309)
            sConceito = 'NCM';
        else
            sConceito = 'Servi�o';


        for (i = 1; i < fg.Rows; i++) {
            var nRow = i;

            if ((glb_nTipoConceitoID == 309) || (glb_nTipoConceitoID == 318)) {
                if ((fg.TextMatrix(nRow, getColIndexByColKey(fg, 'DescricaoItem')) <= 0) && (fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Quantidade')) > 0))
                    sMsg += '- Descri��o do Item\n';

                if ((fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Quantidade')) > 0) && (fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ValorUnitario')) <= 0))
                    sMsg += '- Valor Unit';

                if ((fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Quantidade')) > 0) && (fg.TextMatrix(nRow, getColIndexByColKey(fg, 'FinalidadeID')) <= 0))
                    sMsg += '- Finalidade';
            }

            sMsg = (sMsg != '' ? 'Selecione o ' + sConceito + ' e informe:\n' : '') + sMsg;
        }

        if (sMsg != '') {
            if (window.top.overflyGen.Alert(sMsg) == 1)
                lockControlsInModalWin(false);
            return null;
        }
        else {
            if ((glb_nTipoConceitoID == 309) || (glb_nTipoConceitoID == 318)) {
                for (glb_nRow = 1; glb_nRow < fg.Rows; glb_nRow++) {

                    if (fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'Quantidade')) > 0) {
                        incluiServicos(glb_nRow);
                    }
                }
            }
            else {
                incluiServicos(fg.Row);
            }
        }




        if ((glb_nTipoConceitoID == 309) || (glb_nTipoConceitoID == 318)) {
            lockControlsInModalWin(false);
        }
        else { // 1. O usuario clicou o botao OK
            sendJSMessage(getHtmlId(), JS_DATAINFORM, ('OK_CALLFORM_INF'), null);
        }

    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_INF'), null);
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqincluiservicosKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Alimenta campos de localidade
********************************************************************/
function pessoaLocalidade() {
    var nTipoPedidoID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TipoPedidoID'].value");
    var sPessoaID;
    var bEhImportacao;
    var sEmpresaID;


    // Deve sempre considerar o prestador. BJBN 15/12/2014
    //Corre��o solicitada pela Mazars. FRG - 26/06/2017.
    sPessoaID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['PessoaID'].value");
    sEmpresaID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EmpresaID'].value");

    setConnection(dsoLocalidades);

    if (nTipoPedidoID == 602) {
        sPessoaID = sEmpresaID;
    }
    
    dsoLocalidades.SQL = 'SELECT dbo.fn_Localidade_Dado(dbo.fn_Pessoa_Localidade(' + sPessoaID + ', 2, NULL, NULL), 2) AS UF, ' +
                            'dbo.fn_Localidade_Dado(dbo.fn_Pessoa_Localidade(' + sPessoaID + ', 3, NULL, NULL), 1) AS Municipio';
    
    dsoLocalidades.ondatasetcomplete = pessoaLocalidade_DSC;
    dsoLocalidades.refresh();
}

function pessoaLocalidade_DSC() {
    if ((!dsoLocalidades.recordset.BOF) && (!dsoLocalidades.recordset.EOF)) {
        txtUF.value = dsoLocalidades.recordset['UF'].value;
        txtMunicipio.value = dsoLocalidades.recordset['Municipio'].value;
    }
}

/********************************************************************
Lista servi�os no click do bot�o listar
********************************************************************/
function btnListar_onclick() {
    if ((glb_nTipoConceitoID == 309) || (glb_nTipoConceitoID == 318)) //NCM - Ativo/Consumo
        AtivoConsumo();
    else //Servi�o Federal
        Servicos();

    disableOK();
}

/********************************************************************
Lista servi�os quando usuario pressiona enter
********************************************************************/
function argumentoKeyPress() {
    if (event.keyCode == 13) {
        if ((glb_nTipoConceitoID == 309) || (glb_nTipoConceitoID == 318)) //NCM - Ativo/Consumo
            AtivoConsumo();
        else //Servi�o Federal
            Servicos();

        disableOK();
    }
}

function extorno_DSC() {
    var sMensagem = dsoExtorno.recordset['Resultado'].value;

    if (sMensagem != '')
        window.top.overflyGen.Alert(sMensagem);
    else
        window.top.overflyGen.Alert('Financeiro(s) extornado(s) com sucesso!');

    Financeiros();
}

/*******************************************************************
Select que preenche o grid
*******************************************************************/
function Servicos() {
    txtArgumento.value = trimStr(txtArgumento.value);

    var nTipoPedidoID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TipoPedidoID'].value");
    var sPessoaID;

    var sArgumento = txtArgumento.value;

    // Deve sempre considerar o prestador. BJBN 15/12/2014
    //Corre��o solicitada pela Mazars. FRG - 26/06/2017.
    sPessoaID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['PessoaID'].value");
    sEmpresaID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EmpresaID'].value");

    // Se for pedido de saida, utilizar o municipio da Empresa. FRG - 12/01/2018.
    if (nTipoPedidoID == 602) {
        sPessoaID = sEmpresaID;
    }

    var sWhere = '(dbo.fn_Produto_Descricao2(a.ConServicoID, NULL, 13) + CONVERT(VARCHAR, a.ConceitoID) LIKE ' + '\'' + '%' + sArgumento + '%' + '\'))';

    setConnection(dsoServicos);

    // Email: Alcateia RJ, De: Fiscal <fiscal@alcateia.com.br>, Data: seg 04/12/2017 16:59. FRG - 04/12/2017.
    // Verifica se o c�digo do cliente existe no Overfly;
    dsoServicos.SQL = 'SELECT a.ConceitoID, c.RecursoAbreviado, dbo.fn_Produto_Descricao2(a.ConServicoID, NULL, 13) AS Servico, Space(0) AS DescricaoItem, a.AliquotaISS, ' +
                            '0.00 AS ValorUnitario, b.Conceito AS Federal, a.Codigo AS Municipal, a.ConServicoID ' +
                        'FROM Conceitos_Servicos a WITH(NOLOCK) ' +
		                    'INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ConceitoID) ' +
		                    'INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = b.EstadoID) ' +
                        'WHERE ((a.LocalidadeID =  dbo.fn_Pessoa_Localidade(' + sPessoaID + ', 3, NULL, NULL)) AND (b.EstadoID = 2) AND (b.TipoConceitoID = 320) AND (a.EstadoID = 2) AND ' +
                                sWhere;


    dsoServicos.ondatasetcomplete = financeirosPessoa_DSC;
    dsoServicos.refresh();
   
}

function financeirosPessoa_DSC() {

    if (((dsoServicos.recordset.BOF) && (dsoServicos.recordset.EOF))) {
        window.top.overflyGen.Alert('C�digo de servi�o n�o est� cadastrado!');
        lockControlsInModalWin(false);
        return;
    }
    else {
        txtArgumento.value = trimStr(txtArgumento.value);

        var nTipoPedidoID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TipoPedidoID'].value");
        var sPessoaID;
        
       // var sArgumentoOriginal = txtArgumento.value;
        var sArgumento = txtArgumento.value;
        var sArgumentoFederal;
        //var nConceitoID = dsoServicos.recordset['ConceitoID'].value;
        var nConceitos = '';
        var nCidadePessoa;
        var nLocalidadeBusca;

        // Inicio;
        if (!(dsoServicos.recordset.BOF && dsoServicos.recordset.EOF)) {
            dsoServicos.recordset.MoveFirst();

            while (!dsoServicos.recordset.EOF) {
                fg.Row = fg.Rows - 1;

                fg.AddItem('', fg.Row + 1);

                if (fg.Row < (fg.Rows - 1))
                    fg.Row++;

                nConceitos = nConceitos + dsoServicos.recordset['ConceitoID'].value;

                dsoServicos.recordset.MoveNext();

                if (!(dsoServicos.recordset.EOF))
                    nConceitos = nConceitos + ',';
            }
        }
        // Fim;

        sPessoaID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EmpresaID'].value");
        nCidadePessoa = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['CidadePessoa'].value");
        /*
        sArgumentoFederal = 'SELECT aa.ConceitoID FROM Conceitos_Servicos aa WITH(NOLOCK)  ' +
                                'WHERE (dbo.fn_Produto_Descricao2(aa.ConServicoID, NULL, 13) + CONVERT(VARCHAR, aa.ConceitoID) LIKE ' + '\'' + '%' + sArgumento + '%' + '\') AND ' + 
                                '(aa.ConceitoID = a.ConceitoID))';
        */

        sArgumentoFederal = 'SELECT aa.ConServicoID FROM Conceitos_Servicos aa WITH(NOLOCK)  ' +
                                'WHERE (aa.ConceitoID IN ( ' + nConceitos + ')) AND aa.LocalidadeID = a.LocalidadeID)';
        
        //var sWhere = '(dbo.fn_Produto_Descricao2(a.ConServicoID, NULL, 13) + CONVERT(VARCHAR, a.ConceitoID) LIKE ' + '\'' + '%' + sArgumento + '%' + '\'))';
        var sWhere = '(a.ConServicoID IN (' + sArgumentoFederal + '))';

        setConnection(dsoServicos);

        //Estocagem do pedido no RJ
        //SE O PRESTADOR ESTIVER NO MUNICIPIO "RIO DE JANEIRO", ACEITAREMOS A NOTA TEMPORARIAMENTE COM O C�DIGO DO MUNICIPIO DO RJ; - FRG - 19/01/2018.
        //SE O PRESTADOR N�O ESIVER NO MUNICIPIO DO "RIO DE JANEIRO", N�O ACEITAREMOS A NOTA; - FRG - 19/01/2018.
        // Devido a mudan�a de endere�o no RJ de 13649 (RJ) para 13642 (Queimados), esta tratativa ser� tempor�ria. FRG - 04/12/2017.
        // Email: Alcateia RJ, De: Fiscal <fiscal@alcateia.com.br>, Data: seg 04/12/2017 16:59. FRG - 04/12/2017.
        // Altera��o: Empresas do RJ dever�o utilizar a localidade do cadastro (Queimados). FRG - 04/04/2018.     
            dsoServicos.SQL = 'SELECT a.ConceitoID, c.RecursoAbreviado, dbo.fn_Produto_Descricao2(a.ConServicoID, NULL, 13) AS Servico, Space(0) AS DescricaoItem, a.AliquotaISS, ' +
                                     '0.00 AS ValorUnitario, b.Conceito AS Federal, a.Codigo AS Municipal, a.ConServicoID ' +
                                'FROM Conceitos_Servicos a WITH(NOLOCK) ' +
                                    'INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ConceitoID) ' +
                                    'INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = b.EstadoID) ' +
                                'WHERE ((a.LocalidadeID =  dbo.fn_Pessoa_Localidade(' + sPessoaID + ', 3, NULL, NULL)) AND (b.EstadoID = 2) AND (b.TipoConceitoID = 320) AND (a.EstadoID = 2) AND  ' +
                                        sWhere;
       
        dsoServicos.ondatasetcomplete = financeiros_DSC;
        dsoServicos.refresh();
    }
}

function financeiros_DSC() {

    startGridInterface(fg);
    fg.FontSize = '8';
    fg.FrozenCols = 0;

    headerGrid(fg, ['ID',
                    'E',
                    'Servi�o',
                    'Descri��o do Item' + replicate(' ', 70),
                    'ISS',
                    'Valor Unit       ',
                    'Federal',
                    'Municipal',
                    'ConServicoID'], [4, 6, 7, 8 ]);

    fillGridMask(fg, dsoServicos, ['ConceitoID*',
                                    'RecursoAbreviado*',
                                    'Servico*',
                                    'DescricaoItem',
                                    'AliquotaISS*',
                                    'ValorUnitario',
                                    'Federal',
                                    'Municipal',
                                    'ConServicoID*'],
				  				    ['', '', '', '', '999999999.99', '999999999.99', '', '', ''],
								    ['', '', '', '', '###,###,###.00', '###,###,###.00', '', '', '']);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, 2);

    fg.Redraw = 2;
    fg.Editable = true;

    lockControlsInModalWin(false);

    // destrava botao Incluir se tem linhas no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
    }
}

function AtivoConsumo() {
    txtArgumento.value = trimStr(txtArgumento.value);

    var sArgumento = txtArgumento.value;
    var sPessoaID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['PessoaID'].value");
    var nEmpresaID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EmpresaID'].value");
    var nPedidoID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['PedidoID'].value");
    var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TransacaoID\'].value');
    var sWhere = 'dbo.fn_Produto_Descricao2(a.ConceitoID, NULL, 12) + CONVERT(VARCHAR, a.ConceitoID) LIKE ' + '\'' + '%' + sArgumento + '%' + '\'';
    var sContribuinte = '~dbo.fn_Pessoa_Incidencia(' + nEmpresaID + ',-' + sPessoaID + ')';

    glb_nDOS = 3;

    setConnection(dsoFinalidade);
    dsoFinalidade.SQL = 'SELECT a.ItemID AS FinalidadeID,  a.ItemMasculino AS Finalidade ' +
                                'FROM dbo.TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                                        'INNER JOIN dbo.Pessoas b WITH(NOLOCK) ON a.Filtro LIKE \'%(\' + CONVERT(VARCHAR(16), b.ClassificacaoID) +\')%\' ' +
                                'WHERE (a.EstadoID = 2 AND a.TipoID = 418) ' +
                                        'AND b.PessoaID = ' + sPessoaID + ' ' +
                                        'AND (a.Filtro LIKE \'%{' + nTransacaoID + '}%\') ' +
                                        'AND (((' + sContribuinte + ' = 1) AND ((a.Filtro LIKE \'%<CB1>%\') OR (a.Filtro LIKE \'%<CB@_>%\' ESCAPE \'@\'))) ' +
			                            'OR ((' + sContribuinte + ' = 0) AND ((a.Filtro LIKE \'%<CB0>%\') OR (a.Filtro LIKE \'%\<CB@_>%\' ESCAPE \'@\')))) ' +
                                'ORDER BY a.Ordem';
    dsoFinalidade.ondatasetcomplete = AtivoConsumo_DSC;
    dsoFinalidade.Refresh();

    setConnection(dsoAtivoConsumo);

    if ((nTransacaoID == 231) || (nTransacaoID == 241)) {
        dsoAtivoConsumo.SQL = 'SELECT dbo.fn_Produto_Descricao2(a.ConceitoID, NULL, 12) AS NCM, b.RecursoAbreviado, a.ConceitoID, SPACE(0) AS DescricaoItem , ' +
                                     '0.00 AS Quantidade, 0.00 AS ValorUnitario, 0.00 AS ValorTotal, NULL AS FinalidadeID, NULL AS UnidadeID, a.Observacoes AS DescricaoRodape  ' +
                                 'FROM Conceitos a WITH(NOLOCK) ' +
                                     'INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) ' +
                                 'WHERE a.TipoConceitoID IN(318) AND a.EstadoID = 2 AND ' + sWhere +
                                 ' ORDER BY dbo.fn_Produto_Descricao2(a.ConceitoID, NULL, 12) ';
    }

    if ((nTransacaoID != 231) && (nTransacaoID != 241)) {
        dsoAtivoConsumo.SQL = 'SELECT dbo.fn_Produto_Descricao2(a.ConceitoID, NULL, 12) AS NCM, b.RecursoAbreviado, a.ConceitoID, SPACE(0) AS DescricaoItem , ' +
                                  '0.00 AS Quantidade, 0.00 AS ValorUnitario, 0.00 AS ValorTotal, NULL AS FinalidadeID, NULL AS UnidadeID, a.Observacoes AS DescricaoRodape  ' +
                              'FROM Conceitos a WITH(NOLOCK) ' +
                                  'INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) ' +
                              'WHERE a.TipoConceitoID IN(309) AND a.EstadoID = 2 AND ((a.Observacao LIKE \'%AI%\') OR (a.Observacao LIKE \'%MUC%\')) AND ' + sWhere +
                              ' ORDER BY dbo.fn_Produto_Descricao2(a.ConceitoID, NULL, 12) ';
    }
    dsoAtivoConsumo.ondatasetcomplete = AtivoConsumo_DSC;
    dsoAtivoConsumo.refresh();

    setConnection(dsoUnidade);
    dsoUnidade.SQL = 'SELECT 0 AS UnidadeID, SPACE(0) AS Unidade ' +
                        'UNION ALL ' +
                        'SELECT ItemID AS UnidadeID, ItemMasculino AS Unidade ' +
                            'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                            'WHERE ((TipoID = 103) AND (EstadoID = 2)) ';

    dsoUnidade.ondatasetcomplete = AtivoConsumo_DSC;
    dsoUnidade.Refresh();

}

function AtivoConsumo_DSC() {
    glb_nDOS--;

    if (glb_nDOS > 0)
        return;

    startGridInterface(fg);
    fg.FontSize = '8';
    fg.FrozenCols = 0;

    fg.ExplorerBar = 4;
    fg.ExplorerBar = 5;

    headerGrid(fg, ['ID',
                    'E',
                    'NCM',
                    'Descri��o do Item',
                    'Quant',
                    'Valor Unit',
                    'Valor Total',
                    'Finalidade',
                    'Unidade',
                    'DescricaoRodape'], [9]);

    fillGridMask(fg, dsoAtivoConsumo, ['ConceitoID*',
                                        'RecursoAbreviado*',
                                        'NCM*',
                                        'DescricaoItem',
                                        'Quantidade',
                                        'ValorUnitario',
                                        'ValorTotal*',
                                        'FinalidadeID',
                                        'UnidadeID',
                                        'DescricaoRodape'],
				  				        ['', '', '', '', '999999.9999', '999999999.99', '999.999.999.99', '', '', ''],
								        ['', '', '', '', '######.0000', '#########.00', '###,###,###.00', '', '', '']);

    insertcomboData(fg, getColIndexByColKey(fg, 'FinalidadeID'), dsoFinalidade, 'Finalidade', 'FinalidadeID');
    insertcomboData(fg, getColIndexByColKey(fg, 'UnidadeID'), dsoUnidade, 'Unidade', 'UnidadeID');

    alignColsInGrid(fg, [0]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, 6);

    fg.Redraw = 2;
    fg.Editable = true;

    lockControlsInModalWin(false);

    // destrava botao Incluir se tem linhas no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
    }

    glb_bAtivoConsumo = true;

}

function js_IncluiServico_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {
    if (glb_bAtivoConsumo) {

        var sDescricaoRodape = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'DescricaoRodape'));

        if (sDescricaoRodape == '') {
            txt.txtDescricao.style.visibility = 'hidden';
        }
        else {
            txtDescricao.readOnly = true;
            txtDescricao.value = sDescricaoRodape;
        }
    }
}

function js_ModalIncluiServicos_BeforeEdit(fg, row, col) {
    if ((glb_nTipoConceitoID == 309) || (glb_nTipoConceitoID == 318)) {
        fg.EditMaxLength = 80;
    }
}


function js_ModalIncluiServicos_AfterEdit(Row, Col) {
    if ((glb_nTipoConceitoID == 309) || (glb_nTipoConceitoID == 318)) {
        var nColValor = getColIndexByColKey(fg, 'ValorUnitario');
        var nColQtd = getColIndexByColKey(fg, 'Quantidade');

        if ((fg.col == nColValor) || (fg.col == nColQtd))
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

        if ((fg.col == nColValor) || (fg.col == nColQtd))
            calculaTotal();
    }

    else {
        var nColValor = getColIndexByColKey(fg, 'ValorUnitario');

        if (fg.Editable) {
            if (fg.col == nColValor)
                fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
        }
    }

    disableOK();
}

function disableOK() {
    var nColValor = getColIndexByColKey(fg, 'ValorUnitario');

    if (fg.rows < 2)
        btnOK.disabled = true;
    else
        btnOK.disabled = false;
}

/********************************************************************
Alimenta ClassificacaoID
********************************************************************/
function tipoConceito() {
    var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TransacaoID\'].value');

    setConnection(dsoTipoConceito);

    dsoTipoConceito.SQL = 'SELECT TOP 1 TipoConceitoID ' +
                            'FROM Operacoes_Conceitos WITH(NOLOCK) ' +
                            'WHERE OperacaoID = ' + nTransacaoID;

    dsoTipoConceito.ondatasetcomplete = tipoConceito_DSC;
    dsoTipoConceito.refresh();
}

function tipoConceito_DSC() {
    setupPage();
}

function calculaTotal() {
    var nValorTotal = 0;

    if ((!isNaN(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Quantidade')))) &&
         (!isNaN(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ValorUnitario')))))
        nValorTotal = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Quantidade')) * fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ValorUnitario'));

    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorTotal*')) = nValorTotal;
}