/********************************************************************
modalpagamentocomissoes.js

Library javascript para o modalpagamentocomissoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_BuildingFgAssociacoes = false;
var glb_BuildingFgComissao = false;
var glbPedidoID; 
var glbVerifica;
var glbRow;
var glbLinhaVerifica = 0;
var glbLinhaF = 0;
var glbValor = 0;
var glbValorF = 0;
var glbID = 0;
var glbValorAssociar;
var glbMensagemVerificaValor;
var glb_saldoAnterior = 0;
var glb_verificasaldo = 0;
var glb_associarComissaoDevolucao = 1;
var glb_associarFinanceiros = 2;
var glb_ListaDevolucoes = false;
var glb_PreencheParceiro = false;

// FINAL DE VARIAVEIS GLOBAIS ***************************************
var EmpresaLogadaID = getCurrEmpresaData();
var dsoGrid = new CDatatransport('dsoGrid');
var dsoGera = new CDatatransport('dsoGera');
var dsoGeraOcorrenciaDesconto = new CDatatransport('dsoGeraOcorrenciaDesconto');
var dsoParceiroComissao = new CDatatransport('dsoParceiroComissao');
var dsoParceiroFinanceiro = new CDatatransport('dsoParceiroFinanceiro');
var dsoTpComissao = new CDatatransport('dsoTpComissao');
var dsoGridFR = new CDatatransport('dsoGridFR');
var dsoGridComissoesDevolucao = new CDatatransport('dsoGridComissoesDevolucao');
var dsoVerificaDevolucoes = new CDatatransport('dsoVerificaDevolucoes');
var dsoGrid = new CDatatransport('dsoGrid');
/********************************************************************
RELACAO DAS FUNCOES

NOTA: Ver tambem o arquivo commongerargaregnre.js

FUNCOES GERAIS:
window_onload()
setupPage()
txtPesquisa_onKeyPress()
txtPesquisa_ondigit(ctl)
btnFindPesquisa_onclick(ctl)
btn_onclick(ctl)
startPesq(strPesquisa)
dataToReturn()

Eventos de grid particulares desta janela:
fg_modalgerargaregnreDblClick()
fg_modalgerargaregnreKeyPress(KeyAscii)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // ajusta o body do html

    with (modalpagamentocomissoesBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Pagamento de Comiss�es ', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var paramBtnListar = 0;
    var paramBtnGerAss = 0;

    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
    }

    adjustElementsInForm([['lbltpComissao', 'selTipoComissaoID', 22, 1, -5],
                          ['lblChavePesquisa', 'selChavePesquisa', 13, 2, -5],
                          ['lblArgumento', 'txtArgumento', 12, 2, -2],
                          ['btnPesqParceiro', 'btn', 20, 2, -1],
                          ['lblParceiroComissaoID', 'selParceiroComissaoID', 22, 2, -5],
                          ['lblValor', 'txtValor', 10, 2, -5],
                          ['btnListar', 'btn', 50, 2, -2, 25],
                          ['lblFiltroPesquisa', 'selFiltroPesquisa', 13, 2, 5, -30],
                          ['lblPesquisa', 'txtPesquisa', 6, 2, 15],
                          ['btnPesquisa', 'btn', 20, 2, -1],
                          ['lblParceiroFinanceiroID', 'selParceiroFinanceiroID', 22, 2, -10],
                          ['lblTipoFinanceiro', 'selTipoFinanceiro', 9, 2, -5],
    //['lblFinanceiro', 'txtFinanceiro', 9, 2, -15],
                          ['btnListar_Associacoes', 'btn', 50, 2]], null, null, true);

    selTipoComissaoID.onchange = selTipoComissaoID_onchange;
    selParceiroComissaoID.onchange = selParceiroComissaoID_onchange;
    selParceiroFinanceiroID.onchange = selParceiroFinanceiroID_onchange;
    selTipoFinanceiro.onchange = selTipoFinanceiro_onchange;
    selTipoAssociacao.onchange = selTipoAssociacao_onchange;
    selFiltroPesquisa.onchange = selFiltroPesquisa_onchange;

    txtValor.onkeypress = verifyNumericEnterNotLinked;
    txtValor.setAttribute('thePrecision', 11, 1);
    txtValor.setAttribute('theScale', 2, 1);
    txtValor.setAttribute('minMax', new Array(0, 9999999999));
    txtValor.setAttribute('verifyNumPaste', 1);

    /*txtFinanceiro.onkeypress = verifyNumericEnterNotLinked;
    txtFinanceiro.setAttribute('thePrecision', 11, 1);
    txtFinanceiro.setAttribute('theScale', 0, 1);
    txtFinanceiro.setAttribute('minMax', new Array(0, 9999999999));
    txtFinanceiro.setAttribute('verifyNumPaste', 1);*/

    adjust_adjustElementsInForm();
    txtPesquisa.onkeypress = txtPesquisa_onkeypress;
    txtArgumento.onkeypress = txtArgumento_onkeypress;

    with (divGridAssociacoes.style) {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = (ELEM_GAP * 2) + 490;
        top = (ELEM_GAP * 3) + 120;
        width = modWidth - 519;
        height = MAX_FRAMEHEIGHT_OLD - 195;
    }

    with (fgAssociacoes.style) {
        left = 0;
        top = 0;
        width = parseInt(divGridAssociacoes.style.width, 10);
        height = parseInt(divGridAssociacoes.style.height, 10);
        fgAssociacoes.Rows = 0;
    }

    // Ajuste do Grid Comissoes
    with (lblGridComissoes.style) {
        backgroundColor = 'transparent';
        width = 30;
        top = 107;
        left = 7;
    }

    // Ajuste do Grid Financeiro Recebimentos
    with (lblGridAssociacoes.style) {
        backgroundColor = 'transparent';
        width = 200;
        top = 107;
        left = 500;
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';
    btnCanc.value = 'Cancelar';

    selTipoFinanceiro.onchange = zeraGridfgAssociacoes;

    //fillCmbParceiroComissao();

    selParceiroComissaoID.disabled = (selParceiroComissaoID.length < 2);

    fillCmbFiltroPesquisa();

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fgComissoes.focus();
}

function zeraGridfgAssociacoes() {
    fgAssociacoes.Rows = 0;
}

function selTipoComissaoID_onchange() {
    //fillCmbParceiroComissao();
    adjust_adjustElementsInForm();
}

function adjust_adjustElementsInForm() {
    var frameRect;
    var modWidth = 0;

    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
    }

    clearComboEx(['selParceiroFinanceiroID']);
    selParceiroFinanceiroID_onchange();
    selParceiroFinanceiroID.disabled = true;
    txtPesquisa.value = '';
    //txtFinanceiro.value = '';

    clearComboEx(['selParceiroComissaoID']);
    selParceiroComissaoID_onchange();
    selParceiroComissaoID.disabled = true;
    txtArgumento.value = '';

    //Comiss�o interna
    if (selTipoComissaoID.value == 642) {
        fgComissoes.Rows = 0;
        fgAssociacoes.Rows = 0;

        divControlsAssociacoes.style.visibility = 'inherit';

        btnGerar.style.visibility = 'hidden';
        btnGerar.style.visibility = 'hidden';

        btnAssociar.style.visibility = 'inherit';
        btnAssociar.style.visibility = 'inherit';

        lblValor.style.visibility = 'hidden';
        txtValor.style.visibility = 'hidden';

        divGridAssociacoes.style.visibility = 'inherit';
        fgAssociacoes.style.visibility = 'inherit';

        if (selTipoAssociacao.value == 1)
            selTipoAssociacaoChanges();

        adjustElementsInForm([['lbltpComissao', 'selTipoComissaoID', 22, 1, -5],
                          ['lblTipoAssociacao', 'selTipoAssociacao', 20, 1, 310],
                          ['lblTipoFinanceiro', 'selTipoFinanceiro', 9, 1],
                          ['lblChavePesquisa', 'selChavePesquisa', 13, 2, -5],
                          ['lblArgumento', 'txtArgumento', 12, 2, -2],
                          ['btnPesqParceiro', 'btn', 20, 2, -1],
                          ['lblParceiroComissaoID', 'selParceiroComissaoID', 22, 2, -5],
                          ['btnListar', 'btn', 50, 2, 20, 25],
                          ['lblFiltroPesquisa', 'selFiltroPesquisa', 11, 2, 5, -30],
                          ['lblPesquisa', 'txtPesquisa', 12, 2, 5],
                          ['btnPesquisa', 'btn', 20, 2, -1],
                          ['lblParceiroFinanceiroID', 'selParceiroFinanceiroID', 22, 2, -5],
        //['lblFinanceiro', 'txtFinanceiro', 9, 2, -2],
                          ['btnListar_Associacoes', 'btn', 50, 2, 5]], null, null, true);

        with (divGridComissoes.style) {
            border = 'solid';
            borderWidth = 1;
            backgroundColor = 'transparent';
            left = (ELEM_GAP * 2) - 4;
            top = (ELEM_GAP * 3) + 120;
            width = modWidth - 522;
            height = MAX_FRAMEHEIGHT_OLD - 195;
        }

        with (fgComissoes.style) {
            left = 0;
            top = 0;
            width = parseInt(divGridComissoes.style.width, 10);
            height = parseInt(divGridComissoes.style.height, 10);
        }

        // Posiciona botao Listar
        /*        with (btnListar) 
        {
        style.width = 65;
        style.top = 47;
        style.left = modWidth - ELEM_GAP - 605;
        }*/

        // Posiciona botao Gerar
        with (btnGerar) {
            style.width = 90;
            style.top = 500;
            style.left = modWidth - ELEM_GAP - 585;
        }

        // Posiciona botao Associar - mesma posi��o do Gerar Pedido
        with (btnAssociar) {
            style.width = 90;
            style.top = 500;
            style.left = modWidth - ELEM_GAP - 530;
        }
    }
    else {
        fgComissoes.Rows = 0;
        fgAssociacoes.Rows = 0;

        divControlsComissao.style.visibility = 'inherit';
        divControlsAssociacoes.style.visibility = 'hidden';

        btnGerar.style.visibility = 'inherit';
        btnGerar.style.visibility = 'inherit';

        btnAssociar.style.visibility = 'hidden';
        btnAssociar.style.visibility = 'hidden';

        lblValor.style.visibility = 'inherit';
        txtValor.style.visibility = 'inherit';

        divGridAssociacoes.style.visibility = 'hidden';
        fgAssociacoes.style.visibility = 'hidden';

        adjustElementsInForm([['lbltpComissao', 'selTipoComissaoID', 22, 1, -5],
                          ['lblChavePesquisa', 'selChavePesquisa', 13, 2, -5],
                          ['lblArgumento', 'txtArgumento', 12, 2, -2],
                          ['btnPesqParceiro', 'btn', 20, 2, -1],
                          ['lblParceiroComissaoID', 'selParceiroComissaoID', 22, 2, -5],
                          ['lblValor', 'txtValor', 10, 2, -5],
                          ['btnListar', 'btn', 50, 2, -2, 25],
                          ['lblPesquisa', 'txtPesquisa', 6, 2, 15],
                          ['btnPesquisa', 'btn', 20, 2, -1],
                          ['lblParceiroFinanceiroID', 'selParceiroFinanceiroID', 22, 2, -8],
                          ['lblTipoFinanceiro', 'selTipoFinanceiro', 9, 2, -5],
        //['lblFinanceiro', 'txtFinanceiro', 9, 2, -15],
                          ['btnListar_Associacoes', 'btn', 50, 2]], null, null, true);

        with (divGridComissoes.style) {
            border = 'solid';
            borderWidth = 1;
            backgroundColor = 'transparent';
            left = (ELEM_GAP * 2) - 4;
            top = (ELEM_GAP * 3) + 120;
            width = modWidth - 40;
            height = MAX_FRAMEHEIGHT_OLD - 195;
        }

        with (fgComissoes.style) {
            left = 0;
            top = 0;
            width = parseInt(divGridComissoes.style.width, 10);
            height = parseInt(divGridComissoes.style.height, 10);
        }

        // Posiciona botao Listar
        /*        with (btnListar) 
        {
        style.width = 65;
        style.top = 47;
        style.left = modWidth - ELEM_GAP - 520;
        }*/

        // Posiciona botao Gerar
        with (btnGerar) {
            style.width = 90;
            style.top = 500;
            style.left = modWidth - ELEM_GAP - 530;
        }

        // Posiciona botao Associar - mesma posi��o do Gerar Pedido
        with (btnAssociar) {
            style.width = 90;
            style.top = 500;
            style.left = modWidth - ELEM_GAP - 530;
        }
    }
}

function fillCmbFiltroPesquisa() {
    var oOption = document.createElement("OPTION");
    var aOptions = new Array();
    var i = 1;

    clearComboEx(['selFiltroPesquisa']);

    aOptions[1] = 'Fantasia';

    if (selTipoAssociacao.value == glb_associarComissaoDevolucao) {
        aOptions[2] = 'Pedido';
        aOptions[3] = 'Nota Fiscal';
    }
    else {
        aOptions[2] = 'Financeiro';
        aOptions[3] = 'Duplicata';
    }

    do {
        oOption = document.createElement("OPTION");
        oOption.text = aOptions[i];
        oOption.value = i;
        selFiltroPesquisa.add(oOption);

        i++;
    } while (i < aOptions.length);
}

function fillCmbParceiroFinanceiro() {
    var sFiltro = '';

    if (txtPesquisa.value.length == 0) {
        if (window.top.overflyGen.Alert('Adicione um argumento para pesquisa') == 0)
            return null;

        return null;
    }

    /*Condi��o de Pesquisa relacionada � Comiss�o de Devolu��o*/
    if (selTipoAssociacao.value == glb_associarComissaoDevolucao) {
        switch (selFiltroPesquisa.value) {
            case "1":
                sFiltro = 'AND a.Fantasia LIKE \'' + txtPesquisa.value + '%\''; break;
            case "2":
                sFiltro = 'AND CONVERT(VARCHAR(12), c.PedidoID) = \'' + txtPesquisa.value + '\''; break;
            case "3":
                sFiltro = 'AND d.NotaFiscal LIKE \'' + txtPesquisa.value + '\''; break;
        }
    }
    /*Condi��o de Pesquisa relacionada ao Financeiro*/
    else {
        switch (selFiltroPesquisa.value) {
            case "1":
                sFiltro = 'AND a.Fantasia LIKE \'' + txtPesquisa.value + '%\''; break;
            case "2":
                sFiltro = 'AND CONVERT(VARCHAR(12), e.FinanceiroID) = \'' + txtPesquisa.value + '\''; break;
            case "3":
                sFiltro = 'AND e.Duplicata LIKE \'' + txtPesquisa.value + '%\''; break;
        }
    }

    setConnection(dsoParceiroFinanceiro);

    dsoParceiroFinanceiro.SQL = 'SELECT 0 as fldID, SPACE(0) AS fldName ' +
                    'UNION ' +
                      'SELECT DISTINCT TOP 50 a.PessoaID AS fldID, a.Fantasia + \' (\' + CONVERT(VARCHAR(16), a.PessoaID) + \')\' AS fldName ' +
	                        'FROM Pessoas a WITH(NOLOCK) ' +
                                'INNER JOIN dbo.Financeiro e WITH(NOLOCK) ON (a.PessoaID = e.ParceiroID) ' +
	                        'WHERE a.EstadoID <> 5 ' + sFiltro + ' ' +
	                        'ORDER BY fldName ';

    dsoParceiroFinanceiro.ondatasetcomplete = fillCmbParceiroFinanceiro_DSC;
    dsoParceiroFinanceiro.refresh();
}

function fillCmbParceiroFinanceiro_DSC() {
    var optionStr = '';
    var optionValue = '';
    var oOption = document.createElement("OPTION");

    clearComboEx(['selParceiroFinanceiroID']);

    while (!dsoParceiroFinanceiro.recordset.EOF) {
        optionStr = dsoParceiroFinanceiro.recordset['fldName'].value;
        optionValue = dsoParceiroFinanceiro.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selParceiroFinanceiroID.add(oOption);
        dsoParceiroFinanceiro.recordset.MoveNext();
    }

    selParceiroFinanceiroID.disabled = (selParceiroFinanceiroID.length < 2);
    selParceiroFinanceiroID_onchange();

    if (glb_PreencheParceiro)
        selParceiroFinanceiroID.value = selParceiroComissaoID.value;
    else if (selParceiroFinanceiroID.length == 2)
        selParceiroFinanceiroID.value = optionValue;

    glb_PreencheParceiro = false;
}

function fillCmbParceiroComissao() {
    var aEmpresa = getCurrEmpresaData();
    var nEmpresaID = aEmpresa[0];
    var nLenArgumento = txtArgumento.value.length;
    var sFiltro = '';

    /***************************
    value = 1 Fantasia
    value = 2 Nome
    value = 3 CNPJ
    value = 4 Pedido
    value = 5 Nota Fiscal
    ***************************/
    if (nLenArgumento > 0) {
        if (selChavePesquisa.value == 1)
            sFiltro = 'AND a.Fantasia LIKE \'' + txtArgumento.value + '%\'';
        else if (selChavePesquisa.value == 2)
            sFiltro = 'AND a.Nome LIKE \'' + txtArgumento.value + '%\'';
        else if (selChavePesquisa.value == 3)
            sFiltro = 'AND d.Numero = \'' + txtArgumento.value + '\'';
        else if (selChavePesquisa.value == 4)
            sFiltro = 'AND CONVERT(VARCHAR(12), f.PedidoID) = \'' + txtArgumento.value + '\'';
        else if (selChavePesquisa.value == 5)
            sFiltro = 'AND CONVERT(VARCHAR(12), g.NotaFiscal) = \'' + txtArgumento.value + '\'';
    }

    setConnection(dsoParceiroComissao);

    dsoParceiroComissao.SQL = 'SELECT 0 as fldID, SPACE(0) AS fldName ' +
                                    'UNION ' +
                                  'SELECT DISTINCT a.PessoaID AS fldID, a.Fantasia + \' (\' + CONVERT(VARCHAR(10), a.PessoaID) + \')\' AS fldName ' +
	                                    'FROM Pessoas a WITH(NOLOCK) ' +
	                                        'INNER JOIN Pessoas_Documentos d WITH(NOLOCK) ON (d.PessoaID = a.PessoaID) AND (d.TipoDocumentoID IN (101, 111)) ' +
	                                        'LEFT OUTER JOIN Pedidos_Parcelas e WITH(NOLOCK) ON (e.PessoaID = a.PessoaID) ' +
	                                        'LEFT OUTER JOIN Pedidos f WITH(NOLOCK) ON (f.PedidoID = e.PedidoID) ' +
	                                        'LEFT OUTER JOIN NotasFiscais g WITH(NOLOCK) ON (g.NotaFiscalID = f.NotaFiscalID) AND (g.Emissor = 1) ' +
	                                    'WHERE a.EstadoID = 2 ' + sFiltro + ' ' +
	                                    'ORDER BY fldName ';

    /*
    'SELECT DISTINCT a.PessoaID AS fldID, d.Fantasia AS fldName ' +
    'FROM fn_ParcelasComissoes_tbl(NULL, NULL, 1071) a ' +
    'INNER JOIN Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) ' +
    'INNER JOIN fn_Matriz_Filial_tbl(' + nEmpresaID + ') c ON (c.EmpresaID = b.EmpresaID) ' +
    'INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = a.PessoaID) ' +
    'INNER JOIN Pessoas_Documentos e WITH(NOLOCK) ON (e.PessoaID = d.PessoaID) AND (e.TipoDocumentoID IN (101, 111)) ' +
    'INNER JOIN NotasFiscais f WITH(NOLOCK) ON (f.PedidoID = b.PedidoID) AND (Emissor = 1) ' +
    'WHERE TipoParcelaID = ' + selTipoComissaoID.value + ' ' + sFiltro + ' ' +
    'ORDER BY fldName ';
    */
    dsoParceiroComissao.ondatasetcomplete = fillCmbParceiroComissao_DSC;
    dsoParceiroComissao.refresh();
}

function fillCmbParceiroComissao_DSC() {
    var optionStr = '';
    var optionValue = '';
    var oOption = document.createElement("OPTION");

    clearComboEx(['selParceiroComissaoID']);

    while (!dsoParceiroComissao.recordset.EOF) {
        optionStr = dsoParceiroComissao.recordset['fldName'].value;
        optionValue = dsoParceiroComissao.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selParceiroComissaoID.add(oOption);
        dsoParceiroComissao.recordset.MoveNext();
    }

    selParceiroComissaoID.disabled = (selParceiroComissaoID.length < 2);
    selParceiroComissaoID_onchange();

    if (selParceiroComissaoID.length == 2)
        selParceiroComissaoID.value = optionValue;
}

/*Grid de Comissoes*/
function fillGridData() {
    var bittpComissao;
    var aEmpresa = getCurrEmpresaData();
    var nEmpresaID = aEmpresa[0];
    var sFiltro = '';
    var sFiltro2 = '';

    if (selParceiroComissaoID.value <= 0) {
        if (window.top.overflyGen.Alert('Selecione um parceiro') == 0)
            return null;

        return null;
    }

    lockControlsInModalWin(true);

    // zera o grid
    fgComissoes.Rows = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);
    setConnection(dsoVerificaDevolucoes);

    if (selTipoComissaoID.value == 641) {
        bittpComissao = 1;
    }
    else {
        bittpComissao = 0;
        sFiltro2 = ' AND ValorParcela > 0 ';
    }

    switch (selChavePesquisa.value) {
        case "4":
            sFiltro = 'AND a.PedidoID = ' + txtArgumento.value + ' '; break;
        case "5":
            sFiltro = 'AND d.NotaFiscal = ' + txtArgumento.value + ' '; break;
    }

    dsoGrid.SQL = 'SELECT a.PedidoID, e.Fantasia AS Empresa, CONVERT(BIT, ' + bittpComissao + ') AS Aplicar, ' +
                         'c.Fantasia, d.NotaFiscal, d.dtNotaFiscal, a.ValorParcela, a.PedParcelaID, 0 AS Saldo, a.ValorParcela AS ValorParcelaOriginal ' +
		                    'FROM fn_ParcelasComissoes_tbl(NULL, NULL, 1071) AS a ' +
			                    'INNER JOIN Pedidos AS b WITH(NOLOCK) ON a.PedidoID = b.PedidoID ' +
			                    'INNER JOIN Pessoas AS c WITH(NOLOCK) ON b.PessoaID = c.PessoaID ' +
			                    'INNER JOIN NotasFiscais AS d WITH(NOLOCK) ON b.NotaFiscalID = d.NotaFiscalID ' +
			                    'INNER JOIN fn_Matriz_Filial_tbl(' + nEmpresaID + ') AS e ON (e.EmpresaID = b.EmpresaID) ' +
		                    'WHERE a.PessoaID = ' + selParceiroComissaoID.value + sFiltro + ' AND a.TipoParcelaID = ' + selTipoComissaoID.value + sFiltro2 +
		                    'ORDER BY a.Valorparcela DESC';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();

    /*dso que verifica se o Pareceiro selecionado possui comissao interna de devolu��o*/
    dsoVerificaDevolucoes.SQL = 'SELECT 1 as Parcela WHERE EXISTS( ' +
                                                        'SELECT  a.PedParcelaID ' +
                                                        'FROM    fn_ParcelasComissoes_tbl(NULL, NULL, 1071) AS a ' +
                                                                'INNER JOIN Pedidos AS b WITH ( NOLOCK ) ON a.PedidoID = b.PedidoID ' +
                                                                'INNER JOIN Pessoas AS c WITH ( NOLOCK ) ON b.PessoaID = c.PessoaID ' +
                                                                'INNER JOIN NotasFiscais AS d WITH ( NOLOCK ) ON b.NotaFiscalID = d.NotaFiscalID ' +
                                                                'INNER JOIN fn_Matriz_Filial_tbl (2) AS e ON ( e.EmpresaID = b.EmpresaID ) ' +
                                                                'INNER JOIN dbo.Pedidos_Parcelas f ON ( a.PedParcelaID = f.PedParcelaID ) ' +
                                                        'WHERE   a.PessoaID = ' + selParceiroComissaoID.value + ' ' +
                                                                'AND a.TipoParcelaID = 642 ' +
                                                                'AND a.ValorParcela < 0)';

    dsoVerificaDevolucoes.ondatasetcomplete = dsoVerificaDevolucoes_DSC;
    dsoVerificaDevolucoes.Refresh();

    //Comiss�o Interna - Ap�s a montagem das comissoes internas, monta o grid para as comissoes internas de devolu��o do mesmo parceiro
    if (selTipoComissaoID.value == 642 && selTipoAssociacao.value == glb_associarComissaoDevolucao) {
        selFiltroPesquisa.value = 1;
        //Campo 'Pesquisa' recebe o fantasia do Parceiro sem o ID
        txtPesquisa.value = selParceiroComissaoID[selParceiroComissaoID.selectedIndex].text.substr(0, selParceiroComissaoID[1].text.indexOf('(') - 1);
        glb_ListaDevolucoes = true;
        glb_PreencheParceiro = true;
        fillCmbParceiroFinanceiro();
        fillGridComissoesDevolucoes();
    }
}

function fillGridData_DSC() {
    var bgColorWhite;
    var bgColorWhite = 0xffffff;
    var y = 0;
    var aEsconde = new Array();

    glb_BuildingFgComissao = true;

    if (!(dsoGrid.recordset.EOF) || (dsoGrid.recordset.BOF)) {
        aEsconde[aEsconde.length] = [8];
        aEsconde[aEsconde.length] = [9];

        //Comissao Revenda
        if (selTipoComissaoID.value == 641)
            aEsconde[aEsconde.length] = [7];

        startGridInterface(fgComissoes);
        fgComissoes.FontSize = '8';
        fgComissoes.FrozenCols = 0;

        headerGrid(fgComissoes, ['Pedido',
	                             'Empresa',
	                             'Aplicar',
	                             'Cliente',
	                             'NF',
	                             'Data NF',
	                             'Valor',
	                             'Saldo',
	                             'ValorParcelaOriginal',
	                             'PedParcelaID'], aEsconde);
        fillGridMask(fgComissoes, dsoGrid, ['PedidoID*',
						                    'Empresa*',
						                    'Aplicar',
						                    'Fantasia*',
						                    'NotaFiscal*',
						                    'dtNotaFiscal*',
						                    gridComissaoValorParcela(),
						                    'Saldo*',
						                    'ValorParcelaOriginal',
						                    'PedParcelaID'],
					                       ['', '', '', '', '', '', '999999999.99', '999999999.99', '', ''],
					                       ['', '', '', '', '', '', '(###,###,##0.00)', '###,###,##0.00', '', '']);

        gridHasTotalLine(fgComissoes, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C']]);
        totalLine();

        var colValorParcela = getColIndexByColKey(fgComissoes, gridComissaoValorParcela());

        if (selTipoComissaoID.value == 641) {
            for (i = 2; i < fgComissoes.Rows; i++) {
                if (fgComissoes.ValueMatrix(i, colValorParcela) < 0) {
                    fgComissoes.Select(i, colValorParcela, i, colValorParcela);
                    fgComissoes.FillStyle = 1;
                    fgComissoes.CellForeColor = 0X0000FF;
                }
            }
        }

        fgComissoes.AutoSizeMode = 0;
        fgComissoes.AutoSize(0, fgComissoes.Cols - 1);

        fgComissoes.Editable = true;

        // Nao retirar o if abaixo, ela garante que o grid
        // so deixa editar a coluna
        if (fgComissoes.Rows > 1)
            fgComissoes.Col = getColIndexByColKey(fgComissoes, 'Aplicar');

        fgComissoes.Redraw = 0;

        //Comissao Interna
        if (selTipoComissaoID.value == 642) {
            for (y = 2; y < fgComissoes.Rows; y++) {
                fgComissoes.Cell(6, y, getColIndexByColKey(fgComissoes, 'Saldo*'), y, getColIndexByColKey(fgComissoes, 'Saldo*')) = bgColorWhite;
            }
        }

        fgComissoes.Redraw = 2;
    }

    lockControlsInModalWin(false);

    glb_BuildingFgComissao = false;
}


function fillGridComissoesDevolucoes() {
    var bittpComissao;
    var aEmpresa = getCurrEmpresaData();
    var nEmpresaID = aEmpresa[0];
    var nParceiroID;
    var sFiltro = '';

    if (!glb_ListaDevolucoes && selParceiroFinanceiroID.value <= 0) {
        if (window.top.overflyGen.Alert('Selecione um parceiro') == 0)
            return null;

        return null;
    }

    if (glb_ListaDevolucoes)
        nParceiroID = selParceiroComissaoID.value;
    else
        nParceiroID = selParceiroFinanceiroID.value;

    glb_ListaDevolucoes = false;

    lockControlsInModalWin(true);

    // zera o grid
    fgAssociacoes.Rows = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGridComissoesDevolucao);

    bittpComissao = 0;

    switch (selFiltroPesquisa.value) {
        case "2":
            sFiltro = "AND a.PedidoID = " + txtPesquisa.value; break;
        case "3":
            sFiltro = "AND d.NotaFiscal LIKE \'" + txtPesquisa.value + '%\''; break;
    }

    dsoGridComissoesDevolucao.SQL =
                                    'SELECT  a.PedidoID, ' +
		                                'a.PedParcelaID AS PedParcelaDevolucaoID, ' +
                                        'e.Fantasia AS Empresa, ' +
                                        'c.PessoaID, ' +
                                        'c.Fantasia, ' +
                                        'd.NotaFiscal, ' +
                                        'CONVERT(BIT, 0) AS OK, ' +
                                        '-f.ValorParcela AS ValorParcela, ' +
                                        'a.ValorParcela AS SaldoDevedor, ' +
                                        '0 AS Associar ' +
                                    'FROM    fn_ParcelasComissoes_tbl(NULL, NULL, 1071) AS a ' +
                                            'INNER JOIN Pedidos AS b WITH ( NOLOCK ) ON a.PedidoID = b.PedidoID ' +
                                            'INNER JOIN Pessoas AS c WITH ( NOLOCK ) ON b.PessoaID = c.PessoaID ' +
                                            'INNER JOIN NotasFiscais AS d WITH ( NOLOCK ) ON b.NotaFiscalID = d.NotaFiscalID ' +
                                            'INNER JOIN fn_Matriz_Filial_tbl (2) AS e ON ( e.EmpresaID = b.EmpresaID ) ' +
                                            'INNER JOIN dbo.Pedidos_Parcelas f ON (a.PedParcelaID = f.PedParcelaID) ' +
                                    'WHERE   a.PessoaID = ' + nParceiroID + ' ' +
                                            'AND a.TipoParcelaID = 642 ' +
                                            'AND a.ValorParcela < 0 ' + sFiltro + ' ' +
                                    'ORDER BY a.Valorparcela ASC';


    /*'SELECT a.PedidoID, e.Fantasia AS Empresa, CONVERT(BIT, ' + bittpComissao + ') AS Aplicar, ' +
    'c.Fantasia, d.NotaFiscal, d.dtNotaFiscal, a.ValorParcela, a.PedParcelaID, 0 AS Saldo, a.ValorParcela AS ValorParcelaOriginal ' +
    'FROM fn_ParcelasComissoes_tbl(NULL, NULL, 1071) AS a ' +
    'INNER JOIN Pedidos AS b WITH(NOLOCK) ON a.PedidoID = b.PedidoID ' +
    'INNER JOIN Pessoas AS c WITH(NOLOCK) ON b.PessoaID = c.PessoaID ' +
    'INNER JOIN NotasFiscais AS d WITH(NOLOCK) ON b.NotaFiscalID = d.NotaFiscalID ' +
    'INNER JOIN fn_Matriz_Filial_tbl(' + nEmpresaID + ') AS e ON (e.EmpresaID = b.EmpresaID) ' +
    'WHERE a.PessoaID = ' + nParceiroID + ' AND a.TipoParcelaID = ' + selTipoComissaoID.value + ' AND ValorParcela < 0 ' +
    'ORDER BY a.Valorparcela';*/

    dsoGridComissoesDevolucao.ondatasetcomplete = fillGridComissoesDevolucoes_DSC;
    dsoGridComissoesDevolucao.Refresh();
}

function fillGridComissoesDevolucoes_DSC() {
    var bgColorWhite;
    var bgColorWhite = 0xffffff;
    var y = 0;
    var aEsconde = new Array();

    glb_BuildingFgAssociacoes = true;

    if (!(dsoGridComissoesDevolucao.recordset.EOF) || (dsoGridComissoesDevolucao.recordset.BOF)) {

        startGridInterface(fgAssociacoes);
        fgAssociacoes.FontSize = '8';
        fgAssociacoes.FrozenCols = 0;

        headerGrid(fgAssociacoes, ['Pedido',
	                               'Empresa',
	                               'Pessoa',
	                               'Fantasia',
	                               'NF',
	                               'OK',
	                               'Valor Parcela',
	                               'Saldo Devedor',
	                               'Associar',
	                               'PedParcelaDevolucaoID'], [2, 9]);
        fillGridMask(fgAssociacoes, dsoGridComissoesDevolucao,
                                           ['PedidoID*',
						                    'Empresa*',
						                    'PessoaID*',
						                    'Fantasia*',
						                    'NotaFiscal*',
						                    'OK',
						                    gridComissaoValorParcela(),
						                    'SaldoDevedor*',
						                    'Associar',
						                    'PedParcelaDevolucaoID'],
					                       ['', '', '', '', '', '', '999999999.99', '999999999.99', '999999999.99', ''],
					                       ['', '', '', '', '', '', '(###,###,##0.00)', '(###,###,##0.00)', '###,###,##0.00', '']);

        gridHasTotalLine(fgAssociacoes, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C']]);
        totalLine();

        fgAssociacoes.AutoSizeMode = 0;
        fgAssociacoes.AutoSize(0, fgAssociacoes.Cols - 1);

        fgAssociacoes.Editable = true;

        // Nao retirar o if abaixo, ela garante que o grid
        // so deixa editar a coluna
        if (fgAssociacoes.Rows > 1)
            fgAssociacoes.Col = getColIndexByColKey(fgAssociacoes, 'Associar');

        fgAssociacoes.Redraw = 0;

        for (y = 2; y < fgAssociacoes.Rows; y++) {
            fgAssociacoes.Cell(6, y, getColIndexByColKey(fgAssociacoes, 'Associar'), y, getColIndexByColKey(fgAssociacoes, 'Associar')) = bgColorWhite;
        }

        fgAssociacoes.Redraw = 2;
    }

    lockControlsInModalWin(false);

    glb_BuildingFgAssociacoes = false;
}

function fillGridDataFR() {
    var sFinanceiro = '';
    var sParceiro = '';
    var strSQL = '';
    var i;
    var sFiltro = '';

    // Zera valores do grid Comiss�es
    for (i = 2; i < fgComissoes.Rows; i++) {
        if (getCellValueByColKey(fgComissoes, 'Aplicar', i) != 0) {
            fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Aplicar')) = "0";
            fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = "0";
        }
    }

    if ((selParceiroFinanceiroID.value <= 0) /*&& (txtFinanceiro.value == '')*/) {
        if (window.top.overflyGen.Alert('Selecione um parceiro') == 0)
            return null;

        return null;
    }

    /*if ((isNaN(txtPesquisa.value)) && selFiltroPesquisa.value == ) {
    if (window.top.overflyGen.Alert('Financeiro inv�lido.') == 0)
    return null;

    return null;
    }*/

    /*if (txtFinanceiro.value != '')
    sFinanceiro = ' AND a.FinanceiroID = ' + txtFinanceiro.value + ' ';*/

    if (selParceiroFinanceiroID.value > 0)
        sParceiro = 'AND a.ParceiroID = ' + selParceiroFinanceiroID.value + ' ';

    lockControlsInModalWin(true);

    // zera o grid
    fgAssociacoes.Rows = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGridFR);

    switch (selFiltroPesquisa.value) {
        case "2":
            sFiltro = "AND a.FinanceiroID = " + txtPesquisa.value; break;
        case "3":
            sFiltro = "AND a.Duplicata LIKE \'" + txtPesquisa.value + '%\''; break;
    }

    //Bacil - Alterar aqui. usar tbl de empresas.

    dsoGridFR.SQL = 'SET NOCOUNT ON CREATE TABLE #GridFinanceiros (EmpresaID INT, Empresa VARCHAR(20), FinanceiroID INT, Duplicata VARCHAR(15), PessoaID INT, Fantasia VARCHAR(20), OK BIT, Valor NUMERIC(11,2), SaldoDevedor NUMERIC(11,2), ' +
										            'SaldoAssociado NUMERIC(11,2), Associar NUMERIC(11,2)) ' +
                    'INSERT INTO #GridFinanceiros ' +
                      'SELECT a.EmpresaID, dbo.fn_Pessoa_Fantasia(a.EmpresaID, 0), a.FinanceiroID, a.Duplicata, b.PessoaID, b.Fantasia, CONVERT(BIT,0) AS OK, a.Valor, a.SaldoDevedor, ISNULL(d.Valor, 0) AS Valor, 0 AS Associar ' +
							    'FROM Financeiro a WITH(NOLOCK) ' +
									    'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ParceiroID) ' +
									    'LEFT OUTER JOIN Financeiro_Comissoes d WITH(NOLOCK) ON (a.FinanceiroID = d.FinanceiroID) ' +
                                'WHERE (a.TipoFinanceiroID = ' + selTipoFinanceiro.value + ') ' +
                                    'AND (a.EstadoID = 41) ' + sParceiro + sFiltro +
                    'SELECT EmpresaID, Empresa, FinanceiroID, Duplicata, PessoaID, Fantasia, OK, Valor, CONVERT(NUMERIC(11,2), (CASE WHEN 1001 = ' + selTipoFinanceiro.value + ' THEN (Valor - SUM(SaldoAssociado)) ELSE SaldoDevedor END)) AS SaldoDevedor, Associar ' +
                        'FROM #GridFinanceiros ' +
                        'WHERE SaldoDevedor > 0 ' +
                        'GROUP BY EmpresaID, Empresa, FinanceiroID, Duplicata, PessoaID, Fantasia, OK, Valor, SaldoDevedor, Associar ' +
                        'HAVING CONVERT(NUMERIC(11,2), (CASE WHEN 1001 = 1001 THEN Valor ELSE SaldoDevedor END)) - SUM(SaldoAssociado) > 0 ' +
                        'DROP TABLE #GridFinanceiros SET NOCOUNT OFF';

    dsoGridFR.ondatasetcomplete = fillGridDataFR_DSC;
    dsoGridFR.Refresh();
}

function fillGridDataFR_DSC() {
    var bgColorWhite;
    var bgColorWhite = 0xffffff;
    var y = 0;
    var nColAssociar;

    glb_BuildingFgAssociacoes = true;

    if (!(dsoGridFR.recordset.EOF) || (dsoGridFR.recordset.BOF)) {
        startGridInterface(fgAssociacoes);
        fgAssociacoes.FontSize = '8';
        fgAssociacoes.FrozenCols = 0;

        headerGrid(fgAssociacoes, ['ID',
                                   'Duplicata',
                                   'Empresa',
                                   'Pessoa',
                                   'Fantasia',
                                   'OK',
                                   'Valor',
                                   'Saldo Devedor',
                                   'Associar'], []);
        fillGridMask(fgAssociacoes, dsoGridFR, ['FinanceiroID*',
                                                'Duplicata*',
                                                'Empresa',
		                                        'PessoaID*',
		                                        'Fantasia*',
		                                        'OK',
		                                        'Valor*',
		                                        'SaldoDevedor*',
		                                        'Associar'],
		                                        ['', '', '', '', '', '', '999999999.99', '999999999.99', '999999999.99'],
			                                    ['', '', '', '', '', '', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00']);

        alignColsInGrid(fgAssociacoes, [1]);

        gridHasTotalLine(fgAssociacoes, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C']]);
        totalLineFR();

        fgAssociacoes.AutoSizeMode = 0;
        fgAssociacoes.AutoSize(0, fgAssociacoes.Cols - 1);

        // Nao retirar o if abaixo, ela garante que o grid
        // so deixa editar a coluna
        if (fgAssociacoes.Rows > 1)
            fgAssociacoes.Col = getColIndexByColKey(fgAssociacoes, 'Associar');

        nColAssociar = getColIndexByColKey(fgAssociacoes, 'Associar');

        fgAssociacoes.Editable = true;

        fgAssociacoes.Redraw = 0;

        for (y = 2; y < fgAssociacoes.Rows; y++) {
            fgAssociacoes.Cell(6, y, nColAssociar, y, nColAssociar) = bgColorWhite;
        }

        fgAssociacoes.Redraw = 2;
    }

    lockControlsInModalWin(false);

    glb_BuildingFgAssociacoes = false;
}


/********************************************************************
Atualiza linha totais quando o bot�o ok � true
********************************************************************/
function totalLine() {
    var nValor = 0.00;
    var i = 0;
    var OldStateGrid = fgComissoes.Editable;

    fgComissoes.Editable = true;

    for (i = 2; i < fgComissoes.Rows; i++) {
        if (getCellValueByColKey(fgComissoes, 'Aplicar', i) != 0) {
            nValor += fgComissoes.ValueMatrix(i, getColIndexByColKey(fgComissoes, gridComissaoValorParcela()));
        }
    }

    if (fgComissoes.Rows > 2) {
        fgComissoes.TextMatrix(1, getColIndexByColKey(fgComissoes, gridComissaoValorParcela())) = roundNumber(nValor, 2);
    }

    fgComissoes.Editable = OldStateGrid;
}


function totalLineFR() {
    var nValor = 0.00;
    var i = 0;
    var OldStateGrid = fgAssociacoes.Editable;

    fgAssociacoes.Editable = true;

    for (i = 2; i < fgAssociacoes.Rows; i++) {
        if (getCellValueByColKey(fgAssociacoes, 'OK', i) != 0) {
            nValor += fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'Associar'));
        }
    }

    if (fgAssociacoes.Rows > 2) {
        fgAssociacoes.TextMatrix(1, getColIndexByColKey(fgAssociacoes, 'Associar')) = nValor;
        glbValorAssociar = nValor;
    }

    fgAssociacoes.Editable = OldStateGrid;
}


/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fgComissoes_modalpagamentocomissoesDblClick() {
    if (selTipoComissaoID.value == 642)
        return null;

    //Bacil Batman
    var nPreencheOk = 0;

    if ((fgComissoes.Row == 2) && (fgComissoes.Col == getColIndexByColKey(fgComissoes, 'Aplicar'))) {
        for (i = 2; i < fgComissoes.Rows; i++) {
            if (i == 2) {
                for (y = 2; y < fgComissoes.Rows; y++) {
                    if (fgComissoes.TextMatrix(y, getColIndexByColKey(fgComissoes, 'Aplicar')) == 0)
                        nPreencheOk = nPreencheOk + 1;
                }
            }

            if (nPreencheOk != 0)
                fgComissoes.TextMatrix(i, getColIndexByColKey(fgComissoes, 'Aplicar')) = -1;
            else
                fgComissoes.TextMatrix(i, getColIndexByColKey(fgComissoes, 'Aplicar')) = 0;
        }
    }

    totalLine();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar) 
    var controlID = ctl.id;
    var sMsg = '';

    if (controlID == 'btnListar_Associacoes') {
        if (selTipoAssociacao.value == glb_associarFinanceiros) //Associar Financeiros
            fillGridDataFR();
        else if (selTipoAssociacao.value == glb_associarComissaoDevolucao) //Associar Comissoes Internas de Devolu��o
            fillGridComissoesDevolucoes();
    }
    else if (controlID == 'btnListar') {
        fillGridData();
    }
    else if (controlID == 'btnGerar') {
        gerarPedido();
    }
    else if (controlID == 'btnAssociar') {
        gerarOcorrenciaDesconto();
    }
    else if (controlID == 'btnPesquisa') {
        fillCmbParceiroFinanceiro();
    }
    else if (controlID == 'btnPesqParceiro') {
        if (txtArgumento.value.length > 0)
            fillCmbParceiroComissao();
        else {
            if (window.top.overflyGen.Alert('Adicione um argumento para pesquisa') == 0)
                return null;

            return null;
        }
    }
    else {
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Cria um Pedido a partir de Comiss�o Normal
********************************************************************/
function gerarPedido() {
    var i;
    var strPedParcelaID = '';
    var _retMsg;

    if (selParceiroComissaoID.value == 0) {
        if (window.top.overflyGen.Alert('Selecione um parceiro') == 0)
            return null;

        return null;
    }

    for (i = 2; i < fgComissoes.Rows; i++) {

        if (getCellValueByColKey(fgComissoes, 'Aplicar', i) != 0) {
            strPedParcelaID += '|' + fgComissoes.ValueMatrix(i, getColIndexByColKey(fgComissoes, 'PedParcelaID'));
            strPedParcelaID += ':' + fgComissoes.ValueMatrix(i, getColIndexByColKey(fgComissoes, gridComissaoValorParcela()));
        }
    }
    strPedParcelaID += '|';

    if (strPedParcelaID.length < 2) {
        if (window.top.overflyGen.Alert('Selecione uma comiss�o no grid.') == 0)
            return null;

        return null;
    }

    txtValor.value = replaceStr(txtValor.value, ',', '.');

    if (fgComissoes.ValueMatrix(1, getColIndexByColKey(fgComissoes, gridComissaoValorParcela())) != txtValor.value) {
        if (window.top.overflyGen.Alert('Valor da nota fiscal n�o corresponde com a soma total das comiss�es selecionadas') == 0)
            return null;

        return null;
    }

    _retMsg = window.top.overflyGen.Confirm('Gerar pedido - voc� tem certeza?');

    if ((_retMsg == 0) || (_retMsg == 2))
        return null;

    var strPas = '?';
    strPas += 'nEmpresaID=' + escape(EmpresaLogadaID[0]);
    strPas += '&nPessoaID=' + escape(selParceiroComissaoID.value);
    strPas += '&nParceiroID=' + escape(selParceiroComissaoID.value);
    strPas += '&nProdutoID=8514';
    strPas += '&nServicoID=NULL';
    strPas += '&strDiscriminacao=NULL';
    strPas += '&strObservacoes=NULL';
    strPas += '&strPedParcelaID=' + escape(strPedParcelaID);
    strPas += '&nTotais=' + escape(txtValor.value);
    strPas += '&nUser=' + escape(getCurrUserID());

    lockControlsInModalWin(true);

    dsoGera.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/comissoespedidos.aspx' + strPas;
    dsoGera.ondatasetcomplete = gerarPedido_DSC;
    dsoGera.refresh();
}

/********************************************************************
Retorno do servidor, apos gerar o pedido
********************************************************************/
function gerarPedido_DSC() {
    var _retConf = null;
    var sHTMLSupID = null;
    var nEmpresaID = EmpresaLogadaID[0];


    lockControlsInModalWin(false);

    if (!(dsoGera.recordset.BOF && dsoGera.recordset.EOF)) {
        var sResultado = dsoGera.recordset['Resultado'].value;
        var sPedidoID = dsoGera.recordset['PedidoID'].value;

        if (sResultado != null) {
            if (window.top.overflyGen.Alert(sResultado) == 0)
                return null;

            return null;
        }

        // Gerando o pedido
        else if (sPedidoID != null) {
            _retConf = window.top.overflyGen.Confirm('Gerado o Pedido: ' + sPedidoID + '\n' +
													    'Detalhar Pedido?');
            if ((_retConf == 0) || (_retConf == 2)) {
                fillGridData();
            }
            else if (_retConf == 1) {
                sHTMLSupID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getHtmlId()');
                sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, sPedidoID);
            }
        }
    }
}

/********************************************************************
Cria��o / Ocorr�ncia de desconto
********************************************************************/
function gerarOcorrenciaDesconto() {
    var i;
    var sTipoAssociacao = '';
    var nValorAssociar = '';
    var nPedParcelaID = 0;
    var strPas = '';
    var nAssociacaoID = '';

    sTipoAssociacao = (selTipoAssociacao.value == glb_associarComissaoDevolucao ? 'ComissaoDevolucao' : 'Financeiros');

    for (i = 2; i < fgComissoes.Rows; i++) {
        if (getCellValueByColKey(fgComissoes, 'Aplicar', i) != 0) {
            nPedParcelaID = fgComissoes.ValueMatrix(i, getColIndexByColKey(fgComissoes, 'PedParcelaID'));
        }
    }

    for (i = 2; i < fgAssociacoes.Rows; i++) {
        if (getCellValueByColKey(fgAssociacoes, 'OK', i) != 0) {
            /*nFinanceiroID += '|' + fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'FinanceiroID*'));
            nFinanceiroID += ';';*/

            //Se TipoAssociacao = Financeiros pega FinanceiroID para associar. Se TipoAssociacao = Devolucoes pega PedParcelaDevolucaoID para associacao
            nAssociacaoID += '|' + (selTipoAssociacao.value == glb_associarFinanceiros ? fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'FinanceiroID*')) : fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'PedParcelaDevolucaoID')));
            nAssociacaoID += ';';

            if (selTipoAssociacao.value == glb_associarFinanceiros) {
                //Verifica se o valor a ser associado � maios do que 0
                if (fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'Associar')) <= 0) {
                    if (window.top.overflyGen.Alert('O valor associado ao financeiro ' + fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'FinanceiroID*')) +
                        ' deve maior que 0.') == 0)
                        return null;

                    return null;
                }
            }
            else {
                //Verifica se o valor a ser associado � maios do que 0
                if (fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'Associar')) <= 0) {
                    if (window.top.overflyGen.Alert('O valor associado ao Pedido ' + fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'PedidoID*')) +
                        ' deve maior que 0.') == 0)
                        return null;

                    return null;
                }
            }


            nValorAssociar += '|' + fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'Associar'));
            nValorAssociar += ';';
        }
    }

    if (nPedParcelaID == 0) {
        if (window.top.overflyGen.Alert('Selecione uma comiss�o.') == 0)
            return null;

        return null;
    }

    if ((nAssociacaoID == '') || (nValorAssociar == '')) {
        if (window.top.overflyGen.Alert('Selecione um financeiro para associar a comiss�o.') == 0)
            return null;

        return null;
    }

    strPas = '?';
    strPas += 'sTipoAssociacao=' + escape(sTipoAssociacao);
    strPas += '&nAssociacaoID=' + escape(nAssociacaoID);
    strPas += '&nPedParcelaID=' + escape(nPedParcelaID);
    strPas += '&nValorAssociar=' + escape(nValorAssociar);
    strPas += '&nEmpresaID=' + escape(EmpresaLogadaID[0]);

    if (selTipoAssociacao.value == glb_associarFinanceiros)
        strPas += '&nTipoFinanceiro=' + escape(selTipoFinanceiro.value);

    strPas += '&nUserID=' + escape(getCurrUserID());

    lockControlsInModalWin(true);

    dsoGeraOcorrenciaDesconto.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/ocorrenciadescontogerador.aspx' + strPas;

    try {
        dsoGeraOcorrenciaDesconto.ondatasetcomplete = gerarOcorrenciaDesconto_DSC;
        dsoGeraOcorrenciaDesconto.refresh();
    }
    catch (e) {
        if (selTipoAssociacao.value == glb_associarFinanceiros) {
            if (selTipoFinanceiro.value == 1001) {
                if (window.top.overflyGen.Alert('Erro na gera��o de Associa��o/Ocorr�ncia') == 0)
                    return null;
            }
            else {
                if (window.top.overflyGen.Alert('Erro na gera��o de Ocorr�ncia ') == 0)
                    return null;
            }
        }
        else
            if (window.top.overflyGen.Alert('Erro na gera��o de Associa��o') == 0)
                return null;

        lockControlsInModalWin(false);
    }
}

function gerarOcorrenciaDesconto_DSC() {
    if (selTipoAssociacao.value == glb_associarFinanceiros) {
        if (selTipoFinanceiro.value == 1001) {
            if (window.top.overflyGen.Alert('Associa��o de Financeiro gerada com sucesso') == 0)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Associa��o e Ocorr�ncia gerada com sucesso') == 0)
                return null;
        }

        fillGridData();
        fillGridDataFR();
    }
    else {
        if (window.top.overflyGen.Alert('Associa��o de Comiss�o Devolu��o gerada com sucesso') == 0)
            return null;

        fillGridData();
        fillGridComissoesDevolucoes();
    }


}
/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fgComissoes_modalpagamentocomissoes_AfterEdit(Row, Col) {
    var i = 0;
    var y = 0;
    var nValor = 0.00;

    if (glb_GridIsBuilding)
        return;

    //Comissao Interna
    if (selTipoComissaoID.value == 642) {
        if (glbLinhaVerifica != 0) {
            fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Aplicar')) = "0";
            fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = "0";
        }

        if (fgComissoes.ValueMatrix(Row, getColIndexByColKey(fgComissoes, 'Aplicar')) != 0) {
            nValor = fgComissoes.ValueMatrix(Row, getColIndexByColKey(fgComissoes, gridComissaoValorParcela()));
            fgComissoes.TextMatrix(Row, getColIndexByColKey(fgComissoes, 'Saldo*')) = nValor;
            glbLinhaVerifica = Row;

            for (y = 2; y < fgAssociacoes.Rows; y++) {
                if (getCellValueByColKey(fgAssociacoes, 'OK', y) != 0) {
                    fgAssociacoes.TextMatrix(y, getColIndexByColKey(fgAssociacoes, 'Associar')) = "0";
                    fgAssociacoes.TextMatrix(y, getColIndexByColKey(fgAssociacoes, 'OK')) = "0";
                }
            }
        }
        else {
            fgComissoes.TextMatrix(Row, getColIndexByColKey(fgComissoes, 'Saldo*')) = "0";
            glbLinhaVerifica = 0;

            for (i = 2; i < fgAssociacoes.Rows; i++) {
                if (getCellValueByColKey(fgAssociacoes, 'OK', i) != 0) {
                    fgAssociacoes.TextMatrix(i, getColIndexByColKey(fgAssociacoes, 'OK')) = "0";
                    fgAssociacoes.TextMatrix(i, getColIndexByColKey(fgAssociacoes, 'Associar')) = "0";
                }
            }
        }
    }
    else {
        if (Col == getColIndexByColKey(fgComissoes, gridComissaoValorParcela()))
            fgComissoes.TextMatrix(Row, Col) = treatNumericCell(fgComissoes.TextMatrix(Row, Col));
    }

    totalLine();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fgComissoes_modalpagamentocomissoes_AfterRowCol(grid, OldRow, OldCol, NewRow, NewCol) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fgAssociacoes_modalpagamentocomissoesDblClick(grid, row, col) {
    if (selTipoAssociacao.value == glb_associarFinanceiros)
        sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(EmpresaLogadaID[0], fgAssociacoes.TextMatrix(fgAssociacoes.Row, getColIndexByColKey(fgAssociacoes, 'FinanceiroID*'))));
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fgAssociacoes_modalpagamentocomissoesKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fgAssociacoes_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    /*if (glb_GridIsBuilding)
    return false;
        
    if (fgAssociacoes.Rows > 2) 
    {
    if (NewRow < 2) 
    {
    //fgAssociacoes.Row = OldRow;
    glb_validRow = OldRow;
    return true;
    }
    } */
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fgAssociacoes_modalpagamentocomissoes_BeforeEdit(grid, row, col) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fgComissoes_modalpagamentocomissoes_BeforeEdit(grid, row, col) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_EnterCell_Prg() {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fgComissoes_modalpagamentocomissoesKeyPress() {
    ;
}
/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fgAssociacoes_modalpagamentocomissoes_AfterEdit(Row, Col) {
    if (glb_GridIsBuilding)
        return;

    var nValor = 0.00;
    var teste = 0;

    var ValorTeste = 0;
    var i = 0;
    var OldStateGrid = fgAssociacoes.Editable;

    fgAssociacoes.Editable = true;

    fgAssociacoes.TextMatrix(Row, Col) = treatNumericCell(fgAssociacoes.TextMatrix(Row, Col));

    for (i = 2; i < fgComissoes.Rows; i++) {
        if (getCellValueByColKey(fgComissoes, 'Aplicar', i) != 0) {
            teste = 1;
        }
    }

    if (teste == 0) {
        fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'OK')) = "0";
        fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar')) = "0";

        if (window.top.overflyGen.Alert('Selecione uma comiss�o') == 0)
            return null;
    }
    else if (teste == 1) {
        //N�o deixa associar comissoes internas se o parceiro possuir comiss�o de devolu��o
        if (dsoVerificaDevolucoes.recordset.RecordCount() != 0 && selTipoAssociacao.value == glb_associarFinanceiros) {
            fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar')) = "0";
            fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'OK')) = "0";
            fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = "0";
            fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Aplicar')) = "0";

            if (window.top.overflyGen.Alert('Precisa Associar todas as comissoes de devolu��o deste parceiro.') == 0)
                return null;
        }

        // Se o associar for maior que o Saldo Devedor invoca mensagem
        if (fgAssociacoes.ValueMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar')) > Math.abs(fgAssociacoes.ValueMatrix(Row, getColIndexByColKey(fgAssociacoes, 'SaldoDevedor*')))) {
            fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar')) = "0";
            fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = "0";

            for (i = 2; i < fgAssociacoes.Rows; i++) {
                if (getCellValueByColKey(fgAssociacoes, 'OK', i) != 0) {
                    if (i != Row) {
                        fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) + fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'Associar'));
                    }
                }
            }
            fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, gridComissaoValorParcela())) - fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*'));

            fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar')) = "0";
            fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'OK')) = "0";

            if (window.top.overflyGen.Alert('Valor associado n�o pode ser maior que o saldo devedor') == 0)
                return null;
        }
        // Se tem checkbox selecionado
        else if (fgAssociacoes.ValueMatrix(Row, getColIndexByColKey(fgAssociacoes, 'OK')) != 0) {
            // Verifica se tem saldo
            glb_verificasaldo = 0;

            for (i = 2; i < fgAssociacoes.Rows; i++) {
                if (getCellValueByColKey(fgAssociacoes, 'OK', i) != 0) {
                    glb_verificasaldo = glb_verificasaldo + fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'Associar'));
                }
            }

            if (glb_verificasaldo != fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, gridComissaoValorParcela()))) {
                // Pediu pra associar atrav�s do OK
                if (Col == getColIndexByColKey(fgAssociacoes, 'OK')) {
                    // Verifica se o saldo devedor n�o � maior que o saldo
                    if (fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) <= Math.abs(fgAssociacoes.ValueMatrix(Row, getColIndexByColKey(fgAssociacoes, 'SaldoDevedor*')))) {
                        fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar')) = fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*'));
                        fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = "0";
                    }
                    else {
                        // Preenche o campo Associar
                        fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar')) = Math.abs(fgAssociacoes.ValueMatrix(Row, getColIndexByColKey(fgAssociacoes, 'SaldoDevedor*')));

                        // Subtrai no grid Comiss�es na coluna Saldo o valor Associar
                        fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) - fgAssociacoes.ValueMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar'));
                    }
                }
                else {
                    // fgAssociacoes.TextMatrix(i, getColIndexByColKey(fgAssociacoes, 'Associar')) = "0";
                    fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = "0";

                    for (i = 2; i < fgAssociacoes.Rows; i++) {
                        if (getCellValueByColKey(fgAssociacoes, 'OK', i) != 0) {
                            fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) + fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'Associar'));
                        }
                    }

                    // Se o associar for maior que o Saldo / Valor Parcela
                    if (fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) > fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, gridComissaoValorParcela()))) {
                        //fgAssociacoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgAssociacoes, 'Associar')) = "0";
                        fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = "0";

                        for (i = 2; i < fgAssociacoes.Rows; i++) {
                            if (getCellValueByColKey(fgAssociacoes, 'OK', i) != 0) {
                                if (i != Row) {
                                    fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) + fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'Associar'));
                                }
                            }
                        }
                        fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, gridComissaoValorParcela())) - fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*'));

                        fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'OK')) = "0";
                        fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar')) = "0";
                        if (window.top.overflyGen.Alert('Saldo insuficiente') == 0)
                            return null;
                    }
                    else {
                        // Quando um valor associado for alterado na coluna Associar
                        fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, gridComissaoValorParcela())) - fgAssociacoes.ValueMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar'));

                        for (i = 2; i < fgAssociacoes.Rows; i++) {
                            if (getCellValueByColKey(fgAssociacoes, 'OK', i) != 0) {
                                if (i != Row) {
                                    fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) - fgAssociacoes.ValueMatrix(i, getColIndexByColKey(fgAssociacoes, 'Associar'));
                                }
                            }
                        }
                        // Desmarca a coluna OK caso o Associar for zerado
                        if (fgAssociacoes.ValueMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar')) == 0) {
                            fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'OK')) = "0";
                        }
                    }
                }
            }
            // Saldo � insuficiente
            else {
                fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'OK')) = "0";
                fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar')) = "0";
                if (window.top.overflyGen.Alert('Saldo insuficiente') == 0)
                    return null;
            }
        }
        // N�o tem checkbox selecionado
        else {
            // Pediu pra zerar o associar (Col = OK)
            if (Col == getColIndexByColKey(fgAssociacoes, 'OK')) {
                // Devolve valor associar para o Saldo / Saldo Devedor
                fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) + fgAssociacoes.ValueMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar'));
                fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar')) = 0;
            }
            else if (fgAssociacoes.ValueMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar')) > fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*'))) {
                fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'OK')) = "0";
                fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar')) = "0";
                if (window.top.overflyGen.Alert('Saldo insuficiente') == 0)
                    return null;
            }
            else {
                // Usu�rio digitou um valor na coluna associar
                fgComissoes.TextMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) = fgComissoes.ValueMatrix(glbLinhaVerifica, getColIndexByColKey(fgComissoes, 'Saldo*')) - fgAssociacoes.ValueMatrix(Row, getColIndexByColKey(fgAssociacoes, 'Associar'));
                fgAssociacoes.TextMatrix(Row, getColIndexByColKey(fgAssociacoes, 'OK')) = "-1";
            }
        }
        // Realiza a soma dos valores associado
        totalLineFR();
    }
    fgAssociacoes.Editable = OldStateGrid;

}

function js_fgAssociacoes_modalpagamentocomissoes_AfterRowCol(grid, OldRow, OldCol, NewRow, NewCol) {
    ;
}

function txtArgumento_onkeypress() {
    if (event.keyCode == 13)
        fillCmbParceiroComissao();
}

function txtPesquisa_onkeypress() {
    if (event.keyCode == 13)
        fillCmbParceiroFinanceiro();
}

function selParceiroComissaoID_onchange() {
    var stexto = '';

    setLabel(lblParceiroComissaoID, selParceiroComissaoID, 'Parceiro');

    fgComissoes.Rows = 0;
}

function selParceiroFinanceiroID_onchange() {
    var stexto = '';

    setLabel(lblParceiroFinanceiroID, selParceiroFinanceiroID, 'Parceiro');

    fgAssociacoes.Rows = 0;
}

function selTipoFinanceiro_onchange() {
    selParceiroFinanceiroID_onchange();
}

function selFiltroPesquisa_onchange() {
    txtPesquisa.value = "";
}

function selTipoAssociacao_onchange() {
    selTipoAssociacaoChanges();
    zeraGridfgAssociacoes();
}

function selTipoAssociacaoChanges() {

    clearComboEx(['selParceiroFinanceiroID']);
    selParceiroFinanceiroID.disabled = true;
    txtPesquisa.value = "";

    fillCmbFiltroPesquisa();

    if (selTipoAssociacao.value == glb_associarComissaoDevolucao) { //Associar Comissoes de Devolu��o

        lblTipoFinanceiro.style.visibility = 'hidden';
        selTipoFinanceiro.style.visibility = 'hidden';

        /*lblFinanceiro.style.visibility = 'hidden';
        txtFinanceiro.style.visibility = 'hidden';*/

        //txtFinanceiro.value = "";
        setLabel(lblGridAssociacoes, null, 'Comiss�o Devolu��o');
    }
    else { //Associar Financeiros
        lblTipoFinanceiro.style.visibility = 'inherit';
        selTipoFinanceiro.style.visibility = 'inherit';

        /*lblFinanceiro.style.visibility = 'inherit';
        txtFinanceiro.style.visibility = 'inherit';*/

        setLabel(lblGridAssociacoes, null, 'Financeiros');
    }
}

function setLabel(lbl, sel, sTextoInicial) {
    var stexto = sTextoInicial;

    if (sel != null)
        if (sel.value > 0)
            stexto += ' ' + sel.value;

    lbl.style.width = stexto.length * FONT_WIDTH;
    eval(lbl.id + '.innerText = \'' + stexto + '\'');
}

function js_modalpagamento_fgComissoes_EnterCell() {
    if (glb_GridIsBuilding || glb_BuildingFgComissao)
        return;

    if (fgComissoes.Rows > 2) {
        if (fgComissoes.Row < 2)
            fgComissoes.Row = 2;

        if (!((fgComissoes.Col == getColIndexByColKey(fgComissoes, 'Aplicar')) || (fgComissoes.Col == getColIndexByColKey(fgComissoes, gridComissaoValorParcela()))))
            fgComissoes.Col = getColIndexByColKey(fgComissoes, 'Aplicar');
    }
}

function js_modalpagamento_fgAssociacoes_EnterCell() {
    if (glb_GridIsBuilding || glb_BuildingFgAssociacoes)
        return;

    if (fgAssociacoes.Rows > 2) {
        if (fgAssociacoes.Row < 2)
            fgAssociacoes.Row = 2;

        if (!((fgAssociacoes.Col == getColIndexByColKey(fgAssociacoes, 'OK')) || (fgAssociacoes.Col == getColIndexByColKey(fgAssociacoes, 'Associar'))))
            fgAssociacoes.Col = getColIndexByColKey(fgAssociacoes, 'OK');
    }
}

function gridComissaoValorParcela() {
    var string = 'ValorParcela';

    if (selTipoComissaoID.value == 642)
        string += '*';

    return string;
}

function fgComissoes_modalpagamentocomissoes_ValidateEdit() {
    if (fgComissoes.Col == getColIndexByColKey(fgComissoes, gridComissaoValorParcela())) {
        var valInEditText = transformStringInNumeric(fgComissoes.EditText);

        if (parseFloat(valInEditText) > fgComissoes.ValueMatrix(fgComissoes.Row, getColIndexByColKey(fgComissoes, 'ValorParcelaOriginal'))) {
            fgComissoes.EditText = fgComissoes.ValueMatrix(fgComissoes.Row, getColIndexByColKey(fgComissoes, gridComissaoValorParcela()));

            if (window.top.overflyGen.Alert('Valor a associar n�o pode ser maior que o valor da comiss�o.') == 0)
                return null;

            return null;
        }
    }
}


function dsoVerificaDevolucoes_DSC() {
 ;
}