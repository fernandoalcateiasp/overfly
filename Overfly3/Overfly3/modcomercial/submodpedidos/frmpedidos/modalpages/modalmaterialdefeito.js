/********************************************************************
modalmaterialdefeito.js

Library javascript para o modalmaterialdefeito.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nPedidoID = null;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;

// controla se a modal esta carregando
var glb_ModalWinIsLoading = true;

// controla o retorno do servidor dos dados de combos dinamicos
var glb_CounterCmbsDynamics = 0;
var glb_timerVincAsstec = null;
var glb_PassedOneInDblClick = false;
var glb_localTimerRefresh = null;

var dsoPesq = new CDatatransport('dsoPesq');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoMaterialDefeito = new CDatatransport('dsoMaterialDefeito');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
LISTA DAS FUNCOES

window_onload()
setupPage()
loadSelProduto()
dsoCmbDynamic_DSC()
selProduto_onchange()
btn_onclick(ctl)
startPesq()
dsopesq_DSC()
cleanGrid()
btnOK_Status()

Eventos particulares de grid
js_fg_modVincAsstAfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
js_fg_modVincAsstDblClick( grid, Row, Col)
js_fg_

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

	glb_nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');
    
    // ajusta o body do html
    var elem = document.getElementById('modalmaterialdefeitoBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostrar a modal foi transferido para a funcao dsoCmbDynamic_DSC()
        
    loadSelProduto();    
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Material com Defeito', 1);

    var elem;
    var frameRect, modWidth, modHeight;
    var borderThickness = 6;
    
    // Ajusta elementos da janela
    
    // dimensoes da modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // ajusta o divProduto
    adjustElementsInForm([['lblProduto', 'selProduto', 60, 1, -10]],
                          null,null,true);
                          
    elem = window.document.getElementById('divProduto');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height, 10);
        width = modWidth - (2 * ELEM_GAP) - borderThickness ;
        height = parseInt(selProduto.currentStyle.top, 10) + parseInt( selProduto.currentStyle.height, 10);
    }
        
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divProduto.currentStyle.top) + parseInt(divProduto.currentStyle.height) + ELEM_GAP;
        width = parseInt(divProduto.currentStyle.width);
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 2) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);

    headerGrid(fg,['Produto',
                   'ID',
                   'N�mero S�rie',
                   'AsstecID'], []);
    
    fg.Redraw = 2;
    
    // Reajusta largura selProduto
    selProduto.style.width = parseInt(fg.currentStyle.width) + 2;
    
    selProduto.onchange = selProduto_onchange;
    selProduto.disabled = true;
}

/********************************************************************
Solicita dados do combo produtos ao servidor
********************************************************************/
function loadSelProduto()
{
    var strSQLSelect = '';
	var strSQLFrom = '';
	var strSQLWhere = '';

	lockControlsInModalWin(true);
	
	cleanGrid();

    // controla o retorno do servidor dos dados de combos dinamicos
    // (selProduto)
    glb_CounterCmbsDynamics = 1;

    // parametrizacao do dso dsoCmbDynamic01 (designado para selProduto)
    setConnection(dsoCmbDynamic01);
    
	// Produtos
	strSQLSelect = "SELECT 0 AS Indice, 0 AS fldID, 'TODOS' AS fldName ";
	strSQLSelect += 'UNION ALL ';
    strSQLSelect += 'SELECT 1 AS Indice, Produtos.ConceitoID AS fldID, Produtos.Conceito AS fldName ';

    strSQLFrom = 'FROM Pedidos_Itens Itens WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK) ';

	strSQLWhere = 'WHERE ' +
			'(Itens.PedidoID = ' + glb_nPedidoID + ' AND Itens.ProdutoID = Produtos.ConceitoID) ' +
		'ORDER BY Indice, fldName ';

	dsoCmbDynamic01.SQL = strSQLSelect + strSQLFrom + strSQLWhere;

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();
}

/********************************************************************
Preenche combo produtos com dados vindos do servidor
********************************************************************/
function dsoCmbDynamic_DSC()
{
    // Trata existencia de dados no carregamento
    // para mostrar ou nao a modal
    if ( glb_ModalWinIsLoading == true )
    {
        glb_ModalWinIsLoading = false;
        
        if ( dsoCmbDynamic01.recordset.BOF && dsoCmbDynamic01.recordset.EOF )
        {
            btn_onclick(btnCanc);            
            return null;
        }
        else
        {
            // mostra a janela modal com o arquivo carregado
            showExtFrame(window, true);
    
            // coloca foco no campo apropriado
            window.focus();
            if ( document.getElementById('selProduto').disabled == false )
                selProduto.focus();
        }
    }

	var i;
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selProduto];
    var aDSOsDunamics = [dsoCmbDynamic01];

    // Inicia o carregamento de combos dinamicos (selProduto)

    clearComboEx(['selProduto']);
    
    glb_CounterCmbsDynamics --;

    if (glb_CounterCmbsDynamics == 0) {
        for (i = 0; i < aCmbsDynamics.length; i++) {
            oldDataSrc = aCmbsDynamics[i].dataSrc;
            oldDataFld = aCmbsDynamics[i].dataFld;
            aCmbsDynamics[i].dataSrc = '';
            aCmbsDynamics[i].dataFld = '';

            while (!aDSOsDunamics[i].recordset.EOF) {
                optionStr = aDSOsDunamics[i].recordset['fldName'].value;
                optionValue = aDSOsDunamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;                
                aCmbsDynamics[i].add(oOption);
                aDSOsDunamics[i].recordset.MoveNext();
            }

            aCmbsDynamics[i].dataSrc = oldDataSrc;
            aCmbsDynamics[i].dataFld = oldDataFld;
        }
    }

 	
 	lockControlsInModalWin(false);
    window.focus();
    
    adjustProdutoLabel();
 	
 	// trava/destrava os combos de produtos se tem dados
 	// seleciona o combo de produtos
 	if ( selProduto.options.length == 0 )
 	{
 	    selProduto.disabled = true;
 	    btnOK_Status();
 	}    
    else
    {
 	    selProduto.disabled = false;
 	    // seleciona o combo
        selProduto.focus();
        // garante item selecionado no combo
        selProduto.selectedIndex = 0;
        // carrega o grid
        startPesq();    
    }
}

function adjustProdutoLabel()
{
	setLabelOfControl(lblProduto, selProduto.value);
}

/********************************************************************
Troca do item selecionado no combo produtos
********************************************************************/
function selProduto_onchange() {
    adjustProdutoLabel();
    cleanGrid();
    startPesq();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        //sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null );
        materialDefeito();
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
}

/********************************************************************
Pesquida de dados no servidor para preenchimento do grid
********************************************************************/
function startPesq() {
    if (glb_localTimerRefresh != null) {
        window.clearInterval(glb_localTimerRefresh);
        glb_localTimerRefresh = null;
    }

    var strSQLSelect = '';
    var strSQLFrom = '';
    var strSQLWhere = '';
    var strSQLSelect2 = '';
    var strSQLFrom2 = '';
    var strSQLWhere2 = '';
    var strSQLSelect3 = '';
    var strSQLFrom3 = '';
    var strSQLWhere3 = '';
    var strSQLOrder = '';
    var strSQL = '';
    var nProdutoID = selProduto.value;

    lockControlsInModalWin(true);

    //INICIO DE NOVO NS ---------------------------------------------------------------
    strSQLSelect = 'SELECT DISTINCT Movimentos.NumMovimentoID, NULL AS PedProdutoID, NULL AS PedLoteID, Produtos.ConceitoID AS ProdutoID, Produtos.Conceito AS Produto, ' +
                   'NumerosSerie.NumeroSerie, Movimentos.Defeito AS Defeito, 0 AS OK ';

    strSQLFrom = 'FROM Pedidos WITH(NOLOCK), NumerosSerie_Movimento Movimentos WITH(NOLOCK), NumerosSerie WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK)/*, Pedidos_Itens WITH(NOLOCK)*/ ';

    strSQLWhere = 'WHERE (' +
			'Pedidos.PedidoID = ' + glb_nPedidoID + ' AND Pedidos.EstadoID = 28 AND ' +
			'Pedidos.PedidoID = Movimentos.PedidoID AND ' +
			'Movimentos.NumeroSerieID = NumerosSerie.NumeroSerieID AND ' +
			'NumerosSerie.ProdutoID = Produtos.ConceitoID ';

    if (nProdutoID != 0)
        strSQLWhere += ' AND NumerosSerie.ProdutoID = ' + nProdutoID;

    strSQLWhere += ') ';

    strSQLSelect2 = 'UNION ALL SELECT DISTINCT NULL AS NumMovimentoID, Movimentos.PedProdutoID, NULL AS PedLoteID, Produtos.ConceitoID AS ProdutoID, Produtos.Conceito AS Produto, ' +
	                'NULL AS NumeroSerie, Movimentos.Defeito AS Defeito, 0 AS OK ';

    strSQLFrom2 = 'FROM Pedidos WITH(NOLOCK), Pedidos_ProdutosSeparados Movimentos WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK) ';

    strSQLWhere2 = 'WHERE (' +
			'Pedidos.PedidoID = ' + glb_nPedidoID + ' AND Pedidos.EstadoID = 28 AND ' +
			'Pedidos.PedidoID = Movimentos.PedidoID AND Movimentos.Quantidade = 1 AND ' +
			'Movimentos.ProdutoID = Produtos.ConceitoID ';

    if (nProdutoID != 0)
        strSQLWhere2 += ' AND Movimentos.ProdutoID = ' + nProdutoID;

    strSQLWhere2 += ') ';

    strSQLSelect3 = 'UNION ALL SELECT DISTINCT NULL AS NumMovimentoID, NULL  AS PedProdutoID, Movimentos.PedProdutoID AS PedLoteID, Produtos.ConceitoID AS ProdutoID, ' +
	                'Produtos.Conceito AS Produto, NULL AS NumeroSerie, Movimentos.Defeito AS Defeito, 0 AS OK ';

    strSQLFrom3 = 'FROM Pedidos WITH(NOLOCK), Pedidos_ProdutosLote Movimentos WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK) ';

    strSQLWhere3 = 'WHERE (' +
			'Pedidos.PedidoID = ' + glb_nPedidoID + ' AND Pedidos.EstadoID = 28 AND ' +
			'Pedidos.PedidoID = Movimentos.PedidoID AND Movimentos.Quantidade = 1 AND ' +
			'Movimentos.ProdutoID = Produtos.ConceitoID ';

    if (nProdutoID != 0)
        strSQLWhere3 += ' AND Movimentos.ProdutoID = ' + nProdutoID;

    strSQLWhere3 += ') ';
    //FIM DE NOVO NS ---------------------------------------------------------------

    strSQLOrder = 'ORDER BY Produto, NumeroSerie ';

    strSQL = strSQLSelect + strSQLFrom + strSQLWhere +
		strSQLSelect2 + strSQLFrom2 + strSQLWhere2 +
		strSQLSelect3 + strSQLFrom3 + strSQLWhere3 + strSQLOrder;

    setConnection(dsoPesq);

    dsoPesq.SQL = strSQL;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

/********************************************************************
Retorno do servidor da funcao startPesq
********************************************************************/
function dsopesq_DSC() {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    startGridInterface(fg);

    fg.FrozenCols = 0;

    headerGrid(fg, ['ID',
				   'Produto',
				   'N�mero S�rie',
                   'Defeito',
                   'NumMovimentoID',
                   'PedProdutoID',
                   'PedLoteID',
                   'OK'], [4, 5, 6, 7]);

    fillGridMask(fg, dsoPesq, ['ProdutoID*',
							 'Produto*',
				   			 'NumeroSerie*',
                             'Defeito',
                             'NumMovimentoID',
                             'PedProdutoID',
                             'PedLoteID',
                             'OK'],
                             ['', '', '', '', '', '', '', ''],
                             ['', '', '', '', '', '', '', '']);

    alignColsInGrid(fg, [0]);

    fg.ColDataType(3) = 11;

    fg.Editable = true;

    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    fg.Redraw = 0;

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.Redraw = 2;

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        fg.Row = 1;
        window.focus();
        fg.focus();
    }
    else {
        selProduto.focus();
    }

    btnOK_Status();

    // writeInStatusBar('child', 'cellMode', 'XXXXXXXX');    
}


/********************************************************************
Remove as linhas do grid
********************************************************************/
function cleanGrid() {
    if (fg.Rows > 1)
        fg.Rows = 1;

    btnOK.disabled = true;
}


/********************************************************************
Habilita desabilita o botao OK
********************************************************************/
function btnOK_Status() {
    btnOK.disabled = true;

    var i;
    var btnOKDisabled = true;

    // critica do grid
    if ((fg.Rows > 1) && (fg.Row != 0))
        btnOKDisabled = false;

    btnOK.disabled = btnOKDisabled;
}
/********************************************************************
Chama pagina ASP para gerar o pedido no servidor
********************************************************************/
function materialDefeito() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;

    lockControlsInModalWin(true);
    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    for (i = 1; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) == 0)
            continue;

        nBytesAcum = strPars.length;

        if ((nBytesAcum >= glb_nMaxStringSize) && (strPars != '')) {
            nBytesAcum = 0;
            glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
            strPars = '';
            nDataLen = 0;
        }

        nDataLen++;

        strPars += (strPars == '' ? '?' : '&') + 'nNMID=' + escape(trimStr((fg.ValueMatrix(i, getColIndexByColKey(fg, 'NumMovimentoID'))).toString()));
        strPars += '&nPPID=' + escape(trimStr((fg.ValueMatrix(i, getColIndexByColKey(fg, 'PedProdutoID'))).toString()));
        strPars += '&nPLID=' + escape(trimStr((fg.ValueMatrix(i, getColIndexByColKey(fg, 'PedLoteID'))).toString()));
        strPars += '&nDefeito=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Defeito')) == -1 ? "true" : "false" );

        /*
		strPars += '&nEID=';
		
		if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Defeito')) == 0)
		     strPars += escape(0);
		else
		     strPars += escape(343);
        */
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);

    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoMaterialDefeito.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/materialdefeito.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoMaterialDefeito.ondatasetcomplete = sendDataToServer_DSC;
            dsoMaterialDefeito.refresh();
        }
        else {
            lockControlsInModalWin(false);
            glb_localTimerRefresh = window.setInterval('startPesq()', 30, 'JavaScript');
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Erro. Tente novamente.') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_localTimerRefresh = window.setInterval('startPesq()', 30, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

function startBtnOK() {
    btn_onclick(btnOK);
}

// EVENTOS DE GRID PARTICULARES DESTA PAGINA ************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modVincAsstAfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {
    btnOK_Status();
}

function fg_AfterEdit(nRow) {
    fg.TextMatrix(nRow, getColIndexByColKey(fg, 'OK')) = 1;
}

function js_fg_modVincAsstDblClick(grid, Row, Col) {
    var i = 0;
    var bChecked = 1;

    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    if (Col != getColIndexByColKey(grid, 'Defeito'))
        return null;

    if (grid.Row <= 0)
        return null;

    if (grid.ValueMatrix(1, getColIndexByColKey(grid, 'Defeito')) != 0)
        bChecked = 0;

    glb_PassedOneInDblClick = true;

    for (i = 1; i < grid.Rows; i++) {
        grid.TextMatrix(i, getColIndexByColKey(grid, 'Defeito')) = bChecked;
        grid.TextMatrix(i, getColIndexByColKey(grid, 'OK')) = 1;
    }
}
// FINAL DE EVENTOS DE GRID PARTICULARES DESTA PAGINA ***************