/********************************************************************
modalCampanhas.js

Library javascript para o modalCampanhas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Controla se algum dado do grid foi alterado   
var dsoGrid = new CDatatransport('dsoGrid');
var dsoAssociar = new CDatatransport('dsoAssociar');
var dsoDireitos = new CDatatransport('dsoDireitos');

var glb_aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
var glb_nUsuarioID = getCurrUserID();

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalCampanhas.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fgAssociadas, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fgAssociadas
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalCampanhas.ASP

js_modalCampanhasKeyPress(KeyAscii)
js_modalCampanhas_AfterRowColChange
js_modalCampanhas_ValidateEdit()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalCampanhasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Campanhas', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    btnAssociar.disabled = true;
    
    // desabilita o botao OK
    btnOK.disabled = true;
 
    // esconde os botoes OK e Canc
    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    // Ajusta controles
    adjustElementsInForm([['lblIncluir','chkIncluir',3,1,-10],
						  ['lblTipoCampanha','selTipoCampanha',15,1,-9],
						  ['lblCusto','chkCusto',3,1], 
						  ['lblCliente','chkCliente',3,1],
						  ['btnFillGrid','btn',btnOK.offsetWidth,1],
						  ['btnAssociar','btn',btnOK.offsetWidth,1,ELEM_GAP],
						  ['btnDissociar','btn',btnOK.offsetWidth,1,ELEM_GAP],
						  ['btnAprovar','btn',btnOK.offsetWidth,1,ELEM_GAP]],
                          null,null,true);

	chkIncluir.checked = false;
	chkIncluir.onclick = chkIncluir_onclick;
	chkCusto.checked = true;
	chkCliente.checked = false;
	selTipoCampanha.onchange = selTipoCampanha_onchange;
	btnFillGrid.style.height = btnOK.offsetHeight;
	btnAssociar.style.height = btnOK.offsetHeight;
	btnDissociar.style.height = btnOK.offsetHeight;
	btnAprovar.style.height = btnOK.offsetHeight;

    // ajusts o divControles
    with (divControles.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10) + 1;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnAssociar.currentStyle.top, 10) + parseInt(btnAssociar.currentStyle.height, 10) + ELEM_GAP;
    }
    
    //btnAssociar.style.left = chkCliente.offsetLeft + chkCliente.offsetWidth + ELEM_GAP + 5;
    //btnFillGrid.style.left = btnAssociar.offsetLeft + btnAssociar.offsetWidth + ELEM_GAP;
        
    // ajusta o divFGAssociadas
    with (divFGAssociadas.style)
    {
        border = 'none';
        borderWidth = 0;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControles.currentStyle.top, 10) + parseInt(divControles.currentStyle.height, 10) + 2;
        width = modWidth - 2 * ELEM_GAP - 4;    
        height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(top, 10) + 4;
    }
    
    with (fgAssociadas.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFGAssociadas.style.width, 10) - 3;
        height = parseInt(divFGAssociadas.style.height, 10) - 3;
    }
    
    // ajusta o divFGAssociadas
    with (divFGCampanhas.style)
    {
        border = 'none';
        borderWidth = 0;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControles.currentStyle.top, 10) + parseInt(divControles.currentStyle.height, 10) + 2;
        width = modWidth - 2 * ELEM_GAP - 4;    
        height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(top, 10) + 4;
    }
    
    with (fgCampanhas.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFGCampanhas.style.width, 10) - 3;
        height = parseInt(divFGCampanhas.style.height, 10) - 3;
    }
    
    startGridInterface(fgAssociadas);
    fgAssociadas.FontSize = '8';
    fgAssociadas.Cols = 0;
    fgAssociadas.Cols = 1;
    fgAssociadas.ColWidth(0) = parseInt(divFGAssociadas.currentStyle.width, 10) * 18;    
    fgAssociadas.Redraw = 2;

    startGridInterface(fgCampanhas);
    fgCampanhas.FontSize = '8';
    fgCampanhas.Cols = 0;
    fgCampanhas.Cols = 1;
    fgCampanhas.ColWidth(0) = parseInt(divFGCampanhas.currentStyle.width, 10) * 18;    
    fgCampanhas.Redraw = 2;
    
    setGridsVisibility();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        ;
    }
    else if (controlID == 'btnCanc')
    {
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }
    else if (controlID == 'btnFillGrid')
    {
		fillGridCampanhas();
    }
    else if (controlID == 'btnAssociar')    
    {
		associarCampanha(0);
    }
    else if (controlID == 'btnDissociar')    
    {
		associarCampanha(1);
    }
    else if (controlID == 'btnAprovar')
    {
		aprovarCampanha();
    }
}

function chkIncluir_onclick()
{
	setGridsVisibility();
	fillGridData();
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var bHasRowsInGrid = fgAssociadas.Rows > 1;
	btnAssociar.disabled = !(bHasRowsInGrid);
	
	chkIncluir.disabled = (glb_nEstadoID != 21);

	lblCusto.style.visibility = ((chkIncluir.checked && (selTipoCampanha.value==1321)) ? 'inherit' : 'hidden');
	chkCusto.style.visibility = ((chkIncluir.checked && (selTipoCampanha.value==1321)) ? 'inherit' : 'hidden');

	lblCliente.style.visibility = ((chkIncluir.checked && (selTipoCampanha.value==1321)) ? 'inherit' : 'hidden');
	chkCliente.style.visibility = ((chkIncluir.checked && (selTipoCampanha.value==1321)) ? 'inherit' : 'hidden');
	
	btnFillGrid.style.visibility = (chkIncluir.checked ? 'inherit' : 'hidden');
	btnFillGrid.disabled = ! ((fgAssociadas.Row > 0) && (selTipoCampanha.value > 0) && (selTipoCampanha.value != 1322));
	btnAssociar.style.visibility = (chkIncluir.checked ? 'inherit' : 'hidden');
	btnAssociar.disabled = ! ((fgCampanhas.Row > 0) && (fgAssociadas.Row > 0));
	
	btnDissociar.disabled = true;
	if ((fgAssociadas.Row > 0) && (glb_nEstadoID==21))
	{
		if ( (getCellValueByColKey(fgAssociadas, 'PedIteCampanhaID', fgAssociadas.Row) != '') &&
			 (getCellValueByColKey(fgAssociadas, 'TipoCampanhaID', fgAssociadas.Row) != '') &&
			 (getCellValueByColKey(fgAssociadas, 'TipoCampanhaID', fgAssociadas.Row) != 1322) )
			btnDissociar.disabled = false;
	}
	
	btnAprovar.style.visibility = (chkIncluir.checked ? 'hidden' : 'inherit');
	btnAprovar.aprova = 0;

	direitoAprovaCampanha();
	
	if (fgAssociadas.Row > 0)
	{
		if ( (getCellValueByColKey(fgAssociadas, 'PedIteCampanhaID', fgAssociadas.Row) != '') &&
			 (getCellValueByColKey(fgAssociadas, 'TipoCampanhaID', fgAssociadas.Row) != '') &&
			 (getCellValueByColKey(fgAssociadas, 'TipoCampanhaID', fgAssociadas.Row) != 1322) &&
			 (getCellValueByColKey(fgAssociadas, 'Aprovador', fgAssociadas.Row) == '') )
			btnAprovar.aprova = 1;
	}
	else
		btnAprovar.disabled = true;
		
	btnAprovar.value = (btnAprovar.aprova ? 'Aprovar' : 'Desaprovar');
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    lockControlsInModalWin(true);
    
	var strSQL = '';
	var sFiltro = '';

	if ((chkIncluir.checked) && (selTipoCampanha.value != 0))
		strSQL = 'SELECT a.Ordem AS Ordem, b.ConceitoID AS ProdutoID, b.Conceito AS Produto, NULL AS TipoCampanha,' +
			'NULL AS Campanha, NULL AS Codigo, NULL AS Fornecedor, a.Quantidade AS Quantidade, NULL AS Moeda,' +
			'NULL AS ValorUnitario, ' +
			'NULL AS ValorTotal, NULL AS Aprovador, NULL AS TipoCampanhaID, a.PedItemID AS PedItemID, NULL AS PedIteCampanhaID, NULL AS CamProdutoID, ' +
			'NULL AS CampanhaID ' +
		'FROM Pedidos_Itens a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
		'WHERE (a.PedidoID = ' + glb_nPedidoID + ' AND a.ProdutoID = b.ConceitoID AND ' +
			'(SELECT COUNT(*) FROM Pedidos_Itens_Campanhas aa WITH(NOLOCK), Campanhas_Produtos bb WITH(NOLOCK), Campanhas cc WITH(NOLOCK) ' +
				'WHERE (a.PedItemID = aa.PedItemID AND aa.CamProdutoID = bb.CamProdutoID AND ' +
					'bb.CampanhaID = cc.CampanhaID AND cc.TipoCampanhaID = ' + selTipoCampanha.value + ')) = 0) ' +
		'UNION ALL ';

	if (selTipoCampanha.value != 0)
		sFiltro += ' AND e.TipoCampanhaID = ' + selTipoCampanha.value + ' ';

    // zera o grid
    fgAssociadas.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	strSQL += 'SELECT a.Ordem AS Ordem, b.ConceitoID AS ProdutoID, b.Conceito AS Produto, f.ItemMasculino AS TipoCampanha,' +
		'e.Campanha, e.Codigo, g.Fantasia AS Fornecedor, a.Quantidade, i.SimboloMoeda AS Moeda,' +
		'CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(c.Valor, a.Quantidade)) AS ValorUnitario, ' +
		'c.Valor AS ValorTotal, j.Fantasia AS Aprovador, e.TipoCampanhaID, a.PedItemID, c.PedIteCampanhaID, d.CamProdutoID AS CamProdutoID, ' +
		'e.CampanhaID ' +
	'FROM Pedidos_Itens a WITH(NOLOCK) ' +
	    'INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ProdutoID = b.ConceitoID) ' +
	    'INNER JOIN Pedidos_Itens_Campanhas c WITH(NOLOCK) ON (a.PedItemID = c.PedItemID) ' +
		'LEFT OUTER JOIN Pessoas j WITH(NOLOCK) ON (c.AprovadorID = j.PessoaID) ' +
		'INNER JOIN Campanhas_Produtos d WITH(NOLOCK) ON (c.CamProdutoID = d.CamProdutoID) ' +
		'INNER JOIN Campanhas e WITH(NOLOCK) ON (d.CampanhaID = e.CampanhaID) ' +
		'INNER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (e.TipoCampanhaID = f.ItemID) ' +
		'LEFT OUTER JOIN Pessoas g WITH(NOLOCK) ON (e.FornecedorID = g.PessoaID), ' +
		'Recursos h WITH(NOLOCK) ' +
		'INNER JOIN Conceitos i WITH(NOLOCK) ON (h.MoedaID = i.ConceitoID) ' +
	'WHERE (a.PedidoID = ' + glb_nPedidoID + ' AND ' +
		'h.TipoRecursoID = 1 AND h.EstadoID = 2 ' + sFiltro + ') ' +
	'ORDER BY Ordem, TipoCampanhaID';

    dsoGrid.SQL = strSQL;

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
    showExtFrame(window, true);
    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fgAssociadas.Redraw = 0;
    fgAssociadas.Editable = false;
    fgAssociadas.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

    headerGrid(fgAssociadas,['Ordem' ,
				   'ID',
				   'Produto',
				   'Tipo Camp',
				   'ID',
				   'Campanha',
				   'C�digo',
                   'Fornecedor',
                   'Quant',
                   '$',
                   'Valor Unit',
                   'Valor Total',
                   'Aprovador',
                   'TipoCampanhaID',
                   'PedItemID',
                   'CamProdutoID',
                   'PedIteCampanhaID'],[13,14,15,16]);
                       
    fillGridMask(fgAssociadas,dsoGrid,['Ordem' ,
							'ProdutoID',
							'Produto',
							'TipoCampanha',
							'CampanhaID',
							'Campanha',
							'Codigo',
							'Fornecedor',
							'Quantidade',
							'Moeda',
							'ValorUnitario',
							'ValorTotal',
							'Aprovador',
							'TipoCampanhaID',
							'PedItemID',
							'CamProdutoID',
							'PedIteCampanhaID'],
                             ['','','','','','','','','','','','','','','','',''],
                             ['','','','','','','','','','','###,###,###.00','###,###,###.00','','','','','']);

	gridHasTotalLine(fgAssociadas, 'Totais', 0xC0C0C0, null, true, [[1,'#####','C'],
														  [8,'######','S'],
														  [11,'###,###,###.00','S']]);
    fgAssociadas.AutoSizeMode = 0;
    fgAssociadas.AutoSize(0,fgAssociadas.Cols-1);
    fgAssociadas.FrozenCols = 3;
    fgAssociadas.Redraw = 2;

    // Merge de Colunas
    fgAssociadas.MergeCells = 4;
    fgAssociadas.MergeCol(-1) = true;

	alignColsInGrid(fgAssociadas,[0,1, 8, 10, 11]);    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if ( fgAssociadas.Rows > 1 )
        fgAssociadas.Col = 3;
    
    lockControlsInModalWin(false);

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fgAssociadas.Row < 1) && (fgAssociadas.Rows > 1) )
        fgAssociadas.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fgAssociadas.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fgAssociadas.Rows > 1)
    {
        window.focus();
        fgAssociadas.focus();
    }                
    else
    {
        ;
    }            

	fgCampanhas.Rows = 1;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridCampanhas()
{
    lockControlsInModalWin(true);
	var sFiltroCampanha = '';
	var nProdutoID = 0;
	var nPedItemID = 0;
	var nQuantidade = 0;
	
	if (fgAssociadas.Row > 0)
	{
		nProdutoID = getCellValueByColKey(fgAssociadas, 'ProdutoID', fgAssociadas.Row);
		nPedItemID = fgAssociadas.ValueMatrix(fgAssociadas.Row, getColIndexByColKey(fgAssociadas, 'PedItemID'));
		nQuantidade = fgAssociadas.ValueMatrix(fgAssociadas.Row, getColIndexByColKey(fgAssociadas, 'Quantidade'));
	}		
	
	if (selTipoCampanha.value > 0)
		sFiltroCampanha = ' AND b.TipoCampanhaID = ' + selTipoCampanha.value + ' ';
		
	var sFiltro = '';

	if (selTipoCampanha.value == 1321)
	{
		if (chkCusto.checked)
			sFiltro = ' AND b.Custo = 1 ';

		if (chkCliente.checked)
			sFiltro += ' AND b.ClienteID = ' + glb_nParceiroID + ' ';
	}
	else if (selTipoCampanha.value == 1323)
	{
		sFiltro += ' AND dbo.fn_PedidoItem_Bundle(' + nPedItemID + ', a.CamProdutoID, 0, NULL, 2) IS NOT NULL ';
	}
		
	var strSQL = 'SELECT a.CampanhaID, d.RecursoAbreviado AS Estado, b.Campanha AS Campanha,' +
		'b.Codigo, e.Fantasia AS Fornecedor, f.Fantasia AS Cliente, a.Quantidade, g.SimboloMoeda AS Moeda,' +
		'a.ValorUnitario, dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 2) AS Custo,' +
		'dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 3) AS ValorCampanha,' +
		'dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 7) AS SaldoQuantidade,' +
		'dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 8) AS SaldoValor, a.CamProdutoID, ' +
		'ISNULL((SELECT TOP 1 CONVERT(NUMERIC(1), cc.RebatePermiteDPA) FROM Pedidos_Itens_Campanhas aa WITH(NOLOCK), Campanhas_Produtos bb WITH(NOLOCK), Campanhas cc WITH(NOLOCK) ' +
			'WHERE (aa.PedItemID = ' + nPedItemID + ' AND aa.CamProdutoID = bb.CamProdutoID AND ' +
				'bb.CampanhaID = cc.CampanhaID AND cc.TipoCampanhaID = 1322 )), 0) AS RebatePermiteDPA, ' +
		'dbo.fn_PedidoItem_CampanhaTotais(' + nPedItemID + ', 1322, 1, 1) AS ValorTotalRebate, ' +
		'CONVERT(NUMERIC(11,2), dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, 0, 3) * ' + nQuantidade + ') AS ValorTotalCampanha ' +
	'FROM Campanhas_Produtos a WITH(NOLOCK) ' +
	    'INNER JOIN Campanhas b WITH(NOLOCK) ON (a.CampanhaID = b.CampanhaID) ' +
	    'LEFT OUTER JOIN Pessoas e WITH(NOLOCK) ON (b.FornecedorID = e.PessoaID) ' +
	    'LEFT OUTER JOIN Pessoas f WITH(NOLOCK) ON (b.ClienteID = f.PessoaID) ' +
	    'INNER JOIN Campanhas_Empresas c WITH(NOLOCK) ON (b.CampanhaID = c.CampanhaID) ' +
	    'INNER JOIN Recursos d WITH(NOLOCK) ON (b.EstadoID = d.RecursoID) ' +
	    'INNER JOIN Conceitos g WITH(NOLOCK) ON (b.MoedaID = g.ConceitoID ) ' +
	'WHERE (a.ProdutoID = ' + nProdutoID + ' AND ' +
		'b.EstadoID = 41 ' + sFiltroCampanha + ' AND ' +
		'ISNULL(dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 7), 1) > 0 AND ' +
		'ISNULL(dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 8), 1) > 0 AND ' +
		'dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 3) > 0 AND ' +
		'c.EmpresaID = ' + glb_aEmpresaData[0] + ' AND ' +
		'dbo.fn_Data_Zero(GETDATE()) BETWEEN ISNULL(dbo.fn_CampanhaProduto_Datas(a.CamProdutoID, 1), dbo.fn_Data_Zero(GETDATE())) AND ' +
						   					'ISNULL(dbo.fn_CampanhaProduto_Datas(a.CamProdutoID, 2), dbo.fn_Data_Zero(GETDATE())) ' + sFiltro + ') ' +
	'ORDER BY b.CampanhaID';

    // zera o grid
    fgCampanhas.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL = strSQL;
    dsoGrid.ondatasetcomplete = fillGridCampanhas_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridCampanhas_DSC()
{
    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fgCampanhas.Redraw = 0;
    fgCampanhas.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	var aHoldCols = [8,9,13,14,15,16];
	var bEditable = false;
	
	if (selTipoCampanha.value == 1321)
	{
		if (!chkCusto.checked) 
			bEditable = true;
		else
			aHoldCols = [13,14,15,16];	
	}

    headerGrid(fgCampanhas,['ID', 
						'Est', 
						'Campanha',
						'C�digo', 
						'Fornecedor', 
						'Cliente', 
						'Quant', 
						'$',
						'Valor Unit', 
						'Custo',
						'Valor Camp',
						'Saldo Quant',
						'Saldo Valor',
						'CamProdutoID',
						'RebatePermiteDPA',
						'ValorTotalRebate',
						'ValorTotalCampanha'],aHoldCols);
                       
    fillGridMask(fgCampanhas,dsoGrid,['CampanhaID*', 
							'Estado*', 
							'Campanha*',
							'Codigo*', 
							'Fornecedor*', 
							'Cliente*', 
							'Quantidade*', 
							'Moeda*',
							'ValorUnitario*', 
							'Custo*',
							'ValorCampanha',
							'SaldoQuantidade*',
							'SaldoValor*',
							'CamProdutoID',
							'RebatePermiteDPA',
							'ValorTotalRebate',
							'ValorTotalCampanha'],
                             ['','', '','', '', '', '999999', '','99999999.99', '99999999.99','99999999.99','99999999.99','99999999.99','','','',''],
                             ['','', '','', '', '', '######', '','########.##', '########.##','########.##','########.##','########.##','','','','']);

    fgCampanhas.AutoSizeMode = 0;
    fgCampanhas.AutoSize(0,fgCampanhas.Cols-1);
    fgCampanhas.FrozenCols = 3;
    fgCampanhas.Redraw = 2;

    // Merge de Colunas
    fgCampanhas.MergeCells = 4;
    fgCampanhas.MergeCol(-1) = true;

	alignColsInGrid(fgCampanhas,[0,6, 8, 9, 10,11,12]);    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if ( fgCampanhas.Rows > 1 )
        fgCampanhas.Col = 10;
    
    lockControlsInModalWin(false);

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fgCampanhas.Row < 1) && (fgCampanhas.Rows > 1) )
        fgCampanhas.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fgCampanhas.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fgCampanhas.Rows > 1)
    {
        window.focus();
        fgCampanhas.focus();
    }                
    else
    {
        ;
    }            

	fgCampanhas.Editable = bEditable;
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}


/********************************************************************
Funcao criada pelo programador.
Associa a Campanha

Parametro:
nDissociar 0-Associa, 1-Dissocia

Retorno:
nenhum
********************************************************************/
function associarCampanha(nDissociar)
{
    var nPedItemID = fgAssociadas.ValueMatrix(fgAssociadas.Row, getColIndexByColKey(fgAssociadas, 'PedItemID'));
    var nCamProdutoID = 0;
    var nValor = 0;
    var nValorMaximo = 0;
    var strPars = '?';
    var nRebatePermiteDPA = 0;
    var nValorTotalRebate = 0;
    var nValorTotalCampanha = 0;
    var sMsg = '';
    var _retMsg = '';

    if (nDissociar == 1)
		nCamProdutoID = fgAssociadas.ValueMatrix(fgAssociadas.Row, getColIndexByColKey(fgAssociadas, 'CamProdutoID'));
    else
		nCamProdutoID = fgCampanhas.ValueMatrix(fgCampanhas.Row, getColIndexByColKey(fgCampanhas, 'CamProdutoID'));

    lockControlsInModalWin(true);

	if ( (selTipoCampanha.value == 1321) && (nDissociar == 0) )
	{
		if (!chkCusto.checked)
		{
			nValor = parseFloat(replaceStr(getCellValueByColKey(fgCampanhas, 'ValorCampanha', fgCampanhas.Row), ',', '.'));
			nValorMaximo = parseFloat(replaceStr(getCellValueByColKey(fgCampanhas, 'ValorUnitario*', fgCampanhas.Row), ',', '.'));

			if ((nValor == 0) || (nValor > nValorMaximo))
			{
				//destrava a Interface
				lockControlsInModalWin(false);

				if ( window.top.overflyGen.Alert('Valor inv�lido') == 0 )
					return null;

				return null;
			}				
		}

		nRebatePermiteDPA = parseFloat(replaceStr(getCellValueByColKey(fgCampanhas, 'RebatePermiteDPA', fgCampanhas.Row), ',', '.'));
		nValorTotalRebate = parseFloat(replaceStr(getCellValueByColKey(fgCampanhas, 'ValorTotalRebate', fgCampanhas.Row), ',', '.'));
		nValorTotalCampanha = parseFloat(replaceStr(getCellValueByColKey(fgCampanhas, 'ValorTotalCampanha', fgCampanhas.Row), ',', '.'));

		if ((nValorTotalRebate > 0) && (nRebatePermiteDPA == 0))
		{
			sMsg = 'Rebate n�o permite DPA e ser� deletado.\n';
			
			if (nValorTotalRebate > nValorTotalCampanha)
				sMsg += 'Valor do DPA inferior ao valor do Rebate.\n';

			sMsg += 'Incluir DPA assim mesmo?';

			_retMsg = window.top.overflyGen.Confirm(sMsg);
		
			if ( _retMsg == 0 )
				return null;
			else if ( _retMsg == 2 )
			{
				//destrava a Interface
				lockControlsInModalWin(false);
				return null;
			}
		}
	}

    strPars += 'nPedItemID=' + escape(nPedItemID);
    strPars += '&nCamProdutoID=' + escape(nCamProdutoID);
    strPars += '&nValor=' + escape(nValor);
    strPars += '&nDissociar=' + escape(nDissociar);

    dsoAssociar.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/associarcampanha.aspx'+strPars;
    dsoAssociar.ondatasetcomplete = associarCampanha_DSC;
    dsoAssociar.Refresh();
}

/********************************************************************
Funcao criada pelo programador.
Retorno da funcao que associa a Campanha

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function associarCampanha_DSC()
{
    //destrava a Interface
    lockControlsInModalWin(false);

	if ( !((dsoAssociar.recordset.BOF) && (dsoAssociar.recordset.EOF)) )
	{
		if ((dsoAssociar.recordset['Resultado'].value != null) && (dsoAssociar.recordset['Resultado'].value != ''))
		{
            if ( window.top.overflyGen.Alert(dsoAssociar.recordset['Resultado'].value) == 0 )
                return null;
		}
		else
		{
			fillGridData();
		}
	}

}

/********************************************************************
Funcao criada pelo programador.
aprovar Campanha

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function aprovarCampanha()
{
    var strPars = '?';
    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')') );
    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')') );

	lockControlsInModalWin(true);
    strPars += 'nPedIteCampanhaID=' + escape(getCellValueByColKey(fgAssociadas, 'PedIteCampanhaID', fgAssociadas.Row));
    strPars += '&nUsuarioID=' + escape(glb_nUsuarioID);
    strPars += '&bAprovar=' + escape(btnAprovar.aprova);
    strPars += '&bDireito=' + escape((nA1 && nA2) ? '1' : '0');

    dsoAssociar.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/aprovarcampanha.aspx' + strPars;
    dsoAssociar.ondatasetcomplete = aprovarCampanha_DSC;
    dsoAssociar.Refresh();
}

/********************************************************************
Funcao criada pelo programador.
Retorno da funcao que associa a Campanha

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function aprovarCampanha_DSC()
{
    //destrava a Interface
    lockControlsInModalWin(false);

	if ( !((dsoAssociar.recordset.BOF) && (dsoAssociar.recordset.EOF)) )
	{
	    if ((dsoAssociar.recordset['Resultado'].value != null) && (dsoAssociar.recordset['Resultado'].value != '') && (dsoAssociar.recordset['Resultado'].value != ' '))
		{
            if ( window.top.overflyGen.Alert(dsoAssociar.recordset['Resultado'].value) == 0 )
                return null;
		}
		else
		{
			fillGridData();
		}
	}

}

function setGridsVisibility()
{
	if (chkIncluir.checked)
	{
		divFGCampanhas.style.visibility = 'inherit';
		fgCampanhas.style.visibility = 'inherit';
		divFGCampanhas.style.top = parseInt(divControles.currentStyle.top, 10) + parseInt(divControles.currentStyle.height, 10) + 2;
		divFGCampanhas.style.height = 175;
		fgCampanhas.style.height = 175;

		divFGAssociadas.style.height = 174;
		fgAssociadas.style.height = 174;
		divFGAssociadas.style.top = divFGCampanhas.offsetTop + fgCampanhas.offsetHeight + ELEM_GAP;
	}
	else
	{
		divFGAssociadas.style.top = parseInt(divControles.currentStyle.top, 10) + parseInt(divControles.currentStyle.height, 10) + 2;
		divFGAssociadas.style.height = 362;
		fgAssociadas.style.height = parseInt(divFGAssociadas.style.height, 10) - 3;
		divFGCampanhas.style.visibility = 'hidden';
		fgCampanhas.style.visibility = 'hidden';
    }
}

function selTipoCampanha_onchange()
{
	setLabels();
	fillGridData();
}

function setLabels()
{
	setLabelOfControlEx(lblTipoCampanha, selTipoCampanha);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalCampanhasKeyPress(KeyAscii)
{
    ;
}

function fg_modalCampanhas_AfterEdit(grid, nRow, nCol)
{
    // Ajuste para gravar o valor formatado corretamente. LYF 16/10/2018
    if (nCol == getColIndexByColKey(fgCampanhas, 'ValorCampanha')) {
        fgCampanhas.TextMatrix(nRow, nCol) = treatNumericCell(fgCampanhas.TextMatrix(nRow, nCol));
    }
}

function fg_modalCampanhas_AfterRowColChange(grid)
{
    if (grid == fgAssociadas)
    {
		fgCampanhas.Rows = 1;
    }

    setupBtnsFromGridState();
}

function fg_EnterCell_Prg()
{
	return null;
}

function direitoAprovaCampanha()
{
    setConnection(dsoDireitos);

    //Direitos do lista de pre�o
    dsoDireitos.SQL = 'SELECT TOP 1 Direitos_btn.Alterar1 AS A1, Direitos_btn.Alterar2 AS A2 ' +
                        'FROM RelacoesPesRec RelPesRec ' +
                            'INNER JOIN RelacoesPesRec_Perfis RelPesRecPerfis WITH(NOLOCK) ON (RelPesRec.RelacaoID = RelPesRecPerfis.RelacaoID) ' +
                            'INNER JOIN Recursos Perfis WITH(NOLOCK) ON (RelPesRecPerfis.PerfilID = Perfis.RecursoID) ' +
                            'INNER JOIN Recursos_Direitos Direitos WITH(NOLOCK) ON (Perfis.RecursoID = Direitos.PerfilID) ' +
                            'INNER JOIN Recursos_Direitos Direitos_btn WITH(NOLOCK) ON (Direitos_btn.RecursoMaeID = Direitos.RecursoID) AND (Direitos.PerfilID = Direitos_btn.PerfilID) ' +
                        'WHERE (RelPesRec.SujeitoID IN ((SELECT ' + glb_nUsuarioID + ' ' +
                                                        'UNION ALL ' +
                                                        'SELECT UsuarioDeID ' +
                                                            'FROM DelegacaoDireitos ' +
                                                            'WHERE (UsuarioParaID = ' + glb_nUsuarioID + ' AND GETDATE() BETWEEN dtInicio AND dtFim))) AND ' +
                                'RelPesRec.ObjetoID = 999 AND RelPesRec.TipoRelacaoID = 11 AND RelPesRecPerfis.EmpresaID = ' + glb_aEmpresaData[0] + '  AND Perfis.EstadoID = 2 AND ' +
                                'Direitos.RecursoMaeID = 5112 AND Direitos.RecursoID =  24002 AND Direitos_btn.ContextoID = 5112 AND Direitos_btn.RecursoID = 40006) ' +
                        'ORDER BY Direitos_btn.Alterar2 DESC, Direitos_btn.Alterar1 DESC ';

    dsoDireitos.ondatasetcomplete = dsoDireitos_DSC;
    dsoDireitos.Refresh();
}

function dsoDireitos_DSC()
{
    var nA1 = dsoDireitos.recordset['A1'].value;
    var nA2 = dsoDireitos.recordset['A2'].value;

    if ((nA1 == 1) && (nA2 == 1))
        btnAprovar.disabled = false;
    else
        btnAprovar.disabled = true;
}

// FINAL DE EVENTOS DE GRID *****************************************
