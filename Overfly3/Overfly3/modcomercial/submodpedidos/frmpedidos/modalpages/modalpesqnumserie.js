/********************************************************************
modalpesqnumserie.js

Library javascript para o modalpesqnumserie.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_FGTimerInt = null;
var glb_nDOSGrid1 = 0;
var glb_nDOSGrid2 = 0;
var glb_bFocusToFG = false;
var glb_PassedOneInDblClick = false;

var dsoProduto = new CDatatransport('dsoProduto');
var dsoGrid = new CDatatransport('dsoGrid');
var dsoCidade = new CDatatransport('dsoCidade');
var dsoGrid2 = new CDatatransport('dsoGrid2');

// FINAL DE VARIAVEIS GLOBAIS ***************************************
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalpesqnumserie.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalpesqnumserie.ASP

js_fg_modalpesqnumserieBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalpesqnumserieDblClick( grid, Row, Col)
js_modalpesqnumserieKeyPress(KeyAscii)
js_modalpesqnumserie_AfterRowColChange
js_modalpesqnumserie_ValidateEdit()
js_modalpesqnumserie_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalpesqnumserieBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Pesquisa de n�meros de s�rie', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
    
    with (divButtons1.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = ELEM_GAP * 4;
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        //top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        top = divButtons1.offsetTop + divButtons1.offsetHeight + 1;
        width = modWidth - 2 * ELEM_GAP - 6;    
        //height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
        height = 180;
    }

    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
	with (divButtons2.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divFG.offsetTop + divFG.offsetHeight;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = ELEM_GAP;
    }    
    
    // ajusta o divFG
    with (divFG2.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        //top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        top = divButtons2.offsetTop + divButtons2.offsetHeight + 1;
        width = modWidth - 2 * ELEM_GAP - 6;    
        //height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
        height = 180;
    }
    
    with (fg2.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10);
        height = parseInt(divFG2.style.height, 10);
    }
    
    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    if (glb_nPedidoID != 0)
    {
		lblPesquisa.style.visibility = 'hidden';
		txtPesquisa.style.visibility = 'hidden';
		
		adjustElementsInForm([['lblDetalhe','chkDetalhe',3,1,-10,-10],
							['lblProduto','selProduto',25,1,-5],
							['lblIdentificador','txtIdentificador',18,1],
							['lblNumeroSerie','txtNumeroSerie',19,1],
							['btnListar','btn',78,1,5]],null,null,true);
	}
	else
	{
		adjustElementsInForm([['lblDetalhe','chkDetalhe',3,1,-10,-10],
							['lblPesquisa','txtPesquisa',13,1,-5],
							['lblProduto','selProduto',25,1],
							['lblIdentificador','txtIdentificador',18,1],
							['lblNumeroSerie','txtNumeroSerie',19,1],
							['btnListar','btn',78,1,5]],null,null,true);
	}
	
	selProduto.disabled = (selProduto.options.length == 0);
	selProduto.onchange = selProduto_onchange;
	txtPesquisa.maxLength = 30;
	btnListar.style.height = 24;
	txtPesquisa.onkeydown = txtPesquisa_onkeydown;
	txtIdentificador.onkeydown = txtIdentificador_onkeydown;
	txtNumeroSerie.onkeydown = txtNumeroSerie_onkeydown;
	
	chkDetalhe.onclick = chkDetalhe_onclick;
	chkDetalhe_onclick();
	
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;

    startGridInterface(fg2);
    fg2.Cols = 0;
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;    
    fg2.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		;   
    }
    // codigo privado desta janela
    else if (controlID == 'btnCanc')
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }
    else if (controlID == 'btnListar')
    {
		fillGridData();
    }
    else if (controlID == 'btnGravar')
    {
		saveDataInGrid();
    }
    else if (controlID == 'btnExcluir')
    {
		excluirLinha(1);
    }
    else if (controlID == 'btnIncluir')
    {
		incluir();
    }
    else if (controlID == 'btnPC')
    {
		fillCidadeDestinoID();
    }
}

function fillCidadeDestinoID()
{
	if (fg.Row > 0)
	{
		// Altera o campo do dso com o novo valor
		if (dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)
		{
			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CidadeDestinoID')) = 
				(selCidadeID.value == 0 ? '' : selCidadeID.value);

			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '^CidadeDestinoID^dsoLkp01^FldID^FldName*')) = 
				selCidadeID.options[selCidadeID.selectedIndex].innerText;
		
			return null;
		}

		dsoGrid.recordset.MoveFirst();
		
		if (fg.TextMatrix(fg.Row, fg.Cols-1) != '')
			dsoGrid.recordset.Find(fg.ColKey(fg.Cols-1) + ' = ' + fg.TextMatrix(fg.Row, fg.Cols-1));

		if (!dsoGrid.recordset.EOF)
		{
			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CidadeDestinoID')) = 
				(selCidadeID.value == 0 ? '' : selCidadeID.value);

			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '^CidadeDestinoID^dsoLkp01^FldID^FldName*')) = 
				selCidadeID.options[selCidadeID.selectedIndex].innerText;
				
			if (fg.TextMatrix(fg.Row, fg.Cols-1) != '')
				dsoGrid.recordset['CidadeDestinoID'].value = (selCidadeID.value == 0 ? null : selCidadeID.value);

			glb_dataGridWasChanged = true;
			setupBtnsFromGridState();
		}
	}
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    showExtFrame(window, true);
    
    window.focus();
    chkDetalhe.focus();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
	;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    if (glb_FGTimerInt != null)
    {
        window.clearInterval(glb_FGTimerInt);
        glb_FGTimerInt = null;
    }

	if (glb_nPedidoID == 0)
	{
		if (trimStr(txtNumeroSerie.value).length < 4)
		{
            if ( window.top.overflyGen.Alert ('N�mero de s�rie inv�lido.') == 0 )
                return null;
            
            return null;
		}
	}

    var nTransportadoraID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');
    var nPesTransportadoraID = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'fg.TextMatrix(fg.Row, fg.Cols-1)');
	var aEmpresa = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'getCurrEmpresaData()');

    lockControlsInModalWin(true);
    
    // zera o grid
    fg.Rows = 1;

    glb_dataGridWasChanged = false;
	glb_nDOSGrid1 = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);
    dsoGrid.SQL = 'SET NOCOUNT ON ' +
		'EXEC sp_Produto_NumerosSerie ' + (selProduto.selectedIndex <= 0 ? 'NULL' : selProduto.value) + ', ' +
		(trimStr(txtIdentificador.value) == '' ? 'NULL' : '\'' + trimStr(txtIdentificador.value) + '\'') + ', ' +
		(trimStr(txtNumeroSerie.value) == '' ? 'NULL' : '\'' + trimStr(txtNumeroSerie.value) + '\'') + ',' +
		(glb_nPedidoID == 0 ? 'NULL' : glb_nPedidoID) + ', NULL, NULL ' +
		'SET NOCOUNT OFF';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDOSGrid1--;
	
	if (glb_nDOSGrid1 > 0)
		return;

    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FontSize = '8';
    fg.FrozenCols = 3;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
    
    var aHoldCols = new Array();

	if (glb_nPedidoID == 0)
		aHoldCols = [2, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15]; 
	else
		aHoldCols = [13, 14, 15]; 
	
    headerGrid(fg,['Produto',
                    'ID',
                    'Quant',
                    'Q/Cx',
                    'Cx',
                    'Un',
                    'Caixa',
                    'Qt Gat',
                    'N�mero S�rie   ',
					'Estoque',
					'OP',
                    'Asstec',
                    'Colaborador',
                    'NumeroSerieID',
                    'NumMovimentoID',
                    'PedProdutoID'], aHoldCols);

    fillGridMask(fg,dsoGrid,['Produto',
                                'ProdutoID',
                                'QuantidadeItem',
                                'QuantidadePorCaixa',
                                'QuantidadeCaixas',
                                'QuantidadeUnidades',
                                'Caixa',
                                'QuantidadeGatilhada',
                                'NumeroSerie',
                                'Estoque',								
								'OrdemProducaoID',
                                'AsstecID',
                                'Colaborador',
                                'NumeroSerieID',
                                'NumMovimentoID',
                                'PedProdutoID'],
                                ['','','','','','','','','','','','','','','',''],
                                ['','','','','','','','','','','','','','','','']);

    glb_aCelHint = [[0,2,'Quantidade do �tem'],
					[0,7,'Quantidade gatilhada']];

    alignColsInGrid(fg,[1,2,3,4,5,6,7,10,11]);                           

    // Calcula a Ordem
    var nOrdem, nProdutoID, nCaixa, nNumeroSerieID;
    var nItens, nQuantidade, nCaixas, nUnidades, nCaixasGatilhadas;
    
    nOrdem = 0;
    nItens = 0;
    nQuantidade = 0;
    nCaixas = 0;
    nUnidades = 0;
    nQuantidadeGatilhada = 0;
    nCaixasGatilhadas = 0;
    
    if (fg.Rows>1)
    {
        nProdutoID = fg.TextMatrix(1, getColIndexByColKey(fg, 'ProdutoID'));
        nCaixa = fg.TextMatrix(1, getColIndexByColKey(fg, 'Caixa'));
        for (i=1; i<fg.Rows; i++)
        {
			if (fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoID')) != nProdutoID)
				nOrdem = 1;
			else
				nOrdem++;
				
			if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoID')) != nProdutoID) || (i==1))
			{
				nItens++;
				nQuantidade += fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeItem'));
				nCaixas += fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeCaixas'));
				nUnidades += fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeUnidades'));
			}

			if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'Caixa')) != nCaixa) || (i==1))
			{
				if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Caixa')) != '')
					nCaixasGatilhadas ++;
			}
	        
			nProdutoID = fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoID'));
			nCaixa = fg.TextMatrix(i, getColIndexByColKey(fg, 'Caixa'));
        }
		
		gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true,
			[[getColIndexByColKey(fg, 'Caixa'),'#########','S'],
				[getColIndexByColKey(fg, 'QuantidadeGatilhada'),'#########','S']]);

		fg.TextMatrix(1, getColIndexByColKey(fg, 'ProdutoID')) = nItens;
		fg.TextMatrix(1, getColIndexByColKey(fg, 'QuantidadeItem')) = nQuantidade;
		fg.TextMatrix(1, getColIndexByColKey(fg, 'QuantidadeCaixas')) = nCaixas;
		fg.TextMatrix(1, getColIndexByColKey(fg, 'QuantidadeUnidades')) = nUnidades;
		fg.TextMatrix(1, getColIndexByColKey(fg, 'Caixa')) = nCaixasGatilhadas;
		fg.TextMatrix(1, getColIndexByColKey(fg, 'NumeroSerie')) = fg.Rows - 2;
    }
    
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;
    
    lockControlsInModalWin(false);

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if (fg.Rows > 2)
        fg.Row = 2;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

function fillGridData2()
{
	fg2.Rows = 1;

	if (fg.Row < 2)
		return null;
		
	if (!chkDetalhe.checked)
	{
		fg2.Rows = 1;
		return null;
	}
	
	var strSQL = '';
	var nNumeroSerieID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'NumeroSerieID'));
	var nPedProdutoID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PedProdutoID'));
	
	if ((nNumeroSerieID == null) || (nNumeroSerieID == ''))
	{
		if ((nPedProdutoID == null) || (nPedProdutoID == ''))
			return null;
	}
	
	if ((nPedProdutoID == null) || (nPedProdutoID == ''))
	{
		if ((nNumeroSerieID == null) || (nNumeroSerieID == ''))
			return null;
	}
		
    lockControlsInModalWin(true);

    glb_nDOSGrid2 = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid2);
    
    //INICIO DE NOVO NS----------------------------------------------
    
    strSQL = 'SELECT dbo.fn_Pedido_Datas(b.PedidoID, 1) AS Data, e.Fantasia AS Empresa, ' +
			'b.PedidoID, c.NotaFiscal, d.Operacao AS Transacao, f.Fantasia AS Pessoa ';
	
	if ( !((nNumeroSerieID == null) || (nNumeroSerieID == '')) )
	{
		strSQL +=  'FROM NumerosSerie_Movimento a WITH(NOLOCK) ' +
		            'INNER JOIN Pedidos b WITH(NOLOCK) ON (a.PedidoID = b.PedidoID) ' +
		            'LEFT OUTER JOIN NotasFiscais c WITH(NOLOCK) ON (b.NotaFiscalID = c.NotaFiscalID) ' +
					'INNER JOIN Operacoes d WITH(NOLOCK) ON (b.TransacaoID=d.OperacaoID) ' +
					'INNER JOIN Pessoas e WITH(NOLOCK) ON (b.EmpresaID = e.PessoaID) ' +
					'INNER JOIN Pessoas f WITH(NOLOCK) ON (b.PessoaID = f.PessoaID) ' +
					'WHERE (a.NumeroSerieID = ' + nNumeroSerieID + ' AND c.EstadoID=67) ' +
					'ORDER BY ISNULL(dbo.fn_Pedido_Datas(b.PedidoID, 1), GETDATE()) DESC';
	}
	else if ( !((nPedProdutoID == null) || (nPedProdutoID == '')) )
	{
		strSQL +=  'FROM Pedidos_ProdutosLote a WITH(NOLOCK) ' +
		            'INNER JOIN Pedidos b WITH(NOLOCK) ON (a.PedidoID = b.PedidoID) ' +
		            'LEFT OUTER JOIN NotasFiscais c WITH(NOLOCK) ON (b.NotaFiscalID = c.NotaFiscalID) ' +
					'INNER JOIN Operacoes d WITH(NOLOCK) ON (b.TransacaoID=d.OperacaoID) ' +
					'INNER JOIN Pessoas e WITH(NOLOCK) ON (b.EmpresaID = e.PessoaID) ' +
					'INNER JOIN Pessoas f WITH(NOLOCK) ON (b.PessoaID = f.PessoaID) ' +
					'WHERE (a.PedProdutoID = ' + nPedProdutoID + ' AND c.EstadoID=67) ' +
					'ORDER BY ISNULL(dbo.fn_Pedido_Datas(b.PedidoID, 1), GETDATE()) DESC';
	}
		
	//FIM DE NOVO NS----------------------------------------------
		
	dsoGrid2.SQL = strSQL;

    dsoGrid2.ondatasetcomplete = fillGridData_DSC2;
    dsoGrid2.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC2()
{
	glb_nDOSGrid2--;
	
	if (glb_nDOSGrid2 > 0)
		return;

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg2.Redraw = 0;
    fg2.Editable = false;
    startGridInterface(fg2);
    fg2.FontSize = '8';
    fg2.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

    headerGrid(fg2,['Data',
                  'Empresa',
                  'Pedido',
                  'NF',
                  'Transa��o',
                  'Pessoa'],[]);
    
    fillGridMask(fg2,dsoGrid2,['Data',
							   'Empresa',
							   'PedidoID',
							   'NotaFiscal',
							   'Transacao',
							   'Pessoa'],
                             ['','','','','',''],
                             [dTFormat,'','','','','']);

	alignColsInGrid(fg2,[2,3]);
	
	gridHasTotalLine(fg2, 'Total', 0xC0C0C0, null, true,
		[[getColIndexByColKey(fg2, 'PedidoID'),'#########','C']]);
	

    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0,fg2.Cols-1);
    fg2.Redraw = 2;
    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    
    lockControlsInModalWin(false);

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fg2.Row < 1) && (fg2.Rows > 1) )
        fg2.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fg2.Redraw = 2;
	fg.Enabled = true;
    // se tem linhas no grid, coloca foco no grid
    if (glb_bFocusToFG)
    {
		glb_bFocusToFG = false;
        window.focus();
        fg.focus();
    }
    else if (fg2.Rows > 1)
    {
        window.focus();
        fg2.focus();
    }                

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
    if (!writeNewRowsInDSO(1))
		return null;
	
	glb_dataGridWasChanged = false;		
	lockControlsInModalWin(true);		
    try
    {
        dsoGrid.SubmitChanges();
    }
    catch(e)
    {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887)
        {
            if ( window.top.overflyGen.Alert ('Este registro acaba de ser alterado ou removido.') == 0 )
                return null;
        }
        
        fg.Rows = 1;
        lockControlsInModalWin(false);
        
		glb_FGTimerInt = window.setInterval('fillGridData()', 30, 'JavaScript');
        return null;
    }    

    dsoGrid.ondatasetcomplete = saveDataInGrid_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
	glb_FGTimerInt = window.setInterval('fillGridData()', 30, 'JavaScript');
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid2()
{
    if (!writeNewRowsInDSO(2))
		return null;
	
	lockControlsInModalWin(true);		
    try
    {
        dsoGrid2.SubmitChanges();
    }
    catch(e)
    {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887)
        {
            if ( window.top.overflyGen.Alert ('Este registro acaba de ser alterado ou removido.') == 0 )
                return null;
        }
        
        fg2.Rows = 1;
        lockControlsInModalWin(false);
        
        return null;
    }    

    dsoGrid2.ondatasetcomplete = saveDataInGrid2_DSC;
    dsoGrid2.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid2_DSC()
{
	;
}

function excluirLinha(nTipo)
{
	var grid;
	var dso;
	var oldRow = 0;
	
	if (nTipo == 1)
	{
		grid = fg;
		dso = dsoGrid;
	}
	else if (nTipo == 2)
	{
		grid = fg2;
		dso = dsoGrid2;
	}
	
	if (fg.Row > 0)
	{
        _retMsg = window.top.overflyGen.Confirm("Deseja excluir esta linha?");
	        	
	    if ( (_retMsg == 0) || (_retMsg == 2) )
	        return null;
	
		if (grid.TextMatrix(grid.Row, grid.Cols-1) == '')
		{
			oldRow = grid.Row;
			grid.RemoveItem(grid.Row);
			
			if (grid.Rows > oldRow)
				grid.Row = oldRow;
			else if (grid.Rows > 1)
				grid.Row = grid.Rows - 1;
				
			setupBtnsFromGridState();
			return null;
		}
		
        // Altera o campo do dso com o novo valor
        if (!dso.recordset.EOF)
        {
			dso.recordset.MoveFirst();
			dso.recordset.Find(grid.ColKey(grid.Cols-1) + ' = ' + grid.TextMatrix(grid.Row, grid.Cols-1));
        }

        if (!dso.recordset.EOF)
        {
			grid.RemoveItem(grid.Row);
			dso.recordset.Delete();
				
			if (nTipo == 1)
				saveDataInGrid();
			else if (nTipo == 2)
				saveDataInGrid2();
        }
	}
}

function incluir()
{
	fg.Editable = true;
	fg.Rows = fg.Rows + 1;
	fg.TopRow = fg.Rows - 1;
	fg.Row = fg.Rows - 1;
	glb_dataGridWasChanged = true;
	setupBtnsFromGridState();
}

function incluir2()
{
	fg2.Editable = true;
	fg2.Rows = fg2.Rows + 1;
	fg2.TopRow = fg2.Rows - 1;
	fg2.Row = fg2.Rows - 1;
	setupBtnsFromGridState();
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalpesqnumserieBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalpesqnumserieDblClick(grid, Row, Col)
{
	if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }

	glb_PassedOneInDblClick = true;
    fillGridData2();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalpesqnumserieKeyPress(KeyAscii)
{
	if (KeyAscii == 13)
	{
		glb_bFocusToFG = true;
		fillGridData2();
	}
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalpesqnumserie_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalpesqnumserie_AfterEdit(Row, Col)
{
    var nType = 0;

    if (fg.Editable)
    {
		if (dsoGrid.recordset.Fields.Count > 0)
		{
			nType = dsoGrid.recordset[fg.ColKey(Col)].type;
			// Se decimal , numerico , int, bigint ou boolean
			if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
				fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
		}

		if (fg.TextMatrix(Row, fg.Cols-1) != '')
		{
			// Altera o campo do dso com o novo valor

			dsoGrid.recordset.MoveFirst();
			dsoGrid.recordset.Find(fg.ColKey(fg.Cols-1) + ' = ' + fg.TextMatrix(Row, fg.Cols-1));

			if ( !(dsoGrid.recordset.EOF) )
			{
				nType = dsoGrid.recordset[fg.ColKey(Col)].type;
	            
				// Se decimal , numerico , int, bigint ou boolean
				if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
					dsoGrid.recordset[fg.ColKey(Col)].value = fg.ValueMatrix(Row, Col);
				else    
					dsoGrid.recordset[fg.ColKey(Col)].value = fg.TextMatrix(Row, Col);
	                
				glb_dataGridWasChanged = true;
				setupBtnsFromGridState();
			}
        }
        else
        {
			glb_dataGridWasChanged = true;
			setupBtnsFromGridState();
		}
    }
}

function fg_Rotas_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
{
	fg2.Rows = 1;
}

function fg_Rotas_AfterRowColChange2()
{
	if ((fg2.Rows > 2) && (fg2.Row < 2))
		fg2.Row = 2;
}

function js_modalpesqnumserie_AfterEdit2(Row, Col)
{
    var nType;
	
    if (fg2.Editable)
    {
		if (dsoGrid.recordset.Fields.Count > 0)
		{
			nType = dsoGrid2.recordset[fg2.ColKey(Col)].type;
			// Se decimal , numerico , int, bigint ou boolean
			if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
				fg2.TextMatrix(Row, Col) = treatNumericCell(fg2.TextMatrix(Row, Col));
		}
    
		if (fg2.TextMatrix(Row, fg2.Cols-1) != '')
		{
			// Altera o campo do dso com o novo valor
			dsoGrid2.recordset.MoveFirst();
			dsoGrid2.recordset.Find(fg2.ColKey(fg2.Cols-1) + ' = ' + fg2.TextMatrix(Row, fg2.Cols-1));

			if ( !(dsoGrid2.recordset.EOF) )
			{
				nType = dsoGrid2.recordset[fg2.ColKey(Col)].type;
	            
				// Se decimal , numerico , int, bigint ou boolean
				if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
					dsoGrid2.recordset[fg2.ColKey(Col)].value = fg2.ValueMatrix(Row, Col);
				else    
					dsoGrid2.recordset[fg2.ColKey(Col)].value = fg2.TextMatrix(Row, Col);
				setupBtnsFromGridState();
			}
        }
        else
        {
			setupBtnsFromGridState();
		}
    }
}

// FINAL DE EVENTOS DE GRID *****************************************

function btnLupaClicked()
{
	if (fg.Row < 1)
		return null;

	var nUFID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'UFDestinoID'));

	if (isNaN(nUFID))
		return null;

	if ((nUFID == null)||(nUFID == ''))
		return null;		

	lockControlsInModalWin(true);
    setConnection(dsoCidade);
	dsoCidade.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
		'UNION ALL ' +
		'SELECT LocalidadeID AS fldID, Localidade AS fldName ' +
		'FROM Localidades WITH(NOLOCK) ' +
		'WHERE LocalizacaoID=' + nUFID + ' AND EstadoID=2 ' +
		'ORDER BY fldName';
    dsoCidade.ondatasetcomplete = fillCidade_DSC;
    dsoCidade.Refresh();
}

function fillCidade_DSC()
{
	if ( !((dsoCidade.recordset.BOF) &&(dsoCidade.recordset.EOF)) )
	{
	    clearComboEx(['selCidadeID']);
    
        while (! dsoCidade.recordset.EOF )
        {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoCidade.recordset['fldName'].value;
            oOption.value = dsoCidade.recordset['fldID'].value;
            selCidadeID.add(oOption);
            dsoCidade.recordset.MoveNext();
        }
	}
	lockControlsInModalWin(false);
}

// nTipo 1 : grid 1
// nTipo 2 : grid 2
function writeNewRowsInDSO(nTipo)
{
	var i, j;
	var sColuna = '';
	var nFieldKeyID = 0;
	var nType = 0;
	var aCamposObrigatorios = new Array();
	var sMsgError = '';
	var nLineError = 0;
	var bRetVal = true;
	var nLinkedFieldValueID = 0;
	var sFldKey = '';
	var sLinkedField = '';
	var grid;
	var dso;
	
	if (nTipo == 1)
	{
		sFldKey = 'PesTraRotaID';
		sLinkedField = 'PesTransportadoraID';
		grid = fg;
		dso = dsoGrid;
		nLinkedFieldValueID = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'fg.TextMatrix(fg.Row, fg.Cols-1)');
	}
	else if (nTipo == 2)
	{
		sFldKey = 'PesTraRotPesoID';
		sLinkedField = 'PesTraRotaID';
		grid = fg2;
		dso = dsoGrid2;
		nLinkedFieldValueID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, sLinkedField));
	}
		
	for (i=1; i<grid.Rows; i++)
	{
		if (!(dso.recordset.BOF && dso.recordset.EOF))
		{
			dso.recordset.MoveFirst();
			nFieldKeyID = grid.TextMatrix(i,getColIndexByColKey(grid, sFldKey));
		}

		// Linha Nova
		if (nFieldKeyID == '')
			dso.recordset.AddNew();
		else
			continue;

		dso.recordset[sLinkedField].value = nLinkedFieldValueID;			
		for (j=0; j<grid.Cols; j++)
		{
			sColuna = grid.ColKey(j);

			if ( (sColuna.substr(0,6).toUpperCase() == '_CALC_') ||
				 (sColuna.substr(0,1) == '^') )
				 continue;

			if ((nTipo==1) && (sColuna.toUpperCase() == 'PESTRAROTAID'))
				continue;

			if ((nTipo==2) && (sColuna.toUpperCase() == 'PESTRAROTPESOID'))
				continue;
				
			if (nTipo==1)
			{
				if ((sColuna.toUpperCase() == 'MEIOTRANSPORTEID') &&
					(grid.TextMatrix(i, j) == ''))
				{
					sMsgError = 'Preencha o campo (Meio)';
					nLineError = i;
				}
				else if ((sColuna.toUpperCase() == 'CIDADEORIGEMID') &&
					(grid.TextMatrix(i, j) == ''))
				{
					sMsgError = 'Preencha o campo (Cidade Orig)';
					nLineError = i;
				}
				else if ((sColuna.toUpperCase() == 'UFDESTINOID') &&
						(grid.TextMatrix(i, j) == ''))
				{
					sMsgError = 'Preencha o campo (UF Dest)';
					nLineError = i;
				}
			}
			
			if (sMsgError != '')
				break;

			nType = dso.recordset[grid.ColKey(j)].type;
	        
			// Se decimal , numerico , int, bigint ou boolean
			if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
			{
				if ( (sColuna.substr(sColuna.length - 2).toUpperCase() == 'ID') &&
					 (grid.ValueMatrix(i, j) == 0) )
					dso.recordset[grid.ColKey(j)].value = null;
				else
					dso.recordset[grid.ColKey(j)].value = grid.ValueMatrix(i, j);
			}
			else    
				dso.recordset[grid.ColKey(j)].value = grid.TextMatrix(i, j);
		}
	}
	
	if (sMsgError != '')
	{
		dso.recordset.Delete();
		bRetVal = false;
        if ( window.top.overflyGen.Alert (sMsgError) == 0 )
            return null;
            
		grid.Row = nLineError;
	}
	
	return bRetVal;
}

function txtIdentificador_onkeydown()
{
    if (event.keyCode == 13)
    {
		txtNumeroSerie.focus();
	}
}

function txtNumeroSerie_onkeydown()
{
    if (event.keyCode == 13)
		fillGridData();
}

function txtPesquisa_onkeydown()
{
    if (event.keyCode == 13)
    {
		lockControlsInModalWin(true);

		clearComboEx(['selProduto']);
		// parametrizacao do dso dsoGrid
		setConnection(dsoProduto);
		dsoProduto.SQL = 'SET NOCOUNT ON CREATE TABLE #Produtos (ProdutoID INT) ' +
			'INSERT INTO #Produtos ' +
				'EXEC sp_Identificador_Produtos NULL, ' + '\'' + trimStr(txtPesquisa.value) + '\'' + ', NULL, 2 ' +
			'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +
			'UNION ALL ' +
			'SELECT 1 AS Indice, b.ConceitoID AS fldID, b.Conceito AS fldName ' +
				'FROM #Produtos a, Conceitos b WITH(NOLOCK) ' +
				'WHERE (a.ProdutoID = b.ConceitoID) ' +
				'ORDER BY Indice, fldName ' +
			'DROP TABLE #Produtos SET NOCOUNT OFF';

		dsoProduto.ondatasetcomplete = dsoProduto_DSC;
		dsoProduto.Refresh();
    }
}

function dsoProduto_DSC()
{
    while (! dsoProduto.recordset.EOF )
    {
        optionStr = dsoProduto.recordset['fldName'].value;
		optionValue = dsoProduto.recordset['fldID'].value;

		var oOption = document.createElement("OPTION");
		oOption.text = optionStr;
		oOption.value = optionValue;
		selProduto.add(oOption);

        dsoProduto.recordset.MoveNext();
    }
	
	lockControlsInModalWin(false);
	selProduto.disabled = (selProduto.options.length == 0);
	
	if (selProduto.options.length == 2)
		selProduto.selectedIndex = 1;

	adjustLabelsCombos();
}

function adjustLabelsCombos()
{
    setLabelOfControl(lblProduto, (selProduto.selectedIndex <= 0 ? '' : selProduto.value));
}

function selProduto_onchange()
{
	adjustLabelsCombos();
}

function chkDetalhe_onclick()
{
	comuteInterface();
}

function comuteInterface()
{
	if (chkDetalhe.checked)
	{
		divFG.style.height = 180;
		divButtons2.style.visibility = 'inherit';
		divFG2.style.visibility = 'inherit';
		fg2.style.visibility = 'inherit';
	}
	else
	{
		divFG.style.height = 380;
		divButtons2.style.visibility = 'hidden';
		divFG2.style.visibility = 'hidden';
		fg2.style.visibility = 'hidden';
	}
	
	fg.style.height = divFG.style.height;
}
