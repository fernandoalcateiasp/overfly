/********************************************************************
modalpesquisaXML.js

Library javascript para o modalapuracaoimpostos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
var dsoPesquisaXML = new CDatatransport('dsoPesquisaXML');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
fillGridData()
fillGridData_DSC()
PesquisarXML()
paintCellsSpecialyReadOnly()

js_fg_modalpesquisaXMLDblClick(grid, Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalpesquisaXMLBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a modal
	showExtFrame(window, true);
	window.focus();

	btnPesquisaXML.disabled = false;
	txtChaveAcesso.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Pesquisa Arquivo XML NFe', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    //glb_aEmpresaData = getCurrEmpresaData();
    //glb_nUserID = glb_USERID;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    btnPesquisaXML.disabled = true;
    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';
	
    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblChaveAcesso', 'txtChaveAcesso', 44, 1],
						  ['btnPesquisaXML', 'btn', 52, 1, 5, -2]], 
						  null, null, true);

    btnPesquisaXML.style.height = 25;

    txtChaveAcesso.maxLength = 44;
    txtChaveAcesso.onkeypress = txtChaveAcesso_onKeyPress;

    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
		backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
            btnPesquisaXML.offsetTop + btnPesquisaXML.offsetHeight + 2;
        width = btnPesquisaXML.offsetLeft + btnPesquisaXML.offsetWidth + ELEM_GAP;
        height = txtChaveAcesso.offsetTop + txtChaveAcesso.offsetHeight;
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }

    with (fgXML.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }


    startGridInterface(fgXML);
    fgXML.Cols = 1;
    fgXML.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fgXML.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    if (controlID == 'btnPesquisaXML')
    {
		PesquisarXML();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtChaveAcesso_onKeyPress() {
    //Usuario teclou diferente de Enter(13) e diferente de numerico (48 a 57)
    if (event.keyCode != 13 && (event.keyCode < 48 || event.keyCode > 57)) 
        return false;
    
    if (event.keyCode == 13) //tecla Enter
    {
        fgXML.Rows = 1;

        PesquisarXML();
    }
}

function PesquisarXML() {
    if (!verificaChaveAcesso())
        return null;
    
    fillGridData();
}

function verificaChaveAcesso() {
    if (txtChaveAcesso.value.length != 44) 
    {
        if (window.top.overflyGen.Alert('Chave de Acesso com tamanho inv�lido.') == 0)
            return null;

        return false;
    }

    return true;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    lockControlsInModalWin(true);

    strPars = '?nChaveAcesso=' + escape(txtChaveAcesso.value);

    dsoPesquisaXML.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/pesquisaXML.aspx' + strPars;
    dsoPesquisaXML.ondatasetcomplete = fillGridData_DSC;
    dsoPesquisaXML.refresh();
}


/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {

    if (dsoPesquisaXML.recordset.RecordCount == 0)
        if (window.top.overflyGen.Alert('Chave de Acesso n�o encontrada.') == 0)
            return null;
    
    fgXML.Redraw = 0;
    fgXML.Editable = false;
    startGridInterface(fgXML);
    fgXML.FrozenCols = 0;

    headerGrid(fgXML, ['Chave Acesso',
                       'Emitente',
                       'Nota Fiscal'], 
                       []);
    fillGridMask(fgXML, dsoPesquisaXML,
                    ['ChaveAcesso',
                     'Emitente',
                     'NotaFiscal'],
                    ['', '', ''],
                    ['', '', '']);

    fgXML.AutoSizeMode = 0;

    fgXML.ColWidth(getColIndexByColKey(fgXML, 'ChaveAcesso')) = FONT_WIDTH * 600;
    fgXML.ColWidth(getColIndexByColKey(fgXML, 'Emitente')) = FONT_WIDTH * 400;
    
    fgXML.Redraw = 2;

    lockControlsInModalWin(false);

    fgXML.focus();   
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalapuracaoimpostosDblClick(grid, Row, Col)
{
    ;
}