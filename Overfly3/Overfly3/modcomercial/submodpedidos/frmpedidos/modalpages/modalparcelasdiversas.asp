
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Function ReplaceInText(baseStr, patrn, replStr)
    Dim regEx                         ' Create variables.
    Set regEx = New RegExp            ' Create regular expression.
    regEx.Pattern = patrn             ' Set pattern.
    regEx.IgnoreCase = True           ' Make case insensitive.
    regEx.Global = True               ' Replace globally
    ReplaceInText = regEx.Replace(baseStr, replStr)   ' Make replacement.
End Function
%>

<html id="modalparcelasdiversasHtml" name="modalparcelasdiversasHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalparcelasdiversas.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalparcelasdiversas.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nPedidoID, nValorTotalPedido, nEmpresaID, nPessoaID, nTipoPedidoID, nIdiomaDeID, nIdiomaParaID

sCaller = ""
nPedidoID = 0
nValorTotalPedido = 0
nEmpresaID = 0
nPessoaID = 0
nTipoPedidoID = 0
nIdiomaDeID = 0
nIdiomaParaID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nPedidoID").Count    
    nPedidoID = Request.QueryString("nPedidoID")(i)
Next

For i = 1 To Request.QueryString("nValorTotalPedido").Count    
    nValorTotalPedido = Request.QueryString("nValorTotalPedido")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nPessoaID").Count    
    nPessoaID = Request.QueryString("nPessoaID")(i)
Next

For i = 1 To Request.QueryString("nTipoPedidoID").Count    
    nTipoPedidoID = Request.QueryString("nTipoPedidoID")(i)
Next

For i = 1 To Request.QueryString("nIdiomaDeID").Count    
    nIdiomaDeID = Request.QueryString("nIdiomaDeID")(i)
Next

For i = 1 To Request.QueryString("nIdiomaParaID").Count    
    nIdiomaParaID = Request.QueryString("nIdiomaParaID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nPedidoID = " & CStr(nPedidoID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nValorTotalPedido = " & CStr(nValorTotalPedido) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nIdiomaDeID = " & CStr(nIdiomaDeID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nIdiomaParaID = " & CStr(nIdiomaParaID) & ";"
Response.Write vbcrlf

Dim rsData, rsData2, strSQL
Set rsData = Server.CreateObject("ADODB.Recordset")
Set rsData2 = Server.CreateObject("ADODB.Recordset")
    
Response.Write "var glb_nNivelPagamentoMaximo = 0;"
Response.Write vbcrlf

Response.Write "var glb_nTotalDispesas = 0;"
Response.Write vbcrlf

strSQL = "SELECT ISNULL(dbo.fn_Pedido_Totais(" & CStr(nPedidoID) & ", 32), 0) AS TotalDespesas"

rsData2.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

If (Not (rsData2.BOF AND rsData2.EOF) ) Then

	Response.Write "glb_nTotalDispesas = " & rsData2.Fields("TotalDespesas").Value & ";"
	Response.Write vbcrlf

End If

rsData2.Close

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<body id="modalparcelasdiversasBody" name="modalparcelasdiversasBody" LANGUAGE="javascript" onload="return window_onload()">


    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
    <!-- Div dos campos -->
    <div id="divFields" name="divFields" class="divGeneral">
        <p id="lblHistoricoPadraoID" name="lblHistoricoPadraoID" class="lblGeneral">Hist�rico Padr�o</p>
        <select id="selHistoricoPadraoID" name="selHistoricoPadraoID" class="fldGeneral">
<%
			strSQL = "SELECT DISTINCT a.HistoricoPadraoID AS fldID, dbo.fn_Tradutor(a.HistoricoPadrao, " & nIdiomaDeID & ", " & nIdiomaParaID & ", NULL) AS fldName " & _
					 "FROM HistoricosPadrao a WITH(NOLOCK), HistoricosPadrao_Contas b WITH(NOLOCK) " & _
                     "WHERE a.EstadoID = 2 AND a.HistoricoPadraoID = b.HistoricoPadraoID AND b.Pedido = 1 AND  a.HistoricoPadraoID NOT IN (4222) "
                     
                     
            If (CStr(nTipoPedidoID) = 601) Then
				strSQL = strSQL & " AND a.TipoLancamentoID=1572 "
            Else
				strSQL = strSQL & " AND a.TipoLancamentoID=1571 "
            End If
            
            strSQL = strSQL & "ORDER BY fldName"

			rsData2.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

			If (Not (rsData2.BOF AND rsData2.EOF) ) Then

				While Not (rsData2.EOF)

					Response.Write( "<option value='" & rsData2.Fields("fldID").Value & "'")
					Response.Write( ">" & rsData2.Fields("fldName").Value & "</option>" )
					rsData2.MoveNext()

				WEnd

			End If

			rsData2.Close
%>        
        </select>
        
        <p id="lblTipoComissao" name="lblTipoComissao" class="lblGeneral">Tipo Comiss�o</p>
        <select id="selTipoComissao" name="selTipoComissao" class="fldGeneral"></select>

		<p id="lblPesquisa" name="lblPesquisa" class="lblGeneral">Pesquisar</p>
        <input type="text" id="txtPesquisa" name="txtPesquisa" class="fldGeneral">

		<input type="button" id="btnFindPesquisa" name="btnFindPesquisa" value="OK" LANGUAGE="javascript" onclick="return btnFindPesquisa_onclick(this)" class="btns">

		<p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">Pessoa</p>
        <select id="selPessoaID" name="selPessoaID" class="fldGeneral"></select>

        <p id="lblFormaPagamentoID" name="lblFormaPagamentoID" class="lblGeneral">Forma</p>
        <select id="selFormaPagamentoID" name="selFormaPagamentoID" class="fldGeneral">
<%
			strSQL = "SELECT ItemAbreviado AS fldName, ItemID AS fldID FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                     "WHERE (EstadoID = 2 AND TipoID = 804) " & _
                     "ORDER BY Ordem"

			rsData2.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
			
			If (Not (rsData2.BOF AND rsData2.EOF) ) Then

				While Not (rsData2.EOF)

					Response.Write("<option value='" & rsData2.Fields("fldID").Value & "'>")
					Response.Write(rsData2.Fields("fldName").Value & "</option>")
					rsData2.MoveNext()

				WEnd

			End If

			rsData2.Close
			Set rsData2 = Nothing
%>        
        </select>

		<p id="lblPrazo" name="lblPrazo" class="lblGeneral">Prazo</p>
        <input type="text" id="txtPrazo" name="txtPrazo" class="fldGeneral">
        
		<p id="lblPercentual" name="lblPercentual" class="lblGeneral">Percentual</p>
        <input type="text" id="txtPercentual" name="txtPercentual" class="fldGeneral">

		<p id="lblValor" name="lblValor" class="lblGeneral">Valor</p>
        <input type="text" id="txtValor" name="txtValor" class="fldGeneral">
        
        <p id="lblCartao" name="lblCartao" class="lblGeneral">Cart�o</p>
        <select id="selCartao" name="selCartao" class="fldGeneral"></select>

    </div>
    
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
</body>

</html>
