<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalcadastroHtml" name="modalcadastroHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalcadastro.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nPedidoID, nParceiroID, nEmpresaID, nEhCliente, nCurrEstadoID, nNewEstadoID

sCaller = ""
nPedidoID = 0
nParceiroID = 0
nEmpresaID = 0
nEhCliente = 1
nCurrEstadoID = 0
nNewEstadoID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nPedidoID").Count    
    nPedidoID = Request.QueryString("nPedidoID")(i)
Next

For i = 1 To Request.QueryString("nParceiroID").Count    
    nParceiroID = Request.QueryString("nParceiroID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nEhCliente").Count    
    nEhCliente = Request.QueryString("nEhCliente")(i)
Next

For i = 1 To Request.QueryString("nCurrEstadoID").Count    
    nCurrEstadoID = Request.QueryString("nCurrEstadoID")(i)
Next

For i = 1 To Request.QueryString("nNewEstadoID").Count    
    nNewEstadoID = Request.QueryString("nNewEstadoID")(i)
Next

If ( CStr(nEmpresaID) = ""  ) Then
    nEmpresaID = 0
End If

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nPedidoID = " & nPedidoID & ";"
Response.Write vbcrlf
Response.Write "var glb_nParceiroID = " & nParceiroID & ";"
Response.Write vbcrlf
Response.Write "var glb_nEmpresaID = " & nEmpresaID & ";"
Response.Write vbcrlf
Response.Write "var glb_nEhCliente = " & nEhCliente & ";"
Response.Write vbcrlf
Response.Write "var glb_nCurrEstadoID = " & CStr(nCurrEstadoID) & ";"
Response.Write vbcrlf
Response.Write "var glb_nNewEstadoID = " & CStr(nNewEstadoID) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
js_cadastro_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgRefCom EVENT=AfterEdit>
<!--
 js_BeforeRowColChangeModalCadastro(fgRefCom, arguments[0]);
//-->
</SCRIPT>

</head>

<body id="modalcadastroBody" name="modalcadastroBody" LANGUAGE="javascript" onload="return window_onload()">
      <div id="divMemo" name="divMemo" class="divGeneral">
    </div>
   
    <div id="divFieldsCadastro" name="divFieldsCadastro" class="divGeneral">
        <p id="lblFundacao" name="lblFundacao" class="lblGeneral">Funda��o</p>
        <input type="text" id="txtFundacao" name="txtFundacao" class="fldGeneral">
        <p id="lblAnos" name="lblAnos" class="lblGeneral">Anos</p>
        <input type="text" id="txtAnos" name="txtAnos" class="fldGeneral">
        <p id="lblCNAE" name="lblCNAE" class="lblGeneral">CNAE</p>
        <input type="text" id="txtCNAE" name="txtCNAE" title="Classifica��o Nacional de Atividade Econ�mica" class="fldGeneral">
        <p id="lblClassificacao" name="lblClassificacao" class="lblGeneral">Classifica��o</p>
        <input type="text" id="txtClassificacao" name="txtClassificacao" class="fldGeneral">
        <p id="lblClassificacaoABC" name="lblClassificacaoABC" class="lblGeneral">Class ABC</p>
        <input type="text" id="txtClassificacaoABC" name="txtClassificacaoABC" class="fldGeneral">
        <p id="lblConceito" name="lblConceito" class="lblGeneral">Conceito</p>
        <input type="text" id="txtConceito" name="txtConceito" class="fldGeneral">
        <p id="lbldtValidadeCredito" name="lbldtValidadeCredito" class="lblGeneral">Validade</p>
        <input type="text" id="txtdtValidadeCredito" name="txtdtValidadeCredito" class="fldGeneral" title="Data de Validade do Cr�dito">
        <p id="lblBloqueado" name="lblBloqueado" class="lblGeneral">Bloq</p>
        <input type="checkbox" id="chkBloqueado" name="chkBloqueado" class="fldGeneral" title="Cr�dito est� bloqueado?">
        <p id="lblCapitalSocial" name="lblCapitalSocial" class="lblGeneral">Capital Social</p>
        <input type="text" id="txtCapitalSocial" name="txtCapitalSocial" class="fldGeneral">
        <p id="lblPatrimonioLiquido" name="lblPatrimonioLiquido" class="lblGeneral">Patrim�nio L�quido</p>
        <input type="text" id="txtPatrimonioLiquido" name="txtPatrimonioLiquido" class="fldGeneral">
        <p id="lblFatAnual" name="lblFatAnual" class="lblGeneral">Faturamento Anual</p>
        <input type="text" id="txtFatAnual" name="txtFatAnual" class="fldGeneral">
        <p id="lblLimiteCreditoEmpresa" name="lblLimiteCreditoEmpresa" class="lblGeneral" title="Limite de cr�dito na empresa">Cr�dito Empresa</p>
        <input type="text" id="txtLimiteCreditoEmpresa" name="txtLimiteCreditoEmpresa" class="fldGeneral" title="Limite de cr�dito na empresa">
        <p id="lblSeguroCredito" name="lblSeguroCredito" class="lblGeneral">Seguro Cr�dito</p>
        <input type="text" id="txtSeguroCredito" name="txtSeguroCredito" class="fldGeneral">
        <p id="lblLimite" name="lblLimite" class="lblGeneral" title="Limite de cr�dito geral">Cr�dito total</p>        
        <input type="text" id="txtLimite" name="txtLimite" class="fldGeneral" title="Limite de cr�dito geral">
        <p id="lblSaldoDevedor" name="lblSaldoDevedor" class="lblGeneral">Saldo Devedor</p>
        <input type="text" id="txtSaldoDevedor" name="txtSaldoDevedor" class="fldGeneral">
        <p id="lblSaldoCredito" name="lblSaldoCredito" class="lblGeneral" title="Saldo de cr�dito geral">Saldo Cr�dito</p>
        <input type="text" id="txtSaldoCredito" name="txtSaldoCredito" class="fldGeneral" title="Saldo de cr�dito geral"/>
        <p id="lblSaldoCreditoGrupo" name="lblSaldoCreditoGrupo" class="lblGeneral" title="Saldo de cr�dito geral do grupo (considera apenas o cr�dito pr�prio)">Saldo Cr�dito Grupo</p>
        <input type="text" id="txtSaldoCreditoGrupo" name="txtSaldoCreditoGrupo" class="fldGeneral" title="Saldo de cr�dito geral do grupo (considera apenas o cr�dito pr�prio)"/>
    </div>
    
    <div id="divFGRefComerciais" name="divFGRefComerciais" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgRefCom" name="fgRefCom" VIEWASTEXT>
        </object>
    </div>
    
     <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
    </div>    
    
    <input type="button" id="btnModo" name="btnModo" value="Modo" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
