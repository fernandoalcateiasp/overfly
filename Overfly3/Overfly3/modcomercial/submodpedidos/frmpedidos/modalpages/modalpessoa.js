/********************************************************************
modalpessoa.js

Library javascript para o modalpessoa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoPesq = new CDatatransport('dsoPesq');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() 
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modalpessoaBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtPessoa').disabled == false )
        txtPessoa.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Pessoa', 1);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    
    // ajusta o divPessoa
    elem = window.document.getElementById('divPessoa');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;
        
    }

    useBtnListar(true, divPessoa);

    if (glb_sCaller == 'SUP')
    {
        adjustElementsInForm([['lblEhCliente', 'selEhCliente', 12, 1, -10, -10],
                          ['lblParceiro', 'chkParceiro', 3, 1],
                          ['lblMeusClientes', 'chkMeusClientes', 3, 1, -1],
                          ['lblChavePesquisa', 'selChavePesquisa', 12, 1, -10],
                          ['lblPessoa', 'txtPessoa', 34, 1]],
                          null,null,true);
    }
    else if (glb_sCaller == 'INF') {
        selEhCliente.style.visibility = 'hidden';
        lblEhCliente.style.visibility = 'hidden';
        lblMeusClientes.style.visibility = 'hidden';
        chkMeusClientes.style.visibility = 'hidden';
        lblParceiro.style.visibility = 'hidden';
        chkParceiro.style.visibility = 'hidden';
       

        adjustElementsInForm([['lblChavePesquisa', 'selChavePesquisa', 12, 1, -10],
                              ['lblPessoa', 'txtPessoa', 34, 1]],
                               null,null,true);

    }
    with (btnListar)
    {
        style.top = parseInt(txtPessoa.currentStyle.top, 10);
        style.left = parseInt(txtPessoa.currentStyle.left, 10) +
                     parseInt(txtPessoa.currentStyle.width, 10) + ELEM_GAP;
    }                          

    
        selEhCliente.onchange = selEhCliente_onChange;


        var aOptions = [[1, 'Cliente'],
                        [0, 'Fornecedor']];
        var i;

        for (i = 0; i <= 1; i++) {
            var oOption = document.createElement("OPTION");
            oOption.value = aOptions[i][0];
            oOption.text = aOptions[i][1];
            selEhCliente.add(oOption);
        }

       var aOptions2 = [[0, 'ID'],
                        [1, 'Nome'],
                        [2, 'Fantasia'],
                        [3, 'Documento']];

       for (i = 0; i <= 3; i++) {
       var oOption = document.createElement("OPTION");
           oOption.value = aOptions2[i][0];
           oOption.text = aOptions2[i][1];
           selChavePesquisa.add(oOption);

        }

        selChavePesquisa.selectedIndex = 1;

    // Contexto de Entrada (Fornecedor)
    if ( glb_nContextoID == 5111 )
    {
        selEhCliente.selectedIndex = 1;
        chkParceiro.style.visibility = 'hidden';
        lblParceiro.style.visibility = 'hidden';
        chkMeusClientes.style.visibility = 'hidden';
        lblMeusClientes.style.visibility = 'hidden';
    }
    // Contexto de Saida (Cliente)
    else if (glb_sCaller != 'INF')
    {
        selEhCliente.selectedIndex = 0;
        chkParceiro.style.visibility = 'inherit';
        lblParceiro.style.visibility = 'inherit';
        chkMeusClientes.style.visibility = 'inherit';
        lblMeusClientes.style.visibility = 'inherit';

    }
        
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divPessoa').style.top) + parseInt(document.getElementById('divPessoa').style.height) + ELEM_GAP;
        // width = temp + 221;
        width = temp + 321;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);

    headerGrid(fg,['Nome',
                   'ID',
                   'Fantasia',
                   'Documento',
                   'Cidade',
                   'UF',
                   'Pa�s',
                   'Parceiro',
                   'TransacaoID',
                   'Transacao',
                   'Incidencia',
                   'Estado Pessoa',
                   'estadoPessoaAtivo',
                   'ProdutoDesativo'], [2, 7, 8, 9, 10, 12, 13]);
    
    fg.Redraw = 2;
    
    txtPessoa.onkeypress = txtPessoa_onKeyPress;
    
    lblParceiro.onclick = invertChkBox;
    lblMeusClientes.onclick = invertChkBox;
}

function txtPessoa_onKeyPress()
{
    if ( event.keyCode == 13 )
        btnListar_onclick();
}

function txtPessoa_ondigit(ctl)
{
    changeBtnGif(ctl.value);
}

function changeBtnGif (strTest)
{
    if (strTest.length != 0)
    {
        btnListar.disabled = false;
    }
    else
    {
        btnListar.disabled = true;
    }
}

function btnListar_onclick()
{
    txtPessoa.value = trimStr(txtPessoa.value);
    
    changeBtnGif (txtPessoa.value);

    if (btnListar.disabled)
        return;
    
    startPesq(txtPessoa.value);
}

function fg_DblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if ((ctl.id == btnOK.id)  && (glb_sCaller == 'SUP'))
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S' , new Array(
                      fg.TextMatrix(fg.Row, 2), fg.TextMatrix(fg.Row, 1), selEhCliente.value, 
                      fg.TextMatrix(fg.Row, 7),
                      fg.TextMatrix(fg.Row, 8), fg.TextMatrix(fg.Row, 9), fg.TextMatrix(fg.Row, 10),
                      fg.TextMatrix(fg.Row, 12), fg.TextMatrix(fg.Row, 13),
                      fg.TextMatrix(fg.Row, 14)));

    }
    else if ((ctl.id == btnOK.id) && (glb_sCaller == 'INF')) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', new Array(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'fldID')), fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Fantasia'))));

    }

    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null );    

}

function startPesq(strPesquisa)
{
    lockControlsInModalWin(true);
    
    if (glb_sCaller == 'SUP') {
        //Altera��o: Remo��o da obrigatoriedade da pessoa e a rela��o da pessoa estar ativa, utilizado para devolu��o e rma LFS:05/05/2011
        var nEhCliente = selEhCliente.value;
        var nEntrada = 0;
        var sPesquisa = ' AND Pessoas.Nome';

        if (selChavePesquisa.value == 0) {
            sPesquisa = 'AND Pessoas.PessoaID';
        }
        else if (selChavePesquisa.value == 2) {
            sChavePesquisa = 'Pessoas.Fantasia';
            sPesquisa = ' AND Pessoas.Fantasia';
        }
        else if (selChavePesquisa.value == 3) {
            sPesquisa = 'AND Documento.Numero';
        }
        if (glb_nContextoID == 5111)
            nEntrada = 1;

        var strPars = '?nEmpresaID=' + escape(glb_nEmpresaID) +
                        '&nEhCliente=' + escape(nEhCliente) +
                        '&nCliente=' + escape(chkMeusClientes.checked) +
                        '&nParceiro=' + escape(chkParceiro.checked) +
                        '&nEntrada=' + escape(nEntrada) +
                        '&sPesquisa=' + escape(sPesquisa) +
                        '&sArgumento=' + escape(strPesquisa) +
                        '&nUsuarioID=' + escape(glb_USERID);

        setConnection(dsoPesq);
        dsoPesq.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/listarpessoa.aspx' + strPars;
        dsoPesq.ondatasetcomplete = dsopesq_DSC;
        dsoPesq.Refresh();
    }
    else if (glb_sCaller == 'INF') {

        var sPesquisa = ' AND (a.Nome';
        sChavePesquisa = 'a.Nome';

        if (selChavePesquisa.value == 0) {
            sPesquisa = 'AND (a.PessoaID';
        }
        else if (selChavePesquisa.value == 2) {
            sChavePesquisa = ' a.Fantasia';
            sPesquisa = ' AND (a.Fantasia';
        }
        else if (selChavePesquisa.value == 3) {
            sPesquisa = 'AND (c.Numero';
        }

        strSQL = " SELECT DISTINCT TOP 200 " + sChavePesquisa + " AS fldName, a.PessoaID AS fldID, a.Fantasia AS Fantasia, ISNULL(c.Numero, '') AS Numero, e.Localidade AS Cidade, " +
                 " f.Localidade AS UF, g.Localidade AS Pais, '' AS Parceiro, '' AS TransacaoID, '' AS Transacao, '' AS Incidencia, " +
                 " h.Recurso AS estadoPessoa, '' as estadoPessoaAtivo, '' AS ProdutoDesativo, d.PaisID AS PaisID " +
                 "FROM Pessoas a WITH(NOLOCK) " +
                     "INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON b.ItemID = a.ClassificacaoID " +
                     "LEFT JOIN Pessoas_Documentos c WITH(NOLOCK) ON c.PessoaID = a.PessoaID AND c.TipoDocumentoID = 111 " +
                     "INNER JOIN Pessoas_Enderecos d WITH(NOLOCK) ON d.PessoaID = a.PessoaID " +
                     "INNER JOIN Localidades e WITH(NOLOCK) ON e.LocalidadeID = d.CidadeID " +
                     "INNER JOIN Localidades f WITH(NOLOCK) ON f.LocalidadeID = d.UFID " +
                     "INNER JOIN Localidades g WITH(NOLOCK) ON g.LocalidadeID = d.PaisID " +
                     "LEFT JOIN Recursos h WITH(NOLOCK) ON h.RecursoID = a.EstadoID " +
                 "WHERE ((a.EstadoID = 2) AND (a.TipoPessoaID = 52) AND ((b.Filtro LIKE '%<CLI>%') OR (a.ClassificacaoID = 65))) ";

        if (strPesquisa != '')
            strSQL +=  sPesquisa +"  LIKE '%" + strPesquisa + "%')";

        strSQL += " ORDER BY " + sChavePesquisa;

        setConnection(dsoPesq);
        dsoPesq.SQL = strSQL;
        dsoPesq.ondatasetcomplete = dsopesq_DSC;
        dsoPesq.Refresh();

    }

}

function dsopesq_DSC()
{
	var sChavePesquisa = 'Nome';
	if (selChavePesquisa.value == 2)
		sChavePesquisa = 'Fantasia';

    startGridInterface(fg);
    fg.FrozenCols = 0;

    headerGrid(fg,[sChavePesquisa,
                   'ID',
                   'Fantasia',
                   'Documento',
                   'Cidade',
                   'UF',
                   'Pa�s',
                   'Parceiro',
                   'TransacaoID',
                   'Transacao',
                   'Incidencia',
                   'Estado Pessoa',
                   'estadoPessoaAtivo',
                   'ProdutoDesativo',
                   'PaisID'], [2, 7, 8, 9, 10, 12, 13, 14]);

    fillGridMask(fg,dsoPesq,['fldName',
                             'fldID',
                             'Fantasia',
                             'Numero',
                             'Cidade',
                             'UF',
                             'Pais',
                             'Parceiro',
 							 'TransacaoID',
							 'Transacao',
                             'Incidencia',
                             'estadoPessoa',
                             'estadoPessoaAtivo',
                             'ProdutoDesativo',
                             'PaisID'], ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);
    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    fg.Redraw = 0;
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    fg.Redraw = 2;
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
    }    
    else
    {
        btnOK.disabled = true;    
        txtPessoa.focus();
    }    
        
    // writeInStatusBar('child', 'cellMode', 'XXXXXXXX');    
    
}

function fg_KeyPress(KeyAscii)
{
    // Enter
    if ( (KeyAscii == 13) && (fg.Row > 0) )
    {
        btn_onclick(btnOK);
    }
}

// Funcao disparada no on change do combo selEhCliente
function selEhCliente_onChange ()
{
    if (this.value == 0)
    {
        chkParceiro.style.visibility = 'hidden';
        lblParceiro.style.visibility = 'hidden';
        chkMeusClientes.style.visibility = 'hidden';
        lblMeusClientes.style.visibility = 'hidden';
    }
    else        
    {
        chkParceiro.style.visibility = 'inherit';
        lblParceiro.style.visibility = 'inherit';
        chkMeusClientes.style.visibility = 'inherit';
        lblMeusClientes.style.visibility = 'inherit';
    }
    fg.Rows = 1;
    btnOK.disabled = true;
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    return true;
}