/********************************************************************
modalprintvendas_pedidos.js

Library javascript para o modalprint.asp
Pedidos 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_Empresa = getCurrEmpresaData();
var glb_nEmpresaID = glb_Empresa[0];

var glb_HTTPRequest = null;
var glb_aTotal =
[
    [0, 'Ano', 1, []],
    [1, 'Bairro', 1, [9, 16, 19]],
    [2, 'CFOP', 1, [21]],
    [3, 'Cidade', 1, [9, 16, 19]],
    [4, 'Classifica��o', 1, []],
    [5, 'Cliente', 1, [1, 3, 4, 9, 11, 16, 19]],
    [6, 'Pessoa', 1, [1, 3, 4, 9, 11, 16, 19]],
    [7, 'Dia', 1, [0, 14, 18, 20]],
    [8, 'Equipe', 1, []],
    [9, 'Estado', 1, [16, 19]],
    [10, 'Fam�lia', 1, []],
    [11, 'Lista', 1, []],
    [12, 'Marca', 1, []],
    [13, 'Ger Produto', 1, []],
    [14, 'Linhas de Produto', 1, []],
    [15, 'M�s', 1, [0, 18]],
    [16, 'Pa�s', 1, []],
    [17, 'Produto', 1, [9, 12]],
    [18, 'Quarter', 1, [0]],
    [19, 'Regi�o', 1, [16]],
    [20, 'Semana', 1, [0, 13, 18]],
    [21, 'Transa��o', 1, []],
    [22, 'Vendedor', 1, []],
    [23, 'Origem', 1, []],
    [24, 'Prg Marketing', 1, []]
];

var glb_aOrdem =
[
    [0, 'Alfabetica'],
//[1, 'Contribui��o'],
    [1, 'Contr Contabil'],
//[2,'Contr Ajustada'],
    [2, 'Contr Gerencial'],
    [3, 'Faturamento'],
    [4, 'Preco Base Cont'],
    [5, 'Preco Base'],
    [6, 'Financeiro'],
    [7, 'Margem Cont'],
    [8, 'Margem Ger'],
    [9, 'Pedido'],
    [10, 'Quantidade']
];

var glb_aKey =
[
    ['', 'DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)),', 1, 'Ano'],  /* ANO */
    ['', 'Enderecos.Bairro + SPACE(1) + Cidades.Localidade,', 1, 'Bairro'],  /*  BAIRRO */
    ['', 'CFOPs.OperacaoID,', 0],  /* CFOP */
    ['', 'Enderecos.CidadeID,', 0],  /* CIDADE */
    ['', 'Parceiros.ClassificacaoID,', 0],  /* CLASSIFICACAO */
    ['', 'Parceiros.PessoaID,', 0],  /* CLIENTE */
    ['', 'Pessoas.PessoaID,', 0],  /* PESSOA */
    ['', 'dbo.fn_Data_Zero(Itens.dtMovimento),', 1, 'Dia'],  /* DIA */
    ['', 'Pedidos.AlternativoID,', 0],  /* EQUIPE */
    ['', 'Enderecos.UFID,', 0],  /* ESTADO */
    ['', 'Familias.ConceitoID,', 2, 'Familia'],  /* FAMILIA */
    ['', 'RelPessoas.ListaPreco,', 0],  /* LISTA */
    ['', 'Marcas.ConceitoID,', 2, 'Marca'],  /* MARCA */
    ['', 'GPs.PessoaID,', 0],  /* GER PRODUTOS */
    ['', 'LinhasProduto.ConceitoID,', 0],  /* LINHAS DE PRODUTO */
    ['', 'LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + ' + '\'' + '/' + '\'' + ' + dbo.fn_Pad(LTRIM(STR(DATEPART(mm, dbo.fn_Data_Zero(Itens.dtMovimento)))),2,CHAR(48),' + '\'' + 'L' + '\'' + '),', 1, 'Mes'],  /* MES */
    ['', 'Enderecos.PaisID,', 0],  /* PAIS */
    ['', 'Produtos.ConceitoID,', 2, 'Produto'],  /* PRODUTO */
    ['', 'LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + ' + '\'' + '/' + '\'' + ' + LTRIM(STR(DATEPART(qq, dbo.fn_Data_Zero(Itens.dtMovimento)))),', 1, 'Quarter'],  /* QUARTER */
    ['', 'Estados.TipoRegiaoID,', 0],  /* REGIAO */
    ['', 'LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + ' + '\'' + '/' + '\'' + ' + LTRIM(STR(DATEPART(wk, dbo.fn_Data_Zero(Itens.dtMovimento)))),', 1, 'Semana'],  /* SEMANA */
    ['', 'Transacoes.OperacaoID,', 0],  /* TRANSACAO */
    ['', 'Pedidos.ProprietarioID,', 0],  /* VENDEDOR */
    ['', 'Pedidos.OrigemPedidoID,', 0],  /* ORIGEM */
    ['', 'Pedidos.ProgramaMarketingID,', 0]  /* PRG MARKETING */
];

// var glb_aFields =
// [
// ['NULL AS ID, DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)) AS Nome,'],  /* ANO */
// ['NULL AS ID, Enderecos.Bairro + SPACE(1) + Cidades.Localidade AS Nome,'],  /* BAIRRO */
// ['CFOPs.OperacaoID AS ID, LEFT(CFOPs.Operacao,33) AS Nome,'],  /* CFOP */
// ['Enderecos.CidadeID AS ID, Cidades.Localidade + SPACE(1) + Estados.CodigoLocalidade2 AS Nome,'],  /* CIDADE */
// ['Parceiros.ClassificacaoID AS ID, Classificacoes.ItemMasculino AS Nome,'],  /* CLASSIFICACAO */
// ['Parceiros.PessoaID AS ID, Parceiros.Fantasia AS Nome,'],  /* CLIENTE */
// ['Pessoas.PessoaID AS ID, Pessoas.Fantasia AS Nome,'],  /* PESSOA */
// ['NULL AS ID, dbo.fn_Data_Zero(Itens.dtMovimento) AS Nome,'],  /* DIA */
// ['Pedidos.AlternativoID AS ID, Equipes.Fantasia AS Nome,'],  /* EQUIPE */
// ['Enderecos.UFID AS ID, Estados.Localidade + SPACE(1) + Paises.CodigoLocalidade2 AS Nome,'],  /* ESTADO */
// ['Familias.ConceitoID AS ID, Familias.Conceito AS Nome,'],  /* FAMILIA */
// ['RelPessoas.ListaPreco AS ID, RelPessoas.ListaPreco AS Nome,'],  /* LISTA */
// ['Marcas.ConceitoID AS ID, Marcas.Conceito AS Nome,'],  /* MARCA */
// ['GPs.PessoaID AS ID, GPs.Fantasia AS Nome,'],  /* GER PRODUTOS */
// ['LinhasProduto.ConceitoID AS ID, LinhasProduto.Conceito AS Nome,'],  /* LINHAS DE PRODUTO */
// ['NULL AS ID, LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + ' + '\'' + '/' + '\'' + ' + dbo.fn_Pad(LTRIM(STR(DATEPART(mm, dbo.fn_Data_Zero(Itens.dtMovimento)))),2,CHAR(48),' + '\'' + 'L' + '\''+ ') AS Nome,'],  /* MES */
// ['Enderecos.PaisID AS ID, Paises.Localidade AS Nome,'],  /* PAIS */
// ['Produtos.ConceitoID AS ID, Produtos.Conceito AS Nome,'],  /* PRODUTO */
// ['NULL AS ID, LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + ' + '\'' + '/' + '\'' + ' + LTRIM(STR(DATEPART(qq, dbo.fn_Data_Zero(Itens.dtMovimento)))) AS Nome,'],  /* QUARTER */ 
// ['Estados.TipoRegiaoID AS ID, Regioes.ItemMasculino + SPACE(1) + Paises.CodigoLocalidade2 AS Nome,'],  /* REGIAO */
// ['NULL AS ID, LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + ' + '\'' + '/' + '\'' + ' + LTRIM(STR(DATEPART(wk, dbo.fn_Data_Zero(Itens.dtMovimento)))) AS Nome,'],  /* SEMANA */
// ['Transacoes.OperacaoID AS ID, Transacoes.Operacao AS Nome,'],  /* TRANSACAO */
// ['Pedidos.ProprietarioID AS ID, Vendedores.Fantasia AS Nome,']  /* VENDEDOR */
// ];

// var glb_aGroupBy =
// [
// ['DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)) '],  /* ANO */
// ['Enderecos.Bairro + SPACE(1) + Cidades.Localidade '],  /* BAIRRO */
// ['CFOPs.OperacaoID, CFOPs.Operacao '],  /* CFOP */
// ['Enderecos.CidadeID, Cidades.Localidade, Estados.CodigoLocalidade2 '],  /* CIDADE */
// ['Parceiros.ClassificacaoID, Classificacoes.ItemMasculino '],  /* CLASSIFICACAO */
// ['Parceiros.PessoaID, Parceiros.Fantasia '],  /* CLIENTE */
// ['Pessoas.PessoaID, Pessoas.Fantasia '],  /* PESSOA */
// ['dbo.fn_Data_Zero(Itens.dtMovimento) '],  /* DIA */
// ['Pedidos.AlternativoID, Equipes.Fantasia '],  /* EQUIPE */
// ['Enderecos.UFID, Estados.Localidade, Paises.CodigoLocalidade2 '],  /* ESTADO */
// ['Familias.ConceitoID, Familias.Conceito '],  /* FAMILIA */
// ['RelPessoas.ListaPreco, RelPessoas.ListaPreco '],  /* LISTA */
// ['Marcas.ConceitoID, Marcas.Conceito '],  /* MARCA */
// ['GPs.PessoaID, GPs.Fantasia '],  /* GER PRODUTOS */
// ['LinhasProduto.ConceitoID, LinhasProduto.Conceito '],  /* LINHAS DE PRODUTO */
// ['LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + ' + '\'' + '/' + '\'' + ' + dbo.fn_Pad(LTRIM(STR(DATEPART(mm, dbo.fn_Data_Zero(Itens.dtMovimento)))),2,CHAR(48),' + '\'' + 'L' + '\''+ ') '],  /* MES */
// ['Enderecos.PaisID, Paises.Localidade '],  /* PAIS */
// ['Produtos.ConceitoID, Produtos.Conceito '],  /* PRODUTO */
// ['LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + ' + '\'' + '/' + '\'' + ' + LTRIM(STR(DATEPART(qq, dbo.fn_Data_Zero(Itens.dtMovimento)))) '],  /* QUARTER */
// ['Estados.TipoRegiaoID, Regioes.ItemMasculino, Paises.CodigoLocalidade2 '],  /* REGIAO */
// ['LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + ' + '\'' + '/' + '\'' + ' + LTRIM(STR(DATEPART(wk, dbo.fn_Data_Zero(Itens.dtMovimento)))) '],  /* SEMANA */
// ['Transacoes.OperacaoID, Transacoes.Operacao '],  /* TRANSACAO */
// ['Pedidos.ProprietarioID, Vendedores.Fantasia ']  /* VENDEDOR */
// ];

// var glb_aOrderBy = 
// [
// ['DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento))', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* ANO */
// ['Enderecos.Bairro + SPACE(1) + Cidades.Localidade', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* BAIRRO */
// ['CFOPs.Operacao', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* CFOP */
// ['Cidades.Localidade, Estados.CodigoLocalidade2', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* CIDADE */
// ['Classificacoes.ItemMasculino', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* CLASSIFICACAO */
// ['Parceiros.Fantasia', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* CLIENTE */
// ['Pessoas.Fantasia', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* PESSOA */
// ['dbo.fn_Data_Zero(Itens.dtMovimento)', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* DIA */
// ['Equipes.Fantasia', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* EQUIPE */
// ['Estados.Localidade, Paises.CodigoLocalidade2', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* ESTADO */
// ['Familias.Conceito', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* FAMILIA */
// ['RelPessoas.ListaPreco', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* LISTA */
// ['Marcas.Conceito', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* MARCA */
// ['GPs.Fantasia', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* GER PRODUTOS */
// ['LinhasProduto.Conceito', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* LINHAS DE PRODUTO */
// ['LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + ' + '\'' + '/' + '\'' + ' + dbo.fn_Pad(LTRIM(STR(DATEPART(mm, dbo.fn_Data_Zero(Itens.dtMovimento)))),2,CHAR(48),' + '\'' + 'L' + '\'' + ')', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* MES */
// ['Paises.Localidade', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* PAIS */
// ['Produtos.Conceito', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* PRODUTO */
// ['LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + ' + '\'' + '/' + '\'' + ' + LTRIM(STR(DATEPART(qq, dbo.fn_Data_Zero(Itens.dtMovimento))))', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* QUARTER */
// ['Regioes.ItemMasculino', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* REGIAO */
// ['LTRIM(STR(DATEPART(yyyy, dbo.fn_Data_Zero(Itens.dtMovimento)))) + ' + '\'' + '/' + '\'' + ' + LTRIM(STR(DATEPART(wk, dbo.fn_Data_Zero(Itens.dtMovimento))))', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* SEMANA */
// ['Transacoes.Operacao', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'], /* TRANSACAO */
// ['Vendedores.Fantasia', 'Contr_Cont DESC', 'Contr_Ger DESC', 'Faturamento DESC', 'Preco_Base_Cont DESC', 'Preco_Base DESC', 'Financeiro DESC', 'MC_Cont DESC', 'MC DESC', 'Pedidos DESC', 'Quantidade DESC'] /* VENDEDOR */
// ];

var glb_aDetailIndex = ['Nome', 'Contr_Cont', 'Contr_Ger', 'Faturamento', 'Preco_Base_Cont', 'Preco_Base', 'Financeiro', 'MC_Cont', 'MC', 'Pedidos', 'Quantidade'];

var glb_strSQL_Select;
var glb_strSQL_From;
var glb_strSQL_Where1;
var glb_strSQL_Where2;

var glb_sTimer = null;
var glb_nQtdDynamicsDSO = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
totalizacaoVendas()
totalizacaoVendas_DSC()
********************************************************************/


//********************************************************************
// IMPLEMENTACAO DAS FUNCOES
//********************************************************************
function EmpresasIn2() {
    var i;
    var empresasID;

    // Come�a a montar a lista de empresas.
    empresasID = "(";

    // Monta a lista de empresas.
    for (i = 0; i < selEmpresas.options.length; i++) {
        // Se a empresa estiver selecionada, coloca ela na lista.
        if (selEmpresas.options[i].selected == true) {
            if (empresasID != "(") empresasID += ",";

            empresasID += selEmpresas.options[i].value;
        }
    }

    // Termina de montar a lista das empresas.
    empresasID += ")";

    return empresasID;
}

function adjustDivTotalVendas() {
    var i, j, optionStr, optionValue;
    var aCmbsDynamics;
    var aaCombos;

    txtDataInicio2.maxLength = 10;
    txtDataFim2.maxLength = 10;

    txtDataInicio2.value = glb_dCurrDate;
    txtDataFim2.value = glb_dCurrDate;

    selTotal1.onchange = selTotal_Change;
    selTotal2.onchange = selTotal_Change;
    selTotal3.onchange = selTotal_Change;
    selTotal4.onchange = selTotal_Change;

    selTotalOrdem1.onchange = selTotalOrdem_Change;
    selTotalOrdem2.onchange = selTotalOrdem_Change;
    selTotalOrdem3.onchange = selTotalOrdem_Change;
    selTotalOrdem4.onchange = selTotalOrdem_Change;

    aCmbsDynamics = [selTotal1, selTotalOrdem1, selTotalOrdem2, selTotalOrdem3, selTotalOrdem4, selTotalOrdem5];
    aaCombos = [glb_aTotal, glb_aOrdem, glb_aOrdem, glb_aOrdem, glb_aOrdem, glb_aOrdem];

    clearComboEx(['selTotal1', 'selTotalOrdem1', 'selTotalOrdem2', 'selTotalOrdem3', 'selTotalOrdem4', 'selTotalOrdem5']);

    for (i = 0; i <= aCmbsDynamics.length - 1; i++) {
        for (j = 0; j <= aaCombos[i].length - 1; j++) {
            if (i == 0)
                if (aaCombos[i][j][2] == 0)
                    continue;
            optionValue = aaCombos[i][j][0];
            optionStr = aaCombos[i][j][1];
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
        }
    }

    fillCmbTotal2Ahead(1, 0);

    adjustElementsInForm([['lblTotal1', 'selTotal1', 13, 1, -10, -10],
                          ['lblTotalOrdem1', 'selTotalOrdem1', 13, 1],
                          ['lblDataInicio2', 'txtDataInicio2', 10, 1],
                          ['lblDataFim2', 'txtDataFim2', 10, 1],
                          ['lblTotal2', 'selTotal2', 13, 2, -10],
                          ['lblTotalOrdem2', 'selTotalOrdem2', 13, 2],
                          ['lblMoedaLocal', 'chkMoedaLocal', 3, 2],
                          ['lblTipoResultado', 'selTipoResultado', 10, 2, -10],
                          ['lblTotal3', 'selTotal3', 13, 3, -10],
                          ['lblTotalOrdem3', 'selTotalOrdem3', 13, 3],
                          ['lblEmpresas', 'selEmpresas', 13, 3],
                          ['lblTotal4', 'selTotal4', 13, 4, -10],
                          ['lblTotalOrdem4', 'selTotalOrdem4', 13, 4],
                          ['lblTotal5', 'selTotal5', 13, 5, -10],
                          ['lblTotalOrdem5', 'selTotalOrdem5', 13, 5],
                          ['lblFiltro2', 'txtFiltro2', 40, 6, -10, 3],

                          ['lblFormato', 'selFormato', 9, 6]], null, null, true);


    selTotal3.disabled = true;
    selTotalOrdem3.disabled = true;
    selTotal4.disabled = true;
    selTotalOrdem4.disabled = true;
    selTotal5.disabled = true;
    selTotalOrdem5.disabled = true;

    selEmpresas.style.height = 118;
    selEmpresas.style.width = 170;


}

function totalizacaoVendas() {
    //resetGlbsSQLVars();

    glb_nQtdDynamicsDSO = 2;

    // var sDataInicio = '\'' + dateFormatToSearch(txtDataInicio2.value) + '\'';
    // var sDataFim = '\'' + dateFormatToSearch(txtDataFim2.value) + '\'';
    // var sInformation = '';
    // var sFiltro = (trimStr(txtFiltro2.value).length > 0 ? ' AND (' + trimStr(txtFiltro2.value) + ')' : '');
    // var sMarca = '';

    // if ((chkPedidosWeb.checked) && (chkPedidosPb.checked)) {
    // sFiltro = sFiltro + ' AND (Pedidos.OrigemPedidoID IN (606,608))';
    // } else if (chkPedidosWeb.checked) {
    // sFiltro = sFiltro + ' AND (Pedidos.OrigemPedidoID = 606) ';
    // } else if (chkPedidosPb.checked) {
    // sFiltro = sFiltro + ' AND (Pedidos.OrigemPedidoID = 608) ';
    // }

    // if (glb_nEmpresaID != 7)
    // {
    // if (glb_nTipoPerfilComercialID == 263)//Gerente de produto
    // {
    // sMarca = ' AND ProdutosEmpresa.ProprietarioID = ' + glb_USERID + ' ';
    // }
    // else if (glb_nTipoPerfilComercialID == 264)//Assistente de Produto
    // {
    // sMarca = ' AND ProdutosEmpresa.AlternativoID = ' + glb_USERID + ' ';
    // }
    // else if (glb_nTipoPerfilComercialID == 265)//Vendedor e Televendas
    // {
    // sMarca = ' AND Pedidos.ProprietarioID = ' + glb_USERID + ' ';
    // }
    // }

    // var nRegistros = '';

    // var nSelTotal1 = selTotal1.value;
    // var nSelTotalOrdem1 = selTotalOrdem1.value;

    // var strSQL_Select = 'SELECT ' + nRegistros + glb_aFields[nSelTotal1] + glb_strSQL_Select;
    // var strSQL_From = glb_strSQL_From;
    // var strSQL_Where1 = glb_strSQL_Where1;
    // var strSQL_Where2 = glb_strSQL_Where2 +
    // '(dbo.fn_Data_Zero(Itens.dtMovimento)>=' + sDataInicio + ' AND dbo.fn_Data_Zero(Itens.dtMovimento)<=' + sDataFim + ')' + sMarca + sFiltro + '))';
    // var strSQL_GroupBy = 'GROUP BY ' + glb_aGroupBy[nSelTotal1];
    // var strSQL_OrderBy = 'ORDER BY ' + glb_aOrderBy[nSelTotal1][nSelTotalOrdem1];
    // var strSQLTotais = strSQL_Select + strSQL_From + strSQL_Where1 + strSQL_Where2 + strSQL_GroupBy + strSQL_OrderBy;

    setConnection(dsoMoedaLocal);

    dsoMoedaLocal.SQL =
        'SELECT TOP 1 Moedas.SimboloMoeda ' +
        'FROM RelacoesPesRec Empresas WITH(NOLOCK), RelacoesPesRec_Moedas MoedasEmpresas WITH(NOLOCK), Conceitos Moedas WITH(NOLOCK) ' +
        'WHERE Empresas.TipoRelacaoID=12 AND Empresas.SujeitoID = ' + glb_nEmpresaID + ' AND Empresas.ObjetoID=999 AND ' +
            'MoedasEmpresas.RelacaoID=Empresas.RelacaoID AND MoedasEmpresas.Faturamento=1 AND ' +
            'Moedas.ConceitoID=MoedasEmpresas.MoedaID ' +
        'ORDER BY MoedasEmpresas.Ordem ';

    dsoMoedaLocal.ondatasetcomplete = totalizacaoVendas_DSC;
    dsoMoedaLocal.Refresh();

    // Seleciona as empresas logadas.
    setConnection(dsoEmpresasSelecionadas);
    dsoEmpresasSelecionadas.SQL = "select Fantasia from Pessoas with(nolock) where PessoaID in " + EmpresasIn2();
    dsoEmpresasSelecionadas.ondatasetcomplete = totalizacaoVendas_DSC;
    dsoEmpresasSelecionadas.Refresh();
}

function totalizacaoVendas_DSC() {

    glb_nQtdDynamicsDSO--;

    if (glb_nQtdDynamicsDSO > 0)
        return null;


    var dirA1;
    var dirA2;

    var ListColum = new String();
    var ListLabels = new String();


    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);


    var nContribuicao = 0;
    var nFaturamento = 0;
    var nPedidos = 0;
    var nQuantidade = 0;
    var nRegistros = 0;
    var nCounter = 0;

    var i, j;
    var nSelTotal1 = selTotal1.value;
    var nSelTotalOrdem1 = selTotalOrdem1.value;
    var nSelTotal2 = selTotal2.value;
    var nSelTotalOrdem2 = selTotalOrdem2.value;
    var nSelTotal3 = selTotal3.value;
    var nSelTotalOrdem3 = selTotalOrdem3.value;
    var nSelTotal4 = selTotal4.value;
    var nSelTotalOrdem4 = selTotalOrdem4.value;
    var nSelTotal5 = selTotal5.value;
    var nSelTotalOrdem5 = selTotalOrdem5.value;

    //var lTotalBold = false;
    var sDataInicio = '\'' + dateFormatToSearch(txtDataInicio2.value) + '\'';
    var sDataFim = '\'' + dateFormatToSearch(txtDataFim2.value) + '\'';
    var sInformation = '';
    var sFiltro = (trimStr(txtFiltro2.value).length > 0 ? ' AND (' + trimStr(txtFiltro2.value) + ')' : '');
    var sMarca = '';

    // if ((chkPedidosWeb.checked) && (chkPedidosPb.checked)) {
    // sFiltro = sFiltro + ' AND (Pedidos.OrigemPedidoID IN (606,608))';
    // } else if (chkPedidosWeb.checked) {
    // sFiltro = sFiltro + ' AND (Pedidos.OrigemPedidoID = 606) ';
    // } else if (chkPedidosPb.checked) {
    // sFiltro = sFiltro + ' AND (Pedidos.OrigemPedidoID = 608) ';
    // }

    // if (!(dirA1 && dirA2))
    // {
    // sFiltro += ' AND Pedidos.ProprietarioID = ' + glb_USERID + ' ';
    // }

    // if (glb_nEmpresaID != 7)
    // {
    // if (glb_nTipoPerfilComercialID == 263)//Gerente de produto
    // {
    // sMarca = ' AND ProdutosEmpresa.ProprietarioID = ' + glb_USERID + ' ';
    // }
    // else if (glb_nTipoPerfilComercialID == 264)//Assistente de Produto
    // {
    // sMarca = ' AND ProdutosEmpresa.AlternativoID = ' + glb_USERID + ' ';
    // }
    // else if (glb_nTipoPerfilComercialID == 265)//Vendedor e Televendas
    // {
    // sMarca = ' AND Pedidos.ProprietarioID = ' + glb_USERID + ' ';
    // }
    // if ((!(dirA1 && dirA2)) && (glb_nTipoPerfilComercialID == 265))
    // {
    // sMarca = '';
    // }
    // }

    var strSQL_Select, strSQL_From, strSQL_Where1, strSQL_Where2, strSQL_GroupBy, strSQL_OrderBy;
    var strSQL = new Array();
    var sExternalKey = '';
    var aTotais = null;
    var sSelectAnalise = '';
    var sGroupByAnalise = '';
    var sSimboloMoeda = '';

    if (!chkDataEx(txtDataInicio2.value)) {
        if (window.top.overflyGen.Alert('Data de In�cio � Inv�lida') == 0)
            return null;
        lockControlsInModalWin(false);
        return null;
    }
    else if (!chkDataEx(txtDataFim2.value)) {
        if (window.top.overflyGen.Alert('Data de Fim � Inv�lida') == 0)
            return null;
        lockControlsInModalWin(false);
        return null;
    }
    else if (putDateInYYYYMMDD(txtDataInicio2.value) > putDateInYYYYMMDD(txtDataFim2.value)) {
        if (window.top.overflyGen.Alert('Data Final maior que Data Inicial') == 0)
            return null;
        lockControlsInModalWin(false);
        return null;
    }

    // nRegistros = '';

    // var sGroupBy = 'GROUP BY ' + glb_aKey[nSelTotal1][0] + glb_aGroupBy[nSelTotal1] + sGroupByAnalise;
    // var sOrderBy = 'ORDER BY ' + glb_aKey[nSelTotal1][0];

    // for (i=0; i<5; i++)
    // {
    // sGroupBy += ',' + glb_aGroupBy[eval('nSelTotal' + (i+1))];

    // sExternalKey += glb_aKey[(i==0 ? nSelTotal1 : eval('nSelTotal' + (i)))][(i==0 ? 0 : 1)];

    // if ( (i>0) && (glb_aKey[eval('nSelTotal' + (i))][2] != 0) )
    // sExternalKey += sExternalKey.substr(0, sExternalKey.lastIndexOf(',') ) + ' AS ' + glb_aKey[eval('nSelTotal' + (i))][3] + ', ';

    // strSQL_Select = 'SELECT ' + (i==0 ? nRegistros : '') + sExternalKey + glb_aFields[eval('nSelTotal' + (i+1))] + 
    // (i==0 ? sSelectAnalise : '' ) + glb_strSQL_Select;

    // strSQL_From = glb_strSQL_From;

    // strSQL_Where1 = glb_strSQL_Where1;

    // strSQL_Where2 = glb_strSQL_Where2 +
    // '(dbo.fn_Data_Zero(Itens.dtMovimento)>=' + sDataInicio + ' AND dbo.fn_Data_Zero(Itens.dtMovimento)<=' + sDataFim + ')' + sMarca + sFiltro + '))';

    // strSQL_GroupBy = sGroupBy;

    // if (i==0)
    // sOrderBy += glb_aOrderBy[eval('nSelTotal' + (i+1))][nSelTotalOrdem1];
    // else
    // sOrderBy += ',' + glb_aOrderBy[eval('nSelTotal' + (i+1))][0];

    // strSQL_OrderBy = sOrderBy;

    // strSQL[i] = strSQL_Select + strSQL_From + strSQL_Where1 + strSQL_Where2 + strSQL_GroupBy + strSQL_OrderBy;
    // }
    var sChave = glb_aTotal[nSelTotal1][1];

    if (nSelTotal1 != nSelTotal2) {
        sChave += ' / ' + glb_aTotal[nSelTotal2][1];
        lTotalBold = true;
    }

    sInformation = '';
    sInformation2 = '';
    sSimboloMoeda = 'US$';
    if (!((dsoMoedaLocal.recordset.BOF) && (dsoMoedaLocal.recordset.EOF)) && (chkMoedaLocal.checked))
        sSimboloMoeda = dsoMoedaLocal.recordset['SimboloMoeda'].value;

    for (j = 1; j <= 5; j++) {
        if (!((j > 1) && (eval('nSelTotal' + j) == eval('nSelTotal' + (j - 1)))))
            sInformation += 'Total' + j + ':' + glb_aTotal[eval('nSelTotal' + j)][1] + '/' +
                glb_aOrdem[eval('nSelTotalOrdem' + j)][1] + '   ';
    }

    sInformation2 = 'Per�odo:' + txtDataInicio2.value + ' a ' + txtDataFim2.value + '   ' + 'Moeda:' + sSimboloMoeda;

    // Monta a lista de empresas que fizeram parte do relat�rio.
    var sInformation3 = "Empresa(s): ";
    for (; !dsoEmpresasSelecionadas.recordset.EOF; dsoEmpresasSelecionadas.recordset.moveNext()) {
        if (sInformation3 == "Empresa(s): ") {
            sInformation3 += dsoEmpresasSelecionadas.recordset["Fantasia"].value;
        } else {
            sInformation3 += ", " + dsoEmpresasSelecionadas.recordset["Fantasia"].value;
        }
    }

    // Titulo 1
    var nLeft = 0;
    var nTopParams1 = 0;
    var nTopParams2 = 4;
    var nTopParams3 = 8;
    var nTopHeader = 47;
    var nFont = 8;
    var TipoResult;

    // ListColum = (selTipoResultado.value == 471 ? "Ger:" : "Cont:") + "ID:Nome:Pedidos:Quantidade:Faturamento:Impostos";
    ListColum = "ID:Nome:Pedidos:Quantidade:Faturamento:Impostos";
    ListLabels = "ID:Nome:Pedidos:Quantidade:Faturamento:Impostos";

    //Gerencial
    if (selTipoResultado.value == 471) {
        ListColum = ListColum + ":Desp_Ger:Preco_Base:Custos_Ger:Custo_Base:Contr_Ger:MC";
        ListLabels = ListLabels + ":Despesas:Pre�o Base:Custo:Custo Base:Contr:MC";

        TipoResult = "Gerencial";
    }

    //Contabil

    else if (selTipoResultado.value == 470) {

        ListColum = ListColum + ":Preco_Base_Cont:Custo_Base_Cont:Contr_Cont:MC_Cont";
        ListLabels = ListLabels + ":Pre�o Base:Custo Base:Contribui��o:MC";

        TipoResult = "Cont�bil";
    }

    ListColum += ":CAP";
    ListLabels += ":CAP";

    var QueryN2 = "0";
    var QueryN3 = "0";
    var QueryN4 = "0";
    var QueryN5 = "0";
    var IndexKey = "0";
    var IndexKey3 = "0";
    var IndexKey4 = "0";
    var IndexKey5 = "0";
    var FieldsMae3 = "0";
    var FieldsMae4 = "0";
    var FieldsMae5 = "0";
    var sKeyNivelAnt = "0";
    var N2KeyNivelAnt = "0";

    var CallModal = new Boolean();
    var Nivel = 1;

    CallModal = false;

    // Totalizacao por 2 Niveis
    if (nSelTotal1 != nSelTotal2) {

        Nivel = 2;
        var sIndexKey;

        if (glb_aKey[nSelTotal1][2] != 0) {
            sIndexKey = glb_aKey[nSelTotal1][3] + ';' + glb_aDetailIndex[nSelTotalOrdem2];
            if (glb_aKey[nSelTotal1][2] == 1)

                N2KeyNivelAnt = 'Nome';
            else if (glb_aKey[nSelTotal1][2] == 2)

                N2KeyNivelAnt = 'ID';
        }
        else {
            N2KeyNivelAnt = 'ID';

            sIndexKey = (glb_aKey[nSelTotal1][1]).substr((glb_aKey[nSelTotal1][1]).lastIndexOf('.') + 1);
            sIndexKey = sIndexKey.substr(0, sIndexKey.lastIndexOf(',')) + ';' + glb_aDetailIndex[nSelTotalOrdem2];
        }

        IndexKey = sIndexKey;

        // Dados do Total
        nLeft = 0;

        // Totalizacao por 3 Niveis
        if (nSelTotal2 != nSelTotal3) {

            Nivel = 3;

            sIndexKey = '';


            if (glb_aKey[nSelTotal1][2] != 0)
                sIndexKey = glb_aKey[nSelTotal1][3] + ';';
            else {
                sIndexKey = (glb_aKey[nSelTotal1][1]).substr((glb_aKey[nSelTotal1][1]).lastIndexOf('.') + 1);
                sIndexKey = sIndexKey.substr(0, sIndexKey.lastIndexOf(',')) + '; ';
            }


            if (glb_aKey[nSelTotal2][2] != 0)
                sIndexKey += glb_aKey[nSelTotal2][3] + ';';
            else {
                sIndexKey += (glb_aKey[nSelTotal2][1]).substr((glb_aKey[nSelTotal2][1]).lastIndexOf('.') + 1);
                sIndexKey = sIndexKey.substr(0, sIndexKey.lastIndexOf(',')) + '; ';
            }
            sIndexKey += glb_aDetailIndex[nSelTotalOrdem3];

            var sFieldsMae = '';


            sKeyNivelAnt = (glb_aKey[nSelTotal2][2] == 1 ? ';Nome' : ';ID');


            if (glb_aKey[nSelTotal1][2] != 0)

                sFieldsMae = glb_aKey[nSelTotal1][3] + sKeyNivelAnt;

            else {
                sFieldsMae = (glb_aKey[nSelTotal1][1]).substr((glb_aKey[nSelTotal1][1]).lastIndexOf('.') + 1);
                sFieldsMae = sFieldsMae.substr(0, sFieldsMae.lastIndexOf(','));
                sFieldsMae += sKeyNivelAnt;
            }

            IndexKey3 = sIndexKey;
            FieldsMae3 = sFieldsMae;

            // Dados do Total
            nLeft = 0;

            // Totalizacao por 4 Niveis
            if (nSelTotal3 != nSelTotal4) {

                Nivel = 4;

                sIndexKey = '';

                if (glb_aKey[nSelTotal1][2] != 0)
                    sIndexKey = glb_aKey[nSelTotal1][3] + ';';
                else {
                    sIndexKey = (glb_aKey[nSelTotal1][1]).substr((glb_aKey[nSelTotal1][1]).lastIndexOf('.') + 1);
                    sIndexKey = sIndexKey.substr(0, sIndexKey.lastIndexOf(',')) + '; ';
                }


                if (glb_aKey[nSelTotal2][2] != 0)
                    sIndexKey += glb_aKey[nSelTotal2][3] + ';';
                else {
                    sIndexKey += (glb_aKey[nSelTotal2][1]).substr((glb_aKey[nSelTotal2][1]).lastIndexOf('.') + 1);
                    sIndexKey = sIndexKey.substr(0, sIndexKey.lastIndexOf(',')) + '; ';
                }

                if (glb_aKey[nSelTotal3][2] != 0)
                    sIndexKey += glb_aKey[nSelTotal3][3] + ';';
                else {
                    sIndexKey += (glb_aKey[nSelTotal3][1]).substr((glb_aKey[nSelTotal3][1]).lastIndexOf('.') + 1);
                    sIndexKey = sIndexKey.substr(0, sIndexKey.lastIndexOf(',')) + '; ';
                }

                sIndexKey += glb_aDetailIndex[nSelTotalOrdem4];

                sFieldsMae = '';


                sKeyNivelAnt = (glb_aKey[nSelTotal3][2] == 1 ? ';Nome' : ';ID');


                if (glb_aKey[nSelTotal1][2] != 0)
                    sFieldsMae = glb_aKey[nSelTotal1][3];
                else {
                    sFieldsMae = (glb_aKey[nSelTotal1][1]).substr((glb_aKey[nSelTotal1][1]).lastIndexOf('.') + 1);
                    sFieldsMae = sFieldsMae.substr(0, sFieldsMae.lastIndexOf(','));
                }


                sFieldsMae += ';';

                if (glb_aKey[nSelTotal2][2] != 0)
                    sFieldsMae += glb_aKey[nSelTotal2][3];
                else {
                    sFieldsMae += (glb_aKey[nSelTotal2][1]).substr((glb_aKey[nSelTotal2][1]).lastIndexOf('.') + 1);
                    sFieldsMae = sFieldsMae.substr(0, sFieldsMae.lastIndexOf(','));
                }


                sFieldsMae += sKeyNivelAnt;

                IndexKey4 = sIndexKey;
                FieldsMae4 = sFieldsMae;


                // Dados do Total
                nLeft = 0;

                // Totalizacao por 5 Niveis
                if (nSelTotal4 != nSelTotal5) {


                    Nivel = 5;

                    sIndexKey = '';


                    if (glb_aKey[nSelTotal1][2] != 0)
                        sIndexKey = glb_aKey[nSelTotal1][3] + ';';
                    else {
                        sIndexKey = (glb_aKey[nSelTotal1][1]).substr((glb_aKey[nSelTotal1][1]).lastIndexOf('.') + 1);
                        sIndexKey = sIndexKey.substr(0, sIndexKey.lastIndexOf(',')) + '; ';
                    }


                    if (glb_aKey[nSelTotal2][2] != 0)
                        sIndexKey += glb_aKey[nSelTotal2][3] + ';';
                    else {
                        sIndexKey += (glb_aKey[nSelTotal2][1]).substr((glb_aKey[nSelTotal2][1]).lastIndexOf('.') + 1);
                        sIndexKey = sIndexKey.substr(0, sIndexKey.lastIndexOf(',')) + '; ';
                    }

                    if (glb_aKey[nSelTotal3][2] != 0)
                        sIndexKey += glb_aKey[nSelTotal3][3] + ';';
                    else {
                        sIndexKey += (glb_aKey[nSelTotal3][1]).substr((glb_aKey[nSelTotal3][1]).lastIndexOf('.') + 1);
                        sIndexKey = sIndexKey.substr(0, sIndexKey.lastIndexOf(',')) + '; ';
                    }

                    if (glb_aKey[nSelTotal4][2] != 0)
                        sIndexKey += glb_aKey[nSelTotal4][3] + ';';
                    else {
                        sIndexKey += (glb_aKey[nSelTotal4][1]).substr((glb_aKey[nSelTotal4][1]).lastIndexOf('.') + 1);
                        sIndexKey = sIndexKey.substr(0, sIndexKey.lastIndexOf(',')) + '; ';
                    }

                    sIndexKey += glb_aDetailIndex[nSelTotalOrdem5];

                    sFieldsMae = '';


                    sKeyNivelAnt = (glb_aKey[nSelTotal4][2] == 1 ? ';Nome' : ';ID');


                    if (glb_aKey[nSelTotal1][2] != 0)
                        sFieldsMae = glb_aKey[nSelTotal1][3];
                    else {
                        sFieldsMae = (glb_aKey[nSelTotal1][1]).substr((glb_aKey[nSelTotal1][1]).lastIndexOf('.') + 1);
                        sFieldsMae = sFieldsMae.substr(0, sFieldsMae.lastIndexOf(','));
                    }


                    sFieldsMae += ';';

                    if (glb_aKey[nSelTotal2][2] != 0)
                        sFieldsMae += glb_aKey[nSelTotal2][3];
                    else {
                        sFieldsMae += (glb_aKey[nSelTotal2][1]).substr((glb_aKey[nSelTotal2][1]).lastIndexOf('.') + 1);
                        sFieldsMae = sFieldsMae.substr(0, sFieldsMae.lastIndexOf(','));
                    }


                    sFieldsMae += ';';

                    if (glb_aKey[nSelTotal3][2] != 0)
                        sFieldsMae += glb_aKey[nSelTotal3][3];
                    else {
                        sFieldsMae += (glb_aKey[nSelTotal3][1]).substr((glb_aKey[nSelTotal3][1]).lastIndexOf('.') + 1);
                        sFieldsMae = sFieldsMae.substr(0, sFieldsMae.lastIndexOf(','));
                    }


                    sFieldsMae += sKeyNivelAnt;

                    IndexKey5 = sIndexKey;
                    FieldsMae5 = sFieldsMae;

                    // Dados do Total
                    nLeft = 0;
                }
            }
        }
    }

    var find = "\\+";
    var strTeste = new RegExp(find, "g");

    var QueryN1;

    glb_USERID = getCurrUserID();

    QueryN1 = strSQL[0];
    QueryN2 = strSQL[1];
    QueryN3 = strSQL[2];
    QueryN4 = strSQL[3];
    QueryN5 = strSQL[4];

    var bchkMoedaLocal = false;

    if (chkMoedaLocal.checked)
        bchkMoedaLocal = true;

    //    var tipo = "PDF";

    var formato = selFormato.value;
    

    var strParameters = "relatorioID=" + selReports.value + "&formato=" + formato + "&sDataInicio=" + sDataInicio + "&sDataFim=" + sDataFim + "&sFiltro=" + sFiltro + "&sMarca=" + sMarca +
                        "&glb_nTipoPerfilComercialID=" + glb_nTipoPerfilComercialID + 
                        "&bchkMoedaLocal=" + bchkMoedaLocal + "&dirA1=" + dirA1 + "&dirA2=" + dirA2 + "&glb_USERID=" + glb_USERID + "&EmpresasIn2=" + EmpresasIn2() +
    //                      "&query1=" + QueryN1 + "&queryn2=" + QueryN2 + "&queryn3=" + QueryN3 + "&queryn4=" + QueryN4 + "&queryn5=" + QueryN5 +
                        "&indexkey=" + IndexKey + "&indexkey3=" + IndexKey3 + "&indexkey4=" + IndexKey4 + "&indexkey5=" + IndexKey5 + "&nSelTotalOrdem1=" + nSelTotalOrdem1 +
                        "&nSelTotalOrdem2=" + nSelTotalOrdem2 + "&nSelTotalOrdem3=" + nSelTotalOrdem3 + "&nSelTotalOrdem4=" + nSelTotalOrdem4 + "&nSelTotalOrdem5=" + nSelTotalOrdem5 +
                        "&fieldsmae3=" + FieldsMae3 + "&fieldsmae4=" + FieldsMae4 + "&fieldsmae5=" + FieldsMae5 + "&Nivel=" + Nivel +
                        "&keyAnt=" + N2KeyNivelAnt + "&nSelTotal1=" + nSelTotal1 + "&nSelTotal2=" + nSelTotal2 + "&nSelTotal3=" + nSelTotal3 + "&nSelTotal4=" + nSelTotal4 +
                        "&nSelTotal5=" + nSelTotal5 + "&information1=" + sInformation + "&information2=" + sInformation2 + "&information3=" + sInformation3 +
                        "&ListColum=" + ListColum + "&ListLabels=" + ListLabels + "&TipoResult=" + TipoResult + "&glb_nEmpresaID=" + glb_nEmpresaID;

    //strParameters = replaceStr(strParameters, "+", "%2B");

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/Reports.aspx?' + strParameters;


//    window.document.location = SYS_PAGESURLROOT + "/PrintJet/PaginaTeste.aspx?" + strParameters;

    //DireitoEspecifico
    //Pedidos->Saidas->Modal Print
    //Relatorio: 40140 -> Totalizacao de Vendas
    //A1||A2 -> Impressao
    //A2 -> Zebrar
    //A1&&A2 -> Excel
    //desabilitado -> Word        
    //(A1&&A2) -> somente pedidos onde o proprietario � igual ao usuario logado

}

function reports_onreadystatechange() {
    if ((document.readyState == 'loaded') ||
        (document.readyState == 'interactive') ||
        (document.readyState == 'complete')) {
        //winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
        pb_StopProgressBar(true);
        lockControlsInModalWin(false);
    }
}

function resetGlbsSQLVars() {
    var sTaxaMoeda = ' * 1 * POWER(-1, Pedidos.TipoPedidoID) ';
    var sTaxaMoedaContabil = ' / Pedidos.TaxaMoeda * POWER(-1, Pedidos.TipoPedidoID) ';
    var sDevolucao = ' * POWER(-1, Pedidos.TipoPedidoID) ';
    var nSelTotal1 = selTotal1.value;

    if (chkMoedaLocal.checked) {
        sTaxaMoeda = ' * Pedidos.TaxaMoeda * POWER(-1, Pedidos.TipoPedidoID) ';
        sTaxaMoedaContabil = ' * 1 * POWER(-1, Pedidos.TipoPedidoID) ';
    }

    glb_strSQL_Select = ' COUNT(DISTINCT Pedidos.PedidoID) AS Pedidos, ' +
        'SUM(Itens.Quantidade' + sDevolucao + ') AS Quantidade, CONVERT(NUMERIC(11,2), SUM(Itens.ValorFaturamento' + sTaxaMoeda + ')) AS Faturamento, ' +
        'CONVERT(NUMERIC(11,2), SUM(dbo.fn_PedidoItem_Totais(Itens.PedItemID,10)' + sTaxaMoedaContabil + ')) AS Impostos, ' +
        'CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosGerenciais.ValorDespesa, 0)' + sTaxaMoeda + ')) AS Desp_Ger, ' +
        'CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosGerenciais.ValorFinanceiro, Itens.ValorFinanceiro)' + sTaxaMoeda + ')) AS Financeiro, ' +
        'CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosGerenciais.PrecoBase, Itens.ValorFaturamentoBase)' + sTaxaMoeda + ')) AS Preco_Base, ' +
        'CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosContabil.PrecoBase, Itens.ValorFaturamentoBase)' + sTaxaMoedaContabil + ')) AS Preco_Base_Cont, ' +
        'CONVERT(NUMERIC(11,2), SUM((dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1321, 1, 1) + ' +
		    'dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1322, 1, 1) + ' +
		    'dbo.fn_PedidoItem_CampanhaTotais(Itens.PedItemID, 1323, 1, 1))' + sTaxaMoeda + ')) AS Custos_Ger, ' +
		'CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosGerenciais.CustoBase, Itens.ValorCustoBase) ' + sTaxaMoeda + ')) AS Custo_Base, ' +
		'CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosContabil.CustoBase, Itens.ValorCustoBase) ' + sTaxaMoedaContabil + ')) AS Custo_Base_Cont, ' +
        'CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosContabil.ValorContribuicao, Itens.ValorContribuicao)' + sTaxaMoedaContabil + ')) AS [Contr_Cont], ' +
        'CONVERT(NUMERIC(11,2), SUM(ISNULL(ResultadosGerenciais.ValorContribuicao, Itens.ValorContribuicaoAjustada)' + sTaxaMoeda + ')) AS [Contr_Ger], ' +
        'CONVERT(NUMERIC(12,2), dbo.fn_DivideZero(SUM(ISNULL(ResultadosGerenciais.ValorContribuicao, Itens.ValorContribuicaoAjustada)' + sTaxaMoeda + ') ,' +
			'SUM(ISNULL(ResultadosGerenciais.PrecoBase, Itens.ValorFaturamentoBase)' + sTaxaMoeda + ')) * 100) AS MC, ' +
        'CONVERT(NUMERIC(12,2), dbo.fn_DivideZero(SUM(ISNULL(ResultadosContabil.ValorContribuicao, Itens.ValorContribuicao)' + sTaxaMoedaContabil + ') ,' +
			'SUM(ISNULL(ResultadosContabil.PrecoBase, Itens.ValorFaturamentoBase)' + sTaxaMoedaContabil + ')) * 100) AS MC_Cont, ' +
		'COUNT(DISTINCT Pedidos.ParceiroID) as CAP ';
    //'CONVERT(NUMERIC(11,2), SUM(Itens.ValorComissao' + sTaxaMoeda + ')) AS [Comiss�o], CONVERT(NUMERIC(10,2), dbo.fn_DivideZero(SUM(Itens.ValorComissao), SUM(Itens.ValorContribuicaoAjustada)) * 100) AS CV ';

    glb_strSQL_From = 'FROM Pedidos_Itens Itens WITH(NOLOCK) ' +
    'INNER JOIN Operacoes CFOPs WITH(NOLOCK) ON (Itens.CFOPID = CFOPs.OperacaoID) ' +
    'INNER JOIN Pedidos WITH(NOLOCK) ON (Itens.PedidoID=Pedidos.PedidoID) ' +
    'LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) ' +
    'INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.SujeitoID=Pedidos.EmpresaID) ' +
    'INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID=Transacoes.OperacaoID) ' +
    'INNER JOIN Pessoas Vendedores WITH(NOLOCK) ON (Pedidos.ProprietarioID=Vendedores.PessoaID) ' +
    'INNER JOIN Pessoas Equipes WITH(NOLOCK) ON (Pedidos.AlternativoID=Equipes.PessoaID) ' +
    'INNER JOIN Pessoas Parceiros WITH(NOLOCK) ON (Pedidos.ParceiroID=Parceiros.PessoaID) ' +
    'LEFT OUTER JOIN TiposAuxiliares_Itens Classificacoes WITH(NOLOCK) ON (Parceiros.ClassificacaoID=Classificacoes.ItemID) ' +
    'INNER JOIN Pessoas Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID=Pessoas.PessoaID) ' +
    'INNER JOIN RelacoesPessoas RelPessoas WITH(NOLOCK) ON (Pedidos.ParceiroID=RelPessoas.SujeitoID) ' +
    'INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pedidos.ParceiroID=Enderecos.PessoaID) ' +
    'LEFT OUTER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) ' +
    'LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) ' +
    'LEFT OUTER JOIN Localidades Estados WITH(NOLOCK) ON (Enderecos.UFID=Estados.LocalidadeID) ' +
    'LEFT OUTER JOIN TiposAuxiliares_Itens Regioes WITH(NOLOCK) ON (Estados.TipoRegiaoID=Regioes.ItemID) ' +
    'INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID=Produtos.ConceitoID) ' +
    'INNER JOIN Conceitos Familias WITH(NOLOCK) ON (Produtos.ProdutoID=Familias.ConceitoID) ' +
    'INNER JOIN Conceitos Marcas WITH(NOLOCK) ON (Produtos.MarcaID=Marcas.ConceitoID) ' +
    'INNER JOIN Pessoas GPs WITH(NOLOCK) ON (GPs.PessoaID=ProdutosEmpresa.ProprietarioID) ' +
    'LEFT OUTER JOIN Conceitos LinhasProduto WITH(NOLOCK) ON (LinhasProduto.ConceitoID=Produtos.LinhaProdutoID) ' +
    //Incluido pelo projeto Custo. BJBN
    //Custo Gerencial 471.
    'LEFT OUTER JOIN Pedidos_Itens_Resultados ResultadosGerenciais WITH(NOLOCK) ON (ResultadosGerenciais.PedITemID = Itens.PedItemID) AND (ResultadosGerenciais.TipoResultadoID = 471) ' +
    //Custo Gerencial 470.
    'LEFT OUTER JOIN Pedidos_Itens_Resultados ResultadosContabil WITH(NOLOCK) ON (ResultadosContabil.PedITemID = Itens.PedItemID) AND (ResultadosContabil.TipoResultadoID = 470) ';

    glb_strSQL_Where1 = 'WHERE ((Pedidos.EmpresaID in ' + EmpresasIn2() + ' AND ' +
        'Pedidos.PessoaID <> Pedidos.EmpresaID AND ' +
        'RelPessoas.ObjetoID = Pedidos.EmpresaID AND RelPessoas.TipoRelacaoID=21 AND ' +
        'RelPessoas.EstadoID <> 5 AND ' +
        'Enderecos.EndFaturamento=1 AND Enderecos.Ordem=1 AND ';

    glb_strSQL_Where2 = 'ProdutosEmpresa.TipoRelacaoID=61 AND ' +
        'Transacoes.Resultado=1 AND Pedidos.Suspenso=0 AND ' +
	    '((Pedidos.TipoPedidoID=601 AND Pedidos.EstadoID>=30) OR (Pedidos.TipoPedidoID=602 AND Pedidos.EstadoID>=29)) AND ';

}

function selTotal_Change() {
    // Pega o numero do combo que disparou o evento (1 a 5)
    var nCurrSelTotal = parseInt(this.id.substr(8, 1));
    var sNextSelID = '';
    var sNextSelOrdemID = '';
    var nValAnt;
    var nCurrVal;
    var j = 0;

    // Repreenche os combos apartir do disparador desse evento ate o ultimo
    fillCmbTotal2Ahead(nCurrSelTotal, this.value);

    // Da loop apartir do combo seguinte ao disparador desse evento ate o ultimo
    for (j = nCurrSelTotal + 1; j <= 5; j++) {
        sNextSelID = 'selTotal' + j;
        sNextSelOrdemID = 'selTotalOrdem' + j;
        selOptByValueInSelect('modalprintHtml', sNextSelID, this.value);

        // Se nao for o primeiro combo do loop
        if (j > (nCurrSelTotal + 1)) {
            eval(sNextSelID + '.disabled = true');
            eval(sNextSelOrdemID + '.disabled = true');
        }
        else {
            if (nCurrSelTotal > 1) {
                nValAnt = eval('selTotal' + (nCurrSelTotal - 1) + '.value');
                nCurrVal = this.value;
                if (nValAnt == nCurrVal) {
                    eval(sNextSelID + '.disabled = true');
                    eval(sNextSelOrdemID + '.disabled = true');
                    continue;
                }
            }

            eval(sNextSelID + '.disabled = false');
            eval(sNextSelOrdemID + '.disabled = false');
        }
    }
}

function selTotalOrdem_Change() {

    // Pega o numero do combo que disparou o evento (1 a 5)
    var nCurrSelOrdem = parseInt(this.id.substr(13, 1));
    var sNextSelID = '';
    var nValAnt;
    var nCurrVal;
    var j = 0;

    // Da loop apartir do combo seguinte ao disparador desse evento ate o ultimo
    for (j = nCurrSelOrdem + 1; j <= 5; j++) {
        sNextSelID = 'selTotalOrdem' + j;
        selOptByValueInSelect('modalprintHtml', sNextSelID, this.value);

        // Se nao for o primeiro combo do loop
        if (j > (nCurrSelOrdem + 1)) {
            eval(sNextSelID + '.disabled = true');
        }
       else {
            eval(sNextSelID + '.disabled = false');
        }
    }
    
}


function fillCmbTotal2Ahead(nCmbTotal, nSelectedTotal) {
    var i, j, k, m;
    var aSelTotais = new Array();
    var bValidItem = true;
    var nItemSelected = 0;

    j = 0;
    for (i = nCmbTotal + 1; i <= 5; i++) {
        clearComboEx(['selTotal' + i]);
        aSelTotais[j] = eval('selTotal' + i);
        j++;
    }

    for (i = 0; i < aSelTotais.length; i++) {
        for (j = 0; j <= glb_aTotal.length - 1; j++) {
            bValidItem = true;

            for (k = 1; k <= nCmbTotal; k++) {
                nItemSelected = eval('selTotal' + k + '.value');
                nItemSelected = (nItemSelected == '' ? 0 : nItemSelected);
                if (ascan(glb_aTotal[nItemSelected][3], glb_aTotal[j][0], false) >= 0)
                    bValidItem = false;
            }

            if (nCmbTotal > 1) {
                for (m = nCmbTotal - 1; m >= 1; m--) {
                    nItemSelectedAnt = eval('selTotal' + m + '.value');
                    nItemSelectedAnt = (nItemSelectedAnt == '' ? 0 : nItemSelectedAnt);

                    if ((nItemSelectedAnt == glb_aTotal[j][0]) && (nSelectedTotal != glb_aTotal[j][0]))
                        bValidItem = false;
                }
            }

            if (bValidItem) {
                var oOption = document.createElement("OPTION");
                oOption.value = glb_aTotal[j][0];
                oOption.text = glb_aTotal[j][1];
                aSelTotais[i].add(oOption);
            }
        }
    }
}
