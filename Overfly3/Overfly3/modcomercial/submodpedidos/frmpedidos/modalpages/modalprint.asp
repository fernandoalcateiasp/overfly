<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'Funcao de uso geral, retorna um caracter replicate n vezes
Function replicate(ByVal sString, ByVal nVezes)

    Dim i, sNewString

    sNewString = ""
    
    If (IsNull(sString)) Then
        sString = ""
    End If

    For i=1 To nVezes
        sNewString = sNewString & sString
    Next

    replicate = sNewString

End Function

'Funcao de uso geral, PadL
Function padL(ByVal s, ByVal n, ByVal c)

    If (IsNull(s)) Then
        s = ""
    End If
    
    s = Trim(s)

    padL = replicate(c, n - Len(s)) & s

End Function

'Funcao de uso geral, PadR
Function padR(ByVal s, ByVal n, ByVal c)
    If (IsNull(s)) Then
        s = ""
    End If

    s = Trim(s)
    padR = s & replicate(c, n - Len(s))
End Function
%>
<html id="modalprintHtml" name="modalprintHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalprint.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
   
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_progressbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodalprint.js" & Chr(34) & "></script>" & vbCrLf

	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/relatoriominuta.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalprint_pedidos.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalprinttotalizacaovendas_pedidos.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/relatorioordemproducao.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalprintvendas_pedidos.js" & Chr(34) & "></script>" & vbCrLf
    %>


<%
'Script de variaveis globais

Dim i, sCaller, nEmpresaID, sEmpresaFantasia, sPaisEmpresa, nContextoID, nUserID, nPerfilComercial
Dim relatorioID, relatorio, EhDefault, relatorioFantasia, dirA1, dirA2, dirA1A2True
Dim nPedidoID, nNumeroVolumes, nNumeroSerieID, nProdutoID
Dim currDateFormat, bOpenInProposta

sCaller = ""
nEmpresaID = 0
sPaisEmpresa = ""
sEmpresaFantasia = ""
nContextoID = 0
nUserID = 0
nPerfilComercial = 0
nPedidoID = 0
nNumeroVolumes = 0
nNumeroSerieID = 0
nProdutoID = 0
bOpenInProposta = 0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

'nEmpresaID - ID da empresa logada
For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

'sEmpresaFantasia - fantasia da empresa logada
For i = 1 To Request.QueryString("sEmpresaFantasia").Count    
    sEmpresaFantasia = Request.QueryString("sEmpresaFantasia")(i)
Next

'nContextoID - ID do contexto
For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

'nUserID - ID do usuario logado
For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

'nPedidoID - ID do Pedido
For i = 1 To Request.QueryString("nPedidoID").Count    
    nPedidoID = Request.QueryString("nPedidoID")(i)
Next

'nNumeroVolumes - Numero de Volumes do Pedido
For i = 1 To Request.QueryString("nNumeroVolumes").Count    
    nNumeroVolumes = Request.QueryString("nNumeroVolumes")(i)
Next

'nNumeroSerieID - ID do Numero de Serie a imprimir
For i = 1 To Request.QueryString("nNumeroSerieID").Count    
    nNumeroSerieID = Request.QueryString("nNumeroSerieID")(i)
Next

For i = 1 To Request.QueryString("nProdutoID").Count    
    nProdutoID = Request.QueryString("nProdutoID")(i)
Next

'Formato corrente de data
For i = 1 To Request.QueryString("currDateFormat").Count
    currDateFormat = Request.QueryString("currDateFormat")(i)
Next

For i = 1 To Request.QueryString("bOpenInProposta").Count
    bOpenInProposta = Request.QueryString("bOpenInProposta")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

Dim rsData, strSQL
strSQL = "SELECT TOP 1 b.NomeInternacional " & _
		  "FROM Pessoas_Enderecos a WITH(NOLOCK), Localidades b WITH(NOLOCK) " & _
		  "WHERE a.PessoaID = " & CStr(nEmpresaID) & " AND a.PaisID=b.LocalidadeID " & _
		  "ORDER BY a.EndFaturamento DESC, Ordem"

Set rsData = Server.CreateObject("ADODB.Recordset")         
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

If (Not ((rsData.BOF) AND (rsData.EOF))) Then
	sPaisEmpresa = rsData.Fields("NomeInternacional").Value
End If

rsData.Close

Response.Write "var glb_TotalPesoBruto = 0;"
Response.Write vbcrlf

Response.Write "var glb_TotalGrossSKID = 0;"
Response.Write vbcrlf

strSQL = "SELECT ISNULL(TotalPesoBruto, 0) AS TotalPesoBruto, ISNULL(TotalGrossSKID,0) AS TotalGrossSKID " & _
		  "FROM Pedidos a WITH(NOLOCK) " & _
		  "WHERE a.PedidoID = " & CStr(nPedidoID)

Set rsData = Server.CreateObject("ADODB.Recordset")         
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

If (Not ((rsData.BOF) AND (rsData.EOF))) Then

	Response.Write "glb_TotalPesoBruto = " & CStr(rsData.Fields("TotalPesoBruto").Value) & ";"
	Response.Write vbcrlf

	Response.Write "glb_TotalGrossSKID = " & CStr(rsData.Fields("TotalGrossSKID").Value) & ";"
	Response.Write vbcrlf

End If
rsData.Close

strSQL = "SELECT TOP 1 ISNULL(b.TipoPerfilComercialID, 0) AS TipoPerfilComercialID " & _
         "FROM RelacoesPessoas a WITH(NOLOCK) " & _
            "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.CargoID) " & _
         "WHERE	a.TipoRelacaoID = 31 AND a.EstadoID = 2 AND a.SujeitoID = " & nUserID & _
         " ORDER BY a.FuncionarioDireto DESC, b.TipoPerfilComercialID"
         
Set rsData = Server.CreateObject("ADODB.Recordset")         
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

If (Not ((rsData.BOF) AND (rsData.EOF))) Then
   nPerfilComercial = rsData.Fields("TipoPerfilComercialID").Value
End If

rsData.Close

Set rsData = Nothing

Response.Write "var glb_nTipoPerfilComercialID = " & nPerfilComercial & ";"
Response.Write vbcrlf

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & nEmpresaID & ";"
Response.Write vbcrlf

Response.Write "var glb_sEmpresaFantasia = " & Chr(39) & sEmpresaFantasia & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nContextoID = " & nContextoID & ";"
Response.Write vbcrlf

Response.Write "var glb_nPedidoID = " & nPedidoID & ";"
Response.Write vbcrlf

Response.Write "var glb_nNumeroVolumes = " & nNumeroVolumes & ";"
Response.Write vbcrlf

Response.Write "var glb_nNumeroSerieID = " & nNumeroSerieID & ";"
Response.Write vbcrlf

Response.Write "var glb_nProdutoID = " & nProdutoID & ";"
Response.Write vbcrlf

Response.Write "var glb_bOpenInProposta = " & CStr(bOpenInProposta) & ";"
Response.Write vbcrlf

Response.Write "var glb_sPaisEmpresa= '" & CStr(sPaisEmpresa) & "';"
Response.Write vbcrlf

If (currDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (currDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (currDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If

Response.Write "var glb_sCurrDateYYYYMMDD = '" & CStr(Year(Date)) & padL(CStr(Month(Date)), 2, "0") & _
	padL(CStr(Day(Date)), 2, "0") & "';" & vbcrlf

Response.Write vbcrlf

'Monta array dos relatorios
Response.Write "// Array dos relatorios, com seus ids, nomes"
Response.Write vbcrlf

If ( (sCaller) = "PL" ) Then
    i = 0
Else
    i = 1
End If        

strSQL = "SELECT DISTINCT d.RecursoID AS RelatorioID, d.Recurso AS Relatorio, " & _
         "c.EhDefault, d.RecursoFantasia AS RelatorioFantasia, c.Ordem, " & _
         "g.Alterar1 AS A1, g.Alterar2 AS A2 " & _
         "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), " & _
         "RelacoesPesRec e WITH(NOLOCK), RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " & _
         "WHERE a.ObjetoID= " & CStr(nContextoID) & " AND a.TipoRelacaoID=1 AND a.EstadoID=2 " & _
         "AND a.SujeitoID=b.RecursoID AND b.EstadoID=2 AND b.TipoRecursoID=4 " & _ 
         "AND b.Principal=1 AND b.RecursoID=c.ObjetoID AND c.EstadoID=2 " & _
         "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID " & _
         "AND d.EstadoID=2 AND d.TipoRecursoID=5 AND d.ClassificacaoID=14 AND d.EhDetalhe= " & CStr(i) & " " & _
         "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) WHERE a.RecursoID= " & CStr(nContextoID) & " )) " & _
         "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " & _
         "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " & _
         "AND e.SujeitoID IN ((SELECT " & CStr(nUserID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(nUserID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
         "AND e.RelacaoID=f.RelacaoID " & _
         "AND f.EmpresaID= " & CStr(nEmpresaID) & " AND g.ContextoID= " & CStr(nContextoID) & " " & _
         "AND c.ObjetoID=g.RecursoMaeID AND d.RecursoID=g.RecursoID " & _
         "AND (g.Consultar1=1 OR g.Consultar2=1) " & _
         "AND f.PerfilID=g.PerfilID AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " & _
         "ORDER BY c.Ordem"

Set rsData = Server.CreateObject("ADODB.Recordset")         

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write vbcrlf
Response.Write "var glb_arrayReports = new Array();"
Response.Write vbcrlf
Response.Write vbcrlf
'------------------
i = 0
Do While (Not rsData.EOF)
        
    relatorioID = rsData.Fields("RelatorioID").Value
    relatorio = rsData.Fields("Relatorio").Value
    EhDefault = rsData.Fields("EhDefault").Value
    relatorioFantasia = rsData.Fields("RelatorioFantasia").Value
    dirA1 = rsData.Fields("A1").Value
    dirA2 = rsData.Fields("A2").Value
    
    If ( dirA1 AND dirA2 ) Then
        dirA1A2True = TRUE        
    Else
        dirA1A2True = FALSE    
    End If
        
    rsData.MoveNext
        
    Do While (Not rsData.EOF)
        If ( CLng(relatorioID) = CLng(rsData.Fields("RelatorioID").Value) ) Then
            'dirX1 e dirX2 e o anterior sempre
            If ( (rsData.Fields("A1").Value) AND (rsData.Fields("A2").Value) ) Then
                dirA1A2True = TRUE        
            End If

            dirA1 = dirA1 OR rsData.Fields("A1").Value
            dirA2 = dirA2 OR rsData.Fields("A2").Value
            rsData.MoveNext
        Else
            Exit Do
        End If    
    Loop    
        
    If ( (dirA1) AND (dirA2) ) Then
        If ( NOT (dirA1A2True)  ) Then
            dirA1 = FALSE
            dirA2 = TRUE
        End If
    End If    

    Response.Write "glb_arrayReports[" & CStr(i) & "] = new Array("
    Response.Write Chr(39)
    Response.Write CStr(relatorio)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write CStr(relatorioID)
    Response.Write ","
    Response.Write LCase(CStr(EhDefault))
    Response.Write ","
    Response.Write Chr(39)
    Response.Write CStr(relatorioFantasia)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write LCase(CStr(dirA1))
    Response.Write ","
    Response.Write LCase(CStr(dirA2))
    Response.Write ");"
    i = i + 1

Loop

rsData.Close
Set rsData = Nothing

Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "</script>"

Response.Write vbcrlf

%>
<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>
    
<SCRIPT LANGUAGE=javascript FOR=oPrinter EVENT=OnPrintFinish>
<!--
 oPrinter_OnPrintFinish();
//-->
</SCRIPT>
    
</head>

<body id="modalprintBody" name="modalprintBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle de impressao matricial -->
    <object classid="clsid:1DF93CFF-D5C8-4A6D-983F-19DBAAC698D9" id = "oPrinter" name = "oPrinter" width=0 height=0 VIEWASTEXT></object> 
	<!-- //@@ Objeto para geracao do arquivo ECF -->

    <p id="lblReports" name="lblReports" class="paraNormal"></p>
    <select id="selReports" name="selReports" class="fldGeneral"></select>

    <div id="divSolicitacaoCompra" name="divSolicitacaoCompra" class="divGeneral">
        <p id="lblContato1" name="lblContato1" class="lblGeneral">Contato</p>
        <select id="selContato1" name="selContato1" class="fldGeneral"></select>
        <p id="lblValidadeSolicitacao" name="lblValidadeSolicitacao" class="lblGeneral">Validade</p>
        <input type="text" id="txtValidadeSolicitacao" name="txtValidadeSolicitacao" class="fldGeneral" title="Validade da Solicita��o (dias �teis)"></input>                
        <p id="lblFormatoSolicitacao" name="lblFormatoSolicitacao" class="lblGeneral">Formato</p>
        <select id="selFormatoSolicitacao" name="selFormatoSolicitacao" class="fldGeneral" onchange="return selFormato_onchange(this)">
            <option value="1">PDF</option>		
            <option value="3">E-Mail</option>	
        </select>
        <p id="lblToSolicitacao" name="lblToSolicitacao" class="lblGeneral">Para:</p>
        <input type="text" id="txtToSolicitacao" name="txtToSolicitacao" class="fldGeneral" title="Para quem vai enviar o email ?"></input>
        <p id="lblCCSolicitacao" name="lblCCSolicitacao" class="lblGeneral">Cc:</p>
        <input type="text" id="txtCCSolicitacao" name="txtCCSolicitacao" class="fldGeneral" title="Com c�pia"></input>
        <p id="lblCCoSolicitacao" name="lblCCoSolicitacao" class="lblGeneral">Cco:</p>
        <input type="text" id="txtCCoSolicitacao" name="txtCCoSolicitacao" class="fldGeneral" title="Com c�pia oculta"></input>
        <p id="lblInformacoesAdicionais" name="lblInformacoesAdicionais" class="lblGeneral">IA</p>
        <input type="checkbox" id="chkInformacoesAdicionais" name="chkInformacoesAdicionais" class="fldGeneral" title="Informa��es Adicionais"></input>
        <p id="lblFichaTecnica2" name="lblFichaTecnica2" class="lblGeneral">FT</p>
        <input type="checkbox" id="chkFichaTecnica2" name="chkFichaTecnica2" class="fldGeneral" title="Ficha T�cnica?"></input>
        <p id="lblObservacoes2" name="lblObservacoes2" class="lblGeneral">Obs</p>
        <input type="checkbox" id="chkObservacoes2" name="chkObservacoes2" class="fldGeneral" title="Inclui Observa��es?"></input>
    </div>

    <div id="divPropostaComercial" name="divPropostaComercial" class="divGeneral">
        <p id="lblContato2" name="lblContato2" class="lblGeneral">Contato</p>
        <select id="selContato2" name="selContato2" class="fldGeneral" onchange="return selContato2_onchange(this)"></select>
        <p id="lblValidadeProposta" name="lblValidadeProposta" class="lblGeneral">Validade</p>
        <input type="text" id="txtValidadeProposta" name="txtValidadeProposta" class="fldGeneral" title="Validade da Proposta (dias �teis)"></input>        
        <p id="lblMarca" name="lblMarca" class="lblGeneral">Marca</p>
        <input type="checkbox" id="chkMarca" name="chkMarca" class="fldGeneral" title="Inclui Marca?"></input>
        <p id="lblModelo" name="lblModelo" class="lblGeneral">Modelo</p>
        <input type="checkbox" id="chkModelo" name="chkModelo" class="fldGeneral" title="Inclui Modelo?"></input>
        <p id="lblFichaTecnica" name="lblFichaTecnica" class="lblGeneral">FT</p>
        <input type="checkbox" id="chkFichaTecnica" name="chkFichaTecnica" class="fldGeneral" title="Inclui Ficha T�cnica?"></input>
        <p id="lblSUP" name="lblSUP" class="lblGeneral">SUP</p>
        <input type="checkbox" id="chkSUP" name="chkSUP" class="fldGeneral" title="Inclui SUP?"></input>
        <p id="lblObservacoes" name="lblObservacoes" class="lblGeneral">Obs</p>
        <input type="checkbox" id="chkObservacoes" name="chkObservacoes" class="fldGeneral" title="Inclui Observa��es?"></input>
                
        <p id="lblFormatoProposta" name="lblFormatoProposta" class="lblGeneral">Formato</p>
        <select id="selFormatoProposta" name="selFormatoProposta" class="fldGeneral" onchange="return selFormato_onchange(this)">
            <option value="1">PDF</option>		
            <option value="3">E-Mail</option>		            			
        </select>
        <p id="lblTo" name="lblTo" class="lblGeneral">Para:</p>
        <input type="text" id="txtTo" name="txtTo" class="fldGeneral" title="Para quem vai enviar o email ?"></input>
        <p id="lblCC" name="lblCC" class="lblGeneral">Cc:</p>
        <input type="text" id="txtCC" name="txtCC" class="fldGeneral" title="Com c�pia"></input>
        <p id="lblCCo" name="lblCCo" class="lblGeneral">Cco:</p>
        <input type="text" id="txtCCo" name="txtCCo" class="fldGeneral" title="Com c�pia oculta"></input>



    </div>

    <div id="divListaEmbalagem" name="divListaEmbalagem" class="divGeneral">
        <p id="lblFormatoListaEmbalagem" name="lblFormatoListaEmbalagem" class="lblGeneral">Formato</p>
        <select id="selFormatoListaEmbalagem" name="selFormatoListaEmbalagem" class="fldGeneral">
            <option value="1">PDF</option>		
            <option value="2">Excel</option>		            			
        </select>
    </div>


    <div id="divDemonstrativoCalculoICMSST" name="divDemonstrativoCalculoICMSST" class="divGeneral">
        <p id="lblFormatoSolicitacao2" name="lblFormatoSolicitacao2" class="lblGeneral">Formato</p>
        <select id="selFormatoSolicitacao2" name="selFormatoSolicitacao2" class="fldGeneral">
            <option value="2">Excel</option>
            <option value="1">PDF</option>
        </select>
    </div>


    <div id="divEtiquetasEmbalagem" name="divEtiquetasEmbalagem" class="divGeneral">
        <p id="lblEtiquetaVertical" name="lblEtiquetaVertical" class="lblGeneral">EV</p>
        <input type="text" id="txtEtiquetaVertical" name="txtEtiquetaVertical" class="fldGeneral" title="Etiquetas Verticais"></input>
        <p id="lblEtiquetaHorizontal" name="lblEtiquetaHorizontal" class="lblGeneral">EH</p>
        <input type="text" id="txtEtiquetaHorizontal" name="txtEtiquetaHorizontal" class="fldGeneral" title="Etiquetas Horizontais"></input>
        <p id="lblAltura" name="lblAltura" class="lblGeneral">Alt</p>
        <input type="text" id="txtAltura" name="txtAltura" class="fldGeneral" title="Altura (linhas)"></input>
        <p id="lblLargura" name="lblLargura" class="lblGeneral">Larg</p>
        <input type="text" id="txtLargura" name="txtLargura" class="fldGeneral" title="Largura (mm)"></input>
        <p id="lblMargemSuperior" name="lblMargemSuperior" class="lblGeneral">MS</p>
        <input type="text" id="txtMargemSuperior" name="txtMargemSuperior" class="fldGeneral" title="Margem Superior (linhas)"></input>
        <p id="lblMargemEsquerda" name="lblMargemEsquerda" class="lblGeneral">ME</p>
        <input type="text" id="txtMargemEsquerda" name="txtMargemEsquerda" class="fldGeneral" title="Margem Esquerda (mm)"></input>
        <p id="lblInicio" name="lblInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtInicio" name="txtInicio" class="fldGeneral"></input>
        <p id="lblFim" name="lblFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtFim" name="txtFim" class="fldGeneral"></input>
    </div>

    <div id="divBarCode" name="divBarCode" class="divGeneral">
        <p id="lblEtiquetas" name="lblEtiquetas" class="lblGeneral">Etiquetas</p>
        <select id="selEtiquetas" name="selEtiquetas" class="fldGeneral"></select>
        <p id="lblTemNumeroSerie" name="lblTemNumeroSerie" class="lblGeneral">NS</p>
        <input type="checkbox" id="chkTemNumeroSerie" name="chkTemNumeroSerie" class="fldGeneral" title="Produto tem n�mero de s�rie?"></input>
        <p id="lblVias" name="lblVias" class="lblGeneral">Vias</p>
        <input type="text" id="txtVias" name="txtVias" class="fldGeneral"></input>
        <p id="lblLabelHeight" name="lblLabelHeight" class="lblGeneral">Alt</p>
        <input type="text" id="txtLabelHeight" name="txtLabelHeight" class="fldGeneral" title="Altura (mm)"></input>
        <p id="lblLabelGap" name="lblLabelGap" class="lblGeneral">Gap</p>
        <input type="text" id="txtLabelGap" name="txtLabelGap" class="fldGeneral" title="Espa�amento entre etiquetas"></input>
        <p id="lblInicio3" name="lblInicio3" class="lblGeneral">In�cio</p>
        <input type="text" id="txtInicio3" name="txtInicio3" class="fldGeneral"></input>
        <p id="lblFim3" name="lblFim3" class="lblGeneral">Fim</p>
        <input type="text" id="txtFim3" name="txtFim3" class="fldGeneral"></input>
    </div>

    <div id="divRelatorioVendas" name="divRelatorioVendas" class="divGeneral">
        <p id="lblFabricante" name="lblFabricante" class="lblGeneral">Fabricante</p>
        <select id="selFabricante" name="selFabricante" class="fldGeneral"></select>
        <p id="lblPadrao" name="lblPadrao" class="lblGeneral">Padr�o</p>
        <select id="selPadrao" name="selPadrao" class="fldGeneral"></select>
        <p id="lblSoResultado" name="lblSoResultado" class="lblGeneral">Res</p>
        <input type="checkbox" id="chkSoResultado" name="chkSoResultado" class="fldGeneral" title="Somente pedidos com resultado?"></input>
        <p id="lblDadosParceiro" name="lblDadosParceiro" class="lblGeneral">Par</p>
        <input type="checkbox" id="chkDadosParceiro" name="chkDadosParceiro" class="fldGeneral" title="Dados adicionais do parceiro"></input>
        <p id="lblDadosProduto" name="lblDadosProduto" class="lblGeneral">Prod</p>
        <input type="checkbox" id="chkDadosProduto" name="chkDadosProduto" class="fldGeneral" title="Dados adicionais do produto"></input>
        <p id="lblDadosResultado" name="lblDadosResultado" class="lblGeneral">Res</p>
        <input type="checkbox" id="chkDadosResultado" name="chkDadosResultado" class="fldGeneral" title="Dados adicionais do resultado"></input>
        <p id="lblReportaFabricante" name="lblReportaFabricante" class="lblGeneral">RF</p>
        <input type="checkbox" id="chkReportaFabricante" name="chkReportaFabricante" class="fldGeneral" title="Somente produtos que reportam ao fabricante?"></input>
        <p id="lblPedidosWeb2" name="lblPedidosWeb2" class="lblGeneral">Web</p>
        <input type="checkbox" id="chkPedidosWeb2" name="chkPedidosWeb2" class="fldGeneral" title="S� pedidos da Web"></input>
        <p id="lblComissaoVendas" name="lblComissaoVendas" class="lblGeneral">Com</p>
        <input type="checkbox" id="chkComissaoVendas" name="chkComissaoVendas" class="fldGeneral" title="Incluir dados de comiss�o?"></input>
        <p id="lblMicrosoftTipoRelatorio" name="lblMicrosoftTipoRelatorio" class="lblGeneral">Tipo</p>
        <select id="selMicrosoftTipoRelatorio" name="selMicrosoftTipoRelatorio" class="fldGeneral">
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
        </select>

        <p id="lblMoedaLocal2" name="lblMoedaLocal2" class="lblGeneral">ML</p>
        <input type="checkbox" id="chkMoedaLocal2" name="chkMoedaLocal2" class="fldGeneral" title="Moeda local"></input>
        
        <p id="lblDataInicio1" name="lblDataInicio1" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio1" name="txtDataInicio1" class="fldGeneral"></input>
        <p id="lblDataFim1" name="lblDataFim1" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim1" name="txtDataFim1" class="fldGeneral"></input>
        <p id="lblFiltro1" name="lblFiltro1" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro1" name="txtFiltro1" class="fldGeneral"></input>
        <p id="lblFamilias" name="lblFamilias" class="lblGeneral">Fam�lias</p>
        <select id="selFamilias" name="selFamilias" class="fldGeneral" MULTIPLE></select>
        <p id="lblEmpresas2" name="lblEmpresas2" class="lblGeneral">Empresas</p>
        <select id="selEmpresas2" name="selEmpresas2" class="fldGeneral" MULTIPLE>
        
		<%
		
			Set rsData = Server.CreateObject("ADODB.Recordset")

			strSQL = "SELECT a.PessoaID AS fldID, a.Fantasia AS fldName " & _
				"FROM Pessoas a WITH(NOLOCK) " & _
				"WHERE a.PessoaID = " & nEmpresaID & " and a.EstadoID = 2 " & _
				"UNION " & _
				"SELECT b.PessoaID AS EmpresaID, b.Fantasia AS Empresa " & _
				"FROM FopagEmpresas a WITH(NOLOCK) INNER JOIN Pessoas b WITH(NOLOCK) ON a.EmpresaAlternativaID=b.PessoaID " & _
				"WHERE a.EmpresaID = " & nEmpresaID & " AND a.FuncionarioID IS NULL and b.EstadoID = 2 "
			
			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				
				If ( CLng(rsData.Fields("fldID").Value) = CLng(nEmpresaID) ) Then
					Response.Write " SELECTED "
				End If

				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>		
        </select>
        <p id="lblFormatoRelatorioVendas" name="lblFormatoRelatorioVendas" class="lblGeneral">Formato</p>
        <select id="selFormatoRelatorioVendas" name="selFormatoRelatorioVendas" class="fldGeneral" onchange="return selFormato_onchange(this)">            
            <option value="2">Excel</option>	
            <option value="1">PDF</option>		            
        </select>


    </div>

    <div id="divRelatorioCompras" name="divRelatorioCompras" class="divGeneral">
        <p id="lblFabricante2" name="lblFabricante2" class="lblGeneral">Fabricante</p>
        <select id="selFabricante2" name="selFabricante2" class="fldGeneral"></select>
        <p id="lblPadrao2" name="lblPadrao2" class="lblGeneral">Padr�o</p>
        <select id="selPadrao2" name="selPadrao2" class="fldGeneral"></select>
        <p id="lblMoedaLocal3" name="lblMoedaLocal3" class="lblGeneral">ML</p>
        <input type="checkbox" id="chkMoedaLocal3" name="chkMoedaLocal3" class="fldGeneral" title="Moeda local"></input>
        <p id="lblDataInicio3" name="lblDataInicio3" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio3" name="txtDataInicio3" class="fldGeneral"></input>
        <p id="lblDataFim3" name="lblDataFim3" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim3" name="txtDataFim3" class="fldGeneral"></input>
        <p id="lblFiltro3" name="lblFiltro3" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro3" name="txtFiltro3" class="fldGeneral"></input>
        <p id="lblFamilias2" name="lblFamilias2" class="lblGeneral">Fam�lias</p>
        <select id="selFamilias2" name="selFamilias2" class="fldGeneral" MULTIPLE></select>
    </div>

    <div id="divTotalVendas" name="divTotalVendas" class="divGeneral">
        <p id="lblTotal1" name="lblTotal1" class="lblGeneral">Total 1</p>
        <select id="selTotal1" name="selTotal1" class="fldGeneral"></select>
        <p id="lblTotalOrdem1" name="lblTotalOrdem1" class="lblGeneral">Ordem</p>
        <select id="selTotalOrdem1" name="selTotalOrdem1" class="fldGeneral"></select>
        <p id="lblTotal2" name="lblTotal2" class="lblGeneral">Total 2</p>
        <select id="selTotal2" name="selTotal2" class="fldGeneral"></select>
        <p id="lblTotalOrdem2" name="lblTotalOrdem2" class="lblGeneral">Ordem</p>
        <select id="selTotalOrdem2" name="selTotalOrdem2" class="fldGeneral"></select>
        <p id="lblTotal3" name="lblTotal3" class="lblGeneral">Total 3</p>
        <select id="selTotal3" name="selTotal3" class="fldGeneral"></select>
        <p id="lblTotalOrdem3" name="lblTotalOrdem3" class="lblGeneral">Ordem</p>
        <select id="selTotalOrdem3" name="selTotalOrdem3" class="fldGeneral"></select>
        <p id="lblTotal4" name="lblTotal4" class="lblGeneral">Total 4</p>
        <select id="selTotal4" name="selTotal4" class="fldGeneral"></select>
        <p id="lblTotalOrdem4" name="lblTotalOrdem4" class="lblGeneral">Ordem</p>
        <select id="selTotalOrdem4" name="selTotalOrdem4" class="fldGeneral"></select>
        <p id="lblTotal5" name="lblTotal5" class="lblGeneral">Total 5</p>
        <select id="selTotal5" name="selTotal5" class="fldGeneral"></select>
        <p id="lblTotalOrdem5" name="lblTotalOrdem5" class="lblGeneral">Ordem</p>
        <select id="selTotalOrdem5" name="selTotalOrdem5" class="fldGeneral"></select>
        <p id="lblEmpresas" name="lblEmpresas" class="lblGeneral">Empresas</p>
        <select id="selEmpresas" name="selEmpresas" class="fldGeneral" MULTIPLE>
        
		<%
		
			Set rsData = Server.CreateObject("ADODB.Recordset")

			strSQL = "SELECT a.PessoaID AS fldID, a.Fantasia AS fldName " & _
				"FROM Pessoas a WITH(NOLOCK) " & _
				"WHERE a.PessoaID = " & nEmpresaID & " and a.EstadoID = 2 " & _
				"UNION " & _
				"SELECT b.PessoaID AS EmpresaID, b.Fantasia AS Empresa " & _
				"FROM FopagEmpresas a WITH(NOLOCK) INNER JOIN Pessoas b WITH(NOLOCK) ON a.EmpresaAlternativaID=b.PessoaID " & _
				"WHERE a.EmpresaID = " & nEmpresaID & " AND a.FuncionarioID IS NULL and b.EstadoID = 2 "
			
			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				
				If ( CLng(rsData.Fields("fldID").Value) = CLng(nEmpresaID) ) Then
					Response.Write " SELECTED "
				End If

				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>		
        </select>
        <p id="lblDataInicio2" name="lblDataInicio2" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio2" name="txtDataInicio2" class="fldGeneral"></input>
        <p id="lblDataFim2" name="lblDataFim2" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim2" name="txtDataFim2" class="fldGeneral"></input>
        <p id="lblMoedaLocal" name="lblMoedaLocal" class="lblGeneral">ML</p>
        <input type="checkbox" id="chkMoedaLocal" name="chkMoedaLocal" class="fldGeneral" title="Moeda local"></input>
        
	    <p id="lblTipoResultado" name="lblTipoResultado" class="lblGeneral">Tipo</p>
        <select id="selTipoResultado" name="selTipoResultado" class="fldGeneral">
        <%
                Set rsData = Server.CreateObject("ADODB.Recordset")

                strSQL = "SELECT DISTINCT ItemID AS fldID, RTRIM(LTRIM(RIGHT(ItemMasculino,9))) AS fldName " &_
				    "FROM " &_
					    "TiposAuxiliares_Itens WITH(NOLOCK) " &_
				    "WHERE " &_
					    "TipoID = 118 " &_
				    "ORDER BY " &_
					    "fldName"

	           rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

 		        While Not rsData.EOF
				    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
    				
				    If ( CLng(rsData.Fields("fldID").Value) = CLng(471) ) Then
					    Response.Write " SELECTED "
				    End If

				    Response.Write "  TipoID=" & rsData.Fields("fldID").Value & " " & vbCrlf
				    Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				    rsData.MoveNext
			    Wend
    		
			    rsData.Close
			    Set rsData = Nothing
        %>		
        </select>
        
        <p id="lblFiltro2" name="lblFiltro2" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro2" name="txtFiltro2" class="fldGeneral"></input>
        <p id="lblFormato" name="lblFormato" class="lblGeneral">Formato</p>
        <select id="selFormato" name="selFormato" class="fldGeneral">
            <option value="2">Excel</option>
            <option value="1">PDF</option>					
        </select>
    </div>

    <div id="divRetiradaMercadoria" name="divRetiradaMercadoria" class="divGeneral">
        <p id="lblPessoa" name="lblPessoa" class="lblGeneral">Pessoa</p>
        <select id="selPessoa" name="selPessoa" class="fldGeneral"></select>
    </div>
    
    <div id="divPedidoRetorno" name="divPedidoRetorno" class="divGeneral">
        <p id="lblGerarPedido" name="lblGerarPedido" class="lblGeneral">Gerar Pedido</p>
        <input type="checkbox" id="chkGerarPedido" name="chkGerarPedido" class="fldGeneral" title="Gerar o pedido de retorno?"></input>
    </div>

    <div id="divOrdemDeProducao" name="divOrdemDeProducao" class="divGeneral">
    </div>

    <div id="divRelatorioTransportadora" name="divRelatorioTransportadora" class="divGeneral">
        <p id="lblTransportadora" name="lblTransportadora" class="lblGeneral">Transportadora</p>
        <select id="selTransportadora" name="selTransportadora" class="fldGeneral"></select>
        
	    <input type="button" id="btnTransportadoras" name="btnTransportadoras" value="Transportadora" LANGUAGE="javascript" onclick="return btn_transportadoraClick(this)" class="btns" title="Preencher combo de transportadoras.">
        
        <p id="lblFretePago" name="lblFretePago" class="lblGeneral">FP</p>
        <input type="checkbox" id="chkFretePago" name="chkFretePago" class="fldGeneral" title="Frete pago?"></input>
        <p id="lblDataInicioTransp" name="lblDataInicioTransp" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicioTransp" name="txtDataInicioTransp" class="fldGeneral"></input>
        <p id="lblDataFimTransp" name="lblDataFimTransp" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFimTransp" name="txtDataFimTransp" class="fldGeneral"></input>
        <p id="lblFiltroTransp" name="lblFiltroTransp" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltroTransp" name="txtFiltroTransp" class="fldGeneral"></input>
    </div>

    <div id="divRelatorioDeposito" name="divRelatorioDeposito" class="divGeneral">
        <p id="lblDeposito" name="lblDeposito" class="lblGeneral">Deposito</p>
        <select id="selDeposito" name="selDeposito" class="fldGeneral"></select>
        
	    <input type="button" id="btnDeposito" name="btnDeposito" value="Deposito" LANGUAGE="javascript" onclick="return btn_DepositoClick(this)" class="btns" title="Preencher combo de transportadoras.">
        
        <p id="lblFP" name="lblFP" class="lblGeneral">FP</p>
        <input type="checkbox" id="chkFP" name="chkFP" class="fldGeneral" title="Frete pago?"></input>
        <p id="lblE" name="lblE" class="lblGeneral">E</p>
        <input type="checkbox" id="chkE" name="chkE" class="fldGeneral" LANGUAGE=javascript onclick="return chkLiberaco_onclick(this)" title="Est. Embalagem"></input>
        <p id="lblZ" name="lblZ" class="lblGeneral">Z</p>
        <input type="checkbox" id="chkZ" name="chkZ" class="fldGeneral" onclick="return chkLiberaco_onclick(this)" title="Est. Deletado"></input>
        <p id="lblL" name="lblL" class="lblGeneral">L</p>
        <input type="checkbox" id="chkL" name="chkL" class="fldGeneral" onclick="return chkLiberaco_onclick(this)" title="Est. Liquidado"></input>
        <p id="lblC" name="lblC" class="lblGeneral">C</p>
        <input type="checkbox" id="chkC" name="chkC" class="fldGeneral" onclick="return chkLiberaco_onclick(this)" title="Est. Cadastrado"></input>
        <p id="lblF" name="lblF" class="lblGeneral">F</p>
        <input type="checkbox" id="chkF" name="chkF" class="fldGeneral" onclick="return chkLiberaco_onclick(this)" title="Est. Faturado"></input>
        <p id="lblD" name="lblD" class="lblGeneral">D</p>
        <input type="checkbox" id="chkD" name="chkD" class="fldGeneral" onclick="return chkLiberaco_onclick(this)" title="Est. Despachado"></input>
        <p id="lblEntrada" name="lblEntrada" class="lblGeneral">E</p>
        <input type="checkbox" id="chkEntrada" name="chkEntrada" class="fldGeneral" onclick="return chkLiberaco_onclick(this)" title="Entrada"></input>
        <p id="lblSaida" name="lblSaida" class="lblGeneral">S</p>
        <input type="checkbox" id="chkSaida" name="chkSaida" class="fldGeneral" onclick="return chkLiberaco_onclick(this)" title="Saida"></input>
        <p id="lblDataInicioLiberacao" name="lblDataInicioLiberacao" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicioLiberacao" name="txtDataInicioLiberacao" class="fldGeneral"></input>
        <p id="lblDataFimLiberacao" name="lblDataFimLiberacao" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFimLiberacao" name="txtDataFimLiberacao" class="fldGeneral"></input>
        <p id="lblFiltroDeposito" name="lblFiltroDeposito" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltroDeposito" name="txtFiltroDeposito" class="fldGeneral"></input>
    </div>
    
    <div id="divMinutaTransporte" name="divMinutaTransporte" class="divGeneral">
        <p id="lbldtInicio" name="lbldtInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtdtInicio" name="txtdtInicio" class="fldGeneral"></input>
        <p id="lbldtFim" name="lbldtFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtdtFim" name="txtdtFim" class="fldGeneral"></input>
        <p id="lblPesquisa" name="lblPesquisa" class="lblGeneral">Pesquisa</p>
        <input type="text" id="txtPesquisa" name="txtPesquisa" class="fldGeneral"></input>
        <p id="lblTransportadoraMT" name="lblTransportadoraMT" class="lblGeneral">Transportadora</p>
        <select id="selTransportadoraMT" name="selTransportadoraMT" class="fldGeneral"></select>
	    <input type="button" id="btnPesquisar" name="btnPesquisar" value="Pesquisar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Preencher combo de transportadoras.">
        <p id="lblMinuta" name="lblMinuta" class="lblGeneral">Minuta</p>
        <select id="selMinuta" name="selMinuta" class="fldGeneral"></select>
        <p id="lblFormatoMinuta" name="lblFormatoMinuta" class="lblGeneral">Formato</p>        
	    <select id="selFormatoMinuta" name="selFormatoMinuta" class="fldGeneral">
            <option value="1">PDF</option>
            <option value="2">Excel</option>		
        </select>
    </div>
    <div id="divComissoes" name="divComissoes" class="divGeneral">
        <p id="lblTipoComissao" name="lblTipoComissao" class="lblGeneral">Tipo Comiss�o</p>
        <select id="selTipoComissaoID" name="selTipoComissaoID" class="fldGeneral"> 
        
        <%		
			Set rsData = Server.CreateObject("ADODB.Recordset")

			strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem  " & _
				        "FROM dbo.TiposAuxiliares_Itens  WITH(NOLOCK) " & _
				        "WHERE TipoID = 406 AND EstadoID = 2 " & _
				        "ORDER BY Ordem " 
			
			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				
				If ( CLng(rsData.Fields("Ordem").Value) = CLng(1) ) Then
					Response.Write " SELECTED "
				End If

				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>		
        
        </select>
        <p id="lblEstadoComissao" name="lblEstadoComissao" class="lblGeneral">Estado Comiss�o</p>
        <select id="selEstadoComissaoID" name="selEstadoComissaoID" class="fldGeneral">
        <%		
			Set rsData = Server.CreateObject("ADODB.Recordset")

			strSQL = "SELECT 0 AS fldID, 'Todos' AS fldName, 0 Ordem  UNION " & _
			            "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem  " & _
				        "FROM dbo.TiposAuxiliares_Itens  WITH(NOLOCK) " & _
				        "WHERE TipoID = 423 AND EstadoID = 2 AND Filtro LIKE '%<PED>%' " & _
				        "ORDER BY Ordem " 
			
			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				
				If ( CLng(rsData.Fields("fldID").Value) = CLng(1071) ) Then
					Response.Write " SELECTED "
				End If

				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>		
        
        </select>
        <p id="lblEmpresaComissao" name="lblEmpresaComissao" class="lblGeneral">Empresas</p>
        <select id="selEmpresaComissao" name="selEmpresaComissao" class="fldGeneral" MULTIPLE>		
        <%		
			Set rsData = Server.CreateObject("ADODB.Recordset")

			strSQL = "SELECT a.PessoaID AS fldID, a.Fantasia AS fldName " & _
				"FROM Pessoas a WITH(NOLOCK) " & _
				"WHERE a.PessoaID = " & nEmpresaID & " and a.EstadoID = 2 " & _
				"UNION " & _
				"SELECT b.PessoaID AS EmpresaID, b.Fantasia AS Empresa " & _
				"FROM FopagEmpresas a WITH(NOLOCK) INNER JOIN Pessoas b WITH(NOLOCK) ON a.EmpresaAlternativaID=b.PessoaID " & _
				"WHERE a.EmpresaID = " & nEmpresaID & " AND a.FuncionarioID IS NULL and b.EstadoID = 2 "
			
			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				
				If ( CLng(rsData.Fields("fldID").Value) = CLng(nEmpresaID) ) Then
					Response.Write " SELECTED "
				End If

				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>		        
        </select>        
        <p id="lbldtVencimentoInicio" name="lbldtVencimentoInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtdtVencimentoInicio" name="txtdtVencimentoInicio" class="fldGeneral"></input>
        <p id="lbldtVencimentoFim" name="lbldtVencimentoFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtdtVencimentoFim" name="txtdtVencimentoFim" class="fldGeneral"></input>
        <p id="lblArgumento" name="lblArgumento" class="lblGeneral">Argumento</p>
        <input type="text" id="txtArgumento" name="txtArgumento" class="fldGeneral"></input>        
        <p id="lblProprietario" name="lblProprietario" class="lblGeneral">Prop</p>
        <input type="checkbox" id="chkProprietario" name="chkProprietario" class="fldGeneral" title="Pesquisa por proprietario?"></input>
        <input type="button" id="btnPesquisarParceiroProprietario" name="btnPesquisarParceiroProprietario" value="P" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Preencher combo de parceiros"></input>        
        <p id="lblParceiroProprietario" name="lblParceiroProprietario" class="lblGeneral">Parceiro</p>
        <select id="selParceirosProprietariosID" name="selParceirosProprietariosID" class="fldGeneral"></select>
        <p id="lblFiltroComissoes" name="lblFiltroComissoes" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltroComissoes" name="txtFiltroComissoes" class="fldGeneral"></input>

        <p id="lblFormatoDivComissoes" name="lblFormatoDivComissoes" class="lblGeneral">Formato</p>
        <select id="selFormatoDivComissoes" name="selFormatoDivComissoes" class="fldGeneral">
            <option value="1">PDF</option>		
            <option value="2">Excel</option>		            			
        </select>

    </div>
    
    <div id="divRelatorioFabricante" name="divRelatorioFabricante" class="divGeneral">
        <p id="lblTotalGross" name="lblTotalGross" class="lblGeneral" title="Total Gross Kg (with SKID)">Total Gross</p>
        <input type="text" id="txtTotalGross" name="txtTotalGross" class="fldGeneral" title="Total Gross Kg"></input>
        <p id="lblWS" name="lblWS" class="lblGeneral">Skid</p>
        <input type="checkbox" id="chkWS" name="chkWS" class="fldGeneral" title="With Skid"></input>
	</div>
	
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>   

    <iframe id="frmReport" name="frmReport" style="display:none"  frameborder="no"></iframe>
    
</body>

</html>
