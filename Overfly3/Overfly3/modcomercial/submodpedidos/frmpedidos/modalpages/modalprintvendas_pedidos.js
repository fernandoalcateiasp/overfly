/********************************************************************
modalprintvendas_pedidos.js

Library javascript para o modalprint.asp
Pedidos 
********************************************************************/
var glb_Empresa = getCurrEmpresaData();
var glb_nEmpresaID = glb_Empresa[0];
var nItensSelected = 0;

function relatorioVendas()
{
    var i, nItensSelected;

    var dirA1;
    var dirA2;
    var strSQL = "";
    var sReportaFabricante = '';
    var sPedidosWeb = '';
    var sDistinct = '';
    var sMesAno = '';

    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);

    if (!chkDataEx(txtDataInicio1.value)) {
        if (window.top.overflyGen.Alert('Data de In�cio � Inv�lida') == 0)
            return null;
        lockControlsInModalWin(false);
        return null;
    }
    else if (!chkDataEx(txtDataFim1.value)) {
        if (window.top.overflyGen.Alert('Data de Fim � Inv�lida') == 0)
            return null;
        lockControlsInModalWin(false);
        return null;
    }
    // Padr�o SanDisk
    if (selPadrao.value == 954) {
        var sResultado = (chkSoResultado.checked ? ' AND Transacoes.Resultado = 1 ' : '');

        if (selFabricante.value == 0) {
            if (window.top.overflyGen.Alert('Selecione um fabricante') == 0)
                return null;

            lockControlsInModalWin(false);
            return null;
        }
    }

    if (selPadrao.value == 707) {
        sFiltroEmpresa = "";
        var rExp = '/\D/g';
        var aString = rExp.split();
        //var aString = sDataFim.Split(rExp);
        // MM/YYYY
        sMesAno = padL(aString[0], 2, "0") + "/" + aString[2];
    }

    var aEmpresa = getCurrEmpresaData();
    var sDataInicio = '\'' + dateFormatToSearch(txtDataInicio1.value) + ' 00:00:00' + '\'';
    var sDataFim = '\'' + dateFormatToSearch(txtDataFim1.value) + ' 23:59:59' + '\'';

    var sFiltro = (trimStr(txtFiltro1.value).length > 0 ? ' AND (' + trimStr(txtFiltro1.value) + ')' : '');    
    var nIdiomaDeID = aEmpresa[7];
    var nIdiomaParaID = aEmpresa[8];
    var EmpresaNome = aEmpresa[6];
    var Empresas = EmpresasIn();
    var Familias = FamiliaIn();


    var formato = selFormatoRelatorioVendas.value;

    var strParameters = "relatorioID=" + selReports.value + "&formato=" + formato + "&sDataInicio=" + sDataInicio + "&sDataFim=" + sDataFim + "&Filtro=" + txtFiltro1.value +
                        "&glb_nTipoPerfilComercialID=" + glb_nTipoPerfilComercialID + "&DATE_SQL_PARAM=" + DATE_SQL_PARAM +
                        "&ReportaFabricante=" + chkReportaFabricante.checked + "&PedidosWeb=" + chkPedidosWeb2.checked +
                        "&MoedaLocal=" + chkMoedaLocal2.checked + "&SoResultado=" + chkSoResultado.checked + "&DadosProduto=" + chkDadosProduto.checked + "&DadosParceiro=" + chkDadosParceiro.checked +
                        "&DadosResultado=" + chkDadosResultado.checked + "&ComissaoVendas=" + chkComissaoVendas.checked +
                        "&selFabricante=" + selFabricante.value + "&selPadrao=" + selPadrao.value + "&MesAno=" + sMesAno +
                        "&IdiomaDeID=" + nIdiomaDeID + "&IdiomaParaID=" + nIdiomaParaID + "&EmpresaID=" + glb_nEmpresaID +
                        "&MicrosoftTipoRelatorio=" + selMicrosoftTipoRelatorio.value + "&EmpresaNome=" + EmpresaNome +
                        "&dirA1=" + dirA1 + "&dirA2=" + dirA2 + "&UserID=" + glb_USERID + "&EmpresasIn=" + Empresas + "&Familias=" + Familias +
                        "&Title=" + "RelatoriodeVendas";//translateTerm(selReports.options[selReports.selectedIndex].innerText, null);

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/Reports.aspx?' + strParameters;
}

function EmpresasIn() {
    var i;
    var empresasID;

    // Come�a a montar a lista de empresas.
    empresasID = "(";

    // Monta a lista de empresas.
    for (i = 0; i < selEmpresas2.options.length; i++) {
        // Se a empresa estiver selecionada, coloca ela na lista.
        if (selEmpresas2.options[i].selected == true) {
            if (empresasID != "(") empresasID += ",";

            empresasID += selEmpresas2.options[i].value;
        }
    }

    // Termina de montar a lista das empresas.
    empresasID += ")";

    return empresasID;
}

function FamiliaIn() {
    var i;
    var Familia;

    Familia = "(";

    for (i = 0; i < selFamilias.length; i++) {
        if (selFamilias.options[i].selected == true) {
            nItensSelected++;
            Familia += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.ProdutoID=' + selFamilias.options[i].value;
        }
    }
    // Termina de montar a lista das empresas.
    Familia += ")";

    return Familia;
}
