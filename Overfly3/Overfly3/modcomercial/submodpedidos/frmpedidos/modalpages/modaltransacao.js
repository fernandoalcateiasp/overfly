/********************************************************************
modaltransacao.js

Library javascript para o modaltransacao.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';

var dsoTransacao = new CDatatransport('dsoTransacao');
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // Inicia o carregamento do grid
    fillModalGrid();
}

function fg_DblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if (fg.Row >= 1 )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S' ,
                new Array(fg.TextMatrix(fg.Row, 0), fg.TextMatrix(fg.Row, 1), fg.TextMatrix(fg.Row, 2), fg.TextMatrix(fg.Row, 3), fg.TextMatrix(fg.Row, 4)));
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null );    

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Transa��o', 1);
    
    // ajusta elementos da janela
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = ((FONT_WIDTH * 60) + 10);
        height = (MAX_FRAMEHEIGHT_OLD / 2) - ((ELEM_GAP * 6) + 8);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.currentStyle.width, 10);
        height = parseInt(divFG.currentStyle.height, 10) - 1;
    }
}

function fillModalGrid()
{
    var nTipoPedido;
    var sFiltroEhCliente;
    var aEmpresaData = getCurrEmpresaData();

    setConnection(dsoTransacao);

    // se nTipoPedido = 1 pedido de entrada, se nTipoPedido = 0 pedido de saida
    if (glb_nContextoID == 5111)
        nTipoPedido = 1;
    else if (glb_nContextoID == 5112)
        nTipoPedido = 0;
    
    // Filtro de EhCliente
    // 0 - S� Fornecedor
    // 1 - S� Cliente
    if ( glb_nEhCliente == 0 )
        sFiltroEhCliente = ' AND a.Fornecedor = 1 ';
    else
        sFiltroEhCliente = ' AND a.Cliente = 1 ';

    var nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');
    var aEmpresaData = getCurrEmpresaData();

    //Tag "<US>" criada para n�o listar as Transa��es que s�o de uso do Sistema. LYF 14/11/2018
    dsoTransacao.SQL = 'SELECT a.OperacaoID AS TransacaoID' +
                  ', dbo.fn_Tradutor(a.Operacao, ' + aEmpresaData[7] + ', ' + aEmpresaData[8] + ', NULL) AS Transacao ' +
                  ', a.ClienteDesativo ' +
                  ', a.TomacaoServico ' +
                  ', ISNULL(dbo.fn_Transacao_PermiteConceito( ' + aEmpresaData[7] + ', a.OperacaoID, 309, NULL), 0) AS NCM ' +
                  'FROM Operacoes a WITH(NOLOCK) ' +
                  'WHERE (a.TipoOperacaoID = 652 AND a.EstadoID = 2 AND a.Nivel = 3 AND ' +
                  '((a.PaisID IS NULL) OR (a.PaisID = ' + glb_nEmpresaPaisID + ')) AND ' +
                  'a.Entrada = ' + nTipoPedido + sFiltroEhCliente + 'AND ' +
                  'ISNULL(a.Observacoes, \'\') NOT LIKE \'%<US>%\') ';

    //Se o cliente est� desativado somente exibe as transa��es permitidas
    if (glb_clienteDesativo) {
        dsoTransacao.SQL = dsoTransacao.SQL + ' AND a.ClienteDesativo = 1 ';
    }
    
    dsoTransacao.SQL = dsoTransacao.SQL + ' ORDER BY ISNULL(a.Ordem, 999), a.OperacaoID';

    dsoTransacao.ondatasetcomplete = fillModalGrid_DSC;
    dsoTransacao.Refresh();
}

function fillModalGrid_DSC()
{
    var i;
    
    startGridInterface(fg);
    
    fg.FrozenCols = 0;

    headerGrid(fg, ['ID', 'Transa��o', 'ClienteDesativo', 'TomacaoServico', 'NCM'], [2, 3, 4]);

    fillGridMask(fg, dsoTransacao, ['TransacaoID', 'Transacao', 'ClienteDesativo', 'TomacaoServico', 'NCM'], ['', '', '', '', '']);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
        btnOK.disabled = false;
    else
        btnOK.disabled = true;

    // ajusta o body do html
    with (modaltransacaoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    if ( (glb_nTransacaoID != '') && (fg.Rows > 1) )
    {
        fg.Redraw = 0;
        fg.Row = 1;

        for (i=1; i<fg.Rows; i++)    
        {
            if (fg.TextMatrix(i,0) == glb_nTransacaoID)
            {
                fg.Row = i;
                fg.TopRow = i;
                break;
            }
        }
        fg.Redraw = 2;
    }
    
    // coloca foco no grid
    fg.focus();
}

function fg_KeyPress(KeyAscii)
{
    // Enter
    if ( (KeyAscii == 13) && (fg.Row > 0) )
    {
        btn_onclick(btnOK);
    }
}