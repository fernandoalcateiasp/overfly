/********************************************************************
modalKardex.js

Library javascript para o modalKardex.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_EstadoID=0;
var glb_PedidoFechado=0;
var glb_CFOPIDAlteraImposto = false;
var glb_CFOPID = false;
var glb_GridEditabled=false;
var glb_PedidoID = 0;
var glb_ComplementoValor = false;
var glb_nDSOCounter = 0;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;

// 0 - Impostos.
// 1 - Impostos complementares.
var glb_BtnImpostosMode = 0;

var dsoGrid = new CDatatransport('dsoGrid ');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();  
           
    glb_nDSOCounter = 2;

    // ajusta o body do html
    with (modalKardexBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 50;
    modalFrame.style.left = 0;
    
    // configuracao inicial do html
    setupPage();

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    getDataInServer();
    //setBtnImpostosMode();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    // 2. O usuario clicou o botao Cancela
    //if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false );    

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Kardex', 1);
    
    loadDataAndTreatInterface();    
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;
        
    // Ajusta botoes OK/Cancel para esta janela
    with (btnOK.style)
    {
        disabled = true;
        left = (widthFree - 2 * parseInt(width, 10) - ELEM_GAP) / 2;
    }
    with (btnCanc.style)
    {
        left = (parseInt(btnOK.currentStyle.left,10) +  parseInt(btnOK.currentStyle.width,10) + ELEM_GAP);
    }    
 
    lblTipo.style.top = topFree;
    lblTipo.style.left = ELEM_GAP;
    lblTipo.style.width = '100px';
    
    selTipo.style.top = lblTipo.offsetTop + lblTipo.offsetHeight;
    selTipo.style.left = ELEM_GAP;
    selTipo.style.width = FONT_WIDTH * 15;
    selTipo.value = '375';
    selTipo.onchange = selTipoOnchange;
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = selTipo.offsetTop + selTipo.offsetHeight + ELEM_GAP;
        width = modWidth - frameBorder - 2 * ELEM_GAP;    
        height = parseInt(btnOK.currentStyle.top, 10) -
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10) + 50;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    with (btnCanc) 
    {
        style.visibility = 'hidden';
    }
    with (btnOK) 
    {
        style.visibility = 'hidden';
    }

    selTipoOnchange();
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServer()//ERRO
{
    var sFiltro = '';
    var nPedido = glb_nPedidoID;
    var nTipo = parseInt((selTipo.options[selTipo.selectedIndex].value), 10);
    //if (selTipo.selectedIndex.value = true)
    
    sFiltro = ' AND a.TipoCustoID = ' + nTipo;
    
    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT b.ProdutoID AS ID, c.Conceito AS Produto, e.LoteID AS Lote, ISNULL(a.Quantidade,0) AS Quantidade, f.SimboloMoeda AS Moeda, ISNULL(b.TaxaMoeda,0) AS Taxa, ' +
		                 'ISNULL((ISNULL(a.ValorDespesasTotal, 0) + ISNULL(g.ValorImposto, 0) + a.ValorCustoTotal),0.00) AS ValorCustoBruto, ' +
		                 'ISNULL(g.ValorImposto,0) AS Impostos, ' +
		                 'ISNULL(a.ValorDespesasTotal,0) AS ValorDespesas, ' +
		                 'ISNULL((a.ValorCustoTotal / a.Quantidade),0) AS ValorCusto, ' +
		                 'ISNULL(a.ValorCustoTotal,0) AS ValorCustoTotal, ' +
		                 'CONVERT(VARCHAR,a.dtPagamento,103) AS Pagamento, ' +
		                 'ISNULL( b.ValorCustoFOB,0) AS ValorCustoFOB, ' +
		                 'CONVERT(VARCHAR,a.dtKardex,103) AS Calculadoem ' +
	                'FROM Pedidos_Itens_Kardex a WITH(NOLOCK) ' +
		                'INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.PedItemID = a.PedItemID) ' +
		                'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ProdutoID) ' +
		                'LEFT OUTER JOIN Lotes_Itens d WITH(NOLOCK) ON (d.LotPedItemID = a.LotePedItemID) ' +
		                'LEFT OUTER JOIN Lotes e WITH(NOLOCK) ON (e.LoteID = d.LoteID) ' +
		                'INNER JOIN Conceitos f WITH(NOLOCK) ON (f.ConceitoID = a.MoedaID) ' +
		                'LEFT OUTER JOIN (SELECT PedItemID, SUM(ValorImposto) AS ValorImposto FROM Pedidos_Itens_Impostos ' +
						                'GROUP BY PedItemID) g ON (g.PedItemID = a.PedItemID)' +
                    'WHERE b.PedidoID = ' + nPedido + sFiltro + ' ' +
                    'ORDER BY ISNULL(e.LoteID, 0), b.ProdutoID';

    dsoGrid.ondatasetcomplete = fillModalGrid_DSC;
    dsoGrid.Refresh();                      

    /*if (glb_BtnImpostosMode == 0) 
    {
        dsoGen01.ondatasetcomplete = fillModalGrid_DSC;
    }*/

}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGrid_DSC()
{
    var i, j, nSumImp;
    var nAliquota = 0;

    var dTFormat = '';

    EhImportacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EhImportacao' + '\'' + '].value');

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    if (selTipo.value == '374') 
    {
        if (!EhImportacao)
            HiddeColumn = [2, 8];
        else
            HiddeColumn = [2];
    }

    else if (selTipo.value == '375')
        HiddeColumn = [];

    fg.Redraw = 0;
    fg.FontSize = '8';
    
    headerGrid(fg,['ID',
                   'Produto',
                   'Lote',
                   'Quantidade',
                   'Moeda',
                   'Taxa',
                   'Custo Bruto',
                   'Impostos',
                   'Despesas',
                   'Custo Unit�rio',
                   'Custo Total',
                   'Pagamento',
                   'Custo FOB',
                   'Calculado em'], HiddeColumn);

    fillGridMask(fg, dsoGrid, ['ID',
                               'Produto',
                               'Lote',
                               'Quantidade',
                               'Moeda',
                               'Taxa',
                               'ValorCustoBruto',
                               'Impostos',
                               'ValorDespesas',
                               'ValorCusto',
                               'ValorCustoTotal',
                               'Pagamento',
                               'ValorCustoFOB',
                               'Calculadoem'],
                              ['', '', '', '', '', '', '999999999.99', '', '999999999.99', '999999999.99', '999999999.99', '', '999999999.99', ''],
                              ['', '', '', '', '', '', '###,###,##0.00', '', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '', '###,###,##0.00', '']);


    alignColsInGrid(fg, [0,2,3,5,6,7,8,9,10,12]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.BorderStyle = 1;
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    with (modalKardexBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
               
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();
    
    fg.Editable = glb_GridEditabled;
    lockControlsInModalWin(false);
    btnOK.disabled = true;
    selTipo.focus();   
}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/

function js_modalKardex_AfterEdit(Row, Col)
{
	if (glb_BtnImpostosMode == 0)
	    js_modalKardexNormais_AfterEdit(Row, Col);
	else if (glb_BtnImpostosMode == 1)
	    js_modalKardexComplementares_AfterEdit(Row, Col);
}

function js_modalKardexComplementares_AfterEdit(Row, Col)
{
	fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
	fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;
	btnOK.disabled = false;
}

function js_modalKardexNormais_AfterEdit(Row, Col)
{
    ;
}

function js_BeforeRowColChangemodalKardex(grid, OldRow, OldCol, NewRow, NewCol, Cancel)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalKardexDblClick(grid, Row, Col)
{
    ;
}

function currCellIsLocked(grid, nRow, nCol)
{
    ;
}

function selTipoOnchange()
{
    lblTipo.innerText = "Tipo " + selTipo.options[selTipo.selectedIndex].getAttribute("TipoID", 1);
    getDataInServer();
	
}
