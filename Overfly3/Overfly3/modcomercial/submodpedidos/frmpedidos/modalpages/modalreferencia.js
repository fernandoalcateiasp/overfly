/********************************************************************
modalreferencia.js
Library javascript para o modalreferencia.asp

********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_timerInterval = null;

// controla se os itens do pedido corrente no inf foram alterados
var glb_pedidoAlterado = false;

// controla ultimo combo alterado pelo usuario
// 0 - nenhum dos dois, 1 - Produto, 2 - Marca
var glb_Produto = false;
var glb_Marca = false;

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_callFillGridTimerInt = null;
var glb_nFamiliaID = null;
var glb_produtoDesativo = false;
var glb_nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TransacaoID\'].value');
var glb_nPermiteOutroProduto = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'PermiteOutroProduto\'].value');
var glb_bEhRetorno = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'EhRetorno\'].value');
var glb_nTipoPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoPedidoID' + '\'' + '].value');
var glb_nFornecedor = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Fornecedor' + '\'' + '].value');
var glb_nRowTrocaReferencia = 0;
var glb_nRowTrocaPesquisa = 0;

var dsoReferencia = new CDatatransport('dsoReferencia');
var dsoPesqProd = new CDatatransport('dsoPesqProd');
var dsoGen02 = new CDatatransport('dsoGen02');
var dsoGen03 = new CDatatransport('dsoGen03');
var dsoehImportacao = new CDatatransport('dsoehImportacao');
var dsoFamilia = new CDatatransport('dsoFamilia');
var dsoDescricao = new CDatatransport('dsoDescricao');
var dsoGravacao = new CDatatransport('dsoGravacao');
var dsoAtualizaValorUnitario = new CDatatransport('dsoAtualizaValorUnitario');
var dsoPedItemReferencia = new CDatatransport('dsoPedItemReferencia');
var dsoPescon = new CDatatransport('dsoPescon');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_pedidos_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 1;
    var nColTroca = getColIndexByColKey(fg, colunasReadOnly('Troca'));
    
    if ( glb_totalCols__ != false )
        firstLine = 2;
        
    if (NewRow >= firstLine)
        fg.Row = NewRow;
    else if ((NewRow < firstLine) && (fg.Rows > firstLine))
        fg.Row = firstLine;

    if (OldCol != NewCol && NewCol == nColTroca)
        js_ModalReferencia_ValidateEditRef(NewRow, NewCol);    
    
}


function js_fg_AfterRowColChange2(fg2, OldRow, OldCol, NewRow, NewCol) 
{
    var nColTroca = getColIndexByColKey(fg2, colunasReadOnly('Troca'));

    if (OldCol != NewCol && NewCol == nColTroca)
        js_ModalReferencia_ValidateEditPesq(NewRow, NewCol);    
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_ModalReferencia_ValidateEditPesq(Row, Col) 
{
    var nColTroca = getColIndexByColKey(fg2, colunasReadOnly('Troca'));
    
    if ((fg2.Editable) && (fg2.Col == nColTroca))
    {
        //esta marcando um novo
        if (fg2.EditText == 2) {

            //verifica se ja tem um marcado e desmarca
            if (glb_nRowTrocaPesquisa > 0)
                fg2.TextMatrix(glb_nRowTrocaPesquisa, nColTroca) = "";

            glb_nRowTrocaPesquisa = Row;
        }
        //esta demarcando
        else
            glb_nRowTrocaPesquisa = 0;
    }
    
    if (glb_nRowTrocaPesquisa > 0 && glb_nRowTrocaReferencia>0)
        btnTrocar.style.visibility = 'inherit';
    else
        btnTrocar.style.visibility = 'hidden';
    
    //setTotaisGrid(Row, Col);
    showHideFlds();

    if (colunasReadOnly('Troca') == 'Troca*')
        fg2.TextMatrix(Row, nColTroca) = "";
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_ModalReferencia_AfterEdit(Row, Col)
{
    var nColPrecoInterno = getColIndexByColKey(fg, colunasReadOnly('PrecoInterno'));
    var nColPrecoRevenda = getColIndexByColKey(fg, colunasReadOnly('PrecoRevenda'));
    var nColPrecoUnitario = getColIndexByColKey(fg, colunasReadOnly('PrecoUnitario'));
    var nColCalcValorTotal = getColIndexByColKey(fg, '_calc_ValorTotal_10*');
    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade');
    var nPrecoInterno = 0;
    var nPrecoRevenda = 0;
    var nPrecoUnitario = 0;
    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'Resultado\'].value');
    var nColTroca = getColIndexByColKey(fg, colunasReadOnly('Troca'));


    if (fg.Editable) 
    {
            
        if ((fg.col == nColPrecoInterno) || (fg.col == nColPrecoRevenda) || (fg.col == nColPrecoUnitario) || (fg.Col == nColQuantidade))
        {
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
            fg.TextMatrix(1, nColQuantidade) = 0;
            fg.TextMatrix(1, nColCalcValorTotal) = 0;
              
            nPrecoInterno = fg.ValueMatrix(Row, nColPrecoInterno);
            nPrecoRevenda = fg.ValueMatrix(Row, nColPrecoRevenda);
            nPrecoUnitario = fg.ValueMatrix(Row, nColPrecoUnitario);

            if (bResultado) 
            {
                if (nColPrecoInterno == Col) 
                {
                    if (nPrecoInterno > nPrecoRevenda)
                        fg.TextMatrix(Row, nColPrecoRevenda) = nPrecoInterno;

                    if (nPrecoInterno > nPrecoUnitario)
                        fg.TextMatrix(Row, nColPrecoUnitario) = nPrecoInterno;
                }
                else if (nColPrecoRevenda == Col) 
                {
                    if (nPrecoRevenda < nPrecoInterno)
                        fg.TextMatrix(Row, nColPrecoInterno) = nPrecoRevenda;

                    if (nPrecoRevenda > nPrecoUnitario)
                        fg.TextMatrix(Row, nColPrecoUnitario) = nPrecoRevenda;
                }
                else if (nColPrecoUnitario == Col) 
                {
                    if (nPrecoUnitario < nPrecoRevenda)
                        fg.TextMatrix(Row, nColPrecoRevenda) = nPrecoUnitario;

                    if (nPrecoUnitario < nPrecoInterno)
                        fg.TextMatrix(Row, nColPrecoInterno) = nPrecoUnitario;
                }
            }

            if (fg.ValueMatrix(Row, nColQuantidade) == 0)
                fg.TextMatrix(Row, nColCalcValorTotal) = '';
            else
                fg.TextMatrix(Row, nColCalcValorTotal) = fg.ValueMatrix(Row, nColQuantidade) * fg.ValueMatrix(Row, nColPrecoUnitario);
        }

        setTotaisGrid(Row, Col);

       /* if ((fg.col == nColPrecoInterno) || (fg.col == nColPrecoRevenda) || (fg.col == nColPrecoUnitario))
            atualizaValorUnitario(Row);*/

        if (fg.Col == nColTroca) 
        {
            //esta marcando um novo
            if (fg.EditText == 2) 
            {
                //verifica se ja tem um marcado e desmarca
                if (glb_nRowTrocaReferencia > 0)
                    fg.TextMatrix(glb_nRowTrocaReferencia, nColTroca) = "";

                if (glb_nRowTrocaReferencia != Row) 
                {
                    glb_nRowTrocaReferencia = Row;
                    fg.TextMatrix(glb_nRowTrocaReferencia, nColTroca) = "-1";
                }
            }
            //esta demarcando
            else
                glb_nRowTrocaReferencia = 0;
        }

    }

    if (glb_nRowTrocaPesquisa > 0 && glb_nRowTrocaReferencia > 0)
        btnTrocar.style.visibility = 'inherit';
    else
        btnTrocar.style.visibility = 'hidden';

    showHideFlds();

    if (colunasReadOnly('Troca') == 'Troca*')
        fg.TextMatrix(Row, nColTroca) = "";

}

function setTotaisGrid(nRow, Col)
{
    var nTotalQuantidade = 0;
    var nTotalValorTotal = 0;
    var nColPrecoUnitario = getColIndexByColKey(fg, colunasReadOnly('PrecoUnitario'));
    var nColCalcValorTotal = getColIndexByColKey(fg, '_calc_ValorTotal_10*');
    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade');
    var i = 0;

    if (fg.Rows > 1) 
    {
        fg.TextMatrix(1, nColQuantidade) = 0;
        fg.TextMatrix(1, nColCalcValorTotal) = 0;
    }

    for (i = 2; i < fg.Rows; i++) 
    {
        fg.TextMatrix(i, nColCalcValorTotal) = fg.ValueMatrix(i, nColPrecoUnitario) * fg.ValueMatrix(i, nColQuantidade);
        nTotalQuantidade += fg.ValueMatrix(i, nColQuantidade);
        nTotalValorTotal += fg.ValueMatrix(i, nColCalcValorTotal);
    }

    if (fg.Rows > 1) 
    {
        fg.TextMatrix(1, nColQuantidade) = nTotalQuantidade;
        fg.TextMatrix(1, nColCalcValorTotal) = nTotalValorTotal;
    }
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 25;
    //modalFrame.style.left = 0;   
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) 
    {   // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', glb_pedidoAlterado);
    //3. O Usuario clicou o botao Trocar
    else if (ctl.id == btnTrocar.id)
        btnTrocar_Clicked();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Inclus�o de Refer�ncia', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // Carrega o dso com dados de combos
    fillCmbsData();
    
    showHideFlds();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    btnOK.value = 'Incluir';
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o divFields
    elem = window.document.getElementById('divFields');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - 2 * ELEM_GAP;
        // altura e fixa para tres linhas de campos
        height = 3 * (16 + 24) + ELEM_GAP;
        y_gap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    
    // ajusta elementos no divFields
    adjustElementsInForm([['lblPedidoReferencia','txtPedidoReferencia',10,1,-10,-10],
                          ['lblNotaFiscalReferencia', 'txtNotaFiscalReferencia', 10, 1, -1],
                          ['btnFindReferencia', 'btn', 40, 1, 5]], null, null, true);
    
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = y_gap + ELEM_GAP - 100 ;
        width = modWidth - frameBorder - 2 * ELEM_GAP ;
        height = parseInt(lblNotaFiscalReferencia.currentStyle.top, 10) +
                 parseInt(top, 10) - frameBorder - ELEM_GAP + 90;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
        
    }

    // ajusta o divPesquisa
    elem = window.document.getElementById('divPesquisa');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP + 50;
        width = modWidth - frameBorder - 2 * ELEM_GAP;
        // altura e fixa para tres linhas de campos
        height = 3 * (16 + 24) + ELEM_GAP - 20;
        y_gap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    //divPesquisa
    adjustElementsInForm([['lblProdutoID','txtProdutoID',10,4,-10],
                          ['lblProduto','selProduto',24,4,-1],
                          ['lblMarca','selMarca',20,4,-1],
                          ['lblCaracteristica', 'selCaracteristica', 30, 4, 76],
                          ['lblValorLimite', 'txtValorLimite', 12, 4, 1],
                          ['lblHomologacao','chkHomologacao',3,5,-10],
                          ['lblPreLancamento','chkPreLancamento',3,5],
                          ['lblLancamento','chkLancamento',3,5],
                          ['lblAtivo','chkAtivo',3,5],
                          ['lblPromocao','chkPromocao',3,5],
                          ['lblPontaEstoque','chkPontaEstoque',3,5],
                          ['lblChave', 'selChave', 9, 5],
                          ['lblPalavraChave', 'txtPalavraChave', 30, 5, 2],
                          ['lblFiltro', 'txtFiltro', 43, 5]], null, null, true);

    startGridInterface(fg2);
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;

    //glb_nRowTrocaReferencia = 5;
    // ajusta o divFG2
    elem = window.document.getElementById('divFG2');
    with (elem.style) {
        visibility = 'hidden'; 
        border = 'transparent';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = y_gap + ELEM_GAP + 120;
        width = modWidth - frameBorder - 2 * ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) +
                 parseInt(top, 10) - frameBorder - ELEM_GAP - 635;
    }
    
    elem = document.getElementById('fg2');
    with (elem.style) {
        visibility = 'hidden';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG2').currentStyle.width);
        height = parseInt(document.getElementById('divFG2').currentStyle.height);
    }

    //divPesCon
    adjustElementsInForm([['lblConceitoID', 'txtConceitoID', 10, 6],
                          ['lblQuantidade', 'txtQuantidade', 10, 6, -1],
                          ['btnFindPesCon', 'btn', 40, 6, 5]], null, null, true);

    startGridInterface(fg3);
    fg3.Cols = 1;
    fg3.ColWidth(0) = parseInt(divFG3.currentStyle.width, 10) * 18;

    // ajusta o divFG3
    elem = window.document.getElementById('divFG3');
    with (elem.style) {
        visibility = 'hidden';
        border = 'transparent';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = y_gap + ELEM_GAP + 75;
        width = modWidth - frameBorder - 2 * ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) +
                 parseInt(top, 10) - frameBorder - ELEM_GAP - 550;
    }

    elem = document.getElementById('fg3');
    with (elem.style) {
        visibility = 'hidden';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG3').currentStyle.width);
        height = parseInt(document.getElementById('divFG3').currentStyle.height);
    }
    
	var nUserID = getCurrUserID();
	var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

	useBtnListar(true, divPesquisa, selMarca);

    with (btnListar)
    {
        style.left = parseInt(selMarca.currentStyle.left, 10) +
        parseInt(selMarca.currentStyle.width, 10) + ELEM_GAP;
        style.top = parseInt(selMarca.currentStyle.top, 10) - 1;
        style.width = parseInt(btnListar.currentStyle.width, 10) + 25;
        
    }

    with (btnFindReferencia) {
        style.left = parseInt(txtNotaFiscalReferencia.currentStyle.left, 10) +
        parseInt(txtNotaFiscalReferencia.currentStyle.width, 10) + ELEM_GAP - 6;
        style.top = parseInt(txtNotaFiscalReferencia.currentStyle.top, 10) - 1;

        style.heigth = parseInt(txtNotaFiscalReferencia.currentStyle.heigth, 10) - 1;
    }

    with (btnTrocar.style) 
    {
        visibility = 'hidden';
        left = parseInt(btnCanc.currentStyle.left, 10) + parseInt(btnCanc.currentStyle.width, 10)+ 10;
        top = parseInt(btnCanc.currentStyle.top, 10);
        width = parseInt(btnCanc.currentStyle.width, 10);
    }

    // restringe digitacao em campos numericos
    // e da outras providencias
    setMaskAndLengthToInputs();
}

/********************************************************************
Restringe quantidade de caracteres digitados, coloca
mascara para digitacao em campos numericos,
seleciona conteudo do campo quando em foco etc
********************************************************************/
function setMaskAndLengthToInputs()
{
    // Seleciona conteudo de campo texto quando em foco
    txtPedidoReferencia.onfocus = selFieldContent;
    txtNotaFiscalReferencia.onfocus = selFieldContent;
    txtPalavraChave.onfocus = selFieldContent;
    txtValorLimite.onfocus = selFieldContent;
    txtFiltro.onfocus = selFieldContent;
    
    // Altera checkBox se clicado no seu label
    lblHomologacao.onclick = invertChkBox;
    lblPreLancamento.onclick = invertChkBox;
    lblLancamento.onclick = invertChkBox;
    lblAtivo.onclick = invertChkBox;
    lblPromocao.onclick = invertChkBox;
    lblPontaEstoque.onclick = invertChkBox;
    
    // CheckBox que entram checados
    chkPreLancamento.checked = true;
    chkLancamento.checked = true;
    chkAtivo.checked = true;
    chkPromocao.checked = true;
    chkPontaEstoque.checked = true;

    txtPedidoReferencia.onkeydown = execRef_OnKey;
    txtPedidoReferencia.onkeypress = verifyNumericEnterNotLinked;
    txtPedidoReferencia.onkeyup = showHideFlds;
    txtPedidoReferencia.setAttribute('thePrecision', 10, 1);
    txtPedidoReferencia.setAttribute('theScale', 0, 1);
    txtPedidoReferencia.setAttribute('verifyNumPaste', 1);
    txtPedidoReferencia.setAttribute('minMax', new Array(1, 9999999999), 1);
    txtPedidoReferencia.value = '';

    txtNotaFiscalReferencia.onkeydown = execRef_OnKey;
    txtNotaFiscalReferencia.onkeypress = verifyNumericEnterNotLinked;
    txtNotaFiscalReferencia.onkeyup = showHideFlds;
    txtNotaFiscalReferencia.setAttribute('thePrecision', 10, 1);
    txtNotaFiscalReferencia.setAttribute('theScale', 0, 1);
    txtNotaFiscalReferencia.setAttribute('verifyNumPaste', 1);
    txtNotaFiscalReferencia.setAttribute('minMax', new Array(1, 9999999999), 1);
    txtNotaFiscalReferencia.value = '';
    
    txtProdutoID.onkeypress = verifyNumericEnterNotLinked;
    txtProdutoID.onkeydown = execPesq_OnKey;
    txtProdutoID.setAttribute('thePrecision', 10, 1);
    txtProdutoID.setAttribute('theScale', 0, 1);
    txtProdutoID.setAttribute('verifyNumPaste', 1);
    txtProdutoID.setAttribute('minMax', new Array(1, 9999999999), 1);
    txtProdutoID.value = '';
    
    txtPalavraChave.onkeypress = execPesq_OnKey;

    txtValorLimite.onkeypress = verifyNumericEnterNotLinked;
    txtValorLimite.setAttribute('thePrecision', 11, 1);
    txtValorLimite.setAttribute('theScale', 2, 1);
    txtValorLimite.setAttribute('minMax', new Array(0, 999999999.99), 1);
    txtValorLimite.setAttribute('verifyNumPaste', 1);
    txtValorLimite.value = '';
    
    selProduto.onchange = selProdutoChanged;
    selMarca.onchange = selMarcaChanged;


    txtConceitoID.onkeydown = execPesCon_OnKey;
    txtConceitoID.onkeypress = verifyNumericEnterNotLinked;
    txtConceitoID.onkeyup = showHideFlds;
    txtConceitoID.setAttribute('thePrecision', 10, 1);
    txtConceitoID.setAttribute('theScale', 0, 1);
    txtConceitoID.setAttribute('verifyNumPaste', 1);
    txtConceitoID.setAttribute('minMax', new Array(1, 9999999999), 1);
    txtConceitoID.value = '';

    txtQuantidade.onkeydown = execPesCon_OnKey;
    txtQuantidade.onkeypress = verifyNumericEnterNotLinked;
    txtQuantidade.onkeyup = showHideFlds;
    txtQuantidade.setAttribute('thePrecision', 10, 1);
    txtQuantidade.setAttribute('theScale', 0, 1);
    txtQuantidade.setAttribute('verifyNumPaste', 1);
    txtQuantidade.setAttribute('minMax', new Array(1, 9999999999), 1);
    txtQuantidade.value = '';

}

/********************************************************************
Pesquisa se Enter em campos com esta key
********************************************************************/
function execPesq_OnKey()
{
    txtPedidoReferencia.value = trimStr(txtPedidoReferencia.value);
    txtNotaFiscalReferencia.value = trimStr(txtNotaFiscalReferencia.value);
    txtProdutoID.value = trimStr(txtProdutoID.value);
    
    if ((this.id == 'txtProdutoID') && (event.keyCode != 13))
    {
        fg2.Rows = 1;
    }

    if ( event.keyCode == 13 )
        btnListar_onclick();
}


function execRef_OnKey() {
    txtPedidoReferencia.value = trimStr(txtPedidoReferencia.value);
    txtNotaFiscalReferencia.value = trimStr(txtNotaFiscalReferencia.value);


    if ((this.id == 'txtPedidoReferencia' || this.id == 'txtNotaFiscalReferencia') && (event.keyCode != 13)) {
        fg.Rows = 1;
    }

    if (event.keyCode == 13)
        btnFindReferencia_onclick();

}

function execPesCon_OnKey() {
    txtConceitoID.value = trimStr(txtConceitoID.value);
    txtQuantidade.value = trimStr(txtQuantidade.value);


    if ((this.id == 'txtConceitoID' || this.id == 'txtQuantidade') && (event.keyCode != 13)) 
        fg3.Rows = 1;
    

    if (event.keyCode == 13)
        btnFindPesCon_onclick();

}

function ehImportacao()
{
    txtPedidoReferencia.value = trimStr(txtPedidoReferencia.value);

    if (txtPedidoReferencia.value.length > 0) {    
        setConnection(dsoehImportacao);
        dsoehImportacao.SQL = 'SELECT ISNULL(dbo.fn_Pedido_Importacao(' + txtPedidoReferencia.value + '), 0) AS ehImportacao ';
        dsoehImportacao.ondatasetcomplete = ehImportacao_DSC;
        dsoehImportacao.Refresh();
    }
    else
        glb_timerInterval = window.setInterval('fillgridData()', 10, 'JavaScript');
}

function ehImportacao_DSC()
{

    if (dsoehImportacao.recordset['ehImportacao'].value != 0)
        glb_nReferencia = 1;

    glb_timerInterval = window.setInterval('fillgridData()', 10, 'JavaScript');
}

/********************************************************************
Mostra esconde campos
********************************************************************/
function showHideFlds() 
{
   
   txtPedidoReferencia.value = trimStr(txtPedidoReferencia.value);
   glb_nReferencia = 1;

   if (glb_nRowTrocaReferencia > 0)
    {
        lblProdutoID.style.visibility = 'inherit';
        txtProdutoID.style.visibility = 'inherit';
        lblProduto.style.visibility = 'inherit';
        selProduto.style.visibility = 'inherit';
        lblMarca.style.visibility = 'inherit';
        selMarca.style.visibility = 'inherit';
        lblCaracteristica.style.visibility = 'inherit';
        selCaracteristica.style.visibility = 'inherit';
        lblPalavraChave.style.visibility = 'inherit';
        txtPalavraChave.style.visibility = 'inherit';
        lblValorLimite.style.visibility = 'inherit';
        txtValorLimite.style.visibility = 'inherit';
        lblHomologacao.style.visibility = 'inherit';
        chkHomologacao.style.visibility = 'inherit';
        lblPreLancamento.style.visibility = 'inherit';
        chkPreLancamento.style.visibility = 'inherit';
        lblLancamento.style.visibility = 'inherit';
        chkLancamento.style.visibility = 'inherit';
        lblAtivo.style.visibility = 'inherit';
        chkAtivo.style.visibility = 'inherit';
        lblPromocao.style.visibility = 'inherit';
        chkPromocao.style.visibility = 'inherit';
        lblPontaEstoque.style.visibility = 'inherit';
        chkPontaEstoque.style.visibility = 'inherit';
        lblChave.style.visibility = 'inherit';
        selChave.style.visibility = 'inherit';
        lblFiltro.style.visibility = 'inherit';
        txtFiltro.style.visibility = 'inherit';
        btnListar.style.visibility = 'inherit';

        fg2.style.visibility = 'inherit';
        divFG2.style.visibility = 'inherit';
           
    }
    else
    {
        lblProdutoID.style.visibility = 'hidden';
        txtProdutoID.style.visibility = 'hidden';
        lblProduto.style.visibility = 'hidden';
        selProduto.style.visibility = 'hidden';
        lblMarca.style.visibility = 'hidden';
        selMarca.style.visibility = 'hidden';
        lblCaracteristica.style.visibility = 'hidden';
        selCaracteristica.style.visibility = 'hidden';
        lblPalavraChave.style.visibility = 'hidden';
        txtPalavraChave.style.visibility = 'hidden';
        lblValorLimite.style.visibility = 'hidden';
        txtValorLimite.style.visibility = 'hidden';
        lblHomologacao.style.visibility = 'hidden';
        chkHomologacao.style.visibility = 'hidden';
        lblPreLancamento.style.visibility = 'hidden';
        chkPreLancamento.style.visibility = 'hidden';
        lblLancamento.style.visibility = 'hidden';
        chkLancamento.style.visibility = 'hidden';
        lblAtivo.style.visibility = 'hidden';
        chkAtivo.style.visibility = 'hidden';
        lblPromocao.style.visibility = 'hidden';
        chkPromocao.style.visibility = 'hidden';
        lblPontaEstoque.style.visibility = 'hidden';
        chkPontaEstoque.style.visibility = 'hidden';
        lblChave.style.visibility = 'hidden';
        selChave.style.visibility = 'hidden';
        lblFiltro.style.visibility = 'hidden';
        txtFiltro.style.visibility = 'hidden';
        btnListar.style.visibility = 'hidden';
        fg2.style.visibility = 'hidden';
        divFG2.style.visibility = 'hidden';

    }
    
    if (glb_bEhRetorno == false && glb_nTipoPedidoID == 602) 
    {
        txtConceitoID.style.visibility = 'inherit';
        lblConceitoID.style.visibility = 'inherit';
        txtQuantidade.style.visibility = 'inherit';
        lblQuantidade.style.visibility = 'inherit';
        btnFindPesCon.style.visibility = 'inherit';
        fg3.style.visibility = 'inherit';
        divFG3.style.visibility = 'inherit';
    }
    else {
        txtConceitoID.style.visibility = 'hidden';
        lblConceitoID.style.visibility = 'hidden';
        lblQuantidade.style.visibility = 'hidden';
        txtQuantidade.style.visibility = 'hidden';
        btnFindPesCon.style.visibility = 'hidden';
        fg3.style.visibility = 'hidden';
        divFG3.style.visibility = 'hidden';
        
        
    }
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();
    
    return true;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    // 1. O usuario clicou o botao OK
    //sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , null );
	
	var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'Resultado' + '\'' + '].value');
	var bTemNF = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'TemNF' + '\'' + '].value');
    
    var i, j, nNumeroItens;
    var aDataToSave = new Array();
    
    nNumeroItens = glb_nQuantidadeItens;
    j = 0;
    var nVariacaoItem = 0;
    var nValorInterno = 0;
    var nValorRevenda = 0;
    var nValorUnitario = 0;
    var bTemPPB = false;

    var nColProdutoID = getColIndexByColKey(fg, 'ProdutoID*');
    var nColProdutoID2 = getColIndexByColKey(fg, 'ProdutoID2*'); 
    var nColQuantidade= getColIndexByColKey(fg, 'Quantidade');
    //var nColImposto2Base = getColIndexByColKey(fg, 'Imposto2Base');
    var nColValorTabela = getColIndexByColKey(fg, 'ValorTabela');
    var nColMoedaID = getColIndexByColKey(fg, 'MoedaID');
    var nColTaxaMoeda = getColIndexByColKey(fg, 'TaxaMoeda');
    var nColGrupoImpostoID = getColIndexByColKey(fg, 'GrupoImpostoID');
    var nColPediItemReferenciaID = getColIndexByColKey(fg, 'PedItemID*'); 
    var nColFinalidadeID = getColIndexByColKey(fg, 'FinalidadeID');
    var nColTemPPB = getColIndexByColKey(fg, 'TemPPB');
    var nColPedItemEncomendaID = getColIndexByColKey(fg, 'PedItemEncomendaID');
    var nColEhEncomenda = getColIndexByColKey(fg, 'EhEncomenda');
    var nColValorInterno = getColIndexByColKey(fg, colunasReadOnly('PrecoInterno'));
    var nColValorRevenda = getColIndexByColKey(fg, colunasReadOnly('PrecoRevenda'));
    var nColValorUnitario = getColIndexByColKey(fg, colunasReadOnly('PrecoUnitario'));

    var nProdutoID;
    
    
    // Verifica coluna de quantidades do grid
    for ( i=2; i<fg.Rows; i++)
    {   
        if (fg.ValueMatrix(i, nColQuantidade) == 0) 
        {
            fg.TextMatrix(i, nColQuantidade) = '';
            continue;
        }
        else {
			if ( (fg.TextMatrix(i, nColTemPPB) == 1) && (bResultado) && (bTemNF) && (glb_nTipoPedidoID=='602'))
			{
				nNumeroItens+=3;
				bTemPPB = true;
			}
			else
				nNumeroItens++;

            if (fg.ValueMatrix(i, nColValorTabela) != 0)
            {
                nValorInterno = fg.ValueMatrix(i, nColValorInterno);
                nValorRevenda = fg.ValueMatrix(i, nColValorRevenda);
                nValorUnitario = fg.ValueMatrix(i, nColValorUnitario);

                nVariacaoItem = ((nValorUnitario / roundNumber(fg.ValueMatrix(i, nColValorTabela) *
                                                               (1 + (glb_nVariacao / 100)), 2)) - 1) * 100;

                if (nVariacaoItem > 999.99)
                    nVariacaoItem = 999.99;
                else if (nVariacaoItem < -999.99)
                    nVariacaoItem = -999.99;                
            }
            else
            {
                nVariacaoItem = 0;
                nValorInterno = 0;
                nValorRevenda = 0;
                nValorUnitario = 0;
            }
            
            if (fg.ValueMatrix(i, nColProdutoID) != fg.ValueMatrix(i, nColProdutoID2))
                nProdutoID = fg.ValueMatrix(i, nColProdutoID2);
            else
                nProdutoID = fg.ValueMatrix(i, nColProdutoID);
            
            
            aDataToSave[j] = new Array(nProdutoID,
                                       fg.ValueMatrix(i, nColFinalidadeID), 
                                       fg.ValueMatrix(i, nColGrupoImpostoID),
                                       fg.ValueMatrix(i, nColQuantidade),
                                       fg.ValueMatrix(i, nColValorTabela),
                                       nVariacaoItem,
                                       nValorInterno,
                                       nValorRevenda,
                                       nValorUnitario,
                                       fg.ValueMatrix(i, nColPediItemReferenciaID),
                                       fg.ValueMatrix(i, nColMoedaID),
                                       fg.ValueMatrix(i, nColTaxaMoeda),
                                       fg.ValueMatrix(i, nColPedItemEncomendaID),
                                       fg.ValueMatrix(i, nColEhEncomenda),
                                       0); //Passa-se 0 na posi��o 14 desse array porque n�o existe servi�o vindo de um pedido de refer�ncia.
            j++;            
        }

        // Se nao for pedido de entrada
		if (glb_nTipoPedidoID != 601)
		{
			if ( nNumeroItens > glb_nNumeroMaxItens )
			{
				if (bTemPPB)
				{
					if ( window.top.overflyGen.Alert('Excedido o limite m�ximo de �tens neste pedido (PPB)!') == 0 )
						return null;
				}
				else
				{
					if ( window.top.overflyGen.Alert('Excedido o limite m�ximo de �tens neste pedido!') == 0 )
						return null;
				}

				lockControlsInModalWin(false);
				window.focus();
				fg.focus();
				return null;
			}
        }
    }

    if ( nNumeroItens == glb_nQuantidadeItens )
    {
        if ( window.top.overflyGen.Alert('Nenhum �tem a ser gravado!') == 0 )
            return null;
        lockControlsInModalWin(false);
        window.focus();
        fg.focus();
        return null;
    }
    
    
    if (glb_nFamiliaID == 2830)
    {
        var sDescricaoComp = null;
        Descricao();
        sDescricaoComp = 'NF.: ' + dsoDescricao.recordset['NotaFiscal'].value + ' Data: ' + dsoDescricao.recordset['dtNotaFiscal'].value; 
        
        Gravacao(txtPalavraChave.value);
        dsoGravacao.recordset['Descricao'].value = sDescricaoComp;
        dsoGravacao.SubmitChanges();
        dsoGravacao.Refresh();
        aDataToSave[0][7]= dsoDescricao.recordset['PedReferenciaID'].value;
    }
    

    saveItens(aDataToSave, aDataToSave.length, aDataToSave[0].length);
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modalreferenciaBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    if (txtPalavraChave.currentStyle.visibility != 'hidden')
        if (txtPalavraChave.disabled == false)
            txtPalavraChave.focus();
}

/********************************************************************
Usuario clicou o botao listar
********************************************************************/
function btnListar_onclick()
{
    var stopPesq = false;
    
    // trima todos os campos
    txtPedidoReferencia.value = trimStr(txtPedidoReferencia.value);
    txtNotaFiscalReferencia.value = trimStr(txtNotaFiscalReferencia.value);
    txtProdutoID.value = trimStr(txtProdutoID.value);
    txtValorLimite.value = trimStr(txtValorLimite.value);
    txtFiltro.value = trimStr(txtFiltro.value);    

    stopPesq = ( (trimStr(txtProdutoID.value) == '') &&
                 ((selProduto.selectedIndex <= 0) ||
                 (selMarca.selectedIndex <= 0)));

    fg2.Rows = 1;

    if ( stopPesq )
    {
        if ( window.top.overflyGen.Alert('Preencha ao menos o ID ou Produto e Marca, para listar.') == 0 )
            return null;
                    
        window.focus();
                    
        return true;
    }    
    
    lockControlsInModalWin(true);
    
    // conforme conteudo de txtPedidoReferencia ou txtNotaFiscalReferencia
    // considera ou nao os valores digitados nos campos
    // txtPalavraChave
    // txtValorLimite
    // txtFiltro.value
    
    ehImportacao();

    //    glb_timerInterval = window.setInterval ( 'fillgridData()', 10 , 'JavaScript');
}

function fillgridData()
{
    if ( glb_timerInterval != null )
    {
        window.clearInterval(glb_timerInterval);
        glb_timerInterval = null;
    }

    lockControlsInModalWin(true);
    
    var sFiltroPedido = '';
    var sFiltroNotaFiscal = '';
	var nEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'getCurrEmpresaData()');

	var nIdiomaDeID = nEmpresaData[7];
	var nIdiomaParaID = nEmpresaData[8];
        
    var strSQL = '';
    var strSQLSelect = '';
    var strSQLSelect2 = '';
    var strSQLFrom = '';
    var strSQLWhere = '';
    var nEstadoID;
    var sFiltroFornecedor = '';
    var sFiltroEstoque = '';
    var sTop = '';
    var sImposto2Base = '';
	
	var bTemNF = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'TemNF' + '\'' + '].value');
	var nTemNF = (bTemNF ? 1 : 0);
	var nMetodoCustoID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'MetodoCustoID' + '\'' + '].value'), 10);
	
	var nEstoqueID = 356;

	if (nMetodoCustoID != 371)
		nEstoqueID = 361;


    glb_PrecoEhDaReferencia = false;
    
    var nProdutoID = txtProdutoID.value;
    var nConceitoConcretoID;
    var nMarcaID;
    var sCaracteristica;
    var nValorLimite = txtValorLimite.value;
    var sPalavraChave = txtPalavraChave.value;

    var sFiltro = '';
    var sFiltroFROM = '';
    var sFiltroSELECTReferencia = '';
    var sFiltroFROMReferencia = '';
    var sFiltroReferencia = '';
    var sChave = selChave.value;
    
    if(sChave == 'ProdutoID')
	{
		Familia(sPalavraChave);
		if ( !(dsoFamilia.recordset.BOF && dsoFamilia.recordset.EOF) )
		    glb_nFamiliaID = dsoFamilia.recordset['ProdutoID'].value;    
	}        
			
    if  ( nProdutoID != '' )
    {
        sTop = ' TOP 50 ';
        nProdutoID = parseInt(nProdutoID ,10);
        sFiltro += ' AND ProdutosEmpresa.ObjetoID>= ' + nProdutoID + ' ';
    }                

    if (selProduto.options.length == 0)
        nConceitoConcretoID = 0;
    else
        nConceitoConcretoID = selProduto.value;

    if (selMarca.options.length == 0)
        nMarcaID = 0;
    else
        nMarcaID = selMarca.value;

    if (selCaracteristica.options.length == 0)
        sCaracteristica = 0;
    else
        sCaracteristica = selCaracteristica.options[selCaracteristica.selectedIndex].innerText;

    if ( nConceitoConcretoID != 0 )
    {
        sFiltro = ' AND Produtos.ProdutoID = ' + nConceitoConcretoID;
    }        

    if ( nMarcaID != 0 )
    {
        sFiltro += ' AND Produtos.MarcaID = ' + nMarcaID; 
    }

    if ( sCaracteristica != 0)
    {
        sFiltro += ' AND Produtos.ConceitoID=Caracteristicas.ProdutoID AND dbo.fn_Produto_CaracteristicaOrdem(Caracteristicas.ProdutoID, Caracteristicas.CaracteristicaID)=1 AND ' +
                   'Caracteristicas.Valor LIKE ' + '\'' + '%'  + sCaracteristica + '%' + '\'' + ' '; 
        
        sFiltroFROM = ', Conceitos_Caracteristicas Caracteristicas WITH(NOLOCK) ';
    }

    if  ( nValorLimite != '' )
    {
        nValorLimite = parseInt(nValorLimite ,10);
        sFiltro += ' AND (CONVERT(NUMERIC(11,2), dbo.fn_Pedido_Item(' + glb_nPedidoID + ', Produtos.ConceitoID, GETDATE(), NULL, 1)* ' + (1+(glb_nVariacao/100)) + '))<= ' + nValorLimite;
    }                
    if  ( sPalavraChave != '' )
    {
        sFiltroFROM += ', Conceitos Marcas  WITH(NOLOCK), Conceitos Familias  WITH(NOLOCK), ' +
			'RelacoesConceitos RelConceitos2  WITH(NOLOCK), Conceitos Conceitos2  WITH(NOLOCK), RelacoesConceitos RelConceitos1  WITH(NOLOCK), Conceitos Conceitos1  WITH(NOLOCK) ';
        sFiltro += ' AND Produtos.MarcaID = Marcas.ConceitoID AND Produtos.ProdutoID = Familias.ConceitoID AND ' +
                   'Familias.EstadoID = 2 AND Familias.ConceitoID = RelConceitos2.SujeitoID AND ' +
				   'RelConceitos2.TipoRelacaoID = 41 AND RelConceitos2.EstadoID = 2 AND ' +
				   'RelConceitos2.ObjetoID = Conceitos2.ConceitoID AND Conceitos2.EstadoID = 2 AND ' +
				   'Conceitos2.ConceitoID = RelConceitos1.SujeitoID AND RelConceitos1.TipoRelacaoID = 41 AND ' +
				   'RelConceitos1.EstadoID = 2 AND RelConceitos1.ObjetoID = Conceitos1.ConceitoID AND ' + 
				   'Conceitos1.EstadoID = 2 AND ' +
                   '((Produtos.ConceitoID LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' +
                   ' (Produtos.Conceito LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' +
                   ' (dbo.fn_Tradutor(Familias.Conceito, ' + nIdiomaDeID + ', ' + nIdiomaParaID + ', NULL) LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' + 
                   ' (dbo.fn_Tradutor(Conceitos1.Conceito, ' + nIdiomaDeID + ', ' + nIdiomaParaID + ', NULL) LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' +
                   ' (dbo.fn_Tradutor(Conceitos2.Conceito, ' + nIdiomaDeID + ', ' + nIdiomaParaID + ', NULL) LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' +
                   ' (Marcas.Conceito LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' + 
                   ' (Produtos.Modelo LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' + 
                   ' (Produtos.Descricao LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' + 
                   ' (Produtos.PartNumber LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' )) '; 
    }                
    if  ( txtFiltro.value != '' )
    {
        sFiltro += ' AND (' + txtFiltro.value + ') ';
    }                

    // Estados
    var sFiltroEstados = ' ';    
    var bFirst = true;

    // loop nos checkbox de estados para montar a string de filtro
    for ( i=0; i<(divPesquisa.children.length); i++ )
    {
        elem = divPesquisa.children[i];
        
        if ( (elem.tagName).toUpperCase() == 'INPUT' )
        {
            if ( ((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked) )
            {
                if (bFirst)
                {
                    bFirst=false;
                    sFiltroEstados = ' AND (ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                }                    
                else 
                {
                    sFiltroEstados += ' OR ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                }
            }
        }
        //Caso seja umas das transacoes de troca, retorna tamb�m os produtos desativos.
        if (glb_nTransacaoID == 522 || glb_nTransacaoID == 526 || glb_nTransacaoID == 511 || glb_nTransacaoID == 512 || glb_nTransacaoID == 831 || glb_nTransacaoID == 835)
        {
            sFiltroEstados += ' OR ProdutosEmpresa.EstadoID=4 ';
        }
    }

    sFiltroEstados += ')';
    
    var sValor = '';
    var sPrecoUnitario = 'NULL';
  
    var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[\'TransacaoID\'].value');
    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[\'EmpresaID\'].value');
    var nMatrizFilial = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[\'MatrizFilial\'].value');
    var bEhCliente = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[\'EhCliente\'].value');
    var bImpostosIncidencia = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ImpostosIncidencia\'].value');
    var bFopagEmpresas = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'FopagEmpresas\'].value');
    
    if((bEhCliente == 'true') || (bEhCliente == '1'))
        bEhCliente = 1;                
    else
        bEhCliente = 0;
    
    if((bImpostosIncidencia == 'true') || (bImpostosIncidencia == '1'))
        bImpostosIncidencia = 1;                
    else
        bImpostosIncidencia = 0;

    //SAS0075-Troca - O valor � sugerido ao usu�rio atraves de uma fn que se baseia na transa��o. SGP 04/04/2014
    sValor = 'dbo.fn_Produto_ValorUnitario(Produtos.ConceitoID, ' + glb_nPedidoID + ', NULL, NULL, NULL, NULL)';

    sImposto2Base = '(CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, -6)))';
    var sPrecoTabela = '(CONVERT(NUMERIC(16,4), dbo.fn_Pedido_Item(' + glb_nPedidoID + ', Produtos.ConceitoID, GETDATE(), NULL, 1)))';
    
    strSQLSelect = 'SELECT DISTINCT ' + sTop +
		'Produtos.Conceito AS Produto, Produtos.ConceitoID AS ProdutoID, ' +
	    'Estados.RecursoAbreviado AS Estado, ' +
    	'(CONVERT(INT, dbo.fn_Pedido_Item(' + glb_nPedidoID + ', Produtos.ConceitoID, GETDATE(), NULL, 4))) AS GrupoImpostoID, ' +
		'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID, ProdutosEmpresa.ObjetoID, ' + nEstoqueID + ', NULL, NULL, NULL, NULL, NULL) AS Estoque, ' +
    	' NULL AS PrecoInterno, NULL AS PrecoRevenda,' + sValor + ' AS PrecoUnitario, ';

	strSQLSelect2 =	'(CONVERT(INT, dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, 2))) AS MoedaID, ' + 
		'(CONVERT(NUMERIC(15,7), dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, 3))) AS TaxaMoeda, ' +
		'(CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, 5))) AS Imposto1, ' +
		'(CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, 6))) AS Imposto2,' +
		sImposto2Base + ' AS Imposto2Base,' +
 	    'ProdutosEmpresa.Observacao AS Observacao, NULL AS PedidoReferencia ' + sFiltroSELECTReferencia + ', ' +
 	    'NULL AS  FinalidadeID,' +
 	    'NULL AS ValorInterno, ' +
 	    'NULL AS ValorRevenda, ' +
 	     sValor + ' AS ValorUnitario, ' +
 	     sValor + ' AS ValorTabela, ' +
 	    'dbo.fn_Produto_PPB(Produtos.ConceitoID, ' + nEmpresaData[4] + ', NULL) AS TemPPB, NULL AS PedItemEncomendaID, NULL AS EhEncomenda, SPACE(0) AS Troca, ' +
 	    'Produtos.PartNumber AS PartNumber ';

	// Adicionado tipo de compra para validar fornecedor no pes/con. 14/11/2013
	if (((glb_nTipoPessoa < 0) || (glb_nTipoPessoa == 334 && glb_nTransacaoID == 111 && bFopagEmpresas.indexOf(escape(glb_nPessoaID)) < 0)) 
	    && (!((sPalavraChave == '17012') || (sPalavraChave == '19222') || (sPalavraChave == '21043'))))
	{
		sFiltroFROM += ',RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK) ';
		sFiltroFornecedor = 'ProdutosEmpresa.RelacaoID=Fornecedores.RelacaoID AND Fornecedores.FornecedorID=' + glb_nPessoaID + ' AND ';
	}
    //Em Pedidos onde h� troca de produto, na saida deve desconsiderar os produtos com estoque zerado. SGP
	if (glb_nTipoPedidoID == 602) 
	{
	    sFiltroEstoque = ' (dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID, ProdutosEmpresa.ObjetoID, ' + nEstoqueID + ', NULL, NULL, NULL, NULL, NULL) > 0) AND ';
	}
    
	strSQLFrom = 'FROM ' +
	    'RelacoesPesCon ProdutosEmpresa  WITH(NOLOCK), Conceitos Produtos  WITH(NOLOCK), Recursos Estados  WITH(NOLOCK) ' + sFiltroFROMReferencia + sFiltroFROM + ' ';

	strSQLWhere = 'WHERE ' +
	    '(ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
	    sFiltroEstoque +
	    sFiltroFornecedor +
	    'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID';
	
    strSQLWhere += ' AND ProdutosEmpresa.EstadoID=Estados.RecursoID ' + sFiltro + ' ' + sFiltroEstados + ' ' + sFiltroReferencia + ' ) ' + 
	    'ORDER BY ' + sChave;

    strSQL = strSQLSelect + strSQLSelect2 + strSQLFrom + strSQLWhere;

    setConnection(dsoPesqProd);

    dsoPesqProd.SQL = strSQLSelect + strSQLSelect2 + strSQLFrom + strSQLWhere;
    dsoPesqProd.ondatasetcomplete = fillgridData_DSC;
    try {
        dsoPesqProd.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro Inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);

        fg2.Rows = 1;
        btnOK.disabled = true;

        if (txtFiltro.disabled == false) {
            window.focus();
            txtFiltro.focus();
        }
        return null;
    }
}

function fillgridData_DSC() {
    
    var i = 0;
    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'Resultado\'].value');

    var aHoldCols = new Array();

    var nArray = [['Produto', 'Produto*', '', '', false, false],
                    ['ID', 'ProdutoID*', '', '', false, true],
                    ['Est', 'Estado*', '', '', false, false],
                    ['Estoque', 'Estoque*', '', '', false, true],
                    ['Valor Int', 'ValorInterno*', '', '', true, false],
                    ['Valor Rev', 'ValorRevenda*', '', '', true, false],
                    ['Valor Unit', 'ValorUnitario*', '', '', true, false],
                    ['Pre�o Int', 'PrecoInterno*', '9999999999.99', '###,###,##0.00', !(bResultado), true],
                    ['Pre�o Rev', 'PrecoRevenda*', '9999999999.99', '###,###,##0.00', !(bResultado), true],
                    ['Pre�o Unit','PrecoUnitario*', '9999999999.99', '###,###,##0.00', false, true],
                    ['ValorTabela', 'ValorTabela*', '', '', true, false],
                    //['I1', 'Imposto1*', '999.99', '##0.00', false, true],
                    //['I2', 'Imposto2*', '999.99', '##0.00', false, true],
                    ['_calc_HoldKey_1', '_calc_HoldKey_1*', '9999999999.99', '###,###,##0.00', true, false],
                    ['PartNumber', 'PartNumber*', '', '', false, true],
                    ['Observa��o', 'Observacao*', '', '', false, false],
                    ['MoedaID', 'MoedaID*', '', '', true, false],
                    ['TaxaMoeda', 'TaxaMoeda*', '', '', true, false],
                    ['GrupoImpostoID', 'GrupoImpostoID*', '', '', true, false],
                    ['PedidoReferencia', 'PedidoReferencia*', '', '', true, false],
                    ['Imposto2Base', 'Imposto2Base*', '', '', true, false],
                    ['FinalidadeID', 'FinalidadeID*', '', '', true, false],
                    ['TemPPB', 'TemPPB*', '', '', true, false],
                    ['PedItemEncomendaID', 'PedItemEncomendaID*', '', '', true, false],
                    ['EhEncomenda', 'EhEncomenda*', '', '', true, false],
                    ['Troca', colunasReadOnly('Troca'), '', '', false, true]];

    headerGrid(fg2, aReturnFieldsToGrid(nArray, 0), aReturnFieldsToGrid(nArray, 4));

    fillGridMask(fg2, dsoPesqProd, aReturnFieldsToGrid(nArray, 1), aReturnFieldsToGrid(nArray, 2), aReturnFieldsToGrid(nArray, 3));

    var nColPrecoInterno = getColIndexByColKey(fg2, 'PrecoInterno*');
    var nColPrecoRevenda = getColIndexByColKey(fg2, 'PrecoRevenda*');
    var nColPrecoUnitario = getColIndexByColKey(fg2,'PrecoUnitario*');
    var nColValor = getColIndexByColKey(fg2, 'Valor*');
    var nColValorInterno = getColIndexByColKey(fg2, 'ValorInterno*');
    var nColValorRevenda = getColIndexByColKey(fg2, 'ValorRevenda*');
    var nColValorUnitario = getColIndexByColKey(fg2, 'ValorUnitario*');
    var nColTroca = getColIndexByColKey(fg2, colunasReadOnly('Troca'));

    //adiciona linha de totais no grid.
    //gridHasTotalLine(fg2, 'Totais', 0xC0C0C0, null, true, [[nColQuantidade, '#######', 'S'], [nColCalcValorTotal, '##,###,###,###.00', 'S']]);
    fg2.Redraw = 0;
    fg2.Editable = false;
    //startGridInterface(fg2);
    fg2.FrozenCols = 0;
    fg2.BorderStyle = 1;
    fg2.ColDataType(nColTroca) = 11;
    
    //Preenche colular valor total e linha de totais.
    //setTotaisGrid();

    //alinha colunas a direita de acordo com a defini��o do nArray.
    alignColsInGrid(fg2, aReturnFieldsToGrid(nArray, 5));

    //Preenche pre�o interno e revenda igual ao preco unitario quando os mesmo vierem zerados.
    if (bResultado) {
        for (i = 2; i < fg2.Rows; i++) {
            if (fg2.ValueMatrix(i, nColPrecoRevenda) == 0)
                fg2.TextMatrix(i, nColPrecoRevenda) = fg2.ValueMatrix(i, nColPrecoUnitario);

            if (fg2.ValueMatrix(i, nColPrecoInterno) == 0)
                fg2.TextMatrix(i, nColPrecoInterno) = fg2.ValueMatrix(i, nColPrecoRevenda);

            if (fg2.ValueMatrix(i, nColValorRevenda) == 0)
                fg2.TextMatrix(i, nColValorRevenda) = fg2.ValueMatrix(i, nColValorUnitario);

            if (fg2.ValueMatrix(i, nColValorInterno) == 0)
                fg2.TextMatrix(i, nColValorInterno) = fg2.ValueMatrix(i, nColValorRevenda);
        }
    }

    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0, fg2.Cols - 1);
    //fg.ColWidth(5) = FONT_WIDTH * 11 * 16;

    fg2.FrozenCols = 2;

    fg2.Redraw = 2;

    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if (fg2.Rows > 1)
        fg2.Col = nColTroca;

    if (fg2.Rows > 1)
        fg2.Editable = true;

    lockControlsInModalWin(false);

    // destrava botao OK se tem linhas no grid
    if (fg2.Rows > 1)
        btnOK.disabled = false;
    else
        btnOK.disabled = true;

    // se tem linhas no grid, coloca foco no grid, caso contrario
    // coloca foco no txtPalavraChave
    if (fg2.Rows > 1)
        fg2.focus();
    else {
        if (txtPalavraChave.currentStyle.visibility != 'hidden')
            if (!txtPalavraChave.disabled)
            txtPalavraChave.focus();
        else {
            if (!txtPedidoReferencia.disabled)
                txtPedidoReferencia.focus();
        }
    }
    
    //desmarca os caras
    if (glb_nRowTrocaPesquisa > 0) {
        
        btnTrocar.style.visibility = 'hidden';
        glb_nRowTrocaPesquisa = 0;
    }
    showHideFlds();
    
}
function saveItens(aData, aDataLen, aDataElemLen)
{
	var strPars = '';
	var nBytesAcum = 0;
	var nDataLen = 0;
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
	var nLenExt = 0;                                  
	
	lockControlsInModalWin(true);
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;

    lockControlsInModalWin(true);

    for (i=0; i<aDataLen; i++)
    {
		nBytesAcum=strPars.length;		
		
		if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
		{
			nBytesAcum = 0;
			if (strPars != '')
			{
				glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + 
					'&nDataLen=' + escape(nLenExt);
				strPars = '';
				nLenExt = 0;
				nDataLen = 0;
			}

			strPars = '?';
			strPars += 'nPedidoID=' + escape(glb_nPedidoID);
			strPars += '&nEstadoID=' + escape(nEstadoID);
			strPars += '&nVariacao=' + escape(glb_nVariacao);
			strPars += '&nDataElemLen=' + escape(aDataElemLen);
		}
		nLenExt++;
		nDataLen++;

		for (j = 0; j < aDataElemLen; j++)
		    strPars += '&aData= ' + i.toString() + '/' + escape(aData[i][j]);
	}

	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nLenExt);

	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGen03.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/saveitemspedido.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGen03.ondatasetcomplete = sendDataToServer_DSC;
			dsoGen03.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_callFillGridTimerInt = window.setInterval('saveItens_DSC()', 10, 'JavaScript');
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		return null;
	}
}

function sendDataToServer_DSC()
{
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

function saveItens_DSC()
{

    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade');
    
	if (glb_callFillGridTimerInt != null)
	{
		window.clearInterval(glb_callFillGridTimerInt);
		glb_callFillGridTimerInt = null;
	}

    var i;
    
    if ( !(dsoGen03.recordset.BOF && dsoGen03.recordset.EOF) )
    {
        // Outro usuario mudou o estadoID do pedido para diferente de cotacao
        if (dsoGen03.recordset['fldErrorNumber'].value > 50000)
        {
            if ( window.top.overflyGen.Alert (dsoGen03.recordset['fldErrorText'].value) == 0 )
                return null;
                
            lockControlsInModalWin(false);       
            window.focus();
            fg.focus();    
            return null;
        }
    }
    
    // pedido foi alterado
    glb_pedidoAlterado = true;
    
    for ( i=2; i<fg.Rows; i++ )
    {
        if (!((isNaN(parseFloat(fg.TextMatrix(i, nColQuantidade)))) ||
                (parseFloat(fg.TextMatrix(i, nColQuantidade)) == 0)))
            glb_nQuantidadeItens++;

        fg.TextMatrix(i, nColQuantidade) = '';
    }        

	setTotaisGrid(0);
    lockControlsInModalWin(false);       
    window.focus();
    fg.focus();
}

// Funcao de carregamento de combos
function fillCmbsData()
{
    var strSQL;
    var strSQLSelect1 = '';
    var strSQLSelect2 = '';
    var sFiltroFROM = '';
    var sFiltroFornecedor = '';
    var nEstadoID;

    var nEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'getCurrEmpresaData()');
	var nIdiomaDeID = nEmpresaData[7];
	var nIdiomaParaID = nEmpresaData[8];
    
	if (glb_nTipoPessoa < 0)
	{
		sFiltroFROM = ',RelacoesPesCon_Fornecedores Fornecedores ';
		nEstadoID = 11;
		sFiltroFornecedor = 'ProdutosEmpresa.RelacaoID=Fornecedores.RelacaoID AND Fornecedores.FornecedorID=' + glb_nPessoaID + ' AND ';
	}
	else
		nEstadoID = 12;

	if ((!glb_Produto)&&(!glb_Marca))
	{
		strSQLSelect1 = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +   
			'UNION ALL ' +
				'SELECT DISTINCT 0 AS Indice, Familias.ConceitoID AS fldID, ' +
					'dbo.fn_Tradutor(Familias.Conceito, ' + nIdiomaDeID + ', ' + nIdiomaParaID + ', NULL) AS fldName ' +
					'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK) ' + sFiltroFROM +
					'WHERE ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
						'(ProdutosEmpresa.EstadoID=2 OR ProdutosEmpresa.EstadoID BETWEEN ' + nEstadoID + ' AND 15) AND ' + sFiltroFornecedor +
						'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND Produtos.EstadoID=2 AND ' +
						'Produtos.ProdutoID=Familias.ConceitoID ';

		strSQLSelect2 =	'UNION ALL ' +
				'SELECT 1 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +   
			'UNION ALL ' + 
				'SELECT DISTINCT 1 AS Indice, Marcas.ConceitoID AS fldID, Marcas.Conceito AS fldName ' +
					'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK) ' + sFiltroFROM + 
					'WHERE ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
						'(ProdutosEmpresa.EstadoID=2 OR ProdutosEmpresa.EstadoID BETWEEN ' + nEstadoID + ' AND 15) AND ' + sFiltroFornecedor +
						'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND Produtos.EstadoID=2 AND ' +
						'Produtos.MarcaID=Marcas.ConceitoID ' +
			'ORDER BY Indice, fldName';
	}
    else if (glb_Produto)
    {
        strSQLSelect1 = 'SELECT 1 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +   
	        'UNION ALL ' +
				'SELECT DISTINCT 1 AS Indice, Marcas.ConceitoID AS fldID, Marcas.Conceito AS fldName ' +
					'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK) ' + sFiltroFROM + 
					'WHERE ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
						'(ProdutosEmpresa.EstadoID=2 OR ProdutosEmpresa.EstadoID BETWEEN ' + nEstadoID + ' AND 15) AND ' + sFiltroFornecedor +
						'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND Produtos.EstadoID=2 AND ' +
						'Produtos.MarcaID=Marcas.ConceitoID AND Produtos.ProdutoID = ' + selProduto.value + ' ';

		strSQLSelect2 =	'UNION ALL ' +
				'SELECT 2 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +
		    'UNION ALL ' +
		        'SELECT DISTINCT 2 AS Indice, Caracteristicas.CaracteristicaID AS fldID, Caracteristicas.Valor AS fldName ' +
					'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos_Caracteristicas Caracteristicas WITH(NOLOCK) ' + sFiltroFROM +
					'WHERE ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
						'(ProdutosEmpresa.EstadoID=2 OR ProdutosEmpresa.EstadoID BETWEEN ' + nEstadoID + ' AND 15) AND ' + sFiltroFornecedor +
						'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND Produtos.EstadoID=2 AND ' +
						'Produtos.ConceitoID=Caracteristicas.ProdutoID AND dbo.fn_Produto_CaracteristicaOrdem(Caracteristicas.ProdutoID, Caracteristicas.CaracteristicaID) = 1 AND Produtos.ProdutoID = ' + selProduto.value + ' ' +
		    'ORDER BY Indice, fldName';

		strSQL = strSQLSelect1 + strSQLSelect2;
    }
	else if (glb_Marca)
	{
        // So os Produtos
        if (selProduto.value == 0)
        {
			strSQLSelect1 = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +   
				'UNION ALL ' +
					'SELECT DISTINCT 0 AS Indice, Familias.ConceitoID AS fldID, dbo.fn_Tradutor(Familias.Conceito, ' + nIdiomaDeID + ', ' + nIdiomaParaID + ', NULL) AS fldName ' +
						'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK) ' + sFiltroFROM +
						'WHERE ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
							'(ProdutosEmpresa.EstadoID=2 OR ProdutosEmpresa.EstadoID BETWEEN ' + nEstadoID + ' AND 15) AND ' + sFiltroFornecedor +
							'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND Produtos.EstadoID=2 AND ' +
							'Produtos.ProdutoID=Familias.ConceitoID AND Produtos.MarcaID = ' + selMarca.value + ' ' +
                'ORDER BY Indice, fldName';
        }
        //So as caracteristicas
        else
        {
			strSQLSelect1 =	'SELECT 2 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +   
			    'UNION ALL ' +
			        'SELECT DISTINCT 2 AS Indice, Caracteristicas.CaracteristicaID AS fldID, Caracteristicas.Valor AS fldName ' +
						'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos_Caracteristicas Caracteristicas WITH(NOLOCK) ' + sFiltroFROM + 
						'WHERE ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
							'(ProdutosEmpresa.EstadoID=2 OR ProdutosEmpresa.EstadoID BETWEEN ' + nEstadoID + ' AND 15) AND ' + sFiltroFornecedor +
							'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND Produtos.EstadoID=2 AND Produtos.MarcaID = ' + selMarca.value + ' AND ' +
							'Produtos.ConceitoID=Caracteristicas.ProdutoID AND dbo.fn_Produto_CaracteristicaOrdem(Caracteristicas.ProdutoID, Caracteristicas.CaracteristicaID) = 1 AND Produtos.ProdutoID = ' + selProduto.value + ' ' +                     
			    'ORDER BY Indice, fldName';
       }
	}
    
	strSQL = strSQLSelect1 + strSQLSelect2;

    setConnection(dsoGen02);
    dsoGen02.SQL = strSQL;
    dsoGen02.ondatasetcomplete = fillCmbsData_DSC;
    dsoGen02.Refresh();
}

//Funcao de retorno da funcao fillCmbsData()
function fillCmbsData_DSC()
{
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selProduto,selMarca,selCaracteristica];

    
    if (glb_Produto)
    {
        clearComboEx(['selMarca','selCaracteristica']);
    }
    
    if (glb_Marca)
    {
        if (selProduto.value == 0)
            clearComboEx(['selProduto','selCaracteristica']);
        else
            clearComboEx(['selCaracteristica']);
    }
    
    if ((!glb_Produto)&&(!glb_Marca))
    {
        clearComboEx(['selProduto','selMarca','selCaracteristica']);
    }    
    
    while (! dsoGen02.recordset.EOF )
    {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoGen02.recordset['fldName'].value;
        oOption.value = dsoGen02.recordset['fldID'].value;
        aCmbsDynamics[dsoGen02.recordset['Indice'].value].add(oOption);
        dsoGen02.recordset.MoveNext();
    }

    lockControlsInModalWin(false);

    if ( selProduto.options.length > 0 )
        selProduto.disabled = false;
    else
        selProduto.disabled = true;
        
    if ( selMarca.options.length > 0 )        
        selMarca.disabled = false;
    else
        selMarca.disabled = true;

    if ( selCaracteristica.options.length > 0 )        
        selCaracteristica.disabled = false;
    else
        selCaracteristica.disabled = true;

    // mostra a janela se for o caso
    if ( modalreferenciaBody.currentStyle.visibility == 'hidden' )   
        showModalAfterDataLoad();

}

function selProdutoChanged()
{
    if (this.value == 0)
    {
        selCaracteristica.disabled = true;
        clearComboEx(['selCaracteristica']);
 
        if ( glb_Produto == true )
        {
            glb_Produto = false;
            // combo de marca
            fillCmbsData();
        }
    }
    else
    {
        if ( glb_Produto == true )
        {
            // combo de marca e combo de caracteristica
            fillCmbsData();    
        }
        else
        {
            if ( glb_Marca == true )
            {
                // combo de caracteristica
                fillCmbsData();
            }
            else
            {
                glb_Produto = true;
                // combo de marca e combo de caracteristica
                fillCmbsData();
            }
        }
    }
}

function selMarcaChanged()
{
    if (this.value == 0)
    {
        if ( glb_Marca == true )
        {
            selCaracteristica.disabled = true;
            clearComboEx(['selCaracteristica']);
            
            glb_Marca = false;
            // combo de produto
            fillCmbsData();    
        }
    }
    else
    {
        if ( glb_Marca == true )
        {
            selCaracteristica.disabled = true;
            clearComboEx(['selCaracteristica']);
 
            // combo de produto
            fillCmbsData();    
        }
        else
        {
            if ( glb_Produto == false )
            {
                selCaracteristica.disabled = true;
                clearComboEx(['selCaracteristica']);
 
                glb_Marca = true;
                // combo de produto
                fillCmbsData();
            }
        }
    }
}

function Familia(pConceitoID)
{
           
    //'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value'
    var strSQLFamilia = 'SELECT ProdutoID FROM Conceitos WITH(NOLOCK) WHERE CONVERT(VARCHAR, ConceitoID) = ' + '\'' + pConceitoID  + '\'';
    setConnection(dsoFamilia);
    dsoFamilia.SQL =  strSQLFamilia;
    dsoFamilia.ondatasetcomplete = Familia_DSC;
    dsoFamilia.refresh();

}
       
function Familia_DSC()
{
    return null;
}

function Descricao()
{
    // Aterar a de4scri��o do Conceito
    // NF.: 8787 Data: 01/12/2010
    var strSQL = 'SELECT NF.NotaFiscal, CONVERT(VARCHAR, NF.dtNotaFiscal,103) dtNotaFiscal, NF.PedidoID AS PedReferenciaID ' +
	                      'FROM NotasFiscais NF WITH(NOLOCK) ' +
	                      'INNER JOIN NotasFiscais_Pessoas Pessoa WITH(NOLOCK) ON Pessoa.NotaFiscalID = NF.NotaFiscalID ' +
		                  'WHERE NF.NotaFiscal = ' + txtNotaFiscalReferencia.value +  ' AND NF.EmpresaID = ' + glb_nEmpresaID + ' AND Pessoa.PessoaID = ' + glb_nPessoaID + '' ;
    
    setConnection(dsoDescricao);
    dsoDescricao.SQL = strSQL;
    dsoDescricao.ondatasetcomplete = Descricao_DSC;
    dsoDescricao.refresh();
}

function Descricao_DSC()
{
    return null;
}

function Gravacao(pConceitoID)
{
    var strSQL = 'SELECT * FROM Conceitos WITH(NOLOCK) WHERE ConceitoID = ' + pConceitoID + '';
    setConnection(dsoGravacao);
    dsoGravacao.SQL = strSQL;
    dsoGravacao.ondatasetcomplete = Gravacao_DSC;
    dsoGravacao.refresh();
}

function Gravacao_DSC(){
    return null;
}

function aReturnFieldsToGrid(nArray, nindex) 
{
    var i = 0;
    var aNewArray = new Array();

    for (i = 0; i < nArray.length; i++) 
    {
        if (nindex == 4 || nindex == 5) 
        {
            if (nArray[i][nindex] == true)
                aNewArray[aNewArray.length] = i;
        }
        else
            aNewArray[aNewArray.length] = nArray[i][nindex];
    }

    return aNewArray;
}
/*
function atualizaValorUnitario(nRow) 
{
    var nPrecoInterno = fg.ValueMatrix(nRow, getColIndexByColKey(fg, colunasReadOnly('PrecoInterno')));
    var nPrecoRevenda = fg.ValueMatrix(nRow, getColIndexByColKey(fg, colunasReadOnly('PrecoRevenda')));
    var nPrecoUnitario = fg.ValueMatrix(nRow, getColIndexByColKey(fg, colunasReadOnly('PrecoUnitario')));

    var nFinalidade = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'FinalidadeID'));
    var nProdutoID = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'ProdutoID*'));

    lockControlsInModalWin(true);

    nFinalidade = (nFinalidade == 0 ? 'NULL' : nFinalidade);
    
    setConnection(dsoAtualizaValorUnitario);
    dsoAtualizaValorUnitario.SQL = 'SELECT ' + nRow + ' AS Linha, ' +
                                        'dbo.fn_Pedido_ProdutoValorUnitario (' + glb_nPedidoID + ',' + nProdutoID + ',' + nFinalidade + ',' + nPrecoInterno + ') AS ValorInterno, ' +
                                        'dbo.fn_Pedido_ProdutoValorUnitario (' + glb_nPedidoID + ',' + nProdutoID + ',' + nFinalidade + ',' + nPrecoRevenda + ') AS ValorRevenda, ' +
                                        'dbo.fn_Pedido_ProdutoValorUnitario (' + glb_nPedidoID + ',' + nProdutoID + ',' + nFinalidade + ',' + nPrecoUnitario + ') AS ValorUnitario';
    dsoAtualizaValorUnitario.ondatasetcomplete = atualizaValorUnitario_DSC;
    dsoAtualizaValorUnitario.Refresh();
}

function atualizaValorUnitario_DSC() 
{
    if (!(dsoAtualizaValorUnitario.recordset.BOF && dsoAtualizaValorUnitario.recordset.EOF)) 
    {
        var nRow = dsoAtualizaValorUnitario.recordset['Linha'].value;
        var nValorInterno = dsoAtualizaValorUnitario.recordset['ValorInterno'].value;
        var nValorRevenda = dsoAtualizaValorUnitario.recordset['ValorRevenda'].value;
        var nValorUnitario = dsoAtualizaValorUnitario.recordset['ValorUnitario'].value;

        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ValorInterno')) = nValorInterno;
        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ValorRevenda')) = nValorRevenda;
        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ValorUnitario')) = nValorUnitario;
    }
    
    lockControlsInModalWin(false);
}
*/
function colunasReadOnly(coluna) 
{

    if (coluna == 'Troca')
    {
        if (glb_nPermiteOutroProduto == 1)
            return coluna;
        else
            return coluna + '*';
        
    }
    else if (glb_nTransacaoID == 112)
        return coluna + '*';
    else
        return coluna;
}


/********************************************************************
Usuario clicou o botao listar do pedido Referencia
********************************************************************/
function btnFindReferencia_onclick() 
{
    var stopPesq = false;
    var nPedidoReferenciaID = txtPedidoReferencia.value;
    var nNotaFiscalReferenciaID = txtNotaFiscalReferencia.value; 

    // trima todos os campos
    txtPedidoReferencia.value = trimStr(txtPedidoReferencia.value);
    txtNotaFiscalReferencia.value = trimStr(txtNotaFiscalReferencia.value);
    
    fg.Rows = 1;

    if (txtPedidoReferencia.value == "" && txtNotaFiscalReferencia.value == "") {
        if (window.top.overflyGen.Alert('Preencha ao menos um campo, para listar.') == 0)
            return null;

        window.focus();

        return true;
    }

    lockControlsInModalWin(true);
    
    var sPrecoUnitario = 'NULL';
    var sValorUnitario = '';
    var sEstoque = '';
    var sImposto1 = '';
    var sImposto2 = '';
    var sTemPPB = '';
    var sFiltroPedido = '';
    var sFiltroNotaFiscal = '';
    var nEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var sFiltroFROMForncedor = '';
    var sFiltroFornecedorTroca = '';
    var nIdiomaDeID = nEmpresaData[7];
    var nIdiomaParaID = nEmpresaData[8];

    var strSQL = '';
    var strSQLSelect = '';
    var strSQLSelect2 = '';
    var strSQLFrom = '';
    var strSQLWhere = '';
    var nEstadoID;
    var sFiltroFornecedor = '';
    var sTop = '';
    var nPedidoReferenciaID = txtPedidoReferencia.value;
    var nTransacaoReferenciaID = '';
    var nNotaFiscalReferenciaID = txtNotaFiscalReferencia.value;
    var sImposto2Base = '';

    var bTemNF = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TemNF' + '\'' + '].value');
    var nTemNF = (bTemNF ? 1 : 0);
    var nMetodoCustoID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'MetodoCustoID' + '\'' + '].value'), 10);

    var nEstoqueID = 356;


    glb_PrecoEhDaReferencia = true;
    startGridInterface(fg);


    if (nMetodoCustoID != 371)
        nEstoqueID = 361;

    if (nPedidoReferenciaID != '')
        nPedidoReferenciaID = parseInt(nPedidoReferenciaID, 10);

    if ((nNotaFiscalReferenciaID != '') && (parseInt(nNotaFiscalReferenciaID, 10) > 0))
        sFiltroNotaFiscal = 'INNER JOIN NotasFiscais WITH(NOLOCK) ON Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67 ' +
                                    'AND NotasFiscais.NotaFiscal = \'' + nNotaFiscalReferenciaID + '\' ' +
                                'INNER JOIN NotasFiscais_Pessoas WITH(NOLOCK) ON NotasFiscais_Pessoas.NotaFiscalID = NotasFiscais.NotaFiscalID ' +
                                    'AND NotasFiscais_Pessoas.PessoaID = ' + glb_nPessoaID + ' ';

    if ((nPedidoReferenciaID != '') && (parseInt(nPedidoReferenciaID, 10) > 0))
        sFiltroPedido = 'AND Pedidos.PedidoID = ' + nPedidoReferenciaID + ' ';

    sEstoque = 'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID, ProdutosEmpresa.ObjetoID, ' + nEstoqueID + ', NULL, NULL, NULL, NULL, NULL)';

    sImposto1 = '(CASE WHEN 1 = ' + nTemNF +
                        ' THEN ' +
                            '(SELECT TOP 1 CONVERT(NUMERIC(5,2), ISNULL(SUM(ItensImposto1.Aliquota),0)) ' +
                                'FROM Pedidos_Itens_Impostos ItensImposto1 WITH(NOLOCK) ' +
                                        'INNER JOIN Conceitos Impostos1 WITH(NOLOCK) ON ItensImposto1.ImpostoID=Impostos1.ConceitoID ' +
				                'WHERE Itens.PedItemID=ItensImposto1.PedItemID ' +
				                        'AND Impostos1.IncidePreco = 1 ' +
				                        'AND Impostos1.InclusoPreco = 1 ' +
				                        'AND Impostos1.AlteraAliquota = 1) ELSE 0 END)';

    sImposto2 = '(CASE WHEN 1 = ' + nTemNF +
                        ' THEN ' +
                            '(SELECT TOP 1 CONVERT(NUMERIC(5,2), ISNULL(SUM(ItensImposto2.Aliquota),0)) ' +
				                'FROM Pedidos_Itens_Impostos ItensImposto2 WITH(NOLOCK) ' +
				                        'INNER JOIN Conceitos Impostos2 WITH(NOLOCK) ON ItensImposto2.ImpostoID = Impostos2.ConceitoID ' +
				                'WHERE Itens.PedItemID=ItensImposto2.PedItemID ' +
				                        'AND Impostos2.IncidePreco = 1 ' +
				                        'AND Impostos2.InclusoPreco = 0 ' +
				                        'AND Impostos2.DestacaNota = 1) ELSE 0 END)';

    sImposto2Base = '(CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, -6)))';

    sTemPPB = 'dbo.fn_Produto_PPB(Itens.ProdutoID, ' + nEmpresaData[4] + ', NULL)';

    //SAS0075-Troca - O valor � sugerido ao usu�rio atraves de uma fn que se baseia na transa��o. SGP 04/04/2014
    sValor = 'dbo.fn_Produto_ValorUnitario(Itens.ProdutoID, ' + glb_nPedidoID + ', NULL, NULL, Itens.PedItemID, NULL)';
    sFiltroFROMForncedor = '';
    sFiltroFornecedorTroca = '';
    nTransacaoReferenciaID = '(SELECT TransacaoID FROM Pedidos WITH(NOLOCK) WHERE PedidoID = ' + nPedidoReferenciaID + ')';
    
    if (glb_nTransacaoID == 525) {
        sFiltroFornecedorTroca = ' AND (SELECT DISTINCT PessoaID FROM Pedidos WITH(NOLOCK) WHERE PedidoID = ' + glb_nPedidoID + ') = Pedidos.PessoaID ';
    }
    
    //Para pedidos de troca direto e indireto.
    else if (glb_nFornecedor == 1 && glb_nPermiteOutroProduto == 1) {
    sFiltroFROMForncedor += 'INNER JOIN RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK) ON ProdutosEmpresa.RelacaoID=Fornecedores.RelacaoID ' +
                                'INNER JOIN RelacoesPessoas RelacaoAsstec WITH(NOLOCK) ON (Fornecedores.FornecedorID = RelacaoAsstec.SujeitoID) ';

    sFiltroFornecedorTroca = ' AND ((RelacaoAsstec.TipoRelacaoID = 22) ' +
                             ' OR ((RelacaoAsstec.SujeitoID = ' + glb_nPessoaID + ') AND (Fornecedores.FornecedorID = ' + glb_nPessoaID + ')))';
    }


    strSQLSelect = 'SELECT DISTINCT Itens.PedItemID, ' +
                'Produtos.Conceito AS Produto, ' +
                'Produtos.ConceitoID AS ProdutoID, ' +
                'Estados.RecursoAbreviado AS Estado, ' +
				'Itens.GrupoImpostoID AS GrupoImpostoID, ' +
				sEstoque + ' AS Estoque, ' +
				'Itens.Quantidade AS Quantidade, ' +
				'Itens.ValorTabela AS ValorTabela, ' +
				'Itens.ValorInterno AS PrecoInterno, ' +
				'Itens.ValorRevenda AS PrecoRevenda, ' +
				sValor + ' AS PrecoUnitario, ' +
				'Itens.ValorInterno AS ValorInterno, ' +
				'Itens.ValorRevenda AS ValorRevenda, ' +
				sValor + ' AS ValorUnitario, ' +
				'Itens.MoedaID AS MoedaID, ' +
				'Itens.TaxaMoeda AS TaxaMoeda, ' +
				sImposto1 + ' AS Imposto1, ' +
				sImposto2 + ' AS Imposto2, ' +
				sImposto2Base + ' AS Imposto2Base, ' +
				//'ProdutosEmpresa.Observacao AS Observacao, ' +
				'Pedidos.PedidoID AS PedidoReferencia, ' +
                'Itens.FinalidadeID AS FinalidadeID, ' +
				sTemPPB + ' AS TemPPB, ' +
				'Itens.PedItemEncomendaID AS PedItemEncomendaID, ' +
				'Itens.EhEncomenda AS EhEncomenda, ' +
				'Itens.ProdutoID AS ProdutoID2, ' +
				'Produtos.Conceito AS Produto2, ' +
				'Itens.PedItemID AS PedItemID, ' +
				'SPACE(0) AS Troca ';

    strSQLFrom = 'FROM Pedidos Pedidos WITH(NOLOCK) ' +
                        'INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON Pedidos.PedidoID = Itens.PedidoID ' +
                        'INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON Itens.ProdutoID = ProdutosEmpresa.ObjetoID ' +
                        'INNER JOIN Conceitos Produtos WITH(NOLOCK) ON Itens.ProdutoID = Produtos.ConceitoID ' +
                        'INNER JOIN Recursos Estados WITH(NOLOCK) ON ProdutosEmpresa.EstadoID = Estados.RecursoID ' +
                        sFiltroNotaFiscal +
                        sFiltroFROMForncedor;

    strSQLWhere = 'WHERE ((Pedidos.EmpresaID = ' + glb_nEmpresaID + ') OR ' +
		                        '(dbo.fn_Empresa_Matriz(' + glb_nEmpresaID + ', NULL, NULL) = ' + ' dbo.fn_Empresa_Matriz(' + glb_nPessoaID + ', NULL, NULL))) ' + sFiltroPedido +
		                ' AND ((Pedidos.TipoPedidoID = 601 AND Pedidos.EstadoID >= 26) OR (Pedidos.TipoPedidoID = 602 AND Pedidos.EstadoID >= 29)) ' +
		                ' AND ProdutosEmpresa.SujeitoID = ' + glb_nEmpresaID +
		                ' AND ProdutosEmpresa.TipoRelacaoID=61 ' +
				        ' AND Itens.PedidoID <> ' + glb_nPedidoID + ' ' +
				        ' AND (dbo.fn_Operacoes_Referencia((SELECT TransacaoID FROM Pedidos WITH(NOLOCK) WHERE PedidoID = ' + glb_nPedidoID + '), Pedidos.TransacaoID, 721, NULL) = 1) ' +
				        sFiltroFornecedorTroca +
			        'ORDER BY ProdutoID, Produto';

    setConnection(dsoReferencia);

    dsoReferencia.SQL = strSQLSelect + strSQLSelect2 + strSQLFrom + strSQLWhere;
    dsoReferencia.ondatasetcomplete = fillgridReferencia_DSC;
    try 
    {
        dsoReferencia.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro Inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);

        fg.Rows = 1;
        btnOK.disabled = true;

        if (txtFiltro.disabled == false) {
            window.focus();
            txtFiltro.focus();
        }
        return null;
    }
}


function fillgridReferencia_DSC() 
{
    var i = 0;
    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'Resultado\'].value');

    var aHoldCols = new Array();
    var hideTroca = true;
    
    if (colunasReadOnly('Troca') == 'Troca')
        hideTroca = false;
    
    var nArray = [['Produto', 'Produto*', '', '', false, false],
                       ['ID', 'ProdutoID*', '', '', false, true],
                       ['Est', 'Estado*', '', '', false, false],
                       ['Estoque', 'Estoque*', '', '', false, true],
                       ['Quant', 'Quantidade', '', '', false, true],
                       ['Valor Int', 'ValorInterno', '', '', true, false],
                       ['Valor Rev', 'ValorRevenda', '', '', true, false],
                       ['Valor Unit', 'ValorUnitario', '', '', true, false],
                       ['Pre�o Int', colunasReadOnly('PrecoInterno'), '9999999999.99', '###,###,##0.00', !(bResultado), true],
                       ['Pre�o Rev', colunasReadOnly('PrecoRevenda'), '9999999999.99', '###,###,##0.00', !(bResultado), true],
                       ['Pre�o Unit', colunasReadOnly('PrecoUnitario'), '9999999999.99', '###,###,##0.00', false, true],
                       ['ValorTabela', 'ValorTabela', '', '', true, false],
                       ['Valor Total', '_calc_ValorTotal_10*', '9999999999.99', '###,###,##0.00', false, true],
                       //['I1', 'Imposto1', '999.99', '##0.00', false, true],
                       //['I2', 'Imposto2', '999.99', '##0.00', false, true],
                       ['_calc_HoldKey_1', '_calc_HoldKey_1', '9999999999.99', '###,###,##0.00', true, false],
                       ['MoedaID', 'MoedaID', '', '', true, false],
                       ['TaxaMoeda', 'TaxaMoeda', '', '', true, false],
                       ['GrupoImpostoID', 'GrupoImpostoID', '', '', true, false],
                       ['PedidoReferencia', 'PedidoReferencia', '', '', true, false],
                       ['Imposto2Base', 'Imposto2Base', '', '', true, false],
                       ['FinalidadeID', 'FinalidadeID', '', '', true, false],
                       ['TemPPB', 'TemPPB', '', '', true, false],
                       ['PedItemEncomendaID', 'PedItemEncomendaID', '', '', true, false],
                       ['EhEncomenda', 'EhEncomenda', '', '', true, false],
                       ['ID2', 'ProdutoID2*', '', '', hideTroca, true],
                       ['Produto2', 'Produto2*', '', '', hideTroca, false],
                       ['PedItemID', 'PedItemID*', '', '', true, false],
                       ['Troca', colunasReadOnly('Troca'), '', '', hideTroca, true]];

    headerGrid(fg, aReturnFieldsToGrid(nArray, 0), aReturnFieldsToGrid(nArray, 4));

    fillGridMask(fg, dsoReferencia, aReturnFieldsToGrid(nArray, 1), aReturnFieldsToGrid(nArray, 2), aReturnFieldsToGrid(nArray, 3));

    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade');
    var nColPrecoInterno = getColIndexByColKey(fg, colunasReadOnly('PrecoInterno'));
    var nColPrecoRevenda = getColIndexByColKey(fg, colunasReadOnly('PrecoRevenda'));
    var nColPrecoUnitario = getColIndexByColKey(fg, colunasReadOnly('PrecoUnitario'));
    var nColValor = getColIndexByColKey(fg, 'Valor');
    var nColCalcValorTotal = getColIndexByColKey(fg, '_calc_ValorTotal_10*');
    var nColValorInterno = getColIndexByColKey(fg, 'ValorInterno');
    var nColValorRevenda = getColIndexByColKey(fg, 'ValorRevenda');
    var nColValorUnitario = getColIndexByColKey(fg, 'ValorUnitario');

    fg.Redraw = 0;
    fg.Editable = false;
    
    fg.FrozenCols = 0;
    fg.BorderStyle = 1;
    
    
    //adiciona linha de totais no grid.
    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[nColQuantidade, '#######', 'S'], [nColCalcValorTotal, '##,###,###,###.00', 'S']]);

    //Preenche colular valor total e linha de totais.
    setTotaisGrid();

    //alinha colunas a direita de acordo com a defini��o do nArray.
    alignColsInGrid(fg, aReturnFieldsToGrid(nArray, 5));

    fg.ColDataType(getColIndexByColKey(fg, colunasReadOnly('Troca'))) = 11;

    //Preenche pre�o interno e revenda igual ao preco unitario quando os mesmo vierem zerados.
    if (bResultado) {
        for (i = 2; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, nColPrecoRevenda) == 0)
                fg.TextMatrix(i, nColPrecoRevenda) = fg.ValueMatrix(i, nColPrecoUnitario);

            if (fg.ValueMatrix(i, nColPrecoInterno) == 0)
                fg.TextMatrix(i, nColPrecoInterno) = fg.ValueMatrix(i, nColPrecoRevenda);

            if (fg.ValueMatrix(i, nColValorRevenda) == 0)
                fg.TextMatrix(i, nColValorRevenda) = fg.ValueMatrix(i, nColValorUnitario);

            if (fg.ValueMatrix(i, nColValorInterno) == 0)
                fg.TextMatrix(i, nColValorInterno) = fg.ValueMatrix(i, nColValorRevenda);
        }
    }

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.FrozenCols = 2;
    fg.Redraw = 2;

    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if (fg.Rows > 1)
        fg.Col = nColQuantidade;

    if (fg.Rows > 1)
        fg.Editable = true;

    lockControlsInModalWin(false);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
        btnOK.disabled = false;
    else
        btnOK.disabled = true;
    
    // se tem linhas no grid, coloca foco no grid, caso contrario
    // coloca foco no txtPalavraChave
    if (fg.Rows > 1)
        fg.focus();
    else {
        if (txtPalavraChave.currentStyle.visibility != 'hidden')
            if (!txtPalavraChave.disabled)
            txtPalavraChave.focus();
        else {
            if (!txtPedidoReferencia.disabled)
                txtPedidoReferencia.focus();
        }
    }

    //desmarca os caras
    if (glb_nRowTrocaReferencia > 0) 
    {
        btnTrocar.style.visibility = 'hidden';
        glb_nRowTrocaReferencia = 0;
    }

    //showHideFlds();
    
}

function btnTrocar_Clicked() 
{
    var nColIDProdReferencia = getColIndexByColKey(fg, 'ProdutoID2*');
    var nColProdutoReferencia = getColIndexByColKey(fg, 'Produto2*');

    var nColIDProdPesquisa = getColIndexByColKey(fg2, 'ProdutoID*');
    var nColProdutoPesquisa = getColIndexByColKey(fg2,'Produto*');

    var nColTrocaReferencia = getColIndexByColKey(fg, colunasReadOnly('Troca'));
    var nColTrocaPesquisa = getColIndexByColKey(fg2, colunasReadOnly('Troca'));

    var nColValorUnitReferencia = getColIndexByColKey(fg, 'PrecoUnitario');
    var nColValorUnitPesquisa = getColIndexByColKey(fg2, 'ValorUnitario*');

    //PEGA O PRODUTO SELECIONADO NA PESQUISA E TROCA 
    fg.TextMatrix(glb_nRowTrocaReferencia, nColIDProdReferencia) = fg2.TextMatrix(glb_nRowTrocaPesquisa, nColIDProdPesquisa);
    fg.TextMatrix(glb_nRowTrocaReferencia, nColProdutoReferencia) = fg2.TextMatrix(glb_nRowTrocaPesquisa, nColProdutoPesquisa);
    fg.TextMatrix(glb_nRowTrocaReferencia, nColValorUnitReferencia) = fg2.ValueMatrix(glb_nRowTrocaPesquisa, nColValorUnitPesquisa);
    
    //desmarca os caras
    fg.TextMatrix(glb_nRowTrocaReferencia, nColTrocaReferencia) = "";
    fg2.TextMatrix(glb_nRowTrocaPesquisa, nColTrocaPesquisa) = "";
    btnTrocar.style.visibility = 'hidden';
    lockControlsInModalWin(false);

    js_ModalReferencia_AfterEdit(glb_nRowTrocaReferencia, nColValorUnitReferencia);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_ModalReferencia_ValidateEditRef(Row, Col) {
    var nColTroca = getColIndexByColKey(fg, colunasReadOnly('Troca'));

    if ((fg.Editable) && (fg.Col == nColTroca)) {
        //esta marcando um novo
        if (fg.EditText == 2) {

            //verifica se ja tem um marcado e desmarca
            if (glb_nRowTrocaReferencia > 0)
                fg.TextMatrix(glb_nRowTrocaReferencia, nColTroca) = "";

            if (glb_nRowTrocaReferencia != Row) {
                glb_nRowTrocaReferencia = Row;
                fg.TextMatrix(glb_nRowTrocaReferencia, nColTroca) = "-1";
            }
        }
        //esta demarcando
        else
            glb_nRowTrocaReferencia = 0;
    }

    if (glb_nRowTrocaPesquisa > 0 && glb_nRowTrocaReferencia > 0)
        btnTrocar.style.visibility = 'inherit';
    else
        btnTrocar.style.visibility = 'hidden';

    showHideFlds();
    //setTotaisGrid(Row, Col);
}

function btnFindPesCon_onclick() 
{

    var stopPesq = false;
    var nEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'getCurrEmpresaData()');
    var nEmpresaID = nEmpresaData[0];

    // trima todos os campos
    txtConceitoID.value = trimStr(txtConceitoID.value);
    txtQuantidade.value = trimStr(txtQuantidade.value);


    stopPesq = ((trimStr(txtConceitoID.value) == '') ||
                 (trimStr(txtQuantidade.value) == ''));

    fg3.Rows = 1;

    if (stopPesq) 
    {
        if (window.top.overflyGen.Alert('Preencha o ID e a quantidade para listar.') == 0)
            return null;

        window.focus();

        return true;
    }

    lockControlsInModalWin(true);
    
    setConnection(dsoPescon);


    dsoPescon.SQL = 'SELECT Ped.PessoaID, dbo.fn_Pessoa_Fantasia(Ped.PessoaID,0) AS Pessoa, dbo.fn_Pessoa_Fantasia(Ped.ParceiroID,0) AS Parceiro, Ped.PedidoID AS PedidoID, Ope.Operacao AS Operacao, ' +
                    'Con.Conceito AS Produto, PedItens.ValorUnitario AS ValorUnitario, Entradas.Associar AS Saldo ' +
	                'FROM DBO.fn_Produto_Entradas_tbl(' + nEmpresaID + ', ' + txtConceitoID.value + ', ' + txtQuantidade.value + ', 361, NULL, NULL, GETDATE(), ' + glb_nTransacaoID + ' , 1) AS Entradas ' +
	                'INNER JOIN Pedidos_Itens PedItens WITH(NOLOCK) ON PedItens.PedItemID = Entradas.PedItemID ' +
	                'INNER JOIN Pedidos Ped WITH(NOLOCK) ON Ped.PedidoID = PedItens.PedidoID ' +
	                'INNER JOIN Operacoes Ope WITH(NOLOCK) ON Ope.OperacaoID = Ped.TransacaoID ' +
	                'INNER JOIN Conceitos Con WITH(NOLOCK) ON Con.ConceitoID = PedItens.ProdutoID ';
    dsoPescon.ondatasetcomplete = fillgridPesCon_DSC;
    try 
    {
        dsoPescon.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro Inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);

        fg3.Rows = 1;
        btnOK.disabled = true;

        if (txtFiltro.disabled == false) {
            window.focus();
            txtFiltro.focus();
        }
        return null;
    }
}


function fillgridPesCon_DSC() {
    
    var aHoldCols = new Array();

    var nArray = [['Pedido', 'PedidoID', '', '', false, false],
                  ['PessoaID', 'PessoaID', '', '', false, false],
                  ['Pessoa', 'Pessoa', '', '', false, false],
                  ['Parceiro', 'Parceiro', '', '', false, false],
                  ['Operacao', 'Operacao', '', '', false, false],
                  ['Produto', 'Produto', '', '', false, false],
                  ['Valor Unit', 'ValorUnitario', '', '', false, true],
                  ['Saldo', 'Saldo', '', '', false, true]];
    
    headerGrid(fg3, aReturnFieldsToGrid(nArray, 0), aReturnFieldsToGrid(nArray, 4));

    fillGridMask(fg3, dsoPescon, aReturnFieldsToGrid(nArray, 1), aReturnFieldsToGrid(nArray, 2), aReturnFieldsToGrid(nArray, 3));
    
    //alinha colunas a direita de acordo com a defini��o do nArray.
    alignColsInGrid(fg3, aReturnFieldsToGrid(nArray, 5));

    
    fg3.Redraw = 0;
    fg3.Editable = false;
    //startGridInterface(fg);
    fg3.FrozenCols = 0;
    fg3.BorderStyle = 1;
    
    fg3.AutoSizeMode = 0;
    fg3.AutoSize(0, fg3.Cols - 1);
    fg3.FrozenCols = 2;
    fg3.Redraw = 2;

    lockControlsInModalWin(false);
}