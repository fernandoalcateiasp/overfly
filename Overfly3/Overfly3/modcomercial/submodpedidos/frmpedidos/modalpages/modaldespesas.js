/********************************************************************
modaldespesas.js

Library javascript para o modaldespesas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoGen01 = new CDatatransport('dsoGen01');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() 
{
    window_onload_1stPart();

    // ajusta o body do html
    with (modaldespesasBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

//     configuracao inicial do html
    setupPage();

    getDataInServer();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 52;
    modalFrame.style.left = 0;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Despesas');
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', null);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Despesas', 1);

    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;

    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;

    // Ajusta botoes OK/Cancel para esta janela
    // O botao cancela nao e usado neste asp
    with (btnCanc.style) {
        visibility = 'hidden';
        width = 0;
        height = 0;
    }

    btnCanc.disabled = true;

    // Por default o botao OK vem destravado e muda de posicao
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
    // move o btnOK de posicao e muda seu caption
    btnOK.value = 'Fechar';

    with (btnOK.style) {
        left = (widthFree - parseInt(width, 10)) / 2;
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - 2 * ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) +
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10);
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServer() {
    var strSQL = 'SELECT ' +
			'a.Ordem, a.ProdutoID, h.ItemMasculino AS Excecao, ' +
			'b.Conceito, d.ItemMasculino AS Despesa, ' +
			'j.ItemAbreviado AS Origem, c.AliquotaImposto, e.Conceito AS Familia, ' +
			'f.Conceito AS Marca, i.ItemMasculino AS ClassificacaoPessoa, k.ItemMasculino AS ClassificacaoProduto, l.ItemMasculino AS Finalidade,' +
			'c.Percentual, c.Valor, c.FinanceiroID ' +
		'FROM ' +
			'Pedidos g WITH(NOLOCK) ' +
			'INNER JOIN Pedidos_Itens a WITH(NOLOCK) ON a.PedidoID=g.PedidoID ' +
			'INNER JOIN Conceitos b WITH(NOLOCK) ON a.ProdutoID = b.ConceitoID ' +
			'INNER JOIN Pedidos_Itens_Despesas c WITH(NOLOCK) ON a.PedItemID = c.PedItemID ' +
			'LEFT OUTER JOIN Conceitos f WITH(NOLOCK) ON c.MarcaID = f.ConceitoID ' +
			'LEFT OUTER JOIN Conceitos e WITH(NOLOCK) ON c.FamiliaID = e.ConceitoID ' +
			'INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON c.TipoDespesaID = d.ItemID ' +
			'LEFT OUTER JOIN TiposAuxiliares_Itens h WITH(NOLOCK) ON c.TipoExcecaoID = h.ItemID ' +
			'LEFT OUTER JOIN TiposAuxiliares_Itens i WITH(NOLOCK) ON c.ClassificacaoPessoaID = i.ItemID ' +
			'LEFT OUTER JOIN TiposAuxiliares_Itens j WITH(NOLOCK) ON c.OrigemID = j.ItemID ' +
			'LEFT OUTER JOIN TiposAuxiliares_Itens k WITH(NOLOCK) ON c.ClassificacaoProdutoID = i.ItemID ' +
	        'LEFT OUTER JOIN TiposAuxiliares_Itens l WITH(NOLOCK) ON c.FinalidadeID = i.ItemID ' +
		'WHERE ' +
			'g.PedidoID = ' + glb_nPedidoID + ' ' +
		'ORDER BY ' +
			'a.Ordem, d.ItemMasculino';

    setConnection(dsoGen01);
    dsoGen01.SQL = strSQL;
    dsoGen01.ondatasetcomplete = fillModalGrid_DSC;
    dsoGen01.Refresh();
}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGrid_DSC() {
    var i, j, nSumImp;
    var nAliquota = 0;

    fg.Redraw = 0;

    headerGrid(fg, ['Ordem',
				   'ID',
				   'Produto',
				   'Despesa',
				   'Origem',
				   'Al�q Imp',
				   'Fam�lia',
				   'Marca',
				   'Class Pessoa',
                   'Class Produto',
                   'Finalidade',
				   'Exce��o',
				   'Percentual',
				   'Valor',
				   'Financeiro'], []);

    fillGridMask(fg, dsoGen01, ['Ordem',
							  'ProdutoID',
							  'Conceito',
							  'Despesa',
							  'Origem',
							  'AliquotaImposto',
							  'Familia',
							  'Marca',
							  'ClassificacaoPessoa',
                              'ClassificacaoProduto',
                              'Finalidade',
							  'Excecao',
							  'Percentual',
							  'Valor',
							  'FinanceiroID'],
                              ['', '', '', '', '', '', '', '', '', '', '999.99', '999.99', ''],
                              ['', '', '', '', '', '', '', '', '', '', '##0.00', '##0.00', '']);

    alignColsInGrid(fg, [1, 5, 10, 11]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    // linha de Totais
    gridHasTotalLine(fg, 'Total', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'Valor'), '###,###,##0.00', 'S']]);

    fg.Redraw = 2;

    with (modaldespesasBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();
}
