/********************************************************************
modalnumserie.js

Library javascript para o modalnumserie.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_desgatilha = 0;
var glb_timerInterval = null;
var glb_ImportaCodigoBarra = false;
var glb_nTipoPedidoID = null;
var glb_bAsstec = null;
var glb_nLastNumber = 0;
var glb_bFirstLoad = true;

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_callFillGridTimerInt = null;
var glb_aDataFromServer = new Array();
var glb_aRecNo = new Array();
var glb_nCaixaID = -1;
var glb_nCaixaGeradaID = 0;
var glb_bForceGridRefresh = false;
var glb_ProdutoFocus = false;

var dsoGen01 = new CDatatransport('dsoGen01');
var dsoGen02 = new CDatatransport('dsoGen02');
var dsoQuantidade = new CDatatransport('dsoQuantidade');
var dsoGravacaoLote = new CDatatransport('dsoGravacaoLote');
var dsoSeparar = new CDatatransport('dsoSeparar');
var dsoCaixa = new CDatatransport('dsoCaixa');

var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif'; 

/********************************************************************
RELACAO DAS FUNCOES:
window_onload()
btn_onclick(ctl)
setupPage()
loadDataAndTreatInterface()
enableDisableControls()
btnDeletar_onclick()
recalcTotalGridLocal()
btnLimpar_onclick()
btnPreencherCaixa_onclick()
btnZerarCaixa_onclick()
btnZerarCaixa_onclick_DSC()
btnGravar_onclick()
sendDataToServer()
sendDataToServer_DSC()				
adjustLocalGrid()
chkLocal_onclick()
btnOK_Status()
setMaskAndLengthToInputs()
setonkeyup()
setNextFocus(othis, nTipoChamada)
checkQuantValue()
btnOK_Clicked()
fillModalGrid()
fillModalGrid_DSC()
showModalAfterDataLoad()										
treatNumeroSerie()
treatNumeroSerieLocal()
treatNumeroSerieInServer()
treatNumeroSerieInServer_DSC()
changeGridData(nQuantidadeGatilhada)
btnEIR_onClick()
btnImportar_onClick()
btnImportar_onClick_DSC()
btnDesgatilhar_onClick()
btnDesgatilhar_onClick_DSC()
btnRefresh_onClick()
setVisibilityControls()
setColorAndselFieldContent()
pesqProduto(sProduto, nTipoRetorno)
setQuantidade(nTipoChamada)
focusInvisibleColorDefaultFld()
oPrinter_OnPrintFinish()
setupInterfaceState()																
btnSeparar_onClick()
dsoSeparar_DSC()
btnSepararTodos_onClick()
dsoSepararTodos_DSC()
fg_AfterRowColChange_NumSerie()
setBtnsLabel()
btnPickingList_onClick()
getCurrDate()
fillComboCaixa()
dsoCaixa_DSC()	
selCaixaID_onchange()
fillLocalGrid()
fillLocalGrid_DSC()		
    
EVENTOS DOS OBJETOS DE IMPRESSAO:

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    fillModalGrid();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    window.focus();
    if (ctl.id == btnOK.id)
        btnOK.focus();
    //else if (ctl.id == btnCanc.id )
    //    btnCanc.focus();

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    glb_nTipoPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoPedidoID' + '\'' + '].value');
    glb_bAsstec = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Asstec' + '\'' + '].value');

    // texto da secao01
    secText('N�meros de S�rie', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // O estado do botao btnOK
    btnOK_Status();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var x_gap, y_gap;
    var empresaData = getCurrEmpresaData();

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;

    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;

    // Ajusta botoes OK/Cancel para esta janela
    // O botao cancela nao e usado neste asp
    with (btnCanc.style) {
        visibility = 'hidden';
        width = 0;
        height = 0;
    }

    btnCanc.disabled = true;

    // Por default o botao OK vem travado e muda o parent
    btnOK.disabled = true;
    // move o btnOK de posicao e muda seu caption
    btnOK.value = 'Fechar';

    with (btnEIR.style) {
        height = parseInt(btnOK.currentStyle.height, 10);
        width = parseInt(btnOK.currentStyle.width, 10);
    }
    btnEIR.onclick = btnEIR_onClick;

    with (btnImportar.style) {
        height = parseInt(btnOK.currentStyle.height, 10);
        width = parseInt(btnOK.currentStyle.width, 10);
    }
    btnImportar.onclick = btnImportar_onClick;

    with (btnDesgatilhar.style) {
        height = parseInt(btnOK.currentStyle.height, 10);
        width = parseInt(btnOK.currentStyle.width, 10);
    }
    btnDesgatilhar.onclick = btnDesgatilhar_onClick;

    with (btnRefresh.style) {
        height = parseInt(btnOK.currentStyle.height, 10);
        width = parseInt(btnOK.currentStyle.width, 10);
    }
    btnRefresh.onclick = btnRefresh_onClick;

    with (btnSeparar.style) {
        height = parseInt(btnOK.currentStyle.height, 10);
        width = parseInt(btnOK.currentStyle.width, 10);
    }
    btnSeparar.onclick = btnSeparar_onClick;
    btnSeparar.setAttribute('estorno', 0, 1);

    with (btnSepararTodos.style) {
        height = parseInt(btnOK.currentStyle.height, 10);
        width = parseInt(btnOK.currentStyle.width, 10);
    }
    btnSepararTodos.onclick = btnSepararTodos_onClick;
    btnSepararTodos.setAttribute('estorno', 0, 1);

    with (btnPickingList.style) {
        height = parseInt(btnOK.currentStyle.height, 10);
        width = parseInt(btnOK.currentStyle.width, 10);
    }
    btnPickingList.onclick = btnPickingList_onClick;

    // largura livre apos mover o botao OK
    widthFree = modWidth - frameBorder - parseInt(btnOK.currentStyle.width, 10);

    // ajusta o divFields
    elem = window.document.getElementById('divFields');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = widthFree - 3 * ELEM_GAP;
        // altura e fixa para uma linha de campos
        height = (16 + 24);
        y_gap = parseInt(top, 10) + parseInt(height, 10);
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = y_gap;
        width = modWidth - frameBorder - 2 * ELEM_GAP;
        height = modHeight - parseInt(top, 10) - frameBorder - (4 * ELEM_GAP);
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }

    // ajusta o divFieldsLocal
    elem = window.document.getElementById('divFieldsLocal');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = y_gap;
        width = modWidth - frameBorder - 2 * ELEM_GAP;
        height = btnGravar.offsetTop + btnGravar.offsetHeight + (ELEM_GAP * 1.5);
    }

    // ajusta o divLocal
    elem = window.document.getElementById('divLocal');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divFieldsLocal.offsetTop + divFieldsLocal.offsetHeight;
        width = modWidth - frameBorder - 2 * ELEM_GAP;
        height = modHeight - parseInt(top, 10) - frameBorder - (7 * ELEM_GAP);
    }

    elem = document.getElementById('fgLocal');
    with (elem.style) {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }

    btnEIR.style.top = parseInt(divFG.currentStyle.top, 10) +
					  parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;
    btnEIR.style.left = ELEM_GAP;

    btnImportar.style.top = parseInt(divFG.currentStyle.top, 10) +
					  parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;
    btnImportar.style.left = parseInt(btnEIR.currentStyle.left, 10) +
	                         parseInt(btnEIR.currentStyle.width, 10) + ELEM_GAP;

    btnDesgatilhar.style.top = parseInt(divFG.currentStyle.top, 10) +
					  parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;
    btnDesgatilhar.style.left = parseInt(btnImportar.currentStyle.left, 10) +
	                            parseInt(btnImportar.currentStyle.width, 10) + ELEM_GAP;

    // reposiciona o botao Refresh
    btnRefresh.style.top = parseInt(divFG.currentStyle.top, 10) +
					        parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;

    btnRefresh.style.left = parseInt(btnDesgatilhar.currentStyle.left, 10) +
	                            parseInt(btnDesgatilhar.currentStyle.width, 10) + ELEM_GAP;

    btnSeparar.style.top = parseInt(divFG.currentStyle.top, 10) +
					        parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;

    btnSeparar.style.left = parseInt(btnRefresh.currentStyle.left, 10) +
	                            parseInt(btnRefresh.currentStyle.width, 10) + ELEM_GAP;

    btnSepararTodos.style.top = parseInt(divFG.currentStyle.top, 10) +
					        parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;

    btnSepararTodos.style.left = parseInt(btnSeparar.currentStyle.left, 10) +
	                            parseInt(btnSeparar.currentStyle.width, 10) + ELEM_GAP;

    btnPickingList.style.top = parseInt(divFG.currentStyle.top, 10) +
					        parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;

    btnPickingList.style.left = parseInt(btnSepararTodos.currentStyle.left, 10) +
	                            parseInt(btnSepararTodos.currentStyle.width, 10) + ELEM_GAP;

    // reposiciona o botao ok
    btnOK.style.top = parseInt(divFG.currentStyle.top, 10) +
					  parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;

    btnOK.style.left = parseInt(btnPickingList.currentStyle.left, 10) +
	                            parseInt(btnPickingList.currentStyle.width, 10) + ELEM_GAP;

    adjustElementsInForm([['lblLocal', 'chkLocal', 3, 1, -10, -10],
						  ['lblEtiquetaPropria', 'chkEtiquetaPropria', 3, 1, -5],
						  ['lblProduto', 'txtProduto', 18, 1],
 						  ['lblQuantidade', 'txtQuantidade', 5, 1],
 						  ['lblControle', 'chkControle', 3, 1],
						  ['lblNumeroSerie', 'txtNumeroSerie', 24, 1, -5],
						  ['lblCodigoBarra', 'txtCodigoBarra', 24, 1]], null, null, true);

    adjustElementsInForm([['lblCaixaID', 'selCaixaID', 25, 1, -10, -10],
						  ['btnPreencherCaixa', 'btn', 24, 1],
						  ['btnZerarCaixa', 'btn', btnOK.offsetWidth, 1, 5],
						  ['btnGravar', 'btn', btnOK.offsetWidth, 1, 5],
						  ['btnDeletar', 'btn', btnOK.offsetWidth, 1, 5],
						  ['btnLimpar', 'btn', btnOK.offsetWidth, 1, 5]]);

    btnPreencherCaixa.style.height = btnOK.offsetHeight;
    btnZerarCaixa.style.height = btnOK.offsetHeight;
    btnGravar.style.height = btnOK.offsetHeight;
    btnDeletar.style.height = btnOK.offsetHeight;
    btnLimpar.style.height = btnOK.offsetHeight;

    btnPreencherCaixa.onclick = btnPreencherCaixa_onclick;
    btnZerarCaixa.onclick = btnZerarCaixa_onclick;
    btnGravar.onclick = btnGravar_onclick;
    btnDeletar.onclick = btnDeletar_onclick;
    btnLimpar.onclick = btnLimpar_onclick;

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;

    startGridInterface(fgLocal);
    fgLocal.Cols = 1;
    fgLocal.ColWidth(0) = parseInt(divLocal.currentStyle.width, 10) * 18;
    headerGrid(fgLocal, ['Ordem', 'ID', 'N�mero de S�rie', 'Quant', 'Mensagem', '', '', ''], []);
    fgLocal.ColKey(0) = 'RecNo';
    fgLocal.ColKey(1) = 'ProdutoID';
    fgLocal.ColKey(2) = 'NumeroSerie';
    fgLocal.ColKey(3) = 'Quantidade';
    fgLocal.ColKey(4) = 'Mensagem';
    fgLocal.ColKey(5) = 'TipoEtiqueta';
    fgLocal.ColKey(6) = 'Produto';
    fgLocal.ColKey(7) = 'CaixaID';
    fgLocal.ColHidden(5) = true;
    fgLocal.ColHidden(6) = true;
    fgLocal.ColHidden(7) = true;

    fgLocal.Redraw = 2;

    chkLocal.checked = false;
    chkLocal.onclick = chkLocal_onclick;
    chkLocal_onclick();

    //chkLocal.disabled = (glb_nTipoPedidoID != 601);

    selCaixaID.onchange = selCaixaID_onchange;

    // Por default o botao OK vem travado e muda o parent
    btnOK.disabled = false;

    // restringe digitacao em campos numericos
    // e da outras providencias
    setMaskAndLengthToInputs();
    setupInterfaceState();
}

function enableDisableControls() {
    if (!glb_bReadOnly)
        return null;

    //var nEmpresaData = getCurrEmpresaData();
    //Tratativa temporaria solicitada por Camilo Rodrigues. FRG - 17/04/2017.
    //if (nEmpresaData[0] != 7)
    //{
        //chkLocal.disabled = glb_bReadOnly;
        chkEtiquetaPropria.disabled = glb_bReadOnly;
        txtProduto.readOnly = glb_bReadOnly;
        txtQuantidade.readOnly = glb_bReadOnly;
        chkControle.disabled = glb_bReadOnly;
        txtNumeroSerie.readOnly = glb_bReadOnly;
        txtCodigoBarra.readOnly = glb_bReadOnly;
        btnGravar.disabled = glb_bReadOnly;
        btnDeletar.disabled = glb_bReadOnly;
        btnLimpar.disabled = glb_bReadOnly;
        
        btnEIR.disabled = glb_bReadOnly;
        btnImportar.disabled = glb_bReadOnly;
        btnDesgatilhar.disabled = glb_bReadOnly;
        btnRefresh.disabled = glb_bReadOnly;
        btnSeparar.disabled = glb_bReadOnly;
        btnSepararTodos.disabled = glb_bReadOnly;
        btnCanc.disabled = glb_bReadOnly;
    //}
}

function btnDeletar_onclick() {
    var i;

    if (fgLocal.Rows <= 3) {
        fgLocal.Rows = 1;
        glb_nLastNumber = 0;
    }
    else if (fgLocal.Row >= 2) {
        fgLocal.RemoveItem(fgLocal.Row);

        glb_nLastNumber = 0;

        for (i = 2; i < fgLocal.Rows; i++)
            fgLocal.TextMatrix(i, getColIndexByColKey(fgLocal, 'RecNo')) = ++glb_nLastNumber;
    }

    recalcTotalGridLocal();
    //ContaQuantBanco();
    setupInterfaceState();
}

function recalcTotalGridLocal() {
    gridHasTotalLine(fgLocal, 'Totais', 0xC0C0C0, null, true, [[1, '#####', 'C'], [3, '#####', 'S']]);
}

function btnLimpar_onclick() {
    fgLocal.Rows = 1;
    glb_nLastNumber = 0;
    recalcTotalGridLocal();
    //ContaQuantBanco();
    setupInterfaceState();
}

function btnPreencherCaixa_onclick() {
    fillComboCaixa();
}

function btnZerarCaixa_onclick() {
    var nProdutoID;
    var nCaixaID;

    if (selCaixaID.selectedIndex > 0) {
        nProdutoID = selCaixaID.options(selCaixaID.selectedIndex).ProdutoID;

        nCaixaID = selCaixaID.value;

        if (nCaixaID < 0)
            nCaixaID = 0;

        var _retConf = window.top.overflyGen.Confirm('Deseja retornar todos os N�meros de S�rie da caixa selecionada?', 1);

        if (_retConf == 0)
            return null;
        else if (_retConf == 1) {
            // esta funcao trava o html contido na janela modal
            lockControlsInModalWin(true);

            var strPars = new String();

            strPars = '?nPedidoID=' + escape(glb_nPedidoID);
            strPars += '&nOrdemProducaoID=' + escape(glb_nOrdemProducaoID);
            strPars += '&nCaixaID=' + escape(nCaixaID);
            strPars += '&nProdutoID=' + escape(nProdutoID);
            strPars += '&nUserID=' + escape(getCurrUserID());

            /*
            o usuario apertou o botao "desgatilhar" ou o botao "zerar caixa";
            os parametros @Quantidade, @Identificador, @NumeroSerie, @EP sempre v�m nulos da interface.
	
            @PedidoID INT, 
            @OrdemProducaoID INT,
            @Quantidade INT, - NULL neste caso
            @Identificador VARCHAR(20), - NULL neste caso
            @EP BIT, - NULL neste caso
            @NumeroSerie VARCHAR(25),-- OUTPUT, - NULL neste caso
            @ProdutoID INT,-- OUTPUT,
            @CaixaID INT OUTPUT,
            @QuantidadeDesgatilhada INT OUTPUT, 
            @StringErro VARCHAR(8000) OUTPUT
            */

            dsoGen02.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/desgatilharnumeroserie.aspx' + strPars;
            dsoGen02.ondatasetcomplete = btnZerarCaixa_onclick_DSC;
            dsoGen02.refresh();
        }
        else
            return null;
    }
}

function btnZerarCaixa_onclick_DSC() {
    if ((dsoGen02.recordset.Fields['Resultado'].value == null) ||
		 (dsoGen02.recordset.Fields['Resultado'].value == '')) {
        btnLimpar_onclick();
        clearComboEx(['selCaixaID']);
        fillModalGrid();
    }
    else {
        // esta funcao destrava o html contido na janela modal
        lockControlsInModalWin(false);

        window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);
        if ((dsoSeparar.recordset['Resultado'].value != null) &&
				(dsoSeparar.recordset['Resultado'].value != '')) {
            if (window.top.overflyGen.Alert(dsoGen02.recordset.Fields['Resultado'].value) == 0)
                return null;
        }
    }
}

function btnGravar_onclick() {
    glb_aDataFromServer = new Array();
    glb_aRecNo = new Array();
    glb_nLastNumber = 0;

    var strPars = '';
    var nBytesAcum = 0;
    var nDataLen = 0;
    var aNumSerie = new Array();
    var nProdutoID = '';
    var nProdutoIDError = 0;
    var nNumeroSerie = '';
    var sErro = '';
    var _retConf = null;
    var nProdutos = 0;

    lockControlsInModalWin(true);
    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    for (i = 2; i < fgLocal.Rows; i++) {
        aNumSerie[aNumSerie.length] = new Array(fgLocal.TextMatrix(i, getColIndexByColKey(fgLocal, 'ProdutoID')),
			(fgLocal.TextMatrix(i, getColIndexByColKey(fgLocal, 'NumeroSerie'))).length);
    }
    asort(aNumSerie, 0);

    for (i = 0; i < aNumSerie.length; i++) {
        if (nProdutoIDError == aNumSerie[i][0])
            continue;

        if (nProdutoID != aNumSerie[i][0]) {
            nProdutoID = aNumSerie[i][0];
            nNumeroSerie = aNumSerie[i][1];
        }
        else if (nNumeroSerie != aNumSerie[i][1]) {
            sErro += aNumSerie[i][0] + ';\n';
            nProdutoIDError = aNumSerie[i][0];
        }
    }

    if (sErro != '') {
        _retConf = window.top.overflyGen.Confirm('Os n�meros de s�rie dos produtos abaixo\n' +
			'est�o com quantidade de caracteres diferentes:\n\n' + sErro, 1);

        if (_retConf == 0)
            return null;
        else if (_retConf != 1) {
            lockControlsInModalWin(false);
            return null;
        }
    }

    for (i = 2; i < fgLocal.Rows; i++) {
        nBytesAcum = strPars.length;

        if ((nBytesAcum >= glb_nMaxStringSize) && (strPars != '')) {
            nBytesAcum = 0;
            glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
            strPars = '';
            nDataLen = 0;

        }

        nDataLen++;

        strPars += (strPars == '' ? '?' : '&') + 'nPedID=' + escape(glb_nPedidoID);
        strPars += '&nOPID=' + escape(glb_nOrdemProducaoID);
        strPars += '&nD=' + escape(glb_desgatilha);
        strPars += '&nUID=' + escape(getCurrUserID());
        strPars += '&nRN=' + escape(fgLocal.TextMatrix(i, getColIndexByColKey(fgLocal, 'RecNo')));
        strPars += '&nTN=' + escape(0); //escape(fgLocal.TextMatrix(i, getColIndexByColKey(fgLocal, 'TipoEtiqueta')));
        strPars += '&nQ=' + escape(fgLocal.TextMatrix(i, getColIndexByColKey(fgLocal, 'Quantidade')));
        strPars += '&sP=' + escape(fgLocal.TextMatrix(i, getColIndexByColKey(fgLocal, 'Produto')));
        strPars += '&sNS=' + escape(fgLocal.TextMatrix(i, getColIndexByColKey(fgLocal, 'NumeroSerie')));
        strPars += '&nPID=' + escape(fgLocal.TextMatrix(i, getColIndexByColKey(fgLocal, 'ProdutoID')));
        strPars += '&nCID=' + escape(fgLocal.TextMatrix(i, getColIndexByColKey(fgLocal, 'CaixaID')));
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);

    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }
    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGravacaoLote.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/numeroserie.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer] + '&nCaixaGeradaID=' + escape(glb_nCaixaGeradaID);
            dsoGravacaoLote.ondatasetcomplete = sendDataToServer_DSC;
            dsoGravacaoLote.refresh();
        }
        else {
            glb_nCaixaGeradaID = 0;
            glb_callFillGridTimerInt = window.setInterval('adjustLocalGrid()', 10, 'JavaScript');
            btnLimpar_onclick();
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}

function sendDataToServer_DSC() {
    if (dsoGravacaoLote.recordset.fields.Count > 0) {
        if (!((dsoGravacaoLote.recordset.BOF) && (dsoGravacaoLote.recordset.EOF))) {
            dsoGravacaoLote.recordset.MoveFirst();
            while (!dsoGravacaoLote.recordset.EOF) 
            {
                glb_aRecNo[glb_aRecNo.length] = dsoGravacaoLote.recordset['RecNo'].value;
                glb_aDataFromServer[glb_aDataFromServer.length] = new Array(0, '', 0, '');
                glb_aDataFromServer[glb_aDataFromServer.length - 1][0] = dsoGravacaoLote.recordset['RecNo'].value;
                glb_aDataFromServer[glb_aDataFromServer.length - 1][1] = dsoGravacaoLote.recordset['NumeroSerie'].value;
                glb_aDataFromServer[glb_aDataFromServer.length - 1][2] = dsoGravacaoLote.recordset['ProdutoID'].value;
                glb_aDataFromServer[glb_aDataFromServer.length - 1][3] = dsoGravacaoLote.recordset['Resultado'].value;
                glb_aDataFromServer[glb_aDataFromServer.length - 1][4] = dsoGravacaoLote.recordset['CaixaID'].value;
                glb_nCaixaGeradaID = dsoGravacaoLote.recordset['CaixaID'].value;

                dsoGravacaoLote.recordset.MoveNext();
            }
        }
    }

    glb_nPointToaSendDataToServer++;
    glb_nCaixaGeradaID = (glb_nCaixaGeradaID == null ? 0 : glb_nCaixaGeradaID);
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

function adjustLocalGrid() {
    if (glb_callFillGridTimerInt != null) {
        window.clearInterval(glb_callFillGridTimerInt);
        glb_callFillGridTimerInt = null;
    }

    asort(glb_aRecNo);
    asort(glb_aDataFromServer);

    var i = 0;
    var nRecNo = 0;
    var nIndex = -1;
    var sUltimoCodigoBarra = '';
    var sMsg = '';
    var nIndexAdjust = 0;
    var nLines = fgLocal.Rows;
    var nCaixaID;

    for (i = 2; i < nLines; i++) {
        nRecNo = fgLocal.TextMatrix(i - nIndexAdjust, getColIndexByColKey(fgLocal, 'RecNo'));
        nIndex = ascan(glb_aRecNo, nRecNo, false);

        if (nIndex >= 0) {
            sMsg = glb_aDataFromServer[nIndex][3];
            sMsg = (sMsg == null ? '' : sMsg);
            nCaixaID = glb_aDataFromServer[nIndex][4];

            if (sMsg == '') {
                fgLocal.RemoveItem(i - nIndexAdjust);
                nIndexAdjust++;
                sUltimoCodigoBarra = glb_aDataFromServer[nIndex][2].toString() + glb_aDataFromServer[nIndex][1].toString();
            }
            else {
                if (sMsg == '*')
                    sMsg = 'N�mero de S�rie j� associado a este pedido.';
                fgLocal.TextMatrix(i - nIndexAdjust, getColIndexByColKey(fgLocal, 'Mensagem')) = sMsg;

                if (nCaixaID == null)
                    nCaixaID = -1;

                fgLocal.TextMatrix(i - nIndexAdjust, getColIndexByColKey(fgLocal, 'CaixaID')) = nCaixaID;
            }
        }
    }

    if (fgLocal.Rows == 2)
        fgLocal.Rows = 1;

    if (sUltimoCodigoBarra != '') {
        txtCodigoBarra.value = sUltimoCodigoBarra;
    }

    recalcTotalGridLocal();

    selCaixaID.value = 0;
    btnRefresh_onClick();
    txtProduto.focus();
}

function chkLocal_onclick() {
    var modWidth = 0;
    var modHeight = 0;
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));

    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    var nFullHeight = modHeight - (12 * ELEM_GAP);

    if (chkLocal.checked) {
        divFieldsLocal.style.top = 74;
        divFieldsLocal.style.visibility = 'visible';
        divLocal.style.top = divFieldsLocal.offsetTop + divFieldsLocal.offsetHeight + 5;
        divLocal.style.height = 195;
        divLocal.style.visibility = 'visible';
        fgLocal.style.height = 195;
        fgLocal.style.visibility = 'visible';

        divFG.style.height = 95;
        fg.style.height = 95;
        divFG.style.top = divLocal.offsetTop + divLocal.offsetHeight + ELEM_GAP;
    }
    else {
        divFG.style.top = 74;
        divFG.style.height = nFullHeight;
        fg.style.height = nFullHeight;
        divFieldsLocal.style.visibility = 'hidden';
        divLocal.style.visibility = 'hidden';
        fgLocal.style.visibility = 'hidden';
    }

    setupInterfaceState();
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver documento digitado
********************************************************************/
function btnOK_Status() {
    var btnOKStatus = false;

    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Restringe quantidade de caracteres digitados, coloca
mascara para digitacao em campos numericos,
seleciona conteudo do campo quando em foco etc
********************************************************************/
function setMaskAndLengthToInputs() {
    chkEtiquetaPropria.onclick = setVisibilityControls;

    txtProduto.maxLength = 20;
    txtProduto.onfocus = setColorAndselFieldContent;
    txtProduto.onkeyup = setonkeyup;
    txtProduto.onblur = setQuantidade;

    txtQuantidade.onblur = checkQuantValue;
    txtQuantidade.maxLength = 5;
    txtQuantidade.onfocus = setColorAndselFieldContent;
    txtQuantidade.onkeypress = verifyNumericEnterNotLinked;
    txtQuantidade.onkeyup = setonkeyup;
    txtQuantidade.setAttribute('thePrecision', 5, 1);
    txtQuantidade.setAttribute('theScale', 0, 1);
    txtQuantidade.setAttribute('verifyNumPaste', 1);
    txtQuantidade.setAttribute('minMax', new Array(1, 99999), 1);

    /*if (chkLocal.checked)
        ContaQuantBanco();
    else*/
        txtQuantidade.value = '1';

    txtNumeroSerie.maxLength = 25;
    txtNumeroSerie.onfocus = setColorAndselFieldContent;
    txtNumeroSerie.onkeyup = setonkeyup;

    txtCodigoBarra.onfocus = setColorAndselFieldContent;
    txtCodigoBarra.disabled = true;
}

/********************************************************************
Configuracao do proximo focus
********************************************************************/
function setonkeyup() {
    if (event.keyCode == 13)
        setNextFocus(this, 1);
}

/********************************************************************
Configuracao do proximo focus
nTipoChamada
1-Enter
2-Retorno do banco
********************************************************************/
function setNextFocus(othis, nTipoChamada) {
    var nEtqSelected = parseInt((chkEtiquetaPropria.checked ? 1 : 2), 10);
    var nQuantidade = parseInt(txtQuantidade.value, 10);
    var bGatilha = true;
    var empresaPaisID = getCurrEmpresaData();

    window.focus();
    if (othis.id == 'txtNumeroSerie') {
        //Partimos daqui
        if (nTipoChamada == 1)
            if (chkLocal.checked) {
                if (!(VerificaGridLocal())) {
                    treatNumeroSerie();
                    //ContaQuantBanco();
                    txtNumeroSerie.focus();
                    //txtProduto.focus();

                }
            }
            else
                treatNumeroSerie();

        else if (nTipoChamada == 2) {
            if (nQuantidade > 0)
                txtNumeroSerie.focus();
            else {
                glb_nCaixaID = -1;

                //Ticket#2017042498001591 - Andr� Jesus solicitou que a mensagem n�o seja exibida nas empresas do Brasil. FRG - 26/04/2017.
                if (empresaPaisID[1] != 130) {
                    window.top.overflyGen.Alert('A quantidade m�xima da caixa foi atingida.');
                }

                /*if (chkLocal.checked)
                    ContaQuantBanco();
                else*/

                    txtQuantidade.value = '1';
                    setQuantidade();
    
                if (txtProduto.value != '') {
                    //txtProduto.value = '';
                    window.focus();

                    if (txtQuantidade.value > 0)
                        txtNumeroSerie.focus();
                    else
                        txtProduto.focus();
                }
                else {
                    window.focus();
                    txtNumeroSerie.focus();
                }
            }
        }
    }
    else if (nEtqSelected == 2) {
        if (othis.id == 'txtProduto')
            txtQuantidade.focus();
        else if (othis.id == 'txtQuantidade') {
            if (fg.Row >= 2) {
                if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoGatilhamento')) == 0)
                    bGatilha = false;
            }

            if (bGatilha == false) {
                if (nTipoChamada == 1)
                    treatNumeroSerie();
                else if (nTipoChamada == 2) {
                    glb_nCaixaID = -1;

                    /*if (chkLocal.checked)
                        ContaQuantBanco();
                    else*/
                        txtQuantidade.value = '1';


                    if (txtProduto.value != '') {
                        txtProduto.value = '';
                        window.focus();
                        txtProduto.focus();
                    }
                }
            }
            else {
                window.focus();
                txtNumeroSerie.focus();
            }
        }
    }
}
/*******************************************************************************************************
Para fazer a Verifica��o do GridLocal.
********************************************************************************************************/
function VerificaGridLocal() {

    var NumeroSerie = txtNumeroSerie.value;
    var Produto = pesqProduto(txtProduto.value, 1);

    if (fgLocal.Rows <= 1)
        return false;
    else {
        for (var i = 2; i < fgLocal.Rows; i++) {

            var NumSerie = fgLocal.TextMatrix(i, getColIndexByColKey(fgLocal, 'NumeroSerie'));
            var ProdutoID = '*' + fgLocal.TextMatrix(i, getColIndexByColKey(fgLocal, 'ProdutoID'));

            if ((NumSerie == NumeroSerie) && (Produto == ProdutoID)) {
                if (window.top.overflyGen.Alert('N�mero de s�rie j� existente.') == 0)
                    return true;

                return true;
            }
        }

        return false;
    }
}
/********************************************************************
Conta as quantidades dos itens Locais.
********************************************************************/
function ContaQuant(Quant) {

    var ProdutoID = pesqProduto(txtProduto.value, 1);
    var NumeroSerie = txtNumeroSerie.value;
    var Quantidade = 0;
    var QuantidadePedido = 0;
    var QuantidadeLocal = 0;

    for (var i = 1; i < fg.Rows; i++) {
        var Produto = fg.ValueMatrix(i, getColIndexByColKey(fg, 'ConceitoID'));
        if (ProdutoID == Produto)
            QuantidadePedido = QuantidadePedido + (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade')));
    }
    for (var i = 1; i < fgLocal.Rows; i++) {
        var Produto = fgLocal.ValueMatrix(i, getColIndexByColKey(fgLocal, 'ProdutoID'));
        if (ProdutoID == Produto)
            QuantidadeLocal = QuantidadeLocal + (fgLocal.ValueMatrix(i, getColIndexByColKey(fgLocal, 'Quantidade')));
    }

    Quantidade = QuantidadePedido - QuantidadeLocal - Quant;

    return Quantidade;
}

function ContaQuantBanco() {
    var ProdutoID = pesqProduto(txtProduto.value, 1);
    var NumeroSerie = txtNumeroSerie.value;
    var Sql = '';

    if (ProdutoID != "") {

        setConnection(dsoQuantidade);
        Sql = 'SELECT COUNT(a.ProdutoID) as Quant' +
                           ' FROM NumerosSerie a WITH(NOLOCK) ' +
                           ' INNER JOIN NumerosSerie_Movimento b WITH(NOLOCK) ON (a.NumeroSerieID = b.NumeroSerieID)' +
                           ' WHERE (a.ProdutoID = ' + ProdutoID + ' ) AND (b.PedidoID =' + glb_nPedidoID + ')';

        dsoQuantidade.SQL = Sql;
        dsoQuantidade.ondatasetcomplete = Quantidade_DSC;
        dsoQuantidade.Refresh();
    }
    else
        Quantidade_DSC();
}
/********************************************************************
Valor minimo do campo de quantidade e controle
********************************************************************/

function Quantidade_DSC() {

    var Quant = 0;

    if (txtProduto.value == "")
        Quant = 0;
    else
        Quant = dsoQuantidade.recordset['Quant'].value;

    Quant = ContaQuant(Quant);

    txtQuantidade.value = Quant;

}
/********************************************************************
Valor minimo do campo de quantidade e controle
********************************************************************/
function checkQuantValue() {
    /*if (this.value == '' || this.value == '0')
    this.value = '1';

    //setQuantidade(1);*/

    //if (!(chkLocal.checked))
        setQuantidade(1);
    //else
      //  ContaQuantBanco();
}
/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked() {
    // 1. O usuario clicou o botao OK
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', null);
}

/********************************************************************
Preenche o grid 
********************************************************************/
function fillModalGrid() {
    var empresaData = getCurrEmpresaData();
    var strSQL = '';
    var strSelect1 = '';
    var strSelect2 = '';
    var strFrom = '';
    var strAdicionalFrom = '';
    var strWhere = '';
    var strAdicionalWhere = '';
    var strGroupBy1 = '';
    var strGroupBy2 = '';
    var strOrderBy = '';
    var sFiltroSelect = '';
    var sFiltroFrom = '';
    var sFiltroWhere = '';
    var sOrdem1 = '';
    var sOrdem2 = '';
    var sSelectColaborador = '';
    var sGroupByColaborador = '';
    var sOrdemProducaoID = 'NULL';

    //lockBtnLupa(btnPreencherCaixa, true);

    if (glb_nOrdemProducaoID == 0) {
        sSelectColaborador = 'Itens.ColaboradorID AS ColaboradorID, ' +
			'(SELECT TOP 1 Fantasia FROM Pessoas WITH(NOLOCK) WHERE (PessoaID = Itens.ColaboradorID)) AS Colaborador, ';
        sFiltroSelect = '';
        sFiltroFrom = ' INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (Pedidos.PedidoID = Itens.PedidoID) ';
        sOrdem1 = '';
        sOrdem2 = 'dbo.fn_Produto_Localizacoes(Pedidos.EmpresaID, Produtos.ConceitoID, Pedidos.PedidoID, NULL, 1),';
        sFiltroWhere = ' ';
        sGroupByColaborador = ', Itens.ColaboradorID';
        sOrdemProducaoID = 'NULL';
    }
    else {
        sSelectColaborador = 'NULL AS ColaboradorID, NULL AS Colaborador, ';
        sFiltroSelect = ' AND Movimentos.OrdemProducaoID = ' + glb_nOrdemProducaoID;
        sFiltroFrom = ' INNER JOIN OrdensProducao WITH(NOLOCK) ON (Pedidos.PedidoID = OrdensProducao.PedidoID AND ) INNER JOIN OrdensProducao_Itens Itens WITH(NOLOCK) ON (OrdensProducao.OrdemProducaoID = Itens.OrdemProducaoID) ';
        sOrdem1 = ' dbo.fn_OrdemProducaoItem_Ordem(Itens.OrdItemID), ';
        sOrdem2 = '';
        sFiltroWhere = ' AND OrdensProducao.OrdemProducaoID = ' + glb_nOrdemProducaoID + ' AND Itens.TipoMaterialID IN(761,762) ';
        sOrdemProducaoID = glb_nOrdemProducaoID;
    }

    strSelect1 = 'SELECT ' +
		'0 AS Indice, Produtos.ConceitoID, Produtos.Conceito, Produtos.Modelo, Produtos.PartNumber, ' +
		'dbo.fn_Produto_Localizacoes(Pedidos.EmpresaID, Produtos.ConceitoID, Pedidos.PedidoID, NULL, 1) AS Localizacao, ' +
		'(CASE (ISNULL(Transacoes.CodigoBarra, 0)) WHEN 0 THEN 0 ' +
		'ELSE (ISNULL(ProdutosEmpresa.TipoGatilhamento, 0)) END) AS TipoGatilhamento, ' +
		'ISNULL(Produtos.Controle, 0) AS Controle, ' +
		'ISNULL(Produtos.NumeroSerieEspecifico, 0) AS NumeroSerieEspecifico, ' +
		'ISNULL(Produtos.EtiquetaPropria, 0) AS EtiquetaPropria, ' +
		sOrdem1 +
		'dbo.fn_Pedido_NumeroSerieQuantidade(Pedidos.PedidoID, ' + sOrdemProducaoID + ', Produtos.ConceitoID, 4) AS QuantidadeCaixas, dbo.fn_Pedido_NumeroSerieQuantidade(Pedidos.PedidoID, ' + sOrdemProducaoID + ', Produtos.ConceitoID, 5) AS QuantidadeUnitaria, ' +
		'CONVERT(NUMERIC(6), SUM(Itens.Quantidade)) AS Quantidade, ' +
		'dbo.fn_Pedido_NumeroSerieQuantidade(Pedidos.PedidoID, ' + sOrdemProducaoID + ', Produtos.ConceitoID, 3) AS Falta, ' +
		sSelectColaborador +
			'ISNULL((SELECT TOP 1 ImportaCodigoBarra FROM RelacoesPesRec WITH(NOLOCK) WHERE (SujeitoID = Pedidos.EmpresaID AND ' +
				'TipoRelacaoID = 12 AND ObjetoID = 999)), 0) AS ImportaCodigoBarra, ' +
				'ISNULL(dbo.fn_Produto_PesosMedidas(Produtos.ConceitoID, NULL, 10), 1) AS QuantidadeMaxima, ISNULL(Produtos.Controle, 0) AS Controle, ISNULL(Produtos.NumeroSerieEspecifico, 0) AS NumeroSerieEspecifico, ' +
				'Produtos.PartNumber AS PartNumber, NULL AS Identificador , dbo.fn_Produto_Descricao2(Itens.PedItemID, ' + empresaData[8] + ', 21 ) as DescricaoFiscal';

    strFrom = ' FROM Pedidos WITH(NOLOCK) ' + sFiltroFrom + ' ' +
	    'INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (Itens.ProdutoID = Produtos.ConceitoID) ' +
        'INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (Pedidos.TransacaoID = Transacoes.OperacaoID) ' +
	    'INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Produtos.ConceitoID = ProdutosEmpresa.ObjetoID AND Pedidos.EmpresaID = ProdutosEmpresa.SujeitoID) ';

    strWhere = ' WHERE (Pedidos.PedidoID = ' + glb_nPedidoID + sFiltroWhere + ' AND ' +
				'((Produtos.ProdutoSeparavel = 1) OR (Pedidos.TransacaoID IN (511, 512, 515, 516, 521, 523, 525, 527) AND Produtos.ProdutoSeparavel IN (0,1))) AND ProdutosEmpresa.TipoRelacaoID = 61) ';

    strGroupBy1 = ' GROUP BY Pedidos.PedidoID, Itens.PedItemID, Pedidos.EmpresaID, Produtos.ConceitoID, Produtos.Conceito, Produtos.Modelo, Produtos.PartNumber, ' +
		'Produtos.Controle, Produtos.NumeroSerieEspecifico, Produtos.EtiquetaPropria, ' + sOrdem1 + ' Pedidos.EmpresaID, Transacoes.CodigoBarra, ProdutosEmpresa.TipoGatilhamento ' + sGroupByColaborador;

    strSelect2 = ' UNION ALL SELECT ' +
		'1 AS Indice, Produtos.ConceitoID, Produtos.Conceito, Produtos.Modelo, Produtos.PartNumber, NULL, ' +
		'(CASE (ISNULL(Transacoes.CodigoBarra, 0)) WHEN 0 THEN 0 ' +
		'ELSE (ISNULL(ProdutosEmpresa.TipoGatilhamento, 0)) END) AS TipoGatilhamento, ' +
		'ISNULL(Produtos.Controle, 0) AS Controle, ' +
		'ISNULL(Produtos.NumeroSerieEspecifico, 0) AS NumeroSerieEspecifico, ' +
		'ISNULL(Produtos.EtiquetaPropria, 0) AS EtiquetaPropria, ' +
		sOrdem1 +
		'NULL AS QuantidadeCaixas, NULL AS QuantidadeUnitaria, ' +
		'NULL AS Quantidade, ' +
		'NULL AS Falta, ' +
		sSelectColaborador +
		'NULL, ISNULL(dbo.fn_Produto_PesosMedidas(Produtos.ConceitoID, NULL, 10), 1) AS QuantidadeMaxima, ISNULL(Produtos.Controle, 0) AS Controle, ISNULL(Produtos.NumeroSerieEspecifico, 0) AS NumeroSerieEspecifico, ' +
				'Produtos.PartNumber, Identificadores.Identificador , dbo.fn_Produto_Descricao2(Itens.PedItemID,' + empresaData[8] + ', 21 ) as DescricaoFiscal';

    strAdicionalFrom = ' LEFT OUTER JOIN Conceitos_Identificadores Identificadores WITH(NOLOCK) ON (Produtos.ConceitoID = Identificadores.ProdutoID) ';

    strAdicionalWhere = ' ';

    strGroupBy2 = ' GROUP BY Pedidos.PedidoID,Itens.PedItemID, Produtos.ConceitoID, Produtos.Conceito, Produtos.Modelo, Produtos.PartNumber, ' +
		'Produtos.Controle, Produtos.NumeroSerieEspecifico, Produtos.EtiquetaPropria, ' + sOrdem1 + ' Identificadores.Identificador, Transacoes.CodigoBarra, ProdutosEmpresa.TipoGatilhamento ' + sGroupByColaborador;

    strOrderBy = ' ORDER BY TipoGatilhamento DESC, ' + sOrdem1 + sOrdem2 + ' Produtos.Conceito, Produtos.Modelo, Produtos.ConceitoID ';

    strSQL = strSelect1 + strFrom + strWhere + strGroupBy1 +
		strSelect2 + strFrom + strAdicionalFrom + strWhere + strAdicionalWhere + strGroupBy2 +
		strOrderBy;

    setConnection(dsoGen01);

    dsoGen01.SQL = strSQL;

    dsoGen01.ondatasetcomplete = fillModalGrid_DSC;
    dsoGen01.Refresh();
}

function fillModalGrid_DSC() {
    var i;
    var aHoldCols = new Array();
    var sCliente = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selPessoaID.options(selPessoaID.selectedIndex).innerText');
    var sTransportadora = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selTransportadoraID.options(selTransportadoraID.selectedIndex).innerText');

    if (!(dsoGen01.recordset.BOF && dsoGen01.recordset.EOF)) {
        // move o cursor do dso para o primeiro registro
        dsoGen01.recordset.MoveFirst();

        // filtra o dso para todos os modulos associados ao no raiz
        dsoGen01.recordset.setFilter('Indice = 0');

        glb_ImportaCodigoBarra = dsoGen01.recordset.Fields['ImportaCodigoBarra'].value;
        startGridInterface(fg);
        fg.FontSize = '8';
        fg.FrozenCols = 3;

        if (glb_nOrdemProducaoID == 0)
            aHoldCols = [2,3,4,10];
        else
            aHoldCols = [2,3,4,10, 11];

        headerGrid(fg, ['ID','Descri��o','Produto', 'Modelo', 'PartNumber', 'Quant', 'Localiza��o', 'Cx', 'Un', 'Falta', 'ID', 'Colaborador', 'GAT', 'Contr', 'NSE', 'EP'], aHoldCols);

        fillGridMask(fg, dsoGen01, ['ConceitoID', 'DescricaoFiscal', 'Conceito', 'Modelo', 'PartNumber', 'Quantidade', 'Localizacao', 'QuantidadeCaixas', 'QuantidadeUnitaria', 'Falta', 'ColaboradorID', 'Colaborador', 'TipoGatilhamento', 'Controle', 'NumeroSerieEspecifico', 'EtiquetaPropria'],
								 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);

        alignColsInGrid(fg, [0, 4, 6, 7, 8, 11]);
        gridHasTotalLine(fg, '# ' + glb_nPedidoID, 0xC0C0C0, null, true, [[3, '######', 'S'], [5, '######', 'S'], [6, '######', 'S'], [7, '######', 'S']]);

        fg.TextMatrix(1, getColIndexByColKey(fg, 'DescricaoFiscal')) = sCliente + ' (' + sTransportadora + ')' + ' - ' + glb_sdtLiberacao;
        //fg.TextMatrix(1, getColIndexByColKey(fg, 'Quantidade')) = glb_sdtLiberacao;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Colaborador')) = glb_sVendedor;

        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);

        // desfiltra o dso
        dsoGen01.recordset.setFilter('');
    }

    txtNumeroSerie.readOnly = (fg.Rows <= 1);

    showModalAfterDataLoad();
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad() {
    if (glb_nTipoPedidoID == 601) {
        btnEIR.style.visibility = 'inherit';
        btnEIR.disabled = false;
    }
    else {
        btnEIR.style.visibility = 'hidden';
        btnEIR.disabled = true;
    }

    if ((glb_ImportaCodigoBarra) && (glb_nTipoPedidoID == 601) && (glb_nOrdemProducaoID == 0)) {
        btnImportar.style.visibility = 'inherit';
        btnImportar.disabled = false;
    }
    else {
        btnImportar.style.visibility = 'hidden';
        btnImportar.disabled = true;
    }

    // ajusta o body do html
    with (modalnumserieBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    window.focus();

    if (btnCanc.disabled == false)
        btnCanc.focus();

    // esta funcao destrava o html contido na janela modal
    lockControlsInModalWin(false);
    setupInterfaceState();

    setVisibilityControls();
    enableDisableControls();

    if ((glb_bFirstLoad) && (glb_nOrdemProducaoID == 0)) {
        glb_bFirstLoad = false;

        for (i = 2; i < fg.Rows; i++) {
            if ((!glb_bReadOnly) &&
			    (fg.TextMatrix(i, getColIndexByColKey(fg, 'ColaboradorID')) != '') &&
				(fg.TextMatrix(i, getColIndexByColKey(fg, 'ColaboradorID')) != getCurrUserID())) {
                if (window.top.overflyGen.Alert('Outro usu�rio j� est� separando itens deste pedido.') == 0)
                    return null;

                break;
            }
        }
    }

    if (fg.Cols > 0) {
        fg.LeftCol = 0;
        fg.Col = 0;
    }

    txtProduto.focus();
    //lockBtnLupa(btnPreencherCaixa, false);
}

/********************************************************************
Trata conteudo do numero de serie txtNumeroSerie localmente ou
no servidor de dados

Parametros:     - nenhum
Retorno:        - true se OK, caso contrario false
********************************************************************/
function treatNumeroSerie() {
    txtProduto.value = trimStr(txtProduto.value);
    txtNumeroSerie.value = trimStr(txtNumeroSerie.value);

    var nEtqSelected = parseInt((chkEtiquetaPropria.checked ? 1 : 2), 10);
    var nQuantidade = parseInt(txtQuantidade.value, 10);
    var sProduto = txtProduto.value;
    var sNumeroSerie = txtNumeroSerie.value;
    var bGatilha = true;

    if (fg.Row >= 2) {
        if ((fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoGatilhamento')) == 0))
            bGatilha = false;
        else if ((fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoGatilhamento')) == 1) ||
				(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoGatilhamento')) == 2)) {
            if ((glb_nMetodoCustoID == 372) && (glb_nTipoPedidoID == 601))
                bGatilha = false;
        }
    }

    if (fg.Rows <= 1) {
        window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

        if (window.top.overflyGen.Alert('N�o existe produto gatilhavel neste pedido.') == 0)
            return null;

        focusInvisibleColorDefaultFld();
        return null;
    }

    if ((nEtqSelected == 1) && (sNumeroSerie.length < 7)) {
        window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

        if (window.top.overflyGen.Alert('N�mero de S�rie inv�lido.') == 0)
            return null;

        focusInvisibleColorDefaultFld();
        return null;
    }

    if ((nEtqSelected == 1) && (bGatilha == false)) {
        window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

        if (window.top.overflyGen.Alert('Este produto n�o pode ser gatilhado como Etiqueta pr�pria.') == 0)
            return null;

        focusInvisibleColorDefaultFld();
        return null;
    }

    if (nEtqSelected == 2) {
        if (nQuantidade == 0) {
            window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

            if (window.top.overflyGen.Alert('Quantidade inv�lida.') <= 0)
                return null;

            focusInvisibleColorDefaultFld();
            return null;
        }
        else if ((sNumeroSerie.length == 0) && (bGatilha != false)) {
            window.focus();

            if ((txtProduto.value == '') || (parseInt(txtQuantidade.value, 10) == 1)) {
                ContaQuant();
                txtNumeroSerie.focus();
            } //txtProduto.focus();
            else
                txtQuantidade.focus();

            return null;
        }
        else if ((sNumeroSerie.length <= 2) && (bGatilha != false)) {
            window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

            if (window.top.overflyGen.Alert('N�mero de S�rie inv�lido.') == 0)
                return null;

            focusInvisibleColorDefaultFld();
            return null;
        }

        if (((chkLocal.checked) && (sProduto == '')) || ((chkLocal.checked) && (bGatilha == false))) {
            window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

            if (window.top.overflyGen.Alert('Produto inv�lido.') == 0)
                return null;

            focusInvisibleColorDefaultFld();
            return null;
        }
    }

    if (chkLocal.checked)
        treatNumeroSerieLocal();
    else
        treatNumeroSerieInServer();
}

function treatNumeroSerieLocal() {
    var nProdutoID = 0;
    var sProduto = txtProduto.value;
    var nQuantidadeMaxima;
    var nQuantidade = (chkControle.checked == true ? 1 : parseInt(txtQuantidade.value, 10));
    var nQuantidadeControle;
    var nQuantidadeCampo = parseInt(txtQuantidade.value, 10);
    var strPars = new String();
    var nInsertedRow = Math.max(fgLocal.Rows, 1);
    var nEtqSelected = parseInt((chkEtiquetaPropria.checked ? 1 : 2), 10);

    selCaixaID.value = 0;

    if (fgLocal.Rows <= 2)
        glb_nLastNumber = 0;
    else
        glb_nLastNumber = fgLocal.ValueMatrix(fgLocal.Rows - 1, getColIndexByColKey(fgLocal, 'RecNo'));

    nProdutoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ConceitoID'));

    fgLocal.AddItem('', nInsertedRow);

    if (fgLocal.Row < nInsertedRow)
        fgLocal.Row = nInsertedRow;

    fgLocal.ColDataType(getColIndexByColKey(fgLocal, 'RecNo')) = 12;
    fgLocal.TextMatrix(fgLocal.Row, getColIndexByColKey(fgLocal, 'RecNo')) = ++glb_nLastNumber;
    fgLocal.ColWidth(getColIndexByColKey(fgLocal, 'RecNo')) = 90 * FONT_WIDTH;

    fgLocal.ColDataType(getColIndexByColKey(fgLocal, 'ProdutoID')) = 12;
    fgLocal.TextMatrix(fgLocal.Row, getColIndexByColKey(fgLocal, 'ProdutoID')) = nProdutoID;
    fgLocal.ColWidth(getColIndexByColKey(fgLocal, 'ProdutoID')) = 90 * FONT_WIDTH;

    fgLocal.ColDataType(getColIndexByColKey(fgLocal, 'NumeroSerie')) = 12;
    fgLocal.TextMatrix(fgLocal.Row, getColIndexByColKey(fgLocal, 'NumeroSerie')) = txtNumeroSerie.value;
    fgLocal.ColWidth(getColIndexByColKey(fgLocal, 'NumeroSerie')) = 300 * FONT_WIDTH;

    fgLocal.ColDataType(getColIndexByColKey(fgLocal, 'Mensagem')) = 12;
    fgLocal.TextMatrix(fgLocal.Row, getColIndexByColKey(fgLocal, 'Mensagem')) = '';
    fgLocal.ColWidth(getColIndexByColKey(fgLocal, 'Mensagem')) = 500 * FONT_WIDTH;

    fgLocal.ColDataType(getColIndexByColKey(fgLocal, 'TipoEtiqueta')) = 12;
    fgLocal.TextMatrix(fgLocal.Row, getColIndexByColKey(fgLocal, 'TipoEtiqueta')) = nEtqSelected;

    fgLocal.ColDataType(getColIndexByColKey(fgLocal, 'Quantidade')) = 12;
    fgLocal.TextMatrix(fgLocal.Row, getColIndexByColKey(fgLocal, 'Quantidade')) = nQuantidade;
    fgLocal.ColWidth(getColIndexByColKey(fgLocal, 'Quantidade')) = 100 * FONT_WIDTH;

    fgLocal.ColDataType(getColIndexByColKey(fgLocal, 'Produto')) = 12;
    fgLocal.TextMatrix(fgLocal.Row, getColIndexByColKey(fgLocal, 'Produto')) = txtProduto.value;

    if (glb_nTipoPedidoID == 602)
        glb_nCaixaID = -1;
    else if (glb_nTipoPedidoID == 601) {
        nQuantidadeMaxima = pesqProduto(sProduto, 2);

        if (fgLocal.rows <= 2)
        {
            if (nQuantidadeMaxima == nQuantidadeCampo)
            {
                glb_nCaixaID = 0;
            }
            else
                glb_nCaixaID = -1;
        }
    }

    fgLocal.ColDataType(getColIndexByColKey(fgLocal, 'CaixaID')) = 12;
    fgLocal.TextMatrix(fgLocal.Row, getColIndexByColKey(fgLocal, 'CaixaID')) = glb_nCaixaID;

    alignColsInGrid(fgLocal, [0, 1, 3]);

    if (nEtqSelected == 1)
        nQuantidadeControle = 1;
    else {
        if (chkControle.checked)
            nQuantidadeControle = 1;
        else
            nQuantidadeControle = parseInt(txtQuantidade.value, 10);

        if (glb_desgatilha == 0)
            txtQuantidade.value = parseInt(txtQuantidade.value, 10) - nQuantidadeControle;
        else if (glb_desgatilha == 1)
            txtQuantidade.value = 0;
    }

    nQuantidade = parseInt(txtQuantidade.value, 10);

    recalcTotalGridLocal();
    fgLocal.Row = fgLocal.Rows - 1;
    fgLocal.TopRow = fgLocal.Row;

    setupInterfaceState();
    window.top.overflyGen.PlayAlertSound(true);

    if (nQuantidade > 0)
        setNextFocus(txtNumeroSerie, 2);
    else
    {
        glb_ProdutoFocus = true;
        btnGravar_onclick();
    }
}

/********************************************************************
Trata conteudo do N�mero de Serie no banco de dados

Parametros:     - nenhum
Retorno:        - irrelevante
********************************************************************/
function treatNumeroSerieInServer() {
    if (glb_timerInterval != null) {
        window.clearInterval(glb_timerInterval);
        glb_timerInterval = null;
    }

    // trava modal
    lockControlsInModalWin(true);

    var strPars = new String();

    var nQuantidade = (chkControle.checked == true ? 1 : parseInt(txtQuantidade.value, 10));
    var nEtqSelected = (chkEtiquetaPropria.checked ? 1 : 0);
    var nProdutoID = 0;
    nProdutoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ConceitoID'));
    var nEmpresaData = getCurrEmpresaData();

    var nQuantidadeCampo = parseInt(txtQuantidade.value, 10);
    var nQuantidadeMaxima;
    var sProduto = txtProduto.value;
    var nDataLen = 1;

    if (glb_nTipoPedidoID == 602)
        glb_nCaixaID = -1;
    else if (glb_nTipoPedidoID == 601) {
        nQuantidadeMaxima = pesqProduto(sProduto, 2);

        if (nQuantidadeMaxima == nQuantidadeCampo)
            glb_nCaixaID = 0;
    }

    // @PedidoID
    strPars = '?nPedID=' + escape(glb_nPedidoID);
    // @OrdemProducaoID
    strPars += '&nOPID=' + escape(glb_nOrdemProducaoID);
    // @Quantidade
    strPars += '&nQ=' + escape(nQuantidade);
    // @Identificador
    strPars += '&sP=' + escape(txtProduto.value);
    // @EP (etiqueta propria ou fabricante)
    strPars += '&nTN=' + escape(nEtqSelected);
    // @Usuario logado
    strPars += '&nUID=' + escape(getCurrUserID());
    // @Numero de serie
    strPars += '&sNS=' + escape(replaceStr(txtNumeroSerie.value, ' ', ''));
    // @ProdutoID
    strPars += '&nPID=' + escape(nProdutoID);
    // @Cai xaID
    strPars += '&nCID=' + escape(glb_nCaixaID);
    // RecordNumber - uso exclusivo do .asp invocado
    strPars += '&nRN=' + escape(1);
    // Gatilha ou desgatilha o produto - uso exclusivo do .asp invocado
    strPars += '&nD=' + escape(glb_desgatilha);
    // NdataLen parametro obrigatorio para gravacao no .asp
    strPars += '&nDataLen=' + escape(nDataLen);

    if (nEmpresaData[0] == 2) {
        try {
            dsoGen02.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/numeroserie.aspx' + strPars;
            dsoGen02.ondatasetcomplete = treatNumeroSerieInServer_DSC;
            dsoGen02.refresh();
        }
        catch (e) {
            glb_bReadOnly = true;
            enableDisableControls();

            return null;
        }
    }
    else {

        dsoGen02.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/numeroserie.aspx' + strPars;
        dsoGen02.ondatasetcomplete = treatNumeroSerieInServer_DSC;
        dsoGen02.refresh();
    }
}

/********************************************************************
Retorno do tratamento do Numero de S�rie no banco de dados
no servidor

Parametros:     - nenhum
Retorno:        - irrelevante
********************************************************************/
function treatNumeroSerieInServer_DSC() {
    var nQuantidadeGatilhada;
    // destrava modal
    lockControlsInModalWin(false);

    if ((dsoGen02.recordset.Fields['Resultado'].value == null) ||
		 (dsoGen02.recordset.Fields['Resultado'].value == '')) {
        txtCodigoBarra.value = dsoGen02.recordset.Fields['ProdutoID'].value + dsoGen02.recordset.Fields['NumeroSerie'].value;

        if ((dsoGen02.recordset.Fields['CaixaID'].value != null) &&
			(dsoGen02.recordset.Fields['CaixaID'].value != '')) {
            glb_nCaixaID = dsoGen02.recordset.Fields['CaixaID'].value;
        }
        else
            glb_nCaixaID = -1;

        if ((dsoGen02.recordset.Fields['QuantidadeGatilhada'].value != null) &&
			(dsoGen02.recordset.Fields['QuantidadeGatilhada'].value != '')) {
            nQuantidadeGatilhada = dsoGen02.recordset.Fields['QuantidadeGatilhada'].value;
        }
        else
            nQuantidadeGatilhada = 0;

        changeGridData(nQuantidadeGatilhada);

        window.top.overflyGen.PlayAlertSound(true);

        if (glb_bForceGridRefresh == true) {
            glb_bForceGridRefresh = false;
            txtProduto.value = '';
            txtNumeroSerie.value = '';
            fillModalGrid();
        }
        else
            setNextFocus(txtNumeroSerie, 2);

    }
    else if (dsoGen02.recordset.Fields['Resultado'].value == '*') {
        window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

        var _retConf = window.top.overflyGen.Confirm('N�mero de S�rie j� associado a este pedido.\nDeseja retornar o produto ao Estoque?', 1);

        if (_retConf == 0)
            return null;
        else if (_retConf == 1) {
            glb_desgatilha = 1;
            glb_bForceGridRefresh = true;
            glb_timerInterval = window.setInterval('treatNumeroSerieInServer()', 10, 'JavaScript');
        }
        else {
            // limpa campo txtNumeroSerie
            txtNumeroSerie.value = '';
            focusInvisibleColorDefaultFld();
        }
    }
    else {
        window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

        if (window.top.overflyGen.Alert(dsoGen02.recordset.Fields['Resultado'].value) == 0)
            return null;
        focusInvisibleColorDefaultFld();
    }
}

/********************************************************************
Altera dados expostos no grid em funcao do retorno do tratamento 
do conteudo do codigo de barra txtNumeroSerie no servidor

Parametros:     - nenhum
Retorno:        - irrelevante
********************************************************************/
function changeGridData(nQuantidadeGatilhada) {
    // pesquisa produto no grid
    var nProdutoID = parseInt((txtCodigoBarra.value).substr(0, 5), 10);
    var i;

    var nEtqSelected = (chkEtiquetaPropria.checked ? 1 : 2);
    var nQuantidade;

    if (nEtqSelected != 1) {
        if (glb_desgatilha == 0)
            txtQuantidade.value = parseInt(txtQuantidade.value, 10) - nQuantidadeGatilhada;
        else if (glb_desgatilha == 1) {
            glb_nCaixaID = -1;
            txtQuantidade.value = 0;
        }
    }
    for (i = 1; i < fg.Rows; i++) {
        if (nProdutoID == parseInt(fg.TextMatrix(i, getColIndexByColKey(fg, 'ConceitoID')), 10)) {
            fg.TextMatrix(i, getColIndexByColKey(fg, 'Falta')) = parseInt(fg.TextMatrix(i, getColIndexByColKey(fg, 'Falta')), 10) - nQuantidadeGatilhada;
            if (fg.Rows > 2) {
                var qntFalta = isNaN(parseInt(fg.TextMatrix(1, getColIndexByColKey(fg, 'Falta')), 10)) == true ? 0 : parseInt(fg.TextMatrix(1, getColIndexByColKey(fg, 'Falta')), 10);

                fg.TextMatrix(1, getColIndexByColKey(fg, 'Falta')) = qntFalta - nQuantidadeGatilhada;
            }

            fg.Row = i;
            fg.TopRow = fg.Row;
            glb_desgatilha = 0;

            break;
        }
    }

    window.focus();
    fg.focus();

    // limpa campo txtNumeroSerie
    txtNumeroSerie.value = '';
}

/********************************************************************
Usuario clicou o botao EIR
********************************************************************/
function btnEIR_onClick() {
    var nPrintBarcode = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrPrinterBarCode()');
    var nLabelGap = sendJSMessage(getHtmlId(), JS_WIDEMSG, 'LABELBARCODE_GAP', null);
    var empresaData = getCurrEmpresaData();
    var sEmpresaFantasia = empresaData[3];
    var sProduto = '';

    if (fg.Row >= 2)
        sProduto = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ConceitoID')) + ' ' + fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Conceito')) + ' EIR';

    if (sProduto == '') {
        if (window.top.overflyGen.Alert('Selecionar um produto no grid.') == 0)
            return null;
    }
    else {
        // Reseta o buffer de impressao
        oPrinter.ResetPrinterBarLabel(1, 9, 9);

        oPrinter.InsertBarLabelData(sEmpresaFantasia.substr(0, 13),
		                            removeCharFromString(sProduto, '"'),
		                            glb_nPedidoID);

        oPrinter.PrintBarLabels(nPrintBarcode, true, LABELBARCODE_HEIGHT, nLabelGap);
    }
}

/********************************************************************
Usuario clicou o botao Importar.
Vai ao banco obter dados.
********************************************************************/
function btnImportar_onClick() {
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    nUsuarioID = getCurrUserID();
    var strPars = new String();

    strPars = '?nPedidoID=' + escape(glb_nPedidoID);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);

    dsoGen02.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/importarnumeroserie.aspx' + strPars;
    dsoGen02.ondatasetcomplete = btnImportar_onClick_DSC;
    dsoGen02.refresh();
}

/********************************************************************
Retorno de usuario clicou o botao Importar
Volta do banco com dados
********************************************************************/
function btnImportar_onClick_DSC() {
    if (dsoGen02.recordset.Fields['Resultado'].value == null) {
        fillModalGrid();
    }
    else {
        // esta funcao destrava o html contido na janela modal
        lockControlsInModalWin(false);

        window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

        if (window.top.overflyGen.Alert(dsoGen02.recordset.Fields['Resultado'].value) == 0)
            return null;
    }

}

/********************************************************************
Usuario clicou o botao Desgatilhar.
Vai ao banco obter dados.
********************************************************************/
function btnDesgatilhar_onClick() {
    var nProdutoID = '';

    if (fg.Row >= 2)
        nProdutoID = parseInt(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ConceitoID')), 10);

    if (nProdutoID == '') {
        if (window.top.overflyGen.Alert('Selecionar um produto no grid.') == 0)
            return null;
    }
    else {
        var _retConf = window.top.overflyGen.Confirm('Deseja retornar todos os N�meros de S�rie do produto ' + nProdutoID + ' ao Estoque?', 1);

        if (_retConf == 0)
            return null;
        else if (_retConf == 1) {
            // esta funcao trava o html contido na janela modal
            lockControlsInModalWin(true);

            var strPars = new String();

            strPars = '?nPedidoID=' + escape(glb_nPedidoID);
            strPars += '&nOrdemProducaoID=' + escape(glb_nOrdemProducaoID);
            strPars += '&nCaixaID=' + escape(-1);
            strPars += '&nProdutoID=' + escape(nProdutoID);
            strPars += '&nUserID=' + escape(getCurrUserID());

            /*
            o usuario apertou o botao "desgatilhar" ou o botao "zerar caixa";
            os parametros @Quantidade, @Identificador, @NumeroSerie, @EP sempre v�m nulos da interface.
	
            @PedidoID INT, 
            @OrdemProducaoID INT,
            @Quantidade INT, - NULL neste caso
            @Identificador VARCHAR(20), - NULL neste caso
            @EP BIT, - NULL neste caso
            @NumeroSerie VARCHAR(25),-- OUTPUT, - NULL neste caso
            @ProdutoID INT,-- OUTPUT,
            @CaixaID INT OUTPUT,
            @QuantidadeDesgatilhada INT OUTPUT, 
            @StringErro VARCHAR(8000) OUTPUT
            */

            dsoGen02.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/desgatilharnumeroserie.aspx' + strPars;
            dsoGen02.ondatasetcomplete = btnDesgatilhar_onClick_DSC;
            dsoGen02.refresh();
        }
        else
            return null;
    }
}

/********************************************************************
Retorno de usuario clicou o botao Desgatilhar
Volta do banco com dados
********************************************************************/
function btnDesgatilhar_onClick_DSC() {
    if ((dsoGen02.recordset.Fields['Resultado'].value == null) ||
        (dsoGen02.recordset.Fields['Resultado'].value == '')) {
        fillModalGrid();
    }
    else {
        // esta funcao destrava o html contido na janela modal
        lockControlsInModalWin(false);

        window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

        if (window.top.overflyGen.Alert(dsoGen02.recordset.Fields['Resultado'].value) == 0)
            return null;
    }

}

/********************************************************************
Usuario clicou o botao Refresh.
Vai ao banco obter dados.
********************************************************************/
function btnRefresh_onClick() {
    fillModalGrid();
}

function setVisibilityControls() {
    var nEtqSelected = parseInt((chkEtiquetaPropria.checked ? 1 : 2), 10);

    btnSeparar.style.visibility = 'inherit';
    btnSepararTodos.style.visibility = 'inherit';

    if (glb_nOrdemProducaoID > 0) {
        btnSeparar.style.visibility = 'hidden';
        btnSepararTodos.style.visibility = 'hidden';
    }

    lblEtiquetaPropria.style.visibility = 'inherit';
    chkEtiquetaPropria.style.visibility = 'inherit';
    lblProduto.style.visibility = 'inherit';
    txtProduto.style.visibility = 'inherit';
    lblQuantidade.style.visibility = 'inherit';
    txtQuantidade.style.visibility = 'inherit';
    lblControle.style.visibility = 'hidden';
    chkControle.style.visibility = 'hidden';
    lblNumeroSerie.style.visibility = 'inherit';
    txtNumeroSerie.style.visibility = 'inherit';
    lblCodigoBarra.style.visibility = 'inherit';
    txtCodigoBarra.style.visibility = 'inherit';

    window.focus();

    if ((chkLocal.checked) && (fgLocal.Rows > 2)) {
        window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);
        focusInvisibleColorDefaultFld();
    }
    else if (nEtqSelected == 1) {
        lblProduto.style.visibility = 'hidden';
        txtProduto.style.visibility = 'hidden';
        lblQuantidade.style.visibility = 'hidden';
        txtQuantidade.style.visibility = 'hidden';
        chkControle.checked = false;
        /*if (chkLocal.checked)
            ContaQuantBanco();
        else*/
            txtQuantidade.value = '1';

        if ((!txtNumeroSerie.readOnly) && (!txtNumeroSerie.currentStyle.visibility != 'hidden'))
            txtNumeroSerie.focus();
    }
    else {
        ContaQuant();
        txtNumeroSerie.focus();
    } 
}
/**************************************************************************
Seleciona conteudo de um controle e muda a cor quando o mesmo recebe foco
**************************************************************************/
function setColorAndselFieldContent()
{
    var glb_sControleFocus;

    txtProduto.style.fontWeight = '';
    txtProduto.style.backgroundColor = 'transparent';
    txtQuantidade.style.fontWeight = '';
    txtQuantidade.style.backgroundColor = 'transparent';
    txtNumeroSerie.style.fontWeight = '';
    txtNumeroSerie.style.backgroundColor = 'transparent';
    txtCodigoBarra.style.fontWeight = '';
    txtCodigoBarra.style.backgroundColor = 'transparent';

    if (this.id == 'txtNumeroSerie')
    {
        if (txtQuantidade.value <= 0)
        {
            txtProduto.onfocus = null;
            txtProduto.focus();
            txtProduto.onfocus = setColorAndselFieldContent;
        }
    }

    this.style.fontWeight = 'bold';
    this.style.backgroundColor = '#FF7F50';
    this.select();
}

/********************************************************************
Pesquisa produto por identificador
Parametros:
sProduto - Identificador a ser pesquisado
nTipoRetorno -  1 - ProdutoID
2 - Quantidade maxima
3 - Controle
4 - NS Espec�fico
retorno
null - nao encontrou o registro
********************************************************************/
function pesqProduto(sProduto, nTipoRetorno) {
    var sRet = null;

    var aRet = new Array('ConceitoID', 'QuantidadeMaxima', 'Controle', 'NumeroSerieEspecifico');
    var nProdutoID = 0;
    var nProdutoSelectedID = 0;
    var bProdutosDiferentes = false;
    var nRegistros = 0;

    if ((fg.Rows > 1) && (fg.Row > 1))
        nProdutoSelectedID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ConceitoID'));

    dsoGen01.recordset.setFilter('');

    if ((dsoGen01.recordset.BOF) && (dsoGen01.recordset.EOF))
        return sRet;

    if ((sProduto == null) || (trimStr(sProduto) == ''))
        return sRet;

    // Pesquisa feita por ID
    if (sProduto.substr(0, 1) == '*') {
        if ((!isNaN(sProduto.substr(1))) && sProduto.length > 1)
            dsoGen01.recordset.setFilter('Indice=1 AND ConceitoID =' + sProduto.substr(1));
        else
            return sRet;
    }
    // Pesquisa feita por Identificador
    else {
        dsoGen01.recordset.setFilter( 'Modelo = ' + '\'' + sProduto + '\'' + ' OR ' + ' PartNumber  = ' + '\'' + sProduto + '\'' + ' OR Identificador  = ' + '\'' +	sProduto + '\'');
    }

    if (dsoGen01.recordset.RecordCount() > 0) {
        dsoGen01.recordset.MoveFirst();

        while (!dsoGen01.recordset.EOF) {
            nRegistros++;
            dsoGen01.recordset.MoveNext();
        }
    }

    if (nRegistros == 0) {
        dsoGen01.recordset.setFilter('');
        return sRet;
    }
    else if (nRegistros == 1) {
        dsoGen01.recordset.MoveFirst();

        while (!dsoGen01.recordset.EOF) {
            sRet = dsoGen01.recordset[aRet[nTipoRetorno - 1]].value;
            dsoGen01.recordset.setFilter('');
            return sRet;
            dsoGen01.recordset.MoveNext();
        }
    }
    else {
        dsoGen01.recordset.MoveFirst();
        nProdutoID = dsoGen01.recordset['ConceitoID'].value;

        if (nProdutoID != nProdutoSelectedID) {
            while (!dsoGen01.recordset.EOF) {
                if (nProdutoID != dsoGen01.recordset['ConceitoID'].value) {
                    if (nProdutoSelectedID == dsoGen01.recordset['ConceitoID'].value) {
                        bProdutosDiferentes = false;
                        break;
                    }
                    else
                        bProdutosDiferentes = true;
                }
                dsoGen01.recordset.MoveNext();
            }
        }

        if (!bProdutosDiferentes) {
            if (dsoGen01.recordset.EOF)
                dsoGen01.recordset.MoveFirst();

            sRet = dsoGen01.recordset[aRet[nTipoRetorno - 1]].value;
            dsoGen01.recordset.setFilter('');
            return sRet;
        }
        else {
            window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

            if (window.top.overflyGen.Alert('Selecionar o produto no grid e tentar de novo.') == 0)
                return null;

            focusInvisibleColorDefaultFld();
            sRet = -1;
            dsoGen01.recordset.setFilter('');
            return sRet;
        }
    }
}

/********************************************************************
Ajusta o valor da quantidade
Parametros:
nTipoChamada - null onblur/Enter de txtProduto
1 Onblur de Quantidade
********************************************************************/
function setQuantidade(nTipoChamada) {
    var nEtqSelected = parseInt((chkEtiquetaPropria.checked ? 1 : 2), 10);
    txtProduto.value = trimStr(txtProduto.value);
    var sProduto = txtProduto.value;
    var nProdutoID = pesqProduto(sProduto, 1);
    var nQuantidadeMaxima = pesqProduto(sProduto, 2);
    var bControle = pesqProduto(sProduto, 3);
    var bNumeroSerieEspecifico = pesqProduto(sProduto, 4);
    var nQuantidadeRestante;
    var nQuantidadePedido;
    var nQuantidade = parseInt(txtQuantidade.value, 10);
    var bGatilha = true;

    if (nTipoChamada != 1) {
        if (sProduto != '') {
            if (nProdutoID == null) {
                glb_nCaixaID = -1;
                txtQuantidade.readOnly = true;
                lblControle.style.visibility = 'hidden';
                chkControle.style.visibility = 'hidden';
                chkControle.checked = false;
                /*if (chkLocal.checked)
                    ContaQuantBanco();
                else*/
                    txtQuantidade.value = '1';


                window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

                if (window.top.overflyGen.Alert('Pedido n�o cont�m este produto') == 0)
                    return null;

                focusInvisibleColorDefaultFld();
                return null;
            }
        }
        else {
            glb_nCaixaID = -1;
            txtQuantidade.readOnly = true;
            lblControle.style.visibility = 'hidden';
            chkControle.style.visibility = 'hidden';
            chkControle.checked = false;
            window.focus();

            /*if (chkLocal.checked)
                ContaQuantBanco();
            else*/
                txtQuantidade.value = '1';

            if ((!txtNumeroSerie.readOnly) && (!txtNumeroSerie.currentStyle.visibility != 'hidden'))
                txtNumeroSerie.focus();
        }
    }

    if (nProdutoID != null) {
        for (i = 1; i < fg.Rows; i++) {
            if (nProdutoID == parseInt(fg.TextMatrix(i, getColIndexByColKey(fg, 'ConceitoID')), 10)) {
                nQuantidadeRestante = parseInt(fg.TextMatrix(i, getColIndexByColKey(fg, 'Falta')), 10);
                nQuantidadePedido = parseInt(fg.TextMatrix(i, getColIndexByColKey(fg, 'Quantidade')), 10);

                if ((fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoGatilhamento')) == 0))
                    bGatilha = false;
                else if ((fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoGatilhamento')) == 1) ||
						(fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoGatilhamento')) == 2)) {
                    if ((glb_nMetodoCustoID == 372) && (glb_nTipoPedidoID == 601))
                        bGatilha = false;
                }

                fg.Row = i;
                fg.TopRow = fg.Row;
                break;
            }
        }

        if ((nEtqSelected == 1) && (bGatilha == false)) {
            window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

            if (window.top.overflyGen.Alert('Este produto n�o pode ser gatilhado como Etiqueta pr�pria.') == 0)
                return null;

            focusInvisibleColorDefaultFld();
            return null;
        }

        lblControle.style.visibility = 'hidden';
        chkControle.style.visibility = 'hidden';
        chkControle.checked = bControle;

        if ((glb_nTipoPedidoID == 601) && (nQuantidadeMaxima == 1) && (nQuantidadeRestante > nQuantidade)) {
            glb_nCaixaID = -1;
            txtQuantidade.readOnly = false;

            /*if (chkLocal.checked)
                ContaQuantBanco();
            else*/
                txtQuantidade.value = '1';

        }
        else if ((nQuantidadeMaxima <= 1) || (nQuantidadeRestante < nQuantidadeMaxima)) {
            glb_nCaixaID = -1;
            txtQuantidade.readOnly = true;

            /*if (chkLocal.checked)
                ContaQuantBanco();
            else*/
                //txtQuantidade.value = '1';
                //Ajuste solicitado por Camilo Rodrigues. FRG - 20/04/2017.
                txtQuantidade.value = nQuantidadeRestante;

        }
        else {
            glb_nCaixaID = -1;
            txtQuantidade.readOnly = false;

            if (nTipoChamada != 1)
                txtQuantidade.value = (nQuantidadeMaxima <= nQuantidadeRestante ? nQuantidadeMaxima : 1);
            else if (nQuantidade > nQuantidadeMaxima) {
                txtQuantidade.value = (nQuantidadeMaxima <= nQuantidadeRestante ? nQuantidadeMaxima : 1);
            }

            if ((bControle == false) && (bNumeroSerieEspecifico == true) && (bGatilha == true)) {
                lblControle.style.visibility = 'inherit';
                chkControle.style.visibility = 'inherit';
            }
        }

        if (nTipoChamada != 1) {
            window.focus();

            if ((parseInt(txtQuantidade.value, 10) > 1) && (bControle == 0))
                txtQuantidade.focus();
            else {
                if (bGatilha)
                {
                    if ((chkLocal.checked) && (glb_ProdutoFocus))
                    {
                        txtProduto.focus();
                        glb_ProdutoFocus = false;
                    }
                    else 
                        txtNumeroSerie.focus();
                }                    
                else {
                    treatNumeroSerie();
                }
            }
        }
    }
}

function focusInvisibleColorDefaultFld() {
    window.focus();
    focusInvisibleFld();

    txtProduto.style.fontWeight = '';
    txtProduto.style.backgroundColor = 'transparent';
    txtQuantidade.style.fontWeight = '';
    txtQuantidade.style.backgroundColor = 'transparent';
    txtNumeroSerie.style.fontWeight = '';
    txtNumeroSerie.style.backgroundColor = 'transparent';
    txtCodigoBarra.style.fontWeight = '';
    txtCodigoBarra.style.backgroundColor = 'transparent';
}

function oPrinter_OnPrintFinish() {
    return null;
}

function setupInterfaceState() {
    var bState = true;
    var bState2 = true;

    if (!glb_bReadOnly) {
        if ((chkLocal.checked) && (fgLocal.Rows > 2))
            bState = false;

        if ((!bState) && (selCaixaID.value != 0))
            bState2 = false;
    }

    btnZerarCaixa.disabled = bState2;
    btnGravar.disabled = bState;
    btnDeletar.disabled = bState;
    btnLimpar.disabled = bState;
    selCaixaID.disabled = (selCaixaID.options.length == 0);
}

function btnSeparar_onClick() {
    var strPars = '?nPedidoID=' + escape(glb_nPedidoID) +
		'&nUsuarioID=' + escape(getCurrUserID()) +
		'&nProdutoID=' + escape(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ConceitoID'))) +
		'&bEstorno=' + escape(btnSeparar.getAttribute('estorno', 1));

    lockControlsInModalWin(true);
    dsoSeparar.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/separar.aspx' + strPars;
    dsoSeparar.ondatasetcomplete = dsoSeparar_DSC;
    dsoSeparar.refresh();
}

function dsoSeparar_DSC() {
    if (dsoSeparar.recordset.fields.count > 0) {
        if (!((dsoSeparar.recordset.BOF) && (dsoSeparar.recordset.EOF))) {
            if ((dsoSeparar.recordset['Resultado'].value != null) &&
				(dsoSeparar.recordset['Resultado'].value != ''))
                if (window.top.overflyGen.Alert(dsoSeparar.recordset['Resultado'].value) == 0)
                    return null;
        }
    }
    btnRefresh_onClick();
}

function btnSepararTodos_onClick() {
    var _retConf = window.top.overflyGen.Confirm('Tem certeza que deseja ' + btnSepararTodos.value + '?', 1);

    if (_retConf == 0)
        return null;
    else if (_retConf != 1)
        return null;

    var strPars = '?nPedidoID=' + escape(glb_nPedidoID) +
		'&nUsuarioID=' + escape(getCurrUserID()) +
		'&nProdutoID=' + escape(0) +
		'&bEstorno=' + escape(btnSepararTodos.getAttribute('estorno', 1));

    lockControlsInModalWin(true);
    dsoSeparar.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/separar.aspx' + strPars;
    dsoSeparar.ondatasetcomplete = dsoSepararTodos_DSC;
    dsoSeparar.refresh();
}

function dsoSepararTodos_DSC() {
    if (dsoSeparar.recordset.fields.count > 0) {
        if (!((dsoSeparar.recordset.BOF) && (dsoSeparar.recordset.EOF))) {
            if ((dsoSeparar.recordset['Resultado'].value != null) &&
				(dsoSeparar.recordset['Resultado'].value != ''))
                if (window.top.overflyGen.Alert(dsoSeparar.recordset['Resultado'].value) == 0)
                    return null;
        }
    }
    btnRefresh_onClick();
}

function fg_AfterRowColChange_NumSerie() {
    if (glb_GridIsBuilding)
        return true;

    fg_AfterRowColChange();

    setBtnsLabel();
}

function setBtnsLabel() {
    var i = 0;
    btnSeparar.value = 'Separar';
    btnSeparar.setAttribute('estorno', 0, 1);
    btnSepararTodos.value = 'Separar Todos';
    btnSepararTodos.setAttribute('estorno', 0, 1);

    if (fg.Row <= 1)
        return null;

    if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Colaborador')) != '') {
        btnSeparar.value = 'Estornar';
        btnSeparar.setAttribute('estorno', 1, 1);
    }

    for (i = 2; i < fg.Rows; i++) {
        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Colaborador')) != '') {
            btnSepararTodos.value = 'Estornar Todos';
            btnSepararTodos.setAttribute('estorno', 1, 1);
            break;
        }
    }
}

function btnPickingList_onClick() {
    if (fg.Rows <= 2)
        return null;

    var i = 0;
    var bOK = false;

    for (i = 2; i < fg.Rows; i++) {
        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'ColaboradorID')) != '')
            bOK = true;

        fg.RowHeight(i) = 400;
    }

    if (!bOK) {
        if (window.top.overflyGen.Alert('Separe pelo menos um item.') == 0)
            return null;

        return null;
    }

    var gridLine = fg.gridLines;
    fg.gridLines = 2;

    fg.Redraw = 0;
    fg.FontSize = '8';

    fg.ColHidden(getColIndexByColKey(fg, 'TipoGatilhamento')) = true;
    fg.ColHidden(getColIndexByColKey(fg, 'Controle')) = true;
    fg.ColHidden(getColIndexByColKey(fg, 'NumeroSerieEspecifico')) = true;
    fg.ColHidden(getColIndexByColKey(fg, 'EtiquetaPropria')) = true;
    fg.ColHidden(getColIndexByColKey(fg, 'Falta')) = true;

    fg.TextMatrix(1, getColIndexByColKey(fg, 'Colaborador')) = glb_sVendedor;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'Localizacao')) = 'PICKING LIST - ' + (glb_nTipoPedidoID == 601 ? 'ENTRADA' : 'SA�DA');

    fg.ColWidth(getColIndexByColKey(fg, 'ConceitoID')) = 10 * 90;
    fg.ColWidth(getColIndexByColKey(fg, 'DescricaoFiscal')) = 10 * 760;
    /*fg.ColWidth(getColIndexByColKey(fg, 'Conceito')) = 10 * 250;
    fg.ColWidth(getColIndexByColKey(fg, 'Modelo')) = 10 * 228;
    fg.ColWidth(getColIndexByColKey(fg, 'PartNumber')) = 10 * 155;*/
    fg.ColWidth(getColIndexByColKey(fg, 'ColaboradorID')) = 10 * 58;
    fg.ColWidth(getColIndexByColKey(fg, 'Colaborador')) = 10 * 130;
    fg.ColWidth(getColIndexByColKey(fg, 'Localizacao')) = 10 * 300;
    fg.ColWidth(getColIndexByColKey(fg, 'QuantidadeCaixas')) = 10 * 54;
    fg.ColWidth(getColIndexByColKey(fg, 'QuantidadeUnitaria')) = 10 * 54;
    fg.ColWidth(getColIndexByColKey(fg, 'Quantidade')) = 10 * 72;
    fg.ColWidth(getColIndexByColKey(fg, 'Falta')) = 10 * 87;

    fg.PrintGrid('Picking List', false, 2, 0, 450);
    fg.TextMatrix(1, getColIndexByColKey(fg, 'Localizacao')) = '';

    fg.ColHidden(getColIndexByColKey(fg, 'TipoGatilhamento')) = false;
    fg.ColHidden(getColIndexByColKey(fg, 'Controle')) = false;
    fg.ColHidden(getColIndexByColKey(fg, 'NumeroSerieEspecifico')) = false;
    fg.ColHidden(getColIndexByColKey(fg, 'EtiquetaPropria')) = false;
    fg.ColHidden(getColIndexByColKey(fg, 'Falta')) = false;

    for (i = 2; i < fg.Rows; i++)
        fg.RowHeight(i) = -1;

    fg.gridLines = gridLine;
    fg.FontSize = '8';
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 1;
}

function getCurrDate() {
    var d, s = "";
    d = new Date();

    if (DATE_SQL_PARAM == 103) {
        s += padL(d.getDate().toString(), 2, '0') + "/";
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
    }
    else {
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
        s += padL(d.getDate().toString(), 2, '0') + "/";
    }

    s += d.getYear() + " ";
    s += padL(d.getHours().toString(), 2, '0') + ":";
    s += padL(d.getMinutes().toString(), 2, '0');

    return (s);
}

function fillComboCaixa() {
    var strSQL = '';

    lockControlsInModalWin(true);

    setConnection(dsoCaixa);

    strSQL = 'SELECT 1 AS Indice, 0 AS CaixaID, SPACE(0) AS ProdutoCaixa, 0 AS ProdutoID ' +
		'UNION ALL ' +
		'SELECT 2 AS Indice, -b.ProdutoID AS CaixaID, (CONVERT(VARCHAR(10), b.ProdutoID) + CHAR(47) + ' + '\'' + '-' + '\'' + ' + CHAR(47) + CONVERT(VARCHAR(10), COUNT(*))) AS ProdutoCaixa, b.ProdutoID AS ProdutoID ' +
			'FROM NumerosSerie_Movimento a WITH(NOLOCK), NumerosSerie b WITH(NOLOCK) ' +
			'WHERE (a.PedidoID = ' + glb_nPedidoID + ' AND a.NumeroSerieID = b.NumeroSerieID AND b.CaixaID IS NULL) ' +
			'GROUP BY b.ProdutoID ' +
		'UNION ALL ' +
		'SELECT 3 AS Indice, b.CaixaID AS CaixaID, (CONVERT(VARCHAR(10), b.ProdutoID) + CHAR(47) + CONVERT(VARCHAR(100), dbo.fn_NumeroSerie_Caixa(b.CaixaID, ' + glb_nPedidoID + ', b.ProdutoID)) + CHAR(47) + CONVERT(VARCHAR(10), COUNT(*))) AS ProdutoCaixa, b.ProdutoID AS ProdutoID ' +
			'FROM NumerosSerie_Movimento a WITH(NOLOCK), NumerosSerie b WITH(NOLOCK) ' +
			'WHERE (a.PedidoID = ' + glb_nPedidoID + ' AND a.NumeroSerieID = b.NumeroSerieID AND b.CaixaID IS NOT NULL) ' +
			'GROUP BY b.ProdutoID, b.CaixaID ' +
			'ORDER BY Indice, ProdutoID, CaixaID, ProdutoCaixa';

    dsoCaixa.SQL = strSQL;
    dsoCaixa.ondatasetcomplete = dsoCaixa_DSC;
    dsoCaixa.Refresh();
}

function dsoCaixa_DSC() {
    var optionStr;
    var optionValue;

    clearComboEx(['selCaixaID']);

    while (!dsoCaixa.recordset.EOF) {
        optionStr = dsoCaixa.recordset['ProdutoCaixa'].value;
        optionValue = dsoCaixa.recordset['CaixaID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        oOption.ProdutoID = dsoCaixa.recordset['ProdutoID'].value;

        selCaixaID.add(oOption);

        dsoCaixa.recordset.MoveNext();
    }

    lockControlsInModalWin(false);
    setupInterfaceState();
}

function selCaixaID_onchange() {
    fillLocalGrid();
}

function fillLocalGrid() {
    var strSQL = '';
    var sFiltro = '';
    var nCaixaID = selCaixaID.value;
    var nProdutoID = 0;

    fgLocal.Rows = 1;

    if (nCaixaID != 0) {
        if (nCaixaID > 0)
            sFiltro = ' AND b.CaixaID = ' + nCaixaID + ' ';
        else {
            nProdutoID = Math.abs(nCaixaID);
            sFiltro = ' AND b.ProdutoID = ' + nProdutoID + ' AND b.CaixaID IS NULL ';
        }

        lockControlsInModalWin(true);

        setConnection(dsoCaixa);

        strSQL = 'SELECT 1 AS [RecNo], b.ProdutoID AS ProdutoID, b.NumeroSerie AS NumeroSerie, ' +
					'1 AS Quantidade, NULL AS Mensagem, 2 AS TipoEtiqueta, ' +
					'(CHAR(42) + CONVERT(VARCHAR(10), b.ProdutoID)) AS Produto, b.CaixaID ' +
				'FROM NumerosSerie_Movimento a WITH(NOLOCK), NumerosSerie b WITH(NOLOCK) ' +
				'WHERE (a.PedidoID = ' + glb_nPedidoID + ' AND ' +
					'a.NumeroSerieID = b.NumeroSerieID ' + sFiltro + ') ' +
				'ORDER BY b.NumeroSerie ';

        dsoCaixa.SQL = strSQL;
        dsoCaixa.ondatasetcomplete = fillLocalGrid_DSC;
        dsoCaixa.Refresh();
    }
    else
        setupInterfaceState();
}

function fillLocalGrid_DSC() {
    var i;
    var aHoldCols = new Array();

    aHoldCols = [5, 6, 7];

    startGridInterface(fgLocal);

    fgLocal.Cols = 1;
    fgLocal.ColWidth(0) = parseInt(divLocal.currentStyle.width, 10) * 18;

    headerGrid(fgLocal, ['Ordem', 'ID', 'N�mero de S�rie', 'Quant', 'Mensagem', '', '', ''], aHoldCols);

    fillGridMask(fgLocal, dsoCaixa, ['RecNo', 'ProdutoID', 'NumeroSerie', 'Quantidade', 'Mensagem', 'TipoEtiqueta', 'Produto', 'CaixaID'],
								['', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '']);

    recalcTotalGridLocal();
    alignColsInGrid(fgLocal, [0, 1, 3]);

    fgLocal.AutoSizeMode = 0;
    fgLocal.AutoSize(0, fgLocal.Cols - 1);

    lockControlsInModalWin(false);
    setupInterfaceState();
}
