/********************************************************************
modalcomissaovendas.js

Library javascript para o modalcomissaovendas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoGen01 = new CDatatransport('dsoGen01'); 

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    getDataInServer();
        
    // mostrar a janela movido para o final de preenchimento do grid
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', null );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Demonstrativo de comiss�o', 1);

    adjustElementsInForm([['lbltpComissao', 'seltpComissao', 20, 1, -5]], null, null, true);

    seltpComissao.onchange = getDataInServer;
    
    loadDataAndTreatInterface();    
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    //topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    topFree = 70;
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;
        
    // Ajusta botoes OK/Cancel para esta janela
    // O botao cancela nao e usado neste asp
    with (btnCanc.style)
    {
        visibility = 'hidden';
        width = 0;
        height = 0;
    }
    
    btnCanc.disabled = true;
    
    // Por default o botao OK vem destravado e muda de posicao
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
    // move o btnOK de posicao e muda seu caption
    btnOK.value = 'Fechar';
    
    with (btnOK.style)
    {
        left = (widthFree - parseInt(width, 10)) / 2;
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - 2 * ELEM_GAP;    
        height = parseInt(btnOK.currentStyle.top, 10) +
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10);
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServer()
{
    var nTpComissao = seltpComissao.value;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGen01);

    var strSQL = 'EXEC sp_Pedido_DemonstrativoComissaoVendas ' + glb_nPedidoID + ', NULL, ' + nTpComissao;

    dsoGen01.SQL = strSQL;
    dsoGen01.ondatasetcomplete = fillModalGrid_DSC;
    dsoGen01.Refresh();
}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGrid_DSC()
{
    var i, j, nSumImp;
    var nAliquota = 0;

    fg.Redraw = 0;

    if (seltpComissao.value == 641) 
    {
        headerGrid(fg, ['Descri��o',
                        'Valor cliente',
                        'Valor revenda',
                        'Comiss�o'], []);

        fillGridMask(fg, dsoGen01, ['Descricao',
                                    'ValorCliente',
							        'ValorRevenda',
							        'Comissao'],
                                      ['', '', '', ''],
                                      ['', '(###,###,##0.00)', '(###,###,##0.00)', '(###,###,##0.00)']);
    }
    else 
    {
        headerGrid(fg, ['Descri��o',
                        'Valor cliente',
                        'Valor interno',
                        'Comiss�o'], []);

        fillGridMask(fg, dsoGen01, ['Descricao',
                                    'ValorCliente',
							        'ValorInterno',
							        'Comissao'],
                                      ['', '', '', ''],
                                      ['', '(###,###,##0.00)', '(###,###,##0.00)', '(###,###,##0.00)']);
    
    
    }

    alignColsInGrid(fg, [1, 2, 3]);

    var j = 0;
    if (fg.Rows > 1) {
        for (i = 1; i < fg.Rows; i++) {
            for (j = 0; j < fg.Cols; j++) {
                if (fg.ValueMatrix(i, j) < 0) {
                    fg.Select(i, j, i, j);
                    fg.CellForeColor = 0X0000FF;
                }

                if (i == (fg.Rows - 1)) {
                    if ((j != 0) && (j != 3))
                        fg.TextMatrix(i, j) = '';

                    fg.Select(i, j, i, j);
                    fg.CellFontBold = true;
                    fg.Cell(6, i, j, i, j) = 0XDCDCDC;
                }
            }
        }
        fg.Row = 1;
    }

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    fg.Redraw = 2;
            
    with (modalcomissaovendasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
               
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();
}
