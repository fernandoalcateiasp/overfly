
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modaldespachoHtml" name="modaldespachoHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modaldespacho.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
 
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/relatoriominuta.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modaldespacho.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nEmpresaID, nUserID, sEmpresaFantasia

sCaller = ""
nEmpresaID = 0
nUserID = 0
sEmpresaFantasia = ""

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

For i = 1 To Request.QueryString("sEmpresaFantasia").Count    
    sEmpresaFantasia = Request.QueryString("sEmpresaFantasia")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"

Response.Write "var glb_nUserID = " & CStr(nUserID) & ";"

Response.Write "var glb_sEmpresaFantasia = " & Chr(39) & CStr(sEmpresaFantasia) & Chr(39) & ";"

Response.Write "var glb_bDireitoDepacho = false;"

Response.Write vbcrlf

Dim rsData, rsData1, strSQL

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT COUNT(*) AS DireitoDepacho FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), Recursos_Direitos c WITH(NOLOCK) " & _
			"WHERE (a.SujeitoID = " & CStr(nUserID) & " AND a.ObjetoID = 999 AND a.TipoRelacaoID = 11 AND a.EstadoID = 2 AND " & _
				"a.RelacaoID = b.RelacaoID AND b.EmpresaID = " & CStr(nEmpresaID) & " AND b.PerfilID = c.PerfilID AND " & _
				"c.ContextoID = 5112 AND c.RecursoMaeID = 24000 AND c.RecursoID = 29 AND c.EstadoParaID = 31 AND " & _
				"c.Alterar1 = 1 AND c.Alterar2 = 1) "

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Do While (Not rsData.EOF)
	If (CInt(rsData.Fields("DireitoDepacho").Value) > 0) Then
		Response.Write "glb_bDireitoDepacho = true;"
	End If
	Exit Do
Loop

rsData.Close
Set rsData = Nothing
	
Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modaldespacho_ValidateEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modaldespacho_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modaldespachoKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modaldespacho_BeforeEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modaldespachoDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modaldespachoBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Atualizar linha de mensagens
js_fg_AfterRowColmodaldespacho (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>

<script ID="vbFunctions" LANGUAGE="vbscript">
<!--

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

//-->
</script>

</head>

<body id="modaldespachoBody" name="modaldespachoBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <!-- Div Controles -->
    <div id="divControls" name="divControls" class="divGeneral">
		<p id="lblDespacho" name="lblDespacho" class="lblGeneral">D</p>
		<input type="checkbox" id="chkDespacho" name="chkDespacho" class="fldGeneral" title="Despachar mercadorias ou acusar chegada de portador?">
		<p id="lblEstados" name="lblEstados" class="lblGeneral">E</p>
		<input type="checkbox" id="chkEstados" name="chkEstados" class="fldGeneral" title="Todos os estados pendentes?">
		<p id="lblTransportadora" name="lblTransportadora" class="lblGeneral">T</p>
		<input type="checkbox" id="chkTransportadora" name="chkTransportadora" class="fldGeneral" title="Transportadora ou cliente?">
        <p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">Pessoa</p>
        <select id="selPessoaID" name="selPessoaID" class="fldGeneral"></select>
        <p id="lblDepositoID" name="lblDepositoID" class="lblGeneral">Dep�sito</p>
        <select id="selDepositoID" name="selDepositoID" class="fldGeneral">
        
<%

	Set rsData1 = Server.CreateObject("ADODB.Recordset")

    strSQL = "EXEC sp_Empresa_Depositos " & CStr(nEmpresaID) & ", NULL, NULL"

	rsData1.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    Response.Write( "<option value=0>Todos</option>" )

	If (Not (rsData1.BOF AND rsData1.EOF) ) Then

		While Not (rsData1.EOF)

			Response.Write( "<option value='" & rsData1.Fields("fldID").Value & "'>" )
			Response.Write( rsData1.Fields("fldName").Value & "</option>" )

			rsData1.MoveNext()
			
		WEnd

	End If

	rsData1.Close
	Set rsData1 = Nothing
%>			    
        
        </select>
        <p id="lblPortador" name="lblPortador" class="lblGeneral">Portador</p>
        <input type="text" id="txtPortador" name="txtPortador" class="fldGeneral">
        <p id="lblDocumentoEstadualPortador" name="lblDocumentoEstadualPortador" class="lblGeneral">Doc Est Portador</p>
        <input type="text" id="txtDocumentoEstadualPortador" name="txtDocumentoEstadualPortador" class="fldGeneral">

        <p id="lblDocumentoFederalPortador" name="lblDocumentoFederalPortador" class="lblGeneral">Doc Fed Portador</p>
        <input type="text" id="txtDocumentoFederalPortador" name="txtDocumentoFederalPortador" class="fldGeneral">
        
        <p id="lblFiltro" name="lblFiltro" class="lblGeneral">Filtro</p>
        <input type="text" id="TextFiltro" name="txtFiltro" class="fldGeneral">      

        <p id="lblConhecimento" name="lblConhecimento" class="lblGeneral">Conhecimento</p>
        <input type="text" id="txtConhecimento" name="txtConhecimento" class="fldGeneral">
    </div>    

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
            
	<input type="button" id="btnFillGrid" name="btnFillGrid" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnOK" name="btnOK" value="Despachar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
         
    <iframe id="frmReport" name="frmReport" style="display:none"  frameborder="no"></iframe>

</body>

</html>
