
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
	strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalCampanhasHtml" name="modalCampanhasHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalCampanhas.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalCampanhas.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nUserID, nPedidoID, nEstadoID, nParceiroID, sCaller
Dim strSQL, rsData

nUserID = 0
nPedidoID = 0
nEstadoID = 0
nParceiroID = 0
sCaller = ""


For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

Response.Write "var glb_nUserID = " & CStr(nUserID) & ";"

For i = 1 To Request.QueryString("nPedidoID").Count    
    nPedidoID = Request.QueryString("nPedidoID")(i)
Next

Response.Write "var glb_nPedidoID = " & CStr(nPedidoID) & ";"

For i = 1 To Request.QueryString("nEstadoID").Count    
    nEstadoID = Request.QueryString("nEstadoID")(i)
Next

Response.Write "var glb_nEstadoID = " & CStr(nEstadoID) & ";"

For i = 1 To Request.QueryString("nParceiroID").Count    
    nParceiroID = Request.QueryString("nParceiroID")(i)
Next

Response.Write "var glb_nParceiroID = " & CStr(nParceiroID) & ";"

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fgAssociadas -->
<script LANGUAGE=javascript FOR=fgAssociadas EVENT=BeforeSort>
<!--
fg_ModConcExtrBeforeSort(fgAssociadas, arguments[0]);
//-->
</script>
<script LANGUAGE=javascript FOR=fgAssociadas EVENT=AfterSort>
<!--
fg_ModConcExtrAfterSort(fgAssociadas, arguments[0]);
//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=fgAssociadas EVENT=AfterRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
js_fg_AfterRowColChange (fgAssociadas, arguments[0], arguments[1], arguments[2], arguments[3]);
fg_modalCampanhas_AfterRowColChange (fgAssociadas);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fgAssociadas EVENT=BeforeRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
js_fg_BeforeRowColChange (fgAssociadas, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgAssociadas EVENT=AfterEdit>
<!--
	fgAssociadas_AfterEdit(fgAssociadas, fgAssociadas.Row, fgAssociadas.Col);	
//-->
</SCRIPT>

<!-- fgAssociadas -->
<script LANGUAGE=javascript FOR=fgCampanhas EVENT=BeforeSort>
<!--
fg_ModConcExtrBeforeSort(fgCampanhas, arguments[0]);
//-->
</script>

<script LANGUAGE=javascript FOR=fgCampanhas EVENT=AfterSort>
<!--
fg_ModConcExtrAfterSort(fgCampanhas, arguments[0]);
//-->
</script>
<SCRIPT LANGUAGE=javascript FOR=fgCampanhas EVENT=AfterRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fgCampanhas EVENT=BeforeRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
js_fg_BeforeRowColChange (fgCampanhas, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgCampanhas EVENT=EnterCell>
<!--
 js_fg_EnterCell (fgCampanhas);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgCampanhas EVENT=AfterEdit>
<!--
	fg_modalCampanhas_AfterEdit(fgCampanhas, fgCampanhas.Row, fgCampanhas.Col)
//-->
</SCRIPT>

</head>

<body id="modalCampanhasBody" name="modalCampanhasBody" LANGUAGE="javascript" onload="return window_onload()">
   <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
    <!-- Div dos Controles -->
    <div id="divControles" name="divControles" class="divGeneral">
		<p id="lblIncluir" name="lblIncluir" class="lblGeneral">Incluir</p>
		<input type="checkbox" id="chkIncluir" name="chkIncluir" class="fldGeneral"></input>
        <p id="lblTipoCampanha" name="lblTipoCampanha" class="lblGeneral">Tipo Campanha</p>
        <select id="selTipoCampanha" name="selTipoCampanha" class="fldGeneral">
<%
	Set rsData = Server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT 0 AS Ordem, 0 AS fldID, SPACE(0) AS fldName " & _
		"UNION ALL " & _
		"SELECT Ordem, ItemID AS fldID, ItemMasculino AS fldName " & _
		"FROM tiposauxiliares_itens WITH(NOLOCK) " & _
		"WHERE Tipoid=812 and Aplicar=1 and Estadoid=2 " & _
		"ORDER BY Ordem"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	While (Not rsData.EOF)
		Response.Write "<option value =" & rsData.Fields("fldID").Value 
		Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & chr(13) & chr(10)

		rsData.MoveNext
	WEnd

	rsData.Close
	Set rsData = Nothing
%>		
</select>

		<p id="lblCusto" name="lblCusto" class="lblGeneral">Custo</p>
		<input type="checkbox" id="chkCusto" name="chkCusto" class="fldGeneral" title="DPA depende do custo do produto?"></input>
		<p id="lblCliente" name="lblCliente" class="lblGeneral">Cliente</p>
		<input type="checkbox" id="chkCliente" name="chkCliente" class="fldGeneral" title="Cliente"></input>

		<input type="button" id="btnDissociar" name="btnDissociar" value="Dissociar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
		<input type="button" id="btnAprovar" name="btnAprovar" value="Aprovar" aprova="0" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
		<input type="button" id="btnFillGrid" name="btnFillGrid" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
		<input type="button" id="btnAssociar" name="btnAssociar" value="Associar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
    </div>
    
    <!-- Div do grid -->
    <div id="divFGAssociadas" name="divFGAssociadas" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgAssociadas" name="fgAssociadas" VIEWASTEXT>
        </object>
    </div>    
            
    <!-- Div do grid -->
    <div id="divFGCampanhas" name="divFGCampanhas" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgCampanhas" name="fgCampanhas" VIEWASTEXT>
        </object>
    </div>    

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
</body>

</html>
