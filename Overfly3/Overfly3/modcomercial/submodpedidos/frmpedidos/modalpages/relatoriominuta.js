function relatorioMinutaTransporte(nMinuta,Formato,Modal) //1 - modalprint_pedidos 2 - modaldespacho
{
    var strSQL = '';
	var strSQL2 = '';
	var strDataInicio = '';
	var strDataFim = '';
	var strParams ='';
    var sTitle = '';
    var nMinutaOriginal = nMinuta;
 
    	
	if(nMinutaOriginal == null)
	{
		if (selMinuta.selectedIndex < 0)
		{
			if ( window.top.overflyGen.Alert('Selecione uma minuta.') == 0 )
				return null;

			lockControlsInModalWin(false);
			return null;
		}
		if (chkDataEx(txtdtInicio.value) == false)
		{
			lockControlsInModalWin(false);
			return null;
		}
		
		if (chkDataEx(txtdtFim.value) == false)
		{
			lockControlsInModalWin(false);
			return null;
		}

		strDataInicio = putDateInMMDDYYYY2(txtdtInicio.value);
		strDataFim = putDateInMMDDYYYY2(txtdtFim.value);
		
		if (strDataInicio != '')
			strDataInicio = ' AND b.dtEntradaSaida >= ' + '\'' + strDataInicio + '\'' + ' ';

		if (strDataFim != '')
			strDataFim = ' AND b.dtEntradaSaida <= ' + '\'' + strDataFim + ' 23:59:59' + '\'' + ' ';
		
		nMinuta = selMinuta.value;
	}

	
	strParams += 'Minuta ' + nMinuta;

	if (nMinutaOriginal==null)
	{
		if (trimStr(txtdtInicio.value) != '')
			strParams += 'In�cio ' + txtdtInicio.value + '   ';

		if (trimStr(txtdtFim.value) != '')
			strParams += 'Fim ' + txtdtFim.value + '   ';
		sTitle = selReports.options[selReports.selectedIndex].innerText;
	}
	else
		sTitle = 'Minuta de Transporte';

	var nEmpresaLogada = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

	if (Formato == null) {
	    Formato = 1;
	}
   

	var strParameters = "RelatorioID=" + "40146" + "&nEmpresaLogada=" + nEmpresaLogada[0] + "&glb_sEmpresaFantasia=" + nEmpresaLogada[3] + "&Formato=" + Formato +
                        "&nMinuta=" + nMinuta + "&strParams=" + strParams + "&strDataInicio=" + strDataInicio + "&strDataFim=" + strDataFim + "&sTitle=" + sTitle;

	if(Modal == 1){
	    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
	    lockControlsInModalWin(true);

	    var frameReport = document.getElementById("frmReport");
	    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
	    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modcomercial/submodpedidos/frmpedidos/serverside/Reports.aspx?" + strParameters;

	    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
	}
	else {
	    var frameReport = document.getElementById("frmReport");
	    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modcomercial/submodpedidos/frmpedidos/serverside/Reports.aspx?" + strParameters;
	}
}
