/********************************************************************
modalprogramacaoentrega.js
Segundo o Marcio essa modal n�o utilizada. A Delta esta fazendo a tarefa de programa��o. DCS 04/07/2008
Library javascript para o modalprogramacaoentrega.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dsoGrid = new CDatatransport('dsoGrid');
var dsoDetalhamento = new CDatatransport('dsoDetalhamento');
var dsoCmbPessoas = new CDatatransport('dsoCmbPessoas');
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
getCurrEmpresaData()
fillGridData()
fillGridData_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalprogramacaoentrega.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

js_fg_DblClick()

FINAL DE DEFINIDAS NO ARQUIVO modalprogramacaoentrega.ASP
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    
    // ajusta o body do html
    with (modalprogramacaoentregaBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
    with (btnCanc.style)
    {
		left = (parseInt(divFG.style.width,10) / 2) - (parseInt(btnCanc.style.width,10) / 2);
		visibility = 'hidden';
	}
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Programa��o de Entrega', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
	
	adjustElementsInForm([['lblInicio','txtInicio',17,1,-10,-10],
						  ['lblFim','txtFim',17,1],
						  ['lblIncremento','txtIncremento',10,1],
						  ['lblPessoa','selPessoa',22,1],
						  ['btnFindPessoa','btn',24,1],
						  ['btnFillGrid','btn',btnOK.offsetWidth,1, 10],
						  ['lblFiltro','txtFiltro',93,2,-10]],null,null,true);
	
	txtInicio.value = glb_dCurrDate + ' 08:00';
	txtFim.value = glb_dCurrDate + ' 18:00';
	txtIncremento.value = '10';
	btnFillGrid.style.height = btnOK.offsetHeight;
	
    txtIncremento.onkeypress = verifyNumericEnterNotLinked;
    txtIncremento.setAttribute('thePrecision', 3, 1);
    txtIncremento.setAttribute('theScale', 0, 1);
    txtIncremento.setAttribute('minMax', new Array(0, 999), 1);
    txtIncremento.setAttribute('verifyNumPaste', 1);
    txtIncremento.onfocus = selFieldContent;
    
    txtInicio.onfocus = selFieldContent;
    txtInicio.maxLength = 16;

    txtFim.onfocus = selFieldContent;
    txtFim.maxLength = 16;
    
    selPessoa.disabled = true;
    selPessoa.onchange = selPessoa_onchange;

	// ajusta o divControles
	with (divControles.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(txtFiltro.currentStyle.top, 10) + parseInt(txtFiltro.currentStyle.height, 10);
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControles.currentStyle.top, 10) +
			  parseInt(divControles.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) -
				 parseInt(top, 10);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // reajusta dimensoes e reposiciona a janela
    (getFrameInHtmlTop( getExtFrameID(window))).style.top = 
		(getFrameInHtmlTop( 'frameSup01')).offsetTop;
		
	redimAndReposicionModalWin(modWidth, modHeight, false);
    
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    
    with (fg)
    {
        FontName = 'Tahoma, Arial, MS SansSerif';
        FontSize = '10';
   		Editable = false;
		AllowUserResizing = 1;
		VirtualData = true;
		Ellipsis = 1;
		SelectionMode = 1;
		AllowSelection = false;
		Rows = 1;
		Cols = 2;
		FixedRows = 1;
		FixedCols = 1;
		ScrollBars = 3;
		OutLineBar = 5;
		GridLines = 0;
		FormatString = 'Descri��o' + '\t' + 'Ped' + '\t' + 'Peso' + '\t' + 'Cub' + '\t' + 'Pl' + '\t' + 'Cx' + '\t' + 
			'Un' + '\t' + 'Previs�o Entrega' + '\t' + 'Fim';
		OutLineBar = 1;
        GridLinesFixed = 13;
		GridLines = 1;
		GridColor = 0X000000;
		ColKey(0) = 'Descricao';
		ColKey(1) = 'Pedidos';
		ColKey(2) = 'Peso';
		ColKey(3) = 'Cubagem';
		ColKey(4) = 'Paletes';
		ColKey(5) = 'Caixas';
		ColKey(6) = 'Unidades';
		ColKey(7) = 'dtPrevisaoEntrega';
		ColKey(8) = 'dtFim';
    }
    
    fg.Redraw = 2;

	showExtFrame(window, true);
	btnFindPessoa.src = glb_LUPA_IMAGES[0].src;

	window.focus();
	txtInicio.focus();
}

function selPessoa_onchange()
{
	fg.Rows = 1;
	adjustLabelsCombos();
}

function adjustLabelsCombos()
{
	if (selPessoa.selectedIndex <= 0)
		setLabelOfControl(lblPessoa, '');
	else
		setLabelOfControl(lblPessoa, selPessoa.value);
}

/********************************************************************
Enter no campo data
********************************************************************/
function txtData_onKeyPress()
{
    if ( event.keyCode == 13 )
        btn_onclick(btnFillGrid);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		;
    }
    else if (controlID == 'btnCanc')
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }    
    else if (controlID == 'btnFillGrid')
		btnFillGrid_onclick();
}

function btnFillGrid_onclick()
{
	fg.Rows = 1;
    fillGridData();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    if (txtInicio.value != '')
    {
        if ( !chkDataEx(txtInicio.value) )
        {
            if ( window.top.overflyGen.Alert ('Data inicial inv�lida') == 0 )
                return null;
                
            txtInicio.focus();
            return null;
        }
    }

    if (txtFim.value != '')
    {
        if ( !chkDataEx(txtFim.value) )
        {
            if ( window.top.overflyGen.Alert ('Data final inv�lida') == 0 )
                return null;

            txtFim.focus();
            return null;
        }
    }

    lockControlsInModalWin(true);

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    var strSQL = '';

	strSQL = 'EXEC sp_Pedido_ProgramacaoEntrega ' + glb_aEmpresaData[0] + ','+ 
		'\'' + putDateInMMDDYYYY(trimStr(txtInicio.value)) + '\'' + ' , ' +
		'\'' + putDateInMMDDYYYY(trimStr(txtFim.value)) + '\'' + ', ' +
		txtIncremento.value + ', ' +
		(selPessoa.selectedIndex == -1 ? 'NULL' : selPessoa.value) + ', 2';

    dsoGrid.SQL = strSQL;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
    var dTFormat = '';
    var i, j;
    var bgColorGreen, bgColorYellow, bgColorRed;
    var nTipoDetalhamentoID = 0;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    
	if ( !(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) ) 
	{
		glb_GridIsBuilding = true;
		while (!dsoGrid.recordset.EOF)
		{
			fg.Row = fg.Rows - 1;

			fg.AddItem('', fg.Row+1);
			
			if (fg.Row < (fg.Rows-1))
				fg.Row++;
			

			fg.ColDataType(getColIndexByColKey(fg, 'Descricao')) = 12;
			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Descricao')) = dsoGrid.recordset['Descricao'].value;	
			
			fg.ColDataType(getColIndexByColKey(fg, 'Pedidos')) = 12;
			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Pedidos')) = dsoGrid.recordset['Pedidos'].value;

			if (dsoGrid.recordset['Peso'].value != null)
			{
				fg.ColDataType(getColIndexByColKey(fg, 'Peso')) = 12;
				fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Peso')) = dsoGrid.recordset['Peso'].value;	
			}
			
			if (dsoGrid.recordset['Cubagem'].value != null)
			{
				fg.ColDataType(getColIndexByColKey(fg, 'Cubagem')) = 12;
				fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Cubagem')) = dsoGrid.recordset['Cubagem'].value;	
			}

			if (dsoGrid.recordset['Paletes'].value != null)
			{
				fg.ColDataType(getColIndexByColKey(fg, 'Paletes')) = 12;
				fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Paletes')) = dsoGrid.recordset['Paletes'].value;	
			}

			if (dsoGrid.recordset['Caixas'].value != null)
			{
				fg.ColDataType(getColIndexByColKey(fg, 'Caixas')) = 12;
				fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Caixas')) = dsoGrid.recordset['Caixas'].value;	
			}

			if (dsoGrid.recordset['Unidades'].value != null)
			{
				fg.ColDataType(getColIndexByColKey(fg, 'Unidades')) = 12;
				fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Unidades')) = dsoGrid.recordset['Unidades'].value;	
			}

			if (dsoGrid.recordset['dtPrevisaoEntrega'].value != null)
			{
				fg.ColDataType(getColIndexByColKey(fg, 'dtPrevisaoEntrega')) = 12;
				fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'dtPrevisaoEntrega')) = dsoGrid.recordset['dtPrevisaoEntrega'].value;	
			}

			if (dsoGrid.recordset['dtFim'].value != null)
			{
				fg.ColDataType(getColIndexByColKey(fg, 'dtFim')) = 12;
				fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'dtFim')) = dsoGrid.recordset['dtFim'].value;	
			}
	
			fg.IsSubTotal(fg.Row) = true;
			fg.RowOutlineLevel(fg.Row) = dsoGrid.recordset['Nivel'].value - 1;
			
			dsoGrid.recordset.MoveNext();
		}
	}

    putMasksInGrid(fg, ['','99999','99999999.99','99999.99999999','99999','99999','99999','99/99/9999','99/99/9999'], 
                       ['','#####','###,###,##0.00','####0.00000000','#####','#####','#####',dTFormat  + ' hh:mm',dTFormat  + ' hh:mm']);
                       
	alignColsInGrid(fg,[1,2,3,4,5,6]);

	glb_GridIsBuilding = false;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( fg.Rows > 1 )
        fg.Row = 1;
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;
    fg.Outline(3);
    
    lockControlsInModalWin(false);

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }
    else
    {
        ;
    }            
}

function js_fg_DblClick()
{
	var node = fg.GetNode();

	try
	{
		node.Expanded = !node.Expanded;
	}	
	catch(e)
	{
		;	
	}	
}

function fg_modalprogramacaoentrega_AfterRowColChange()
{
    if ( glb_GridIsBuilding )
        return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
    if (btnFindPessoa.src == glb_LUPA_IMAGES[1].src)
        return;
	
	loadCmbPessoa();
}

function loadCmbPessoa()
{
    if (txtInicio.value != '')
    {
        if ( !chkDataEx(txtInicio.value) )
        {
            if ( window.top.overflyGen.Alert ('Data inicial inv�lida') == 0 )
                return null;
                
            txtInicio.focus();
            return null;
        }
    }

    if (txtFim.value != '')
    {
        if ( !chkDataEx(txtFim.value) )
        {
            if ( window.top.overflyGen.Alert ('Data final inv�lida') == 0 )
                return null;

            txtFim.focus();
            return null;
        }
    }
	
	lockControlsInModalWin(true);
    setConnection(dsoCmbPessoas);
	
	dsoCmbPessoas.SQL = 'EXEC sp_Pedido_ProgramacaoEntrega ' + glb_aEmpresaData[0] + ',' + 
		'\'' + putDateInMMDDYYYY(trimStr(txtInicio.value)) + '\'' + ' , ' +
		'\'' + putDateInMMDDYYYY(trimStr(txtFim.value)) + '\'' + ', ' +
		(trimStr(txtIncremento.value) == '' ? 'NULL' : txtIncremento.value) + ', NULL, 1';

    dsoCmbPessoas.ondatasetcomplete = loadCmbPessoa_DSC;
	dsoCmbPessoas.Refresh();
}

function loadCmbPessoa_DSC()
{
	clearComboEx(['selPessoa']);

	if (! ((dsoCmbPessoas.recordset.BOF)&&(dsoCmbPessoas.recordset.EOF)) )
	{
		optionStr = '';
		optionValue = 0;
		var oOption = document.createElement("OPTION");
		oOption.text = optionStr;
		oOption.value = optionValue;
		selPessoa.add(oOption);

		dsoCmbPessoas.recordset.MoveFirst();
		while (! dsoCmbPessoas.recordset.EOF )
		{
			optionStr = dsoCmbPessoas.recordset['Pessoa'].value;
			optionValue = dsoCmbPessoas.recordset['PessoaID'].value;
			var oOption = document.createElement("OPTION");
			oOption.text = optionStr;
			oOption.value = optionValue;
			selPessoa.add(oOption);
			dsoCmbPessoas.recordset.MoveNext();
		}
	}

	lockControlsInModalWin(false);
	if (selPessoa.options.length == 2)
		selPessoa.selectedIndex = 1;
	else	
		selPessoa.selectedIndex = 0;
		
	selPessoa.disabled = (selPessoa.options.length == 0);
	adjustLabelsCombos();
}
