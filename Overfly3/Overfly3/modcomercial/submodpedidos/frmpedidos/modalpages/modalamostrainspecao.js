/********************************************************************
modalamostrainspecao.js

Library javascript para o modalamostrainspecao.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Controla se as colunas OK e Observacoes devem ser readOnly e funcao
// do Estado da OP

var glb_sLockFields = '';
var glb_desgatilha = 0;
var glb_timerInterval = null;
var glb_nDSOs = 0;
var glb_bCarregamento = true;

var glb_divGridRetractedHeight = 0;
var glb_divGridExtendedHeight = 0;

var glb_bFirstFill = false;
var glb_AmostraInspecaoTimerInt = null;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;


var dsoGen01 = new CDatatransport('dsoGen01');
var dsoGen02 = new CDatatransport('dsoGen02');
var dsoGen03 = new CDatatransport('dsoGen03');
var dsoGatilhamento = new CDatatransport('dsoGatilhamento');
var dsoCmbResultadoID = new CDatatransport('dsoCmbResultadoID');
var dsoDadosInspecao = new CDatatransport('dsoDadosInspecao');
 

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_pedidos_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 0;
    
    if ( glb_totalCols__ != false )
        firstLine = 1;
        
    if (NewRow > firstLine)
    {
        fg.Row = NewRow;
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalamostrainspecao_AfterEdit(Row, Col)
{
    if (fg.Editable)
    {
        if (fg.col == 5)
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalamostrainspecao_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGen01, grid.ColKey(col));
    
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    var rectModal;
    
    window_onload_1stPart();
	glb_sLockFields = (glb_nPedidoEstadoID != 28 ? '*' : '');

    // configuracao inicial do html
    setupPage();
    
    // Move a janela alguns pixels para baixo
    rectModal = getRectFrameInHtmlTop('frameModal');
    
    if ( rectModal )    
    {
		rectModal[1] += 10;
		rectModal[3] -= 6;
		
        moveFrameInHtmlTop('frameModal', rectModal);
    }

    // ajusta o body do html
    with (modalamostrainspecaoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
	startPesq();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    // usuario clicou o boptao que chaveia interface
    if (ctl.id == btnChangeInterface.id )
    {
		changeInterfaceMode(ctl, ctl.getAttribute('interfaceMode', 1));
    }
    else if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );
	else if (ctl.id == btnDesgatilhar.id )
	{
		var _retConf = window.top.overflyGen.Confirm('Deseja desgatilha todos os n�meros de s�rie deste RIR?', 1);

		if ( _retConf == 1)
		{
			lockControlsInModalWin(true);
			glb_desgatilha = 2;
			gatilhaNumeroSerie();
		}
		else
		{
			lockControlsInModalWin(false);
		}
	}	
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Amostra da Inspe��o', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

 	// ajusta elementos no divFields
    adjustElementsInForm([['lblRIRID','txtRIRID', 10, 1, -10, -10],
						  ['lblProdutoID', 'txtProdutoID', 10, 1],
						  ['lblEstadoID','txtEstadoID', 2, 1],
						  ['lblProduto','txtProduto', 25, 1, -3],
						  ['lblNumeroSerie','txtNumeroSerie', 20, 1],
						  ['lblQuantidade', 'txtQuantidade', 5, 1],
						  ['lblFalta', 'txtFalta', 5, 1]],null,null,true);

	txtRIRID.value = glb_nPedRIRID;
	txtQuantidade.value = glb_nQuantidade;
	txtProdutoID.value = glb_nProdutoID;
	txtEstadoID.value = glb_sEstadoProduto;
	txtProduto.value = glb_sProduto;

	txtRIRID.readOnly = true;
	txtQuantidade.readOnly = true;
	txtFalta.readOnly = true;
	txtProdutoID.readOnly = true;
	txtEstadoID.readOnly = true;
	txtProduto.readOnly = true;
	txtNumeroSerie.maxLength = 20;
	txtNumeroSerie.onkeyup = txtNumeroSerie_onkeyup;
	txtNumeroSerie.onfocus = selFieldContent;

    // ajusta o divFields
    elem = window.document.getElementById('divFields');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - (2 * ELEM_GAP);

        // altura e fixa para tres linhas de campos
        height = parseInt(txtRIRID.currentStyle.top, 10) + 
				 parseInt(txtRIRID.currentStyle.height, 10) + ELEM_GAP;

        y_gap = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        // top = topFree + ELEM_GAP;
        top = y_gap;
        width = modWidth - frameBorder - (2 * ELEM_GAP);    
        //height = parseInt(btnOK.currentStyle.top, 10) -
        //         parseInt(top, 10) - frameBorder - ELEM_GAP;
        
        height = 92;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    btnOK.value = 'Gravar';
    btnDesgatilhar.style.height = parseInt(btnOK.style.height);
    btnDesgatilhar.style.width = parseInt(btnOK.style.width);

	btnOK.style.left = (parseInt(document.getElementById('divFG').currentStyle.width,10) /2) -
					   (parseInt(btnOK.style.width, 10) / 2);
					   
	btnDesgatilhar.style.top = parseInt(btnOK.style.top,10);
	btnDesgatilhar.style.left = parseInt(btnOK.style.left,10) - ELEM_GAP - parseInt(btnDesgatilhar.style.width, 10);
	
	btnCanc.style.left = parseInt(btnOK.style.left,10) + ELEM_GAP + parseInt(btnOK.style.width, 10);
	
	btnChangeInterface.style.top = parseInt(btnOK.style.top,10);
	btnChangeInterface.style.left = ELEM_GAP;
						   
	// ajusta o divDadosInspecao
    elem = window.document.getElementById('divDadosParaInsp');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';

        left = parseInt(divFG.currentStyle.left, 10);
        top = parseInt(divFG.currentStyle.top, 10) +
              parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;
        width = parseInt(divFG.currentStyle.width, 10);    
        
        height = parseInt(btnOK.currentStyle.top, 10) -
                 parseInt(top, 10) - frameBorder - ELEM_GAP;
    }
    
    elem = window.document.getElementById('lblDadosParaInsp');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = FONT_WIDTH * elem.innerText.length;    

		backgroundColor = 'transparent';        
    }
    
    elem = window.document.getElementById('txtDadosParaInsp');
    with (elem.style)
    {
        left = 0;
        top = parseInt(lblDadosParaInsp.currentStyle.top, 10) +
              parseInt(lblDadosParaInsp.currentStyle.height, 10);
        width = parseInt(divDadosParaInsp.currentStyle.width, 10);
        height = parseInt(divDadosParaInsp.currentStyle.height, 10) - 
                 parseInt(elem.currentStyle.top, 10);

		backgroundColor = 'transparent';        
		fontFamily = 'Courier New';
        fontSize = '10pt';   
    }
	elem.readOnly = true;
	
	glb_divGridRetractedHeight = parseInt(divFG.currentStyle.height, 10);
	glb_divGridExtendedHeight = parseInt(divDadosParaInsp.currentStyle.top, 10) + 
	                            parseInt(divDadosParaInsp.currentStyle.height, 10) -
	                            parseInt(divFG.currentStyle.top, 10);
					   
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

function txtNumeroSerie_onkeyup()
{
    if ( event.keyCode == 13 )
    {
		glb_desgatilha = 0;
        gatilhaNumeroSerie();
    }
    
    return null;
}

function gatilhaNumeroSerie()
{
	if ( (trimStr(txtNumeroSerie.value) == '') && (glb_desgatilha != 2))
		return null;
	
	if ( glb_timerInterval != null )
	{
		window.clearInterval(glb_timerInterval);
		glb_timerInterval = null;
	}

    var strPars = new String();
    var i, j;

    lockControlsInModalWin(true);
    strPars = '?nPedRIRID=' + escape(glb_nPedRIRID);
    strPars += '&sNumeroSerie=' + escape(txtNumeroSerie.value);
    strPars += '&nDesgatilha=' + escape(glb_desgatilha);

    // pagina asp no servidor saveitemspedido.asp
    dsoGatilhamento.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/gatilhamento.aspx' + strPars;
    dsoGatilhamento.ondatasetcomplete = dsoGatilhamento_DSC;
    dsoGatilhamento.refresh();
}

function dsoGatilhamento_DSC()
{
	lockControlsInModalWin(false);
	
	if ( !((dsoGatilhamento.recordset.BOF) && (dsoGatilhamento.recordset.EOF)) )
	{
		if (dsoGatilhamento.recordset['Mensagem'].value != null)
		{
			if (dsoGatilhamento.recordset['Mensagem'].value == 'NS')
			{
				window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);

				var _retConf = window.top.overflyGen.Confirm('N�mero de S�rie j� associado a este RIR.\nDeseja retornar?', 1);
				    
				if ( _retConf == 0 )
				    return null;
				else if ( _retConf == 1)
				{
				    glb_desgatilha = 1;
					glb_timerInterval = window.setInterval('gatilhaNumeroSerie()', 10, 'JavaScript');
				}
				
				window.focus();
				txtNumeroSerie.focus();    
				
				return null;
			}
			else if ( window.top.overflyGen.Alert(dsoGatilhamento.recordset['Mensagem'].value) ==0 )
                return null;
                
            window.focus();
            txtNumeroSerie.focus();    
		}
		else
			startPesq();
	}
	else
		startPesq();
	
	return null;
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;

    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();

    return true;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    // 1. O usuario clicou o botao OK
    //sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , null );

    var i, j, nNumeroItens;
    var aDataToSave = new Array();
    var sObs = '';
    var nResultadoID = '';
    nNumeroItens = 0;
    j = 0;

    // Verifica coluna de quantidades do grid
    for ( i=1; i<fg.Rows; i++)
    {
        nNumeroItens++;
		nVariacaoItem = 0;
		sObs = trimStr(getCellValueByColKey(fg, 'Observacao' + glb_sLockFields, i));
		sObs = (sObs == '' ? 'NULL' : '\'' + sObs + '\'');

		nResultadoID = getCellValueByColKey(fg, 'ResultadoID' + glb_sLockFields, i);
		nResultadoID = (nResultadoID == '' ? 'NULL' : nResultadoID);

        aDataToSave[j] = new Array(getCellValueByColKey(fg, 'RIRNumeroSerieID', i), 
								   nResultadoID,
								   sObs);
        j++;
    }

    saveItens(aDataToSave, aDataToSave.length, aDataToSave[0].length);
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modalamostrainspecaoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
	txtNumeroSerie.focus();
}

function startPesq()
{
	if (glb_bCarregamento)
		glb_nDSOs = 3;
	else		
		glb_nDSOs = 2;

    lockControlsInModalWin(true);
    
    strSQL = 'SELECT *, ' +
		'(SELECT b.NumeroSerie ' +
				'FROM NumerosSerie_Movimento a WITH(NOLOCK), NumerosSerie b WITH(NOLOCK) ' +
				'WHERE (Pedidos_RIR_Itens.NumMovimentoID = a.NumMovimentoID AND ' +
					'a.NumeroSerieID = b.NumeroSerieID)) AS NumeroSerie ' +
			'FROM Pedidos_RIR_Itens WITH(NOLOCK) ' +
			'WHERE (PedRIRID = ' + glb_nPedRIRID + ') ' +
			'ORDER BY NumeroSerie';

    setConnection(dsoGen01);

    dsoGen01.SQL = strSQL;
    dsoGen01.ondatasetcomplete = startPesq_DSC;
    dsoGen01.Refresh();
    
	setConnection(dsoCmbResultadoID);

	dsoCmbResultadoID.SQL = 'SELECT NULL AS ItemID, SPACE(0) AS ItemAbreviado, SPACE(0) AS ItemMasculino, 0 AS Ordem ' +
		'UNION ALL SELECT ItemID, ItemAbreviado, ItemMasculino, Ordem ' +
		'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
		'WHERE (EstadoID=2 AND TipoID=415) ' +
		'ORDER BY Ordem';

	dsoCmbResultadoID.ondatasetcomplete = startPesq_DSC;
	dsoCmbResultadoID.Refresh();
    
	if (glb_bCarregamento)
	{
		setConnection(dsoDadosInspecao);

		strSQL = 'SELECT dbo.fn_PedidoRIR_DadosInspecao(' + glb_nPedRIRID + ') AS DadosInspecao ';

		dsoDadosInspecao.SQL = strSQL;
		dsoDadosInspecao.ondatasetcomplete = startPesq_DSC;
		dsoDadosInspecao.Refresh();
	}
}

function startPesq_DSC()
{
	glb_nDSOs--;

	if (glb_nDSOs > 0)
		return null;

	if (glb_bCarregamento)
	{
		glb_bCarregamento = false;

		txtDadosParaInsp.value = '';

		if ( !(dsoDadosInspecao.recordset.BOF && dsoDadosInspecao.recordset.EOF) )
		{
			if ( dsoDadosInspecao.recordset.Fields['DadosInspecao'].value != null )
				txtDadosParaInsp.value = dsoDadosInspecao.recordset.Fields['DadosInspecao'].value;
		}
	}

	var i=0;
	var nQuantidade = 0;
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    
    headerGrid(fg,['Ordem',
                   'N�mero de S�rie',
                   'Resultado',
                   'Observa��o',
                   'OrdItemID'], [4]);

    fillGridMask(fg,dsoGen01,['_calc_Ordem_5*',
							  'NumeroSerie*',
							  'ResultadoID' + glb_sLockFields,
							  'Observacao' + glb_sLockFields,
							  'RIRNumeroSerieID'],
                              ['999','','','',''],
                              ['###','','','','']);
	
	for (i=1; i<fg.Rows; i++)
		fg.TextMatrix(i, 0) = i;
	
	insertcomboData(fg,2,dsoCmbResultadoID,'*ItemAbreviado|ItemMasculino','ItemID');
    alignColsInGrid(fg,[0]);
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    fg.Redraw = 2;
    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if ( fg.Rows > 1 )
        fg.Col = 3;
    
    if ( (fg.Rows > 1) && (glb_sLockFields == '') )
        fg.Editable = true;
    else    
		fg.Editable = false;

    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        btnOK.disabled = false;
        btnDesgatilhar.disabled = false;
    }    
    else
    {
        btnOK.disabled = true;
        btnDesgatilhar.disabled = true;
    }
    
    if (glb_sLockFields == '*')
    {
		txtNumeroSerie.readOnly = true;
		btnDesgatilhar.disabled = true;
		btnOK.disabled = true;
    }

	if (! isNaN(txtQuantidade.value))
		nQuantidade = parseInt(txtQuantidade.value, 10);
	else
		nQuantidade = 0;
	
	txtFalta.value = nQuantidade - (fg.Rows - 1);
	window.focus();
	txtNumeroSerie.focus();
}


function saveItens(aData, aDataLen, aDataElemLen)
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;

    lockControlsInModalWin(true);
    
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;

    for (i=0; i<aDataLen; i++)
    {
		nBytesAcum=strPars.length;
		
		if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
		{
			nBytesAcum = 0;
			if (strPars != '')
			{
				glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
				strPars = '';
				nDataLen = 0;
			}
			
			strPars += '?nDataElemLen=' + escape(aDataElemLen);
		}

        for (j=0; j<aDataElemLen; j++)
            strPars += '&aData' + j.toString() + '=' + escape(aData[i][j]);
		
		nDataLen++;				
	}
	
	if (nDataLen>0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);

	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGen03.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/saveamostrainspecao.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGen03.ondatasetcomplete = sendDataToServer_DSC;
			dsoGen03.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_AmostraInspecaoTimerInt = window.setInterval('finishWrite()', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_AmostraInspecaoTimerInt = window.setInterval('finishWrite()', 10, 'JavaScript');  
	}
}

function sendDataToServer_DSC()
{
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

function finishWrite()
{
	if (glb_AmostraInspecaoTimerInt != null)
    {
        window.clearInterval(glb_AmostraInspecaoTimerInt);
        glb_AmostraInspecaoTimerInt = null;
    }

    var i;
    
    try
    {
		if ( !(dsoGen03.recordset.BOF && dsoGen03.recordset.EOF) )
		{
			// Outro usuario mudou o estadoID do pedido para diferente de cotacao
			if (dsoGen03.recordset['fldErrorNumber'].value > 50000)
			{
				if ( window.top.overflyGen.Alert (dsoGen03.recordset['fldErrorText'].value) == 0 )
					return null;
	                
				lockControlsInModalWin(false);       
				window.focus();
				fg.focus();    
				return null;
			}
		}
    }
    catch(e)
    {	
		;
    }
    
    lockControlsInModalWin(false);       
	window.focus();
	txtNumeroSerie.focus();
}

function changeInterfaceMode(ctl, interfaceMode)
{
	if ( (interfaceMode == null) || (interfaceMode == '') || (interfaceMode == 'GRID') )
	{
		btnChangeInterface.title = 'Grid e Dados para Inspe��o';		
		
		divDadosParaInsp.style.visibility = 'hidden';
		
		divFG.style.height = glb_divGridExtendedHeight;
		fg.style.height = glb_divGridExtendedHeight;
				
		ctl.setAttribute('interfaceMode', 'GRID_TEXT', 1);
	}
	else if (interfaceMode == 'GRID_TEXT')	
	{
		btnChangeInterface.title = 'Apenas Grid';	
		
		divDadosParaInsp.style.visibility = 'inherit';
		
		divFG.style.height = glb_divGridRetractedHeight;
		fg.style.height = glb_divGridRetractedHeight;	
	
		ctl.setAttribute('interfaceMode', 'GRID', 1);
	}
	
	lockControlsInModalWin(false);       
    window.focus();
    fg.focus();    
}