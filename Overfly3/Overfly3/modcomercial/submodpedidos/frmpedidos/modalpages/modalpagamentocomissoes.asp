<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    Dim rsData, strSQL
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalpagamentocomissoesHtml" name="modalpagamentocomissoesHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalpagamentocomissoes.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalpagamentocomissoes.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller

sCaller = ""

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=fgComissoes EVENT=EnterCell>
<!--
 //js_fg_EnterCell(fgComissoes);
 js_modalpagamento_fgComissoes_EnterCell(fgComissoes);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgComissoes EVENT=BeforeRowColChange>
<!--
 //Atencao programador:
 //Setimo parametro abaixo = true, permite ao usuario mudar de linha em grid
 //clicando em celula read only.
 js_fg_BeforeRowColChange (fgComissoes, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], false);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgComissoes EVENT=KeyPress>
<!--
 fgComissoes_modalpagamentocomissoesKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgComissoes EVENT=ValidateEdit>
<!--
 fgComissoes_modalpagamentocomissoes_ValidateEdit()
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgComissoes EVENT=DblClick>
<!--
 fgComissoes_modalpagamentocomissoesDblClick(fgComissoes.grid, fgComissoes.row, fgComissoes.col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgComissoes EVENT=AfterEdit>
<!--
 fgComissoes_modalpagamentocomissoes_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgComissoes EVENT=BeforeEdit>
<!--
 js_fgComissoes_modalpagamentocomissoes_BeforeEdit(fgComissoes, fgComissoes.Row, fgComissoes.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgComissoes EVENT=AfterRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
js_fg_AfterRowColChange (fgComissoes, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGrid, fgComissoes, arguments[0], arguments[1], arguments[2], arguments[3]);
//Atualizar linha de mensagens
js_fgComissoes_modalpagamentocomissoes_AfterRowCol(fgComissoes, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgAssociacoes EVENT=EnterCell>
<!-- 
 js_modalpagamento_fgAssociacoes_EnterCell();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgAssociacoes EVENT=BeforeRowColChange>
<!--
 //Atencao programador:
 //Setimo parametro abaixo = true, permite ao usuario mudar de linha em grid
 //clicando em celula read only.
// js_fg_BeforeRowColChange (fgAssociacoes, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4],false);
 js_fgAssociacoes_BeforeRowColChange(fgAssociacoes, arguments[0], arguments[1], arguments[2], arguments[3], false);
//-->
</SCRIPT>


<SCRIPT LANGUAGE=javascript FOR=fgAssociacoes EVENT=KeyPress>
<!--
 fgAssociacoes_modalpagamentocomissoesKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgAssociacoes EVENT=DblClick>
<!--
 fgAssociacoes_modalpagamentocomissoesDblClick(fgAssociacoes.grid, fgAssociacoes.row, fgAssociacoes.col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgAssociacoes EVENT=AfterEdit>
<!--
 fgAssociacoes_modalpagamentocomissoes_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgAssociacoes EVENT=BeforeEdit>
<!--
 js_fgAssociacoes_modalpagamentocomissoes_BeforeEdit(fgAssociacoes, fgAssociacoes.Row, fgAssociacoes.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgAssociacoes EVENT=AfterRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
js_fg_AfterRowColChange (fgAssociacoes, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGridFR, fgAssociacoes, arguments[0], arguments[1], arguments[2], arguments[3]);
treatCheckBoxReadOnly2 (dsoGridComissoesDevolucao, fgAssociacoes, arguments[0], arguments[1], arguments[2], arguments[3]);
//Atualizar linha de mensagens
js_fgAssociacoes_modalpagamentocomissoes_AfterRowCol(fgAssociacoes, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

</head>

<body id="modalpagamentocomissoesBody" name="modalpagamentocomissoesBody" LANGUAGE="javascript" onload="return window_onload()">
    <div id="divControls" name="divControls" class="paraNormal">
        <p id="lbltpComissao" name="lbltpComissao" class="lblGeneral">Tipo Comiss�o</p>
        <select id="selTipoComissaoID" name="selTipoComissaoID" class="fldGeneral">
          <%Set rsData = Server.CreateObject("ADODB.Recordset")
             strSQL = "SELECT a.ItemID as fldID, a.ItemMasculino AS fldName " & _
                        "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _
                        "WHERE a.TipoID = 406 AND a.EstadoID = 2 "
                
			    rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			    While Not rsData.EOF
				 Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)

				 Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				 rsData.MoveNext
			    Wend
			rsData.Close
			Set rsData = Nothing%>
        </select>
    </div>

    <div id="divControlsComissao" name="divControlsComissao" class="paraNormal">
        <p id="lblChavePesquisa" name="lblChavePesquisa" class="lblGeneral">Chave de Pesquisa</p>
        <select id="selChavePesquisa" name="selChavePesquisa" class="fldGeneral">
            <option value=1 selected>Fantasia</option>
            <option value=2>Nome</option>
            <option value=3>CNPJ</option>
            <option value=4>Pedido</option>
            <option value=5>Nota Fiscal</option>
        </select>

        <p id="lblArgumento" name="lblArgumento" class="lblGeneral">Argumento</p>
        <input type="text" id="txtArgumento" name="txtArgumento" class="fldGeneral">

        <input type="button" id="btnPesqParceiro" name="btnPesqParceiro" value="P" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
                        
        <p id="lblParceiroComissaoID" name="lblParceiroComissaoID" class="lblGeneral">Parceiro</p>
        <select id="selParceiroComissaoID" name="selParceiroComissaoID" class="fldGeneral"></select>

        <p id="lblValor" name="lblValor" class="lblGeneral" title="Valor total da nota fiscal de servi�o do cliente">Total Nota Fiscal</p>
        <input type="text" id="txtValor" name="txtValor" class="fldGeneral" title="Valor total da nota fiscal de servi�o do cliente">
        <p id="lblGridComissoes" name="lblGridComissoes" class="lblGeneral">Comiss�es</p>
    </div>  
    <div id="divControlsAssociacoes" name="divControlsAssociacoes" class="paraNormal">
        <p id="lblFiltroPesquisa" name="lblFiltroPesquisa" class="lblGeneral">Filtro Pesquisa</p>
        <select id="selFiltroPesquisa" name="selFiltroPesquisa" class="fldGeneral"></select>
        
        <p id="lblPesquisa" name="lblPesquisa" class="lblGeneral">Pesquisa</p>
        <input type="text" id="txtPesquisa" name="txtPesquisa" class="fldGeneral" title="Pesquise um parceiro">
        <input type="button" id="btnPesquisa" name="btnPesquisa" value="P" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        
        <p id="lblTipoAssociacao" name="lblTipoAssociacao" class="lblGeneral">Forma de Associa��o</p>
        <select id="selTipoAssociacao" name="selTipoAssociacao" class="fldGeneral">
            <option value=1 selected>Comiss�o de Devolu��o</option>
            <option value=2>Financeiro</option>
        </select>
        
        
        
        <p id="lblTipoFinanceiro" name="lblTipoFinanceiro" class="lblGeneral">Tipo Financeiro</p>
            <select id="selTipoFinanceiro" name="selTipoFinanceiro" class="fldGeneral">
            <%Set rsData = Server.CreateObject("ADODB.Recordset")
                strSQL = "SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName " & _
			                "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _			 
			                "WHERE a.TipoID = 801 " & _
			                "ORDER BY a.Ordem "
	                rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText
    	        
        		    While Not rsData.EOF
		        	    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			            Response.Write( rsData.Fields("fldName").Value & "</option>" )
    			        rsData.MoveNext()
		            Wend
	            rsData.Close
	            Set rsData = Nothing%>
		    </select>
        
        <p id="lblParceiroFinanceiroID" name="lblParceiroFinanceiroID" class="lblGeneral">Parceiro</p>
        <select id="selParceiroFinanceiroID" name="selParceiroFinanceiroID" class="fldGeneral"></select>        
        
        <input type="button" id="btnListar_Associacoes" name="btnListar_Associacoes" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
                
        <p id="lblGridAssociacoes" name="lblGridAssociacoes" class="lblGeneral">Comiss�o Devolu��o</p>
    </div>  

    <div id="divGridComissoes" name="divGridComissoes" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgComissoes" name="fgComissoes" VIEWASTEXT>
        </object>
    </div>  
    
    <div id="divGridAssociacoes" name="divGridAssociacoes" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgAssociacoes" name="fgAssociacoes" VIEWASTEXT>
        </object>
    </div>
        
    <input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnGerar" name="btnGerar" value="Gerar Pedido" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnAssociar" name="btnAssociar" value="Associar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>
    
</body>

</html>
