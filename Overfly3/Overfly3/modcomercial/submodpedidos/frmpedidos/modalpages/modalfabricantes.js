/********************************************************************
modalfabricantes.js

Library javascript para o modalfabricantes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nQtdServerCalls = 0;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_OcorrenciasTimerInt = null;
var glb_bReset = false;


var dsoGrava = new CDatatransport('dsoGrava');
var dsoGrid = new CDatatransport('dsoGrid');
var dsoCmb01 = new CDatatransport('dsoCmb01');
var dsoCmb02 = new CDatatransport('dsoCmb02');
var dsoCmb03 = new CDatatransport('dsoCmb03');
var dsoAssociar = new CDatatransport('dsoAssociar');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalfabricantes.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fgAssociadas, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fgAssociadas
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalfabricantes.ASP

js_modalfabricantesKeyPress(KeyAscii)
js_modalfabricantes_AfterRowColChange
js_modalfabricantes_ValidateEdit()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalfabricantesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Fabricantes', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    btnGravar.disabled = true;
    
    // desabilita o botao OK
    btnOK.disabled = true;
    chkOK.disabled = true;
 
    // esconde os botoes OK e Canc
    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    // Ajusta controles
    adjustElementsInForm([['btnReset','btn',btnOK.offsetWidth,1],
						  ['btnIncluir','btn',btnOK.offsetWidth,1,ELEM_GAP],
						  ['btnExcluir','btn',btnOK.offsetWidth,1,ELEM_GAP],
						  ['btnGravar','btn',btnOK.offsetWidth,1,ELEM_GAP],
						  ['lblOK','chkOK',3,1]],
                          null,null,true);

	btnIncluir.style.height = btnOK.offsetHeight;
	btnGravar.style.height = btnOK.offsetHeight;
	btnReset.style.height = btnOK.offsetHeight;
	btnExcluir.style.height = btnOK.offsetHeight;

    // ajusts o divControles
    with (divControles.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10) + 1;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnGravar.currentStyle.top, 10) + parseInt(btnGravar.currentStyle.height, 10) + ELEM_GAP;
    }
    
    // ajusta o divFGAssociadas
    with (divFGAssociadas.style)
    {
        border = 'none';
        borderWidth = 0;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControles.currentStyle.top, 10) + parseInt(divControles.currentStyle.height, 10) + 2;
        width = modWidth - 2 * ELEM_GAP - 4;    
        height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(top, 10) + 4;
    }
    
    with (fgAssociadas.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFGAssociadas.style.width, 10) - 3;
        height = parseInt(divFGAssociadas.style.height, 10) - 3;
    }
    
    startGridInterface(fgAssociadas);
    fgAssociadas.FontSize = '10';
    fgAssociadas.Cols = 0;
    fgAssociadas.Cols = 1;
    fgAssociadas.ColWidth(0) = parseInt(divFGAssociadas.currentStyle.width, 10) * 18;    
    fgAssociadas.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        ;
    }
    else if (controlID == 'btnCanc')
    {
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }
    else if (controlID == 'btnIncluir')
    {
		btnIncluirClick();
    }
    else if (controlID == 'btnGravar')    
    {
		btnGravarClick();
    }
    else if (controlID == 'btnReset')    
    {
		btnResetClick();
    }
    else if (controlID == 'btnExcluir')
    {
		btnExcluirClick();
    }
}

function btnIncluirClick()
{
	fgAssociadas.Rows = fgAssociadas.Rows + 1;
	//paintReadOnlyCols(fgAssociadas);
}

function btnGravarClick()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;
	
	lockControlsInModalWin(true);
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
    
    for (i=1; i<fgAssociadas.Rows; i++)
    {
		if ((trimStr(getCellValueByColKey(fgAssociadas, 'ProdutoID', i)) == '') ||
			(trimStr(getCellValueByColKey(fgAssociadas, 'Quantidade', i)) == '') ||
			(trimStr(getCellValueByColKey(fgAssociadas, 'FabricanteID', i)) == ''))
			continue;
    
		nBytesAcum=strPars.length;
		
		if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
		{
			nBytesAcum = 0;
			if (strPars != '')
			{
				glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
				strPars = '';
				nDataLen = 0;
			}
				
			strPars += '?nTipoResultado=' + escape(3);
		}

		nDataLen++;
		
		if (getCellValueByColKey(fgAssociadas, '_PedFabricanteID', i) != '')
			strPars += '&nPedidoID=' + escape('-' + getCellValueByColKey(fgAssociadas, '_PedFabricanteID', i));
		else
			strPars += '&nPedidoID=' + escape(glb_nPedidoID);

		strPars += '&nProdutoID=' + escape(getCellValueByColKey(fgAssociadas, 'ProdutoID', i));
		strPars += '&nQuantidade=' + escape(getCellValueByColKey(fgAssociadas, 'Quantidade', i));
		strPars += '&nFabricanteID=' + escape(getCellValueByColKey(fgAssociadas, 'FabricanteID', i));
	}
	
	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
		
	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGrava.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/gravarfabricantes.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
	}
}

function sendDataToServer_DSC()
{
	if ( !(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF))
	{
		dsoGrava.recordset.MoveFirst();
		if ( (dsoGrava.recordset['Resultado'].value != null) && 
			 (dsoGrava.recordset['Resultado'].value != '') )
		{
			if ( window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0 )
				return null;
				
			lockControlsInModalWin(false);
			return null;
		}
	}

	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

function btnExcluirClick()
{
	var currLine = fgAssociadas.Row;
	
	lockControlsInModalWin(true);
	if (getCellValueByColKey(fgAssociadas, '_PedFabricanteID', fgAssociadas.Row) == '')
	{
        fgAssociadas.RemoveItem(currLine);
        if ( currLine > 1 )
            fgAssociadas.Row = currLine - 1;
        else if ( currLine == 1 && fgAssociadas.Rows > 1)
            fgAssociadas.Row = 1;
        //else
            //fgAssociadas.Row = 0;

		lockControlsInModalWin(false);
		setupBtnsFromGridState();
		return null;
	}
	
	glb_nQtdServerCalls = 1;
	var strPars = '';
	var nPedFabricanteID = getCellValueByColKey(fgAssociadas, '_PedFabricanteID', fgAssociadas.Row);
	
	strPars = '?nPedidoID=' + escape(-nPedFabricanteID) +
		'&nTipoResultado=' + escape(4);

	dsoGrava.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/listarfabricantes.aspx' + strPars;
    dsoGrava.ondatasetcomplete = btnExcluirClick_DSC;
    dsoGrava.Refresh();
}

function btnExcluirClick_DSC()
{
	lockControlsInModalWin(false);
	glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
}

function btnResetClick()
{
	glb_bReset = true;
	glb_nQtdServerCalls = 1;
	var strPars = '';
	
	chkOK.checked = false;

	strPars = '?nPedidoID=' + escape(glb_nPedidoID) +
		'&nTipoResultado=' + escape(2);

	dsoGrid.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/listarfabricantes.aspx' + strPars;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
	btnReset.disabled = false;
	btnIncluir.disabled = false;
	btnExcluir.disabled = fgAssociadas.Row < 1;
	btnGravar.disabled = true;
	var i=0;
	
	for (i=1; i<fgAssociadas.Rows; i++)
	{
		if (fgAssociadas.Row > 0)
		{
			if ((trimStr(getCellValueByColKey(fgAssociadas, 'ProdutoID', i)) != '') &&
				(trimStr(getCellValueByColKey(fgAssociadas, 'Quantidade', i)) != '') &&
				(trimStr(getCellValueByColKey(fgAssociadas, 'FabricanteID', i)) != ''))
			{
				btnGravar.disabled = false;
				break;
			}
			else
				btnGravar.disabled = true;
		}
	}
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_OcorrenciasTimerInt != null)
	{
		window.clearInterval(glb_OcorrenciasTimerInt);
		glb_OcorrenciasTimerInt = null;
	}

	glb_nQtdServerCalls = 4;
    lockControlsInModalWin(true);
    
	var strSQL = '';
	var sFiltro = '';
	var strPars = '';
	
	strPars = '?nPedidoID=' + escape(glb_nPedidoID) +
		'&nTipoResultado=' + escape(1);

    // zera o grid
    fgAssociadas.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoCmb01);

    dsoGrid.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/listarfabricantes.aspx' + strPars;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();

    // parametrizacao do dso dsoGrid
    setConnection(dsoCmb01);

	strSQL = 'SELECT b.ConceitoID, b.Conceito ' +
		'FROM Pedidos_Itens a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
		'WHERE (a.PedidoID = ' + glb_nPedidoID + ' AND a.ProdutoID = b.ConceitoID) ' +
		'GROUP BY b.ConceitoID, b.Conceito ' +
		'ORDER BY b.Conceito';

    dsoCmb01.SQL = strSQL;
    dsoCmb01.ondatasetcomplete = fillGridData_DSC;
    dsoCmb01.Refresh();

    // parametrizacao do dso dsoGrid
    setConnection(dsoCmb02);

	strSQL = 'SELECT a.PessoaID, (a.Fantasia + SPACE(1) + CHAR(40) + c.CodigoLocalidade2 + CHAR(41)) AS Fantasia, c.NomeInternacional ' +
		'FROM Pessoas a WITH(NOLOCK), Pessoas_Enderecos b WITH(NOLOCK), Localidades c WITH(NOLOCK) ' +
		'WHERE (a.EstadoID = 2 AND a.EhFabrica = 1 AND a.PessoaID = b.PessoaID AND  ' +
		'b.EndFaturamento = 1 AND b.Ordem = 1 AND b.PaisID = c.LocalidadeID) ' +
		'GROUP BY a.PessoaID, a.Fantasia, c.NomeInternacional, c.CodigoLocalidade2 ' +
		'ORDER BY a.Fantasia, c.NomeInternacional';

    dsoCmb02.SQL = strSQL;
    dsoCmb02.ondatasetcomplete = fillGridData_DSC;
    dsoCmb02.Refresh();

    // parametrizacao do dso dsoGrid
    setConnection(dsoCmb03);

	strSQL = 'SELECT dbo.fn_Pedido_RelatorioFabricante(' + glb_nPedidoID + ') AS OK';

    dsoCmb03.SQL = strSQL;
    dsoCmb03.ondatasetcomplete = fillGridData_DSC;
    dsoCmb03.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nQtdServerCalls--;
	
	if (glb_nQtdServerCalls > 0)
		return null;
		
    showExtFrame(window, true);
    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    var bOK = false;
    
    fgAssociadas.Redraw = 0;
    fgAssociadas.Editable = true;
    fgAssociadas.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

    headerGrid(fgAssociadas,['_PedFabricanteID' ,
				   'Produto',
				   'ID',
				   'Quant Total',
				   'Quant',
				   'Fabricante',
				   'Pa�s'],[0]);
                       
    fillGridMask(fgAssociadas,dsoGrid,['_PedFabricanteID' ,
							'ProdutoID',
							'ProdutoID*',
							'QuantidadeTotal*',
							'Quantidade',
							'FabricanteID',
							'Pais*'],
                             ['','','','9999999999','9999999999','',''],
                             ['','','','##########','##########','','']);
	
	insertcomboData(fgAssociadas,1,dsoCmb01,'Conceito|ConceitoID','ConceitoID');
	insertcomboData(fgAssociadas, 5, dsoCmb02, 'Fantasia|NomeInternacional', 'PessoaID');
	
    fgAssociadas.AutoSizeMode = 0;
    fgAssociadas.AutoSize(0,fgAssociadas.Cols-1);
    fgAssociadas.ColWidth(5) = 100 * FONT_WIDTH * 3; 
    fgAssociadas.Redraw = 2;

	alignColsInGrid(fgAssociadas,[2,3,4]);    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if ( fgAssociadas.Rows > 1 )
        fgAssociadas.Col = 1;
    
    lockControlsInModalWin(false);

	if (glb_bReset) 
		glb_bReset = false;
	else if (!((dsoCmb03.recordset.BOF)&&(dsoCmb03.recordset.EOF)))
		bOK = dsoCmb03.recordset['OK'].value;

	chkOK.checked = bOK;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fgAssociadas.Row < 1) && (fgAssociadas.Rows > 1) )
        fgAssociadas.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fgAssociadas.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fgAssociadas.Rows > 1)
    {
        window.focus();
        fgAssociadas.focus();
    }                
    else
    {
        ;
    }            

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalfabricantesKeyPress(KeyAscii)
{
    ;
}

function fg_modalfabricantes_AfterRowColChange(grid)
{
    setupBtnsFromGridState();
}

function fg_EnterCell_Prg()
{
	return null;
}

function fg_modalfabricantes_AfterEdit(grid, nRow, nCol)
{
	fgAssociadas.TextMatrix(nRow, getColIndexByColKey(fgAssociadas, 'ProdutoID*')) =
		fgAssociadas.TextMatrix(nRow, getColIndexByColKey(fgAssociadas, 'ProdutoID'));

	setupBtnsFromGridState();		
}

// FINAL DE EVENTOS DE GRID *****************************************
