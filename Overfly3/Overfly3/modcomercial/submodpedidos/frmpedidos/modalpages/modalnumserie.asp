
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalnumserieHtml" name="modalnumserieHtml">

<head>

<title></title>

<%

Dim j, nCurrUserID

For j = 1 To Request.QueryString("nCurrUserID").Count    
    nCurrUserID = Request.QueryString("nCurrUserID")(j)
Next
%>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf        
    
 	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalnumserie.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "var glb_USERID = 0;"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nPedidoID, nOrdemProducaoID, nContextoID, nEstadoID, nTemProducao, nMetodoCustoID
Dim sDATE_SQL_PARAM

nPedidoID = 0
nOrdemProducaoID = 0
nContextoID = 0
nEstadoID = 0
nTemProducao = 0
nMetodoCustoID = 0

For i = 1 To Request.QueryString("nPedidoID").Count    
    nPedidoID = Request.QueryString("nPedidoID")(i)
Next

For i = 1 To Request.QueryString("nOrdemProducaoID").Count    
    nOrdemProducaoID = Request.QueryString("nOrdemProducaoID")(i)
Next

For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

For i = 1 To Request.QueryString("nEstadoID").Count    
    nEstadoID = Request.QueryString("nEstadoID")(i)
Next

For i = 1 To Request.QueryString("nTemProducao").Count    
    nTemProducao = Request.QueryString("nTemProducao")(i)
Next

'Formato corrente de data
For i = 1 To Request.QueryString("sDATE_SQL_PARAM").Count
    sDATE_SQL_PARAM = Request.QueryString("sDATE_SQL_PARAM")(i)
Next

Response.Write "var glb_nPedidoID = " & CStr(nPedidoID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nOrdemProducaoID = " & CStr(nOrdemProducaoID) & ";"
Response.Write vbcrlf

If (CLng(nOrdemProducaoID)=0 AND CLng(nTemProducao)=0 AND CLng(nContextoID)=5111 AND CLng(nEstadoID)=27) Then
	Response.Write "var glb_bReadOnly = false;"
ElseIf (CLng(nOrdemProducaoID)=0 AND CLng(nTemProducao)=0 AND CLng(nContextoID)=5112 AND CLng(nEstadoID)=25) Then
	Response.Write "var glb_bReadOnly = false;"
ElseIf (CLng(nOrdemProducaoID)>0 AND (CLng(nEstadoID)=121 OR CLng(nEstadoID)=122 OR _
	CLng(nEstadoID)=123 OR CLng(nEstadoID)=72 OR CLng(nEstadoID)=25)) Then
	Response.Write "var glb_bReadOnly = false;"
Else
	Response.Write "var glb_bReadOnly = true;"
End If

Response.Write vbcrlf

Dim rsData, strSQL, sVendedor, sdtPrevisaoEntrega, sdtLiberacao

sVendedor = ""
sdtPrevisaoEntrega = ""
sdtLiberacao = ""

strSQL = "SELECT b.Fantasia AS Vendedor, " & _
			"(CONVERT(VARCHAR(10), a.dtPrevisaoEntrega, " & CStr(sDATE_SQL_PARAM) & ") + SPACE(1) + CONVERT(VARCHAR(8), a.dtPrevisaoEntrega, 108)) AS sdtPrevisaoEntrega, " & _
			"(CONVERT(VARCHAR(10), dbo.fn_Pedido_Datas(a.PedidoID, 5), " & CStr(sDATE_SQL_PARAM) & ") + SPACE(1) + CONVERT(VARCHAR(8), dbo.fn_Pedido_Datas(a.PedidoID, 5), 108)) AS sdtLiberacao " & _
		  "FROM Pedidos a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " & _
		  "WHERE (a.PedidoID = " & CStr(nPedidoID) & " AND a.ProprietarioID = b.PessoaID) "

Set rsData = Server.CreateObject("ADODB.Recordset")         
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

If (Not ((rsData.BOF) AND (rsData.EOF))) Then
	sVendedor = rsData.Fields("Vendedor").Value
	sdtPrevisaoEntrega = rsData.Fields("sdtPrevisaoEntrega").Value
	sdtLiberacao = rsData.Fields("sdtLiberacao").Value
End If

Response.Write "var glb_sVendedor = '" & sVendedor & "';"
Response.Write vbcrlf

Response.Write "var glb_sdtPrevisaoEntrega = '" & sdtPrevisaoEntrega & "';"
Response.Write vbcrlf

Response.Write "var glb_sdtLiberacao = '" & sdtLiberacao & "';"
Response.Write vbcrlf

rsData.Close
Set rsData = Nothing

'MetodoCustoID
strSQL =	"SELECT b.MetodoCustoID " & _
				"FROM Pedidos a WITH(NOLOCK) " & _
				    "INNER JOIN Operacoes b WITH(NOLOCK) ON (a.TransacaoID = b.OperacaoID) " & _
				"WHERE (a.PedidoID = " & CStr(nPedidoID) & ") "

Set rsData = Server.CreateObject("ADODB.Recordset")         
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

If (Not ((rsData.BOF) AND (rsData.EOF))) Then
	If NOT IsNull(rsData.Fields("MetodoCustoID").Value) Then 
		nMetodoCustoID = rsData.Fields("MetodoCustoID").Value
	End If	
End If

rsData.Close
Set rsData = Nothing

Response.Write "var glb_nMetodoCustoID = " & CStr(nMetodoCustoID) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
 fg_AfterRowColChange_NumSerie();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=oPrinter EVENT=OnPrintFinish>
<!--
 oPrinter_OnPrintFinish();
//-->
</SCRIPT>

</head>

<body id="modalnumserieBody" name="modalnumserieBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle de impressao matricial -->
    <object classid="clsid:1DF93CFF-D5C8-4A6D-983F-19DBAAC698D9" id = "oPrinter" name = "oPrinter" width=0 height=0></object>
    <input type="text" id="txtInvisible" name="txtInvisible" class="fldGeneral"></input>

    <div id="divFields" name="divFields" class="divGeneral">
        <p id="lblLocal" name="lblLocal" class="lblGeneral">Local</p>
        <input type="checkbox" id="chkLocal" name="chkLocal" class="fldGeneral" title="Trabalhar em modo local?"></input>
        <p id="lblEtiquetaPropria" name="lblEtiquetaPropria" class="lblGeneral">EP</p>
        <input type="checkbox" id="chkEtiquetaPropria" name="chkEtiquetaPropria" class="fldGeneral" title="Etiqueta pr�pria ou de fabricante?"></input>
        <p id="lblProduto" name="lblProduto" class="lblGeneralPesq">Produto</p>
        <input type="text" id="txtProduto" name="txtProduto" class="fldGeneralPesq"></input>
        <p id="lblQuantidade" name="lblQuantidade" class="lblGeneralPesq">Quant</p>
        <input type="text" id="txtQuantidade" name="txtQuantidade" class="fldGeneralPesq"></input>
        <p id="lblControle" name="chkControle" class="lblGeneral">C</p>
        <input type="checkbox" id="chkControle" name="chkControle" class="fldGeneral" title="Controle"></input>
        <p id="lblNumeroSerie" name="lblNumeroSerie" class="lblGeneralPesq">N�mero de S�rie</p>
        <input type="text" id="txtNumeroSerie" name="txtNumeroSerie" class="fldGeneralPesq"></input>
        <p id="lblCodigoBarra" name="lblCodigoBarra" class="lblGeneralPesq">�ltimo C�digo de Barra</p>
        <input type="text" id="txtCodigoBarra" name="txtCodigoBarra" class="fldGeneralPesq"></input>
    </div>

     <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" class="fldGeneral" VIEWASTEXT>
        </object>
    </div>    

	<div id="divFieldsLocal" name="divFieldsLocal" class="divGeneral">
        <p id="lblCaixaID" name="lblCaixaID" class="lblGeneral">Produto/Caixa/Quant</p>
        <select id="selCaixaID" name="selCaixaID" class="fldGeneral">
			<option value="0"></option>
		</select>
		<input type="button" id="btnPreencherCaixa" name="btnPreencherCaixa" value="P" LANGUAGE="javascript" class="btns" title ="Preencher combo">
		<input type="button" id="btnZerarCaixa" name="btnZerarCaixa" value="Zerar caixa" LANGUAGE="javascript" class="btns" title ="Zerar n�meros de s�rie desta caixa no banco de dados">
		<input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" class="btns" title="Gravar grid local">
		<input type="button" id="btnDeletar" name="btnDeletar" value="Deletar" LANGUAGE="javascript" class="btns" title="Deletar n�mero de s�rie do grid local">
		<input type="button" id="btnLimpar" name="btnLimpar" value="Limpar" LANGUAGE="javascript" class="btns" title="Limpar grid local">
	</div>

     <div id="divLocal" name="divLocal" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgLocal" name="fgLocal" class="fldGeneral" VIEWASTEXT>
        </object>
    </div>    

	<input type="button" id="btnEIR" name="btnEIR" value="EIR" LANGUAGE="javascript" class="btns">
	<input type="button" id="btnImportar" name="btnImportar" value="Importar" Title "Importar N�meros de S�rie?" LANGUAGE="javascript" class="btns">
	<input type="button" id="btnDesgatilhar" name="btnDesgatilhar" value="Desgatilhar" Title "Desgatilhar N�meros de S�rie?" LANGUAGE="javascript" class="btns">
	<input type="button" id="btnRefresh" name="btnRefresh" value="Refresh" Title "Refresh" LANGUAGE="javascript" class="btns">
	<input type="button" id="btnSeparar" name="btnSeparar" value="Separar" Title "Separar" LANGUAGE="javascript" class="btns">
	<input type="button" id="btnSepararTodos" name="btnSepararTodos" value="Separar Todos" Title "Separar Todos" LANGUAGE="javascript" class="btns">
	<input type="button" id="btnPickingList" name="btnPickingList" value="Picking List" Title "Picking List" LANGUAGE="javascript" class="btns">
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
