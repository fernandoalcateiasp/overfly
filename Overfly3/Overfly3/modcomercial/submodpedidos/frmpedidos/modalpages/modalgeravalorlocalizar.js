/********************************************************************
modalgeravalorlocalizar.js

Library javascript para o modalgeravalorlocalizar.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nUserID = 0;
var glb_sFinanceiros = 'NULL';
var glb_bRetencao = false;
var glb_nCont = 0;
var glb_nTipoChamada = 0;
var glb_nValorID = 0;
var glb_Row = 1;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_VLTimerInt = null;
var glb_PassedOneInDblClick = false;
var glb_bMultiplo = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;

var dsoGrid = new CDatatransport('dsoGrid');
var dsoGridRetencoes = new CDatatransport('dsoGridRetencoes');
var dsoPesquisa = new CDatatransport('dsoPesquisa');
var dsoGrava = new CDatatransport('dsoGrava');
var dsoIncluiRetencoes = new CDatatransport('dsoIncluiRetencoes');
var dsoCombo1 = new CDatatransport('dsoCombo1');
var dsoCombo2 = new CDatatransport('dsoCombo2');
var dsoComboPessoa = new CDatatransport('dsoComboPessoa');
/********************************************************************
Objeto controlador de execucao dos eventos dos grids e de grid com
linha de totais

Nota:
nColDataType = 0 (String), 1 (Numerico), 2 (Data), 3 (Boolean)
ncolUsedToSort = -1 nenhuma coluna
ncolOrderUsedToSort = 1 ascendente, -1 descendente

Instanciado pela variavel glb_gridsEvents
********************************************************************/

function __resetOrderParams() {
    this.ncolUsedToSort_fgValores = -1;
    this.ncolOrderUsedToSort_fgValores = 0;
    this.ncolUsedToSort_fgLacContas = -1;
    this.ncolOrderUsedToSort_fgPesquisa = 0;
}

function __gridsEvents() {
    this.bTotalLine_fgValores = false;
    this.bTotalLine_fgPesquisa = false;
    this.bBlockEvents_fgValores = false;
    this.bBlockEvents_fgPesquisa = false;
    this.ncolUsedToSort_fgValores = -1;
    this.ncolOrderUsedToSort_fgValores = 0;
    this.ncolUsedToSort_fgLacContas = -1;
    this.ncolOrderUsedToSort_fgPesquisa = 0;

    this.resetOrderParams = __resetOrderParams;
}

var glb_gridsEvents = new __gridsEvents();

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillComboData()
fillComboData_DSC()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly(grid)
fillGridPesquisa()
fillGridPesquisa_DSC()
pesquisaDocumento()
pesquisaDocumento_DSC()

js_fgValores_BeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fgValores_DblClick(grid, Row, Col)
js_fgValores_KeyPress(KeyAscii)
js_fgValores_ValidateEdit(grid, row, col)
js_fgValores_BeforeEdit(grid, row, col)
js_fgValores_AfterEdit(Row, Col)
js_fgValores_BeforeRowColChange (grid, OldRow, OldCol, NewRow, NewCol, Cancel)
js_fgValores_AfterRowColChange (grid, OldRow, OldCol, NewRow, NewCol)
js_fgValores_EnterCell (grid)

js_fgPesquisa_BeforeRowColChange (grid, OldRow, OldCol, NewRow, NewCol, Cancel)
js_fgPesquisa_AfterRowColChange (grid, OldRow, OldCol, NewRow, NewCol)
js_fgPesquisa_DblClick (grid, Row, Col)
********************************************************************/
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalgeravalorlocalizarBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    showExtFrame(window, true);

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {

    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Gerar Valores a Localizar');


    secText('Gerar Valores a Localizar', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var elemGrid;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divGridPesquisa
    adjustElementsInForm([['lblVariacaoValor', 'txtVariacaoValor', 9, 1, -10, -10],
						  ['lblVariacaoDias', 'txtVariacaoDias', 5, 1],
						  ['lblPessoaID', 'selPessoaID', 20, 1],
						  ['lblFantasia', 'txtFantasia', 22, 1],
						  ['lblValorID', 'txtValorID', 10, 1]], null, null, true);

    with (txtVariacaoValor) {
        onfocus = selFieldContent;
        onkeypress = verifyNumericEnterNotLinked;
        onkeydown = txtFields_onKeyDown;
        setAttribute('thePrecision', 11, 1);
        setAttribute('theScale', 2, 1);
        setAttribute('verifyNumPaste', 1);
    }

    txtVariacaoValor.value = 0;

    with (txtVariacaoDias) {
        onfocus = selFieldContent;
        onkeypress = verifyNumericEnterNotLinked;
        onkeydown = txtFields_onKeyDown;
        setAttribute('thePrecision', 2, 1);
        setAttribute('theScale', 0, 1);
        setAttribute('minMax', new Array(0, 30));
        setAttribute('verifyNumPaste', 1);
    }

    txtVariacaoDias.value = 0;

    selPessoaID.onchange = selPessoaID_onchange;

    with (txtFantasia) {
        onfocus = selFieldContent;
        onkeydown = txtFields_onKeyDown;
        setAttribute('verifyNumPaste', 1);
        maxLength = 20;
    }

    with (txtValorID) {
        onfocus = selFieldContent;
        onkeypress = verifyNumericEnterNotLinked;
        onkeydown = txtFields_onKeyDown;
        setAttribute('thePrecision', 10, 1);
        setAttribute('theScale', 0, 1);
        setAttribute('minMax', new Array(0, 9999999999));
        setAttribute('verifyNumPaste', 1);
    }

    // ajusta o divGridValores
    elem = divGridValores;
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP +
			  btnRefresh.offsetTop + btnRefresh.offsetHeight + 2 + 48;
        width = modWidth - 2 * ELEM_GAP - 4;
        height = 150 - 30;
    }

    elem = fgValores;
    elem1 = divGridValores;
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(elem1.style.width, 10);
        height = parseInt(elem1.style.height, 10);
    }

    // ajusta o divGridPesquisa
    elem = divGridPesquisa;
    elem1 = divGridValores;
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = elem1.offsetTop + elem1.offsetHeight + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 4;
        height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(top, 10);
    }

    elemGrid = fgPesquisa;
    elem = divGridPesquisa;
    with (elemGrid.style) {
        left = 0;
        top = parseInt(lblVariacaoValor.currentStyle.height, 10) + parseInt(txtVariacaoValor.currentStyle.height, 10) + 8;
        width = parseInt(elem.style.width, 10);
        height = parseInt(elem.style.height, 10) - (parseInt(lblVariacaoValor.currentStyle.height, 10) + parseInt(txtVariacaoValor.currentStyle.height, 10)) - 6;
    }

    adjustElementsInForm([['lblGrid', 'chkGrid', 3, 1, 0, 20],
						  ['lblPendente', 'chkPendente', 3, 1, -6],
						  ['lblPedido', 'chkPedido', 3, 1, -6],
						  ['lblDtInicio', 'txtDtInicio', 10, 1, -8],
						  ['lblDtFim', 'txtDtFim', 10, 1, -3],
						  ['lblHistoricoPadraoID', 'selHistoricoPadraoID', 22, 1, -3],
						  ['lblParceiro', 'chkParceiro', 3, 1, -3],
						  ['lblPessoaID2', 'selPessoaID2', 17, 1, -9],
                          ['btnFindPessoa', 'btn', 21, 1],
                          ['lblCodigoBarra', 'txtCodigoBarra', 41, 2],
						  ['lblModo', 'chkModo', 3, 2],
                          ['lblServico', 'chkServico', 3, 2],
                          ['lblFormaPagamentoID', 'selFormaPagamentoID', 7, 2, -5],
                          ['lblRelPesContaID', 'selRelPesContaID', 19, 2]], null, null, true);

    with (btnAssociar.style) {
        top = txtValorID.offsetTop;
        width = btnOK.offsetWidth;
        height = btnOK.offsetHeight;
        left = divGridPesquisa.offsetWidth - parseInt(width, 0);
    }

    elem = btnAssociar;
    with (btnListar.style) {
        left = elem.offsetLeft - elem.offsetWidth - (ELEM_GAP / 2);
        top = txtValorID.offsetTop;
        width = btnOK.offsetWidth;
        height = btnOK.offsetHeight;
    }

    // Reposiciona botao Dissociar
    with (btnDissociar) {
        style.width = 60;
        style.height = btnOK.currentStyle.height;
        style.top = txtCodigoBarra.currentStyle.top;
        style.left = modWidth - ELEM_GAP - parseInt(btnDissociar.currentStyle.width) - 4;
    }

    // Reposiciona botao OK (Incluir)
    with (btnOK) {
        style.width = btnDissociar.currentStyle.width;
        style.top = btnDissociar.currentStyle.top;
        style.left = parseInt(btnDissociar.currentStyle.left, 10) - parseInt(btnDissociar.currentStyle.width, 10) - 4;
        value = 'Incluir';
        title = 'Incluir valor a localizar';
    }

    // Reposiciona botao Refresh
    with (btnRefresh) {
        style.width = btnDissociar.currentStyle.width;
        style.height = btnOK.currentStyle.height;
        style.top = btnFindPessoa.currentStyle.top;
        style.left = btnOK.currentStyle.left;
    }

    // Reposiciona botao DetalharVL
    with (btnDetalharVL) {
        style.width = btnDissociar.currentStyle.width;
        style.height = btnOK.currentStyle.height;
        style.top = selPessoaID2.currentStyle.top;
        style.left = parseInt(btnRefresh.currentStyle.left, 10) + parseInt(btnRefresh.currentStyle.width, 10) + 4;
    }

    if (glb_nTipoValorID == 1001)
        chkGrid.checked = false;
    else
        chkGrid.checked = true;

    chkGrid_onclick();
    chkGrid.onclick = chkGrid_onclick;
    chkPendente.onclick = chkPendente_onclick;
    chkPedido.onclick = chkPedido_onclick;

    txtDtInicio.onfocus = selFieldContent;
    txtDtInicio.maxLength = 10;
    txtDtInicio.onkeypress = txtData_onKeyPress;
    txtDtInicio.value = glb_dCurrDate;

    txtDtFim.onfocus = selFieldContent;
    txtDtFim.maxLength = 10;
    txtDtFim.onkeypress = txtData_onKeyPress;
    txtDtFim.value = glb_dCurrDate;

    selHistoricoPadraoID.onchange = selHistoricoPadraoID_onchange;
    chkParceiro.onclick = chkParceiro_onclick;
    selPessoaID2.onchange = selPessoaID2_onchange;
    txtCodigoBarra.onkeypress = txtData_onKeyPress;
    txtCodigoBarra.onfocus = selFieldContent;
    txtCodigoBarra.maxLength = 44;
    chkModo.onclick = chkModo_onclick;
    chkModo.disabled = false;
    selFormaPagamentoID.onchange = selFormaPagamentoID_onchange;
    selRelPesContaID.onchange = selRelPesContaID_onchange;

    lblFormaPagamentoID.style.visibility = 'hidden';
    selFormaPagamentoID.style.visibility = 'hidden';
    lblRelPesContaID.style.visibility = 'hidden';
    selRelPesContaID.style.visibility = 'hidden';

    lblServico.style.visibility = 'hidden';
    chkServico.style.visibility = 'hidden';

    if (glb_sCaller != 'PL') {
        lblPedido.style.visibility = 'hidden';
        chkPedido.style.visibility = 'hidden';
        lblDtInicio.style.visibility = 'hidden';
        txtDtInicio.style.visibility = 'hidden';
        lblDtFim.style.visibility = 'hidden';
        txtDtFim.style.visibility = 'hidden';
        lblHistoricoPadraoID.style.visibility = 'hidden';
        selHistoricoPadraoID.style.visibility = 'hidden';
        lblParceiro.style.visibility = 'hidden';
        chkParceiro.style.visibility = 'hidden';
        lblPessoaID2.style.visibility = 'hidden';
        selPessoaID2.style.visibility = 'hidden';
        btnFindPessoa.style.visibility = 'hidden';
        lblCodigoBarra.style.visibility = 'hidden';
        txtCodigoBarra.style.visibility = 'hidden';
        lblModo.style.visibility = 'hidden';
        chkModo.style.visibility = 'hidden';
        lblFormaPagamentoID.style.visibility = 'hidden';
        selFormaPagamentoID.style.visibility = 'hidden';
        lblRelPesContaID.style.visibility = 'hidden';
        selRelPesContaID.style.visibility = 'hidden';
    }
    else {
        chkPendente.checked = true;

        if (glb_nTipoValorID == 1002) {
            lblModo.style.visibility = 'hidden';
            chkModo.style.visibility = 'hidden';
        }
    }

    // Esconde botao cancel
    with (btnCanc) {
        style.visibility = 'hidden';
    }

    elem = fgValores;
    elem1 = divGridValores;
    startGridInterface(elem, 1, 1);
    with (elem) {
        Redraw = 0;
        Cols = 1;
        ColWidth(0) = parseInt(elem1.currentStyle.width, 10) * 18;
        FontSize = '8';
        Redraw = 2;
    }

    elem = fgPesquisa;
    elem1 = divGridPesquisa;
    startGridInterface(elem, 1, 1);
    with (elem) {
        Redraw = 0;
        Cols = 1;
        ColWidth(0) = parseInt(elem1.currentStyle.width, 10) * 18;
        FontSize = '8';
        Redraw = 2;
    }

    selPessoaID_onchange();

    hr_L_FGBorder.style.visibility = 'hidden';
    hr_R_FGBorder.style.visibility = 'hidden';
    hr_B_FGBorder.style.visibility = 'hidden';
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    if (ctl.disabled)
        return true;

    var nValorID, bHasRowsInGrid, bHasRowsInGrid2;

    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        if (!glb_bMultiplo) {
            saveDataInGrid(1);
        }
        else {
            calculaTotais();

            saveDataInGrid(4);
        }
    }
    else if (controlID == 'btnDissociar') {
        saveDataInGrid(3);
    }
    else if (controlID == 'btnListar') {
        fillGridPesquisa();
    }
    else if (controlID == 'btnAssociar') {
        saveDataInGrid(2);
    }
    else if (controlID == 'btnDetalharVL') {
        nValorID = '';
        bHasRowsInGrid = fgValores.Rows > 1;
        bHasRowsInGrid2 = fgPesquisa.Rows > 1;

        if (bHasRowsInGrid)
            nValorID = getCellValueByColKey(fgValores, 'ValorID*', fgValores.Row);
        if ((nValorID == '') && (bHasRowsInGrid2))
            nValorID = getCellValueByColKey(fgPesquisa, 'ValorID', fgPesquisa.Row);
        if (nValorID != '')
            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'showvalorlocalizar(' + nValorID + ')');
    }
    else if (controlID == 'btnFindPessoa') {
        if (!verificaData())
            return null;

        if (trimStr(txtDtInicio.value) == '') {
            if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
                return null;

            window.focus();
            txtDtInicio.focus();

            return null;
        }
        else if (trimStr(txtDtFim.value) == '') {
            if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
                return null;

            window.focus();
            txtDtFim.focus();

            return null;
        }

        fillComboPessoa();
    }
    else if (controlID == 'btnRefresh') {
        if (!verificaData())
            return null;

        if (trimStr(txtDtInicio.value) == '') {
            if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
                return null;

            window.focus();
            txtDtInicio.focus();

            return null;
        }
        else if (trimStr(txtDtFim.value) == '') {
            if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
                return null;

            window.focus();
            txtDtFim.focus();

            return null;
        }

        fillGridData();
    }
        // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
- null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    glb_nUserID = glb_USERID;

    setupBtnsFromGridState();

    lockControlsInModalWin(true);

    glb_VLTimerInt = window.setInterval('fillComboPessoa()', 10, 'JavaScript');

    fillComboData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState() {
    var bHasRowsInGrid = fgValores.Rows > 1;
    var bHasRowsInGrid2 = fgPesquisa.Rows > 1;
    var i;

    btnDetalharVL.disabled = true;
    btnDissociar.disabled = true;
    btnOK.disabled = true;
    btnListar.disabled = true;
    btnAssociar.disabled = true;

    if ((glb_bMultiplo) && (bHasRowsInGrid)) {
        for (i = 2; i < fgValores.Rows; i++) {
            if (fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'OK')) != 0) {
                btnOK.disabled = false;
                break;
            }
        }
    }
    else {
        if (bHasRowsInGrid) {
            if (getCellValueByColKey(fgValores, 'ValorID*', fgValores.Row) != '') {
                btnDetalharVL.disabled = false;
                btnDissociar.disabled = false;
            }
            else if ((getCellValueByColKey(fgValores, 'VLValor', fgValores.Row) != '') &&
					(getCellValueByColKey(fgValores, 'VLFinanceiroValorInterface', fgValores.Row) != '') &&
					(parseFloat(replaceStr(getCellValueByColKey(fgValores, 'VLValor', fgValores.Row), ',', '.')) != 0) &&
					(parseFloat(replaceStr(getCellValueByColKey(fgValores, 'VLFinanceiroValorInterface', fgValores.Row), ',', '.')) != 0)) {
                btnListar.disabled = false;

                if ((!bHasRowsInGrid2) &&
					(parseInt(getCellValueByColKey(fgValores, 'PodeIncluir', fgValores.Row), 10) == 1))
                    btnOK.disabled = false;
            }
        }

        if (bHasRowsInGrid2) {
            btnDetalharVL.disabled = false;

            if (getCellValueByColKey(fgValores, 'ValorID*', fgValores.Row) == '')
                btnAssociar.disabled = false;
        }

        if ((glb_nPedidoID > 0) && (glb_nEstadoID != 0) && (glb_nEstadoID != 24) && (glb_nEstadoID != 25) && (glb_nEstadoID != 26) && (glb_nEstadoID != 33)) {
            btnDissociar.disabled = true;
            btnOK.disabled = true;
            btnAssociar.disabled = true;
        }
    }
}

/********************************************************************
Solicitar dados do combo ao servidor
********************************************************************/
function fillComboData() {
    var strSQL = '';
    //	var strSQLVL = '';
    var nEmpresaData = getCurrEmpresaData();
    var nEmpresaID = nEmpresaData[0];

    if (glb_nTipoValorID == 1001)
        glb_nDSOs = 2;
    else
        glb_nDSOs = 1;

    // parametrizacao do dso dsoCombo
    setConnection(dsoCombo1);

    var sIncluir = ' 1 AS Incluir ';

    if (glb_nTipoValorID == 1002)
        sIncluir = ' CONVERT(NUMERIC(1), (CASE WHEN c.Filtro NOT LIKE ' + '\'' + '%[*]%' + '\'' + ' THEN 1 ELSE 0 END)) AS Incluir ';

    strSQL = 'SELECT -1 AS ItemID, SPACE(1) AS ItemAbreviado, 0 AS Ordem, CONVERT(NUMERIC(1), 0) AS Incluir ' +
		'UNION ALL SELECT c.ItemID AS ItemID, c.ItemAbreviado AS ItemAbreviado, c.Ordem AS Ordem, ' +
		sIncluir +
		'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_FormasPagamento b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK) ' +
		'WHERE (a.SujeitoID = ' + glb_nEmpresaID + ' AND a.TipoRelacaoID = 12 AND ' +
			'a.ObjetoID = 999 AND a.EstadoID = 2 AND a.RelacaoID = b.RelacaoID AND b.FormaPagamentoID = c.ItemID AND ' +
			'((c.Filtro LIKE ' + '\'' + '%(9130)%' + '\'' + '))) ' +
		'ORDER BY Ordem ';

    dsoCombo1.SQL = strSQL;

    dsoCombo1.ondatasetcomplete = fillComboData_DSC;
    dsoCombo1.Refresh();

    if (glb_nTipoValorID == 1001) {
        // parametrizacao do dso dsoCombo
        setConnection(dsoCombo2);

        strSQL = 'SELECT b.RelPesContaID, dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) AS ContaNome, ' +
					  'c.Fantasia AS ContaComplemento ' +
                      'FROM RelacoesPessoas a WITH(NOLOCK), RelacoesPessoas_Contas b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Bancos d WITH(NOLOCK) ' +
					  'WHERE a.TipoRelacaoID = 24 AND a.EstadoID = 2 AND a.SujeitoID= ' + nEmpresaID + ' ' +
					  'AND a.RelacaoID=b.RelacaoID AND b.EstadoID = 2 AND a.ObjetoID = c.PessoaID AND c.EstadoID = 2 ' +
					  'AND c.BancoID = d.BancoID AND d.EstadoID = 2 ' +
                      'ORDER BY ContaNome';

        dsoCombo2.SQL = strSQL;

        dsoCombo2.ondatasetcomplete = fillComboData_DSC;
        dsoCombo2.Refresh();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do combo
********************************************************************/
function fillComboData_DSC() {
    glb_nDSOs--;

    if (glb_nDSOs > 0)
        return null;

    if (glb_nPedidoID != null)
        glb_VLTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    if (glb_VLTimerInt != null) {
        window.clearInterval(glb_VLTimerInt);
        glb_VLTimerInt = null;
    }

    /*
    if ((fgValores.Rows > 1) && (glb_nFinanceiroID <= 0) && (glb_nPedidoID <= 0))
    {
    financeirosDesconsiderar();
    }
    */

    var aGrid = null;
    var i = 0;
    var strSQL = '';
    var sFiltro = '';
    var sDataInicio = trimStr(txtDtInicio.value) + ' 00:00:00.000';
    var sDataFim = trimStr(txtDtFim.value) + ' 23:59:59.997';
    var sCodigoBarra = trimStr(txtCodigoBarra.value);
    var nRelPesContaID = 'NULL';

    lockControlsInModalWin(true);


    if (fgPesquisa.Rows > 1) {
        fgPesquisa.Rows = 1;
    }


    glb_nDSOs = 1;

    // zera o grid
    fgValores.Rows = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    // Form Pedidos
    if (glb_nPedidoID > 0) {
        if ((glb_nEstadoID != 24) && (glb_nEstadoID != 25) && (glb_nEstadoID != 26) && (glb_nEstadoID != 33)) {
            strSQL = 'SELECT NULL AS RelPesContaID, CONVERT(BIT, 0) AS OK, b.PrazoPagamento AS PrazoPagamento, DATEADD(dd, b.PrazoPagamento, dbo.fn_Data_Zero(GETDATE())) AS dtVencimento, ' +
				'NULL AS Pessoa, NULL AS Parceiro, NULL AS PedidoID, NULL AS Duplicata, b.ValorParcela AS ValorParcela, NULL AS FinanceiroID, NULL AS ValorID, NULL AS Estado, ' +
				'NULL AS Documento, NULL AS Emitente, ' +
				'dbo.fn_Data_Zero(GETDATE()) AS dtEmissao, CONVERT(VARCHAR(15), NULL) AS BancoAgencia, CONVERT(VARCHAR(17), NULL) AS Conta, CONVERT(VARCHAR(16), NULL) AS NumeroDocumento, ' +
				'e.SimboloMoeda AS Moeda, ' +
				'b.ValorParcela AS VLValor, ' +
                '0 AS ValorEncargos, ' +
				'b.ValorParcela AS VLFinanceiroValorInterface, ' +
				'CONVERT(VARCHAR(30), NULL) AS Observacao, ' +
				'c.Ordem AS Ordem, c.ItemID AS FormaPagamentoID, ' +
				'dbo.fn_Financeiro_DadosBancarios (b.FinanceiroID) AS DadosBancarios, ' +
                'b.ValorParcela AS VLFinanceiroValor, ' +
				'CONVERT(NUMERIC(1), (CASE WHEN ISNULL(c.Filtro, SPACE(0)) LIKE ' + '\'' + '%[*]%' + '\'' + ' THEN 0 ELSE 1 END)) AS PodeIncluir, ' +
				'NULL AS FormaPagamentoVLID, ' +
				'0 AS Retencoes, ' +
			    '0 AS PCC, ' +
			    '0 AS RetencoesGerais, ' +
			    '0 AS GerarValor ' +
				'FROM Pedidos a WITH(NOLOCK), Pedidos_Parcelas b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Conceitos e WITH(NOLOCK) ' +
				'WHERE (a.PedidoID = ' + glb_nPedidoID + ' AND a.PedidoID = b.PedidoID AND b.HistoricoPadraoID IS NULL AND b.FormaPagamentoID = c.ItemID AND ' +
					'((c.Filtro LIKE ' + '\'' + '%(9130)%' + '\'' + ')) AND ' +
					'a.PessoaID = d.PessoaID AND a.MoedaID = e.ConceitoID) ' +
				'ORDER BY Ordem, FinanceiroID';
        }
        else {
            strSQL = 'SELECT NULL AS RelPesContaID, CONVERT(BIT, 0) AS OK, a.PrazoPagamento AS PrazoPagamento, c.dtVencimento AS dtVencimento, ' +
				'NULL AS Pessoa, NULL AS Parceiro, NULL AS PedidoID, NULL AS Duplicata, a.ValorParcela AS ValorParcela, a.FinanceiroID AS FinanceiroID, NULL AS ValorID, NULL AS Estado, ' +
				'NULL AS Documento, NULL AS Emitente, ' +
				'dbo.fn_Data_Zero(GETDATE()) AS dtEmissao, CONVERT(VARCHAR(11), NULL) AS BancoAgencia, CONVERT(VARCHAR(17), NULL) AS Conta, CONVERT(VARCHAR(6), NULL) AS NumeroDocumento, ' +
				'e.SimboloMoeda AS Moeda, ' +
				'(CASE a.FormaPagamentoID WHEN 1032 THEN NULL ELSE dbo.fn_ValorLocalizar_Posicao(c.FinanceiroID, 5) END) AS VLValor, ' +
                '0 AS ValorEncargos, ' +
				'(CASE a.FormaPagamentoID WHEN 1032 THEN NULL ELSE dbo.fn_ValorLocalizar_Posicao(c.FinanceiroID, 5) END) AS VLFinanceiroValorInterface, ' +
				'CONVERT(VARCHAR(30), NULL) AS Observacao, ' +
				'b.Ordem AS Ordem, b.ItemID AS FormaPagamentoID, ' +
				'dbo.fn_Financeiro_DadosBancarios (a.FinanceiroID) AS DadosBancarios, ' +
                'dbo.fn_ValorLocalizar_Posicao(c.FinanceiroID, 5) AS VLFinanceiroValor, ' +
				'CONVERT(NUMERIC(1), (CASE WHEN ISNULL(b.Filtro, SPACE(0)) LIKE ' + '\'' + '%[*]%' + '\'' + ' THEN 0 ELSE 1 END)) AS PodeIncluir, ' +
				'NULL AS FormaPagamentoVLID, ' +
                '0 AS Retencoes, ' +
			    '0 AS PCC, ' +
			    '0 AS RetencoesGerais, ' +
			    '0 AS GerarValor ' +
				'FROM Pedidos_Parcelas a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Financeiro c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Conceitos e WITH(NOLOCK) ' +
				'WHERE (a.PedidoID = ' + glb_nPedidoID + ' AND a.HistoricoPadraoID IS NULL AND a.FormaPagamentoID = b.ItemID AND ' +
					'((b.Filtro LIKE ' + '\'' + '%(9130)%' + '\'' + ')) AND ' +
					'a.FinanceiroID = c.FinanceiroID AND c.PessoaID = d.PessoaID AND ' +
					'c.MoedaID = e.ConceitoID AND ' +
					'dbo.fn_ValorLocalizar_Posicao(c.FinanceiroID, 5) > 0) ';

            if (!chkPendente.checked)
                strSQL += ' UNION ALL ' +
					'SELECT NULL AS RelPesContaID, CONVERT(BIT, 0) AS OK, a.PrazoPagamento AS PrazoPagamento, c.dtVencimento AS dtVencimento, ' +
						'NULL AS Pessoa, NULL AS Parceiro, NULL AS PedidoID, NULL AS Duplicata, a.ValorParcela AS ValorParcela, a.FinanceiroID AS FinanceiroID, f.ValorID AS ValorID, h.RecursoAbreviado AS Estado, ' +
						'f.Documento AS Documento, f.Emitente AS Emitente, ' +
						'f.dtEmissao AS dtEmissao, f.BancoAgencia AS BancoAgencia, f.Conta, f.NumeroDocumento AS NumeroDocumento, ' +
						'g.SimboloMoeda AS Moeda, ' +
						'f.Valor AS VLValor, ' +
                        'ISNULL(e.ValorEncargos, 0) AS ValorEncargos, ' +
                        'e.Valor AS VLFinanceiroValorInterface, ' +
						'e.Observacao AS Observacao, ' +
						'b.Ordem AS Ordem, b.ItemID AS FormaPagamentoID, dbo.fn_Financeiro_DadosBancarios (a.FinanceiroID) AS DadosBancarios, e.Valor AS VLFinanceiroValor, ' +
						'CONVERT(NUMERIC(1), (CASE WHEN ISNULL(b.Filtro, SPACE(0)) LIKE ' + '\'' + '%[*]%' + '\'' + ' THEN 0 ELSE 1 END)) AS PodeIncluir, ' +
						'f.FormaPagamentoID AS FormaPagamentoVLID, ' +
                        '0 AS Retencoes, ' +
			            '0 AS PCC, ' +
			            '0 AS RetencoesGerais, ' +
			            '0 AS GerarValor ' +
						'FROM Pedidos_Parcelas a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Financeiro c WITH(NOLOCK), Pessoas d WITH(NOLOCK), ' +
							'ValoresLocalizar_Financeiros e WITH(NOLOCK), ValoresLocalizar f WITH(NOLOCK), Conceitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) ' +
						'WHERE (a.PedidoID = ' + glb_nPedidoID + ' AND a.HistoricoPadraoID IS NULL AND a.FormaPagamentoID = b.ItemID AND ' +
							'((b.Filtro LIKE ' + '\'' + '%(9130)%' + '\'' + ')) AND ' +
							'a.FinanceiroID = c.FinanceiroID AND c.PessoaID = d.PessoaID AND  ' +
							'a.FinanceiroID = e.FinanceiroID AND e.ValorID = f.ValorID AND ' +
							'f.MoedaID = g.ConceitoID AND f.EstadoID = h.RecursoID) ';

            strSQL += ' ORDER BY Ordem, FinanceiroID';
        }
    }
        // Form Financeiro
    else if (glb_nFinanceiroID > 0) {
        strSQL = 'SELECT ' + nRelPesContaID + ' AS RelPesContaID, CONVERT(BIT, 0) AS OK, a.PrazoPagamento AS PrazoPagamento, a.dtVencimento AS dtVencimento, ' +
			'NULL AS Pessoa, NULL AS Parceiro, NULL AS PedidoID, NULL AS Duplicata, a.Valor AS ValorParcela, a.FinanceiroID AS FinanceiroID, NULL AS ValorID, NULL AS Estado, ' +
			'NULL AS Documento, NULL AS Emitente, ' +
			'dbo.fn_Data_Zero(GETDATE()) AS dtEmissao, CONVERT(VARCHAR(9), NULL) AS BancoAgencia, CONVERT(VARCHAR(15), NULL) AS Conta, CONVERT(VARCHAR(6), NULL) AS NumeroDocumento, ' +
			'd.SimboloMoeda AS Moeda, ' +
        /*
        'dbo.fn_ValorLocalizar_Posicao(a.FinanceiroID, 5) AS VLValor, ' +
        'dbo.fn_ValorLocalizar_Posicao(a.FinanceiroID, 5) AS VLFinanceiroValorInterface, ' +
        */
        //Alterado para retencoes
			'aa.Saldo AS VLValor, ' +
            '0 AS ValorEncargos, ' +
		    'aa.Saldo AS VLFinanceiroValorInterface, ' +
        //Fim: Alterado para retencoes
			'CONVERT(VARCHAR(30), NULL) AS Observacao, ' +
			'b.Ordem AS Ordem, (CASE WHEN ISNULL(b.Filtro, SPACE(0)) LIKE ' + '\'' + '%(9130)%' + '\'' + ' THEN b.ItemID ELSE NULL END) AS FormaPagamentoID, dbo.fn_Financeiro_DadosBancarios (a.FinanceiroID) AS DadosBancarios, ' +
            'dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, a.dtVencimento, NULL, NULL) AS VLFinanceiroValor, ' +
			'CONVERT(NUMERIC(1), (CASE WHEN ISNULL(b.Filtro, SPACE(0)) NOT LIKE ' + '\'' + '%[*]%' + '\'' + ' OR a.TipoFinanceiroID = 1001 THEN 1 ELSE 0 END)) AS PodeIncluir, ' +
			'NULL AS FormaPagamentoVLID, 0 AS Retencoes, ' +
			'0 AS PCC, ' +
			'0 AS RetencoesGerais, ' +
            '0 AS GerarValor ' +
			'FROM Financeiro a WITH(NOLOCK) ' +
			    'INNER JOIN TiposAuxiliares_Itens b WITH ( NOLOCK ) ON (a.FormaPagamentoID = b.ItemID)' +
			    'INNER JOIN Pessoas c WITH ( NOLOCK ) ON (a.PessoaID = c.PessoaID)' +
			    'INNER JOIN Conceitos d WITH ( NOLOCK ) ON (a.MoedaID = d.ConceitoID) ' +
			    'INNER JOIN (SELECT aa.FinanceiroID, dbo.fn_Financeiro_SaldoPagamento(aa.FinanceiroID) [Saldo] FROM dbo.Financeiro aa WITH(NOLOCK)) aa ON (a.FinanceiroID = aa.FinanceiroID) ' +
			'WHERE (a.FinanceiroID = ' + glb_nFinanceiroID + ' AND ' +
			       'a.EstadoID NOT IN (1,5,48,49) AND ' +
				   'aa.Saldo > 0) ';

        if (!chkPendente.checked)
            strSQL += ' UNION ALL ' +
				'SELECT e.RelPesContaID AS RelPesContaID, CONVERT(BIT, 0) AS OK, a.PrazoPagamento AS PrazoPagamento, a.dtVencimento AS dtVencimento, ' +
					'NULL AS Pessoa, NULL AS Parceiro, NULL AS PedidoID, NULL AS Duplicata, a.Valor AS ValorParcela, a.FinanceiroID AS FinanceiroID, e.ValorID AS ValorID, g.RecursoAbreviado AS Estado, ' +
					'e.Documento AS Documento, e.Emitente AS Emitente, ' +
					'e.dtEmissao AS dtEmissao, e.BancoAgencia AS BancoAgencia, e.Conta, e.NumeroDocumento AS NumeroDocumento, ' +
					'f.SimboloMoeda AS Moeda, ' +
					'e.Valor AS VLValor, ' +
                    'ISNULL(d.ValorEncargos, 0) AS ValorEncargos, ' +
                    'd.Valor AS VLFinanceiroValorInterface, ' +
					'd.Observacao AS Observacao, ' +
					'b.Ordem AS Ordem, b.ItemID AS FormaPagamentoID, dbo.fn_Financeiro_DadosBancarios (a.FinanceiroID) AS DadosBancarios, d.Valor AS VLFinanceiroValor, ' +
					'CONVERT(NUMERIC(1), (CASE WHEN ISNULL(b.Filtro, SPACE(0)) NOT LIKE ' + '\'' + '%[*]%' + '\'' + ' OR a.TipoFinanceiroID = 1001 THEN 1 ELSE 0 END)) AS PodeIncluir, ' +
					'e.FormaPagamentoID AS FormaPagamentoVLID, ' +
					'0 AS Retencoes, ' +
					'0 AS PCC, ' +
					'0 AS RetencoesGerais, ' +
					'0 AS GerarValor ' +
					'FROM Financeiro a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK), ValoresLocalizar_Financeiros d WITH(NOLOCK), ValoresLocalizar e WITH(NOLOCK), Conceitos f WITH(NOLOCK), Recursos g WITH(NOLOCK) ' +
					'WHERE (a.FinanceiroID = ' + glb_nFinanceiroID + ' AND a.EstadoID NOT IN (1,5,48,49) AND e.FormaPagamentoID = b.ItemID AND ' +
						'a.PessoaID = c.PessoaID AND a.FinanceiroID = d.FinanceiroID AND d.ValorID = e.ValorID AND ' +
						'e.MoedaID = f.ConceitoID AND e.EstadoID = g.RecursoID) ';

        strSQL += ' ORDER BY Ordem, FinanceiroID';
    }
        // Form Financeiro varios registros
    else {
        if (sDataInicio != '') {
            sDataInicio = normalizeDate_DateTime(sDataInicio, 1);

            sDataInicio = '\'' + sDataInicio + '\'';
        }
        else
            sDataInicio = 'dbo.fn_Data_Zero(GETDATE())';

        if (sDataFim != '') {
            sDataFim = normalizeDate_DateTime(sDataFim, 1);

            sDataFim = '\'' + sDataFim + '\'';
        }
        else
            sDataFim = 'dbo.fn_Data_Zero(GETDATE())';

        if (chkPedido.checked) {
            
            if (chkServico.checked)
                sFiltro = ' AND a.PedidoID IS NOT NULL AND dbo.fn_Pedido_TemServico (a.PedidoID) = 1 ';
            else
                sFiltro = ' AND a.PedidoID IS NOT NULL AND dbo.fn_Pedido_TemServico (a.PedidoID) = 0 ';

        }
        else {
            sFiltro = ' AND a.PedidoID IS NULL ';
        }

        if (selHistoricoPadraoID.value != 0)
            sFiltro += ' AND a.HistoricoPadraoID = ' + selHistoricoPadraoID.value + ' ';
        /*
        else
        sFiltro += ' AND a.HistoricoPadraoID IS NULL ';
        */
        if (selPessoaID2.value != 0) {
            if (chkParceiro.checked)
                sFiltro += ' AND a.ParceiroID = ' + selPessoaID2.value + ' ';
            else
                sFiltro += ' AND a.PessoaID = ' + selPessoaID2.value + ' ';
        }

        if (sCodigoBarra != '')
            sFiltro += ' AND a.CodigoBarra = ' + '\'' + sCodigoBarra + '\'' + ' ';

        strSQL = ' SELECT ' + nRelPesContaID + ' AS RelPesContaID, CONVERT(BIT, 0) AS OK, a.PrazoPagamento AS PrazoPagamento, a.dtVencimento AS dtVencimento, ' +
			'c.Fantasia AS Pessoa, e.Fantasia AS Parceiro, a.PedidoID AS PedidoID, a.Duplicata AS Duplicata, a.Valor AS ValorParcela, a.FinanceiroID AS FinanceiroID, NULL AS ValorID, NULL AS Estado, ' +
			'NULL AS Documento, NULL AS Emitente, ' +
			'dbo.fn_Data_Zero(GETDATE()) AS dtEmissao, CONVERT(VARCHAR(9), NULL) AS BancoAgencia, CONVERT(VARCHAR(15), NULL) AS Conta, CONVERT(VARCHAR(6), NULL) AS NumeroDocumento, ' +
			'd.SimboloMoeda AS Moeda, ' +
        /*
        'dbo.fn_ValorLocalizar_Posicao(a.FinanceiroID, 5) AS VLValor, ' +
        'dbo.fn_ValorLocalizar_Posicao(a.FinanceiroID, 5) AS VLFinanceiroValorInterface, ' +
        */
        //Alterado para retencoes
			'aa.Saldo AS VLValor, ' +
            '0 AS ValorEncargos, ' +
            'aa.Saldo AS VLFinanceiroValorInterface, ' +
        //Fim: Alterado para retencoes
			'CONVERT(VARCHAR(30), NULL) AS Observacao, ' +
			'b.Ordem AS Ordem, (CASE WHEN ISNULL(b.Filtro, SPACE(0)) LIKE ' + '\'' + '%(9130)%' + '\'' + ' THEN b.ItemID ELSE NULL END) AS FormaPagamentoID, dbo.fn_Financeiro_DadosBancarios (a.FinanceiroID) AS DadosBancarios, ' +
			'dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, a.dtVencimento, NULL, NULL) AS VLFinanceiroValor, ' +
			'CONVERT(NUMERIC(1), (CASE WHEN ISNULL(b.Filtro, SPACE(0)) NOT LIKE ' + '\'' + '%[*]%' + '\'' + ' OR a.TipoFinanceiroID = 1001 THEN 1 ELSE 0 END)) AS PodeIncluir, ' +
			'NULL AS FormaPagamentoVLID, 0 AS Retencoes, ' +
			'0 AS PCC, ' +
			'0 AS RetencoesGerais, ' +
			'0 AS GerarValor ' +
			'FROM Financeiro a WITH(NOLOCK) ' +
			    'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (a.FormaPagamentoID = b.ItemID) ' +
			    'INNER JOIN Pessoas c WITH(NOLOCK) ON (a.PessoaID = c.PessoaID) ' +
			    'INNER JOIN Conceitos d WITH(NOLOCK) ON (a.MoedaID = d.ConceitoID) ' +
			    'INNER JOIN Pessoas e WITH(NOLOCK) ON (a.ParceiroID = e.PessoaID) ' +
			    'INNER JOIN (SELECT aa.FinanceiroID, dbo.fn_Financeiro_SaldoPagamento(aa.FinanceiroID) [Saldo] FROM dbo.Financeiro aa WITH(NOLOCK)) aa ON (a.FinanceiroID = aa.FinanceiroID) ' +
                'LEFT JOIN Financeiro_Retencoes_2 f WITH(NOLOCK) ON ((f.FinanceiroID = a.FinanceiroID) AND (f.ImpostoID = 18)) ' +
			'WHERE (a.EmpresaID = ' + glb_nEmpresaID + ' ' +
			       'AND a.EstadoID NOT IN (1,5,48,49) ' +
			       'AND a.TipoFinanceiroID = ' + glb_nTipoValorID + ' ' +
				   'AND a.dtVencimento BETWEEN ' + sDataInicio + ' AND ' + sDataFim + ' AND ' +
				   'aa.Saldo > 0 ' +
				   sFiltro +
                   'AND ISNULL(f.ValorRetencao, 0) = 0 ) ';

        if (!chkPendente.checked)
            strSQL += ' UNION ALL ' +
				'SELECT e.RelPesContaID AS RelPesContaID, CONVERT(BIT, 0) AS OK, a.PrazoPagamento AS PrazoPagamento, a.dtVencimento AS dtVencimento, ' +
					'c.Fantasia AS Pessoa, h.Fantasia AS Parceiro, a.PedidoID AS PedidoID, a.Duplicata AS Duplicata, a.Valor AS ValorParcela, a.FinanceiroID AS FinanceiroID, e.ValorID AS ValorID, g.RecursoAbreviado AS Estado, ' +
					'e.Documento AS Documento, e.Emitente AS Emitente, ' +
					'e.dtEmissao AS dtEmissao, e.BancoAgencia AS BancoAgencia, e.Conta, e.NumeroDocumento AS NumeroDocumento, ' +
					'f.SimboloMoeda AS Moeda, ' +
					'e.Valor AS VLValor, ' +
                    'ISNULL(d.ValorEncargos, 0) AS ValorEncargos, ' +
                    'd.Valor AS VLFinanceiroValorInterface, ' +
					'd.Observacao AS Observacao, ' +
					'b.Ordem AS Ordem, b.ItemID AS FormaPagamentoID, dbo.fn_Financeiro_DadosBancarios (a.FinanceiroID) AS DadosBancarios, d.Valor AS VLFinanceiroValor, ' +
					'CONVERT(NUMERIC(1), (CASE WHEN ISNULL(b.Filtro, SPACE(0)) NOT LIKE ' + '\'' + '%[*]%' + '\'' + ' OR a.TipoFinanceiroID = 1001 THEN 1 ELSE 0 END)) AS PodeIncluir, ' +
					'e.FormaPagamentoID AS FormaPagamentoVLID, ' +
					'0 AS Retencoes, ' +
					'0 AS PCC, ' +
					'0 AS RetencoesGerais, ' +
					'0 AS GerarValor ' +
					'FROM Financeiro a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK), ValoresLocalizar_Financeiros d WITH(NOLOCK), ValoresLocalizar e WITH(NOLOCK), Conceitos f WITH(NOLOCK), Recursos g WITH(NOLOCK), Pessoas h WITH(NOLOCK) ' +
					'WHERE (a.EmpresaID = ' + glb_nEmpresaID + ' AND a.EstadoID NOT IN (1,5,48,49) AND a.TipoFinanceiroID = ' + glb_nTipoValorID + ' AND ' +
						'a.dtVencimento BETWEEN ' + sDataInicio + ' AND ' + sDataFim + ' AND ' +
						'e.FormaPagamentoID = b.ItemID AND a.PessoaID = c.PessoaID AND a.FinanceiroID = d.FinanceiroID AND d.ValorID = e.ValorID AND ' +
						'e.MoedaID = f.ConceitoID AND e.EstadoID = g.RecursoID AND a.ParceiroID = h.PessoaID ' + sFiltro + ') ';

        strSQL += ' ORDER BY dtVencimento, Pessoa, FinanceiroID';
    }

    dsoGrid.SQL = strSQL;

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;

    if (glb_nDSOs > 0)
        return null;

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var i;
    var bTempValue;

    fgValores.Redraw = 0;
    fgValores.Editable = false;
    startGridInterface(fgValores, 1, 1);

    fgValores.FontSize = '8';
    fgValores.FrozenCols = 0;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    var aHoldCols = new Array();

    if (glb_sCaller == 'PL')
        aHoldCols = [0, 28, 29, 30, 31, 32];
    else
        aHoldCols = [0, 6, 7, 8, 9, 28, 29, 30, 31, 32];

    if (glb_nTipoValorID == 1001) {
        aHoldCols[aHoldCols.length] = 15;
        aHoldCols[aHoldCols.length] = 16;
        aHoldCols[aHoldCols.length] = 17;
        aHoldCols[aHoldCols.length] = 18;
        aHoldCols[aHoldCols.length] = 19;
        aHoldCols[aHoldCols.length] = 20;

        if (glb_bMultiplo) {
            aHoldCols[aHoldCols.length] = 1;
            aHoldCols[aHoldCols.length] = 2;
        }
        else
            aHoldCols[aHoldCols.length] = 3;
    }
    else {
        aHoldCols[aHoldCols.length] = 2;
        aHoldCols[aHoldCols.length] = 3;
        aHoldCols[aHoldCols.length] = 25; //Retencoes. Adicionado paro o projeto web. BJBN
        aHoldCols[aHoldCols.length] = 26; //Altera��o para reten��o
    }

    headerGrid(fgValores, ['_calc_temp_1*',
						  'Form',
						  'Bco/Ag/Cta',
						  'OK',
						  'Praz',
						  'Vencimento',
						  'Pessoa',
						  'Parceiro',
						  'Pedido',
						  'Duplicata',
						  'Valor',
						  'Financ',
						  'Dados Banc�rios',
						  'ValorID',
						  'E',
						  'Documento',
						  'Emitente',
						  'Emiss�o',
						  'Banco/Ag�ncia',
						  'Conta',
						  'N�mero',
						  '$',
						  'Valor',
                          'Encargos',
						  'Aplicar',
						  'Reten��es',
						  'Valor Aplicado',
						  'Observa��o',
						  'Valor',
						  'PodeIncluir',
						  'PCC',
						  'RetencoesGerais',
						  'FormaPagamentoVLID'], aHoldCols);

    glb_aCelHint = [[0, 12, 'Banco / Ag�ncia / Conta']];

    fillGridMask(fgValores, dsoGrid, ['_calc_temp_1*',
									'FormaPagamentoID',
									'RelPesContaID',
									'OK',
									'PrazoPagamento*',
									'dtVencimento*',
									'Pessoa*',
									'Parceiro*',
									'PedidoID*',
									'Duplicata*',
									'ValorParcela*',
									'FinanceiroID*',
									'DadosBancarios*',
									'ValorID*',
									'Estado*',
									'Documento',
									'Emitente',
									'dtEmissao',
									'BancoAgencia',
									'Conta',
									'NumeroDocumento',
									'Moeda*',
									'VLValor',
                                    'ValorEncargos',
									'VLFinanceiroValorInterface',
									'Retencoes*', //25
									'GerarValor*', //26
									'Observacao', //27
								    'VLFinanceiroValor', //28
								    'PodeIncluir', //29
								    'PCC*', //30
								    'RetencoesGerais*', //31
								    'FormaPagamentoVLID'],
									['', '', '', '', '', '99/99/9999', '', '', '', '', '999999999.99', '9999999999', '', '', '', '', '', '99/99/9999', '', '', '', '', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '', '', '', '', ''],
									['', '', '', '', '', dTFormat, '', '', '', '', '###,###,##0.00', '', '', '', '', '', '', dTFormat, '', '', '', '', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '', '', '', '', '']);

    fgValores.Redraw = 0;

    //glb_aCelHint = [0, 12, 'Banco | Ag�ncia | Conta | Nome do banco'];


    insertcomboData(fgValores, getColIndexByColKey(fgValores, 'FormaPagamentoID'), dsoCombo1, 'ItemAbreviado', 'ItemID');

    if (glb_nTipoValorID == 1001) {
        insertcomboData(fgValores, getColIndexByColKey(fgValores, 'RelPesContaID'), dsoCombo2, '*ContaNome|ContaComplemento', 'RelPesContaID');
    }

    if (glb_bMultiplo) {
        gridHasTotalLine(fgValores, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fgValores, 'ValorParcela*'), '###,###,###,###.00', 'S'],
															  [getColIndexByColKey(fgValores, 'VLValor'), '###,###,###,###.00', 'S'],
															  [getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface'), '###,###,###,###.00', 'S'],
															  [getColIndexByColKey(fgValores, 'GerarValor*'), '###,###,###,###.00', 'S']]);
        calculaTotais();
    }

    alignColsInGrid(fgValores, [4, 8, 10, 11, 13, 21, 22]);

    fgValores.AutoSizeMode = 0;
    fgValores.AutoSize(0, fgValores.Cols - 1);

    fgValores.FrozenCols = 7;

    paintCellsSpecialyReadOnly(fgValores);

    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a partir da primeira coluna readonly

    if (fgValores.Rows > 1) {
        if ((glb_bMultiplo) && (fgValores.Rows > 2)) {
            fgValores.Row = 2;
            fgValores.LeftCol = getColIndexByColKey(fgValores, 'PedidoID*');
        }
        else if (!glb_bMultiplo)
            fgValores.Row = 1;

        fgValores.Col = getColIndexByColKey(fgValores, 'FormaPagamentoID');
        fgValores.Editable = true;
    }

    fgValores.ExplorerBar = 5;

    fgValores.ColWidth(getColIndexByColKey(fgValores, 'VLValor')) = 150 * FONT_WIDTH;
    fgValores.ColWidth(getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')) = 150 * FONT_WIDTH;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    fgValores.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fgValores.Rows > 1) {
        window.focus();
        fgValores.focus();
    }
    else {
        ;
    }

    // ajusta estado dos botoes
    setupBtnsFromGridState();

    if (fgValores.Rows > 1) {
        //Recalcula os valores de reten��o
        recalcularRetencoes(true);
    }


}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridPesquisa() {
    if (glb_VLTimerInt != null) {
        window.clearInterval(glb_VLTimerInt);
        glb_VLTimerInt = null;
    }

    if (fgValores.Row < 1) {
        if (window.top.overflyGen.Alert('Selecione uma linha no grid superior.') == 0)
            return null;

        return true;
    }

    var aGrid = null;
    var i = 0;

    lockControlsInModalWin(true);

    var nTop = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');
    var nFormaPagamentoID = 0;
    var nRelPesContaID = 0;
    var nPodeIncluir = 0;
    var sDocumento = '';
    var sEmitente = '';
    var sBancoAgencia = '';
    var sNumeroDocumento = '';
    var sdtBalancete = '';
    var sdtVencimento = '';
    var nVariacaoValor = 0;
    var sFiltroVariacaoValor = '';
    var nVariacaoDias = 0;
    var sFiltroVariacaoDias = '';
    var nPessoaID = 0;
    var sOuterJoin = 'LEFT OUTER';
    var nValorFinanceiro = 0;
    var nValorAplicar = 0;
    var sFiltro = '';
    var sFiltroEstados = '';
    var nFilialID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'FilialID\'].value');

    nFormaPagamentoID = getCellValueByColKey(fgValores, 'FormaPagamentoID', fgValores.Row);

    if ((nFormaPagamentoID == '') || (nFormaPagamentoID <= 0)) {
        lockControlsInModalWin(false);

        if (window.top.overflyGen.Alert('Definir forma de pagamento.') == 0)
            return null;

        return null;
    }

    nRelPesContaID = getCellValueByColKey(fgValores, 'RelPesContaID', fgValores.Row);

    if ((nRelPesContaID != '') && (nRelPesContaID > 0))
        sFiltro += ' AND a.RelPesContaID = ' + nRelPesContaID + ' ';

    nPodeIncluir = getCellValueByColKey(fgValores, 'PodeIncluir', fgValores.Row);
    sDocumento = trimStr(getCellValueByColKey(fgValores, 'Documento', fgValores.Row));
    sEmitente = trimStr(getCellValueByColKey(fgValores, 'Emitente', fgValores.Row));
    sBancoAgencia = trimStr(getCellValueByColKey(fgValores, 'BancoAgencia', fgValores.Row));
    sNumeroDocumento = trimStr(getCellValueByColKey(fgValores, 'NumeroDocumento', fgValores.Row));

    sFiltro += ' AND a.FormaPagamentoID = ' + nFormaPagamentoID + ' ';

    if (sDocumento != '')
        sFiltro += ' AND a.Documento LIKE ' + '\'' + '%' + sDocumento + '%' + '\'' + ' ';
    if (sEmitente != '')
        sFiltro += ' AND a.Emitente LIKE ' + '\'' + '%' + sEmitente + '%' + '\'' + ' ';
    if (sBancoAgencia != '')
        sFiltro += ' AND a.BancoAgencia LIKE ' + '\'' + '%' + sBancoAgencia + '%' + '\'' + ' ';
    if (sNumeroDocumento != '')
        sFiltro += ' AND a.NumeroDocumento LIKE ' + '\'' + '%' + sNumeroDocumento + '%' + '\'' + ' ';

    // Cheque
    if (nFormaPagamentoID == 1032)
        sdtBalancete = 'a.dtApropriacao';
    else
        sdtBalancete = 'a.dtEmissao';

    if (parseInt(nPodeIncluir, 10) == 1)
        sFiltroEstados = '51, 41, 53, 56';
    else
        sFiltroEstados = '41, 56';

    sdtVencimento = dateFormatToSearch(getCellValueByColKey(fgValores, 'dtVencimento*', fgValores.Row));

    txtValorID.value = trimStr(txtValorID.value);
    if (txtValorID.value != '')
        sFiltro += ' AND a.ValorID = ' + parseInt(txtValorID.value, 10) + ' ';

    nPessoaID = parseInt(selPessoaID.value, 10);
    if (nPessoaID > 0)
        sFiltro += ' AND a.PessoaID = ' + nPessoaID + ' ';
    else {
        txtFantasia.value = trimStr(txtFantasia.value);
        if (txtFantasia.value != '') {
            sFiltro += ' AND e.Fantasia LIKE ' + '\'' + '%' + txtFantasia.value + '%' + '\'' + ' ';
            sOuterJoin = 'INNER';
        }
    }

    nValorFinanceiro = parseFloat(replaceStr(getCellValueByColKey(fgValores, 'VLValor', fgValores.Row), ',', '.'));
    nValorAplicar = parseFloat(replaceStr(getCellValueByColKey(fgValores, 'VLFinanceiroValorInterface', fgValores.Row), ',', '.'));

    txtVariacaoValor.value = trimStr(txtVariacaoValor.value);
    if (txtVariacaoValor.value != '') {
        nVariacaoValor = parseFloat(txtVariacaoValor.value);
        sFiltroVariacaoValor = ' AND (a.Valor BETWEEN ' +
					'( ' + (nValorFinanceiro - nVariacaoValor) + ') AND ' +
					'( ' + (nValorFinanceiro + nVariacaoValor) + ')) ';

    }
    txtVariacaoDias.value = trimStr(txtVariacaoDias.value);
    if (txtVariacaoDias.value != '') {
        nVariacaoDias = parseFloat(txtVariacaoDias.value);
        sFiltroVariacaoDias = ' AND (' + '\'' + sdtVencimento + '\'' + ' BETWEEN ' +
						'DATEADD (dd, ' + (-nVariacaoDias) + ', ' + sdtBalancete + ') AND ' +
						'DATEADD (dd, ' + nVariacaoDias + ', ' + sdtBalancete + ')) ';

    }

    glb_nDSOs = 1;

    // zera o grid
    fgPesquisa.Rows = 1;


    //Vari�vel que verifica se o Pedido tem o FilialID Gravado


    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    strSQLVL = 'SELECT TOP ' + nTop + ' f.Fantasia AS Empresa, b.ItemAbreviado AS FormaPagamento, a.ValorID AS ValorID, c.RecursoAbreviado AS Estado, ' +
			'a.Documento AS Documento, a.Emitente AS Emitente, ' +
			'a.dtEmissao AS dtEmissao, a.dtApropriacao AS dtApropriacao, ' +
			'a.BancoAgencia AS BancoAgencia, a.Conta, a.NumeroDocumento AS NumeroDocumento, ' +
			'd.SimboloMoeda AS Moeda, a.Valor AS Valor, 0 AS ValorEncargos, dbo.fn_ValorLocalizar_Posicao(a.ValorID, 4) AS SaldoFinanceiro, a.Historico ' +
		'FROM ValoresLocalizar a WITH(NOLOCK) ' +
		        'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.FormaPagamentoID = b.ItemID ' +
		        'INNER JOIN Recursos c WITH(NOLOCK) ON a.EstadoID = c.RecursoID ' +
		        'INNER JOIN Conceitos d WITH(NOLOCK) ON a.MoedaID = d.ConceitoID ' +
		        sOuterJoin + ' JOIN Pessoas e WITH(NOLOCK) ON a.PessoaID = e.PessoaID ' +
		        'INNER JOIN Pessoas f WITH(NOLOCK) ON f.PessoaID = a.EmpresaID ' +
  //Associacao VL entre Matriz e Filial, Solicitado por Barbara Rezende. TSA.: 19/2/2013
            'WHERE a.EmpresaID IN ((dbo.fn_Empresa_Filial(' + glb_nEmpresaID + ', 1, NULL, NULL)), (dbo.fn_Empresa_Filial(' + glb_nEmpresaID + ', 1, NULL, 1)), ' + glb_nEmpresaID + ') ' +
                'AND (a.EstadoID IN (' + sFiltroEstados + ') AND ' +
			        'a.TipoValorID = ' + glb_nTipoValorID + ' AND ' +
			        'dbo.fn_ValorLocalizar_Posicao(a.ValorID, 4) > 0 ' +
				        sFiltroVariacaoValor + sFiltroVariacaoDias + sFiltro + ') ' +
		        'ORDER BY b.Ordem, a.ValorID';


    dsoGrid.SQL = strSQLVL;
    dsoGrid.ondatasetcomplete = fillGridPesquisa_DSC;
    dsoGrid.Refresh();


}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridPesquisa_DSC() {
    glb_nDSOs--;

    if (glb_nDSOs > 0)
        return null;

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var i;
    var bTempValue;

    fgPesquisa.Redraw = 0;
    fgPesquisa.Editable = false;
    startGridInterface(fgPesquisa, 1, 1);

    fgPesquisa.FontSize = '8';
    fgPesquisa.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fgPesquisa, ['Empresa',
	                       'Forma',
						   'ValorID',
						   'Est',
						   'Documento',
						   'Emitente',
						   'Emiss�o',
						   'Apropria��o',
						   'Banco/Ag�ncia',
						   'Conta',
						   'N�mero',
						   '$',
						   'Valor',
                           'Encargos',
						   'Saldo Financ',
						   'Historico'], []);

    fillGridMask(fgPesquisa, dsoGrid, ['Empresa',
                                     'FormaPagamento',
									 'ValorID',
									 'Estado',
									 'Documento',
									 'Emitente',
									 'dtEmissao',
									 'dtApropriacao',
									 'BancoAgencia',
									 'Conta',
									 'NumeroDocumento',
									 'Moeda',
									 'Valor',
                                     'ValorEncargos',
									 'SaldoFinanceiro',
									 'Historico'],
									['', '', '', '', '', '99/99/9999', '99/99/9999', '', '', '', '', '', '', ''],
									['', '', '', '', '', dTFormat, dTFormat, '', '', '', '', '###,###,##0.00', '###,###,##0.00', '']);

    alignColsInGrid(fgPesquisa, [1, 11, 12]);

    fgPesquisa.AutoSizeMode = 0;
    fgPesquisa.AutoSize(0, fgPesquisa.Cols - 1);

    fgPesquisa.FrozenCols = 1;

    if (fgPesquisa.Rows > 1) {
        fgPesquisa.Row = 1;
        fgPesquisa.Col = getColIndexByColKey(fgPesquisa, 'Documento');
    }

    fgPesquisa.Redraw = 2;

    fgPesquisa.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fgPesquisa.Rows > 1) {
        window.focus();
        fgPesquisa.focus();
    }
    else {
        ;
    }

    // Merge de Colunas
    fgPesquisa.MergeCells = 4;
    fgPesquisa.MergeCol(-1) = true;

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Pesquisa Documento no servidor
********************************************************************/
function pesquisaDocumento(sDocumento) {
    if (sDocumento == '')
        fgValores.TextMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'Emitente')) = '';
    else {

        lockControlsInModalWin(true);

        // parametrizacao do dso dsoPesquisa
        setConnection(dsoPesquisa);

        dsoPesquisa.SQL = 'SELECT TOP 1 Fantasia AS Emitente ' +
							 'FROM Pessoas WITH(NOLOCK) ' +
							 'WHERE (EstadoID = 2 AND ' +
								'dbo.fn_Pessoa_Documento(PessoaID, NULL, NULL, 101, 111, 0) = ' + '\'' + sDocumento + '\'' + ') ' +
							 'ORDER BY Emitente ';

        dsoPesquisa.ondatasetcomplete = pesquisaDocumento_DSC;
        dsoPesquisa.Refresh();
    }
}

/********************************************************************
Retorno do servidor 
********************************************************************/
function pesquisaDocumento_DSC() {
    var sEmitente = '';

    if (!(dsoPesquisa.recordset.BOF && dsoPesquisa.recordset.EOF)) {
        dsoPesquisa.recordset.MoveFirst();
        sEmitente = dsoPesquisa.recordset['Emitente'].value;
    }

    fgValores.TextMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'Emitente')) = sEmitente;

    lockControlsInModalWin(false);
}

/********************************************************************
Salva as linhas alteradas no grid
nTipoChamada: 1-btnIncluir (Modo Simples)
2-btnAssociar
3-btnDissociar
4-btnIncluir (Modo Multiplo)
********************************************************************/
function saveDataInGrid(nTipoChamada) {
    glb_nTipoChamada = nTipoChamada;

    var strPars = new String();
    var i = 0;
    var bFirst = true;

    lockControlsInModalWin(true);

    var nValorID = 0;
    var nFinanceiroID = 0;
    var nFormaPagamentoID = 0;
    var nRelPesContaID = 0;
    var sDocumento = '';
    var sEmitente = '';
    var sdtEmissao = '';
    var sBancoAgencia = '';
    var sConta = '';
    var sNumeroDocumento = '';
    var nValor = 0;
    var nValorEncargos = 0;
    var nValorAplicar = 0;
    var sObservacao = '';
    var nIncluir = 0;
    var nSaldoFinanceiro = 0;
    var sMensagem = '';
    var nBytesAcum = 0;
    var bGeraValorLoca = false;

    if (!glb_bMultiplo)
        nFormaPagamentoID = getCellValueByColKey(fgValores, 'FormaPagamentoID', fgValores.Row);
    else
        nFormaPagamentoID = selFormaPagamentoID.value;

    if ((nFormaPagamentoID == '') || (nFormaPagamentoID <= 0))
        sMensagem += 'Definir forma de pagamento.' + String.fromCharCode(13, 10);


    if (nTipoChamada == 1) {
        nValorID = getCellValueByColKey(fgValores, 'ValorID*', fgValores.Row);

        if (nValorID == '')
            nValorID = 0;

        nFinanceiroID = getCellValueByColKey(fgValores, 'FinanceiroID*', fgValores.Row);

        if (glb_nTipoValorID == 1001) {
            // Cheque ou CCC
            if ((nFormaPagamentoID == 1032) || (nFormaPagamentoID == 1033)) {
                nRelPesContaID = getCellValueByColKey(fgValores, 'RelPesContaID', fgValores.Row);

                if ((nRelPesContaID == '') || (nRelPesContaID <= 0))
                    sMensagem += 'Definir Conta.' + String.fromCharCode(13, 10);
            }
        }
        else {
            sDocumento = trimStr(getCellValueByColKey(fgValores, 'Documento', fgValores.Row));
            sEmitente = trimStr(getCellValueByColKey(fgValores, 'Emitente', fgValores.Row));
            sdtEmissao = dateFormatToSearch(trimStr(getCellValueByColKey(fgValores, 'dtEmissao', fgValores.Row)));
            sBancoAgencia = trimStr(getCellValueByColKey(fgValores, 'BancoAgencia', fgValores.Row));
            sConta = trimStr(getCellValueByColKey(fgValores, 'Conta', fgValores.Row));
            sNumeroDocumento = trimStr(getCellValueByColKey(fgValores, 'NumeroDocumento', fgValores.Row));

            // Dinheiro
            if (nFormaPagamentoID == 1031) {
                if (sDocumento != '')
                    sMensagem += 'Limpar Documento.' + String.fromCharCode(13, 10);
                if (sEmitente != '')
                    sMensagem += 'Limpar Emitente.' + String.fromCharCode(13, 10);
                if (sBancoAgencia != '')
                    sMensagem += 'Limpar Banco/Ag�ncia.' + String.fromCharCode(13, 10);
                if (sConta != '')
                    sMensagem += 'Limpar Conta.' + String.fromCharCode(13, 10);
                if (sNumeroDocumento != '')
                    sMensagem += 'Limpar N�mero do Documento.' + String.fromCharCode(13, 10);
            }
                // Cheque
            else if (nFormaPagamentoID == 1032) {
                if ((sDocumento == '') && ((glb_nEmpresaID != 7) && (glb_nEmpresaID != 8)))
                    sMensagem += 'Preencher Documento.' + String.fromCharCode(13, 10);
                if (sEmitente == '')
                    sMensagem += 'Preencher Emitente.' + String.fromCharCode(13, 10);
                if (sBancoAgencia == '')
                    sMensagem += 'Preencher Banco/Ag�ncia.' + String.fromCharCode(13, 10);
                if (sConta == '')
                    sMensagem += 'Preencher Conta.' + String.fromCharCode(13, 10);
                if (sNumeroDocumento == '')
                    sMensagem += 'Preencher N�mero do Documento.' + String.fromCharCode(13, 10);
            }
                // Cr�dito
            else if (nFormaPagamentoID == 1033) {
                if (sDocumento != '')
                    sMensagem += 'Limpar Documento.' + String.fromCharCode(13, 10);
                if (sEmitente != '')
                    sMensagem += 'Limpar Emitente.' + String.fromCharCode(13, 10);
                if (sNumeroDocumento != '')
                    sMensagem += 'Limpar N�mero do Documento.' + String.fromCharCode(13, 10);
            }
        }

        nValor = parseFloat(fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'VLValor')));
        //A Elaine Souza comentou a respeito da cria��o de VL em financeiros com reten��es. FRG - 11/02/2016.
        //nValorAplicar = parseFloat(fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')));
        nValorAplicar = parseFloat(fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'GerarValor*')));

        if ((nValorAplicar > nValor) || (nValor == 0) || (nValorAplicar == 0))
            sMensagem += 'Valores inv�lidos.' + String.fromCharCode(13, 10);

        sObservacao = trimStr(getCellValueByColKey(fgValores, 'Observacao', fgValores.Row));
        nIncluir = 1;
    }
    else if (nTipoChamada == 2) {
        nValorID = getCellValueByColKey(fgPesquisa, 'ValorID', fgPesquisa.Row);

        if (nValorID == '')
            nValorID = 0;

        nFinanceiroID = getCellValueByColKey(fgValores, 'FinanceiroID*', fgValores.Row);
        nValor = fgPesquisa.ValueMatrix(fgPesquisa.Row, getColIndexByColKey(fgPesquisa, 'Valor'));

        nValorAplicar = fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface'));
        nSaldoFinanceiro = fgPesquisa.ValueMatrix(fgPesquisa.Row, getColIndexByColKey(fgPesquisa, 'SaldoFinanceiro'));

        if (nValorAplicar > nSaldoFinanceiro)
            nValorAplicar = nSaldoFinanceiro;

        nIncluir = 1;
    }
    else if (nTipoChamada == 3) {
        nValorID = getCellValueByColKey(fgValores, 'ValorID*', fgValores.Row);

        if (nValorID == '')
            nValorID = 0;

        nFinanceiroID = getCellValueByColKey(fgValores, 'FinanceiroID*', fgValores.Row);
        nIncluir = 0;
    }

    nValorEncargos = fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'ValorEncargos'));

    if (sMensagem != '') {
        lockControlsInModalWin(false);

        if (window.top.overflyGen.Alert(sMensagem) == 0)
            return null;

        return null;
    }

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    if (nTipoChamada != 4) {
        strPars += '?nUserID=' + escape(glb_nUserID);
        strPars += '&nValorID=' + escape(nValorID);
        strPars += '&nFormaPagamentoID=' + escape(nFormaPagamentoID);
        strPars += '&sDocumento=' + escape(sDocumento);
        strPars += '&sEmitente=' + escape(sEmitente);
        strPars += '&sdtEmissao=' + escape(sdtEmissao);
        strPars += '&sBancoAgencia=' + escape(sBancoAgencia);
        strPars += '&sConta=' + escape(sConta);
        strPars += '&sNumeroDocumento=' + escape(sNumeroDocumento);

        strPars += '&nRelPesContaID=' + escape(nRelPesContaID);
        strPars += '&nFinanceiroID=' + escape(nFinanceiroID);

        if (glb_nPedidoID > 0)
            strPars += '&nValor=' + escape(nValor);
        else
            strPars += '&nValor=' + escape(nValorAplicar);

        strPars += '&nValorEncargos=' + escape(nValorEncargos);
        strPars += '&nValorAplicar=' + escape(nValorAplicar);
        strPars += '&sObservacao=' + escape(sObservacao);
        strPars += '&nIncluir=' + escape(nIncluir);
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
    }
    else {
        for (i = 2; i < fgValores.Rows; i++) {
            if (fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'OK')) == 0)
                continue;

            nBytesAcum = strPars.length;
            bGeraValorLoca = true;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                    strPars = '';
                }

                strPars += '?nUserID=' + escape(glb_nUserID);

                // Primeira vez
                if (glb_aSendDataToServer.length == 0)
                    strPars += '&nValorID=' + escape(nValorID);

                strPars += '&nFormaPagamentoID=' + escape(nFormaPagamentoID);
                strPars += '&sDocumento=' + escape(sDocumento);
                strPars += '&sEmitente=' + escape(sEmitente);
                strPars += '&sdtEmissao=' + escape(sdtEmissao);
                strPars += '&sBancoAgencia=' + escape(sBancoAgencia);
                strPars += '&sConta=' + escape(sConta);
                strPars += '&sNumeroDocumento=' + escape(sNumeroDocumento);
                strPars += '&nFormaPagamentoID=' + escape(nFormaPagamentoID);
                strPars += '&nRelPesContaID=' + escape(selRelPesContaID.value);
                strPars += '&nIncluir=' + escape(1);
                bFirst = true;
            }

            strPars += '&nFinanceiroID=' + escape(fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'FinanceiroID*')));

            //Caso seja do from de Pedidos
            if (glb_nPedidoID > 0) {
                if (bFirst) {
                    strPars += '&nValor=' + escape(fgValores.ValueMatrix(1, getColIndexByColKey(fgValores, 'VLValor')));
                    bFirst = false;
                }
                else
                    strPars += '&nValor=' + escape(fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'VLValor')));
            }
                //Caso seja do form de Financeiro
            else {
                if (bFirst) {
                    strPars += '&nValor=' + escape(fgValores.ValueMatrix(1, getColIndexByColKey(fgValores, 'GerarValor*')));
                    bFirst = false;
                }
                else
                    strPars += '&nValor=' + escape(fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'GerarValor*')));
            }

            strPars += '&nValorEncargos=' + escape(nValorEncargos);
            strPars += '&nValorAplicar=' + escape(fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'GerarValor*')));
            strPars += '&sObservacao=' + escape(fgValores.TextMatrix(i, getColIndexByColKey(fgValores, 'Observacao')));
        }

        if (bGeraValorLoca)
            glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
    }

    glb_nPointToaSendDataToServer = 0;
    sendDataToServer();
}

function verificaoConta(nFormaPagamentoID) {
    var nRelPesContaID;

    // Cheque ou CCC
    if ((nFormaPagamentoID == 1032) || (nFormaPagamentoID == 1033)) {
        nRelPesContaID = getCellValueByColKey(fgValores, 'RelPesContaID', fgValores.Row);

        if ((nRelPesContaID == '') || (nRelPesContaID <= 0))
            return false;
    }

    return true;
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            // Apartir da segunda vez
            if (glb_nPointToaSendDataToServer > 0) {
                if (!((dsoGrava.recordset.BOF) && (dsoGrava.recordset.EOF))) {
                    if ((dsoGrava.recordset['Resultado'].value != null) && (trimStr(dsoGrava.recordset['Resultado'].value) != '')) {
                        if (window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0)
                            return null;

                        lockControlsInModalWin(false);
                        glb_VLTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
                        return null;
                    }

                    if ((dsoGrava.recordset['Valor2ID'].value != null) && (dsoGrava.recordset['Valor2ID'].value != 0)) {
                        glb_aSendDataToServer[glb_nPointToaSendDataToServer] += '&nValorID=' + escape(dsoGrava.recordset['Valor2ID'].value);
                        glb_nValorID = dsoGrava.recordset['Valor2ID'].value;
                    }
                }
            }

            dsoGrava.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/sincronizavalorlocalizar.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else
            saveDataInGrid_DSC();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_VLTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}
/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    lockControlsInModalWin(false);

    var bResultado = false;

    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        dsoGrava.recordset.MoveFirst();
        if ((dsoGrava.recordset['Resultado'].value != null) &&
			 (dsoGrava.recordset['Resultado'].value != '') &&
            (dsoGrava.recordset['Resultado'].value != '\n')) {
            bResultado = true;

            if (window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0)
                return null;
        }

        if ((dsoGrava.recordset['Valor2ID'].value != null) && (dsoGrava.recordset['Valor2ID'].value != 0))
            glb_nValorID = dsoGrava.recordset['Valor2ID'].value;
    }

    if (!bResultado)
        incluiRetencoes(glb_nTipoChamada);
    else
        glb_VLTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

function fillComboPessoa() {
    if (glb_VLTimerInt != null) {
        window.clearInterval(glb_VLTimerInt);
        glb_VLTimerInt = null;
    }

    var sDataInicio = trimStr(txtDtInicio.value);
    var sDataFim = trimStr(txtDtFim.value);
    var sFiltro = '';
    var sJoinPessoas = '';

    if (chkPendente.checked)
        sFiltro += ' AND dbo.fn_Financeiro_SaldoPagamento(a.FinanceiroID) > 0 ';
    //sFiltro += ' AND dbo.fn_ValorLocalizar_Posicao(a.FinanceiroID, 5) > 0 ';

    lockControlsInModalWin(true);

    if (sDataInicio != '') {
        sDataInicio = normalizeDate_DateTime(sDataInicio, 1);

        sDataInicio = '\'' + sDataInicio + '\'';
    }
    else
        sDataInicio = 'dbo.fn_Data_Zero(GETDATE())';

    if (sDataFim != '') {
        sDataFim = normalizeDate_DateTime(sDataFim, 1);

        sDataFim = '\'' + sDataFim + '\'';
    }
    else
        sDataFim = 'dbo.fn_Data_Zero(GETDATE())';

    if (chkParceiro.checked)
        sJoinPessoas += ' a.ParceiroID = b.PessoaID ';
    else
        sJoinPessoas += ' a.PessoaID = b.PessoaID ';

    if (selHistoricoPadraoID.value != 0)
        sFiltro += ' AND a.HistoricoPadraoID = ' + selHistoricoPadraoID.value + ' ';
    else
        sFiltro += ' AND a.HistoricoPadraoID IS NULL ';

    clearComboEx(['selPessoaID2']);

    setConnection(dsoComboPessoa);

    dsoComboPessoa.SQL =
	            'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
				 'UNION ALL ' +
				'SELECT DISTINCT ' +
				    'b.PessoaID AS fldID, ' +
				    'b.Fantasia AS fldName ' +
				'FROM Financeiro a WITH(NOLOCK) ' +
				    'INNER JOIN Pessoas b WITH ( NOLOCK ) ON (' + sJoinPessoas + ') ' + ' ' +
				'WHERE(a.EmpresaID = ' + glb_nEmpresaID + ' ' +
				    'AND a.EstadoID NOT IN (5,48,49) ' +
				    'AND a.TipoFinanceiroID = ' + glb_nTipoValorID + ' ' +
				    'AND a.dtVencimento BETWEEN ' + sDataInicio + ' AND ' + sDataFim +
				     sFiltro + ') ';

    dsoComboPessoa.ondatasetcomplete = fillComboPessoa_DSC;
    dsoComboPessoa.Refresh();

}

function fillComboPessoa_DSC() {
    while (!dsoComboPessoa.recordset.EOF) {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoComboPessoa.recordset['fldName'].value;
        oOption.value = dsoComboPessoa.recordset['fldID'].value;

        selPessoaID2.add(oOption);
        dsoComboPessoa.recordset.MoveNext();
    }

    adjustLabelsCombos();
    lockControlsInModalWin(false);

    if (selPessoaID2.options.length == 0)
        selPessoaID2.disabled = true;
    else
        selPessoaID2.disabled = false;

}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtData_onKeyPress() {
    if (event.keyCode == 13)
        btn_onclick(btnRefresh);
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtFields_onKeyDown() {
    if (event.keyCode == 13)
        btn_onclick(btnListar);
}

function adjustLabelsCombos() {
    setLabelOfControl(lblPessoaID, (selPessoaID.value == 0 ? '' : selPessoaID.value));
    setLabelOfControl(lblHistoricoPadraoID, (selHistoricoPadraoID.value == 0 ? '' : selHistoricoPadraoID.value));
    setLabelOfControl(lblPessoaID2, (selPessoaID2.value == 0 ? '' : selPessoaID2.value));
    setLabelOfControl(lblFormaPagamentoID, (selFormaPagamentoID.value == 0 ? '' : selFormaPagamentoID.value));
    setLabelOfControl(lblRelPesContaID, (selRelPesContaID.value == 0 ? '' : selRelPesContaID.value));
}

function selPessoaID_onchange() {
    var sShow = 'inherit';

    if (parseInt(selPessoaID.value, 10) != 0)
        sShow = 'hidden';

    lblFantasia.style.visibility = sShow;
    txtFantasia.style.visibility = sShow;

    adjustLabelsCombos();
}

function selHistoricoPadraoID_onchange() {
    adjustLabelsCombos();
}

function selPessoaID2_onchange() {
    adjustLabelsCombos();
}

function selFormaPagamentoID_onchange() {
    var sShow = 'inherit';

    if (parseInt(selFormaPagamentoID.value, 10) == 1031)
        sShow = 'hidden';

    lblRelPesContaID.style.visibility = sShow;
    selRelPesContaID.style.visibility = sShow;

    adjustLabelsCombos();
}

function selRelPesContaID_onchange() {
    adjustLabelsCombos();
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly(grid) {
    paintReadOnlyCols(grid);
}

function verificaData() {
    var sDataInicio = trimStr(txtDtInicio.value);
    var sDataFim = trimStr(txtDtFim.value);
    var bDataIsValid = true;

    if (sDataInicio != '')
        bDataInicioIsValid = chkDataEx(txtDtInicio.value);

    if (sDataFim != '')
        bDataFimIsValid = chkDataEx(txtDtFim.value);

    if (!bDataInicioIsValid && !bDataFimIsValid) {
        if (window.top.overflyGen.Alert('Datas inv�lidas.') == 0)
            return null;

        return false;
    }

    if (!bDataInicioIsValid) {
        if (window.top.overflyGen.Alert('Data inicio inv�lida.') == 0)
            return null;

        window.focus();

        if (sDataInicio != '')
            txtDtInicio.focus();

        return false;
    }

    if (!bDataFimIsValid) {
        if (window.top.overflyGen.Alert('Data fim inv�lida.') == 0)
            return null;

        window.focus();

        if (sDataFim != '')
            txtDtFim.focus();

        return false;
    }

    return true;
}

function chkGrid_onclick() {
    var nHeight = (150 - 30);
    var sVisibility = '';

    if (chkGrid.checked) {
        sVisibility = 'inherit';
    }
    else {
        sVisibility = 'hidden';
        nHeight += parseInt(divGridPesquisa.offsetHeight, 10);
    }

    divGridPesquisa.style.visibility = sVisibility;
    divGridValores.style.height = nHeight;
    fgValores.style.height = nHeight;

    if ((chkGrid.checked) && (fgValores.Row > 0))
        fgValores.TopRow = fgValores.Row;
}
function chkPendente_onclick() {
    return null;
}

function chkPedido_onclick() {

    // zera o grid
    fgValores.Rows = 1;

    if (chkPedido.checked) {
        lblServico.style.visibility = 'inherit';
        chkServico.style.visibility = 'inherit';
    }
    else {
        lblServico.style.visibility = 'hidden';
        chkServico.style.visibility = 'hidden';
    }

    return null;
}

function chkParceiro_onclick() {
    if (chkParceiro.checked)
        lblPessoaID2.innerText = 'Parceiro';
    else
        lblPessoaID2.innerText = 'Pessoa';

    adjustLabelsCombos();
}

function chkModo_onclick() {
    var sShow = 'hidden';
    glb_bMultiplo = false;

    if (chkModo.checked) {
        sShow = 'inherit';
        glb_bMultiplo = true;
    }

    lblFormaPagamentoID.style.visibility = sShow;
    selFormaPagamentoID.style.visibility = sShow;
    lblRelPesContaID.style.visibility = sShow;
    selRelPesContaID.style.visibility = sShow;

    btn_onclick(btnRefresh);

    if (glb_bMultiplo)
        recalcularRetencoes(false);
}

function chkServico_onclick() {

    // zera o grid
    fgValores.Rows = 1;

    return null;
}

/********************************************************************
Calcula Totais
********************************************************************/
function calculaTotais() {
    var i = 0;
    var nCounter = 0;
    var nValorParcela = 0;
    var nVLValor = 0;
    var nVLValorAplicar = 0;
    var nGerarValor = 0;

    for (i = 2; i < fgValores.Rows; i++) {
        if (fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'OK')) != 0) {
            nCounter++;
            nValorParcela += fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'ValorParcela*'));
            nVLValor += fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'VLValor'));
            nVLValorAplicar += fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface'));
            nGerarValor += fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'GerarValor*'));
        }
    }

    if (fgValores.Rows > 1) {
        fgValores.TextMatrix(1, getColIndexByColKey(fgValores, 'FinanceiroID*')) = nCounter;
        fgValores.TextMatrix(1, getColIndexByColKey(fgValores, 'ValorParcela*')) = nValorParcela;
        fgValores.TextMatrix(1, getColIndexByColKey(fgValores, 'VLValor')) = nVLValor;
        fgValores.TextMatrix(1, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')) = nVLValorAplicar;
        fgValores.TextMatrix(1, getColIndexByColKey(fgValores, 'GerarValor*')) = nGerarValor;
    }
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function fg_ModGerValorLocBeforeSort(grid, col) {
    glb_LASTLINESELID = '';

    if (grid.Row > 0) {
        if (grid == fgValores)
            glb_LASTLINESELID = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'FinanceiroID*'));
        else if (grid == fgPesquisa)
            glb_LASTLINESELID = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'ValorID'));
    }
    else
        glb_LASTLINESELID = '';
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function fg_ModGerValorLocAfterSort(grid, col) {
    var i, keyToSelectRow, bBlockEvents;

    keyToSelectRow = '';

    if ((glb_LASTLINESELID != '') && (grid.Rows > 1)) {
        for (i = 1; i < grid.Rows; i++) {
            if (grid == fgValores)
                keyToSelectRow = grid.TextMatrix(i, getColIndexByColKey(grid, 'FinanceiroID*'));
            else if (grid == fgPesquisa)
                keyToSelectRow = grid.TextMatrix(i, getColIndexByColKey(grid, 'ValorID'));

            if (keyToSelectRow == glb_LASTLINESELID) {
                if (grid == fgValores) {
                    bBlockEvents = glb_gridsEvents.bBlockEvents_fgValores;
                    glb_gridsEvents.bBlockEvents_fgValores = true;
                }
                else if (grid == fgPesquisa) {
                    bBlockEvents = glb_gridsEvents.bBlockEvents_fgPesquisa;
                    glb_gridsEvents.bBlockEvents_fgPesquisa = true;
                }

                grid.TopRow = i;
                grid.Row = i;

                if (grid == fgValores)
                    glb_gridsEvents.bBlockEvents_fgValores = bBlockEvents;
                else if (grid == fgPesquisa)
                    glb_gridsEvents.bBlockEvents_fgPesquisa = bBlockEvents;

                break;
            }
        }
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fgValores_DblClick(grid, Row, Col) {
    var i;
    var bFill = true;
    var nFirstRow = (glb_bMultiplo ? 2 : 1);

    if ((Col != getColIndexByColKey(grid, 'OK')) && (!chkGrid.checked))
        return null;

    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    glb_PassedOneInDblClick = true;

    if (Col == getColIndexByColKey(grid, 'OK')) {
        // limpa coluna se tem um check box checado
        for (i = nFirstRow; i < grid.Rows; i++) {
            if (grid.ValueMatrix(i, Col) != 0) {
                bFill = false;
                break;
            }
        }

        for (i = nFirstRow; i < grid.Rows; i++) {
            if (bFill)
                grid.TextMatrix(i, Col) = 1;
            else
                grid.TextMatrix(i, Col) = 0;
        }

        recalcularRetencoes(false);
        calculaTotais();

        setupBtnsFromGridState();
    }
    else if (grid.Row > 0)
        btn_onclick(btnListar);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fgValores_KeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fgValores_ValidateEdit(grid, Row, Col) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fgValores_modalgerarvalorlocalizarBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fgValores_BeforeEdit(grid, row, col) {
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if (fldDet == null)   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ((fldDet[0] == 200) || (fldDet[0] == 129))  // varchar ou char
    {
        grid.EditMaxLength = fldDet[1];      
    }
    else
        grid.EditMaxLength = 0;

}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fgValores_AfterEdit(Row, Col) {
    var nValorID, nFormaPagamentoID, nFormaPagamentoVLID;
    var sDocumento = '';
    var nVLValor;
    var nSaldoFinanceiro = 0;
    var nVLValorFinanceiro = 0;
    var nPodeIncluir = 0;
    var nFinanceiroID = 0;
    var nRetencoesGerais = 0;
    var nGerarValor = 0;

    if (Col == getColIndexByColKey(fgValores, 'FormaPagamentoID')) {
        nValorID = fgValores.ValueMatrix(Row, getColIndexByColKey(fgValores, 'ValorID*'));
        nFormaPagamentoID = fgValores.ValueMatrix(Row, getColIndexByColKey(fgValores, 'FormaPagamentoID'));
        nFormaPagamentoVLID = fgValores.ValueMatrix(Row, getColIndexByColKey(fgValores, 'FormaPagamentoVLID'));

        if ((nValorID != '') && (nFormaPagamentoVLID != nFormaPagamentoID)) {
            fgValores.TextMatrix(Row, getColIndexByColKey(fgValores, 'FormaPagamentoID')) = nFormaPagamentoVLID;

            if (window.top.overflyGen.Alert('Financeiro com ValorID j� associado.') == 0)
                return null;

            return null;
        }

        nPodeIncluir = 0;

        if (!(dsoCombo1.recordset.BOF && dsoCombo1.recordset.EOF)) {
            dsoCombo1.recordset.MoveFirst();
            while (!dsoCombo1.recordset.EOF) {
                if (dsoCombo1.recordset['ItemID'].value == fgValores.ValueMatrix(Row, Col)) {
                    nPodeIncluir = dsoCombo1.recordset['Incluir'].value;
                    break;
                }

                dsoCombo1.recordset.moveNext();
            }
        }

        fgValores.TextMatrix(Row, getColIndexByColKey(fgValores, 'PodeIncluir')) = nPodeIncluir;
    }

    if (Col == getColIndexByColKey(fgValores, 'Documento')) {
        sDocumento = trimStr(getCellValueByColKey(fgValores, 'Documento', fgValores.Row));
        pesquisaDocumento(sDocumento);
    }
    else if ((Col == getColIndexByColKey(fgValores, 'VLValor')) || (Col == getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface'))) {
        fgValores.TextMatrix(Row, Col) = treatNumericCell(fgValores.TextMatrix(Row, Col));

        if (fgValores.TextMatrix(Row, Col) == '' || (isNaN(fgValores.ValueMatrix(Row, Col))))
            fgValores.TextMatrix(Row, Col) = '0.00';

        nVLValor = parseFloat(fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'VLValor')));
        nSaldoFinanceiro = parseFloat(fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'VLFinanceiroValor')));
        nRetencoesGerais = parseFloat(fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'RetencoesGerais*')));

        if (Col == getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')) {
            nVLValorFinanceiro = parseFloat(fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')));
           

            if (nVLValorFinanceiro < nVLValor)
                nVLValor = nVLValorFinanceiro;
        }

        if ((nVLValor - nRetencoesGerais) > nSaldoFinanceiro)
            nVLValor = nSaldoFinanceiro;


        fgValores.TextMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')) = nVLValor;
        
        //Atualiza��o do campo "Valor Aplicado" na interface. - FRG 11/02/2016.
        /*
        fgValores.TextMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'Retencoes*')) = parseFloat(fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')) * (4.65 / 100));

        fgValores.TextMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'PCC*')) = fgValores.TextMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'Retencoes*'));

        nGerarValor = (parseFloat(fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface'))) -
                      (parseFloat(fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'RetencoesGerais*'))) + parseFloat(fgValores.TextMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'Retencoes*')))));

        fgValores.TextMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'GerarValor*')) = nGerarValor;
        */

        nGerarValor = parseFloat(fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')));

        fgValores.TextMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'GerarValor*')) = nGerarValor;
        
        financeirosDesconsiderar();

        recalcularRetencoes(true);
    }
    else if (Col == getColIndexByColKey(fgValores, 'ValorEncargos'))
    {
        fgValores.TextMatrix(Row, Col) = treatNumericCell(fgValores.TextMatrix(Row, Col));
    }

    //Alterado para calcular reten��es dos financeiros.adicionado fun��o: recalcularRetencoes.
    if ((glb_bMultiplo) && ((Col == getColIndexByColKey(fgValores, 'OK')) ||
		 (Col == getColIndexByColKey(fgValores, 'VLValor')) ||
		 (Col == getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')))) {
        recalcularRetencoes(false);
        calculaTotais();
    }

    /*
    //alterado para calcular reten��es dos financeiros
    if ((Col == getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')) || (Col == getColIndexByColKey(fgValores, 'OK')))
    {
    recalcularRetencoes(false);
    calculaTotais();
    }
    */

    setupBtnsFromGridState();
}

function js_fgValores_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    if (glb_GridIsBuilding)
        return true;
}

/********************************************************************
Atualiza linha de mensagens abaixo do grid.

Parametros:
grid
OldRow
OldCol
NewRow
NewCol
********************************************************************/
function js_fgValores_AfterRowColChange(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return true;

    if ((glb_bMultiplo) && (fgValores.Rows > 2) && (NewRow == 1)) {
        fgValores.Row = 2;
    }

    if (fgPesquisa.Rows > 1)
        fgPesquisa.Rows = 1;

    setupBtnsFromGridState();
}

/********************************************************************

********************************************************************/
function js_fgPesquisa_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    if (glb_GridIsBuilding)
        return true;
}

function js_fgPesquisa_AfterRowColChange(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return true;

    setupBtnsFromGridState();
}

function js_fgPesquisa_DblClick(grid, Row, Col) {
    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    glb_PassedOneInDblClick = true;

    if (grid.Row > 0)
        btn_onclick(btnAssociar);
}

// FINAL DE EVENTOS DE GRID *****************************************

function financeirosDesconsiderar(bGrid) {

    var sFinanceiros = '';

    glb_sFinanceiros = 'NULL';

    if ((!glb_bMultiplo) && (!bGrid)) {
        glb_sFinanceiros = '[' + fgValores.TextMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'FinanceiroID*')) + ', ' +
            replaceStr(fgValores.TextMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')).toString(), ',', '.') + ']';

        sFinanceiros = fgValores.TextMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'FinanceiroID*'));
    }

    var linhaInicial;

    if (glb_bMultiplo)
        linhaInicial = 2;
    else
        linhaInicial = 1;

    for (i = linhaInicial; i < fgValores.Rows; i++) {
        if ((!glb_bMultiplo) && (!bGrid))
            break;

        if (fgValores.TextMatrix(i, getColIndexByColKey(fgValores, 'FinanceiroID*')) != 0) {

           if ((glb_bMultiplo) || (bGrid)) {
                if (glb_sFinanceiros != 'NULL')
                    glb_sFinanceiros += '[' + fgValores.TextMatrix(i, getColIndexByColKey(fgValores, 'FinanceiroID*')) + ', ' +
                        replaceStr(fgValores.TextMatrix(i, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')).toString(), ',', '.') + ']';
                else
                    glb_sFinanceiros = '[' + fgValores.TextMatrix(i, getColIndexByColKey(fgValores, 'FinanceiroID*')) + ', ' +
                        replaceStr(fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')).toString(), ',', '.') + ']';
           }
           else if ((fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'OK')) == 0) && (glb_bMultiplo)) {
               if (glb_sFinanceiros != 'NULL')
                   glb_sFinanceiros += '[' + fgValores.TextMatrix(i, getColIndexByColKey(fgValores, 'FinanceiroID*')) + ', 0]';
               else
                   glb_sFinanceiros = '[' + fgValores.TextMatrix(i, getColIndexByColKey(fgValores, 'FinanceiroID*')) + ', 0]';
           }

            if (sFinanceiros == '')
                sFinanceiros = fgValores.TextMatrix(i, getColIndexByColKey(fgValores, 'FinanceiroID*'));
            else
                sFinanceiros += ', ' + fgValores.TextMatrix(i, getColIndexByColKey(fgValores, 'FinanceiroID*'));
        }
    }

    if (glb_sFinanceiros != 'NULL')
        glb_sFinanceiros = '\'' + glb_sFinanceiros + '\'';


    if (sFinanceiros == '')
        sFinanceiros = 0;

    return sFinanceiros;
}

function temRetencao() {
    glb_bRetencao = false;

    var nInicio = ((glb_bMultiplo) ? 2 : 1);

    for (i = nInicio; i < fgValores.Rows; i++) {
        if (fgValores.TextMatrix(i, getColIndexByColKey(fgValores, 'Retencoes*')) > 0) {
            glb_bRetencao = true;
            break;
        }
    }
}

function recalcularRetencoes(bGrid) {
    if (glb_nTipoValorID == 1002) {
        atualizaValor();
        temRetencao();
        return null;
    }
    if (bGrid) {
        var nFinanceiroID = fgValores.TextMatrix((fgValores.Rows - 1), getColIndexByColKey(fgValores, 'FinanceiroID*'));
        var strSQL = '';
        var sFinanceiros = financeirosDesconsiderar(bGrid);

        strSQL = 'SELECT a.FinanceiroID, ISNULL(dbo.fn_Financeiro_ValoresRetencoes(a.FinanceiroID, NULL, NULL, NULL, NULL, NULL, NULL, 3), 0) + ' +
                            ' ISNULL(dbo.fn_Financeiro_ValoresRetencoes(a.FinanceiroID, NULL, 18, 1, ' + glb_sFinanceiros + ', NULL, a.FinanceiroID, 2), 0) AS Retencao, ' +
                        'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(a.FinanceiroID, NULL, 18, 1, ' + glb_sFinanceiros + ', NULL, a.FinanceiroID, 2), 0) AS PCC, ' +
                        'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(a.FinanceiroID, NULL, NULL, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesGerais, ' +
                        'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(a.FinanceiroID, NULL, 18, NULL, NULL, NULL, NULL, 3), 0) AS RetencoesPCC ' +
                    'FROM Financeiro a WITH(NOLOCK) ' +
                    'WHERE a.FinanceiroID IN (' + sFinanceiros + ')';

        /*
        strSQL = 'SELECT a.FinanceiroID, ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroID + ', NULL, NULL, 1, ' + glb_sFinanceiros + ', NULL, a.FinanceiroID, 2), 0) AS Retencao, ' +
        'ISNULL(dbo.fn_Financeiro_ValoresRetencoes(' + nFinanceiroID + ', NULL, 18, 1, ' + glb_sFinanceiros + ', NULL, a.FinanceiroID, 2), 0) AS PCC ' +
        'FROM Financeiro a WITH(NOLOCK) ' +
        'WHERE a.FinanceiroID IN (' + sFinanceiros + ')';
        */
        setConnection(dsoGridRetencoes);
        dsoGridRetencoes.SQL = strSQL;
        dsoGridRetencoes.ondatasetcomplete = recalcularRetencoes_DSC;
        dsoGridRetencoes.Refresh();
    }
    return null;
}

function recalcularRetencoes_DSC() {
    var nFinanceiroID = 0;
    var nRetencoesPCC = new Array();
    var icount = 0;

    var nInicio = ((glb_bMultiplo) ? 2 : 1);

    for (icount = nInicio; icount < fgValores.Rows; icount++) {
        nFinanceiroID = fgValores.ValueMatrix(icount, getColIndexByColKey(fgValores, 'FinanceiroID*'));

        dsoGridRetencoes.recordset.MoveFirst();
        dsoGridRetencoes.recordset.setFilter('FinanceiroID=' + nFinanceiroID);       

        if ((!dsoGridRetencoes.recordset.EOF) && (!dsoGridRetencoes.recordset.BOF)) {
            if (dsoGridRetencoes.recordset['RetencoesPCC'].value <= 0)
                fgValores.TextMatrix(icount, getColIndexByColKey(fgValores, 'Retencoes*')) = dsoGridRetencoes.recordset['Retencao'].value;
            else
                fgValores.TextMatrix(icount, getColIndexByColKey(fgValores, 'Retencoes*')) = dsoGridRetencoes.recordset['PCC'].value;

            fgValores.TextMatrix(icount, getColIndexByColKey(fgValores, 'PCC*')) = dsoGridRetencoes.recordset['PCC'].value;
            fgValores.TextMatrix(icount, getColIndexByColKey(fgValores, 'RetencoesGerais*')) = dsoGridRetencoes.recordset['RetencoesGerais'].value;

          //  /*
            nGerarValor = (parseFloat(fgValores.ValueMatrix(icount, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface'))) -
                          (parseFloat(fgValores.ValueMatrix(icount, getColIndexByColKey(fgValores, 'RetencoesGerais*'))) + parseFloat(fgValores.TextMatrix(icount, getColIndexByColKey(fgValores, 'PCC*')))));

            fgValores.TextMatrix(icount, getColIndexByColKey(fgValores, 'GerarValor*')) = nGerarValor;
           // */

            dsoGridRetencoes.recordset.MoveNext();
        }

        dsoGridRetencoes.recordset.setFilter('');
    }

    atualizaValor();
    temRetencao();
}

function atualizaValor(nRetencoesPCC) {
    var nInicio = ((glb_bMultiplo) ? 2 : 1);

    for (i = nInicio; i < fgValores.Rows; i++) {
        fgValores.TextMatrix(i, getColIndexByColKey(fgValores, 'GerarValor*')) = (fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface')) -
                                                                                    fgValores.ValueMatrix(i, getColIndexByColKey(fgValores, 'Retencoes*')));
    }
}

function incluiRetencoes(nTipoChamada) {
    //Gera reten��o somente em caso de inclus�o do VL.
    if ((nTipoChamada != 1) && (nTipoChamada != 4)) {
        glb_nCont = 0;
        incluiRetencoes_DSC();

        return null;
    }

    var strPars = new String();
    var nFinanceiroID = 0;
    var nValorBase = 0;
    var nValorRetencao = 0;
    var nPedidoID = 0;
    var nImpostoID = 0;
    var bEhPagamento = 1;
    var nFormaPagamentoID = 0;
    var sMensagem = '';

    lockControlsInModalWin(true);

    glb_nTipoChamada = nTipoChamada;

    if ((glb_bMultiplo) && (glb_Row == 1))
        glb_Row = 2;

    glb_nCont = (fgValores.Rows - glb_Row);

    if (glb_Row < fgValores.Rows) {
        if (!glb_bMultiplo)
            glb_nCont = 0;
        else
            glb_nCont--;

        nFinanceiroID = 0;
        nValorRetencao = 0;

        //Somente linhas selecionadas
        if (nTipoChamada == 1) {
            nFinanceiroID = fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'FinanceiroID*'));

            nValorRetencao = fgValores.ValueMatrix(fgValores.Row, getColIndexByColKey(fgValores, 'PCC*'));
        }
            //Linhas multiplas
        else if (nTipoChamada == 4) {
            if ((fgValores.ValueMatrix(glb_Row, getColIndexByColKey(fgValores, 'OK')) != 0) && (fgValores.ValueMatrix(glb_Row, getColIndexByColKey(fgValores, 'Retencoes*')) > 0)) {
                nFinanceiroID = fgValores.ValueMatrix(glb_Row, getColIndexByColKey(fgValores, 'FinanceiroID*'));

                nValorRetencao = fgValores.ValueMatrix(glb_Row, getColIndexByColKey(fgValores, 'PCC*'));
            }
        }

        if ((nValorRetencao >= 0) && (nFinanceiroID > 0) && (glb_nTipoValorID == 1001)) {

            nValorBase = fgValores.ValueMatrix(glb_Row, getColIndexByColKey(fgValores, 'VLFinanceiroValorInterface*'));
            transformStringInNumeric(nValorBase);

            strPars = '?nFinanceiroID=' + escape(nFinanceiroID);
            strPars += '&nValor=' + escape(nValorBase);
            strPars += '&nValorID=' + escape(glb_nValorID);
            strPars += '&nImpostoID=' + escape(0);
            strPars += '&sDtFaturamento=' + escape(0);
            strPars += '&sFinanceiros=' + escape(replaceStr(glb_sFinanceiros, '\'', ''));


            setConnection(dsoIncluiRetencoes);
            dsoIncluiRetencoes.URL = SYS_ASPURLROOT + '/serversidegen/calcularetencoes.asp' + strPars;
            dsoIncluiRetencoes.ondatasetcomplete = incluiRetencoes_DSC;
            dsoIncluiRetencoes.refresh();
        }
        else
            incluiRetencoes_DSC();

        if (nTipoChamada == 1) {
            glb_Row = 1;
            return null;

        }

    }
}

function incluiRetencoes_DSC() {
    if ((glb_nTipoChamada == 4) && (glb_nCont != 0)) {
        glb_Row += 1;
        incluiRetencoes(glb_nTipoChamada);
    }

    if (glb_nCont == 0) {
        glb_Row = 1;

        // glb_VLTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');

        fillGridData();

        glb_nValorID = 0;

        lockControlsInModalWin(false);
    }
}