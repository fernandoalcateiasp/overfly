/********************************************************************
modalnfe.js

Library javascript para o modalnfe.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var modPesqnfe_Timer = null;
var gblDepositoID = 0;
var dsoNotaFiscal = new CDatatransport('dsoNotaFiscal');
var dsoImpressaoNFe = new CDatatransport('dsoImpressaoNFe');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES

NOTA: Ver tambem o arquivo commonnfe.js

FUNCOES GERAIS:
window_onload()
setupPage()
txtPesquisa_onKeyPress()
txtPesquisa_ondigit(ctl)
btnFindPesquisa_onclick(ctl)
btn_onclick(ctl)
startPesq(strPesquisa)
dataToReturn()

Eventos de grid particulares desta janela:
fg_ModPesqnfeDblClick()
fg_ModPesqnfeKeyPress(KeyAscii)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // ajusta o body do html

    with (modalnfeBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();
    lockControlsInModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Impress�o de DANFEs', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;

    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 50;
    modalFrame.style.left = 0;

    btnOK.style.visibility = 'hidden';
    btnCanc.value = 'Cancelar';
    btnCanc.style.visibility = 'hidden';
    btnImprimir.disabled = false;
    btnRefresh.disabled = false;

    adjustElementsInForm([
        ['lblImpressoras', 'selImpressoras', 20, 1, 0, 20],
        ['lblDepositoID', 'selDepositoID', 16, 1],
        ['lblFiltro', 'txtFiltro', 25, 1],
		['btnImprimir', 'btn', 100, 1],
		['btnRefresh', 'btn', 100, 1]], null, null, true);

    selImpressoras.onchange = selImpressoras_onchange;
    selDepositoID.onchange = selDepositoID_onchange;
    txtFiltro.onkeypress = txtFiltro_onKeyPress;

    with (divFG.style) {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = (ELEM_GAP * 2) - 10;
        //top = (ELEM_GAP * 3) + 10;
        top = ELEM_GAP + parseInt(btnRefresh.style.top, 10) + parseInt(btnRefresh.style.height, 10);
        width = modWidth - 25;
        height = MAX_FRAMEHEIGHT_OLD - 45;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.style.width, 10) * 18;
    fg.Redraw = 2;

    // Alinhamento dos bot�es.
    btnImprimir.style.left = 780;
    btnRefresh.style.left = 888;

    if (selImpressoras.selectedIndex >= 0) {
        selImpressoras_onchange();
        NotaFiscal();
    }
    else
        return null;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqnfeDblClick(Row) {
    ImprimirNFe(Row);
}

/********************************************************************
Impressao do DANFE
********************************************************************/
function ImprimirNFe(Row) {
    var mensagem;

    if ((Row == null) || (Row < 0)) {
        if (fg.Row < 0) {
            window.top.overflyGen.Alert('Selecione pelo menos uma linha.');
            lockControlsInModalWin(false);
            return null;
        }
        else
            Row = fg.Row;
    }

    var sEstadoNF = fg.TextMatrix(Row, getColIndexByColKey(fg, 'EstadoNF'));
    var nTipoEmissaoID = fg.TextMatrix(Row, getColIndexByColKey(fg, 'TipoEmissaoID'));
    var nDocumentoFiscalID = fg.TextMatrix(Row, getColIndexByColKey(fg, 'DocumentoFiscalID'));
    var sURL = fg.TextMatrix(Row, getColIndexByColKey(fg, 'ArquivoDanfePDF'));
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var nNumeroVias = 1;

    //Nota Fiscal de Servico
    if ((nDocumentoFiscalID == 1123)) {
        mensagem = 'Imprimir RPS?';
        var _retMsg = window.top.overflyGen.Confirm(mensagem);
        if (_retMsg == 1) {
            lockControlsInModalWin(true);
            var strPars = new String();
            strPars = '?nNotaFiscalID=' + fg.TextMatrix(Row, getColIndexByColKey(fg, 'NotaFiscalID'));
            strPars += '&nImpressoraID=' + selImpressoras.value;
            strPars += '&nNumeroVias=' + nNumeroVias;
            dsoImpressaoNFe.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/impressaoNFSe.aspx' + strPars;
            dsoImpressaoNFe.ondatasetcomplete = ImprimirNFe_DSC;
            dsoImpressaoNFe.refresh();
        }

    }
        //Nota Fiscal de Servi�o Eletronica
    else if ((nDocumentoFiscalID == 1124) && (aEmpresaData[0] != 7)) {
        if (sURL != null)
            window.open(sURL);
        else
            window.top.overflyGen.Alert('N�o h� nota para ser impressa.');
    }
    else {
        if (sEstadoNF == 'Z') {
            //AjustesRejeicaoNFePendenteS
            window.top.overflyGen.Alert('Nota Fiscal Eletr�nica rejeitada.\n' +
            'Solicitar ao propriet�rio do pedido os ajustes necess�rios.');
        }
        else if ((sEstadoNF == 'A' && nTipoEmissaoID != 893) || (sEstadoNF == 'G' && nTipoEmissaoID == 893) || (sEstadoNF == 'A' && nTipoEmissaoID == 893)) {
            // Verifica se o TipoEmissaoID esta setada como FS-DA (893)
            // Se sim informa o usuario a necessidade de alimentar a impressora
            // com formulario de segurana para impressao do DANFE
            if (fg.TextMatrix(Row, getColIndexByColKey(fg, 'TipoEmissaoID')) == 893) {
                mensagem = 'Tipo de emiss�o FS-DA.\n' +
                'Insira duas folhas de seguran�a na impressora.\n\n' +
                'Imprimir DANFE?';
            }
            else {
                mensagem = 'Imprimir DANFE?';
            }

            var _retMsg = window.top.overflyGen.Confirm(mensagem);
            if (_retMsg == 1) {
                lockControlsInModalWin(true);
                var strPars = new String();
                strPars = '?nNotaFiscalID=' + fg.TextMatrix(Row, getColIndexByColKey(fg, 'NotaFiscalID'));
                strPars += '&bDanfe=1';
                strPars += '&nImpressoraID=' + selImpressoras.value;
                dsoImpressaoNFe.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/impressaoNFe.aspx' + strPars;
                dsoImpressaoNFe.ondatasetcomplete = ImprimirNFe_DSC;
                dsoImpressaoNFe.refresh();
            }
        }
    }
}
/********************************************************************
Retorno da impressao do DANFE
********************************************************************/
function ImprimirNFe_DSC() {
    lockControlsInModalWin(false);
    NotaFiscal();
}

/********************************************************************
Clique botao OK ou Cancela 
********************************************************************/
function btn_onclick(ctl) {
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S', null);
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null);
    }
        // 3. O usuario clicou no botao Imprimir
    else if (ctl.id == 'btnImprimir') {
        ImprimirNFe();
    }
    else if (ctl.id == 'btnRefresh') {
        //window.top.overflyGen.Alert(selImpressoras.value);
        NotaFiscal();
    }

}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqnfeKeyPress(KeyAscii) {
    ;
}

/*******************************************************************
Imprimi notas fiscais
*******************************************************************/
function btnImprimir_onclick(Row) {

}

/*******************************************************************
Select que preenche o grid
*******************************************************************/
function NotaFiscal() {
    var nRegistroID = sendJSMessage("SUP_HTML", JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ValorID'].value");
    var contextoID = getCmbCurrDataInControlBar('sup', 1);

    var sFiltroRejeitado;
    var sFiltroPendenteImp;
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var sFiltroDeposito = '';
    var sFiltro = '';
    var sVinculadoSelect = '';
    var sVinculadoFrom = '';

    if (txtFiltro.value.length > 0)
        sFiltro = txtFiltro.value + ' AND ';

    // Deposito
    if (gblDepositoID != 0)
        sFiltroDeposito = ' AND (b.DepositoID = ' + gblDepositoID + ')';

    setConnection(dsoNotaFiscal);

    // Entrada
    if (contextoID[1] == 5111) {
        sFiltroRejeitado = '((a.TipoNotaFiscalID = 601 AND b.EstadoID = 24) OR (a.TipoNotaFiscalID = 601 AND a.TipoEmissaoID = 893 AND b.AjustesRejeicaoNFePendentes = 1)) ';
        sFiltroPendenteImp = '((a.TipoNotaFiscalID = 601 AND a.EstadoID = 67) OR (a.TipoNotaFiscalID = 601 AND a.TipoEmissaoID = 893)) ';
    }
        // Saida
    else {
        sFiltroRejeitado = '((a.TipoNotaFiscalID = 602 AND b.EstadoID = 26 AND b.AjustesRejeicaoNFePendentes = 1) OR (a.TipoNotaFiscalID = 602 AND a.TipoEmissaoID = 893 AND b.AjustesRejeicaoNFePendentes = 1)) ';
        sFiltroPendenteImp = '((a.TipoNotaFiscalID = 602 AND b.EstadoID = 29 AND a.EstadoID = 67) OR (a.TipoNotaFiscalID = 602 AND a.TipoEmissaoID = 893)) ';
    }

    if (aEmpresaData[0] != 7) {
        sVinculadoSelect = 'e.VinculadoID, dbo.fn_Pedido_Cor(b.PedidoID, NULL, NULL, 11) AS CorVinculado, ';
        sVinculadoFrom = 'LEFT JOIN Pedidos_Vinculados e WITH(NOLOCK) ON (e.PedidoID = b.PedidoID) ';
    }

    dsoNotaFiscal.SQL = 'SELECT dbo.fn_TipoAuxiliar_Item(a.DocumentoFiscalID, 2) AS DocumentoFiscal, ' + ((sVinculadoSelect != '') ? sVinculadoSelect : ' ') + 'NULL AS NotaFiscal, ' +
                                'c.RecursoAbreviado AS EstadoNF, a.PedidoID, a.TransacaoID, d.RecursoAbreviado AS EstadoPedido, dbo.fn_Pessoa_Fantasia(b.PessoaID, 0) AS Pessoa, ' +
                                'CONVERT(VARCHAR(10), a.dtNotaFiscal, 103) AS DataNota, dbo.fn_Pessoa_Fantasia(b.TransportadoraID, 0) AS Transportadora, ' +
		                        'dbo.fn_Pessoa_Fantasia(b.ProprietarioID, 0) AS Vendedor, \'\' ICMSST, dbo.fn_Log_Motivo(5110,24000,b.PedidoID,NULL,NULL,NULL,null) Observacoes, a.NotaFiscalID, ' +
		                        'dbo.fn_Pedido_Datas(a.PedidoID, 5) AS LiberacaoPedido, a.tipoEmissaoID as TipoEmissaoID, a.TipoNotaFiscalID, b.Observacao, ' +
		                        'DATEDIFF(N,dbo.fn_Log_Data(5110, 24000, b.PedidoID, NULL, NULL, NULL, 1), GETDATE()) TempoUltimoRetorno, ' +
		                        'dbo.fn_Pessoa_Fantasia(b.DepositoID, 0) AS Deposito, dbo.fn_TipoAuxiliar_Item(a.TipoEmissaoID, 0 ) AS TipoEmissao, a.DocumentoFiscalID AS DocumentoFiscalID, ' +
		                        'a.ArquivoDanfePDF ' +

	                        'FROM NotasFiscais a WITH(NOLOCK) ' +
			                        'INNER JOIN Pedidos	b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) ' +
			                        'INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = a.EstadoID) ' +
			                        'INNER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = b.EstadoID) ' +
			                        ((sVinculadoFrom != '') ? sVinculadoFrom : ' ') +
	                        'WHERE ' + sFiltro + ' a.ModeloNF IN (\'55\', \'Se\') AND a.EstadoID = 5 ' +
	                                'AND a.EmpresaID = ' + aEmpresaData[0] + ' ' +
			                        'AND b.AjustesRejeicaoNFePendentes = 1 AND b.NotaFiscalID IS NULL AND a.Emissor = 1 ' +
			                        'AND ' + sFiltroRejeitado + sFiltroDeposito +
			                        'AND a.NotaFiscalID = (SELECT MAX(NotaFiscalID) FROM NotasFiscais WITH(NOLOCK) WHERE  PedidoID = b.PedidoID) ' +
                        'UNION ALL ' +
                        'SELECT dbo.fn_TipoAuxiliar_Item(a.DocumentoFiscalID, 2) AS DocumentoFiscal, ' + ((sVinculadoSelect != '') ? sVinculadoSelect : '') + 'a.NotaFiscal, c.RecursoAbreviado AS EstadoNF, a.PedidoID, a.TransacaoID, d.RecursoAbreviado AS EstadoPedido, dbo.fn_Pessoa_Fantasia(b.PessoaID, 0) AS Pessoa, ' +
                            'CONVERT(VARCHAR(10), a.dtNotaFiscal, 103) AS DataNota, dbo.fn_Pessoa_Fantasia(b.TransportadoraID, 0) AS Transportadora, ' +
		                    'dbo.fn_Pessoa_Fantasia(b.ProprietarioID, 0) AS Vendedor, (CASE b.Pagamento_ICMSST WHEN 0 THEN \'\' WHEN 1 THEN \'GARE\' WHEN 2 THEN \'GNRE\' WHEN 3 THEN \'GARE e GNRE\' END) ICMSST, dbo.fn_Log_Motivo(5120,24030,a.NotaFiscalID,NULL,NULL,NULL,null) Observacoes, a.NotaFiscalID, ' +
		                    'dbo.fn_Pedido_Datas(a.PedidoID, 5) AS LiberacaoPedido, a.tipoEmissaoID as TipoEmissaoID,a.TipoNotaFiscalID, b.Observacao, ' +
		                    'DATEDIFF(N,dbo.fn_Log_Data(5120, 24030, a.NotaFiscalID, NULL, NULL, NULL, 1), GETDATE()) TempoUltimoRetorno, ' +
		                    'dbo.fn_Pessoa_Fantasia(b.DepositoID, 0) AS Deposito, dbo.fn_TipoAuxiliar_Item(a.TipoEmissaoID, 0 ) AS TipoEmissao, a.DocumentoFiscalID AS DocumentoFiscalID, ' +
		                    'a.ArquivoDanfePDF ' +

	                        'FROM NotasFiscais a WITH(NOLOCK) ' +
			                        'INNER JOIN Pedidos	b WITH(NOLOCK) ON (b.NotaFiscalID = a.NotaFiscalID) ' +
			                        'INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = a.EstadoID) ' +
			                        'INNER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = b.EstadoID) ' +
			                        ((sVinculadoFrom != '') ? sVinculadoFrom : '') +
	                        'WHERE ' + sFiltro + ' + a.ModeloNF IN (\'55\', \'Se\') AND a.Emissor = 1 ' +
	                            'AND a.EmpresaID = ' + aEmpresaData[0] + ' ' +
	                            'AND ISNULL(a.QuantidadeImpressao, 0) = 0 ' +
	                            'AND ' + sFiltroPendenteImp + sFiltroDeposito;

    dsoNotaFiscal.ondatasetcomplete = NotaFiscal_DSC;

    try {
        dsoNotaFiscal.refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;
        lockControlsInModalWin(false);
        window.focus();
        txtFiltro.focus();
    }
}

function NotaFiscal_DSC() {
    var dTFormat = '';
    dTFormat = 'dd/mm/yyyy';

    if ((dsoNotaFiscal.recordset.BOF) || (dsoNotaFiscal.recordset.EOF)) {
        window.top.overflyGen.Alert("N�o existem notas");
        fg.Rows = 1;
        lockControlsInModalWin(false);
        return null;
    }

    dsoNotaFiscal.recordset.moveFirst();

    startGridInterface(fg);
    fg.FontSize = '8';
    fg.FrozenCols = 0;

    //Inicio = 0
    headerGrid(fg, ['Vinc',
	               'Doc',
	               'NF',
                   'E',
                   'Pedido',
                   'Tran',
                   'E',
                   'Pessoa',
                   'Data',
                   'Transportadora',
                   'ICMSST',
                   'Observa��o',
                   'Libera��o ',
                   'Vendedor',
                   'Observa��es',
                   'Deposito',
                   'Tipo Emiss�o',
                   'NotaFiscalID',
                   'TempoUltimoRetorno',
                   'TipoEmissaoID',
                   'CorVinculado',
                   'DocumentoFiscalID',
                   'ArquivoDanfePDF'],
                   [16, 17, 18, 19, 20, 22]);


    glb_aCelHint = [0, 0, 'Tipo de Documento Fiscal'];

    fillGridMask(fg, dsoNotaFiscal, ['VinculadoID',
	                                'DocumentoFiscal',
	                                'NotaFiscal',
                                    'EstadoNF',
                                    'PedidoID',
                                    'TransacaoID',
                                    'EstadoPedido',
                                    'Pessoa',
                                    'DataNota',
                                    'Transportadora',
                                    'ICMSST',
                                    'Observacao',
                                    'LiberacaoPedido',
                                    'Vendedor',
                                    'Observacoes',
                                    'Deposito',
                                    'TipoEmissao',
                                    'NotaFiscalID',
                                    'TempoUltimoRetorno',
                                    'TipoEmissaoID',
                                    'CorVinculado',
                                    'DocumentoFiscalID',
                                    'ArquivoDanfePDF'],
				  				    ['', '', '', '', '', '', '', '', '99/99/9999', '', '', '', '99/99/9999 99:99', '', '', '', '', '', '', '', '', ''],
								    ['', '', '', '', '', '', '', '', '', '', '', '', dTFormat + ' hh:mm', '', '', '', '', '', '', '', '', '']);





    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.ExplorerBar = 5;
    fg.FrozenCols = 1;
    fg.Redraw = 2;
    fg.Editable = false;

    // Adiciona cor nas linhas do Grid
    for (i = 1; i < fg.Rows; i++) {
        var sEstadoNF = fg.TextMatrix(i, getColIndexByColKey(fg, 'EstadoNF'));
        var nTipoEmissaoID = fg.TextMatrix(i, getColIndexByColKey(fg, 'TipoEmissaoID'));
        var nTempoUltimoRetorno = fg.TextMatrix(i, getColIndexByColKey(fg, 'TempoUltimoRetorno'));

        // NFe Rejeitada(Z) a mais de 59min pintar Vermelho
        if ((sEstadoNF == 'Z') && (nTempoUltimoRetorno >= 60))
            fg.Cell(6, i, 1, i, 11) = 0X7280FA; //Vermelho
            // NFe Gerada e nao transmitida (G) em Fomulario de Seguranca (893) e
            // NFe Autorizada (A) em Formulario de Seguranca  pintar de Amarelo
        else if ((sEstadoNF == 'G' && nTipoEmissaoID == 893) || (sEstadoNF == 'A' && nTipoEmissaoID == 893))
            fg.Cell(6, i, 1, i, 11) = 0X8CE6F0; //Amarelo

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'CorVinculado')) != '')
            //fg.Cell(6, i, 1, i, 11) = 0X8CE6F0; //Amarelo
            fg.Cell(6, i, getColIndexByColKey(fg, 'VinculadoID'), i, getColIndexByColKey(fg, 'VinculadoID')) = eval(fg.TextMatrix(i, getColIndexByColKey(fg, 'CorVinculado')));
    }

    lockControlsInModalWin(false);

    // destrava botao Incluir se tem linhas no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
    }
}

function selImpressoras_onchange() {
    lblImpressoras.title = 'UNC: ' + selImpressoras.options[selImpressoras.selectedIndex].getAttribute("UNC", 1) + ' Local: ' + selImpressoras.options[selImpressoras.selectedIndex].getAttribute("Local", 1);
}

function selDepositoID_onchange() {
    gblDepositoID = selDepositoID.value;
    NotaFiscal();
}

function txtFiltro_onKeyPress() {
    if (event.keyCode == 13) {
        lockControlsInModalWin(true);
        NotaFiscal();
    }
}