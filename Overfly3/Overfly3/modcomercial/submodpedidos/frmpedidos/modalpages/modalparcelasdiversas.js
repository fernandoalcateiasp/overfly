/********************************************************************
modalparcelasdiversas.js

Library javascript para o modalparcelasdiversas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nUserID = 0;

var dsoGravacao = new CDatatransport('dsoGravacao');
var dsoPesquisa = new CDatatransport('dsoPesquisa');
var dsoTipoParcela = new CDatatransport('dsoTipoParcela');
var dsoCartao = new CDatatransport('dsoCartao');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
chks_onclick()
sels_onchange()
btn_onclick(ctl)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

	setupControlsData();
	
	// configuracao inicial do html
    setupPage();   
	
    // ajusta o body do html
    with (modalparcelasdiversasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
	
	showExtFrame(window, true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Incluir Despesas', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
        
    // reajusta dimensoes e reposiciona a janela
	//redimAndReposicionModalWin(345, 180, false);
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    adjustElementsInForm([['lblHistoricoPadraoID', 'selHistoricoPadraoID', 24, 1, -10, -10],
                          ['lblTipoComissao', 'selTipoComissao', 24, 1],
						  ['lblPesquisa','txtPesquisa',10,2,-10],
						  ['btnFindPesquisa','btn',24,2],
						  ['lblPessoaID','selPessoaID',41,2],
						  ['lblFormaPagamentoID','selFormaPagamentoID',7,3,-10],
						  ['lblPrazo','txtPrazo',8,3],
						  ['lblPercentual','txtPercentual',7,3],
						  ['lblValor', 'txtValor', 11, 3],
						  ['lblCartao', 'selCartao', 12, 3]], null, null, true);

	selPessoaID.onchange = sels_onchange;
	selPessoaID.disabled = true;

	selHistoricoPadraoID.onchange = selHistoricoPadraoID_onchange;
	selTipoComissao.onchange = selTipoComissao_onchange;
	selCartao.onchange = selCartao_onchange;
	txtPesquisa.onkeydown = txtPesquisa_onkeydown;
	txtValor.onkeydown = txtValor_onkeydown;
	txtValor.onblur = txtValor_onblur;
	txtPercentual.onkeydown = txtPercentual_onkeydown;
	txtPercentual.onblur = txtPercentual_onblur;

	// ajusta o divFields
    elem = divFields;
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = txtPercentual.offsetTop + txtPercentual.offsetHeight + 2;
    }
    
    selHistoricoPadraoID_onchange();
}

/********************************************************************
Usuario teclou Enter em campo texto
********************************************************************/
function txt_onkeydown()
{
	if ( event.keyCode == 13 )	
		;
}

/********************************************************************
Usuario clicou em checkbox
********************************************************************/
function chks_onclick()
{
	;
}

/********************************************************************
Usuario alterou selecao de combo
********************************************************************/
function sels_onchange()
{
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		grava();
    }
    else if (controlID == 'btnCanc')
    {
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null); 
    }
}

function setupControlsData()
{
	txtPrazo.onkeypress = verifyNumericEnterNotLinked;
	txtPrazo.setAttribute('verifyNumPaste', 1);
	txtPrazo.setAttribute('thePrecision', 3, 1);
	txtPrazo.setAttribute('theScale', 0, 1);
	txtPrazo.setAttribute('minMax', new Array(0, 100), 1);
	txtPrazo.onfocus = selFieldContent;
    txtPrazo.maxLength = 3;
    
	txtPercentual.onkeypress = verifyNumericEnterNotLinked;
	txtPercentual.setAttribute('verifyNumPaste', 1);
	txtPercentual.setAttribute('thePrecision', 3, 1);
	txtPercentual.setAttribute('theScale', 2, 1);
	txtPercentual.setAttribute('minMax', new Array(0, 100), 1);
	txtPercentual.onfocus = selFieldContent;
    txtPercentual.maxLength = 3;
    
    txtValor.onkeypress = verifyNumericEnterNotLinked;
	txtValor.setAttribute('verifyNumPaste', 1);
	txtValor.setAttribute('thePrecision', 11, 1);
	txtValor.setAttribute('theScale', 2, 1);
	txtValor.setAttribute('minMax', new Array(0, glb_nValorTotalPedido), 1);
	txtValor.onfocus = selFieldContent;
    txtValor.maxLength = 11;

    txtPesquisa.maxLength = 20;
}

function adjustBtnsStatus()
{
	;
}

function btnFindPesquisa_onclick()
{
	setConnection(dsoPesquisa);
	dsoPesquisa.SQL = 'SELECT TOP 80 a.PessoaID AS fldID, a.Fantasia + ' + '\'' + ' (' + '\'' + ' + CONVERT(VARCHAR(10), a.PessoaID) + ' + 
		'\'' + ')' + '\'' + ' AS fldText ' +
		'FROM Pessoas a WITH(NOLOCK) ' +
		'WHERE a.Fantasia LIKE ' + '\'' + txtPesquisa.value + '%' + '\'' + ' ' +
		'ORDER BY a.Fantasia';

	dsoPesquisa.ondatasetcomplete = dsoPesquisa_DSC;
	dsoPesquisa.Refresh();
}

function dsoPesquisa_DSC()
{
    clearComboEx(['selPessoaID']);

    while (!dsoPesquisa.recordset.EOF)
    {
        optionStr = dsoPesquisa.recordset['fldText'].value;
		optionValue = dsoPesquisa.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selPessoaID.add(oOption);
        dsoPesquisa.recordset.MoveNext();
    }

    selPessoaID.disabled = (selPessoaID.options.length == 0);
}

function preencheTipoParcela(nTipo) 
{
    var sWhere = '';
    
    if (nTipo == 3304)
        sWhere = ' AND (a.Filtro LIKE \'%Financ%\') ';
    else
        sWhere = ' AND (ISNULL(a.Filtro, \'\') NOT LIKE \'%Financ%\') ';

    var strSQL = 'SELECT a.ItemID AS fldID, dbo.fn_Tradutor(a.ItemMasculino, ' + glb_nIdiomaDeID + ', ' + glb_nIdiomaParaID + ', NULL) AS fldText ' +
                    'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                    'WHERE a.EstadoID = 2 AND a.TipoID = 406 ' + sWhere +
                    'ORDER BY a.Ordem ';
    
    setConnection(dsoTipoParcela);
    dsoTipoParcela.SQL = strSQL;

    dsoTipoParcela.ondatasetcomplete = dsoTipoParcela_DSC;
    dsoTipoParcela.Refresh();
}

function dsoTipoParcela_DSC() 
{
    clearComboEx(['selTipoComissao']);

    while (!dsoTipoParcela.recordset.EOF) 
    {
        optionStr = dsoTipoParcela.recordset['fldText'].value;
        optionValue = dsoTipoParcela.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selTipoComissao.add(oOption);
        dsoTipoParcela.recordset.MoveNext();
    }

    lblTipoComissao.style.visibility = 'inherit';
    selTipoComissao.style.visibility = 'inherit';

    selTipoComissao_onchange();
}

function txtPesquisa_onkeydown()
{
	if (event.keyCode == 13)
		btnFindPesquisa_onclick();
}

function txtValor_onkeydown()
{
	if (event.keyCode == 13)
		calcPercentual();
}

function txtValor_onblur()
{
	calcPercentual();
}

function txtPercentual_onkeydown()
{
	if (event.keyCode == 13)
		calcValor();
}

function txtPercentual_onblur()
{
	calcValor();
}

function calcPercentual()
{
	var nValor = txtValor.value;
	
	if ( (glb_nValorTotalPedido == null) || (glb_nValorTotalPedido == 0) || (isNaN(nValor)) )
	{
		txtPercentual.value = '';
		return;
	}
	
	txtPercentual.value = roundNumber((nValor/glb_nValorTotalPedido) * 100, 2);
}

function calcValor()
{
	var nPercentual = txtPercentual.value;
	
	if ( (glb_nValorTotalPedido == null) || (glb_nValorTotalPedido == 0) || (isNaN(nPercentual)) )
	{
		txtValor.value = '';
		return;
	}
	
	txtValor.value = roundNumber(glb_nValorTotalPedido * (nPercentual/100), 2);
}

function grava()
{
	var sError = '';
	var strPars = '';
	var nLimiteValor = 0;
	var empresaData = getCurrEmpresaData();

	if (selHistoricoPadraoID.selectedIndex < 0)
		sError += 'Preencha o Hist�rico padr�o.\n';
	
	if (selPessoaID.selectedIndex < 0)
		sError += 'Preencha a pessoa.\n';
	
	if (selFormaPagamentoID.selectedIndex < 0)
		sError += 'Preencha a forma de pagamento.\n';
	
	if (selFormaPagamentoID.selectedIndex < 0)
		sError += 'Preencha a forma de pagamento.\n';

	if (trimStr(txtPrazo.value) == '')
		sError += 'Preencha o prazo.\n';

	if (trimStr(txtValor.value) == '')
	    sError += 'Preencha o valor.\n';

	if (selHistoricoPadraoID.value == 3174) {
	    if (!(selHistoricoPadraoID.value == 3174 && selFormaPagamentoID.value == 1038))
	        sError += 'Forma incorreta para Reemb de Frete.\n';
	}		
	if (sError == '')
	{
		nLimiteValor = ((glb_nValorTotalPedido/2) - glb_nTotalDispesas);
		
	    // Exce��o temporaria para Allplus solicitada pelo Camilo. BJBN 12/05/2015.
		if ((txtValor.value > nLimiteValor) && (empresaData[0] != 7))
			sError += 'O valor desta parcela n�o pode ser superior a ' + roundNumber(nLimiteValor, 2) + '.\n';
	}
		
	if (sError != '')
	{
		if ( window.top.overflyGen.Alert(sError) == 0 )
			return null;
			
		return null;
	}

	lockControlsInModalWin(true);

	var nTipoComissao = 0;

	if (selHistoricoPadraoID.value == 3141 || selHistoricoPadraoID.value == 3143 || selHistoricoPadraoID.value == 3304)
	    nTipoComissao = selTipoComissao.value;
	
	try
	{
		strPars = '?';
		strPars += 'nPedidoID=' + escape(glb_nPedidoID);
		strPars += '&nPessoaID=' + escape(selPessoaID.value);
		strPars += '&nHistoricoPadraoID=' + escape(selHistoricoPadraoID.value);
		strPars += '&nTipoComissao=' + escape(nTipoComissao);		
		strPars += '&nFormaPagamentoID=' + escape(selFormaPagamentoID.value);
		strPars += '&Prazo=' + escape(txtPrazo.value);
		strPars += '&Valor=' + escape(txtValor.value);
	                
		dsoGravacao.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/gravaparceladiversos.aspx' + strPars;
		dsoGravacao.ondatasetcomplete = gravacao_DSC;
		dsoGravacao.refresh();
	}
	catch(e)
	{
		// Numero de erro qdo o registro foi alterado ou removido por outro usuario
		// ou operacao de gravacao abortada no banco
		if (e.number == -2147217887)
		    if ( window.top.overflyGen.Alert('Erro ao gravar.') == 0 )
		        return null;

		// Simula o botao OK para fechar a janela e forcar um refresh no sup
		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
		return null;
	}
}

function gravacao_DSC()
{
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
}

function selHistoricoPadraoID_onchange()
{
    var empresaData = getCurrEmpresaData();
    
	selOptByValueInSelect(getHtmlId(), 'selFormaPagamentoID', 0);
	txtPrazo.value = '';
	txtPercentual.value = '';
	clearComboEx(['selPessoaID']);
	calcValor();

	txtValor.disabled = false;
	txtPercentual.disabled = false;

	if (selHistoricoPadraoID.selectedIndex >= 0)
	{
	    if (selHistoricoPadraoID.value == 3174)
	        selOptByValueInSelect(getHtmlId(), 'selFormaPagamentoID', 1038);

	    if (selHistoricoPadraoID.value == 3304) {
	        preencheCartao();
	    }
	    else {
	        lblCartao.style.visibility = 'hidden';
	        selCartao.style.visibility = 'hidden';
	    }
	    
	    if (selHistoricoPadraoID.value == 3141 || selHistoricoPadraoID.value == 3143 || selHistoricoPadraoID.value == 3304)
	    {
	        lblTipoComissao.innerText = 'Tipo Comiss�o';
	        preencheTipoParcela(selHistoricoPadraoID.value);
	    }
	    else
	    {
	        lblTipoComissao.style.visibility = 'hidden';
	        selTipoComissao.style.visibility = 'hidden';
	    }
	}

	selPessoaID.disabled = (selPessoaID.options.length == 0);
}

function selTipoComissao_onchange() 
{
    var nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');
    var sPessoa = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selPessoaID.innerText');

    if ((selHistoricoPadraoID.selectedIndex >= 0) && (selTipoComissao.selectedIndex >= 0))
    {
        if (selHistoricoPadraoID.value == 3304) 
        {
            lblTipoComissao.innerText = 'Tipo Financiamento';
            clearComboEx(['selPessoaID']);
            oOption = document.createElement("OPTION");
            oOption.text = sPessoa;
            oOption.value = nPessoaID;
            selPessoaID.add(oOption);

            selOptByValueInSelect(getHtmlId(), 'selFormaPagamentoID', 1037);

            //BNDES
            if (selTipoComissao.value == 646)
                selCartao_onchange();

            //IBM
            else if (selTipoComissao.value == 647)
            {
                txtPrazo.value = 7;
                txtPercentual.value = 9.91;

                lblCartao.style.visibility = 'hidden';
                selCartao.style.visibility = 'hidden';


                calcValor();

                txtValor.disabled = true;
                txtPercentual.disabled = true;
            }
        }
    }
}

function preencheCartao() {

    var strSQL = 'SELECT ItemID AS fldID, ItemAbreviado AS fldText ' +
	                'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
	                'WHERE TipoID = 800 AND EstadoID = 2 ';

    setConnection(dsoCartao);
    dsoCartao.SQL = strSQL;
    dsoCartao.ondatasetcomplete = dsoCartao_DSC;
    dsoCartao.Refresh();
}

function dsoCartao_DSC() {
    clearComboEx(['selCartao']);

    while (!dsoCartao.recordset.EOF) {
        optionStr = dsoCartao.recordset['fldText'].value;
        optionValue = dsoCartao.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selCartao.add(oOption);
        dsoCartao.recordset.MoveNext();
    }

    lblCartao.style.visibility = 'inherit';
    selCartao.style.visibility = 'inherit';

    selCartao_onchange();
}

function selCartao_onchange() {
    //BNDES
    txtPrazo.value = 28;

    if (selCartao.value == 997)//Cart�o Cabal
    {
        txtPercentual.value = 2.0;
    }
    // Elo e Visa
    else if ((selCartao.value == 998) || (selCartao.value == 1000)) {
        if ((glb_nValorTotalPedido >= 0.01) && (glb_nValorTotalPedido <= 50000.00))
            txtPercentual.value = 2.7;
        else if ((glb_nValorTotalPedido >= 50000.01) && (glb_nValorTotalPedido <= 100000.00))
            txtPercentual.value = 2.5;
        else if ((glb_nValorTotalPedido >= 100000.01) && (glb_nValorTotalPedido <= 250000.00))
            txtPercentual.value = 2.35;
        else if ((glb_nValorTotalPedido >= 250000.01) && (glb_nValorTotalPedido <= 500000.00))
            txtPercentual.value = 1.70;
        else if (glb_nValorTotalPedido >= 500000.01)
            txtPercentual.value = 1.35;
    }
    else if (selCartao.value == 999)//MasterCard
        txtPercentual.value = 2.7;

    lblCartao.style.visibility = 'inherit';
    selCartao.style.visibility = 'inherit';

    calcValor();

    txtValor.disabled = true;
    txtPercentual.disabled = true;
}