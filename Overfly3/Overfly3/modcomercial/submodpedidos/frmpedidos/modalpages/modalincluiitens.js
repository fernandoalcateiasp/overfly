/********************************************************************
modalincluiitens.js
Library javascript para o modalincluiitens.asp

********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_timerInterval = null;

// controla se os itens do pedido corrente no inf foram alterados
var glb_pedidoAlterado = false;

// controla ultimo combo alterado pelo usuario
// 0 - nenhum dos dois, 1 - Produto, 2 - Marca
var glb_Produto = false;
var glb_Marca = false;

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_callFillGridTimerInt = null;
var glb_nFamiliaID = null;
var glb_produtoDesativo = false;
var glb_nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'TransacaoID\'].value');
var glb_ParceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ParceiroID\'].value');

// DSO
var dsoGen01 = new CDatatransport("dsoGen01");
var dsoGen02 = new CDatatransport("dsoGen02");
var dsoGen03 = new CDatatransport("dsoGen03");
var dsoehImportacao = new CDatatransport("dsoehImportacao");
var dsoFamilia = new CDatatransport("dsoFamilia");
var dsoDescricao = new CDatatransport("dsoDescricao");
var dsoGravacao = new CDatatransport("dsoGravacao");
var dsoReferencia = new CDatatransport("dsoReferencia");
var dsoAtualizaValorUnitario = new CDatatransport("dsoAtualizaValorUnitario");
var dsoVerificaItem = new CDatatransport("dsoVerificaItem");   
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_pedidos_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 1;
    
    if ( glb_totalCols__ != false )
        firstLine = 2;
        
    if (NewRow >= firstLine)
        fg.Row = NewRow;
    else if ((NewRow < firstLine) && (fg.Rows > firstLine))
		fg.Row = firstLine;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_ModalIncluiItens_AfterEdit(Row, Col)
{
    var nColPrecoInterno = getColIndexByColKey(fg, colunasPrecoReadOnly('PrecoInterno'));
    var nColPrecoRevenda = getColIndexByColKey(fg, colunasPrecoReadOnly('PrecoRevenda'));
    var nColPrecoUnitario = getColIndexByColKey(fg, colunasPrecoReadOnly('PrecoUnitario'));
    var nColCalcValorTotal = getColIndexByColKey(fg, '_calc_ValorTotal_10*');
    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade');
    var nColdtVigenciaServicoInicio = getColIndexByColKey(fg, 'dtVigenciaServicoInicio');
    var nColdtVigenciaServicoFim = getColIndexByColKey(fg, 'dtVigenciaServicoFim');
    var nPrecoInterno = 0;
    var nPrecoRevenda = 0;
    var nPrecoUnitario = 0;
    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'Resultado\'].value');
    
    if (fg.Editable)
    {
        if ((fg.col == nColPrecoInterno) || (fg.col == nColPrecoRevenda) || (fg.col == nColPrecoUnitario) || (fg.Col == nColQuantidade))
        {
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
            fg.TextMatrix(1, nColQuantidade) = 0;
            fg.TextMatrix(1, nColCalcValorTotal) = 0;
              
            nPrecoInterno = fg.ValueMatrix(Row, nColPrecoInterno);
            nPrecoRevenda = fg.ValueMatrix(Row, nColPrecoRevenda);
            nPrecoUnitario = fg.ValueMatrix(Row, nColPrecoUnitario);

            if (bResultado) 
            {
                if (nColPrecoInterno == Col) 
                {
                    if (nPrecoInterno > nPrecoRevenda)
                        fg.TextMatrix(Row, nColPrecoRevenda) = nPrecoInterno;

                    if (nPrecoInterno > nPrecoUnitario)
                        fg.TextMatrix(Row, nColPrecoUnitario) = nPrecoInterno;
                }
                else if (nColPrecoRevenda == Col) 
                {
                    if (nPrecoRevenda < nPrecoInterno)
                        fg.TextMatrix(Row, nColPrecoInterno) = nPrecoRevenda;

                    if (nPrecoRevenda > nPrecoUnitario)
                        fg.TextMatrix(Row, nColPrecoUnitario) = nPrecoRevenda;
                }
                else if (nColPrecoUnitario == Col) 
                {
                    if (nPrecoUnitario < nPrecoRevenda)
                        fg.TextMatrix(Row, nColPrecoRevenda) = nPrecoUnitario;

                    if (nPrecoUnitario < nPrecoInterno)
                        fg.TextMatrix(Row, nColPrecoInterno) = nPrecoUnitario;
                }
            }

            if (fg.ValueMatrix(Row, nColQuantidade) == 0)
                fg.TextMatrix(Row, nColCalcValorTotal) = '';
            else
                fg.TextMatrix(Row, nColCalcValorTotal) = fg.ValueMatrix(Row, nColQuantidade) * fg.ValueMatrix(Row, nColPrecoUnitario);
        }

        else if (fg.col == nColdtVigenciaServicoInicio) {

            if ((verificaData(fg.TextMatrix(Row, nColdtVigenciaServicoInicio)) == false)) {
                fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

            }

            else if ((new Date(normalizeDate_DateTime(dateAdd('d', '31', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) < (new Date())) {

                fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                if (window.top.overflyGen.Alert('Vig�ncia in�cio n�o deve ser anterior aos �ltimos 30 dias.') == 0)
                    return null;

                return false;
            }

            else if (fg.TextMatrix(Row, nColdtVigenciaServicoFim).length > 0) {

                if ((new Date(normalizeDate_DateTime(dateAdd('d', '31', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) >
                        (new Date(normalizeDate_DateTime(fg.TextMatrix(Row, nColdtVigenciaServicoFim), (DATE_FORMAT == "DD/MM/YYYY" ? true : false))))) {

                    fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                    if (window.top.overflyGen.Alert('Per�odo de vig�ncia dever� ser de pelo menos 30 dias.') == 0)
                        return null;

                    return false;
                }

                else if ((new Date(normalizeDate_DateTime(dateAdd('yyyy', '5', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) <
                        (new Date(normalizeDate_DateTime(fg.TextMatrix(Row, nColdtVigenciaServicoFim), (DATE_FORMAT == "DD/MM/YYYY" ? true : false))))) {

                    fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                    if (window.top.overflyGen.Alert('Per�odo de vig�ncia m�xima s�o 5 anos.') == 0)
                        return null;

                    return false;
                }
            }
        }

        else if (fg.col == nColdtVigenciaServicoFim) {

            if ((verificaData(fg.TextMatrix(Row, nColdtVigenciaServicoFim)) == false)) {
                fg.TextMatrix(Row, nColdtVigenciaServicoFim) = '';

            }

            else if ((new Date(normalizeDate_DateTime(dateAdd('d', '1', fg.TextMatrix(Row, nColdtVigenciaServicoFim)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) < (new Date())) {

                fg.TextMatrix(Row, nColdtVigenciaServicoFim) = '';

                if (window.top.overflyGen.Alert('Vig�ncia fim deve ser maior que a data atual.') == 0)
                    return null;

                return false;
            }

            else if (fg.TextMatrix(Row, nColdtVigenciaServicoInicio).length > 0) {

                if ((new Date(normalizeDate_DateTime(dateAdd('d', '31', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) >
                        (new Date(normalizeDate_DateTime(fg.TextMatrix(Row, nColdtVigenciaServicoFim), (DATE_FORMAT == "DD/MM/YYYY" ? true : false))))) {

                    fg.TextMatrix(Row, nColdtVigenciaServicoFim) = '';

                    if (window.top.overflyGen.Alert('Per�odo de vig�ncia dever� ser de pelo menos 30 dias.') == 0)
                        return null;

                    return false;
                }

                else if ((new Date(normalizeDate_DateTime(dateAdd('yyyy', '5', fg.TextMatrix(Row, nColdtVigenciaServicoInicio)), (DATE_FORMAT == "DD/MM/YYYY" ? true : false)))) <
                        (new Date(normalizeDate_DateTime(fg.TextMatrix(Row, nColdtVigenciaServicoFim), (DATE_FORMAT == "DD/MM/YYYY" ? true : false))))) {

                    fg.TextMatrix(Row, nColdtVigenciaServicoInicio) = '';

                    if (window.top.overflyGen.Alert('Per�odo de vig�ncia m�xima s�o 5 anos.') == 0)
                        return null;

                    return false;
                }
            }
        }

        setTotaisGrid(Row, Col);

       // if ((fg.col == nColPrecoInterno) || (fg.col == nColPrecoRevenda) || (fg.col == nColPrecoUnitario))
         //   atualizaValorUnitario(Row);
    }
}

function setTotaisGrid(nRow, Col)
{
    var nTotalQuantidade = 0;
    var nTotalValorTotal = 0;
    var nColPrecoUnitario = getColIndexByColKey(fg, colunasPrecoReadOnly('PrecoUnitario'));
    var nColCalcValorTotal = getColIndexByColKey(fg, '_calc_ValorTotal_10*');
    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade');
    var i = 0;

    if (fg.Rows > 1) 
    {
        fg.TextMatrix(1, nColQuantidade) = 0;
        fg.TextMatrix(1, nColCalcValorTotal) = 0;
    }

    for (i = 2; i < fg.Rows; i++) 
    {
        fg.TextMatrix(i, nColCalcValorTotal) = fg.ValueMatrix(i, nColPrecoUnitario) * fg.ValueMatrix(i, nColQuantidade);
        nTotalQuantidade += fg.ValueMatrix(i, nColQuantidade);
        nTotalValorTotal += fg.ValueMatrix(i, nColCalcValorTotal);
    }

    if (fg.Rows > 1) 
    {
        fg.TextMatrix(1, nColQuantidade) = nTotalQuantidade;
        fg.TextMatrix(1, nColCalcValorTotal) = nTotalValorTotal;
    }
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 60;
    modalFrame.style.left = 6;   
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', glb_pedidoAlterado );    
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Inclus�o de Itens', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
    
    // Carrega o dso com dados de combos
    fillCmbsData();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    btnOK.value = 'Incluir';
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o divFields
    elem = window.document.getElementById('divFields');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - 2 * ELEM_GAP;
        // altura e fixa para tres linhas de campos
        height = 3 * (16 + 24) + ELEM_GAP;
        y_gap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = y_gap + ELEM_GAP + 30;
        width = modWidth - frameBorder - 2 * ELEM_GAP;    
        height = parseInt(btnOK.currentStyle.top, 10) -
                 parseInt(top, 10) - frameBorder - ELEM_GAP;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    
    // ajusta elementos no divFields
    adjustElementsInForm([['lblProdutoID', 'txtProdutoID', 10, 1, -10, -10],
                          ['lblProduto','selProduto',24,1,-1],
                          ['lblMarca','selMarca',16,1,-1],
                          ['lblCaracteristica','selCaracteristica',30,2,-10],
                          ['lblFiltro','txtFiltro',49,2],
                          ['lblHomologacao','chkHomologacao',3,3,-10],
                          ['lblPreLancamento','chkPreLancamento',3,3],
                          ['lblLancamento','chkLancamento',3,3],
                          ['lblAtivo','chkAtivo',3,3],
                          ['lblPromocao','chkPromocao',3,3],
                          ['lblPontaEstoque','chkPontaEstoque',3,3],
                          ['lblOrdem','selOrdem',9,3],
                          ['lblValorLimite','txtValorLimite',12,3,1],
                          ['lblPalavraChave', 'txtPalavraChave', 31, 3, 2]], null, null, true);

    

	var nUserID = getCurrUserID();
	var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

    useBtnListar(true, divFields, selCaracteristica);
    
    with (btnListar)
    {
        style.left = parseInt(selMarca.currentStyle.left, 10) +
               parseInt(selMarca.currentStyle.width, 10) + ELEM_GAP - 6;
        style.top = parseInt(selMarca.currentStyle.top, 10) - 1;
    }
            
    // restringe digitacao em campos numericos
    // e da outras providencias
    setMaskAndLengthToInputs();
}

/********************************************************************
Restringe quantidade de caracteres digitados, coloca
mascara para digitacao em campos numericos,
seleciona conteudo do campo quando em foco etc
********************************************************************/
function setMaskAndLengthToInputs()
{
    // Seleciona conteudo de campo texto quando em foco
    txtPalavraChave.onfocus = selFieldContent;
    txtValorLimite.onfocus = selFieldContent;
    txtFiltro.onfocus = selFieldContent;
    
    // Altera checkBox se clicado no seu label
    lblHomologacao.onclick = invertChkBox;
    lblPreLancamento.onclick = invertChkBox;
    lblLancamento.onclick = invertChkBox;
    lblAtivo.onclick = invertChkBox;
    lblPromocao.onclick = invertChkBox;
    lblPontaEstoque.onclick = invertChkBox;
    
    // CheckBox que entram checados
    chkPreLancamento.checked = true;
    chkLancamento.checked = true;
    chkAtivo.checked = true;
    chkPromocao.checked = true;
    chkPontaEstoque.checked = true;
    
    txtProdutoID.onkeypress = verifyNumericEnterNotLinked;
    txtProdutoID.onkeydown = execPesq_OnKey;
    txtProdutoID.setAttribute('thePrecision', 10, 1);
    txtProdutoID.setAttribute('theScale', 0, 1);
    txtProdutoID.setAttribute('verifyNumPaste', 1);
    txtProdutoID.setAttribute('minMax', new Array(1, 9999999999), 1);
    txtProdutoID.value = '';
    
    txtPalavraChave.onkeypress = execPesq_OnKey;

    txtValorLimite.onkeypress = verifyNumericEnterNotLinked;
    txtValorLimite.setAttribute('thePrecision', 11, 1);
    txtValorLimite.setAttribute('theScale', 2, 1);
    txtValorLimite.setAttribute('minMax', new Array(0, 999999999.99), 1);
    txtValorLimite.setAttribute('verifyNumPaste', 1);
    txtValorLimite.value = '';
    
    selProduto.onchange = selProdutoChanged;
    selMarca.onchange = selMarcaChanged;
}

/********************************************************************
Pesquisa se Enter em campos com esta key
********************************************************************/
function execPesq_OnKey()
{
    txtProdutoID.value = trimStr(txtProdutoID.value);
    
    if ((this.id == 'txtProdutoID') && (event.keyCode != 13))
        fg.Rows = 1;

    if ( event.keyCode == 13 )
        btnListar_onclick();        
}


/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();
    
    return true;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    var nColProdutoID = getColIndexByColKey(fg, 'ProdutoID*');
    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade');
    var strSQL = '';

    for (i = 2; i < fg.Rows; i++)
    {
        if (fg.ValueMatrix(i, nColQuantidade) == 0) {
            fg.TextMatrix(i, nColQuantidade) = '';
            continue;
        }
        else
        {
            var nValidaProdutoID = fg.ValueMatrix(i, nColProdutoID);
            var itemCompativel;

            strSQL += (strSQL == '' ? '' : 'UNION ') +
                'SELECT CONVERT(BIT, dbo.fn_Pedido_Vinculado(' + glb_nPedidoID + ', ' + nValidaProdutoID.toString() + ', NULL, NULL, 6)) AS Compativel, '+ nValidaProdutoID.toString() + ' AS ProdutoID ';
            // verificaItem(nValidaProdutoID);
        }
    }

    setConnection(dsoVerificaItem);
    dsoVerificaItem.SQL = strSQL;
    dsoVerificaItem.ondatasetcomplete = verificaItem_DSC;
    dsoVerificaItem.refresh();
}

function verificaItem_DSC() 
{
    // 1. O usuario clicou o botao OK
    //sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , null );

    var nTipoPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoPedidoID' + '\'' + '].value');
    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Resultado' + '\'' + '].value');
    var bTemNF = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TemNF' + '\'' + '].value');

    var i, j, nNumeroItens;
    var aDataToSave = new Array();

    nNumeroItens = glb_nQuantidadeItens;
    j = 0;
    var nVariacaoItem = 0;
    var nValorInterno = 0;
    var nValorRevenda = 0;
    var nValorUnitario = 0;
    var bTemPPB = false;

    var nColProdutoID = getColIndexByColKey(fg, 'ProdutoID*');
    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade');
    //var nColImposto2Base = getColIndexByColKey(fg, 'Imposto2Base');
    var nColValorTabela = getColIndexByColKey(fg, 'ValorTabela');
    var nColMoedaID = getColIndexByColKey(fg, 'MoedaID');
    var nColTaxaMoeda = getColIndexByColKey(fg, 'TaxaMoeda');
    var nColGrupoImpostoID = getColIndexByColKey(fg, 'GrupoImpostoID');
    var nColPedidoReferencia = getColIndexByColKey(fg, 'PedidoReferencia');
    var nColFinalidadeID = getColIndexByColKey(fg, 'FinalidadeID');
    var nColTemPPB = getColIndexByColKey(fg, 'TemPPB');
    var nColPedItemEncomendaID = getColIndexByColKey(fg, 'PedItemEncomendaID');
    var nColEhEncomenda = getColIndexByColKey(fg, 'EhEncomenda');
    var nColValorInterno = getColIndexByColKey(fg, 'ValorInterno');
    var nColValorRevenda = getColIndexByColKey(fg, 'ValorRevenda');
    var nColValorUnitario = getColIndexByColKey(fg, 'ValorUnitario');
    var nColPrecoInterno = getColIndexByColKey(fg, colunasPrecoReadOnly('PrecoInterno'));
    var nColPrecoRevenda = getColIndexByColKey(fg, colunasPrecoReadOnly('PrecoRevenda'));
    var nColPrecoUnitario = getColIndexByColKey(fg, colunasPrecoReadOnly('PrecoUnitario'));
    var nConServicoID = getColIndexByColKey(fg, 'ConServicoID');
    var nColdtVigenciaServicoInicio = getColIndexByColKey(fg, colunasPrecoReadOnly('dtVigenciaServicoInicio'));
    var nColdtVigenciaServicoFim = getColIndexByColKey(fg, colunasPrecoReadOnly('dtVigenciaServicoFim'));
    var sProdutosIncompativeis = "";


    // Verifica coluna de quantidades do grid
    for (i = 2; i < fg.Rows; i++)
    {
        if (fg.ValueMatrix(i, nColQuantidade) == 0)
        {
            fg.TextMatrix(i, nColQuantidade) = '';
            continue;
        }
        else
        {
            var nValidaProdutoID = fg.ValueMatrix(i, nColProdutoID);
            var itemCompativel;
            var sdtVigenciaServicoInicio;
            var sdtVigenciaServicoFim;

            // verificaItem(nValidaProdutoID);

            dsoVerificaItem.recordset.MoveFirst();
            dsoVerificaItem.recordset.setFilter('ProdutoID=' + nValidaProdutoID);

            if (!(dsoVerificaItem.recordset.BOF && dsoVerificaItem.recordset.EOF))
            {
                if (!dsoVerificaItem.recordset['Compativel'].value)
                {
                    itemCompativel = false;
                    fg.TextMatrix(i, nColQuantidade) = '';

                    lockControlsInModalWin(false);
                    window.focus();
                    fg.focus();
                }
                else
                {
                    itemCompativel = true;
                }
            }

            dsoVerificaItem.recordset.setFilter('');

            if (!itemCompativel)
                sProdutosIncompativeis += "Item " + nValidaProdutoID + " incompat�vel com o Pedido.\n";
            else {
                if ((fg.TextMatrix(i, nColTemPPB) == 1) && (bResultado) && (bTemNF) && (nTipoPedidoID == '602')) {
                    nNumeroItens += 3;
                    bTemPPB = true;
                }
                else
                    nNumeroItens++;

                if (fg.ValueMatrix(i, nColValorTabela) != 0) {
                    if (fg.ColHidden(nColPrecoUnitario)) {
                        nValorInterno = fg.ValueMatrix(i, nColValorInterno);
                        nValorRevenda = fg.ValueMatrix(i, nColValorRevenda);
                        nValorUnitario = fg.ValueMatrix(i, nColValorUnitario);
                    }
                    else if (fg.ColHidden(nColValorUnitario)) {
                        nValorInterno = fg.ValueMatrix(i, nColPrecoInterno);
                        nValorRevenda = fg.ValueMatrix(i, nColPrecoRevenda);
                        nValorUnitario = fg.ValueMatrix(i, nColPrecoUnitario);
                    }

                    nVariacaoItem = ((nValorUnitario / roundNumber(fg.ValueMatrix(i, nColValorTabela) *
                                                                   (1 + (glb_nVariacao / 100)), 2)) - 1) * 100;

                    if (nVariacaoItem > 999.99)
                        nVariacaoItem = 999.99;
                    else if (nVariacaoItem < -999.99)
                        nVariacaoItem = -999.99;
                }
                else {
                    nVariacaoItem = 0;
                    nValorInterno = 0;
                    nValorRevenda = 0;
                    nValorUnitario = 0;
                }

                if (fg.TextMatrix(i, nColdtVigenciaServicoInicio) != '') {
                    sdtVigenciaServicoInicio = normalizeDate_DateTime(fg.TextMatrix(i, nColdtVigenciaServicoInicio), true);
                }
                else
                    sdtVigenciaServicoInicio = 'NULL';

                if (fg.TextMatrix(i, nColdtVigenciaServicoFim) != '') {
                    sdtVigenciaServicoFim = normalizeDate_DateTime(fg.TextMatrix(i, nColdtVigenciaServicoFim), true);
                }
                else
                    sdtVigenciaServicoFim = 'NULL';

                aDataToSave[j] = new Array(fg.ValueMatrix(i, nColProdutoID),
                                           fg.ValueMatrix(i, nColFinalidadeID),
                                           fg.ValueMatrix(i, nColGrupoImpostoID),
                                           fg.ValueMatrix(i, nColQuantidade),
                                           fg.ValueMatrix(i, nColValorTabela),
                                           nVariacaoItem,
                                           nValorInterno,
                                           nValorRevenda,
                                           nValorUnitario,
                                           fg.ValueMatrix(i, nColPedidoReferencia),
                                           fg.ValueMatrix(i, nColMoedaID),
                                           fg.ValueMatrix(i, nColTaxaMoeda),
                                           fg.ValueMatrix(i, nColPedItemEncomendaID),
                                           fg.ValueMatrix(i, nColEhEncomenda),
                                           fg.ValueMatrix(i, nConServicoID),//14
                                           sdtVigenciaServicoInicio,// 15
                                           sdtVigenciaServicoFim);//16

            }

            j++;
        }

        // Se nao for pedido de entrada
        if (nTipoPedidoID != 601) {
            if (nNumeroItens > glb_nNumeroMaxItens) {
                if (bTemPPB) {
                    if (window.top.overflyGen.Alert('Excedido o limite m�ximo de �tens neste pedido (PPB)!') == 0)
                        return null;
                }
                else {
                    if (window.top.overflyGen.Alert('Excedido o limite m�ximo de �tens neste pedido!') == 0)
                        return null;
                }

                lockControlsInModalWin(false);
                window.focus();
                fg.focus();
                return null;
            }
        }
    }

    // Se foram selecionados itens incompat�veis com pedido alerta usu�rio
    if (sProdutosIncompativeis != "")
        if (window.top.overflyGen.Alert(sProdutosIncompativeis) == 0)
            return null;

    if (nNumeroItens == glb_nQuantidadeItens) {
        if (window.top.overflyGen.Alert('Nenhum �tem a ser gravado!') == 0)
            return null;
        lockControlsInModalWin(false);
        window.focus();
        fg.focus();
        return null;
    }


    saveItens(aDataToSave, aDataToSave.length, aDataToSave[0].length);
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modalincluiitensBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    if (txtPalavraChave.currentStyle.visibility != 'hidden')
        if (txtPalavraChave.disabled == false)
            txtPalavraChave.focus();
}

/********************************************************************
Usuario clicou o botao listar
********************************************************************/
function btnListar_onclick()
{
    var stopPesq = false;
    
    // trima todos os campos
    txtProdutoID.value = trimStr(txtProdutoID.value);
    txtValorLimite.value = trimStr(txtValorLimite.value);
    txtFiltro.value = trimStr(txtFiltro.value);    

    stopPesq = ( (trimStr(txtProdutoID.value) == '') &&
                 (selProduto.selectedIndex <= 0) &&
                 (selMarca.selectedIndex <= 0) &&
                 (selCaracteristica.selectedIndex <= 0) &&
                 (trimStr(txtFiltro.value) == '') &&
                 (trimStr(txtValorLimite.value) == '') &&
                 (trimStr(txtPalavraChave.value) == '') );

    fg.Rows = 1;

    if ( stopPesq )
    {
        if ( window.top.overflyGen.Alert('Preencha ao menos um campo, para listar.') == 0 )
            return null;
                    
        window.focus();
                    
        return true;
    }    
    
    lockControlsInModalWin(true);
    
    // conforme conteudo de txtPedidoReferencia ou txtNotaFiscalReferencia
    // considera ou nao os valores digitados nos campos
    // txtPalavraChave
    // txtValorLimite
    // txtFiltro.value

    glb_timerInterval = window.setInterval('fillgridData()', 10, 'JavaScript');

    //    glb_timerInterval = window.setInterval ( 'fillgridData()', 10 , 'JavaScript');
}

function fillgridData()
{
    if ( glb_timerInterval != null )
    {
        window.clearInterval(glb_timerInterval);
        glb_timerInterval = null;
    }

    lockControlsInModalWin(true);
    
    var sFiltroPedido = '';
    var sFiltroNotaFiscal = '';
	var nEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'getCurrEmpresaData()');

	var nIdiomaDeID = nEmpresaData[7];
	var nIdiomaParaID = nEmpresaData[8];
        
    var strSQL = '';
    var strSQLSelect = '';
    var strSQLSelect2 = '';
    var strSQLSelectServicos = '';
    var strSQLFrom = '';
    var strSQLWhere = '';
    var nEstadoID;
    var sFiltroFornecedor = '';
    var sTop = '';
    //var nPedidoReferenciaID = txtPedidoReferencia.value; 
    //var nNotaFiscalReferenciaID = txtNotaFiscalReferencia.value; 
    var sImposto2Base = '';

    var bTemNF = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TemNF' + '\'' + '].value');
    var nTemNF = (bTemNF ? 1 : 0);
    var nMetodoCustoID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'MetodoCustoID' + '\'' + '].value'), 10);
    var bServico = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Servico' + '\'' + '].value');
    var bProdutoServico = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ProdutoServico' + '\'' + '].value');
    var bProduto = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Produto' + '\'' + '].value');

    var nEstoqueID = 356;

	if (nMetodoCustoID != 371)
		nEstoqueID = 361;

    // N�o tem pedido de refer�ncia
    
    glb_PrecoEhDaReferencia = false;
    var nProdutoID = txtProdutoID.value;
    var nConceitoConcretoID;
    var nMarcaID;
    var sCaracteristica;
    var nValorLimite = txtValorLimite.value;
    var sPalavraChave = txtPalavraChave.value;

    var sFiltro = '';
    var sFiltroFROM = '';
    var sFiltroSELECTReferencia = '';
    var sFiltroFROMReferencia = '';
    var sFiltroReferencia = '';
	var sQuantidade = ' SPACE(0) AS Quantidade, ';
    var sOrdem = selOrdem.value;
    
    if(sOrdem == 'ProdutoID')
	{
		Familia(sPalavraChave);
		if ( !(dsoFamilia.recordset.BOF && dsoFamilia.recordset.EOF) )
		    glb_nFamiliaID = dsoFamilia.recordset['ProdutoID'].value;    
	}        

    if  ( nProdutoID != '' )
    {
        sTop = ' TOP 50 ';
        nProdutoID = parseInt(nProdutoID ,10);
        sFiltro += ' AND ProdutosEmpresa.ObjetoID>= ' + nProdutoID + ' ';
    }                

    if (selProduto.options.length == 0)
        nConceitoConcretoID = 0;
    else
        nConceitoConcretoID = selProduto.value;

    if (selMarca.options.length == 0)
        nMarcaID = 0;
    else
        nMarcaID = selMarca.value;

    if (selCaracteristica.options.length == 0)
        sCaracteristica = 0;
    else
        sCaracteristica = selCaracteristica.options[selCaracteristica.selectedIndex].innerText;

    if ( nConceitoConcretoID != 0 )
    {
        sFiltro = ' AND Produtos.ProdutoID = ' + nConceitoConcretoID;
    }        

    if ( nMarcaID != 0 )
    {
        sFiltro += ' AND Produtos.MarcaID = ' + nMarcaID; 
    }

    if ( sCaracteristica != 0)
    {
        sFiltro += ' AND Produtos.ConceitoID=Caracteristicas.ProdutoID AND dbo.fn_Produto_CaracteristicaOrdem(Caracteristicas.ProdutoID, Caracteristicas.CaracteristicaID)=1 AND ' +
                   'Caracteristicas.Valor LIKE ' + '\'' + '%'  + sCaracteristica + '%' + '\'' + ' '; 
        
        sFiltroFROM = ', Conceitos_Caracteristicas Caracteristicas WITH(NOLOCK) ';
    }

    if  ( nValorLimite != '' )
    {
        nValorLimite = parseInt(nValorLimite ,10);
        sFiltro += ' AND (CONVERT(NUMERIC(11,2), dbo.fn_Pedido_Item(' + glb_nPedidoID + ', Produtos.ConceitoID, GETDATE(), NULL, 1)* ' + (1+(glb_nVariacao/100)) + '))<= ' + nValorLimite;
    }                
    if  ( sPalavraChave != '' )
    {
        sFiltroFROM += ', Conceitos Marcas  WITH(NOLOCK), Conceitos Familias  WITH(NOLOCK), ' +
			'RelacoesConceitos RelConceitos2  WITH(NOLOCK), Conceitos Conceitos2  WITH(NOLOCK), RelacoesConceitos RelConceitos1  WITH(NOLOCK), Conceitos Conceitos1  WITH(NOLOCK) ';
        sFiltro += ' AND Produtos.MarcaID = Marcas.ConceitoID AND Produtos.ProdutoID = Familias.ConceitoID AND ' +
                   'Familias.EstadoID = 2 AND Familias.ConceitoID = RelConceitos2.SujeitoID AND ' +
				   'RelConceitos2.TipoRelacaoID = 41 AND RelConceitos2.EstadoID = 2 AND ' +
				   'RelConceitos2.ObjetoID = Conceitos2.ConceitoID AND Conceitos2.EstadoID = 2 AND ' +
				   'Conceitos2.ConceitoID = RelConceitos1.SujeitoID AND RelConceitos1.TipoRelacaoID = 41 AND ' +
				   'RelConceitos1.EstadoID = 2 AND RelConceitos1.ObjetoID = Conceitos1.ConceitoID AND ' + 
				   'Conceitos1.EstadoID = 2 AND ' +
                   '((Produtos.ConceitoID LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' +
                   ' (Produtos.Conceito LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' +
                   ' (dbo.fn_Tradutor(Familias.Conceito, ' + nIdiomaDeID + ', ' + nIdiomaParaID + ', NULL) LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' + 
                   ' (dbo.fn_Tradutor(Conceitos1.Conceito, ' + nIdiomaDeID + ', ' + nIdiomaParaID + ', NULL) LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' +
                   ' (dbo.fn_Tradutor(Conceitos2.Conceito, ' + nIdiomaDeID + ', ' + nIdiomaParaID + ', NULL) LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' +
                   ' (Marcas.Conceito LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' + 
                   ' (Produtos.Modelo LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' + 
                   ' (Produtos.Descricao LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' + 
                   ' (Produtos.PartNumber LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' )) '; 
    }                
    if  ( txtFiltro.value != '' )
    {
        sFiltro += ' AND (' + txtFiltro.value + ') ';
    }                

    // Estados
    var sFiltroEstados = ' ';    
    var bFirst = true;

    // loop nos checkbox de estados para montar a string de filtro
    for ( i=0; i<(divFields.children.length); i++ )
    {
        elem = divFields.children[i];
        
        if ( (elem.tagName).toUpperCase() == 'INPUT' )
        {
            if ( ((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked) )
            {
                if (bFirst)
                {
                    bFirst=false;
                    sFiltroEstados = ' AND (ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                }                    
                else 
                {
                    sFiltroEstados += ' OR ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                }
            }
        }
        //Caso seja umas das transacoes de troca, retorna tamb�m os produtos desativos.
        if (glb_nTransacaoID == 522 || glb_nTransacaoID == 526 || glb_nTransacaoID == 511 || glb_nTransacaoID == 512 || glb_nTransacaoID == 515 || glb_nTransacaoID == 831 || glb_nTransacaoID == 835)
        {
            sFiltroEstados += ' OR ProdutosEmpresa.EstadoID=4 ';
        }
    }

    sFiltroEstados += ')';

    var sValor = '';
    var sPrecoUnitario = 'NULL';
  
    var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[\'TransacaoID\'].value');
    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[\'EmpresaID\'].value');
    var nMatrizFilial = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[\'MatrizFilial\'].value');
    var bEhCliente = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[\'EhCliente\'].value');
    var bImpostosIncidencia = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ImpostosIncidencia\'].value');
    var bFopagEmpresas = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'FopagEmpresas\'].value');
    /*
		strSQLSelect2 =	'(CONVERT(INT, dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, 2))) AS MoedaID, ' + 
			'(CONVERT(NUMERIC(15,7), dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, 3))) AS TaxaMoeda, ' +
			'(CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, 5))) AS Imposto1, ' +
			'(CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, 6))) AS Imposto2,' +
			sImposto2Base + ' AS Imposto2Base,' +
     	    'ProdutosEmpresa.Observacao AS Observacao, NULL AS PedidoReferencia ' + sFiltroSELECTReferencia + ', ' +
     	    'NULL AS  FinalidadeID,' +
     	    'NULL AS ValorInterno, ' +
     	    'NULL AS ValorRevenda, ' +
     	    'dbo.fn_Pedido_ProdutoValorUnitario(' + glb_nPedidoID + ', ProdutosEmpresa.ObjetoID, NULL, ' + sValor + ') AS ValorUnitario, ' +
     	    'dbo.fn_Pedido_ProdutoValorUnitario(' + glb_nPedidoID + ', ProdutosEmpresa.ObjetoID, NULL, ' + sPrecoTabela + ') AS ValorTabela, ' +
     	    'dbo.fn_Produto_PPB(Produtos.ConceitoID, ' + nEmpresaData[4] + ', NULL) AS TemPPB, NULL AS PedItemEncomendaID, NULL AS EhEncomenda ';

		// Adicionado tipo de compra para validar fornecedor no pes/con. 14/11/2013
		if (((glb_nTipoPessoa < 0) || (glb_nTipoPessoa == 334 && glb_nTransacaoID == 111 && bFopagEmpresas.indexOf(glb_nPessoaID.toString()) < 0)) 
		    && (!((sPalavraChave == '17012') || (sPalavraChave == '19222') || (sPalavraChave == '21043'))))
		{
			sFiltroFROM += ',RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK) ';
			sFiltroFornecedor = 'ProdutosEmpresa.RelacaoID=Fornecedores.RelacaoID AND Fornecedores.FornecedorID=' + glb_nPessoaID + ' AND ';
		}

		strSQLFrom = 'FROM ' +
		    'RelacoesPesCon ProdutosEmpresa  WITH(NOLOCK), Conceitos Produtos  WITH(NOLOCK), Recursos Estados  WITH(NOLOCK) ' + sFiltroFROMReferencia + sFiltroFROM + ' ';

		strSQLWhere = 'WHERE ' +
		    '(ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
		    sFiltroFornecedor +
		    'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID';
		    
        //Verifica se filtra somente por produtos ativos: LFS 17/05/2011
        if (!glb_produtoDesativo){
           strSQLWhere += ' AND Produtos.EstadoID=2 ';
        }
		    
        strSQLWhere += ' AND ProdutosEmpresa.EstadoID=Estados.RecursoID ' + sFiltro + ' ' + sFiltroEstados + ' ' + sFiltroReferencia + ' ) ' + 
		    'ORDER BY ' + sOrdem;
    */
    if((bEhCliente == 'true') || (bEhCliente == '1'))
        bEhCliente = 1;                
    else
        bEhCliente = 0;
    
    if((bImpostosIncidencia == 'true') || (bImpostosIncidencia == '1'))
        bImpostosIncidencia = 1;                
    else
        bImpostosIncidencia = 0;

    //SAS0075-Troca - O valor � sugerido ao usu�rio atraves de uma fn que se baseia na transa��o. SGP 04/04/2014
    sValor = 'dbo.fn_Produto_ValorUnitario(Produtos.ConceitoID, ' + glb_nPedidoID + ', NULL, NULL, NULL, NULL)';

    sImposto2Base = '(CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, -6)))';
    var sPrecoTabela = '(CONVERT(NUMERIC(11,2), dbo.fn_Pedido_Item(' + glb_nPedidoID + ', Produtos.ConceitoID, GETDATE(), NULL, 1)))';
    
    strSQLSelect = 'SELECT DISTINCT ' + sTop +
		'dbo.fn_Produto_Descricao2(Produtos.ConceitoID, NULL, 12) AS Produto, Produtos.ConceitoID AS ProdutoID, ' +
	    'Estados.RecursoAbreviado AS Estado, ' +
    	'(CONVERT(INT, dbo.fn_Pedido_Item(' + glb_nPedidoID + ', Produtos.ConceitoID, GETDATE(), NULL, 4))) AS GrupoImpostoID, ' +
		'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID, ProdutosEmpresa.ObjetoID, ' + nEstoqueID + ', NULL, NULL, NULL, NULL, NULL) AS Estoque, ' +
    	sQuantidade + ' NULL AS PrecoInterno, NULL AS PrecoRevenda,' + sValor + ' AS PrecoUnitario, ';

	strSQLSelect2 =	'(CONVERT(INT, dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, 2))) AS MoedaID, ' + 
		'(CONVERT(NUMERIC(15,7), dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, 3))) AS TaxaMoeda, ' +
		'(CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, 5))) AS Imposto1, ' +
		'(CONVERT(NUMERIC(5,2), dbo.fn_Pedido_Item(' + glb_nPedidoID + ',Produtos.ConceitoID, GETDATE(), NULL, 6))) AS Imposto2,' +
		sImposto2Base + ' AS Imposto2Base,' +
 	    'ProdutosEmpresa.Observacao AS Observacao, NULL AS PedidoReferencia ' + sFiltroSELECTReferencia + ', ' +
 	    'NULL AS  FinalidadeID,' +
 	    'NULL AS ValorInterno, ' +
 	    'NULL AS ValorRevenda, ' +
 	     //sValor + ' AS ValorUnitario, ' +
 	     //sValor + ' AS ValorTabela, ' +
 	    'dbo.fn_Produto_PPB(Produtos.ConceitoID, ' + nEmpresaData[4] + ', NULL) AS TemPPB, NULL AS PedItemEncomendaID, NULL AS EhEncomenda, ';

	strSQLSelectServicos = 'ProdutosEmpresa.ServicoID AS ConServicoID, NULL AS dtVigenciaServicoInicio, NULL AS dtVigenciaServicoFim ';  /*' ( SELECT TOP 1 aa.ConServicoID ' +
		                        ' FROM dbo.Conceitos_Servicos aa WITH(NOLOCK) ' +
		                        'WHERE aa.ConceitoID = ProdutosEmpresa.CodigoListaServicoID AND aa.LocalidadeID = dbo.fn_Pessoa_Localidade(' + glb_nEmpresaID + ', 3, NULL, NULL) ' + 
		                            'AND aa.AliquotaISS IS NOT NULL ' +
                                    ') AS ConServicoID ';*/

	// Adicionado tipo de compra para validar fornecedor no pes/con. 14/11/2013
	// SAS0075 - Quando for remessa para Fornecedor Indireto deve liberar todos os itens, independente do Fornecedor.
	if (((glb_nTipoPessoa < 0) || (glb_nTipoPessoa == 334 && glb_nTransacaoID == 111 && bFopagEmpresas.indexOf(escape(glb_nPessoaID)) < 0))
	    && (!((sPalavraChave == '17012') || (sPalavraChave == '19222') || (sPalavraChave == '21043'))) && (glb_nTransacaoID != 526))	        
	{
		sFiltroFROM += ',RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK) ';
		sFiltroFornecedor = 'ProdutosEmpresa.RelacaoID=Fornecedores.RelacaoID AND Fornecedores.FornecedorID=' + glb_nPessoaID + ' AND ';
	}

	strSQLFrom = 'FROM ' +
	    'RelacoesPesCon ProdutosEmpresa  WITH(NOLOCK), Conceitos Produtos  WITH(NOLOCK), Recursos Estados  WITH(NOLOCK) ' + sFiltroFROMReferencia + sFiltroFROM + ' ';

	strSQLWhere = 'WHERE ' +
	    '(ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
	    sFiltroFornecedor +
	    'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID';

	//Transa��es que sejam Servi�o = 1 e ServicoProduto = 1, mostram apenas produtos classificados como servi�os
	if (!bProduto)
	    strSQLWhere += ' AND Produtos.ClassificacaoID <> 311 ';

	if (!bProdutoServico)
	    strSQLWhere += ' AND Produtos.ClassificacaoID <> 315 ';

	strSQLWhere += ' AND ProdutosEmpresa.EstadoID=Estados.RecursoID ' + sFiltro + ' ' + sFiltroEstados + ' ' + sFiltroReferencia + ' ) ' +
	    'ORDER BY ' + sOrdem;

        //Comentado trecho que acrescenta IPI no valor unitario do Item.
        sValorUnitario = 'Itens.ValorUnitario  * (CASE WHEN (ISNULL((SELECT PedAtual.TransacaoID '+
                                                            'FROM Pedidos PedAtual WITH(NOLOCK) '+
                                                            'WHERE PedAtual.PedidoID = ' + glb_nPedidoID + '),0) <> 316 AND 1 = ' + nTemNF + ') '+
                    'THEN ' +
				        '(1 + (SELECT TOP 1 CONVERT(NUMERIC(7,4), ISNULL(dbo.fn_DivideZero(SUM(ItensImposto2.ValorImposto), '+
				                                                                '(Itens.ValorUnitario * Itens.Quantidade)),0)) ' + 
				                'FROM Pedidos_Itens_Impostos ItensImposto2 WITH(NOLOCK) ' +
				                       ' INNER JOIN  Conceitos Impostos2 WITH(NOLOCK) ON ItensImposto2.ImpostoID = Impostos2.ConceitoID ' +
				                'WHERE Itens.PedItemID=ItensImposto2.PedItemID AND Impostos2.IncidePreco = 1 ' + 
				                    'AND Impostos2.InclusoPreco = 0 AND Impostos2.DestacaNota = 1)) ELSE 1 END)';

        sEstoque = 'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID, ProdutosEmpresa.ObjetoID, ' + nEstoqueID + ', NULL, NULL, NULL, 375, NULL)';
        
        sImposto1 = '(CASE WHEN 1 = ' + nTemNF + 
                        ' THEN '+ 
    setConnection(dsoGen01);

    dsoGen01.SQL = strSQLSelect + strSQLSelect2 + strSQLSelectServicos + strSQLFrom + strSQLWhere;
    dsoGen01.ondatasetcomplete = fillgridData_DSC;
    try
    {
        dsoGen01.Refresh();
    }
    catch(e)
    {
        if ( window.top.overflyGen.Alert('Filtro Inv�lido.') == 0 )
            return null;
            
        lockControlsInModalWin(false);
        
        fg.Rows = 1;
        btnOK.disabled = true;
        
        if (txtFiltro.disabled == false)
        {
            window.focus();
            txtFiltro.focus();
        }    
        return null;
    }
}

function fillgridData_DSC()
{
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    var i = 0;
    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'Resultado\'].value');
    
    var aHoldCols = new Array();
    
    var nArray = [['Produto', 'Produto*', '', '', false, false],
                       ['ID', 'ProdutoID*', '', '', false, true],
                       ['Est', 'Estado*', '', '', false, false],
                       ['Estoque', 'Estoque*', '', '', false, true],
                       ['Quant', 'Quantidade', '', '', false, true],
                       ['Valor Int', 'ValorInterno', '', '', true, false],
                       ['Valor Rev', 'ValorRevenda', '', '', true, false],
                       ['Valor Unit', 'PrecoUnitario', '', '', true, false],
                       ['Pre�o Int', colunasPrecoReadOnly('PrecoInterno'), '9999999999.99', '###,###,##0.00', !(bResultado), true],
                       ['Pre�o Rev', colunasPrecoReadOnly('PrecoRevenda'), '9999999999.99', '###,###,##0.00', !(bResultado), true],
                       ['Pre�o Unit', colunasPrecoReadOnly('PrecoUnitario'), '9999999999.9999', '###,###,##0.0000', false, true],
                       ['ValorTabela', 'PrecoUnitario', '', '', true, false],
                       ['Valor Total', '_calc_ValorTotal_10*', '9999999999.99', '###,###,##0.00', false, true],
                       //['I1', 'Imposto1', '999.99', '##0.00', false, true],
                       //['I2', 'Imposto2', '999.99', '##0.00', false, true],
                       ['_calc_HoldKey_1', '_calc_HoldKey_1', '9999999999.99', '###,###,##0.00', true, false],
                       ['In�cio Vig�ncia', 'dtVigenciaServicoInicio', '99/99/9999', dTFormat, (glb_nTransacaoID == 215) ? false : true],
                       ['Fim Vig�ncia', 'dtVigenciaServicoFim', '99/99/9999', dTFormat, (glb_nTransacaoID == 215) ? false : true],
                       ['Observa��o', 'Observacao*', '', '', false, false],                       
                       ['MoedaID', 'MoedaID', '', '', true, false],
                       ['TaxaMoeda', 'TaxaMoeda', '', '', true, false],
                       ['GrupoImpostoID', 'GrupoImpostoID', '', '', true, false],
                       ['PedidoReferencia', 'PedidoReferencia', '', '', true, false],
                       ['Imposto2Base', 'Imposto2Base', '', '', true, false],                       
                       ['FinalidadeID', 'FinalidadeID', '', '', true, false],
                       ['TemPPB', 'TemPPB', '', '', true, false],
                       ['PedItemEncomendaID', 'PedItemEncomendaID', '', '', true, false],
                       ['EhEncomenda', 'EhEncomenda', '', '', true, false],
                       ['ConServicoID', 'ConServicoID', '', '', true, false]];

    headerGrid(fg, aReturnFieldsToGrid(nArray, 0), aReturnFieldsToGrid(nArray, 4));
    
    fillGridMask(fg, dsoGen01, aReturnFieldsToGrid(nArray, 1), aReturnFieldsToGrid(nArray, 2), aReturnFieldsToGrid(nArray, 3));

    fg.ColKey(7) = 'ValorUnitario';
    fg.ColKey(11) = 'ValorTabela';

    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade');
    var nColPrecoInterno = getColIndexByColKey(fg, colunasPrecoReadOnly('PrecoInterno'));
    var nColPrecoRevenda = getColIndexByColKey(fg, colunasPrecoReadOnly('PrecoRevenda'));
    var nColPrecoUnitario = getColIndexByColKey(fg, colunasPrecoReadOnly('PrecoUnitario'));
    var nColValor = getColIndexByColKey(fg, 'Valor');
    var nColCalcValorTotal = getColIndexByColKey(fg, '_calc_ValorTotal_10*');
    var nColValorInterno = getColIndexByColKey(fg, 'ValorInterno');
    var nColValorRevenda = getColIndexByColKey(fg, 'ValorRevenda');
    var nColValorUnitario = getColIndexByColKey(fg, 'ValorUnitario');
    
    //adiciona linha de totais no grid.
    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[nColQuantidade, '#######', 'S'], [nColCalcValorTotal, '##,###,###,###.00', 'S']]);

    //Preenche colular valor total e linha de totais.
    setTotaisGrid();

    //alinha colunas a direita de acordo com a defini��o do nArray.
    alignColsInGrid(fg, aReturnFieldsToGrid(nArray, 5));

    //Preenche pre�o interno e revenda igual ao preco unitario quando os mesmo vierem zerados.
    if (bResultado) 
    {
        for (i = 2; i < fg.Rows; i++) 
        {
            if (fg.ValueMatrix(i, nColPrecoRevenda) == 0)
                fg.TextMatrix(i, nColPrecoRevenda) = fg.ValueMatrix(i, nColPrecoUnitario);

            if (fg.ValueMatrix(i, nColPrecoInterno) == 0)
                fg.TextMatrix(i, nColPrecoInterno) = fg.ValueMatrix(i, nColPrecoRevenda);
                
            if (fg.ValueMatrix(i, nColValorRevenda) == 0)
                fg.TextMatrix(i, nColValorRevenda) = fg.ValueMatrix(i, nColValorUnitario);

            if (fg.ValueMatrix(i, nColValorInterno) == 0)
                fg.TextMatrix(i, nColValorInterno) = fg.ValueMatrix(i, nColValorRevenda);                
        }
    }

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    //fg.ColWidth(5) = FONT_WIDTH * 11 * 16;
    
    fg.FrozenCols = 2;
    
    fg.Redraw = 2;
    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if ( fg.Rows > 1 )
        fg.Col = nColQuantidade;
    
    if (fg.Rows > 1)
        fg.Editable = true;

    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
        btnOK.disabled = false;
    else
        btnOK.disabled = true;    
    
    // se tem linhas no grid, coloca foco no grid, caso contrario
    // coloca foco no txtPalavraChave
    if (fg.Rows > 1)
            fg.focus();
    else
        txtPalavraChave.focus();

}

function saveItens(aData, aDataLen, aDataElemLen)
{
	var strPars = '';
	var nBytesAcum = 0;
	var nDataLen = 0;
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
	var nLenExt = 0;                                  
	
	lockControlsInModalWin(true);
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;

    lockControlsInModalWin(true);

    for (i=0; i<aDataLen; i++)
    {
		nBytesAcum=strPars.length;		
		
		if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
		{
			nBytesAcum = 0;
			if (strPars != '')
			{
				glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + 
					'&nDataLen=' + escape(nLenExt);
				strPars = '';
				nLenExt = 0;
				nDataLen = 0;
			}

			strPars = '?';
			strPars += 'nPedidoID=' + escape(glb_nPedidoID);
			strPars += '&nEstadoID=' + escape(nEstadoID);
			strPars += '&nVariacao=' + escape(glb_nVariacao);
			strPars += '&nDataElemLen=' + escape(aDataElemLen);			
		}
		nLenExt++;
		nDataLen++;

		for (j = 0; j < aDataElemLen; j++)
		    strPars += '&aData= ' + i.toString() + '/' + escape(aData[i][j]);
		
	}

	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nLenExt);

	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
		    dsoGen03.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/saveitemspedido.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGen03.ondatasetcomplete = sendDataToServer_DSC;
			dsoGen03.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_callFillGridTimerInt = window.setInterval('saveItens_DSC()', 10, 'JavaScript');
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		return null;
	}
}

function sendDataToServer_DSC()
{
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');

	if ((dsoGen03.recordset['Resultado'].value != null) && (dsoGen03.recordset['Resultado'].value != '') && (isNaN(dsoGen03.recordset['Resultado'].value))) {
	    if (window.top.overflyGen.Alert(dsoGen03.recordset['Resultado'].value) == 0)
	        return null;
	}
}

function saveItens_DSC()
{

    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade');
    
	if (glb_callFillGridTimerInt != null)
	{
		window.clearInterval(glb_callFillGridTimerInt);
		glb_callFillGridTimerInt = null;
	}

    var i;
    
    if ( !(dsoGen03.recordset.BOF && dsoGen03.recordset.EOF) )
    {
        // Outro usuario mudou o estadoID do pedido para diferente de cotacao
        if (dsoGen03.recordset['fldErrorNumber'].value > 50000)
        {
            if ( window.top.overflyGen.Alert (dsoGen03.recordset['fldErrorText'].value) == 0 )
                return null;
                
            lockControlsInModalWin(false);       
            window.focus();
            fg.focus();    
            return null;
        }
    }
    
    // pedido foi alterado
    glb_pedidoAlterado = true;
    
    for ( i=2; i<fg.Rows; i++ )
    {
        if (!((isNaN(parseFloat(fg.TextMatrix(i, nColQuantidade)))) ||
                (parseFloat(fg.TextMatrix(i, nColQuantidade)) == 0)))
            glb_nQuantidadeItens++;

        fg.TextMatrix(i, nColQuantidade) = '';
    }        

	setTotaisGrid(0);
    lockControlsInModalWin(false);       
    window.focus();
    fg.focus();
}

// Funcao de carregamento de combos
function fillCmbsData()
{
    var strSQL;
    var strSQLSelect1 = '';
    var strSQLSelect2 = '';
    var sFiltroFROM = '';
    var sFiltroFornecedor = '';
    var nEstadoID;

    var nEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'getCurrEmpresaData()');
	var nIdiomaDeID = nEmpresaData[7];
	var nIdiomaParaID = nEmpresaData[8];
    
	if ((glb_nTipoPessoa < 0) && (glb_nTransacaoID != 526))
	{
		sFiltroFROM = ',RelacoesPesCon_Fornecedores Fornecedores ';
		nEstadoID = 11;
		sFiltroFornecedor = 'ProdutosEmpresa.RelacaoID=Fornecedores.RelacaoID AND Fornecedores.FornecedorID=' + glb_nPessoaID + ' AND ';
	}
	else
		nEstadoID = 12;

	if ((!glb_Produto)&&(!glb_Marca))
	{
		strSQLSelect1 = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +   
			'UNION ALL ' +
				'SELECT DISTINCT 0 AS Indice, Familias.ConceitoID AS fldID, ' +
					'dbo.fn_Tradutor(Familias.Conceito, ' + nIdiomaDeID + ', ' + nIdiomaParaID + ', NULL) AS fldName ' +
					'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK) ' + sFiltroFROM +
					'WHERE ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
						'(ProdutosEmpresa.EstadoID=2 OR ProdutosEmpresa.EstadoID BETWEEN ' + nEstadoID + ' AND 15) AND ' + sFiltroFornecedor +
						'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND Produtos.EstadoID=2 AND ' +
						'Produtos.ProdutoID=Familias.ConceitoID ';

		strSQLSelect2 =	'UNION ALL ' +
				'SELECT 1 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +   
			'UNION ALL ' + 
				'SELECT DISTINCT 1 AS Indice, Marcas.ConceitoID AS fldID, Marcas.Conceito AS fldName ' +
					'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK) ' + sFiltroFROM + 
					'WHERE ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
						'(ProdutosEmpresa.EstadoID=2 OR ProdutosEmpresa.EstadoID BETWEEN ' + nEstadoID + ' AND 15) AND ' + sFiltroFornecedor +
						'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND Produtos.EstadoID=2 AND ' +
						'Produtos.MarcaID=Marcas.ConceitoID ' +
			'ORDER BY Indice, fldName';
	}
    else if (glb_Produto)
    {
        strSQLSelect1 = 'SELECT 1 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +   
	        'UNION ALL ' +
				'SELECT DISTINCT 1 AS Indice, Marcas.ConceitoID AS fldID, Marcas.Conceito AS fldName ' +
					'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK) ' + sFiltroFROM + 
					'WHERE ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
						'(ProdutosEmpresa.EstadoID=2 OR ProdutosEmpresa.EstadoID BETWEEN ' + nEstadoID + ' AND 15) AND ' + sFiltroFornecedor +
						'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND Produtos.EstadoID=2 AND ' +
						'Produtos.MarcaID=Marcas.ConceitoID AND Produtos.ProdutoID = ' + selProduto.value + ' ';

		strSQLSelect2 =	'UNION ALL ' +
				'SELECT 2 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +
		    'UNION ALL ' +
		        'SELECT DISTINCT 2 AS Indice, Caracteristicas.CaracteristicaID AS fldID, Caracteristicas.Valor AS fldName ' +
					'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos_Caracteristicas Caracteristicas WITH(NOLOCK) ' + sFiltroFROM +
					'WHERE ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
						'(ProdutosEmpresa.EstadoID=2 OR ProdutosEmpresa.EstadoID BETWEEN ' + nEstadoID + ' AND 15) AND ' + sFiltroFornecedor +
						'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND Produtos.EstadoID=2 AND ' +
						'Produtos.ConceitoID=Caracteristicas.ProdutoID AND dbo.fn_Produto_CaracteristicaOrdem(Caracteristicas.ProdutoID, Caracteristicas.CaracteristicaID) = 1 AND Produtos.ProdutoID = ' + selProduto.value + ' ' +
		    'ORDER BY Indice, fldName';

		strSQL = strSQLSelect1 + strSQLSelect2;
    }
	else if (glb_Marca)
	{
        // So os Produtos
        if (selProduto.value == 0)
        {
			strSQLSelect1 = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +   
				'UNION ALL ' +
					'SELECT DISTINCT 0 AS Indice, Familias.ConceitoID AS fldID, dbo.fn_Tradutor(Familias.Conceito, ' + nIdiomaDeID + ', ' + nIdiomaParaID + ', NULL) AS fldName ' +
						'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK) ' + sFiltroFROM +
						'WHERE ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
							'(ProdutosEmpresa.EstadoID=2 OR ProdutosEmpresa.EstadoID BETWEEN ' + nEstadoID + ' AND 15) AND ' + sFiltroFornecedor +
							'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND Produtos.EstadoID=2 AND ' +
							'Produtos.ProdutoID=Familias.ConceitoID AND Produtos.MarcaID = ' + selMarca.value + ' ' +
                'ORDER BY Indice, fldName';
        }
        //So as caracteristicas
        else
        {
			strSQLSelect1 =	'SELECT 2 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +   
			    'UNION ALL ' +
			        'SELECT DISTINCT 2 AS Indice, Caracteristicas.CaracteristicaID AS fldID, Caracteristicas.Valor AS fldName ' +
						'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos_Caracteristicas Caracteristicas WITH(NOLOCK) ' + sFiltroFROM + 
						'WHERE ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
							'(ProdutosEmpresa.EstadoID=2 OR ProdutosEmpresa.EstadoID BETWEEN ' + nEstadoID + ' AND 15) AND ' + sFiltroFornecedor +
							'ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND Produtos.EstadoID=2 AND Produtos.MarcaID = ' + selMarca.value + ' AND ' +
							'Produtos.ConceitoID=Caracteristicas.ProdutoID AND dbo.fn_Produto_CaracteristicaOrdem(Caracteristicas.ProdutoID, Caracteristicas.CaracteristicaID) = 1 AND Produtos.ProdutoID = ' + selProduto.value + ' ' +                     
			    'ORDER BY Indice, fldName';
       }
	}
    
	strSQL = strSQLSelect1 + strSQLSelect2;

    setConnection(dsoGen02);
    dsoGen02.SQL = strSQL;
    dsoGen02.ondatasetcomplete = fillCmbsData_DSC;
    dsoGen02.Refresh();
}

//Funcao de retorno da funcao fillCmbsData()
function fillCmbsData_DSC()
{
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selProduto,selMarca,selCaracteristica];
    
    if (glb_Produto)
    {
        clearComboEx(['selMarca','selCaracteristica']);
    }
    
    if (glb_Marca)
    {
        if (selProduto.value == 0)
            clearComboEx(['selProduto','selCaracteristica']);
        else
            clearComboEx(['selCaracteristica']);
    }
    
    if ((!glb_Produto)&&(!glb_Marca))
    {
        clearComboEx(['selProduto','selMarca','selCaracteristica']);
    }    
    
    while (! dsoGen02.recordset.EOF )
    {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoGen02.recordset['fldName'].value;
        oOption.value = dsoGen02.recordset['fldID'].value;
        aCmbsDynamics[dsoGen02.recordset['Indice'].value].add(oOption);
        dsoGen02.recordset.MoveNext();
    }

    lockControlsInModalWin(false);

    if ( selProduto.options.length > 0 )
        selProduto.disabled = false;
    else
        selProduto.disabled = true;
        
    if ( selMarca.options.length > 0 )        
        selMarca.disabled = false;
    else
        selMarca.disabled = true;

    if ( selCaracteristica.options.length > 0 )        
        selCaracteristica.disabled = false;
    else
        selCaracteristica.disabled = true;

    // mostra a janela se for o caso
    if ( modalincluiitensBody.currentStyle.visibility == 'hidden' )   
        showModalAfterDataLoad();

}

function selProdutoChanged()
{
    if (this.value == 0)
    {
        selCaracteristica.disabled = true;
        clearComboEx(['selCaracteristica']);
 
        if ( glb_Produto == true )
        {
            glb_Produto = false;
            // combo de marca
            fillCmbsData();
        }
    }
    else
    {
        if ( glb_Produto == true )
        {
            // combo de marca e combo de caracteristica
            fillCmbsData();    
        }
        else
        {
            if ( glb_Marca == true )
            {
                // combo de caracteristica
                fillCmbsData();
            }
            else
            {
                glb_Produto = true;
                // combo de marca e combo de caracteristica
                fillCmbsData();
            }
        }
    }
}

function selMarcaChanged()
{
    if (this.value == 0)
    {
        if ( glb_Marca == true )
        {
            selCaracteristica.disabled = true;
            clearComboEx(['selCaracteristica']);
            
            glb_Marca = false;
            // combo de produto
            fillCmbsData();    
        }
    }
    else
    {
        if ( glb_Marca == true )
        {
            selCaracteristica.disabled = true;
            clearComboEx(['selCaracteristica']);
 
            // combo de produto
            fillCmbsData();    
        }
        else
        {
            if ( glb_Produto == false )
            {
                selCaracteristica.disabled = true;
                clearComboEx(['selCaracteristica']);
 
                glb_Marca = true;
                // combo de produto
                fillCmbsData();
            }
        }
    }
}

function Familia(pConceitoID)
{
           
            //'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value'
            var strSQLFamilia = 'SELECT ProdutoID FROM Conceitos WITH(NOLOCK) WHERE CONVERT(VARCHAR, ConceitoID) = ' + '\'' + pConceitoID  + '\'';
            setConnection(dsoFamilia);
            dsoFamilia.SQL =  strSQLFamilia;
            dsoFamilia.ondatasetcomplete = Familia_DSC;
            dsoFamilia.refresh();
            
}
       
function Familia_DSC()
{
    return null;
}

function Gravacao(pConceitoID)
{
    var strSQL = 'SELECT * FROM Conceitos WITH(NOLOCK) WHERE ConceitoID = ' + pConceitoID + '';
    setConnection(dsoGravacao);
    dsoGravacao.SQL = strSQL;
    dsoGravacao.ondatasetcomplete = Gravacao_DSC;
    dsoGravacao.refresh();
}

function Gravacao_DSC(){
    return null;
}

function aReturnFieldsToGrid(nArray, nindex) 
{
    var i = 0;
    var aNewArray = new Array();

    for (i = 0; i < nArray.length; i++) 
    {
        if (nindex == 4 || nindex == 5) 
        {
            if (nArray[i][nindex] == true)
                aNewArray[aNewArray.length] = i;
        }
        else
            aNewArray[aNewArray.length] = nArray[i][nindex];
    }

    return aNewArray;
}

/*function atualizaValorUnitario(nRow) 
{
    var nPrecoInterno = fg.ValueMatrix(nRow, getColIndexByColKey(fg, colunasPrecoReadOnly('PrecoInterno')));
    var nPrecoRevenda = fg.ValueMatrix(nRow, getColIndexByColKey(fg, colunasPrecoReadOnly('PrecoRevenda')));
    var nPrecoUnitario = fg.ValueMatrix(nRow, getColIndexByColKey(fg, colunasPrecoReadOnly('PrecoUnitario')));

    var nFinalidade = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'FinalidadeID'));
    var nProdutoID = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'ProdutoID*'));

    lockControlsInModalWin(true);

    nFinalidade = (nFinalidade == 0 ? 'NULL' : nFinalidade);
    
    setConnection(dsoAtualizaValorUnitario);
    dsoAtualizaValorUnitario.SQL = 'SELECT ' + nRow + ' AS Linha, ' +
                                        'dbo.fn_Pedido_ProdutoValorUnitario (' + glb_nPedidoID + ',' + nProdutoID + ',' + nFinalidade + ',' + nPrecoInterno + ') AS ValorInterno, ' +
                                        'dbo.fn_Pedido_ProdutoValorUnitario (' + glb_nPedidoID + ',' + nProdutoID + ',' + nFinalidade + ',' + nPrecoRevenda + ') AS ValorRevenda, ' +
                                        'dbo.fn_Pedido_ProdutoValorUnitario (' + glb_nPedidoID + ',' + nProdutoID + ',' + nFinalidade + ',' + nPrecoUnitario + ') AS ValorUnitario';
    dsoAtualizaValorUnitario.ondatasetcomplete = atualizaValorUnitario_DSC;
    dsoAtualizaValorUnitario.Refresh();
}

function atualizaValorUnitario_DSC() 
{
    if (!(dsoAtualizaValorUnitario.recordset.BOF && dsoAtualizaValorUnitario.recordset.EOF)) 
    {
        var nRow = dsoAtualizaValorUnitario.recordset['Linha'].value;
        var nValorInterno = dsoAtualizaValorUnitario.recordset['ValorInterno'].value;
        var nValorRevenda = dsoAtualizaValorUnitario.recordset['ValorRevenda'].value;
        var nValorUnitario = dsoAtualizaValorUnitario.recordset['ValorUnitario'].value;

        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ValorInterno')) = nValorInterno;
        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ValorRevenda')) = nValorRevenda;
        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ValorUnitario')) = nValorUnitario;
    }
    
    lockControlsInModalWin(false);
}*/

function colunasPrecoReadOnly(coluna) {

    if (glb_nTransacaoID == 112)
        return coluna + '*';
    else
        return coluna;
}
//function verificaItem(nValidaProdutoID)
//{
//    var strSQL_ItemCompativel = 'SELECT CONVERT(BIT, dbo.fn_Pedido_Vinculado(' + glb_nPedidoID + ', ' + nValidaProdutoID.toString() + ', NULL, NULL, 6)) AS Compativel';
//    setConnection(dsoVerificaItem);
//    dsoVerificaItem.SQL = strSQL_ItemCompativel;
//    dsoVerificaItem.ondatasetcomplete = verificaItem_DSC;
//    dsoVerificaItem.refresh();
//}

//function verificaItem_DSC()
//{
//    return null;

//    /*if ( !(dsoVerificaItem.recordset.BOF && dsoVerificaItem.recordset.EOF) )
//    {
//    if (!dsoVerificaItem.recordset('Compativel').Value)
//    {
//    lockControlsInModalWin(false);       
//    window.focus();
//    fg.focus();    
//    return false;
//    }
//    else
//    {
//    return true;
//    }
//    }*/
//}

function dateAdd(p_Interval, p_Number, p_Date) {

    /*
    //Ajuste string para formato americano.
    if (aEmpresa[1] == 130) {
        var dia = p_Date.substring(0, 2)
        var mes = p_Date.substring(3, 5)
        var ano = p_Date.substring(6, 10)

        p_Date = mes + '/' + dia + '/' + ano
    }
    */

    /*if (DATE_FORMAT == "DD/MM/YYYY")
        putDateInMMDDYYYY2(p_Date);*/

    p_Date = normalizeDate_DateTime(p_Date, (DATE_FORMAT == "DD/MM/YYYY" ? true : false));

    if (!new Date(p_Date)) { return "invalid date: '" + p_Date + "'"; }
    if (isNaN(p_Number)) { return "invalid number: '" + p_Number + "'"; }

    p_Number = new Number(p_Number);
    var dt = new Date(p_Date);

    switch (p_Interval.toLowerCase()) {
        case "yyyy": {
            dt.setFullYear(dt.getFullYear() + p_Number);
            break;
        }
        case "q": {
            dt.setMonth(dt.getMonth() + (p_Number * 3));
            break;
        }
        case "m": {
            dt.setMonth(dt.getMonth() + p_Number);
            break;
        }
        case "y":			// day of year
        case "d":			// day
        case "w": {		// weekday
            dt.setDate(dt.getDate() + p_Number);
            break;
        }
        case "ww": {	// week of year
            dt.setDate(dt.getDate() + (p_Number * 7));
            break;
        }
        case "h": {
            dt.setHours(dt.getHours() + p_Number);
            break;
        }
        case "n": {		// minute
            dt.setMinutes(dt.getMinutes() + p_Number);
            break;
        }
        case "s": {
            dt.setSeconds(dt.getSeconds() + p_Number);
            break;
        }
        case "ms": {	// JS extension
            dt.setMilliseconds(dt.getMilliseconds() + p_Number);
            break;
        }
        default: {
            return "invalid interval: '" + p_Interval + "'";
        }
    }

    if (DATE_FORMAT == "DD/MM/YYYY")
        return dataFormatada(dt, 1);
    else
        return dataFormatada(dt, 2);
}

function dataFormatada(data, tipo) {
    var dia = data.getDate();
    if (dia.toString().length == 1)
        dia = "0" + dia;
    var mes = data.getMonth() + 1;
    if (mes.toString().length == 1)
        mes = "0" + mes;
    var ano = data.getFullYear();

    if (tipo == 1)
        return dia + "/" + mes + "/" + ano;
    else if (tipo == 2)
        return mes + "/" + dia + "/" + ano;
}


//Valida datas
function verificaData(sData) {
    var sData = trimStr(sData);
    var bDataIsValid = true;

    if (sData != '')
        bDataIsValid = chkDataEx(sData);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        return false;
    }
    return true;
}

//BETWEEN entre datas
function beweenDate(sdtStart, sdtEnd, sType) {
    sdtStart = new Date(sdtStart);
    sdtEnd = new Date(sdtEnd);
    var dr = moment.range(sdtStart, sdtEnd);

    if (sType == 1)
        return dr.diff('months');
    else if (sType == 2)
        return dr.diff('days');
}