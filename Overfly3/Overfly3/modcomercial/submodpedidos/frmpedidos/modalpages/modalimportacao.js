/********************************************************************
modalparcelasdiversas.js

Library javascript para o modalimportacao.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nUserID = 0;
var glb_nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');
var glb_empresaData = getCurrEmpresaData();

var dsofillFields = new CDatatransport('dsofillFields');
var dsotaxaPTAX = new CDatatransport('dsotaxaPTAX');
var dsoGravacao = new CDatatransport('dsoGravacao');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupControlsData()
setupPage()
btn_onclick(ctl)
invoiceNew()
invoiceNew_DSC()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    setupControlsData();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalimportacaoBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    //carrega campos caso houver dados
    fillFields();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupControlsData()
{
    txtInvoiceID.disabled = true;
    txtTaxa.disabled = true;

    txtInvoice.maxLength = 15;
    txtData.maxLength = 10;
    txtTaxa.maxLength = 5;
    txtValor.maxLength = 16;
}

function setupPage() {
    secText('Importa��o de servi�o', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;

    // reajusta dimensoes e reposiciona a janela
    //redimAndReposicionModalWin(345, 180, false);

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    adjustElementsInForm([['lblInvoiceID', 'txtInvoiceID', 10, 2],
						  ['lblInvoice', 'txtInvoice', 10, 2],
						  ['lblData', 'txtData', 12, 2],
						  ['lblTaxa', 'txtTaxa', 10, 2],
						  ['lblValor', 'txtValor', 12, 2]], null, null, true);

    // ajusta o divFields
    elem = divFields;
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP - 11;
        top = (parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP) - 54;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = txtTaxa.offsetTop + txtTaxa.offsetHeight + 2;
    }
}

function fillFields()
{
        setConnection(dsofillFields);

        dsofillFields.SQL = 'SELECT a.InvoiceID, a.Invoice, CONVERT(VARCHAR, a.dtEmissao, 103) AS dtEmissao, a.TaxaMoeda, a.ValorTotalInvoice ' +
                                'FROM Invoices a WITH(NOLOCK) ' +
                                    'INNER JOIN Invoices_Itens b ON (a.InvoiceID = b.InvoiceID) ' +
                                    'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.PedItemID = b.PedItemID) ' +
                                'WHERE c.PedidoID = ' + glb_nPedidoID;

        dsofillFields.ondatasetcomplete = fillFields_DSC;
        dsofillFields.Refresh();
}

function fillFields_DSC()
{
    if (dsofillFields.recordset.Fields['InvoiceID'].value != null)
    {
        txtInvoiceID.value = dsofillFields.recordset.Fields['InvoiceID'].value;
        txtInvoice.value = dsofillFields.recordset.Fields['Invoice'].value;
        txtData.value = dsofillFields.recordset.Fields['dtEmissao'].value;
        txtTaxa.value = dsofillFields.recordset.Fields['TaxaMoeda'].value;
        txtValor.value = dsofillFields.recordset.Fields['ValorTotalInvoice'].value;

        lokedFields();
    }

    showExtFrame(window, true);

    return null;
}

function txtData_onkeyup()
{
    if ((txtData.value).length == 10)
    {
        verificaData(txtData.value);
        taxaPTAX(dateFormatToSearch(txtData.value));
    }
}

//D�lar PTAX
function taxaPTAX(sData)
{
    setConnection(dsotaxaPTAX);

    dsotaxaPTAX.SQL = 'SELECT TOP 1 b.CotacaoVenda AS PTAX ' +
                            'FROM RelacoesConceitos a WITH(NOLOCK) ' +
                                'INNER JOIN RelacoesConceitos_Cotacoes b WITH(NOLOCK) ON (b.RelacaoID = a.RelacaoID) ' +
                            'WHERE ((a.RelacaoID = 704) AND (b.dtCotacao < ' + '\'' + sData + '\'' + ')) ' +
                            'ORDER BY b.dtCotacao DESC';

    dsotaxaPTAX.ondatasetcomplete = taxaPTAX_DSC;
    dsotaxaPTAX.Refresh();
}

function taxaPTAX_DSC()
{
    if ((dsotaxaPTAX.recordset.Fields['PTAX'].value) != null)
        txtTaxa.value = dsotaxaPTAX.recordset.Fields['PTAX'].value;
}

/*Trava campos*/
function lokedFields()
{
    //Campos preenchidos devem ser readonly
    txtInvoice.readOnly = true;
    txtData.readOnly = true;
    txtTaxa.readOnly = true;
    txtValor.readOnly = true;

    //Desabilita btns
    btnOK.disabled = true;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        invoiceNew();
    }
    // 2. O usuario clicou o botao Cancela
    else if (controlID == btnCanc.id)
    {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
    }
}

function invoiceNew() {
    var sError = '';
    var strPars = '';
    var nPedidoID;
    var nPessoaID;
    var sDataInvoice;

    nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');
    sDataInvoice = dateFormatToSearch(txtData.value);

    if (trimStr(sDataInvoice) == '')
        sError += 'Preencha a data da Invoice.\n';
    else if ((txtData.value).length != 10)
        sError += 'Data da Invoice inv�lida.\n';

    if (trimStr(txtValor.value) == '')
        sError += 'Preencha o valor da invoice.\n';

    if (!(txtTaxa.value > 0))
        sError += 'Favor verificar a data da Invoice.\n';

    if (sError != '') {
        if (window.top.overflyGen.Alert(sError) == 0)
            return null;

        return null;
    }

    lockControlsInModalWin(true);

    try {
        txtInvoiceID.value = changeVirgulaPonto(txtValor.value);

        strPars = '?';
        strPars += 'nPedidoID=' + glb_nPedidoID;
        strPars += '&nEmpresaID=' + glb_empresaData[0];
        strPars += '&nEstadoID=' + '1';
        strPars += '&sInvoice=' + escape(txtInvoice.value);
        strPars += '&sDtEmissao=' + sDataInvoice;
        strPars += '&nExportadorID=' + nPessoaID;
        strPars += '&nTaxaMoeda=' + escape(txtTaxa.value);
        strPars += '&nValorTotalInvoice=' + escape(txtInvoiceID.value);

        dsoGravacao.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/crianovainvoice.aspx' + strPars;
        dsoGravacao.ondatasetcomplete = invoiceNew_DSC;
        dsoGravacao.refresh();
    }
    catch (e) {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        // ou operacao de gravacao abortada no banco
        if (e.number == -2147217887)
            if (window.top.overflyGen.Alert('Erro ao criar nova invoice de servi�o.') == 0)
                return null;

        // Simula o botao OK para fechar a janela e forcar um refresh no sup
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
        return null;
    }
}

function invoiceNew_DSC() {
    var nResultado = (dsoGravacao.recordset.Fields['Resultado'].value);

    txtInvoiceID.value = nResultado;

    //Destrava modal
    lockControlsInModalWin(false);

    lokedFields();

    window.focus();

    return null;
}

function changeVirgulaPonto(strExp) {
    var i;
    var sReturn = '';
    for (i = 0; i <= strExp.length - 1 ; i++)
        sReturn += (strExp.substr(i, 1) == ',' ? '.' : strExp.substr(i, 1));
    return sReturn;
}

function verificaData(sData) {
    var sData = trimStr(sData);
    var bDataIsValid = true;

    if (sData != '')
        bDataIsValid = chkDataEx(sData);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        lockControlsInModalWin(false);
        return false;
    }
    return true;
}