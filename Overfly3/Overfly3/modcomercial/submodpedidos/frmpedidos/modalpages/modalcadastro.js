/********************************************************************
modalcadastro.js

Library javascript para o modalcadastro.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nDynDsos = 0;
var glb_bOK = true;
var glb_bCredito = false;
var glb_aEmpresaData = null;
var dsoCampos = new CDatatransport('dsoCampos');
var dsoAtributos = new CDatatransport('dsoAtributos');
var dsoInfFinanc = new CDatatransport('dsoInfFinanc');
var dsoFinanceiros = new CDatatransport('dsoFinanceiros');
  
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES

NOTA: Ver tambem o arquivo commonrelacoes.js

FUNCOES GERAIS:
window_onload()
setupPage()
btn_onclick(ctl)
getFieldsData()
dataToReturn()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    glb_aEmpresaData = getCurrEmpresaData();
    
    // configuracao inicial do html
    setupPage();   
    
	modoStart();
	//modoCredito();
	//changeModo();

    //getFieldsData();
    
    // ajusta o body do html
    with (modalcadastroBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Libera��o de Pedido' + ' - ' + glb_nPedidoID, 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }

    // ajusta o divFieldsCadastro
    with (divFieldsCadastro.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10);
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = ELEM_GAP * (11 + 4);
    }

    adjustElementsInForm([['lblFundacao','txtFundacao', 10, 1, -10],
                          ['lblAnos','txtAnos', 4, 1],
                          ['lblCNAE','txtCNAE', 7, 1],
                          ['lblClassificacao', 'txtClassificacao', 17, 1],
                          ['lblClassificacaoABC', 'txtClassificacaoABC', 8, 1],
                          ['lblConceito', 'txtConceito', 8, 1],
						  ['lbldtValidadeCredito','txtdtValidadeCredito', 10, 2, -10],
						  ['lblBloqueado','chkBloqueado', 3, 2, -5],
                          ['lblCapitalSocial','txtCapitalSocial', 13, 2,-5],
                          ['lblPatrimonioLiquido','txtPatrimonioLiquido', 13, 2],
                          ['lblFatAnual','txtFatAnual', 13, 2],
                          ['lblLimiteCreditoEmpresa', 'txtLimiteCreditoEmpresa', 13, 3, -10],
                          ['lblSeguroCredito', 'txtSeguroCredito', 13, 3],
                          ['lblLimite', 'txtLimite', 13, 3],
                          ['lblSaldoDevedor', 'txtSaldoDevedor', 13, 3],
                          ['lblSaldoCredito', 'txtSaldoCredito', 13, 3],
                          ['lblSaldoCreditoGrupo', 'txtSaldoCreditoGrupo', 13, 3]], null, null, true);
    
    txtFundacao.disabled = true;
    txtFundacao.readOnly = true;
    txtAnos.disabled = true;
    txtAnos.readOnly = true;
    txtCNAE.disabled = true;
    txtCNAE.readOnly = true;
    txtClassificacao.disabled = true;
    txtClassificacao.readOnly = true;
    txtClassificacaoABC.disabled = true;
    txtClassificacaoABC.readOnly = true;
    txtConceito.disabled = true;
    txtConceito.readOnly = true;
    txtdtValidadeCredito.disabled = true;
    txtdtValidadeCredito.readOnly = true;
	chkBloqueado.disabled = true;
    txtCapitalSocial.disabled = true;
    txtCapitalSocial.readOnly = true;
    txtPatrimonioLiquido.disabled = true;
    txtPatrimonioLiquido.readOnly = true;
    txtFatAnual.disabled = true;
    txtFatAnual.readOnly = true;
    txtLimiteCreditoEmpresa.disabled = true;
    txtLimiteCreditoEmpresa.reaOnly = true;
    txtSeguroCredito.disabled = true;
    txtSeguroCredito.reaOnly = true;
    txtLimite.disabled = true;
    txtLimite.readOnly = true;
    txtSaldoDevedor.readOnly = true;
    //txtSaldoCredito.disabled = true;
    txtSaldoCredito.readOnly = true;
    //txtSaldoCreditoGrupo.disabled = true;
    txtSaldoCreditoGrupo.readOnly = true;

    // ajusta o divMemo
    with (divMemo.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divFieldsCadastro.style.top, 10) + parseInt(divFieldsCadastro.style.height, 10) +
              btnCanc.offsetHeight + ELEM_GAP + 2;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = (ELEM_GAP * 6) - 5;
    }

    // Desabilita e esconde o botao cancela
    btnCanc.disabled = true;
    btnCanc.style.visibility = 'hidden';
    
    // Move botao OK para e troca o caption
    btnOK.innerText = 'Liberar' ;
    with (btnOK.style)
    {
		top = parseInt(divFieldsCadastro.currentStyle.top, 10) + parseInt(txtFundacao.currentStyle.top, 10);
		left = modWidth - parseInt(btnOK.currentStyle.width, 10) - (ELEM_GAP * 2);
    }

    btnOK.disabled = false;
    
    // Move botao Modo para e troca o caption
    btnModo.innerText = 'Modo' ;
    with (btnModo.style)
    {
		top = parseInt(divFieldsCadastro.currentStyle.top, 10) + parseInt(txtFundacao.currentStyle.top, 10);
		left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnOK.currentStyle.width, 10) - (ELEM_GAP / 2);
    }
    
    // esconde os divs
    divMemo.style.visibility = 'hidden';
    divFGRefComerciais.style.visibility = 'hidden';
    divFG.style.visibility = 'hidden';
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, dataToReturn() );    
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
    // 2. O usuario clicou o botao de Modo
    else if (ctl.id == btnModo.id )
    {
		changeModo();
        getFieldsData();
    }
}

/********************************************************************
Ida ao servidor para obter dados para campos
********************************************************************/
function getFieldsData()
{
	var strPars = new String();

	glb_nDynDsos = 0;
	
	var bDSOCampos = false;
	var bDSOAtributos = false;
	var bDSOInfFinanc = false;
	var bDSOFinanceiros = false;
	var nUsuarioID = getCurrUserID();
	var nContextoID = 0;
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    
    if ( ((typeof(aContextoID)).toUpperCase() == 'OBJECT') && (aContextoID.length >= 2) )
        nContextoID = aContextoID[1];

	if ( getMode().toUpperCase() == 'ATRIBUTOS')
	{
		if (dsoCampos.recordset.Fields.Count == 0)
		{    
			glb_nDynDsos++;
		    
			strPars = '?nTipo=' + escape(1);
			strPars += '&nContextoID=' + escape(nContextoID);
			strPars += '&nPedidoID=' + escape(glb_nPedidoID);
			strPars += '&nUsuarioID=' + escape(nUsuarioID);
			strPars += '&nEmpresaID=' + escape(glb_nEmpresaID);
			strPars += '&nEhCliente=' + escape(glb_nEhCliente);
			strPars += '&nParceiroID='  + escape(glb_nParceiroID);
            strPars += '&DATE_SQL_PARAM=' + escape(DATE_SQL_PARAM);
            strPars += '&nPaisID=' + escape(glb_aEmpresaData[1]);

			bDSOCampos = true;
			dsoCampos.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/pesqcadastro.aspx' + strPars;
			dsoCampos.ondatasetcomplete = pesquisa_DSC;
		}

		if (dsoAtributos.recordset.Fields.Count == 0)
		{    
			glb_nDynDsos++;
		    
			strPars = '?nTipo=' + escape(2);
			strPars += '&nContextoID=' + escape(nContextoID);
			strPars += '&nPedidoID=' + escape(glb_nPedidoID);
			strPars += '&nUsuarioID=' + escape(nUsuarioID);
			strPars += '&nEmpresaID=' + escape(glb_nEmpresaID);
			strPars += '&nEhCliente=' + escape(glb_nEhCliente);
			strPars += '&nParceiroID='  + escape(glb_nParceiroID);
            strPars += '&DATE_SQL_PARAM=' + escape(DATE_SQL_PARAM);
            strPars += '&nPaisID=' + escape(glb_aEmpresaData[1]);

			bDSOAtributos = true;
			dsoAtributos.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/pesqcadastro.aspx' + strPars;
			dsoAtributos.ondatasetcomplete = pesquisa_DSC;
		}
	}
	else
	{
		if (dsoInfFinanc.recordset.Fields.Count == 0)
		{    
			glb_nDynDsos++;
		    
			strPars = '?nTipo=' + escape(3);
			strPars += '&nContextoID=' + escape(nContextoID);
			strPars += '&nPedidoID=' + escape(glb_nPedidoID);
			strPars += '&nUsuarioID=' + escape(nUsuarioID);
			strPars += '&nEmpresaID=' + escape(glb_nEmpresaID);
			strPars += '&nEhCliente=' + escape(glb_nEhCliente);
			strPars += '&nParceiroID='  + escape(glb_nParceiroID);
            strPars += '&DATE_SQL_PARAM=' + escape(DATE_SQL_PARAM);
            strPars += '&nPaisID=' + escape(glb_aEmpresaData[1]);

			bDSOInfFinanc = true;
			dsoInfFinanc.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/pesqcadastro.aspx' + strPars;
			dsoInfFinanc.ondatasetcomplete = pesquisa_DSC;
		}

		if (dsoFinanceiros.recordset.Fields.Count == 0)
		{    
			glb_nDynDsos++;
		    
			strPars = '?nTipo=' + escape(4);
			strPars += '&nContextoID=' + escape(nContextoID);
			strPars += '&nPedidoID=' + escape(glb_nPedidoID);
			strPars += '&nUsuarioID=' + escape(nUsuarioID);
			strPars += '&nEmpresaID=' + escape(glb_nEmpresaID);
			strPars += '&nEhCliente=' + escape(glb_nEhCliente);
			strPars += '&nParceiroID='  + escape(glb_nParceiroID);
            strPars += '&DATE_SQL_PARAM=' + escape(DATE_SQL_PARAM);
            strPars += '&nPaisID=' + escape(glb_aEmpresaData[1]);

			bDSOFinanceiros = true;
			dsoFinanceiros.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/pesqcadastro.aspx' + strPars;
			dsoFinanceiros.ondatasetcomplete = pesquisa_DSC;
		}
	}

	lockControlsInModalWin(true);

	if (glb_nDynDsos == 0)
		pesquisa_DSC();
	else
	{
		if (bDSOCampos)
			dsoCampos.Refresh();

		if (bDSOAtributos)
			dsoAtributos.Refresh();

		if(bDSOInfFinanc)
			dsoInfFinanc.Refresh();

		if (bDSOFinanceiros)
			dsoFinanceiros.Refresh();
	}		
}

/********************************************************************
Retorno de pagina asp no servidor com dados para campos
********************************************************************/
function pesquisa_DSC()
{
    glb_nDynDsos--;
    
    if (glb_nDynDsos > 0)
        return null;
    
	var nSaldoDevedor = 0;
	var dTFormat = '';
	var nValorCelula;
    var i, j;
    var nLimiteCredito = 0;
    var nSaldoCredito = 0;
    var nSaldoCreditoGrupo = 0;

	if ( getMode().toUpperCase() == 'ATRIBUTOS')
	{
        if (!((dsoCampos.recordset.BOF) && (dsoCampos.recordset.EOF)))
		{
			if (dsoCampos.recordset.Fields['dtFundacao'].value != null)
				txtFundacao.value = dsoCampos.recordset.Fields['dtFundacao'].value;

			if (dsoCampos.recordset.Fields['AnosFundacao'].value != null)
				txtAnos.value = dsoCampos.recordset.Fields['AnosFundacao'].value;

			if (dsoCampos.recordset.Fields['CNAE'].value != null)
				txtCNAE.value = dsoCampos.recordset.Fields['CNAE'].value;

			if (dsoCampos.recordset.Fields['DescricaoCNAE'].value != null)
				txtCNAE.title = dsoCampos.recordset.Fields['DescricaoCNAE'].value;

			if (dsoCampos.recordset.Fields['Classificacao'].value != null)
                txtClassificacao.value = dsoCampos.recordset.Fields['Classificacao'].value;

            if (dsoCampos.recordset.Fields['ClassificacaoABC'].value != null)
                txtClassificacaoABC.value = dsoCampos.recordset.Fields['ClassificacaoABC'].value;

            if (dsoCampos.recordset.Fields['Conceito'].value != null)
                txtConceito.value = dsoCampos.recordset.Fields['Conceito'].value;

			if (dsoCampos.recordset.Fields['dtValidadeCredito'].value != null)
				txtdtValidadeCredito.value = dsoCampos.recordset.Fields['dtValidadeCredito'].value;
				
			chkBloqueado.checked = (dsoCampos.recordset.Fields['Bloqueado'].value == true);

			if (dsoCampos.recordset.Fields['CapitalSocial'].value != null)
				txtCapitalSocial.value = dsoCampos.recordset.Fields['CapitalSocial'].value;

			if (dsoCampos.recordset.Fields['PatrimonioLiquido'].value != null)
				txtPatrimonioLiquido.value = dsoCampos.recordset.Fields['PatrimonioLiquido'].value;

			if (dsoCampos.recordset.Fields['FaturamentoAnual'].value != null)
				txtFatAnual.value = dsoCampos.recordset.Fields['FaturamentoAnual'].value;

			if (dsoCampos.recordset.Fields['LimiteCredito'].value != null)
				txtLimite.value = dsoCampos.recordset.Fields['LimiteCredito'].value;

            if (dsoCampos.recordset['SaldoDevedor'].value != null)
                txtSaldoDevedor.value = dsoCampos.recordset['SaldoDevedor'].value;

            if (dsoCampos.recordset['LimiteCreditoEmpresa'].value != null)
                txtLimiteCreditoEmpresa.value = dsoCampos.recordset.Fields['LimiteCreditoEmpresa'].value;

            if (dsoCampos.recordset['SeguroCredito'].value != null)
                txtSeguroCredito.value = dsoCampos.recordset.Fields['SeguroCredito'].value;

            if (dsoCampos.recordset['SaldoCredito'].value != null)
                txtSaldoCredito.value = dsoCampos.recordset['SaldoCredito'].value;

            txtSaldoCredito.style.color = 'black';

            if (dsoCampos.recordset['SaldoCreditoGrupo'].value != null)
                txtSaldoCreditoGrupo.value = dsoCampos.recordset['SaldoCreditoGrupo'].value;

            txtSaldoCreditoGrupo.style.color = 'black';

            if (dsoCampos.recordset['SaldoCredito'].value != null) {
                nSaldoCredito = replaceStr(txtSaldoCredito.value, '.', '');
                nSaldoCredito = replaceStr(nSaldoCredito, ',', '.');
            }

            if (dsoCampos.recordset['SaldoCreditoGrupo'].value != null) {
                nSaldoCreditoGrupo = replaceStr(txtSaldoCreditoGrupo.value, '.', '');
                nSaldoCreditoGrupo = replaceStr(nSaldoCreditoGrupo, ',', '.');
            }

            if (dsoCampos.recordset.Fields['LimiteCredito'].value != null) {
                nLimiteCredito = replaceStr(txtLimite.value, '.', '');
                nLimiteCredito = replaceStr(nLimiteCredito, ',', '.');
            }

            if (dsoCampos.recordset.Fields['SaldoDevedor'].value != null) {
                nSaldoDevedor = replaceStr(dsoCampos.recordset.Fields['SaldoDevedor'].value, '.', '');
                nSaldoDevedor = replaceStr(nSaldoDevedor, ',', '.');
            }

            if (nSaldoDevedor > 0)
                txtSaldoDevedor.value = "-" + dsoCampos.recordset.Fields['SaldoDevedor'].value;

            if (nSaldoCredito < 0)
            {
                txtSaldoCredito.style.color = 'red';
                txtSaldoDevedor.style.color = 'red';
            }

            if (nSaldoCreditoGrupo < 0) {
                txtSaldoCreditoGrupo.style.color = 'red';
            }
		}

		if ( !((dsoAtributos.recordset.BOF)&&(dsoAtributos.recordset.EOF)) )
		{
			fgRefCom.FrozenCols = 0;
			fgRefCom.FontSize = '10';
			headerGrid(fgRefCom,['Atributo',
								 'Pedido',
								 'M�nimo',
								 'M�ximo',
							 	 'OK',
							 	 'Direito',
							 	 'OK2',
							 	 'AtributoID'],[5,6,7]);
		                       
			fillGridMask(fgRefCom,dsoAtributos,['Atributo*',
												'ValorPedido*',
												'LimiteUsuarioMinimo*',
												'LimiteUsuarioMaximo*',
												'OK',
												'Direito',
												'_calc_OK2_1',
												'AtributoID'],
												['','','','','','','',''],
												['','###,###,###.00','###,###,###.00','###,###,###.00','','','','']);

			var bgColorRed = 0X7280FA;

			for (i=1; i<fgRefCom.Rows; i++)
			{
				if (fgRefCom.ValueMatrix(i,getColIndexByColKey(fgRefCom, 'AtributoID')) == 36)
				{
					if (nSaldoCredito < 0)
					{
                        fgRefCom.TextMatrix(i, getColIndexByColKey(fgRefCom, 'ValorPedido*')) = nLimiteCredito - nSaldoDevedor;
						fgRefCom.TextMatrix(i, getColIndexByColKey(fgRefCom, 'OK')) = '0';

						fgRefCom.FillStyle = 1;
						fgRefCom.Cell(6, i, getColIndexByColKey(fgRefCom, 'ValorPedido*'), i, getColIndexByColKey(fgRefCom, 'ValorPedido*')) = bgColorRed;
						
                        if (fgRefCom.ValueMatrix(i, getColIndexByColKey(fgRefCom, 'LimiteUsuarioMaximo*')) < fgRefCom.TextMatrix(i, getColIndexByColKey(fgRefCom, 'ValorPedido*')))
						{
							fgRefCom.FillStyle = 1;
							fgRefCom.Cell(6, i, getColIndexByColKey(fgRefCom, 'LimiteUsuarioMaximo*'), i, getColIndexByColKey(fgRefCom, 'LimiteUsuarioMaximo*')) = bgColorRed;
							fgRefCom.TextMatrix(i, getColIndexByColKey(fgRefCom, 'Direito')) = '0';
						}
					}
					else
						fgRefCom.TextMatrix(i, getColIndexByColKey(fgRefCom, 'OK')) = '1';
				}
				else
				{
					if (fgRefCom.ValueMatrix(i,getColIndexByColKey(fgRefCom, 'AtributoID')) == 50)
					{
						fgRefCom.FillStyle = 1;
						fgRefCom.Cell(6, i, getColIndexByColKey(fgRefCom, 'ValorPedido*'), i, getColIndexByColKey(fgRefCom, 'ValorPedido*')) = bgColorRed;
					}

					if ( fgRefCom.ValueMatrix(i,getColIndexByColKey(fgRefCom, 'ValorPedido*')) < fgRefCom.ValueMatrix(i,getColIndexByColKey(fgRefCom, 'LimiteUsuarioMinimo*')) )
					{
						fgRefCom.FillStyle = 1;
						fgRefCom.Cell(6, i, getColIndexByColKey(fgRefCom, 'LimiteUsuarioMinimo*'), i, getColIndexByColKey(fgRefCom, 'LimiteUsuarioMinimo*')) = bgColorRed;
					}

					if ( fgRefCom.ValueMatrix(i,getColIndexByColKey(fgRefCom, 'ValorPedido*')) > fgRefCom.ValueMatrix(i,getColIndexByColKey(fgRefCom, 'LimiteUsuarioMaximo*')) )
					{
						fgRefCom.FillStyle = 1;
						fgRefCom.Cell(6, i, getColIndexByColKey(fgRefCom, 'LimiteUsuarioMaximo*'), i, getColIndexByColKey(fgRefCom, 'LimiteUsuarioMaximo*')) = bgColorRed;
					}
				}
				
				if ( fgRefCom.ValueMatrix(i,getColIndexByColKey(fgRefCom, 'OK')) == 0 )
					glb_bOK = false;
				
				fgRefCom.TextMatrix(i, getColIndexByColKey(fgRefCom, '_calc_OK2_1')) = fgRefCom.TextMatrix(i, getColIndexByColKey(fgRefCom, 'OK'));

			}

			alignColsInGrid(fgRefCom,[1,2,3]);
			fgRefCom.FrozenCols = 1;

			fgRefCom.SelectionMode = 0;
			fgRefCom.AutoSizeMode = 0;
			fgRefCom.AutoSize(0,fgRefCom.Cols-1,false,0);
			
			fgRefCom.Editable = true;
			window.focus();
			fgRefCom.focus();
			
		}
	}
	else
	{
		glb_bCredito = true;

		if ( !((dsoInfFinanc.recordset.BOF)&&(dsoInfFinanc.recordset.EOF)) )
		{
			dsoInfFinanc.recordset.setFilter('Indice = 2');

			if ( !((dsoInfFinanc.recordset.BOF)&&(dsoInfFinanc.recordset.EOF)) )
			{
				fgRefCom.FontSize = '8';
				headerGrid(fgRefCom,['Data',
								'Empresa',
								'Cliente Desde',
								'�ltima Compra',
								'Valor',
								'Maior Compra',
								'Valor',
								'Limite de Cr�dito',
								'Prazo',
								'Atraso',
								'Conceito'],[]);

				fillGridMask(fgRefCom,dsoInfFinanc,['dtReferencia',
										'Empresa',
										'dtInicio',
										'dtUltimaCompra',
										'ValorUltimaCompra',
										'dtMaiorCompra',
										'ValorMaiorCompra',
										'LimiteCredito',
										'Prazo',
										'Atraso',
										'Conceito'],
										['99/99/9999', '', '99/99/9999', '99/99/9999', '999999999.99', '99/99/9999', '999999999.99',
										'999999999.99', '999', '999', ''],
										[dTFormat, '', dTFormat, dTFormat, '###,###,###.00', dTFormat, '###,###,###.00',
										'###,###,###.00', '###', '###', '']);

				alignColsInGrid(fgRefCom,[4,6,7,8,9]);
				fgRefCom.FrozenCols = 2;
				fgRefCom.AutoSize(0,fgRefCom.Cols-1);
			}

			dsoInfFinanc.recordset.setFilter('');
		}
	
		if ( !((dsoFinanceiros.recordset.BOF)&&(dsoFinanceiros.recordset.EOF)) )
		{
			if ( DATE_FORMAT == "DD/MM/YYYY" )
				dTFormat = 'dd/mm/yyyy';
			else if ( DATE_FORMAT == "MM/DD/YYYY" )
				dTFormat = 'mm/dd/yyyy';

			fg.FrozenCols = 0;
			fg.FontSize = '8';
			headerGrid(fg,['Vencimento',
						'Atr',
						'Financeiro',
						'Est',
						'Pedido',
						'CFOP',
						'Duplicata',
						'Emiss�o',
						'Forma',
						'Prazo',
						'$',
						'Valor',
						'Saldo',
						'Saldo Atual',
						'Pessoa',
						'Empresa'],[]);
		                       
			fillGridMask(fg,dsoFinanceiros,['dtVencimento',
									'Atraso',
									'FinanceiroID',
									'Estado',
									'PedidoID',
									'Codigo',
									'Duplicata',
									'dtEmissao',
									'FormaPagamento',
									'PrazoPagamento',
									'Moeda',
									'Valor',
									'SaldoDevedor',
									'SaldoAtualizado',
									'Pessoa',
									'Empresa'],
									['','','','','','','','','','','','','','','',''],
									[dTFormat,'','','','','','',dTFormat,'','','',
									'###,###,###.00','###,###,###.00','###,###,###.00','','']);

			alignColsInGrid(fg,[1,2,4,11,12,13]);
			fg.FrozenCols = 3;

			for (i=1; i<fg.Rows; i++)
			{
				for (j=11; j<=13; j++)
				{
					nValorCelula = fg.ValueMatrix(i,j);
					if ( (!(isNaN(nValorCelula))) && (nValorCelula < 0) )
					{
						fg.Select(i, j, i, j);
						fg.FillStyle = 1;
						fg.CellForeColor = 0X0000FF;
					}    
				}
			}

			// Linha de Totais
			gridHasTotalLine(fg, 'Totais (US$)', 0xC0C0C0, null, true, [[12,'###,###,###,##0.00','S']]);

			// Atualiza o valor Saldo Devedor e do Saldo Atualizado
			if (fg.Rows>2)
			{
				if (nSaldoDevedor != null)
					fg.TextMatrix(1, 12) = nSaldoDevedor;
			}
		    
			fg.AutoSizeMode = 0;
			fg.AutoSize(0,fg.Cols-1,false,0);
		}
	}

	// mostra a janela modal com o arquivo carregado
	showExtFrame(window, true);
	window.focus();
	lockControlsInModalWin(false);

	btnOK.disabled = false;
}

/********************************************************************
Monta e retorna array de dados a serem enviados para o sub form que chamou
a modal
********************************************************************/
function dataToReturn()
{
    var aData = new Array();
    var i=0;
    var bLimiteMaximo = 1;

    aData[0] = glb_nCurrEstadoID;
    aData[1] = glb_nNewEstadoID;
    
	if (getMode().toUpperCase() == 'ATRIBUTOS')
	{
		for (i=1; i<fgRefCom.Rows; i++)
		{
			if (fgRefCom.ValueMatrix(i, getColIndexByColKey(fgRefCom, 'OK')) == 0)
			{
				bLimiteMaximo = 0;
				break;
			}				
		}
    }
    else
		bLimiteMaximo = 0;
		
	aData[2] = bLimiteMaximo;
    
    return aData;
}

/********************************************************************
Evento particular de grid desta janela
********************************************************************/
function js_cadastro_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 0;
    
    if ( glb_totalCols__ != false )
        firstLine = 1;
        
    if (NewRow > firstLine)
    {
        fg.Row = NewRow;
    }
}

/********************************************************************
Seta o modo Atributo ou Credito da interface
Parametro:
	sModo = 'Atributos' ou 'Credito'
********************************************************************/
function setMode(sModo)
{
	btnModo.setAttribute('Modo', sModo, 1);	
}

/********************************************************************
Retorna o modo Atributo ou Credito da interface
********************************************************************/
function getMode()
{
	return btnModo.getAttribute('Modo', 1);	
}

/********************************************************************
Troca o modo Atributo ou Credito da interface
********************************************************************/
function changeModo()
{
	if ( getMode().toUpperCase() == 'ATRIBUTOS')
	{
		btnModo.value = 'Atributos';
		btnModo.title = 'Comutar para atributos de libera��o';
		modoCredito();
	}	
	else if ((getMode().toUpperCase() == 'CREDITO') || (getMode().toUpperCase() == 'START'))
	{
		btnModo.value = 'Cr�dito';
		btnModo.title = 'Comutar para informa��es de cr�dito';
		modoAtributos();
	}
}

function modoStart()
{
	var elem;
    var frameRect;
    var modWidth = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }
    
	// ajusta o divFGRefComerciais
    with (divFGRefComerciais.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMemo.style.top, 10) +
              parseInt(divMemo.style.height, 10) + ELEM_GAP - 104;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (ELEM_GAP * 10) - 5;
    }
    
    with (fgRefCom.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFGRefComerciais.style.width, 10);
        height = parseInt(divFGRefComerciais.style.height, 10);
        
        fgRefCom.Redraw = 0;
        startGridInterface(fgRefCom, 0, 1);
        fg.Cols = 0;
        fgRefCom.Cols = 1;
        fgRefCom.ColWidth(0) = parseInt(divFGRefComerciais.style.width, 10) * 18;
        fgRefCom.Redraw = 2;
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divFGRefComerciais.style.top, 10) + parseInt(divFGRefComerciais.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnCanc.currentStyle.top, 10) + parseInt(btnCanc.currentStyle.height, 10) +
            ELEM_GAP + 2 -  
            (parseInt(divFGRefComerciais.style.top, 10) + parseInt(divFGRefComerciais.style.height, 10) + ELEM_GAP) -
            ELEM_GAP;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    fg.Redraw = 0;
    startGridInterface(fg, 0, 1);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.style.width, 10) * 18;
    fg.Redraw = 2;
    
    // mostra os divs
    divMemo.style.visibility = 'inherit';
    divFGRefComerciais.style.visibility = 'inherit';
	divFG.style.visibility = 'inherit';

	setMode('Start');
	btnModo.value = 'Visualizar';
	showExtFrame(window, true);
	window.focus();
	lockControlsInModalWin(false);

	return null;
}

/********************************************************************
Coloca a interface no modo Credito
********************************************************************/
function modoCredito()
{
	var elem;
    var frameRect;
    var modWidth = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }
    
	// ajusta o divFGRefComerciais
    with (divFGRefComerciais.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMemo.style.top, 10) + parseInt(divMemo.style.height, 10) + ELEM_GAP - 104;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (ELEM_GAP * 10) + 57- 5;
    }

    with (fgRefCom.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFGRefComerciais.style.width, 10);
        height = parseInt(divFGRefComerciais.style.height, 10);
        
        fgRefCom.Redraw = 0;
        startGridInterface(fgRefCom, 0, 1);
        fg.Cols = 0;
        fgRefCom.Cols = 1;
        fgRefCom.ColWidth(0) = parseInt(divFGRefComerciais.style.width, 10) * 18;
        fgRefCom.Redraw = 2;
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divFGRefComerciais.style.top, 10) + parseInt(divFGRefComerciais.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnCanc.currentStyle.top, 10) + parseInt(btnCanc.currentStyle.height, 10) +
            ELEM_GAP + 2 -  
            (parseInt(divFGRefComerciais.style.top, 10) + parseInt(divFGRefComerciais.style.height, 10) + ELEM_GAP) -
            ELEM_GAP;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    fg.Redraw = 0;
    startGridInterface(fg, 0, 1);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.style.width, 10) * 18;
    fg.Redraw = 2;
    
    // mostra os divs
    divMemo.style.visibility = 'inherit';
    divFGRefComerciais.style.visibility = 'inherit';
	divFG.style.visibility = 'inherit';

	setMode('Credito');
}

/********************************************************************
Coloca a interface no modo Atributos
********************************************************************/
function modoAtributos()
{
	var elem;
    var frameRect;
    var modWidth = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }
    
	// ajusta o divFGRefComerciais
    with (divFGRefComerciais.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMemo.style.top, 10) - 40;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = divFG.offsetTop + divFG.offsetHeight + 44 - divMemo.offsetTop ;
    }
    
    with (fgRefCom.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFGRefComerciais.style.width, 10);
        height = parseInt(divFGRefComerciais.style.height, 10);
        
        startGridInterface(fgRefCom, 0, 1);
        fg.Cols = 0;
        fgRefCom.Cols = 1;
        fgRefCom.ColWidth(0) = parseInt(divFGRefComerciais.style.width, 10) * 18;
    }

	// esconde os divs
	divMemo.style.visibility = 'hidden';	
	divFG.style.visibility = 'hidden';

	setMode('Atributos');
}

function js_BeforeRowColChangeModalCadastro(grid, OldRow, OldCol, NewRow, NewCol, Cancel)
{
	//Forca celula read-only    
	if (!glb_GridIsBuilding)
	{
		if ( glb_FreezeRolColChangeEvents )
			return true;

		if (getMode().toUpperCase() == 'ATRIBUTOS')
		{
			if (grid.ValueMatrix(OldRow, getColIndexByColKey(grid, 'Direito')) == 0)
				grid.TextMatrix(OldRow, getColIndexByColKey(grid, 'OK')) = grid.TextMatrix(OldRow, getColIndexByColKey(grid, '_calc_OK2_1'));
		}
    }
}

function currCellIsLocked(grid, NewRow, NewCol)
{
	var retVal = false;
	
	if (getMode().toUpperCase() == 'ATRIBUTOS')
	{
		if (grid.ValueMatrix(NewRow,getColIndexByColKey(grid, 'Direito')) == 0 )
			retVal = false;
	}
	
	return retVal;
}