/********************************************************************
modalanteciparentrega.js

Library javascript para o modalanteciparentrega.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Controla se algum dado do grid foi alterado   
var dsoGrid = new CDatatransport('dsoGrid');
var dsoParticionados = new CDatatransport('dsoParticionados');
var dsoAnteciparEntrega = new CDatatransport('dsoAnteciparEntrega');

var glb_aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
var glb_nUsuarioID = getCurrUserID();

// FINAL DE VARIAVEIS GLOBAIS ***************************************


/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalAnteciparEntregaBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // define altura da modal
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 50;

    // configuracao inicial do html
    setupPage();   

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Antecipar Entrega', 1);

    // busca elementos da janela
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // ajusts o divControles
    with (divControles.style)
    {
    	border = 'none';
    	backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 5;
        height = parseInt(btnOK.currentStyle.height, 10) + ELEM_GAP;
    }

    // Configura bot�o OK e Cancelar
    btnOK.disabled = false;
    btnOK.style.top = parseInt(divControles.currentStyle.top, 10);
    btnOK.style.width = 110;
    btnOK.style.left = parseInt(divControles.currentStyle.width, 10) - parseInt(btnOK.currentStyle.width, 10);
    btnOK.innerText = 'Antecipar Entrega';
    btnCanc.style.visibility = 'hidden';

    // ajusta o divGrids
    with (divGrids.style) {
        border = 'none';
        borderWidth = 0;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControles.currentStyle.top, 10) + parseInt(divControles.currentStyle.height, 10);
        width = parseInt(divControles.currentStyle.width, 10);
        height = modHeight - parseInt(divControles.currentStyle.top) - parseInt(divControles.currentStyle.height) - ELEM_GAP - 5;
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'groove';
        borderWidth = 1.5;
        backgroundColor = 'transparent';
        width = parseInt(divControles.currentStyle.width, 10);
        height = (parseInt(divGrids.currentStyle.height) - ELEM_GAP) * 0.65 - (parseInt(lblParticionados.currentStyle.top, 10) + parseInt(lblParticionados.currentStyle.height, 10));
    }

    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10) - 3;
        height = parseInt(divFG.style.height, 10) - 3;
    }

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;

    lblParticionados.style.top = parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;;

    // ajusta o divFGParticionados
    with (divFGParticionados.style) {
        border = 'groove';
        borderWidth = 1.5;
        backgroundColor = 'transparent';
        //top = parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;
        top = parseInt(lblParticionados.currentStyle.top, 10) + parseInt(lblParticionados.currentStyle.height, 10);
        width = parseInt(divControles.currentStyle.width, 10);
        height = (parseInt(divGrids.currentStyle.height) - ELEM_GAP) * 0.35;
    }

    with (fgParticionados.style) {
        left = 0;
        top = 0;
        width = parseInt(divFGParticionados.style.width, 10) - 3;
        height = parseInt(divFGParticionados.style.height, 10) - 3;
    }

    startGridInterface(fgParticionados);
    fgParticionados.Cols = 1;
    fgParticionados.ColWidth(0) = parseInt(divFGParticionados.currentStyle.width, 10) * 18;
    fgParticionados.Redraw = 2;

    // Lista itens do pedido em quest�o
    fillGridData();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    if (controlID == 'btnOK')
        anteciparEntrega();

    // esta funcao fecha a janela modal e destrava a interface
    else if (controlID == 'btnCanc')
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null ); 
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    lockControlsInModalWin(true);
   
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    var strSQL = 'SELECT DISTINCT a.Ordem, a.ProdutoID, dbo.fn_Produto_Descricao2(a.PedItemID, NULL, 20) AS Produto, a.Quantidade, ' +

        'CONVERT(INT, NULL) AS Antecipar, CONVERT(BIT, 0) AS Ok, dbo.fn_Produto_Estoque(b.EmpresaID, a.ProdutoID, 341, NULL, NULL, NULL, 375, c.LoteID) AS Estoque, ' +
                        'a.ValorUnitario, dbo.fn_PedidoItem_Totais(a.PedItemID, 8) AS ValorTotal, a.PedItemID ' +
                    'FROM Pedidos_Itens a WITH(NOLOCK) ' +
                        'INNER JOIN Pedidos b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) ' +
                        'INNER JOIN Lotes_Itens c WITH(NOLOCK) ON (c.PedItemID = a.PedItemID) ' +
                    'WHERE ((a.PedidoID = ' + glb_nPedidoID + ') AND ((b.TipoPedidoID = 601) OR ((b.TipoPedidoID = 602) AND (a.EhEncomenda = 1)))) ' +
                    'ORDER BY a.Ordem';

    dsoGrid.SQL = strSQL;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
//showExtFrame(window, true);

//var dTFormat = '';
    
//if ( DATE_FORMAT == "DD/MM/YYYY" )
//    dTFormat = 'dd/mm/yyyy';
//else if ( DATE_FORMAT == "MM/DD/YYYY" )
//    dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Editable = true;
    fg.FrozenCols = 0;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

    headerGrid(fg, ['Ordem',            // 0
                    'ID',               // 1
                    'Produto',          // 2
                    'Quant',            // 3
                    'Antecipar',        // 4
                    'OK',               // 5
                    'Estoque',             // 6
                    'Valor Unit�rio',   // 7
                    'Valor Total',      // 8
                    'PedItemID'],       // 9
                    [9]);
                       
    fillGridMask(fg, dsoGrid, [ 'Ordem*',            // 0
                                'ProdutoID*',        // 1
                                'Produto*',          // 2
                                'Quantidade*',       // 3
                                'Antecipar',         // 4
                                'Ok',                // 5
                                'Estoque*',          // 6
                                'ValorUnitario*',    // 7
                                'ValorTotal*',       // 8
                                'PedItemID'],        // 9
                              ['', '', '', '', '', '', '', '', '', ''],
                              ['', '', '######', '######', '', '', '', '###,###,###.##', '###,###,###.##', '']);

    //glb_aCelHint = [[0, 5, 'Confirma antecipa��o?']];


    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[3, '######', 'S'],
                                                          [6, '######', 'S'],
                                                          [7, '###,###,###.##', 'S'],
                                                          [8, '###,###,###.##', 'S']]);

    fg.FontSize = '9';
    fg.ExplorerBar = 5;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;

    lockControlsInModalWin(false);

    // Lista itens dos demais pedidos j� particionados anteriormente
    fillGridDataParticionados();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridDataParticionados()
{
    lockControlsInModalWin(true);

    // parametrizacao do dso dsoParticionados
    setConnection(dsoParticionados);

    var strSQL = 'SELECT d.dtPedido, b.PedidoID, e.RecursoAbreviado AS Estado, c.ProdutoID, dbo.fn_Produto_Descricao2(c.PedItemID, NULL, 20) AS Produto, c.Quantidade, ' +
                        'ValorUnitario, dbo.fn_PedidoItem_Totais(PedItemID, 8) AS ValorTotal, PedItemID ' +
                    'FROM Pedidos_Vinculados a WITH(NOLOCK) ' +
                        'INNER JOIN Pedidos_Vinculados b WITH(NOLOCK) ON (b.VinculadoID = a.VinculadoID) ' +
                        'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) ' +
                        'INNER JOIN Pedidos d WITH(NOLOCK) ON (d.PedidoID = c.PedidoID) ' +
                        'INNER JOIN Recursos e WITH(NOLOCK) ON (e.RecursoID = d.EstadoID) ' +
                    'WHERE ((a.PedidoID = ' + glb_nPedidoID + ') AND (b.PedidoID <> a.PedidoID) AND (a.TipoVinculoID = 4401) /*AND (d.EstadoID > 26)*/) ' +
                    'ORDER BY d.dtPedido DESC, b.PedidoID DESC, c.Ordem';

    dsoParticionados.SQL = strSQL;
    dsoParticionados.ondatasetcomplete = fillGridDataParticionados_DSC;
    dsoParticionados.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridDataParticionados_DSC()
{
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fgParticionados.Redraw = 0;
    fgParticionados.Editable = false;
    fgParticionados.FrozenCols = 0;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fgParticionados, ['Data',             // 0
                                 'Pedido',           // 1
                                 'Est',              // 2
                                 'ID',               // 3
                                 'Produto',          // 4
                                 'Quant',            // 5
                                 'Valor Unit�rio',   // 6
                                 'Valor Total',      // 7
                                 'PedItemID'],       // 8
                                 [8]);

    fillGridMask(fgParticionados, dsoParticionados,  ['dtPedido*',          // 0 
                                                      'PedidoID*',          // 1
                                                      'Estado*',            // 2
                                                      'ProdutoID*',         // 3
                                                      'Produto*',           // 4
                                                      'Quantidade*',        // 5
                                                      'ValorUnitario*',     // 6
                                                      'ValorTotal*',        // 7
                                                      'PedItemID'],         // 8
                                                      ['99/99/9999', '', '', '', '', '', '', '', ''],
                                                      [dTFormat, '', '', '', '', '######', '###,###,###.##', '###,###,###.##', '']);

    alignColsInGrid(fgParticionados, [1, 3, 5, 6, 7]);

    fgParticionados.FontSize = '9';
    fgParticionados.ExplorerBar = 5;
    fgParticionados.AutoSize(0, fgParticionados.Cols - 1);
    fgParticionados.Redraw = 2;

    // Merge de Colunas
    fgParticionados.MergeCells = 2;
    fgParticionados.MergeCol(-1) = true;

    lockControlsInModalWin(false);

    fg.focus();
}


/********************************************************************
Funcao criada pelo programador.
aprovar Campanha

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function anteciparEntrega()
{
    var nTotalQuantidade = parseInt(fg.TextMatrix(1, getColIndexByColKey(fg, 'Quantidade')));
    var nTotalAntecipar = parseInt(fg.TextMatrix(1, getColIndexByColKey(fg, 'Antecipar')));

    if (nTotalQuantidade == nTotalAntecipar) {
        if (window.top.overflyGen.Alert('N�o � poss�vel antecipar a entrega total do pedido.') == 0)
            return null;

        fg.focus();
        return;
    }

    lockControlsInModalWin(true);
    btnOK.innerText = 'Aguarde...';

    var registrosMarcados = false;
    var quantidadeMaxima = true;
    var strPars = '';

    strPars = '?nUsuarioID=' + glb_nUsuarioID.toString();
    strPars += '&nTipoResultado=1';

    for (var i = 2; i < fg.Rows; i++) {

        if ((getCellValueByColKey(fg, 'Ok', i) != 0) && (parseInt(fg.TextMatrix(i, getColIndexByColKey(fg, 'Antecipar'))) > 0)) {
            strPars += '&nPedItemID=' + escape(getCellValueByColKey(fg, 'PedItemID', i));
            strPars += '&nQuantidadeAntecipar=' + escape(getCellValueByColKey(fg, 'Antecipar', i));

            registrosMarcados = true;
        }
    }

    if (!registrosMarcados) {
        if (window.top.overflyGen.Alert('Selecione algum item para antecipar entrega.') == 0)
            return null;

        btnOK.innerText = 'Antecipar Entrega';
        lockControlsInModalWin(false);
        fg.focus();
    }
    else {
        dsoAnteciparEntrega.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/anteciparentrega.aspx' + strPars;
        dsoAnteciparEntrega.ondatasetcomplete = anteciparEntrega_DSC;
        dsoAnteciparEntrega.Refresh();
    }
}

/********************************************************************
Funcao criada pelo programador.
Retorno da funcao que associa a Campanha

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function anteciparEntrega_DSC()
{
    //destrava a Interface
    lockControlsInModalWin(false);

    if (!((dsoAnteciparEntrega.recordset.BOF) && (dsoAnteciparEntrega.recordset.EOF)))
    {
        var resultado = dsoAnteciparEntrega.recordset['Mensagem'].value;

        if ((resultado != null) && (resultado != '') && (resultado != ' '))
		{
            if (window.top.overflyGen.Alert(resultado) == 0)
                return null;

            btnOK.innerText = 'Antecipar Entrega';
            fillGridData();
		}
		else
		{
            fillGridData();
            btnOK.innerText = 'Antecipar Entrega';
		}
	}
}


// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/

function fg_modalAnteciparEntrega_AfterEdit(fg, Row, Col) {
    
    var nColAntecipar = getColIndexByColKey(fg, 'Antecipar');
    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade*');
    var nColOk = getColIndexByColKey(fg, 'Ok');
    var nColEstoque = getColIndexByColKey(fg, 'Estoque*');
    var nTipoPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoPedidoID' + '\'' + '].value');

    if (Col == nColAntecipar) {
        var nAntecipar = parseInt(fg.TextMatrix(Row, nColAntecipar));

        if (nAntecipar > 0) {
            var nQuantidade = parseInt(fg.TextMatrix(Row, nColQuantidade));
            var nEstoque = parseInt(fg.TextMatrix(Row, nColEstoque));

            var sErro = '';

            if (nAntecipar > nQuantidade)
                sErro += 'Quantidade m�xima para antecipar � de ' + nQuantidade + '. \n';

            if ((nAntecipar > nEstoque) && (nTipoPedidoID == 602))
                sErro += 'Quantidade maior que quantidade dispon�vel no Lote.';

            if (sErro != ''){
                if (window.top.overflyGen.Alert(sErro) == 0)
                    return null;

                fg.TextMatrix(Row, nColAntecipar) = '';
                fg.TextMatrix(Row, nColOk) = 0;
            }
            else
                fg.TextMatrix(Row, nColOk) = 1;
        }
        else {
            fg.TextMatrix(Row, nColAntecipar) = '';
            fg.TextMatrix(Row, nColOk) = 0;
        }
    }

    // Atualiza linha de total de pe�as � antecipar
    var nAntecipar = 0;

    for (var i = 2; i < fg.Rows; i++) {
        nAntecipar += parseInt(((fg.TextMatrix(i, nColAntecipar) == "") ? 0 : fg.TextMatrix(i, nColAntecipar)));
    }
    if (nAntecipar == 0)
        fg.TextMatrix(1, nColAntecipar) = "";
    else
        fg.TextMatrix(1, nColAntecipar) = nAntecipar;

}


// FINAL DE EVENTOS DE GRID *****************************************
