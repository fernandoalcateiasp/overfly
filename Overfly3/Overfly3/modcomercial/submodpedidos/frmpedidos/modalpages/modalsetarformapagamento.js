/********************************************************************
modalsetarformapagamento.js

Library javascript para o modalsetarformapagamento.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nUserID = 0;
var dsoGravacao = new CDatatransport('dsoGravacao');
// Controla se algum dado do grid foi alterado

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
chks_onclick()
sels_onchange()
btn_onclick(ctl)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

	fillControlsData();
	
	getDataServer_DSC();
}

function getDataServer()
{
	var nLimiteCredito = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'LimiteCredito' + '\'' + '].value');
	
	if (isNaN(nLimiteCredito))
		nLimiteCredito = 0;
	
	setConnection(dsoGravacao);
	dsoGravacao.SQL = 'SELECT a.ConceitoID, ISNULL(b.Bloquear, 0), ' +
			'b.FinanciamentoPadraoID, b.FormaPagamentoID, b.NivelPagamento, ' +
        'FROM RelacoesPessoas a WITH(NOLOCK) ' +
            'INNER JOIN Pessoas_Creditos b WITH(NOLOCK) ON (b.PessoaID = a.SujeitoID) ' +
		'WHERE a.RelacaoID = ' + glb_nPedParcelaID;

	dsoGravacao.ondatasetcomplete = getDataServer_DSC;
	dsoGravacao.Refresh();
}

function getDataServer_DSC()
{	
    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalsetarformapagamentoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    showExtFrame(window, true);
    
    if (!glb_bCanChange)
    {
		if ( window.top.overflyGen.Alert('Esta Forma de Pagamento n�o pode ser setada como default.') == 0 )
		    return null;

		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null); 		    
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Forma de Pagamento default', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var y_Gap = 0;
    var div_Width = 0;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // reajusta dimensoes e reposiciona a janela
	redimAndReposicionModalWin(320, 160, false);
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    elem1 = lblTexto;
	with (elem1.style)
    {
		left = 0;
		top = 0;
		fontSize = '8pt';
		width = (elem1.innerText.length)*5;
	    height = '12pt';
	    backgroundColor = 'transparent';
	}
    y_Gap = elem1.offsetTop + elem1.offsetHeight + ELEM_GAP;
    div_Width = elem1.offsetLeft + elem1.offsetWidth;
    
    elem = rdFormaAtual;
    elem1 = lblFormaAtual;
    with (elem.style)
    {
		left = 0;
		top = y_Gap;
		width = '10pt';
	    height = '10pt';
	}
	with (elem1.style)
    {
		left = elem.offsetLeft + elem.offsetWidth + (ELEM_GAP / 2);
		top = elem.offsetTop - 2;
		width = (elem1.innerText.length)*FONT_WIDTH;
	    height = '12pt';
		fontSize = '10pt';
	    backgroundColor = 'transparent';
	}
	elem1.onclick = lbl_onclick;
	
	div_Width = Math.max( div_Width, elem1.offsetLeft + elem1.offsetWidth);
	y_Gap = elem1.offsetTop + elem1.offsetHeight + ELEM_GAP;
	
    elem = rdFormaUltimoPedido;
    elem1 = lblFormaUltimoPedido;
    with (elem.style)
    {
		left = 0;
		top = y_Gap;
		width = '10pt';
	    height = '10pt';
	}
	with (elem1.style)
    {
		left = elem.offsetLeft + elem.offsetWidth + (ELEM_GAP / 2);
		top = elem.offsetTop - 2;
		width = (elem1.innerText.length)*6;
	    height = '12pt';
	    fontSize = '10pt';
	    backgroundColor = 'transparent';
	}
    elem1.onclick = lbl_onclick;
    
    div_Width = Math.max( div_Width, elem1.offsetLeft + elem1.offsetWidth);
    
	// ajusta o divFields
    elem = divFields;
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = div_Width;    
        
        left = btnOK.offsetLeft + ((btnCanc.offsetLeft + btnCanc.offsetWidth - btnOK.offsetLeft) / 2) -
			   (div_Width / 2);
        
        height = lblFormaUltimoPedido.offsetTop + lblFormaUltimoPedido.offsetHeight + 2;
    }
    
}

/********************************************************************
Usuario clicou label de radio
********************************************************************/
function lbl_onclick()
{
	if (this == lblFormaAtual)
	{
		rdFormaAtual.checked = true;
		rdFormaUltimoPedido.checked = false;
	}
	else if (this == lblFormaUltimoPedido)
	{
		rdFormaAtual.checked = false;
		rdFormaUltimoPedido.checked = true;
	}
}

/********************************************************************
Usuario teclou Enter em campo texto
********************************************************************/
function txt_onkeydown()
{
	if ( event.keyCode == 13 )	
		;
}

/********************************************************************
Usuario clicou em checkbox
********************************************************************/
function chks_onclick()
{
	;
}

/********************************************************************
Usuario alterou selecao de combo
********************************************************************/
function sels_onchange()
{
	adjustLabelCombos(this);

	if (this == selConceitoID)
	{
		adjustBtnsStatus();
	}
}

function adjustLabelCombos(elem)
{
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		gravaFormaPagamento();

		return null;
    }
    else if (controlID == 'btnCanc')
    {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null); 
    }
}

function gravaFormaPagamento()
{
	if ((!rdFormaAtual.checked) && (!rdFormaUltimoPedido.checked))
	{
		if ( window.top.overflyGen.Alert('Selecione uma Forma de Pagamento.') == 0 )
		    return null;
		    
		return null;		    
	}
	
	lockControlsInModalWin(true);
	
	var strPars = new String();
	var sFormaPagamentoID = '';
	
	if (rdFormaAtual.checked)
		sFormaPagamentoID = rdFormaAtual.getAttribute('ItemID', 1);
	else
		sFormaPagamentoID = rdFormaUltimoPedido.getAttribute('ItemID', 1);
		
	strPars = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/setaformapagamento.aspx';
	strPars += '?nRelacaoID=' + escape(glb_nRelacaoID);
	strPars += '&sFormaPagamentoID=' + escape(sFormaPagamentoID);
	
	dsoGravacao.URL = strPars;
	dsoGravacao.ondatasetcomplete = gravacao_DSC;
	dsoGravacao.Refresh();
}

function gravacao_DSC()
{
    if (! (dsoGravacao.recordset.BOF && dsoGravacao.recordset.EOF) )
    {
		if ( window.top.overflyGen.Alert(dsoGravacao.recordset['Verificacao'].value) == 0 )
		    return null;
    }
    
	// Simula o botao OK para fechar a janela e forcar um refresh no sup
	sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
}

/********************************************************************
Preenche os campos de dado
********************************************************************/
function fillControlsData()
{
	lockControlsInModalWin(false);
}


function adjustBtnsStatus()
{
	;
}
