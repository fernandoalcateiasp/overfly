/********************************************************************
modalbaixacomissoes.js

Library javascript para o modalbaixacomissoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Controla se as colunas OK e Observacoes devem ser readOnly e funcao
// do Estado da OP

var dsoGen01 = new CDatatransport('dsoGen01');
var dsoGen02 = new CDatatransport('dsoGen02');
var dsoGen03 = new CDatatransport('dsoGen03');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_pedidos_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalbaixacomissoes_AfterEdit(Row, Col)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalbaixacomissoes_BeforeEdit(grid, row, col)
{
	;
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    with (modalbaixacomissoesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
	startPesq();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Demonstrativo de baixa de comiss�es', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        // backgroundColor = 'transparent';
        backgroundColor = 'red';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        // top = y_gap;
        width = modWidth - frameBorder - (2 * ELEM_GAP);    
        height = parseInt(btnOK.currentStyle.top, 10) +
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10);
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    btnOK.value = 'Fechar';
	btnOK.style.left = (parseInt(document.getElementById('divFG').currentStyle.width,10) /2) -
					   (parseInt(btnOK.style.width, 10) / 2);

	btnOK.style.visibility = 'hidden';
	btnCanc.style.visibility = 'hidden';
					   
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modaleventoscontabeis_contasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    fg.focus();
}

function startPesq()
{
    lockControlsInModalWin(true);
    strSQL = 'SELECT (CASE WHEN b.PedidoID > 0 THEN dbo.fn_Pessoa_Fantasia(b.EmpresaID, 0) ELSE dbo.fn_Pessoa_Fantasia(c.EmpresaID, 0) END) AS Empresa, ' +
                   '(CASE WHEN b.PedidoID > 0 THEN dbo.fn_Pessoa_Fantasia(b.PessoaID, 0) ELSE dbo.fn_Pessoa_Fantasia(c.PessoaID, 0) END) AS Pessoa, ' +
                   '(CASE WHEN b.PedidoID > 0 THEN dbo.fn_Pessoa_Fantasia(b.ParceiroID, 0) ELSE dbo.fn_Pessoa_Fantasia(c.ParceiroID, 0) END) AS Parceiro, ' +
                   'b.PedidoID, c.FinanceiroID, ' +
                   '(CASE WHEN b.PedidoID > 0 THEN d.dtNotaFiscal ELSE g.dtNotaFiscal END) AS dtNotaFiscal, ' +
                   '(CASE WHEN b.PedidoID > 0 THEN d.NotaFiscal ELSE g.NotaFiscal END) AS NotaFiscal, a.Valor AS ValorAssociado, dbo.fn_Pessoa_Fantasia(a.UsuarioID, 0) AS Usuario, ' +
                   'a.dtAssociacao ' +
                'FROM Financeiro_Comissoes a WITH(NOLOCK) ' +
                        'LEFT JOIN Pedidos b WITH(NOLOCK) ON a.PedidoID = b.PedidoID ' +
                        'LEFT JOIN Financeiro c WITH(NOLOCK) ON a.FinanceiroID = c.FinanceiroID ' +
                        'LEFT JOIN NotasFiscais d WITH(NOLOCK) ON b.NotaFiscalID = d.NotaFiscalID ' +
                        'LEFT JOIN Pedidos e WITH(NOLOCK) ON c.PedidoID = e.PedidoID ' +
                        'LEFT JOin NotasFiscais g WITH(NOLOCK) ON e.NotaFiscalID = g.NotaFiscalID ' +
                'WHERE a.PedParcelaID =' + glb_nPedParcelaID + ' ';
                'ORDER BY Empresa, dtAssociacao';  

    setConnection(dsoGen01);
    
    dsoGen01.SQL = strSQL;
    dsoGen01.ondatasetcomplete = startPesq_DSC;
    dsoGen01.Refresh();
}

function startPesq_DSC()
{
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';
    
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    
    headerGrid(fg,['Empresa',
                   'Pessoa',
                   'Parceiro',
                   'Pedido',
                   'Financeiro',
                   'Data NF',
                   'Nota Fiscal',
                   'Valor Associado',
                   'Usu�rio',
                   'Data Associa��o'], []);
    fillGridMask(fg,dsoGen01,['Empresa',
                              'Pessoa',
							  'Parceiro',
							  'PedidoID',
							  'FinanceiroID',
							  'dtNotaFiscal' ,
							  'NotaFiscal',
							  'ValorAssociado',
							  'Usuario',
							  'dtAssociacao'],
                              ['', '', '', '', '', '', '', '999999999.99', '', ''],
                              ['', '', '', '', '', dTFormat, '', '(#,###,###,##0.00)', '', dTFormat + ' hh:mm:ss']);

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[7, '###,###,###,###.00', 'S']]);
    
    alignColsInGrid(fg,[0, 3, 4, 5, 6, 7, 8, 9]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    fg.Redraw = 2;
    
    if (fg.Rows > 1)
		fg.Row = 1;
    
    lockControlsInModalWin(false);
    btnOK.disabled = false;
	fg.focus();
}


