<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalreferenciaHtml" name="modalreferenciaHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalreferencia.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/frmpedidos/modalpages/modalreferencia.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "var glb_USERID = 0;"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nEmpresaID, nPedidoID, nPessoaID, nTipoPessoa 
Dim nVariacao, nPercentualSUP, nReferencia, nQuantidadeItens, nNumeroMaxItens, nArredondaValor, bPedidoImportacao, produtoDesativo

nEmpresaID = 0
nPedidoID = 0
nPessoaID = 0
nTipoPessoa = 0
nVariacao = 0
nPercentualSUP = 0
nReferencia = 0
nQuantidadeItens = 0
nNumeroMaxItens = 0
nArredondaValor = 0
bPedidoImportacao = 0
produtoDesativo = 0

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nPedidoID").Count    
    nPedidoID = Request.QueryString("nPedidoID")(i)
Next

For i = 1 To Request.QueryString("nPessoaID").Count    
    nPessoaID = Request.QueryString("nPessoaID")(i)
Next

For i = 1 To Request.QueryString("nTipoPessoa").Count    
    nTipoPessoa = Request.QueryString("nTipoPessoa")(i)
Next

For i = 1 To Request.QueryString("nVariacao").Count    
    nVariacao = Request.QueryString("nVariacao")(i)
Next

For i = 1 To Request.QueryString("nPercentualSUP").Count    
    nPercentualSUP = Request.QueryString("nPercentualSUP")(i)
Next

For i = 1 To Request.QueryString("nReferencia").Count    
    nReferencia = Request.QueryString("nReferencia")(i)
Next

For i = 1 To Request.QueryString("nQuantidadeItens").Count    
    nQuantidadeItens = Request.QueryString("nQuantidadeItens")(i)
Next

For i = 1 To Request.QueryString("nNumeroMaxItens").Count    
    nNumeroMaxItens = Request.QueryString("nNumeroMaxItens")(i)
Next

For i = 1 To Request.QueryString("nArredondaValor").Count    
    nArredondaValor = Request.QueryString("nArredondaValor")(i)
Next

For i = 1 To Request.QueryString("produtoDesativo").Count    
    produtoDesativo = Request.QueryString("produtoDesativo")(i)
Next

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nPedidoID = " & CStr(nPedidoID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nPessoaID = " & CStr(nPessoaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nTipoPessoa = " & CStr(nTipoPessoa) & ";"
Response.Write vbcrlf

Response.Write "var glb_nVariacao = " & CStr(nVariacao) & ";"
Response.Write vbcrlf

Response.Write "var glb_nReferencia = " & CStr(nReferencia) & ";"
Response.Write vbcrlf

Response.Write "var glb_nPercentualSUP = " & CStr(nPercentualSUP) & ";"
Response.Write vbcrlf

Response.Write "var glb_nQuantidadeItens = " & CStr(nQuantidadeItens) & ";"
Response.Write vbcrlf

If (CInt(bPedidoImportacao) = 1) Then
    Response.Write "var glb_nNumeroMaxItens = 999;"
Else
    Response.Write "var glb_nNumeroMaxItens = " & CStr(nNumeroMaxItens) & ";"
End If
Response.Write vbcrlf

Response.Write "var glb_nArredondaValor = " & CStr(nArredondaValor) & ";"
Response.Write vbcrlf

Response.Write "var glb_produtoDesativo = " & CStr(produtoDesativo) & ";"
Response.Write vbcrlf

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
js_pedidos_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>


<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg2 EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange2 (fg2, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg2 EVENT=CellChanged>
<!--
 js_ModalReferencia_ValidateEditPesq (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_ModalReferencia_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

</head>

<body id="modalreferenciaBody" name="modalreferenciaBody" LANGUAGE="javascript" onload="return window_onload()">
   
    <div id="divFields" name="divFields" class="divGeneral">
        <p id="lblPedidoReferencia" name="lblPedidoReferencia" class="lblGeneral">Pedido</p>
        <input type="text" id="txtPedidoReferencia" name="txtPedidoReferencia" class="fldGeneral" title="Pedido de Refer�ncia"></input>
        <p id="lblNotaFiscalReferencia" name="lblNotaFiscalReferencia" class="lblGeneral">Nota Fiscal</p>
        <input type="text" id="txtNotaFiscalReferencia" name="txtNotaFiscalReferencia" class="fldGeneral" title="Nota fiscal de Refer�ncia"></input>
        <input type="button" id="btnFindReferencia" name="btnFindReferencia" value="Listar" LANGUAGE="javascript" onclick="return btnFindReferencia_onclick(this)" class="btns">        
        
    </div>
    <div id="divPesquisa" name="divPesquisa" class="divGeneral">
        <p id="lblProdutoID" name="lblProdutoID" class="lblGeneral">ID</p>
        <input type="text" id="txtProdutoID" name="txtProdutoID" class="fldGeneral"></input>
        <p id="lblProduto" name="lblProduto" class="lblGeneral">Produto</p>
        <select id="selProduto" name="selProduto" class="fldGeneral"></select>
        <p id="lblMarca" name="lblMarca" class="lblGeneral">Marca</p>
        <select id="selMarca" name="selMarca" class="fldGeneral"></select>
        <p id="lblCaracteristica" name="lblCaracteristica" class="lblGeneral">Caracter�stica</p>
        <select id="selCaracteristica" name="selCaracteristica" class="fldGeneral"></select>                                
        <p id="lblFiltro" name="lblFiltro" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro" name="txtFiltro" class="fldGeneral"></input>
        <p id="lblHomologacao" name="lblHomologacao" class="lblGeneral">H</p>
        <input type="checkbox" id="chkHomologacao" name="chkHomologacao" value="11" class="fldGeneral" title="Homologa��o"></input>
        <p id="lblPreLancamento" name="lblPreLancamento" class="lblGeneral">R</p>
        <input type="checkbox" id="chkPreLancamento" name="chkPreLancamento" value="12" class="fldGeneral" title="Pr� Lan�amento"></input>
        <p id="lblLancamento" name="lblLancamento" class="lblGeneral">L</p>
        <input type="checkbox" id="chkLancamento" name="chkLancamento" value="13" class="fldGeneral" title="Lan�amento"></input>
        <p id="lblAtivo" name="lblAtivo" class="lblGeneral">A</p>
        <input type="checkbox" id="chkAtivo" name="chkAtivo" value="2" class="fldGeneral" title="Ativo"></input>
        <p id="lblPromocao" name="lblPromocao" class="lblGeneral">P</p>
        <input type="checkbox" id="chkPromocao" name="chkPromocao" value="14" class="fldGeneral" title="Promo��o"></input>
        <p id="lblPontaEstoque" name="lblPontaEstoque" class="lblGeneral">O</p>
        <input type="checkbox" id="chkPontaEstoque" name="chkPontaEstoque" value="15" class="fldGeneral" title="Ponta de Estoque"></input>
        <p id="lblChave" name="lblChave" class="lblGeneral">Chave</p>
        <select id="selChave" name="selChave" class="fldGeneral">
			<option value="ProdutoID" selected>ID</option>
			<option value="Produto">Produto</option>
        </select>
        <p id="lblValorLimite" name="lblValorLimite" class="lblGeneral">Valor Limite</p>
        <input type="text" id="txtValorLimite" name="txtValorLimite" class="fldGeneral"></input>
        <p id="lblPalavraChave" name="lblPalavraChave" class="lblGeneral">Palavra Chave</p>
        <input type="text" id="txtPalavraChave" name="txtPalavraChave" class="fldGeneral"></input>
    </div>
    
    <div id="divPesCon" name="divPesCon" class="divGeneral">
        <p id="lblConceitoID" name="lblConceitoID" class="lblGeneral">ID</p>
        <input type="text" id="txtConceitoID" name="txtConceitoID" class="fldGeneral"></input>
        <p id="lblQuantidade" name="lblQuantidade" class="lblGeneral">Quantidade</p>
        <input type="text" id="txtQuantidade" name="txtQuantidade" class="fldGeneral"></input>
        <input type="button" id="btnFindPesCon" name="btnFindPesCon" value="Listar" LANGUAGE="javascript" onclick="return btnFindPesCon_onclick(this)" class="btns">
    </div>

    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" class="fldGeneral" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
    
    <div id="divFG2" name="divFG2" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg2" name="fg2" class="fldGeneral" VIEWASTEXT>
        </object>
        <img id="Img1" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="Img2" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="Img3" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
    
    <div id="divFG3" name="divFG3" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg3" name="fg3" class="fldGeneral" VIEWASTEXT>
        </object>
        <img id="Img4" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="Img5" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="Img6" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>   
    
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    <input type="button" id="btnTrocar" name="btnTrocar" value="Trocar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
