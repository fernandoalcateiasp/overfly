// VARIAVEIS GLOBAIS ************************************************
var glb_Print_2nd_Copy = null;
var glb_timerListaEmbalagem = null;
var glb_bPrintPreview = null;

var glb_nDsosNota_Extra_ShipTo = 0;

var strParameters;

var glbContRel = 0;


// FINAL DE VARIAVEIS GLOBAIS ***************************************

/****************************************************************************
INICIO DAS FUNCOES DE IMPRESSAO DE NOTA FISCAIS
****************************************************************************/

/********************************************************************
Impressao da Nota Fiscal (ID=40151)
********************************************************************/
function imprimeNotaFiscal() {
    //Controla a quantidade de dsos para impressao da nota
    glb_ndsosNota = 5;

    var nNotaCorrente = glb_nNotaFiscalID;
    var nEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    

    //Header - Nota Fiscal  
    setConnection(dsoHeader);

    dsoHeader.SQL =
        'SELECT ' +
            'NF.NotaFiscalID, Imagens.Arquivo AS FotoEmpresa, ' +
            '(CASE NF.TipoNotaFiscalID WHEN 601 THEN ' + '\'' + 'X' + '\'' + ' ELSE SPACE(0) END) AS Entrada, ' +
            '(CASE NF.TipoNotaFiscalID WHEN 602 THEN ' + '\'' + 'X' + '\'' + ' ELSE SPACE(0) END) AS Saida, ' +
            'STR(NF.NotaFiscal) AS NotaFiscal, CONVERT(VARCHAR, NF.dtNotaFiscal, ' + DATE_SQL_PARAM + ') AS dtNotaFiscal, ' +
            'ISNULL(CONVERT(VARCHAR, NF.dtEntradaSaida, ' + DATE_SQL_PARAM + '), SPACE(0)) AS dtEntradaSaida, ' +
		    'ISNULL(CONVERT(VARCHAR(2), dbo.fn_Pad(DATEPART(hh, NF.dtEntradaSaida), 2, CHAR(48), CHAR(76))) + CHAR(58) + CONVERT(VARCHAR(2), dbo.fn_Pad(DATEPART(mi, NF.dtEntradaSaida), 2, CHAR(48), CHAR(76))), SPACE(0)) AS HoraEntradaSaida, ' +
            'NF.CFOPs, NF.Natureza, NF.TransacaoID AS TransacaoID, STR(NF.PedidoID) AS PedidoID, STR(NF.Formulario) AS Formulario, NFDest.Nome, ' +
            'STR(NFDest.PessoaID) AS PessoaID, ISNULL(STR(NFTransp.PessoaID), SPACE(0)) AS TransportadoraID, ' +
            'ISNULL(NFDest.DocumentoFederal, SPACE(0)) AS Documento1, ISNULL(NFDest.DocumentoMunicipal, SPACE(0)) AS DocumentoMunicipal, ' +
            '(CASE ISNULL(PessoaEnderecoPais.EnderecoInvertido, 0) WHEN 1 ' +
                'THEN (ISNULL(NFDest.Numero, SPACE(0)) + \' \' + ISNULL(NFDest.Endereco, SPACE(0)) +  \' \' + ISNULL(NFDest.Complemento, SPACE(0))) ' +
                'ELSE (ISNULL(NFDest.Endereco, SPACE(0)) +  \' \' + ISNULL(NFDest.Numero, SPACE(0)) + \' \' + ISNULL(NFDest.Complemento, SPACE(0))) END)  AS Endereco, ' +
            'ISNULL(NFDest.Bairro, SPACE(0)) AS Bairro, NFDest.CEP, NFDest.Cidade, NFDest.UF, NFDest.Pais, UPPER(LEFT(NFDest.Pais, 2)) AS PaisAbreviado, ' +
            '(ISNULL(NFDest.DDI, SPACE(0)) + ISNULL(NFDest.DDD, SPACE(0)) + \' \' + ISNULL(NFDest.Telefone, SPACE(0))) AS Telefone, ' +
            'ISNULL(NFDest.DocumentoEstadual, SPACE(0)) AS Documento2, ' +
            'NFISS.ValorImposto AS ValorTotalImposto3, NFISS.BaseCalculo AS BaseCalculoImposto3, LEFT(ROUND(((NFISS.ValorImposto/NFISS.BaseCalculo)*100),2),4) as AliquotaISS, NF.ValorTotalServicos, NFICMS.BaseCalculo AS BaseCalculoImposto1, NFICMS.ValorImposto AS ValorTotalImposto1, ' +
            'NFICMSST.BaseCalculo AS BaseCalculoImposto1Subst, NFICMSST.ValorImposto AS ValorTotalImposto1Subst, NF.ValorTotalProdutos, ISNULL(NF.ValorFrete, 0) AS ValorFrete, ' +
            'ISNULL(NF.ValorSeguro, 0) AS ValorSeguro, ISNULL(NF.OutrasDespesas, 0) AS OutrasDespesas, ' +
            '(CASE NF.EmpresaID WHEN 7 THEN ISNULL(NFTAX.ValorImposto, 0) ELSE ISNULL(NFIPI.ValorImposto, 0) END) AS ValorTotalImposto2, NF.ValorTotalNota, ' +
            'ISNULL(NFTransp.Nome,SPACE(0)) AS Transportadora, ISNULL(NF.Frete, SPACE(0)) AS Frete, ' +
            '(CASE ISNULL(PessoaEnderecoPais2.EnderecoInvertido, 0) WHEN 1 ' +
                'THEN (ISNULL(NFTransp.Numero, SPACE(0)) + \' \' + ISNULL(NFTransp.Endereco, SPACE(0)) +  \' \' + ISNULL(NFTransp.Complemento, SPACE(0))) ' +
                'ELSE (ISNULL(NFTransp.Endereco, SPACE(0)) +  \' \' + ISNULL(NFTransp.Numero, SPACE(0)) + \' \' + ISNULL(NFTransp.Complemento, SPACE(0))) END)  AS TranspEndereco, ' +
            'ISNULL(NFTransp.Cidade, SPACE(0)) AS TranspCidade, ' +
            'ISNULL(NFTransp.Bairro,SPACE(0)) AS TranspBairro, ISNULL(NFTransp.CEP,SPACE(0)) AS TranspCEP, ' +
            'ISNULL(NFTransp.UF,SPACE(0)) AS TranspUF, ISNULL(NFTransp.Pais,SPACE(0)) AS TranspPais, ' +
            '(ISNULL(NFTransp.DDI, SPACE(0)) + ISNULL(NFTransp.DDD, SPACE(0)) + \' \' + ISNULL(NFTransp.Telefone, SPACE(0))) AS TranspTelefone, ' +
            'ISNULL(NFTransp.DocumentoFederal,SPACE(0)) AS TranspDocumento1, ISNULL(NFTransp.DocumentoEstadual,SPACE(0)) AS TranspDocumento2, ' +
            'ISNULL(NF.PlacaVeiculo, SPACE(0)) AS PlacaVeiculo, ISNULL(NF.UFVeiculo, SPACE(0)) AS UFVeiculo, ' +
            'STR(ISNULL(NF.Quantidade, 0)) AS Quantidade, ' +
            'NF.Especie, ISNULL(dbo.fn_Tradutor(NF.MeioTransporte, ' + nEmpresaData[7] + ', ' + nEmpresaData[8] + ', NULL),SPACE(0)) AS MeioTransporte, ' +
            'NF.PesoBruto, NF.PesoLiquido, NF.Vendedor, ' +
            'ISNULL(NF.SeuPedido, SPACE(1)) AS SeuPedido, ' +
            '(SELECT ISNULL(CONVERT(VARCHAR(5), CONVERT(NUMERIC(3),PrazoMedioPagamento)) + \' Days\', SPACE(0)) FROM Pedidos WITH(NOLOCK) WHERE Pedidos.PedidoID=NF.PedidoID) AS PMP, ' +
            '(SELECT (CASE SUBSTRING(Observacao,1,1) WHEN \'/\' THEN SUBSTRING(Observacao,2,39) ELSE \'Miami\' END) ' +
            'FROM Pedidos WITH(NOLOCK) WHERE Pedidos.PedidoID=NF.PedidoID) AS CidadeEmpresa, ' +
		    'ISNULL((SELECT dbo.fn_Tradutor(bb.ItemAbreviado, ' + nEmpresaData[7] + ', ' + nEmpresaData[8] + ', NULL) FROM ' +
			    'Pedidos aa, TiposAuxiliares_Itens bb ' +
			    'WHERE (aa.NotaFiscalID = NF.NotaFiscalID AND ISNULL(aa.ModalidadeTransporteID, 621) = bb.ItemID)), SPACE(0)) AS Incoterm, ' +
			   '(SELECT TOP 1 ISNULL(LTRIM(RTRIM(Observacao)), SPACE(0)) FROM Pedidos WITH(NOLOCK) WHERE Pedidos.PedidoID=NF.PedidoID) AS ObservacoesInvoice ' +
           'FROM NotasFiscais NF WITH(NOLOCK) ' +
            'LEFT OUTER JOIN [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] Imagens WITH(NOLOCK) ON (NF.EmpresaID = Imagens.RegistroID AND Imagens.FormID=1210 AND Imagens.SubFormID=20100 AND Imagens.TipoArquivoID = 1451) ' +
            'INNER JOIN NotasFiscais_Pessoas NFDest WITH(NOLOCK) ON (NFDest.NotaFiscalID = NF.NotaFiscalID) AND (NFDest.TipoID = 791) ' + //Destinatario
            'LEFT OUTER JOIN NotasFiscais_Pessoas NFTransp WITH(NOLOCK) ON (NFTransp.NotaFiscalID = NF.NotaFiscalID) AND (NFTransp.TipoID = 793) ' +
            'LEFT OUTER JOIN NotasFiscais_Impostos NFIPI WITH(NOLOCK) ON (NFIPI.NotaFiscalID = NF.NotaFiscalID) AND (NFIPI.ImpostoID = 8) ' +
            'LEFT OUTER JOIN NotasFiscais_Impostos NFICMS WITH(NOLOCK) ON (NFICMS.NotaFiscalID = NF.NotaFiscalID) AND (NFICMS.ImpostoID = 9) ' +
            'LEFT OUTER JOIN NotasFiscais_Impostos NFISS WITH(NOLOCK) ON (NFISS.NotaFiscalID = NF.NotaFiscalID) AND (NFISS.ImpostoID = 10) ' +
            'LEFT OUTER JOIN NotasFiscais_Impostos NFICMSST WITH(NOLOCK) ON (NFICMSST.NotaFiscalID = NF.NotaFiscalID) AND (NFICMSST.ImpostoID = 25) ' +
            'LEFT OUTER JOIN NotasFiscais_Impostos NFTAX WITH(NOLOCK) ON (NFTAX.NotaFiscalID = NF.NotaFiscalID) AND (NFTAX.ImpostoID = 22) ' +
            'LEFT OUTER JOIN Localidades PessoaEnderecoPais WITH(NOLOCK) ON (NFDest.PaisID = PessoaEnderecoPais.LocalidadeID) ' +
            'LEFT OUTER JOIN Localidades PessoaEnderecoPais2 WITH(NOLOCK) ON (NFTransp.PaisID = PessoaEnderecoPais2.LocalidadeID) ' +
        'WHERE NF.NotaFiscalID = ' + nNotaCorrente;

    dsoHeader.ondatasetcomplete = dsoNotaFiscal_DSC;
    dsoHeader.refresh();

    // Detalhe 1 - Fatura
    setConnection(dsoDetail01);

    dsoDetail01.SQL = 'SELECT a.NotaFiscalID, a.Duplicata, ' +
					    'CONVERT(VARCHAR, a.Vencimento, ' + DATE_SQL_PARAM + ') AS Vencimento, ' +
					    'a.Valor, ' +
					    'CONVERT(VARCHAR(3), DATEDIFF(dd, b.dtNotaFiscal, a.Vencimento)) AS Prazo ' +
                      'FROM NotasFiscais_Duplicatas a WITH(NOLOCK), NotasFiscais b WITH(NOLOCK) ' +
                      'WHERE (a.NotaFiscalID = ' + nNotaCorrente + ' AND a.NotaFiscalID = b.NotaFiscalID AND b.EstadoID=67) ' +
                      'ORDER BY a.Vencimento ';

    dsoDetail01.ondatasetcomplete = dsoNotaFiscal_DSC;
    dsoDetail01.refresh();

    // Detalhe 2 - Produto
    setConnection(dsoDetail02);

    dsoDetail02.SQL = 'SELECT a.NotaFiscalID, STR(a.ProdutoID) AS ItemID, a.Familia AS Produto, a.Marca, a.Modelo,  ISNULL(LEFT(a.Descricao,28), SPACE(1)) AS Descricao, ' +
                      'ISNULL(a.CFOP, SPACE(1)) AS CFOP, ' +
                      'ISNULL(LEFT(a.Descricao,20), SPACE(1)) AS DescricaoReduzida, ' +
                      'ISNULL(a.InformacoesAdicionais, ISNULL(dbo.fn_NotaFiscalItem_Mensagem(a.PedItemID), SPACE(0))) AS PaisOrigem, ' +
                      '(CASE WHEN (dbo.fn_Produto_PesosMedidas(a.ProdutoID, 0, 1)) > 0 THEN \'Net(Kg): \' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),a.Quantidade * dbo.fn_Produto_PesosMedidas(a.ProdutoID, 0, 1))) ELSE SPACE(0) END) AS PesoLiquido, ' +
                      '(CASE WHEN (dbo.fn_Produto_PesosMedidas(a.ProdutoID, 1, 1)) > 0 THEN \'Gross(Kg): \' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),a.Quantidade * dbo.fn_Produto_PesosMedidas(a.ProdutoID, 1, 1))) ELSE SPACE(0) END) AS PesoBruto, ' +
                      'ISNULL(a.NCM, SPACE(1)) AS ClassificacaoFiscal, (ISNULL(a.Origem, 0) + ISNULL(d.CodigoTributacao, 0)) AS SituacaoTributaria, a.Unidade, STR(a.Quantidade) AS Quantidade, ' +
                      'a.ValorUnitario, a.ValorTotal, ' +
                      'STR(d.Aliquota) AS AliquotaImposto1, STR(e.Aliquota) AS AliquotaImposto2, ' +
                      'd.Aliquota AS AliquotaImposto1Modelo2, ' +
                      'e.Aliquota AS AliquotaImposto2Modelo2, e.ValorImposto AS ValorImposto2, CONVERT(BIT, ISNULL(c.EhMicro, 0)) AS EhMicro, ' +
                      'ISNULL(a.InformacoesAdicionais, ISNULL(dbo.fn_NotaFiscalItem_Mensagem(a.PedItemID), SPACE(0))) AS MensagemItem, ' +
                      'f.PartNumber AS PartNumber ' +
                      'FROM NotasFiscais_Itens a WITH(NOLOCK) ' +
                            'INNER JOIN Conceitos b WITH(NOLOCK) ON a.ProdutoID = b.ConceitoID ' +
                            'INNER JOIN Conceitos c WITH(NOLOCK) ON b.ProdutoID = c.ConceitoID ' +
                            'LEFT  JOIN Conceitos f WITH(NOLOCK) ON a.ProdutoID = f.ConceitoID ' +
                            'LEFT OUTER JOIN NotasFiscais_Itens_Impostos d WITH(NOLOCK) ON d.NFItemID = a.NFItemID AND d.ImpostoID = 9' + //ICMS
                            'LEFT OUTER JOIN NotasFiscais_Itens_Impostos e WITH(NOLOCK) ON e.NFItemID = a.NFItemID AND e.ImpostoID = 8 ' + //IPI
                      'WHERE a.Tipo = \'P\'' + ' AND a.NotaFiscalID = ' + nNotaCorrente;

    dsoDetail02.ondatasetcomplete = dsoNotaFiscal_DSC;
    dsoDetail02.refresh();

    // Detalhe 3 - Servico
    setConnection(dsoDetail03);

    dsoDetail03.SQL = 'SELECT Itens.NotaFiscalID, STR(Itens.ProdutoID) AS ItemID, Itens.Familia AS Produto, Itens.Marca, Itens.Modelo, ' +
                      '(CASE WHEN NF.EmpresaID = 4 THEN ISNULL(LEFT(Itens.Descricao,35), SPACE(1)) ELSE ISNULL(LEFT(Itens.Descricao,28), SPACE(1)) END) AS Descricao, ' +
                      'ISNULL(Itens.CFOP, SPACE(1)) AS CFOP, ISNULL(Itens.CodigoListaServico, SPACE(1)) AS CodigoListaServico, ' +
                      'Itens.Unidade, STR(Itens.Quantidade) AS Quantidade, ' +
                      'ISNULL(Itens.InformacoesAdicionais, ISNULL(dbo.fn_NotaFiscalItem_Mensagem(Itens.PedItemID), SPACE(0))) AS PaisOrigem, ' +
				      '(CASE WHEN (dbo.fn_Produto_PesosMedidas(Itens.ProdutoID, 0, 1)) > 0 THEN \'Net(Kg): \' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),Itens.Quantidade * dbo.fn_Produto_PesosMedidas(Itens.ProdutoID, 0, 1))) ELSE SPACE(0) END) AS PesoLiquido, ' +
                      '(CASE WHEN (dbo.fn_Produto_PesosMedidas(Itens.ProdutoID, 1, 1)) > 0 THEN \'Gross(Kg): \' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),Itens.Quantidade * dbo.fn_Produto_PesosMedidas(Itens.ProdutoID, 1, 1))) ELSE SPACE(0) END) AS PesoBruto, ' +
                      'Itens.ValorUnitario, Itens.ValorTotal AS ValorTotal ' +
                      'FROM NotasFiscais_Itens Itens WITH(NOLOCK) ' +
                        'INNER JOIN NotasFiscais NF WITH(NOLOCK) ON (NF.NotaFiscalID = Itens.NotaFiscalID AND NF.EstadoID=67) ' +
                      'WHERE Tipo = \'S\'' + ' AND Itens.NotaFiscalID = ' + nNotaCorrente;

    dsoDetail03.ondatasetcomplete = dsoNotaFiscal_DSC;
    dsoDetail03.refresh();
    
    // Detalhe 4 - Mensagem
    setConnection(dsoDetail04);

    dsoDetail04.SQL = 'SELECT NotaFiscalID, Mensagem AS Mensagem ' +
                      'FROM NotasFiscais_Mensagens WITH(NOLOCK) ' +
                      'WHERE NotaFiscalID = ' + nNotaCorrente + ' ' +
                      'ORDER BY Ordem';

    dsoDetail04.ondatasetcomplete = dsoNotaFiscal_DSC;
    dsoDetail04.refresh();
}

/********************************************************************
Criado pelo programador
Controla o retorno dos dsos da nota e inicia a impressao da nota

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoNotaFiscal_DSC() {
    glb_ndsosNota--;

    if (glb_ndsosNota > 0)
        return null;

    printInvoice();
}

/********************************************************************
Criado pelo programador
Imprime a Nota Fiscal no Formulario Modelo 3: Formato Letter (PrintJet)
Utilizado pelas empresas do EUA

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function printInvoice() {
    var nEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    setConnection(dsoExtra01);
    dsoExtra01.SQL = 'SELECT UPPER(Empresa.Nome) AS Nome, ' +
        'ISNULL(Empresa.Numero + SPACE(1), SPACE(0)) + ISNULL(Empresa.Endereco + SPACE(1), SPACE(0)) + ' +
        'ISNULL(Empresa.Complemento + SPACE(1), SPACE(0)) + ISNULL(Empresa.Bairro + SPACE(1), SPACE(0)) AS End1, ' +
        'ISNULL(Empresa.Cidade + SPACE(1), SPACE(0)) + ISNULL(Empresa.UF + SPACE(1), SPACE(0)) + ' +
        'ISNULL(Empresa.Pais + SPACE(1), SPACE(0)) AS End2, ' +
        'ISNULL(Empresa.CEP, SPACE(0)) AS CEP, ' +
        'ISNULL(Empresa.DDI, SPACE(0)) + ISNULL(Empresa.DDD, SPACE(0))+ SPACE(1) + ISNULL(Empresa.Telefone, SPACE(0)) AS Telefone, ' +
        'ISNULL(Empresa.DDI, SPACE(0)) + ISNULL(Empresa.DDD, SPACE(0))+ SPACE(1) + ISNULL(Empresa.Fax, SPACE(0)) AS Fax, Empresa.Fax AS nFax, ' +
        'SUBSTRING(dbo.fn_Pessoa_URL(Empresa.PessoaID, 125, NULL), 8, 72) AS URL, ' +
        'dbo.fn_Pessoa_URL(Empresa.PessoaID, 124, NULL) AS Email ' +
      'FROM NotasFiscais_Pessoas Empresa WITH(NOLOCK) ' +
      'WHERE (Empresa.TipoID = 790 AND Empresa.NotaFiscalID = ' + glb_nNotaFiscalID + ') ';

    dsoExtra01.ondatasetcomplete = printInvoice_ShipTo;
    dsoExtra01.refresh();
}

/********************************************************************
Criado pelo programador
Dados do ShipTo para
imprimir a Nota Fiscal no Formulario Modelo 3: Formato Letter (PrintJet)
Utilizado pelas empresas do EUA

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function printInvoice_ShipTo() {
    setConnection(dsoShipTo);

    dsoShipTo.SQL = 'SELECT NFPessoa.PessoaID, NFPessoa.Nome, NFPessoa.DocumentoFederal AS Documento1, NFPessoa.DocumentoEstadual AS Documento2, NFPessoa.Pais, NFPessoa.CEP, NFPessoa.UF, NFPessoa.Cidade, ' +
					    'NFPessoa.Bairro, ' +
					    '(CASE ISNULL(Localidades.EnderecoInvertido, 0) WHEN 1 ' +
                            'THEN (ISNULL(NFPessoa.Numero, SPACE(0)) + \' \' + ISNULL(NFPessoa.Endereco, SPACE(0)) +  \' \' + ISNULL(NFPessoa.Complemento, SPACE(0))) ' +
                            'ELSE (ISNULL(NFPessoa.Endereco, SPACE(0)) +  \' \' + ISNULL(NFPessoa.Numero, SPACE(0)) + \' \' + ISNULL(NFPessoa.Complemento, SPACE(0))) END)  AS Endereco, ' +
					    '(NFPessoa.DDI + NFPessoa.DDD + \' \' + NFPessoa.Telefone) AS Telefone ' +
					    'FROM NotasFiscais_Pessoas NFPessoa WITH(NOLOCK) ' +
                            'INNER JOIN Localidades Localidades WITH(NOLOCK) ON (NFPessoa.PaisID = Localidades.LocalidadeID) ' +
					    'WHERE NFPessoa.NotaFiscalID =' + glb_nNotaFiscalID + ' AND NFPessoa.TipoID = 792';

    dsoShipTo.ondatasetcomplete = printInvoice_DSC;
    dsoShipTo.refresh();
}

function printInvoice_DSC(bSecCopy) {
    var nTransacaoID;
    var bCustomsWarehouse = false;
    var term_Acquire_SoldTo = 'Sold to:';
    var mensagem;
    var MensagemInvoice;
    var mensagem2;
    var nEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    strParameters = "DATE_SQL_PARAM=" + DATE_SQL_PARAM + "&glb_nNotaFiscalID=" + glb_nNotaFiscalID +
                        "&nEmpresaData1=" + nEmpresaData[7] + "&nEmpresaData2=" + nEmpresaData[8] +
                        "&bCustomsWarehouse=" + bCustomsWarehouse;

   var frameReport = document.getElementById("frmReport");

    //frameReport.contentWindow.close();

    //frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;

   frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modcomercial/submodpedidos/frmpedidos/serverside/Invoice.aspx?" + strParameters + "&via2=false";   

    glbContRel = 1;
}

function reports_onreadystatechange() {

    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
        (frameReport.contentWindow.document.readyState == 'interactive') ||
        (frameReport.contentWindow.document.readyState == 'complete')) {
        //winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
        //        pb_StopProgressBar(true);
        //        lockControlsInModalWin(false);

        //        var strParameters = "DATE_SQL_PARAM=" + DATE_SQL_PARAM + "&glb_nNotaFiscalID=" + glb_nNotaFiscalID +
        //                        "&nEmpresaData1=" + nEmpresaData[7] + "&nEmpresaData2=" + nEmpresaData[8] +
        //                        "&bCustomsWarehouse=" + bCustomsWarehouse + "&via2=true";

        //        if (glbContRel == 1) {

        //            glbContRel = 2;

        //            window.document.location = SYS_PAGESURLROOT + '/PrintJet/Invoice.aspx?' + strParameters + "&via2=true";

        //           
        //        }

        //        window.print();

        //        lockControlsInModalWin(false);
    }
}
/****************************************************************************
FINAL DAS FUNCOES DE IMPRESSAO DE NOTA FISCAIS
****************************************************************************/
