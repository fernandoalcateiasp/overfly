var glb_timerListaEmbalagem = null;
var glb_bPrintPreview = null;

/********************************************************************
relatorioOrdemProducao

Parametros: nCaller: 0-> Funcao chamada pelo form Ordens de Producao
					 1-> Funcao chamada pelo form Pedidos
********************************************************************/
function relatorioOrdemProducao(nCaller, bPrintPreview)
{
	var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');
	var nOPID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'OrdemProducaoID' + '\'' + '].value');
	var sOPCondiditon = '';
	var aEmpresa = getCurrEmpresaData();
	var sEmpresaFantasia = aEmpresa[3];
    var dirA1;
    var dirA2;

	nPedidoID = (nPedidoID == null ? '' : nPedidoID);
	nOPID = (nOPID == null ? '' : nOPID);
	
	if (nCaller == 0)
		sOPCondiditon = 'OrdensProducao.OrdemProducaoID = ' + nOPID + ' AND ';

    jPrinter.ResetReport(1);
    jPrinter.LandScape = false;
    setPrinterPaperParams(jPrinter);
    

    if (bPrintPreview)
    {
		dirA1= valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
		dirA2= valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);
    
		if ((dirA1 != null) && (dirA1 != null))
		{
            //DireitoEspecifico
            //Ordens de Producao-> Ordens de Producao->Sup->Modal Print
            //Relatorio: 40172 -> Lista de Embalagem de OP
            //A1||A2 -> Impressao
            //A2 -> Zebrar
            //A1&&A2-> Excel
            //desabilitado -> Word 
            
			jPrinter.RightPrint = ((dirA1)||(dirA2));
			jPrinter.RightZebrar = (dirA2);
			jPrinter.RightExcel = ((dirA1)&&(dirA2));
			jPrinter.RightWord = false;
		}
	}
	
    jPrinter.Show_Fld_EmpresaLogada_TitleBar(true);
	jPrinter.Show_Fld_DateTime_TitleBar(true);
	jPrinter.ShowImage1_PageTitle(0, 0, 0, 0, false);
	jPrinter.BandaTotal_AlignToBottom(false);
    
    jPrinter.Dominium = SYS_ASPURLROOT;
    jPrinter.StrConn = getStrConnection();
    
    if ( window.document.getElementById('selReports') != null )
		jPrinter.Title = translateTerm(selReports.options[selReports.selectedIndex].innerText, null);
	else	
		jPrinter.Title = 'Lista de Embalagem de OP';
    
    jPrinter.SetBandHeight('T',20);
    jPrinter.SetBandHeight('PH',28);
    jPrinter.SetBandHeight('M',14);
    jPrinter.SetBandColor('PH', 0XC0C0C0);
    jPrinter.ShowLineInBand(1,'PH',0X000000,2,'N',13);
    jPrinter.ShowLineInBand(1,'M',0X000000,2,'T');
    jPrinter.SetBandHeight('PF',200);

    jPrinter.EmpresaLogada = sEmpresaFantasia;
    jPrinter.MasterQuerySQL = 
        'SELECT OrdensProducao.OrdemProducaoID, Pedidos.PedidoID, dbo.fn_Pad(CONVERT(VARCHAR(6), NotasFiscais.NotaFiscal), 6, ' + '\'' + '0' + '\'' + ', ' + '\'' + 'L' + '\'' + ') AS NotaFiscal, ' +
			'NotasFiscais.dtNotaFiscal, (Pessoas.Fantasia + SPACE(1) + ' + '\'' + '(' + '\'' + ' + CONVERT(VARCHAR(10), Pessoas.PessoaID) + ' + '\'' + ')' + '\'' + ' ) AS Cliente, ' + 
			'(CONVERT(VARCHAR(3), CONVERT(INTEGER, OrdensProducao.PrazoGarantia/12)) + SPACE(1) + ' + '\'' + 'anos at�' + '\'' + ' + SPACE(1) + CONVERT(VARCHAR, DATEADD(mm, OrdensProducao.PrazoGarantia, NotasFiscais.dtNotaFiscal), 103)) AS Garantia, ' +
			'CONVERT(INTEGER, OrdensProducao.PrazoGarantia/12) AS PrazoGarantia ,  ' +
			'CONVERT(VARCHAR, DATEADD(mm, OrdensProducao.PrazoGarantia, NotasFiscais.dtNotaFiscal), 103) AS dtGarantia ' +
	    'FROM Pedidos WITH(NOLOCK) ' +
	    'LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID = NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) ' +
	    'INNER JOIN Pessoas WITH(NOLOCK) ON (Pedidos.PessoaID = Pessoas.PessoaID) ' +
	    'INNER JOIN OrdensProducao WITH(NOLOCK) ON (Pedidos.PedidoID = OrdensProducao.PedidoID) ' +
	    'WHERE (' + sOPCondiditon + 'Pedidos.PedidoID = ' + nPedidoID + ') ' +
	    'ORDER BY OrdensProducao.OrdemProducaoID';

    jPrinter.MasterIndexFieldName = 'OrdemProducaoID';
    
    jPrinter.DetailQuerySQL01 = 
        'SELECT Itens.OrdemProducaoID, Itens.OrdItemID, Produtos.ConceitoID, ' +
			'(CASE ISNULL(Familias.EhMicro, 0) WHEN 1 THEN LEFT(Produtos.Conceito, 6) + ISNULL(SPACE(1) + dbo.fn_Pedido_ProdutoModelo(Pedidos.PedidoID, Produtos.ConceitoID), SPACE(0)) ELSE Produtos.Conceito END) AS Conceito, CONVERT(INTEGER, Itens.Quantidade) AS Quantidade, Itens.Observacao ' +
	    'FROM Pedidos WITH(NOLOCK), OrdensProducao WITH(NOLOCK), OrdensProducao_Itens Itens WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK) ' +
	    'WHERE (' + sOPCondiditon + ' Pedidos.PedidoID = ' + nPedidoID + ' AND Pedidos.PedidoID = OrdensProducao.PedidoID AND ' +
			'OrdensProducao.OrdemProducaoID = Itens.OrdemProducaoID AND Itens.ProdutoID = Produtos.ConceitoID AND ' +
			'Produtos.ProdutoID = Familias.ConceitoID) ' +
	    'ORDER BY OrdensProducao.OrdemProducaoID, dbo.fn_OrdemProducaoItem_Ordem(Itens.OrdItemID), Produtos.ConceitoID';
	jPrinter.DetailIndexFieldName01 = 'OrdemProducaoID';

    jPrinter.DetailQuerySQL02 = 
        'SELECT Itens.OrdemProducaoID, Itens.OrdItemID, Itens.ProdutoID, NumerosSerie.NumeroSerie, 1 AS Quantidade ' +
	'FROM OrdensProducao WITH(NOLOCK), OrdensProducao_Itens Itens WITH(NOLOCK), Pedidos WITH(NOLOCK), NumerosSerie_Movimento Movimentos WITH(NOLOCK), NumerosSerie WITH(NOLOCK) ' +
	'WHERE (' + sOPCondiditon + ' OrdensProducao.OrdemProducaoID = Itens.OrdemProducaoID AND ' +
		'OrdensProducao.PedidoID = Pedidos.PedidoID AND Pedidos.PedidoID = ' + nPedidoID + ' AND Pedidos.PedidoID = Movimentos.PedidoID AND ' +
		'OrdensProducao.OrdemProducaoID = Movimentos.OrdemProducaoID AND Movimentos.NumeroSerieID = NumerosSerie.NumeroSerieID AND ' +
		'Itens.ProdutoID = NumerosSerie.ProdutoID) ' +
	'ORDER BY dbo.fn_OrdemProducaoItem_Ordem(Itens.OrdItemID), Itens.ProdutoID, NumerosSerie.NumeroSerie';
	jPrinter.setBandCascade('D2', 'OrdItemID',  'OrdItemID');

    var lUseBold = true;
    var nFontTitleSize = 8;
    var lUseBoldSubDetail = false;
    var nLeft=0;
    var nTop = 0;

    jPrinter.ShowLineInBand(1,'PH',0X000000,2,'N',12);

	nLeft=0;

	jPrinter.AddTextInBand('Pedido: ', 0, nLeft, 'PH', true, 8, 'E');
    jPrinter.AddFieldFromBandToBand('M','PH','PedidoID', 0, nLeft+12 , true, 'E', 8);

	jPrinter.AddTextInBand('NF:', 0, nLeft+=28, 'PH', true, 8, 'E');
	jPrinter.AddFieldFromBandToBand('M','PH','NotaFiscal', 0, nLeft+6 , true, 'E', 8);
	jPrinter.AddFieldFromBandToBand('M','PH','dtNotaFiscal', 0, nLeft+=18 , true, 'E', 8);

	jPrinter.AddTextInBand('Cliente:', 0, nLeft+=34, 'PH', true, 8, 'E');
	jPrinter.AddFieldFromBandToBand('M','PH','Cliente', 0, nLeft+12 , true, 'E', 8);

	jPrinter.AddTextInBand('Garantia:', 0, nLeft+=69, 'PH', true, 8, 'E');
	jPrinter.AddFieldFromBandToBand('M','PH','Garantia', 0, nLeft+14 , true, 'E', 8);

    nLeft=0;

    jPrinter.AddTextInBand('Item', nTop+4, nLeft, 'PH', true, 8, 'E');

    jPrinter.AddTextInBand('ID', nTop+4, nLeft+=8, 'PH', true, 8, 'E');

    jPrinter.AddTextInBand('Produto', nTop+4, nLeft+=12, 'PH', true, 8, 'E');

    jPrinter.AddTextInBand('Qtd', nTop+4, nLeft+=40, 'PH', true, 8, 'E');

    jPrinter.AddTextInBand('N�mero S�rie', nTop+4, nLeft+=9, 'PH', true, 8, 'E');

    jPrinter.AddTextInBand('Observa��o', nTop+4, nLeft+=32, 'PH', true, 8, 'E');

    nLeft=0;

	jPrinter.AddTextInBand('OP:', 0.5, nLeft, 'M', true, 8, 'E');
	jPrinter.AddFieldInBand('M','OrdemProducaoID',0.5,nLeft+=6,true,'E',nFontTitleSize);

    nLeft=0;

    jPrinter.AddOrdemInBand(nTop,nLeft-9,'D1',true,nFontTitleSize,'D',false);

    jPrinter.AddFieldInBand('D1','ConceitoID',nTop,nLeft+=8,true,'E',nFontTitleSize);

    jPrinter.AddFieldInBand('D1','Conceito',nTop,nLeft+=12,true,'E',nFontTitleSize);

    jPrinter.AddFieldInBand('D1','Quantidade',nTop,nLeft+=30,true,'D',nFontTitleSize);

    jPrinter.AddFieldInBand('D2','NumeroSerie',nTop,nLeft+=19,lUseBoldSubDetail,'E',nFontTitleSize);

    jPrinter.AddFieldInBand('D1','Observacao',nTop,nLeft+=32,lUseBoldSubDetail,'E',nFontTitleSize);

	if (document.getElementById('chkImprimeTermoGarantia') != null)
	{
		if (chkImprimeTermoGarantia.checked)
		{
			jPrinter.ShowLineInBand(1, 'PF', 0, 2, 'T');

			jPrinter.AddTextInBand('TERMO DE GARANTIA',1,0,'PF',true,10,'E');
    
			nFontTitleSize = 8;
			nTop = 1;
			nTopGap = 3;
			nLeft1 = 0;
			nLeft2 = 5;
			nLeft3 = 10;
			nLeft4 = 15;
			nLeft5 = 20;

			jPrinter.AddTextInBand('TERMO DE GARANTIA',nTop,0,'PF',true,10,'E');

			jPrinter.AddTextInBand('1 Condi��es Gerais',nTop += 6,nLeft1,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-Garantia contra defeitos, desde que em condi��es normais de uso;',nTop += nTopGap,nLeft2,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-Garantia v�lida, se acompanhada de nota fiscal;',nTop += nTopGap,nLeft2,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-Os produtos devem estar devidamente embalados;',nTop += nTopGap,nLeft2,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-As despesas de transporte correm por conta e risco do propriet�rio;',nTop += nTopGap,nLeft2,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-A garantia n�o cobre defeitos causados por:',nTop += nTopGap,nLeft2,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-transporte, instala��o, utiliza��o e opera��o inadequada;',nTop += nTopGap,nLeft3,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-quedas, acidentes ou ainda apresentarem altera��o e/ou viola��o do n�mero de s�rie e etiquetas de garantia.',nTop += nTopGap,nLeft3,'PF',false,nFontTitleSize,'E');

			jPrinter.AddTextInBand('2 Prazo de Garantia',nTop += nTopGap,nLeft1,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('Garantia de     ano(s), contados a partir da data de emiss�o da nota fiscal at�                    .',nTop += nTopGap,nLeft2,'PF',false,nFontTitleSize,'E');
			jPrinter.AddFieldFromBandToBand('M','PF','PrazoGarantia', nTop, nLeft2 + 16, false, 'E', 8);
			jPrinter.AddFieldFromBandToBand('M','PF','dtGarantia', nTop, nLeft2 + 100, false, 'E', 8);

			jPrinter.AddTextInBand('3 T�rmino da Garantia',nTop += nTopGap,nLeft1,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-Pelo decurso normal do prazo da garantia;',nTop += nTopGap,nLeft2,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-Por liga��es el�tricas impr�prias;',nTop += nTopGap,nLeft2,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-Por viola��o, ajustes ou conserto efetuado por pessoas n�o autorizadas.',nTop += nTopGap,nLeft2,'PF',false,nFontTitleSize,'E');

			jPrinter.AddTextInBand('4 Assist�ncia T�cnica',nTop += nTopGap,nLeft1,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-Caso o micro apresente problema:',nTop += nTopGap,nLeft2,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-fa�a backup do disco r�gido, se houver;',nTop += nTopGap,nLeft3,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-envie o micro para Alcabyt, acompanhado de:',nTop += nTopGap,nLeft3,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-c�pia da nota fiscal de compra; e',nTop += nTopGap,nLeft4,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-c�pia deste romaneio, com a descri��o do problema.',nTop += nTopGap,nLeft4,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-Retorno de pe�as consertadas em garantia:',nTop += nTopGap,nLeft2,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-as pe�as ser�o retornadas, conforme os prazos abaixo, contados a partir de seu recebimento na Alcabyt:',nTop += nTopGap,nLeft3,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-retorno em at� 7 dias, tratando-se de produto:',nTop += nTopGap,nLeft4,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-adquirido em at� 30 dias (per�odo de instala��o); e',nTop += nTopGap,nLeft5,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-em perfeito estado de conserva��o e com a embalagem original;',nTop += nTopGap,nLeft5,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-retorno em at� 30 dias, tratando-se de produto:',nTop += nTopGap,nLeft4,'PF',false,nFontTitleSize,'E');
			jPrinter.AddTextInBand('-adquirido a mais de 30 dias.',nTop += nTopGap,nLeft5,'PF',false,nFontTitleSize,'E');
		}
	}
	
    glb_bPrintPreview = bPrintPreview;
    
    glb_timerListaEmbalagem = window.setInterval('sendDataOPToPrinter()', 500, 'JavaScript');
}

function sendDataOPToPrinter()
{
	if (glb_timerListaEmbalagem != null)
	{
		window.clearInterval(glb_timerListaEmbalagem);
		glb_timerListaEmbalagem = null;
	}

	// Forcado aqui para sobrepassar automacao para impressoras HP
    jPrinter.MarginBotton = 7 + _MARGIN_BOTTON;
    jPrinter.MarginLeft = 8;//_MARGIN_LEFT;
    jPrinter.MarginRightPortrait = 8;//_MARGIN_RIGHT_PORTRAIT;
	
	if (glb_bPrintPreview)
        jPrinter.Print();
   else
        jPrinter.Print(false, false, true);
}
