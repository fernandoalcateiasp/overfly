/********************************************************************
modalvinculaasstec.js

Library javascript para o modalvinculaasstec.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_nPedidoID = null;
// controla se a modal esta carregando
var glb_ModalWinIsLoading = true;
// controla o retorno do servidor dos dados de combos dinamicos
var glb_CounterCmbsDynamics = 0;
var glb_timerVincAsstec = null;
var dsoPesq = new CDatatransport('dsoPesq');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoVincAsstec = new CDatatransport('dsoVincAsstec');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
LISTA DAS FUNCOES

window_onload()
setupPage()
loadSelProduto()
dsoCmbDynamic_DSC()
selProduto_onchange()
btn_onclick(ctl)
startPesq()
dsopesq_DSC()
cleanGrid()
btnOK_Status()
execSP_Pedido_AsstecNumeroSerie()
execSP_Pedido_AsstecNumeroSerie_DSC()

Eventos particulares de grid
js_fg_modVincAsstAfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
js_fg_modVincAsstDblClick( grid, Row, Col)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    glb_nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    // ajusta o body do html
    var elem = document.getElementById('modalvinculaasstecBody');

    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    loadSelProduto();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Vincular Asstec', 1);

    var elem;
    var frameRect, modWidth, modHeight;
    var borderThickness = 6;

    // Ajusta elementos da janela

    // dimensoes da modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // ajusta o divProduto
    adjustElementsInForm([['lblProduto', 'selProduto', 60, 1, -10]],
                          null, null, true);

    elem = window.document.getElementById('divProduto');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height, 10);
        width = modWidth - (2 * ELEM_GAP) - borderThickness;
        height = parseInt(selProduto.currentStyle.top, 10) + parseInt(selProduto.currentStyle.height, 10);
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divProduto.currentStyle.top) + parseInt(divProduto.currentStyle.height) + ELEM_GAP;
        width = parseInt(divProduto.currentStyle.width);
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 2) + 4;
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }

    startGridInterface(fg);

    headerGrid(fg, ['Produto',
                   'ID',
                   'N�mero S�rie',
                   'AsstecID'], []);

    fg.Redraw = 2;

    // Reajusta largura selProduto
    selProduto.style.width = parseInt(fg.currentStyle.width) + 2;

    selProduto.onchange = selProduto_onchange;
    selProduto.disabled = true;
}

/********************************************************************
Solicita dados do combo produtos ao servidor
********************************************************************/
function loadSelProduto() {
    var strSQL = '';
    var strSQLSelect = '';
    var strSQLFrom = '';
    var strSQLWhere = '';
    var strSQLSelect2 = '';
    var strSQLFrom2 = '';
    var strSQLWhere2 = '';
    var strSQLSelect3 = '';
    var strSQLFrom3 = '';
    var strSQLWhere3 = '';
    var strSQLOrderBy = '';

    lockControlsInModalWin(true);

    cleanGrid();

    // controla o retorno do servidor dos dados de combos dinamicos
    // (selProduto)
    glb_CounterCmbsDynamics = 1;

    // parametrizacao do dso dsoCmbDynamic01 (designado para selProduto)
    setConnection(dsoCmbDynamic01);

    // Produtos
    strSQLSelect = 'SELECT 1 AS Tipo, ' +
			'Movimentos.NumMovimentoID AS fldID, ' +
		    '(Produtos.Conceito + SPACE(1) + ' +
			'RTRIM(LTRIM(CONVERT(VARCHAR, Produtos.ConceitoID))) + SPACE(1) + ' +
			'NumerosSerie.NumeroSerie) AS fldName ';

    strSQLFrom = 'FROM NumerosSerie_Movimento Movimentos WITH(NOLOCK), NumerosSerie WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK) ';

    strSQLWhere = 'WHERE ' +
			'(Movimentos.PedidoID = ' + glb_nPedidoID + ' AND Movimentos.AsstecID IS NULL AND ' +
			'Movimentos.NumeroSerieID = NumerosSerie.NumeroSerieID AND ' +
			'NumerosSerie.ProdutoID = Produtos.ConceitoID) ';

    strSQLSelect2 = 'UNION ALL SELECT 2 AS Tipo, ' +
			'Movimentos.PedProdutoID AS fldID, ' +
		    '(Produtos.Conceito + SPACE(1) + ' +
			'RTRIM(LTRIM(CONVERT(VARCHAR, Produtos.ConceitoID)))) AS fldName ';

    strSQLFrom2 = 'FROM Pedidos_ProdutosSeparados Movimentos WITH(NOLOCK), Conceitos Produtos  WITH(NOLOCK) ';

    strSQLWhere2 = 'WHERE ' +
			'(Movimentos.PedidoID = ' + glb_nPedidoID + ' AND Movimentos.AsstecID IS NULL AND ' +
			'Movimentos.ProdutoID = Produtos.ConceitoID) ';

    //INICIO DE NOVO NS ---------------------------------------------------------------		
    strSQLSelect3 = 'UNION ALL SELECT 3 AS Tipo, ' +
			'Movimentos.PedProdutoID AS fldID, ' +
		    '(Produtos.Conceito + SPACE(1) + ' +
			'RTRIM(LTRIM(CONVERT(VARCHAR, Produtos.ConceitoID)))) AS fldName ';

    strSQLFrom3 = 'FROM Pedidos_ProdutosLote Movimentos WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK) ';

    strSQLWhere3 = 'WHERE ' +
			'(Movimentos.PedidoID = ' + glb_nPedidoID + ' AND Movimentos.AsstecID IS NULL AND ' +
			'Movimentos.ProdutoID = Produtos.ConceitoID) ';

    strSQLOrderBy = 'ORDER BY Tipo, fldName ';

    strSQL = strSQLSelect + strSQLFrom + strSQLWhere +
			strSQLSelect2 + strSQLFrom2 + strSQLWhere2 +
			strSQLSelect3 + strSQLFrom3 + strSQLWhere3 +
			 strSQLOrderBy;
    //FIM DE NOVO NS ---------------------------------------------------------------		

    dsoCmbDynamic01.SQL = strSQL;

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();
}

/********************************************************************
Preenche combo produtos com dados vindos do servidor
********************************************************************/
function dsoCmbDynamic_DSC() {
    // Trata existencia de dados no carregamento
    // para mostrar ou nao a modal
    if (glb_ModalWinIsLoading == true) {
        glb_ModalWinIsLoading = false;

        if (dsoCmbDynamic01.recordset.BOF && dsoCmbDynamic01.recordset.EOF) {
            btn_onclick(btnCanc);
            return null;
        }
        else {
            // mostra a janela modal com o arquivo carregado
            showExtFrame(window, true);

            // coloca foco no campo apropriado
            window.focus();
            if (document.getElementById('selProduto').disabled == false)
                selProduto.focus();
        }
    }

    var i;
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selProduto];
    var aDSOsDunamics = [dsoCmbDynamic01];

    // Inicia o carregamento de combos dinamicos (selProduto)

    clearComboEx(['selProduto']);

    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics == 0) {
        for (i = 0; i < aCmbsDynamics.length; i++) {
            oldDataSrc = aCmbsDynamics[i].dataSrc;
            oldDataFld = aCmbsDynamics[i].dataFld;
            aCmbsDynamics[i].dataSrc = '';
            aCmbsDynamics[i].dataFld = '';

            while (!aDSOsDunamics[i].recordset.EOF) {
                optionStr = aDSOsDunamics[i].recordset['fldName'].value;
                optionValue = aDSOsDunamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                oOption.setAttribute('Tipo', aDSOsDunamics[i].recordset['Tipo'].value, 1);
                aCmbsDynamics[i].add(oOption);
                aDSOsDunamics[i].recordset.MoveNext();
            }

            aCmbsDynamics[i].dataSrc = oldDataSrc;
            aCmbsDynamics[i].dataFld = oldDataFld;
        }
    }

    lockControlsInModalWin(false);
    window.focus();

    // trava/destrava os combos de produtos se tem dados
    // seleciona o combo de produtos
    if (selProduto.options.length == 0) {
        selProduto.disabled = true;
        btnOK_Status();
    }
    else {
        selProduto.disabled = false;
        // seleciona o combo
        selProduto.focus();
        // garante item selecionado no combo
        selProduto.selectedIndex = 0;
        // carrega o grid
        startPesq();
    }
}

/********************************************************************
Troca do item selecionado no combo produtos
********************************************************************/
function selProduto_onchange() {
    cleanGrid();

    startPesq();
}

/*******************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {

    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        //sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null );
        execSP_Pedido_AsstecNumeroSerie();


    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
}

/********************************************************************
Pesquida de dados no servidor para preenchimento do grid
********************************************************************/
function startPesq() {
    var nNumMovimentoID, nPedProdutoID, nPedLoteID;
    var nTipo = selProduto.options.item(selProduto.selectedIndex).getAttribute('Tipo', 1);
    var strPars = '?';

    strPars += 'nPedidoID=' + escape(glb_nPedidoID);
    // eh Numero Serie
    if (nTipo == 1) {
        strPars += '&nNumMovimentoID=' + escape(selProduto.value);
        strPars += '&nPedProdutoID=' + escape(0);
        strPars += '&nPedLoteID=' + escape(0);
    }
    // eh Produto Separado
    else if (nTipo == 2) {
        strPars += '&nNumMovimentoID=' + escape(0);
        strPars += '&nPedProdutoID=' + escape(selProduto.value);
        strPars += '&nPedLoteID=' + escape(0);
    }
    // eh Produto Lote
    else if (nTipo == 3) {
        strPars += '&nNumMovimentoID=' + escape(0);
        strPars += '&nPedProdutoID=' + escape(0);
        strPars += '&nPedLoteID=' + escape(selProduto.value);
    }
    dsoPesq.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/listaasstec.aspx' + strPars;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

/********************************************************************
Retorno do servidor da funcao startPesq
********************************************************************/
function dsopesq_DSC() {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    startGridInterface(fg);

    fg.FrozenCols = 0;

    headerGrid(fg, ['Produto',
                   'ID',
                   'N�mero S�rie',
                   'AsstecID',
                   'Cliente'], []);

    fillGridMask(fg, dsoPesq, ['Conceito*',
                             'ConceitoID*',
                             'NumeroSerie*',
                             'AsstecID*',
                             'Cliente*'],
                             ['', '', '', '', ''],
                             ['', '', '', '', '']);

    alignColsInGrid(fg, [1, 3]);

    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    fg.Redraw = 0;

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.Redraw = 2;

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        fg.Row = 1;
        window.focus();
        fg.focus();
    }
    else {
        selProduto.focus();
    }

    btnOK_Status();

    // writeInStatusBar('child', 'cellMode', 'XXXXXXXX');    
}

/********************************************************************
Criado pelo programador
Executa a Procedure de vinculacao de Asstec

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function execSP_Pedido_AsstecNumeroSerie() {
    lockControlsInModalWin(true);

    var nTipo = selProduto.options.item(selProduto.selectedIndex).getAttribute('Tipo', 1);
    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');
    var strPars = '?';
    strPars += 'nPedidoID=' + escape(nPedidoID);
    //INICIO DE NOVO NS ---------------------------------------------------------------		
    // eh Numero Serie
    if (nTipo == 1) {
        strPars += '&nNumMovimentoID=' + escape(selProduto.value);
        strPars += '&nPedProdutoID=' + escape(0);
        strPars += '&nPedLoteID=' + escape(0);
    }
    // eh Produto Separado
    else if (nTipo == 2) {
        strPars += '&nNumMovimentoID=' + escape(0);
        strPars += '&nPedProdutoID=' + escape(selProduto.value);
        strPars += '&nPedLoteID=' + escape(0);
    }
    // eh Produto Lote
    else if (nTipo == 3) {
        strPars += '&nNumMovimentoID=' + escape(0);
        strPars += '&nPedProdutoID=' + escape(0);
        strPars += '&nPedLoteID=' + escape(selProduto.value);
    }
    //FIM DE NOVO NS ---------------------------------------------------------------		
    strPars += '&nAsstecID=' + escape(fg.ValueMatrix(fg.Row, 3));
    strPars += '&nTipoResultado=' + escape(2);

    dsoVincAsstec.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/vinculaasstec.aspx' + strPars;
    dsoVincAsstec.ondatasetcomplete = execSP_Pedido_AsstecNumeroSerie_DSC;
    dsoVincAsstec.Refresh();
}

/********************************************************************
Criado pelo programador
Retorno da funcao que Executa a Procedure de vinculacao de Asstec
chama a funcao que abre a JM Vincula Asstec: se nao for Pedido de Saida e bEhCliente = false
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function execSP_Pedido_AsstecNumeroSerie_DSC() {

    var nAsstecDuplicada = dsoVincAsstec.recordset['nAsstecDuplicada'].Value;
    //destrava a Interface
    lockControlsInModalWin(false);

    loadSelProduto();
}

/********************************************************************
Remove as linhas do grid
********************************************************************/
function cleanGrid() {
    if (fg.Rows > 1)
        fg.Rows = 1;

    btnOK.disabled = true;
}


/********************************************************************
Habilita desabilita o botao OK
********************************************************************/
function btnOK_Status() {
    btnOK.disabled = true;

    var i;
    var btnOKDisabled = true;

    // critica do grid
    if ((fg.Rows > 1) && (fg.Row != 0))
        btnOKDisabled = false;

    btnOK.disabled = btnOKDisabled;
}

// EVENTOS DE GRID PARTICULARES DESTA PAGINA ************************

function js_fg_modVincAsstAfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {
    btnOK_Status();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modVincAsstDblClick(grid, Row, Col) {
    if (btnOK.disabled != true) {
        btnOK.disabled = true;

        glb_timerVincAsstec = window.window.setInterval('startBtnOK()', 10, 'JavaScript');
    }
}

function startBtnOK() {
    if (glb_timerVincAsstec != null) {
        window.clearInterval(glb_timerVincAsstec);
        glb_timerVincAsstec = null;
    }

    btn_onclick(btnOK);
}

// FINAL DE EVENTOS DE GRID PARTICULARES DESTA PAGINA ***************