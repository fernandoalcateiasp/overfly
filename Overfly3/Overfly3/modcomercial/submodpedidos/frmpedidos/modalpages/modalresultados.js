/********************************************************************
modalresultados.js

Library javascript para o modalresultados.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_EstadoID=0;
var glb_PedidoFechado=0;
var glb_CFOPIDAlteraImposto = false;
var glb_CFOPID = false;
var glb_GridEditabled=false;
var glb_PedidoID = 0;
var glb_ComplementoValor = false;
var glb_nDSOCounter = 0;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;

// 0 - Impostos.
// 1 - Impostos complementares.
var glb_BtnImpostosMode = 0;

var dsoGridContabil = new CDatatransport('dsoGridContabil');
var dsoGridGerencial = new CDatatransport('dsoGridGerencial');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/ 
function window_onload()
{
    window_onload_1stPart();

    glb_nDSOCounter = 2;

    // ajusta o body do html
    with (modalresultadosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 50;
    modalFrame.style.left = 0;

    // configuracao inicial do html
    setupPage();

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    selTipoOnchange();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Resultados', 1);

    loadDataAndTreatInterface();
    // sera implementado na fase 3 do custo
    btnDemonstrativo.disabled = true;    
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;
        
    // Ajusta botoes OK/Cancel para esta janela
    with (btnOK.style)
    {
        disabled = true;
        left = (widthFree - 2 * parseInt(width, 10) - ELEM_GAP) / 2;
    }
    with (btnCanc.style)
    {
        left = (parseInt(btnOK.currentStyle.left,10) +  parseInt(btnOK.currentStyle.width,10) + ELEM_GAP);
    }

    with (btnDemonstrativo.style) {
        top = topFree + 16;
        left = ELEM_GAP;
        width = FONT_WIDTH * 7 + 35;
    }
    
    lblTipo.style.top = topFree;
    lblTipo.style.left = btnDemonstrativo.offsetLeft + btnDemonstrativo.offsetWidth + ELEM_GAP;
    lblTipo.style.width = '50px';
    
    selTipo.style.top = lblTipo.offsetTop + lblTipo.offsetHeight;
    selTipo.style.left = btnDemonstrativo.offsetLeft + btnDemonstrativo.offsetWidth + ELEM_GAP;
    selTipo.style.width = FONT_WIDTH * 12;
    selTipo.onchange = selTipoOnchange;

    lblTotais.style.top = topFree;
    lblTotais.style.left = lblTipo.offsetLeft + lblTipo.offsetWidth + ELEM_GAP + 40;
    lblTotais.style.width = FONT_WIDTH * 7;

    chkTotais.style.top = lblTotais.offsetTop + lblTotais.offsetHeight;
    chkTotais.style.left = selTipo.offsetLeft + selTipo.offsetWidth + ELEM_GAP;
    chkTotais.style.width = FONT_WIDTH * 3;
    chkTotais.onclick = chkTotaisOnclick;
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = selTipo.offsetTop + selTipo.offsetHeight + ELEM_GAP;
        width = modWidth - frameBorder - 2 * ELEM_GAP;    
        height = parseInt(btnOK.currentStyle.top, 10) -
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10) + 55;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    with (btnCanc) 
    {
        style.visibility = 'hidden';
    }
   
    with (btnOK) 
    {
        style.visibility = 'hidden';
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) 
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    // 2. O usuario clicou o botao Cancela
    if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);

}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function fillGridContabil()//374
{
    var nPedido = glb_nPedidoID; 
    setConnection(dsoGridContabil);

    dsoGridContabil.SQL = 'SELECT DISTINCT c.ProdutoID AS ID, d.Conceito AS Produto, c.Quantidade AS Quant, f.SimboloMoeda AS Moeda, g.TaxaMoeda AS Taxa, ' +
		                'dbo.fn_PedidoItem_Totais(a.PedItemID,19) AS ReceitaBruta, ' +
		                'dbo.fn_DivideZero(dbo.fn_PedidoItem_Totais(a.PedItemID,10),c.Quantidade) AS Impostos, ' +
		                'dbo.fn_DivideZero(a.PrecoBase, c.Quantidade) AS ReceitaLiquida, ' +
		                'dbo.fn_DivideZero(a.CustoBase, c.Quantidade) AS CMV, ' +
                        'dbo.fn_DivideZero(a.ValorContribuicao, c.Quantidade) AS ResultadoBruto, ' +
		                'ROUND((dbo.fn_DivideZero(a.ValorContribuicao, a.PrecoBase) * 100),2) AS MC, ' +
		                'a.dtResultado AS Calculadoem ' +
                    'FROM Pedidos_Itens_Resultados a WITH(NOLOCK) ' +
	                    'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.PedItemID = a.PedItemID) ' +
	                    'INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = c.ProdutoID) ' +
	                    'INNER JOIN RelacoesPesCon e WITH(NOLOCK) ON (e.ObjetoID = c.ProdutoID) ' +
	                    'INNER JOIN Conceitos f WITH(NOLOCK) ON (f.ConceitoID = a.MoedaID)' +
	                    'INNER JOIN Pedidos g WITH(NOLOCK) ON (g.PedidoID = c.PedidoID)' +
                    'WHERE c.PedidoID = ' + nPedido + 
                    '  AND a.TipoResultadoID = 470';

    dsoGridContabil.ondatasetcomplete = fillGridContabil_DSC;
    dsoGridContabil.Refresh(); 

}

/********************************************************************
Preenchimento do grid de Cont�bil
********************************************************************/
function fillGridContabil_DSC()
{
    var i, j, nSumImp;
    var nAliquota = 0;
    var HiddeColumn = [];

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.Redraw = 0;
    fg.FontSize = '8';

    headerGrid(fg, ['ID',
                    'Produto',
                    'Quantidade',
                    'Moeda',
                    'Taxa',
                    'Receita Bruta',
                    'Impostos',
                    'Receita L�quida',
                    'CMV',
                    'Resultado Bruto',
                    'MC',
                    'Calculado em'],[]);
    
    glb_aCelHint = [[0, 2, 'Quantidade'],
                    [0, 8, 'Custo M�dio de Venda'],
                    [0, 10, 'Margem Contribui��o']];
                    
                    
    fillGridMask(fg, dsoGridContabil, ['ID',  
                                'Produto',  
                                'Quant',  
                                'Moeda',  
                                'Taxa',  
                                'ReceitaBruta',  
                                'Impostos',  
                                'ReceitaLiquida',  
                                'CMV',  
                                'ResultadoBruto',  
                                'MC',
                                'Calculadoem'],
                                ['', '', '999999999999', '', '99999999.99999', '#99999999.99', '#99999999.99', '#99999999.99', '#99999999.99', '#99999999.99', '#99999999.99', '99/99/9999'],
                                ['', '', '###,###,###,999', '', '##,###,###.00000', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', dTFormat + ' hh:mm:ss']);

    alignColsInGrid(fg, [0, 2, 3, 4, 5, 6, 7, 8, 9, 10]);


    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.BorderStyle = 1;
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;
            
    with (modalresultadosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
               
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();
    
    fg.Editable = glb_GridEditabled;
    lockControlsInModalWin(false);
    btnOK.disabled = true;
    selTipo.focus();

}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function fillGridGerencial()//375
{
    var nPedido = glb_nPedidoID;
    setConnection(dsoGridGerencial);

    dsoGridGerencial.SQL = 'SELECT DISTINCT c.ProdutoID AS ID, d.Conceito AS Produto, c.Quantidade AS Quant, f.SimboloMoeda AS Moeda, g.TaxaMoeda AS Taxa, ' +
		                'ROUND(dbo.fn_DivideZero(dbo.fn_PedidoItem_Totais(a.PedItemID,19),g.TaxaMoeda), 2) AS PrecoFatura, ' +
		                'dbo.fn_DivideZero(dbo.fn_DivideZero(dbo.fn_PedidoItem_Totais(a.PedItemID,10),c.Quantidade),g.TaxaMoeda) AS Impostos, ' +
		                'dbo.fn_DivideZero(ISNULL(a.ValorDespesa,0),c.Quantidade) AS DespGerenciais, ' +
		                'dbo.fn_DivideZero(a.PrecoBase,c.Quantidade) AS PrecoBase, ' +
		                'dbo.fn_DivideZero((dbo.fn_PedidoItem_CampanhaTotais(c.PedItemID, 1321, 1, 1) + ' +
						' dbo.fn_PedidoItem_CampanhaTotais(c.PedItemID, 1322, 1, 1) + ' +
						' dbo.fn_PedidoItem_CampanhaTotais(c.PedItemID, 1323, 1, 1)) * POWER(-1, g.TipoPedidoID),c.Quantidade) AS CustosGerenciais, ' +
		                'dbo.fn_DivideZero(a.CustoBase,c.Quantidade) AS CustoBase, ' +
		                'dbo.fn_DivideZero(a.ValorContribuicao,c.Quantidade) AS Contribuicao, ' +
                        '(CASE WHEN (a.ValorContribuicao < 0.00 AND a.PrecoBase < 0.00)  '+
                           'THEN (ROUND((dbo.fn_DivideZero(a.ValorContribuicao, a.PrecoBase) * 100),2) * -1)  '+
                            'ELSE ROUND((dbo.fn_DivideZero(a.ValorContribuicao, a.PrecoBase) * 100),2) END) AS MC, ' +
		          //      'ROUND((dbo.fn_DivideZero(a.ValorContribuicao, a.PrecoBase) * 100),2) AS MC, ' +
		                'a.dtResultado AS Calculadoem ' +
                    'FROM Pedidos_Itens_Resultados a WITH(NOLOCK) ' +
	                    'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.PedItemID = a.PedItemID) ' +
	                    'INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = c.ProdutoID) ' +
	                    'INNER JOIN RelacoesPesCon e WITH(NOLOCK) ON (e.ObjetoID = c.ProdutoID) ' +
	                    'INNER JOIN Conceitos f WITH(NOLOCK) ON (f.ConceitoID = a.MoedaID)' +
	                    'INNER JOIN Pedidos g WITH(NOLOCK) ON (g.PedidoID = c.PedidoID)' +
                    ' WHERE c.PedidoID = ' + nPedido +
                    '  AND a.TipoResultadoID = 471';

    dsoGridGerencial.ondatasetcomplete = fillGridGerencial_DSC;
    dsoGridGerencial.Refresh();

}

/********************************************************************
Preenchimento do grid de Gerencial
********************************************************************/
function fillGridGerencial_DSC() {
    var i, j, nSumImp;
    var nAliquota = 0;
    var HiddeColumn = [];

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.Redraw = 2;
    fg.FontSize = '8';

    headerGrid(fg, ['ID',
                    'Produto',
                    'Quantidade',
                    'Moeda',
                    'Taxa',
                    'Pre�o Fatura',
                    'Impostos',
                    'Despesas Gerenciais',
                    'Pre�o Base',
                    'Custos Gerenciais',
                    'Custo Base',
                    'Contribui��o',
                    'MC',
                    'Calculado em'],[]);


    glb_aCelHint = [[0, 2, 'Quantidade'],
                    [0, 7, 'Despesas Gerenciais'],
                    [0, 9, 'Custos Gerenciais'],
                    [0, 12, 'Margem Contribui��o']];
                    
    fillGridMask(fg, dsoGridGerencial, ['ID',
                                'Produto',
                                'Quant',
                                'Moeda',
                                'Taxa',
                                'PrecoFatura',
                                'Impostos',
                                'DespGerenciais',
                                'PrecoBase',
                                'CustosGerenciais',
                                'CustoBase',
                                'Contribuicao',
                                'MC',
                                'Calculadoem'],
                                ['', '', '', '999999999999', '99999999.99999', '#99999999.99', '#99999999.99', '#99999999.99', '#99999999.99', '#99999999.99', '#99999999.99', '#99999999.99', '#99999999.99', '99/99/9999'],
                                ['', '', '', '###,###,###,999', '##,###,###.00000', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', dTFormat + ' hh:mm:ss']);

    alignColsInGrid(fg,[0,2,3,4,5,6,7,8,9,10,11,12]);
    

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.BorderStyle = 1;
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    with (modalresultadosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();

    fg.Editable = glb_GridEditabled;
    lockControlsInModalWin(false);
    btnOK.disabled = true;
    selTipo.focus();

}



/********************************************************************
Preenchimento do grid de impostos
********************************************************************/

function js_modalresultados_AfterEdit(Row, Col)
{
	if (glb_BtnImpostosMode == 0)
		js_modalresultadosNormais_AfterEdit(Row, Col);
	else if (glb_BtnImpostosMode == 1)
		js_modalresultadosComplementares_AfterEdit(Row, Col);
}

function js_modalresultadosComplementares_AfterEdit(Row, Col)
{
	fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
	fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;
	btnOK.disabled = false;
}

function js_modalresultadosNormais_AfterEdit(Row, Col)
{
    ;
}

function js_BeforeRowColChangemodalresultados(grid, OldRow, OldCol, NewRow, NewCol, Cancel)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalresultadosDblClick(grid, Row, Col)
{
    ;
}

function currCellIsLocked(grid, nRow, nCol)
{
    ;
}

function selTipoOnchange() 
{
    lblTipo.innerText = "Tipo " + selTipo.options[selTipo.selectedIndex].getAttribute("TipoID", 1);
    
    if (selTipo.value == '374')
        fillGridContabil();

    else if (selTipo.value == '375')
        fillGridGerencial();
    
    chkTotais.checked = 0;
}

function chkTotaisOnclick() {
    var qtd;
    //se estiver desmarcando, chama o dso pq esta todo unitario
    if (!chkTotais.checked)
        selTipoOnchange();
    else {
        for (var y = 1; y <= (fg.Rows - 1); y++) {
            qtd = fg.TextMatrix(y, getColIndexByColKey(fg, 'Quant'));

            if (selTipo.value == '374') {
                fg.TextMatrix(y, getColIndexByColKey(fg, 'ReceitaBruta')) = parseFloat(fg.ValueMatrix(y, getColIndexByColKey(fg, 'ReceitaBruta')) * qtd);
                fg.TextMatrix(y, getColIndexByColKey(fg, 'Impostos')) = fg.ValueMatrix(y, getColIndexByColKey(fg, 'Impostos')) * qtd;
                fg.TextMatrix(y, getColIndexByColKey(fg, 'ReceitaLiquida')) = fg.ValueMatrix(y, getColIndexByColKey(fg, 'ReceitaLiquida')) * qtd;
                fg.TextMatrix(y, getColIndexByColKey(fg, 'CMV')) = fg.ValueMatrix(y, getColIndexByColKey(fg, 'CMV')) * qtd;
                fg.TextMatrix(y, getColIndexByColKey(fg, 'ResultadoBruto')) = fg.ValueMatrix(y, getColIndexByColKey(fg, 'ResultadoBruto')) * qtd;
            }
            else {
                fg.TextMatrix(y, getColIndexByColKey(fg, 'PrecoFatura')) = fg.ValueMatrix(y, getColIndexByColKey(fg, 'PrecoFatura')) * qtd;
                fg.TextMatrix(y, getColIndexByColKey(fg, 'Impostos')) = fg.ValueMatrix(y, getColIndexByColKey(fg, 'Impostos')) * qtd;
                fg.TextMatrix(y, getColIndexByColKey(fg, 'DespGerenciais')) = fg.ValueMatrix(y, getColIndexByColKey(fg, 'DespGerenciais')) * qtd;
                fg.TextMatrix(y, getColIndexByColKey(fg, 'PrecoBase')) = fg.ValueMatrix(y, getColIndexByColKey(fg, 'PrecoBase')) * qtd;
                fg.TextMatrix(y, getColIndexByColKey(fg, 'CustosGerenciais')) = fg.ValueMatrix(y, getColIndexByColKey(fg, 'CustosGerenciais')) * qtd;
                fg.TextMatrix(y, getColIndexByColKey(fg, 'CustoBase')) = fg.ValueMatrix(y, getColIndexByColKey(fg, 'CustoBase')) * qtd;
                fg.TextMatrix(y, getColIndexByColKey(fg, 'Contribuicao')) = fg.ValueMatrix(y, getColIndexByColKey(fg, 'Contribuicao')) * qtd;
            }

        }

        if (selTipo.value == '374') {
            // Linha de Totais
            gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(fg, 'ReceitaBruta'), '###,###,###,##0.00', 'S'],
                        [getColIndexByColKey(fg, 'Impostos'), '###,###,###,##0.00', 'S'],
                        [getColIndexByColKey(fg, 'ReceitaLiquida'), '###,###,###,##0.00', 'S'],
                        [getColIndexByColKey(fg, 'CMV'), '###,###,###,##0.00', 'S'],
                        [getColIndexByColKey(fg, 'ResultadoBruto'), '###,###,###,##0.00', 'S']]);
                        
            var nReceitaLiquida;
            var nResultadoBruto;
            nReceitaLiquida = fg.ValueMatrix(1, getColIndexByColKey(fg, 'ReceitaLiquida'));
            nResultadoBruto = fg.ValueMatrix(1, getColIndexByColKey(fg, 'ResultadoBruto'));
            fg.TextMatrix(1, getColIndexByColKey(fg, 'MC')) = ((nResultadoBruto / nReceitaLiquida) * 100);
        }
        else {
            // Linha de Totais
            gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(fg, 'PrecoFatura'), '###,###,###,##0.00', 'S'],
                        [getColIndexByColKey(fg, 'Impostos'), '###,###,###,##0.00', 'S'],
                        [getColIndexByColKey(fg, 'DespGerenciais'), '###,###,###,##0.00', 'S'],
                        [getColIndexByColKey(fg, 'PrecoBase'), '###,###,###,##0.00', 'S'],
                        [getColIndexByColKey(fg, 'CustosGerenciais'), '###,###,###,##0.00', 'S'],
                        [getColIndexByColKey(fg, 'CustoBase'), '###,###,###,##0.00', 'S'],
                        [getColIndexByColKey(fg, 'Contribuicao'), '###,###,###,##0.00', 'S']]);

            var nContribuicao;
            var nPrecoBase;
            nContribuicao = fg.ValueMatrix(1, getColIndexByColKey(fg, 'Contribuicao'));
            nPrecoBase = fg.ValueMatrix(1, getColIndexByColKey(fg, 'PrecoBase'));
            fg.TextMatrix(1, getColIndexByColKey(fg, 'MC')) = ((nContribuicao / nPrecoBase) * 100);
        }

    } 
}