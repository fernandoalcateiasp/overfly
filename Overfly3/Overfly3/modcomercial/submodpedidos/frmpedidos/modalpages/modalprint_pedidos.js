
/********************************************************************
modalprint_pedidos.js

Library javascript para o modalprint.asp
Pedidos 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nDynamicCmbss = 0;
var glb_nDynamicCmbsCompras = 0;
var glb_bUnlockModal = true;
var glb_space = String.fromCharCode(32);
var glb_aTranslate_Proposta = null;
var glb_aTranslate_EtiquetaEmbalagem = null;
var glb_callPrtMercadoriaTimer = null;
var glb_nEhMicro = 0;
var glb_nDSOsProposta = 0;
var glb_nDSOsSolicitacao = 0;
var glb_ShowThisWinTimer = null;
var glb_relFabricante = 0;
var glb_Print_2nd_Copy = null;
var glb_callrelatorioRetiradaMercadoriaTimer = null;
var pageRequest = new XMLHttpRequest();

var dsoPrint01 = new CDatatransport("dsoPrint01");
var dsoFabricantes = new CDatatransport("dsoFabricantes");
var dsoFamilias = new CDatatransport("dsoFamilias");
var dsoPadrao = new CDatatransport("dsoPadrao");
var dsoCmbs04 = new CDatatransport("dsoCmbs04");
var dsoContato1 = new CDatatransport("dsoContato1");
var dsoProprietarios = new CDatatransport("dsoProprietarios");
var dsoDeposito = new CDatatransport("dsoDeposito");
var dsoExtra01 = new CDatatransport("dsoExtra01");
var dsoContato2 = new CDatatransport("dsoContato2");
var dsoGen01 = new CDatatransport("dsoGen01");
var dsoTotais = new CDatatransport("dsoTotais");
var dsoMoedaLocal = new CDatatransport("dsoMoedaLocal");
var dsoGravaTotalGross = new CDatatransport("dsoGravaTotalGross");
var dsoPessoas = new CDatatransport("dsoPessoas");
var dsoEMail = new CDatatransport("dsoEMail");
var dsoProposta = new CDatatransport("dsoProposta");
var dsoRelatorioFabricante = new CDatatransport("dsoRelatorioFabricante");
var dsoMinuta = new CDatatransport("dsoMinuta");
var dsoTransportadoraMinuta = new CDatatransport("dsoTransportadoraMinuta");
var dsoParceirosProprietarios = new CDatatransport("dsoParceirosProprietarios");
var dsoCabecalhoDemonstrativoCalculoICMSST = new CDatatransport("dsoCabecalhoDemonstrativoCalculoICMSST");
var dsoMensagemDemonstrativoCalculoICMSST = new CDatatransport("dsoMensagemDemonstrativoCalculoICMSST");
var dsoArquivoGNRE = new CDatatransport("dsoArquivoGNRE");
var dsoTransportadoras = new CDatatransport("dsoTransportadoras");
var dsoEmpresasSelecionadas = new CDatatransport("dsoEmpresasSelecionadas");
var dsoPropostaHTML = new CDatatransport("dsoPropostaHTML");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    linkDivsAndReports()
    invertChkBox()
    chkBox_Clicked()
    selReports_Change()
    btnOK_Status()
    btnOK_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    noRights()
    translateTherm(sTherm)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    glb_bUseProgressBar = true;
    asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // Seleciona ultimo relatorio utilizado
    selectReportInCombo();


    // ajusta o body do html
    with (modalprintBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra o div correspondente ao relatorio
    showDivByReportID();

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    window.focus();

    if (selReports.disabled == false)
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();


}
/********************************************************************
Clique botao OK ou Cancela / Teste ECF
********************************************************************/
function btn_onclick(ctl) {

    window.focus();

    if (ctl.id == btnOK.id)
        btnOK.focus();
    else if (ctl.id == btnCanc.id)
        btnCanc.focus();

    if (ctl.id == btnPesquisar.id)
        pesquisaMinuta();

    else if (ctl.id == btnPesquisarParceiroProprietario.id)
        pesquisaParceiroProprietario();

    if (ctl.id == btnOK.id) {
        // esta funcao trava o html contido na janela modal
        lockControlsInModalWin(true);

        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        btnOK.disabled = true;
        btnCanc.disabled = true;
        pb_StopProgressBar(true);

        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}


/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Relat�rios', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;

    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    with (lblReports.style) {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // ajusta combo de relatorios
    with (selReports.style) {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // ajusta o divSolicitacaoCompra
    with (divSolicitacaoCompra.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divPropostaComercial
    with (divPropostaComercial.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divListaEmbalagem
    with (divListaEmbalagem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o Div de Etiquetas de Embalagem
    with (divEtiquetasEmbalagem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divBarCode
    with (divBarCode.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o Div de Relatorio Vendas
    with (divDemonstrativoCalculoICMSST.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    with (divRelatorioVendas.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o Div de Relatorio Compras
    with (divRelatorioCompras.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o Div de Total Vendas
    with (divTotalVendas.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divRetiradaMercadoria
    with (divRetiradaMercadoria.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divPedidoRetorno
    with (divPedidoRetorno.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divOrdemDeProducao
    with (divOrdemDeProducao.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divRelatorioTransportadora
    with (divRelatorioTransportadora.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divRelatorioDeposito
    with (divRelatorioDeposito.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }


    // ajusta o divMinutaTransporte
    with (divMinutaTransporte.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divComissoes
    with (divComissoes.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divRelatorioFabricante
    with (divRelatorioFabricante.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // carrega dados na interface e configura travamento de seus elementos
    //loadDataAndTreatInterface();

    // O estado do botao btnOK
    btnOK_Status();

    // O usuario tem direito ou nao
    noRights();

    // Array de dicionario usado para traduzir proposta/pedido
    glb_aTranslate_Proposta = [['Proposta Comercial', 'Quotation'],
                      ['Cliente', 'Customer'],
                      ['Dados da Proposta', 'Quotation'],
                      ['Proposta', 'Quotation'],
                      ['Data', 'Date'],
                      ['Valor', 'Total Amount'],
	                  ['Valor Convertido', ''],
                      ['Previs�o Entrega', 'Estimated Delivery'],
                      ['Frete', 'Freight'],
                      ['Validade', 'Expiration'],
                      ['dias �teis', 'days'],
                      ['Seu Pedido', 'Your PO'],
                      ['�tens', 'Items'],
                      ['ID', 'ID'],
                      ['Produto', 'Product'],
                      ['Quant', 'Qty'],
                      ['Unit�rio', 'Unitary'],
                      ['Total', 'Total'],
                      ['Pagamento', 'Payment'],
                      ['Forma', 'Form'],
                      ['Prazo', 'Term'],
                      ['Valor', 'Total'],
                      ['Observa��es', 'Comments'],
                      ['Atenciosamente', 'Best Regards'],
                      ['De acordo do cliente', 'Agreed by customer'],
                      ['Aprovar esta proposta', 'Approve this quotation'],
                      ['acesse', 'access'],
                      ['�rea de Neg�cios', 'business Area'],
                      ['p�gina Pedidos', 'Orders'],
                      ['proposta', 'quotation'],
                      ['e', 'and'],
                      ['aprove a proposta', 'submit the quotation'],
                      ['Ficha T�cnica', 'Technical Especification'],
                      ['Descri��o', 'Description'],
                      ['Part Number', 'Part Number'],
                      ['Peso', 'Weight'],
                      ['Gar', 'Warr'],
                      ['A Pagar', 'To be collected'],
                      ['Pago', 'Paid'],
                      ['Solicita��o de Compra', 'Purchase Order'],
                      ['Solicita��o', 'PO'],
                      ['Faturar Para', 'Invoice To']];

    glb_aTranslate_EtiquetaEmbalagem = [['NF', 'Invoice'],
										['Pedido', 'Order'],
										['Volume', 'Volume'],
										['Transp', 'Carrier'],
										['Rem', ''],
										['Data NF', 'Invoice Date']];

    asort(glb_aTranslate_Proposta, 0);
    asort(glb_aTranslate_EtiquetaEmbalagem, 0);
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;

    //if (glb_Teste == 0)
    //{

    //}
    //else {
    selReports.onchange = selReports_Change;

    // carrega a lista de relatorios
    loadReportsInList();

    // linka os divs nos relatorios
    linkDivsAndReports();

    // ajusta o divEtiquetasEmbalagem
    adjustDivEtiquetasEmbalagem();

    // ajusta o divRelatorioCompras
    adjustDivRelatorioCompras();

    // ajusta o divRetiradaMercadoria
    adjustDivRetiradaMercadoria();

    // ajusta o divPedidoRetorno
    adjustDivPedidoRetorno();

    // ajusta o divOrdemDeProducao
    adjustDivOrdemDeProducao();

    // ajusta o divBarCode
    adjustDivBarCode();

    // ajusta o divSolicitacaoCompra
    adjustDivSolicitacaoCompra();

    // ajusta o divPropostaComercial
    adjustDivPedido();

    // ajusta o divPropostaComercial
    adjustDivPropostaComercial();

    // ajusta o divListaEmbalagem
    adjustDivListaEmbalagem();

    // ajusta o divRelatorioTransportadora
    adjustDivRelatorioTransportadora();

    // ajusta o divMinutaTransporte
    adjustdivMinutaTransporte();

    // ajusta o divComissoes
    adjustdivComissoes();

    adjustdivRelatorioFabricante();

    // ajusta o divRelatorioDeposito
    adjustDivRelatorioDeposito();

    // restringe digitacao em campos numericos
    setMaskAndLengthToInputs();

    // ajusta o divRelatorioVendas
    adjustDivRelatorioVendas();
    // ajusta o divTotalVendas
    adjustDivTotalVendas();

    adjustDivDemonstrativoCalculoICMSST();
    //}
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList() {
    var i;

    if (glb_arrayReports != null) {
        for (i = 0; i < glb_arrayReports.length; i++) {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);

            if (glb_bOpenInProposta == 0) {
                if (glb_arrayReports[i][2])
                    selReports.selectedIndex = i;
            }
            else {
                if (glb_nContextoID == 5111) {
                    // Solicitacao de compra
                    if (glb_arrayReports[i][1] == 40132)
                        selReports.selectedIndex = i;
                }
                else if (glb_nContextoID == 5112) {
                    // Proposta Comercial
                    if (glb_arrayReports[i][1] == 40133)
                        selReports.selectedIndex = i;
                }
            }
        }

        if (selReports.selectedIndex < 0)
            selReports.selectedIndex = 0;

        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports() {
    divSolicitacaoCompra.setAttribute('report', 40132, 1);
    divPropostaComercial.setAttribute('report', 40133, 1);
    divListaEmbalagem.setAttribute('report', 40134, 1);
    divDemonstrativoCalculoICMSST.setAttribute('report', 40147, 1);//Demostrativo ICMS 
    divEtiquetasEmbalagem.setAttribute('report', 40135, 1);
    divBarCode.setAttribute('report', 40136, 1);
    divRetiradaMercadoria.setAttribute('report', 40137, 1);
    divRelatorioVendas.setAttribute('report', 40139, 1);
    divRelatorioCompras.setAttribute('report', 40142, 1);
    divTotalVendas.setAttribute('report', 40140, 1);
    divPedidoRetorno.setAttribute('report', 40141, 1);
    divOrdemDeProducao.setAttribute('report', 40172, 1);
    divRelatorioTransportadora.setAttribute('report', 40143, 1);
    divRelatorioDeposito.setAttribute('report', 40179, 1);
    divMinutaTransporte.setAttribute('report', 40146, 1);
    divComissoes.setAttribute('report', 40148, 1);
    divRelatorioFabricante.setAttribute('report', 40145, 1);
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox() {
    var ctl = this.nextSibling;

    if (ctl.disabled == true)
        return;

    ctl.checked = !ctl.checked;

    // O estado do botao btnOK
    btnOK_Status();

    window.focus();
    if (ctl.disabled == false)
        ctl.focus();

    return true;
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change() {
    // mostra o div correspondente ao relatorio
    showDivByReportID();

    // ajusta o divRelatorioVendas
    adjustDivRelatorioVendas();

    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if (this.disabled == false)
        this.focus();

    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status() {
    var btnOKStatus = true;
    var i, elem;

    if (selReports.selectedIndex != -1) {
        // Pedido
        if (selReports.value == 40131)
            btnOKStatus = false;
            // Solicitacao de Compra
        else if (selReports.value == 40132) {
            var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
            'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

            if (nEstadoID != 21)
                btnOKStatus = false;
        }
            // Proposta Comercial
        else if ((glb_sCaller == 'S') && (selReports.value == 40133)) {
            var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
            'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

            if ((nEstadoID == 22) || (nEstadoID == 23) || (nEstadoID == 24))
                btnOKStatus = false;
        }

            // Arquivo GNRE
        else if (selReports.value == 40148)
            btnOKStatus = false;
            // Lista de DemonstrativoCalculoICMSST
        else if (selReports.value == 40147)
            btnOKStatus = false;
            // Lista de Embalagem
        else if (selReports.value == 40134)
            btnOKStatus = false;
            // Etiqueta de pedido
        else if ((selReports.value == 40135) && (glb_nNumeroVolumes > 0)) {
            var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

            if (nEstadoID > 26)
                btnOKStatus = false;
        }
        else if (selReports.value == 40136)
            btnOKStatus = false;
        else if (selReports.value == 40137)
            btnOKStatus = false;
        else if (selReports.value == 40139)
            btnOKStatus = false;
        else if (selReports.value == 40140)
            btnOKStatus = false;
        else if (selReports.value == 40141)
            btnOKStatus = false;
        else if (selReports.value == 40142)
            btnOKStatus = false;
        else if (selReports.value == 40143)
            btnOKStatus = false;
        else if (selReports.value == 40145)
            btnOKStatus = false;

        else if (selReports.value == 40146)
            if (selMinuta.value > 0)
                btnOKStatus = false;
            else
                btnOKStatus = true;
        else if (selReports.value == 40148)
            btnOKStatus = false;
            // Nota Fiscal
        else if (selReports.value == 40151) {
            var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
            'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

            var bTemNF = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
            'dsoSup01.recordset[' + '\'' + 'TemNF' + '\'' + '].value');

            if (nEstadoID < 27)
                btnOKStatus = true;
            else if ((nEstadoID >= 27) && (bTemNF))
                btnOKStatus = false;
        }
        else if (selReports.value == 40172)
            btnOKStatus = false;
            // Boleto
        else if (selReports.value == 40182) {
            var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
            'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');

            var bTemBloqueto = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
            'dsoSup01.recordset[' + '\'' + 'TemBloqueto' + '\'' + '].value');

            if ((nEstadoID >= 29) && (bTemBloqueto))
                btnOKStatus = false;
        }
    }

    btnOK.disabled = btnOKStatus;
}

function pesquisaMinuta() {
    var dtInicio, dtFim, sPesquisa, sSQL, sFrom, sWhere, sOrderBy;

    if (!verificaData(txtdtInicio.value))
        return null;
    if (!verificaData(txtdtFim.value))
        return null;

    dtInicio = dateFormatToSearch(txtdtInicio.value);
    dtFim = dateFormatToSearch(txtdtFim.value);
    sPesquisa = trimStr(txtPesquisa.value);

    lockControlsInModalWin(true);
    if (selTransportadoraMT.value == 0) {
        if (window.top.overflyGen.Alert('Escolha uma transportado.') == 0)
            return null;

        lockControlsInModalWin(false);
        btnOK_Status();
        return null;
    }
    setConnection(dsoMinuta);

    sSQL = 'SELECT a.MinutaID AS fldID, ISNULL(CONVERT(VARCHAR,MAX(a.dtChegadaPortador),103) + \' \' + CONVERT(VARCHAR,MAX(a.dtChegadaPortador),108), SPACE(0)) AS Data ' +
			'FROM dbo.vw_Pedidos_MinutasTransporte a WITH(NOEXPAND) ' +
			'WHERE a.EmpresaID = ' + glb_nEmpresaID + ' AND a.TransportadoraID = ' + selTransportadoraMT.value + ' ';

    sOrderBy = ' GROUP BY a.MinutaID ORDER BY a.MinutaID';

    if ((sPesquisa != '') && (!isNaN(sPesquisa)))
        sSQL += 'AND (a.MinutaID >= ' + sPesquisa + ') ';

    if ((dtInicio != '') && (dtFim != ''))
        sSQL += 'AND (a.dtChegadaPortador BETWEEN \'' + dtInicio + ' 00:00\' AND \'' + dtFim + ' 23:59\') ';

    else if ((dtInicio != '') && (dtFim == ''))
        sSQL += 'AND (a.dtChegadaPortador >= \'' + dtInicio + ' 00:00\') ';

    else if ((dtInicio == '') && (dtFim != ''))
        sSQL += 'AND (a.dtChegadaPortador <= \'' + dtFim + ' 23:59\') ';


    dsoMinuta.SQL = sSQL + sOrderBy;
    dsoMinuta.ondatasetcomplete = minutas_DSC;
    dsoMinuta.Refresh();
}

function minutas_DSC() {
    var optionValue;
    var optionStr;
    var optionStr2;
    var oOption;

    clearComboEx(['selMinuta']);

    while (!dsoMinuta.recordset.EOF) {
        optionValue = dsoMinuta.recordset.Fields['fldID'].value;
        optionStr = dsoMinuta.recordset.Fields['fldID'].value;
        optionStr2 = ' ' + dsoMinuta.recordset.Fields['Data'].value;
        oOption = document.createElement("OPTION");
        oOption.text = '(' + optionStr + ')' + optionStr2;
        oOption.value = optionValue;
        selMinuta.add(oOption);
        dsoMinuta.recordset.MoveNext();
    }

    lockControlsInModalWin(false);
    btnOK_Status();
}

function verificaData(sData) {
    var sData = trimStr(sData);
    var bDataIsValid = true;

    if (sData != '')
        bDataIsValid = chkDataEx(sData);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        return false;
    }
    return true;
}

function btn_transportadoraClick() {
    lockControlsInModalWin(true);
    setConnection(dsoTransportadoras);

    dsoTransportadoras.SQL = 'SELECT 0 AS PessoaID, SPACE(0) AS Fantasia ' +
							 'UNION ALL ' +
								'SELECT -1 AS PessoaID, ' + '\'' + 'O Mesmo' + '\'' + ' AS Fantasia ' +
							 'UNION ALL ' +
								'SELECT DISTINCT b.PessoaID, b.Fantasia ' +
								'FROM Pedidos a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
								'WHERE (a.EmpresaID = ' + glb_nEmpresaID + ' AND ' +
									'a.TransportadoraID = b.PessoaID AND a.ParceiroID <> a.TransportadoraID AND ' +
									'a.PessoaID <> a.TransportadoraID AND a.TipoPedidoID=602 AND a.EstadoID >= 29) ' +
								'ORDER BY Fantasia ';

    dsoTransportadoras.ondatasetcomplete = dsoTransportadoras_DSC;
    dsoTransportadoras.Refresh();
}

function btn_DepositoClick() {
    lockControlsInModalWin(true);
    setConnection(dsoDeposito);

    dsoDeposito.SQL = 'SELECT 0 AS PessoaID, SPACE(0) AS Fantasia ' +
							 'UNION ALL ' +
								'SELECT DISTINCT b.PessoaID, b.Fantasia ' +
								'FROM Pedidos a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
								'WHERE (a.EmpresaID = ' + glb_nEmpresaID + ' AND ' +
									'a.DepositoID = b.PessoaID AND a.ParceiroID <> a.DepositoID AND ' +
									'a.PessoaID <> a.DepositoID AND a.TipoPedidoID=602 AND a.EstadoID >= 29) ' +
								'ORDER BY Fantasia ';

    dsoDeposito.ondatasetcomplete = dsoDeposito_DSC;
    dsoDeposito.Refresh();
}

function chkLiberaco_onclick() {

    if ((chkZ.checked || chkC.checked || chkF.checked || chkE.checked || chkD.checked || chkL.checked)
        && (chkEntrada.checked || chkSaida.checked))
        btnOK.disabled = false;
    else
        btnOK.disabled = true;

}


/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked() {
    if ((selReports.value == 40131) && (glb_sCaller == 'S'))
        pedido();
    else if ((selReports.value == 40132) && (glb_sCaller == 'S')) {
        if (isNaN(parseInt(txtValidadeSolicitacao.value, 10))) {
            if (window.top.overflyGen.Alert('N�mero inv�lido') == 0)
                return null;
            lockControlsInModalWin(false);
            txtValidadeSolicitacao.focus();
            return null;
        }
        else if (parseInt(txtValidadeSolicitacao.value, 10) < 1) {
            if (window.top.overflyGen.Alert('N�mero de dias n�o pode ser inferior a 1 (um)') == 0)
                return null;
            lockControlsInModalWin(false);
            txtValidadeSolicitacao.focus();
            return null;
        }
        propostaSolicitacao(selReports.value);
    }
    else if ((selReports.value == '40133') && (glb_sCaller == 'S')) {
        if (isNaN(parseInt(txtValidadeProposta.value, 10))) {
            if (window.top.overflyGen.Alert('N�mero inv�lido') == 0)
                return null;
            lockControlsInModalWin(false);
            txtValidadeProposta.focus();
            return null;
        }
        else if (parseInt(txtValidadeProposta.value, 10) < 1) {
            if (window.top.overflyGen.Alert('N�mero de dias n�o pode ser inferior a 1 (um)') == 0)
                return null;
            lockControlsInModalWin(false);
            txtValidadeProposta.focus();
            return null;
        }
        propostaSolicitacao(selReports.value);
    }
    else if ((selReports.value == '40134') && (glb_sCaller == 'S'))
        ListaEmbalagem();
    else if ((selReports.value == '40147') && (glb_sCaller == 'S'))
        call_DemonstrativoCalculoICMSST();
    else if ((selReports.value == '40135') && (glb_sCaller == 'S'))
        etiquetasEmbalagem();
    else if ((selReports.value == '40136') && (glb_sCaller == 'S')) {
        sendJSMessage(getHtmlId(), JS_WIDEMSG, 'LABELBARCODE_GAP', txtLabelGap.value);
        barCode();
        //etiquetaLocalizacao();
    }
    else if ((selReports.value == '40137') && (glb_sCaller == 'PL'))
        retiradaMercadoria();
    else if ((selReports.value == '40139') && (glb_sCaller == 'PL') && (selPadrao.value != 956))
        relatorioVendas();
    else if ((selReports.value == '40142') && (glb_sCaller == 'PL'))
        relatorioCompras();
    else if ((selReports.value == '40140') && (glb_sCaller == 'PL'))
        totalizacaoVendas();
    else if ((selReports.value == 40143) && (glb_sCaller == 'PL'))
        relatorioTransportadoras();
    else if ((selReports.value == 40145) && (glb_sCaller == 'S'))
        relatorioFabricantes();
    else if ((selReports.value == 40146) && (glb_sCaller == 'PL'))
        relatorioMinutaTransporte(selMinuta.value, selFormatoMinuta.value, 1);
    else if ((selReports.value == 40148) && (glb_sCaller == 'PL'))
        relatorioComissao();
    else if ((selReports.value == 40151) && (glb_sCaller == 'S')) {
        var sModeloNF = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ModeloNF\'].value');
        var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'EmpresaID\'].value');
        var nDocumentoFiscalID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'DocumentoFiscalID\'].value');
        var sURL = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ArquivoDanfePDF\'].value');

        // Quando � NFe
        if ((sModeloNF == "55") || (sModeloNF == "65")) {
            showFrameInHtmlTop('frameModal', false);
            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'openModalViewXML()');
        }
        else {
            if ((nDocumentoFiscalID == 1124) && (nEmpresaID != 7)) {
                if (sURL != null)
                    window.open(sURL);
                else
                    window.top.overflyGen.Alert('N�o h� nota para ser impressa.');
            }
            else if (nEmpresaID == 7)
                imprimeNotaFiscal();
            else
                window.top.overflyGen.Alert('N�o � poss�vel imprimir essa nota fiscal.');

            sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
        }
    }
    //else if ((selReports.value == 40172) && (glb_sCaller == 'S'))
        // Este relatorio se encontra na madalprint do form de ordens de producao - Desabilitado no Projeto Overfly 2.0 - MTH 04/01/2018
        //relatorioOrdemProducao(1, true);
    else if ((selReports.value == 40182) && (glb_sCaller == 'S'))
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, 'BLOQUETO');
    else
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
}

/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID() {
    var i, coll, attr, currRep;

    attr = null;
    currRep = 0;

    currRep = selReports.value;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (attr == currRep)
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';
        }
    }
}



/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr() {
    var i, coll, attr, retVal;

    attr = null;
    retVal = 0;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (coll[i].style.visibility == 'visible') {
                retVal = attr;
                break;
            }
        }
    }
    return retVal;
}


/********************************************************************
Ajusta os elementos labels e checkbox do divEtiquetasEmbalagem
********************************************************************/
function adjustDivEtiquetasEmbalagem() {
    var i;
    var elem;
    var hGap = 0;
    var labelLeft;
    var labelTop;
    var labelWidth;
    var labelFloor;
    var lineY = 0;

    // lblEtiquetaVertical
    elem = lblEtiquetaVertical;
    with (elem.style) {
        backgroundColor = 'transparent';
        left = hGap;
        top = lineY;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }

    // txtEtiquetaVertical
    elem = txtEtiquetaVertical;
    with (elem.style) {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // lblEtiquetaHorizontal
    elem = lblEtiquetaHorizontal;
    with (elem.style) {
        backgroundColor = 'transparent';
        left = hGap + ELEM_GAP;
        top = lineY;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }

    // txtEtiquetaHorizontal
    elem = txtEtiquetaHorizontal;
    with (elem.style) {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // lblAltura
    elem = lblAltura;
    with (elem.style) {
        backgroundColor = 'transparent';
        left = hGap + ELEM_GAP;
        top = lineY;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }

    // txtAltura
    elem = txtAltura;
    with (elem.style) {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // lblLargura
    elem = lblLargura;
    with (elem.style) {
        backgroundColor = 'transparent';
        left = hGap + ELEM_GAP;
        top = lineY;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }

    // txtLargura
    elem = txtLargura;
    with (elem.style) {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // lblMargemSuperior
    elem = lblMargemSuperior;
    with (elem.style) {
        backgroundColor = 'transparent';
        left = hGap + ELEM_GAP;
        top = lineY;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }

    // txtMargemSuperior
    elem = txtMargemSuperior;
    with (elem.style) {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // lblMargemEsquerda
    elem = lblMargemEsquerda;
    with (elem.style) {
        backgroundColor = 'transparent';
        left = hGap + ELEM_GAP;
        top = lineY;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }

    // txtMargemEsquerda
    elem = txtMargemEsquerda;
    with (elem.style) {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    hGap = 0;
    lineY = parseInt(txtMargemEsquerda.currentStyle.top, 10) +
            parseInt(txtMargemEsquerda.currentStyle.height, 10) +
            ELEM_GAP;

    // lblInicio
    elem = lblInicio;
    with (elem.style) {
        backgroundColor = 'transparent';
        left = hGap;
        top = lineY;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }

    // txtInicio
    elem = txtInicio;
    with (elem.style) {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // lblFim
    elem = lblFim;
    with (elem.style) {
        backgroundColor = 'transparent';
        left = hGap + ELEM_GAP;
        top = labelTop;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
    }

    // txtFim
    elem = txtFim;
    with (elem.style) {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // valores Default nos campos
    txtInicio.value = 1;
    txtFim.value = glb_nNumeroVolumes;

    txtEtiquetaVertical.value = 8;
    txtEtiquetaHorizontal.value = 1;
    txtAltura.value = 12;
    txtLargura.value = 107;
    txtMargemSuperior.value = 0;
    txtMargemEsquerda.value = 3;
}

function adjustDivRelatorioVendas() {
    txtDataInicio1.maxLength = 10;
    txtDataFim1.maxLength = 10;

    txtDataInicio1.value = glb_dCurrDate;
    txtDataFim1.value = glb_dCurrDate;

    selFabricante.onchange = selFabricante_OnChange;
    selPadrao.onchange = selPadrao_OnChange;
    selFamilias.onchange = selFamilias_onchange;

    chkSoResultado.checked = true;

    adjustElementsInForm([['lblFabricante', 'selFabricante', 19, 1, -10, -10],
                          ['lblPadrao', 'selPadrao', 19, 2, -10, 0],
                          ['lblSoResultado', 'chkSoResultado', 3, 3, -10],
                          ['lblDadosParceiro', 'chkDadosParceiro', 3, 3, -3],
                          ['lblDadosProduto', 'chkDadosProduto', 3, 3, -3],
                          ['lblDadosResultado', 'chkDadosResultado', 3, 3, -3],
                          ['lblReportaFabricante', 'chkReportaFabricante', 3, 3, -3],
                          ['lblPedidosWeb2', 'chkPedidosWeb2', 3, 3, -3],
                          ['lblComissaoVendas', 'chkComissaoVendas', 3, 3, -3],
                          ['lblMicrosoftTipoRelatorio', 'selMicrosoftTipoRelatorio', 4, 3, -37],
                          ['lblMoedaLocal2', 'chkMoedaLocal2', 3, 3, -16],
                          ['lblDataInicio1', 'txtDataInicio1', 13, 4, -10],
                          ['lblEmpresas2', 'selEmpresas2', 13, 3],
                          ['lblDataFim1', 'txtDataFim1', 13, 4],
                          ['lblFormatoRelatorioVendas', 'selFormatoRelatorioVendas', 10, 5, -10],
                          ['lblFiltro1', 'txtFiltro1', 50, 5, -90, 6]], null, null, true);

    lblFamilias.style.left = 228;
    lblFamilias.style.top = 100;
    selFamilias.style.left = 227;
    selFamilias.style.top = 118;
    selFamilias.style.width = 173;
    selFamilias.style.height = 118;

    txtFiltro1.style.top = 240;
    lblFiltro1.style.top = 223;
    lblEmpresas2.style.left = 227;
    lblEmpresas2.style.top = -6;
    selEmpresas2.style.left = 227;
    selEmpresas2.style.top = 9;
    selEmpresas2.style.width = 174;
    selEmpresas2.style.height = 89;
    txtDataFim1.style.left = 110;
    lblDataFim1.style.left = 110;
    chkMoedaLocal2.style.left = 182;
    lblMoedaLocal2.style.left = 187;

    chkDadosResultado.onclick = chkDadosResultado_onclick;

    lblComissaoVendas.style.visibility = 'hidden';
    chkComissaoVendas.style.visibility = 'hidden';
    lblMicrosoftTipoRelatorio.style.visibility = 'hidden';
    selMicrosoftTipoRelatorio.style.visibility = 'hidden';
    lblMoedaLocal2.style.visibility = 'hidden';
    chkMoedaLocal2.style.visibility = 'hidden';


    glb_nDynamicCmbss = 3;

    setConnection(dsoFabricantes);
    dsoFabricantes.SQL = 'SELECT 0 AS fldID, SPACE(20) AS fldName, 0 AS EhDefault ' +
                    'UNION ALL ' +
                    'SELECT DISTINCT c.PessoaID AS fldID, c.Fantasia AS fldName, 0 AS EhDefault ' +
                    'FROM RelacoesPesCon a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
                    'WHERE a.SujeitoID=' + glb_nEmpresaID + ' AND a.ObjetoID=b.ConceitoID AND ' +
                    '(a.EstadoID=2 OR a.EstadoID>=6) AND b.FabricanteID=c.PessoaID ' +
                    'ORDER BY fldName';

    dsoFabricantes.ondatasetcomplete = relatorioVendas_DSC1;
    dsoFabricantes.Refresh();

    setConnection(dsoFamilias);

    dsoFamilias.SQL = 'SELECT ConceitoID AS fldID, Conceito AS fldName, 0 AS EhDefault ' +
                    'FROM Conceitos WITH(NOLOCK) ' +
                    'WHERE (EstadoID=2 AND TipoConceitoID=302) ' +
                    'ORDER BY Conceito';

    dsoFamilias.ondatasetcomplete = relatorioVendas_DSC2;
    dsoFamilias.Refresh();

    setConnection(dsoPadrao);

    dsoPadrao.SQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, EhDefault AS EhDefault, Filtro as Filtro " +
                    "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                    "WHERE (EstadoID=2 AND TipoID=410  AND Filtro LIKE \'%<VEN>%\') " +
                    "ORDER BY EhDefault DESC, ItemMasculino";
    //dsoPadrao.SQL = 'SELECT 0 AS fldID, 0 AS fldName, 0 AS EhDefault, 0 as Filtro ' ;
    dsoPadrao.ondatasetcomplete = relatorioVendas_DSC3;
    dsoPadrao.Refresh();
}

function adjustDivDemonstrativoCalculoICMSST() {
    adjustElementsInForm([['lblFormatoSolicitacao2', 'selFormatoSolicitacao2', 10, 1, -8, 0]], null, null, true);
}

function adjustDivRelatorioCompras() {
    txtDataInicio3.maxLength = 10;
    txtDataFim3.maxLength = 10;

    txtDataInicio3.value = glb_dCurrDate;
    txtDataFim3.value = glb_dCurrDate;

    selFabricante2.onchange = selFabricante2_OnChange;
    selPadrao2.onchange = selPadrao2_OnChange;

    adjustElementsInForm([['lblFabricante2', 'selFabricante2', 13, 1, -10, -10],
                          ['lblPadrao2', 'selPadrao2', 13, 1],
                          ['lblMoedaLocal3', 'chkMoedaLocal3', 3, 2, -10],
                          ['lblDataInicio3', 'txtDataInicio3', 13, 3, -10],
                          ['lblDataFim3', 'txtDataFim3', 13, 3],
                          ['lblFiltro3', 'txtFiltro3', 50, 4, -10, 6]], null, null, true);

    lblFamilias2.style.left = 228;
    lblFamilias2.style.top = -3;
    selFamilias2.style.left = 227;
    selFamilias2.style.top = 12;
    selFamilias2.style.width = 173;
    selFamilias2.style.height = 118;
    chkMoedaLocal3.checked = true;
}

function adjustDivRetiradaMercadoria() {
    adjustElementsInForm([['lblPessoa', 'selPessoa', 23, 1, -10, -10]], null, null, true);

    setConnection(dsoPessoas);
    dsoPessoas.SQL =
        'SELECT Pedidos.PessoaID, Pessoas.Fantasia, Pedidos.PedidoID, Empresas.NumeroExpedicao ' +
        'FROM Pedidos WITH(NOLOCK), RelacoesPesRec Empresas WITH(NOLOCK), Pessoas WITH(NOLOCK) ' +
        'WHERE (Pedidos.EmpresaID = Empresas.SujeitoID AND Empresas.ObjetoID = 999 AND ' +
            'Empresas.TipoRelacaoID = 12 AND Pedidos.PessoaID = Pessoas.PessoaID AND Pedidos.EstadoID = 29 AND ' +
            'Pedidos.ParceiroID = Pedidos.TransportadoraID AND Pedidos.TipoPedidoID = 602 AND Pedidos.EmpresaID = ' + glb_nEmpresaID + ') ' +
        'ORDER BY Pessoas.Fantasia';

    dsoPessoas.ondatasetcomplete = retiradaMercadoria_DSC;
    dsoPessoas.Refresh();
}

function retiradaMercadoria_DSC() {
    var i;
    var aDSOs = [dsoPessoas];
    var aCmbs = [selPessoa];
    clearComboEx(['selPessoa']);
    var nIDBefore = -1;
    var nSelectedIndex = -1;
    var nPedidoID = -1;
    var nCount = -1;

    for (i = 0; i < aCmbs.length; i++) {

        if (sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'fg.Rows > 1'))
            nPedidoID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'fg.TextMatrix(fg.Row, 0)');


        while (!aDSOs[i].recordset.EOF) {

            if (aDSOs[i].recordset['PessoaID'].value != nIDBefore) {
                var oOption = document.createElement("OPTION");
                oOption.text = aDSOs[i].recordset['Fantasia'].value;
                oOption.value = aDSOs[i].recordset['PessoaID'].value;
                aCmbs[i].add(oOption);

                nCount++;

                if (nPedidoID == aDSOs[i].recordset['PedidoID'].value)
                    nSelectedIndex = nCount;

            }

            nIDBefore = aDSOs[i].recordset['PessoaID'].value;
            aDSOs[i].recordset.MoveNext();
        }

        if (nSelectedIndex > -1)
            selPessoa.selectedIndex = nSelectedIndex;
    }

    if (selPessoa.options.length > 0)
        selPessoa.disabled = false;
    else
        selPessoa.disabled = true;
}

function chkDadosResultado_onclick() {
    var sMoedaVisibility = (chkDadosResultado.checked ? 'inherit' : 'hidden');

    lblMoedaLocal2.style.visibility = sMoedaVisibility;
    chkMoedaLocal2.style.visibility = sMoedaVisibility;
}

function relatorioVendas_DSC1() {

    var i;
    clearComboEx(['selFabricante']);

    while (!dsoFabricantes.recordset.EOF) {
        optionStr = dsoFabricantes.recordset['fldName'].value;
        optionValue = dsoFabricantes.recordset['fldID'].value;
        select = dsoFabricantes.recordset['EhDefault'].value;

        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        oOption.selected = select;

        selFabricante.add(oOption);
        dsoFabricantes.recordset.MoveNext();
    }
    glb_nDynamicCmbss = glb_nDynamicCmbss - 1;
    relatorioCompras_DSC();
}
function relatorioVendas_DSC2() {

    var i;

    clearComboEx(['selFamilias']);

    while (!dsoFamilias.recordset.EOF) {
        optionStr = dsoFamilias.recordset['fldName'].value;
        optionValue = dsoFamilias.recordset['fldID'].value;
        select = dsoFamilias.recordset['EhDefault'].value;

        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        oOption.selected = select;

        selFamilias.add(oOption);
        dsoFamilias.recordset.MoveNext();
    }

    glb_nDynamicCmbss = glb_nDynamicCmbss - 1;
    relatorioCompras_DSC();
}
function relatorioVendas_DSC3() {

    var i;
    clearComboEx(['selPadrao']);

    while (!dsoPadrao.recordset.EOF) {
        optionStr = dsoPadrao.recordset['fldName'].value;
        optionValue = dsoPadrao.recordset['fldID'].value;
        // select = dsoPadrao.recordset['EhDefault'].value;
        //filtro = dsoPadrao.recordset['Filtro'].value;

        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        //oOption.selected = select;
        //oOption.setAttribute = filtro

        selPadrao.add(oOption);
        dsoPadrao.recordset.MoveNext();
    }

    glb_nDynamicCmbss = glb_nDynamicCmbss - 1;
    selFamilias_onchange();
    relatorioCompras_DSC();
}

function relatorioCompras_DSC() {
    var i;

    if (glb_nDynamicCmbss > 0)
        return null;

    var aDSOs = [dsoFabricantes, dsoFamilias, dsoPadrao];
    var aCmbs = [selFabricante2, selFamilias2, selPadrao2];
    clearComboEx(['selFabricante2', 'selFamilias2', 'selPadrao2']);

    for (i = 0; i < aCmbs.length; i++) {
        aDSOs[i].recordset.MoveFirst();

        while (!aDSOs[i].recordset.EOF) {

            if ((aCmbs[i] == selPadrao) || (aCmbs[i] == selPadrao2)) {
                // Preenche combo padr�o do Relat�rio de vendas
                if (selReports.value == 40139) {
                    if (!aDSOs[i].recordset['Filtro'].value.match(/VEN/)) {
                        aDSOs[i].recordset.MoveNext();
                        continue;
                    }
                }
                    // Preenche combo padr�o do Relat�rio de compras
                else if (selReports.value == 40142) {
                    if (!aDSOs[i].recordset['Filtro'].value.match(/COM/)) {
                        aDSOs[i].recordset.MoveNext();
                        continue;
                    }
                }
            }

            var oOption = document.createElement("OPTION");
            oOption.text = aDSOs[i].recordset['fldName'].value;
            oOption.value = aDSOs[i].recordset['fldID'].value;
            if (aDSOs[i].recordset['EhDefault'].value == true)
                oOption.selected = true;
            aCmbs[i].add(oOption);
            aDSOs[i].recordset.MoveNext();
        }
    }

    //loadDataAndTreatInterface();
}

function adjustDivBarCode() {
    var aBarCode;
    var nPrintBarcode = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrPrinterBarCode()');

    txtInicio3.maxLength = 25;
    txtFim3.maxLength = 25;

    if (glb_nNumeroSerieID == 0) {
        aBarCode = [[1, 'Faixa'],
                    [2, 'Todas']];
    }
    else {
        aBarCode = [[0, 'S� esta'],
                    [1, 'Faixa'],
                    [2, 'Todas']];

        lblInicio3.style.visibility = 'hidden';
        txtInicio3.style.visibility = 'hidden';
        lblFim3.style.visibility = 'hidden';
        txtFim3.style.visibility = 'hidden';
    }

    txtVias.value = '1';

    // Impressora Yanco
    if (nPrintBarcode == 1) {
        txtLabelHeight.disabled = true;
        lblLabelHeight.style.visibility = 'hidden';
        txtLabelHeight.style.visibility = 'hidden';
        txtLabelGap.disabled = true;
        lblLabelGap.style.visibility = 'hidden';
        txtLabelGap.style.visibility = 'hidden';
    }
    else {
        txtLabelHeight.disabled = false;
        lblLabelHeight.style.visibility = 'inherit';
        txtLabelHeight.style.visibility = 'inherit';
        txtLabelGap.disabled = false;
        lblLabelGap.style.visibility = 'inherit';
        txtLabelGap.style.visibility = 'inherit';
    }

    txtLabelHeight.value = LABELBARCODE_HEIGHT;
    txtLabelGap.value = sendJSMessage(getHtmlId(), JS_WIDEMSG, 'LABELBARCODE_GAP', null);

    var i, j, optionStr, optionValue;
    var aCmbsDynamics = [selEtiquetas];
    var aaCombos = [aBarCode];

    clearComboEx(['selEtiquetas']);

    for (i = 0; i <= aCmbsDynamics.length - 1; i++) {
        for (j = 0; j <= aaCombos[i].length - 1; j++) {
            optionValue = aaCombos[i][j][0];
            optionStr = aaCombos[i][j][1];
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
        }
    }

    adjustElementsInForm([['lblEtiquetas', 'selEtiquetas', 13, 1, -10, -10],
						  ['lblTemNumeroSerie', 'chkTemNumeroSerie', 3, 1],
                          ['lblVias', 'txtVias', 3, 1],
                          ['lblLabelHeight', 'txtLabelHeight', 4, 1],
                          ['lblLabelGap', 'txtLabelGap', 4, 1],
                          ['lblInicio3', 'txtInicio3', 25, 2, -10],
                          ['lblFim3', 'txtFim3', 25, 2]], null, null, true);

    chkTemNumeroSerie.checked = true;
}

function adjustDivSolicitacaoCompra() {
    if (glb_sCaller != 'S')
        return null;

    glb_nDSOsSolicitacao = 2;

    var nParceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                     'dsoSup01.recordset[' + '\'' + 'ParceiroID' + '\'' + '].value');

    setConnection(dsoContato1);

    dsoContato1.SQL =
        'SELECT b.ContatoID AS PessoaID, c.Fantasia + CHAR(45) + b.Cargo AS Contato, b.Cargo,' +
        '(SELECT TOP 1 URL AS EMailContato FROM Pessoas_URLs WITH(NOLOCK) WHERE Pessoas_URLs.PessoaID = c.PessoaID AND Pessoas_URLs.TipoURLID=124 AND Pessoas_URLs.Ordem = 1) AS EMailContato, ' +
        ' b.Ordem AS Ordem ' +
            'FROM Relacoespessoas a WITH(NOLOCK), Relacoespessoas_Contatos b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
            'WHERE a.SujeitoID=' + glb_nEmpresaID + ' AND a.ObjetoID=' + nParceiroID + ' AND ' +
                'a.RelacaoID=b.RelacaoID AND b.ContatoID=c.PessoaID AND b.Comercial=1 ' +
        'UNION ALL ' +
        'SELECT a.PessoaID, a.Nome AS Contato, SPACE(0) AS Cargo,' +
        '(SELECT TOP 1 URL AS EMailContato FROM Pessoas_URLs WITH(NOLOCK) WHERE Pessoas_URLs.PessoaID = a.PessoaID AND Pessoas_URLs.TipoURLID=124 AND Pessoas_URLs.Ordem = 1) AS EMailContato, ' +
        '99 AS Ordem ' +
            'FROM Pessoas a WITH(NOLOCK) ' +
            'WHERE a.PessoaID=' + nParceiroID + ' ' +
        'ORDER BY Ordem';

    dsoContato1.ondatasetcomplete = dsoContato1_DSC;
    dsoContato1.Refresh();

    setConnection(dsoProprietarios);

    dsoProprietarios.SQL = 'SELECT DISTINCT dbo.fn_Pessoa_URL(c.PessoaID, 124, NULL) AS Email ' +
								'FROM Pedidos_Itens a WITH(NOLOCK), RelacoesPesCon b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
								'WHERE (a.PedidoID = ' + glb_nPedidoID + ' AND a.ProdutoID = b.ObjetoID AND ' +
									'b.TipoRelacaoID = 61 AND b.SujeitoID = ' + glb_nEmpresaID + ' AND ' +
									'b.ProprietarioID = c.PessoaID AND c.EstadoID = 2 AND ' +
									'dbo.fn_Pessoa_URL(c.PessoaID, 124, NULL) IS NOT NULL)';

    dsoProprietarios.ondatasetcomplete = dsoContato1_DSC;
    dsoProprietarios.Refresh();
}

function dsoContato1_DSC() {
    glb_nDSOsSolicitacao--;

    if (glb_nDSOsSolicitacao > 0)
        return null;

    var optionValue;
    var optionStr;

    clearComboEx(['selContato1']);
    while (!dsoContato1.recordset.EOF) {
        optionValue = dsoContato1.recordset.Fields['PessoaID'].value;
        optionStr = dsoContato1.recordset.Fields['Contato'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selContato1.add(oOption);
        oOption.setAttribute('EMailContato', dsoContato1.recordset.Fields['EMailContato'].value, 1);
        dsoContato1.recordset.MoveNext();
    }

    txtValidadeSolicitacao.maxLength = 2;
    txtValidadeSolicitacao.value = 3;

    adjustElementsInForm([['lblContato1', 'selContato1', 30, 1, -10, -10],
                          ['lblValidadeSolicitacao', 'txtValidadeSolicitacao', 3, 1, -2],
                          ['lblInformacoesAdicionais', 'chkInformacoesAdicionais', 3, 1, -3],
                          ['lblFichaTecnica2', 'chkFichaTecnica2', 3, 1, -4],
                          ['lblObservacoes2', 'chkObservacoes2', 3, 1, -4],
                          ['lblFormatoSolicitacao', 'selFormatoSolicitacao', 8, 1],
                          ['lbltoSolicitacao', 'txtToSolicitacao', 70, 2, -10],
                          ['lblCCSolicitacao', 'txtCCSolicitacao', 70, 3, -10],
                          ['lblCCoSolicitacao', 'txtCCoSolicitacao', 70, 4, -10]
    ], null, null, true);

    chkFichaTecnica2.checked = false;
    chkInformacoesAdicionais.checked = false;
    chkObservacoes2.checked = false;

    selFormatoSolicitacao.onchange = selFormato_onchange;
    adjustDivSolicitacaoCompra_Email();
}

function adjustDivPedido() {
    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');
    /*
    setConnection(dsoProposta);

    dsoProposta.SQL = 'SELECT (SELECT Moedas.SimboloMoeda FROM Pedidos WITH(NOLOCK), Conceitos Moedas WITH(NOLOCK) WHERE Pedidos.PedidoID= ' + nPedidoID + ' AND ' +
			'Moedas.ConceitoID=Pedidos.MoedaID) AS Moeda, ' +
		'(SELECT MoedasConvertidas.SimboloMoeda ' +
			'FROM Conceitos MoedasConvertidas WITH(NOLOCK), Recursos Sistema WITH(NOLOCK) ' +
			'WHERE Sistema.RecursoID=999 AND Sistema.MoedaID = MoedasConvertidas.ConceitoID) AS MoedaConversao';

    dsoProposta.ondatasetcomplete = dsoDivPedido_DSC;
    dsoProposta.Refresh();
    */
}

function dsoDivPedido_DSC() {
    return null;
}

function adjustDivPropostaComercial() {
    if (glb_sCaller != 'S')
        return null;

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var nParceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                     'dsoSup01.recordset[' + '\'' + 'ParceiroID' + '\'' + '].value');
    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

    glb_nDSOsProposta = 2;

    setConnection(dsoContato2);

    dsoContato2.SQL =
           'SELECT 0 AS Indice, b.ContatoID AS PessoaID, c.Fantasia + ISNULL(CHAR(45) + b.Cargo, SPACE(0)) AS Contato, b.Cargo, b.Ordem AS Ordem, ' +
            '(SELECT TOP 1 URL AS EMailContato FROM Pessoas_URLs WITH(NOLOCK) WHERE Pessoas_URLs.PessoaID = c.PessoaID AND Pessoas_URLs.TipoURLID=124 AND Pessoas_URLs.Ordem = 1) AS EMailContato, 0 AS EhMicro ' +
            'FROM Relacoespessoas a WITH(NOLOCK), Relacoespessoas_Contatos b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
            'WHERE a.ObjetoID=' + glb_nEmpresaID + ' AND a.SujeitoID=' + nParceiroID + ' AND ' +
                'a.RelacaoID=b.RelacaoID AND b.ContatoID=c.PessoaID AND b.Comercial=1  AND c.EstadoID <> 4 ' +
        'UNION ALL ' +
        'SELECT 1 AS Indice, PessoaID, Fantasia AS Contato, SPACE(0) AS Cargo, 99 AS Ordem, ' +
        '(SELECT TOP 1 URL AS EMailContato FROM Pessoas_URLs WITH(NOLOCK) WHERE Pessoas_URLs.PessoaID = Pessoas.PessoaID AND Pessoas_URLs.TipoURLID=124 AND Pessoas_URLs.Ordem = 1) AS EMailContato, 0 AS EhMicro ' +
            'FROM Pessoas WITH(NOLOCK) ' +
            'WHERE PessoaID=' + nParceiroID + ' ' +
        'UNION ALL ' +
		'SELECT 2 AS Indice, 0 AS PessoaID, SPACE(0) AS Contato, SPACE(0) AS Cargo, 0 AS Ordem, SPACE(0) AS EMailContato, COUNT(*) AS EhMicro ' +
			'FROM Pedidos_Itens a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
			'WHERE (a.PedidoID = ' + glb_nPedidoID + ' AND ' +
				'a.ProdutoID = b.ConceitoID AND b.ProdutoID = c.ConceitoID AND c.EhMicro = 1) ' +
        'ORDER BY Indice, Ordem';

    dsoContato2.ondatasetcomplete = dsoContato2_DSC;
    dsoContato2.Refresh();

    setConnection(dsoProposta);

    dsoProposta.SQL = 'SELECT dbo.fn_Data_Zero(Pedidos.dtPrevisaoEntrega) AS dtPrevisaoEntrega, ' +
		'dbo.fn_Data_Zero(GETDATE()) AS dtHoje, ' +
		'(SELECT Moedas.SimboloMoeda FROM Pedidos WITH(NOLOCK), Conceitos Moedas WITH(NOLOCK) WHERE Pedidos.PedidoID=' + nPedidoID + ' AND ' +
		'Moedas.ConceitoID=Pedidos.MoedaID) AS Moeda,' +
		'(SELECT MoedasConvertidas.SimboloMoeda ' +
			'FROM Conceitos MoedasConvertidas WITH(NOLOCK), Recursos Sistema WITH(NOLOCK) ' +
			'WHERE Sistema.RecursoID=999 AND Sistema.MoedaID=MoedasConvertidas.ConceitoID) AS MoedaConversao, ' +
		'(CASE WHEN (dbo.fn_DivideZero(dbo.fn_DivideZero(Pedidos.ValorTotalPedido, Pedidos.TaxaMoeda), RelPesRec.FaturamentoMensal) * 100) >= 5 ' +
		'THEN 1 ELSE 0 END) AS SendMailFaturamentoExcedido, dbo.fn_Pessoa_URL(' + aEmpresaData[0] + ', 124, NULL) AS EmailVendas ' +
		'FROM Pedidos WITH(NOLOCK), RelacoesPesRec RelPesRec WITH(NOLOCK) ' +
		'WHERE Pedidos.PedidoID = ' + nPedidoID + ' AND Pedidos.EmpresaID=RelPesRec.SujeitoID AND RelPesRec.ObjetoID=999 AND ' +
			'RelPesRec.TipoRelacaoID=12';

    dsoProposta.ondatasetcomplete = dsoContato2_DSC;
    dsoProposta.Refresh();
}

function dsoContato2_DSC() {
    glb_nDSOsProposta--;

    if (glb_nDSOsProposta > 0)
        return null;

    var optionValue;
    var optionStr;
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var txtPaisEmpresaID = aEmpresaData[1];

    clearComboEx(['selContato2']);
    while (!dsoContato2.recordset.EOF) {
        if (dsoContato2.recordset.Fields['Indice'].value <= 1) {
            optionValue = dsoContato2.recordset.Fields['PessoaID'].value;
            optionStr = dsoContato2.recordset.Fields['Contato'].value;
            oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            oOption.setAttribute('EMailContato', dsoContato2.recordset.Fields['EMailContato'].value, 1);
            selContato2.add(oOption);
        }
        else {
            glb_nEhMicro = dsoContato2.recordset.Fields['EhMicro'].value;
        }

        dsoContato2.recordset.MoveNext();
    }

    txtValidadeProposta.maxLength = 2;

    //Foi solicitado pelo Adilson para alterar a Validade "default" do Relat�rio de Proposta Comercial para 1 dia ao inv�s de 3. (#2017060698000317). FRG - 16/06/2017.
    if (txtPaisEmpresaID == 130)
        txtValidadeProposta.value = 1;
    else
        txtValidadeProposta.value = 3;

    adjustElementsInForm([['lblContato2', 'selContato2', 30, 1, -10, -10],
                          ['lblValidadeProposta', 'txtValidadeProposta', 3, 1],
                          ['lblMarca', 'chkMarca', 3, 1],
                          ['lblModelo', 'chkModelo', 3, 1],
                          ['lblFichaTecnica', 'chkFichaTecnica', 3, 1, -5],
                          ['lblSUP', 'chkSUP', 3, 1],
                          ['lblObservacoes', 'chkObservacoes', 3, 1],
                          ['lblFormatoProposta', 'selFormatoProposta', 8, 1],
                          ['lblto', 'txtTo', 70, 2, -10],
                          ['lblCC', 'txtCC', 70, 3, -10],
                          ['lblCCo', 'txtCCo', 70, 4, -10]], null, null, true);


    selFormatoProposta.onchange = selFormato_onchange;
    adjustDivPropostaComercial_Email();

    // Alcabyt
    if (aEmpresaData[0] == 3) {
        chkMarca.checked = false;
        chkModelo.checked = false;
        chkFichaTecnica.checked = false;
        chkSUP.checked = false;
        chkObservacoes.checked = false;
    }
    else {

        chkMarca.checked = true;
        chkModelo.checked = true;
        chkFichaTecnica.checked = true;
        chkSUP.checked = false;
        chkObservacoes.checked = false;
    }
}

function adjustDivListaEmbalagem() {
    adjustElementsInForm([['lblFormatoListaEmbalagem', 'selFormatoListaEmbalagem', 10, 1, -8, 0]], null, null, true);
}

function adjustDivPedidoRetorno() {
    adjustElementsInForm([['lblGerarPedido', 'chkGerarPedido', 3, 1, -10, -10]], null, null, true);
}

function adjustDivOrdemDeProducao() {
    return null;
}

function adjustDivRelatorioTransportadora() {
    if (glb_sCaller != 'PL')
        return null;

    var optionValue;
    var optionStr;

    txtDataInicioTransp.maxLength = 10;
    txtDataFimTransp.maxLength = 10;

    txtDataInicioTransp.value = glb_dCurrDate;
    txtDataFimTransp.value = glb_dCurrDate;

    adjustElementsInForm([['lblTransportadora', 'selTransportadora', 24, 1, -10, -10],
						  ['btnTransportadoras', 'btn', 10, 1],
                          ['lblFretePago', 'chkFretePago', 3, 2, -10],
                          ['lblDataInicioTransp', 'txtDataInicioTransp', 10, 2, -5],
                          ['lblDataFimTransp', 'txtDataFimTransp', 10, 2, -5],
                          ['lblFiltroTransp', 'txtFiltroTransp', 50, 3, -10]], null, null, true);

    selTransportadora.disabled = true;
    selTransportadora.onchange = selTransportadora_onchange;

    btnTransportadoras.style.width = parseInt(btnOK.currentStyle.width, 10) + 8;
    btnTransportadoras.style.height = btnOK.style.height;
    btnTransportadoras.style.top = parseInt(btnTransportadoras.style.top, 10) - 3;
}

function adjustDivRelatorioDeposito() {
    if (glb_sCaller != 'PL')
        return null;

    var optionValue;
    var optionStr;

    txtDataInicioLiberacao.maxLength = 10;
    txtDataFimLiberacao.maxLength = 10;

    txtDataInicioLiberacao.value = glb_dCurrDate;
    txtDataFimLiberacao.value = glb_dCurrDate;

    adjustElementsInForm([['lblDeposito', 'selDeposito', 24, 1, -10, -10],
						  ['btnDeposito', 'btn', 10, 1],
						  ['lblZ', 'chkZ', 3, 4, -10],
                          ['lblC', 'chkC', 3, 4, -6],
                          ['lblE', 'chkE', 3, 4, -6],
                          ['lblF', 'chkF', 3, 4, -6],
                          ['lblD', 'chkD', 3, 4, -6],
                          ['lblL', 'chkL', 3, 4, -6],
                          ['lblEntrada', 'chkEntrada', 3, 3, -10],
                          ['lblSaida', 'chkSaida', 3, 3, -6],
                          ['lblFP', 'chkFP', 3, 2, -10],
                          ['lblDataInicioLiberacao', 'txtDataInicioLiberacao', 15, 2, -5],
                          ['lblDataFimLiberacao', 'txtDataFimLiberacao', 15, 2, -5],
                          ['lblFiltroDeposito', 'txtFiltroDeposito', 50, 5, -10]], null, null, true);

    //selDeposito.disabled = true;
    //selDeposito.onchange = selDeposito_onchange;

    btnDeposito.style.width = parseInt(btnOK.currentStyle.width, 10) + 8;
    btnDeposito.style.height = btnOK.style.height;
    btnDeposito.style.top = parseInt(btnDeposito.style.top, 10) - 3;

    // campo txtDataEstoque digitacao maxima: 12/12/2002 12:12:12
    txtDataInicioLiberacao.maxLength = 19;
    txtDataFimLiberacao.maxLength = 19;
    // e seleciona conteudo quando em foco
    txtDataInicioLiberacao.onfocus = selFieldContent;
    txtDataFimLiberacao.onfocus = selFieldContent;
    // e so aceita caracteres coerentes com data
    txtDataInicioLiberacao.onkeypress = verifyDateTimeNotLinked;
    txtDataFimLiberacao.onkeypress = verifyDateTimeNotLinked;
}
function adjustdivComissoes() {
    if (glb_sCaller != 'PL')
        return null;

    adjustElementsInForm([['lblTipoComissao', 'selTipoComissaoID', 25, 1, -10, -10],
                          ['lblEmpresaComissao', 'selEmpresaComissao', 20, 1],
						  ['lblEstadoComissao', 'selEstadoComissaoID', 25, 2, -10],
                          ['lbldtVencimentoInicio', 'txtdtVencimentoInicio', 12, 3, -10],
                          ['lbldtVencimentoFim', 'txtdtVencimentoFim', 12, 3],
                          ['lblProprietario', 'chkProprietario', 3, 4, -10],
                          ['lblArgumento', 'txtArgumento', 15, 4, -2],
                          ['btnPesquisarParceiroProprietario', 'btn', 25, 4],
                          ['lblParceiroProprietario', 'selParceirosProprietariosID', 25, 4, -2],
                          ['lblFiltroComissoes', 'txtFiltroComissoes', 39, 5, -10],
                          ['lblFormatoDivComissoes', 'selFormatoDivComissoes', 8, 5, -2]],
                          null, null, true);

    selEmpresaComissao.style.height = 118;
    selEmpresaComissao.style.width = 170;
    chkProprietario.onclick = chkProprietario_onclick;
    txtArgumento.maxLength = 14;

    txtArgumento.onkeypress = txtArgumento_onkeypress;

    selParceirosProprietariosID.disabled = true;
}

function adjustdivMinutaTransporte() {
    if (glb_sCaller != 'PL')
        return null;

    var optionValue;
    var optionStr;

    txtDataInicioTransp.maxLength = 10;
    txtDataFimTransp.maxLength = 10;

    txtDataInicioTransp.value = glb_dCurrDate;
    txtDataFimTransp.value = glb_dCurrDate;

    adjustElementsInForm([['lbldtInicio', 'txtdtInicio', 10, 1, -10, -10],
						  ['lbldtFim', 'txtdtFim', 10, 1],
                          ['lblPesquisa', 'txtPesquisa', 20, 1],
                          ['lblTransportadoraMT', 'selTransportadoraMT', 21, 2, -10],
                          ['btnPesquisar', 'btn', 20, 2, 6, -2],
                          ['lblMinuta', 'selMinuta', 32, 3, -10],
                          ['lblFormatoMinuta', 'selFormatoMinuta', 7, 4, -10]], null, null, true);

    txtdtInicio.maxLength = 10;
    txtdtFim.maxLength = 10;

    //selMinuta.disabled = true;

    btnPesquisar.style.width = parseInt(btnOK.currentStyle.width, 10);
    btnPesquisar.style.height = btnOK.style.height;
    selTransportadoraMT.onchange = selTransportadoraMT_onchange;

    setConnection(dsoTransportadoraMinuta);

    dsoTransportadoraMinuta.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
                    'UNION ALL ' +
                    'SELECT DISTINCT b.PessoaID AS fldID, b.Fantasia AS fldName ' +
                    'FROM dbo.vw_Pedidos_MinutasTransporte a WITH(NOEXPAND) ' +
							'INNER JOIN Pessoas b WITH(NOLOCK) ON a.TransportadoraID = b.PessoaID ' +
                    'WHERE a.EmpresaID=' + glb_nEmpresaID + ' ' +
                    'ORDER BY fldName';

    dsoTransportadoraMinuta.ondatasetcomplete = transportadorasMinuta_DSC;
    dsoTransportadoraMinuta.Refresh();

}

function transportadorasMinuta_DSC() {
    var optionValue;
    var optionStr;
    var oOption;

    clearComboEx(['selTransportadoraMT']);

    while (!dsoTransportadoraMinuta.recordset.EOF) {
        optionValue = dsoTransportadoraMinuta.recordset.Fields['fldID'].value;
        optionStr = dsoTransportadoraMinuta.recordset.Fields['fldName'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selTransportadoraMT.add(oOption);
        dsoTransportadoraMinuta.recordset.MoveNext();
    }
}

function adjustdivRelatorioFabricante() {
    glb_TotalGrossSKID = Math.max(glb_TotalGrossSKID, glb_TotalPesoBruto);


    adjustElementsInForm([['lblTotalGross', 'txtTotalGross', 7, 1, -10, -10],
	                      ['lblWS', 'chkWS', 3, 1]], null, null, true);

    txtTotalGross.value = glb_TotalGrossSKID;

    txtTotalGross.onfocus = selFieldContent;
    txtTotalGross.onkeypress = verifyNumericEnterNotLinked;
    txtTotalGross.setAttribute('thePrecision', 7, 1);
    txtTotalGross.setAttribute('theScale', 2, 1);
    txtTotalGross.setAttribute('minMax', new Array(0, 99999.99));
    txtTotalGross.setAttribute('verifyNumPaste', 1);
    chkWS.checked = true;
}

function dsoTransportadoras_DSC() {
    clearComboEx(['selTransportadora']);

    while (!dsoTransportadoras.recordset.EOF) {
        optionValue = dsoTransportadoras.recordset.Fields['PessoaID'].value;
        optionStr = dsoTransportadoras.recordset.Fields['Fantasia'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selTransportadora.add(oOption);
        dsoTransportadoras.recordset.MoveNext();
    }

    lockControlsInModalWin(false);

    if (selTransportadora.options.length >= 1)
        selTransportadora.disabled = false;

}

function dsoDeposito_DSC() {
    clearComboEx(['selDeposito']);

    while (!dsoDeposito.recordset.EOF) {
        optionValue = dsoDeposito.recordset.Fields['PessoaID'].value;
        optionStr = dsoDeposito.recordset.Fields['Fantasia'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selDeposito.add(oOption);
        dsoDeposito.recordset.MoveNext();
    }

    lockControlsInModalWin(false);

    if (selDeposito.options.length >= 1)
        selDeposito.disabled = false;

}
/********************************************************************
Restringe quantidade de caracteres digitados, coloca
mascara para digitacao em campos numericos,
seleciona conteudo do campo quando em foco etc
********************************************************************/
function setMaskAndLengthToInputs() {
    txtEtiquetaVertical.onkeypress = verifyNumericEnterNotLinked;
    txtEtiquetaHorizontal.onkeypress = verifyNumericEnterNotLinked;
    txtAltura.onkeypress = verifyNumericEnterNotLinked;
    txtLargura.onkeypress = verifyNumericEnterNotLinked;
    txtMargemSuperior.onkeypress = verifyNumericEnterNotLinked;
    txtMargemEsquerda.onkeypress = verifyNumericEnterNotLinked;
    txtInicio.onkeypress = verifyNumericEnterNotLinked;
    txtFim.onkeypress = verifyNumericEnterNotLinked;

    txtEtiquetaVertical.setAttribute('verifyNumPaste', 1);
    txtEtiquetaHorizontal.setAttribute('verifyNumPaste', 1);
    txtAltura.setAttribute('verifyNumPaste', 1);
    txtLargura.setAttribute('verifyNumPaste', 1);
    txtMargemSuperior.setAttribute('verifyNumPaste', 1);
    txtMargemEsquerda.setAttribute('verifyNumPaste', 1);
    txtInicio.setAttribute('verifyNumPaste', 1);
    txtFim.setAttribute('verifyNumPaste', 1);

    txtEtiquetaVertical.setAttribute('thePrecision', 2, 1);
    txtEtiquetaHorizontal.setAttribute('thePrecision', 2, 1);
    txtAltura.setAttribute('thePrecision', 2, 1);
    txtLargura.setAttribute('thePrecision', 3, 1);
    txtMargemSuperior.setAttribute('thePrecision', 2, 1);
    txtMargemEsquerda.setAttribute('thePrecision', 2, 1);
    txtInicio.setAttribute('thePrecision', 5, 1);
    txtFim.setAttribute('thePrecision', 5, 1);

    txtEtiquetaVertical.setAttribute('theScale', 0, 1);
    txtEtiquetaHorizontal.setAttribute('theScale', 0, 1);
    txtAltura.setAttribute('theScale', 0, 1);
    txtLargura.setAttribute('theScale', 0, 1);
    txtMargemSuperior.setAttribute('theScale', 0, 1);
    txtMargemEsquerda.setAttribute('theScale', 0, 1);
    txtInicio.setAttribute('theScale', 0, 1);
    txtFim.setAttribute('theScale', 0, 1);

    txtEtiquetaVertical.setAttribute('minMax', new Array(0, 99), 1);
    txtEtiquetaHorizontal.setAttribute('minMax', new Array(0, 99), 1);
    txtAltura.setAttribute('minMax', new Array(0, 99), 1);
    txtLargura.setAttribute('minMax', new Array(0, 999), 1);
    txtMargemSuperior.setAttribute('minMax', new Array(0, 99), 1);
    txtMargemEsquerda.setAttribute('minMax', new Array(0, 99), 1);
    txtInicio.setAttribute('minMax', new Array(1, glb_nNumeroVolumes), 1);
    txtFim.setAttribute('minMax', new Array(1, glb_nNumeroVolumes), 1);

    txtEtiquetaVertical.onfocus = selFieldContent;
    txtEtiquetaHorizontal.onfocus = selFieldContent;
    txtAltura.onfocus = selFieldContent;
    txtLargura.onfocus = selFieldContent;
    txtMargemSuperior.onfocus = selFieldContent;
    txtMargemEsquerda.onfocus = selFieldContent;
    txtInicio.onfocus = selFieldContent;
    txtFim.onfocus = selFieldContent;

    txtInicio.onchange = checkdata;
    txtFim.onchange = checkdata;

    // Altera checkBox se clicado no seu label
    lblDadosParceiro.onclick = invertChkBox;
    lblDadosProduto.onclick = invertChkBox;

    // Relatorio Vendas
    txtDataInicio1.onfocus = selFieldContent;
    txtDataFim1.onfocus = selFieldContent;
    txtFiltro1.onfocus = selFieldContent;

    // Total Vendas
    txtDataInicio2.onfocus = selFieldContent;
    txtDataFim2.onfocus = selFieldContent;
    txtFiltro2.onfocus = selFieldContent;

    // Codigo de Barra
    selEtiquetas.onchange = selEtiquetas_onChange;
    txtVias.onkeypress = verifyNumericEnterNotLinked;
    txtVias.setAttribute('thePrecision', 2, 1);
    txtVias.setAttribute('theScale', 0, 1);
    txtVias.setAttribute('minMax', new Array(0, 99), 1);
    txtVias.setAttribute('verifyNumPaste', 1);
    txtVias.onfocus = selFieldContent;
    txtInicio3.onfocus = selFieldContent;
    txtFim3.onfocus = selFieldContent;

    txtLabelHeight.onkeypress = verifyNumericEnterNotLinked;
    txtLabelGap.onkeypress = verifyNumericEnterNotLinked;
    txtLabelHeight.onfocus = selFieldContent;
    txtLabelGap.onfocus = selFieldContent;
    txtLabelHeight.setAttribute('verifyNumPaste', 1);
    txtLabelGap.setAttribute('verifyNumPaste', 1);
    txtLabelHeight.setAttribute('thePrecision', 3, 1);
    txtLabelGap.setAttribute('thePrecision', 3, 1);
    txtLabelHeight.setAttribute('theScale', 0, 1);
    txtLabelGap.setAttribute('theScale', 0, 1);
    txtLabelHeight.setAttribute('minMax', new Array(0, 999), 1);
    txtLabelGap.setAttribute('minMax', new Array(0, 999), 1);
}

function selTransportadoraMT_onchange() {
    clearComboEx(['selMinuta']);
}

function selEtiquetas_onChange() {
    // 0 - so esta etiqueta
    // 1 - faixa
    // 2 - todas

    if (selEtiquetas.value == 1) {
        lblInicio3.style.visibility = 'inherit';
        txtInicio3.style.visibility = 'inherit';
        lblFim3.style.visibility = 'inherit';
        txtFim3.style.visibility = 'inherit';
    }
    else {
        lblInicio3.style.visibility = 'hidden';
        txtInicio3.style.visibility = 'hidden';
        lblFim3.style.visibility = 'hidden';
        txtFim3.style.visibility = 'hidden';
    }
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights() {
    if (selReports.options.length != 0)
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;

    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else
            coll[i].style.visibility = 'hidden';
    }

    // desabilita o combo de relatorios
    selReports.disabled = true;

    // desabilita o botao OK
    btnOK.disabled = true;

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];

    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);

    elem = document.getElementById('selReports');

    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);

    // a altura livre    
    modHeight -= topFree;

    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt';
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';

    // acrescenta o elemento
    window.document.body.appendChild(elem);

    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);

    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;

    return null;
}

function oPrinter_OnPrintFinish() {
    winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
}

function checkdata() {
    if (this == txtInicio) {
        if (parseInt(txtInicio.value, 10) > parseInt(txtFim.value, 10)) {
            txtInicio.value = '';
            txtInicio.focus();
        }
    }
    else {
        if (parseInt(txtFim.value, 10) < parseInt(txtInicio.value, 10)) {
            txtFim.value = '';
            txtFim.focus();
        }
    }
}

/********************************************************************
Criado pelo programador
Impressao das Etiquetas de Embalagem do Pedido

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function etiquetasEmbalagem() {
    setConnection(dsoPrint01);

    dsoPrint01.SQL =
        'SELECT ' +
            'UPPER(b.Nome) AS Pessoa, ' +
            '(CASE ISNULL(h.EnderecoInvertido, 0) WHEN 1 THEN (ISNULL(c.Numero + SPACE(1),SPACE(0)) + ISNULL(c.Endereco,SPACE(0))) ' +
			'ELSE (ISNULL(c.Endereco + SPACE(1),SPACE(0)) + ISNULL(c.Numero,SPACE(0))) END) AS Endereco, ' +
            'ISNULL(c.Complemento,SPACE(0)) AS Complemento, ISNULL(c.Bairro,SPACE(0)) AS Bairro, ' +
            'ISNULL(c.CEP,SPACE(0)) AS CEP, ISNULL(d.Localidade,SPACE(0)) AS Cidade, ' +
            'ISNULL(e.CodigoLocalidade2,SPACE(0)) AS UF, ISNULL(h.NomeInternacional, SPACE(0)) AS Pais, a.PedidoID AS PedidoID, a.NumeroVolumes AS Volumes, ' +
            '(CASE a.TransportadoraID WHEN a.ParceiroID THEN ' + '\'' + 'O mesmo' + '\'' + ' ' +
                'WHEN a.EmpresaID THEN ' + '\'' + 'Nosso carro' + '\'' + ' ' +
                'ELSE ' +
                '(SELECT Fantasia ' +
                    'FROM Pessoas WITH(NOLOCK) ' +
                    'WHERE PessoaID=a.TransportadoraID) ' +
            'END) AS Transportadora, ' +
            'ISNULL(CONVERT(VARCHAR(6), f.NotaFiscal),SPACE(0)) AS NotaFiscal, CONVERT(VARCHAR(10), f.dtNotaFiscal, ' + DATE_SQL_PARAM + ')AS dtNotaFiscal, g.Nome AS Remetente ' +
        'FROM Pedidos a WITH(NOLOCK) ' +
        'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.PessoaID=b.PessoaID) ' +
        'INNER JOIN Pessoas_Enderecos c WITH(NOLOCK) ON (b.PessoaID=c.PessoaID) ' +
        'INNER JOIN Localidades d WITH(NOLOCK) ON (c.CidadeID=d.LocalidadeID) ' +
        'LEFT OUTER JOIN Localidades e WITH(NOLOCK) ON (c.UFID=e.LocalidadeID) ' +
        'LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON (c.PaisID=h.LocalidadeID) ' +
        'LEFT OUTER JOIN NotasFiscais f WITH(NOLOCK) ON (a.NotaFiscalID=f.NotaFiscalID AND f.EstadoID=67) ' +
        'INNER JOIN Pessoas g WITH(NOLOCK) ON (a.EmpresaID=g.PessoaID) ' +
        'WHERE (a.PedidoID= ' + glb_nPedidoID + ' AND c.Ordem=1) ';

    dsoPrint01.ondatasetcomplete = etiquetasEmbalagem_DSC;
    dsoPrint01.Refresh();
}

function etiquetasEmbalagem_DSC() {
    var nEtiquetasVerticais = 8;
    var nEtiquetasHorizontais = 1;
    var nAltura = 12;  // numero de linhas possiveis da etiqueta (cpp)
    var nLargura = 107;  // largura da etiqueta (mm)
    var nMargemSuperior = 0;  // numero de linhas a pular na primeira etiqueta
    var nMargemEsquerda = 3; // distancia da margem esquerda (mm)
    var nEtiquetaInicio;
    var nEtiquetaFim;
    var aEmpresa = getCurrEmpresaData();
    var nIdiomaParaID = parseInt(aEmpresa[8], 10);

    var nLinhas = 10;  // numero de linhas utilizaveis da etiqueta
    var nAjuste;  // ajusta a linha de impressao

    if (txtEtiquetaVertical.value != '')
        nEtiquetasVerticais = parseInt(txtEtiquetaVertical.value, 10);
    if (txtEtiquetaHorizontal.value != '')
        nEtiquetasHorizontais = parseInt(txtEtiquetaHorizontal.value, 10);
    if (txtAltura.value != '')
        nAltura = parseInt(txtAltura.value, 10);
    if (txtLargura.value != '')
        nLargura = parseInt(txtLargura.value, 10);
    if (txtMargemSuperior.value != '')
        nMargemSuperior = parseInt(txtMargemSuperior.value, 10);
    if (txtEtiquetaVertical.value != '')
        nMargemEsquerda = parseInt(txtMargemEsquerda.value, 10);

    if (trimStr(txtInicio.value) == '')
        nEtiquetaInicio = 1;
    else
        nEtiquetaInicio = parseInt(txtInicio.value, 10);

    if (trimStr(txtFim.value) == '')
        nEtiquetaFim = glb_nNumeroVolumes;
    else
        nEtiquetaFim = parseInt(txtFim.value, 10);

    var aCelula = new Array();
    var i, j, k, m, n;

    for (i = 0; i < nLinhas; i++)
        aCelula[i] = new Array();

    var sComplementoBairro;

    // seta os parametros de inicializacao da impressora
    oPrinter.DefaultPrinterNFParams(8, nAltura, 12, 'N', false);
    oPrinter.ParallelPort = 1;
    oPrinter.ResetPrinter();
    oPrinter.ResetTextForPrint();

    dsoPrint01.recordset.MoveFirst();

    i = nEtiquetaInicio;
    while (i <= nEtiquetaFim) {
        oPrinter.MoveToNewLine(nMargemSuperior);
        j = 0;
        while ((i <= nEtiquetaFim) && (j < nEtiquetasVerticais)) {
            k = 0;
            while ((i <= nEtiquetaFim) && (k < nEtiquetasHorizontais)) {
                aCelula[9][k] = '';
                aCelula[0][k] = dsoPrint01.recordset['Pessoa'].value;
                aCelula[1][k] = (dsoPrint01.recordset['Endereco'].value).substr(0, 44);
                nAjuste = 0;
                sComplementoBairro = trimStr((dsoPrint01.recordset['Complemento'].value) + ' ' + (dsoPrint01.recordset['Bairro'].value));
                if (sComplementoBairro != '')
                    aCelula[2 - nAjuste][k] = sComplementoBairro;
                else
                    nAjuste++;

                // Portugues
                if (nIdiomaParaID == 246)
                    aCelula[3 - nAjuste][k] = (dsoPrint01.recordset['CEP'].value) + ' ' + (dsoPrint01.recordset['Cidade'].value) + ' ' + (dsoPrint01.recordset['UF'].value);
                else
                    aCelula[3 - nAjuste][k] = (dsoPrint01.recordset['Cidade'].value) + ' ' + (dsoPrint01.recordset['UF'].value) + ' ' + (dsoPrint01.recordset['Pais'].value) + ' ' + dsoPrint01.recordset['CEP'].value;

                aCelula[4 - nAjuste][k] = '';
                aCelula[5 - nAjuste][k] = translateTherm('Pedido', glb_aTranslate_EtiquetaEmbalagem) + ': ' + dsoPrint01.recordset['PedidoID'].value + '   ' + translateTherm('NF', glb_aTranslate_EtiquetaEmbalagem) + ': ' + dsoPrint01.recordset['NotaFiscal'].value;
                aCelula[6 - nAjuste][k] = translateTherm('Volume', glb_aTranslate_EtiquetaEmbalagem) + ': ' + (i) + '/' + glb_nNumeroVolumes + '      ' + translateTherm('Data NF', glb_aTranslate_EtiquetaEmbalagem) + ': ' + dsoPrint01.recordset['dtNotaFiscal'].value;
                aCelula[7 - nAjuste][k] = translateTherm('Transp', glb_aTranslate_EtiquetaEmbalagem) + ': ' + dsoPrint01.recordset['Transportadora'].value;
                aCelula[8 - nAjuste][k] = '';
                if (nIdiomaParaID == 246)
                    aCelula[9 - nAjuste][k] = 'Rem: ' + dsoPrint01.recordset['Remetente'].value;
                else
                    aCelula[9 - nAjuste][k] = dsoPrint01.recordset['Remetente'].value;

                i++;
                k++;
            }

            // Impressao dos Dados
            for (m = 0; m < nLinhas; m++) {
                for (n = 0; n < k; n++) {
                    oPrinter.SendTextForPrint(nMargemEsquerda + n * nLargura, aCelula[m][n], 'N', (m == 0));
                }
                oPrinter.MoveToNewLine(1);
            }
            oPrinter.MoveToNewLine(nAltura - m);
            j++;
        }
    }
    oPrinter.PrintText();
}

/********************************************************************************************************************
Relatorios de Proposta Comercial e Solicitacao de Compras

Parametros: nRelatorioID 40132 -> Solicitacao de Compras
						 40133 -> Proposta Comercial
						 
Retorno: Nenhum						 
********************************************************************************************************************/
function propostaSolicitacao(nRelatorioID) {
    var dirA1;
    var dirA2;
    var aEmpresa = getCurrEmpresaData();

    var nIdiomaDeID = aEmpresa[7];
    var nIdiomaParaID = aEmpresa[8];

    var nContatoID = (nRelatorioID == 40133 ? parseInt(selContato2.value, 10) : parseInt(selContato1.value, 10));
    var sContatoCargo = '';
    var sContatoCargoProposta = '';
    var i, j;
    var nLeftDadosProposta = 0;
    var strPars = '';
    var sPesos = '';
    var sDimensoes = '';
    var sPrazos = '';
    var sMeses = '';

    var bInformacoesAdicionais;
    var bHTML;
    var bFichaTecnica;
    var bSup;
    var bObs = false;
    var sReport = '';
    var sReport2 = '';
    var nTransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransacaoID' + '\'' + '].value');

    if (nRelatorioID == 40133) {
        if (dsoProposta.recordset['dtPrevisaoEntrega'].value < dsoProposta.recordset['dtHoje'].value) {
            var _retMsg = window.top.overflyGen.Confirm('Previs�o de Entrega Vencida.\n' +
				'Gerar a Proposta com Previs�o de Entrega Imediata?');

            if (_retMsg == 0)
                return null;
            else if (_retMsg == 2) {
                lockControlsInModalWin(false);
                return null;
            }
        }
    }

    var formato = (nRelatorioID == 40133 ? selFormatoProposta.value : selFormatoSolicitacao.value);


    if (formato == "1") {

        bSup = ((nRelatorioID == 40133) && (chkSUP.checked));
        bObs = (nRelatorioID == 40133 ? chkObservacoes.checked : chkObservacoes2.checked);
        //    bHTML = (nRelatorioID == 40133 ? chkHTML.checked : chkHTML2.checked);
        bFichaTecnica = (nRelatorioID == 40133 ? chkFichaTecnica.checked : chkFichaTecnica2.checked);
        bInformacoesAdicionais = ((nRelatorioID == 40132) && (chkInformacoesAdicionais.checked));
        nContatoID = (isNaN(nContatoID) ? 0 : nContatoID);



        var Title = translateTherm(selReports.options[selReports.selectedIndex].innerText, glb_aTranslate_Proposta);
        var sMoeda = dsoProposta.recordset['Moeda'].value;


        var strParameters = "RelatorioID=" + selReports.value + "&Formato=" + formato + "&DATE_SQL_PARAM=" + DATE_SQL_PARAM + "&glb_nTipoPerfilComercialID=" + glb_nTipoPerfilComercialID + "&EmpresaID=" + glb_nEmpresaID + "&PedidoID=" + glb_nPedidoID +
                              "&nIdiomaDeID=" + nIdiomaDeID + "&nIdiomaParaID=" + nIdiomaParaID + "&glb_USERID=" + glb_USERID + "&glb_nEhMicro=" + glb_nEhMicro +
                              "&nTransacaoID=" + nTransacaoID + "&nContatoID=" + nContatoID + "&sContatoCargo=" + sContatoCargo + "&bInformacoesAdicionais=" + bInformacoesAdicionais + "&bSup=" + bSup +
                              "&MoedaID=" + sMoeda + "&Title=" + Title.toString() + "&bFichaTecnica=" + bFichaTecnica + "&nValidade=" + txtValidadeProposta.value;

        pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
        lockControlsInModalWin(true);
        window.document.onreadystatechange = reports_onreadystatechange;
        window.document.location = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/Reports.aspx?' + strParameters;
    }
    else if (formato == "3") {
        var sSite = '';
        var subject = glb_sEmpresaFantasia + glb_space + '-' + glb_space + translateTherm(selReports.options[selReports.selectedIndex].innerText, glb_aTranslate_Proposta) + glb_space + glb_nPedidoID;
        bSup = ((nRelatorioID == 40133) && (chkSUP.checked));

        nPropostaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

        for (i = 0; i < glb_aSites.length; i++) {
            if ((glb_aSites[i][0] == glb_nEmpresaID) || (glb_aSites[i][1] == glb_nEmpresaID)) {
                sSite = glb_aSites[i][2];
                break;
            }
        }

        strPars = '?nUsuarioID=' + escape(getCurrUserID());
        strPars += '&nIdiomaDeID=' + escape(nIdiomaDeID);
        strPars += '&nIdiomaParaID=' + escape(nIdiomaParaID);
        strPars += '&nPedidoID=' + escape(glb_nPedidoID);
        strPars += '&nValidade=' + escape(txtValidadeProposta.value);
        strPars += '&nContatoID=' + escape(nContatoID);
        strPars += '&sContatoCargo=' + escape(sContatoCargoProposta);
        strPars += '&nFichaTecnica=' + escape((bFichaTecnica ? 1 : 0));
        strPars += '&nEhMicro=' + escape(glb_nEhMicro);
        strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
        strPars += '&sEmpresa=' + escape(aEmpresa[6]);
        strPars += '&sSite=' + escape(sSite);
        strPars += '&DATE_SQL_PARAM=' + escape(DATE_SQL_PARAM);
        strPars += '&bMarca=' + escape(chkMarca.checked ? 1 : 0);
        strPars += '&bModelo=' + escape(chkModelo.checked ? 1 : 0);
        strPars += '&bNcm=' + escape(chkModelo.checked ? 1 : 0);
        strPars += '&bSUP=' + escape(bSup ? 1 : 0);
        strPars += '&bObservacoes=' + escape(bObs ? 1 : 0);
        strPars += '&nRelatorioID=' + escape(nRelatorioID);
        strPars += '&bInformacoesAdicionais=' + escape((bInformacoesAdicionais ? 1 : 0));
        strPars += '&nTransacaoID=' + escape(nTransacaoID);
        for (i = 0; i < glb_aTranslate_Proposta.length; i++) {
            for (j = 0; j < 2; j++)
                strPars += '&aData' + j.toString() + '=' + escape(glb_aTranslate_Proposta[i][j]);
        }
        strPars += '&nTranslateLen=' + escape(glb_aTranslate_Proposta.length);
        strPars += '&nTranslateDim=' + escape(2);
        strPars += '&sMailFrom=' + escape(getCurrUserEmail());
        strPars += '&sMailTo=' + escape((nRelatorioID == 40133 ? txtTo.value : txtToSolicitacao.value));
        strPars += '&sMailCC=' + escape((nRelatorioID == 40133 ? txtCC.value : txtCCSolicitacao.value));
        strPars += '&sMailBCC=' + escape((nRelatorioID == 40133 ? txtCCo.value : txtCCoSolicitacao.value));
        strPars += '&sSubject=' + escape(subject);
        strPars += '&sMailBody=' + escape('');


        lockControlsInModalWin(true);

        dsoPropostaHTML.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/EnviaEmailProposta.aspx' + strPars;
        dsoPropostaHTML.ondatasetcomplete = dsoPropostaHTML_DSC;
        dsoPropostaHTML.Refresh();

    }
}

function dsoPropostaHTML_DSC() {
    var emailID;
    if (!(dsoPropostaHTML.recordset.BOF && dsoPropostaHTML.recordset.EOF)) {
        emailID = dsoPropostaHTML.recordset['EmailID'].value;
        var strPars = '?nMailtoSendID=' + escape(emailID);
        strPars += '&bHTML=' + escape(1);

        dsoEMail.URL = SYS_ASPURLROOT + '/serversidegenEx/emails/sendemail.aspx' + strPars;
        dsoEMail.ondatasetcomplete = sendMail_DSC2;
        dsoEMail.Refresh();
    }
}
function sendMail_DSC2() {
    var mensagem = "";
    if (!(dsoEMail.recordset.BOF && dsoEMail.recordset.EOF)) {
        mensagem = (dsoEMail.recordset.Fields['Msg'].value == null ? '' : dsoEMail.recordset.Fields['Msg'].value);

        if (mensagem != '') {
            window.top.overflyGen.Alert(mensagem);
        }
        else
            window.top.overflyGen.Alert("E-mail enviado com sucesso !");

        lockControlsInModalWin(false);
    }
}

function relatorioCompras() {
    var i, nItensSelected;

    var dirA1;
    var dirA2;

    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);

    if (!chkDataEx(txtDataInicio3.value)) {
        if (window.top.overflyGen.Alert('Data de In�cio � Inv�lida') == 0)
            return null;
        lockControlsInModalWin(false);
        return null;
    }
    else if (!chkDataEx(txtDataFim3.value)) {
        if (window.top.overflyGen.Alert('Data de Fim � Inv�lida') == 0)
            return null;
        lockControlsInModalWin(false);
        return null;
    }

    var sTaxaMoeda = ' / Pedidos.TaxaMoeda ';

    if (chkMoedaLocal3.checked)
        sTaxaMoeda = ' * 1 ';

    var sDataInicio = '\'' + dateFormatToSearch(txtDataInicio3.value) + '\'';
    var sDataFim = '\'' + dateFormatToSearch(txtDataFim3.value) + '\'';

    var sFiltroFabricante = (selFabricante2.value == 0 ? '' : ' AND Produtos.FabricanteID=' + selFabricante2.value);

    var sFiltroFamilia = '';
    var strSQL = '';
    nItensSelected = 0;

    var sSQLDateFormat = (DATE_FORMAT == 'DD/MM/YYYY' ? '103' : '101');

    for (i = 0; i < selFamilias2.length; i++) {
        if (selFamilias2.options[i].selected == true) {
            nItensSelected++;
            sFiltroFamilia += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.ProdutoID=' + selFamilias2.options[i].value;
        }
    }

    sFiltroFamilia += (nItensSelected > 0 ? ')' : '');
    var sFiltro = (trimStr(txtFiltro3.value).length > 0 ? ' AND (' + trimStr(txtFiltro3.value) + ') ' : '');

    if (!(dirA1 && dirA2))
        sFiltro += ' AND Pedidos.ProprietarioID = ' + glb_USERID + ' ';

    var strSQL_Select1 = '';

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    // var formato = selFormato.value;
    var formato = 2;

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&sTaxaMoeda=" + sTaxaMoeda + "&sDataInicio=" + sDataInicio + "&sDataFim=" + sDataFim + "&sFiltro=" + sFiltro + "&sFiltroFabricante=" + sFiltroFabricante +
                        "&sFiltroFamilia=" + sFiltroFamilia + "&nPadraoPedido=" + selPadrao2.value;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/Reports.aspx?' + strParameters;
}

function barCode() {
    //Teste Bacil
    testeEtiquetas();
    return null;
    //Teste Bacil

    lockControlsInModalWin(true);

    setConnection(dsoGen01);

    var empresaData = getCurrEmpresaData();

    var bTemNumeroSerie = chkTemNumeroSerie.checked;

    var sFiltro = '';
    var sFiltro1 = '';

    if (bTemNumeroSerie) {
        // Faixa de Etiquetas
        if (selEtiquetas.value == 1) {
            txtInicio3.value = trimStr(txtInicio3.value);
            txtFim3.value = trimStr(txtFim3.value);

            var nProdutoInicioID = parseInt((txtInicio3.value).substr(0, 5), 10);
            var nProdutoFimID = parseInt((txtFim3.value).substr(0, 5), 10);

            var sInicio = (txtInicio3.value).substr(5, ((txtInicio3.value).length) - 5);
            var sFim = (txtFim3.value).substr(5, ((txtFim3.value).length) - 5);
            //INICIO DE NOVO NS--------------------------------------------------
            if (sInicio != '') {
                sFiltro = 'AND b.NumeroSerie >= ' + '\'' + sInicio + '\'' + ' AND b.ProdutoID >= ' + nProdutoInicioID;
                sFiltro1 = 'AND a.Lote >= ' + '\'' + sInicio + '\'' + ' AND a.ProdutoID >= ' + nProdutoInicioID;
            }
            if (sFim != '') {
                sFiltro += ' AND b.NumeroSerie <= ' + '\'' + sFim + '\'' + ' AND b.ProdutoID <= ' + nProdutoFimID;
                sFiltro1 += ' AND a.Lote <= ' + '\'' + sFim + '\'' + ' AND a.ProdutoID <= ' + nProdutoFimID;
            }
            //FIM DE NOVO NS--------------------------------------------------			
        }
            // Uma Etiqueta
        else if (selEtiquetas.value == 0) {
            sFiltro = 'AND b.NumeroSerieID = ' + glb_nNumeroSerieID;
            sFiltro1 = 'AND a.PedProdutoID = ' + glb_nNumeroSerieID;
        }

        dsoGen01.SQL = 'SELECT c.Conceito, c.ConceitoID AS ConceitoID, b.NumeroSerie AS NumeroSerie, ' +
					'ISNULL((SELECT TOP 1 ImprimeNomeEmpresaCB ' +
							'FROM RelacoesPesRec WITH(NOLOCK) WHERE (SujeitoID = ' + empresaData[0] + ' AND ObjetoID = 999 AND ' +
								'EstadoID = 2 AND TipoRelacaoID = 12)), 0) AS ImprimeNomeEmpresaCB ' +
					'FROM NumerosSerie_Movimento a WITH(NOLOCK), NumerosSerie b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
					'WHERE a.PedidoID= ' + glb_nPedidoID + ' AND a.NumeroSerieID=b.NumeroSerieID AND ' +
					'b.ProdutoID=c.ConceitoID ' +
					sFiltro + ' ' +
//INICIO DE NOVO NS--------------------------------------------------
					'UNION ALL ' +
					'SELECT b.Conceito, b.ConceitoID AS ConceitoID, a.Lote AS NumeroSerie, ' +
					'ISNULL((SELECT TOP 1 ImprimeNomeEmpresaCB ' +
							'FROM RelacoesPesRec WITH(NOLOCK) WHERE (SujeitoID = ' + empresaData[0] + ' AND ObjetoID = 999 AND ' +
								'EstadoID = 2 AND TipoRelacaoID = 12)), 0) AS ImprimeNomeEmpresaCB ' +
					'FROM Pedidos_ProdutosLote a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
					'WHERE a.PedidoID= ' + glb_nPedidoID +
					' AND a.ProdutoID=b.ConceitoID ' +
					sFiltro1 + ' ' +
//FIM DE NOVO NS--------------------------------------------------
					'ORDER BY ConceitoID, NumeroSerie';
    }
    else {
        // Faixa de Etiquetas
        if (selEtiquetas.value == 1) {
            txtInicio3.value = trimStr(txtInicio3.value);
            txtFim3.value = trimStr(txtFim3.value);

            var nProdutoInicioID = parseInt((txtInicio3.value).substr(0, 5), 10);
            var nProdutoFimID = parseInt((txtFim3.value).substr(0, 5), 10);

            var sInicio = (txtInicio3.value).substr(5, ((txtInicio3.value).length) - 5);
            var sFim = (txtFim3.value).substr(5, ((txtFim3.value).length) - 5);

            if (sInicio != '')
                sFiltro = ' AND b.ConceitoID >= ' + nProdutoInicioID + ' ';
            if (sFim != '')
                sFiltro += ' AND b.ConceitoID <= ' + nProdutoFimID + ' ';
        }
            // Uma Etiqueta
        else if (selEtiquetas.value == 0) {
            sFiltro = 'AND b.ConceitoID = ' + glb_nProdutoID;
        }

        dsoGen01.SQL = 'SELECT b.Conceito, b.ConceitoID, ' +
					'ISNULL((SELECT TOP 1 ImprimeNomeEmpresaCB ' +
							'FROM RelacoesPesRec WITH(NOLOCK) WHERE (SujeitoID = ' + empresaData[0] + ' AND ObjetoID = 999 AND ' +
								'EstadoID = 2 AND TipoRelacaoID = 12)), 0) AS ImprimeNomeEmpresaCB ' +
					'FROM Pedidos_ProdutosSeparados a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
					'WHERE a.PedidoID= ' + glb_nPedidoID + ' AND a.ProdutoID=b.ConceitoID ' +
					sFiltro + ' ' +
					'GROUP BY b.Conceito, b.ConceitoID ' +
					'ORDER BY b.Conceito';

    }

    dsoGen01.ondatasetcomplete = barCode_DSC;
    dsoGen01.Refresh();

}

function testeEtiquetas() {
    var empresaData = getCurrEmpresaData();
    var sEmpresaFantasia = empresaData[3];
    var sBarCode = '1000010001230005';
    var sCliente = 'Gogeek';
    var sLocalEntrega = 'Gogeek';
    var sMunicipioUF = 'S�o Paulo/SP';
    var sCEP = '04566032';
    var sTransportadora = 'Braspress';

    oPrinter.ResetPrinterBarLabel(1, 9, 9);
    oPrinter.teste(sEmpresaFantasia, sBarCode, sCliente, sLocalEntrega, sMunicipioUF, sCEP, sTransportadora);
    oPrinter.PrintBarLabels(nPrintBarcode, true, parseInt(txtLabelHeight.value, 10),
                                parseInt(txtLabelGap.value, 10), 0, 3);
}

function barCode_DSC() {
    lockControlsInModalWin(false);

    var numRecs, i, retVal;
    var nPrintBarcode = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrPrinterBarCode()');
    var bTemNumeroSerie = chkTemNumeroSerie.checked;

    numRecs = 0;

    if (!(dsoGen01.recordset.BOF && dsoGen01.recordset.EOF)) {
        dsoGen01.recordset.MoveLast;
        numRecs = dsoGen01.recordset.RecordCount;
        dsoGen01.recordset.MoveFirst();
    }

    numRecs = numRecs * parseInt(trimStr(txtVias.value), 10);

    if (numRecs == 0) {
        if (window.top.overflyGen.Alert('Nenhuma etiqueta a ser impressa!') == 0)
            return null;
    }
    else {
        if (selEtiquetas.value != 0) {
            if (numRecs == 1)
                retVal = window.top.overflyGen.Confirm('Imprime ' + numRecs.toString() + ' etiqueta?');
            else
                retVal = window.top.overflyGen.Confirm('Imprime ' + numRecs.toString() + ' etiquetas?');
        }
        else {
            retVal = window.top.overflyGen.Confirm('Imprime a etiqueta ' + dsoGen01.recordset.Fields['ConceitoID'].value.toString() + dsoGen01.recordset.Fields['NumeroSerie'].value.toString() + ' ?');
        }
    }

    if (retVal == 0)
        return null;
    else if (retVal == 1) {
        var nVias;
        nVias = parseInt((txtVias.value), 10);
        if (nVias <= 0)
            nVias = 1;

        oPrinter.ResetPrinterBarLabel(1, 9, 9);

        var empresaData = getCurrEmpresaData();
        var sEmpresaFantasia = empresaData[3];

        dsoGen01.recordset.MoveFirst();

        if (dsoGen01.recordset.Fields['ImprimeNomeEmpresaCB'].value)
            sEmpresaFantasia = empresaData[3];
        else
            sEmpresaFantasia = '';

        while (!dsoGen01.recordset.EOF) {
            for (i = 1; i <= nVias; i++) {

                if (bTemNumeroSerie) {
                    oPrinter.InsertBarLabelData((sEmpresaFantasia).substr(0, 13),
												removeCharFromString(dsoGen01.recordset.Fields['Conceito'].value, '"'),
												dsoGen01.recordset.Fields['ConceitoID'].value.toString() +
												dsoGen01.recordset.Fields['NumeroSerie'].value.toString());
                }
                else {
                    oPrinter.InsertBarLabelData((sEmpresaFantasia).substr(0, 13),
												removeCharFromString(dsoGen01.recordset.Fields['Conceito'].value, '"'),
												dsoGen01.recordset.Fields['ConceitoID'].value.toString());
                }
            }
            dsoGen01.recordset.MoveNext();
        }

        oPrinter.PrintBarLabels(nPrintBarcode, true, parseInt(txtLabelHeight.value, 10),
                                parseInt(txtLabelGap.value, 10));

        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
    }

}

function etiquetaLocalizacao() {
    lockControlsInModalWin(true);

    setConnection(dsoGen01);

    var empresaData = getCurrEmpresaData();

    dsoGen01.SQL = 'SELECT TOP 4 dbo.fn_LocalizacaoEstoque_Localizacao(LocalizacaoID, 1) AS Codigo, ' +
			'dbo.fn_LocalizacaoEstoque_Localizacao(LocalizacaoID, 2) AS Descricao ' +
		'FROM LocalizacoesEstoque WITH(NOLOCK) ' +
		'WHERE (LocalizacaoID IN (380) AND EmpresaID = ' + empresaData[0] + ') ' +
		'ORDER BY dbo.fn_LocalizacaoEstoque_Localizacao(LocalizacaoID, 1)';

    dsoGen01.ondatasetcomplete = etiquetaLocalizacao_DSC;
    dsoGen01.Refresh();

}

function etiquetaLocalizacao_DSC() {
    lockControlsInModalWin(false);

    var numRecs, i, retVal;
    var nPrintBarcode = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrPrinterBarCode()');

    numRecs = 0;

    if (!(dsoGen01.recordset.BOF && dsoGen01.recordset.EOF)) {
        dsoGen01.recordset.MoveLast;
        numRecs = dsoGen01.recordset.RecordCount;
        dsoGen01.recordset.MoveFirst();
    }

    numRecs = numRecs * parseInt(trimStr(txtVias.value), 10);

    if (numRecs == 0) {
        if (window.top.overflyGen.Alert('Nenhuma etiqueta a ser impressa!') == 0)
            return null;
    }
    else {
        if (selEtiquetas.value != 0) {
            if (numRecs == 1)
                retVal = window.top.overflyGen.Confirm('Imprime ' + numRecs.toString() + ' etiqueta?');
            else
                retVal = window.top.overflyGen.Confirm('Imprime ' + numRecs.toString() + ' etiquetas?');
        }
        else {
            retVal = window.top.overflyGen.Confirm('Imprime a etiqueta ' + dsoGen01.recordset.Fields['Codigo'].value.toString() + ' ?');
        }
    }

    if (retVal == 0)
        return null;
    else if (retVal == 1) {
        var nVias;
        nVias = parseInt((txtVias.value), 10);
        if (nVias <= 0)
            nVias = 1;

        oPrinter.ResetPrinterBarLabel(1, 9, 9);

        dsoGen01.recordset.MoveFirst();

        while (!dsoGen01.recordset.EOF) {
            for (i = 1; i <= nVias; i++) {
                oPrinter.InsertLabelLocalizacaoData(removeDiatricsOnJava(removeCharFromString(dsoGen01.recordset.Fields['Descricao'].value, '"')),
													removeDiatricsOnJava(removeCharFromString(dsoGen01.recordset.Fields['Codigo'].value, '"')));

            }
            dsoGen01.recordset.MoveNext();
        }

        oPrinter.PrintBarLabels(nPrintBarcode, true, parseInt(txtLabelHeight.value, 10),
                                parseInt(txtLabelGap.value, 10), 80, 1);

        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
    }

}

function removeAlias(strExp) {
    var i;
    var sReturn = '';
    for (i = 0; i <= strExp.length - 1 ; i++) {
        if (strExp.substr(i + 1, 1) != '.')
            sReturn += strExp.substr(i, 1);
        else
            i++;
    }
    return sReturn;
}

function changeVirgulaPonto(strExp) {
    var i;
    var sReturn = '';
    for (i = 0; i <= strExp.length - 1 ; i++)
        sReturn += (strExp.substr(i, 1) == ',' ? ';' : strExp.substr(i, 1));
    return sReturn;
}

function selFabricante_OnChange() {
    showDadosAdicionaisRelVendas(true);
}

function selPadrao_OnChange() {
    selFabricante.disabled = false;
    chkSoResultado.disabled = false;
    chkDadosParceiro.disabled = false;
    chkDadosProduto.disabled = false;
    chkDadosResultado.disabled = false;
    chkMoedaLocal2.disabled = false;

    lblComissaoVendas.style.visibility = 'hidden';
    chkComissaoVendas.style.visibility = 'hidden';
    lblMicrosoftTipoRelatorio.style.visibility = 'hidden';
    selMicrosoftTipoRelatorio.style.visibility = 'hidden';

    lblSoResultado.style.visibility = 'inherit';
    chkSoResultado.style.visibility = 'inherit';

    lblDadosParceiro.style.visibility = 'inherit';
    chkDadosParceiro.style.visibility = 'inherit';

    lblDadosProduto.style.visibility = 'inherit';
    chkDadosProduto.style.visibility = 'inherit';

    lblDadosResultado.style.visibility = 'inherit';
    chkDadosResultado.style.visibility = 'inherit';

    lblReportaFabricante.style.visibility = 'inherit';
    chkReportaFabricante.style.visibility = 'inherit';

    lblPedidosWeb2.style.visibility = 'inherit';
    chkPedidosWeb2.style.visibility = 'inherit';

    lblFiltro1.style.visibility = 'inherit';
    txtFiltro1.style.visibility = 'inherit';

    if (selPadrao.value == 956) {
        lblSoResultado.style.visibility = 'hidden';
        chkSoResultado.style.visibility = 'hidden';

        lblDadosParceiro.style.visibility = 'hidden';
        chkDadosParceiro.style.visibility = 'hidden';

        lblDadosProduto.style.visibility = 'hidden';
        chkDadosProduto.style.visibility = 'hidden';

        lblDadosResultado.style.visibility = 'hidden';
        chkDadosResultado.style.visibility = 'hidden';

        lblReportaFabricante.style.visibility = 'hidden';
        chkReportaFabricante.style.visibility = 'hidden';

        lblPedidosWeb2.style.visibility = 'hidden';
        chkPedidosWeb2.style.visibility = 'hidden';

        lblFiltro1.style.visibility = 'hidden';
        txtFiltro1.style.visibility = 'hidden';

        selFamilias.disabled = true;
    }
        // Se Padrao Microsoft
    else if (selPadrao.value == 703) {
        lblMicrosoftTipoRelatorio.style.visibility = 'inherit';
        selMicrosoftTipoRelatorio.style.visibility = 'inherit';
    }
        // Padr�o pedidos
    else if (selPadrao.value == 706) {
        lblComissaoVendas.style.visibility = 'inherit';
        chkComissaoVendas.style.visibility = 'inherit';
    }

    else if (selPadrao.value == 717) {

        chkSoResultado.checked = true;
        chkDadosParceiro.checked = false;
        chkDadosProduto.checked = false;
        chkDadosResultado.checked = false;

        chkReportaFabricante.disabled = true;
        selFabricante.disabled = false;
        chkSoResultado.disabled = true;
        chkDadosParceiro.disabled = true;
        chkDadosProduto.disabled = true;
        chkDadosResultado.disabled = true;
    }

    else if (selPadrao.value == 951) {
        selOptByValueInSelect(getHtmlId(), 'selFabricante', 15395);

        chkSoResultado.checked = true;
        chkDadosParceiro.checked = false;
        chkDadosProduto.checked = false;
        chkDadosResultado.checked = false;

        selFabricante.disabled = true;
        chkSoResultado.disabled = true;
        chkDadosParceiro.disabled = true;
        chkDadosProduto.disabled = true;
        chkDadosResultado.disabled = true;
    }
    else if (selPadrao.value == 952) {
        chkSoResultado.checked = true;
        chkDadosParceiro.checked = false;
        chkDadosProduto.checked = false;
        chkDadosResultado.checked = false;

        selFabricante.disabled = false;
        chkSoResultado.disabled = true;
        chkDadosParceiro.disabled = true;
        chkDadosProduto.disabled = true;
        chkDadosResultado.disabled = true;
    }
    else if (selPadrao.value == 953) {
        chkSoResultado.checked = true;
        chkDadosParceiro.checked = false;
        chkDadosProduto.checked = false;
        chkDadosResultado.checked = false;
        chkMoedaLocal2.checked = false;

        selFabricante.disabled = false;
        chkSoResultado.disabled = false;
        chkDadosParceiro.disabled = true;
        chkDadosProduto.disabled = true;
        chkDadosResultado.disabled = true;
        chkMoedaLocal2.disabled = true;

    }
    else if (selPadrao.value == 954) {
        chkSoResultado.checked = true;
        chkDadosParceiro.checked = false;
        chkDadosProduto.checked = false;
        chkDadosResultado.checked = false;
        chkMoedaLocal2.checked = false;

        selFabricante.disabled = false;
        chkSoResultado.disabled = false;
        chkDadosParceiro.disabled = true;
        chkDadosProduto.disabled = true;
        chkDadosResultado.disabled = false;
        chkMoedaLocal2.disabled = true;
    }
    chkDadosResultado_onclick();
    showDadosAdicionaisRelVendas(true);

    // Chama a fun��o com 0 caso n�o for relat�rio de ECF
    if (selPadrao.value != 956) {
        selFamilias.disabled = false;
    }
}

function selFabricante2_OnChange() {
    showDadosAdicionaisRelVendas(true);
}

function selPadrao2_OnChange() {
    showDadosAdicionaisRelVendas(true);
}

function showDadosAdicionaisRelVendas(bShow) {
    if (selPadrao.value != 956) {
        var strVisibility = (bShow ? 'inherit' : 'hidden');

        lblDadosParceiro.style.visibility = strVisibility;
        chkDadosParceiro.style.visibility = strVisibility;
        lblDadosProduto.style.visibility = strVisibility;
        chkDadosProduto.style.visibility = strVisibility;
    }
}

function ListaEmbalagem() {
    var nEmpresaLogada = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var Formato = selFormatoListaEmbalagem.value;
    var sLinguaLogada = getDicCurrLang();

    var strParameters = "RelatorioID=" + selReports.value + "&nEmpresaLogada=" + nEmpresaLogada[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + Formato +
                        "&sLinguaLogada=" + sLinguaLogada + "&nPedidoID=" + glb_nPedidoID;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modcomercial/submodpedidos/frmpedidos/serverside/Reports.aspx?" + strParameters;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
}

function call_DemonstrativoCalculoICMSST() {
    setConnection(dsoCabecalhoDemonstrativoCalculoICMSST);//dso do ICMS 

    dsoArquivoGNRE.SQL =
		"select " +
			"Localidades.CodigoLocalidade2 as UF " +
		"from " +
			"Pedidos WITH(NOLOCK) " +
			"inner join NotasFiscais WITH(NOLOCK) on NotasFiscais.NotaFiscalID = Pedidos.NotaFiscalID AND NotasFiscais.EstadoID=67 " +
			"inner join Pessoas WITH(NOLOCK) on Pedidos.PessoaID = Pessoas.PessoaID " +
			"inner join Pessoas_Enderecos WITH(NOLOCK) on Pessoas_Enderecos.PessoaID = Pessoas.PessoaID " +
			"inner join Localidades WITH(NOLOCK) on Localidades.LocalidadeID = Pessoas_Enderecos.UFID " +
		"where " +
			"Pedidos.PedidoID = " + glb_nPedidoID;

    dsoArquivoGNRE.ondatasetcomplete = call_arquivognre_aspx_DSC;
    dsoArquivoGNRE.refresh();
}

function call_arquivognre_aspx_DSC() {
    strPars = '?nEmpresaID=' + glb_nEmpresaID +
		'&UF=' + dsoArquivoGNRE.recordset['UF'].value +
		'&nPedidosID=/' + glb_nPedidoID + '/';

    htmlPath = SYS_ASPURLROOT + "/modcomercial/submodpedidos/frmpedidos/serverside/arquivognre.aspx" + strPars;

    window.open(htmlPath);

    lockControlsInModalWin(false);
}


function call_DemonstrativoCalculoICMSST() {
    var sLinguaLogada = getDicCurrLang();

    var formato = selFormatoSolicitacao2.value;
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&nEmpresaLogadaIdioma=" + aEmpresaData[8] + "&DATE_SQL_PARAM=" + DATE_SQL_PARAM + 
                        "&glb_nPedidoID=" + glb_nPedidoID + "&sLinguaLogada=" + sLinguaLogada;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modcomercial/submodpedidos/frmpedidos/serverside/Reports.aspx?" + strParameters;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
}


function retiradaMercadoria() {
    if ((dsoPessoas.recordset.BOF) && (dsoPessoas.recordset.EOF)) {
        winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
        return null;
    }

    dsoPessoas.recordset.MoveFirst();

    dsoPessoas.recordset.Find('PessoaID', selPessoa.value);
    glb_bUnlockModal = false;
    prtMercadoria();
}


var pedidos = '';
function prtMercadoria() {
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var Formato = 1;
    pedidos = '';
    var nEmpresaData = getCurrEmpresaData();

    if ((!(dsoPessoas.recordset.EOF)) && (dsoPessoas.recordset['PessoaID'].value == selPessoa.value)) {
        pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
        lockControlsInModalWin(true);

        while (dsoPessoas.recordset.Bookmark() < dsoPessoas.recordset.RecordCount()) {
            if (dsoPessoas.recordset['PessoaID'].value == selPessoa.value) {
                if (pedidos != '') {
                    pedidos += "," + dsoPessoas.recordset['PedidoID'].value;
                }
                else {
                    pedidos += dsoPessoas.recordset['PedidoID'].value;
                }
            }
            dsoPessoas.recordset.MoveNext();
        }

        ImprimeRelatorioRetiradaMercadoria();
    }
    else {
        glb_bUnlockModal = true;
    }
}

function ImprimeRelatorioRetiradaMercadoria() {
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var Formato = 1;

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + Formato +
                        "&PedidoID=" + pedidos;

    dsoPessoas.recordset.MoveNext();

    try {
        var frameReport = document.getElementById("frmReport");
        frameReport.removeChild(frameReport.documentElement);
    }
    catch (e) {
        ;
    }

    frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/Reports.aspx?' + strParameters;

    pb_StopProgressBar(true);
    lockControlsInModalWin(false);
}

/********************************************************************
Imprime Nota Fiscal
Parametro:  Nenhum

Retorno: nenhum
********************************************************************/
function imprimeNotaFiscal() {
    lockControlsInModalWin(true);
    sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'callPrintNF()');
}

/********************************************************************
Destrava janela apos impressao da nota fiscal, quando a nota
for impressa atravez desta janela.
********************************************************************/
function unlockModalWinPrintPedidos() {
    if (getFrameInHtmlTop(getExtFrameID(window)).currentStyle.visibility == 'visible')
        lockControlsInModalWin(false);
}

function relatorioTransportadoras() {
    if (!chkDataEx(txtDataInicioTransp.value)) {
        if (window.top.overflyGen.Alert('Data de In�cio � Inv�lida') == 0)
            return null;
        lockControlsInModalWin(false);
        return null;
    }
    else if (!chkDataEx(txtDataFimTransp.value)) {
        if (window.top.overflyGen.Alert('Data de Fim � Inv�lida') == 0)
            return null;
        lockControlsInModalWin(false);
        return null;
    }

    var sDataInicio = '\'' + dateFormatToSearch(txtDataInicioTransp.value) + '\'';
    var sDataFim = '\'' + dateFormatToSearch(txtDataFimTransp.value) + '\'';

    var strSQL = '';

    var sSQLDateFormat = (DATE_FORMAT == 'DD/MM/YYYY' ? '103' : '101');

    var sFiltro = (trimStr(txtFiltroTransp.value).length > 0 ? ' AND (' + trimStr(txtFiltroTransp.value) + ') ' : '');

    var strSQL_Select1 = '';
    var sTranspCondition = '';

    if (selTransportadora.value == -1)
        sTranspCondition = ' AND Pedidos.ParceiroID = Pedidos.TransportadoraID ';
    else if (selTransportadora.value == 0)
        sTranspCondition = null;
    else
        sTranspCondition = 'AND Pedidos.TransportadoraID = ' + selTransportadora.value + ' ';

    sFiltro = trimStr(txtFiltroTransp.value);
    sFiltro = (sFiltro != '' ? ' AND ' + sFiltro : '');

    if (sFiltro == '') {
        sFiltro = null;
    }

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    // var formato = selFormato.value;
    var formato = 2;

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&sDataInicio=" + sDataInicio + "&sDataFim=" + sDataFim + "&sFiltro=" + sFiltro + "&chkFretePago=" + (chkFretePago.checked ? "1" : "0") +
                        "&sTranspCondition=" + sTranspCondition + "&sDATE_SQL_PARAM=" + DATE_SQL_PARAM;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/Reports.aspx?' + strParameters;

}

function selFamilias_onchange() {
    var nItensSelected = 0;

    for (i = 0; i < selFamilias.length; i++) {
        if (selFamilias.options[i].selected == true) {
            nItensSelected++;

            if (nItensSelected > 1)
                break;
        }
    }

    if (nItensSelected == 1)
        setLabelOfControl(lblFamilias, selFamilias.value);
    else
        setLabelOfControl(lblFamilias, '');
}

function selTransportadora_onchange() {
    if (selTransportadora.value != 0)
        setLabelOfControl(lblTransportadora, selTransportadora.value);
    else
        setLabelOfControl(lblTransportadora, '');
}

function pedido() {
    var dirA1;
    var dirA2;
    var aEmpresa = getCurrEmpresaData();
    var nIdiomaParaID = parseInt(aEmpresa[8], 10);
    var nTop = 0;
    var nTop2 = 0;
    var sMoeda = dsoProposta.recordset['Moeda'].value;
    var sMoedaConversao = dsoProposta.recordset['MoedaConversao'].value;

    var bEhCliente = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
            'dsoSup01.recordset[' + '\'' + 'EhCliente' + '\'' + '].value');


    //var formato = selFormato.value;

    var strParameters = "relatorioID=" + selReports.value + "&Moeda=" + sMoeda + "&MoedaConversao=" + sMoedaConversao + "&EhCliente=" + (bEhCliente == 1 ? true : false) + "&IdiomaParaID=" + nIdiomaParaID + "&EmpresaID=" + glb_nEmpresaID + "&PedidoID=" + glb_nPedidoID + "&DATE_SQL_PARAM=" + DATE_SQL_PARAM;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/Reports.aspx?' + strParameters;
    //window.document.location = SYS_PAGESURLROOT + '/PrintJet/PaginaTeste.aspx?';
}

function relatorioFabricantes() {
    var glb_TotalGrossSKID = txtTotalGross.value;

    if (isNaN(glb_TotalGrossSKID)) {
        if (window.top.overflyGen.Alert('Valor do Total Gross � inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    if (parseFloat(glb_TotalGrossSKID) < glb_TotalPesoBruto) {
        if (window.top.overflyGen.Alert('O valor do Total Gross n�o pode ser menor que o peso bruto.') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    var strPars = '?PedidoID=' + escape(glb_nPedidoID) +
		'&TotalGross=' + escape(glb_TotalGrossSKID);

    dsoGravaTotalGross.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/gravatotalgross.aspx' + strPars;
    dsoGravaTotalGross.ondatasetcomplete = relatorioFabricantes_DSC;
    dsoGravaTotalGross.Refresh();
}

function relatorioFabricantes_DSC() {
    var formato = 1;

    var sNomePessoa = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'NomePessoa' + '\'' + '].value');
    var nNotaFiscal = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selNotaFiscalID.options[selNotaFiscalID.selectedIndex].innerText');
    var sDataNF = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtDataNF.value');
    var strCustomerPO = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtSeuPedido.value');
    var glb_TotalGrossSKID = txtTotalGross.value;

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    var strParameters = "RelatorioID=" + selReports.value + "&aEmpresaData0=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&nEmpresaLogadaIdioma=" + aEmpresaData[8] + "&glb_nPedidoID=" + glb_nPedidoID + "&glb_nEmpresaID=" + glb_nEmpresaID + "&sNomePessoa=" + sNomePessoa + 
                        "&nNotaFiscal=" + nNotaFiscal + "&sDataNF=" + sDataNF + "&glb_sPaisEmpresa=" + glb_sPaisEmpresa + "&strCustomerPO=" + strCustomerPO + "&glb_TotalGrossSKID=" + glb_TotalGrossSKID + 
                        "&chkWS=" + (chkWS.checked ? true : false);

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modcomercial/submodpedidos/frmpedidos/serverside/Reports.aspx?" + strParameters;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
}

function relatorioComissao() {
    var dirA1;
    var dirA2;
    var strSQL = "";
    var i;
    var sSQLDateFormat = '';
    var sDataInicio = '';
    var sDataFim = '';
    var sFiltroEmpresas = '';
    var sFiltro = '';
    var sParceiroProprietarioID = selParceirosProprietariosID.value;
    var sParceiroID;
    var sEstadoID = selEstadoComissaoID.value;
    var sTipoComissaoID = selTipoComissaoID.value;

    if (!chkDataEx(txtdtVencimentoInicio.value)) {
        if (window.top.overflyGen.Alert('Data in�cio inv�lida') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    if (!chkDataEx(txtdtVencimentoFim.value)) {
        if (window.top.overflyGen.Alert('Data fim inv�lida') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }


    if (isNaN(sParceiroProprietarioID) || sParceiroProprietarioID <= 0) {
        if (!chkProprietario.checked) {
            if (window.top.overflyGen.Alert('Selecione um parceiro') == 0)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Selecione um Proprietario') == 0)
                return null;
        }

        lockControlsInModalWin(false);
        return null;
    }

    if (sEstadoID == 0) {
        sEstadoID = 'NULL';
        sFiltro = ' AND Comissao.EstadoID IN (1070,1071,1072,1073) ';
    }

    sDataInicio = '\'' + dateFormatToSearch(txtdtVencimentoInicio.value) + ' 00:00:00' + '\'';
    sDataFim = '\'' + dateFormatToSearch(txtdtVencimentoFim.value) + ' 23:59:59' + '\'';

    sSQLDateFormat = (DATE_FORMAT == 'DD/MM/YYYY' ? '103' : '101');

    for (i = 0; i < selEmpresaComissao.length; i++) {
        if (selEmpresaComissao.options[i].selected == true)
            sFiltroEmpresas += (sFiltroEmpresas == '' ? ' AND (' : ' OR ') + 'Pedidos.EmpresaID=' + selEmpresaComissao.options[i].value;
    }

    sFiltroEmpresas += (sFiltroEmpresas.length > 0 ? ')' : '');

    sFiltro += (trimStr(txtFiltroComissoes.value).length > 0 ? ' AND (' + trimStr(txtFiltroComissoes.value) + ') ' : '');

    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);

    if (!chkProprietario.checked) {
        sParceiroID = sParceiroProprietarioID;

        if (!(dirA1 && dirA2))
            sFiltro += ' AND Pedidos.ProprietarioID = ' + glb_USERID + ' ';
    }
    else {
        sParceiroID = 'NULL';
        sFiltro += ' AND Pedidos.ProprietarioID = ' + ((!(dirA1 && dirA2)) ? glb_USERID : sParceiroProprietarioID) + ' ';
    }
    var strParams = '';
    var elem = null;

    for (i = 0; i < divComissoes.children.length; i++) {
        elem = divComissoes.children[i];

        // Campos Textos
        if (((elem.tagName).toUpperCase() == 'INPUT') && ((elem.type).toUpperCase() == 'TEXT')
              && ((elem.id).toUpperCase() != 'TXTARGUMENTO')
               && ((elem.id).toUpperCase() != 'TXTFILTROCOMISSOES')) {
            if ((elem.value != null) && (trimStr(elem.value) != '')) {
                strParams += (elem.previousSibling).innerText;
                strParams += ':';
                strParams += trimStr(elem.value);
                strParams += '   ';
            }
        }
        else if ((elem.tagName).toUpperCase() == 'SELECT') {
            if ((elem.selectedIndex != -1)
                && ((elem.id).toUpperCase() != 'SELEMPRESACOMISSAO')
                && ((elem.id).toUpperCase() != 'SELTIPOCOMISSAOID')
                && ((elem.id).toUpperCase() != 'SELESTADOCOMISSAOID')) {
                if ((elem.options(elem.selectedIndex).innerText != null) && (trimStr(elem.options(elem.selectedIndex).innerText) != '')) {
                    strParams += (elem.previousSibling).innerText;
                    strParams += ':';
                    strParams += trimStr(elem.options(elem.selectedIndex).innerText);
                    strParams += '   ';
                }
            }
        }
    }

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var formato = selFormatoDivComissoes.value;
    var sLinguaLogada = getDicCurrLang();
    //var formato = 2;

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&sDataInicio=" + sDataInicio + "&sDataFim=" + sDataFim + "&sFiltro=" + sFiltro + "&sParceiroID=" + sParceiroID + "&sEstadoID=" + sEstadoID +
                        "&sTipoComissaoID=" + sTipoComissaoID + "&sFiltroEmpresas=" + sFiltroEmpresas + "&strParams=" + strParams + "&sLinguaLogada=" + sLinguaLogada;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/Reports.aspx?' + strParameters;
}

function pesquisaParceiroProprietario() {
    var sArgumento = txtArgumento.value;
    var sWhere = '';

    if (sArgumento.length < 2) {
        if (window.top.overflyGen.Alert('Argumento de pesquisa inv�lido') == 0)
            return null;

        return null;
    }

    if (chkProprietario.checked)
        sWhere = 'AND a.TipoPessoaID = 51 AND ClassificacaoID = 57 ';
    else
        sWhere = 'AND a.TipoPessoaID = 52 ';

    if (isNaN(sArgumento))
        sWhere = 'AND a.Fantasia LIKE \'' + sArgumento + '%\'';
    else if (!(isNaN(sArgumento)) && sArgumento.length == 14)
        sWhere = 'AND b.Numero LIKE \'' + sArgumento + '%\'';
    else
        sWhere = 'AND a.PessoaID LIKE \'' + sArgumento + '%\'';


    lockControlsInModalWin(true);

    setConnection(dsoParceirosProprietarios);

    sSQL = 'SELECT 0 AS PessoaID, SPACE(0) AS Fantasia UNION ' +
            'SELECT TOP 50 a.PessoaID AS PessoaID, a.Fantasia + SPACE(2) + \'(\'+ CONVERT(VARCHAR(16), a.PessoaID) + \')\' AS Fantasia ' +
			'FROM dbo.Pessoas a WITH(NOLOCK) ' +
            'INNER JOIN dbo.Pessoas_Documentos b WITH(NOLOCK) ON (a.PessoaID = b.PessoaID) ' +
			'WHERE a.EstadoID = 2 AND b.TipoDocumentoID = 111 ' + sWhere +
            'ORDER BY Fantasia';

    dsoParceirosProprietarios.SQL = sSQL;
    dsoParceirosProprietarios.ondatasetcomplete = pesquisaParceiroProprietario_DSC;
    dsoParceirosProprietarios.Refresh();
}

function pesquisaParceiroProprietario_DSC() {
    var optionValue;
    var optionStr;

    clearComboEx(['selParceirosProprietariosID']);

    while (!dsoParceirosProprietarios.recordset.EOF) {
        optionValue = dsoParceirosProprietarios.recordset.Fields['PessoaID'].value;
        optionStr = dsoParceirosProprietarios.recordset.Fields['Fantasia'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;

        selParceirosProprietariosID.add(oOption);
        dsoParceirosProprietarios.recordset.MoveNext();
    }

    lockControlsInModalWin(false);

    if (selParceirosProprietariosID.options.length == 2)
        selParceirosProprietariosID.selectedIndex = 1;

    selParceirosProprietariosID.disabled = (selParceirosProprietariosID.options.length == 1);

    btnOK_Status();
}

function txtArgumento_onkeypress() {
    if (event.keyCode == 13)
        pesquisaParceiroProprietario();
}

function chkProprietario_onclick() {
    clearComboEx(['selParceirosProprietariosID']);
    selParceirosProprietariosID.disabled = true;

    if (chkProprietario.checked)
        lblParceiroProprietario.innerText = 'Proprietario';
    else
        lblParceiroProprietario.innerText = 'Parceiro';
}

function selContato2_onchange() {
    selFormato_onchange();
}

function selContato1_onchange() {
    selFormato_onchange();
}

function selFormato_onchange() {

    if (selReports.value == 40133) {
        adjustDivPropostaComercial_Email();
    }
    if (selReports.value == 40132) {
        adjustDivSolicitacaoCompra_Email();
    }

}
function adjustDivPropostaComercial_Email() {

    if (selFormatoProposta.value == "1") {
        lblTo.style.visibility = 'hidden';
        txtTo.style.visibility = 'hidden';
        lblCC.style.visibility = 'hidden';
        txtCC.style.visibility = 'hidden';
        lblCCo.style.visibility = 'hidden';
        txtCCo.style.visibility = 'hidden';
    }
    else if (selFormatoProposta.value == "3") {
        lblTo.style.visibility = 'inherit';
        txtTo.style.visibility = 'inherit';
        lblCC.style.visibility = 'inherit';
        txtCC.style.visibility = 'inherit';
        lblCCo.style.visibility = 'inherit';
        txtCCo.style.visibility = 'inherit';

        txtTo.value = selContato2.options(selContato2.selectedIndex).getAttribute('EMailContato', 1);
        txtCCo.value = getCurrUserEmail();
    }

}

function adjustDivSolicitacaoCompra_Email() {

    if (selFormatoSolicitacao.value == "1") {
        lblToSolicitacao.style.visibility = 'hidden';
        txtToSolicitacao.style.visibility = 'hidden';
        lblCCSolicitacao.style.visibility = 'hidden';
        txtCCSolicitacao.style.visibility = 'hidden';
        lblCCoSolicitacao.style.visibility = 'hidden';
        txtCCoSolicitacao.style.visibility = 'hidden';
    }
    else if (selFormatoSolicitacao.value == "3") {
        lblToSolicitacao.style.visibility = 'inherit';
        txtToSolicitacao.style.visibility = 'inherit';
        lblCCSolicitacao.style.visibility = 'inherit';
        txtCCSolicitacao.style.visibility = 'inherit';
        lblCCoSolicitacao.style.visibility = 'inherit';
        txtCCoSolicitacao.style.visibility = 'inherit';

        txtToSolicitacao.value = selContato1.options(selContato1.selectedIndex).getAttribute('EMailContato', 1);
        txtCCoSolicitacao.value = getCurrUserEmail();
    }

}
