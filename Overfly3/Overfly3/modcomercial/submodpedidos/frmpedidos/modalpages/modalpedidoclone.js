/********************************************************************
modalpedidoclone.js

Library javascript para o modaleventoscontabeis_contas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Controla se as colunas OK e Observacoes devem ser readOnly e funcao
// do Estado da OP
var EmpresaLogadaID = getCurrEmpresaData();
var glb_EmpresaID = EmpresaLogadaID[0];
var dsoGen01 = new CDatatransport('dsoGen01');
var dsoGrava = new CDatatransport('dsoGrava');
var dsoGen03 = new CDatatransport('dsoGen03');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_pedidos_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 0;

    if (glb_totalCols__ != false)
        firstLine = 1;

    if (NewRow > firstLine) {
        fg.Row = NewRow;
    }

}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalpedidoclone_AfterEdit(Row, Col)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalpedidoclone_BeforeEdit(grid, row, col)
{
    ;
}

function js_fg_BeforeRowColChangePedidoLogistica(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    //Forca celula read-only    
    if (!glb_GridIsBuilding) {
        if (glb_FreezeRolColChangeEvents)
            return true;

        if (cellIsLocked(NewRow, NewCol)) {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
    }
}

function cellIsLocked(nRow, nCol) {
    var retVal = false;

    if (nCol != getColIndexByColKey(fg, 'OK'))
        return true;

    return retVal;
}

function js_fg_modalpedidocloneDblClick(grid, Row, Col) {

    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if ((Col != getColIndexByColKey(grid, 'OK')))
        return true;

    // trap so header
    if (Row != 1)
        return true;
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    with (modalpedidocloneBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 49;
    modalFrame.style.left = 0;
   
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
	startPesq();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    else if (ctl.id == btnListar.id) 
        startPesq();
    else if (ctl.id == btnGravar.id) 
        Gravar();
    
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Pedidos Clone', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10)-100;
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder-100;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

    with (divControls.style) {
        border = 5;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = btnOK.offsetLeft + btnOK.offsetWidth + ELEM_GAP - 10 ;
        height = 85;
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 1;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = btnListar.offsetTop + ELEM_GAP * 9;
        //width = MAX_FRAMEWIDTH_OLD - parseInt(left, 10) * (ELEM_GAP - 10);
        width = modWidth - 2 * ELEM_GAP - 30 ;
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 15)+20;
        //height = MAX_FRAMEHEIGHT_OLD - parseInt(top, 10) - (ELEM_GAP - 67)-100;
    }
    
    elem = document.getElementById('fg');
    with (elem.style) 
    {
        border = 1;
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10)+20;
        height = parseInt(divFG.style.height, 10)-70;
    }

    topFree = parseInt(divMod01.style.height, 10) + ELEM_GAP;
    adjustElementsInForm([['lblChavePesquisa', 'selChavePesquisa', 20, 1, 1, -10],
                         ['lblArgumento', 'txtArgumento', 22, 1, -2],
                         ['lblFiltro', 'txtFiltro', 22, 1, -2],
                         ['btnListar', 'btn', 30, 1,10],
                         ['btnGravar', 'btn', 30, 1,15]], null, null, true);
    
    btnOK.value = 'Fechar';
	btnOK.style.left = (parseInt(document.getElementById('divFG').currentStyle.width,10) /2) -
					   (parseInt(btnOK.style.width, 10) / 2);

	btnOK.style.visibility = 'hidden';
	btnCanc.style.visibility = 'hidden';

	btnListar.style.height = selChavePesquisa.offsetHeight + 2;
	btnGravar.style.height = selChavePesquisa.offsetHeight + 2;

	btnListar.style.width = selChavePesquisa.offsetHeight + 20;
	btnGravar.style.width = selChavePesquisa.offsetHeight + 20;
    
	startGridInterface(fg);
	fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modaleventoscontabeis_contasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    fg.focus();
}

function startPesq()
{
    var sWhere = '';
    var sInner = '';
    var sOrderby = '';
    var sArgumento = '';
    var sFiltro = '';
    var nLenArgumento = txtArgumento.value.length;
    var aLinesState = new Array();
    var nLenFiltro = txtFiltro.value.length;

    lockControlsInModalWin(true);
    glb_GridIsBuilding = true;
    /***************************
    value = 01 Pedido
    value = 02 Data
    value = 03 Nota Fiscal
    value = 04 Data Nota Fiscal
    value = 05 Pessoa
    value = 06 Total Pedido
    value = 07 Colaborador
    value = 08 Transportadora
    value = 09 Previsao Entrega
    value = 10 O mesmo
    value = 11 Numero Serie
    value = 12 Libera��o
        
    ***************************/
    sInner += '  ';
    
    if (nLenArgumento > 0) {
        if (selChavePesquisa.value == 1) {
            sArgumento = 'AND CONVERT(VARCHAR(12), a.PedidoID) = \'' + txtArgumento.value + '\'';
        }
        else if (selChavePesquisa.value == 2) {
            sArgumento = 'AND a.dtPedido >= \'' + dateFormatToSearch(txtArgumento.value) + '\'';
            sOrderby = ' DtPedido, ';
        }
        else if (selChavePesquisa.value == 3) {
            sArgumento = 'AND CONVERT(VARCHAR(12), a.NotaFiscal) = \'' + txtArgumento.value + '\'';
        }
        else if (selChavePesquisa.value == 4) {
            sArgumento = 'AND c.dtNotaFiscal >= \'' + dateFormatToSearch(txtArgumento.value) + '\'';
            sOrderby = ' dtNotaFiscal, ';
        }
        else if (selChavePesquisa.value == 5) {
            sArgumento = 'AND b.Nome LIKE \'%' + txtArgumento.value + '%\'';
        }
        else if (selChavePesquisa.value == 6) 
        {
            sArgumento = 'AND dbo.fn_Pedido_Totais(a.PedidoID,21) >= \'' + txtArgumento.value + '\'';
        }
        else if (selChavePesquisa.value == 7) 
        {
            sArgumento = 'AND e.Fantasia LIKE \'%' + txtArgumento.value + '%\'';
        }
        else if (selChavePesquisa.value == 8) 
        {
            sArgumento = 'AND f.Fantasia LIKE \'%' + txtArgumento.value + '%\'';
        }
        else if (selChavePesquisa.value == 9) 
        {
            sArgumento = 'AND a.dtPrevisaoEntrega >= \'' + dateFormatToSearch(txtArgumento.value) + '\'';
            sOrderby = ' a.dtPrevisaoEntrega, ';
        }
        else if (selChavePesquisa.value == 10) 
        {
            sArgumento = 'AND a.PessoaID = a.TransportadoraID ';
        }
        else if (selChavePesquisa.value == 11) 
        {
            sInner = ' INNER JOIN dbo.fn_Pedido_NumeroSerie_tbl(' + txtArgumento.value + ') g ON (a.PedidoID = g.PedidoID) ';
        }
        else if (selChavePesquisa.value == 12) 
        {
            sArgumento = 'AND dbo.fn_Pedido_Datas(a.PedidoID,5) >= \'' + dateFormatToSearch(txtArgumento.value) + '\'';
        }
    }
    
    if (nLenFiltro > 0)
        sFiltro = ' AND ' + txtFiltro.value;
    
    var sSQL = 'SELECT DISTINCT TOP 10 0 AS OK , a.PedidoID, d.RecursoAbreviado AS Estado, a.TransacaoID, a.dtPedido AS DtPedido, b.Fantasia AS Pessoa, c.NotaFiscal, ' +
               'CONVERT(VARCHAR(10),c.dtNotaFiscal,103) AS dtNotaFiscal, ' +
            'a.ValorTotalPedido, ' +
            '(CASE a.TransportadoraID WHEN a.ParceiroID THEN \'O mesmo\' ' +
            'WHEN a.EmpresaID THEN \'Nosso carro\' ' +
            'ELSE (SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID=a.TransportadoraID) END) AS Transportadora, ' +
            ' a.Observacao ' +
            ' FROM Pedidos a WITH(NOLOCK) ' +
            ' INNER JOIN Pessoas b WITH(NOLOCK) ON (a.PessoaID = b.PessoaID) ' +
            ' INNER JOIN  NotasFiscais c WITH(NOLOCK) ON (c.NotaFiscalID = a.NotaFiscalID) ' +
            ' INNER JOIN Recursos d WITH(NOLOCK) ON (a.EstadoID = d.RecursoID) ' +
            ' INNER JOIN Pessoas e WITH(NOLOCK) ON (a.ProprietarioID = e.PessoaID) ' +
            ' INNER JOIN Pessoas f WITH(NOLOCK) ON (a.TransportadoraID = f.PessoaID) ' +
            ' INNER JOIN pedidos_itens i WITH(NOLOCK) ON a.PedidoID = i.PedidoID ' +
	        ' INNER JOIN Conceitos j WITH(NOLOCK) ON i.ProdutoID = j.ConceitoID ' +
            sInner +
            ' WHERE a.TransacaoID in /*(111,322)*/(111) AND a.EmpresaID = ' + glb_EmpresaID +
            ' AND a.EstadoID = 28 AND a.TipoPedidoID = 601 ' +
            ' AND a.PedidoID NOT IN (SELECT y.PedidoID ' +
            '                          FROM Pedidos_Itens q WITH(NOLOCK) ' +
            '                             INNER JOIN Pedidos_Itens y WITH(NOLOCK) ON (q.PedItemReferenciaID = y.PedItemID)' +
            '                             INNER JOIN Pedidos z WITH(NOLOCK) ON (z.PedidoID = q.PedidoID) ' +
            '                             INNER JOIN Operacoes_Baixas w WITH(NOLOCK) ON (w.OperacaoID = z.TransacaoID) ' +
            '                           WHERE y.PedidoID = a.PedidoID) ' +
            ' AND j.ProdutoSeparavel = 1 ' +
            ' AND a.dtPedido>=\'' + dateFormatToSearch('2013-08-01') +'\' '+ 
            sArgumento + sFiltro + 
            ' ORDER BY ' + sOrderby + 'A.PedidoID DESC';



    setConnection(dsoGen01);
    
    dsoGen01.SQL = sSQL;
    dsoGen01.ondatasetcomplete = startPesq_DSC;
    try 
    {
        dsoGen01.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }

    
}

function startPesq_DSC()
{
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    glb_currDSO = dsoGen01;
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    glb_GridIsBuilding = true;

    headerGrid(fg, ['PedidoID',
                    'OK',
                    'Pedido',
                    'Est',
                    'Tran',
                    'DtPedido',
                    'Pessoa',
                    'Nota Fiscal',
                    'Dt Nota Fiscal',
                    'Valor Pedido',
                    'Transportadora',
                    'Observa��o'], [0]);

    //glb_aCelHint = [[0, 4, 'Mensagem usada somente se estiver presente em uma rela��o']];

    fillGridMask(fg, dsoGen01, [  'PedidoID*',
                                  'OK',
                                  'PedidoID*',
                                  'Estado*',
                                  'TransacaoID*',
                                  'DtPedido*',
                                  'Pessoa*',
                                  'NotaFiscal*',
                                  'dtNotaFiscal*',
                                  'ValorTotalPedido*',
                                  'Transportadora*',
                                  'Observacao*'],
                                  ['', '', '', '', '', '99/99/9999', '', '', '99/99/9999', '999999999.99', '', ''],
                                  ['', '', '', '', '', dTFormat, '', '', dTFormat, '(###,###,##0.00)', '', '']);
    
    alignColsInGrid(fg,[0, 3, 4, 5, 6, 7, 8, 9]);
    
    fg.ColDataType(1) = 11;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.ExplorerBar = 5;
    fg.Redraw = 2;
    btnOK.disabled = false;
    
    lockControlsInModalWin(false);
    if (fg.Rows > 1)
		fg.Row = 1;

    if (fg.Rows <= 1) {
        btnGravar.disabled = true;
        btnOK.disabled = true;
    }       
    glb_GridIsBuilding = false;
    fg.Editable = true;
	fg.focus();
}


function Gravar() 
{
    setConnection(dsoGrava);
    lockControlsInModalWin(true);

    nDataLen = 0;
    strPars = 'nEmpresaID=' + escape(glb_EmpresaID) + '&nUserID=' + escape(glb_nUserID) + '&nTipoPedido=' + escape(2);

    for (y = 1; y <= (fg.Rows - 1); y++) {
        if (fg.TextMatrix(y, getColIndexByColKey(fg, 'OK')) == -1)
        {
           strPars = strPars + '&nPedidoID=' + escape(fg.TextMatrix(y, getColIndexByColKey(fg, 'PedidoID*')));
           nDataLen++;
        }
    }


    strPars += '&nDataLen=' + escape(nDataLen);

    try {
        dsoGrava.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/clonapedido.aspx?' + strPars;
        dsoGrava.ondatasetcomplete = Gravar_DSC;
        dsoGrava.Refresh();

    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
    }
}

function Gravar_DSC() { 
    lockInterface(false);
    var msgAlertGeral;
    var msgAlert = '';
    
    while (!dsoGrava.recordset.EOF) {
         msgAlert += dsoGrava.recordset.Fields["Resultado"].value;
        dsoGrava.recordset.MoveNext();
    }

    if (msgAlert != '')
        window.top.overflyGen.Alert(msgAlert);
        
    startPesq();
    return null;
    
}
