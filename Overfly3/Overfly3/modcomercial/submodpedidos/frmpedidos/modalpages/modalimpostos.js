/********************************************************************
modalimpostos.js

Library javascript para o modalimpostos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_EstadoID=0;
var glb_PedidoFechado=0;
var glb_CFOPIDAlteraImposto = false;
var glb_CFOPID = false;
var glb_GridEditabled=false;
var glb_PedidoID = 0;
var glb_ComplementoValor = false;
var glb_nDSOCounter = 0;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_EmpresaID = getCurrEmpresaData();
var glb_Empresa = glb_EmpresaID[0];
var glb_PedidoComplemento = false;
var glb_EhToamacaoServ = 0;
var glb_TransacaoID = 0;


var dsoGen01 = new CDatatransport('dsoGen01');
var dsoGen02 = new CDatatransport('dsoGen02');
var dsoCFOPImpostos = new CDatatransport('dsoCFOPImpostos');
var dsoComplementoImposto = new CDatatransport('dsoComplementoImposto');
var dsoDiferencialAliquotas = new CDatatransport('dsoDiferencialAliquotas');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// 0 - Impostos.
// 1 - Impostos complementares.
// 2 - Apura��o de impostos.
var glb_BtnImpostosMode = 0;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    glb_PedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');
    glb_EstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
    glb_PedidoFechado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Fechado' + '\'' + '].value');
    glb_PedidoComplemento = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PedidoComplemento' + '\'' + '].value');
    glb_EhImportacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EhImportacao' + '\'' + '].value');
    glb_TransacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TransacaoID' + '\'' + '].value');
    glb_EhToamacaoServ = ((glb_TransacaoID == 221 || glb_TransacaoID == 222) ? 1 : 0);
    glb_EhUSoConsumo = ((glb_TransacaoID == 741 || glb_TransacaoID == 711) ? 1 : 0);

    glb_nDSOCounter = 2;

    setConnection(dsoCFOPImpostos);

    dsoCFOPImpostos.SQL = 'SELECT TOP 1 ISNULL(b.AlteraImpostos,0) AS AlteraImpostos, a.CFOPID AS CFOPID ' +
        'FROM Pedidos_Itens a WITH(NOLOCK) ' +
            'INNER JOIN Operacoes b WITH(NOLOCK) ON (a.CFOPID=b.OperacaoID) ' +
        'WHERE (a.PedidoID=' + glb_PedidoID + ') ' +
        'ORDER BY ISNULL(b.AlteraImpostos,0)';

    dsoCFOPImpostos.ondatasetcomplete = window_onload_DSC;
    dsoCFOPImpostos.Refresh();

    setConnection(dsoComplementoImposto);

    dsoComplementoImposto.SQL = 'SELECT CONVERT(BIT, COUNT(*)) AS TemComplementoImpostos ' +
        'FROM Pedidos_Itens a WITH(NOLOCK) ' +
            'INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ProdutoID=b.ConceitoID) ' +
        'WHERE (a.PedidoID=' + glb_PedidoID + ' AND b.ProdutoID = 2830) ';

    dsoComplementoImposto.ondatasetcomplete = window_onload_DSC;
    dsoComplementoImposto.Refresh();

    //setBtnImpostosMode();
}

function window_onload_DSC() {
    glb_nDSOCounter--;

    glb_GridEditabled = false;

    if (glb_nDSOCounter > 0)
        return null;

    if ((dsoCFOPImpostos.recordset.BOF) && (dsoCFOPImpostos.recordset.EOF))
        glb_CFOPIDAlteraImposto = false;
    else
        glb_CFOPIDAlteraImposto = dsoCFOPImpostos.recordset['AlteraImpostos'].value;
    //rrf
    if ((dsoCFOPImpostos.recordset.BOF) && (dsoCFOPImpostos.recordset.EOF))
        glb_CFOPID = false;
    else
        glb_CFOPID = dsoCFOPImpostos.recordset['CFOPID'].value;

    if (glb_CFOPIDAlteraImposto == 1)
        glb_CFOPIDAlteraImposto = true;
    else
        glb_CFOPIDAlteraImposto = false;

    if ((dsoComplementoImposto.recordset.BOF) && (dsoComplementoImposto.recordset.EOF))
        glb_ComplementoValor = false;
    else
        glb_ComplementoValor = dsoComplementoImposto.recordset['TemComplementoImpostos'].value;

    if (glb_ComplementoValor == 1)
        glb_ComplementoValor = true;
    else
        glb_ComplementoValor = false;

    if ((glb_EstadoID == 21) && (((glb_PedidoFechado == 1) && ((glb_CFOPIDAlteraImposto) || (glb_ComplementoValor) || (glb_PedidoComplemento))) || (glb_EhToamacaoServ == 1) || (glb_EhUSoConsumo == 1)))
        glb_GridEditabled = true;

    if (glb_Empresa == 255534)
        glb_CFOPIDAlteraImposto = true;

    // configuracao inicial do html
    setupPage();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 49;
    modalFrame.style.left = 0;

    getDataInServer();
}

function btn_onclick_Apuracao(ctl) {
    getDataInServer();
}



/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        if (glb_BtnImpostosMode == 0)
            updateImpostosDataInServer();
        else if (glb_BtnImpostosMode == 1)
            updateImpostosComplementaresDataInServer();
        else if (glb_BtnImpostosMode == 2)
            updateImpostosComplementaresDataInServer();
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Impostos', 1);

    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    //topFree = parseInt(divMod01.style.height, 10) + ELEM_GAP;

    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;

    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;

    // Ajusta botoes OK/Cancel para esta janela
    with (btnOK.style) {
        disabled = true;
        left = (widthFree - 2 * parseInt(width, 10) - ELEM_GAP) / 2;
    }
    with (btnCanc.style) {
        left = (parseInt(btnOK.currentStyle.left, 10) + parseInt(btnOK.currentStyle.width, 10) + ELEM_GAP);
    }

    /*
    // Selecao e botao.
    btnImpostos.style.top = topFree + 16;
    btnImpostos.style.left = ELEM_GAP;
    btnImpostos.style.width = FONT_WIDTH * 7 + 80;
    */

    selTipo.style.top = topFree + 16;
    selTipo.style.left = ELEM_GAP;
    selTipo.style.width = FONT_WIDTH * 7 + 120;

    lblTipo.style.top = topFree;
    lblTipo.style.left = selTipo.offsetLeft;
    lblTipo.style.width = FONT_WIDTH * 7;

    lblImpostoID.style.top = topFree;
    lblImpostoID.style.left = selTipo.offsetLeft + selTipo.offsetWidth + ELEM_GAP;
    lblImpostoID.style.width = FONT_WIDTH * 7;

    selImpostoID.style.top = lblImpostoID.offsetTop + lblImpostoID.offsetHeight;
    selImpostoID.style.left = selTipo.offsetLeft + selTipo.offsetWidth + ELEM_GAP;
    selImpostoID.style.width = FONT_WIDTH * 7;
    selImpostoID.onchange = selImpostoIDOnchange;

    //lblImpostoApuracao
    //chkImpostoApuracao

    // Selecao e botao.
    lblImpostoApuracao.style.top = topFree;
    lblImpostoApuracao.style.left = selImpostoID.offsetLeft + selImpostoID.offsetWidth + ELEM_GAP;
    lblImpostoApuracao.style.width = FONT_WIDTH * 7;

    chkImpostoApuracao.style.top = lblImpostoApuracao.offsetTop + lblImpostoApuracao.offsetHeight;
    chkImpostoApuracao.style.left = selImpostoID.offsetLeft + selImpostoID.offsetWidth + ELEM_GAP;
    chkImpostoApuracao.style.width = FONT_WIDTH * 7;
    //chkImpostoApuracao.onchange = chkImpostoApuracaoOnchange;

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = selImpostoID.offsetTop + selImpostoID.offsetHeight + ELEM_GAP;
        width = modWidth - frameBorder - 2 * ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) -
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10);
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServer()//ERRO
{
    var strPars = new String();

    if (chkImpostoApuracao.checked == true) {
        glb_BtnImpostosMode = 2;
    }
    else if (glb_BtnImpostosMode != 1)
        glb_BtnImpostosMode = 0;
    /*else
        setBtnImpostosMode();*/

    // mandar os parametros para o servidor
    strPars = '?nPedidoID=' + escape(glb_nPedidoID) +
		'&nModo=' + escape(glb_BtnImpostosMode) +
		'&nImpostoID=' + escape(selImpostoID.value);

    dsoGen01.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/impostosdata.aspx' + strPars;

    if (glb_BtnImpostosMode == 0) {
        dsoGen01.ondatasetcomplete = fillModalGrid_DSC;
    }
    else if (glb_BtnImpostosMode == 1) {
        dsoGen01.ondatasetcomplete = fillModalGridComplementares_DSC;
    }
    else if (glb_BtnImpostosMode == 2) {
        dsoGen01.ondatasetcomplete = fillModalGridApuracao_DSC;
    }

    dsoGen01.refresh();
}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGrid_DSC() {
    var i, j, nSumImp;
    var nAliquota = 0;

    fg.Redraw = 0;

    headerGrid(fg, ['Ordem',
                   'ID',
                   'Produto',
                   'NCM',
                   'Imposto',
                   'Carga',
                   'Al�quota',
                   'Base',
                   'Base C�lculo',
                   'Valor Imposto',
                   'Tipo',
                   'AlteraAliquota',
                   'EsquemaCredito',
                   'InclusoPreco',
                   'ImpostoID',
                   'PedImpostoID',
                   'OK',
                   'CFOPID'], [10, 11, 12, 13, 14, 15, 16, 17]);


    glb_aCelHint = [[0, 3, 'Clique duas vezes no NCM para abrir o documento NCM']];


    fillGridMask(fg, dsoGen01, ['Ordem',
                              'ProdutoID',
                              'Conceito',
							  'NCM',
                              'Imposto',
                              'Carga',
                              'Aliquota',
                              'Base',
                              'BaseCalculo',
                              'ValorImposto',
                              'TipoImposto',
                              'AlteraAliquota',
                              'EsquemaCredito',
                              'InclusoPreco',
                              'ImpostoID',
                              'PedImpostoID',
                              'OK',
                              'CFOPID'],
                              ['', '', '', '', '', '999.99', '999.99', '999.99', '999999999.99', '999999999.99', '', '', '', '', '', '', '', ''],
                              ['', '', '', '', '', '##0.00', '##0.00', '##0.00', '###,###,##0.00', '###,###,##0.00', '', '', '', '', '', '', '', '']);

    alignColsInGrid(fg, [0, 1, 5, 6, 7, 8, 9]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    // linha de Totais
    gridHasTotalLine(fg, 'Total', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'ValorImposto'), '###,###,##0.00', 'S']]);

    // soma apenas os impostos tipo 1
    // e coloca cor nas linhas tipo 1
    if (fg.Rows > 2) {
        nSumImp = 0;
        j = 0;
        for (i = 1; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')) == 1) {
                nSumImp += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto'));
                j++;
            }
        }

        with (fg) {
            if (j > 0)
                Cell(6, getColIndexByColKey(fg, 'Conceito'), 0, getColIndexByColKey(fg, 'Conceito') + j - 1, getColIndexByColKey(fg, 'ValorImposto')) = 0XE0FFFF;

            if (glb_nTotalPedido > 0)
                nAliquota = (nSumImp / glb_nTotalPedido) * 100;

            TextMatrix(1, getColIndexByColKey(fg, 'Aliquota')) = nAliquota;
            TextMatrix(1, getColIndexByColKey(fg, 'BaseCalculo')) = glb_nTotalPedido;
            TextMatrix(1, getColIndexByColKey(fg, 'ValorImposto')) = nSumImp;
        }
        fg.Row = 2;
        fg.Col = getColIndexByColKey(fg, 'Aliquota');
    }

    fg.Redraw = 2;

    with (modalimpostosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();

    fg.Editable = glb_GridEditabled;

    btnOK.disabled = true;
}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGridComplementares_DSC() {

    /* HABS 30/04/2010 - glb_GridIsBuilding(Oculta mensagem "Stack overflow at line: 0"   */

    glb_GridIsBuilding = true;

    var i, j, nSumImp;

    fg.Redraw = 0;

    headerGrid(fg, ['Ordem',
					'ID',
					'Produto',
					'NCM',
					'Imposto',
					'Al�quota',
					'Base C�lculo',
					'Valor Imposto',
					'Tipo',
					'ImpostoID',
					'CFOPID',
					'CFOPAIC',
					'ImpostoAIC',
					'PedImpostoID',
					'OK'], [8, 9, 10, 11, 12, 13, 14]);

    glb_aCelHint = [[0, 3, 'Clique duas vezes no NCM para abrir o documento NCM']];

    fillGridMask(fg, dsoGen01, ['Ordem',
								'ProdutoID',
								'Conceito',
								'NCM',
								'Imposto',
								'Aliquota',
								'BaseCalculo',
								'ValorImposto',
								'TipoImposto',
								'ImpostoID',
								'CFOPID',
								'CFOPAIC',
								'ImpostoAIC',
								'PedImpostoID',
								'OK'],
								['', '', '', '', '', '', '999999999.99', '999999999.99', '', '', '', '', '', ''],
								['', '', '', '', '', '', '###,###,##0.00', '###,###,##0.00', '', '', '', '', '', '']);

    alignColsInGrid(fg, [0, 1, 6, 7]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    // linha de Totais
    gridHasTotalLine(fg, 'Total', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'ValorImposto'), '###,###,##0.00', 'S']]);

    // soma apenas os impostos tipo 1
    // e coloca cor nas linhas tipo 1
    if (fg.Rows > 2) {
        fg.Row = 2;
        fg.Col = getColIndexByColKey(fg, 'BaseCalculo');
    }

    fg.Redraw = 2;

    with (modalimpostosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();

    fg.Editable = glb_GridEditabled;

    btnOK.disabled = true;

    glb_GridIsBuilding = false;

}
/*********************************************************************************************/
//Preenchimento do grid Impostos Apura��o;
/*********************************************************************************************/

function fillModalGridApuracao_DSC() {
    var i, j, nSumImp;
    var nAliquota = 0;
    var aHint = new Array();

    fg.Redraw = 0;

    headerGrid(fg, ['Ordem',
                   'ID',
                   'Produto',
                   'NCM',
                   'CFOPID',
                   'CST',
                   'Imposto',
                   'Carga',
                   'Al�quota',
                   'Base',
                   'Base C�lculo',
                   'Valor Imposto',
                   'Tipo',
                   'AlteraAliquota',
                   'EsquemaCredito',
                   'InclusoPreco',
                   'ImpostoID',
                   'PedImpostoID',
                   'OK',
                   'Apura��o',
                   'DescricaoCST',
                   'CFOP'], [12, 13, 14, 15, 16, 17, 18, 20, 21]);

    j = 0;
    aHint[j] = [0, 3, 'Clique duas vezes no NCM para abrir o documento NCM'];

    fillGridMask(fg, dsoGen01, ['Ordem',
                              'ProdutoID',
                              'Conceito',
							  'NCM',
							  'CFOPID',
							  'CST',
                              'Imposto',
                              'Carga',
                              'Aliquota',
                              'Base',
                              'BaseCalculo',
                              'ValorImposto',
                              'TipoImposto',
                              'AlteraAliquota',
                              'EsquemaCredito',
                              'InclusoPreco',
                              'ImpostoID',
                              'PedImpostoID',
                              'OK',
                              'ImpostoApuracao',
                              'DescricaoCST',
                              'CFOP'],
                              ['', '', '', '', '', '', '', '999.99', '999.99', '999.99', '999999999.99', '999999999.99', '', '', '', '', '', '', '', '', '', ''],
                              ['', '', '', '', '', '', '', '##0.00', '##0.00', '##0.00', '###,###,##0.00', '###,###,##0.00', '', '', '', '', '', '', '', '', '']);

    glb_aCelHint = aHint;

    alignColsInGrid(fg, [0, 1, 7, 8, 9, 10, 11, 12]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    fg.ColDataType(getColIndexByColKey(fg, 'ImpostoApuracao')) = 11;

    // linha de Totais
    gridHasTotalLine(fg, 'Total', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'ValorImposto'), '###,###,##0.00', 'S']]);

    i = 2;

    while (i < fg.Rows) {
        if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'DescricaoCST')) != ' ') && (fg.TextMatrix(i, getColIndexByColKey(fg, 'DescricaoCST')) != '')) {
            j++;
            aHint[j] = [i, getColIndexByColKey(fg, 'CST'), fg.TextMatrix(i, getColIndexByColKey(fg, 'DescricaoCST'))];
        }

        if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'CFOP')) != ' ') && (fg.TextMatrix(i, getColIndexByColKey(fg, 'CFOP')) != '')) {
            j++;
            aHint[j] = [i, getColIndexByColKey(fg, 'CFOPID'), fg.TextMatrix(i, getColIndexByColKey(fg, 'CFOP'))];
        }

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'CST')) == "")
            fg.Cell(6, i, getColIndexByColKey(fg, 'CST'), i, getColIndexByColKey(fg, 'CST')) = 0x00ffff;

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'ImpostoApuracao')) == "")
            fg.Cell(6, i, getColIndexByColKey(fg, 'ImpostoApuracao'), i, getColIndexByColKey(fg, 'ImpostoApuracao')) = 0x00ffff;

        i++;
    }


    // soma apenas os impostos tipo 1
    // e coloca cor nas linhas tipo 1
    if (fg.Rows > 2) {
        nSumImp = 0;
        j = 0;
        for (i = 1; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')) == 1) {
                nSumImp += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto'));
                j++;
            }
        }

        with (fg) {
            if (j > 0)
                Cell(6, getColIndexByColKey(fg, 'Conceito'), 0, getColIndexByColKey(fg, 'Conceito') + j - 1, getColIndexByColKey(fg, 'ImpostoApuracao')) = 0XE0FFFF;

            if (glb_nTotalPedido > 0)
                nAliquota = (nSumImp / glb_nTotalPedido) * 100;

            TextMatrix(1, getColIndexByColKey(fg, 'Aliquota')) = nAliquota;
            TextMatrix(1, getColIndexByColKey(fg, 'BaseCalculo')) = glb_nTotalPedido;
            TextMatrix(1, getColIndexByColKey(fg, 'ValorImposto')) = nSumImp;
        }
        fg.Row = 2;
        fg.Col = getColIndexByColKey(fg, 'Aliquota');
    }

    fg.Redraw = 2;

    with (modalimpostosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();

    fg.Editable = glb_GridEditabled;

    btnOK.disabled = true;
}

function js_ModalImpostos_AfterEdit(Row, Col) {
    if (glb_BtnImpostosMode == 0)
        js_ModalImpostosNormais_AfterEdit(Row, Col);
    else if (glb_BtnImpostosMode == 1)
        js_ModalImpostosComplementares_AfterEdit(Row, Col);
}

function js_ModalImpostosComplementares_AfterEdit(Row, Col) {
    fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
    fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;
    btnOK.disabled = false;
}

function js_ModalImpostosNormais_AfterEdit(Row, Col) {
    var i = 0;
    var nSumImp = 0;
    var nSumImpEspecifico = 0;
    var nAliquota = 0;
    var nValorImposto = 0;
    var nAliquota = 0;
    var nBase = 0;
    var nImpostoID = 0;
    var nSumImpBCEspecifico = 0;

    fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

    nAliquota = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Aliquota'));

    if ((nAliquota == 0) && (glb_PedidoComplemento == false)) {
        (window.top.overflyGen.Alert('Aliquota igual a 0.'));
        return null;
    }

    nBase = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'BaseCalculo'));
    nValorImposto = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ValorImposto'));
    nImpostoID = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ImpostoID'));

    if (glb_PedidoComplemento) {
        if (fg.Rows > 2) {
            for (i = 1; i < fg.Rows; i++) {
                if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')) == 2)
                    nSumImp += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto'));
            }
        }
        // Atualiza o total do imposto
        for (i = 1; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')) == 1) {
                if (nImpostoID == fg.ValueMatrix(i, getColIndexByColKey(fg, 'ImpostoID'))) {

                    if (nValorImposto > 0 && nBase > 0)
                        fg.TextMatrix(i, getColIndexByColKey(fg, 'Aliquota')) = ((nValorImposto / nBase) * 100);

                    fg.TextMatrix(i, getColIndexByColKey(fg, 'ValorImposto')) = nValorImposto;
                    fg.TextMatrix(i, getColIndexByColKey(fg, 'BaseCalculo')) = nBase;
                    fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = 1;
                }
            }
        }

        fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorImposto')) = nSumImp;

        fg.Row = Row;
        fg.Col = Col;
    }
    else if (glb_EhToamacaoServ) {
        if (fg.Rows > 2)  {
            for (i = 1; i < fg.Rows; i++) {
                if ((fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')) == 2) && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aliquota')) > 0)) {

                    nAliquota = 0;
                    fg.TextMatrix(i, getColIndexByColKey(fg, 'ValorImposto')) = (((fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aliquota'))) / 100) * fg.ValueMatrix(i, getColIndexByColKey(fg, 'BaseCalculo')));
                    nSumImp += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto'));
                    nAliquota += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aliquota'));

                }
            }
            if (nSumImp > 0)
                fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorImposto')) = nSumImp;
        }

        // Atualiza o total do imposto
        for (i = 1; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')) == 1) {
                if (nImpostoID == fg.ValueMatrix(i, getColIndexByColKey(fg, 'ImpostoID'))) {

                    fg.TextMatrix(i, getColIndexByColKey(fg, 'ValorImposto')) = nSumImp;
                    fg.TextMatrix(i, getColIndexByColKey(fg, 'Aliquota')) = nAliquota;
                    fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = 1;

                }
            }
        }
    }
    else {
        fg.TextMatrix(Row, getColIndexByColKey(fg, 'BaseCalculo')) = 0;

        if (fg.Rows > 2) {
            for (i = 1; i < fg.Rows; i++) {
                if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')) == 2) {
                    nSumImp += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto'));

                    if (i == Row)
                        fg.TextMatrix(i, getColIndexByColKey(fg, 'BaseCalculo')) = roundNumber((fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto')) / (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aliquota')) / 100)), 2);

                    if (nImpostoID == fg.ValueMatrix(i, getColIndexByColKey(fg, 'ImpostoID'))) {
                        nSumImpEspecifico += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto'));
                        nSumImpBCEspecifico += fg.ValueMatrix(i, getColIndexByColKey(fg, 'BaseCalculo'));
                        if (glb_Empresa == 255534)
                            fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = 1;

                    }
                }
            }

            // Atualiza o total do imposto
            for (i = 1; i < fg.Rows; i++) {
                if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')) == 1) {
                    if (nImpostoID == fg.ValueMatrix(i, getColIndexByColKey(fg, 'ImpostoID'))) {
                        fg.TextMatrix(i, getColIndexByColKey(fg, 'ValorImposto')) = nSumImpEspecifico;
                        fg.TextMatrix(i, getColIndexByColKey(fg, 'BaseCalculo')) = nSumImpBCEspecifico;
                        fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = 1;

                    }
                }
            }

            fg.Row = Row;
            fg.Col = Col;
        }

        if (nAliquota < 0) {
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'Aliquota')) = 0;
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorImposto')) = 0;
        }

        if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoImposto')) == 1) {
            if (nBase > 0)
                fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorImposto')) = roundNumber(nBase * (nAliquota / 100), 2);
            else {
                nBase = 0;
                fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorImposto')) = 0;
                fg.TextMatrix(Row, getColIndexByColKey(fg, 'BaseCalculo')) = 0;
            }
        }

        //Totais            
        if (fg.Rows > 2) {
            for (i = 1; i < fg.Rows; i++) {
                if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')) == 1) {
                    nSumImp += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto'));
                }
            }

            with (fg) {
                if (nBase > 0)
                    nAliquota = (nSumImp / glb_nTotalPedido) * 100;
                TextMatrix(1, getColIndexByColKey(fg, 'Aliquota')) = nAliquota;
                TextMatrix(1, getColIndexByColKey(fg, 'ValorImposto')) = nSumImp;
                TextMatrix(1, getColIndexByColKey(fg, 'BaseCalculo')) = glb_nTotalPedido;
            }
            fg.Row = Row;
            fg.Col = Col;
        }
    }

    fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;
    btnOK.disabled = false;
}

function js_BeforeRowColChangeModalImpostos(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {

    glb_GridEditabled = false;


    if (glb_BtnImpostosMode == 0) {
        if ((glb_EstadoID == 21) && (((glb_PedidoFechado == 1) && ((glb_CFOPIDAlteraImposto) || (glb_ComplementoValor) || (glb_PedidoComplemento))) || (glb_EhToamacaoServ == 1) || (glb_EhUSoConsumo == 1)))
            glb_GridEditabled = true;
    }
    else if (glb_BtnImpostosMode == 1) {
        if (NewRow > 0) {
            if ((grid.ValueMatrix(NewRow, getColIndexByColKey(grid, 'CFOPAIC')) != '0') &&
				(grid.ValueMatrix(NewRow, getColIndexByColKey(grid, 'ImpostoAIC')) != '0'))
                glb_GridEditabled = true;
        }
    }
    if (glb_Empresa == 255534)
        glb_GridEditabled = true;


    fg.Editable = glb_GridEditabled;

    //Forca celula read-only    
    if ((!glb_GridIsBuilding) && (glb_GridEditabled)) {
        if (glb_FreezeRolColChangeEvents)
            return true;

        if (!currCellIsLocked(grid, NewRow, NewCol)) {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalimpostosDblClick(grid, Row, Col) {
    if ((Row > 0) && (Col == 3)) {
        var sNCM = fg.TextMatrix(Row, getColIndexByColKey(fg, 'NCM'));
        sNCM = sNCM.substring(0, 4);

        strPars = '?sNCM=' + escape(sNCM);
        htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/serverside/imagemncm.asp' + strPars;
        window.open(htmlPath);
    }
}

function currCellIsLocked(grid, nRow, nCol) {
    var bRetVal = false;
    glb_CFOPID = dsoCFOPImpostos.recordset['CFOPID'].value;

    if (glb_GridEditabled) {
        if (glb_BtnImpostosMode == 0) {
            if ((grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'TipoImposto')) == 2) &&
				    ((grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'AlteraAliquota')) == 1) ||
                    (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'AlteraAliquota')) == 0) &&
                    (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'EsquemaCredito')) == 1) &&
                    (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'InclusoPreco')) == 0)) &&
				    ((nCol == getColIndexByColKey(grid, 'ValorImposto')) ||
				        (nCol == getColIndexByColKey(grid, 'BaseCalculo')) ||
			            (nCol == getColIndexByColKey(grid, 'Aliquota'))))
                bRetVal = true;


            if ((grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'TipoImposto')) == 2) &&
				    (glb_ComplementoValor) && (nCol == getColIndexByColKey(grid, 'ValorImposto')))
                bRetVal = true;

            if ((glb_CFOPID == '31020') && ((nCol == getColIndexByColKey(grid, 'BaseCalculo')) ||
			           (nCol == getColIndexByColKey(grid, 'ValorImposto')) || (nCol == getColIndexByColKey(grid, 'Aliquota'))))
                bRetVal = true;

            if ((grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'CFOPID')) == 14030) && (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'ImpostoID')) == 25) &&
			        (glb_Empresa == 255534) && ((nCol == getColIndexByColKey(grid, 'ValorImposto')) || (nCol == getColIndexByColKey(grid, 'Aliquota'))) && (glb_EstadoID == 21))
                bRetVal = true;

            if (glb_PedidoComplemento)
                if (nCol == getColIndexByColKey(grid, 'BaseCalculo') || nCol == getColIndexByColKey(grid, 'ValorImposto'))
                    bRetVal = true;
                else
                    bRetVal = false;

            if (glb_EhToamacaoServ)
                bRetVal = true;

        }
        else if (glb_BtnImpostosMode == 1) {
            if (((nCol == getColIndexByColKey(grid, 'BaseCalculo')) ||
			      (nCol == getColIndexByColKey(grid, 'ValorImposto'))) &&
				  (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'CFOPAIC')) != '0') &&
				   (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'ImpostoAIC')) != '0'))
                bRetVal = true;
        }
    }
    return bRetVal;
}

function updateImpostosComplementaresDataInServer() {
    var strPars = '';
    for (i = 2; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) == 1) {
            strPars += (strPars == '' ? '?' : '&') + 'nPedImpostoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PedImpostoID')));
            strPars += '&nValorImposto=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto')));
            strPars += '&nBaseCalculo=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'BaseCalculo')));
        }
    }

    if (strPars != '') {
        dsoGen02.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/updateimpostoscomplementaresdata.aspx' + strPars;
        dsoGen02.ondatasetcomplete = updateImpostosComplementaresDataInServer_DSC;
        dsoGen02.refresh();
    }
    else
        updateImpostosComplementaresDataInServer_DSC();
}

function updateImpostosComplementaresDataInServer_DSC() {
    lockControlsInModalWin(false);
    getDataInServer();
    return null;
}

function updateImpostosDataInServer() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    for (i = 1; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) == 0)
            continue;

        nBytesAcum = strPars.length;

        if ((nBytesAcum >= glb_nMaxStringSize) && (strPars != '')) {
            nBytesAcum = 0;
            glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
            strPars = '';
            nDataLen = 0;
        }

        nDataLen++;

        strPars += (strPars == '' ? '?' : '&') + 'nPedImpostoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PedImpostoID')));
        strPars += '&nValorImposto=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto')));
        strPars += '&nBaseCalculo=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'BaseCalculo')));
        strPars += '&nTipoImposto=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')));
        if (glb_PedidoComplemento || glb_EhImportacao || glb_EhToamacaoServ)
            strPars += '&nAliquota=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aliquota')));
        else
            strPars += '&nAliquota=' + escape(0);
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);


    if (glb_aSendDataToServer.length > 0)
        sendDataToServer();
    else
        updateImpostosDataInServer_DSC();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGen02.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/updateimpostosdata.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGen02.ondatasetcomplete = sendDataToServer_DSC;
            dsoGen02.refresh();
        }
        else {
            glb_localTimerRefresh = window.setInterval('updateImpostosDataInServer_DSC()', 30, 'JavaScript');
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Erro. Tente novamente.') == 0)
            return null;

        glb_localTimerRefresh = window.setInterval('updateImpostosDataInServer_DSC()', 30, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

function updateImpostosDataInServer_DSC() {
    if (glb_localTimerRefresh != null) {
        window.clearInterval(glb_localTimerRefresh);
        glb_localTimerRefresh = null;
    }

    lockControlsInModalWin(false);
    getDataInServer();
    return null;
}

function selTipoChange(ctrl) {
    // glb_BtnImpostosMode = (glb_BtnImpostosMode == 0 ? 1 : 0);

    if (selTipo.value == 2)
    {
        glb_BtnImpostosMode = 1;
        chkImpostoApuracao.style.visibility = 'hidden';
        lblImpostoApuracao.style.visibility = 'hidden';
    }
    else if (selTipo.value == 3)
    {
        glb_BtnImpostosMode = 3;

        chkImpostoApuracao.style.visibility = 'hidden';
        lblImpostoApuracao.style.visibility = 'hidden';

        fillGridDifAl();
    }
    else
    {
        glb_BtnImpostosMode = 0;
        chkImpostoApuracao.style.visibility = 'inherit';
        lblImpostoApuracao.style.visibility = 'inherit';
    }

    //setBtnImpostosMode();

    if (glb_BtnImpostosMode != 3)
        getDataInServer();
}

function selImpostoIDOnchange()
{
    if (glb_BtnImpostosMode == 3)
        fillGridDifAl();
    else
        getDataInServer();
}

function fillGridDifAl()
{
    var sWhereImposto = '';

    if (selImpostoID.value > 0)
        sWhereImposto = ' AND (b.ImpostoID = ' + selImpostoID.value + ')';

    setConnection(dsoDiferencialAliquotas);

    dsoDiferencialAliquotas.SQL = 'SELECT STR(a.Ordem, 3) AS Ordem, STR(a.ProdutoID, 7) AS ProdutoID, c.Conceito, b.ImpostoID, d.Imposto, b.AliquotaInterestadual AS AliquotaInter, ' +
                                    'b.AliquotaDestino AS AliquotaDestino, b.AliquotaFCPDestino AS AliquotaFCP, b.BaseCalculoDestino AS BaseDestino, ISNULL(b.ValorImpostoOrigem, 0) AS ValorOrigem, ' +
                                    'ISNULL(b.ValorImpostoDestino, 0) AS ValorDestino, ISNULL(b.ValorFCPDestino, 0) AS ValorFCP, ' +
                                    '((ISNULL(b.ValorImpostoOrigem, 0)) + ISNULL(b.ValorImpostoDestino, 0) + ISNULL(b.ValorFCPDestino, 0)) AS ValorTotal ' +
                                'FROM Pedidos_Itens a WITH(NOLOCK) ' +
                                    'INNER JOIN Pedidos_Itens_ImpostoInterestadual b WITH(NOLOCK) ON (b.PedItemID = a.PedItemID) ' +
                                    'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = a.ProdutoID) ' +
                                    'INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = b.ImpostoID) ' +
                                'WHERE (a.PedidoID = ' + glb_PedidoID + ')' + sWhereImposto;

    dsoDiferencialAliquotas.ondatasetcomplete = fillGridDifAl_DSC;
    dsoDiferencialAliquotas.Refresh();
}

function fillGridDifAl_DSC()
{
    glb_GridIsBuilding = true;

    fg.Redraw = 0;

    headerGrid(fg, ['Ordem',
					'ID',
					'Produto',
					'Imposto',
                    'Al�quota Destino',
                    'Al�quota FCP',
					'BC Destino',
					'Valor Origem',
                    'Valor Destino',
                    'Valor FCP',
                    'Valor Total'], []);

    fillGridMask(fg, dsoDiferencialAliquotas, ['Ordem',
								'ProdutoID',
								'Conceito',
								'Imposto',
                                'AliquotaDestino',
                                'AliquotaFCP',
								'BaseDestino',
								'ValorOrigem',
                                'ValorDestino',
                                'ValorFCP',
                                'ValorTotal'],
								['', '', '', '', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '999999999.99'],
								['', '', '', '', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00']);

    alignColsInGrid(fg, [0, 1, 4, 5, 6, 7, 8, 9, 10]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    // linha de Totais
    gridHasTotalLine(fg, 'Total', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'ValorOrigem'), '###,###,##0.00', 'S'],
                                                         [getColIndexByColKey(fg, 'ValorDestino'), '###,###,##0.00', 'S'],
                                                         [getColIndexByColKey(fg, 'ValorFCP'), '###,###,##0.00', 'S'],
                                                         [getColIndexByColKey(fg, 'ValorTotal'), '###,###,##0.00', 'S']]);

    // coloca foco no grid
    fg.focus();

    fg.Editable = false;

    btnOK.disabled = true;

    glb_GridIsBuilding = false;

}