/********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true) 
        sSQL = 'SELECT TOP 1 *, '+
               'CONVERT(VARCHAR, a.dtEmissao, ' + DATE_SQL_PARAM + ') as V_dtEmissao, ' +
               'CONVERT(VARCHAR, a.dtInicio, ' + DATE_SQL_PARAM + ') as V_dtInicio, ' +
               'CONVERT(VARCHAR, a.dtFim, ' + DATE_SQL_PARAM + ') as V_dtFim, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID = AlternativoID )>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM Lotes a ' +
               'WHERE ProprietarioID = ' + nID + ' ORDER BY a.LoteID DESC';
    
    else
        sSQL = 'SELECT * , ' +
               'CONVERT(VARCHAR, a.dtEmissao, ' + DATE_SQL_PARAM + ') as V_dtEmissao, ' +
               'CONVERT(VARCHAR, a.dtInicio, ' + DATE_SQL_PARAM + ') as V_dtInicio, ' +
               'CONVERT(VARCHAR, a.dtFim, ' + DATE_SQL_PARAM + ') as V_dtFim, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID = a.AlternativoID )>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM dbo.Lotes a ' +
               'WHERE   a.LoteID = ' + nID + ' ORDER BY a.LoteID DESC';

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso) {
    setConnection(dso);

    var sql;

    sql = 'SELECT *, ' +
          'CONVERT(VARCHAR, dtEmissao, ' + DATE_SQL_PARAM + ') as V_dtEmissao, ' +
          'CONVERT(VARCHAR, dtInicio, ' + DATE_SQL_PARAM + ') as V_dtInicio, ' +
          'CONVERT(VARCHAR, dtFim, ' + DATE_SQL_PARAM + ') as V_dtFim, ' +
          '0 AS Prop1, 0 AS Prop2 ' +
          'FROM Lotes WITH(NOLOCK) WHERE LoteID = 0';

    dso.SQL = sql;
}              

