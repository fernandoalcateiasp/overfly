/********************************************************************
modalProdutos.js

Library javascript para o modalProdutos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var modPesqRel_TimerHandler = null;
var glb_aEmpresaData = getCurrEmpresaData();

var dsoPesq = new CDatatransport("dsoPesq");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalProdutosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtPesquisa').disabled == false )
        txtPesquisa.focus();
    
    btnOK.disabled = true;
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Produto', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    
    // ajusta o divPesquisa
    with (divPesquisa.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        height = 40;
        
    }
    
    // txtPesquisa
    txtPesquisa.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (txtPesquisa.style)
    {
        left = 0;
        top = 16;
        width = (txtPesquisa.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }
    
    txtPesquisa.onkeypress = txtPesquisa_onKeyPress;
    
    // btnFindPesquisa
    btnFindPesquisa.disabled = true;
    with (btnFindPesquisa.style)
    {
        top = parseInt(txtPesquisa.style.top, 10);
        left = parseInt(txtPesquisa.style.left, 10) + parseInt(txtPesquisa.style.width, 10) + 2;
        width = 80;
        height = 24;
    }
    
    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }

    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.style.top, 10) + parseInt(divPesquisa.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    headerGrid(fg,['ID',
                   'Produto'], []);
    
    fg.Redraw = 2;
    
    lblPesquisa.innerText = 'Produto';
}

function txtPesquisa_onKeyPress()
{
    if ( event.keyCode == 13 )
    {
        lockControlsInModalWin(true);
        modPesqRel_TimerHandler = window.setInterval('btnFindPesquisa_onclick()', 30, 'JavaScript');
    }
}


function txtPesquisa_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    if ( modPesqRel_TimerHandler != null )
    {
        window.clearInterval(modPesqRel_TimerHandler);
        modPesqRel_TimerHandler = null;
    }
    
    txtPesquisa.value = trimStr(txtPesquisa.value);
    
    changeBtnState(txtPesquisa.value);

    if (btnFindPesquisa.disabled)
    {
        lockControlsInModalWin(false);
        return;
    }    
    
    startPesq(txtPesquisa.value);
}


function fg_ModPesqRelDblClick()
{
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqRelKeyPress(KeyAscii)
{
    if ( fg.Row < 1 )
        return true;
 
    if ( (KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false) )
        btn_onclick(btnOK);
 
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id)
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , new Array(
                      fg.TextMatrix(fg.Row, 0), fg.TextMatrix(fg.Row, 1)));
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

function startPesq(strPesquisa)
{
    lockControlsInModalWin(true);
    var Empresa;
    Empresa = getCurrEmpresaData();
    var nIdiomaEmpresaID = Empresa[8];
    var nEmpresaID = Empresa[0];

    var strPars = '?';
    var nIdiomaParaID = parseInt(Empresa[8], 10);
    var sFiltro = 'a.EstadoID = 2';
    var TipoResultado = '3';

    strPars += 'strToFind=' + escape(strPesquisa);
    strPars += '&nIdiomaParaID=' + escape(nIdiomaParaID);
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&sFiltro=' + escape(sFiltro);
    strPars += '&nTipoResultado=' + escape(TipoResultado);


    dsoPesq.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmLotes/serverside/pesqprodutos.aspx' + strPars;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
    
}

function dsopesq_DSC()
{
    startGridInterface(fg);
    fg.FrozenCols = 0;

    headerGrid(fg,['ID',
                   'Produto'], []);

    fillGridMask(fg, dsoPesq, ['ProdutoID',
                               'Descricao'],['','']);


    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
        
    }    
    else
        btnOK.disabled = true;


    
}
