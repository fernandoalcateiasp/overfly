using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.frmLotes.serverside
{
	public partial class pesqempresas : System.Web.UI.OverflyPage
	{
		private string strOperator = " LIKE ";

		private string toFind = "";

		public string strToFind
		{
			set { toFind = value != null ? value : ""; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "SELECT TOP 100 a.Nome AS fldName,a.PessoaID AS fldID,a.Fantasia AS Fantasia " +
                    "FROM Pessoas a WITH(NOLOCK) " +
                    "WHERE (a.Nome " + strOperator + "'%" + toFind + "%' " +
                    (strOperator.Equals(" LIKE ") ?
                    "OR a.Fantasia " + strOperator + "'%" + toFind + "%" + "') " :
                        ") "
                    ) +
                    "ORDER BY fldName "
                )
            );

		}
	}
}
