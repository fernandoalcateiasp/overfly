using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Overfly3.systemEx.serverside;
using WSData;
using java.lang;


namespace Overfly3.modcomercial.submodpedidos.frmLotes.serverside
{
    public partial class pesqprodutos : System.Web.UI.OverflyPage
    {
        //private string strOperator = " >= ";
        private static Integer Zero = Constants.INT_ZERO;
        private Integer EmpresaID;
        private Integer IdiomaID;
        private Integer TipoResultado;
        private string toFind = "";
        private string Filtro = "";
        string sSQL = "";

        public string strToFind
        {
            set { toFind = value != null ? value : ""; }
        }
        public string sFiltro
        {
            set { Filtro = value != null ? value : ""; }
        }
        public Integer nIdiomaParaID
        {
            set { IdiomaID = value != null ? value : Zero; }
        }
        public Integer nTipoResultado
        {
            set { TipoResultado = value != null ? value : Zero; }
        }
        public Integer nEmpresaID
        {
            set { EmpresaID = value != null ? value : Zero; }
        }

        protected DataSet GeraResultado()
        {
            System.Data.DataSet resultDataSet;

            sSQL = "DECLARE @Produtos TABLE (ProdutoID INT, Descricao VARCHAR (200))" +
                    " INSERT INTO @Produtos ( ProdutoID,Descricao ) " +
                    "EXEC sp_Produto_Busca '" + toFind + "' , " + IdiomaID + " , " + "NULL" + " , '" + Filtro + " ', " + TipoResultado +
                    " SELECT ProdutoID , Descricao " +
                    " FROM @Produtos a " +
                    " INNER JOIN dbo.RelacoesPesCon b WITH (NOLOCK) ON a.ProdutoID = b.ObjetoID AND b.SujeitoID = " + EmpresaID +
                    " WHERE b.EstadoID NOT IN (4,5)"; 

            resultDataSet = DataInterfaceObj.getRemoteData(sSQL);
            return resultDataSet;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(GeraResultado());
        }
    }
}
