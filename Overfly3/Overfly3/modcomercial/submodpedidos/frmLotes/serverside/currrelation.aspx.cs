using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.submodpedidos.frmLotes.serverside
{
	public partial class currrelation : System.Web.UI.OverflyPage
	{
		private Integer produtoId;
		private Integer empresaId;

		protected Integer nProdutoID
		{
			set { produtoId = value; }
		}
		protected Integer nEmpresaID
		{
			set { empresaId = value; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"SELECT TOP 1 RelacaoID AS fldID " +
					 "FROM RelacoesPesCon WITH(NOLOCK) " +
					 "WHERE TipoRelacaoID=61 " +
						 "AND SujeitoID = " + empresaId.ToString() +
						 "AND ObjetoID = " + produtoId.ToString()
				)
			);
		}
	}
}
