/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)       
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    

    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.LoteID, a.ProprietarioID,a.AlternativoID,a.Observacoes FROM Lotes a WHERE '+
        sFiltro + 'a.LoteID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                    'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24161) // Lotes_Produtos
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM    dbo.Lotes_Produtos a WITH ( NOLOCK ) ' +
                  'WHERE ' + sFiltro + ' a.LoteID = ' + idToFind;
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24162) // Lotes_Pedidos_Itens
    {
        dso.SQL = 'SELECT b.PedidoID, f.RecursoAbreviado AS EstadoPedido, d.PessoaID, d.Fantasia, ' +
                  ' (CONVERT (VARCHAR, b.CFOPID)) + \' - \' + e.Operacao as CFOP, ' +
                  ' b.ProdutoID, g.RecursoAbreviado AS EstadoProduto, h.Conceito as Produto, b.Quantidade, a.LotPedItemID' +   
                  ' FROM dbo.Lotes_Itens a WITH(NOLOCK)' +
                  ' INNER JOIN dbo.Pedidos_Itens b WITH(NOLOCK) ON a.PedItemID = b.PedItemID ' +
                  ' INNER JOIN dbo.Conceitos h WITH(NOLOCK) ON b.ProdutoID = h.ConceitoID ' +
                  ' INNER JOIN dbo.Pedidos c WITH ( NOLOCK ) ON b.PedidoID = c.PedidoID ' +
                  ' INNER JOIN dbo.Pessoas d WITH ( NOLOCK ) ON c.PessoaID = d.PessoaID ' +
                  ' LEFT OUTER JOIN dbo.Operacoes e WITH ( NOLOCK ) ON b.CFOPID = e.OperacaoID ' +
                  ' INNER JOIN dbo.Recursos f WITH (NOLOCK)   ON c.EstadoID = f.RecursoID ' +
				  ' INNER JOIN dbo.Recursos g WITH (NOLOCK)   ON B.EstadoID = g.RecursoID ' +
                  ' WHERE ' + sFiltro + ' a.LoteID = ' + idToFind + ' ' +
                  ' ORDER BY b.PedidoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24163) // Lotes_Pessoas
    {
        dso.SQL = 'SELECT a.LotPessoaID, a.PessoaID as PessoaID, A.LOTEID as LoteID' +
                  ' FROM LOTES_PESSOAS A WITH(NOLOCK)' +
                  ' WHERE ' + sFiltro + ' a.LoteID = ' + idToFind;
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID) 
{
    setConnection(dso);
    var nUsuarioID = getCurrUserID();
    // LOG
    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a, Recursos b ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a, Pessoas b ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a, TiposAuxiliares_Itens b ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a, Recursos b ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }

    }
    else if (pastaID == 24163) {
        if (dso.id == 'dso05GridLkp') {
            dso.SQL = 'SELECT a.PessoaID, a.Fantasia ' +
                      'FROM Pessoas a WITH(NOLOCK)'+
                      'INNER JOIN Lotes_Pessoas b WITH(NOLOCK) ON b.PessoaID = a.PessoaID' +
                      ' WHERE b.LoteID = ' + nRegistroID;
        }
    }
    else if (pastaID == 24161) 
    {
        if (dso.id == 'dso06GridLkp') 
    {
        dso.SQL = 'SELECT DISTINCT c.LotProdutoID,c.ProdutoID, e.RecursoAbreviado AS E, d.Conceito AS Conceito, ' +
                  'ISNULL(dbo.fn_Lotes_ProdutosQuantidades(a.LoteID, c.ProdutoID, 7, NULL),0) AS Pedido, c.Quantidade, ' +
                  'ISNULL(dbo.fn_Lotes_ProdutosQuantidades(a.LoteID, c.ProdutoID, 11, NULL), 0) AS Comprado,' +
                  'ISNULL(dbo.fn_Lotes_ProdutosQuantidades(a.LoteID, c.ProdutoID, 3, NULL), 0) AS AComprar,' +
                  'ISNULL(dbo.fn_Lotes_ProdutosQuantidades(a.LoteID, c.ProdutoID, 4, NULL), 0) AS Vendido, ' +
                  'ISNULL(dbo.fn_Lotes_ProdutosQuantidades(a.LoteID, c.ProdutoID, 5, NULL), 0) AS AVender  ' +
                  'FROM    dbo.Lotes a WITH ( NOLOCK ) ' +
                  'INNER JOIN dbo.Lotes_Produtos c WITH ( NOLOCK ) ON a.LoteID = c.LoteID ' +
                  'INNER JOIN dbo.Conceitos d WITH ( NOLOCK ) ON c.ProdutoID = d.ConceitoID ' +
                  'INNER JOIN dbo.Recursos e WITH ( NOLOCK ) ON d.EstadoID = e.RecursoID ' +
                  'WHERE a.LoteID = ' + nRegistroID;
    }
    }
}
/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

    cleanupSelInControlBar('inf', 1);

    vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG  

    vPastaName = window.top.getPastaName(24161);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24161); //Lotes Produtos

    vPastaName = window.top.getPastaName(24162);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24162); //Lotes itens
        
    vPastaName = window.top.getPastaName(24163);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24163); //Lotes Pessoas
 	
    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;
    
    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 24161) // Lotes Produtos
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [], ['LoteID', registroID]);
        }
        else if (folderID == 24162) 
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [], ['LoteID', registroID]);
        }
        else if (folderID == 24163) {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [], ['LoteID', registroID], [2]);
        }
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 24161) // LotesProdutos
    {

        headerGrid(fg, ['ProdutoID',
                        'E',
                        'Produto',
                        'Pedidos',
                        'Quant',
                        'Comprado',
                        'A Comprar',
                        'Vendido',
                        'A Vender',
                        'LotProdutoID'], [9]);

        fillGridMask(fg, currDSO, ['ProdutoID*', '^LotProdutoID^dso06GridLkp^LotProdutoID^E*', '^LotProdutoID^dso06GridLkp^LotProdutoID^Conceito*',
                                    '^LotProdutoID^dso06GridLkp^LotProdutoID^Pedido*', 'Quantidade',
                                    '^LotProdutoID^dso06GridLkp^LotProdutoID^Comprado*', '^LotProdutoID^dso06GridLkp^LotProdutoID^AComprar*',
                                    '^LotProdutoID^dso06GridLkp^LotProdutoID^Vendido*', '^LotProdutoID^dso06GridLkp^LotProdutoID^AVender*',
                                    'LotProdutoID'],
                                   ['', '', '', '', '', '', '', '', '', ''],
                                   ['', '', '', '', '', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
      
    }
    else if (folderID == 24162) // Lotes
    {
        headerGrid(fg, ['PedidoID',
                        'E',
                        'PessoaID',
                        'Pessoa',
                        'CFOP',
                        'ID',
                        'E',
                        'Produto',
                        'Quant',
                        'LotPedItemID'], [9]);

        fillGridMask(fg, currDSO, ['PedidoID', 'EstadoPedido', 'PessoaID', 'Fantasia', 'CFOP', 'ProdutoID', 'EstadoProduto', 'Produto', 'Quantidade', 'LotPedItemID'],
                                   ['', '', '', '', '', '', '', '', '',  '']);

        // Merge de Colunas
        //fg.MergeCells = 4;
        //fg.MergeCol(-1) = true;
      
    }
    else if (folderID == 24163) // Lotes_Pessoas
    {
        headerGrid(fg, ['Pessoa',
                        'LoteID',
                        'PessoaID',
                        'LotPessoaID'], [1,2,3]);

        fillGridMask(fg, currDSO, ['^PessoaID^dso05GridLkp^PessoaID^Fantasia', 'LoteID','PessoaID', 'LotPessoaID'],
                                   ['', '', '', ''],
                                   ['', '', '', '']);

        // Merge de Colunas
        //fg.MergeCells = 4;
        //fg.MergeCol(-1) = true;

    }

}
