/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form notasfiscais
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************



var dsoListData01 = new CDatatransport('dsoListData01');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoPropsPL = new CDatatransport('dsoPropsPL');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'DF', 'Nota', 'Data Nota', 'CFOPs',
                                 'Pedido', 'Vendedor', 'Total Nota', 'Pessoa', 'RPS');
                                 
    //@@ Formatacao das colunas do grid de pesquisa, definicao opcional
    // padrao do framework = null se variavel nao for definida aqui
    var dTFormat = '';
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    glb_aCOLPESQFORMAT = new Array('', '', '', '', dTFormat,
                                   '', '', '','###,###,###,###.00', '', '');

    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var contexto = getCmbCurrDataInControlBar('sup', 1);

    if (contexto[1] == 5121)
    {
        if (empresaData[1] == 130)
            setupEspecBtnsControlBar('sup', 'HHHDDH');
    }
    else
        setupEspecBtnsControlBar('sup', 'HHHDDD');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    // Usuario clicou botao documentos
    if (btnClicked == 1) {
        if (fg.Rows > 1) {
            __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
        }
        else {
            window.top.overflyGen.Alert('Selecione um registro.');
        }
    }
    else if ( controlBar == 'SUP' )
    {
        // usuario clicou botao imprimir
        if (btnClicked == 2) {
            openModalPrint();
        }

        else if (btnClicked == 6)
        {
            openmodalManifesto();
        }
    }    
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Modal de impressao
    if (idElement.toUpperCase() == 'MODALMANIFESTOHTML') {
        if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
        // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    // por questoes esteticas, coloca foco no grid de pesquisa
    if ( fg.disabled == false )
        fg.focus();
    
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/notasfiscais/modalpages/modalprint.asp' + strPars;
    
    showModalWin(htmlPath, new Array(346,200));
}

/* MODAL DE MANIFESTO */
function openmodalManifesto()
{
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];
    var strPars = new String();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)

    strPars = '?sCaller=' + escape('PL');
    strPars += '&nEmpresaID = ' + escape(nCurrEmpresa);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/notasfiscais/modalpages/modalManifesto.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 600));
}
