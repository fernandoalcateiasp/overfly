<%@  language="VBSCRIPT" enablesessionstate="False" %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
	strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalmanifestoHtml" name="modalmanifestoHtml">

<head>

    <title></title>

    <%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/notasfiscais/modalpages/modalManifesto.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
 
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
            
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/notasfiscais/modalpages/modalManifesto.js" & Chr(34) & "></script>" & vbCrLf
    %>

    <%
'Script de variaveis globais
Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim rsData, rsData1, strSQL

Dim i, sCaller, nEmpresaID

sCaller = ""
nEmpresaID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"

Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
    %>
    <script id="wndJSProc" language="javascript">
<!--

    //-->
    </script>

    <script language="javascript" for="fg" event="EnterCell">
<!--
    js_fg_EnterCell(fg);
    //-->
    </script>

    <script language="javascript" for="fg" event="KeyPress">
<!--
    fg_ModPesqnfeKeyPress(arguments[0]);
    //-->
    </script>

    <script language="javascript" for="fg" event="DblClick">
<!--
    fg_ModPesqnfeDblClick(fg, fg.Row, fg.Col);
    //-->
    </script>

    <script language="javascript" for="fg" event="BeforeRowColChange">
        //<!--
        js_fg_modalManifesto_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
        //-->
    </script>

    <script language="javascript" for="fg" event="AfterRowColChange">
<!--
    fg_modalManifestoAfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    //-->
    </script>

</head>

<body id="modalManifestoBody" name="modalManifestoBody" language="javascript" onload="return window_onload()">

    <div id="divFG" name="divFG" class="divGeneral">
        <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" viewastext>
        </object>
        <!--
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
        //-->
    </div>
    <div id="divItens" name="divItens" class="divGeneral">
        <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="Itens" name="Itens" viewastext>
        </object>
        <!--
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
        //-->
    </div>
    <div id="divEventos" name="divEventos" class="divGeneral">
        <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="Eventos" name="Eventos" viewastext>
        </object>
        <!--
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
        //-->
    </div>
    <div id="divPedidos" name="divPedidos" class="divGeneral">
        <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="Pedidos" name="Pedidos" viewastext>
        </object>
    </div>
    <p id="lblEmpresa" name="lblEmpresa" class="lblGeneral">Empresa</p>
    <select id="selEmpresa" name="selEmpresa" class="fldGeneral" onchange="return cmbOnChange(this)">
    </select>

    <p id="lblFornecedorID" name="lblFornecedorID" class="lblGeneral">Fornecedor</p>
    <select id="selFornecedorID" name="selFornecedorID" class="fldGeneral" onchange="return cmbOnChange(this)">
    </select>

    <p id="lblGP" name="lblGP" class="lblGeneral">GP</p>
    <select id="selGP" name="selGP" class="fldGeneral" onchange="return cmbOnChange(this)">
    </select>

    <p id="lblNF" name="lblNF" class="lblGeneral">NF</p>
    <input type="text" id="txtNF" name="txtNF" class="fldGeneral">

    <p id="lblEstado" name="lblEstado" class="lblGeneral">Estado</p>
    <select id="selEstado" name="selEstado" class="fldGeneral" onchange="return cmbOnChange(this)">
    </select>

    <p id="lblTransacao" name="lblTransacao" class="lblGeneral">Transação</p>
    <select id="selTransacao" name="selTransacao" class="fldGeneral" onchange="return cmbOnChange(this)">
    </select>

    <p id="lbldtInicio" name="lbldtInicio" class="lblGeneral">Inicio</p>
    <input type="text" id="txtdtInicio" name="txtdtInicio" class="fldGeneral">

    <p id="lbldtFim" name="lbldtFim" class="lblGeneral">Fim</p>
    <input type="text" id="txtdtFim" name="txtdtFim" class="fldGeneral">

    <p id="lblAutorizacao" name="lblAutorizacao" class="lblGeneral">Status</p>
    <select id="selAutorizacao" name="selAutorizacao" class="fldGeneral" onchange="return cmbOnChange(this)">
        <option value="1" selected></option>
        <option value="2">Aguardando</option>
        <option value="3">Autorizado</option>
        <option value="4">Rejeitado</option>
    </select>

    <p id="lblDetalhe" name="lblDetalhe" class="lblGeneral">Detalhe</p>
    <select id="selDetalhe" name="selDetalhe" class="fldGeneral" onchange="return cmbOnChange(this)">
        <option value="0" selected></option>
        <option value="1">Itens</option>
        <option value="2">Eventos</option>
    </select>
    <p id="lblObrigatorio" name="lblObrigatorio" class="lblGeneral">Obrig</p>
    <input type="checkbox" id="chkObrigatorio" name="chkObrigatorio" title="Listar somente manifestos obrigatórios ?" class="fldGeneral" checked="checked"></input>
    <p id="lblSP" name="lblSP" class="lblGeneral">SP</p>
    <input type="checkbox" id="chkSP" name="chkSP" title="Listar somente manifestos sem pedidos relacionados ?" class="fldGeneral"></input>

    <p id="lblGridItens" name="lblGridItens" class="lblGeneral">Itens do XML</p>
    <p id="lblGridPedidos" name="lblGridPedidos" class="lblGeneral">Itens do Pedido</p>
    <p id="lblGridEventos" name="lblGridEventos" class="lblGeneral">Eventos do Manifesto</p>

    <input type="button" id="btnManifestar" name="btnManifestar" value="Manifestar" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnListar" name="btnListar" value="Listar" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnExcel" name="btnExcel" value="Excel" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnXML" name="btnXML" value="XML" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnGravar" name="btnGravar" value="Gravar" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnOK" name="btnOK" value="OK" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" language="javascript" onclick="return btn_onclick(this)" class="btns">

    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>

    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>


</body>
</html>
