/*
modalManifesto.js

Library javascript para o modalManifesto.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var modPesqnfe_Timer = null;
var gblDepositoID = 0;
var glb_CounterCmbsStatics = 0;
var glb_nUsuarioID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
var glb_aEmpresaData = getCurrEmpresaData();
var glb_OcorrenciasTimerInt = null;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_GestorID = 0;
var glb_CargoID = 0;
var glb_EhSuperior = 0;
var glb_sAlert = '';
var dsoCombos = new CDatatransport('dsoCombos');
var dsoFornecedor = new CDatatransport('dsoFornecedor');
var dsoGP = new CDatatransport('dsoGP');
var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');
var dsoPedidos = new CDatatransport('dsoPedidos');
var dsoUsuario = new CDatatransport('dsoUsuario');
var dsoItens = new CDatatransport('dsoItens');
var dsoGridEventos = new CDatatransport('dsoGridEventos');
var dsoComboManifesto = new CDatatransport('dsoComboManifesto');
var dsoSaveDatas = new CDatatransport('dsoSaveDatas');
var dsoOpenXML = new CDatatransport('dsoOpenXML');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES

NOTA: Ver tambem o arquivo commonnfe.js

FUNCOES GERAIS:
window_onload()
setupPage()
txtPesquisa_onKeyPress()
txtPesquisa_ondigit(ctl)
btnFindPesquisa_onclick(ctl)
btn_onclick(ctl)
startPesq(strPesquisa)
dataToReturn()

Eventos de grid particulares desta janela:
fg_ModPesqnfeDblClick()
fg_ModPesqnfeKeyPress(KeyAscii)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // ajusta o body do html

    with (modalManifestoBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // Carrega os Dados do Colaborador
    dadosDoUsuario();

    // configuracao inicial do html
    setupPage();

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();

    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Manifesto de Notas Fiscais');
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Manifesto', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;

    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 50;
    modalFrame.style.left = 0;

    btnOK.style.visibility = 'hidden';
    btnCanc.value = 'Cancelar';
    btnCanc.style.visibility = 'hidden';
    btnListar.disabled = false;
    btnManifestar.disabled = false;
    btnXML.disabled = false;
    btnGravar.disabled = false;

    adjustElementsInForm([
        ['lblEmpresa', 'selEmpresa', 18, 1, 0, 20],
        ['lblFornecedorID', 'selFornecedorID', 15, 1],
        ['lblGP', 'selGP', 17, 1],
        ['lblNF', 'txtNF', 10, 1],
        ['lblEstado', 'selEstado', 18, 1],
        ['lblTransacao', 'selTransacao', 18, 1],
        ['lbldtInicio', 'txtdtInicio', 10, 2],
        ['lbldtFim', 'txtdtFim', 9, 2],
        ['lblAutorizacao', 'selAutorizacao', 12, 2],
        ['lblDetalhe', 'selDetalhe', 9, 2],
        ['lblObrigatorio', 'chkObrigatorio', 3, 2],
        ['lblSP', 'chkSP', 3, 2],
		['btnListar', 'btn', 70, 2, 25],
        ['btnExcel', 'btn', 70, 2, 10],
		['btnManifestar', 'btn', 70, 2, 10],
        ['btnXML', 'btn', 70, 2, 10],
        ['btnGravar', 'btn', 70, 2, 10]], null, null, true);

    //selImpressoras.onchange = selImpressoras_onchange;
    //selDepositoID.onchange = selDepositoID_onchange;
    //txtFiltro.onkeypress = txtFiltro_onKeyPress;

    with (divFG.style) {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = (ELEM_GAP * 2) - 10;
        top = ELEM_GAP + parseInt(btnManifestar.style.top, 10) + parseInt(btnManifestar.style.height, 10);
        width = modWidth - 25;
        height = 460;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10) - 2;
        height = parseInt(divFG.style.height, 10);
    }

    // ajusta o Itens
    with (divItens.style) {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = (ELEM_GAP * 2) - 10;
        //top = (ELEM_GAP * 3) + 10;
        top = ELEM_GAP + parseInt(btnManifestar.style.top, 10) + parseInt(btnManifestar.style.height, 10) + 275;
        width = 475;
        height = 190;
        visibility = 'hidden';
    }
    with (Itens.style) {
        left = 0;
        top = 0;
        width = parseInt(divItens.style.width, 10) - 2;
        height = parseInt(divItens.style.height, 10);
    }
    // ajusta o Eventos
    with (divEventos.style) {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = (ELEM_GAP * 2) - 10;
        top = ELEM_GAP + parseInt(btnManifestar.style.top, 10) + parseInt(btnManifestar.style.height, 10) + 275;
        width = modWidth - 25;
        height = 190;
        visibility = 'hidden';
    }
    with (Eventos.style) {
        left = 0;
        top = 0;
        width = parseInt(divEventos.style.width, 10) - 2;
        height = parseInt(divEventos.style.height, 10);
    }

    // ajusta o Pedidos
    with (divPedidos.style) {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = parseInt(divItens.style.left, 10) + 500;
        //top = (ELEM_GAP * 3) + 10;
        top = ELEM_GAP + parseInt(btnManifestar.style.top, 10) + parseInt(btnManifestar.style.height, 10) + 275;
        width = 475;
        height = 190;
        visibility = 'hidden';
    }
    with (Pedidos.style) {
        left = 0;
        top = 0;
        width = parseInt(divPedidos.style.width, 10) - 2;
        height = parseInt(divPedidos.style.height, 10);
    }

    lblGridItens.style.visibility = 'hidden';
    lblGridItens.style.top = parseInt(divItens.currentStyle.top, 10) - 15;
    lblGridItens.style.left = parseInt(divItens.currentStyle.left, 10);

    lblGridPedidos.style.visibility = 'hidden';
    lblGridPedidos.style.top = parseInt(divPedidos.currentStyle.top, 10) - 15;
    lblGridPedidos.style.left = parseInt(divPedidos.currentStyle.left, 10);

    lblGridEventos.style.visibility = 'hidden';
    lblGridEventos.style.top = parseInt(divEventos.currentStyle.top, 10) - 15;
    lblGridEventos.style.left = parseInt(divEventos.currentStyle.left, 10);
    fillCombo();
    startGridInterface(fg);
    startGridInterface(Itens);
    startGridInterface(Eventos);
    startGridInterface(Pedidos);
    //  fillGrid();

    txtdtInicio.maxLength = 10;
    txtdtFim.maxLength = 10;
    btnManifestar.disabled = true;


    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.style.width, 10) * 18;
    fg.Redraw = 2;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqnfeDblClick(fg, Row, col) {
    if (selDetalhe.value != 0) {

        lockControlsInModalWin(true);

        var ManifestoID = 0;
        var Pedido = 0;

        ManifestoID = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ManifestoID'));
        Pedido = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'Pedido*'));

        Itens.Rows = 1;
        Pedidos.Rows = 1;
        Eventos.Rows = 1;
        glb_sAlert = '';
        fillGridItens(ManifestoID);
        fillGridPedidos(Pedido);
        fillGrid_Eventos(ManifestoID);
    }
}

function fg_modalManifestoAfterRowColChange(fg, oldRow, oldCol, newRow, newCol) {

    if ((newCol > 1) || (newRow > 1)) {
        Pedidos.Rows = 1;
        Eventos.Rows = 1;
        Itens.Rows = 1;
    }
}
/********************************************************************
Clique botao OK ou Cancela 
********************************************************************/
function btn_onclick(ctl) {
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
        // 3. O usuario clicou no botao Imprimir
    else if (ctl.id == 'btnListar') {
        comboManifesto();
        fillGrid(0);
    }
    else if (ctl.id == 'btnManifestar') {
        //window.top.overflyGen.Alert(selImpressoras.value);
        Manifestar();
    }
    else if (ctl.id == 'btnExcel') {
        fillGrid(1);
    }
    else if (ctl.id == 'btnXML') {
        
        openXML();
    }
    else if (ctl.id == 'btnGravar') {

        var i, j = 0;
        var aDataToSave = new Array();

        for (i = 2; i < fg.Rows; i++) {

            ncolManifestoID = getColIndexByColKey(fg, 'ManifestoID');
            ncolObservacoes = getColIndexByColKey(fg, 'Observacoes');

          //  if (fg.TextMatrix(i, ncolObservacoes) != '') {

                aDataToSave[j] = new Array(fg.ValueMatrix(i, ncolManifestoID),
                                           fg.TextMatrix(i, ncolObservacoes));
            //}

            j++;
        }

        /*
        if (aDataToSave.length == 0) {
            if (window.top.overflyGen.Alert('Nenhuma observa��o foi alterada') == 0)
                return null;

            lockControlsInModalWin(false);
            return null;
        }
        */
        saveDatas(aDataToSave, aDataToSave.length, aDataToSave[0].length);
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqnfeKeyPress(KeyAscii) {
    ;
}

/*******************************************************************
Imprimi notas fiscais
*******************************************************************/
function btnListar_onclick(Row) {
    comboManifesto();
    fillGrid(0);
}

function txtFiltro_onKeyPress() {
    if (event.keyCode == 13) {
        lockControlsInModalWin(true);
        NotaFiscal();
    }
}

function cmbOnChange(cmb) {
    if ((cmb.id).toUpperCase() == 'SELDETALHE') {
        adjustGrid(cmb);
    }
    if ((cmb.id).toUpperCase() == 'SELESTADO') {
        AcertaGrids();
        comboManifesto();
        fillGrid(0);
    }
    if ((cmb.id).toUpperCase() == 'SELAUTORIZACAO') {
        AcertaGrids();
        comboManifesto();
        fillGrid(0);
    }
    if ((cmb.id).toUpperCase() == 'SELEMPRESA') {
        fillComboGP();
        AcertaGrids();
        fillComboFornecedor();
    }
    if ((cmb.id).toUpperCase() == 'SELFORNECEDORID') {
        AcertaGrids();
    }
    if ((cmb.id).toUpperCase() == 'SELGP') {
        AcertaGrids();
    }
}

function adjustGrid(cmb) {
    var opcao = '';
    var combo = ((cmb.id).toUpperCase());

    if (combo == 'SELDETALHE') {
        opcao = cmb.value;

        // sem Div
        if (opcao == 0) {

            divEventos.style.visibility = 'hidden';
            divItens.style.visibility = 'hidden';
            divPedidos.style.visibility = 'hidden';

            lblGridItens.style.visibility = 'hidden';
            lblGridPedidos.style.visibility = 'hidden';
            lblGridEventos.style.visibility = 'hidden';

            with (divFG.style) {
                height = 460;
            }

            with (fg.style) {
                left = 0;
                top = 0;
                width = parseInt(divFG.style.width, 10);
                height = parseInt(divFG.style.height, 10);
            }
        }
            // Div Itens
        else if (opcao == 1) {

            divEventos.style.visibility = 'hidden';
            lblGridEventos.style.visibility = 'hidden';
            divItens.style.visibility = 'inherit';
            lblGridItens.style.visibility = 'inherit';
            lblGridPedidos.style.visibility = 'inherit';
            divPedidos.style.visibility = 'inherit';

            with (divFG.style) {
                height = 250;
            }
            with (fg.style) {
                left = 0;
                top = 0;
                width = parseInt(divFG.style.width, 10);
                height = parseInt(divFG.style.height, 10);
            }
        }
            // Div Eventos
        else if (opcao == 2) {

            divEventos.style.visibility = 'inherit';
            divItens.style.visibility = 'hidden';
            divPedidos.style.visibility = 'hidden';

            lblGridItens.style.visibility = 'hidden';
            lblGridPedidos.style.visibility = 'hidden';
            lblGridEventos.style.visibility = 'inherit';

            with (divFG.style) {
                height = 250;
            }
            with (fg.style) {
                left = 0;
                top = 0;
                width = parseInt(divFG.style.width, 10);
                height = parseInt(divFG.style.height, 10);
            }
        }
    }
}


function Manifestar() {
    saveDataInGrid();
}

function fillGrid(lista_excel) {
    if (lista_excel == 0) {
        if (glb_OcorrenciasTimerInt != null) {
            window.clearInterval(glb_OcorrenciasTimerInt);
            glb_OcorrenciasTimerInt = null;
        }
    }
    var sSql = '';
    var empresaID = 'NULL';
    var FornecedorID = 'NULL';
    var GerenteProdutoID = 'NULL';
    var NotaFiscal = 'NULL';
    var EstadoID = 'NULL';
    var dtInicio = '';
    var dtFim = '';
    var AutorizacaoID = 'NULL';
    var Detalhe = 'NULL';
    var Transacao = 'NULL';
    var Obrigatorio = "0";
    var SP = "0";

    //Trava a interface
    lockControlsInModalWin(true);

    if (lista_excel == 0)
        fg.Rows = 1;

    if (selEmpresa.value != 0)
        empresaID = selEmpresa.value;
    else if (!(selEstado.value != 0)) {
        window.top.overflyGen.Alert('Favor selecionar ao menos uma empresa.');

        //setupPage();
        lockControlsInModalWin(false);
        return null;
    }

    if (selFornecedorID.value != 0)
        FornecedorID = selFornecedorID.value;

    if (selGP.value != 0)
        GerenteProdutoID = selGP.value;

    if (!(trimStr(txtdtInicio.value) == ''))
        dtInicio = dateFormatToSearch(txtdtInicio.value);

    if (!(trimStr(txtdtFim.value) == ''))
        dtFim = dateFormatToSearch(txtdtFim.value);

    if (!(trimStr(txtNF.value) == ''))
        NotaFiscal = txtNF.value;

    if (selEstado.value != 0)
        EstadoID = selEstado.value;

    if (selAutorizacao.value != 1)
        AutorizacaoID = selAutorizacao.value;

    if (selDetalhe.value != 0)
        Detalhe = selDetalhe.value;

    if (selTransacao.value != 0)
        Transacao = selTransacao.value;

    if (chkObrigatorio.checked)
        Obrigatorio = '1';
    else if (!chkObrigatorio.checked)
        Obrigatorio = '0';

    if (chkSP.checked)
        SP = '1';
    else if (!chkSP.checked)
        SP = '0';

    var geraExcelID = 1;
    var formato = 2; //Excel
    var sLinguaLogada = getDicCurrLang();

    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");

    var strParameters = "RelatorioID=" + geraExcelID + "&Formato=" + formato + "&glb_sEmpresaFantasia=" + glb_aEmpresaData[3] + "&glb_nUsuarioID=" + glb_nUsuarioID +
                        "&empresaID=" + empresaID + "&FornecedorID=" + FornecedorID + "&GerenteProdutoID=" + GerenteProdutoID + "&NotaFiscal=" + NotaFiscal +
                        "&EstadoID=" + EstadoID + "&dtInicio=" + dtInicio + "&dtFim=" + dtFim + "&AutorizacaoID=" + AutorizacaoID + "&lista_excel=" + lista_excel +
                        "&Transacao=" + Transacao + "&Obrigatorio=" + (Obrigatorio == 0 ? 'false' : 'true') + "&SP=" + (SP == 0 ? 'false' : 'true') + "&sLinguaLogada=" + sLinguaLogada;
    if (lista_excel == 1) {
        frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/notasfiscais/serverside/ReportsGrid_manifesto.aspx?' + strParameters;
        lockControlsInModalWin(false);
    }
    else if (lista_excel == 0) {
        dsoGrid.URL = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/notasfiscais/serverside/ReportsGrid_manifesto.aspx?' + strParameters;
        dsoGrid.ondatasetcomplete = fillGrid_DSC;

        try {
            dsoGrid.refresh();
        }
        catch (e) {
            if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
                return null;
            lockControlsInModalWin(false);
            window.focus();
            selEmpresa.focus();
        }
    }
}
function fillGrid_DSC() {
    var EstadoID = selEstado.value;
    aHidenCols = new Array();
    aHidenCols = [];
    var i = 0;
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    if ((dsoGrid.recordset.BOF) || (dsoGrid.recordset.EOF)) {
        window.top.overflyGen.Alert("N�o existem Manifestos");
        fg.Rows = 1;
        lockControlsInModalWin(false);
        return null;
    }

    if ((EstadoID == 65) || (EstadoID == 111))
        aHidenCols = [19, 20];
    else if (((glb_CargoID == 615) || (glb_CargoID == 619) || (glb_CargoID == 693)) && ((EstadoID == 113) || (EstadoID == 114))) {
        aHidenCols = [19, 20];
    }
    else
        aHidenCols = [13, 14, 19, 20];

    dsoGrid.recordset.MoveFirst();
    startGridInterface(fg);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;
    fg.Editable = false;
    fg.FontSize = '8';
    fg.FrozenCols = 0;

    //Inicio = 0
    headerGrid(fg, ['Empresa',
	               'Fornecedor',
	               'GP',
                   'Estado',
                   'Dias',
                   'Autoriza��o',
                   'NF',
                   'Pedido',
                   'Transa��o',
                   'XML',
                   'Itens',
                   'Vl Total',
                   'Data NF',
                   'Manifesto',
                   'Motivo',
                   'Data Manifesto',
                   'Usu�rio',
                   'Mensagem Sefaz',
                   'Observac�es',
                   'ManifestoID',
                   'Cor'],
                   aHidenCols);

    glb_aCelHint = [[0, 5, 'Prazo para se manifestar']];


    fillGridMask(fg, dsoGrid, ['Empresa*',
	                                'Fornecedor*',
	                                'GP*',
                                    'Estado*',
                                    'Dias*',
                                    'Status_Manifesto*',
                                    'NotaFiscal*',
                                    'Pedido*',
                                    'Transacao*',
                                    'TemXML*',
                                    'Itens*',
                                    'ValorTotal*',
                                    'dtEmissao*',
                                    'Manifesto',
                                    'Motivo',
                                    'dtManifesto*',
                                    'UsuarioID*',
                                    'MensagemSefaz*',
                                    'Observacoes',
                                    'ManifestoID',
                                    'Cor*'],
				  				    ['', '', '', '', '', '', '', '', '', '', '', '999999999.99', '99/99/9999', '', '', '99/99/9999', '', '', '', '', ''],
								    ['', '', '', '', '', '', '', '', '', '', '', '###,###,##0.00', dTFormat, '', '', dTFormat, '', '', '', '','']);


    // Linha de Totais
    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[6, '######', 'C'],
                                                           [11, '###,###,###,###.00', 'S']], null);





    if ((EstadoID == 65) || (EstadoID == 111)) {
        insertcomboData(fg, getColIndexByColKey(fg, 'Manifesto'), dsoComboManifesto, 'Estado', 'Manifesto');
    }
    else if (((glb_CargoID == 500) || (glb_CargoID == 501) || (glb_CargoID == 615) || (glb_CargoID == 619) || (glb_CargoID == 693)) && ((EstadoID == 113) || (EstadoID == 114))) {
        insertcomboData(fg, getColIndexByColKey(fg, 'Manifesto'), dsoComboManifesto, 'Estado', 'Manifesto');
    }


    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.ColWidth(1) = parseInt(divFG.currentStyle.width, 10) * 2;
    fg.ColWidth(2) = parseInt(divFG.currentStyle.width, 10) * 2;


    fg.Redraw = 2;
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    if ((fg.Rows > 1) && ((EstadoID == 65) || (EstadoID == 111)))
        fg.Editable = true;

    if (((glb_CargoID == 615) || (glb_CargoID == 619) || (glb_CargoID == 693)) && (EstadoID == 113) || (EstadoID == 114))
        fg.Editable = true;

    // Adiciona cor nas linhas do Grid
    for (i > 1; i < fg.Rows; i++) {
        var nCor = fg.TextMatrix(i, getColIndexByColKey(fg, 'Cor*'));
        //var  nCor = 0x000010;

        //var StatusManifesto = getColIndexByColKey(fg, 'Status_Manifesto*');

        var Dias = getColIndexByColKey(fg, 'Dias*');
        //Pinta a celula.
        if (nCor == '0X90EE90') {
            //fg.Cell(6, i, StatusManifesto, i, StatusManifesto) = 0X90EE90;
            fg.Cell(6, i, Dias, i, Dias) = 0X90EE90;
        }
        else if (nCor == '0x00ffff') {
            // fg.Cell(6, i, StatusManifesto, i, StatusManifesto) = 0x00ffff;
            fg.Cell(6, i, Dias, i, Dias) = 0x00ffff;
        }
        else if (nCor == '0X0000FF') {
            //  fg.Cell(6, i, StatusManifesto, i, StatusManifesto) = 0X0000FF;
            fg.Cell(6, i, Dias, i, Dias) = 0X0000FF;
        }
        else if (nCor == '0x000010') {
            // fg.Cell(6, i, StatusManifesto, i, StatusManifesto) = 0x000010; //Pinta celula
            //  fg.Cell(7, i, StatusManifesto, i, StatusManifesto) = 0XFFFFFF; //Pinta Fonte
            fg.Cell(6, i, Dias, i, Dias) = 0x000010; //Pinta celula
            fg.Cell(7, i, Dias, i, Dias) = 0XFFFFFF; //Pinta Fonte

        }
    }
    lockControlsInModalWin(false);
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;


    if ((fg.Row < 1) && (fg.Rows > 1))
        fg.Row = 1;

    // destrava botao Incluir se tem linhas no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
    }
}
function fillCombo() {
    var sSQL = '';
    var sWhere = '';
    glb_CounterCmbsStatics = 1;

    sSQL = " SELECT '1' AS Tipo,  0 AS fldID, space(0) as fldName " +
            "UNION ALL " +
           "SELECT DISTINCT '1' AS Tipo , d.PessoaID AS fldID, d.Fantasia AS fldNome " +
           "FROM RelacoesPesRec a WITH(NOLOCK) " +
           "INNER JOIN RelacoesPesRec_Perfis b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) " +
           "INNER JOIN Recursos_Direitos c WITH(NOLOCK) ON (b.PerfilID = c.PerfilID) " +
           "INNER JOIN Pessoas d WITH(NOLOCK) ON (b.EmpresaID = d.PessoaID) " +
           "WHERE (a.SujeitoID = " + glb_nUsuarioID + "  AND (dbo.fn_Empresa_Mae(d.PessoaID, 2, 1) > 0) AND   a.TipoRelacaoID = 11 AND a.EstadoID = 2 AND " +
           "(c.Alterar1 = 1 OR c.Alterar2 = 1) AND d.PessoaID NOT IN (3,8,9,12) AND ((c.Alterar1 = 1 AND c.Alterar2 = 1))) " +
           "UNION ALL " +
           "SELECT '2' AS Tipo,  0 AS fldID, space(0) as fldName " +
           "UNION ALL " +
           "SELECT '2' AS Tipo,  recursoid AS fldID, RecursoFantasia as fldName " +
           "FROM Overfly.dbo.Recursos WITH (NOLOCK) " +
           "WHERE RecursoID IN (65,111,112,113,114,115) " +
           "UNION ALL " +
           "SELECT '3' AS Tipo,  0 AS fldID, space(0) as fldName " +
           "UNION ALL " +
           "SELECT DISTINCT '3' AS Tipo, b.OperacaoID AS fldID, CONVERT(VARCHAR(MAX),a.TransacaoID )+ ' - ' + b.Operacao  as fldName " +
           " FROM ManifestoNFe a WITH (NOLOCK) " +
           " INNER JOIN Operacoes b WITH(NOLOCK) ON (b.OperacaoID = a.TransacaoID) " +
           "ORDER BY Tipo, fldID ";

    setConnection(dsoCombos);
    dsoCombos.SQL = sSQL;
    dsoCombos.ondatasetcomplete = CarregaCombo_DSC;
    dsoCombos.Refresh();

    fillComboFornecedor();
    fillComboGP();

}

function CarregaCombo_DSC() {
    var optionStr, optionValue;

    clearComboEx(['SELEMPRESA', 'SELESTADO', 'SELTRANSACAO']);

    dsoCombos.recordset.setFilter('Tipo=1');
    while (!dsoCombos.recordset.EOF) {
        optionStr = dsoCombos.recordset['fldName'].value;
        optionValue = dsoCombos.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selEmpresa.add(oOption);
        dsoCombos.recordset.MoveNext();
    }
    dsoCombos.recordset.setFilter('');
    dsoCombos.recordset.MoveFirst();
    dsoCombos.recordset.setFilter('Tipo=2');

    while (!dsoCombos.recordset.EOF) {
        optionStr = dsoCombos.recordset['fldName'].value;
        optionValue = dsoCombos.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selEstado.add(oOption);
        dsoCombos.recordset.MoveNext();
    }
    dsoCombos.recordset.setFilter('');
    dsoCombos.recordset.MoveFirst();
    dsoCombos.recordset.setFilter('Tipo=3');

    while (!dsoCombos.recordset.EOF) {
        optionStr = dsoCombos.recordset['fldName'].value;
        optionValue = dsoCombos.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selTransacao.add(oOption);
        dsoCombos.recordset.MoveNext();
    }
    selEstado.selectedIndex = 1;


}
function fillComboFornecedor() {

    setConnection(dsoFornecedor);
    var sWHERE = '';

    if (selEmpresa.value == 0)
        sWHERE = ('(a.EmpresaID = ' + glb_aEmpresaData[0] + ') ');
    else {
        sWHERE = sWHERE + ' ( a.EmpresaID = ' + selEmpresa.value + ') ';
    }
    if (selFornecedorID.value != 0)
        sWHERE = sWHERE + ' AND ( a.EmissorID = ' + selFornecedorID.value + ')';

    if ((!(trimStr(txtdtInicio.value) == '')) && (!(trimStr(txtdtFim.value) == '')))
        //  sWHERE = sWHERE + " AND ( a.dtEmissao BETWEEN '" + normalizeDate_DateTime(txtdtInicio.value, true) + "' AND '" + normalizeDate_DateTime(txtdtFim.value, true) + "' )";
        sWHERE = sWHERE + " AND ( a.dtEmissao BETWEEN '" + dateFormatToSearch(txtdtInicio.value) + "' AND '" + dateFormatToSearch(txtdtFim.value) + "' )";
    //normalizeDate_DateTime(txtVigencia.v  alue, true)

    if (!(trimStr(txtNF.value) == ''))
        sWHERE = sWHERE + ' AND ( a.NotaFiscal = ' + txtNF.value + ')';

    if (selEstado.value != 0)
        sWHERE = sWHERE + ' AND ( a.EstadoID = ' + selEstado.value + ')';

    /* 632 Ger Produtos
       657 An Produtos
       648 Coo Fiscal
       639 Coo Log�stica
    */
    if (glb_CargoID == 657) {
        sWHERE = sWHERE + ' AND (A.EmissorID  IN (SELECT DISTINCT b.FornecedorID FROM RelacoesPesCon a WITH(NOLOCK) ' +
                     ' INNER JOIN RelacoesPesCon_Fornecedores b WITH(NOLOCK) ON (b.RelacaoID = a.RelacaoID) ' +
                     ' WHERE ((a.TipoRelacaoID = 61)AND (a.ProprietarioID = ' + glb_GestorID + ' )))) ';
    }
    else if ((glb_CargoID == 648) || (glb_CargoID == 639) || (glb_CargoID == 621) || (glb_CargoID == 615) || (glb_CargoID == 693) || (glb_CargoID == 619)) {
        sWHERE = sWHERE + ' ';
    }
    else {
        sWHERE = sWHERE + ' AND (A.EmissorID  IN (SELECT DISTINCT b.FornecedorID FROM RelacoesPesCon a WITH(NOLOCK) ' +
                          ' INNER JOIN RelacoesPesCon_Fornecedores b WITH(NOLOCK) ON (b.RelacaoID = a.RelacaoID) ' +
                          ' WHERE ((a.TipoRelacaoID = 61)AND (a.ProprietarioID = ' + glb_nUsuarioID + ' )))) ';
    }

    dsoFornecedor.SQL = 'SELECT 0 AS FornecedorID, \'\' AS Fornecedor ' +
	    'UNION ALL ' +
	    'SELECT DISTINCT EmissorID as FornecedorID, b.Fantasia as Fornecedor ' +
    	    'FROM ManifestoNFe a WITH(NOLOCK) ' +
                'INNER JOIN  Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.EmissorID)  ' +
            'WHERE ' + sWHERE +
        'ORDER BY Fornecedor ASC ';

    dsoFornecedor.ondatasetcomplete = preencheMarca_DSC;
    dsoFornecedor.Refresh();
}

function preencheMarca_DSC() {
    clearComboEx(['selFornecedorID']);

    if (!(dsoFornecedor.recordset.BOF || dsoFornecedor.recordset.EOF)) {
        dsoFornecedor.recordset.MoveFirst();

        while (!dsoFornecedor.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoFornecedor.recordset['Fornecedor'].value;;
            oOption.value = dsoFornecedor.recordset['FornecedorID'].value;
            selFornecedorID.add(oOption);
            dsoFornecedor.recordset.MoveNext();
        }
        selFornecedorID.disabled = ((selFornecedorID.length <= 1) ? true : false);
    }
}

function fillComboGP() {
    var sSQL = '';
    setConnection(dsoGP);
    /* 632 Ger Produtos
     657 An Produtos
     648 Coo Fiscal
     639 Coo Log�stica
  */

    if (glb_CargoID == 657) {
        sSQL = 'SELECT 0 AS GPID, \'\' AS GP ' +
	    'UNION ' +
    	 ' SELECT DISTINCT b.PessoaID AS GPID, b.Fantasia AS GP ' +
        'FROM RelacoesPesCon a WITH(NOLOCK) ' +
        'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) ' +
        'WHERE (a.SujeitoID = ' + glb_aEmpresaData[0] + ' AND a.TipoRelacaoID = 61 AND a.EstadoID IN (11,12,13,2,14,15)) ' +
				    'AND (dbo.fn_Funcionario_CargoID (b.PessoaID,2) LIKE \'%632%\') ' +
                    'AND (b.PessoaID = ' + glb_GestorID + ') ' +
                    'ORDER BY GP ';
    }
    else if ((glb_CargoID == 648) || (glb_CargoID == 639) || (glb_CargoID == 621) || (glb_CargoID == 615) || (glb_CargoID == 693) || (glb_CargoID == 619)) {
        sSQL = 'SELECT 0 AS GPID, \'\' AS GP ' +
            'UNION ' +
             ' SELECT DISTINCT b.PessoaID AS GPID, b.Fantasia AS GP ' +
            'FROM RelacoesPesCon a WITH(NOLOCK) ' +
            'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) ' +
            'WHERE (a.SujeitoID = ' + (selEmpresa.value == "" ? glb_aEmpresaData[0] : selEmpresa.value) + ' AND b.estadoid = 2 AND a.TipoRelacaoID = 61 AND a.EstadoID IN (11,12,13,2,14,15)) ' +
                        ' ' +
                        'ORDER BY GP ';
    }
    else
        sSQL = 'SELECT 0 AS GPID, \'\' AS GP ';


    dsoGP.SQL = sSQL;
    dsoGP.ondatasetcomplete = preencheGP_DSC;
    dsoGP.Refresh();
}

function preencheGP_DSC() {
    clearComboEx(['selGP']);

    if (!(dsoGP.recordset.BOF || dsoGP.recordset.EOF)) {
        dsoGP.recordset.moveFirst();

        while (!dsoGP.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoGP.recordset['GP'].value;
            oOption.value = dsoGP.recordset['GPID'].value;
            selGP.add(oOption);
            dsoGP.recordset.MoveNext();
        }
        selGP.disabled = ((selGP.length <= 1) ? true : false);
    }
}

function fillGridItens(ManifestoID) {
    var strPars = new String();

    //Trava a interface
    //lockControlsInModalWin(true);

    // zera o grid
    Itens.Rows = 1;

    // parametrizacao do dso dsoItens
    setConnection(dsoItens);

    //dsoItens.SQL = 'EXEC sp_ManifestoNFe_Itens ' + ManifestoID;
    dsoItens.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/notasfiscais/serverside/ListaManifestoItens.aspx?nManifesto=' + ManifestoID;
    dsoItens.ondatasetcomplete = fillGrid_Itens_DSC;

    try {
        dsoItens.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
}

function fillGrid_Itens_DSC() {

    var dTFormat = '';
    dTFormat = 'dd/mm/yyyy';

    if (selDetalhe.value != 1) {
        Eventos.focus();
        lockControlsInModalWin(false);
        return null;
    }

    if ((dsoItens.recordset.BOF) || (dsoItens.recordset.EOF)) {
        glb_sAlert += ((glb_sAlert.length > 0) ? '\n' : '') + 'N�o existem itens para este manifesto.';
        return null;
    }

    dsoItens.recordset.MoveFirst();

    startGridInterface(Itens);
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    Itens.FontSize = '8';
    Itens.FrozenCols = 0;

    //Inicio = 0
    headerGrid(Itens, ['Codigo',
	               'Descri��o',
	               'Quantidade',
                   'Vl Unitario',
                   'Vl Total',
                   'Ordem'],
                   [5]);

    // glb_aCelHint = [0, 0, 'Tipo de Documento Fiscal'];

    fillGridMask(Itens, dsoItens, ['cProd',
                                     'xProd',
                                     'qCom',
                                     'vUnCom',
                                     'vProd',
                                     'Ordem'],
                                     ['', '', '', '999999999.99', '999999999.99', ''],
                                     ['', '', '', '###,###,##0.00', '###,###,##0.00', '']);

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1, '######', 'C'], [3, '###,###,###,###.00', 'S'],
                                                           [4, '###,###,###,###.00', 'S']], null);
    //Itens.MergeCells = 1;
    Itens.MergeCol(-1) = true;
    Itens.AutoSizeMode = 0;
    Itens.AutoSize(0, Itens.Cols - 1);
    Itens.ExplorerBar = 5;
    Itens.FrozenCols = 1;
    Itens.ColWidth(1) = parseInt(divFG.currentStyle.width, 10) * 2;
    Itens.Redraw = 2;
    Itens.Editable = false;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    lockControlsInModalWin(false);

    if (Itens.Rows > 1) {
        window.focus();
        Itens.focus();
    }
}

function fillGrid_Eventos(ManifestoID) {
    var sSql = '';

    //Trava a interface
    //lockControlsInModalWin(true);

    setConnection(dsoGridEventos);
    // zera o grid
    Eventos.Rows = 1;

    sSql = "  SELECT  Overfly.dbo.fn_Recursos_Campos (EstadoID,2) AS Estado, " +
            "CASE StatusAutorizacao WHEN 1 THEN SPACE(0) WHEN 2 THEN 'Aguardando' WHEN 3 THEN 'Autorizado' WHEN 4 THEN 'N�o Autorizado' END AS Status_Manifesto,  " +
            "Overfly.dbo.fn_Pessoa_Fantasia (UsuarioID,0) AS Usuario,  " +
            "DtManifesto AS dtManifesto,  " +
            "MensagemSefaz AS Mensagem " +
            "FROM Overfly.dbo.ManifestoNFe_Eventos WITH (NOLOCK) " +
            "WHERE ManifestoID = " + ManifestoID + " " +
            "ORDER BY ManifestoEventoID DESC ";

    dsoGridEventos.SQL = sSql;
    dsoGridEventos.ondatasetcomplete = fillGrid_Eventos_DSC;

    try {
        dsoGridEventos.refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;
        lockControlsInModalWin(false);
        window.focus();
        selEmpresa.focus();
    }
}

function fillGrid_Eventos_DSC() {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    if (selDetalhe.value != 2) {
        Itens.focus();
        lockControlsInModalWin(false);

        if (glb_sAlert.length > 0) {
            window.top.overflyGen.Alert(glb_sAlert);
        }

        return null;
    }

    if ((dsoGridEventos.recordset.BOF) || (dsoGridEventos.recordset.EOF)) {
        glb_sAlert += ((glb_sAlert.length > 0) ? '\n' : '') + 'N�o existem eventos para este manifesto.';

        if (glb_sAlert.length > 0) {
            window.top.overflyGen.Alert(glb_sAlert);
        }

        return null;
    }

    dsoGridEventos.recordset.MoveFirst();

    startGridInterface(Eventos);
    Eventos.FontSize = '8';
    Eventos.FrozenCols = 0;

    //Inicio = 0
    headerGrid(Eventos, ['Estado',
                 'Autoriza��o',
                 'Usuario',
                 'Data',
                 'Mensagem'],
                 []);

    // glb_aCelHint = [0, 0, 'Tipo de Documento Fiscal'];

    fillGridMask(Eventos, dsoGridEventos, ['Estado',
                                       'Status_Manifesto',
                                       'Usuario',
                                       'dtManifesto',
                                       'Mensagem'],
                                       ['', '', '', '99/99/9999', ''],
                                       ['', '', '', dTFormat + ' hh:mm:ss', '']);

    //Eventos.MergeCells = 1;
    Eventos.MergeCol(-1) = true;
    Eventos.AutoSizeMode = 0;
    Eventos.AutoSize(0, Eventos.Cols - 1);
    Eventos.ExplorerBar = 5;
    Eventos.FrozenCols = 1;
    Eventos.Redraw = 2;
    Eventos.Editable = false;
    lockControlsInModalWin(false);

    // destrava botao Incluir se tem linhas no grid
    if (Eventos.Rows > 1) {
        window.focus();
        Eventos.focus();
    }
}

function comboManifesto() {
    var sSql = '';
    var EstadoID = selEstado.value;

    lockControlsInModalWin(false);

    setConnection(dsoComboManifesto);

    if ((EstadoID == 65) || (EstadoID == 111)) {
        btnManifestar.disabled = false;
        /*        
        Se Estado = Pendentes
             1-
             2-Ci�ncia da opera��o
             3-Desconhecimento da opera��o  
             4-Opera��o n�o realizada        
       Se Estado = Ciencia da opera��o
             1-     
             2-Opera��o n�o realizada        
        */
        if (EstadoID == 65) {

            sSql = "SELECT 0 AS Manifesto, SPACE(0) AS Estado " +
                    "UNION ALL " +
                   "SELECT Recursoid AS Manifesto, RecursoFantasia AS Estado " +
                   "FROM Overfly.dbo.Recursos WITH (NOLOCK) " +
                   "WHERE RecursoID IN (111,112,114) " +
                       "ORDER BY Manifesto ";

        }
        else if (EstadoID == 111) {
            sSql = "SELECT 0 AS Manifesto, SPACE(0) AS Estado " +
                    "UNION ALL " +
                    "SELECT Recursoid AS Manifesto, RecursoFantasia AS Estado " +
                   "FROM Overfly.dbo.Recursos WITH (NOLOCK) " +
                   "WHERE RecursoID IN (114) " +
                       "ORDER BY Manifesto ";
        }

    }

    else if (EstadoID == 112) {
        sSql = "SELECT 0 AS Manifesto, SPACE(0) AS Estado " +
                "UNION ALL " +
                "SELECT Recursoid AS Manifesto, RecursoFantasia AS Estado " +
                "FROM Overfly.dbo.Recursos WITH (NOLOCK) " +
                "WHERE RecursoID IN (113, 114) " +
                    "ORDER BY Manifesto ";
    }

    else if (EstadoID == 0) {
        (window.top.overflyGen.Alert("Este processo poder� levar alguns minutos.") == 1);
        return null;
    }

    if (((glb_CargoID == 615) || (glb_CargoID == 619) || (glb_CargoID == 693)) && (EstadoID == 113) || (EstadoID == 114)) {
        btnManifestar.disabled = false;
        if (EstadoID == 113) {
            sSql = "SELECT 0 AS Manifesto, SPACE(0) AS Estado " +
                   "UNION ALL " +
                   "SELECT Recursoid AS Manifesto, RecursoFantasia AS Estado " +
                   "FROM Overfly.dbo.Recursos WITH (NOLOCK) " +
                   "WHERE RecursoID IN (114) " +
                       "ORDER BY Manifesto ";
        }
        else if (EstadoID == 114) {
            sSql = "SELECT 0 AS Manifesto, SPACE(0) AS Estado " +
                   "UNION ALL " +
                   "SELECT Recursoid AS Manifesto, RecursoFantasia AS Estado " +
                   "FROM Overfly.dbo.Recursos WITH (NOLOCK) " +
                   "WHERE RecursoID IN (113) " +
                       "ORDER BY Manifesto ";
        }

    }

    if (sSql.length == 0) {
        sSql = "SELECT 0 AS Manifesto, SPACE(0) AS Estado " +
               "UNION ALL " +
               "SELECT Recursoid AS Manifesto, RecursoFantasia AS Estado " +
               "FROM Overfly.dbo.Recursos WITH (NOLOCK) " +
               "WHERE RecursoID IN (0) " +
                   "ORDER BY Manifesto ";
    }

    dsoComboManifesto.SQL = sSql;
    dsoComboManifesto.ondatasetcomplete = ComboManifesto_DSC;
    dsoComboManifesto.Refresh();

}

function ComboManifesto_DSC() {
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    lockControlsInModalWin(true);
}

function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;
    var ManifestoID = '';
    var Mensagem = '';
    var Estado = '';
    var sObservacoes = '';

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    lockControlsInModalWin(true);

    for (i = 1; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'Manifesto', i) != 0) {
            nBytesAcum = strPars.length;

            ManifestoID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'ManifestoID'));
            Mensagem = fg.TextMatrix(i, getColIndexByColKey(fg, 'Motivo'));
            Estado = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Manifesto'));
            sObservacoes = fg.TextMatrix(i, getColIndexByColKey(fg, 'Observacoes'));

            if ((Mensagem.length < 15) && (Estado == 114)) {
                window.top.overflyGen.Alert('Para manifestar esta Nota como "Opera��o n�o Realizada" � necess�rio um motivo de no m�nimo 15 caracteres.');
                lockControlsInModalWin(false);
                return null;
            }

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                    strPars = '';
                    nDataLen = 0;
                }
                strPars = '?UsuarioID=' + escape(glb_nUsuarioID);
            }
            strPars += '&ManifestoID=' + escape(ManifestoID);
            strPars += '&EstadoID=' + escape(Estado);
            strPars += '&Mensagem=' + escape(Mensagem != '' ? Mensagem : null);
            strPars += '&Observacoes=' + escape(sObservacoes != '' ? sObservacoes : null);
            nDataLen++;
        }
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

    sendDataToServer();
}
function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/notasfiscais/serverside/manifestar.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            lockControlsInModalWin(false);
            glb_OcorrenciasTimerInt = window.setInterval('fillGrid(0)', 10, 'JavaScript');
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_OcorrenciasTimerInt = window.setInterval('fillGrid(0)', 10, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
    lockControlsInModalWin(false);

    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        dsoGrava.recordset.MoveFirst();


        if (((dsoGrava.recordset['fldResponse'].value).length) > 0) {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['fldResponse'].value) == 0)
                return null;
        }

    }
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

function js_fg_modalManifesto_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    //Forca celula read-only    
    if (!glb_GridIsBuilding) {
        if (glb_FreezeRolColChangeEvents)
            return true;

        if (cellIsLocked(NewRow, NewCol)) {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
    }
}

function cellIsLocked(nRow, nCol) {
    var retVal = false;

    if ((nCol != getColIndexByColKey(fg, 'Manifesto')) && (nCol != getColIndexByColKey(fg, 'Motivo')) && (nCol != getColIndexByColKey(fg, 'Observacoes'))) {
        retVal = true;
    }

    return retVal;
}

function dadosDoUsuario() {
    var sSql = '';

    sSql = ' SELECT a.CargoID , b.EhCargoSuperior, dbo.fn_Pessoa_Superior(a.SujeitoID, a.ObjetoID, 1, 1) AS GestorID ' +
            'FROM Overfly.dbo.RelacoesPessoas a WITH (NOLOCK), Overfly.dbo.Recursos b WITH (NOLOCK) ' +
            'WHERE a.SujeitoID = ' + glb_nUsuarioID + ' AND a.ObjetoID = ' + glb_aEmpresaData[0] + ' ' +
            'AND a.TipoRelacaoID = 31 AND a.CargoID = b.RecursoID ' +
            'AND a.EstadoID = 2';

    setConnection(dsoUsuario);
    dsoUsuario.SQL = sSql;
    dsoUsuario.ondatasetcomplete = dadosDoUsuario_DSC;
    dsoUsuario.Refresh();
}
function dadosDoUsuario_DSC() {
    // mostra a janela modal com o arquivo carregado

    if (!(dsoUsuario.recordset.BOF) || !(dsoUsuario.recordset.EOF)) {
        glb_CargoID = dsoUsuario.recordset['CargoID'].value;
        glb_EhSuperior = dsoUsuario.recordset['EhCargoSuperior'].value;
        glb_GestorID = dsoUsuario.recordset['GestorID'].value;
    }
}
function AcertaGrids() {
    selDetalhe.value = 0;
    adjustGrid(selDetalhe);
}

function fillGridPedidos(Pedido) {
    var strPars = new String();

    //Trava a interface
    //  lockControlsInModalWin(true);

    // zera o grid
    Pedidos.Rows = 1;

    // parametrizacao do dso dsoItens
    setConnection(dsoPedidos);

    dsoPedidos.SQL = ' SELECT a.PedidoID, dbo.fn_Pedido_Estado(a.PedidoID,3) AS Estado,  dbo.fn_Produto_Descricao2 (b.ProdutoID, NULL, 12) AS Descricao, b.Quantidade, b.ValorUnitario AS Valor_Unitario ' +
                     ' FROM Overfly.dbo.Pedidos a WITH (NOLOCK) ' +
                     ' INNER JOIN Overfly.dbo.Pedidos_Itens b WITH (NOLOCK) ON (a.PedidoID = b.PedidoID) ' +
                     ' WHERE a.PedidoID = ' + Pedido;

    dsoPedidos.ondatasetcomplete = fillGrid_Pedidos_DSC;

    try {
        dsoPedidos.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
}

function fillGrid_Pedidos_DSC()
{
    if (selDetalhe.value != 1) {
        Eventos.focus();
        lockControlsInModalWin(false);
        return null;
    }

    if ((dsoPedidos.recordset.BOF) || (dsoPedidos.recordset.EOF)) {
        glb_sAlert += ((glb_sAlert.length > 0) ? '\n' : '') + 'N�o existem Itens nesse Pedido para este manifesto.';
        return null;
    }

    dsoPedidos.recordset.MoveFirst();

    startGridInterface(Pedidos);
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    Pedidos.FontSize = '8';
    Pedidos.FrozenCols = 0;

    //Inicio = 0
    headerGrid(Pedidos, ['Pedido',
                    'E',
	               'Descri��o',
	               'Quantidade',
                   'Vl Unitario'],
                   []);

    // glb_aCelHint = [0, 0, 'Tipo de Documento Fiscal'];

    fillGridMask(Pedidos, dsoPedidos, ['PedidoID',
                                        'Estado',
                                     'Descricao',
                                     'Quantidade',
                                     'Valor_Unitario'],
                                     ['', '', '', '', '999999999.99'],
                                     ['', '', '', '', '###,###,##0.00']);


    gridHasTotalLine(Pedidos, 'Totais', 0xC0C0C0, null, true, [[3, '######', 'S'], [4, '###,###,###,###.00', 'S']], null);
    Pedidos.MergeCells = 2;
    Pedidos.MergeCol(-1) = true;
    Pedidos.AutoSizeMode = 0;
    Pedidos.AutoSize(0, Pedidos.Cols - 1);
    Pedidos.ExplorerBar = 5;
    Pedidos.FrozenCols = 1;
    Pedidos.ColWidth(2) = parseInt(divFG.currentStyle.width, 10) * 2;
    Pedidos.Redraw = 2;
    Pedidos.Editable = false;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    lockControlsInModalWin(false);

}

function saveDatas(aData, aDataLen, aDataElemLen) {
    var strPars = new String();
    var i, j;

    strPars = '?';
    strPars += 'nDataLen=' + escape(aDataLen);
    strPars += '&nDataElemLen=' + escape(aDataElemLen);

    for (i = 0; i < aDataLen; i++) {
        for (j = 0; j < aDataElemLen; j++)
            strPars += '&aData= ' + i.toString() + '/' + escape(aData[i][j]);
    }

    // pagina asp no servidor saveitemspedido.asp
    dsoSaveDatas.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/notasfiscais/serverside/saveDatasManifestos.aspx' + strPars;
    dsoSaveDatas.ondatasetcomplete = saveDatas_DSC;
    dsoSaveDatas.refresh();
}


/*
function dadosGravar() {

    /*
    var nManifestoID;
    var sObservacoes;
    var strPas = '';

    if (fg.Rows > 1) {
        for (i = 2; i < fg.Rows; i++) {

            ManifestoID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'ManifestoID'))
            sObservacoes = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Observacoes'))

            if (sObservacoes != '') {

                strPas += 'UPDATE ManifestoNFe SET Observacoes = ' + sObservacoes + 'WHERE ManifestoID = ' + ManifestoID + ' '
            }
        }


        if (strPas.length > 0) {

            setConnection(dsoDadosGravar);

            dsoDadosGravar.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/notasfiscais/serverside/dadosManifestoGravar.aspx' + strPas;
            dsoDadosGravar.ondatasetcomplete = dadosGravar_DSC;
            dsoDadosGravar.Refresh();
        }
    }
    
}
*/
function saveDatas_DSC() {

    lockControlsInModalWin(false);
    return null;
}

function openXML() {

    var strPars = new String();
    var nManifestoID;

    nManifestoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ManifestoID'));
    strPars = '?';
    strPars += 'nManifestoID=' + escape(nManifestoID);

    // pagina asp no servidor saveitemspedido.asp
    dsoOpenXML.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/notasfiscais/serverside/openXML.aspx' + strPars;
    dsoOpenXML.ondatasetcomplete = openXML_DSC;
    dsoOpenXML.refresh();
}

function openXML_DSC() {

    var nResultado = dsoOpenXML.recordset['Resultado'].value;

    if (nResultado == 1){
        var spathXML = dsoOpenXML.recordset['Mensagem'].value;

        window.open(spathXML);

        lockControlsInModalWin(false);
        return null;
    }
    else {
        var sMensagem = dsoOpenXML.recordset['Mensagem'].value;

        if (window.top.overflyGen.Alert(sMensagem) == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}