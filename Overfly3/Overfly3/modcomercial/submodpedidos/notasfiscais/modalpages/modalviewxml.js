/********************************************************************
modalloadxml.js

Library javascript para o modalloadxml.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nUserID = 0;


var dsoXMLFileNFe = new CDatatransport('dsoXMLFileNFe');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
chks_onclick()
btn_onclick(ctl)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalviewxmlBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    showExtFrame(window, true);
    window.focus();

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Load XML', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // ajusta o divFields
    elem = divFields;
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modWidth - 2 * ELEM_GAP - 6;
    }

    with (selDanfeXml.style) {
        left = 50;
        top = modHeight - 40;
    }

    with (btnOK.style) {
        left = 100;
        top = parseInt(selDanfeXml.style.top) + 1;
    }

    btnCanc.style.visibility = 'hidden';
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // Imprime o documento exibido na modal.
    if (controlID == 'btnOK') {
        lockControlsInModalWin(true);

        // Se estiver exibindo o DANFE, imprime o frame...
        if (selDanfeXml.value == 0) {
            window.frameFields.print();
        }
            // ... caso contrário, grava o XML.
        else {
            setConnection(dsoXMLFileNFe);

            dsoXMLFileNFe.SQL =
				"SELECT " +
					"RIGHT('0' + RTRIM(YEAR(dtNotaFiscal)), 4) AS Ano, " +
					"RIGHT('0' + RTRIM(MONTH(dtNotaFiscal)), 2) AS Mes, " +
					"RIGHT('0' + RTRIM(DAY(dtNotaFiscal)), 2) AS Dia, " +
					"ArquivoXMLNFe " +
				"FROM " +
					"NotasFiscais WITH(NOLOCK) " +
				"WHERE " +
					"NotaFiscalID = " + glb_NotaFiscalID;

            dsoXMLFileNFe.ondatasetcomplete = XMLNFePath_DSC;
            dsoXMLFileNFe.refresh();
        }

        lockControlsInModalWin(false);

        return null;
    }
    else if (controlID == 'btnCanc') {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_SUP', null);
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function XMLNFePath_DSC() {
    window.open(PAGES_URL_ROOT + "/NFe/retorno/Recebido/" +
		dsoXMLFileNFe.recordset("Ano").value + "/" +
		dsoXMLFileNFe.recordset("Mes").value + "/" +
		dsoXMLFileNFe.recordset("Dia").value + "/" +
		dsoXMLFileNFe.recordset("ArquivoXMLNFe").value);
}
