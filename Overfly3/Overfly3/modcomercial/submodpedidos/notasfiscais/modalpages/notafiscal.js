// VARIAVEIS GLOBAIS ************************************************
var glb_Print_2nd_Copy = null;
var glb_timerListaEmbalagem = null;
var glb_bPrintPreview = null;

var glb_nDsosNota_Extra_ShipTo = 0;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/****************************************************************************
INICIO DAS FUNCOES DE IMPRESSAO DE NOTA FISCAIS
****************************************************************************/

/********************************************************************
Impressao da Nota Fiscal (ID=40151)
********************************************************************/
function imprimeNotaFiscal()
{
    //Controla a quantidade de dsos para impressao da nota
    glb_ndsosNota = 5;    
    
    var nNotaCorrente = glb_nNotaFiscalID;
	var nEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');	
   
    //Header - Nota Fiscal  
    setConnection(dsoHeader);
    
    dsoHeader.SQL = 
        'SELECT ' +
            'NF.NotaFiscalID, Imagens.Imagem AS FotoEmpresa, ' + 
            '(CASE NF.TipoNotaFiscalID WHEN 601 THEN ' + '\'' + 'X' + '\'' + ' ELSE SPACE(0) END) AS Entrada, ' +
            '(CASE NF.TipoNotaFiscalID WHEN 602 THEN ' + '\'' + 'X' + '\'' + ' ELSE SPACE(0) END) AS Saida, ' +
            'STR(NF.NotaFiscal) AS NotaFiscal, CONVERT(VARCHAR, NF.dtNotaFiscal, ' + DATE_SQL_PARAM + ') AS dtNotaFiscal, ' +
            'ISNULL(CONVERT(VARCHAR, NF.dtEntradaSaida, '+DATE_SQL_PARAM+'), SPACE(0)) AS dtEntradaSaida, ' +
		    'ISNULL(CONVERT(VARCHAR(2), dbo.fn_Pad(DATEPART(hh, NF.dtEntradaSaida), 2, CHAR(48), CHAR(76))) + CHAR(58) + CONVERT(VARCHAR(2), dbo.fn_Pad(DATEPART(mi, NF.dtEntradaSaida), 2, CHAR(48), CHAR(76))), SPACE(0)) AS HoraEntradaSaida, ' +
            'NF.CFOPs, NF.Natureza, NF.TransacaoID AS TransacaoID, STR(NF.PedidoID) AS PedidoID, STR(NF.Formulario) AS Formulario, NFDest.Nome, ' +
            'STR(NFDest.PessoaID) AS PessoaID, ISNULL(STR(NFTransp.PessoaID), SPACE(0)) AS TransportadoraID, ' +
            'ISNULL(NFDest.DocumentoFederal, SPACE(0)) AS Documento1, ISNULL(NFDest.DocumentoMunicipal, SPACE(0)) AS DocumentoMunicipal, ' + 
            '(CASE ISNULL(PessoaEnderecoPais.EnderecoInvertido, 0) WHEN 1 ' + 
                'THEN (ISNULL(NFDest.Numero, SPACE(0)) + \' \' + ISNULL(NFDest.Endereco, SPACE(0)) +  \' \' ) ' + //+ ISNULL(NFDest.Complemento, SPACE(0))
                'ELSE (ISNULL(NFDest.Endereco, SPACE(0)) +  \' \' + ISNULL(NFDest.Numero, SPACE(0)) + \' \' ) END)  AS Endereco, ' +//+ ISNULL(NFDest.Complemento, SPACE(0))
            'ISNULL(NFDest.Bairro, SPACE(0)) AS Bairro, NFDest.CEP, NFDest.Cidade, NFDest.UF, NFDest.Pais, UPPER(LEFT(NFDest.Pais, 2)) AS PaisAbreviado, ' +
            '(ISNULL(NFDest.DDI, SPACE(0)) + ISNULL(NFDest.DDD, SPACE(0)) + \' \' + ISNULL(NFDest.Telefone, SPACE(0))) AS Telefone, ' + 
            'ISNULL(NFDest.DocumentoEstadual, SPACE(0)) AS Documento2, ' +
            'NFISS.ValorImposto AS ValorTotalImposto3, NFISS.BaseCalculo AS BaseCalculoImposto3, LEFT(ROUND(((NFISS.ValorImposto/NFISS.BaseCalculo)*100),2),4) as AliquotaISS, NF.ValorTotalServicos, NFICMS.BaseCalculo AS BaseCalculoImposto1, NFICMS.ValorImposto AS ValorTotalImposto1, ' +
            'NFICMSST.BaseCalculo AS BaseCalculoImposto1Subst, NFICMSST.ValorImposto AS ValorTotalImposto1Subst, NF.ValorTotalProdutos, ISNULL(NF.ValorFrete, 0) AS ValorFrete, ' +
            'ISNULL(NF.ValorSeguro, 0) AS ValorSeguro, ISNULL(NF.OutrasDespesas, 0) AS OutrasDespesas, ' +
            '(CASE NF.EmpresaID WHEN 7 THEN ISNULL(NFTAX.ValorImposto, 0) ELSE ISNULL(NFIPI.ValorImposto, 0) END) AS ValorTotalImposto2, NF.ValorTotalNota, ' +
            'ISNULL(NFTransp.Nome,SPACE(0)) AS Transportadora, ISNULL(NF.Frete, SPACE(0)) AS Frete, ' +
            '(CASE ISNULL(PessoaEnderecoPais2.EnderecoInvertido, 0) WHEN 1 ' + 
                'THEN (ISNULL(NFTransp.Numero, SPACE(0)) + \' \' + ISNULL(NFTransp.Endereco, SPACE(0)) +  \' \' + ISNULL(NFTransp.Complemento, SPACE(0))) ' +
                'ELSE (ISNULL(NFTransp.Endereco, SPACE(0)) +  \' \' + ISNULL(NFTransp.Numero, SPACE(0)) + \' \' + ISNULL(NFTransp.Complemento, SPACE(0))) END)  AS TranspEndereco, ' +
            'ISNULL(NFTransp.Cidade, SPACE(0)) AS TranspCidade, ' +
            'ISNULL(NFTransp.Bairro,SPACE(0)) AS TranspBairro, ISNULL(NFTransp.CEP,SPACE(0)) AS TranspCEP, ' +
            'ISNULL(NFTransp.UF,SPACE(0)) AS TranspUF, ISNULL(NFTransp.Pais,SPACE(0)) AS TranspPais, ' +
            '(ISNULL(NFTransp.DDI, SPACE(0)) + ISNULL(NFTransp.DDD, SPACE(0)) + \' \' + ISNULL(NFTransp.Telefone, SPACE(0))) AS TranspTelefone, ' +             
            'ISNULL(NFTransp.DocumentoFederal,SPACE(0)) AS TranspDocumento1, ISNULL(NFTransp.DocumentoEstadual,SPACE(0)) AS TranspDocumento2, ' +
            'ISNULL(NF.PlacaVeiculo, SPACE(0)) AS PlacaVeiculo, ISNULL(NF.UFVeiculo, SPACE(0)) AS UFVeiculo, '+
            'STR(ISNULL(NF.Quantidade, 0)) AS Quantidade, ' +
            'NF.Especie, ISNULL(dbo.fn_Tradutor(NF.MeioTransporte, ' + nEmpresaData[7] + ', ' + nEmpresaData[8] + ', NULL),SPACE(0)) AS MeioTransporte, ' +
            'NF.PesoBruto, NF.PesoLiquido, NF.Vendedor, ' +
            'ISNULL(NF.SeuPedido, SPACE(1)) AS SeuPedido, ' +
            '(SELECT ISNULL(CONVERT(VARCHAR(5), CONVERT(NUMERIC(3),PrazoMedioPagamento)) + \' Days\', SPACE(0)) FROM Pedidos WITH(NOLOCK) WHERE Pedidos.PedidoID=NF.PedidoID) AS PMP, ' +
            '(SELECT (CASE SUBSTRING(Observacao,1,1) WHEN \'/\' THEN SUBSTRING(Observacao,2,39) ELSE \'Miami\' END) ' +
            'FROM Pedidos WITH(NOLOCK) WHERE Pedidos.PedidoID=NF.PedidoID) AS CidadeEmpresa, ' +
		    'ISNULL((SELECT dbo.fn_Tradutor(bb.ItemAbreviado, ' + nEmpresaData[7] + ', ' + nEmpresaData[8] + ', NULL) FROM ' +
			    'Pedidos aa, TiposAuxiliares_Itens bb ' +
			    'WHERE (aa.NotaFiscalID = NF.NotaFiscalID AND ISNULL(aa.ModalidadeTransporteID, 621) = bb.ItemID)), SPACE(0)) AS Incoterm, ' +
			   '(SELECT TOP 1 ISNULL(LTRIM(RTRIM(Observacao)), SPACE(0)) FROM Pedidos WITH(NOLOCK) WHERE Pedidos.PedidoID=NF.PedidoID) AS ObservacoesInvoice '+
           'FROM NotasFiscais NF WITH(NOLOCK) ' +
            'LEFT OUTER JOIN Imagens WITH(NOLOCK) ON (NF.EmpresaID = Imagens.RegistroID AND Imagens.FormID=1210 AND Imagens.SubFormID=20100) ' +
            'INNER JOIN NotasFiscais_Pessoas NFDest WITH(NOLOCK) ON (NFDest.NotaFiscalID = NF.NotaFiscalID) AND (NFDest.TipoID = 791) ' + //Destinatario
            'LEFT OUTER JOIN NotasFiscais_Pessoas NFTransp WITH(NOLOCK) ON (NFTransp.NotaFiscalID = NF.NotaFiscalID) AND (NFTransp.TipoID = 793) ' + 
            'LEFT OUTER JOIN NotasFiscais_Impostos NFIPI WITH(NOLOCK) ON (NFIPI.NotaFiscalID = NF.NotaFiscalID) AND (NFIPI.ImpostoID = 8) ' +            
            'LEFT OUTER JOIN NotasFiscais_Impostos NFICMS WITH(NOLOCK) ON (NFICMS.NotaFiscalID = NF.NotaFiscalID) AND (NFICMS.ImpostoID = 9) ' +           
            'LEFT OUTER JOIN NotasFiscais_Impostos NFISS WITH(NOLOCK) ON (NFISS.NotaFiscalID = NF.NotaFiscalID) AND (NFISS.ImpostoID = 10) ' +
            'LEFT OUTER JOIN NotasFiscais_Impostos NFICMSST WITH(NOLOCK) ON (NFICMSST.NotaFiscalID = NF.NotaFiscalID) AND (NFICMSST.ImpostoID = 25) ' +  
            'LEFT OUTER JOIN NotasFiscais_Impostos NFTAX WITH(NOLOCK) ON (NFTAX.NotaFiscalID = NF.NotaFiscalID) AND (NFTAX.ImpostoID = 22) ' +  
            'LEFT OUTER JOIN Localidades PessoaEnderecoPais WITH(NOLOCK) ON (NFDest.PaisID = PessoaEnderecoPais.LocalidadeID) ' +
            'LEFT OUTER JOIN Localidades PessoaEnderecoPais2 WITH(NOLOCK) ON (NFTransp.PaisID = PessoaEnderecoPais2.LocalidadeID) ' +
        'WHERE NF.NotaFiscalID = ' + nNotaCorrente ;

    dsoHeader.ondatasetcomplete = dsoNotaFiscal_DSC;
    dsoHeader.refresh();

    // Detalhe 1 - Fatura
    setConnection(dsoDetail01);
    
    dsoDetail01.SQL = 'SELECT a.NotaFiscalID, a.Duplicata, ' +
					    'CONVERT(VARCHAR, a.Vencimento, '+DATE_SQL_PARAM+') AS Vencimento, ' +
					    'a.Valor, ' +
					    'CONVERT(VARCHAR(3), DATEDIFF(dd, b.dtNotaFiscal, a.Vencimento)) AS Prazo ' +
                      'FROM NotasFiscais_Duplicatas a WITH(NOLOCK), NotasFiscais b WITH(NOLOCK) ' +                      
                      'WHERE (a.NotaFiscalID = ' + nNotaCorrente + ' AND a.NotaFiscalID = b.NotaFiscalID AND b.EstadoID=67) ' +
                      'ORDER BY a.Vencimento ';
                      
    dsoDetail01.ondatasetcomplete = dsoNotaFiscal_DSC;
    dsoDetail01.refresh();

    // Detalhe 2 - Produto
    setConnection(dsoDetail02);

    dsoDetail02.SQL = 'SELECT a.NotaFiscalID, STR(a.ProdutoID) AS ItemID, a.Familia AS Produto, a.Marca, a.Modelo,  ISNULL(LEFT(a.Descricao,28), SPACE(1)) AS Descricao, ' +
                      'ISNULL(a.CFOP, SPACE(1)) AS CFOP, ' +
                      'ISNULL(LEFT(a.Descricao,20), SPACE(1)) AS DescricaoReduzida, ' +
                      'ISNULL(a.InformacoesAdicionais, ISNULL(dbo.fn_NotaFiscalItem_Mensagem(a.PedItemID), SPACE(0))) AS PaisOrigem, ' +
                      '(CASE WHEN (dbo.fn_Produto_PesosMedidas(a.ProdutoID, 0, 1)) > 0 THEN \'Net(Kg): \' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),a.Quantidade * dbo.fn_Produto_PesosMedidas(a.ProdutoID, 0, 1))) ELSE SPACE(0) END) AS PesoLiquido, ' +
                      '(CASE WHEN (dbo.fn_Produto_PesosMedidas(a.ProdutoID, 1, 1)) > 0 THEN \'Gross(Kg): \' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),a.Quantidade * dbo.fn_Produto_PesosMedidas(a.ProdutoID, 1, 1))) ELSE SPACE(0) END) AS PesoBruto, ' +
                      'ISNULL(a.NCM, SPACE(1)) AS ClassificacaoFiscal, (ISNULL(a.Origem, 0) + ISNULL(d.CodigoTributacao, 0)) AS SituacaoTributaria, a.Unidade, STR(a.Quantidade) AS Quantidade, ' +
                      'a.ValorUnitario, a.ValorTotal, ' +
                      'STR(d.Aliquota) AS AliquotaImposto1, STR(e.Aliquota) AS AliquotaImposto2, ' +
                      'd.Aliquota AS AliquotaImposto1Modelo2, ' +
                      'e.Aliquota AS AliquotaImposto2Modelo2, e.ValorImposto AS ValorImposto2, CONVERT(BIT, ISNULL(c.EhMicro, 0)) AS EhMicro, ' +
                      'ISNULL(a.InformacoesAdicionais, ISNULL(dbo.fn_NotaFiscalItem_Mensagem(a.PedItemID), SPACE(0))) AS MensagemItem, ' +
                      'f.PartNumber AS PartNumber ' + 
                      'FROM NotasFiscais_Itens a WITH(NOLOCK) ' +
                            'INNER JOIN Conceitos b WITH(NOLOCK) ON a.ProdutoID = b.ConceitoID ' + 
                            'INNER JOIN Conceitos c WITH(NOLOCK) ON b.ProdutoID = c.ConceitoID ' + 
                            'LEFT  JOIN Conceitos f WITH(NOLOCK) ON a.ProdutoID = f.ConceitoID ' +
                            'LEFT OUTER JOIN NotasFiscais_Itens_Impostos d WITH(NOLOCK) ON d.NFItemID = a.NFItemID AND d.ImpostoID = 9' + //ICMS
                            'LEFT OUTER JOIN NotasFiscais_Itens_Impostos e WITH(NOLOCK) ON e.NFItemID = a.NFItemID AND e.ImpostoID = 8 ' + //IPI
                      'WHERE a.Tipo = \'P\'' + ' AND a.NotaFiscalID = ' + nNotaCorrente;

    dsoDetail02.ondatasetcomplete = dsoNotaFiscal_DSC;
    dsoDetail02.refresh();

    // Detalhe 3 - Servico
    setConnection(dsoDetail03);

    dsoDetail03.SQL = 'SELECT Itens.NotaFiscalID, STR(Itens.ProdutoID) AS ItemID, Itens.Familia AS Produto, Itens.Marca, Itens.Modelo, ' +
                      '(CASE WHEN NF.EmpresaID = 4 THEN ISNULL(LEFT(Itens.Descricao,35), SPACE(1)) ELSE ISNULL(LEFT(Itens.Descricao,28), SPACE(1)) END) AS Descricao, ' +
                      'ISNULL(Itens.CFOP, SPACE(1)) AS CFOP, ISNULL(Itens.CodigoListaServico, SPACE(1)) AS CodigoListaServico, ' +
                      'Itens.Unidade, STR(Itens.Quantidade) AS Quantidade, ' +
                      'ISNULL(Itens.InformacoesAdicionais, ISNULL(dbo.fn_NotaFiscalItem_Mensagem(Itens.PedItemID), SPACE(0))) AS PaisOrigem, ' +
				      '(CASE WHEN (dbo.fn_Produto_PesosMedidas(Itens.ProdutoID, 0, 1)) > 0 THEN \'Net(Kg): \' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),Itens.Quantidade * dbo.fn_Produto_PesosMedidas(Itens.ProdutoID, 0, 1))) ELSE SPACE(0) END) AS PesoLiquido, ' +
                      '(CASE WHEN (dbo.fn_Produto_PesosMedidas(Itens.ProdutoID, 1, 1)) > 0 THEN \'Gross(Kg): \' + CONVERT(VARCHAR(32),CONVERT(NUMERIC(10,2),Itens.Quantidade * dbo.fn_Produto_PesosMedidas(Itens.ProdutoID, 1, 1))) ELSE SPACE(0) END) AS PesoBruto, ' +
                      'Itens.ValorUnitario, Itens.ValorTotal AS ValorTotal ' +
                      'FROM NotasFiscais_Itens Itens WITH(NOLOCK) ' +
                        'INNER JOIN NotasFiscais NF WITH(NOLOCK) ON (NF.NotaFiscalID = Itens.NotaFiscalID AND NF.EstadoID=67) ' +
                      'WHERE Tipo = \'S\'' + ' AND Itens.NotaFiscalID = ' + nNotaCorrente;
                      
    dsoDetail03.ondatasetcomplete = dsoNotaFiscal_DSC;
    dsoDetail03.refresh();

    // Detalhe 4 - Mensagem
    setConnection(dsoDetail04);
    
    dsoDetail04.SQL = 'SELECT NotaFiscalID, Mensagem AS Mensagem ' +
                      'FROM NotasFiscais_Mensagens WITH(NOLOCK) ' +
                      'WHERE NotaFiscalID = ' + nNotaCorrente + ' ' +
                      'ORDER BY Ordem';
                      
    dsoDetail04.ondatasetcomplete = dsoNotaFiscal_DSC;
    dsoDetail04.refresh();
}

/********************************************************************
Criado pelo programador
Controla o retorno dos dsos da nota e inicia a impressao da nota

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoNotaFiscal_DSC()
{
    glb_ndsosNota--;
    
    if (glb_ndsosNota > 0)
        return null;

    printInvoice();
}

/********************************************************************
Criado pelo programador
Imprime a Nota Fiscal no Formulario Modelo 3: Formato Letter (PrintJet)
Utilizado pelas empresas do EUA

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function printInvoice()
{
	var nEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	
	setConnection(dsoExtra01);                              
      dsoExtra01.SQL = 'SELECT UPPER(Empresa.Nome) AS Nome, ' +
        'ISNULL(Empresa.Numero + SPACE(1), SPACE(0)) + ISNULL(Empresa.Endereco + SPACE(1), SPACE(0)) + ' +
        'ISNULL(Empresa.Complemento + SPACE(1), SPACE(0)) + ISNULL(Empresa.Bairro + SPACE(1), SPACE(0)) AS End1, ' +
        'ISNULL(Empresa.Cidade + SPACE(1), SPACE(0)) + ISNULL(Empresa.UF + SPACE(1), SPACE(0)) + ' +
        'ISNULL(Empresa.Pais + SPACE(1), SPACE(0)) AS End2, ' +
        'ISNULL(Empresa.CEP, SPACE(0)) AS CEP, ' +
        'ISNULL(Empresa.DDI, SPACE(0)) + ISNULL(Empresa.DDD, SPACE(0))+ SPACE(1) + ISNULL(Empresa.Telefone, SPACE(0)) AS Telefone, ' +
        'ISNULL(Empresa.DDI, SPACE(0)) + ISNULL(Empresa.DDD, SPACE(0))+ SPACE(1) + ISNULL(Empresa.Fax, SPACE(0)) AS Fax, Empresa.Fax AS nFax, ' +
        'SUBSTRING(dbo.fn_Pessoa_URL(Empresa.PessoaID, 125, NULL), 8, 72) AS URL, ' +
        'dbo.fn_Pessoa_URL(Empresa.PessoaID, 124, NULL) AS Email ' +
      'FROM NotasFiscais_Pessoas Empresa WITH(NOLOCK) ' +
      'WHERE (Empresa.TipoID = 790 AND Empresa.NotaFiscalID = ' + glb_nNotaFiscalID + ') ';	

    dsoExtra01.ondatasetcomplete = printInvoice_ShipTo;
    dsoExtra01.refresh();
}

/********************************************************************
Criado pelo programador
Dados do ShipTo para
imprimir a Nota Fiscal no Formulario Modelo 3: Formato Letter (PrintJet)
Utilizado pelas empresas do EUA

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function printInvoice_ShipTo()
{	
    setConnection(dsoShipTo);
		                              
    dsoShipTo.SQL = 'SELECT NFPessoa.PessoaID, NFPessoa.Nome, NFPessoa.DocumentoFederal AS Documento1, NFPessoa.DocumentoEstadual AS Documento2, NFPessoa.Pais, NFPessoa.CEP, NFPessoa.UF, NFPessoa.Cidade, ' +
					    'NFPessoa.Bairro, ' +
					    '(CASE ISNULL(Localidades.EnderecoInvertido, 0) WHEN 1 ' + 
                            'THEN (ISNULL(NFPessoa.Numero, SPACE(0)) + \' \' + ISNULL(NFPessoa.Endereco, SPACE(0)) +  \' \' + ISNULL(NFPessoa.Complemento, SPACE(0))) ' +
                            'ELSE (ISNULL(NFPessoa.Endereco, SPACE(0)) +  \' \' + ISNULL(NFPessoa.Numero, SPACE(0)) + \' \' + ISNULL(NFPessoa.Complemento, SPACE(0))) END)  AS Endereco, ' +
					    '(NFPessoa.DDI + NFPessoa.DDD + \' \' + NFPessoa.Telefone) AS Telefone ' +
					    'FROM NotasFiscais_Pessoas NFPessoa WITH(NOLOCK) ' +
                            'INNER JOIN Localidades Localidades WITH(NOLOCK) ON (NFPessoa.PaisID = Localidades.LocalidadeID) ' +
					    'WHERE NFPessoa.NotaFiscalID =' + glb_nNotaFiscalID + ' AND NFPessoa.TipoID = 792';

    dsoShipTo.ondatasetcomplete = printInvoice_DSC;
    dsoShipTo.refresh();
}

function printInvoice_DSC(bSecCopy)
{
    var nTransacaoID;
    var bCustomsWarehouse = false;
    var term_Acquire_SoldTo = 'Sold to:';
    var mensagem;
    var MensagemInvoice;
    var mensagem2;

    nTransacaoID = dsoHeader.recordset['TransacaoID'].value;

    // Proforma Invoice (Customs Warehouse)
    // if ((nTransacaoID == 461) || (nTransacaoID == 462)) Deletamos essas transacaoes em 20/02/2015 - Eduardo pediu
    //    bCustomsWarehouse = true;

    jPrinter.ResetReport(1);
    jPrinter.LandScape = false;
    setPrinterPaperParams(jPrinter);

    // Direitos
    jPrinter.RightExcel = false;

    jPrinter.SetBandHeight('T',97);
    jPrinter.SetBandHeight('PH',0);
    jPrinter.SetBandHeight('M',248);

    jPrinter.SetBandHeight('DT1', 7);
    jPrinter.SetBandHeight('DH1', 14);
    jPrinter.SetBandHeight('D1', 20.5);
    jPrinter.SetBandHeight('DF1', 0);

    jPrinter.SetBandHeight('DT2', 0);
    jPrinter.SetBandHeight('DH2', 0);
    jPrinter.SetBandHeight('D2', 20.5);
    jPrinter.SetBandHeight('DF2',0);
    jPrinter.SetBandHeight('PF', 280);
    jPrinter.BandaTotal_AlignToBottom(true);
    
    jPrinter.ShowLineInBand(1,'T',0X000000,3,'B');

    jPrinter.Dominium = SYS_ASPURLROOT;
    jPrinter.StrConn = getStrConnection();
     
    if (bCustomsWarehouse)
        jPrinter.TitleEx('PROFORMA INVOICE', true, 16, 0, 'R', 68);
    else
        jPrinter.TitleEx('COMMERCIAL INVOICE', true, 16, 0, 'R', 68);
    
	jPrinter.Show_Fld_EmpresaLogada_TitleBar(false);
	jPrinter.Show_Fld_DateTime_TitleBar(false);
		
    jPrinter.EmpresaLogada = '';
	jPrinter.MasterQuerySQL = dsoHeader.SQL;
	jPrinter.MasterIndexFieldName = 'NotaFiscalID';
        
    jPrinter.AddImageFieldFromBandToBand('M', 'T', 'FotoEmpresa', 40, 0, 0, 0, true, false);
    
    // Titulo 1
    var nTop = 2;
    var nTop2 = 0;
    var nLeft = 135 - 14;
    var nReplicateQty = 0;
    
    var sCEP = trimStr(dsoExtra01.recordset['CEP'].value);
    sCEP = sCEP.substr(0,5) + '-' + sCEP.substr(5);

    var sTelefone = trimStr((stripNonNumericChars(dsoExtra01.recordset['Telefone'].value)).substr(1));

    if (dsoExtra01.recordset['nFax'].value != null)
    {
        var sFax = trimStr((stripNonNumericChars(dsoExtra01.recordset['Fax'].value)).substr(1));

        sFax = 'Fax ' + sFax.substr(0, 3) + '-' + sFax.substr(3, (sFax).length - 7) + '-' +
		    sFax.substr(3 + (sFax).length - 7);
    }
    else
        var sFax = '';
        
    sTelefone = 'Phone ' + sTelefone.substr(0, 3) + '-' + sTelefone.substr(3, (sTelefone).length - 7) + '-' +
		sTelefone.substr(3 + (sTelefone).length - 7);
    
    jPrinter.AddTextInBand(dsoExtra01.recordset['Nome'].value, nTop, 0, 'M', true, 12, 'E');
    jPrinter.AddTextInBand(dsoExtra01.recordset['End1'].value, nTop+=5, 0, 'M', true, 8, 'E');
    jPrinter.AddTextInBand(dsoExtra01.recordset['End2'].value + ' ' + sCEP, nTop+=4, 0, 'M', true, 8, 'E');
    jPrinter.AddTextInBand(sTelefone + '     ' + sFax, nTop+=4, 0, 'M', true, 8, 'E');
    jPrinter.AddTextInBand(dsoExtra01.recordset['URL'].value + '     ' + 
		dsoExtra01.recordset['EMail'].value, nTop+=4, 0, 'M', true, 8, 'E');

    nTop = 2;

    if (bCustomsWarehouse)
        jPrinter.AddTextInBand('Proforma Invoice:', nTop, nLeft, 'M', true, 12, 'E');
    else
        jPrinter.AddTextInBand('Invoice:', nTop, nLeft, 'M', true, 12, 'E');

    jPrinter.AddTextInBand(padL(dsoHeader.recordset['NotaFiscal'].value, 6, '0'), nTop, nLeft + 50, 'M', true, 12, 'D');
    jPrinter.AddTextInBand('Date:', nTop+=5, nLeft, 'M', false, 10, 'E');
    jPrinter.AddTextInBand(dsoHeader.recordset['dtNotaFiscal'].value, nTop, nLeft + 50, 'M', false, 10, 'D');
    jPrinter.AddTextInBand('Order:', nTop+=4, nLeft, 'M', false, 10, 'E');
    jPrinter.AddTextInBand(padL(dsoHeader.recordset['PedidoID'].value, 10, ' '), nTop, nLeft + 50, 'M', false, 10, 'D');
    jPrinter.AddTextInBand('Salesperson:', nTop+=4, nLeft, 'M', false, 10, 'E');
    jPrinter.AddTextInBand(dsoHeader.recordset['Vendedor'].value, nTop, nLeft + 50, 'M', false, 10, 'D');
    jPrinter.AddTextInBand('Transaction:', nTop+=4, nLeft, 'M', false, 10, 'E');

    if (bCustomsWarehouse)
        jPrinter.AddTextInBand(dsoHeader.recordset['Natureza'].value, nTop, nLeft + 50, 'M', false, 7, 'D');
    else            
        jPrinter.AddTextInBand(dsoHeader.recordset['Natureza'].value, nTop, nLeft + 50, 'M', false, 10, 'D');
    
	jPrinter.ShowLineInBand(1,'M',0X000000,2,'N',nTop+71);
    nTop2 = nTop;
    
    if (bCustomsWarehouse)
        term_Acquire_SoldTo = 'Mecadoria, sem cobertura cambial, destinada ao EADI:';
    else
    {
        term_Acquire_SoldTo = 'Sold to:';
    
        if ( (dsoHeader.recordset['PessoaID'].value == 2) ||
		     (dsoHeader.recordset['PessoaID'].value == 4) ||
		     (dsoHeader.recordset['PessoaID'].value == 10) ||
		     (dsoHeader.recordset['PessoaID'].value == 27682) ||
		     (dsoHeader.recordset['PessoaID'].value == 32078) ||
		     (dsoHeader.recordset['PessoaID'].value == 14121) ||
		     (dsoHeader.recordset['PessoaID'].value == 42896) ||
		     (dsoHeader.recordset['PessoaID'].value == 57723) ||
		     (dsoHeader.recordset['PessoaID'].value == 57725) ||
		     (dsoHeader.recordset['PessoaID'].value == 64006) )
		    term_Acquire_SoldTo = 'Sold to (Acquire):';
    }
//SOLD TO    
    jPrinter.AddTextInBand(term_Acquire_SoldTo, nTop+=6, 0, 'M', false, 7, 'E');
    jPrinter.AddTextInBand(dsoHeader.recordset['Nome'].value, nTop+=4, 0, 'M', false, 10, 'E');
    
    var sAddress1 = dsoHeader.recordset['Endereco'].value + ' ' + dsoHeader.recordset['Bairro'].value;
    var sAddress2 = '';
    
    if ( (trimStr(sAddress1)).length > 55 )
    {
        sAddress1 = dsoHeader.recordset['Endereco'].value;
        sAddress2 = dsoHeader.recordset['Bairro'].value + ' ' + dsoHeader.recordset['Cidade'].value + ' ' + 
		dsoHeader.recordset['UF'].value + ' ' + dsoHeader.recordset['CEP'].value;
	}
	else
	{
        sAddress2 = dsoHeader.recordset['Cidade'].value + ' ' +	dsoHeader.recordset['UF'].value + ' ' + 
			dsoHeader.recordset['CEP'].value;
	}
    
    jPrinter.AddTextInBand(sAddress1, nTop+=4, 0, 'M', false, 10, 'E');
	jPrinter.AddTextInBand(sAddress2, nTop+=4, 0, 'M', false, 10, 'E');		
	jPrinter.AddTextInBand(dsoHeader.recordset['Pais'].value, nTop+=4, 0, 'M', false, 10, 'E');		

	var sPessoaIDDocumentoID = trimStr(dsoHeader.recordset['PessoaID'].value);
    
	if (trimStr(dsoHeader.recordset['Documento1'].value).length >= 5)
	sPessoaIDDocumentoID += '  Doc: ' + dsoHeader.recordset['Documento1'].value;
    
    if (!bCustomsWarehouse)
        jPrinter.AddTextInBand(dsoHeader.recordset['Telefone'].value, nTop+=4, 0, 'M', false, 10, 'E');

    jPrinter.AddTextInBand('ID: ' + sPessoaIDDocumentoID, nTop+=4, 0, 'M', false, 10, 'E');

    if (bCustomsWarehouse)
        jPrinter.AddTextInBand('De acordo com a Instru��o Normativa 241 de 6 de novembro de 2002.', nTop+=4, 0, 'M', false, 7, 'E');
        

/*    jPrinter.AddTextInBand('ID: ' + sPessoaIDDocumentoID, nTop+=4, 0, 'M', false, 10, 'E');

    if (bCustomsWarehouse)
        jPrinter.AddTextInBand('De acordo com a Instru��o Normativa 241 de 6 de novembro de 2002.', nTop+=4, 0, 'M', false, 7, 'E');
    else            
        jPrinter.AddTextInBand(dsoHeader.recordset['Telefone'].value, nTop+=4, 0, 'M', false, 10, 'E');
*/
	nTop = nTop2;
	nLeft=95;
	
	// Insere os dados do ShipTo para impressao
// SHIP TO	
	jPrinter.AddTextInBand('Ship to:', nTop+=6, nLeft, 'M', false, 7, 'E');
	// Se nao tem ShipTo na tabela NotasFiscais_ShipTo
	// usa o Pessoa do Pedido
	if (dsoShipTo.recordset.BOF && dsoShipTo.recordset.EOF)
	{
	    jPrinter.AddTextInBand(dsoHeader.recordset['Nome'].value, nTop+=4, nLeft, 'M', false, 10, 'E');
		jPrinter.AddTextInBand(sAddress1 , nTop+=4, nLeft, 'M', false, 10, 'E');
		jPrinter.AddTextInBand(sAddress2, nTop+=4, nLeft, 'M', false, 10, 'E');
		jPrinter.AddTextInBand(dsoHeader.recordset['Pais'].value, nTop+=4, nLeft, 'M', false, 10, 'E');
		jPrinter.AddTextInBand(dsoHeader.recordset['Telefone'].value, nTop+=4, nLeft, 'M', false, 10, 'E');
		jPrinter.AddTextInBand('ID: ' + sPessoaIDDocumentoID, nTop+=4, nLeft, 'M', false, 10, 'E');
	}
	else
	{
	    var sShipToAddress1 = dsoShipTo.recordset['Endereco'].value + ' ' + dsoShipTo.recordset['Bairro'].value;
		var sShipToAddress2 = '';
	    
		if ( (trimStr(sShipToAddress1)).length > 55 )
		{
		    sShipToAddress1 = dsoShipTo.recordset['Endereco'].value;
		    sShipToAddress2 = dsoShipTo.recordset['Bairro'].value + ' ' + dsoShipTo.recordset['Cidade'].value + ' ' + 
				dsoShipTo.recordset['UF'].value + ' ' + dsoShipTo.recordset['CEP'].value;
		}
		else
		{
		    sShipToAddress2 = dsoShipTo.recordset['Cidade'].value + ' ' +	dsoShipTo.recordset['UF'].value + ' ' + 
				dsoShipTo.recordset['CEP'].value;
		}
		
		var sShipToDocumentoID = trimStr((dsoShipTo.recordset['PessoaID'].value).toString());
    
		if (trimStr(dsoShipTo.recordset['Documento1'].value).length >= 5)
		sShipToDocumentoID += '  Doc: ' + dsoShipTo.recordset['Documento1'].value;
		
		jPrinter.AddTextInBand(dsoShipTo.recordset['Nome'].value, nTop+=4, nLeft, 'M', false, 10, 'E');
		jPrinter.AddTextInBand(sShipToAddress1 , nTop+=4, nLeft, 'M', false, 10, 'E');
		jPrinter.AddTextInBand(sShipToAddress2, nTop+=4, nLeft, 'M', false, 10, 'E');
		jPrinter.AddTextInBand(dsoShipTo.recordset['Pais'].value, nTop+=4, nLeft, 'M', false, 10, 'E');
		jPrinter.AddTextInBand(dsoShipTo.recordset['Telefone'].value, nTop+=4, nLeft, 'M', false, 10, 'E');
		jPrinter.AddTextInBand('ID: ' + sShipToDocumentoID, nTop+=4, nLeft, 'M', false, 10, 'E');
	}
		
	jPrinter.ShowLineInBand(2,'M',0X000000,2,'N',nTop + 156);

	nLeft = 0;
	jPrinter.AddTextInBand('Customer PO', nTop+=6, nLeft, 'M', false, 8, 'E');
	jPrinter.AddTextInBand(dsoHeader.recordset('SeuPedido'].value, nTop+4, nLeft, 'M', false, 10, 'E');

	jPrinter.AddTextInBand('Terms', nTop, nLeft+=30, 'M', false, 8, 'E');

    if (bCustomsWarehouse)
    {
        jPrinter.AddTextInBand('sem cobertura cambial', nTop+4, nLeft, 'M', false, 7, 'E');
    }
    else
    {
        if ( !((dsoDetail01.recordset.BOF)&&(dsoDetail01.recordset.EOF)) )
        {
            dsoDetail01.recordset.MoveFirst;
            jPrinter.AddTextInBand(dsoDetail01.recordset('Prazo'].value, nTop+4, nLeft, 'M', false, 10, 'E');
        }
    }
	jPrinter.AddTextInBand('Due Date', nTop, nLeft+=10, 'M', false, 8, 'E');

    if (!bCustomsWarehouse)
    {
        if ( !((dsoDetail01.recordset.BOF)&&(dsoDetail01.recordset.EOF)) )
        {
            dsoDetail01.recordset.MoveFirst;
            jPrinter.AddTextInBand(dsoDetail01.recordset['Vencimento'].value, nTop+4, nLeft, 'M', false, 10, 'E');
        }
    }

	jPrinter.AddTextInBand('Forwarder', nTop, nLeft+=21, 'M', false, 8, 'E');
	jPrinter.AddTextInBand(dsoHeader.recordset['Transportadora'].value, nTop+4, nLeft, 'M', false, 10, 'E');
	
	jPrinter.AddTextInBand('Ship Via', nTop, nLeft+=111, 'M', false, 8, 'E');
	jPrinter.AddTextInBand(dsoHeader.recordset['MeioTransporte'].value, nTop+4, nLeft, 'M', false, 10, 'E');

	jPrinter.AddTextInBand('Incoterm', nTop, nLeft+=12, 'M', false, 8, 'E');

    if (!bCustomsWarehouse)
	    jPrinter.AddTextInBand(dsoHeader.recordset['Incoterm'].value, nTop+4, nLeft, 'M', false, 10, 'E');

    // Produtos
    var nFontItensSize = 7;
    nLeft = 9;
    jPrinter.DetailQuerySQL01 = dsoDetail02.SQL;
    jPrinter.DetailIndexFieldName01 = 'NotaFiscalID';
    jPrinter.ShowLineInBand(1,'DT1',0X000000,2,'T');
    
    jPrinter.DetailQuerySQL02 = dsoDetail03.SQL;
    jPrinter.DetailIndexFieldName02 = 'NotaFiscalID';
    jPrinter.ShowLineInBand(1,'DT2',0X000000,2,'T');

    jPrinter.AddTextInBand('ID', 0, nLeft -= 7, 'DH1', true, nFontItensSize, 'E');
    jPrinter.AddFieldInBand('D1', 'ItemID', 0, nLeft-8.5 , false, 'D', nFontItensSize);
    jPrinter.AddFieldInBand('D2', 'ItemID', 0, nLeft-8.5 , false, 'D', nFontItensSize);

    jPrinter.AddTextInBand('Product', 0, nLeft+=9, 'DH1', true, nFontItensSize, 'E');
    jPrinter.AddFieldInBand('D1', 'Produto', 0, nLeft, false, 'E', nFontItensSize);
    jPrinter.AddFieldInBand('D1', 'Descricao', 2.7, nLeft, false, 'E', nFontItensSize);    
    jPrinter.AddFieldInBand('D2', 'Produto', 0, nLeft, false, 'E', nFontItensSize);
	jPrinter.AddFieldInBand('D2', 'Descricao', 2.7, nLeft, false, 'E', nFontItensSize);
	
	
    jPrinter.AddTextInBand('Brand', 0, nLeft+=40, 'DH1', true, nFontItensSize, 'E');
    jPrinter.AddFieldInBand('D1', 'Marca', 0, nLeft, false, 'E', nFontItensSize);
    jPrinter.AddFieldInBand('D2', 'Marca', 0, nLeft, false, 'E', nFontItensSize);
    
    jPrinter.AddTextInBand('Model', 0, nLeft+=24, 'DH1', true, nFontItensSize, 'E');
    jPrinter.AddFieldInBand('D1', 'Modelo', 0, nLeft, false, 'E', nFontItensSize);
    jPrinter.AddFieldInBand('D1', 'PaisOrigem', 2.6, nLeft, false, 'E', nFontItensSize-0.1);
    jPrinter.AddFieldInBand('D1', 'PesoLiquido', 2.6, nLeft + 74.5, false, 'E', nFontItensSize - 0.1);
    jPrinter.AddFieldInBand('D1', 'PesoBruto', 2.6, nLeft + 99, false, 'E', nFontItensSize - 0.1);
    jPrinter.AddFieldInBand('D2', 'Modelo', 0, nLeft, false, 'E', nFontItensSize);
    jPrinter.AddFieldInBand('D2', 'PaisOrigem', 2.6, nLeft, false, 'E', nFontItensSize-0.1);
    jPrinter.AddFieldInBand('D2', 'PesoLiquido', 2.6, nLeft + 74.5, false, 'E', nFontItensSize - 0.1);
    jPrinter.AddFieldInBand('D2', 'PesoBruto', 2.6, nLeft + 99, false, 'E', nFontItensSize - 0.1);

    jPrinter.AddTextInBand('Part Number', 0, nLeft += 34, 'DH1', true, nFontItensSize, 'E');
    jPrinter.AddFieldInBand('D1', 'PartNumber', 0, nLeft, false, 'E', nFontItensSize);
    jPrinter.AddFieldInBand('D2', 'PartNumber', 0, nLeft, false, 'E', nFontItensSize);
    
    jPrinter.AddTextInBand('Qty', 0, nLeft+=31, 'DH1', true, nFontItensSize, 'E');
    jPrinter.AddFieldInBand('D1', 'Quantidade', 0, nLeft-12, false, 'D', nFontItensSize);
    jPrinter.AddFieldInBand('D2', 'Quantidade', 0, nLeft-12, false, 'D', nFontItensSize);
    
    jPrinter.AddTextInBand('Price (US$)', 0, nLeft+=9.5, 'DH1', true, nFontItensSize, 'E');
    jPrinter.AddFieldInBand('D1', 'ValorUnitario', 0, nLeft-2, false, 'D', nFontItensSize);
    jPrinter.AddFieldInBand('D2', 'ValorUnitario', 0, nLeft-2, false, 'D', nFontItensSize);
    
    jPrinter.AddTextInBand('Extended (US$)', 0, nLeft+=24, 'DH1', true, nFontItensSize, 'E');
    jPrinter.AddFieldInBand('D1', 'ValorTotal', 0, nLeft+3, false, 'D', nFontItensSize);
    jPrinter.AddFieldInBand('D2', 'ValorTotal', 0, nLeft+3, false, 'D', nFontItensSize);

    jPrinter.ZebradoDetailEnabled01 = true;

    jPrinter.ZebradoDetailEnabled02 = true;

    var nSubTotalInvoice = 0;

    if ( (dsoHeader.recordset['ValorTotalProdutos'].value != null) &&
         (dsoHeader.recordset['ValorTotalServicos'].value != null) )
    {
        nSubTotalInvoice = dsoHeader.recordset['ValorTotalProdutos'].value +
            dsoHeader.recordset['ValorTotalServicos'].value;
    }
    else if (dsoHeader.recordset['ValorTotalProdutos'].value != null)
    {
        nSubTotalInvoice = dsoHeader.recordset['ValorTotalProdutos'].value;
    }
    else if (dsoHeader.recordset['ValorTotalServicos'].value != null)
    {
        nSubTotalInvoice = dsoHeader.recordset['ValorTotalServicos'].value;
    }

	var nFontFooterSize = 8;
	var nTopFooter = 2;
	var nTopMsg = 0;
    // Observa��es
    jPrinter.ShowLineInBand(1,'PF',0X000000,2,'T');
    //jPrinter.AddFieldFromBandToBand('M','PF','Observacoes', 5, 0, false, 'E', 10, false, true);

    if (!bCustomsWarehouse)
	    jPrinter.AddTextInBand('Customer agrees to the following:',nTopFooter,0,'PF',true,9,'E');

	jPrinter.AddTextInBand('Sub Total (US$)',nTopFooter,160 - 10,'PF',false,nFontFooterSize,'E');


    if (!bCustomsWarehouse)
        jPrinter.AddTextInBand('- 25% restocking fee on all returned items. NO CASH REFUNDS. Fee of $25.00 for all returned checks.', nTopFooter += 4, 0, 'PF', false, 8, 'E');
    else
        nTopFooter+=4;

	jPrinter.AddTextInBand(padL(formatNumber(nSubTotalInvoice, 2),14,' '),nTopFooter,183 - 14,'PF',false,9,'D');

    if (!bCustomsWarehouse)
    {
	    jPrinter.AddTextInBand('- All return items must be in original packaging and with invoice only.',nTopFooter+=4,0,'PF',false,8,'E');
	    jPrinter.AddTextInBand('- All merchandise carries manufacturer�s warranty only.', nTopFooter += 4, 0, 'PF', false, 8, 'E');
	    jPrinter.AddTextInBand('- Terms and Conditions printed on the back of this page.', nTopFooter += 4, 0, 'PF', false, 8, 'E');
	    
    }
    else
        nTopFooter+=8;


    jPrinter.AddTextInBand('Tax (US$)', nTopFooter, 160 - 10, 'PF', false, nFontFooterSize, 'E');
    jPrinter.AddTextInBand(padL(formatNumber(dsoHeader.recordset['ValorTotalImposto2'].value, 2), 14, ' '), nTopFooter + 2, 183 - 14, 'PF', false, 9, 'D');
        //jPrinter.AddTextInBand('Details of the goods see attached.',nTopFooter+=4,0,'PF',false,8,'E'); Marco pediu para retirar esta linha.
	
	nTopMsg = nTopFooter + 4;

	jPrinter.AddTextInBand('Shipping (US$)', nTopFooter += 12, 160 - 10, 'PF', false, nFontFooterSize, 'E');
	jPrinter.AddTextInBand(padL(formatNumber(dsoHeader.recordset['ValorFrete'].value, 2), 14, ' '), nTopFooter += 4, 183 - 14, 'PF', false, 9, 'D');	

    if (bCustomsWarehouse)
	    jPrinter.AddTextInBand('Total Proforma Invoice (US$)',nTopFooter += 8,160 - 10,'PF',true,9,'E');
    else
	    jPrinter.AddTextInBand('Total Invoice (US$)',nTopFooter += 8,160 - 10,'PF',true,9,'E');

	jPrinter.AddTextInBand(padL(formatNumber(dsoHeader.recordset['ValorTotalNota'].value, 2),14,' '),nTopFooter+4,183 - 14,'PF',true,9,'D');
	
	jPrinter.ShowLineInBand(2,'PF',0X000000,1,'N',nTopFooter+149);
	
	if (bSecCopy)
	{
        if (bCustomsWarehouse)
	        jPrinter.AddTextInBand('I declare that all the information contained the proforma invoice is true and correct:',nTopFooter+=11,0,'PF',false,9,'E');
        else	        
	        jPrinter.AddTextInBand('I declare that all the information contained the invoice is true and correct:',nTopFooter+=11,0,'PF',false,9,'E');
    }
    else
        jPrinter.AddTextInBand('Received above merchandise in good condition:',nTopFooter+=11,0,'PF',false,9,'E');	    
        
	jPrinter.AddTextInBand('Date: ___/___/______    Signature: _________________________________    Name: _________________________________',nTopFooter+=8,0,'PF',false,9,'E');
	
	if (jPrinter.PaperType == 'A4')
		nReplicateQty = 129;
	else
		nReplicateQty = 126;

    jPrinter.AddTextInBand(replicate('_', nReplicateQty),nTopFooter+=8,0,'PF',true,nFontFooterSize,'E');
    
	
	jPrinter.AddTextInBand('Issued by electronic process',nTopFooter+=8,0,'PF',false,nFontFooterSize,'E');
	
	if ( bSecCopy )
		jPrinter.AddTextInBand('CUSTOMER',nTopFooter,90 - 7,'PF',true,nFontFooterSize,'E');
	else
		jPrinter.AddTextInBand('ORIGINAL',nTopFooter,90 - 7,'PF',true,nFontFooterSize,'E');	


    // Detalhe 4 - Mensagem
    if ( !((dsoDetail04.recordset.BOF)&&(dsoDetail04.recordset.EOF)) )
    {
        dsoDetail04.recordset.MoveFirst;
    }

    for (i = 1; i <= 7; i++) {
        if (!dsoDetail04.recordset.EOF) {
            mensagem = dsoDetail04.recordset['Mensagem'].value;

            if (i == 1) {
                MensagemInvoice = dsoHeader.recordset['ObservacoesInvoice'].value + '.';
                mensagem2 = '*' + trimStr(mensagem);

                if (MensagemInvoice == mensagem2) {
                    jPrinter.AddTextInBand(mensagem, nTopMsg += 2, 0, 'PF', false, 8, 'E');
                    jPrinter.AddTextInBand(' ', nTopMsg += 2, 0, 'PF', false, 9, 'E');
                    dsoDetail04.recordset.MoveNext();
                    i++;
                }
                else {
                    jPrinter.AddTextInBand(mensagem, nTopMsg , 0, 'PF', false, 8, 'E');
                    dsoDetail04.recordset.MoveNext();
                }
            }
            else {
                dsoDetail04.recordset.MoveNext();

                if (!dsoDetail04.recordset.EOF)
                    jPrinter.AddTextInBand(mensagem, nTopMsg += 4, 0, 'PF', false, 8, 'E');
                else {
                    jPrinter.AddTextInBand(' ', nTopMsg += 2, 0, 'PF', false, 9, 'E');
                    jPrinter.AddTextInBand(mensagem, nTopMsg += 5, 0, 'PF', false, 8, 'E');
                    i++;
                }
            }
        }
    }

    jPrinter.MailSubject = '';

	if ( glb_Print_2nd_Copy == null )
		glb_Print_2nd_Copy = true;
	else
		glb_Print_2nd_Copy = false;

    jPrinter.Print(false, false, true);
}

/****************************************************************************
FINAL DAS FUNCOES DE IMPRESSAO DE NOTA FISCAIS
****************************************************************************/

/****************************************************************************
INICIO DAS FUNCOES IMPRESSAO DE LISTA DE EMBALAGEM
****************************************************************************/
/* Desabilitado impress�o da Lista de Embalagem no avan�o do estado de Faturamento para Expedi��o. Verificado com Camilo Rodrigues e Eder Silva que
   essa fun��o n�o � mais utilizada - MTH 07/12/2017

function listaEmbalagem(sReportTitle, nPedidoID, sEmpresaFantasia, bPrintPreview, dirA1, dirA2)
{
	var glb_aTranslate_Proposta = null;
	var srtSQL = '';

    // Array de dicionario usado para traduzir proposta/pedido
    aTranslate_Proposta = [['Lista de Embalagem','Packing List'],
    						['Pedido','Order'],
							['NF','Invoice'],
							['Produto','Product'],
							['Qtd','Qty'],
							['N�mero S�rie','Serial Number'],
							['Data','Date'],
							['Fornecedor','Supplier'],
							['Documento','Document'],
							['Total Itens','Total Items']];

    asort(aTranslate_Proposta, 0);

    jPrinter.ResetReport(1);
    jPrinter.LandScape = false;
    setPrinterPaperParams(jPrinter);
    
    if ((dirA1 != null) && (dirA1 != null))
    {
        //DireitoEspecifico
        //Pedidos->Saidas->SUP->Modal Print
        //Relatorio: 40123 -> Lista de Embalagem
        //A1||A2 -> Impressao
        //A2 -> Zebrar
        //A1&&A2 -> Excel
        //desabilitado -> Word        
    
		jPrinter.RightPrint = ((dirA1)||(dirA2));
		jPrinter.RightZebrar = (dirA2);
		jPrinter.RightExcel = ((dirA1)&&(dirA2));
		jPrinter.RightWord = false;
    }

    // A chamada destes metodos sao necessarias porque
    // pode ter havido uma impressao previa de NotaFiscal
    // tipo AllPlus
    jPrinter.Show_Fld_EmpresaLogada_TitleBar(true);
	jPrinter.Show_Fld_DateTime_TitleBar(true);
	jPrinter.ShowImage1_PageTitle(0, 0, 0, 0, false);
	jPrinter.BandaTotal_AlignToBottom(false);
    
    jPrinter.Dominium = SYS_ASPURLROOT;
    jPrinter.StrConn = getStrConnection();
    jPrinter.SetBandHeight('T',20);
    jPrinter.SetBandHeight('PH',30);
    jPrinter.SetBandHeight('M',14);
    
    // Usado o metodo TitleEx porque
    // pode ter havido uma impressao previa de NotaFiscal
    // tipo AllPlus
    // jPrinter.Title = sReportTitle;
    
    jPrinter.TitleEx(translateTherm(sReportTitle, aTranslate_Proposta), true, 12, 0X800000, 'C', 1);

    jPrinter.EmpresaLogada = sEmpresaFantasia;

    jPrinter.MasterQuerySQL = 
        'SELECT Produtos.ConceitoID, Produtos.Conceito, ' +
        '(SELECT SUM(Quantidade) FROM Pedidos_Itens WITH(NOLOCK) WHERE(Pedidos.PedidoID = PedidoID AND Produtos.ConceitoID = ProdutoID)) AS Quantidade, ' +
			'ISNULL(dbo.fn_Produto_MensagemBeneficio (Itens.ProdutoID, dbo.fn_Pessoa_Localidade(Pedidos.PessoaID, 2,NULL,NULL), GETDATE()), SPACE(0)) AS LegislacaoAplicavel, ' +
            'dbo.fn_Pad(NotasFiscais.NotaFiscal,6,' + '\'' + '0' + '\'' + ',' + '\'' + 'L' + '\'' + ') AS NotaFiscal, NotasFiscais.dtNotaFiscal ' +
	    'FROM Pedidos WITH(NOLOCK) ' +
			'LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (Pedidos.NotaFiscalID=NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) ' +
	        'INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (Pedidos.PedidoID=Itens.PedidoID) ' +
	        'INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (Itens.ProdutoID = ProdutosEmpresa.ObjetoID AND Pedidos.EmpresaID = ProdutosEmpresa.SujeitoID) ' +
			'INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (ProdutosEmpresa.ObjetoID = Produtos.ConceitoID) ' +
	    'WHERE (Pedidos.PedidoID = ' + nPedidoID + ' AND ' +
	        'ProdutosEmpresa.TipoRelacaoID = 61 AND Produtos.ProdutoSeparavel = 1) ' +
		'GROUP BY Pedidos.PedidoID, Produtos.ConceitoID, Produtos.Conceito, Produtos.LegislacaoAplicavel, ' +
            'NotasFiscais.NotaFiscal, NotasFiscais.dtNotaFiscal, Pedidos.PessoaID, Itens.ProdutoID '  +
	    'ORDER BY (SELECT TOP 1 Ordem ' +
				'FROM Pedidos_Itens  WITH(NOLOCK) ' +
				'WHERE (Produtos.ConceitoID = ProdutoID AND Pedidos.PedidoID = PedidoID) ' +
				'ORDER BY Ordem), Produtos.ConceitoID';

    jPrinter.MasterIndexFieldName = 'ConceitoID';

    srtSQL = 'SELECT Itens.ProdutoID, NumerosSerie.NumeroSerie AS NumeroSerie, ' +
			'dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, NumerosSerie.NumeroSerie, 1) AS NotaFiscal, ' + 
			'dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, NumerosSerie.NumeroSerie, 2) AS Data, ' + 
			'dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, NumerosSerie.NumeroSerie, 3) AS Fornecedor, ' + 
			'dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, NumerosSerie.NumeroSerie, 4) AS Documento, ' + 
        	'1 AS Quantidade ' +
	'FROM Pedidos WITH(NOLOCK) ' +
	    'INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (Pedidos.PedidoID = Itens.PedidoID) ' +
	    'INNER JOIN NumerosSerie_Movimento Movimentos WITH(NOLOCK) ON (Itens.PedidoID = Movimentos.PedidoID) ' +
		'INNER JOIN NumerosSerie WITH(NOLOCK) ON (Movimentos.NumeroSerieID = NumerosSerie.NumeroSerieID AND Itens.ProdutoID = NumerosSerie.ProdutoID) ' +
	'WHERE (Pedidos.PedidoID = ' + nPedidoID + ') ' +
		'GROUP BY Pedidos.EmpresaID, Pedidos.PedidoID, Itens.ProdutoID, NumeroSerie ';
	//INICIO DE NOVO NS----------------------------------------------	
	srtSQL += 'UNION ALL ' +
			'SELECT Itens.ProdutoID, Movimentos.Lote AS NumeroSerie, ' +
			'dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, Movimentos.Lote, 1) AS NotaFiscal, ' + 
			'dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, Movimentos.Lote, 2) AS Data, ' + 
			'dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, Movimentos.Lote, 3) AS Fornecedor, ' + 
			'dbo.fn_Produto_Fornecedor(Pedidos.EmpresaID, Itens.ProdutoID, Movimentos.Lote, 4) AS Documento, ' + 
        	'1 AS Quantidade ' +
			'FROM Pedidos WITH(NOLOCK), Pedidos_Itens Itens WITH(NOLOCK), Pedidos_ProdutosLote Movimentos WITH(NOLOCK) ' +
			'WHERE (Pedidos.PedidoID = ' + nPedidoID + ' AND Pedidos.PedidoID = Itens.PedidoID AND Pedidos.PedidoID = Movimentos.PedidoID AND Movimentos.ProdutoID = itens.ProdutoID) ' +
			'GROUP BY Pedidos.EmpresaID, Pedidos.PedidoID, Itens.ProdutoID, Movimentos.Lote ' +
			'ORDER BY Itens.ProdutoID, NumeroSerie';
	//FIM DE NOVO NS----------------------------------------------
				
//				'ORDER BY (SELECT TOP 1 Ordem ' +
//				'FROM Pedidos_Itens WITH(NOLOCK) ' +
//				'WHERE (Itens.ProdutoID = ProdutoID AND Pedidos.PedidoID = PedidoID) ' +
//				'ORDER BY Ordem), Itens.ProdutoID, NumeroSerie';
	
    jPrinter.DetailQuerySQL01 = srtSQL;
    
    jPrinter.DetailIndexFieldName01 = 'ProdutoID';
        
    var lUseBold = true;
    var nFontTitleSize = 8;
    var lUseBoldSubDetail = false;
    var nLeft=0;
    var nTop = 0;

    jPrinter.ShowLineInBand(1,'PH',0X000000,2,'N',12);
    
    jPrinter.AddTextInBand(translateTherm('Pedido', aTranslate_Proposta) + ' ' +nPedidoID, 0, 0, 'PH', true, 8, 'E');
    jPrinter.AddTextInBand(translateTherm('NF', aTranslate_Proposta) + ' ', 0, 28, 'PH', true, 8, 'E');
    jPrinter.AddFieldFromBandToBand('M', 'PH', 'NotaFiscal', 0, 29 + (translateTherm('NF', aTranslate_Proposta).length) * 2, true, 'E', 8);
    jPrinter.AddFieldFromBandToBand('M', 'PH', 'dtNotaFiscal', 0, 44 + (translateTherm('NF', aTranslate_Proposta).length) * 2, true, 'E', 8);

    jPrinter.AddColumnHeader('Item',nTop+15,nLeft,true,nFontTitleSize);
    jPrinter.AddOrdemInBand(nTop,nLeft-9,'M',true,nFontTitleSize,'D',false);

    jPrinter.AddColumnHeader('ID',nTop+15,nLeft+=8,lUseBold,nFontTitleSize);
    jPrinter.AddFieldInBand('M','ConceitoID',nTop,nLeft,true,'E',nFontTitleSize);
    
    jPrinter.AddColumnHeader(translateTherm('Produto', aTranslate_Proposta),nTop+15,nLeft+=12,lUseBold,nFontTitleSize);
    jPrinter.AddFieldInBand('M','Conceito',nTop,nLeft,true,'E',nFontTitleSize);
    
    jPrinter.AddColumnHeader(translateTherm('Qtd', aTranslate_Proposta),nTop+15,nLeft+=34,lUseBold,nFontTitleSize);
    jPrinter.AddFieldInBand('M','Quantidade',nTop,nLeft-10,true,'D',nFontTitleSize);

    jPrinter.AddFieldInBand('M','LegislacaoAplicavel',nTop,nLeft+68,true,'E',nFontTitleSize);
    
    // jPrinter.AddColumnHeader('Item',nTop+15,nLeft+=7,true,nFontTitleSize);
    jPrinter.AddOrdemInBand(nTop,nLeft-10,'D1',lUseBoldSubDetail,nFontTitleSize,'D',false);
    jPrinter.AddTotalColumns('Quantidade',nTop+1,nLeft-10,'D1', true, nFontTitleSize);
    
    jPrinter.AddColumnHeader(translateTherm('N�mero S�rie', aTranslate_Proposta),nTop+15,nLeft+=9,lUseBold,nFontTitleSize);
    jPrinter.AddFieldInBand('D1','NumeroSerie',nTop,nLeft,lUseBoldSubDetail,'E',nFontTitleSize);
    
    jPrinter.AddColumnHeader(translateTherm('NF', aTranslate_Proposta),nTop+15,nLeft+=28,lUseBold,nFontTitleSize);
    jPrinter.AddFieldInBand('D1','NotaFiscal',nTop,nLeft,lUseBoldSubDetail,'E',nFontTitleSize);
    
    jPrinter.AddColumnHeader(translateTherm('Data', aTranslate_Proposta),nTop+15,nLeft+=15,lUseBold,nFontTitleSize);
    jPrinter.AddFieldInBand('D1','Data',nTop,nLeft,lUseBoldSubDetail,'E',nFontTitleSize);
    
    jPrinter.AddColumnHeader(translateTherm('Fornecedor', aTranslate_Proposta),nTop+15,nLeft+=16,lUseBold,nFontTitleSize);
    jPrinter.AddFieldInBand('D1','Fornecedor',nTop,nLeft,lUseBoldSubDetail,'E',nFontTitleSize - 2);
    
    jPrinter.AddColumnHeader(translateTherm('Documento', aTranslate_Proposta),nTop+15,nLeft+=40,lUseBold,nFontTitleSize);
    jPrinter.AddFieldInBand('D1','Documento',nTop,nLeft,lUseBoldSubDetail,'E',nFontTitleSize);

    jPrinter.ShowLineInBand(1, 'PF', 0, 2, 'T');
                             
    jPrinter.setTotalLabel(translateTherm('Total Itens', aTranslate_Proposta),1,0,true,nFontTitleSize);
    
    jPrinter.ZebradoDetailEnabled01 = true;
    jPrinter.SetBandColor('PH', jPrinter.ZebradoColorDetail01);

    glb_bPrintPreview = bPrintPreview;
    
    glb_timerListaEmbalagem = window.setInterval('sendDataToPrinter()', 500, 'JavaScript');
}

function sendDataToPrinter()
{
	if (glb_timerListaEmbalagem != null)
	{
		window.clearInterval(glb_timerListaEmbalagem);
		glb_timerListaEmbalagem = null;
	}
	
	// Forcado aqui para sobrepassar automacao para impressoras HP
    jPrinter.MarginBotton = 7 + _MARGIN_BOTTON;
    jPrinter.MarginLeft = 8;//_MARGIN_LEFT;
    jPrinter.MarginRightPortrait = 8;//_MARGIN_RIGHT_PORTRAIT;

	if (glb_bPrintPreview)
        jPrinter.Print();
    else
        jPrinter.Print(false, false, true);
}

/****************************************************************************
FINAL DAS FUNCOES IMPRESSAO DE LISTA DE EMBALAGEM
****************************************************************************/

function formatNumber(nNumero, nDecimais)
{
	if ((nNumero == null) || (nNumero == ''))
		nNumero = 0;
	
	if ((nDecimais == null) || (nDecimais == '') || isNaN(nDecimais))
		nDecimais = 0;
		
	return oCNAB.FormatNumberX(parseFloat(nNumero), nDecimais);
}
