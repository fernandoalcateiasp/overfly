/********************************************************************
modalviewnfe.js

Library javascript para o modalviewnfe.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nUserID = 0;
var glb_sModeloNF = 0;
var glb_nNotaFiscal = 0;
var glb_PrefixoData = '';
var glb_Caminho_Data = '';
var glb_ArquivoDanfePDF = '';
var glb_ArquivoXMLNFe = '';
var glb_ChaveAcesso = 0;


var dsoXMLFileNFe = new CDatatransport('dsoXMLFileNFe');
var dsoEnviaNFe = new CDatatransport('dsoEnviaNFe');
var dsoModeloNF = new CDatatransport('dsoModeloNF');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
chks_onclick()
btn_onclick(ctl)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalviewnfeBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 0;
    modalFrame.style.left = 0;

    showExtFrame(window, true);
    modeloNF();
    window.focus();

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Visualizar NFe', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // ajusta o divFields
    elem = divFields;
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP - 100;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modWidth - 2 * ELEM_GAP - 6;
    }

    with (selDanfeXml.style) {
        left = 50;
        top = modHeight - 40;
    }

    with (btnOK.style) {
        left = 100;
        top = parseInt(selDanfeXml.style.top) + 1;
    }

    with (btnEnviar.style) {
        left = 195;
        top = parseInt(selDanfeXml.style.top) + 1;
    }

    btnCanc.style.visibility = 'hidden';
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    var nNotaFiscalID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'NotaFiscalID' + '\'' + '].value');
    nNotaFiscalID = (nNotaFiscalID == null ? glb_NotaFiscalID : nNotaFiscalID);

    // Imprime o documento exibido na modal.
    if (controlID == 'btnOK') {
        // Se estiver exibindo o DANFE, imprime o frame...
        setConnection(dsoXMLFileNFe);

        dsoXMLFileNFe.SQL =
			"SELECT ArquivoXMLNFe = CASE WHEN (a.EstadoID = 70 AND a.Emissor = 1) THEN a.ArquivoXMLNFeInutilizado WHEN (a.EstadoID = 7 AND a.Emissor = 1) THEN a.ArquivoXMLNFeCancelado ELSE a.ArquivoXMLNFe END, " +
				    "CaminhoEstado = CASE WHEN (a.EstadoID = 70 AND a.Emissor = 1) THEN 'Inutilizadas' WHEN (a.EstadoID = 7 AND a.Emissor = 1) THEN 'Canceladas' WHEN (a.EstadoID = 69 AND a.Emissor = 1) THEN 'Denegadas' ELSE 'Autorizadas' END, " +
				    "PrefixoData = CASE WHEN (a.TipoNotaFiscalID = 601 AND a.Emissor = 0) THEN 'Recebidas' ELSE 'Emitidas' END, " +
                    "CaminhoData = CASE WHEN (a.TipoNotaFiscalID = 601 AND a.Emissor = 0) THEN CONVERT(VARCHAR(4),YEAR(dbo.fn_Pedido_Datas(b.PedidoID, 8))) + '/' + dbo.fn_Pad(CONVERT(VARCHAR(4),MONTH(dbo.fn_Pedido_Datas(b.PedidoID, 8))), 2, \'0\', \'L\') + '/' + dbo.fn_Pad(CONVERT(VARCHAR(4),DAY(dbo.fn_Pedido_Datas(b.PedidoID, 8))), 2, \'0\', \'L\') " +
                                        "WHEN (a.EstadoID = 70 AND a.Emissor = 1) THEN CONVERT(VARCHAR(4),YEAR(a.dtInutilizacao)) + '/' + dbo.fn_Pad(CONVERT(VARCHAR(4),MONTH(a.dtInutilizacao)), 2, \'0\', \'L\') + '/' + dbo.fn_Pad(CONVERT(VARCHAR(4),DAY(a.dtInutilizacao)), 2, \'0\', \'L\') " +
                                        "WHEN (a.EstadoID = 7 AND a.Emissor = 1) THEN CONVERT(VARCHAR(4),YEAR(dbo.fn_Pedido_Datas(b.PedidoID, 7))) + '/' + dbo.fn_Pad(CONVERT(VARCHAR(4),MONTH(dbo.fn_Pedido_Datas(b.PedidoID, 7))), 2, \'0\', \'L\') + '/' + dbo.fn_Pad(CONVERT(VARCHAR(4),DAY(dbo.fn_Pedido_Datas(b.PedidoID, 7))), 2, \'0\', \'L\') " +
                                        "ELSE CONVERT(VARCHAR(4),YEAR(a.dtNotaFiscal)) + '/' + dbo.fn_Pad(CONVERT(VARCHAR(4),MONTH(a.dtNotaFiscal)), 2, \'0\', \'L\') + '/' + dbo.fn_Pad(CONVERT(VARCHAR(4),DAY(a.dtNotaFiscal)), 2, \'0\', \'L\') END, " +
				"b.PessoaID " +
			"FROM " +
				"NotasFiscais a WITH(NOLOCK) " +
				"INNER JOIN Pedidos b WITH(NOLOCK) ON (a.PedidoID = b.PedidoID) " +
			"WHERE " +
				"a.NotaFiscalID = " + nNotaFiscalID;


        dsoXMLFileNFe.ondatasetcomplete = XMLNFePath_DSC;
        dsoXMLFileNFe.refresh();

        return null;
    }
    else if (controlID == 'btnEnviar') {

        // Envia NFe(XML) com o Pedido
        setConnection(dsoEnviaNFe);

        //dsoEnviaNFe.SQL = "SELECT PedidoID FROM NotasFiscais WITH(NOLOCK) WHERE NotaFiscalID = " + nNotaFiscalID;
        //lockControlsInModalWin(true);

        var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'PedidoID' + '\'' + '].value');

        var strPars = new String();
        strPars = '?nPedidoID=' + nPedidoID;
        dsoEnviaNFe.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/enviaNFe.aspx' + strPars;
        dsoEnviaNFe.ondatasetcomplete = EnviaNFe_DSC;
        dsoEnviaNFe.refresh();
    }
    else if (controlID == 'btnCanc') {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_SUP', null);
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function EnviaNFe_DSC() {
    window.top.overflyGen.Alert("NFe enviada.");
}

function XMLNFePath_DSC(resultServ) {
    if ((resultServ != null) && (resultServ.code < 0)) {
        if (window.top.overflyGen.Alert(resultServ.message) == 0)
            return null;

        return null;
    }

    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];
    var sCaminhoEstado = dsoXMLFileNFe.recordset["CaminhoEstado"].value;
    var sPrefixoData = dsoXMLFileNFe.recordset["PrefixoData"].value;
    var sCaminhoData = dsoXMLFileNFe.recordset["CaminhoData"].value;
    var sArquivoXML = dsoXMLFileNFe.recordset["ArquivoXMLNFe"].value;
    var nPessoaID = dsoXMLFileNFe.recordset["PessoaID"].value;
    var sEmpresaID = new String();

    sEmpresaID = nEmpresaID.toString();
    if (sEmpresaID.length < 2) {
        sEmpresaID = '0' + sEmpresaID;
    }

    var fullPath = PAGES_URL_ROOT + "/NFe/" + sEmpresaID + "/DocumentoEletronico/NF-e/" + sPrefixoData + "/";

    if (sPrefixoData == 'Recebidas') {
        fullPath += sCaminhoEstado + "/" + sCaminhoData + "/" + sArquivoXML;
    }
    else {
        fullPath += sCaminhoEstado + "/" + sCaminhoData + "/" + sArquivoXML;
    }

    window.open(fullPath);
}

/********************************************************************
Modelo da NF
********************************************************************/
function modeloNF() {
    var nNotaFiscalID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'NotaFiscalID' + '\'' + '].value');
    nNotaFiscalID = (nNotaFiscalID == null ? glb_NotaFiscalID : nNotaFiscalID);

    setConnection(dsoModeloNF);
    dsoModeloNF.SQL =
			"SELECT a.ModeloNF, a.NotaFiscal, a.ChaveAcesso, a.ArquivoDanfePDF, " +
			    "ArquivoXMLNFe = CASE WHEN (a.EstadoID = 70 AND a.Emissor = 1) THEN a.ArquivoXMLNFeInutilizado ELSE a.ArquivoXMLNFe END, " +
			    "CONVERT(VARCHAR(4),YEAR(a.dtNotaFiscal)) + '/' + dbo.fn_Pad(CONVERT(VARCHAR(4),MONTH(a.dtNotaFiscal)), 2, \'0\', \'L\') + '/' + dbo.fn_Pad(CONVERT(VARCHAR(4),DAY(a.dtNotaFiscal)), 2, \'0\', \'L\') AS CaminhoData, " +
				"PrefixoData = CASE WHEN (a.TipoNotaFiscalID = 601 AND a.Emissor = 0) THEN 'Recebidas' ELSE 'Emitidas' END, " +
				"b.PessoaID " +
			"FROM " +
				"NotasFiscais a WITH(NOLOCK) " +
				"INNER JOIN Pedidos b WITH(NOLOCK) ON (a.PedidoID = b.PedidoID) " +
			"WHERE " +
				"a.NotaFiscalID = " + nNotaFiscalID;

    dsoModeloNF.ondatasetcomplete = modeloNF_DSC;
    dsoModeloNF.refresh();
}


/********************************************************************
Retorna o Modelo da NF e o Numero da Nota
********************************************************************/
function modeloNF_DSC() {
    glb_sModeloNF = dsoModeloNF.recordset["ModeloNF"].value;
    glb_nNotaFiscal = dsoModeloNF.recordset["NotaFiscal"].value;
    glb_PrefixoData = dsoModeloNF.recordset["PrefixoData"].value;
    glb_Caminho_Data = dsoModeloNF.recordset["CaminhoData"].value;
    glb_ArquivoDanfePDF = dsoModeloNF.recordset["ArquivoDanfePDF"].value;
    glb_ArquivoXMLNFe = dsoModeloNF.recordset["ArquivoXMLNFe"].value;
    glb_ChaveAcesso = dsoModeloNF.recordset["ChaveAcesso"].value;

    selDanfeXml_onchange();
}

/********************************************************************
Exibe DANFE e Resumo da NF
********************************************************************/
function selDanfeXml_onchange() {
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];
    var nNotaFiscalID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'NotaFiscalID' + '\'' + '].value');
    var strurlframe = '';
    var sEmpresaID = new String();
    nNotaFiscalID = (nNotaFiscalID == null ? glb_NotaFiscalID : nNotaFiscalID);

    sEmpresaID = nEmpresaID.toString();
    if (sEmpresaID.length < 2) {
        sEmpresaID = '0' + sEmpresaID;
    }

    var sCaminhoDanfe = PAGES_URL_ROOT + "/NFe/" + sEmpresaID + "/DocumentoEletronico/NF-e/" + glb_PrefixoData + "/DANFEs/" + glb_Caminho_Data + "/" + glb_ArquivoDanfePDF;
    //var sCaminhoDanfe = PAGES_URL_ROOT + "/NFe/" + nEmpresaID + "/" + glb_PrefixoData + "/DANFEs/" + glb_Caminho_Data + "/" + glb_ArquivoDanfePDF; ARF

    //Se ModeloNF � 55(NFe) oculta op��es da NFe
    if (glb_sModeloNF != '55') {
        selDanfeXml.value = 1;
        selDanfeXml.style.visibility = 'hidden';
        btnOK.style.visibility = 'hidden';
        btnEnviar.style.visibility = 'hidden';
    }

    preencheComboDanfXml(glb_PrefixoData);

    // Danfe
    if (selDanfeXml.value == 0) {
        strurlframe = sCaminhoDanfe;
    }
        // Resumo
    else if (selDanfeXml.value == 1) {
        strurlframe = "modalviewresumo.asp?NotaFiscalID=" + nNotaFiscalID;
    }

    if (strurlframe != '') {
        if (strurlframe == '')
            iFrameNfe.src = '';
        else
            frames(iFrameNfe.name).location.replace(strurlframe);

    }
}

function preencheComboDanfXml(prefixoPasta) {
    var oOption;

    if (selDanfeXml.options.length > 0) {
        return;
    }

    if (prefixoPasta == 'Emitidas') {
        oOption = document.createElement("OPTION");

        oOption.text = 'DANFE';
        oOption.value = '0';
        oOption.selected = true;

        selDanfeXml.add(oOption);
    }

    oOption = document.createElement("OPTION");

    oOption.text = 'Resumo';
    oOption.value = '1';
    oOption.selected = (prefixoPasta == 'Recebidas');

    selDanfeXml.add(oOption);
}