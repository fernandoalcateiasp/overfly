
<!-- METADATA TYPE="typelib" NAME="Microsoft CDO for NTS 1.2 library" UUID="{0E064ADD-9D99-11D0-ABE5-00AA0064D470}" VERSION="1.2"-->
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0

    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Function ReplaceTest(baseStr, patrn, replStr)
    Dim regEx                         ' Create variables.
    Set regEx = New RegExp            ' Create regular expression.
    regEx.Pattern = patrn             ' Set pattern.
    regEx.IgnoreCase = True           ' Make case insensitive.
    regEx.Global = True               ' Replace globally
    ReplaceTest = regEx.Replace(baseStr, replStr)   ' Make replacement.
End Function

'Funcao de uso geral, retorna um caracter replicate n vezes
Function replicate(ByVal sString, ByVal nVezes)

    Dim i, sNewString

    sNewString = ""
    
    If (IsNull(sString)) Then
        sString = ""
    End If

    For i=1 To nVezes
        sNewString = sNewString & sString
    Next

    replicate = sNewString

End Function

'Funcao de uso geral, PadL
Function padL(ByVal s, ByVal n, ByVal c)

    If (IsNull(s)) Then
        s = ""
    End If
    
    s = Trim(s)

    padL = replicate(c, n - Len(s)) & s

End Function

'Funcao de uso geral, PadR
Function padR(ByVal s, ByVal n, ByVal c)
    If (IsNull(s)) Then
        s = ""
    End If

    s = Trim(s)
    padR = s & replicate(c, n - Len(s))
End Function

%>

<%
	Dim i, nInvoiceID, nEmpresaID, nIdiomaSistema, nIdiomaEmpresa, mailFrom, mailTo, mailCc, mailBcc, mailSubject, sMailBody
	Dim objSendMail, rsData, dsoExtra01, dsoHeader, dsoDetail01, dsoDetail02, dsoDetail03, dsoDetail04, strSQL
	
	sMailBody = ""

	For i = 1 To Request.QueryString("nInvoiceID").Count    
        nInvoiceID = Request.QueryString("nInvoiceID")(i)
    Next
    
    For i = 1 To Request.QueryString("nEmpresaID").Count    
        nEmpresaID = Request.QueryString("nEmpresaID")(i)
    Next
    
    For i = 1 To Request.QueryString("nIdiomaSistema").Count
        nIdiomaSistema = Request.QueryString("nIdiomaSistema")(i)
    Next
    
    For i = 1 To Request.QueryString("nIdiomaEmpresa").Count
        nIdiomaEmpresa = Request.QueryString("nIdiomaEmpresa")(i)
    Next

    Set rsData = Server.CreateObject("ADODB.Recordset")
	Set dsoExtra01 = Server.CreateObject("ADODB.Recordset")
	Set dsoHeader = Server.CreateObject("ADODB.Recordset")
	Set dsoDetail01 = Server.CreateObject("ADODB.Recordset")
	Set dsoDetail02 = Server.CreateObject("ADODB.Recordset")
	Set dsoDetail03 = Server.CreateObject("ADODB.Recordset")
	Set dsoDetail04 = Server.CreateObject("ADODB.Recordset")

	strSQL = "SELECT UPPER(Empresa.Nome) AS Nome, " & _
		"ISNULL(Enderecos.Numero + SPACE(1), SPACE(0)) + ISNULL(Enderecos.Endereco + SPACE(1), SPACE(0)) + " & _
		"ISNULL(Enderecos.Complemento + SPACE(1), SPACE(0)) + ISNULL(Enderecos.Bairro + SPACE(1), SPACE(0)) AS End1, " & _
		"ISNULL(Cidades.Localidade + SPACE(1), SPACE(0)) + ISNULL(UFs.CodigoLocalidade2 + SPACE(1), SPACE(0)) + " & _
		"ISNULL(Paises.CodigoLocalidade3 + SPACE(1), SPACE(0)) AS End2, " & _
		"ISNULL(Enderecos.CEP,SPACE(0)) AS CEP, " & _
		"dbo.fn_Pessoa_Telefone(Empresa.PessoaID, 119, 119,1,0,NULL) AS Telefone, " & _
		"dbo.fn_Pessoa_Telefone(Empresa.PessoaID, 122, 122,1,0,NULL) AS Fax, " & _
		"SUBSTRING(dbo.fn_Pessoa_URL(Empresa.PessoaID, 125, NULL),8,72) AS URL, " & _
		"dbo.fn_Pessoa_URL(Empresa.PessoaID, 124, NULL) AS EMail " & _
	"FROM Pessoas Empresa WITH(NOLOCK) " & _
	    "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Empresa.PessoaID=Enderecos.PessoaID) " & _
	    "LEFT OUTER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) " & _
	    "LEFT OUTER JOIN Localidades UFs WITH(NOLOCK) ON (Enderecos.UFID=UFs.LocalidadeID) " & _
	    "LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) " & _
	"WHERE (Empresa.PessoaID=" & CStr(nEmpresaID) & ")"

    dsoExtra01.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    strSQL = "SELECT NF.NotaFiscalID, " & _
            "(CASE NF.TipoNotaFiscalID WHEN 601 THEN 'X' ELSE SPACE(0) END) AS Entrada, " & _
            "(CASE NF.TipoNotaFiscalID WHEN 602 THEN 'X' ELSE SPACE(0) END) AS Saida, " & _
            "STR(NF.NotaFiscal) AS NotaFiscal, CONVERT(VARCHAR, NF.dtNotaFiscal, 101) AS dtNotaFiscal, " & _
            "NF.CFOP, NF.Natureza, STR(NF.PedidoID) AS PedidoID, STR(NF.Formulario) AS Formulario, NFDist.Nome, " & _
            "STR(NFDist.PessoaID) AS PessoaID, ISNULL(STR(NFTransp.PessoaID), SPACE(0)) AS TransportadoraID, NFDist.Documento1, " & _ 
            "(NFDist.Endereco + ' ' NFDist.Numero + ' ' + NFDist.Complemento) AS Endereco, " & _
            "NFDist.Bairro, NFDist.CEP, NFDist.Cidade, NFDist.UF, NFDist.Pais, UPPER(LEFT(NFDist.Pais, 2)) AS PaisAbreviado, " & _
            "(NFDist.DDI + NFDist.DDD + " " + NFDist.Telefone) AS Telefone, NFDist.DocumentoFederal AS Documento2, " & _
            "NFISS.ValorImposto AS ValorTotalImposto3, NF.ValorTotalServicos, NFICMS.BaseCalculo AS BaseCalculoImposto1, NFICMS.ValorImposto AS ValorTotalImposto1, " & _
            "0 AS BaseCalculoImposto1Subst, 0 AS ValorTotalImposto1Subst, NF.ValorTotalProdutos, ISNULL(NF.ValorFrete, 0) AS ValorFrete, " & _
            "ISNULL(NF.ValorSeguro, 0) AS ValorSeguro, ISNULL(NF.OutrasDespesas, 0) AS OutrasDespesas, " & _
            "ISNULL(NFIPI.ValorImposto, 0) AS ValorTotalImposto2, NF.ValorTotalNota, " & _
            "ISNULL(NFTransp.Nome,SPACE(0)) AS Transportadora, ISNULL(NF.Frete, SPACE(0)    ) AS Frete, " & _
            "(ISNULL(NFTransp.Endereco,SPACE(0)) + ' ' + ISNULL(NFTransp.Numero,SPACE(0)) + ' ' + ISNULL(NFTransp.Complementpo,SPACE(0))) AS TranspEndereco, " + 
            "ISNULL(NFTransp.Cidade, SPACE(0)) AS TranspCidade, " & _
            "ISNULL(NFTransp.Bairro,SPACE(0)) AS TranspBairro, ISNULL(NFTransp.CEP,SPACE(0)) AS TranspCEP, " & _
            "ISNULL(NFTransp.UF,SPACE(0)) AS TranspUF, ISNULL(NFTransp.Pais,SPACE(0)) AS TranspPais, " & _
            "ISNULL(NFTransp.DDI, SPACE(1)) + ISNULL(NFTransp.DDD, SPACE(1)) + ' ' + ISNULL(NFTransp.Telefone, SPACE(1)) AS TranspTelefone, " & _   
            "ISNULL(NFTransp.DocumentoFederal,SPACE(0)) AS TranspDocumento1, ISNULL(NFTransp.DocumentoEstadual,SPACE(0)) AS TranspDocumento2, " & _
            "NF.PlacaVeiculo, NF.UFVeiculo, STR(NF.Quantidade) AS Quantidade, " & _
            "NF.Especie, ISNULL(dbo.fn_Tradutor(NF.MeioTransporte, " & CStr(nIdiomaSistema) & ", " & CStr(nIdiomaEmpresa) & ", NULL),SPACE(0)) AS MeioTransporte, " & _
            "NF.PesoBruto, NF.PesoLiquido, NF.Vendedor, " & _   
            "ISNULL(NF.SeuPedido, SPACE(1)) AS SeuPedido, " & _
            "(SELECT ISNULL(CONVERT(VARCHAR(5), CONVERT(NUMERIC(3),PrazoMedioPagamento)) + ' Days', SPACE(0)) FROM Pedidos WITH(NOLOCK) WHERE Pedidos.PedidoID=NF.PedidoID) AS PMP, " & _
            "(SELECT (CASE SUBSTRING(Observacao,1,1) WHEN '/' THEN SUBSTRING(Observacao,2,39) ELSE 'Miami' END) " & _
            "FROM Pedidos WITH(NOLOCK) WHERE Pedidos.PedidoID=NF.PedidoID) AS CidadeEmpresa " & _
        "FROM NotasFiscais NF WITH(NOLOCK), RelacoesPesRec PesRec WITH(NOLOCK), RelacoesPesRec_DocumentosFiscais PesRec_Nota WITH(NOLOCK), " & _
            "NotasFiscais_Pessas NFDest WITH(NOLOCK), NotasFiscais_Pessas NFTransp WITH(NOLOCK), NotasFiscais_Impostos NFISS WITH(NOLOCK), " & _
            "NotasFiscais_Impostos NFICMS WITH(NOLOCK), NotasFiscais_Impostos NFIPI WITH(NOLOCK), NotasFiscais_Impostos NFTAX WITH(NOLOCK) "
        "WHERE NF.NotaFiscalID = " & CStr(nInvoiceID) & " AND " & _ 
            "(PesRec.TipoRelacaoID=12 AND PesRec.SujeitoID=NF.EmpresaID AND PesRec.ObjetoID=999) AND " & _  
            "(PesRec_Nota.RelacaoID=PesRec.RelacaoID AND PesRec_Nota.Impressora=NF.Impressora) AND " & _
            "(NFDest.NotaFiscalID = NF.NotaFiscalID) AND ((NFDest.Emissor = 1) AND (NFDest.TipoID = 791)) OR " & _
            "((NFDest.Emissor = 0) AND (NFDest.TipoID = 790)) AND (NFTransp.NotaFiscalID = NF.NotaFiscalID) AND " & _
            "(NFTransp.TipoID = 793) AND (NFISS.NotaFiscalID = NF.NotaFiscalID) AND (NFISS.TipoID = 10) AND " & _
            "(NFICMS.NotaFiscalID = NF.NotaFiscalID) AND (NFICMS.TipoID = 9) AND " & _
            "(NFIPI.NotaFiscalID = NF.NotaFiscalID) AND (NFIPI.TipoID = 8) AND " & _
            "(NFICMSST.NotaFiscalID = NF.NotaFiscalID) AND (NFICMSST.TipoID = 25) AND " & _
            "(NFTAX.NotaFiscalID = NF.NotaFiscalID) AND (NFTAX.TipoID = 22)"

    dsoHeader.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    'Detalhe 1 - Fatura
    strSQL = "SELECT NotaFiscalID, Duplicata, " & _
				"CONVERT(VARCHAR, Vencimento, 101) AS Vencimento, " & _
				"Valor " & _
				"FROM NotasFiscais_Duplicatas WITH(NOLOCK) " & _
				"WHERE NotaFiscalID = " & CStr(nInvoiceID)
                      
    dsoDetail01.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    strSQL = "SELECT NotaFiscalID, STR(ProdutoID) AS ItemID, Familia, Marca, Modelo, ISNULL(Descricao, SPACE(1)) AS Descricao, " & _
                      "ISNULL(LEFT(Descricao,20), SPACE(1)) AS DescricaoReduzida, " & _
                      "ClassificacaoFiscal, SituacaoTributaria, Unidade, STR(Quantidade) AS Quantidade, " & _
                      "ValorUnitario, ValorTotal, " & _
                      "STR(AliquotaImposto1) AS AliquotaImposto1, STR(AliquotaImposto2) AS AliquotaImposto2, " & _
                      "AliquotaImposto1 AS AliquotaImposto1Modelo2, " & _
                      "AliquotaImposto2 AS AliquotaImposto2Modelo2, ValorImposto2 " & _
                      "FROM NotasFiscais_Itens WITH(NOLOCK) " & _
                      "WHERE Tipo = 'P' AND NotaFiscalID = " & CStr(nInvoiceID)

    dsoDetail02.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    ' Detalhe 3 - Servico
    strSQL = "SELECT NotaFiscalID, STR(ProdutoID) AS ItemID, Familia, Marca, Modelo, ISNULL(Descricao, SPACE(1)) AS Descricao, " & _
				"Unidade, STR(Quantidade) AS Quantidade, " & _
				"ValorUnitario, ValorTotal " & _
				"FROM NotasFiscais_Itens WITH(NOLOCK) " & _
				"WHERE Tipo = 'S' AND NotaFiscalID = " & CStr(nInvoiceID)
                      
    dsoDetail03.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    ' Detalhe 4 - Mensagem
    strSQL = "SELECT NotaFiscalID, Mensagem " & _
                      "FROM NotasFiscais_Mensagens WITH(NOLOCK) " & _
                      "WHERE NotaFiscalID = " & CStr(nInvoiceID) & " " & _
                      "ORDER BY Ordem"

    dsoDetail04.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	sMailBody = "<IMG WIDTH='159' HEIGHT='52' SRC='http://www.allplus.com/commonpages/images/general/empresa_7/logo.gif'></IMG>"	
    sMailBody = sMailBody & "<FONT STYLE='FONT-SIZE:12pt;FONT-FAMILY:ARIAL'><B>" & replicate(" ", 67) & "COMMERCIAL INVOICE<B></FONT><BR>"
    sMailBody = sMailBody & Replicate("_", 106) & "<BR>"
    
    Dim sCEP, sTelefone, sFax
    
    sCEP = Trim(dsoExtra01.Fields("CEP").value)
    sCEP = Mid(sCEP, 1, 5) & "-" & Mid(sCEP, 6)
    
    sTelefone = Mid(Trim(dsoExtra01.Fields("Telefone").value), 2)
    sFax = Mid(Trim(dsoExtra01.Fields("Fax").value), 1)

    sTelefone = "Phone " & Mid(sTelefone,1 ,3) & "-" & Mid(sTelefone, 4, Len(sTelefone) - 7) & "-" & _
		Mid(sTelefone, 4 + Len(sTelefone) - 7)

    sFax = "Fax " & Mid(sFax, 1, 3) & "-" & Mid(sFax, 4, Len(sFax) - 7) & "-" & _
		Mid(sFax, 4 + Len(sFax) - 7)
    
    sMailBody = sMailBody & PadR(dsoExtra01.Fields("Nome").value, 78, " ")
    sMailBody = sMailBody & "Invoice:" & PadL(Trim(dsoHeader.Fields("NotaFiscal").value), 20, " ") & "<BR>"
    
    sMailBody = sMailBody & PadR(dsoExtra01.Fields("End1").value, 78, " ")
    sMailBody = sMailBody & "Date:" & PadL(Trim(dsoHeader.Fields("dtNotaFiscal").value), 23, " ") & "<BR>"
    
    sMailBody = sMailBody & PadR(dsoExtra01.Fields("End2").value, 78, " ")
    sMailBody = sMailBody & "Order:" & PadL(Trim(dsoHeader.Fields("PedidoID").value), 22, " ") & "<BR>"
    
    sMailBody = sMailBody & PadR(sTelefone & "   " & sFax, 78, " ")
    sMailBody = sMailBody & "Salesperson:" & PadL(Trim(dsoHeader.Fields("Vendedor").value), 16, " ") & "<BR>"
    
    sMailBody = sMailBody & PadR(dsoExtra01.Fields("URL").value & "   " & dsoExtra01.Fields("EMail").value, 78, " ")
    sMailBody = sMailBody & "Transaction:" & PadL(Trim(dsoHeader.Fields("Natureza").value), 16, " ") & "<BR>"

	sMailBody = sMailBody & Replicate("_", 106) & "<BR>"
	
	Dim term_Acquire_SoldTo
	term_Acquire_SoldTo = "Sold to:"
	
    If ( (CLng(dsoHeader.Fields("PessoaID").value) = 2) OR _
		 (CLng(dsoHeader.Fields("PessoaID").value) = 4) OR _
		 (CLng(dsoHeader.Fields("PessoaID").value) = 10) OR _
		 (CLng(dsoHeader.Fields("PessoaID").value) = 27682) OR _
		 (CLng(dsoHeader.Fields("PessoaID").value) = 32078) OR _
		 (CLng(dsoHeader.Fields("PessoaID").value) = 14121) OR _
		 (CLng(dsoHeader.Fields("PessoaID").value) = 42896) ) Then
		term_Acquire_SoldTo = "Sold to (Acquire):"
	End If
	
    sMailBody = sMailBody & PadR(term_Acquire_SoldTo, 63, " ") & "Ship to:<BR>"
    
    sMailBody = sMailBody & PadR(dsoHeader.Fields("Nome").value, 63, " ")
    sMailBody = sMailBody & dsoHeader.Fields("Transportadora").value & "<BR>"
    
    Dim sAddress1
    Dim sAddress2
    
    sAddress1 = dsoHeader.Fields("Endereco").value & " " & dsoHeader.Fields("Bairro").value
    sAddress2 = ""
    
    If ( Len(Trim(sAddress1)) > 55 ) Then
		sAddress1 = dsoHeader.Fields("Endereco").value
		sAddress2 = dsoHeader.Fields("Bairro").value & " " & dsoHeader.Fields("Cidade").value & " " & _ 
			dsoHeader.Fields("UF").value & " " & dsoHeader.Fields("CEP").value
	Else
		sAddress2 = dsoHeader.Fields("Cidade").value & " " &	dsoHeader.Fields("UF").value & " " & _
			dsoHeader.Fields("CEP").value
	End If
    
    Dim sAddressTransporte1
    Dim sAddressTransporte2

    sAddressTransporte1 = dsoHeader.Fields("TranspEndereco").value & " " & dsoHeader.Fields("TranspBairro").value
    sAddressTransporte2 = ""
    
    If ( Len(Trim(sAddress1)) > 55 ) Then
		sAddressTransporte1 = dsoHeader.Fields("TranspEndereco").value
		sAddressTransporte2 = dsoHeader.Fields("TranspBairro").value & " " & dsoHeader.Fields("TranspCidade").value & " " & _ 
			dsoHeader.Fields("TranspUF").value & " " & dsoHeader.Fields("TranspCEP").value
	Else
		sAddressTransporte2 = dsoHeader.Fields("TranspCidade").value & " " & dsoHeader.Fields("TranspUF").value & " " & _
			dsoHeader.Fields("TranspCEP").value
	End If
	
	sMailBody = sMailBody & PadR(sAddress1, 63, " ") & _
		sAddressTransporte1 & "<BR>"

	sMailBody = sMailBody & PadR(sAddress2, 63, " ") & _
		sAddressTransporte2 & "<BR>"
    
	sMailBody = sMailBody & PadR(dsoHeader.Fields("Pais"), 63, " ") & _
		dsoHeader.Fields("TranspPais") & "<BR>"
    
	sMailBody = sMailBody & PadR(dsoHeader.Fields("Telefone"), 63, " ") & _
		dsoHeader.Fields("TranspTelefone") & "<BR>"
    
	sMailBody = sMailBody & PadR("ID: " & dsoHeader.Fields("PessoaID"), 63, " ") & _
		"ID: " & dsoHeader.Fields("TransportadoraID") & "<BR>"
	
	sMailBody = sMailBody & Replicate("_", 106) & "<BR>"
		
	sMailBody = sMailBody & "<B>" & PadR("Customer PO", 22, " ") & PadR("Due Date", 22, " ") & PadR("Ship Via", 20, " ") & _
		PadR("Forwarder", 30, " ") & "F.O.B" & "</B><BR>"

    sMailBody = sMailBody & PadR(dsoHeader.Fields("SeuPedido").value, 22, " ")
    
    If ( Not (dsoDetail01.BOF AND dsoDetail01.EOF) ) Then
        dsoDetail01.MoveFirst
	    sMailBody = sMailBody & PadR(dsoDetail01.Fields("Vencimento").value, 22, " ")
	Else
		sMailBody = sMailBody & replicate(" ", 22)
	End If
	
	sMailBody = sMailBody & PadR(dsoHeader.Fields("MeioTransporte").value, 20, " ")
	sMailBody = sMailBody & PadR(dsoHeader.Fields("CidadeEmpresa").value, 30, " ") & "<BR>"
	sMailBody = sMailBody & Replicate("_", 106) & "<BR>"    
    sMailBody = sMailBody & PadL("ID", 8, " ") & "  " & PadR("Product", 26, " ") & PadR("Brand", 21, " ") & _
		PadR("Model", 19, " ") & PadL("Qty", 6, " ") & PadL("Price", 12, " ") & PadL("Extended", 12, " ") & "<BR>"
		
	Dim QtdItens
	QtdItens = 0
	
	If ( Not (dsoDetail02.BOF AND dsoDetail02.EOF) ) Then
		dsoDetail02.MoveFirst
		While (Not dsoDetail02.EOF)
			QtdItens = QtdItens + 1
			
			sMailBody = sMailBody & PadL(dsoDetail02.Fields("ItemID").Value, 8, " ") & "  " & _
				PadR(dsoDetail02.Fields("Produto").Value, 26, " ") & _
				PadR(dsoDetail02.Fields("Marca").Value, 21, " ") & _
				PadR(dsoDetail02.Fields("Modelo").Value, 19, " ") & _
				PadL(dsoDetail02.Fields("Quantidade").Value, 6, " ") & _
				PadL(FormatCurrency(dsoDetail02.Fields("ValorUnitario").Value,-1), 12, " ") & _
				PadL(FormatCurrency(dsoDetail02.Fields("ValorTotal").Value,-1), 12, " ") & "<BR>" & _
				replicate(" ", 10) & dsoDetail02.Fields("Descricao").Value & "<BR>"
			
			dsoDetail02.MoveNext
		Wend
	End If

	If ( Not (dsoDetail03.BOF AND dsoDetail03.EOF) ) Then
		dsoDetail03.MoveFirst
		While (Not dsoDetail03.EOF)
			QtdItens = QtdItens + 1
			
			sMailBody = sMailBody & PadL(dsoDetail03.Fields("ItemID").Value, 8, " ") & _
				PadR(dsoDetail03.Fields("Produto").Value, 26, " ") & _
				PadR(dsoDetail03.Fields("Marca").Value, 21, " ") & _
				PadR(dsoDetail03.Fields("Modelo").Value, 19, " ") & _
				PadL(dsoDetail03.Fields("Quantidade").Value, 6, " ") & _
				PadL(FormatCurrency(dsoDetail03.Fields("ValorUnitario").Value,-1), 12, " ") & _
				PadL(FormatCurrency(dsoDetail03.Fields("ValorTotal").Value,-1), 12, " ") & "<BR>" & _
				replicate(" ", 8) & dsoDetail03.Fields("Descricao").Value & "<BR>"

			dsoDetail03.MoveNext
		Wend
	End If
	
	For i=1 To (13*2) - QtdItens
		sMailBody = sMailBody & "<BR>"
	Next
	
    Dim nSubTotalInvoice
    
    nSubTotalInvoice = 0
    If ( Not ( IsNull(dsoHeader.Fields("ValorTotalProdutos").value) AND _
               IsNull(dsoHeader.Fields("ValorTotalServicos").value) ) ) Then
        nSubTotalInvoice = CLng(dsoHeader.Fields("ValorTotalProdutos").value) + _
            CLng(dsoHeader.Fields("ValorTotalServicos").value)
    ElseIf (Not IsNull(dsoHeader.Fields("ValorTotalProdutos").value)) Then
        nSubTotalInvoice = CLng(dsoHeader.Fields("ValorTotalProdutos").value)
    ElseIf (Not IsNull(dsoHeader.Fields("ValorTotalServicos").value)) Then
        nSubTotalInvoice = CLng(dsoHeader.Fields("ValorTotalServicos").value)
    End If    
    
	sMailBody = sMailBody & Replicate("_", 106) & "<BR>"
    sMailBody = sMailBody & PadR("Customer agrees to the following:", 93, " ") & "Sub Total<BR>"
    sMailBody = sMailBody & PadR(" 25% restocking fee on all returned items. NO CASH REFUNDS. Fee of $25.00 for all returned checks.", 102, " ")
    sMailBody = sMailBody & nSubTotalInvoice & "<BR>"
    sMailBody = sMailBody & PadR(" All return items must be in original packaging. All returns with invoice only.", 90, " ") & "<BR>"
    sMailBody = sMailBody & PadR(" All mechandise carries manufacturer�s warranty only.", 93, " ") & "Tax<BR>"
    

	If ( Not (dsoDetail04.BOF AND dsoDetail04.EOF) ) Then
		dsoDetail04.MoveFirst
	End If
	
    For i=1 To 7

		If ( Not dsoDetail04.EOF ) Then
				sMailBody = sMailBody & PadR(dsoDetail04.Fields("Mensagem").Value, 93, " ")
				dsoDetail04.MoveNext
		Else
			sMailBody = sMailBody & replicate(" ",93)
		End If		

		If (i=1) Then
			sMailBody = sMailBody & PadL(CStr(FormatCurrency(dsoHeader.Fields("ValorTotalImposto2").value,-1)), 13, " ") & "<BR>"
		ElseIf (i=3) Then
			sMailBody = sMailBody & "Shipping" & "<BR>"
		ElseIf (i=4) Then
			sMailBody = sMailBody & PadL(CStr(FormatCurrency(dsoHeader.Fields("ValorFrete").value,-1)), 13, " ") & "<BR>"
		ElseIf (i=6) Then
			sMailBody = sMailBody & "Total Invoice" & "<BR>"
		ElseIf (i=7) Then
			sMailBody = sMailBody & PadL(CStr(FormatCurrency(dsoHeader.Fields("ValorTotalNota").value,-1)), 13, " ") & "<BR>"
		Else
			sMailBody = sMailBody & "<BR>"
		End If

    Next
    
    sMailBody = sMailBody & Replicate("_", 106) & "<BR>"    
    
    sMailBody = sMailBody & "<BR><BR><BR><BR><BR>"
    sMailBody = sMailBody & "Received above merchandise in good condition:<BR><BR>" 
    sMailBody = sMailBody & "Date: ___/___/______    Signature: _______________________________    Name: ______________________________" & "<BR>"

    mailTo = ""
    mailFrom = ""
    mailCc = ""
    mailBcc = ""
    mailSubject = ""

    If (Not (dsoHeader.BOF AND dsoHeader.EOF)) Then
        mailTo = "marco.fortunato@alcateia.com.br"
        mailFrom = "marco.fortunato@alcateia.com.br"
        mailCc = ""
        mailBcc = ""
        mailSubject = "Invoice No " & CStr(nInvoiceID)
        sMailBody = ReplaceTest( sMailBody, Chr(13) & Chr(10), "<BR>")
        'sMailBody = ReplaceTest( sMailBody, Chr(32), "&nbsp")
        sMailBody = ReplaceTest( sMailBody, "  ", "&nbsp&nbsp")

        sMailBody = "<HTML><BODY><P><FONT FACE='Courier New' SIZE='1'>" & sMailBody & "</FONT></P></BODY></HTML>"

        'Manda o e mail
        Set objSendMail = CreateObject("CDONTS.NewMail")
                    
        With objSendMail
            .From = mailFrom
            .To = mailTo
            .Cc = mailCc
            .Bcc = mailBcc
            .Subject = mailSubject
            .Body = sMailBody
            .BodyFormat = 0
            .MailFormat = 0
            .Importance = CdoNormal
            .Send
        End With

        Set objSendMail = Nothing
        
        strSQL = "SELECT 'E-Mail enviado com sucesso.' AS msg "
    Else
        strSQL = "SELECT 'N�o foi poss�vel enviar o e-mail.' AS msg "
    End If
    
    
    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    rsData.Save Response, adPersistXML
    
    rsData.Close
    Set rsData = Nothing
	Set dsoExtra01 = Nothing
	Set dsoHeader = Nothing
	Set dsoDetail01 = Nothing
	Set dsoDetail02 = Nothing
	Set dsoDetail03 = Nothing
	Set dsoDetail04 = Nothing
%>
