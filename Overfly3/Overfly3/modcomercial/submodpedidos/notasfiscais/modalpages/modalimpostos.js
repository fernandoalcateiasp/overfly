/********************************************************************
modalimpostos.js

Library javascript para o modalimpostos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_CFOPIDAlteraImposto=false;
var glb_GridEditabled=false;
var glb_NotaFiscalID = 0;
var glb_ComplementoValor = false;
var glb_nDSOCounter = 0;

// 0 - Impostos.
// 1 - Impostos complementares.
var glb_BtnImpostosMode = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoGen01 = new CDatatransport('dsoGen01');
var dsoGen02 = new CDatatransport('dsoGen02');
var dsoCFOPImpostos = new CDatatransport('dsoCFOPImpostos');
var dsoComplementoImposto = new CDatatransport('dsoComplementoImposto');
// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    glb_NotaFiscalID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'NotaFiscalID' + '\'' +'].value');
           
    glb_nDSOCounter = 2;
    
    setConnection(dsoCFOPImpostos);

	dsoCFOPImpostos.SQL = 'SELECT TOP 1 ISNULL(b.AlteraImpostos,0) AS AlteraImpostos ' +
        'FROM NotasFiscais_Itens a WITH(NOLOCK) ' +
            'INNER JOIN Operacoes b WITH(NOLOCK) ON (a.CFOPID=b.OperacaoID) ' +
        'WHERE (a.NotaFiscalID=' + glb_NotaFiscalID + ') ' +
        'ORDER BY ISNULL(b.AlteraImpostos,0)';

	dsoCFOPImpostos.ondatasetcomplete = window_onload_DSC;
	dsoCFOPImpostos.Refresh();    

    setConnection(dsoComplementoImposto);

	dsoComplementoImposto.SQL = 'SELECT CONVERT(BIT, COUNT(*)) AS TemComplementoImpostos ' +
        'FROM NotasFiscais_Itens a WITH(NOLOCK) ' +
            'INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ProdutoID=b.ConceitoID) ' +
        'WHERE (a.NotaFiscalID=' + glb_NotaFiscalID + ' AND b.ProdutoID = 2830) ';

	dsoComplementoImposto.ondatasetcomplete = window_onload_DSC;
	dsoComplementoImposto.Refresh();    
	
	setBtnImpostosMode();
}

function window_onload_DSC()
{
    glb_nDSOCounter--;

    glb_GridEditabled = false;

    if (glb_nDSOCounter > 0)
        return null;

    if ((dsoCFOPImpostos.recordset.BOF) && (dsoCFOPImpostos.recordset.EOF))
        glb_CFOPIDAlteraImposto = false;
    else
        glb_CFOPIDAlteraImposto = dsoCFOPImpostos.recordset['AlteraImpostos'].value;

    if (glb_CFOPIDAlteraImposto == 1)
        glb_CFOPIDAlteraImposto = true;
    else        
        glb_CFOPIDAlteraImposto = false;
        
    if ((dsoComplementoImposto.recordset.BOF) && (dsoComplementoImposto.recordset.EOF))
        glb_ComplementoValor = false;
    else
        glb_ComplementoValor = dsoComplementoImposto.recordset['TemComplementoImpostos'].value;

    if (glb_ComplementoValor == 1)
        glb_ComplementoValor = true;
    else        
        glb_ComplementoValor = false;

    setupPage();   
    
    getDataInServer();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
//		if (glb_BtnImpostosMode == 0)
//			updateImpostosDataInServer();
//		else if (glb_BtnImpostosMode == 1)
//			updateImpostosComplementaresDataInServer();
        return null;
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Impostos', 1);
    
    loadDataAndTreatInterface();    
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;
        
    // Ajusta botoes OK/Cancel para esta janela
    with (btnOK.style)
    {
        disabled = true;
        left = (widthFree - 2 * parseInt(width, 10) - ELEM_GAP) / 2;
    }
    with (btnCanc.style)
    {
        left = (parseInt(btnOK.currentStyle.left,10) +  parseInt(btnOK.currentStyle.width,10) + ELEM_GAP);
    }    
    
    // Selecao e botao.
    btnImpostos.style.top = topFree + 16;
    btnImpostos.style.left = ELEM_GAP;
    btnImpostos.style.width = FONT_WIDTH * 7 + 80;

    lblImpostoID.style.top = topFree;
    lblImpostoID.style.left = btnImpostos.offsetLeft + btnImpostos.offsetWidth + ELEM_GAP;
    lblImpostoID.style.width = FONT_WIDTH * 7;
    
    selImpostoID.style.top = lblImpostoID.offsetTop + lblImpostoID.offsetHeight;
    selImpostoID.style.left = btnImpostos.offsetLeft + btnImpostos.offsetWidth + ELEM_GAP;
    selImpostoID.style.width = FONT_WIDTH * 7;
    selImpostoID.onchange = selImpostoIDOnchange;
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = selImpostoID.offsetTop + selImpostoID.offsetHeight + ELEM_GAP;
        width = modWidth - frameBorder - 2 * ELEM_GAP;    
        height = parseInt(btnOK.currentStyle.top, 10) -
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10);
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServer()
{
    var strPars = new String();
        
    // mandar os parametros para o servidor
    strPars = '?nNotaFiscalID=' + escape(glb_nNotaFiscalID) +
		'&nModo=' + escape(glb_BtnImpostosMode) +
		'&nImpostoID=' + escape(selImpostoID.value);
    
    dsoGen01.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/notasfiscais/serverside/impostosdata.aspx' + strPars;
    
	if(glb_BtnImpostosMode == 0)
	{
		dsoGen01.ondatasetcomplete = fillModalGrid_DSC;
	}
	else if(glb_BtnImpostosMode == 1)
	{
		dsoGen01.ondatasetcomplete = fillModalGridComplementares_DSC;
	}

    dsoGen01.refresh();
}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGrid_DSC()
{
    var i, j, nSumImp;
    var nAliquota = 0;

    fg.Redraw = 0;

    headerGrid(fg,['Ordem',
                   'ID',
                   'Produto',
                   'NCM',
                   'Imposto',
                   'CT',
                   'Al�quota',
                   'Base C�lculo',
                   'Valor Imposto',
                   'Tipo',
                   'AlteraAliquota',
                   'ImpostoID',
                   'OK'],[9,10,11,12]);
    
    glb_aCelHint = [[0,3,'Clique duas vezes no NCM para abrir o documento NCM']];

    fillGridMask(fg,dsoGen01,['Ordem',
                              'ProdutoID',
                              'Conceito',
							  'NCM',
                              'Imposto',
                              'CodigoTributacao',
                              'Aliquota',
                              'BaseCalculo',
                              'ValorImposto',
                              'TipoImposto',
                              'AlteraAliquota',
                              'ImpostoID',
                              'OK'],
                              ['','','','','','','999.99','999999999.99'  ,'999999999.99'  ,'','','',''],
                              ['','','','','','','##0.00','###,###,##0.00','###,###,##0.00','','','','']);
       
    alignColsInGrid(fg,[0,1,5,6,7,8,9]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    // linha de Totais
    gridHasTotalLine(fg, 'Total', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'ValorImposto'),'###,###,##0.00','S']]);
    
    // soma apenas os impostos tipo 1
    // e coloca cor nas linhas tipo 1
    if ( fg.Rows > 2 )
    {
        nSumImp = 0;
        j = 0;
        for ( i=1;i<fg.Rows;i++ )
        {
            if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')) == 1 )
            {
                nSumImp += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto'));
                j++;
            }
        }
        
        with (fg)
        {
            if ( j > 0 )
                Cell(6, getColIndexByColKey(fg, 'Conceito'), 0, getColIndexByColKey(fg, 'Conceito') + j - 1, getColIndexByColKey(fg, 'ValorImposto')) = 0XE0FFFF;

            if (glb_nTotalNota>0)
                nAliquota = (nSumImp/glb_nTotalNota)*100;
            TextMatrix(1, getColIndexByColKey(fg, 'Aliquota')) = nAliquota;
            TextMatrix(1, getColIndexByColKey(fg, 'BaseCalculo')) = glb_nTotalNota;
            TextMatrix(1, getColIndexByColKey(fg, 'ValorImposto')) = nSumImp;
        }
        fg.Row = 2;
        fg.Col = getColIndexByColKey(fg, 'Aliquota');
    }

    fg.Redraw = 2;
            
    with (modalimpostosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
               
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();
    
    fg.Editable = glb_GridEditabled;
        
    btnOK.disabled = true;
}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGridComplementares_DSC()
{
    var i, j, nSumImp;

    fg.Redraw = 0;

    headerGrid(fg,['Ordem',
					'ID',
					'Produto',
					'NCM',
					'Imposto',					
					'Al�quota',
					'Base C�lculo',
					'Valor Imposto',
					'Tipo',
					'ImpostoID',
					'CFOPID',
					'CFOPAIC',
					'ImpostoAIC',
					'OK'],[8,9,10,11,12,13]);

    glb_aCelHint = [[0,3,'Clique duas vezes no NCM para abrir o documento NCM']];
    
    fillGridMask(fg,dsoGen01,['Ordem',
								'ProdutoID',
								'Conceito',
								'NCM',
								'Imposto',								
								'Aliquota',
								'BaseCalculo',
								'ValorImposto',
								'TipoImposto',
								'ImpostoID',
								'CFOPID',
								'CFOPAIC',
								'ImpostoAIC',
								'OK'],
								['','','','','','','999999999.99','999999999.99','','','','',''],
								['','','','','','','###,###,##0.00','###,###,##0.00','','','','','']);
       
    alignColsInGrid(fg,[0,1,6,7]);                           

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    // linha de Totais
    gridHasTotalLine(fg, 'Total', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'ValorImposto'),'###,###,##0.00','S']]);
    
    // soma apenas os impostos tipo 1
    // e coloca cor nas linhas tipo 1
    if ( fg.Rows > 2 )
    {
        fg.Row = 2;
        fg.Col = getColIndexByColKey(fg, 'BaseCalculo');
    }

    fg.Redraw = 2;
            
    with (modalimpostosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
               
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();
    
    fg.Editable = glb_GridEditabled;
        
    btnOK.disabled = true;
}

function js_ModalImpostos_AfterEdit(Row, Col)
{
	if (glb_BtnImpostosMode == 0)
		js_ModalImpostosNormais_AfterEdit(Row, Col);
	else if (glb_BtnImpostosMode == 1)
		js_ModalImpostosComplementares_AfterEdit(Row, Col);
}

function js_ModalImpostosComplementares_AfterEdit(Row, Col)
{
	fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
	fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;
	btnOK.disabled = false;
}

function js_ModalImpostosNormais_AfterEdit(Row, Col)
{
    var i=0;
    var nSumImp = 0;
    var nSumImpEspecifico = 0;
    var nAliquota=0;
    var nValorImposto = 0;
    var nAliquota=0;
    var nBase=0;
    var nImpostoID=0;
    
    fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
    
    nAliquota = fg.ValueMatrix(Row,getColIndexByColKey(fg, 'Aliquota'));
    nBase = fg.ValueMatrix(Row,getColIndexByColKey(fg, 'BaseCalculo'));
    nValorImposto = fg.ValueMatrix(Row,getColIndexByColKey(fg, 'ValorImposto'));
    nImpostoID = fg.ValueMatrix(Row,getColIndexByColKey(fg, 'ImpostoID'));

    if (glb_ComplementoValor)
    {
        fg.TextMatrix(Row,getColIndexByColKey(fg, 'Aliquota')) = 0;
        fg.TextMatrix(Row,getColIndexByColKey(fg, 'BaseCalculo')) = 0;
            
        if ( fg.Rows > 2 )
        {        
            for ( i=1;i<fg.Rows;i++ )
            {
                if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')) == 2 )
                {
                    nSumImp += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto'));

                    if (nImpostoID == fg.ValueMatrix(i, getColIndexByColKey(fg, 'ImpostoID')))
                        nSumImpEspecifico += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto'));               
                }
            }
            
            with (fg)
            {
                TextMatrix(1, getColIndexByColKey(fg, 'Aliquota')) = 0;
                TextMatrix(1, getColIndexByColKey(fg, 'ValorImposto')) = nSumImp;
                TextMatrix(1, getColIndexByColKey(fg, 'BaseCalculo')) = 0;
            }

            // Atualiza o total do imposto
            for ( i=1;i<fg.Rows;i++ )
            {
                if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')) == 1 )
                {
                    if (nImpostoID == fg.ValueMatrix(i, getColIndexByColKey(fg, 'ImpostoID')))
                    {
                        fg.TextMatrix(i, getColIndexByColKey(fg, 'ValorImposto')) = nSumImpEspecifico;
                        fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = 1;
                    }
                }
            }
            
            fg.Row = Row;
            fg.Col = Col;
        }        
    }
    else
    {
        if (nAliquota<0)
        {
            fg.TextMatrix(Row,getColIndexByColKey(fg, 'Aliquota')) = 0;
            fg.TextMatrix(Row,getColIndexByColKey(fg, 'ValorImposto')) = 0;
        }
      
        if (nBase>0)
            fg.TextMatrix(Row,getColIndexByColKey(fg, 'ValorImposto')) = nBase*(nAliquota/100);
        else
        {
            nBase = 0;
            fg.TextMatrix(Row,getColIndexByColKey(fg, 'ValorImposto')) = 0;
            fg.TextMatrix(Row,getColIndexByColKey(fg, 'BaseCalculo')) = 0;
        }
            
        if ( fg.Rows > 2 )
        {        
            for ( i=1;i<fg.Rows;i++ )
            {
                if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')) == 1 )
                {
                    nSumImp += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto'));                
                }
            }
            
            with (fg)
            {
                if (nBase>0)
                    nAliquota = (nSumImp/glb_nTotalNota)*100;
                TextMatrix(1, getColIndexByColKey(fg, 'Aliquota')) = nAliquota;
                TextMatrix(1, getColIndexByColKey(fg, 'ValorImposto')) = nSumImp;
                TextMatrix(1, getColIndexByColKey(fg, 'BaseCalculo')) = glb_nTotalNota;
            }
            fg.Row = Row;
            fg.Col = Col;
        }        
    }
    
    fg.TextMatrix(Row,getColIndexByColKey(fg, 'OK')) = 1;
    btnOK.disabled = false;
}

function js_BeforeRowColChangeModalImpostos(grid, OldRow, OldCol, NewRow, NewCol, Cancel)
{
	glb_GridEditabled = false;
	
	fg.Editable = glb_GridEditabled;

	//Forca celula read-only    
	if ((!glb_GridIsBuilding) && (glb_GridEditabled))
	{
		if ( glb_FreezeRolColChangeEvents )
			return true;

		if (!currCellIsLocked(grid, NewRow, NewCol))
		{
			glb_validRow = OldRow;
			glb_validCol = OldCol;
		}
		else
		    js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalimpostosDblClick(grid, Row, Col)
{
    if (Row > 0)
    {
        var sNCM = fg.TextMatrix(Row, getColIndexByColKey(fg, 'NCM'));
        sNCM = sNCM.substring(0,4);

        strPars = '?sNCM=' + escape(sNCM);
        htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/serverside/imagemncm.asp' + strPars;
        window.open(htmlPath);
    }
}

function currCellIsLocked(grid, nRow, nCol)
{
	var bRetVal = false;

	if (glb_GridEditabled)
	{
		if (glb_BtnImpostosMode == 0)
		{
			if ((grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'TipoImposto')) == 1) &&
				(grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'AlteraAliquota')) == 1) &&
				((nCol == getColIndexByColKey(grid, 'Aliquota')) || 
				 (nCol == getColIndexByColKey(grid, 'BaseCalculo')) ) )
					bRetVal = true;	

			if ((grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'TipoImposto')) == 2) &&
				(glb_ComplementoValor) &&
				(nCol == getColIndexByColKey(grid, 'ValorImposto')) )
					bRetVal = true;	
		}
		else if (glb_BtnImpostosMode == 1)
		{
			if ( ((nCol == getColIndexByColKey(grid, 'BaseCalculo')) ||
			      (nCol == getColIndexByColKey(grid, 'ValorImposto'))) &&
				  (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'CFOPAIC')) != '0')&&
				   (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'ImpostoAIC')) != '0'))
				bRetVal = true;
		}
    }
    return bRetVal;    
}

/*
function updateImpostosComplementaresDataInServer()
{
    var strPars = '';
    for ( i=2;i<fg.Rows;i++ )
    {
        if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) == 1 )
        {
            strPars += (strPars == ''? '?' : '&') +'nPedImpostoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PedImpostoID')));                
            strPars += '&nValorImposto=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto')));                
            strPars += '&nBaseCalculo='+ escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'BaseCalculo')));                
        }
    }

    if (strPars!='')
    {
        dsoGen02.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/updateimpostoscomplementaresdata.asp' + strPars;
        dsoGen02.ondatasetcomplete = updateImpostosComplementaresDataInServer_DSC;
        dsoGen02.refresh();
    }
    else
        updateImpostosComplementaresDataInServer_DSC();                
}

function updateImpostosComplementaresDataInServer_DSC()
{
 	lockControlsInModalWin(false);	
	getDataInServer();	
	return null;
}
*/

/*
function updateImpostosDataInServer()
{
    var strPars = '';
    for ( i=2;i<fg.Rows;i++ )
    {
        if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) == 1 )
        {
            strPars += (strPars == ''? '?' : '&') +'nPedImpostoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PedImpostoID')));                
            strPars += '&nValorImposto=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorImposto')));                
            strPars += '&nBaseCalculo='+ escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'BaseCalculo')));                
            strPars += '&nTipoImposto='+ escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoImposto')));                
        }
    }

    if (strPars!='')  
    {
        dsoGen02.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/updateimpostosdata.asp' + strPars;
        dsoGen02.ondatasetcomplete = updateImpostosDataInServer_DSC;
        dsoGen02.refresh();
    }
    else
        updateImpostosDataInServer_DSC();                
}

function updateImpostosDataInServer_DSC()
{
 	lockControlsInModalWin(false);	
	getDataInServer();	
	return null;
}
*/

function btnImpostosClick(ctrl)
{
	glb_BtnImpostosMode = (glb_BtnImpostosMode == 0 ? 1 : 0);
	
	setBtnImpostosMode();
	
	getDataInServer();
}

function setBtnImpostosMode()
{
	if(glb_BtnImpostosMode == 0)
	{
		btnImpostos.value = 'Impostos complementares';
	}
	else if(glb_BtnImpostosMode == 1)
	{
		btnImpostos.value = 'Impostos';
	}
}

function selImpostoIDOnchange()
{
	getDataInServer();
}
