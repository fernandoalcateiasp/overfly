using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using WSData;
using java.lang;

namespace DanfeXslt
{
	public partial class DanfePage : System.Web.UI.OverflyPage
	{
		private Integer notaFiscalID;
		protected Integer NotaFiscalID
		{
			get { return notaFiscalID; }
			set { notaFiscalID = value; }
		}

		protected string HEADER {
			get {
				return "<?xml version='1.0' encoding='UTF-8'?><?xml-stylesheet type='text/xsl' href='danfeRetorno.xslt'?>";
			}
		}
		
		protected string SQL {
			get {
				string result = "";
				
				result +=
					"SELECT " +
						"REPLACE(CONVERT(VARCHAR(MAX),XMLNFe),'xmlns=\"http://www.portalfiscal.inf.br/nfe\"','') as xmldData " +
					"FROM " +
						"NotasFiscais WITH(NOLOCK)" +
					"WHERE " +
						"XMLNFe IS NOT NULL AND ";

				result += "NotaFiscalID = " + notaFiscalID;
				
				return result;
			}
		}

		private DataSet xmlDataSet;
		protected DataSet XmlDataSet {
			get {
				xmlDataSet = DataInterfaceObj.getRemoteData(SQL);
				
				return xmlDataSet;
			}
		}

		private string resultXML;
		protected string ResultXML {
			get {
				if (resultXML == null) {
					// Recupera os dados do banco.
					if (XmlDataSet.Tables[1].Rows.Count > 0) {
						// Pega o xml da tabela.
						resultXML = (string)XmlDataSet.Tables[1].Rows[0]["xmldData"];
						// Se necess�rio, elimina o cabe�alho e fica s� com os dados.
						if(resultXML.StartsWith("<?xml version='1.0' encoding='UTF-8'?>")) {
							resultXML = resultXML.Substring(
								"<?xml version='1.0' encoding='UTF-8'?>".Length);
						}
						// Coloca o header com as informa��es de transforma��o xslt e os dados.
						resultXML = HEADER + resultXML;
					}
					// N�o h� dados. Ent�o mostra uma mensagem.
					else {
						resultXML = "<msg>N�o h� dados a exibir.</msg>";
					}
				}

				return resultXML;
			}
		}

		protected override void PageLoad(object sender, EventArgs e) {
			Response.ContentType = "text/xml";
			Response.ContentEncoding = System.Text.Encoding.UTF8;
			Response.Write(ResultXML);
		}
	}
}
