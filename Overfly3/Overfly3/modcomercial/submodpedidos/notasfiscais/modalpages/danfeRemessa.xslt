<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:decimal-format name="real" decimal-separator="," grouping-separator="." zero-digit="0" NaN="" />
	
	<xsl:template match="/">

		<html>
			<head>
				<link href="danfe.css" rel="stylesheet" type="text/css" />
			</head>
			<body>
				<!-- CABECALHO -->
				<table border="1" cellpadding="0" cellspacing="0" >
					<tr>
						<td>
							<p class="negrito">
								<xsl:value-of select="NFe/infNFe/emit/xNome" />
							</p>
							<p>
								<xsl:value-of select="NFe/infNFe/emit/enderEmit/xLgr" />, <xsl:value-of select="NFe/infNFe/emit/enderEmit/nro" /> <xsl:value-of select="NFe/infNFe/emit/enderEmit/xCpl" /> - <xsl:value-of select="NFe/infNFe/emit/enderEmit/xBairro" />
								<br />
								<xsl:value-of select="NFe/infNFe/emit/enderEmit/xMun" />, <xsl:value-of select="NFe/infNFe/emit/enderEmit/UF" /> CEP: <xsl:value-of select="NFe/infNFe/emit/enderEmit/CEP" /> 
								<br />
								Fone/Fax: <xsl:value-of select="NFe/infNFe/emit/enderEmit/fone" />
								
							</p>
						</td>
						<td width="160">
							<center>
								<h3>DANFE</h3>
								<p>Documento Auxiliar da Nota Fiscal Eletrônica</p>
							</center>
						</td>
						<td>
							<div id="TipoNFe">
								0 - Entrada	<br />
								1 - Saída
							</div>

							<div id="ValorTipoNFe">
								<span>
									<xsl:value-of select="NFe/infNFe/ide/tpNF" />
								</span>
							</div>
						</td>
						<td>
							<center>
								<p class="negrito">
									Nº <xsl:value-of select="format-number(NFe/infNFe/ide/nNF,'000000000')"/><br />
									Série: <xsl:value-of select="NFe/infNFe/ide/serie"/>
								</p>
							</center>
						</td>
						
					</tr>
					
				</table>
				<br />
				
				<!-- NFe -->
				<table border="1" cellpadding="0" cellspacing="0">
					<tr>
						<th colspan="4">Natureza da Operação</th>
					</tr>
					<tr>
						<td colspan="4">
							<xsl:value-of select="NFe/infNFe/ide/natOp" />
						</td>
					</tr>
					<tr>
						<th>Incrição Estadual</th>
						<th>Incrição Estadual do Sbust. Trib.</th>
						<th>CNPJ</th>
						<th>Protocolo de Autorização de Uso</th>
					</tr>
					<tr>
						<td><xsl:value-of select="NFe/infNFe/emit/IE"/></td>
						<td><xsl:value-of select="NFe/infNFe/emit/IEST"/>123456789-98-08</td>
						<td><xsl:value-of select="NFe/infNFe/emit/CNPJ"/>
						</td>
						<td><xsl:value-of select="protNFe/infProt/nProt"/></td>
					</tr>
				</table>
				<br />
				<!-- DESTINATÁRIO REMETENTE -->
				<p class="titulo">DESTINATÁRIO/REMETENTE</p>
				<table border="1" cellspacing="0" cellpadding="0">
					<tr>
						<th colspan="2">Nome/Razão Social</th>
						<th>CNPJ/CPF</th>
						<th>Data da Emissão</th>
					</tr>
					<tr>
						<td colspan="2"><xsl:value-of select="NFe/infNFe/dest/xNome"/></td>
						<td><xsl:value-of select="NFe/infNFe/dest/CNPJ"/></td>
						<td>
							<xsl:value-of select="concat(substring(NFe/infNFe/ide/dEmi, 9, 2), '/', substring(NFe/infNFe/ide/dEmi, 6, 2), '/', substring(NFe/infNFe/ide/dEmi, 1, 4))" />
						</td>
						
					</tr>
					<tr>
						<th>Endereço</th>
						<th>Bairro</th>
						<th>CEP</th>
						<th>Data da Entrada/Saída</th>
					</tr>
					<tr>
						<td>
							<xsl:value-of select="NFe/infNFe/dest/enderDest/xLgr"/>,
							<xsl:value-of select="NFe/infNFe/dest/enderDest/nro"/> 
							<xsl:value-of select="NFe/infNFe/dest/enderDest/xCpl"/> 
							
						</td>
						<td><xsl:value-of select="NFe/infNFe/dest/enderDest/xBairro"/></td>
						<td><xsl:value-of select="NFe/infNFe/dest/enderDest/CEP"/></td>
						<td><xsl:value-of select="concat(substring(NFe/infNFe/ide/dSaiEnt, 9, 2), '/', substring(NFe/infNFe/ide/dSaiEnt, 6, 2), '/', substring(NFe/infNFe/ide/dSaiEnt, 1, 4))" /></td>
						
					</tr>
					<tr>
						<th>Município</th>
						<th>Fone/Fax</th>
						<th>UF</th>
						<th>Inscrição Estadual</th>
					</tr>
					<tr>
						<td><xsl:value-of select="NFe/infNFe/dest/enderDest/xMun"/></td>
						<td><xsl:value-of select="NFe/infNFe/dest/enderDest/fone"/></td>
						<td><xsl:value-of select="NFe/infNFe/dest/enderDest/UF"/></td>
						<td><xsl:value-of select="NFe/infNFe/dest/IE"/></td>
						
					</tr>
				</table>
				<br />
				<!-- IMPOSTO -->
				<p class="titulo">CÁLCULO DO IMPOSTO</p>
				<table id="TABLE1" border="1" cellpadding="0" cellspacing="0">
					<tr>
						<th>Base de Cálculo do ICMS</th>
						<th>Valor do ICMS</th>
						<th>Base de Cálculo do ICMS </th>
						<th>Valor do ICMS ST</th>
						<th colspan="2">Valor Total do Produto</th>
					</tr>
					<tr>
						<td>
							<xsl:value-of select="format-number(NFe/infNFe/total/ICMSTot/vBC, '#.##0,00', 'real')" />
						</td>
						<td>
							<xsl:value-of select="format-number(NFe/infNFe/total/ICMSTot/vICMS, '#.##0,00', 'real')" />
						</td>
						<td>
							<xsl:value-of select="format-number(NFe/infNFe/total/ICMSTot/vBCST, '#.##0,00', 'real')" />
						</td>
						<td>
							<xsl:value-of select="format-number(NFe/infNFe/total/ICMSTot/vST, '#.##0,00', 'real')" />
						</td>
						<td colspan="2">
							<xsl:value-of select="format-number(NFe/infNFe/total/ICMSTot/vProd, '#.##0,00', 'real')" />
						</td>
					</tr>
					<tr>
						<th>Valor do Frete</th>
						<th>Valor do Seguro</th>
						<th>Desconto</th>
						<th>Outras Despesas Acessórias</th>
						<th>Valor do IPI</th>
						<th>Valor Total da Nota</th>
					</tr>
					<tr>
						<td>
							<xsl:value-of select="format-number(NFe/infNFe/total/ICMSTot/vFrete, '#.##0,00', 'real')" />
						</td>
						<td>
							<xsl:value-of select="format-number(NFe/infNFe/total/ICMSTot/vSeg, '#.##0,00', 'real')" />
						</td>
						<td>
							<xsl:value-of select="format-number(NFe/infNFe/total/ICMSTot/vDesc, '#.##0,00', 'real')" />
						</td>
						<td>
							<xsl:value-of select="format-number(NFe/infNFe/total/ICMSTot/vOutro, '#.##0,00', 'real')" />
						</td>
						<td>
							<xsl:value-of select="format-number(NFe/infNFe/total/ICMSTot/vIPI, '#.##0,00', 'real')" />
						</td>
						<td>
							<xsl:value-of select="format-number(NFe/infNFe/total/ICMSTot/vNF, '#.##0,00', 'real')" />
						</td>
					</tr>
				</table>
				<br />
				<!-- DISTRIBUIDORA -->
				<p class="titulo">TRANSPORTADORA/VOLUMES TRANSPORTADOS</p>
				<table border="1" cellpadding="0" cellspacing="0">
					<tr>
						<th colspan="3">Nome/Razão Social</th>
						<th>Frete por Conta</th>
						<th>Código ANTT</th>
						<th>Placa do Veículo</th>
						<th>UF</th>
						<th>CNPJ/CPF</th>
					</tr>
					<tr>
						<td colspan="3">
							<xsl:value-of select="NFe/infNFe/transp/transporta/xNome" />
						</td>
						<td>
							<!-- FRETE POR CONTA -->
							<xsl:value-of select="NFe/infNFe/transp/modFrete" />
						</td>
						<td>
							<xsl:value-of select="NFe/infNFe/transp/veicTransp/RNTC" />
						</td>
						<td>
							<xsl:value-of select="NFe/infNFe/transp/veicTransp/placa" />
						</td>
						<td>
							<xsl:value-of select="NFe/infNFe/transp/veicTransp/UF" />
						</td>
						<td>
							<xsl:value-of select="NFe/infNFe/transp/transporta/CNPJ" />
						</td>
					</tr>
					<tr>
						<th colspan="3">Endereço</th>
						<th colspan="3">Município</th>
						<th>UF</th>
						<th>Inscrição Estadual</th>
					</tr>
					<tr>
						<td colspan="3">
							<xsl:value-of select="NFe/infNFe/transp/transporta/xEnder" />
						</td>
						<td colspan="3">
							<xsl:value-of select="NFe/infNFe/transp/transporta/xMun" />
						</td>
						<td>
							<xsl:value-of select="NFe/infNFe/transp/transporta/UF" />
						</td>
						<td>
							<xsl:value-of select="NFe/infNFe/transp/transporta/IE" />
						</td>
					</tr>
					<tr>
						<th>Quantidade</th>
						<th>Espécie</th>
						<th>Marca</th>
						<th colspan="3">Numeração</th>
						<th>Peso Bruto</th>
						<th>Peso Líquido</th>
					</tr>
					<tr>
						<td>
							<xsl:value-of select="NFe/infNFe/transp/vol/qVol" />
						</td>
						<td>
							<xsl:value-of select="NFe/infNFe/transp/vol/esp" />
						</td>
						<td>
							<xsl:value-of select="NFe/infNFe/transp/vol/marca" />
						</td>
						<td colspan="3">
							<xsl:value-of select="NFe/infNFe/transp/vol/nVol" />
						</td>
						<td>
							<xsl:value-of select="NFe/infNFe/transp/vol/pesoB" />
						</td>
						<td>
							<xsl:value-of select="NFe/infNFe/transp/vol/pesoL" />
						</td>
					</tr>
				</table>
				<br />
				<!-- ITENS -->
				<p class="titulo">DADOS DOS PRODUTOS/SERVIÇOS</p>
				<table border="1" cellpadding="0" cellspacing="0">
					<tr>
						<th>ID</th>
						<th>Desc do Prod/Serv</th>
						<th>NCM/SH</th>
						<th>CST</th>
						<th>CFOP</th>
						<th>Unid</th>
						<th>Quant</th>
						<th>Valor Unit</th>
						<th>Valor Total</th>
						<th>B.Calc ICMS</th>
						<th>Valor ICMS</th>
						<th>Valor IPI</th>
						<th>Aliq. ICMS</th>
						<th>Aliq. IPI</th>
					</tr>
					<xsl:for-each select="NFe/infNFe/det">
						<tr>
							<td>
								<xsl:value-of select="prod/cProd" />
							</td>
							<td>
								<xsl:value-of select="prod/xProd" />
							</td>
							<td>
								<xsl:value-of select="prod/NCM" />
							</td>
							<td>
								<xsl:value-of select="imposto/ICMS/ICMS00/CST" />
								<xsl:value-of select="imposto/ICMS/ICMS10/CST" />
								<xsl:value-of select="imposto/ICMS/ICMS20/CST" />
								<xsl:value-of select="imposto/ICMS/ICMS30/CST" />
								<xsl:value-of select="imposto/ICMS/ICMS40/CST" />
								<xsl:value-of select="imposto/ICMS/ICMS41/CST" />
								<xsl:value-of select="imposto/ICMS/ICMS50/CST" />
								<xsl:value-of select="imposto/ICMS/ICMS51/CST" />
								<xsl:value-of select="imposto/ICMS/ICMS60/CST" />
								<xsl:value-of select="imposto/ICMS/ICMS70/CST" />
								<xsl:value-of select="imposto/ICMS/ICMS90/CST" />
								<xsl:value-of select="imposto/ICMS/ICMS99/CST" />
							</td>
							<td>
								<xsl:value-of select="prod/CFOP" />
							</td>
							<td>
								<xsl:value-of select="prod/uCom" />
							</td>

							<td>
								<xsl:value-of select="prod/qCom" />
							</td>
							<td>
								<xsl:value-of select="format-number(prod/vUnCom, '#.##0,00', 'real')" />
							</td>
							<td>
								<xsl:value-of select="format-number(prod/vProd, '#.##0,00', 'real')" />
							</td>

							<td>
								<xsl:value-of select="format-number(imposto/ICMS/ICMS00/vBC, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS10/vBC, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS20/vBC, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS30/vBC, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS40/vBC, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS41/vBC, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS50/vBC, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS51/vBC, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS60/vBC, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS70/vBC, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS90/vBC, '#.##0,00', 'real')" />
							</td>
							<td>
								<xsl:value-of select="format-number(imposto/ICMS/ICMS00/vICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS10/vICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS20/vICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS30/vICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS40/vICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS41/vICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS50/vICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS51/vICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS60/vICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS70/vICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS90/vICMS, '#.##0,00', 'real')" />
							</td>
							<td>
								<xsl:value-of select="format-number(imposto/IPI/IPITrib/vIPI, '#.##0,00', 'real')" />
							</td>
							<td>
								<xsl:value-of select="format-number(imposto/ICMS/ICMS00/pICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS10/pICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS20/pICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS30/pICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS40/pICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS41/pICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS50/pICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS51/pICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS60/pICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS70/pICMS, '#.##0,00', 'real')" />
								<xsl:value-of select="format-number(imposto/ICMS/ICMS90/pICMS, '#.##0,00', 'real')" />
							</td>
							<td>
								<xsl:value-of select="format-number(imposto/IPI/IPITrib/pIPI, '#.##0,00', 'real')" />
							</td>
						</tr>
					</xsl:for-each>
				</table>
				<br />
				<!-- ISSQN -->
				<p class="titulo">CÁLCULO DO ISSQN</p>
				<table border="1" cellpadding="0" cellspacing="0">
					<tr>
						<th>Inscrição Municipal</th>
						<th>Valor Total dos Serviços</th>
						<th>Base de Cálculo do ISSQN</th>
						<th>Valor do ISSQN</th>
					</tr>
					<tr>
						<td>
							<xsl:value-of select="NFe/infNFe/emit/IM"/>
						</td>
						<td>
							<xsl:value-of select="format-number(NFe/infNFe/total/ISSQNtot/vServ, '#.##0,00', 'real')" />
						</td>
						<td>
							<xsl:value-of select="format-number(NFe/infNFe/total/ISSQNtot/vBC, '#.##0,00', 'real')" />
						</td>
						<td>
							<xsl:value-of select="format-number(NFe/infNFe/total/ISSQNtot/vISS, '#.##0,00', 'real')" />
						</td>
					</tr>
				</table>
				<br />
				
				<!-- DADOS ADICIONAIS -->
				<p class="titulo">DADOS ADICIONAIS</p>
				<table border="1" cellpadding="0" cellspacing="0" class="DadosAdicionais">
					<tr>
						<th>Informações Complementares</th>
						<th>Reservado ao Fisco</th>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
				<!--
				<br />
				-->				
				<!-- LINHA DE CORTE -->
				<!--
				<hr class="LinhaDeCorte" />
				<br />
				-->


				<!-- CANHOTO -->
				<!--
				<table border="1" cellpadding="0" cellspacing="0">
					<tr>
						<td colspan="2">x</td>
						<td rowspan="2" class="NumeroCanhoto">
							NFe
							<br />
							Nº <xsl:value-of select="format-number(NFe/infNFe/ide/nNF,'000,000,000')"/>
							<br />
							SÉRIE <xsl:value-of select="NFe/infNFe/ide/serie"/>
							
							
							
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
				-->
				
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>