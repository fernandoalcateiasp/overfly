<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
	' Caminho da aplica��o.
	Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

	pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Dim i, notaFiscalID, nPastaXml

notaFiscalID = 0
nPastaXml = 0

For i = 1 To Request.QueryString("notaFiscalID").Count    
	notaFiscalID = Request.QueryString("notaFiscalID")(i)
Next

For i = 1 To Request.QueryString("nPastaXml").Count    
	nPastaXml = Request.QueryString("nPastaXml")(i)
Next
%>

<html>
<head>
	<title></title>

	<style type="text/css">
		p#lblSelecione
		{
			BACKGROUND-COLOR: transparent;
			FONT-FAMILY: Tahoma,Arial,sans-serif;
			FONT-SIZE: 8pt;
			FONT-WEIGHT: normal;
			HEIGHT: 16px;
			LEFT: 10px;
			POSITION: absolute;
			TEXT-ALIGN: left;
			TOP: 30px;
			WIDTH: 160px;
			CURSOR: default;
		}
		input#inputFile
		{
			FONT-FAMILY: Tahoma, Arial;
			FONT-SIZE: 8pt;
			HEIGHT: 24px;
			POSITION: absolute;
			WIDTH: 330px;
			
			TOP: 50px;
			LEFT: 10px;
		}
		input#inputEnviar
		{
			FONT-FAMILY: Tahoma, Arial;
			FONT-SIZE: 8pt;
			HEIGHT: 24px;
			POSITION: absolute;
			WIDTH: 80px;

			TOP: 100px;
			LEFT: 10px;
		}
	</style>
</head>
<body>
<form action="<%Response.Write(pagesURLRoot)%>/modcomercial/submodpedidos/notasfiscais/serverside/saveUploadedFile.asp?notaFiscalID=<% Response.Write notaFiscalID %>&nPastaXml=<% Response.Write nPastaXml %>"
	enctype="multipart/form-data" method="post">
	
	<p id="lblSelecione">Selecione a NFe:</p>
		
	<input id="inputFile" type="file" name="datafile" size="30" class="btns"/>
	<input id="inputEnviar" type="submit" value="Enviar" class="btns"/>

	<input type="hidden" id="notaFiscalID" name="notaFiscalID" value="<% Response.Write notaFiscalID %>"/>
	<input type="hidden" id="nPastaXml" name="nPastaXml" value="<% Response.Write nPastaXml %>"/>
</form>

</body>
</html>