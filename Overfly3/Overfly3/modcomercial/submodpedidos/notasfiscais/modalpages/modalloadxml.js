/********************************************************************
modalloadxml.js

Library javascript para o modalloadxml.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nUserID = 0;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
chks_onclick()
btn_onclick(ctl)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();    

	// configuracao inicial do html
    setupPage();
    
    // ajusta o body do html
    with (modalloadxmlBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    showExtFrame(window, true);
    window.focus();
    
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Load XML', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
        
    // reajusta dimensoes e reposiciona a janela
	//redimAndReposicionModalWin(295, 180, false);
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
	// ajusta o divFields
    elem = divFields;
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modWidth - 2 * ELEM_GAP - 6;
    }
    
    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
	    return null;
		lockControlsInModalWin(true);	
    }
    else if (controlID == 'btnCanc')
    {
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null); 
    }
}
