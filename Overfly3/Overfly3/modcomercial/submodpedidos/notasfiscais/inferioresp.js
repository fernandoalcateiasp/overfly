/********************************************************************
inferioresp.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo'
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID) {
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    if ((folderID == 20008) || (folderID == 20009)) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM NotasFiscais a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.NotaFiscalID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 24035) {
        dso.SQL = 'SELECT * FROM NotasFiscais a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.NotaFiscalID = ' + idToFind;

        return 'dso01JoinSup_DSC';
    }

    else if (folderID == 24038) // Importa��o
    {
        dso.SQL = 'SELECT d.Ordem, a.ProdutoID AS ID, a.Familia AS Produto, a.Modelo, b.DI AS DI, b.dtRegistro AS DataRegistro, ' +
                          'b.CodigoExportador AS CodigoExportador, b.ValorDespesaAduaneira AS ValorDespesaAduaneira, ' +
                          'b.LocalDesembaraco AS LocalDesembaraco, b.UFDesembaraco AS UFDesembaraco, ' +
                          'b.dtDesembaraco AS dtDesembaraco, c.Adicao AS Adicao, c.Sequencia AS Sequencia, ' +
                          'c.CodigoFabricante AS CodigoFabricante, c.ValorDesconto AS ValorDesconto ' +
	                'FROM NotasFiscais_Itens a WITH(NOLOCK) ' +
			                'INNER JOIN NotasFiscais_Itens_Importacao b WITH(NOLOCK) ON (b.NFItemID = a.NFItemID) ' +
			                'INNER JOIN NotasFiscais_Itens_ImportacaoAdicao c WITH(NOLOCK) ON (c.NFItemImportacaoID = b.NFItemImportacaoID) ' +
			                'INNER JOIN Pedidos_Itens d WITH(NOLOCK) ON (d.PedItemID = a.PedItemID) ' +
	                'WHERE (a.NotaFiscalID = ' + idToFind + ') ' +
	                'Order By d.Ordem, b.DI';
        return 'dso01Grid_DSC';
    }

    else if (folderID == 24032) // Dados da Pessoa
    {
        dso.SQL = 'SELECT a.*, b.ItemMasculino AS TipoPessoa , c.* ' +
                    'FROM NotasFiscais_Pessoas a WITH(NOLOCK) ' +
			                'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.TipoID) ' +
                            'INNER JOIN NotasFiscais c WITH(NOLOCK) ON (c.NotaFiscalID = a.NotaFiscalID) ' +
                    'WHERE A.NotaFiscalID = ' + idToFind +
                    ' Order By Ordem';
        return 'dso01Grid_DSC';
    }

    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.RegistroID = ' + idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24033) // Itens
    {
        dso.SQL = 'SELECT a.*, a.Origem + b.CodigoTributacao AS CST ' +
            'FROM NotasFiscais_Itens a WITH(NOLOCK) ' +
                    'LEFT OUTER JOIN NotasFiscais_Itens_Impostos b WITH(NOLOCK) ON (a.NFItemID = b.NFItemID AND b.ImpostoID = 9)' +
            'WHERE ' + sFiltro + 'a.NotaFiscalID = ' + idToFind +
        'ORDER BY a.NFItemID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24034) // Duplicatas
    {
        dso.SQL = 'SELECT a.* FROM NotasFiscais_Duplicatas a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.NotaFiscalID = ' + idToFind + ' ' +
        'ORDER BY a.Duplicata';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24036) // Mensagens
    {
        dso.SQL = 'SELECT a.* FROM NotasFiscais_Mensagens a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.NotaFiscalID = ' + idToFind + ' ' +
        'ORDER BY a.Ordem';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24037) // Cartas de Correcao
    {
        dso.SQL = 'SELECT a.*, b.RecursoAbreviado as Estado ' +
        'FROM CartasCorrecao a WITH(NOLOCK), Recursos b WITH(NOLOCK) WHERE ' + sFiltro +
        'a.EstadoID=b.RecursoID AND a.NotaFiscalID = ' + idToFind + ' ' +
        'ORDER BY a.CartaCorrecaoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24039) // Referencias
    {
        //   fillGridMask(fg,currDSO,['Ordem', 'SerieNF' ,'NotaFiscal', 'ChaveAcesso', 'Ano_Mes', 'DocumentoFederal', 'ModeloNF'],
        dso.SQL = 'SELECT * ' +
                  'FROM NotasFiscais_Referencias WITH(NOLOCK) WHERE NotaFiscalID = ' + idToFind;
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID) {
    setConnection(dso);

    // LOG
    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID) {
    var i = 0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

    cleanupSelInControlBar('inf', 1);

    vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(24032);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24032); //Dados da Pessoa    

    vPastaName = window.top.getPastaName(24033);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24033); //Itens

    vPastaName = window.top.getPastaName(24034);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24034); //Duplicatas

    vPastaName = window.top.getPastaName(24035);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24035); //Transportador

    vPastaName = window.top.getPastaName(24036);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24036); //Mensagens

    vPastaName = window.top.getPastaName(24037);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24037); //Cartas de Corre��o

    vPastaName = window.top.getPastaName(24038);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24038); //Importa��o

    vPastaName = window.top.getPastaName(24039);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 24039); //Referencia		

    // Nao mexer - Inicio de automacao ============================== 	
    // Tenta selecionar novamente a pasta anterior selecionada no combo
    // Se nao mais existe a pasta anteriormente selecionada no combo
    // seleciona a pasta default

    stripFoldersByRight(currSelection);

    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID) {
    //@@ return true se tudo ok, caso contrario return false;

    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (currLine < 0)
            return false;
    }

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO) {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.FrozenCols = 0;

    if (folderID == 20010) // LOG
    {
        headerGrid(fg, ['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);

        fillGridMask(fg, currDSO, ['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['', '99/99/9999', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm:ss', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1);
    }
        //Dados da Pessoa
    else if (folderID == 24032) {
        headerGrid(fg, ['Tipo', 'ID', 'Nome', 'Fantasia', 'Pais',
                       'C�digo', 'CEP', 'UF', 'C�digo', 'Cidade', 'C�digo', 'Bairro', 'Endere�o',
                       'Numero', 'Complemento', 'DDI', 'DDD', 'Telefone', 'Doc Federal', 'Doc Estadual', 'Doc Estadual ST',
                       'Doc Municipal', 'Suframa', 'CNAE', 'RNTC'], []);

        fillGridMask(fg, currDSO, ['TipoPessoa', 'PessoaID', 'Nome', 'Fantasia', 'Pais', 'PaisCodigo', 'CEP', 'UF', 'UFCodigo',
                                 'Cidade', 'CidadeCodigo', 'Bairro', 'Endereco', 'Numero', 'Complemento', 'DDI', 'DDD', 'Telefone',
                                 'DocumentoFederal', 'DocumentoEstadual', 'DocumentoEstadualST', 'DocumentoMunicipal', 'Suframa',
                                 'CNAE', 'RNTC'],
                                ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                                ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);
        fg.FrozenCols = 2;
        // alinha colunas a direita
        alignColsInGrid(fg, [1, 5, 6, 8, 10, 13, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]);
    }
        // Itens
    else if (folderID == 24033) {
        //Alterado para pedidos de toma��o de servico
        var bTomacaoServico = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'TomacaoServico' + '\'' + '].value');

        var bServicoEstadual = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'ServicoEstadual' + '\'' + '].value');

        var aHoldCols = new Array();

        if (bTomacaoServico)
            aHoldCols = [2, 3, 4, 6, 7, 8, 9, 11, 12, 13, 14, 15, 18, 19, 20];
        else if (bServicoEstadual) // Inclus�o do Servi�o Estadual - FRG 25/05/2015.
            aHoldCols = [2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 18, 19, 20];
        else
            aHoldCols = [];

        headerGrid(fg, ['T', 'ID', 'Produto', 'Marca', 'Modelo',
                       'Descri��o', 'CFOP', 'Class Fiscal', 'CST', 'G�nero', 'Lista Servico', 'IVAST', 'MBICMS', 'MBICMSST',
                       'Origem', 'Un', 'Quant', 'Valor Unit�rio', 'Valor Frete', 'Valor Seguro', 'Valor Desconto', 'Valor Total'], aHoldCols);

        fillGridMask(fg, currDSO, ['Tipo', 'ProdutoID', 'Familia', 'Marca', 'Modelo',
                                 'Descricao', 'CFOP', 'NCM', 'CST', 'Genero', 'CodigoListaServico', 'IVAST', 'ModalidadeBCICMS',
                                 'ModalidadeBCICMSST', 'Origem', 'Unidade', 'Quantidade', 'ValorUnitario', 'ValorFrete', 'ValorSeguro',
                                 'ValorDesconto', 'ValorTotal'],
                                ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                                ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '###,###,###.00', '###,###,###.00', '###,###,###.00',
                                '###,###,###.00', '###,###,###.00']);
        fg.FrozenCols = 2;
        // alinha colunas a direita
        alignColsInGrid(fg, [1, 14, 15, 16, 17, 18, 19, 20, 21]);
    }
        // Duplicatas
    else if (folderID == 24034) {
        headerGrid(fg, ['Duplicata', 'Vencimento', 'Valor'], []);
        fillGridMask(fg, currDSO, ['Duplicata', 'Vencimento', 'Valor'],
                                ['', '', ''],
                                ['', dTFormat, '###,###,###.00']);

        // alinha colunas a direita
        alignColsInGrid(fg, [2]);
    }
        // Mensagens
    else if (folderID == 24036) {
        headerGrid(fg, ['Ordem', 'Mensagem'], []);
        fillGridMask(fg, currDSO, ['Ordem', 'Mensagem']
                                     , ['', '']);

    }
        // Cartas Correcao
    else if (folderID == 24037) {
        headerGrid(fg, ['ID', 'Est', 'Data', 'Emissor'], []);
        fillGridMask(fg, currDSO, ['CartaCorrecaoID', 'Estado', 'dtCartaCorrecao', 'Emissor'],
                                ['', '', '', ''],
                                ['', '', dTFormat, '']);
    }

        //Importa��o
    else if (folderID == 24038) {
        headerGrid(fg, ['Ordem', 'ID', 'Produto', 'Modelo', 'DI', 'Data Registro', 'Exp',
                       'DA', 'Local Desembara�o', 'UF', 'Data Desemb', 'Adi��o', 'Sequ�ncia',
                       'Fabricante', 'Valor Desconto'], []);

        glb_aCelHint = [[0, 4, 'Documento de Importa��o'],
                        [0, 6, 'C�digo Exportador'],
                        [0, 7, 'Despesa Aduaneira'],
                        [0, 9, 'UF Desembara�o'],
                        [0, 10, 'Data Desembara�o'],
                        [0, 13, 'C�digo Fabricante']
        ];

        fillGridMask(fg, currDSO, ['Ordem', 'ID', 'Produto', 'Modelo', 'DI', 'DataRegistro', 'CodigoExportador',
                                 'ValorDespesaAduaneira', 'LocalDesembaraco', 'UFDesembaraco', 'dtDesembaraco', 'Adicao', 'Sequencia',
                                 'CodigoFabricante', 'ValorDesconto'],
                                ['', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                                ['', '', '', '', '', '', '', '###,###,###.00', '', '', '', '', '', '', '###,###,###.00']);
        fg.FrozenCols = 2;
        fg.ExplorerBar = 5;
        // alinha colunas a direita
        alignColsInGrid(fg, [0, 4, 7, 11, 12, 14]);

        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }

        //Referencia
    else if (folderID == 24039) {
        headerGrid(fg, ['Chave de Acesso', 'Nota Fiscal', 'S�rie', 'Modelo', 'Ano M�s', 'Documento Federal', 'C�digo UF', 'NotaFiscalReferenciaID'], [7]);

        fillGridMask(fg, currDSO, ['ChaveAcesso', 'NotaFiscal', 'SerieNF', 'ModeloNF', 'Ano_Mes', 'DocumentoFederal', 'UFCodigo', 'NotaFiscalReferenciaID'],
                                ['', '', '', '', '', '', ''],
                                ['', '', '', '', '', '', '']);

        glb_aCelHint = [[0, 6, 'C�digo IBGE do Estado']];

        fg.ExplorerBar = 5;

        fg.FrozenCols = 2;

        // alinha colunas a direita
        // alignColsInGrid(fg,[1,14,15,16,17,18,19,20,21]);
    }

}
