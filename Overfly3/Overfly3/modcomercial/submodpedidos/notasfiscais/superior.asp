<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="notasfiscaissup01Html" name="notasfiscaissup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf    
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/notasfiscais/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/notasfiscais/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="notasfiscaissup01Body" name="notasfiscaissup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- //@@ Os dsos sao definidos de acordo com o form -->
        
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" name="txtEstadoID" class="fldGeneral">
        <p id="lblNotaFiscal" name="lblNotaFiscal" class="lblGeneral">Nota</p>
        <input type="text" id="txtNotaFiscal" name="txtNotaFiscal" DATASRC="#dsoSup01" DATAFLD="NotaFiscal" class="fldGeneral">
        <p id="lbldtNotaFiscal" name="lbldtNotaFiscal" class="lblGeneral">Data Nota</p>
        <input type="text" id="txtdtNotaFiscal" name="txtdtNotaFiscal" DATASRC="#dsoSup01" DATAFLD="V_dtNotaFiscal" class="fldGeneral">
        <p id="lbldtEntradaSaida" name="lbldtEntradaSaida" class="lblGeneral">Entrada/Sa�da</p>
        <input type="text" id="txtdtEntradaSaida" name="txtdtEntradaSaida" DATASRC="#dsoSup01" DATAFLD="V_dtEntradaSaida" class="fldGeneral">
        <p id="lblStatus" name="lblStatus" class="lblGeneral">Status</p>
        <input type="text" id="txtStatus" name="txtStatus" DATASRC="#dsoSup01" DATAFLD="StatusNFe" class="fldGeneral">        
        <p id="lblNatureza" name="lblNatureza" class="lblGeneral">Natureza da Opera��o</p>
        <input type="text" id="txtNatureza" name="txtNatureza" DATASRC="#dsoSup01" DATAFLD="Natureza" class="fldGeneral">
        <p id="lblCFOP" name="lblCFOP" class="lblGeneral">CFOPs</p>
        <input type="text" id="txtCFOP" name="txtCFOP" DATASRC="#dsoSup01" DATAFLD="CFOPs" class="fldGeneral">
        
        <p id="lblNumeroRPS" name="lblNumeroRPS" class="lblGeneral">RPS</p>
        <input type="text" id="txtNumeroRPS" name="txtNumeroRPS" DATASRC="#dsoSup01" DATAFLD="NumeroRPS" class="fldGeneral">
        
        <p id="lblPedido" name="lblPedido" class="lblGeneral">Pedido</p>
        <input type="text" id="txtPedido" name="txtPedido" DATASRC="#dsoSup01" DATAFLD="PedidoID" class="fldGeneral">
        <p id="lblEmissor" name="lblEmissor" class="lblGeneral">Em</p>
        <input type="checkbox" id="chkEmissor" name="chkEmissor" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Emissor" title="Somos o emissor da Nota?">
        <p id="lblDocumentoFiscal" name="lblDocumentoFiscal" class="lblGeneral">DF</p>
        <select id="selDocumentoFiscal" name="selDocumentoFiscal" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DocumentoFiscalID" title="Documento Fiscal"></select>       
        <p id="lblSerieNF" name="lblSerieNF" class="lblGeneral">Serie</p>
        <input type="text" id="txtSerieNF" name="txtSerieNF" DATASRC="#dsoSup01" DATAFLD="SerieNF" class="fldGeneral">
        <p id="lblVendedor" name="lblVendedor" class="lblGeneral">Vendedor</p>
        <input type="text" id="txtVendedor" name="txtVendedor" DATASRC="#dsoSup01" DATAFLD="Vendedor" class="fldGeneral">
        <p id="lblSeuPedido" name="lblSeuPedido" class="lblGeneral">Seu Pedido</p>
        <input type="text" id="txtSeuPedido" name="txtSeuPedido" DATASRC="#dsoSup01" DATAFLD="SeuPedido" class="fldGeneral">
        <p id="lblChaveAcesso" name="lblChaveAcesso" class="lblGeneral">Chave Acesso</p>
        <input type="text" id="txtChaveAcesso" name="txtChaveAcesso" DATASRC="#dsoSup01" DATAFLD="ChaveAcesso" class="fldGeneral">
        <p id="lblValorFrete" name="lblValorFrete" class="lblGeneral">Valor Frete</p>
        <input type="text" id="txtValorFrete" name="txtValorFrete" DATASRC="#dsoSup01" DATAFLD="ValorFrete" class="fldGeneral"></input>
        <p id="lblValorSeguro" name="lblValorSeguro" class="lblGeneral">Valor Seguro</p>
        <input type="text" id="txtValorSeguro" name="txtValorSeguro" DATASRC="#dsoSup01" DATAFLD="ValorSeguro" class="fldGeneral"></input>
        <p id="lblValorDesconto" name="lblValorDesconto" class="lblGeneral">Valor Desconto</p>
        <input type="text" id="txtValorDesconto" name="txtValorDesconto" DATASRC="#dsoSup01" DATAFLD="ValorDesconto" class="fldGeneral"></input>
        <p id="lblOutrasDespesas" name="lblOutrasDespesas" class="lblGeneral">Outras Despesas</p>
        <input type="text" id="txtOutrasDespesas" name="txtOutrasDespesas" DATASRC="#dsoSup01" DATAFLD="OutrasDespesas" class="fldGeneral"></input>                
        <p id="lblValorTotalServicos" name="lblValorTotalServicos" class="lblGeneral">Total Servi�os</p>
        <input type="text" id="txtValorTotalServicos" name="txtValorTotalServicos" DATASRC="#dsoSup01" DATAFLD="ValorTotalServicos" class="fldGeneral"></input>
        <p id="lblValorTotalProdutos" name="lblValorTotalProdutos" class="lblGeneral">Total Produtos</p>
        <input type="text" id="txtValorTotalProdutos" name="txtValorTotalProdutos" DATASRC="#dsoSup01" DATAFLD="ValorTotalProdutos" class="fldGeneral"></input>
        <p id="lblValorTotalProdutosServicoes" name="lblValorTotalProdutosServicoes" class="lblGeneral">Total Prod. e Serv.</p>
        <input type="text" id="txtValorTotalProdutosServicoes" name="txtValorTotalProdutosServicoes" DATASRC="#dsoSup01" DATAFLD="ValorTotalProdutosServicos" class="fldGeneral" title="Total Produtos e Servi�os"></input>        
        <p id="lblValorTotalNota" name="lblValorTotalNota" class="lblGeneral">Total Nota</p>
        <input type="text" id="txtValorTotalNota" name="txtValorTotalNota" DATASRC="#dsoSup01" DATAFLD="ValorTotalNota" class="fldGeneral"></input>
        <p id="lblCodigoServico" name="lblCodigoServico" class="lblGeneral">C�digo Servi�o</p>
        <input type="text" id="txtCodigoServico" name="txtCodigoServico" DATASRC="#dsoSup01" DATAFLD="CodigoServico" class="fldGeneral"></input>
        <p id="lblDescricaoServico" name="lblDescricaoServico" class="lblGeneral">Descri��o Servi�o</p>
        <input type="text" id="txtDescricaoServico" name="txtDescricaoServico" DATASRC="#dsoSup01" DATAFLD="DescricaoServico" class="fldGeneral"></input>
   </div>
    
</body>

</html>
