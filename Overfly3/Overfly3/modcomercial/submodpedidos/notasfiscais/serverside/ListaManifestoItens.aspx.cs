using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.Attributes;

namespace Overfly3.modcomercial.submodpedidos.notasfiscais.serverside
{
    public partial class ListaManifestoItens : System.Web.UI.OverflyPage
	{
        private string Manifesto;

        public string nManifesto
        {
            get { return Manifesto; }
            set { Manifesto = value; }
        }


		protected override void PageLoad(object sender, EventArgs e)
		{
            WriteResultXML(ListagemComboItens());
		}

		protected DataSet ListagemComboItens()
		{
            // Roda a procedure sp_ProdutoMargemMinima_Pesquisar
			ProcedureParameters[] procParams = new ProcedureParameters[1];

			procParams[0] = new ProcedureParameters(
                "@ManifestoID",
				System.Data.SqlDbType.Int,
                Manifesto != "null" ? (Object)Manifesto : DBNull.Value);

			return DataInterfaceObj.execQueryProcedure(
                "sp_ManifestoNFe_Itens",
				procParams);
		}

	}
}
