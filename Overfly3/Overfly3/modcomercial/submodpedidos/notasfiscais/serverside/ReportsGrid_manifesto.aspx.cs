﻿using System;
using System.Web;
using System.Data;

namespace Overfly3.PrintJet
{
    public partial class ReportsGrid_manifesto : System.Web.UI.OverflyPage
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int RelatorioID = Convert.ToInt32(HttpContext.Current.Request.Params["RelatorioID"]);
        private int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string glb_sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        private int glb_nUsuarioID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nUsuarioID"]);
        private int sLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["sLinguaLogada"]);
        private int lista_excel = Convert.ToInt32(HttpContext.Current.Request.Params["lista_excel"]);
        int Datateste = 0;
        bool nulo = false;

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "1":
                        relatorioManifesto();
                        break;
                }
            }
        }

        public void relatorioManifesto()
        {
            //Parametros da procedure
            string empresaID = Convert.ToString(HttpContext.Current.Request.Params["empresaID"]);
            string FornecedorID = Convert.ToString(HttpContext.Current.Request.Params["FornecedorID"]);
            string GerenteProdutoID = Convert.ToString(HttpContext.Current.Request.Params["GerenteProdutoID"]);
            string NotaFiscal = Convert.ToString(HttpContext.Current.Request.Params["NotaFiscal"]);
            string EstadoID = Convert.ToString(HttpContext.Current.Request.Params["EstadoID"]);
            string dtInicio = Convert.ToString(HttpContext.Current.Request.Params["dtInicio"]);
            string dtFim = Convert.ToString(HttpContext.Current.Request.Params["dtFim"]);
            string AutorizacaoID = Convert.ToString(HttpContext.Current.Request.Params["AutorizacaoID"]);
            string Transacao = Convert.ToString(HttpContext.Current.Request.Params["Transacao"]);
            bool Obrigatorio = Convert.ToBoolean(HttpContext.Current.Request.Params["Obrigatorio"]);
            bool SP = Convert.ToBoolean(HttpContext.Current.Request.Params["SP"]);

            //Define formato de data de acordo com a lingua logada
            string param = "dd/MM/yyy";
            if (sLinguaLogada != 246)
                param = "MM/dd/yyy";

            //Recebe data atual para colocar no titulo do Excel
            DateTime DataHoje = DateTime.Now;
            string _data = DataHoje.ToString(param);

            //Formatação da tabela
            param = "<Format>" + param + "</Format>";

            string Title = "Manifesto_" + glb_sEmpresaFantasia + "_" + _data;

            string strSQL = " EXEC sp_ManifestoNFe_Consulta " + 
                                empresaID + "," + FornecedorID + "," + GerenteProdutoID + "," + NotaFiscal + "," + EstadoID + ",'" + dtInicio + "','" + 
                                dtFim + "'," + AutorizacaoID + "," + Transacao + ", " + Obrigatorio + ", " + SP + ", " + glb_nUsuarioID + ", " + lista_excel;

            //SE EXCEL btnExcel
            if (lista_excel == 1)
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");


                DataSet dsFin = new DataSet();
                dsFin = (DataSet)HttpContext.Current.Session["Query"];
                Datateste = dsFin.Tables.Count;

                //Verifica se a Query está vazia e retorna mensagem de erro
                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    return;
                }
                else
                {
                    if (Formato == 1) //Se formato PDF
                    {
                        ;
                    }

                    else if (Formato == 2) //Se formato Excel
                    {
                        Relatorio.CriarObjTabela("0.00", "0.00", "Query1", false, false, false);

                            Relatorio.CriarObjColunaNaTabela("ManifestoID", "ManifestoID", true, "ManifestoID");
                            Relatorio.CriarObjCelulaInColuna("Query", "ManifestoID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Empresa", "Empresa", true, "Empresa");
                            Relatorio.CriarObjCelulaInColuna("Query", "Empresa", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Fornecedor", "Fornecedor", true, "Fornecedor");
                            Relatorio.CriarObjCelulaInColuna("Query", "Fornecedor", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("GP", "GP", true, "GP");
                            Relatorio.CriarObjCelulaInColuna("Query", "GP", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Dias", "Dias", true, "Dias");
                            Relatorio.CriarObjCelulaInColuna("Query", "Dias", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Estado", "Estado", true, "Estado");
                            Relatorio.CriarObjCelulaInColuna("Query", "Estado", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Status_Manifesto", "Status_Manifesto", true, "Status_Manifesto");
                            Relatorio.CriarObjCelulaInColuna("Query", "Status_Manifesto", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("NotaFiscal", "NotaFiscal", true, "NotaFiscal");
                            Relatorio.CriarObjCelulaInColuna("Query", "NotaFiscal", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("TipoFinalidade", "TipoFinalidade", true, "TipoFinalidade");
                            Relatorio.CriarObjCelulaInColuna("Query", "TipoFinalidade", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("TipoOperação", "TipoOperação", true, "TipoOperação");
                            Relatorio.CriarObjCelulaInColuna("Query", "TipoOperação", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Natureza", "Natureza", true, "Natureza");
                            Relatorio.CriarObjCelulaInColuna("Query", "Natureza", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Referencia", "Referencia", true, "Referencia");
                            Relatorio.CriarObjCelulaInColuna("Query", "Referencia", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Pedido", "Pedido", true, "Pedido");
                            Relatorio.CriarObjCelulaInColuna("Query", "Pedido", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("EstadoPedido", "EstadoPedido", true, "EstadoPedido");
                            Relatorio.CriarObjCelulaInColuna("Query", "EstadoPedido", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Transacao", "Transacao", true, "Transacao");
                            Relatorio.CriarObjCelulaInColuna("Query", "Transacao", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("TemXML", "TemXML", true, "TemXML");
                            Relatorio.CriarObjCelulaInColuna("Query", "TemXML", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Itens", "Itens", true, "Itens");
                            Relatorio.CriarObjCelulaInColuna("Query", "Itens", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ValorTotal", "ValorTotal", true, "ValorTotal");
                            Relatorio.CriarObjCelulaInColuna("Query", "ValorTotal", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Manifesto", "Manifesto", true, "Manifesto");
                            Relatorio.CriarObjCelulaInColuna("Query", "Manifesto", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Motivo", "Motivo", true, "Motivo");
                            Relatorio.CriarObjCelulaInColuna("Query", "Motivo", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("dtEmissao", "dtEmissao", true, "dtEmissao");
                            Relatorio.CriarObjCelulaInColuna("Query", "dtEmissao", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("dtManifesto", "dtManifesto", true, "dtManifesto");
                            Relatorio.CriarObjCelulaInColuna("Query", "dtManifesto", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("UsuarioID", "UsuarioID", true, "UsuarioID");
                            Relatorio.CriarObjCelulaInColuna("Query", "UsuarioID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MensagemSefaz", "MensagemSefaz", true, "MensagemSefaz");
                            Relatorio.CriarObjCelulaInColuna("Query", "MensagemSefaz", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ChaveAcesso", "ChaveAcesso", true, "ChaveAcesso");
                            Relatorio.CriarObjCelulaInColuna("Query", "ChaveAcesso", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Observacoes", "Observacoes", true, "Observacoes");
                            Relatorio.CriarObjCelulaInColuna("Query", "Observacoes", "DetailGroup");

                        Relatorio.TabelaEnd();
                    }
                    Relatorio.CriarPDF_Excel(Title, Formato);
                }
            }

            //Se Lista btnListar
            else if (lista_excel == 0)
            {
                //Cria DataSet com retorno da procedure
                DataSet dsLista = DataInterfaceObj.getRemoteData(strSQL);

                //Escreve XML com o DataSet
                WriteResultXML(dsLista);
            }
        }
    }
}