<!--#include file="upload.asp"-->

<!-- %@ LANGUAGE=VBSCRIPT EnableSessionState=False % -->

<%
    'Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
	Dim pagesURLRoot
	Dim sRootPath
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

	pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    sRootPath = objSvrCfg.RootLogicAppPath(Application("appName"))
    
    Set objSvrCfg = Nothing
    
    On Error Resume Next
%>

<%
	Dim PASTA_XML_REMESSA
	Dim PASTA_XML_RETORNO

	PASTA_XML_REMESSA = 24039
	PASTA_XML_RETORNO = 24040


	Dim MyUploader
	Set MyUploader = New FileUploader
	Dim sError, rsCommand, nRecsAffected, strSQL
	
	' Pega o conte�do do arquivo.
	MyUploader.Upload()
	
	' Grava o arquivo no disco.
	Dim File
	For Each File In MyUploader.Files.Items
		File.SaveToDisk sRootPath & "/UploadedFiles"
	Next

	' Recupera os par�metros da nota fiscal e do pasta
	Dim notaFiscalID
	Dim nPastaXml
	
	For i = 1 To Request.QueryString("notaFiscalID").Count    
		notaFiscalID = Request.QueryString("notaFiscalID")(i)
	Next

	For i = 1 To Request.QueryString("nPastaXml").Count    
		nPastaXml = Request.QueryString("nPastaXml")(i)
	Next

	' Atualiza a tabela do banco de dados.
    Set rsCommand = Server.CreateObject("ADODB.Command")

    rsCommand.CommandTimeout = 60 * 10
    rsCommand.ActiveConnection = strConn
    rsCommand.CommandType = adCmdText

	For Each File In MyUploader.Files.Items
		If (CInt(nPastaXml) = PASTA_XML_REMESSA) Then
			strSQL = "UPDATE NotasFiscais " & _
						"SET XMLNFeRemessa='" & BufferContent(File.FileData) & "' " & _
						"WHERE NotaFiscalID = " & Cstr(notaFiscalID)
		ElseIf (CInt(nPastaXml) = PASTA_XML_RETORNO) Then
			strSQL = "UPDATE NotasFiscais " & _
						"SET XMLNFe='" & BufferContent(File.FileData) & "' " & _
						"WHERE NotaFiscalID = " & Cstr(notaFiscalID)
		End If
		nRecsAffected = 0

		rsCommand.CommandText = strSQL
		rsCommand.Execute nRecsAffected

		' Trata o erro ou o sucesso.
		sError = ""
		For Each rsERROR In rsCommand.ActiveConnection.Errors
			sError = rsERROR.Description
			Exit For
		Next

		Set rsCommand = Nothing
	Next
%>

<html> 
	<head>
		<% 
			Response.Write "<script language='javascript'>" & "var glb_notaFiscalID = " & Chr(39) & CStr(notaFiscalID) & Chr(39) & ";" & "var glb_nPastaXml = " & Chr(39) & CStr(nPastaXml) & Chr(39) & ";" & "</script>"
		%>
		
		<script type="text/jscript" language="javascript">
			function recarregar() {
				window.location.replace(window.top.SYS_PAGESURLROOT + "/modcomercial/submodpedidos/notasfiscais/modalpages/NotaUpload.asp?notaFiscalID=" + glb_notaFiscalID + "&nPastaXml=" + glb_nPastaXml);
			}
		</script>
	</head>
	
	<body>
		<%If(sError = "") Then%>

		<br/>
		Arquivo enviado com sucesso.<br/>

		<%End If%>

		<%If(sError <> "") Then%>

		Erro ao enviar o arquivo.<br/> 
		<% Response.Write sError %><br/>
		<br/>
		<input type="button" id="btnRecarrega" name="btnRecarrega" value="Enviar Arquivo" language="javascript" onclick="recarregar();"/>

		<%End If%>

		<%
		RS.Close  
		%>
	</body> 
</html>