using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.notasfiscais.serverside
{
    public partial class impostosdata : System.Web.UI.OverflyPage
    {
        private Integer notaFiscalID;
        private Integer modo;
        private Integer impostoID;
        private string impostosql;

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(
                DataInterfaceObj.getRemoteData(GetSQL)
            );
        }

        public Integer nNotaFiscalID
        {
            get { return notaFiscalID; }
            set { notaFiscalID = value != null ? value : new Integer(0); }
        }
        protected Integer nModo
        {
            set { modo = value != null ? value : new Integer(0); }
        }
        protected Integer nImpostoID
        {
            set { impostoID = value != null ? value : new Integer(-1); }
        }

        protected string GetSQL
        {
            get
            {
                string sql = "";

                if (impostoID.intValue() >= 0)
                {
                    impostosql = " c.ImpostoID =" + impostoID + " AND  ";
                }
                else
                    impostosql = "";

                if (modo.intValue() == 0)
                {


                    sql = "SELECT " +
                                "' ' AS Ordem, " +
                                "' ' AS ProdutoID, " +
                                "'Impostos da Nota' AS Conceito, " +
                                "' ' AS NCM, " +
                                "' ' AS CodigoTributacao, " +
                                "c.ImpostoID, " +
                                "d.Imposto, " +
                                "CONVERT(NUMERIC(5,2), (dbo.fn_DivideZero(c.ValorImposto,c.BaseCalculo)*100)) AS Aliquota, " +
                                "c.ValorImposto, " +
                                "c.BaseCalculo, " +
                                "1 AS TipoImposto, " +
                                "~d.AlteraAliquota AS AlteraAliquota, " +
                                "0 AS OK " +
                            "FROM " +
                                "NotasFiscais_Impostos c WITH(NOLOCK), " +
                                "Conceitos d WITH(NOLOCK) " +
                            "WHERE " +
                                "c.NotaFiscalID=" + (notaFiscalID) + " AND " +
                                impostosql +
                                "c.ImpostoID=d.ConceitoID " +
                            "UNION ALL " +
                            "SELECT " +
                                "STR(dbo.fn_NotaFiscalItem_Ordem(a.NFItemID), 3) AS Ordem, " +
                                "STR(a.ProdutoID, 7) AS ProdutoID, " +
                                "b.Conceito, " +
                                "Classificacao.Conceito AS NCM, " +
                                "c.CodigoTributacao, " +
                                "c.ImpostoID, " +
                                "d.Imposto, " +
                                "c.Aliquota, " +
                                "c.ValorImposto, " +
                                "c.BaseCalculo, " +
                                "2 AS TipoImposto, " +
                                "0 AS AlteraAliquota, " +
                                "0 AS OK " +
                            "FROM " +
                                "NotasFiscais_Itens a WITH(NOLOCK) " +
                                    "INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ProdutoID=b.ConceitoID) " +
                                    "INNER JOIN NotasFiscais WITH(NOLOCK) ON (a.NotaFiscalID = NotasFiscais.NotaFiscalID) " +
                                    "INNER JOIN NotasFiscais_Itens_Impostos c WITH(NOLOCK) ON (a.NFItemID=c.NFItemID) " +
                                    "INNER JOIN Conceitos d WITH(NOLOCK) ON (c.ImpostoID=d.ConceitoID ) " +
                                    "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (ProdutosEmpresa.SujeitoID = NotasFiscais.EmpresaID AND ProdutosEmpresa.ObjetoID = a.ProdutoID) " +
                                    "LEFT JOIN Conceitos Classificacao WITH(NOLOCK) ON (ProdutosEmpresa.ClassificacaoFiscalID = Classificacao.ConceitoID ) " +
                            "WHERE " +
                                "a.NotaFiscalID=" + (notaFiscalID) + " AND " +
                                impostosql +
                                "ProdutosEmpresa.TipoRelacaoID = 61 " +
                            "ORDER BY " +
                                "TipoImposto, Ordem, ProdutoID, c.ImpostoID";
                }
                else if (modo.intValue() == 1)
                {

                    sql = "SELECT " +
                    "STR(dbo.fn_NotaFiscalItem_Ordem(a.NFItemID), 3) AS Ordem, " +
                    "STR(a.ProdutoID, 7) AS ProdutoID, " +
                    "b.Conceito, " +
                    "Classificacao.Conceito AS NCM, " +
                    "c.ImpostoID, " +
                    "d.Imposto, " +
                    "0 AS Aliquota, " +
                    "c.ValorImposto, " +
                    "c.BaseCalculo, " +
                    "2 AS TipoImposto, " +
                    "a.CFOPID, " +
                    "CFOP.AlteraImpostosComplementares AS CFOPAIC, " +
                    "Impostos.AlteraImpostosComplementares AS ImpostoAIC, " +
                    "0 AS OK " +
                "FROM " +
                    "NotasFiscais_Itens a WITH(NOLOCK) " +
                        "INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ProdutoID=b.ConceitoID) " +
                        "INNER JOIN NotasFiscais WITH(NOLOCK) ON (a.NotaFiscalID = NotasFiscais.NotaFiscalID) " +
                        "INNER JOIN NotasFiscais_Itens_ImpostosComplementares c WITH(NOLOCK) ON (a.NFItemID=c.NFItemID) " +
                        "INNER JOIN Conceitos d WITH(NOLOCK) ON (c.ImpostoID=d.ConceitoID) " +
                        "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON (ProdutosEmpresa.SujeitoID = NotasFiscais.EmpresaID AND ProdutosEmpresa.ObjetoID = a.ProdutoID) " +
                        "INNER JOIN Conceitos Classificacao WITH(NOLOCK) ON (ProdutosEmpresa.ClassificacaoFiscalID = Classificacao.ConceitoID) " +
                        "INNER JOIN Operacoes CFOP WITH(NOLOCK) ON (a.CFOPID = CFOP.OperacaoID) " +
                        "INNER JOIN Conceitos Impostos WITH(NOLOCK) ON (c.ImpostoID = Impostos.ConceitoID) " +
                "WHERE " +
                    "a.NotaFiscalID=" + notaFiscalID + " AND " +
                    impostosql +
                    "ProdutosEmpresa.TipoRelacaoID = 61 " +
                "ORDER BY " +
                    "TipoImposto, Ordem, ProdutoID, c.ImpostoID";
                }
                return sql;
            }
        }
    }
}
