﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Reflection;
using System.Xml;
using Overfly3.Attributes;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.notasfiscais.serverside
{
    public partial class Manifestar : System.Web.UI.OverflyPage
    {

        private string erros = "";
        private Integer usuarioID;

        public Integer UsuarioID
        {
            get { return usuarioID; }
            set { usuarioID = value; }
        }
        private Integer[] manifestoID;

        public Integer[] ManifestoID
        {
            get { return manifestoID; }
            set { manifestoID = value; }
        }
        private Integer[] estadoID;


        public Integer[] EstadoID
        {
            get { return estadoID; }
            set { estadoID = value; }
        }
        
        private string[] mensagem;

        public string[] Mensagem
        {
            get { return mensagem; }
            set { mensagem = value; }
        }

        private string[] observacoes;
        
        public string[] Observacoes
        {
            get { return observacoes; }
            set { observacoes = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            Manifesta();
            WriteResultXML(DataInterfaceObj.getRemoteData(
                    "select '" + erros + "' as fldResponse"));
        }

        // Roda a procedure sp_ManifestoNFe_Exporta
        private void Manifesta()       
        {
            int datalen = manifestoID.Length;
            
            for (int i = 0; i < datalen ; i++)
            {
                try
                {
                    ProcedureParameters[] procParams = new ProcedureParameters[5];
                    procParams[0] = new ProcedureParameters(
                        "@ManifestoID",
                        System.Data.SqlDbType.Int,
                        manifestoID[i] != null ? manifestoID[i] : new Integer(0));

                    procParams[1] = new ProcedureParameters(
                        "@EstadoID",
                        System.Data.SqlDbType.Int,
                        estadoID[i] != null ? estadoID[i] : new Integer(0));

                    procParams[2] = new ProcedureParameters(
                        "@UsuarioID",
                        System.Data.SqlDbType.Int,
                        usuarioID != null ? usuarioID : new Integer(0));


                    procParams[3] = new ProcedureParameters(
                        "@Mensagem",
                        System.Data.SqlDbType.VarChar,
                        mensagem[i] != null ? mensagem[i].ToString() : "");

                    procParams[4] = new ProcedureParameters(
                        "@Observacoes",
                        System.Data.SqlDbType.VarChar,
                        observacoes[i] != null ? observacoes[i].ToString() : "");

                    DataInterfaceObj.execNonQueryProcedure(
                        "sp_ManifestoNFe_Exporta",
                        procParams);

                }
                catch (System.Exception exception)
                {

                    //erros += "#ERROR#OverflyPage (" + this.GetType().Name + "):" + exception.Message + " \r\n ";
                    erros += exception.Message + " " + manifestoID[i] + " \r\n ";
                }
            }
        }
    }
}