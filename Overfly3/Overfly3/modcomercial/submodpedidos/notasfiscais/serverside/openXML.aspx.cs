﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.notasfiscais.serverside
{
    public partial class openXML : System.Web.UI.OverflyPage
    {

        private int response;
        private string mensagem;

        private Integer ManifestoID;

        public Integer nManifestoID
        {
            get { return ManifestoID; }
            set { ManifestoID = value; }
        }
    
        private void OpenXML()
        {

            ProcedureParameters[] procParams = new ProcedureParameters[3];
            procParams[0] = new ProcedureParameters(
                "@ManifestoID",
                System.Data.SqlDbType.Int,
                ManifestoID);

            procParams[1] = new ProcedureParameters(
                "@Resultado",
                SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);

            procParams[2] = new ProcedureParameters(
                "@Mensagem",
                SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[2].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_ManifestoNFe_CarregaXML",
                procParams);

            // Obtém o resultado da execução.
            response = int.Parse(procParams[1].Data.ToString());
            mensagem = procParams[2].Data.ToString();
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            OpenXML();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select " + response + " as Resultado, '" +
                        mensagem + "' as Mensagem"
                )
            );
        }
    }
}