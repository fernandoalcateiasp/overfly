/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// FINAL DE VARIAVEIS GLOBAIS ***************************************
var glb_sLastBtnClicked = '';

var dsoSup01 = new CDatatransport('dsoSup01');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoInvoiceEMail = new CDatatransport('dsoInvoiceEMail');
/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()


FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selDocumentoFiscal', '1']]);

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modcomercial/submodpedidos/notasfiscais/inferior.asp',
                              SYS_PAGESURLROOT + '/modcomercial/submodpedidos/notasfiscais/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'NotaFiscalID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);
    
    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID','txtEstadoID',2,1],
                          ['lblNotaFiscal','txtNotaFiscal',10,1],
                          ['lbldtNotaFiscal','txtdtNotaFiscal',10,1],
                          ['lbldtEntradaSaida','txtdtEntradaSaida',10,1],
                          ['lblStatus','txtStatus',3,1],
                          ['lblNatureza','txtNatureza',31,1, -8],
                          ['lblCFOP', 'txtCFOP', 11, 1, -1],
                          ['lblNumeroRPS', 'txtNumeroRPS', 10, 1, -1],
                          ['lblPedido','txtPedido',10,2],
                          ['lblEmissor','chkEmissor',3,2],
                          ['lblDocumentoFiscal', 'selDocumentoFiscal', 8, 2],
                          ['lblSerieNF', 'txtSerieNF', 6, 2],
                          ['lblVendedor','txtVendedor',20,2,-5],
                          ['lblChaveAcesso','txtChaveAcesso',41,2,0],
                          ['lblValorFrete','txtValorFrete',14,3],
                          ['lblValorSeguro','txtValorSeguro',14,3],
                          ['lblValorDesconto','txtValorDesconto',14,3],
                          ['lblOutrasDespesas','txtOutrasDespesas',14,3],
                          ['lblValorTotalServicos','txtValorTotalServicos',14,4],
                          ['lblValorTotalProdutos','txtValorTotalProdutos',14,4],   
                          ['lblValorTotalProdutosServicoes','txtValorTotalProdutosServicoes',14,4],
                          ['lblValorTotalNota','txtValorTotalNota',14,4],
                          ['lblSeuPedido', 'txtSeuPedido', 20, 4],
                          ['lblSeuPedido', 'txtSeuPedido', 20, 4],
                          ['lblCodigoServico', 'txtCodigoServico', 14, 5, 0, -5],
                          ['lblDescricaoServico', 'txtDescricaoServico', 82, 5]], null, null, true);

    txtSerieNF.maxLength = 5;
    lblSeuPedido.style.visibility = 'hidden';                          
    txtSeuPedido.style.visibility = 'hidden';
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWNOTAFISCAL')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
            
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        window.top.focus();
        
        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
        
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.

    // trava as barras do sup para form que so permite alteracao
    glb_BtnsIncAltEstExcl[0] = true;
    // glb_BtnsIncAltEstExcl[2] = true;
    // glb_BtnsIncAltEstExcl[3] = true;

    lbldtEntradaSaida.style.color = 'blue';
    
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    //@@
    glb_sLastBtnClicked = btnClicked;

    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR'))
        showFields();

    /*if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR') )
    fillLocalCmbs();
    else
    {    
    // Mover esta funcao para os finais de retornos de operacoes no
    // banco de dados
    finalOfSupCascade(btnClicked);
    }
    */
    finalOfSupCascade(btnClicked);
}

function showFields()
{
    var sShowServico = (((dsoSup01.recordset['DocumentoFiscalID'].value == 1123) || (dsoSup01.recordset['DocumentoFiscalID'].value == 1124)) ? 'inherit' : 'hidden');

    lblCodigoServico.style.visibility = sShowServico;
    txtCodigoServico.style.visibility = sShowServico;
    lblDescricaoServico.style.visibility = sShowServico;
    txtDescricaoServico.style.visibility = sShowServico;
    lblNumeroRPS.style.visibility = sShowServico;
    txtNumeroRPS.style.visibility = sShowServico;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;

    if (cmbID == 'selDocumentoFiscal')
        setLabelDocumentoFiscal(selDocumentoFiscal.value);	
        
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    //@@ padrao do frame work
    return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    if ( controlBar == 'SUP' )
    {
        var empresa = getCurrEmpresaData();

        // Documentos
        if ((controlBar == 'SUP') && (btnClicked == 1)) {
            __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
        }
        
        // usuario clicou botao imprimir
        else if ( btnClicked == 2 )
            openModalPrint();                

        // Detalha Pedido
        else if ( btnClicked == 4 )
        {
			// Comentado temporariamente para teste do e-mail de invoice
            sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], dsoSup01.recordset['PedidoID'].value));
			// invoiceMail();
		}
        // Modal NFe
        else if ( btnClicked == 5)
        {
            openModalViewXML();
		}
    }
}

function invoiceMail()
{
	var aEmpresa = getCurrEmpresaData();
    var strPars = new String();
    strPars = '';

    strPars += '?nInvoiceID=' + escape(dsoSup01.recordset['NotaFiscalID'].value);
    strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
    strPars += '&nIdiomaSistema=' + escape(aEmpresa[7]);
    strPars += '&nIdiomaEmpresa=' + escape(aEmpresa[8]);
    
    dsoInvoiceEMail.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/notasfiscais/modalpages/emailinvoice.asp' + strPars;
    dsoInvoiceEMail.ondatasetcomplete = invoiceMail_DSC;
    dsoInvoiceEMail.Refresh();
}

function invoiceMail_DSC()
{
	alert('voltou');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Modal de impressao
    if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }    
    }
    // Modal view XML
    else if ( idElement.toUpperCase() == 'MODALVIEWXMLHTML' )    
    {
        if ( param1 == 'OK' )
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // campos read-only
    txtCFOP.disabled = true;
    txtNatureza.disabled = true;
    txtPedido.disabled = true;
    txtVendedor.disabled = true;
    txtChaveAcesso.disabled = true;
    txtValorFrete.disabled = true;
    txtValorSeguro.disabled = true;
    txtValorDesconto.disabled = true;
    txtOutrasDespesas.disabled = true;
    txtValorTotalServicos.disabled = true;
    txtValorTotalProdutos.disabled = true;
    txtValorTotalProdutosServicoes.disabled = true;
    txtValorTotalNota.disabled = true;
    txtSerieNF.disabled = true;
    txtCodigoServico.disabled = true;
    txtDescricaoServico.disabled = true;    
    chkEmissor.disabled = true;
    selDocumentoFiscal.disabled = true;
    
    
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    // @@
    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimentos', 'Detalhar Pedido', 'Resumo', 'Manifesto']);
}
/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    var nNotaFiscalID = dsoSup01.recordset['NotaFiscalID'].value;
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       
    strPars += '&nNotaFiscalID=' + escape(nNotaFiscalID);       

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/submodpedidos/notasfiscais/modalpages/modalprint.asp' + strPars;
    
    showModalWin(htmlPath, new Array(346,200));
}

/*function fillLocalCmbs()
{
    // parametrizacao do dso dsoCmbDynamic01 (designado para selSujeitoID)
    setConnection(dsoCmbDynamic01);

    dsoCmbDynamic01.SQL = 'SELECT a.Impressora FROM RelacoesPesRec_DocumentosFiscais a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK) ' +
                          'WHERE a.RelacaoID=b.RelacaoID AND b.SujeitoID=' + 
                          dsoSup01.recordset['EmpresaID'].value + ' AND b.ObjetoID = 999 AND b.TipoRelacaoID=12 ' +
                          'ORDER BY a.Impressora';
    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();
}

function dsoCmbDynamic_DSC()
{
    clearComboEx(['selImpressora']);

    if ( ((dsoCmbDynamic01.recordset.BOF)&&(dsoCmbDynamic01.recordsetEOF)) )
    {
        // Volta p/ automacao
        finalOfSupCascade(glb_sLastBtnClicked);
        return null;
    }    

    var oldDataSrc = selImpressora.dataSrc;
    var oldDataFld = selImpressora.dataFld;
    var i = 0;
    
    selImpressora.dataSrc = '';
    selImpressora.dataFld = '';
    
    while (! (dsoCmbDynamic01.recordset.EOF) )
    {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoCmbDynamic01.recordset['Impressora'].value;
        oOption.value = dsoCmbDynamic01.recordset['Impressora'].value;
        selImpressora.add(oOption);
        dsoCmbDynamic01.recordset.MoveNext();
    }
    selImpressora.dataSrc = oldDataSrc;
    selImpressora.dataFld = oldDataFld;
    
    // Volta p/ automacao
    finalOfSupCascade(glb_sLastBtnClicked);
}
*/

/*********************************************
Chama modal Visualizar XML
*********************************************/
function openModalViewXML()
{
	var htmlPath;
	var notaFiscalID;
	var nPastaXml;
	
	var aEmpresa = getCurrEmpresaData();
    //var nNotaFiscal = dsoSup01.recordset['NotaFiscal'].value;
    //var nEmpresaID = aEmpresa[0];
	

    //htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/notasfiscais/modalpages/modalvisualizarnfe.asp';
	htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/notasfiscais/modalpages/modalviewnfe.asp';

	// Veio do sup ou do inf?
	var htmlId = (getHtmlId()).toUpperCase();

    //notaFiscalID = dsoSup01.recordset['NotaFiscalID'].value;
    
    // Passar o id do label do combo que vai preencher
    // para configurar a janela
    // e o id do combo que vai preencher, que volta com a mensagem
    // para saber o combo a preencher
    
    //?nEmpresaID=' +nEmpresaID +  '&nNotaFiscal=' + nNotaFiscal ;
    
    var strPars = new String();
    strPars = '?sCaller=' + escape('S');
    
    showModalWin( (htmlPath + strPars) , new Array(1000,590) );

	return null;
}

function setLabelDocumentoFiscal(nDocumentoFiscalID) {
    if (nDocumentoFiscalID == null)
        nDocumentoFiscalID = '';

    var sLabelDocumentoFiscal = 'Documento ' + nDocumentoFiscalID;
    lblDocumentoFiscal.style.width = sLabelDocumentoFiscal.length * FONT_WIDTH;
    lblDocumentoFiscal.innerText = sLabelDocumentoFiscal;

}