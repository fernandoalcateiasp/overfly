﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.operacoes.serverside
{
    public partial class fillcmboperacaomae : System.Web.UI.OverflyPage
    {


        private string Grupo;

        public string sGrupo
        {
            get { return Grupo; }
            set { Grupo = value; }
        }
        private Integer Nivel;

        public Integer nNivel
        {
            get { return Nivel; }
            set { Nivel = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string sql = "SELECT OperacaoID AS fldID, OperacaoID AS fldName " +
                            "FROM Operacoes WITH(NOLOCK) ";

            if (Grupo == "CFOP")
            {
                sql +=
                    " WHERE EstadoID=2 AND TipoOperacaoID = 651";
            }
            else if (Grupo == "TRANSACOES")
            {
                sql += " WHERE EstadoID=2 AND TipoOperacaoID = 652 ";

            }

            sql += "  AND Nivel = " + Nivel.ToString() + " " + " Order BY fldName";

            WriteResultXML(
                     DataInterfaceObj.getRemoteData(sql)
                 );
        }
    }
}