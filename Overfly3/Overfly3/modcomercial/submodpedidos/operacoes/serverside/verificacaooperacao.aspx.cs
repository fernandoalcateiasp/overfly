﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.operacoes.serverside
{
    public partial class verificacaooperacao : System.Web.UI.OverflyPage
    {

        private int Resultado;
        private string Mensagem;

        private Integer OperacaoID;

        public Integer nOperacaoID
        {
            get { return OperacaoID; }
            set { OperacaoID = value; }
        }
        private Integer EstadoDeID;

        public Integer nEstadoDeID
        {
            get { return EstadoDeID; }
            set { EstadoDeID = value; }
        }
        private Integer EstadoParaID;

        public Integer nEstadoParaID
        {
            get { return EstadoParaID; }
            set { EstadoParaID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }

        public void VerificacaoOperacao() {

            ProcedureParameters[] procparam = new ProcedureParameters[6];

            procparam[0] = new ProcedureParameters(
                "@OperacaoID",
                SqlDbType.Int,
                OperacaoID.ToString());
            procparam[1] = new ProcedureParameters(
                "@EstadoDeID",
                SqlDbType.Int,
                EstadoDeID.ToString());
            procparam[2] = new ProcedureParameters(
                "@EstadoParaID",
                SqlDbType.Int,
                EstadoParaID.ToString());
            procparam[3] = new ProcedureParameters(
                "@UsuarioID",
                SqlDbType.Int,
                UsuarioID.ToString());
            procparam[4] = new ProcedureParameters(
                "@Resultado",
                SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procparam[5] = new ProcedureParameters(
                "@Mensagem",
                SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procparam[5].Length = 8000;

            // Executa a procedure.
            DataInterfaceObj.execNonQueryProcedure(
                "sp_Operacao_Verifica",
                procparam
            );

            // Obtém o resultado da execução.
            Resultado = int.Parse(procparam[4].Data.ToString());
            Mensagem = procparam[5].Data.ToString();        
        }       
        protected override void PageLoad(object sender, EventArgs e)
        {
            VerificacaoOperacao();
            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select " + Resultado.ToString() + " as Resultado, '" +
                        Mensagem.ToString() + "' as Mensagem"
                )
            );
        }
    }
}