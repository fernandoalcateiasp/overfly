﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.submodpedidos.operacoes.serverside
{
    public partial class auditoriaCST : System.Web.UI.OverflyPage
    {
        private Integer OperacaoID;

        public Integer nOperacaoID
        {
            get { return OperacaoID; }
            set { OperacaoID = value; }
        }

        private DataSet CST_Valida() 
        {
            ProcedureParameters[] procParams = new ProcedureParameters[1];
            procParams[0] = new ProcedureParameters(
                "@OperacaoID",
                System.Data.SqlDbType.Int,
                OperacaoID.intValue() == 0 ? DBNull.Value : (Object)OperacaoID.ToString());

            return DataInterfaceObj.execQueryProcedure(
                "sp_CST_Valida",
                procParams);
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(CST_Valida());
        }
    }
}