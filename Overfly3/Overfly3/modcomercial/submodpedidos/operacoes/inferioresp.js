/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de localidades
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
fillCmbPastasInCtlBarInf(nTipoRegistroID)    
criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID) {
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    var currCmb2Data;
    var aValue;

    //@@
    if ((folderID == 20008) || (folderID == 20009)) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.OperacaoID, a.ProprietarioID, a.AlternativoID, a.Observacoes ' +
                  'FROM Operacoes a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.OperacaoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.RegistroID = ' + idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24104) // Opera��es Filhas
    {
        dso.SQL = 'SELECT a.OperacaoID,b.RecursoAbreviado, a.Operacao ' +
                  'FROM Operacoes a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.EstadoID = b.RecursoID ' +
                  'AND a.OperacaoMaeID = ' + idToFind + ' ' +
                  'ORDER BY a.OperacaoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24105) // Impostos
    {
        dso.SQL = 'SELECT * FROM Operacoes_Impostos WITH(NOLOCK) ' +
                  'WHERE OperacaoID = ' + idToFind + ' ' +
                  'ORDER BY ImpostoID, TipoIncidenciaID ';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24112) // CST
    {
        dso.SQL = 'SELECT * FROM Operacoes_CST WITH(NOLOCK) ' +
                  'WHERE OperacaoID = ' + idToFind + ' ' +
                  'ORDER BY ImpostoID ';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24106) // CFOPs
    {
        dso.SQL = 'SELECT a.OperacaoID,b.RecursoAbreviado, a.Operacao ' +
                  'FROM Operacoes a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.EstadoID = b.RecursoID ' +
                  'AND a.TipoOperacaoID=651 AND a.TransacaoBaixaID = ' + idToFind + ' ' +
                  'ORDER BY a.OperacaoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24107) // Transa��es
    {
        dso.SQL = 'SELECT a.OperacaoID,b.RecursoAbreviado, a.Operacao, a.Invertido ' +
                  'FROM Operacoes a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.EstadoID = b.RecursoID ' +
                  'AND a.TipoOperacaoID=652 AND a.TransacaoBaixaID = ' + idToFind + ' ' +
                  'ORDER BY a.OperacaoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24108) // Estoques
    {
        dso.SQL = 'SELECT * FROM Operacoes_Estoques WITH(NOLOCK) ' +
                  'WHERE OperacaoID = ' + idToFind + ' ' +
                  'ORDER BY OpeEstoqueID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24109) // Refer�ncias
    {
        dso.SQL = 'SELECT * FROM Operacoes_Referencias WITH(NOLOCK) ' +
                  'WHERE OperacaoID = ' + idToFind + ' ' +
                  'ORDER BY TipoReferenciaID, Ordem ';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24110) // Formas Pagamento
    {
        dso.SQL = 'SELECT * FROM Operacoes_FormasPagamento a WITH(NOLOCK) ' +
                  'WHERE a.OperacaoID = ' + idToFind + ' ' +
                  'ORDER BY a.EhDefault DESC, a.FormaPagamentoID ';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24111) { // Tipo de Custo
        dso.SQL = 'SELECT * ' +
                    'FROM Operacoes_Custo a WITH(NOLOCK) ' +
                    'WHERE a.OperacaoID = ' + idToFind + ' ' +
                    'ORDER BY a.TipoCustoID DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24118) // Opera��es Baixas
    {
        dso.SQL = ' SELECT * ' +
                  ' FROM    dbo.Operacoes_Baixas a WITH ( NOLOCK )' +
                  ' WHERE   a.OperacaoID = ' + idToFind + '';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 24119) // Opera��es Empresas
    {
        dso.SQL = ' SELECT * ' +
                  ' FROM    dbo.Operacoes_Empresas a WITH ( NOLOCK )' +
                  ' WHERE   a.OperacaoID = ' + idToFind + '' +
                  ' ORDER BY a.EmpresaID';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 24120) // Opera��es Conceitos
    {
        dso.SQL = 'SELECT * ' +
                  'FROM dbo.Operacoes_Conceitos a WITH ( NOLOCK ) ' +
                  'WHERE a.OperacaoID = ' + idToFind + ' ' +
                  'ORDER BY (SELECT b.Ordem FROM TiposAuxiliares_Itens b WITH(NOLOCK) WHERE b.ItemID=a.TipoConceitoID)';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 24121) // Opera��es Documentos Fiscais 
    {
        dso.SQL = 'SELECT * ' +
                  'FROM    dbo.Operacoes_DocumentosFiscais a WITH ( NOLOCK ) ' +
                  'WHERE   a.OperacaoID = ' + idToFind + ' ' +
                  'ORDER BY (SELECT b.Ordem FROM TiposAuxiliares_Itens b WITH(NOLOCK) WHERE b.ItemID = a.DocumentoFiscalID)';

        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID) {

    setConnection(dso);

    var aEmpresa = getCurrEmpresaData();
    // LOG
    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
        // Impostos
    else if (pastaID == 24105) {

        if (dso.id == 'dsoCmb01Grid_01') {
            var nPaisID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
			                          'dsoSup01.recordset[' + '\'' + 'PaisID' + '\'' + '].value');
            dso.SQL = 'SELECT ConceitoID, Imposto ' +
                      'FROM Conceitos WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoConceitoID=306 ' +
                      'AND PaisID= ' + nPaisID + ' ' +
                      'AND IncidePreco=1 ' +
                      'ORDER BY Imposto';
        }
        else if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=411 ' +
                      'ORDER BY Ordem';
        }
        else if (dso.id == 'dsoCmb01Grid_03') {
            dso.SQL = 'SELECT ItemID AS OrigemID, ItemAbreviado, ItemAbreviado + SPACE(1) + CHAR(45) + SPACE(1) + ItemMasculino AS Origem ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=114 ' +
                      'ORDER BY Ordem';
        }
        else if (dso.id == 'dsoCmb01Grid_04') {
            dso.SQL = 'SELECT 0 AS Indice, 0 AS UFID, SPACE(0) AS Localidade, SPACE(0) AS UFAbreviada ' +
						'UNION ' +
						'SELECT 1 AS Indice, LocalidadeID AS UFID, Localidade, CodigoLocalidade2 AS UFAbreviada ' +
		                'FROM Localidades WITH(NOLOCK) ' +
                        'WHERE (EstadoID=2 AND TipoLocalidadeID=204 AND LocalizacaoID=' + aEmpresa[1] + ') ' +
                        'ORDER BY Indice, Localidade';
        }
        else if (dso.id == 'dsoCmb01Grid_05') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoID = 431 ' +
                      'ORDER BY Ordem';
        }
    }
    //Codigo Tributacao
    else if (pastaID == 24112) {

        if (dso.id == 'dsoCmb01Grid_01') {
            var nPaisID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
			                          'dsoSup01.recordset[' + '\'' + 'PaisID' + '\'' + '].value');
            dso.SQL = 'SELECT ConceitoID, Imposto ' +
                      'FROM Conceitos WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoConceitoID=306 ' +
                      'AND PaisID= ' + nPaisID + ' ' +
                      'AND IncidePreco=1 ' +
                      'ORDER BY Imposto';
        }
        else if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT a.ItemID, (a.Observacao + \': \' + (CONVERT(VARCHAR, a.ItemAbreviado) + ' + '\' ' + '\'' + ' + CONVERT(VARCHAR(80), a.ItemMasculino))) AS ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                      'INNER JOIN TiposAuxiliares b WITH(NOLOCK) ON (a.TipoID = b.TipoID) ' +
                      'WHERE a.EstadoID=2 AND a.TipoID IN (4011)  ' +
                      'ORDER BY a.Ordem';
        }
    }
        // Estoques
    else if (pastaID == 24108) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID, ItemMasculino, ItemAbreviado ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=106 ' +
                      'ORDER BY Ordem';
        }
    }
        // Refer�ncias
    else if (pastaID == 24109) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                                  'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                                  'WHERE EstadoID=2 AND TipoID=412 ' +
                                  'ORDER BY Ordem';
        }
        else if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT NULL AS OperacaoID, SPACE(0) AS Operacao ' +
				                  'UNION ALL SELECT OperacaoID, Operacao ' +
                                  'FROM Operacoes WITH(NOLOCK) ' +
                                  'WHERE EstadoID=2 AND TipoOperacaoID=652 AND Nivel = 3 ' +
                                  'ORDER BY OperacaoID';
        }
        else if (dso.id == 'dsoCmb01Grid_03') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                                'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                                'WHERE TipoID = 430';
        }
    }
        // Formas Pagamentos
    else if (pastaID == 24110) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID, ItemAbreviado ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=804 ' +
                      'ORDER BY Ordem';
        }
    }
        // Tipo de Custo
    else if (pastaID == 24111) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoID = 117 ' +
                      'ORDER BY Ordem';
        }
    }
        //OperacoesBaixas
    else if (pastaID == 24118) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT  ItemID, ItemAbreviado ' +
                      ' FROM  dbo.TiposAuxiliares_Itens WITH(NOLOCK)' +
                      ' WHERE   TipoID = 106 AND Ordem < = 9';
        }
        else if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT  ItemID, ItemAbreviado ' +
                      ' FROM  dbo.TiposAuxiliares_Itens WITH(NOLOCK)' +
                      ' WHERE   TipoID = 106 AND Ordem < = 9 ';
        }
        else if (dso.id == 'dsoCmb01Grid_03') {
            dso.SQL = 'SELECT  ItemID, ItemAbreviado ' +
                      ' FROM  dbo.TiposAuxiliares_Itens WITH(NOLOCK)' +
                      ' WHERE   TipoID = 115';

        }
        else if (dso.id == 'dsoCmb01Grid_04') {
            dso.SQL = 'SELECT DISTINCT ISNULL(Baixa, 0)AS Baixa ' +
                      ' FROM dbo.Operacoes_Baixas WITH(NOLOCK) ' +
                      ' WHERE OperacaoID =' + nRegistroID +
                      ' UNION ALL' +
                      ' SELECT ISNULL((SELECT  MAX(Baixa)' +
                      ' FROM    dbo.Operacoes_Baixas WITH(NOLOCK) ' +
                      ' WHERE OperacaoID = ' + nRegistroID + ' ), 0) + 1 AS Baixa';

        }
    }
        // Empresas
    else if (pastaID == 24119) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT NULL AS PessoaID, SPACE(0) AS Fantasia ' +
				      'UNION ALL SELECT PessoaID, Fantasia ' +
                      'FROM Pessoas WITH(NOLOCK) ' +
                            'INNER JOIN RelacoesPesRec WITH(NOLOCK) ON (PessoaID = SujeitoID) ' +
                      'WHERE (Pessoas.EstadoID = 2) AND (ISNULL(EstaEmOperacao, 0) = 1) ' +
                             'AND (GETDATE() BETWEEN dtInicio AND dtFim) ' +
                      'ORDER BY PessoaID';

        }
    }

        // Tipos Conceitos
    else if (pastaID == 24120) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemMasculino, ItemID ' +
                        'FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) ' +
                        'WHERE (TipoID = 101 AND EstadoID=2 AND Aplicar=1) ' +
                        'ORDER BY Ordem ';

        }
        else if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT SPACE(0) AS ItemMasculino, 0 AS ItemID, 0 AS Ordem ' +
                      'UNION ALL ' +
                      'SELECT ItemMasculino, ItemID, Ordem ' +
                        'FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK)' +
                        'WHERE (TipoID = 102 AND EstadoID=2 AND Aplicar=1) ' +
                        'ORDER BY Ordem ';

        }
        else if (dso.id == 'dsoCmb01Grid_03') {
            dso.SQL = 'SELECT SPACE(0) AS Localidade, 0 AS LocalidadeID ' +
                      'UNION ALL ' +
                      'SELECT DISTINCT c.Localidade, c.LocalidadeID ' +
                        'FROM RelacoesPesRec a WITH(NOLOCK) ' +
                            'INNER JOIN Pessoas_Enderecos b WITH(NOLOCK) ON (a.SujeitoID = b.PessoaID) ' +
                            'INNER JOIN Localidades c WITH(NOLOCK) ON (b.PaisID = c.LocalidadeID) ' +
                        'WHERE ((a.TipoRelacaoID = 12 AND a.EstadoID = 2)) ' +
                        'ORDER BY Localidade ';

        }
    }
        // Documentos Fiscais
    else if (pastaID == 24121) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemMasculino,ItemID ' +
                      ' FROM  dbo.TiposAuxiliares_Itens WITH(NOLOCK)' +
                      ' WHERE (TipoID = 434 AND EstadoID = 2) ' +
                      ' ORDER BY Ordem ';
        }
        else if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT SPACE(0) AS Localidade, 0 AS LocalidadeID ' +
                  'UNION ALL ' +
                  'SELECT DISTINCT c.Localidade, c.LocalidadeID ' +
	                'FROM RelacoesPesRec a WITH(NOLOCK) ' +
                        'INNER JOIN Pessoas_Enderecos b WITH(NOLOCK) ON (a.SujeitoID = b.PessoaID) ' +
                        'INNER JOIN Localidades c WITH(NOLOCK) ON (b.PaisID = c.LocalidadeID) ' +
	                'WHERE ((a.TipoRelacaoID = 12 AND a.EstadoID = 2)) ' +
	                'ORDER BY Localidade ';

        }
    }


}
/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID) {
    var i = 0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
    var nNivel = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset[' + '\'' + 'Nivel' + '\'' + '].value');

    var aFolders = [[20008, 'true'],  /* Observacoes   */
                    [20009, 'true'],  /* Proprietarios */
                    [20010, 'true'],  /* LOG */
                    [24104, '( ((nTipoRegistroID == 651) || ' +
                              '(nTipoRegistroID == 652)) && (nNivel <= 2) )'],  /* Opercoes Filhas */
                    [24118, '(nTipoRegistroID == 652 && nNivel == 3)'],  /* Opercoes Baixas*/
                    [24105, '(nTipoRegistroID == 651 && nNivel == 3)'],  /* Impostos */
                    [24112, '(nTipoRegistroID == 651 && nNivel == 3)'],  /* CST */
                    [24110, '(nTipoRegistroID == 652 && nNivel == 3)'], /* Formas Pagamentos */
                    [24106, '(nTipoRegistroID == 652 && nNivel == 3)'],  /* CFOPs */
                    [24107, 'nTipoRegistroID == 653'],  /* Transa��es */
                    [24108, 'nTipoRegistroID == 653'],  /* Estoques */
                    [24109, '(nTipoRegistroID == 652 && nNivel == 3)'],
                    [24111, '(nTipoRegistroID == 652 && nNivel == 3)'], /* Tipo de Custo */
                    [24119, '(nTipoRegistroID == 652 && nNivel == 3)'], /* Empresas*/
                    [24120, '(nTipoRegistroID == 652 && nNivel == 3)'], /* Tipo de Conceito*/
                    [24121, '(nTipoRegistroID == 652 && nNivel == 3)']]; /* Documento Fiscal*/

    cleanupSelInControlBar('inf', 1);

    for (i = 0; i < aFolders.length; i++) {
        if (eval(aFolders[i][1])) {
            vPastaName = window.top.getPastaName(aFolders[i][0]);
            if (vPastaName != '')
                addOptionToSelInControlBar('inf', 1, vPastaName, aFolders[i][0]);
        }
    }

    // Nao mexer - Inicio de automacao ============================== 	
    // Tenta selecionar novamente a pasta anterior selecionada no combo
    // Se nao mais existe a pasta anteriormente selecionada no combo
    // seleciona a pasta default

    stripFoldersByRight(currSelection);

    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID) {
    //@@ return true se tudo ok, caso contrario return false;

    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 24105) // Impostos
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [0, 1], ['OperacaoID', registroID]);
        }
        else if (folderID == 24112) // CST
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [0,2], ['OperacaoID', registroID]);
        }
        else if (folderID == 24108) // Estoques
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [0], ['OperacaoID', registroID]);
        }
        else if (folderID == 24109) // Refer�ncias
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [0], ['OperacaoID', registroID]);
        }
        else if (folderID == 24110) // Formas Pagamentos
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [0], ['OperacaoID', registroID]);
        }
        else if (folderID == 24111) // Tipo de Custo
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [0], ['OperacaoID', registroID]);
        }
        else if (folderID == 24118) // Operacoes Baixas
        {
            if ((fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EstoqueSaida')) == '') && (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EstoqueEntrada')) == '')) {
                if (window.top.overflyGen.Alert('Preencha pelo menos um estoque.') == 0)
                    return null;

                currLine = -1;
            }
            else
                currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [], ['OperacaoID', registroID]);
        }
        else if (folderID == 24119) // Empresas
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [2, 3], ['OperacaoID', registroID]);
        }

        else if (folderID == 24120) // Tipos Conceitos
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [0], ['OperacaoID', registroID]);
        }
        else if (folderID == 24121) // Documentos Fiscais
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, fg.Row, [0], ['OperacaoID', registroID]);
        }

        if (currLine < 0)
            return false;
    }

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO) {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.FrozenCols = 0;

    if (folderID == 20010) // LOG
    {
        headerGrid(fg, ['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);

        fillGridMask(fg, currDSO, ['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['', '99/99/9999', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm:ss', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 24104) // Opera��es Filhas
    {
        headerGrid(fg, ['ID', 'Est', 'Opera��o'], []);
        fillGridMask(fg, currDSO, ['OperacaoID', 'RecursoAbreviado', 'Operacao']
                                    , ['', '', '']);
    }

    else if (folderID == 24105) // Impostos
    {
        headerGrid(fg, ['Imposto', 'Incid�ncia', 'Destaque', 'Localidade', 'Origem', 'Mensagem', 'OpeImpostoID'], [6]);
        fillGridMask(fg, currDSO, ['ImpostoID', 'TipoIncidenciaID', 'TipoDestaqueID', 'LocalidadeID', 'OrigemID', 'Mensagem', 'OpeImpostoID']
                               , ['', '', '', '', '', '', '']); //
        glb_aCelHint = [[0, 2, 'Local da nota fiscal onde o imposto deve ser destacado.']];
    }
    else if (folderID == 24112) // CST
    {
        headerGrid(fg, ['Imposto', 'Condi��o', 'CST', 'Exce��o', 'Observa��o', 'OperacaoID', 'OpeCodigoID'], [5, 6]);
        fillGridMask(fg, currDSO, ['ImpostoID', 'Condicao', 'CSTID', 'EhExcecao', 'Observacao', 'OperacaoID', 'OpeCodigoID']
                               , ['', '', '', '', '', '', '']);
        
        glb_aCelHint = [[0, 3, 'Possui exce��o de contabilizar com o CST diferente da Nota?']];
        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(0) = true;
        fg.MergeCol(1) = true;
        
    }
    else if (folderID == 24106) // CFOPs
    {
        headerGrid(fg, ['ID', 'Est', 'CFOP'], []);
        fillGridMask(fg, currDSO, ['OperacaoID', 'RecursoAbreviado', 'Operacao']
                                    , ['', '', '']);
    }
    else if (folderID == 24107)  // Transa��es
    {
        headerGrid(fg, ['ID', 'Est', 'Transa��o', 'Inv'], []);
        fillGridMask(fg, currDSO, ['OperacaoID', 'RecursoAbreviado', 'Operacao', 'Invertido']
                                    , ['', '', '', '']);
    }
    else if (folderID == 24108)  // Estoques
    {
        headerGrid(fg, ['Estoque',
                        'Subtrai',
                        'Dep�sito',
                        'Filial',
                        'OpeEstoqueID'], [4]);
        glb_aCelHint = [[0, 2, 'Grava o DepositoID ou EmpresaID do pedido?'],
                        [0, 3, 'Grava o PessoaID (Filial) ou EmpresaID do pedido?']];

        fillGridMask(fg, currDSO, ['EstoqueID',
                                   'Subtrai',
                                   'Deposito',
                                   'Filial',
                                   'OpeEstoqueID']
                                     , ['', '', '', '', '']);
    }
    else if (folderID == 24109)  // Refer�ncias
    {
        headerGrid(fg, ['Refer�ncia', 'Ordem', 'Saldo Exc', 'Valor Ref', 'Transa��o', 'OK', 'OpeReferenciaID'], [6]);
        glb_aCelHint = [[0, 2, 'Faz baixa financeira autom�tica mesmo com saldo excedente no retorno?'],
                        [0, 3, 'Condi��o do valor do retorno em rela��o ao valor da refer�ncia.'],
                        [0, 5, 'Tem que ser na mesma empresa?']];

        fillGridMask(fg, currDSO, ['TipoReferenciaID', 'Ordem', 'SaldoExcedente', 'CondicaoValorReferenciaID', 'TransacaoID', 'OK', 'OpeReferenciaID']
                                     , ['', '', '', '', '', '', '']);

        alignColsInGrid(fg, [1, 2]);
    }
    else if (folderID == 24110)  // Formas Pagamentos
    {
        headerGrid(fg, ['Forma', 'Default', 'OpeFormaPagamentoID'], [2]);

        glb_aCelHint = [[0, 1, '� a forma de pagamento default?']];

        fillGridMask(fg, currDSO, ['FormaPagamentoID', 'EhDefault', 'OpeFormaPagamentoID'],
                                     ['', '', '']);
    }

    else if (folderID == 24111)  // Tipo de Custo
    {
        headerGrid(fg, ['Custo', 'OpeCustoID'], [1]);

        fillGridMask(fg, currDSO, ['TipoCustoID', 'OpeCustoID'],
                                     ['', '']);
    }
    else if (folderID == 24118) // Opera��es baixas
    {
        headerGrid(fg, ['OperacaoID', 'Baixa', 'Pr�prio', 'Defeito', 'Lote', 'Saida', 'Entrada', 'Empresa', 'Dep�sito', 'OpeBaixaID'], [0, 9]);

        glb_aCelHint = [[0, 4, 'Considera o Lote para efetuar a baixa?'],
                        [0, 7, 'Utiliza EmpresaID (1) ou PessoaID (0) do Pedido na EmpresaID do RelPesCon_Movimentos'],
                        [0, 8, 'Utiliza DepositoID (1) ou PessoaID (0) do Pedido no DepositoID do RelPesCon_Movimentos']];

        fillGridMask(fg, currDSO, ['OperacaoID', 'Baixa', 'Proprio', 'Defeito', 'Lote', 'EstoqueSaida', 'EstoqueEntrada', 'Empresa', 'Deposito', 'OpeBaixaID'],
                                    ['', '', '', '', '', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(0) = true;
        fg.MergeCol(1) = true;

    }
    else if (folderID == 24119)  // Empresas
    {

        headerGrid(fg, ['OperacaoID', 'Empresa', 'Toler�ncia M�nima', 'Toler�ncia M�xima', 'Observa��o', 'OpeEmpresaID'], [0, 5]);
        glb_aCelHint = [[0, 2, 'Toler�ncia percentual em que o valor unit�rio pode variar a menor em rela��o ao valor sugerido.'],
                        [0, 3, 'Toler�ncia percentual em que o valor unit�rio pode variar a maior em rela��o ao valor sugerido.']];

        fillGridMask(fg, currDSO, ['OperacaoID', 'EmpresaID', 'ToleranciaMinimaValorUnitario', 'ToleranciaMaximaValorUnitario', 'Observacao', 'OpeEmpresaID']
                                     , ['', '', '#999.99', '999.99', '', ''],
                                       ['', '', '###,###,##0.00', '###,###,##0.00', '', '']);

    }

    else if (folderID == 24120)  // TiposConceitos
    {

        headerGrid(fg, ['Tipo Conceito', 'Classifica��o', 'Pa�s', 'OpeConceitoID'], [3]);
        fillGridMask(fg, currDSO, ['TipoConceitoID', 'ClassificacaoID', 'PaisID', 'OpeConceitoID'],
                                  ['', '', '', '']);
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }

    else if (folderID == 24121)  // Documentos Fiscais
    {

        headerGrid(fg, ['Tipo Documento', 'Pais', 'OpeDocumentoID'], [2]);
        fillGridMask(fg, currDSO, ['DocumentoFiscalID', 'PaisID', 'OpeDocumentoID'],
                                  ['', '', '']);

    }
}