/********************************************************************
operacoessup01.js

Library javascript para o pedidossup01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// controla o retorno do servidor dos dados de combos dinamicos
// (selSujeitoID e selObjetoID)
var glb_CounterCmbsDynamics = 0;
// guarda o botao clicado enviado pelo frameword
var glb_BtnFromFramWork;
// 
var glb_nTransacaoID = null;

var dsoSup01 = new CDatatransport('dsoSup01');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoCmbDynamic02 = new CDatatransport('dsoCmbDynamic02');
var dsoCmbDynamic03 = new CDatatransport('dsoCmbDynamic03');
var dsoCmbDynamic04 = new CDatatransport('dsoCmbDynamic04');
var dsoCmbDynamic05 = new CDatatransport('dsoCmbDynamic05');
var dsoCmbDynamic06 = new CDatatransport('dsoCmbDynamic06');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoCmbRecursoMae = new CDatatransport('dsoCmbRecursoMae');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoVerificacao = new CDatatransport('dsoVerificacao');
var dsoExcedente = new CDatatransport('dsoExcedente');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()


FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )

FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)    

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload() {
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);

    //@@ array bidimensional de combos ids e campo indice na tabela, �ndice da Automa��o.
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    //glb_aStaticCombos = ([['selTipoRegistroID', '1']]);
    //Altera��o SAS0075-Troca
    glb_aStaticCombos = ([['selTipoRegistroID', '1'],
                          ['selPaisID', '2'],
                          ['selPaisID2', '2'],
                          ['selAplicacaoID', '3'],
                          ['selMetodoCustoID', '4'],
                          ['selReferenciaCustoID', '5'],
                          ['selReferenciaImpostoID', '6'],
                          ['selReferenciaValorUnitarioID', '7'],
                          ['selHistoricoPadraoID', '8'],
                          ['selHistoricoPadraoValorExcedenteID', '8']]);

    windowOnLoad_1stPart();

    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modcomercial/submodpedidos/operacoes/inferior.asp',
                              SYS_PAGESURLROOT + '/modcomercial/submodpedidos/operacoes/pesquisa.asp');

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS

    linkDivsAndSubForms('divSup01_01', ['divSup02_01', 'divSup02_02', 'divSup02_03'], [651, 652, 653]);

    windowOnLoad_2ndPart();

    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'OperacaoID';

    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoRegistroID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoOperacaoID';

}

function setupPage() {
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup', [[1, 'divSup01_01'],
					  [2, 'divSup02_01'],
					  [2, 'divSup02_02'],
                      [2, 'divSup02_03']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
                          ['lblEstadoID', 'txtEstadoID', 2, 1],
                          ['lblOperacao', 'txtOperacao', 60, 1],
                          ['lblTipoRegistroID', 'selTipoRegistroID', 20, 1]]);

    //@@ *** CFOP - Div secundario superior divSup02_01 ***/
    adjustElementsInForm([['lblNivel_01', 'selNivel_01', 4, 2, -8],
                          ['lblOperacaoMaeID_01', 'selOperacaoMaeID_01', 8, 2, -8],
                          ['lblCodigo', 'txtCodigo', 6, 2, -8],
                          ['lblPaisID', 'selPaisID', 13, 2, -8],
                          ['lblTransacaoBaixaID_01', 'selTransacaoBaixaID_01', 6, 2, -8],
                          ['lblAplicacaoID', 'selAplicacaoID', 13, 2, -12],
                          ['lblCodigoTributacaoPIS', 'selCodigoTributacaoPIS', 5, 2, -8],
                          ['lblCodigoTributacaoCOFINS', 'selCodigoTributacaoCOFINS', 5, 2, -8],
                          ['lblCodigoTributacaoIPI', 'selCodigoTributacaoIPI', 5, 2, -8],
                          ['lblCodigoTributacaoICMS', 'selCodigoTributacaoICMS', 5, 2, -8],
                          ['lblFinalidade', 'txtFinalidade', 6, 2, -8],
                          ['lblContribuinte', 'selContribuinte', 6, 2, -16],
                          ['lblSuframa', 'selSuframa', 6, 2, -8],
                          ['lblST', 'selST', 6, 2, -8],
                          ['lblProducao', 'chkProducao', 3, 3, -3],
                          ['lblMicroObrigatorio', 'chkMicroObrigatorio', 3, 3, -3],
                          ['lblEnergia', 'chkEnergia', 3, 3],
                          ['lblComunicacao', 'chkComunicacao', 3, 3],
                          ['lblTransporte', 'chkTransporte', 3, 3],
                          ['lblIndustria', 'chkIndustria', 3, 3],
                          ['lblComercio', 'chkComercio', 3, 3],
                          ['lblServico', 'chkServico', 3, 3],
                          ['lblAlteraAliquota', 'chkAlteraAliquota', 3, 3],
                          ['lblAIC', 'chkAIC', 3, 3],
                          ['lblPermiteNF', 'selPermiteNF', 15, 4],
                          ['lblOrdem', 'txtOrdem', 4, 4],
                          ['lblObservacao_01', 'txtObservacao_01', 96, 4]],
                          ['lblHrCFOP', 'hrCFOP']);

    //@@ *** Transa��o - Div secundario superior divSup02_02 ***/
    //Altera��o SAS0075-Troca
    adjustElementsInForm([['lblNivel_02', 'selNivel_02', 5, 2],
                          ['lblOperacaoMaeID_02', 'selOperacaoMaeID_02', 10, 2],
                          ['lblPaisID2', 'selPaisID2', 16, 2],
                          ['lblMetodoCustoID', 'selMetodoCustoID', 8, 2],
                          ['lblReferenciaCustoID', 'selReferenciaCustoID', 20, 2],
                          ['lblReferenciaImpostoID', 'selReferenciaImpostoID', 20, 2],
                          ['lblReferenciaValorUnitarioID', 'selReferenciaValorUnitarioID', 30, 2],
                          ['lblNivelLiberacao', 'selNivelLiberacao', 5, 3],
                          ['lblOrdem2', 'txtOrdem2', 4, 3],
                          ['lblObservacao_02', 'txtObservacao_02', 50, 3],
                          ['lblHistoricoPadraoID', 'selHistoricoPadraoID', 25, 3],
                          ['lblHistoricoPadraoValorExcedenteID', 'selHistoricoPadraoValorExcedenteID', 25, 3],
                          ['lblEntrada', 'chkEntrada', 3, 4, -3],
                          ['lblFornecedor', 'chkFornecedor', 3, 4, -3],
                          ['lblCliente', 'chkCliente', 3, 4, -3],
                          ['lblEhRetorno', 'chkEhRetorno', 3, 4, -3],
                          ['lblResultado', 'chkResultado', 3, 4, -3],
                          ['lblInadimplencia', 'chkInadimplencia', 3, 4, -3],
                          ['lblRIR', 'chkRIR', 3, 4, -3],
                          ['lblAtualizaFinanceiro', 'chkAtualizaFinanceiro', 3, 4, -3],
                          ['lblBaixaFinanceiro', 'chkBaixaFinanceiro', 3, 4, -3],
                          ['lblCodigoBarra', 'chkCodigoBarra', 3, 4, -3],
                          ['lblMatrizFilial', 'chkMatrizFilial', 3, 4, -3],
                          ['lblDeposito', 'chkDeposito', 3, 4, -3],
                          ['lblAsstec', 'chkAsstec', 3, 4, -3],
                          ['lblExigeVinculacaoAsstec', 'chkExigeVinculacaoAsstec', 3, 4, -3],
                          ['lblPermiteOutroProduto', 'chkPermiteOutroProduto', 3, 4, -3],
                          ['lblExtrato', 'chkExtrato', 3, 4, -3],
                          ['lblTomacaoServico', 'chkTomacaoServico', 3, 4, -3],
                          ['lblPermitirAnteciparEntrega', 'chkPermitirAnteciparEntrega', 3, 4]],
                          ['lblHrTransacao', 'hrTransacao']);

    //@@ *** Baixa - Div secundario superior divSup02_03 ***/
    adjustElementsInForm([['lblObservacao_03', 'txtObservacao_03', 96, 2]],
                          ['lblHrBaixa', 'hrBaixa']);

    //fillCmbsCT();

}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2) {
    // Da automacao, deve constar de todos os ifs do carrierArrived
    if (__currFormState() != 0)
        return null;

    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2) {

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************


/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@

    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habilitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.

}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked) {
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;

    if (((btnClicked == 'SUPDET') ||
          (btnClicked == 'SUPREFR') ||
          (btnClicked == 'SUPANT') ||
          (btnClicked == 'SUPSEG')) && (dsoSup01.recordset[glb_sFldTipoRegistroName].value != 653)) {
        startDynamicCmbs();
    }
    else
        // volta para a automacao    
        finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked) {
    //@@


    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
    glb_BtnFromFramWork = '';

    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked) {
    var i, j;
    //var aSelNivel = new Array('selNivel_01','selNivel_02','selCodigoTributacao');
    var aSelNivel = new Array('selNivel_01', 'selNivel_02');
    var oCurrSel;
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento']);

    if ((getCurrTipoRegID() == 651) || (getCurrTipoRegID() == 652))
        showCompForm(null);

    // combo selNivel_01 e selNivel_02 estatico que nao depende de tabela
    var aOptions = new Array([1, '1'],
                              [2, '2'],
                              [3, '3']);

    /*var aOptions = new Array ([1,'1'],
                              [2,'2']); */

    clearComboEx(aSelNivel);
    for (i = 0; i <= 1; i++) {
        oCurrSel = eval(aSelNivel[i]);
        var oldDataSrc = oCurrSel.dataSrc;
        var oldDataFld = oCurrSel.dataFld;
        var oOption;
        oCurrSel.dataSrc = '';
        oCurrSel.dataFld = '';
        if (i == 2)
            aOptions = [['00', '00'],
						['10', '10'],
						['20', '20'],
						['30', '30'],
						['40', '40'], ['41', '41'],
						['50', '50'], ['51', '51'],
						['60', '60'],
						['70', '70'],
						['90', '90']];

        for (j = 0; j < aOptions.length; j++) {
            oOption = document.createElement("OPTION");
            oOption.text = aOptions[j][1];
            oOption.value = aOptions[j][0];
            oCurrSel.add(oOption);
        }
        oCurrSel.dataSrc = oldDataSrc;
        oCurrSel.dataFld = oldDataFld;

        if ((btnClicked == 'SUPINCL') || (btnClicked == 'SUPALT'))
            oCurrSel.disabled = oCurrSel.options.length == 0;
    }

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;
    var sLinkFld = cmb.dataFld.toUpperCase();


    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

    if (sLinkFld == 'NIVEL') {
        showCompForm(cmb);

        clearComboEx(['selOperacaoMaeID_01', 'selOperacaoMaeID_02']);
        clearComboEx(['selTransacaoBaixaID_01']);

        if (document.getElementById(cmbID).value == 1)
            return;

        fillCmbsOperacaoMaeID(cmbID);
        fillCmbsTransacaoBaixaID();
    }
    else if (sLinkFld == 'TRANSACAOBAIXAID') {
        if (dsoSup01.recordset['TransacaoBaixaID'].value != selTransacaoBaixaID_01.value)
            glb_nTransacaoID = selTransacaoBaixaID_01.value;
        else
            glb_nTransacaoID = dsoSup01.recordset['TransacaoBaixaID'].value;

        startDynamicCmbs();
    }


    adjustLabelsCombos(false);

    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selTipoRegistroID') {
        adjustSupInterface();
        verifyCmb();
    }
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK(currEstadoID, newEstadoID) {
    verifyOperacaoInServer(currEstadoID, newEstadoID);
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID, bSavedID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    // Documentos
    if ((controlBar == 'SUP') && (btnClicked == 1)) {
        __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
    }

    if ((controlBar == 'SUP') && (btnClicked == 2)) {
        openModalPrint();
    }

    if (btnClicked == 3) {
        window.top.openModalControleDocumento('S', '', 422, null, null, 'T');
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    if (btnClicked == 'SUPOK') {
        // Valida o campo Finalidade.
        var validos = "FRCME";
        var valido = true;

        for (i = 0; i < txtFinalidade.length; i++) {
            valido &= (validos.search(txtFinalidade.value.charAt(i)) >= 0);
        }

        if (!valido) {
            window.top.overflyGen.Alert("As letras v�lidas para o campo finalidade\ns�o F, R, C, M ou E.");

            return null;
        }

        if (selNivelLiberacao.value == 0)
            dsoSup01.recordset['NivelLiberacao'].value = null;
    }

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Modal de impressao
    if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
        // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

    else if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode() {
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad() {

    showBtnsEspecControlBar('sup', true, [1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento']);


}

function fillCmbsOperacaoMaeID(cmbID) {
    lockInterface(true);

    clearComboEx(['selOperacaoMaeID_01', 'selOperacaoMaeID_02']);

    var strPars = new String();
    var sGrupo;

    if (cmbID == 'selNivel_01')
        sGrupo = 'CFOP';
    else
        sGrupo = 'TRANSACOES';

    strPars = '?nNivel=' + escape(dsoSup01.recordset['Nivel'].value);
    strPars += '&sGrupo=' + escape(sGrupo);

    dsoCmbDynamic01.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/operacoes/serverside/fillcmboperacaomae.aspx' + strPars;
    if (cmbID == 'selNivel_01')
        dsoCmbDynamic01.ondatasetcomplete = operacaomae01_dsc;
    else
        dsoCmbDynamic01.ondatasetcomplete = operacaomae02_dsc;
    dsoCmbDynamic01.refresh();
}

function fillCmbsTransacaoBaixaID() {
    lockInterface(true);
    var nTipoRegistroID = dsoSup01.recordset[glb_sFldTipoRegistroName].value;

    clearComboEx(['selTransacaoBaixaID_01']);

    if (nTipoRegistroID == 651) {
        setConnection(dsoCmbDynamic02);

        dsoCmbDynamic02.SQL = 'SELECT OperacaoID AS fldID, OperacaoID AS fldName ' +
                              'FROM Operacoes WITH(NOLOCK) ' +
                              'WHERE EstadoID=2 AND TipoOperacaoID=652 AND Nivel=3 ' +
                              'ORDER BY fldName';

        dsoCmbDynamic02.ondatasetcomplete = transacaoBaixaID01_dcs;

        dsoCmbDynamic02.Refresh();
    }
}


function transacaoBaixaID01_dcs() {
    fillCmbDynamics(dsoCmbDynamic02, selTransacaoBaixaID_01);
}

function operacaomae01_dsc() {
    fillCmbDynamics(dsoCmbDynamic01, selOperacaoMaeID_01);
}

function operacaomae02_dsc() {
    fillCmbDynamics(dsoCmbDynamic01, selOperacaoMaeID_02);
}

function fillCmbDynamics(dso, oCmb) {
    clearComboEx([oCmb.id]);
    oCurrSel = oCmb;
    var oldDataSrc = oCurrSel.dataSrc;
    var oldDataFld = oCurrSel.dataFld;
    var oOption;
    oCurrSel.dataSrc = '';
    oCurrSel.dataFld = '';
    dso.recordset.MoveFirst();
    while (!dso.recordset.EOF) {
        /*oOption = document.createElement("OPTION");
        oOption.text = dso.recordset['fldName'].value;
        oOption.value = dso.recordset['fldID'].value;
        oCurrSel.add(oOption);
        dso.recordset.MoveNext();

        */
        optionStr = dso.recordset['fldName'].value;
        optionValue = dso.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        oCurrSel.add(oOption);
        dso.recordset.MoveNext();
    }
    oCurrSel.dataSrc = oldDataSrc;
    oCurrSel.dataFld = oldDataFld;

    lockInterface(false);
    verifyCmb();
}

// Combo C�digo Tribut�rio ICMS
function CodigoTributarioICMS_DSC() {
    fillCmbDynamics(dsoCmbDynamic03, selCodigoTributacaoICMS);
}

// Combo C�digo Tribut�rio IPI
function CodigoTributarioIPI_DSC() {
    fillCmbDynamics(dsoCmbDynamic04, selCodigoTributacaoIPI);
}

// Combo C�digo Tribut�rio PIS
function CodigoTributarioPIS_DSC() {
    fillCmbDynamics(dsoCmbDynamic05, selCodigoTributacaoPIS);
}

// Combo C�digo Tribut�rio COFINS
function CodigoTributarioCOFINS_DSC() {
    fillCmbDynamics(dsoCmbDynamic06, selCodigoTributacaoCOFINS);
}

/********************************************************************
Funcao do programador
Disparada pela troca do combo selClassificacaoID e selTipoRegisttroID
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function showCompForm(oCmb) {
    if (oCmb == null) {
        nNivel = dsoSup01.recordset['Nivel'].value;
        nBaixaFinanceiro = dsoSup01.recordset['BaixaFinanceiro'].value;
        Operacoes = 0;

        if (nBaixaFinanceiro == 1) {
            VerficaOperacao(nBaixaFinanceiro);
        }

    }
    else
        nNivel = oCmb.value;
    if (getCurrTipoRegID() == 651) {
        if (nNivel >= 2) {
            lblOperacaoMaeID_01.style.visibility = 'inherit';
            selOperacaoMaeID_01.style.visibility = 'inherit';
            lblCodigo.style.visibility = 'inherit';
            txtCodigo.style.visibility = 'inherit';

            if (nNivel == 3) {
                lblTransacaoBaixaID_01.style.visibility = 'inherit';
                selTransacaoBaixaID_01.style.visibility = 'inherit';
                lblHistoricoPadraoID.style.visibility = 'inherit';
                selHistoricoPadraoID.style.visibility = 'inherit';
                selHistoricoPadraoValorExcedenteID.style.visibility = (Operacoes == 0 ? 'hidden' : 'inherit');
                lblHistoricoPadraoValorExcedenteID.style.visibility = (Operacoes == 0 ? 'hidden' : 'inherit');
                lblAplicacaoID.style.visibility = 'inherit';
                selAplicacaoID.style.visibility = 'inherit';
                chkComunicacao.style.visibility = 'inherit';
                chkEnergia.style.visibility = 'inherit';
                chkTransporte.style.visibility = 'inherit';
                chkIndustria.style.visibility = 'inherit';
                chkComercio.style.visibility = 'inherit';
                chkServico.style.visibility = 'inherit';
                chkAlteraAliquota.style.visibility = 'inherit';
                chkAIC.style.visibility = 'inherit';
                //selCodigoTributacao.style.visibility = 'inherit';
                selCodigoTributacaoICMS.style.visibility = 'hidden';
                selCodigoTributacaoIPI.style.visibility = 'hidden';
                selCodigoTributacaoPIS.style.visibility = 'hidden';
                selCodigoTributacaoCOFINS.style.visibility = 'hidden';
                chkProducao.style.visibility = 'inherit';
                chkMicroObrigatorio.style.visibility = 'inherit';
                lblComunicacao.style.visibility = 'inherit';
                lblProducao.style.visibility = 'inherit';
                lblMicroObrigatorio.style.visibility = 'inherit';
                lblEnergia.style.visibility = 'inherit';
                lblTransporte.style.visibility = 'inherit';
                lblIndustria.style.visibility = 'inherit';
                lblComercio.style.visibility = 'inherit';
                lblServico.style.visibility = 'inherit';
                lblAlteraAliquota.style.visibility = 'inherit';
                lblCodigoTributacaoPIS.style.visibility = 'hidden';
                lblCodigoTributacaoCOFINS.style.visibility = 'hidden';
                lblCodigoTributacaoIPI.style.visibility = 'hidden';
                lblCodigoTributacaoICMS.style.visibility = 'hidden';
            }
            else {
                lblTransacaoBaixaID_01.style.visibility = 'hidden';
                selTransacaoBaixaID_01.style.visibility = 'hidden';
                lblHistoricoPadraoID.style.visibility = 'hidden';
                selHistoricoPadraoID.style.visibility = 'hidden';
                selHistoricoPadraoValorExcedenteID.style.visibility = 'hidden';
                lblHistoricoPadraoValorExcedenteID.style.visibility = 'hidden';
                lblAplicacaoID.style.visibility = 'hidden';
                selAplicacaoID.style.visibility = 'hidden';
                chkComunicacao.style.visibility = 'hidden';
                chkEnergia.style.visibility = 'hidden';
                chkTransporte.style.visibility = 'hidden';
                chkIndustria.style.visibility = 'hidden';
                chkComercio.style.visibility = 'hidden';
                chkServico.style.visibility = 'hidden';
                chkAlteraAliquota.style.visibility = 'hidden';
                chkAIC.style.visibility = 'hidden';
                selCodigoTributacaoICMS.style.visibility = 'hidden';
                selCodigoTributacaoIPI.style.visibility = 'hidden';
                selCodigoTributacaoPIS.style.visibility = 'hidden';
                selCodigoTributacaoCOFINS.style.visibility = 'hidden';
                chkProducao.style.visibility = 'hidden';
                chkMicroObrigatorio.style.visibility = 'hidden';
                lblComunicacao.style.visibility = 'hidden';
                lblProducao.style.visibility = 'hidden';
                lblMicroObrigatorio.style.visibility = 'hidden';
                lblEnergia.style.visibility = 'hidden';
                lblTransporte.style.visibility = 'hidden';
                lblIndustria.style.visibility = 'hidden';
                lblComercio.style.visibility = 'hidden';
                lblServico.style.visibility = 'hidden';
                lblAlteraAliquota.style.visibility = 'hidden';
                lblAIC.style.visibility = 'hidden';
                lblCodigoTributacaoPIS.style.visibility = 'hidden';
                lblCodigoTributacaoCOFINS.style.visibility = 'hidden';
                lblCodigoTributacaoIPI.style.visibility = 'hidden';
                lblCodigoTributacaoICMS.style.visibility = 'hidden';
            }
        }
        else {
            lblOperacaoMaeID_01.style.visibility = 'hidden';
            selOperacaoMaeID_01.style.visibility = 'hidden';
            lblCodigo.style.visibility = 'hidden';
            txtCodigo.style.visibility = 'hidden';
            lblTransacaoBaixaID_01.style.visibility = 'hidden';
            selTransacaoBaixaID_01.style.visibility = 'hidden';
            lblHistoricoPadraoID.style.visibility = 'hidden';
            selHistoricoPadraoID.style.visibility = 'hidden';
            selHistoricoPadraoValorExcedenteID.style.visibility = (Operacoes == 0 ? 'hidden' : 'inherit');
            lblHistoricoPadraoValorExcedenteID.style.visibility = (Operacoes == 0 ? 'hidden' : 'inherit');
            lblAplicacaoID.style.visibility = 'hidden';
            selAplicacaoID.style.visibility = 'hidden';
            chkComunicacao.style.visibility = 'hidden';
            chkEnergia.style.visibility = 'hidden';
            chkTransporte.style.visibility = 'hidden';
            chkIndustria.style.visibility = 'hidden';
            chkComercio.style.visibility = 'hidden';
            chkServico.style.visibility = 'hidden';
            chkAlteraAliquota.style.visibility = 'hidden';
            chkAIC.style.visibility = 'hidden';
            //selCodigoTributacao.style.visibility = 'hidden';
            selCodigoTributacaoICMS.style.visibility = 'hidden';
            selCodigoTributacaoIPI.style.visibility = 'hidden';
            selCodigoTributacaoPIS.style.visibility = 'hidden';
            selCodigoTributacaoCOFINS.style.visibility = 'hidden';
            chkProducao.style.visibility = 'hidden';
            chkMicroObrigatorio.style.visibility = 'hidden';
            lblComunicacao.style.visibility = 'hidden';
            lblProducao.style.visibility = 'hidden';
            lblMicroObrigatorio.style.visibility = 'hidden';
            lblEnergia.style.visibility = 'hidden';
            lblTransporte.style.visibility = 'hidden';
            lblIndustria.style.visibility = 'hidden';
            lblComercio.style.visibility = 'hidden';
            lblServico.style.visibility = 'hidden';
            lblAlteraAliquota.style.visibility = 'hidden';
            lblAIC.style.visibility = 'hidden';
            lblCodigoTributacaoPIS.style.visibility = 'hidden';
            lblCodigoTributacaoCOFINS.style.visibility = 'hidden';
            lblCodigoTributacaoIPI.style.visibility = 'hidden';
            lblCodigoTributacaoICMS.style.visibility = 'hidden';
        }
    }
    else if (getCurrTipoRegID() == 652) {
        if (nNivel >= 2) {
            lblOperacaoMaeID_02.style.visibility = 'inherit';
            selOperacaoMaeID_02.style.visibility = 'inherit';

            if (nNivel == 3) {
                selMetodoCustoID.style.visibility = 'inherit';
                lblMetodoCustoID.style.visibility = 'inherit';
                selReferenciaCustoID.style.visibility = 'inherit';
                lblReferenciaCustoID.style.visibility = 'inherit';
                selReferenciaImpostoID.style.visibility = 'inherit';
                lblReferenciaImpostoID.style.visibility = 'inherit';
                selReferenciaValorUnitarioID.style.visibility = 'inherit';
                lblReferenciaValorUnitarioID.style.visibility = 'inherit';
                selNivelLiberacao.style.visibility = 'inherit';
                lblNivelLiberacao.style.visibility = 'inherit';
                txtOrdem2.style.visibility = 'inherit';
                lblOrdem2.style.visibility = 'inherit';
                chkPermiteOutroProduto.style.visibility = 'inherit';
                chkExigeVinculacaoAsstec.style.visibility = 'inherit';
                chkEntrada.style.visibility = 'inherit';
                chkFornecedor.style.visibility = 'inherit';
                chkCliente.style.visibility = 'inherit';
                chkEhRetorno.style.visibility = 'inherit';
                chkResultado.style.visibility = 'inherit';
                chkInadimplencia.style.visibility = 'inherit';
                chkRIR.style.visibility = 'inherit';
                chkAtualizaFinanceiro.style.visibility = 'inherit';
                chkBaixaFinanceiro.style.visibility = 'inherit';
                chkCodigoBarra.style.visibility = 'inherit';
                chkMatrizFilial.style.visibility = 'inherit';
                chkDeposito.style.visibility = 'inherit';
                chkAsstec.style.visibility = 'inherit';
                chkExtrato.style.visibility = 'inherit';
                lblPermiteOutroProduto.style.visibility = 'inherit';
                lblExigeVinculacaoAsstec.style.visibility = 'inherit';
                lblEntrada.style.visibility = 'inherit';
                lblFornecedor.style.visibility = 'inherit';
                lblCliente.style.visibility = 'inherit';
                lblEhRetorno.style.visibility = 'inherit';
                lblResultado.style.visibility = 'inherit';
                lblInadimplencia.style.visibility = 'inherit';
                lblRIR.style.visibility = 'inherit';
                lblAtualizaFinanceiro.style.visibility = 'inherit';
                lblBaixaFinanceiro.style.visibility = 'inherit';
                lblCodigoBarra.style.visibility = 'inherit';
                lblMatrizFilial.style.visibility = 'inherit';
                lblDeposito.style.visibility = 'inherit';
                lblAsstec.style.visibility = 'inherit';
                lblExtrato.style.visibility = 'inherit';
                lblCodigoTributacaoPIS.style.visibility = 'inherit';
                lblCodigoTributacaoCOFINS.style.visibility = 'inherit';
                lblCodigoTributacaoIPI.style.visibility = 'inherit';
                lblCodigoTributacaoICMS.style.visibility = 'inherit';
                selHistoricoPadraoValorExcedenteID.style.visibility = (Operacoes == 0 ? 'hidden' : 'inherit');
                lblHistoricoPadraoValorExcedenteID.style.visibility = (Operacoes == 0 ? 'hidden' : 'inherit');
            }

            else {
                lblMetodoCustoID.style.visibility = 'hidden';
                selMetodoCustoID.style.visibility = 'hidden';
                selReferenciaCustoID.style.visibility = 'hidden';
                lblReferenciaCustoID.style.visibility = 'hidden';
                selReferenciaImpostoID.style.visibility = 'hidden';
                lblReferenciaImpostoID.style.visibility = 'hidden';
                selReferenciaValorUnitarioID.style.visibility = 'hidden';
                lblReferenciaValorUnitarioID.style.visibility = 'hidden';
                selNivelLiberacao.style.visibility = 'hidden';
                lblNivelLiberacao.style.visibility = 'hidden';
                txtOrdem2.style.visibility = 'hidden';
                lblOrdem2.style.visibility = 'hidden';
                chkPermiteOutroProduto.style.visibility = 'hidden';
                chkExigeVinculacaoAsstec.style.visibility = 'hidden';
                chkEntrada.style.visibility = 'hidden';
                chkFornecedor.style.visibility = 'hidden';
                chkCliente.style.visibility = 'hidden';
                chkEhRetorno.style.visibility = 'hidden';
                chkResultado.style.visibility = 'hidden';
                chkInadimplencia.style.visibility = 'hidden';
                chkRIR.style.visibility = 'hidden';
                chkAtualizaFinanceiro.style.visibility = 'hidden';
                chkBaixaFinanceiro.style.visibility = 'hidden';
                chkCodigoBarra.style.visibility = 'hidden';
                chkMatrizFilial.style.visibility = 'hidden';
                chkDeposito.style.visibility = 'hidden';
                chkAsstec.style.visibility = 'hidden';
                chkExtrato.style.visibility = 'hidden';
                lblPermiteOutroProduto.style.visibility = 'hidden';
                lblExigeVinculacaoAsstec.style.visibility = 'hidden';
                lblEntrada.style.visibility = 'hidden';
                lblFornecedor.style.visibility = 'hidden';
                lblCliente.style.visibility = 'hidden';
                lblEhRetorno.style.visibility = 'hidden';
                lblResultado.style.visibility = 'hidden';
                lblInadimplencia.style.visibility = 'hidden';
                lblRIR.style.visibility = 'hidden';
                lblAtualizaFinanceiro.style.visibility = 'hidden';
                lblBaixaFinanceiro.style.visibility = 'hidden';
                lblCodigoBarra.style.visibility = 'hidden';
                lblMatrizFilial.style.visibility = 'hidden';
                lblDeposito.style.visibility = 'hidden';
                lblAsstec.style.visibility = 'hidden';
                lblExtrato.style.visibility = 'hidden';
                lblCodigoTributacaoPIS.style.visibility = 'hidden';
                lblCodigoTributacaoCOFINS.style.visibility = 'hidden';
                lblCodigoTributacaoIPI.style.visibility = 'hidden';
                lblCodigoTributacaoICMS.style.visibility = 'hidden';
                selHistoricoPadraoValorExcedenteID.style.visibility = (Operacoes == 0 ? 'hidden' : 'inherit');
                lblHistoricoPadraoValorExcedenteID.style.visibility = (Operacoes == 0 ? 'hidden' : 'inherit');
            }
        }
        else {
            lblOperacaoMaeID_02.style.visibility = 'hidden';
            selOperacaoMaeID_02.style.visibility = 'hidden';
            lblMetodoCustoID.style.visibility = 'hidden';
            selMetodoCustoID.style.visibility = 'hidden';
            selReferenciaCustoID.style.visibility = 'hidden';
            lblReferenciaCustoID.style.visibility = 'hidden';
            selReferenciaImpostoID.style.visibility = 'hidden';
            lblReferenciaImpostoID.style.visibility = 'hidden';
            selReferenciaValorUnitarioID.style.visibility = 'hidden';
            lblReferenciaValorUnitarioID.style.visibility = 'hidden';
            selNivelLiberacao.style.visibility = 'hidden';
            lblNivelLiberacao.style.visibility = 'hidden';
            txtOrdem2.style.visibility = 'hidden';
            lblOrdem2.style.visibility = 'hidden';
            chkPermiteOutroProduto.style.visibility = 'hidden';
            chkExigeVinculacaoAsstec.style.visibility = 'hidden';
            chkEntrada.style.visibility = 'hidden';
            chkFornecedor.style.visibility = 'hidden';
            chkCliente.style.visibility = 'hidden';
            chkEhRetorno.style.visibility = 'hidden';
            chkResultado.style.visibility = 'hidden';
            chkInadimplencia.style.visibility = 'hidden';
            chkRIR.style.visibility = 'hidden';
            chkAtualizaFinanceiro.style.visibility = 'hidden';
            chkBaixaFinanceiro.style.visibility = 'hidden';
            chkCodigoBarra.style.visibility = 'hidden';
            chkMatrizFilial.style.visibility = 'hidden';
            chkDeposito.style.visibility = 'hidden';
            chkAsstec.style.visibility = 'hidden';
            chkExtrato.style.visibility = 'hidden';
            lblPermiteOutroProduto.style.visibility = 'hidden';
            lblExigeVinculacaoAsstec.style.visibility = 'hidden';
            lblEntrada.style.visibility = 'hidden';
            lblFornecedor.style.visibility = 'hidden';
            lblCliente.style.visibility = 'hidden';
            lblEhRetorno.style.visibility = 'hidden';
            lblResultado.style.visibility = 'hidden';
            lblInadimplencia.style.visibility = 'hidden';
            lblRIR.style.visibility = 'hidden';
            lblAtualizaFinanceiro.style.visibility = 'hidden';
            lblBaixaFinanceiro.style.visibility = 'hidden';
            lblCodigoBarra.style.visibility = 'hidden';
            lblMatrizFilial.style.visibility = 'hidden';
            lblDeposito.style.visibility = 'hidden';
            lblAsstec.style.visibility = 'hidden';
            lblExtrato.style.visibility = 'hidden';
            lblCodigoTributacaoPIS.style.visibility = 'hidden';
            lblCodigoTributacaoCOFINS.style.visibility = 'hidden';
            lblCodigoTributacaoIPI.style.visibility = 'hidden';
            lblCodigoTributacaoICMS.style.visibility = 'hidden';
            selHistoricoPadraoValorExcedenteID.style.visibility = (Operacoes == 0 ? 'hidden' : 'inherit');
            lblHistoricoPadraoValorExcedenteID.style.visibility = (Operacoes == 0 ? 'hidden' : 'inherit');
        }
    }

}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs() {
    lockInterface(true);
    clearComboEx(['selOperacaoMaeID_01', 'selOperacaoMaeID_02', 'selCodigoTributacaoICMS', 'selCodigoTributacaoIPI', 'selCodigoTributacaoPIS', 'selCodigoTributacaoCOFINS']);
    var strPars = new String();
    var sGrupo;
    var nTipoRegistroID = dsoSup01.recordset[glb_sFldTipoRegistroName].value;
    var nTransacaoID = (glb_nTransacaoID == null || glb_nTransacaoID == '' ? 'NULL' : glb_nTransacaoID);

    glb_CounterCmbsDynamics = 5;

    if ((nTipoRegistroID == 651) || (nTipoRegistroID == 652))
        glb_CounterCmbsDynamics = 6;

    if (nTipoRegistroID == 651)
        sGrupo = 'CFOP';
    else if (nTipoRegistroID == 652)
        sGrupo = 'TRANSACOES';

    strPars = '?nNivel=' + escape((dsoSup01.recordset['Nivel'].value) - 1);
    strPars += '&sGrupo=' + escape(sGrupo);

    dsoCmbDynamic01.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/operacoes/serverside/fillcmboperacaomae.aspx' + strPars;
    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.refresh();

    if ((nTipoRegistroID == 651) || (nTipoRegistroID == 652)) {
        setConnection(dsoCmbDynamic02);

        if (nTipoRegistroID == 651) {
            dsoCmbDynamic02.SQL = 'SELECT OperacaoID AS fldID, OperacaoID AS fldName ' +
                                  'FROM Operacoes WITH(NOLOCK) ' +
                                  'WHERE EstadoID=2 AND TipoOperacaoID=652 AND Nivel=3 ' +
                                  'ORDER BY fldName';
        }
        else if (nTipoRegistroID == 652) {
            dsoCmbDynamic02.SQL = 'SELECT OperacaoID AS fldID, OperacaoID AS fldName ' +
                                  'FROM Operacoes WITH(NOLOCK) ' +
                                  'WHERE EstadoID=2 AND TipoOperacaoID=653 ' +
                                  'ORDER BY fldName';
        }

        dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
        dsoCmbDynamic02.Refresh();
    }

    // C�digo Tributa��o ICMS
    setConnection(dsoCmbDynamic03);
    dsoCmbDynamic03.SQL = 'SELECT ConCodigoID AS fldID, CodigoTributacao AS fldName ' +
                          'FROM Conceitos_CodigoTributacao WITH(NOLOCK)  ' +
                          'WHERE ConceitoID = 9 AND (Entrada IS NULL OR Entrada = (SELECT Entrada FROM Operacoes Transacao WITH(NOLOCK) WHERE Transacao.OperacaoID = ISNULL(' + nTransacaoID + ',0))) ' +
                          'ORDER BY fldName';
    dsoCmbDynamic03.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic03.Refresh();

    // Co�digo Tributa��o IPI
    setConnection(dsoCmbDynamic04);
    dsoCmbDynamic04.SQL = 'SELECT ConCodigoID AS fldID, CodigoTributacao AS fldName ' +
                          'FROM Conceitos_CodigoTributacao WITH(NOLOCK)  ' +
                          'WHERE ConceitoID = 8 AND (Entrada IS NULL OR Entrada = (SELECT Entrada FROM Operacoes Transacao WITH(NOLOCK) WHERE Transacao.OperacaoID = ISNULL(' + nTransacaoID + ',0))) ' +
                          'ORDER BY fldName';
    dsoCmbDynamic04.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic04.Refresh();

    // C�digo Tributa��o PIS
    setConnection(dsoCmbDynamic05);
    dsoCmbDynamic05.SQL = 'SELECT ConCodigoID AS fldID, CodigoTributacao AS fldName ' +
                          'FROM Conceitos_CodigoTributacao WITH(NOLOCK)  ' +
                          'WHERE ConceitoID = 4 AND (Entrada IS NULL OR Entrada = (SELECT Entrada FROM Operacoes Transacao WITH(NOLOCK) WHERE Transacao.OperacaoID = ISNULL(' + nTransacaoID + ',0))) ' +
                          'ORDER BY fldName';
    dsoCmbDynamic05.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic05.Refresh();

    // C�digo Tributa��o COFINS
    setConnection(dsoCmbDynamic06);
    dsoCmbDynamic06.SQL = 'SELECT ConCodigoID AS fldID, CodigoTributacao AS fldName ' +
                          'FROM Conceitos_CodigoTributacao WITH(NOLOCK)  ' +
                          'WHERE ConceitoID = 5 AND (Entrada IS NULL OR Entrada = (SELECT Entrada FROM Operacoes Transacao WITH(NOLOCK) WHERE Transacao.OperacaoID = ISNULL(' + nTransacaoID + ',0))) ' +
                          'ORDER BY fldName';
    dsoCmbDynamic06.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic06.Refresh();
}

/********************************************************************
Funcao disparada pelo programador.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    glb_CounterCmbsDynamics--;
    if (glb_CounterCmbsDynamics != 0)
        return null;

    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var nTipoRegistroID = dsoSup01.recordset[glb_sFldTipoRegistroName].value;
    var ocmbTransacaoBaixa = (selTransacaoBaixaID_01);  //(nTipoRegistroID == 651 ? selTransacaoBaixaID_01 : selTransacaoBaixaID_02);
    var aCmbsDynamics = [ocmbTransacaoBaixa, selCodigoTributacaoICMS, selCodigoTributacaoIPI, selCodigoTributacaoPIS, selCodigoTributacaoCOFINS];
    var aDSOsDunamics = [dsoCmbDynamic02, dsoCmbDynamic03, dsoCmbDynamic04, dsoCmbDynamic05, dsoCmbDynamic06];
    var oOption;

    // Inicia o carregamento de combo dinamico
    //
    //clearComboEx([ocmbTransacaoBaixa.id]);
    clearComboEx(['ocmbTransacaoBaixa', 'selCodigoTributacaoICMS', 'selCodigoTributacaoIPI', 'selCodigoTributacaoPIS', 'selCodigoTributacaoCOFINS']);

    if (dsoSup01.recordset[glb_sFldTipoRegistroName].value == 651)
        operacaomae01_dsc();
    else if (dsoSup01.recordset[glb_sFldTipoRegistroName].value == 652)
        operacaomae02_dsc();

    CodigoTributarioICMS_DSC();
    CodigoTributarioIPI_DSC();
    CodigoTributarioPIS_DSC();
    CodigoTributarioCOFINS_DSC();

    for (var icounter = 0; icounter <= 0; icounter++) {
        oldDataSrc = aCmbsDynamics[icounter].dataSrc;
        oldDataFld = aCmbsDynamics[icounter].dataFld;
        aCmbsDynamics[icounter].dataSrc = '';
        aCmbsDynamics[icounter].dataFld = '';
        aDSOsDunamics[icounter].recordset.MoveFirst();

        while (!aDSOsDunamics[icounter].recordset.EOF) {
            optionStr = aDSOsDunamics[icounter].recordset['fldName'].value;
            optionValue = aDSOsDunamics[icounter].recordset['fldID'].value;
            oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[icounter].add(oOption);
            aDSOsDunamics[icounter].recordset.MoveNext();
        }

        aCmbsDynamics[icounter].dataSrc = oldDataSrc;
        aCmbsDynamics[icounter].dataFld = oldDataFld;
    }

    adjustLabelsCombos(true);

    if (glb_BtnFromFramWork != '') {
        // volta para a automacao
        finalOfSupCascade(glb_BtnFromFramWork);
    }
}

function adjustLabelsCombos(bPaging) {
    if (bPaging) {
        setLabelOfControl(lblTipoRegistroID, dsoSup01.recordset['TipoOperacaoID'].value);
        setLabelOfControl(lblPaisID, dsoSup01.recordset['PaisID'].value);
        setLabelOfControl(lblPaisID2, dsoSup01.recordset['PaisID'].value);
        setLabelOfControl(lblHistoricoPadraoID, dsoSup01.recordset['HistoricoPadraoID'].value);
        setLabelOfControl(lblHistoricoPadraoValorExcedenteID, dsoSup01.recordset['HistoricoPadraoValorExcedenteID'].value);
        setLabelOfControl(lblAplicacaoID, dsoSup01.recordset['AplicacaoID'].value);
    }
    else {
        setLabelOfControl(lblTipoRegistroID, selTipoRegistroID.value);
        setLabelOfControl(lblPaisID, selPaisID.value);
        setLabelOfControl(lblPaisID2, selPaisID2.value);
        setLabelOfControl(lblHistoricoPadraoID, selHistoricoPadraoID.value);
        setLabelOfControl(lblHistoricoPadraoValorExcedenteID, selHistoricoPadraoValorExcedenteID.value);
        setLabelOfControl(lblAplicacaoID, selAplicacaoID.value);
    }
}

function verifyCmb() {
    return null;
    if (selOperacaoMaeID_01.options.length == 0)
        selOperacaoMaeID_01.disabled = true;

    if (selOperacaoMaeID_02.options.length == 0)
        selOperacaoMaeID_02.disabled = true;

    if (selTransacaoBaixaID_01.options.length == 0)
        selTransacaoBaixaID_01.disabled = true;

    if (selCodigoTributacaoICMS.options.length == 0)
        selCodigoTributacaoICMS.disabled = true;

    if (selCodigoTributacaoIPI.options.length == 0)
        selCodigoTributacaoIPI.disabled = true;

    if (selCodigoTributacaoPIS.options.length == 0)
        selCodigoTributacaoPIS.disabled = true;

    if (selCodigoTributacaoCOFINS.options.length == 0)
        selCodigoTributacaoCOFINS.disabled = true;
}

/********************************************************************
Verificacoes da Operacao
********************************************************************/
function verifyOperacaoInServer(currEstadoID, newEstadoID) {
    var nOperacaoID = dsoSup01.recordset['OperacaoID'].value;
    var strPars = new String();

    strPars = '?nOperacaoID=' + escape(nOperacaoID);
    strPars += '&nEstadoDeID=' + escape(currEstadoID);
    strPars += '&nEstadoParaID=' + escape(newEstadoID);
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/operacoes/serverside/verificacaooperacao.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyOperacaoInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes da Operacao
********************************************************************/
function verifyOperacaoInServer_DSC() {
    var sMensagem = (dsoVerificacao.recordset.Fields['Mensagem'].value == null ? '' : dsoVerificacao.recordset.Fields['Mensagem'].value);
    var nResultado = (dsoVerificacao.recordset.Fields['Resultado'].value == null ? 0 : dsoVerificacao.recordset.Fields['Resultado'].value);

    if (nResultado == 1) {
        var _retConf = window.top.overflyGen.Confirm(sMensagem);

        if (_retConf == 0)
            return null;
        else if (_retConf == 1)
            stateMachSupExec('OK');
        else
            stateMachSupExec('CANC');
    }
    else if (nResultado == 2) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        stateMachSupExec('OK');
    }
    else {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        stateMachSupExec('CANC');
    }
}


function VerficaOperacao(nBaixaFinanceiro) {
    nTransacaoID = dsoSup01.recordset['OperacaoID'].value;

    dsoExcedente.SQL = 'SELECT COUNT (1) AS Operacoes FROM Operacoes a WITH (NOLOCK)' +
                           ' WHERE (a.OperacaoID = ' + nTransacaoID + ') AND (BaixaFinanceiro= ' + (nBaixaFinanceiro == true ? 1 : 0) + ') ' +
                            ' AND (ISNULL(dbo.fn_Operacoes_Referencia(a.OperacaoID, NULL, 722, NULL), 0) = 1) ';

    setConnection(dsoExcedente);
    dsoExcedente.ondatasetcomplete = VerficaOperacao_dsc;
    dsoExcedente.Refresh();

}
function VerficaOperacao_dsc() {
    if (!(dsoExcedente.recordset.BOF && dsoExcedente.recordset.EOF)) {
        if (dsoExcedente.recordset['Operacoes'].value != null && dsoExcedente.recordset['Operacoes'].value.length > 0) {
            if (window.top.overflyGen.Alert(dsoExcedente.recordset['Operacoes'].value) == 0)
                return 0;
        }

        Operacoes = dsoExcedente.recordset['Operacoes'].value;
    }
    else
        return 1;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    return null;
}
