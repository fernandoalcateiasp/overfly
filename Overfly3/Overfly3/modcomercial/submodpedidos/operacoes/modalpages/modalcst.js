/********************************************************************
modalcst.js

********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_nCFOPID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'OperacaoID' + '\'' + '].value');
var dsoGen01 = new CDatatransport('dsoGen01'); 
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    getDataInServer();

    // mostrar a janela movido para o final de preenchimento do grid
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Validar CST', 1);

    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 53;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul)
    heightFree = modHeight - topFree - frameBorder;

    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;

    // Ajusta botoes OK/Cancel para esta janela
    // O botao cancela nao e usado neste asp
    btnOK.style.visibility = 'hidden';
    btnOK.disabled = true;
    btnCanc.style.visibility = 'hidden';
    btnCanc.disabled = true;

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        //top = topFree + parseInt(selProduto.currentStyle.height, 10) + (3 * ELEM_GAP) + 6;
        top = topFree + ELEM_GAP;//parseInt(divFields.currentStyle.top, 10) + parseInt(divFields.currentStyle.height, 10);
        width = modWidth - frameBorder - 2 * ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) +
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10);
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_INF', null);
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_INF', null);
    }

    //lockControlsInModalWin(false);
}


/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServer() {
    lockControlsInModalWin(true);

    var strPars = "";
    var CFOPID = glb_nCFOPID;

    strPars += "?nOperacaoID=" + glb_nCFOPID;

    dsoGen01.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/operacoes/serverside/auditoriaCST.aspx' + strPars;
    dsoGen01.ondatasetcomplete = fillModalGrid_DSC;
    dsoGen01.refresh();
}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGrid_DSC() {
    fg.Redraw = 0;
    var i;
    var bgColorYellow = 0x00ffff;
    var aHint = new Array();

    headerGrid(fg, ['CFOPID',
                   'Imposto',
				   'Tag Origem',
				   'Tag Aliquota',
				   'Tag Base',
                   'CST',
				   'Apura��o',
                   'ImpostoID',
                   'CST_Descricao'], [7, 8]);

    fillGridMask(fg, dsoGen01, ['CFOPID',
                                'Imposto',
				                'TagOrigem',
				                'TagAliquota',
				                'TagBase',
                                'CST',
				                'IA',
                                'ImpostoID',
                                'CST_Descricao'],
                                ['', '', '', '', '', '', '', '', ''],
                                ['', '', '', '', '', '', '', '', '']);
    j = 0;
    aHint[j] = [0, 6, 'Apura Imposto?'];


    for (i = 1; i < fg.Rows; i++)
    {
        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'CST')) == "")
        {
            fg.Cell(6, i, getColIndexByColKey(fg, 'CST'), i, getColIndexByColKey(fg, 'CST')) = bgColorYellow;
            fg.Cell(6, i, getColIndexByColKey(fg, 'IA'), i, getColIndexByColKey(fg, 'IA')) = bgColorYellow;
        }

        if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'CST_Descricao')) != ' ') && (fg.TextMatrix(i, getColIndexByColKey(fg, 'CST_Descricao')) != '')) {
            j++;
            aHint[j] = [i, getColIndexByColKey(fg, 'CST'), fg.TextMatrix(i, getColIndexByColKey(fg, 'CST_Descricao'))];
        }



    }

    glb_aCelHint = aHint;
    alignColsInGrid(fg, []);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 2);
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;
    fg.Redraw = 2;

    with (modalcstBody) {
        style.backgroundColor = 'transparent';
        //scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    lockControlsInModalWin(false);
}


