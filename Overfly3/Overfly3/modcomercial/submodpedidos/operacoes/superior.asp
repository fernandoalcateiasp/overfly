<%@  language="VBSCRIPT" enablesessionstate="False" %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
	strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="operacoessup01Html" name="operacoessup01Html">

<head>

    <title></title>

    <%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
     
     Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/operacoes/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/submodpedidos/operacoes/superioresp.js" & Chr(34) & "></script>" & vbCrLf
    %>

    <script id="wndJSProc" language="javascript">
<!--

    //-->
    </script>

</head>

<!-- //@@ -->
<body id="operacoessup01Body" name="operacoessup01Body" language="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">

    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->

    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" datasrc="#dsoStateMachine" datafld="Estado" class="fldGeneral">
        <p id="lblOperacao" name="lblOperacao" class="lblGeneral">Opera��o</p>
        <input type="text" id="txtOperacao" name="txtOperacao" datasrc="#dsoSup01" datafld="Operacao" class="fldGeneral">
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo</p>
        <select id="selTipoRegistroID" name="selTipoRegistroID" class="fldGeneral" datasrc="#dsoSup01" datafld="TipoOperacaoID"></select>
    </div>

    <!-- Primeiro div secundario superior - CFOP -->
    <div id="divSup02_01" name="divSup02_01" class="divExtern">
        <p id="lblHrCFOP" name="lblHrCFOP" class="lblGeneral">CFOP</p>
        <hr id="hrCFOP" name="hrCFOP" class="lblGeneral">
        <p id="lblNivel_01" name="lblNivel_01" class="lblGeneral">N�vel</p>
        <select id="selNivel_01" name="selNivel_01" class="fldGeneral" datasrc="#dsoSup01" datafld="Nivel"></select>
        <p id="lblOperacaoMaeID_01" name="lblOperacaoMaeID_01" class="lblGeneral">CFOP M�e</p>
        <select id="selOperacaoMaeID_01" name="selOperacaoMaeID_01" class="fldGeneral" datasrc="#dsoSup01" datafld="OperacaoMaeID"></select>
        <p id="lblCodigo" name="lblCodigo" class="lblGeneral">C�digo</p>
        <input type="text" id="txtCodigo" name="txtCodigo" class="fldGeneral" datasrc="#dsoSup01" datafld="Codigo">
        <p id="lblPaisID" name="lblPaisID" class="lblGeneral">Pa�s</p>
        <select id="selPaisID" name="selPaisID" class="fldGeneral" datasrc="#dsoSup01" datafld="PaisID"></select>
        <p id="lblTransacaoBaixaID_01" name="lblTransacaoBaixaID_01" class="lblGeneral">Transa��o</p>
        <select id="selTransacaoBaixaID_01" name="selTransacaoBaixaID_01" class="fldGeneral" datasrc="#dsoSup01" datafld="TransacaoBaixaID"></select>
        <p id="lblAplicacaoID" name="lblAplicacaoID" class="lblGeneral">Aplica��o</p>
        <select id="selAplicacaoID" name="selAplicacaoID" class="fldGeneral" datasrc="#dsoSup01" datafld="AplicacaoID"></select>
        <!--
        <p id="lblCodigoTributacao" name="lblCodigoTributacao" class="lblGeneral">CT</p>
        <select id="selCodigoTributacao" name="selCodigoTributacao" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="CodigoTributacao" title="C�digo de Tributa��o"></select>
        -->
        <p id="lblCodigoTributacaoPIS" name="lblCodigoTributacaoPIS" class="lblGeneral" title="C�digo Tributa��o PIS">PIS</p>
        <select id="selCodigoTributacaoPIS" name="selCodigoTributacaoPIS" class="fldGeneral" datasrc="#dsoSup01" datafld="CodigoTributacao1" title="C�digo de Tributa��o PIS"></select>
        <p id="lblCodigoTributacaoCOFINS" name="lblCodigoTributacaoCOFINS" class="lblGeneral" title="C�digo Tributa��o COFINS">COFINS</p>
        <select id="selCodigoTributacaoCOFINS" name="selCodigoTributacaoCOFINS" class="fldGeneral" datasrc="#dsoSup01" datafld="CodigoTributacao2" title="C�digo de Tributa��o COFINS"></select>
        <p id="lblCodigoTributacaoIPI" name="lblCodigoTributacaoIPI" class="lblGeneral" title="C�digo Tributa��o IPI">IPI</p>
        <select id="selCodigoTributacaoIPI" name="selCodigoTributacaoIPI" class="fldGeneral" datasrc="#dsoSup01" datafld="CodigoTributacao3" title="C�digo de Tributa��o IPI"></select>
        <p id="lblCodigoTributacaoICMS" name="lblCodigoTributacaoICMS" class="lblGeneral" title="C�digo Tributa��o ICMS">ICMS</p>
        <select id="selCodigoTributacaoICMS" name="selCodigoTributacaoICMS" class="fldGeneral" datasrc="#dsoSup01" datafld="CodigoTributacao4" title="C�digo de Tributa��o ICMS"></select>
        <p id="lblFinalidade" name="lblFinalidade" class="lblGeneral" title="Finalidade: F-Fabrica��o, R-Revenda, C-Consumidor, M-Consumo c/ MP Bem, E-Export��o">Finalidade</p>
        <input type="text" id="txtFinalidade" name="txtFinalidade" class="fldGeneral" datasrc="#dsoSup01" datafld="Finalidade" title="Finalidade: F-Fabrica��o, R-Revenda, C-Consumidor, M-Consumo c/ MP Bem, E-Export��o" />
        <p id="lblContribuinte" name="lblContribuinte" class="lblGeneral" title="Contribuinte">Contrib</p>
        <select id="selContribuinte" name="selContribuinte" class="fldGeneral" datasrc="#dsoSup01" datafld="ContribuinteID">
            <option value='0'></option>
            <%
				Dim strSQL, rsData
				Set rsData = Server.CreateObject("ADODB.Recordset")
				
				strSQL = "SELECT ItemID, ItemMasculino " & _
					"FROM " & _
						"TiposAuxiliares_Itens WITH(NOLOCK) " & _
					"WHERE " & _
						"EstadoID=2 AND " & _
						"TipoID=3000 " & _
					"ORDER BY " & _
						"Ordem"
				
				rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

				If (Not (rsData.BOF AND rsData.EOF) ) Then
					While Not (rsData.EOF)
						Response.Write( "<option value='" & rsData.Fields("ItemID").Value & "'>" )
						Response.Write( rsData.Fields("ItemMasculino").Value & "</option>" )

						rsData.MoveNext()
					WEnd
				End If

				rsData.MoveFirst()
            %>
        </select>

        <p id="lblSuframa" name="lblSuframa" class="lblGeneral">Suframa</p>
        <select id="selSuframa" name="selSuframa" class="fldGeneral" datasrc="#dsoSup01" datafld="SuframaID">
            <option value='0'></option>
            <%
				If (Not (rsData.BOF AND rsData.EOF) ) Then
					While Not (rsData.EOF)
						Response.Write( "<option value='" & rsData.Fields("ItemID").Value & "'>" )
						Response.Write( rsData.Fields("ItemMasculino").Value & "</option>" )

						rsData.MoveNext()
					WEnd
				End If

				rsData.MoveFirst()
            %>
        </select>

        <p id="lblST" name="lblST" class="lblGeneral">ST</p>
        <select id="selST" name="selST" class="fldGeneral" datasrc="#dsoSup01" datafld="SubstituicaoTributariaID">
            <option value='0'></option>
            <%
				If (Not (rsData.BOF AND rsData.EOF) ) Then
					While Not (rsData.EOF)
						Response.Write( "<option value='" & rsData.Fields("ItemID").Value & "'>")
						Response.Write( rsData.Fields("ItemMasculino").Value & "</option>" )

						rsData.MoveNext()
					WEnd
				End If

				rsData.MoveFirst()
            %>
        </select>

        <p id="lblProducao" name="lblProducao" class="lblGeneral">Produ��o</p>
        <input type="checkbox" id="chkProducao" name="chkProducao" class="fldGeneral" datasrc="#dsoSup01" datafld="Producao" title="Gera Ordem de Produ��o?">
        <p id="lblMicroObrigatorio" name="lblMicroObrigatorio" class="lblGeneral">MO</p>
        <input type="checkbox" id="chkMicroObrigatorio" name="chkMicroObrigatorio" class="fldGeneral" datasrc="#dsoSup01" datafld="MicroObrigatorio" title="� obrigat�rio ter micro?">
        <p id="lblEnergia" name="lblEnergia" class="lblGeneral">Energia</p>
        <input type="checkbox" id="chkEnergia" name="chkEnergia" class="fldGeneral" datasrc="#dsoSup01" datafld="Energia" title="Energia">
        <p id="lblComunicacao" name="lblComunicacao" class="lblGeneral">Comunica��o</p>
        <input type="checkbox" id="chkComunicacao" name="chkComunicacao" class="fldGeneral" datasrc="#dsoSup01" datafld="Comunicacao" title="Comunica��o">
        <p id="lblTransporte" name="lblTransporte" class="lblGeneral">Transporte</p>
        <input type="checkbox" id="chkTransporte" name="chkTransporte" class="fldGeneral" datasrc="#dsoSup01" datafld="Transporte" title="Transporte">
        <p id="lblIndustria" name="lblIndustria" class="lblGeneral">Ind�stria</p>
        <input type="checkbox" id="chkIndustria" name="chkIndustria" class="fldGeneral" datasrc="#dsoSup01" datafld="Industria" title="Industria">
        <p id="lblComercio" name="lblComercio" class="lblGeneral">Com�rcio</p>
        <input type="checkbox" id="chkComercio" name="chkComercio" class="fldGeneral" datasrc="#dsoSup01" datafld="Comercio" title="Com�rcio">
        <p id="lblServico" name="lblServico" class="lblGeneral">Servi�o</p>
        <input type="checkbox" id="chkServico" name="chkServico" class="fldGeneral" datasrc="#dsoSup01" datafld="Servico" title="Servi�o">
        <p id="lblAlteraAliquota" name="lblAlteraAliquota" class="lblGeneral">AI</p>
        <input type="checkbox" id="chkAlteraAliquota" name="chkAlteraAliquota" class="fldGeneral" datasrc="#dsoSup01" datafld="AlteraImpostos" title="Permite altera��o dos impostos no pedido?">
        <p id="lblAIC" name="lblAIC" class="lblGeneral">AIC</p>
        <input type="checkbox" id="chkAIC" name="chkAIC" class="fldGeneral" datasrc="#dsoSup01" datafld="AlteraImpostosComplementares" title="Permite altera��o dos impostos complementares no pedido?">
        <p id="lblPermiteNF" name="lblPermiteNF" class="lblGeneral">NF</p>
        <select id="selPermiteNF" name="selPermiteNF" class="fldGeneral" datasrc="#dsoSup01" datafld="PermiteNF">
            <%
			Dim strSQL1, rsData1
				Set rsData1 = Server.CreateObject("ADODB.Recordset")
				
				strSQL1 = "SELECT ItemID, ItemMasculino " & _
					"FROM " & _
						"TiposAuxiliares_Itens WITH(NOLOCK) " & _
					"WHERE " & _
						"EstadoID=2 AND " & _
						"TipoID=432 " & _
					"ORDER BY " & _
						"Ordem"
				
				rsData1.Open strSQL1, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

				If (Not (rsData1.BOF AND rsData1.EOF) ) Then
					While Not (rsData1.EOF)
						Response.Write( "<option value='" & rsData1.Fields("ItemID").Value & "'>" )
						Response.Write( rsData1.Fields("ItemMasculino").Value & "</option>" )

						rsData1.MoveNext()
					WEnd
				End If
            %>
        </select>
        <p id="lblOrdem" name="lblOrdem" class="lblGeneral">Ordem</p>
        <input type="text" id="txtOrdem" name="txtOrdem" class="fldGeneral" datasrc="#dsoSup01" datafld="Ordem">
        <p id="lblObservacao_01" name="lblObservacao_01" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao_01" name="txtObservacao_01" class="fldGeneral" datasrc="#dsoSup01" datafld="Observacao">
    </div>

    <!-- Segundo div secundario superior - Transa��o -->
    <div id="divSup02_02" name="divSup02_02" class="divExtern">
        <p id="lblHrTransacao" name="lblHrTransacao" class="lblGeneral">Transa��o___</p>
        <hr id="hrTransacao" name="hrTransacao" class="lblGeneral">
        <p id="lblNivel_02" name="lblNivel_02" class="lblGeneral">N�vel</p>
        <select id="selNivel_02" name="selNivel_02" class="fldGeneral" datasrc="#dsoSup01" datafld="Nivel"></select>
        <p id="lblOperacaoMaeID_02" name="lblOperacaoMaeID_02" class="lblGeneral">Transa��o M�e</p>
        <select id="selOperacaoMaeID_02" name="selOperacaoMaeID_02" class="fldGeneral" datasrc="#dsoSup01" datafld="OperacaoMaeID"></select>
        <p id="lblPaisID2" name="lblPaisID2" class="lblGeneral">Pa�s</p>
        <select id="selPaisID2" name="selPaisID2" class="fldGeneral" datafld="PaisID" datasrc="#dsoSup01"></select>
        <p id="lblMetodoCustoID" name="lblMetodoCustoID" class="lblGeneral">M�todo Custo</p>
        <select id="selMetodoCustoID" name="selMetodoCustoID" class="fldGeneral" datasrc="#dsoSup01" datafld="MetodoCustoID"></select>
        <p id="lblReferenciaCustoID" name="lblReferenciaCustoID" class="lblGeneral">Refer�ncia Custo</p>
        <select id="selReferenciaCustoID" name="selReferenciaCustoID" class="fldGeneral" datasrc="#dsoSup01" datafld="ReferenciaCustoID"></select>
        <p id="lblReferenciaImpostoID" name="lblReferenciaImpostoID" class="lblGeneral">Refer�ncia Imposto</p>
        <select id="selReferenciaImpostoID" name="selReferenciaImpostoID" class="fldGeneral" datasrc="#dsoSup01" datafld="ReferenciaImpostoID"></select>
        <p id="lblReferenciaValorUnitarioID" name="lblReferenciaValorUnitarioID" class="lblGeneral">Refer�ncia Valor Unit�rio</p>
        <select id="selReferenciaValorUnitarioID" name="selReferenciaValorUnitarioID" class="fldGeneral" datasrc="#dsoSup01" datafld="ReferenciaValorUnitarioID"></select>
        <p id="lblHistoricoPadraoID" name="lblHistoricoPadraoID" class="lblGeneral">Hist�rico Padr�o</p>
        <select id="selHistoricoPadraoID" name="selHistoricoPadraoID" class="fldGeneral" datasrc="#dsoSup01" datafld="HistoricoPadraoID"></select>
        <p id="lblHistoricoPadraoValorExcedenteID" name="lblHistoricoPadraoValorExcedenteID" class="lblGeneral">Hist�rico Excedente </p>
        <select id="selHistoricoPadraoValorExcedenteID" name="selHistoricoPadraoValorExcedenteID" class="fldGeneral" datasrc="#dsoSup01" datafld="HistoricoPadraoID" title='Hist�rico Padr�o onde dever� ser contabilizado o valor excedente do retorno em rela��o � remessa.'></select>
        <p id="lblNivelLiberacao" name="lblNivelLiberacao" class="lblGeneral" title="N�vel para libera��o de pedidos">N�vel</p>
        <select id="selNivelLiberacao" name="selNivelLiberacao" class="fldGeneral" datasrc="#dsoSup01" datafld="NivelLiberacao">
            <option value="0"></option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
        </select>
        <p id="lblOrdem2" name="lblOrdem2" class="lblGeneral">Ordem</p>
        <input type="text" id="txtOrdem2" name="txtOrdem2" class="fldGeneral" datasrc="#dsoSup01" datafld="Ordem">
        <p id="lblEntrada" name="lblEntrada" class="lblGeneral">Entr</p>
        <input type="checkbox" id="chkEntrada" name="chkEntrada" class="fldGeneral" datasrc="#dsoSup01" datafld="Entrada" title="� transa��o de Entrada?">
        <p id="lblFornecedor" name="lblFornecedor" class="lblGeneral">Forn</p>
        <input type="checkbox" id="chkFornecedor" name="chkFornecedor" class="fldGeneral" datasrc="#dsoSup01" datafld="Fornecedor" title="� transa��o de fornecedor?">
        <p id="lblCliente" name="lblCliente" class="lblGeneral">Cli</p>
        <input type="checkbox" id="chkCliente" name="chkCliente" class="fldGeneral" datasrc="#dsoSup01" datafld="Cliente" title="� transa��o de cliente?">
        <p id="lblEhRetorno" name="lblEhRetorno" class="lblGeneral">Ret</p>
        <input type="checkbox" id="chkEhRetorno" name="chkEhRetorno" class="fldGeneral" datasrc="#dsoSup01" datafld="EhRetorno" title="� transa��o de retorno?">
        <p id="lblResultado" name="lblResultado" class="lblGeneral">Res</p>
        <input type="checkbox" id="chkResultado" name="chkResultado" class="fldGeneral" datasrc="#dsoSup01" datafld="Resultado" title="Gera Resultado no pedido?">
        <p id="lblInadimplencia" name="lblInadimplencia" class="lblGeneral">Inad</p>
        <input type="checkbox" id="chkInadimplencia" name="chkInadimplencia" class="fldGeneral" datasrc="#dsoSup01" datafld="Inadimplencia" title="Considera esta opera��o para efeito de inadimplencia?">
        <p id="lblRIR" name="lblRIR" class="lblGeneral">RIR</p>
        <input type="checkbox" id="chkRIR" name="chkRIR" class="fldGeneral" datasrc="#dsoSup01" datafld="RIR" title="Gera RIR?">
        <p id="lblAtualizaFinanceiro" name="lblAtualizaFinanceiro" class="lblGeneral">At</p>
        <input type="checkbox" id="chkAtualizaFinanceiro" name="chkAtualizaFinanceiro" class="fldGeneral" datasrc="#dsoSup01" datafld="AtualizaFinanceiro" title="Atualiza Financeiro(juros)?">
        <p id="lblBaixaFinanceiro" name="lblBaixaFinanceiro" class="lblGeneral">Baixa</p>
        <input type="checkbox" id="chkBaixaFinanceiro" name="chkBaixaFinanceiro" class="fldGeneral" datasrc="#dsoSup01" datafld="BaixaFinanceiro" title="Baixa automaticamente o Financeiro?">
        <p id="lblCodigoBarra" name="lblCodigoBarra" class="lblGeneral">CB</p>
        <input type="checkbox" id="chkCodigoBarra" name="chkCodigoBarra" class="fldGeneral" datasrc="#dsoSup01" datafld="CodigoBarra" title="Pede C�digo de Barra no pedido?">
        <p id="lblMatrizFilial" name="lblMatrizFilial" class="lblGeneral">MF</p>
        <input type="checkbox" id="chkMatrizFilial" name="chkMatrizFilial" class="fldGeneral" datasrc="#dsoSup01" datafld="MatrizFilial" title="� transa��o entre matriz e filial?">
        <p id="lblDeposito" name="lblDeposito" class="lblGeneral">Dep�sito</p>
        <input type="checkbox" id="chkDeposito" name="chkDeposito" class="fldGeneral" datasrc="#dsoSup01" datafld="Deposito" title="Esta transa��o permite movimenta��o de estoques entre dep�sitos?">
        <p id="lblAsstec" name="lblAsstec" class="lblGeneral">Asstec</p>
        <input type="checkbox" id="chkAsstec" name="chkAsstec" class="fldGeneral" datasrc="#dsoSup01" datafld="Asstec" title="Transa��o de Assist�ncia T�cnica?">
        <p id="lblPermiteOutroProduto" name="lblPermiteOutroProduto" class="lblGeneral">Prod</p>
        <input type="checkbox" id="chkPermiteOutroProduto" name="chkPermiteOutroProduto" class="fldGeneral" datasrc="#dsoSup01" datafld="PermiteOutroProduto" title="Permite que o produto do item seja diferente do produto do item refer�ncia?">
        <p id="lblExigeVinculacaoAsstec" name="lblExigeVinculacaoAsstec" class="lblGeneral">Vinc Asstec</p>
        <input type="checkbox" id="chkExigeVinculacaoAsstec" name="chkExigeVinculacaoAsstec" class="fldGeneral" datasrc="#dsoSup01" datafld="ExigeVinculacaoAsstec" title="Transa��o exige vincula��o do Asstec no pedido?">
        <p id="lblExtrato" name="lblExtrato" class="lblGeneral">Extrato</p>
        <input type="checkbox" id="chkExtrato" name="chkExtrato" class="fldGeneral" datasrc="#dsoSup01" datafld="Extrato" title="Aparece no Extrato de Estoque do Produto?">
        <p id="lblTomacaoServico" name="lblTomacaoServico" class="lblGeneral">TS</p>
        <input type="checkbox" id="chkTomacaoServico" name="chkTomacaoServico" class="fldGeneral" datasrc="#dsoSup01" datafld="TomacaoServico" title="Toma��o de servi�o?">
         <p id="lblPermitirAnteciparEntrega" name="lblPermitirAnteciparEntrega" class="lblGeneral">PAE</p>
        <input type="checkbox" id="chkPermitirAnteciparEntrega" name="chkPermitirAnteciparEntrega" class="fldGeneral" datasrc="#dsoSup01" datafld="PermitirAnteciparEntrega" title="Permite antecipar entrega?">

        <p id="lblObservacao_02" name="lblObservacao_02" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao_02" name="txtObservacao_02" class="fldGeneral" datasrc="#dsoSup01" datafld="Observacao">
    </div>

    <!-- Terceiro div secundario superior - Baixa -->
    <div id="divSup02_03" name="divSup02_03" class="divExtern">
        <p id="lblHrBaixa" name="lblHrBaixa" class="lblGeneral">Baixa</p>
        <hr id="hrBaixa" name="hrBaixa" class="lblGeneral">
        <p id="lblObservacao_03" name="lblObservacao_03" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao_03" name="txtObservacao_03" class="fldGeneral" datasrc="#dsoSup01" datafld="Observacao">
    </div>

</body>

</html>
