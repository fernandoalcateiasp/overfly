/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form operacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoPropsPL = new CDatatransport('dsoPropsPL');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoListData01 = new CDatatransport('dsoListData01');
/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Opera��o', 'Tipo');

    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
    showBtnsEspecControlBar('sup', true, [1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento']);
    setupEspecBtnsControlBar('sup', 'HHH');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    var contexto = 0;
	var nProcedimentoID = 0;
	var empresaData = getCurrEmpresaData();    
    var nEmpresaID = empresaData[0];
    var strPars = new String();
    var sSQL;

    if (controlBar == 'SUP') 
    {
        if (btnClicked == 1) 
        {
            if (fg.Rows > 1) 
            {
                __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
            }
            else 
            {
                window.top.overflyGen.Alert('Selecione um registro.');
            }
        }
        else if (btnClicked == 2)
            openModalPrint();
        else if (btnClicked == 3) 
        {
            contexto = getCmbCurrDataInControlBar('sup', 1);

            nProcedimentoID = 422; // manual da qualidade
            
            window.top.openModalControleDocumento('PL', '', nProcedimentoID, null, null, 'T');
        }
    }    
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    
    // Modal de impressao
    if (idElement.toUpperCase() == 'MODALPRINTHTML') 
    {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    
    else if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}


/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    return null;
}
