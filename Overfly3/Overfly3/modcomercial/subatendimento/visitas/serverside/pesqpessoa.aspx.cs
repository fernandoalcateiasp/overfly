using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.subatendimento.visitasEx.serverside
{
	public partial class pesqpessoa : System.Web.UI.OverflyPage
	{
		private string strToFind = null;
		
		private string StrToFind {
			get {
				if((strToFind = Request.QueryString["strToFind"]) == null) {
					strToFind = "";
				}
				
				return strToFind;
			}
		}

		private string Operator {
			get {
				if (StrToFind.StartsWith("%") && StrToFind.EndsWith("%")) return " LIKE ";
				else return " >= ";
			}
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"SELECT '' AS fldName, 0 AS fldID, '' AS Fantasia UNION ALL " +
				"SELECT TOP 100 a.Nome AS fldName,a.PessoaID AS fldID,a.Fantasia AS Fantasia " +
                "FROM Pessoas a WITH(NOLOCK) " +
				"WHERE a.EstadoID = 2 AND a.Nome " + Operator + "'" + StrToFind + "' " +
				"ORDER BY fldName "
			));				
		}
	}
}
