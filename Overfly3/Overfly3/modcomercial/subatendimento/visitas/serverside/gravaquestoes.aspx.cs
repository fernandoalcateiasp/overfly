using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcomercial.subatendimento.visitasEx.serverside
{
	public partial class gravaquestoes : System.Web.UI.OverflyPage
	{
		private static Integer[] emptyAInt = new Integer[0];
		private static string[] emptyAStr = new string[0];
		
		private static Integer Zero = new Integer(0);
		
		protected Integer dataInsereAlteraLen = Zero;
		protected Integer dataDeleteLen = Zero;
		protected Integer visitaID = Zero;
		protected Integer[] visQuestaoID = emptyAInt;
		protected Integer[] questaoID = emptyAInt;
		protected Integer[] respostaID = emptyAInt;
		protected Integer[] marcaID = emptyAInt;
		protected Integer[] pessoaID = emptyAInt;
		protected string[] observacao = emptyAStr;
		protected Integer[] DELVisQuestaoID = emptyAInt;
		
		protected int recsAffected;
		protected string resultado;

		protected Integer nDataInsereAlteraLen {    
			set { dataInsereAlteraLen = value != null ? value : Zero; }
		}

		protected Integer nDataDeleteLen {    
			set { dataDeleteLen = value != null ? value : Zero; }
		}

		protected Integer nVisitaID {    
			set { visitaID = value != null ? value : Zero; }
		}

		protected Integer[] nVisQuestaoID {
			set { visQuestaoID = value != null ? value : emptyAInt; }
		}

		protected Integer[] nQuestaoID {
			set { questaoID = value != null ? value : emptyAInt; }
		}

		protected Integer[] nRespostaID {
			set { respostaID = value != null ? value : emptyAInt; }
		}

		protected Integer[] nMarcaID {
			set { marcaID = value != null ? value : emptyAInt; }
		}

		protected Integer[] nPessoaID {
			set { pessoaID = value != null ? value : emptyAInt; }
		}

		protected string[] sObservacao {
			set { observacao = value != null ? value : emptyAStr; }
		}

		protected Integer[] nDELVisQuestaoID {
			set { DELVisQuestaoID = value != null ? value : emptyAInt; }
		}

		protected string SQL {
			get {
				string sql = "";
				
				// Inclusao e Alteracao
				for(int i = 0; i < dataInsereAlteraLen.intValue(); i++) {
					// Inclui
					if(visQuestaoID[i].intValue() == 0) {
						sql += " INSERT INTO Visitas_Questoes (VisitaID, QuestaoID, RespostaID, MarcaID, PessoaID, Observacao) VALUES(" + 
							visitaID + "," +
							questaoID[i] + ", " +
							(respostaID[i] != null ? respostaID[i].ToString() : " NULL ") + ", " +
							(marcaID[i] != null ? marcaID[i].ToString() : " NULL ") + ", " +
							(pessoaID[i] != null ? pessoaID[i].ToString() : " NULL ") + ", " +
							(observacao[i] != null ? observacao[i].ToString() : " NULL ") + ")";
					}
					// Altera
					else {
						sql += " UPDATE Visitas_Questoes SET " +
							"VisitaID=" + visitaID + "," +
							"QuestaoID=" + questaoID[i] + "," +
							"RespostaID=" + (respostaID[i] != null ? respostaID[i].ToString() : " NULL ") + "," +
							"MarcaID=" + (marcaID[i] != null ? marcaID[i].ToString() : " NULL ") + "," +
							"PessoaID=" + (pessoaID[i] != null ? pessoaID[i].ToString() : " NULL ") + "," +
							"Observacao=" + (observacao[i] != null ? observacao[i].ToString() : " NULL ") + " " +
							"WHERE VisQuestaoID=" + visQuestaoID[i];
					}
				}

				// Deleta
				for(int d = 0; d < dataDeleteLen.intValue(); d++) {
					sql += " DELETE Visitas_Questoes " +
						"WHERE VisQuestaoID=" + DELVisQuestaoID[d];
				}

				return sql;
			}
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.ExecuteSQLCommand(SQL);

			WriteResultXML(DataInterfaceObj.getRemoteData(
				"select '' as Resultado"));
		}
	}
}
