
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalquestoesHtml" name="modalquestoesHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/subatendimento/visitas/modalpages/modalquestoes.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/subatendimento/visitas/modalpages/modalquestoes.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nEmpresaID, nUserID

sCaller = ""
nEmpresaID = 0
nUserID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"

Response.Write "var glb_nUserID = " & CStr(nUserID) & ";"

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalquestoes_ValidateEdit(fg, fg.Row, fg.Col)
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalquestoes_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalquestoesKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalquestoes_BeforeEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modalquestoesDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalquestoesBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Atualizar linha de mensagens
js_fg_AfterRowColmodalquestoes (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
//Atencao programador:
//Setimo parametro abaixo = true, permite ao usuario mudar de linha em grid
//clicando em celula read only.
js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>

<script ID="vbFunctions" LANGUAGE="vbscript">
<!--

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

//-->
</script>

</head>

<body id="modalquestoesBody" name="modalquestoesBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <!-- Div Controles -->
    <div id="divControls" name="divControls" class="divGeneral">
        <p id="lblQuestaoID" name="lblQuestaoID" class="lblGeneral">Quest�o</p>
        <select id="selQuestaoID" name="selQuestaoID" class="fldGeneral">
<%
    Dim rsData, strSQL
	Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName " & _
        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
        "WHERE TipoID = 39 " & _
        "ORDER BY fldName"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    Response.Write( "<option value=0></option>" )

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write("<option value='" & rsData.Fields("fldID").Value & "'>")
			Response.Write(rsData.Fields("fldName").Value & "</option>")

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
	Set rsData = Nothing
%>
        </select>
        <p id="lblRespostaID" name="lblRespostaID" class="lblGeneral">Resposta</p>
        <select id="selRespostaID" name="selRespostaID" class="fldGeneral"></select>
        <p id="lblMarcaID" name="lblMarcaID" class="lblGeneral">Marca</p>
        <select id="selMarcaID" name="selMarcaID" class="fldGeneral">
<%
	Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT ConceitoID AS fldID, Conceito AS fldName " & _
	    "FROM Conceitos WITH(NOLOCK) " & _
	    "WHERE (TipoConceitoID = 304) AND (EstadoID = 2) " & _
	    "ORDER BY Conceito"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    Response.Write( "<option value=0></option>" )

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write("<option value='" & rsData.Fields("fldID").Value & "'>")
			Response.Write(rsData.Fields("fldName").Value & "</option>")

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
	Set rsData = Nothing
%>
        </select>
        <p id="lblPessoa" name="lblPessoa" class="lblGeneral">Pessoa</p>
        <input type="text" id="txtPessoa" name="txtPessoa" class="fldGeneral"></input>
        <input type="button" id="btnFindPessoa" name="btnFindPessoa" value="P" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">Pessoa</p>
        <select id="selPessoaID" name="selPessoaID" class="fldGeneral"></select>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" class="fldGeneral"></input>
    </div>    

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
            
	<input type="button" id="btnIncluir" name="btnIncluir" value="Incluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
	<input type="button" id="btnExcluir" name="btnExcluir" value="Excluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnOK" name="btnOK" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
         
</body>

</html>
