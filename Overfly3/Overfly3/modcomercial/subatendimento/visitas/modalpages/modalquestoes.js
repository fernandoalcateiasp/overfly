/********************************************************************
modalquestoes.js

Library javascript para o modalquestoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_aDeletedRecords = null;

// Dados do grid .RDS
var dsoGrid = new CDatatransport("dsoGrid");
// dso da minuta
var dsoMinuta = new CDatatransport("dsoMinuta");
// Gravacao de dados do grid .RDS
var dsoGrava = new CDatatransport("dsoGrava");
var dsoCombo = new CDatatransport("dsoCombo");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalquestoesBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalquestoesDblClick(grid, Row, Col)
js_modalquestoesKeyPress(KeyAscii)
js_modalquestoes_ValidateEdit()
js_modalquestoes_BeforeEdit(grid, row, col)
js_modalquestoes_AfterEdit(Row, Col)
js_fg_AfterRowColmodalquestoes (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES


/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalquestoesBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalquestoesDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    // trap so colunas editaveis
    if ( getColIndexByColKey(grid, 'OK') != Col)
        return true;

    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);
    
    glb_PassedOneInDblClick = true;
    
    // limpa coluna se tem um check box checado
    for ( i=2; i<grid.Rows; i++ )
    {
        if ( grid.ValueMatrix(i, Col) != 0 )
        {
            bFill = false;
            break;
        }
    }
    
    for ( i=2; i<grid.Rows; i++ )
    {
        if ( bFill )
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    totalLine();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalquestoesKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalquestoes_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalquestoes_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalquestoes_AfterEdit(Row, Col)
{
    btnOK.disabled = false;
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalquestoes(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return true;
}

// FINAL DE EVENTOS DE GRID *****************************************

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalquestoesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	showExtFrame(window, true);
		
	fillGridData();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // desabilita o botao OK
    btnOK.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblQuestaoID','selQuestaoID',32,1,-10,-10],
						  ['lblRespostaID','selRespostaID',32,1],
						  ['lblMarcaID','selMarcaID',20,1],
						  ['lblPessoa','txtPessoa',18,2,-10],
						  ['btnFindPessoa','btn',24,2],
						  ['lblPessoaID','selPessoaID',23,2],
						  ['lblObservacao','txtObservacao',21,2]], null, null, true);

	selQuestaoID.onchange = selQuestaoID_onchange;
	selPessoaID.onchange = selPessoaID_onchange;
	selMarcaID.onchange = selMarcaID_onchange;
	txtPessoa.onkeypress = txtPessoa_onKeyPress;
	txtPessoa.maxLength = 20;
	txtObservacao.maxLength = 30;
	selRespostaID.disabled = true;
	selPessoaID.disabled = true;
	adjustLabelsCombos();

    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
			  btnIncluir.offsetTop + btnIncluir.offsetHeight + 2;
        width = selPessoaID.offsetLeft + selPessoaID.offsetWidth + ELEM_GAP;    
        height = selPessoaID.offsetTop + selPessoaID.offsetHeight;    
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + selPessoaID.offsetTop;
		style.width = parseInt(btnOK.currentStyle.width, 10) - 18;
		style.left = modWidth - parseInt(btnOK.currentStyle.width) - 16;
    }
    
    with (btnExcluir)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		style.top = btnOK.currentStyle.top;
		style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnOK.currentStyle.width, 10) - 4;
    }

    with (btnIncluir)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		style.top = btnOK.currentStyle.top;
		style.left = parseInt(btnExcluir.currentStyle.left, 10) - parseInt(btnExcluir.currentStyle.width, 10) - 4;
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

function selPessoaID_onchange()
{
    adjustLabelsCombos();
}

function selMarcaID_onchange()
{
    adjustLabelsCombos();
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtPessoa_onKeyPress()
{
    if (event.keyCode == 13)
		fillCmbPessoa();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnIncluir')
    {
		incluir();
    }
    else if (controlID == 'btnExcluir')
    {
		excluir();
    }
    else if (controlID == 'btnFindPessoa')
    {
		fillCmbPessoa();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Solicitar dados do combo ao servidor
********************************************************************/
function fillCmbPessoa()
{
    clearComboEx(['selPessoaID']);
    
    selPessoaID.disabled = true;
    
    if (trimStr(txtPessoa.value) == '')
        return null;

    lockControlsInModalWin(true);
	var strSQL = '';

    // parametrizacao do dso dsoCombo
    setConnection(dsoCombo);

	strSQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
	    'UNION ALL ' +
	    'SELECT a.PessoaID AS fldID, a.Fantasia AS fldName ' +
        'FROM Pessoas a WITH(NOLOCK) ' +
        'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (a.ClassificacaoID = b.ItemID) ' +
        'WHERE (a.EstadoID = 2) AND (b.Filtro LIKE ' + '\'' + '%<2104>%' + '\'' + ') ' + ' AND ' +
            '(a.Fantasia LIKE ' + '\'' + trimStr(txtPessoa.value) + '%' + '\'' + ') ' +
        'ORDER BY fldName';

	dsoCombo.SQL = strSQL;

    dsoCombo.ondatasetcomplete = fillCmbPessoa_DSC;
    dsoCombo.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do combo
********************************************************************/
function fillCmbPessoa_DSC() {
    while (!dsoCombo.recordset.EOF)
    {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoCombo.recordset['fldName'].value;
        oOption.value = dsoCombo.recordset['fldID'].value;

        selPessoaID.add(oOption);
        dsoCombo.recordset.MoveNext();
    }

    adjustLabelsCombos();
    lockControlsInModalWin(false);
    
    selPessoaID.disabled = (selPessoaID.options.length <= 0);
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    btnOK.disabled = true;
	lockControlsInModalWin(true);
	var nVisitaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['VisitaID'].value");
    // zera o grid
    fg.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	dsoGrid.SQL = 'SELECT a.VisQuestaoID, ISNULL(a.QuestaoID, 0) AS QuestaoID, ISNULL(a.RespostaID, 0) AS RespostaID, ' +
	    'ISNULL(a.MarcaID, 0) AS MarcaID, ISNULL(a.PessoaID, 0) AS PessoaID, a.Observacao, b.ItemMasculino AS Questao, ' +
	    'c.ItemMasculino AS Resposta, d.Conceito AS Marca, e.Fantasia AS Pessoa ' +
        'FROM Visitas_Questoes a WITH(NOLOCK) ' +
	        'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.QuestaoID=b.ItemID ' +
	        'LEFT OUTER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON a.RespostaID=c.ItemID ' +
	        'LEFT OUTER JOIN Conceitos d WITH(NOLOCK) ON a.MarcaID=d.ConceitoID ' +
	        'LEFT OUTER JOIN Pessoas e WITH(NOLOCK) ON a.PessoaID=e.PessoaID ' +
	    'WHERE a.VisitaID=' + nVisitaID + ' ' +
        'ORDER BY b.Ordem';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
	dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i, lastRow, sColuna1, sColuna2, sDado1, sDado2;
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '10';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

    headerGrid(fg,['Quest�o',
				   'Resposta', 
				   'Marca', 				   
				   'Pessoa',
				   'Observa��o', 
				   'QuestaoID', 
				   'RespostaID', 
				   'MarcaID', 
				   'PessoaID',
				   'VisQuestaoID'], [5,6,7,8,9]);

    fillGridMask(fg,dsoGrid,['Questao*',
				             'Resposta*',
				             'Marca*',
				             'Pessoa*',
				             'Observacao',
				             'QuestaoID',
				             'RespostaID',
				             'MarcaID',
				             'PessoaID',
				             'VisQuestaoID'],
							 ['','','','','','','','','',''],
							 ['','','','','','','','','','']);

    fg.Redraw = 0;
    
    //alignColsInGrid(fg,[4,8,10,11,12]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
	paintCellsSpecialyReadOnly();
		    
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
		fg.Editable = true;
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.Redraw = 2;
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
    var nVisitaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['VisitaID'].value");
    
	strPars = '?nVisitaID=' + escape(nVisitaID);
    strPars += '&nDataInsereAlteraLen=' + escape(fg.Rows-1);
    
	for (i=1; i<fg.Rows; i++)
	{
	    strPars += '&nVisQuestaoID=' + escape(getCellValueByColKey(fg, 'VisQuestaoID', i));
	    
	    sQuestaoID = getCellValueByColKey(fg, 'QuestaoID', i);
	    sQuestaoID = (sQuestaoID == '0' ? 'NULL' : sQuestaoID);
	    strPars += '&nQuestaoID=' + escape(sQuestaoID);
	    
	    sRespostaID = getCellValueByColKey(fg, 'RespostaID', i);
	    sRespostaID = (sRespostaID == '0' ? 'NULL' : sRespostaID);
	    strPars += '&nRespostaID=' + escape(sRespostaID);

	    sMarcaID = getCellValueByColKey(fg, 'MarcaID', i);
	    sMarcaID = (sMarcaID == '0' ? 'NULL' : sMarcaID);
	    strPars += '&nMarcaID=' + escape(sMarcaID);

	    sPessoaID = getCellValueByColKey(fg, 'PessoaID', i);
	    sPessoaID = (sPessoaID == '0' ? 'NULL' : sPessoaID);
	    strPars += '&nPessoaID=' + escape(sPessoaID);
	    
	    sOBS = getCellValueByColKey(fg, 'Observacao', i);
	    sOBS = (sOBS == '' ? 'NULL' : '\'' + sOBS + '\'');
	    strPars += '&sObservacao=' + escape(sOBS);
	}
	
	if (glb_aDeletedRecords != null)
	{
	    strPars += '&nDataDeleteLen=' + escape(glb_aDeletedRecords.length);

	    for (i=0; i<glb_aDeletedRecords.length; i++)
	        strPars += '&nDELVisQuestaoID=' + escape(glb_aDeletedRecords[i]);
	}
    else
        strPars += '&nDataDeleteLen=' + escape(0);
	
	lockControlsInModalWin(true);
	dsoGrava.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/visitas/serverside/gravaquestoes.aspx' + strPars;
	dsoGrava.ondatasetcomplete = saveDataInGrid_DSC;
	dsoGrava.refresh();		
}

function selQuestaoID_onchange()
{
	adjustLabelsCombos();
	
	fillCmbResposta();
}

/********************************************************************
Solicitar dados do combo ao servidor
********************************************************************/
function fillCmbResposta()
{
    clearComboEx(['selRespostaID']);
    
    selRespostaID.disabled = true;
    
    if (selQuestaoID.value == 0)
        return null;

    lockControlsInModalWin(true);
	var strSQL = '';

    // parametrizacao do dso dsoCombo
    setConnection(dsoCombo);

	strSQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem ' +
		'UNION ALL ' +
		'SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName, a.Ordem ' +
		'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
		'WHERE (a.TipoID = 40 AND a.Filtro Like ' + '\'' + '%<' + selQuestaoID.value + '>%' + '\'' + ') ' +
		'ORDER BY Ordem';

	dsoCombo.SQL = strSQL;

    dsoCombo.ondatasetcomplete = fillCmbResposta_DSC;
    dsoCombo.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do combo
********************************************************************/
function fillCmbResposta_DSC(
    while ( !dsoCombo.recordset.EOF )
    {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoCombo.recordset['fldName'].value;
        oOption.value = dsoCombo.recordset['fldID'].value;
        
        selRespostaID.add(oOption);
        dsoCombo.recordset.MoveNext();
    }
    
    adjustLabelsCombos();
    lockControlsInModalWin(false);
    
    selRespostaID.disabled = (selRespostaID.options.length <= 1);
}

function adjustLabelsCombos()
{
    setLabelOfControl(lblMarcaID, selMarcaID.value);
    setLabelOfControl(lblPessoaID, selPessoaID.value);
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
	var sResultado = dsoGrava.recordset['Resultado'].value;
	
	if (sResultado != '')
    {
        if ( window.top.overflyGen.Alert(sResultado) == 0 )
            return null;
    
        lockControlsInModalWin(false);    
        return null;
    }    
    
    fillGridData();	
}

function incluir()
{
    var nFieldsFilled = 0;

    if (selQuestaoID.value == 0)
    {
		if (window.top.overflyGen.Alert('Selecione uma quest�o') == 0)
		    return null;
    
        return null;
    }

    if (selRespostaID.selectedIndex > 0)
        nFieldsFilled++;

    if (selMarcaID.selectedIndex > 0)
        nFieldsFilled++;

    if (selPessoaID.selectedIndex > 0)
        nFieldsFilled++;
        
    if (trimStr(txtObservacao.value) != '')
        nFieldsFilled++;
        
    if (nFieldsFilled == 0)
    {
		if (window.top.overflyGen.Alert('Preencha mais um campo') == 0)
		    return null;
    
        return null;
    }

	fg.Rows = fg.Rows + 1;
	fg.TopRow = fg.Rows - 1;
	fg.Row = fg.Rows - 1;
    
    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'VisQuestaoID')) = 0;
    
    if (selQuestaoID.selectedIndex > 0)
    {
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Questao*')) = selQuestaoID.options[selQuestaoID.selectedIndex].innerText;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'QuestaoID')) = selQuestaoID.options[selQuestaoID.selectedIndex].value;
    }
    else
    {
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Questao*')) = '';
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'QuestaoID')) = 0;
    }
    
    if (selRespostaID.selectedIndex > 0)
    {
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Resposta*')) = selRespostaID.options[selRespostaID.selectedIndex].innerText;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RespostaID')) = selRespostaID.options[selRespostaID.selectedIndex].value;
    }
    else
    {
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Resposta*')) = '';
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RespostaID')) = 0;
    }
    
    if (selMarcaID.selectedIndex > 0)
    {
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Marca*')) = selMarcaID.options[selMarcaID.selectedIndex].innerText;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'MarcaID')) = selMarcaID.options[selMarcaID.selectedIndex].value;
    }
    else
    {
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Marca*')) = '';
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'MarcaID')) = 0;
    }

    if (selPessoaID.selectedIndex > 0)
    {
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Pessoa*')) = selPessoaID.options[selPessoaID.selectedIndex].innerText;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID')) = selPessoaID.options[selPessoaID.selectedIndex].value;
    }
    else
    {
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Pessoa*')) = '';
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID')) = 0;
    }
    
    if (trimStr(txtObservacao.value) != '')
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Observacao')) = trimStr(txtObservacao.value);

    selRespostaID.selectedIndex = 0;
    selMarcaID.selectedIndex = 0;
    
    if (selPessoaID.options.length > 0)
        selPessoaID.selectedIndex = 0;

    txtObservacao.value = '';
    
    fg.Redraw = 0;
    paintCellsSpecialyReadOnly();
    fg.Redraw = 2;
    btnOK.disabled = false;
}

function excluir()
{
    var delID = '0';
    
    if (fg.Row <= 0)
        return null;

    delID = getCellValueByColKey(fg, 'VisQuestaoID', fg.Row);

    if (delID != '0')
    {
        _retMsg = window.top.overflyGen.Confirm('Deseja realmente excluir este registro?');

        if ((_retMsg == 0) || (_retMsg == 2))
            return null;
    
        if (glb_aDeletedRecords == null)
            glb_aDeletedRecords = new Array(delID);
        else
            glb_aDeletedRecords[glb_aDeletedRecords.length] = delID;
    }
    
	var oldRow = fg.Row;
	fg.RemoveItem(fg.Row);
	
	if (fg.Rows > oldRow)
		fg.Row = oldRow;
	else if (fg.Rows > 1)
		fg.Row = fg.Rows - 1;
		
    btnOK.disabled = false;
}
