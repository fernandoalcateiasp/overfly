<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    Dim rsData, strSQL
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID name do arquivo -->
<html id="listaprecospesqlistHtml" name="listaprecospesqlistHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_pesqlist.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/subatendimento/listaprecos/pesqlist.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/subatendimento/listaprecos/pesqlistsuperior.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/subatendimento/listaprecos/LCotacao.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/subatendimento/listaprecos/CCotacao.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<script LANGUAGE="javascript" FOR="txtArgumento" EVENT="onkeydown">
<!--
    txtArgumento_onkeydown();
//-->

</script>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!-- 

 fg_KeyPressPesqList(arguments[0]);
 
 fg_KeyPress(arguments[0]);
    
//-->
</SCRIPT>

<script LANGUAGE="javascript" FOR="fg" EVENT="DblClick">
<!--
fg_DblClick_Especific();
//-->
</script>

<script LANGUAGE="javascript" FOR="fg" EVENT="BeforeSort">
<!--
fg_BeforeSort();
//-->
</script>

<script LANGUAGE="javascript" FOR="fg" EVENT="AfterSort">
<!--
fg_AfterSort();
//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
    js_fg_BeforeRowColChange(fg, arguments[0],arguments[1],arguments[2],arguments[3],arguments[4], true);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
	js_fg_EnterCell(fg);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
    js_fg_AfterRowColChangePesqList(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    js_fg_AfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    treatCheckBoxReadOnly2(dsoListData01, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
    js_PesqList_AfterEdit(dsoListData01, fg, fg.Row, fg.Col);
    js_PesqList_AfterEditListaPreco(fg.Row, fg.Col);
//-->
</SCRIPT>

</head>

<!-- //@@ ID e name do body  -->
<body id="listaprecospesqlistBody" name="listaprecospesqlistBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div dos campos da pesquisa  -->    
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Check box de refresh do inferior -->
        <p id="lblRefrInf" name="lblRefrInf" class="lblGeneral" LANGUAGE=javascript onclick="return invertChkBox(chkRefrInf)" title="Refresh">Ref</p>
        <input type="checkbox" id="chkRefrInf" name="chkRefrInf" class="fldGeneral" title="Refresh"></input>

        <!-- Check box de ordem -->
        <p id="lblOrdem" name="lblOrdem" class="lblGeneral" LANGUAGE=javascript onclick="return invertChkBox(chkOrdem)" title="Inverter ordem?">Inv</p>
        <input type="checkbox" id="chkOrdem" name="chkOrdem" class="fldGeneral" title="Inverter ordem?"></input>

        <!-- Check box de Registros Vencidos -->
        <p id="lblRegistrosVencidos" name="lblRegistrosVencidos" class="lblGeneral" LANGUAGE=javascript title="Registros vencidos?">Venc</p>
        <input type="checkbox" id="chkRegistrosVencidos" name="chkRegistrosVencidos" class="fldGeneral" title="Registros vencidos?"></input>    

        <!-- Check box de Lote 
		<p id="lblLote" name="lblLote" class="lblGeneral" onclick="return chkLote_onclick()" title=" Lote ?">Lote</p>
		<input type="checkbox" id="chkLote" name="chkLote" class="fldGeneral" title="Incluir produtos com Lote ?">
        -->

        <!-- label de fonte -->
		<p id="lblFonteID" name="lblFonteID" class="lblGeneral" title=" Fonte">Fonte</p>
		<select id="selFonteID" name="selFonteID" class="fldGeneral">
            <option value=1>Estoque</option>
            <option value=2>Lote</option>
            <option value=3>Cotador</option>
        </select>

        <!-- Select dos proprietarios dos registros -->
        <p id="lblProprietariosPL" name="lblProprietariosPL" class="lblGeneral" LANGUAGE=javascript>Propriet�rio</p>
        <select id="selProprietariosPL" name="selProprietariosPL" class="fldGeneral"></select>

        <!-- Qtd reg/pag -->
        <p id="lblRegistros" name="lblRegistros" class="lblGeneral">Registros</p>
        <select id="selRegistros" name="selRegistros" class="fldGeneral">
            <option value=10>10</option>
            <option value=20>20</option>
            <!-- <option value=200>200</option> -->
            <!-- <option value=300>300</option> -->
        </select>
                
        <!-- Combo chave de pesquisa -->
        <p id="lblPesquisa" name="lblPesquisa" class="lblGeneral">Chave de Pesquisa</p>
        <select id="selPesquisa" name="selPesquisa" class="fldGeneral">
            <!-- //@@ IDs, names, values e texto conforme o form-->
            <option id="VsGenerico" name="VsGenerico" value="bConceitos">Gen�rico</option>
            <option id="VsPartNumber" name="VsPartNumber" value="aCorpProdutos">PartNumber</option>
        </select>

        <!-- Argumento da pesquisa -->

        <p id="lblArgumento" name="lblArgumento" class="lblGeneral">Argumento</p>
        
        <input type="text" id="txtArgumento" name="txtArgumento" class="fldGeneral" LANGUAGE="javascript"></input>
        <div id="acArgumento" class="acGeneral"></div>
                
        <!-- Filtro de refinamento da pesquisa -->
        <p id="lblFiltro" name="lblFiltro" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro" name="txtFiltro" class="fldGeneral"></input>
        
        <!-- Chave para pesquisa Com pessoa ou Sem pessoa -->
        <p id="lblChavePessoaID" name="lblChavePessoaID" class="lblGeneral">Tipo de cota��o</p>
        <select id="selChavePessoaID" name="selChavePessoaID" class="fldGeneral">
            <option value="1">Com pessoa</option>
			<option value="2">Sem pessoa</option>
        </select>
        
        <!-- Campos especificos para esta listagem -->
        <p id="lblParceiroID" name="lblParceiroID" class="lblGeneral">Parceiro</p>
        <select id="selParceiroID" name="selParceiroID" class="fldGeneral"></select>

		<input type="button" id="btnFindCliente" name="btnFindCliente" value="P"  title="Selecionar parceiro" LANGUAGE="javascript" class="btns">		

      	<p id="lblFiltroPessoa" name="lblFiltroPessoa" class="lblGeneral"></p>
        <input type="text" id="txtFiltroPessoa" name="txtFiltroPessoa" class="fldGeneral" onkeyup="filtraPessoa(this.value, selPessoaID);" maxlength="14">
 		<p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">Pessoa</p>
        <select id="selPessoaID" name="selPessoaID" class="fldGeneral"></select>
	

		<p id="lblClasseID" name="lblClasseID" class="lblGeneral">Cla</p>
		<select id="selClassificacoesClaID" name="selClassificacoesClaID" class="fldGeneral"></select>
		
	    <p id="lblFaturamentoDiretoID" name="lblFaturamentoDiretoID" class="lblGeneral">FD</p>
		<input type="checkbox" id="chkFaturamentoDireto" name="chkFaturamentoDireto" class="fldGeneral" title="Faturamento direto?">
		
		<p id="lblTipoPessoa" name="lblTipoPessoa" class="lblGeneral">Fis/Jur</p>
		<select id="selTipoPessoa" name="selTipoPessoa" class="fldGeneral" title="Pessoa F�sica ou jur�dica?"></select>
		
	    <p id="lblCon" name="lblCon" class="lblGeneral">Con</p>
		<input type="checkbox" id="chkContribuinte" name="chkContribuinte" class="fldGeneral" title="Contribuinte?">
		
		<p id="lblSimplesNacionalID" name="lblSimplesNacionalID" class="lblGeneral">SN</p>
		<input type="checkbox" id="chkSimplesNacional" name="chkSimplesNacional" class="fldGeneral" title="Optante do Simples Nacional?">
		
		<p id="lblSuframaID" name="lblSuframaID" class="lblGeneral">Su</p>
		<input type="checkbox" id="chkSuframaID" name="chkSuframaID" class="fldGeneral" title="Tem Suframa?">

		<p id="lblUFID" name="lblUFID" class="lblGeneral">UF</p>
		<select id="selUFID" name="selUFID" class="fldGeneral"></select>
		
		<p id="lblCidadeId" name="lblCidadeId" class="lblGeneral">Cidade</p>
		<select id="selCidadeId" name="selCidadeId" class="fldGeneral"></select>

		<!--
        <p id="lblFrete" name="lblFrete" class="lblGeneral">Frete</p>
		<select id="selFrete" name="selFrete" class="fldGeneral"></select>
        -->

		<p id="lblFinanciamentoID" name="lblFinanciamentoID" class="lblGeneral">Financiamento</p>
		<select id="selFinanciamentoID" name="selFinanciamentoID" class="fldGeneral"></select>
		<p id="lblQuantidade" name="lblQuantidade" class="lblGeneral">Quant</p>
		<input type="text" id="txtQuantidade" name="txtQuantidade" class="fldGeneral">
		<p id="lblMoedaConversaoID" name="lblMoedaConversaoID" class="lblGeneral">Moeda</p> 
		<select id="selMoedaConversaoID" name="selMoedaConversaoID" class="fldGeneral" title="Moeda do Pre�o Convertido"></select>
		<p id="lblEstoque" name="lblEstoque" class="lblGeneral">Est</p>
		<input type="checkbox" id="chkEstoque" name="chkEstoque" class="fldGeneral" title="Somente produtos com estoque dispon�vel?"></input>
		<p id="lblFamiliaID" name="lblFamiliaID" class="lblGeneral">Fam�lia</p> 
		<select id="selFamiliaID" name="selFamiliaID" class="fldGeneral" title="Fam�lia de Produtos"></select>
		<p id="lblMarcaID" name="lblMarcaID" class="lblGeneral">Marca</p> 
		<select id="selMarcaID" name="selMarcaID" class="fldGeneral" title="Marca espec�fica"></select>
		<p id="lblLinhaProdutoID" name="lblLinhaProdutoID" class="lblGeneral">Linha de Produto</p> 
		<select id="selLinhaProdutoID" name="selLinhaProdutoID" class="fldGeneral" title="Linha de Produtos"></select>
		<p id="lblClassificacoesID" name="lblClassificacoesID" class="lblGeneral">Classifica��es</p>
		<select id="selClassificacoesID" name="selClassificacoesID" class="fldGeneral"></select>
		<p id="lblCnae" name="lblCnae" class="lblGeneral">CNAE</p>
		<input type="text" id="txtCnae" name="txtCnae" class="fldGeneral" maxLength="7">
	    <p id="lblIsencoesID" name="lblIsencoesID" class="lblGeneral">Isen��es</p>
        <select id="selIsencoesID" name="selIsencoesID" class="fldGeneral" MULTIPLE></select>
        <p id="lblTransacao" name="lblTransacao" class="lblGeneral">Transa��o</p>
        <select id="selTransacao" name="selTransacao" class="fldGeneral"></select>
        <p id="lblFinalidade" name="lblFinalidade" class="lblGeneral">Finalidade</p>
        <select id="selFinalidade" name="" class="fldGeneral"></select>
        <p id="lblProdutoReferenciaID" nameselFinalidade="lblProdutoReferenciaID" class="lblGeneral">Produto Refer�ncia</p>
        <select id="selProdutoReferenciaID" name="selProdutoReferenciaID" class="fldGeneral"></select>
		<input type="button" id="btnFindProduto" name="btnFindProduto" value="P"  title="Selecionar produto" LANGUAGE="javascript" class="btns">
        <p id="lblTipoRelacaoID" name="lblTipoRelacaoID" class="lblGeneral">Tipo Rela��o</p>
        <select id="selTipoRelacaoID" name="selTipoRelacaoID" class="fldGeneral">
<%
    'Dim rsData, strSQL
    
    Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT a.TipoRelacaoID AS fldID, a.TipoRelacao AS fldName, a.Ordem AS Ordem " & _
        "FROM TiposRelacoes a WITH(NOLOCK) " & _
        "WHERE (a.EstadoID = 2 AND a.RelacaoEntreID = 134 AND a.Filtro LIKE '%<LP>%') " & _
        "ORDER BY Ordem "

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    Response.Write( "<option value=0></option>" )

	If (Not (rsData.BOF AND rsData.EOF) ) Then
		While Not (rsData.EOF)

			Response.Write("<option value='" & rsData.Fields("fldID").Value & "'>")
			Response.Write(rsData.Fields("fldName").Value & "</option>")
			rsData.MoveNext()
		Wend
	End If

	rsData.Close
	Set rsData = Nothing
%>
        </select>
        <input type="button" id="btnListar" name="btnListar" value="L"  title="Listar produtos relacionados" LANGUAGE="javascript" class="btns">
    </div>
        
    <!-- Div do grid de listagem -->    
    <div id="divSup01_02" name="divSup01_02" class="divExtern" LANGUAGE=javascript>
        <!-- Grid de listagem -->
        <OBJECT CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT></OBJECT>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    

</body>

</html>
