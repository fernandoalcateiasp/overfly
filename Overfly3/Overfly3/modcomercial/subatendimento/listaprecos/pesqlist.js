/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form listaprecos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// controla se a modal vem do servidor ou esta carregada escondida
var glb_bWantPurchase = false;
var glb_FirstLoad = true;
var glb_nHorGap = 126;
var glb_ReallocGrid = true;
var glb_aDadosPessoa = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
var glb_TimerListaPrecoVar = null;
var glb_TimerListaPrecoVar2 = null;
var glb_TimerPessoaOnChange = null;
var glb_TimerModalClienteRetorno = null;
var glb_TimerPesqList2 = null;
var glb_TimerTempodecorrido = null;
var dsoPessoa = null;
var glb_callFromDet = false;
var glb_currMode = 'PL';
var glb_bAtualizaMargem = true;
var glb_bAtualizaDesconto = false;
var glb_nA1 = 0;
var glb_nA2 = 0;
var glb_nListagemEspecifica = false;
var glb_DsoToReturn = null;
var glb_nValue = 0;
var glb_nRow;
var glb_RowEdit = 0;
var glb_IniciarFinalizar = 0;
var glb_bEhCliente = true;
var glb_nParceiroID = null;
var glb_bCarregaModoFinalizar = false;
var glb_nEmaClienteID = null;
var glb_nObservacoes = null;
var glb_bCarregaProdutosOferta = false;
var glb_nSacolaID = 0;
var glb_nCotadorPessoaID = null;
var glb_bCotadorCompra = false;

// Controle do atendimento do cliente.
var glb_StatusAtendimento = 0;
var glb_nPesAtendimentoID = null;
var glb_sClipBoardBuffer = '';
var glb_dtAtendimentoInicio = null;
var glb_AtendimentoTimer = null;
var glb_dtAtendimentoFim = null;
var glb_nTipoAtendimentoID = 0;
var glb_aDadosSelecionados = new Array();
var glb_aCotacao = null;
var glb_bPaste = false;
var glb_quoteBOM = null;
var glb_EnableChkOK = true;
var glb_bTemInconsistencia = false;
var glb_bCargaCompletaAtualizada = true;
var glb_nSelFonteID_lastIndex = 1;
var glb_bCotadorPadrao = false;

// Controle autocomplete
var glb_currentFocus = -1;
var glb_AutoCompleteTimer = null;
var glb_autocompleteControle = false;

// Dados da listagem da pesquisa
var dsoListData01 = new CDatatransport('dsoListData01');

var dsoMoeda = new CDatatransport('dsoMoeda');

// Dados dos combos de contexto e filtros
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');

// Dados dos proprietarios para o pesqlist .URL
var dsoPropsPL = new CDatatransport('dsoPropsPL');
var dsoCamposExtras = new CDatatransport('dsoCamposExtras');
var dsoUFCidades = new CDatatransport('dsoUFCidades');
var dsoIsencoes = new CDatatransport('dsoIsencoes');
var dsoCla = new CDatatransport('dsoCla');
//Filtros de Familia/Marca/Linha
var dsoFamilia = new CDatatransport('dsoFamilia');
var dsoMarca = new CDatatransport('dsoMarca');
var dsoLinha = new CDatatransport('dsoLinha');
var dsoProposta = new CDatatransport('dsoProposta');

// Dados dos combos CFOP .RDS
var dsoCFOP = new CDatatransport('dsoCFOP');

var dsoFinalidade = new CDatatransport('dsoFinalidade');

var dsoGravarAgendamento = new CDatatransport('dsoGravarAgendamento');

var dsoCargo = new CDatatransport('dsoCargo');

var dsoArgumento = new CDatatransport('dsoArgumento');

var dsoParceiros = new CDatatransport('dsoParceiros');

var dsoICMSST = new CDatatransport('dsoICMSST');

var dsoSelecionaSacola = new CDatatransport("dsoSelecionaSacola");

var dsoAtualizaGridCotador = new CDatatransport("dsoAtualizaGridCotador");

var dsoInconsistencia = new CDatatransport("dsoInconsistencia");

var dsoCargaCotador = new CDatatransport("dsoCargaCotador");

var dsoBuscaPessoaCotador = new CDatatransport("dsoBuscaPessoaCotador");

var dsoTipoSacola = new CDatatransport("dsoTipoSacola");

var dsoSalvaSacola = new CDatatransport("dsoSalvaSacola");

var dsoSacolaAtual = new CDatatransport("dsoSacolaAtual");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
readjustInterface()
pesqlistIsVisibleAndUnlocked()
btnBarClicked(controlBar, btnClicked)
btnBarNotEspecClicked(controlBar, btnClicked)
modalInformForm(idElement, param1, param2)
specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload() {
    //@@ a pagina tem elementos nao padroes
    glb_pesqListNotPadron = true;

    var nUserID = getCurrUserID();
    var empresaData = getCurrEmpresaData();
    var nEmpresa = empresaData[0];

    txtArgumento.onpaste = txtArgumento_onpaste;

    glb_DsoToReturn = 1;

    setConnection(dsoCargo);

    dsoCargo.SQL = 'SELECT TOP 1 a.CargoID AS CargoID ' +
        'FROM RelacoesPessoas a WITH(NOLOCK) ' +
        'WHERE (a.TipoRelacaoID = 31) AND (a.EstadoID = 2) AND (a.SujeitoID = ' + nUserID + ')' +
        'ORDER BY RelacaoID';

    dsoCargo.ondatasetcomplete = selectParceiros_DSC;
    dsoCargo.refresh();

    glb_nA1 = getCurrRightValue('SUP', 'A1');
    glb_nA2 = getCurrRightValue('SUP', 'A2');
}

// Ajusta autocomplete abaixo de txtArgumento
function adjustAutocomplete() {
    var elemInput = window.document.getElementById('txtArgumento');
    var elemTitle = window.document.getElementById('acArgumento');
    with (elemTitle.style) {
        position = 'absolute';
        zIndex = 9999;
        width = elemInput.style.width;
        top = (elemInput.style.pixelTop + 24) + 'px';
        left = elemInput.style.left;
        fontFamily = 'Tahoma,Arial,MS Sans-Serif';
        fontSize = '10pt';
        overflow = 'auto';
        height = 'auto';
    }

    // Eventos referente ao autocomplete
    txtArgumento.onkeydown = txtArgumento_onkeydown;
    txtArgumento.onmousemove = txtArgumento_onmousemove;
    document.body.onclick = body_onclick;
}

function txtArgumento_onkeydown() {
    //Evento de sele��o da c�lula selecionada ao pressionar o bot�o enter
    if (event.keyCode == 13 && acArgumento.innerHTML != '' && glb_currentFocus != -1) {
        var x = acArgumento.getElementsByTagName("div");
        if (glb_currentFocus > -1 && glb_currentFocus < x.length) {
            x[glb_currentFocus].click();
            acArgumento.innerHTML = '';
            acArgumento.style.backgroundColor = '';
            glb_currentFocus = -1;
        }
    }
    // Evento listar caso j� tenha algo selecionado
    else if (event.keyCode == 13 && (glb_currentFocus == -1)) {
        __btn_LIST('sup');
        acArgumento.innerHTML = '';
        acArgumento.style.backgroundColor = '';
    }
}

function txtArgumento_onmousemove() {
    // Previne que autocomplete continue na tela ao clicar do bot�o "X" que acompanha o label no Win10
    if (txtArgumento.value == '') {
        acArgumento.innerHTML = '';
        acArgumento.style.backgroundColor = '';
    }
}

function body_onclick() {
    // Previne que autocomplete continue na tela ao clicar fora do label
    acArgumento.innerHTML = '';
    acArgumento.style.backgroundColor = '';
    glb_currentFocus = -1;
}

function txtArgumento_onkeyup() {
    if (event.keyCode == 13)
        return;

    // Permite movimenta��o de sele��o entre as c�lulas.
    if ((event.keyCode == 40 || event.keyCode == 38) && acArgumento.innerHTML != '') {
        if (event.keyCode == 40)
            changeFocus(1);

        if (event.keyCode == 38)
            changeFocus(-1);

        return;
    }

    if (glb_AutoCompleteTimer == null && glb_autocompleteControle == false) {
        glb_AutoCompleteTimer = window.setInterval('fn_loadAutoCompleteData()', 400, 'JavaScript');
    }
}

function fn_loadAutoCompleteData() {
    glb_autocompleteControle = true;
    if (glb_AutoCompleteTimer != null) {
        window.clearInterval(glb_AutoCompleteTimer);
        glb_AutoCompleteTimer = null;
    }

    setConnection(dsoArgumento);

    var sArgumento = txtArgumento.value;

    if (sArgumento.indexOf('\'') >= 0) {
        sArgumento = sArgumento.replace('\'', '\' + CHAR(39) + \'');
    }

    var strSQL = "SELECT DISTINCT TOP 7 " +
        "a.Produto " +
        "FROM CorpProdutos a WITH(NOLOCK) " +
        "WHERE (a.Produto LIKE '%" + sArgumento + "%') " +
        "AND ((LEN(a.NCMID) > 0) OR (a.TipoProdutoID IN (1133,1134))) AND (a.EstadoID IN (2) OR a.ProdutoSubstituto IS NOT NULL)";

    if (selMarcaID.value != 0) {
        strSQL += "AND (a.MarcaID = " + selMarcaID.value + ") ";
    }

    strSQL += "ORDER BY a.Produto";

    dsoArgumento.SQL = strSQL;
    dsoArgumento.ondatasetcomplete = autocompleteArgumento_DSC;

    dsoArgumento.refresh();

}

function autocompleteArgumento_DSC() {
    // Esrceve informa��es retornadas por dso em div de autocomplete
    glb_autocompleteControle = false;
    var nItens = 0;
    if (txtArgumento.value != '') {
        acArgumento.innerHTML = '';
        acArgumento.style.backgroundColor = '';
        for (var i = 0; i < dsoArgumento.recordset.aRecords.length; i++) {
            if (dsoArgumento.recordset.aRecords[i][0].indexOf(txtArgumento.value.toUpperCase()) >= 0) {
                nItens++;
                acArgumento.innerHTML += '<div onclick="pasteArgument(\'' + dsoArgumento.recordset.aRecords[i][0] + '\');" style="cursor:pointer">' +
                    dsoArgumento.recordset.aRecords[i][0] +
                    '</div>';
                acArgumento.style.backgroundColor = '#fff';
            }
        }

        acArgumento.style.height = (16.5 * (nItens)) + 'px';
    }

    else {
        acArgumento.innerHTML = '';
        acArgumento.style.backgroundColor = '';
    }
}

// Fun��o para movimenta��o entre c�lulas de autocomplete
function changeFocus(key) {
    var x = acArgumento.getElementsByTagName("div");
    if (glb_currentFocus + key == -1) {
        x[glb_currentFocus].style.backgroundColor = "White";
        x[glb_currentFocus].style.fontColor = "Black";
        glb_currentFocus = -1;
        return;
    }
    else if (glb_currentFocus + key == x.length) {
        x[glb_currentFocus].style.backgroundColor = "White";
        x[glb_currentFocus].style.fontColor = "Black";
        glb_currentFocus = -1;
        return;
    }

    if (glb_currentFocus != -1 && glb_currentFocus != x.length) {
        x[glb_currentFocus].style.backgroundColor = "White";
        x[glb_currentFocus].style.fontColor = "Black";
    }

    if ((glb_currentFocus + key > -1) && (glb_currentFocus + key < x.length)) {
        glb_currentFocus = glb_currentFocus + key;
        x[glb_currentFocus].style.backgroundColor = "DodgerBlue";
        x[glb_currentFocus].style.fontColor = "White";
    }
}

// Fun��o que cola a informa��o do autocomplete no txtArgumento
function pasteArgument(value) {
    txtArgumento.value = value;
    acArgumento.innerHTML = '';
    acArgumento.style.backgroundColor = '';
}


function txtArgumento_onpaste() {
    var strCotacao = window.clipboardData.getData('Text');
    glb_aCotacao = null;

    // Modo cotador
    if (selFonteID.value == 3) {

        if (selMarcaID.value == 0) {
            if (window.top.overflyGen.Alert('Selecione uma marca') == 1)
                return false;
            else
                return false;
        }
        else {
            if (trimStr(strCotacao).length > 120) {
                txtArgumento.value = '';

                glb_aCotacao = LCotacao(strCotacao);

                if (glb_aCotacao != null) {
                    setConnection(dsoMoeda);
                    dsoMoeda.SQL = "SELECT SimboloMoeda, ConceitoID FROM Conceitos WITH(NOLOCK) WHERE TipoConceitoID = 308 AND SimboloMoeda LIKE '" + glb_aCotacao[glb_aCotacao.length - 1] + "'";
                    dsoMoeda.ondatasetcomplete = dsoMoeda_DSC;
                    dsoMoeda.refresh();
                }

                event.returnValue = false;
            }
        }
    }
}

function dsoMoeda_DSC() {
    if (dsoMoeda.recordset.aRecords.length > 0) {

        glb_aCotacao[glb_aCotacao.length - 1] = dsoMoeda.recordset.aRecords[0][1];

        // Insere os par�metros do cotador no body da Request
        try {
            var strCotador = cotadorData();

            if (strCotador != null)
                dsoListData01.parameters = strCotador;
        }
        catch (e) {
            ;
        }

        if (glb_aCotacao.length > 0) {
            __btn_LIST('sup');
            /*if (aCotacao[0].tipo == 3)
                alert('Estimate BOM de ' + (aCotacao.length - 1) + ' linhas');
            else if (aCotacao[0].tipo == 4)
                alert('Quote DID de ' + (aCotacao.length - 1) + ' linhas');*/
        }
        txtQuantidade.disabled = true;
    }
    else {
        alert('N�o foi poss�vel encontrar a moeda, por favor coloque uma moeda v�lida.');
    }
}

function selectParceiros_DSC() {
    var nUserID = getCurrUserID();
    var empresaData = getCurrEmpresaData();
    var nEmpresa = empresaData[0];
    var strSQL = '';
    var nCargoID = dsoCargo.recordset['CargoID'].value;

    setConnection(dsoParceiros);

    strSQL = 'SELECT SPACE(0) AS fldName,	0 AS fldID, NULL AS Fantasia, NULL AS UFID, NULL AS ListaPreco, ' +
        'NULL AS NumeroParcelas, NULL AS FinanciamentoID, NULL AS EmpresaSistema, NULL AS PMP, NULL AS FinanciamentoPadrao, ' +
        'NULL AS ClassificacaoInterna, NULL AS ClassificacaoExterna ';

    //Somente para vendedores - alterado devido a ativa��o das Pesoas e rela��es solicitadas pelo MKT. BJBN
    //if ((nCargoID == 651) || (nCargoID == 563) || (nCargoID == 759) || (nCargoID == 611)) {
    strSQL += 'UNION ALL ' +
        'SELECT DISTINCT TOP 300 Pessoas.Nome AS fldName, Pessoas.PessoaID AS fldID, Pessoas.Fantasia + ISNULL(SPACE(1) + \'(\' + dbo.fn_Pessoa_Documento(RelPessoas.SujeitoID, NULL, NULL, 101, 111, 0), SPACE(0)) + \')\' AS Fantasia, ' +
        'Enderecos.UFID, RelPessoas.ListaPreco AS ListaPreco, c.NumeroParcelas AS NumeroParcelas, ' +
        'c.FinanciamentoID, dbo.fn_Empresa_Sistema(RelPessoas.SujeitoID) AS EmpresaSistema, ' +
        'dbo.fn_Financiamento_PMP(c.FinanciamentoID,NULL,NULL,NULL) AS PMP, ' +
        'dbo.fn_RelacoesPessoas_FinanciamentoPadrao(RelPessoas.SujeitoID, RelPessoas.ObjetoID, 1) AS FinanciamentoPadrao, ' +
        'ISNULL(dbo.fn_RelPessoa_Classificacao(RelPessoas.ObjetoID, RelPessoas.SujeitoID, 0, GETDATE(),1), SPACE(0)) AS ClassificacaoInterna, ' +
        'ISNULL(dbo.fn_RelPessoa_Classificacao(RelPessoas.ObjetoID, RelPessoas.SujeitoID, 1, GETDATE(),1), SPACE(0)) AS ClassificacaoExterna ' +
        'FROM RelacoesPessoas RelPessoas WITH(NOLOCK) ' +
        'INNER JOIN Pessoas WITH(NOLOCK) ON (RelPessoas.SujeitoID = Pessoas.PessoaID) ' +
        'INNER JOIN Pessoas_Creditos PesCredito WITH(NOLOCK) ON (PesCredito.PessoaID = Pessoas.PessoaID) ' +
        'INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pessoas.PessoaID = Enderecos.PessoaID) ' +
        'INNER JOIN RelacoesPesRec Empresas WITH(NOLOCK) ON (Empresas.SujeitoID = RelPessoas.ObjetoID) ' +
        'INNER JOIN FinanciamentosPadrao c WITH(NOLOCK) ON (ISNULL(PesCredito.FinanciamentoLimiteID, 2) = c.FinanciamentoID) ' +
        'INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON ((d.ItemID = Pessoas.ClassificacaoID) AND (d.TipoID = 13)) ' +
        'WHERE RelPessoas.ObjetoID = ' + nEmpresa + ' ' +
        'AND RelPessoas.TipoRelacaoID = 21 ' +
        'AND RelPessoas.EstadoID = 2 ' +
        'AND Pessoas.EstadoID = 2 ' +
        'AND PesCredito.PaisID = ' + empresaData[1] + ' ' +
        'AND Enderecos.Ordem = 1 ' +
        'AND Empresas.TipoRelacaoID = 12 ' +
        'AND Empresas.ObjetoID = 999 ' +
        'AND Empresas.EstadoID = 2 ' +
        'AND RelPessoas.ProprietarioID = ' + nUserID + ' ' +
        //'AND d.Filtro LIKE \'%<CLI>%\' ' + 
        'ORDER BY Fantasia';
    //}

    dsoParceiros.SQL = strSQL;
    dsoParceiros.ondatasetcomplete = window_onload_DSC;
    dsoParceiros.refresh();
}

function window_onload_DSC() {
    glb_DsoToReturn--;

    if (glb_DsoToReturn > 0)
        return null;

    adjustOptionsNumRegsInComboPesqList();

    windowOnLoad_1stPart();

    disableTotalLineInGrid(true);

    //@@ Formatacao das colunas do grid de pesquisa, definicao opcional
    // padrao do framework = null se variavel nao for definida aqui
    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    //Cotador
    if (selFonteID.value == 3) {
        //@@ Ordem e titulos das colunas do grid de pesquisa
        glb_COLPESQORDER = new Array('LineNumber', 'ID', 'Produto', 'Substituto', 'OK', 'Empresa', 'Origem', 'Pre�o', 'Quant', 'MC', 'Desconto', 'NCM', 'Guia', 'ICMS-ST', 'Vig�ncia', 'Pre�o Fob Desc', 'Pre�o Fob', 'FIS',
            'FIE', 'Disp', 'Previs�o Entrega', 'TipoProdutoID', 'Linha Produto', 'CFOP', 'Ver Tamb�m', 'Inconsist�ncia', 'EmpresaID', 'PrecoDigitado', 'MargemDigitada', 'ICMSST_Unitario', 'CorpProdutoID',
            'PartNumber', 'MoedaID', 'Moeda', 'OrigemID');

        glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', '', '###,###,###,###.00', '####', '###.00', '###.00', '', '', '###.00', '', '###,###,###,###.00', '###,###,###,###.00', '###.00', '###.00', '', '',
            '', '', '', '', '', '', '', '', '', '', '', '', '', '');
    }
    else {
        glb_COLPESQORDER = new Array('ID', 'Produto', 'OK', 'Empresa', 'Lote', 'E', 'Pre�o', 'Quant', 'MC', 'Imp', 'Guia', 'ICMS-ST', 'Disp', 'Disp 1', 'Disp 2', 'RV',
            'Equipe', 'QCx', 'Previs�o Entrega', 'Linha', '�ltima compra', 'Observa��o', 'CFOP', 'NCM', 'PrecoDigitado', 'MargemDigitada',
            'MargemMinima', 'ICMSST_Unitario', 'Pre�o 0%', 'DadosProd', 'TemEmailMKT', 'DiasPagamento', 'EmpresaID', 'IDTMP');

        glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', '###,###,###,###.00', '####', '###.00', '###.00', '', '###,###,###,###.00', '', '', '', '', '', '', '',
            '', '', '', '', '', '###,###,###,###.00', '###.00', '###.00', '###,###,###,###.00', '###,###,###,###.00', '', '', '', '','');
    }

    selPesquisa.selectedIndex = 0;
    if (selFonteID.value != 3)
        selPesquisa.disabled = true;

    selMetodoPesq.selectedIndex = 3;

    glb_nA1 = getCurrRightValue('SUP', 'A1');

    glb_nA2 = getCurrRightValue('SUP', 'A2');

    windowOnLoad_2ndPart();

    if (selRegistros.selectedIndex < 0) {
        selRegistros.selectedIndex = 0;
    }

    fillCmbParceiro();

    selLinhaProdutoID.disabled = true;

    preencheFamilia();

    //verificaCargaCotador();

    set_glb_IniciarFinalizarStatus(0);
    txtArgumento.focus();
}

/********************************************************************
Funcao disparada pelo frame work.
Esta pagina tem elementos nao padroes, ajustar toda a interface
a partir desta funcao.
Esta funcao nao esta definida em todos os pesqlist do projeto pois so e
disparada se existir a variavel glb_pesqListNotPadron na funcao abaixo:
function window_onload()
{
//@@ a pagina tem elementos nao padroes
glb_pesqListNotPadron = true;
    
windowOnLoad_1stPart();
    
... continua o codigo ...
}
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function readjustInterface() {
    return null;
}

/********************************************************************
Funcao do programador
Redimensiona e move o grid para baixo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function reallocGrid() {
    fg.style.height = fg.offsetHeight - glb_nHorGap;
    divSup01_02.style.height = divSup01_02.offsetHeight - glb_nHorGap;
    divSup01_02.style.top = divSup01_02.offsetTop + glb_nHorGap;
}

/********************************************************************
Funcao do programador
Redimensiona o div de controles
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function reallocDivSup01() {
    divSup01_01.style.height = divSup01_01.offsetHeight + glb_nHorGap;

    divSup01_01.style.backgroundColor = 'transparent';
}

var aEmpresa = getCurrEmpresaData();

function adjustEspecificFields() {
    lockUnlockSecondLine();
    setupFieldsSecondLine();

    selChavePessoaID.onchange = selChavePessoaID_ElementsInForm;
    selChavePessoaID_ElementsInForm();
    //Presto
    selTipoPessoa.onchange = selTipoPessoa_OnChange;
    selTipoPessoa_OnChange();

    selFamiliaID.onchange = selFamilia_onchange;

    selMarcaID.onchange = selMarca_onchange;

    selLinhaProdutoID.onchange = selLinha_onchange;

    selFonteID.onchange = selFonteID_onchange;

    chkSimplesNacional.onclick = chkDadosPessoas_onchange;

    selCidadeId.onchange = chkDadosPessoas_onchange;

    chkSuframaID.onclick = chkDadosPessoas_onchange;

    selPesquisa.onchange = selPesquisa_onchange;

    // Ajusta campos padr�es removendo campos que perderam a fun��o com pessquisa atrav�s da (sp_Produto_Busca)
    lblOrdem.style.visibility = 'hidden';
    chkOrdem.style.visibility = 'hidden';
    lblRegistrosVencidos.style.visibility = 'hidden';
    chkRegistrosVencidos.style.visibility = 'hidden';
    lblMetodoPesq.style.visibility = 'hidden';
    selMetodoPesq.style.visibility = 'hidden';
    //lblPesquisa.style.visibility = 'hidden';
    //selPesquisa.style.visibility = 'hidden';

    adjustElementsInForm([['lblFonteID', 'selFonteID', 15, 1],
    //['lblLote', 'chkLote', 3, 1, -9],
    //['lblOrdem', 'chkOrdem', 3, 1, -9],
    ['lblProprietariosPL', 'selProprietariosPL', 15, 1],
    ['btnGetProps', 'btn', 20, 1],
    ['lblRegistros', 'selRegistros', 6, 1, -3],
    //['lblMetodoPesq', 'selMetodoPesq', 9, 1, -9],
    ['lblPesquisa', 'selPesquisa', 14, 1, -4],
    ['lblArgumento', 'txtArgumento', 21, 1, -4],
    ['lblFiltro', 'txtFiltro', 44, 1, -4]]);

    lblRefrInf.style.visibility = 'hidden';
    chkRefrInf.style.visibility = 'hidden';

    adjustAutocomplete();
}

function selChavePessoaID_ElementsInForm() {
    var bCotador = (selFonteID.value == 3 ? true : false);

    if ((selChavePessoaID.value == 1) && (!bCotador)) {
        adjustElementsInForm([
            ['lblChavePessoaID', 'selChavePessoaID', 12, 1, 0, 40],
            ['lblParceiroID', 'selParceiroID', 33, 1, -5],
            ['btnFindCliente', 'btn', 20, 1, -19],
            ['lblFiltroPessoa', 'txtFiltroPessoa', 14, 1, -5],
            ['lblPessoaID', 'selPessoaID', 33, 1, -10],
            // ['lblFrete', 'selFrete', 10, 2, 0],
            ['lblFinanciamentoID', 'selFinanciamentoID', 17, 2],
            ['lblQuantidade', 'txtQuantidade', 4, 2],
            ['lblMoedaConversaoID', 'selMoedaConversaoID', 8, 2],
            ['lblEstoque', 'chkEstoque', 3, 2],
            ['lblFamiliaID', 'selFamiliaID', 15, 2, -1],
            ['lblMarcaID', 'selMarcaID', 15, 2],
            ['lblLinhaProdutoID', 'selLinhaProdutoID', 15, 2],
            ['lblTransacao', 'selTransacao', 15, 3],
            ['lblFinalidade', 'selFinalidade', 30, 3],
            ['lblProdutoReferenciaID', 'selProdutoReferenciaID', 25, 3],
            ['btnFindProduto', 'btn', 21, 3],
            ['lblTipoRelacaoID', 'selTipoRelacaoID', 17, 3],
            ['btnListar', 'btn', 21, 3]], null, null, true);

        lockUnlockSecondLine();
        setupFieldsSecondLine();

        /*
        if (selParceiroID.selectedIndex < 1)
            selFrete.disabled = true;
            */

        //Chamada da Fun��o de controle Cota��o Pessoa / Sem Pessoa
        ChavePessoaCotacao(1);
        lblPessoaID.style.visibility = 'inherit';
        selPessoaID.style.visibility = 'inherit';
        lblFiltroPessoa.style.visibility = 'inherit';
        txtFiltroPessoa.style.visibility = 'inherit';
        lblUFID.style.visibility = 'hidden';
        selUFID.style.visibility = 'hidden';
        lblFaturamentoDiretoID.style.visibility = 'hidden';
        chkFaturamentoDireto.style.visibility = 'hidden';
        lblTipoPessoa.style.visibility = 'hidden';
        selTipoPessoa.style.visibility = 'hidden';
        lblCon.style.visibility = 'hidden';
        chkContribuinte.style.visibility = 'hidden';
        lblSimplesNacionalID.style.visibility = 'hidden';
        chkSimplesNacional.style.visibility = 'hidden';
        lblSuframaID.style.visibility = 'hidden';
        chkSuframaID.style.visibility = 'hidden';
        lblCidadeId.style.visibility = 'hidden';
        selCidadeId.style.visibility = 'hidden';
        lblClassificacoesID.style.visibility = 'hidden';
        selClassificacoesID.style.visibility = 'hidden';
        lblIsencoesID.style.visibility = 'hidden';
        selIsencoesID.style.visibility = 'hidden';
        selClassificacoesClaID.style.visibility = 'hidden';
        lblClasseID.style.visibility = 'hidden';
        lblCnae.style.visibility = 'hidden';
        txtCnae.style.visibility = 'hidden';

    }
    else if ((selChavePessoaID.value == 1) && (bCotador)) {
        adjustElementsInForm([
            ['lblChavePessoaID', 'selChavePessoaID', 12, 1, 0, 40],
            ['lblParceiroID', 'selParceiroID', 33, 1],
            ['lblPessoaID', 'selPessoaID', 33, 1],
            ['lblFinanciamentoID', 'selFinanciamentoID', 17, 2],
            ['lblQuantidade', 'txtQuantidade', 4, 2],
            ['lblMarcaID', 'selMarcaID', 15, 2],
            ['lblLinhaProdutoID', 'selLinhaProdutoID', 15, 2],
            ['lblTransacao', 'selTransacao', 15, 3],
            ['lblFinalidade', 'selFinalidade', 30, 3],
            ['lblProdutoReferenciaID', 'selProdutoReferenciaID', 25, 3],
            ['btnFindProduto', 'btn', 21, 3],
            ['lblTipoRelacaoID', 'selTipoRelacaoID', 17, 3],
            ['btnListar', 'btn', 21, 3]], null, null, true);

        lockUnlockSecondLine();
        setupFieldsSecondLine();

        //Chamada da Fun��o de controle Cota��o Pessoa / Sem Pessoa
        ChavePessoaCotacao(1);
        lblPessoaID.style.visibility = 'inherit';
        selPessoaID.style.visibility = 'inherit';
        lblFiltroPessoa.style.visibility = 'hidden';
        txtFiltroPessoa.style.visibility = 'hidden';
        lblUFID.style.visibility = 'hidden';
        selUFID.style.visibility = 'hidden';
        lblFaturamentoDiretoID.style.visibility = 'hidden';
        chkFaturamentoDireto.style.visibility = 'hidden';
        lblTipoPessoa.style.visibility = 'hidden';
        selTipoPessoa.style.visibility = 'hidden';
        lblCon.style.visibility = 'hidden';
        chkContribuinte.style.visibility = 'hidden';
        lblSimplesNacionalID.style.visibility = 'hidden';
        chkSimplesNacional.style.visibility = 'hidden';
        lblSuframaID.style.visibility = 'hidden';
        chkSuframaID.style.visibility = 'hidden';
        lblCidadeId.style.visibility = 'hidden';
        selCidadeId.style.visibility = 'hidden';
        lblClassificacoesID.style.visibility = 'hidden';
        selClassificacoesID.style.visibility = 'hidden';
        lblIsencoesID.style.visibility = 'hidden';
        selIsencoesID.style.visibility = 'hidden';
        selClassificacoesClaID.style.visibility = 'hidden';
        lblClasseID.style.visibility = 'hidden';
        lblCnae.style.visibility = 'hidden';
        txtCnae.style.visibility = 'hidden';
        lblFamiliaID.style.visibility = 'hidden';
        selFamiliaID.style.visibility = 'hidden';
        lblMoedaConversaoID.style.visibility = 'hidden';
        selMoedaConversaoID.style.visibility = 'hidden';
        lblFiltro.style.visibility = 'hidden';
        txtFiltro.style.visibility = 'hidden';
        lblProdutoReferenciaID.style.visibility = 'hidden';
        selProdutoReferenciaID.style.visibility = 'hidden';
        lblTipoRelacaoID.style.visibility = 'hidden';
        selTipoRelacaoID.style.visibility = 'hidden';
        btnFindProduto.style.visibility = 'hidden';
        btnListar.style.visibility = 'hidden';
        chkEstoque.style.visibility = 'hidden';
        lblEstoque.style.visibility = 'hidden';
    }
    else if ((selChavePessoaID.value == 2) && (aEmpresa[1] == 130) && (!bCotador)) {
        adjustElementsInForm([
            ['lblChavePessoaID', 'selChavePessoaID', 12, 1, 0, 40],
            ['lblParceiroID', 'selParceiroID', 26, 1, -7],
            ['btnFindCliente', 'btn', 20, 1],
            ['lblClasseID', 'selClassificacoesClaID', 4.3, 1, -8],
            ['lblTipoPessoa', 'selTipoPessoa', 6, 1, -7],
            ['lblFaturamentoDiretoID', 'chkFaturamentoDireto', 3, 1, -6],
            ['lblCon', 'chkContribuinte', 3, 1, -12],
            ['lblSimplesNacionalID', 'chkSimplesNacional', 3, 1, -11],
            ['lblSuframaID', 'chkSuframaID', 3, 1, -11],
            ['lblUFID', 'selUFID', 6, 1, -14],
            ['lblCidadeId', 'selCidadeId', 20, 1, -8],
            ['lblClassificacoesID', 'selClassificacoesID', 16, 1, -8],
            ['lblCnae', 'txtCnae', 7, 1, -7],
            ['lblIsencoesID', 'selIsencoesID', 9, 1, -7],
            //['lblFrete', 'selFrete', 10, 2, 0],
            ['lblFinanciamentoID', 'selFinanciamentoID', 17, 2],
            ['lblQuantidade', 'txtQuantidade', 4, 2],
            ['lblMoedaConversaoID', 'selMoedaConversaoID', 8, 2],
            ['lblEstoque', 'chkEstoque', 3, 2],
            ['lblFamiliaID', 'selFamiliaID', 15, 2, -1],
            ['lblMarcaID', 'selMarcaID', 15, 2],
            ['lblLinhaProdutoID', 'selLinhaProdutoID', 15, 2],
            ['lblTransacao', 'selTransacao', 15, 3],
            ['lblFinalidade', 'selFinalidade', 30, 3],
            ['lblProdutoReferenciaID', 'selProdutoReferenciaID', 25, 3],
            ['btnFindProduto', 'btn', 21, 3],
            ['lblTipoRelacaoID', 'selTipoRelacaoID', 17, 3],
            ['btnListar', 'btn', 21, 3]], null, null, true);

        selIsencoesID.style.height = 70;

        lockUnlockSecondLine();
        setupFieldsSecondLine();

        selLinhaProdutoID.disabled = true;
        selCidadeId.disabled = true;
        //Chamada da Fun��o de controle Cota��o Pessoa / Sem Pessoa
        ChavePessoaCotacao(1);
        //selPessoaSemPessoa_FinalidadeTransacao();

        selTipoPessoa_Comportamento();

        lblPessoaID.style.visibility = 'hidden';
        selPessoaID.style.visibility = 'hidden';
        lblFiltroPessoa.style.visibility = 'hidden';
        txtFiltroPessoa.style.visibility = 'hidden';
        lblFaturamentoDiretoID.style.visibility = 'inherit';
        chkFaturamentoDireto.style.visibility = 'inherit';
        lblTipoPessoa.style.visibility = 'inherit';
        selTipoPessoa.style.visibility = 'inherit';
        lblCon.style.visibility = 'inherit';
        chkContribuinte.style.visibility = 'inherit';
        lblSimplesNacionalID.style.visibility = 'inherit';
        chkSimplesNacional.style.visibility = 'inherit';
        lblSuframaID.style.visibility = 'inherit';
        chkSuframaID.style.visibility = 'inherit';
        lblUFID.style.visibility = 'inherit';
        selUFID.style.visibility = 'inherit';
        lblCidadeId.style.visibility = 'inherit';
        selCidadeId.style.visibility = 'inherit';
        lblClassificacoesID.style.visibility = 'inherit';
        selClassificacoesID.style.visibility = 'inherit';
        lblIsencoesID.style.visibility = 'inherit';
        selIsencoesID.style.visibility = 'inherit';
        selClassificacoesClaID.style.visibility = 'inherit';
        lblClasseID.style.visibility = 'inherit';
        lblCnae.style.visibility = 'inherit';
        txtCnae.style.visibility = 'inherit';

    }
    else if ((selChavePessoaID.value == 2) && (aEmpresa[1] == 130) && (bCotador)) {
        adjustElementsInForm([
            ['lblChavePessoaID', 'selChavePessoaID', 12, 1, 0, 40],
            ['lblParceiroID', 'selParceiroID', 26, 1],
            ['lblTipoPessoa', 'selTipoPessoa', 6, 1],
            ['lblCon', 'chkContribuinte', 3, 1],
            ['lblSimplesNacionalID', 'chkSimplesNacional', 3, 1, -9],
            ['lblUFID', 'selUFID', 6, 1, -9],
            ['lblCidadeId', 'selCidadeId', 20, 1],
            ['lblFinanciamentoID', 'selFinanciamentoID', 17, 2],
            ['lblQuantidade', 'txtQuantidade', 4, 2],
            ['lblMarcaID', 'selMarcaID', 15, 2],
            ['lblLinhaProdutoID', 'selLinhaProdutoID', 15, 2],
            ['lblTransacao', 'selTransacao', 15, 3],
            ['lblFinalidade', 'selFinalidade', 30, 3],
            ['lblProdutoReferenciaID', 'selProdutoReferenciaID', 25, 3],
            ['btnFindProduto', 'btn', 21, 3],
            ['lblTipoRelacaoID', 'selTipoRelacaoID', 17, 3],
            ['btnListar', 'btn', 21, 3]], null, null, true);

        selIsencoesID.style.height = 70;

        lockUnlockSecondLine();
        setupFieldsSecondLine();

        selLinhaProdutoID.disabled = true;
        selCidadeId.disabled = true;
        //Chamada da Fun��o de controle Cota��o Pessoa / Sem Pessoa
        ChavePessoaCotacao(1);
        //selPessoaSemPessoa_FinalidadeTransacao();

        selTipoPessoa_Comportamento();

        lblPessoaID.style.visibility = 'hidden';
        selPessoaID.style.visibility = 'hidden';
        lblFiltroPessoa.style.visibility = 'hidden';
        txtFiltroPessoa.style.visibility = 'hidden';
        lblFaturamentoDiretoID.style.visibility = 'hidden';
        chkFaturamentoDireto.style.visibility = 'hidden';
        lblTipoPessoa.style.visibility = 'inherit';
        selTipoPessoa.style.visibility = 'inherit';
        lblCon.style.visibility = 'inherit';
        chkContribuinte.style.visibility = 'inherit';
        lblSimplesNacionalID.style.visibility = 'inherit';
        chkSimplesNacional.style.visibility = 'inherit';
        lblSuframaID.style.visibility = 'hidden';
        chkSuframaID.style.visibility = 'hidden';
        lblUFID.style.visibility = 'inherit';
        selUFID.style.visibility = 'inherit';
        lblCidadeId.style.visibility = 'inherit';
        selCidadeId.style.visibility = 'inherit';
        lblClassificacoesID.style.visibility = 'hidden';
        selClassificacoesID.style.visibility = 'hidden';
        lblIsencoesID.style.visibility = 'hidden';
        selIsencoesID.style.visibility = 'hidden';
        selClassificacoesClaID.style.visibility = 'hidden';
        lblClasseID.style.visibility = 'hidden';
        lblCnae.style.visibility = 'hidden';
        txtCnae.style.visibility = 'hidden';

        selTipoPessoa.value = 52;
        selTipoPessoa.disabled = true;

        selUFID.selectedIndex = -1;
    }
    else if (aEmpresa[1] == 167) {
        adjustElementsInForm([
            ['lblChavePessoaID', 'selChavePessoaID', 12, 1, 0, 40],
            ['lblParceiroID', 'selParceiroID', 26, 1, -5],
            ['btnFindCliente', 'btn', 20, 1],
            ['lblClasseID', 'selClassificacoesClaID', 5, 1, -5],
            ['lblTipoPessoa', 'selTipoPessoa', 6, 1],
            ['lblFaturamentoDiretoID', 'chkFaturamentoDireto', 3, 1],
            ['lblCon', 'chkContribuinte', 3, 1, -5],
            ['lblUFID', 'selUFID', 6, 1, -5],
            ['lblCidadeId', 'selCidadeId', 21, 1, -5],
            ['lblClassificacoesID', 'selClassificacoesID', 16, 1, -5],
            ['lblIsencoesID', 'selIsencoesID', 10, 1, -5],
            //['lblFrete', 'selFrete', 10, 2, 0],
            ['lblFinanciamentoID', 'selFinanciamentoID', 17, 2],
            ['lblQuantidade', 'txtQuantidade', 4, 2],
            ['lblMoedaConversaoID', 'selMoedaConversaoID', 8, 2],
            ['lblEstoque', 'chkEstoque', 3, 2],
            ['lblFamiliaID', 'selFamiliaID', 15, 2, -1],
            ['lblMarcaID', 'selMarcaID', 15, 2],
            ['lblLinhaProdutoID', 'selLinhaProdutoID', 15, 2],
            ['lblTransacao', 'selTransacao', 15, 3],
            ['lblFinalidade', 'selFinalidade', 30, 3],
            ['lblProdutoReferenciaID', 'selProdutoReferenciaID', 25, 3],
            ['btnFindProduto', 'btn', 21, 3],
            ['lblTipoRelacaoID', 'selTipoRelacaoID', 17, 3],
            ['btnListar', 'btn', 21, 3]], null, null, true);

        selIsencoesID.style.height = 70;

        //selFrete.disabled = false;
        //selFrete.selectedIndex = 0;

        ChavePessoaCotacao(1);

        lockUnlockSecondLine();
        setupFieldsSecondLine();

        selTipoPessoa_Comportamento();

        lblPessoaID.style.visibility = 'hidden';
        selPessoaID.style.visibility = 'hidden';
        lblFiltroPessoa.style.visibility = 'hidden';
        txtFiltroPessoa.style.visibility = 'hidden';
        lblFaturamentoDiretoID.style.visibility = 'inherit';
        chkFaturamentoDireto.style.visibility = 'inherit';
        lblTipoPessoa.style.visibility = 'inherit';
        selTipoPessoa.style.visibility = 'inherit';
        lblCon.style.visibility = 'inherit';
        chkContribuinte.style.visibility = 'inherit';
        lblSimplesNacionalID.style.visibility = 'hidden';
        chkSimplesNacional.style.visibility = 'hidden';
        lblSuframaID.style.visibility = 'hidden';
        chkSuframaID.style.visibility = 'hidden';
        lblUFID.style.visibility = 'inherit';
        selUFID.style.visibility = 'inherit';
        lblCidadeId.style.visibility = 'inherit';
        selCidadeId.style.visibility = 'inherit';
        lblClassificacoesID.style.visibility = 'inherit';
        selClassificacoesID.style.visibility = 'inherit';
        lblIsencoesID.style.visibility = 'inherit';
        selIsencoesID.style.visibility = 'inherit';
        selClassificacoesClaID.style.visibility = 'inherit';
        lblClasseID.style.visibility = 'inherit';

        //fillIsencoes();
    }

    if (bCotador) {
        selFamiliaID.selectedIndex = -1;
        selLinhaProdutoID.selectedIndex = -1;
        selTipoRelacaoID.selectedIndex = -1;
        selProdutoReferenciaID.selectedIndex = -1;

        chkEstoque.checked = false;

        // Liberar apenas os btns 7, 9 e 10.
        setupEspecBtnsControlBar('sup', 'DDDDDDHDHHHH');

        preencheMarca();
    }
}

// Cria consit�ncia para finalizar atendimento antes de mudar a cota��o - DMC 17/02/2010
function ControleAtendimentoFinaliza() {
    if (glb_IniciarFinalizar == 1) {
        //selChavePessoaID.value = 1;
        window.top.overflyGen.Alert('Finalizar o atendimento.');
        return true;
    }

    //selChavePessoaID.disabled = true;
}

/********************************************************************
Carrega as insen��es (impostos) de acordo com o paisid
DMC 18/01/2011
********************************************************************/
function fillIsencoes() {
    selIsencoesID.disabled = false;
    dsoIsencoes.SQL = 'SELECT ConceitoID, Imposto ' +
        'FROM Conceitos WITH(NOLOCK) ' +
        'WHERE EstadoID = 2 AND TipoConceitoID=306 AND DestacaNota = 1 AND IncidePreco = 1 AND PaisID =' + aEmpresa[1];

    dsoIsencoes.ondatasetcomplete = dsoIsencoes_DSC;
    dsoIsencoes.refresh();
}
function dsoIsencoes_DSC() {
    clearComboEx(['selIsencoesID']);
    while (!dsoIsencoes.recordset.EOF) {
        optionStr = dsoIsencoes.recordset['Imposto'].value;
        optionValue = dsoIsencoes.recordset['ConceitoID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selIsencoesID.add(oOption);
        dsoIsencoes.recordset.MoveNext();
    }
}

function selTransacao_onchange() {
    fg.Rows = 1;

    fillComboFinalidade();
}

function selFinalidade_onchange() {
    fg.Rows = 1;
}

function selProdutoReferenciaID_onchange() {
    setLabelOfControlEx(lblProdutoReferenciaID, selProdutoReferenciaID);
}

function selTipoRelacaoID_onchange() {
    setLabelOfControlEx(lblTipoRelacaoID, selTipoRelacaoID);
    fg.Rows = 1;
}

function selFamilia_onchange() {
    preencheMarca();
}

function selMarca_onchange() {
    verificaCargaCotador();
    preencheLinha();
}

function selLinha_onchange() {
    ;
}

function selFonteID_onchange() {
    var nFonteID = 0, bEstoqueComun = false, bLote = false, bCotador = false;

    if (glb_nSelFonteID_lastIndex != null) {
        if ((selFonteID.value == 3 && glb_nSelFonteID_lastIndex != 3) || (selFonteID.value != 3 && glb_nSelFonteID_lastIndex == 3)) {
            if (ControleAtendimentoFinaliza() == true) {
                selFonteID.value = glb_nSelFonteID_lastIndex;
            }
        }
    }

    glb_nSelFonteID_lastIndex = selFonteID.value;

    zeraGrid();

    nFonteID = selFonteID.value;

    if (nFonteID == 1) {
        bEstoqueComun = true;
        txtArgumento.onkeyup = null;
    }
    else if (nFonteID == 2) {
        bLote = true;
        txtArgumento.onkeyup = null;
    }
    else {
        bCotador = true;
        txtArgumento.onkeyup = txtArgumento_onkeyup;
    }

    if (fg.Rows > 1) {
        if (bLote) {
            fg.ColHidden(getColIndexByColKey(fg, 'Lote')) = false;
            adjustOptionsNumRegsInComboPesqList();
            fg.Rows = 1;
            adjustSupInfControlsBar('PESQMODE');
            pesqlistIsVisibleAndUnlocked();
        }
        else {
            fg.ColHidden(getColIndexByColKey(fg, 'Lote')) = (!bCotador ? true : false);
            fg.Rows = 1;
        }
    }

    if (bCotador) {
        //@@ Ordem e titulos das colunas do grid de pesquisa
        glb_COLPESQORDER = new Array('LineNumber', 'ID', 'Produto', 'Substituto', 'OK', 'Empresa', 'Origem', 'Pre�o', 'Quant', 'MC', 'Desconto', 'NCM', 'Guia', 'ICMS-ST', 'Vig�ncia', 'Pre�o Fob Desc', 'Pre�o Fob', 'FIS',
            'FIE', 'Disp', 'Previs�o Entrega', 'TipoProdutoID', 'Linha Produto', 'CFOP', 'Ver Tamb�m', 'Inconsist�ncia', 'EmpresaID', 'PrecoDigitado', 'MargemDigitada', 'ICMSST_Unitario', 'CorpProdutoID',
            'PartNumber', 'MoedaID', 'Moeda', 'OrigemID');

        glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', '', '###,###,###,###.00', '####', '###.00', '###.00', '', '', '###.00', '', '###,###,###,###.00', '###,###,###,###.00', '###.00', '###.00', '', '',
            '', '', '', '', '', '', '', '', '', '', '', '', '','');

        //pesqlistIsVisibleAndUnlocked();

        selChavePessoaID_ElementsInForm();

        //selChavePessoaID.disabled = true;

        lblFamiliaID.style.visibility = 'hidden';
        selFamiliaID.style.visibility = 'hidden';
        lblMoedaConversaoID.style.visibility = 'hidden';
        selMoedaConversaoID.style.visibility = 'hidden';

        selPesquisa.selectedIndex = 1;
        selPesquisa.disabled = false;
    }
    else {
        glb_COLPESQORDER = new Array('ID', 'Produto', 'OK', 'Empresa', 'Lote', 'E', 'Pre�o', 'Quant', 'MC', 'Imp', 'Guia', 'ICMS-ST', 'Disp', 'Disp 1', 'Disp 2', 'RV',
            'Equipe', 'QCx', 'Previs�o Entrega', 'Linha', '�ltima compra', 'Observa��o', 'CFOP', 'NCM', 'PrecoDigitado', 'MargemDigitada',
            'MargemMinima', 'ICMSST_Unitario', 'Pre�o 0%', 'DadosProd', 'TemEmailMKT', 'DiasPagamento', 'EmpresaID', 'IDTMP');

        glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', '###,###,###,###.00', '####', '###.00', '###.00', '', '###,###,###,###.00', '', '', '', '', '', '', '',
            '', '', '', '', '', '###,###,###,###.00', '###.00', '###.00', '###,###,###,###.00', '###,###,###,###.00', '', '', '', '','');


        selFamiliaID.selectedIndex = -1;
        selLinhaProdutoID.selectedIndex = -1;
        selTipoRelacaoID.selectedIndex = -1;
        selProdutoReferenciaID.selectedIndex = -1;
        selChavePessoaID.selectedIndex = 0;
        selChavePessoaID.value = 1;
        selChavePessoaID_ElementsInForm();
        clearComboEx(['selMarcaID']);

        selChavePessoaID.disabled = false;

        lblFiltro.style.visibility = 'inherit';
        txtFiltro.style.visibility = 'inherit';
        lblFiltroPessoa.style.visibility = 'inherit';
        txtFiltroPessoa.style.visibility = 'inherit';
        lblFamiliaID.style.visibility = 'inherit';
        selFamiliaID.style.visibility = 'inherit';
        lblMoedaConversaoID.style.visibility = 'inherit';
        selMoedaConversaoID.style.visibility = 'inherit';
        lblProdutoReferenciaID.style.visibility = 'inherit';
        selProdutoReferenciaID.style.visibility = 'inherit';
        lblTipoRelacaoID.style.visibility = 'inherit';
        selTipoRelacaoID.style.visibility = 'inherit';
        btnFindProduto.style.visibility = 'inherit';
        btnListar.style.visibility = 'inherit';
        chkEstoque.style.visibility = 'inherit';
        lblEstoque.style.visibility = 'inherit';

        lblPessoaID.style.left = 494;
        selPessoaID.style.left = 494;
        lblEstoque.style.left = 272;
        chkEstoque.style.left = 268;
        lblMarcaID.style.left = 435;
        selMarcaID.style.left = 435;
        lblLinhaProdutoID.style.left = 565;
        selLinhaProdutoID.style.left = 565;
        chkEstoque.style.left = 265;
        lblEstoque.style.left = 265;

        setupEspecBtnsControlBar('sup', 'DHHHHHHHHHHH');

        selPesquisa.selectedIndex = 0;
        selPesquisa.disabled = true;
    }

    if (nFonteID == 3 && selPesquisa.value == 'aCorpProdutos')
        txtArgumento.onkeyup = txtArgumento_onkeyup;
    else
        txtArgumento.onkeyup = null;

    txtArgumento.focus();
}

function selPesquisa_onchange() {
    if (selPesquisa.value == 'bConceitos')
        txtArgumento.onkeyup = null;
    else if (selPesquisa.value == 'aCorpProdutos' && selFonteID.value == 3)
        txtArgumento.onkeyup = txtArgumento_onkeyup;

    txtArgumento.focus();
}

function chkDadosPessoas_onchange() {
    var bCotador = (selFonteID.value == 3 ? true : false);

    if ((bCotador) && (selCidadeId.value > 0))
        buscaPessoaCotador();
}

function txtQuantidade_onkeydown() {
    if (event.keyCode == 13)
        __btn_LIST('sup');
}

function fillStaticCmbs() {
    var i = 0;
    var j = 0;

    var aCmbsDynamics = new Array(selUFID, selFinanciamentoID, selMoedaConversaoID); //, selFrete);
    var selUFID_SUP = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selUFID');
    var selFinanciamentoID_SUP = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selFinanciamentoID');
    var selMoedaConversaoID_SUP = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selMoedaConversaoID');
    //var selFrete_SUP = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selFrete');

    var aSUPCmbs = new Array(selUFID_SUP, selFinanciamentoID_SUP, selMoedaConversaoID_SUP);//, selFrete_SUP);
    var aPESQLISTCmbs = new Array(selUFID, selFinanciamentoID, selMoedaConversaoID);//, selFrete);

    clearComboEx(['selUFID', 'selFinanciamentoID', 'selMoedaConversaoID']);//, 'selFrete']);

    for (i = 0; i <= 2; i++) {
        for (j = 0; j < aSUPCmbs[i].options.length; j++) {

            optionStr = aSUPCmbs[i].options[j].text;
            optionValue = aSUPCmbs[i].options[j].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aPESQLISTCmbs[i].add(oOption);
        }
    }
    selectDefaultValuesStaticCombos();
    lockUnlockSecondLine();

}

function selectDefaultValuesStaticCombos() {
    if (selUFID.options.lenght > 0)
        selUFID.selectedIndex = 0;

    if (selFinanciamentoID.options.lenght > 0)
        selFinanciamentoID.selectedIndex = 0;

    if (selMoedaConversaoID.options.lenght > 0)
        selMoedaConversaoID.selectedIndex = 0;

    defaultValuesForNewPartner();
}

function lockUnlockSecondLine() {
    selParceiroID.disabled = (selParceiroID.options.length <= 1);
    selPessoaID.disabled = (selPessoaID.options.length == 0);
    //txtFiltroPessoa.disabled = (selPessoaID.options.length == 0);
    selUFID.disabled = (selUFID.options.length == 0);
    selFinanciamentoID.disabled = (selFinanciamentoID.options.length == 0);
    selMoedaConversaoID.disabled = (selMoedaConversaoID.options.length == 0);
    selProdutoReferenciaID.disabled = true; //(selProdutoReferenciaID.options.length == 0);	
}

function setupFieldsSecondLine() {
    selParceiroID.onchange = cmbsSecondLineChange;
    selPessoaID.onchange = cmbsSecondLineChange;
    selUFID.onchange = cmbsSecondLineChange;
    selFinanciamentoID.onchange = cmbsSecondLineChange;
    selMoedaConversaoID.onchange = cmbsSecondLineChange;
    chkContribuinte.onclick = selPessoaID_OnChange;
    //selFrete.onchange = cmbsSecondLineChange;

    selParceiroID.onfocusin = selParceiroID_onfocusin;

    elem = txtQuantidade;
    elem.setAttribute('thePrecision', 5, 1);
    elem.setAttribute('theScale', 0, 1);
    elem.setAttribute('verifyNumPaste', 1);
    elem.onkeypress = verifyNumericEnterNotLinked;
    elem.onkeyup = txtQuantidade_onkeyUp;
    elem.setAttribute('minMax', new Array(0, 99999), 1);
    elem.onfocus = selFieldContent;
    elem.maxLenght = 5;

    btnFindCliente.onclick = btnFindCliente_onclick;
    //btnIniciarFinalizar.onclick = btnIniciarFinalizar_onclick;
    btnFindProduto.onclick = btnFindProduto_onclick;
    btnListar.onclick = btnListar_onclick;
    btnFindCliente.style.visibility = 'hidden';
}

function cmbsSecondLineChange() {
    zeraGrid();

    if (this.id == 'selParceiroID')
        selParceiroID_OnChange(true);
    else if (this.id == 'selPessoaID')
        selPessoaID_OnChange();
    else if (this.id == 'selFinanciamentoID')
        setLabelOfControlEx(lblFinanciamentoID, selFinanciamentoID);
}

function selParceiroID_onfocusin() {
    //if (selParceiroID.value > 0) {
    glb_nParceiroID = selParceiroID.value;
    //}
}

function txtQuantidade_onkeyUp() {
    ;
}


/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked() {
    glb_bPaste = false;
    var empresaData = getCurrEmpresaData();
    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    adjustOptionsNumRegsInComboPesqList();
    if (glb_ReallocGrid == true) {
        glb_ReallocGrid = false;

        reallocDivSup01();
        reallocGrid();
        drawBordersAroundTheGrid(fg);
        adjustEspecificFields();
        fillStaticCmbs();
    }

    if (glb_callFromDet) {
        glb_callFromDet = false;

        if ((window.top.__glb_ListOrDet == '') || (window.top.__glb_ListOrDet == 'DET'))
            cloneControlsContentsAndState();
    }


    // Esconde a empresa para allplus e compania...
    //else
    if (empresaData[0] == 7) {
        if (fg.Cols > 4)
            fg.ColHidden(getColIndexByColKey(fg, 'Empresa')) = true;
    }

    // Ze em 18/09/06 avisa o sup que voltou para lista de precos
    try {
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_btnBarNotEspecClicked =' + '\'' + '\'');
    }
    catch (e) {
        ;
    }

    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Produto', 'Extrato de pre�o', 'NCM', 'CRM', 'Copiar dados do produto', 'Comprar', 'Sacola de Compra']);
    setupEspecBtnsControlBar('sup', 'DHHHHHHHHHHH');


    //Cotador
    if (selFonteID.value == 3) {
        glb_COLPESQORDER = new Array('LineNumber', 'ID', 'Produto', 'Substituto', 'OK', 'Empresa', 'Origem', 'Pre�o', 'Quant', 'MC', 'Desconto', 'NCM', 'Guia', 'ICMS-ST', 'Vig�ncia', 'Pre�o Fob Desc', 'Pre�o Fob', 'FIS',
            'FIE', 'Disp', 'Previs�o Entrega', 'TipoProdutoID', 'Linha Produto', 'CFOP', 'Ver Tamb�m', 'Inconsist�ncia', 'EmpresaID', 'PrecoDigitado', 'MargemDigitada', 'ICMSST_Unitario', 'CorpProdutoID',
            'PartNumber', 'MoedaID', 'Moeda', 'OrigemID');
        pintaGrid();
    }
    else
        glb_COLPESQORDER = new Array('ID', 'Produto', 'OK', 'Empresa', 'Lote', 'E', 'Pre�o', 'Quant', 'MC', 'Imp', 'Guia', 'ICMS-ST', 'Disp', 'Disp 1', 'Disp 2', 'RV',
            'Equipe', 'QCx', 'Previs�o Entrega', 'Linha', '�ltima compra', 'Observa��o', 'CFOP', 'NCM', 'PrecoDigitado', 'MargemDigitada',
            'MargemMinima', 'ICMSST_Unitario', 'Pre�o 0%', 'DadosProd', 'TemEmailMKT', 'DiasPagamento', 'EmpresaID', 'IDTMP');
    fg.FontSize = '8';

    try {
        if (selFonteID.value == 1)
            fg.ColHidden(getColIndexByColKey(fg, 'Lote')) = true;
    }
    catch (e) {
        ;
    }

    if (selFonteID.value != 3) {
        if (fg.Cols > 7) {
            alignColsInGrid(fg, [0, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]);

            fg.ColHidden(getColIndexByColKey(fg, 'DadosProd')) = true;
            fg.ColHidden(getColIndexByColKey(fg, 'EmpresaID')) = true;
            fg.ColHidden(getColIndexByColKey(fg, 'TemEmailMKT')) = true;
            fg.ColHidden(getColIndexByColKey(fg, 'DiasPagamento')) = true;
            fg.ColHidden(getColIndexByColKey(fg, 'PrecoDigitado')) = true;
            fg.ColHidden(getColIndexByColKey(fg, 'MargemDigitada')) = true;
            fg.ColHidden(getColIndexByColKey(fg, 'ICMSST_Unitario')) = true;
            fg.ColHidden(getColIndexByColKey(fg, 'MargemMinima')) = true;
            fg.ColHidden(getColIndexByColKey(fg, 'IDTMP')) = true;

            if (glb_nA1 == 0 || glb_nA2 == 0)
                fg.ColHidden(getColIndexByColKey(fg, 'MC')) = true;

            // Esconde a coluna "Preco 0%"
            fg.ColHidden(getColIndexByColKey(fg, 'PrecoZero')) = true;


            /*
                    // Esconde a coluna "E"
                    if (aEmpresa[1] == 167)
                        fg.ColHidden(4) = false;
                    else
                        fg.ColHidden(4) = true;
            */

            // Esconde colunas ICMSST, GUIA, NCM E CFOP na allplus
            if (aEmpresa[1] != 130) {
                fg.ColHidden(getColIndexByColKey(fg, 'Guia')) = true;
                fg.ColHidden(getColIndexByColKey(fg, 'ICMSST')) = true;
                fg.ColHidden(getColIndexByColKey(fg, 'NCM')) = true;
                fg.ColHidden(getColIndexByColKey(fg, 'CFOP')) = true;
            }
            else {
                fg.ColHidden(getColIndexByColKey(fg, 'Guia')) = false;
                fg.ColHidden(getColIndexByColKey(fg, 'ICMSST')) = false;
                fg.ColHidden(getColIndexByColKey(fg, 'NCM')) = false;
                fg.ColHidden(getColIndexByColKey(fg, 'CFOP')) = false;
            }

            fg.MergeCells = 4;
            fg.MergeCol(0) = true;
            fg.MergeCol(1) = true;

            fg.ColWidth(getColIndexByColKey(fg, 'ConceitoID')) = 650;
            fg.ColWidth(getColIndexByColKey(fg, 'Conceito')) = 4800;
            fg.ColWidth(getColIndexByColKey(fg, 'Imp')) = 550;
            fg.ColWidth(getColIndexByColKey(fg, 'Disponivel1')) = 550;



            unblockColsInPesqList(fg, [getColIndexByColKey(fg, 'OK'), getColIndexByColKey(fg, 'Preco'), getColIndexByColKey(fg, 'Guia'), getColIndexByColKey(fg, 'ICMSST'),
            getColIndexByColKey(fg, 'MC'), getColIndexByColKey(fg, 'QuantidadeComprar')]);




            putMasksInGrid(fg, ['', '', '', '', '', '', '999999999.99', '9999', '999.99', '999.99', '', '999999999.99', '', '', '', '', '', '', '', '', '', '', '', '', '999999999.99', '999.99', '999.99', '999999999.99', '999999999.99', '', '', '', ''],
                ['', '', '', '', '', '', '###,###,###,###.00', '####', '###.00', '###.00', '', '###,###,###,###.00', '', '', '', '', '', '', dTFormat, '', '', '', '', '', '###,###,###,###.00', '###.00', '###.00', '###,###,###,###.00', '###,###,###,###.00', '', '', '', '']);
        }

        if (glb_FirstLoad) {
            if (selRegistros.selectedIndex < 0)
                selRegistros.selectedIndex = 0;
            glb_FirstLoad = false;
        }

        glb_nA1 = getCurrRightValue('SUP', 'A1');

        glb_nA2 = getCurrRightValue('SUP', 'A2');


        pintaGrid();

        AjustaPesquisa();
    }
    else if (selFonteID.value == 3 && glb_aCotacao != null) {
        fg.ColHidden(getColIndexByColKey(fg, 'EmpresaID')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'MoedaID')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'PrecoDigitado')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'MargemDigitada')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'ICMSST_Unitario')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'CorpProdutoID')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'PartNumber')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'Moeda')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'TipoProdutoID')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'OrigemID')) = true;

        alignColsInGrid(fg, [
            getColIndexByColKey(fg, 'PrecoFob'),
            getColIndexByColKey(fg, 'Desconto'),
            getColIndexByColKey(fg, 'PrecoFobDesc'),
            getColIndexByColKey(fg, 'Preco'),
            getColIndexByColKey(fg, 'ICMSST'),
            getColIndexByColKey(fg, 'MC'),
            getColIndexByColKey(fg, 'QuantidadeComprar'),
            getColIndexByColKey(fg, 'Disponivel'),
            getColIndexByColKey(fg, 'FIS'),
            getColIndexByColKey(fg, 'FIE')],
            null);

        if (glb_nA1 == 0 || glb_nA2 == 0)
            fg.ColHidden(getColIndexByColKey(fg, 'MC')) = true;

        unblockColsInPesqList(fg, [getColIndexByColKey(fg, 'OK')]);

        dsoListData01.parameters = '';

        pintaGrid();

        glb_bPaste = true;
    }
    else {
        fg.ColHidden(getColIndexByColKey(fg, 'EmpresaID')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'PrecoDigitado')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'MargemDigitada')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'ICMSST_Unitario')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'CorpProdutoID')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'MoedaID')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'LineNumber')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'Vigencia')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'PartNumber')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'Moeda')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'TipoProdutoID')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'OrigemID')) = true;

        alignColsInGrid(fg, [
            getColIndexByColKey(fg, 'PrecoFob'),
            getColIndexByColKey(fg, 'Desconto'),
            getColIndexByColKey(fg, 'PrecoFobDesc'),
            getColIndexByColKey(fg, 'Preco'),
            getColIndexByColKey(fg, 'ICMSST'),
            getColIndexByColKey(fg, 'MC'),
            getColIndexByColKey(fg, 'QuantidadeComprar'),
            getColIndexByColKey(fg, 'Disponivel'),
            getColIndexByColKey(fg, 'FIS'),
            getColIndexByColKey(fg, 'FIE')],
            null);

        if (glb_nA1 == 0 || glb_nA2 == 0)
            fg.ColHidden(getColIndexByColKey(fg, 'MC')) = true;

        unblockColsInPesqList(fg, [getColIndexByColKey(fg, 'OK'), getColIndexByColKey(fg, 'Preco'), getColIndexByColKey(fg, 'Desconto'), getColIndexByColKey(fg, 'Guia'), getColIndexByColKey(fg, 'ICMSST'),
        getColIndexByColKey(fg, 'MC'), getColIndexByColKey(fg, 'QuantidadeComprar')]);

        putMasksInGrid(fg, ['', '', '', '', '', '', '', '999999999.99', '9999', '999.99', '999.99', '', '', '999.99', '', '999999999.99', '999999999.99', '999.99', '999.99', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', '###,###,###,###.00', '####', '###.00', '###.00', '', '', '###.00', '', '###,###,###,###.00', '###,###,###,###.00', '###.00', '###.00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);
    }

    if (fg.Rows == 2) {
        fg.TextMatrix(1, getColIndexByColKey(fg, 'OK')) = 1;
    }


    if (fg.Rows > 1)
        fg.FrozenCols = getColIndexByColKey(fg, 'Ok') + 1;

    if (selFonteID.value == 3) {
        fg.ColWidth(getColIndexByColKey(fg, 'Produto')) = 5714;

        if (!glb_bCargaCompletaAtualizada) {
            for (i = 1; i < fg.Rows; i++) {
                fg.TextMatrix(i, getColIndexByColKey(fg, 'Inconsistencia')) = dsoCargaCotador.recordset['MsgInconsistencia'].value +
                    (fg.TextMatrix(i, getColIndexByColKey(fg, 'Inconsistencia')).length > 0 ? "; " + fg.TextMatrix(i, getColIndexByColKey(fg, 'Inconsistencia')) : "");
            }

            pintaGrid();
        }
    }

    glb_aCotacao = null;
    txtArgumento.onfocus();
}

function pintaGrid() {
    var bgColorGreen = 0X90EE90;
    var bgColorYellow = 0x00ffff;
    var bgColorOrange = 0X60A4F4;
    var bgColorRed = 0X0000FF;
    var bgColorWhite = 0XFFFFFF;
    var bgColorBlack = 0x000010;

    //Bacil Teste
    lockInterface(true);
    for (i = 1; i < fg.Rows; i++) {
        if ((selFonteID.value == 3) && (!glb_bCargaCompletaAtualizada)) {
            fg.Cell(6, i, getColIndexByColKey(fg, 'Produto'), i, getColIndexByColKey(fg, 'Produto')) = bgColorRed;
        }
        else if (selFonteID.value == 3) {
            fg.Cell(6, i, getColIndexByColKey(fg, 'Produto'), i, getColIndexByColKey(fg, 'Produto')) = bgColorWhite;

            if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Inconsistencia')).length > 0) {
                fg.Cell(6, i, getColIndexByColKey(fg, 'Produto'), i, getColIndexByColKey(fg, 'Produto')) = bgColorYellow;
            }
        }

        else {
            if (fg.TextMatrix(i, getColIndexByColKey(fg, 'TemEmailMKT')) == 1)
                fg.Cell(6, i, getColIndexByColKey(fg, 'Empresa*'), i, (fg.Cols - 1)) = bgColorGreen;
            //fg.Cell(6, i, getColIndexByColKey(fg, 'Estado*'), i, getColIndexByColKey(fg, 'Estado*')) = bgColorGreen;

            // Removido a condi��o que verifica as permiss�es do usu�rio para mostrar a cor do produto, conforme Ticket#2018072596008641 e conversado com o Marco Fortunato. LYF 26/07/2018
            //if ((glb_nA1 == 1) && (glb_nA2 == 1)) {
            if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'DiasPagamento')) < 0) && (fg.TextMatrix(i, getColIndexByColKey(fg, 'DiasPagamento')) >= -30))
                fg.Cell(6, i, getColIndexByColKey(fg, 'Estado*'), i, getColIndexByColKey(fg, 'Estado*')) = bgColorYellow;

            else if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'DiasPagamento')) < -30) && (fg.TextMatrix(i, getColIndexByColKey(fg, 'DiasPagamento')) >= -60))
                fg.Cell(6, i, getColIndexByColKey(fg, 'Estado*'), i, getColIndexByColKey(fg, 'Estado*')) = bgColorOrange;

            else if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'DiasPagamento')) < -60) && (fg.TextMatrix(i, getColIndexByColKey(fg, 'DiasPagamento')) >= -100))
                fg.Cell(6, i, getColIndexByColKey(fg, 'Estado*'), i, getColIndexByColKey(fg, 'Estado*')) = bgColorRed;

            else if (fg.TextMatrix(i, getColIndexByColKey(fg, 'DiasPagamento')) < -100) {
                //Pinta cor da fonte
                fg.Cell(7, i, getColIndexByColKey(fg, 'Estado*'), i, getColIndexByColKey(fg, 'Estado*')) = bgColorWhite;

                //Pinta c�lula
                fg.Cell(6, i, getColIndexByColKey(fg, 'Estado*'), i, getColIndexByColKey(fg, 'Estado*')) = bgColorBlack;
            }
            //}
        }
    }
    lockInterface(false);
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    //tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Produto', 'Comprar', 'Carrinho de Compra', 'NCM', 'Copiar dados do produto']);

    var bCotador = (selFonteID.value == 3 ? 1 : 0);

    if (controlBar == 'SUP') {
        // Usuario clicou botao documentos
        if (btnClicked == 1) {
            if (fg.Rows > 1) {
                __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
            }
            else {
                window.top.overflyGen.Alert('Selecione um registro.');
            }
        }
        // usuario clicou botao relatorios
        else if (btnClicked == 2)
            openModalPrint();
        else if (btnClicked == 3) {
            window.top.openModalControleDocumento('PL', '', 720, null, 42, 'T');
        }
        else if (btnClicked == 4)
            if (fg.Rows > 1) {
                var empresaData = getCurrEmpresaData();
                sendJSCarrier(getHtmlId(), 'SHOWCONCEITO', new Array(empresaData[0], getCellValueByColKey(fg, 'ConceitoID*', fg.Row)));
            }
            else {
                if (window.top.overflyGen.Alert('Selecione um produto') == 0)
                    return null;
            }
        else if (btnClicked == 5)
            openModalExtratoPreco();
        else if (btnClicked == 6)
            detalhaNCMPesqlist();
        else if (btnClicked == 7) {
            if ((selChavePessoaID.value == 2) && (!bCotador)) {
                if (window.top.overflyGen.Alert('Selecionar cota��o Com Pessoa') == 0)
                    return null;
            }
            else
                btnCRM_onclick();
        }
        else if (btnClicked == 8) {
            glb_nRow = fg.Row;
            prepareDataToClipBoard();
        }
        else if (btnClicked == 9)
            executePurchaseInModal();
        else if (btnClicked == 10) {
            //N�o permite abrir a sacola no modo Sem Pessoa caso a UF n�o teha sido selecionada
            if ((selUFID.value == 0) && (selChavePessoaID.value == 2)) {
                window.top.overflyGen.Alert('Para cota��o Sem Pessoa selecione uma UF');
                return true;
            }
            else
                openModalTrolley('PL');

        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
- Avancar, retroagir, incluir, alterar, estado, 
excluir, OK, cancelar refresh, anterior e proximo. 
(Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {

    var bCotador = (selFonteID.value == 3 ? 1 : 0);

    if (btnClicked != 'PLPESQ') {
        selProdutoReferenciaID.disabled = true;

        if ((selMetodoPesq.value == 'DEF') && (selPesquisa.value == 'aObjetoID') && (isNaN(txtArgumento.value))) {
            window.top.overflyGen.Alert('O campo Argumento cont�m tipo de dado incompat�vel.');
            return true;
        }
        else if (selMarcaID.value == 0 && glb_aCotacao == null && selFonteID.value == 3) {
            window.top.overflyGen.Alert('Selecione uma marca');
            return true;
        }
        else if ((trimStr(txtArgumento.value).length < 2) &&
            ((selFamiliaID.value == 0) && (selMarcaID.value == 0) && (selLinhaProdutoID.value == 0)) &&
            (((selChavePessoaID.value == 1) && (glb_nEmaClienteID == null)) || (selChavePessoaID.value == 2)) &&
            (!((bCotador) && (glb_aCotacao != null)))) {
            window.top.overflyGen.Alert('Digite um argumento v�lido.');
            return true;
        }
        else if (selChavePessoaID.value == 1) {
            if ((selPessoaID.value == "") || (selPessoaID.value == 0)) {
                window.top.overflyGen.Alert('Selecione um cliente');
                return true;
            }
        }
        else if (selChavePessoaID.value == 2) {
            if ((bCotador) && (selParceiroID.value == 0)) {
                window.top.overflyGen.Alert('Para cota��o Sem Pessoa selecione um Parceiro');
                return true;
            }
            else if (selClassificacoesClaID.value == 0) {
                window.top.overflyGen.Alert('Para cota��o Sem Pessoa informe uma Classifica��o');
                return true;
            }
            else if ((selUFID.value == 0) && (aEmpresa[0] != 7)) {
                window.top.overflyGen.Alert('Para cota��o Sem Pessoa selecione uma UF');
                return true;
            }
            /*
            else if ((selFrete.value == 1) && (selCidadeId.selectedIndex == -1) && (aEmpresa[0] != 7)) {
                window.top.overflyGen.Alert('Para cota��o Sem Pessoa com frete pago selecione uma Cidade');
                return true;
            }
            */
        }
    }

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
********************************************************************/
function detalhaNCMPesqlist() {
    if (fg.Row < 1) {
        if (window.top.overflyGen.Alert('Selecione um produto') == 0)
            return null;

        return null;
    }

    NCM = parseInt(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'NCM*')), 10);
    NCM = NCM.toString().substr(0, 4);

    strPars = '?sNCM=' + NCM;
    htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/serverside/imagemncm.aspx' + strPars;
    window.open(htmlPath);
}

function prepareDataToClipBoard() {
    // Com pessoa � necess�rio iniciar atendimento para registrar no log de atendimentos - DMC 16/08/2011
    if (selChavePessoaID.value == "1") {
        if ((glb_nPesAtendimentoID == null) || (glb_nPesAtendimentoID < 1)) {
            window.top.overflyGen.Alert('O atendimento precisa estar iniciado.');
            return null;

            return null;
        }
    }
    if (glb_nRow < 1) {
        if (window.top.overflyGen.Alert('Selecione um produto') == 0)
            return null;

        return null;
    }

    try {
        var aEmpresa = getCurrEmpresaData();
        var nIdiomaParaID = parseInt(aEmpresa[8], 10);

        var nRegistroID = fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'ConceitoID*'));
        var sProduto = produtoDescricao(); // fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'DadosProd'));
        var nEmpresaID = fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'EmpresaID'));

        if (aEmpresa[0] == 7) {
            var sEmpresa = aEmpresa[3];
        }
        else {
            var sEmpresa = fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'Empresa*'));
        }
        var nQuantidade = fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'QuantidadeComprar'));

        var sMoeda = selMoedaConversaoID.options(selMoedaConversaoID.selectedIndex).innerText;
        var nMoedaID = selMoedaConversaoID.value;

        var nPrecoUnitario = fg.ValueMatrix(glb_nRow, getColIndexByColKey(fg, 'Preco'));
        var sAliquota = fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'Imp*'));
        var sPrazoMedioPagamento = selFinanciamentoID.options(selFinanciamentoID.selectedIndex).innerText;
        var nPMP = selFinanciamentoID.options(selFinanciamentoID.selectedIndex).getAttribute('PMP', 1);
        nPMP = (nPMP == null ? 0 : nPMP);

        if ((isNaN(nQuantidade)) || (nQuantidade == '0') || (nQuantidade == ''))
            nQuantidade = '1';

        copyDatatoClipBoard(nRegistroID, sProduto, nEmpresaID, sEmpresa, nQuantidade, sMoeda, nMoedaID, nPrecoUnitario, sAliquota, sPrazoMedioPagamento, nPMP);

    }
    catch (e) {
        ;
    }
}

function produtoDescricao() {
    var retVal = '';

    if (fg.Rows > 1)
        retVal = fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'DadosProd'));

    return retVal;
}

function copyDatatoClipBoard(nProdutoID, sProduto, nEmpresaID, sEmpresa, nQuantidade, sMoeda, nMoedaID, nPrecoUnitario, sAliquota, sPrazoMedioPagamento, nPMP) {
    var aEmpresa = getCurrEmpresaData();
    var nIdiomaParaID = parseInt(aEmpresa[8], 10);
    var sQuantidade, sPreco, sImposto, sPrazo, sDias;
    var sReturn = String.fromCharCode(13) + String.fromCharCode(10);
    var sEstoque = '';

    // Portugues
    if (nIdiomaParaID == 246) {
        sQuantidade = 'Quantidade';
        sPreco = 'Pre�o';
        sImposto = 'Imposto';
        sPrazo = 'Prazo';
        sDias = 'dias';
        sEstoque = 'Estoque';
    }
    else {
        sQuantidade = 'Quantity';
        sPreco = 'Price';
        sImposto = 'Tax';
        sPrazo = 'Term';
        sDias = 'days';
        sEstoque = 'Estoque';
    }

    var sStrToClipboard = nProdutoID + ' ' + sProduto + sReturn +
        sPreco + ' ' + sMoeda + ' ' + padNumReturningStr(roundNumber(parseFloat(nPrecoUnitario), 2), 2) +
        (trimStr(sAliquota.toString()) != '' ? '  ' + sImposto + ' ' + sAliquota.toString() + '%' : '') + '   ' +
        sPrazo + ' ' + sPrazoMedioPagamento + ' ' + sDias + sReturn +
        sEstoque + ' ' + sEmpresa + '  ' + sQuantidade + ' ' + nQuantidade;

    if (selChavePessoaID.value == "1") {
        //if ((glb_nPesAtendimentoID == null) || (glb_nPesAtendimentoID < 1)) {
        glb_sClipBoardBuffer += (glb_sClipBoardBuffer != '' ? ';' : '') +
            '[' + nProdutoID.toString() + '|' +
            nEmpresaID.toString() + '|' +
            nQuantidade.toString() + '|' +
            nMoedaID + '|' +
            nPrecoUnitario + '|' +
            (sAliquota == '' ? '0' : sAliquota) + '|' +
            nPMP + ']';
        //}
    }

    window.top.overflyGen.PutTextInClipboard = sStrToClipboard;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Modal de impressao
    if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    // Modal do carrinho de compras
    else if (idElement.toUpperCase() == 'MODALTROLLEYHTML') {
        if (param1 == 'OK') {
            // esta aqui apenas para compatibilidade
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            if (selFonteID.value == 3 && selChavePessoaID.value == 2)
                buscaPessoaCotador();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            if (selFonteID.value == 3 && selChavePessoaID.value == 2)
                buscaPessoaCotador();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    // Modal de Extratos de Pre�os
    else if (idElement.toUpperCase() == 'MODALEXTRATOPRECOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    // Modal de emails de promoca
    else if (idElement.toUpperCase() == 'MODALEMAILSMARKETINGHTML') {
        if (param1 == 'OK') {
            // esta aqui apenas para compatibilidade
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCLIENTEHTML') {
        if (param1 == 'OK') {
            glb_sClipBoardBuffer = '';

            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            // Preenche o combo de cliente
            glb_aDadosPessoa = param2;
            glb_TimerListaPrecoVar2 = window.setInterval('addCmbParceiro()', 10, 'JavaScript');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            set_glb_IniciarFinalizarStatus(0);

            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            return 0;
        }
    }
    // Modal Atendimento
    else if (idElement.toUpperCase() == 'MODALATENDIMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            if (dsoArgumento.recordset.aRecords.length != 0) {
                selFonteID.disabled = false;
            }

            delOptCmbParceiro();
            set_glb_IniciarFinalizarStatus(0);
            selParceiroID.value = -1;
            selParceiroID_OnChange();
            setTimerAgendamentoStatus(false);

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            if (dsoArgumento.recordset.aRecords.length != 0) {
                selFonteID.disabled = false;
            }

            var dtStartAtendimento = new Date();

            //Atendimento inciado pela modal atendimento            
            //Carregar parceiro no combo, travar combo. Mudar status para finalizar
            if (param2[0] == 1) {
                delOptCmbParceiro();
                set_glb_IniciarFinalizarStatus(1);

                // Preenche o combo de cliente
                glb_aDadosPessoa[0] = param2[1];
                glb_aDadosPessoa[1] = param2[2];
                glb_aDadosPessoa[2] = param2[3];
                glb_aDadosPessoa[3] = param2[4];
                glb_aDadosPessoa[4] = param2[5];
                glb_aDadosPessoa[5] = param2[6];
                glb_aDadosPessoa[6] = param2[7];
                glb_aDadosPessoa[7] = param2[8];
                glb_aDadosPessoa[8] = param2[9];

                glb_nPesAtendimentoID = param2[10];

                glb_TimerListaPrecoVar2 = window.setInterval('addCmbParceiro()', 10, 'JavaScript');
                //selParceiroID_onchange();

                if ((glb_nEmaClienteID != null) && (selChavePessoaID.value == 1))
                    glb_bCarregaProdutosOferta = true;

                glb_nParceiroID = glb_aDadosPessoa[1];
            }
            else if (param2[0] == 2) {
                glb_nPesAtendimentoID = param2[2];

                set_glb_IniciarFinalizarStatus(1);

                selOptByValueInSelect(getHtmlId(), 'selParceiroID', param2[1]);

                setLabelParceiro();
                selParceiroID_OnChange();
            }
            else {
                if (glb_IniciarFinalizar != 1) {
                    selParceiroID.value = 0;
                    txtFiltroPessoa.value = '';
                    selPessoaID.value = 0;
                }
            }

            if ((param2[0] == 1) || (param2[0] == 2)) {
                glb_dtAtendimentoInicio = dtStartAtendimento.getTime();
                glb_TimerTempodecorrido = window.setInterval('setTimerAgendamentoStatus(true)', 1000, 'JavaScript');;
            }

            if ((glb_nEmaClienteID != null) && (selChavePessoaID.value == 1) && (selPessoaID.value > 1)) {
                txtArgumento.value = '';
                __btn_LIST('sup');
            }

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
            fonteEnable();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }
}
function defaultValuesForNewPartner() {
    zeraGrid();
    //selFrete.selectedIndex = 0;

    txtQuantidade.value = 1;
}

function fonteEnable() {
    // Habilita selFonteID para evitar que selFonteID fique desabilitado ap�s retorno do dsoArgumento
    if (dsoArgumento.recordset.aRecords.length != 0) {
        selFonteID.disabled = false;
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch() {
    //@@ da automacao -> retorno padrao
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    // por questoes esteticas, coloca foco no grid de pesquisa
    if (fg.disabled == false)
        fg.focus();

    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + encodeURIComponent('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + encodeURIComponent(empresaID);
    strPars += '&sEmpresaFantasia=' + encodeURIComponent(empresaFantasia);
    strPars += '&nContextoID=' + encodeURIComponent(contexto[1]);
    strPars += '&nUserID=' + encodeURIComponent(userID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(385, 300));

}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal do carrinho de compras

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalTrolley(sCaller) {
    // por questoes esteticas, coloca foco no grid de pesquisa
    if (sCaller == 'PL') {
        if (fg.disabled == false)
            fg.focus();
    }

    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 539;
    var aEmpresa = getCurrEmpresaData();
    var nEmpresaID = getCurrentEmpresaID();

    // mandar os parametros para o servidor
    // Esta modal fica sempre carregada e o que identifica
    // isto e o terceiro parametro da funcao showModalWin abaixo.
    // No strPars, o primeiro parametro e obrigatorio e e o UNICO!!!
    // que pode ser mandado

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + encodeURIComponent(sCaller);
    strPars += '&nEmpresaID=' + encodeURIComponent(aEmpresa[0]);
    strPars += '&nEmpresaPaisID=' + encodeURIComponent(aEmpresa[1]);
    strPars += '&nIdiomaID=' + encodeURIComponent(aEmpresa[8]);
    strPars += '&nMoedaID=' + encodeURIComponent(aEmpresa[9]);
    strPars += '&sSimbolo=' + encodeURIComponent(aEmpresa[10]);
    strPars += '&bTemProducao=' + encodeURIComponent(aEmpresa[11] == true ? 1 : 0);
    strPars += '&sUrlEmpresa=' + encodeURIComponent(aEmpresa[12]);
    strPars += '&nTipoModal=2';

    var selPaisID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'nEmpresaPaisID');

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modalgen/modaltrolley.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight), 'MODALTROLLEYHTML');
}

function openModalTrolley_SacolasSalvas(sCaller) {
    // por questoes esteticas, coloca foco no grid de pesquisa
    if (sCaller == 'PL') {
        if (fg.disabled == false)
            fg.focus();
    }

    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 539;
    var aEmpresa = getCurrEmpresaData();
    var nEmpresaID = getCurrentEmpresaID();

    // mandar os parametros para o servidor
    // Esta modal fica sempre carregada e o que identifica
    // isto e o terceiro parametro da funcao showModalWin abaixo.
    // No strPars, o primeiro parametro e obrigatorio e e o UNICO!!!
    // que pode ser mandado

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + encodeURIComponent(sCaller);
    strPars += '&nEmpresaID=' + encodeURIComponent(aEmpresa[0]);
    strPars += '&nEmpresaPaisID=' + encodeURIComponent(aEmpresa[1]);
    strPars += '&nIdiomaID=' + encodeURIComponent(aEmpresa[8]);
    strPars += '&nMoedaID=' + encodeURIComponent(aEmpresa[9]);
    strPars += '&sSimbolo=' + encodeURIComponent(aEmpresa[10]);
    strPars += '&bTemProducao=' + encodeURIComponent(aEmpresa[11] == true ? 1 : 0);
    strPars += '&sUrlEmpresa=' + encodeURIComponent(aEmpresa[12]);
    strPars += '&nTipoModal=1';

    var selPaisID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'nEmpresaPaisID');

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modalgen/modaltrolley.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight), 'MODALTROLLEYHTML');
}

function openModalExtratoPreco() {
    if (fg.Row <= 0) {
        if (window.top.overflyGen.Alert('Selecione um produto para refer�ncia.') == 0)
            return null;

        return null;
    }

    if (selFonteID.value == 3) {
        if (window.top.overflyGen.Alert('N�o � poss�vel visualizar extrato de pre�o para itens de Cotador.') == 0)
            return null;

        return null;
    }

    var htmlPath;

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/modalpages/modalextratopreco.asp';

    showModalWin(htmlPath, new Array(550, 600));
}

function getCurrentEmpresaID() {
    var aEmpresa = getCurrEmpresaData();
    var nEmpresaID = aEmpresa[0];

    if (fg.Row > 0)
        nEmpresaID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EmpresaID'));

    return nEmpresaID;
}

function adjustOptionsNumRegsInComboPesqList() {
    var oOption;
    var bLote = selFonteID.value == 2 ? 1 : 0;

    if (selRegistros.options.length != 3) {
        clearComboEx(['selRegistros']);

        oOption = document.createElement("OPTION");
        oOption.text = '10';
        oOption.value = 10;
        selRegistros.add(oOption);

        oOption = document.createElement("OPTION");
        oOption.text = '20';
        oOption.value = 20;
        selRegistros.add(oOption);

        oOption = document.createElement("OPTION");
        oOption.text = '50';
        oOption.value = 50;
        selRegistros.add(oOption);
    }
    if (bLote == 1) {
        oOption = document.createElement("OPTION");
        oOption.text = '100';
        oOption.value = 100;
        selRegistros.add(oOption);
    }
}

/********************************************************************
Trava/destrava botao de lupa.
           
Parametros: 
btnRef      - referencia do botao
cmdLock     - true (trava), false (destrava) 

Retorno:
nenhum
********************************************************************/
function lockBtnLupa(btnRef, cmdLock) {
    if (cmdLock == true)
        btnRef.src = glb_LUPA_IMAGES[1].src;
    else
        btnRef.src = glb_LUPA_IMAGES[0].src;

    // trava/destrava o botao de lupa do objeto
    btnRef.disabled = cmdLock;
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnFindCliente_onclick(btnClicked) {
    openModalCliente();
}
/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
bLimpa     

Retorno:
nenhum
********************************************************************/
function btnFindProduto_onclick(bLimpa) {
    var oOption;
    var nProdutoID;
    var sProduto;

    if (bLimpa == null)
        bLimpa = false;

    clearComboEx(['selProdutoReferenciaID']);

    oOption = document.createElement("OPTION");
    oOption.text = '';
    oOption.value = 0;
    selProdutoReferenciaID.add(oOption);

    if ((fg.Rows > 1) && (!bLimpa)) {
        nProdutoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ConceitoID*'));
        sProduto = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Conceito*'));

        oOption = document.createElement("OPTION");
        oOption.text = sProduto;
        oOption.value = nProdutoID;
        selProdutoReferenciaID.add(oOption);
        selProdutoReferenciaID.selectedIndex = 1;
    }

    setLabelOfControlEx(lblProdutoReferenciaID, selProdutoReferenciaID);

    if (selProdutoReferenciaID.options.length >= 2)
        selProdutoReferenciaID.disabled = false; //= (selProdutoReferenciaID.options.length >= 2);
}

function btnCRM_onclick() {
    /*
    glb_IniciarFinalizar: 
    0 - Iniciar
    1 - Finalizar
    */

    if (glb_IniciarFinalizar == 0) {
        //if (selParceiroID.value > 0) {
        //    // Inicia atendimento
        //    atender();
        //}
        //else {
        //Abre janela de atendimento   
        openModalAtendimento(0);
        //}
    }
    else if (glb_IniciarFinalizar == 1) {
        // abre modal de Atendimento no modo de finaliza��o de atendimento
        openModalAtendimento(1);
    }
    else
        throw "Status inv�lido do bot�o atendimento";
}

function btnListar_onclick() {
    selProdutoReferenciaID.disabled = true;

    if (selProdutoReferenciaID.selectedIndex <= 0) {
        if (window.top.overflyGen.Alert('Selecione um produto refer�ncia.') == 0)
            return null;

        return null;
    }
    else if (selTipoRelacaoID.selectedIndex <= 0) {
        if (window.top.overflyGen.Alert('Selecione um tipo de rela��o.') == 0)
            return null;

        return null;
    }
    else {
        glb_nListagemEspecifica = true;
        __btn_LIST(null);
    }
}


/********************************************************************
Funcao criada pelo programador.
Abre janela modal de cliente 

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalCliente() {
    var htmlPath;
    var strPars = "";
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + encodeURIComponent(nCurrEmpresa);
    strPars += '&sCaller=' + encodeURIComponent('PL');

    htmlPath = SYS_PAGESURLROOT + '/modcomercial/subatendimento/listaprecos/modalpages/modalcliente.asp' + strPars;
    showModalWin(htmlPath, new Array(671, 284));
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearchEx() {
    var strCustomFields = '';
    var nListaPreco = 0;
    var nChavePessoa = 0;
    var i;
    var sIsencoes = '';
    var sNomeIsencoes = '';
    var sCnae = '';

    // Tratamento que recupera as isen��es de combo MULTIPLE - DMC 04/02/2010
    for (i = 0; i < selIsencoesID.length; i++) {
        if (selIsencoesID.options[i].selected == true) {
            sIsencoes += (sIsencoes == '' ? '/' : '') + selIsencoesID.options[i].value + '/';
            sNomeIsencoes += (sNomeIsencoes == '' ? '' : ',') + selIsencoesID.options[i].innerText;
        }
    }

    sIsencoes = (sIsencoes == '' ? '' : sIsencoes + '');
    nChavePessoa = selChavePessoaID.value;
    sCnae = txtCnae.value;

    if (sCnae == null)
        sCnae = '';

    if (selParceiroID.selectedIndex > 0)
        nListaPreco = -selParceiroID.value;

    // Seta valores padr�es para pesqlistsvr
    var _lstPre_ParceiroID, _lstPre_ListaPreco, _lstPre_PessoaID, _lstPre_PessoaID2, _lstPre_ChavePessoaID, _lstPre_Classe, _lstPre_TipoPessoa, _lstPre_FaturamentoDireto,
        _lstPre_Contribuinte, _lstPre_SimplesNacional, _lstPre_Suframa, _lstPre_Cidade, _lstPre_Classificacoes, _lstPre_Cnae, _lstPre_Isencoes, _lstPre_UFID, _lstPre_Pais,
        _lstPre_Frete, _lstPre_MeioTransporteID, _lstPre_FinanciamentoID, _lstPre_Quantidade, _lstPre_MoedaConversaoID, _lstPre_Estoque, _lstPre_Lote, _lstPre_TransacaoID,
        _lstPre_FinalidadeID, _lstPre_ProdutoReferenciaID, _lstPre_TipoRelacaoID, _lstPre_FamiliaID, _lstPre_MarcaID, _lstPre_LinhaProdutoID, _lstPre_Cotador;

    _lstPre_Cotador = (selFonteID.value == 3 ? 1 : 0);
    strCustomFields += '&lstPre_Cotador=' + encodeURIComponent(_lstPre_Cotador);

    // Caso n�o esteja na fonte Cotador
    if (selFonteID.value != 3) {
        _lstPre_ParceiroID = (selParceiroID.selectedIndex > 0 ? selParceiroID.value : 0/*_lstPre_ParceiroID*/);
        _lstPre_ListaPreco = nListaPreco;
        _lstPre_PessoaID = (selPessoaID.selectedIndex >= 0 ? selPessoaID.value : 0/*_lstPre_PessoaID*/);
        _lstPre_ChavePessoaID = nChavePessoa;
        _lstPre_Classe = (selClassificacoesClaID.selectedIndex >= 0 ? selClassificacoesClaID.value : 0/*_lstPre_Classe*/);
        _lstPre_TipoPessoa = selTipoPessoa.value;
        _lstPre_FaturamentoDireto = (chkFaturamentoDireto.checked ? 1 : 0);
        _lstPre_Contribuinte = (chkContribuinte.checked ? 1 : 0);
        _lstPre_SimplesNacional = (chkSimplesNacional.checked ? 1 : 0);
        _lstPre_Suframa = (chkSuframaID.checked ? 1 : 0);
        _lstPre_Cidade = (selCidadeId.selectedIndex >= 0 ? selCidadeId.value : 0/*_lstPre_Cidade*/);
        _lstPre_Classificacoes = (selClassificacoesID.selectedIndex >= 0 ? selClassificacoesID.value : 0/*_lstPre_Classificacoes*/);
        _lstPre_Cnae = sCnae;
        _lstPre_Isencoes = sIsencoes;
        _lstPre_UFID = (selUFID.selectedIndex >= 0 ? selUFID.value : _lstPre_UFID);
        _lstPre_Pais = aEmpresa[1];
        _lstPre_Frete = 0;//selFrete.value;
        _lstPre_FinanciamentoID = (selFinanciamentoID.selectedIndex >= 0 ? selFinanciamentoID.value : 0/*_lstPre_FinanciamentoID*/);
        _lstPre_Quantidade = (trimStr(txtQuantidade.value) == '' ? 1 : txtQuantidade.value);
        _lstPre_MoedaConversaoID = selMoedaConversaoID.value;
        _lstPre_Estoque = (chkEstoque.checked ? 1 : 0);
        _lstPre_Lote = (selFonteID.value == 2 ? 1 : 0);
        _lstPre_TransacaoID = (selTransacao.selectedIndex >= 0 ? selTransacao.value : 0);
        _lstPre_FinalidadeID = (selFinalidade.selectedIndex >= 0 ? selFinalidade.value : 0);

        _lstPre_ProdutoReferenciaID = ((glb_nListagemEspecifica) ? selProdutoReferenciaID.value : 0);
        _lstPre_TipoRelacaoID = ((glb_nListagemEspecifica) ? selTipoRelacaoID.value : 0);

        _lstPre_FamiliaID = (((selFamiliaID.value >= 0) && (selFamiliaID.value != "")) ? selFamiliaID.value : 0);
        _lstPre_MarcaID = (((selMarcaID.value >= 0) && (selMarcaID.value != "")) ? selMarcaID.value : 0);
        _lstPre_LinhaProdutoID = (((selLinhaProdutoID.value >= 0) && (selLinhaProdutoID.value != "")) ? selLinhaProdutoID.value : 0);
        _lstPre_EmaClienteID = ((glb_nEmaClienteID != null && selChavePessoaID.value == 1) ? glb_nEmaClienteID : 0);

        // Regra transportada da pesqlistsvr
        // PessoaID2
        if (selPessoaID.selectedIndex >= 0)
            _lstPre_PessoaID2 = _lstPre_PessoaID;
        else if (parseInt(_lstPre_UFID) > 0)
            _lstPre_PessoaID2 = (parseInt(_lstPre_UFID) * -1).toString();
        else
            _lstPre_PessoaID2 = 0;

        // MeioTransporteID
        if ((parseInt(_lstPre_Frete) <= 1) /*|| (_lstPre_Frete == null)*/)
            _lstPre_MeioTransporteID = 0;
        else
            _lstPre_MeioTransporteID = _lstPre_Frete;

        // Se vendedor digitou argumento, ignora listagem dos produtos ofertados no email marketing
        if (trimStr(txtArgumento.value).length > 0)
            _lstPre_EmaClienteID = 0;

        strCustomFields += '&lstPre_ParceiroID=' + encodeURIComponent(_lstPre_ParceiroID);
        strCustomFields += '&lstPre_ListaPreco=' + encodeURIComponent(nListaPreco);
        strCustomFields += '&lstPre_PessoaID=' + encodeURIComponent(_lstPre_PessoaID);
        strCustomFields += '&lstPre_PessoaID2=' + encodeURIComponent(_lstPre_PessoaID2);

        // Parametros de Pre�o sem Pessoa - DMC 04/02/2010
        strCustomFields += '&lstPre_ChavePessoaID=' + encodeURIComponent(_lstPre_ChavePessoaID);
        strCustomFields += '&lstPre_Classe=' + encodeURIComponent(_lstPre_Classe);
        strCustomFields += '&lstPre_TipoPessoa=' + encodeURIComponent(_lstPre_TipoPessoa);
        strCustomFields += '&lstPre_FaturamentoDireto=' + encodeURIComponent(_lstPre_FaturamentoDireto);
        strCustomFields += '&lstPre_Contribuinte=' + encodeURIComponent(_lstPre_Contribuinte);
        strCustomFields += '&lstPre_SimplesNacional=' + encodeURIComponent(_lstPre_SimplesNacional);
        strCustomFields += '&lstPre_Suframa=' + encodeURIComponent(_lstPre_Suframa);
        strCustomFields += '&lstPre_Cidade=' + encodeURIComponent(_lstPre_Cidade);
        strCustomFields += '&lstPre_Classificacoes=' + encodeURIComponent(_lstPre_Classificacoes);
        strCustomFields += '&lstPre_Cnae=' + encodeURIComponent(_lstPre_Cnae);
        strCustomFields += '&lstPre_Isencoes=' + encodeURIComponent(_lstPre_Isencoes);
        strCustomFields += '&lstPre_Pais=' + encodeURIComponent(_lstPre_Pais);
        strCustomFields += '&lstPre_UFID=' + encodeURIComponent(_lstPre_UFID);
        strCustomFields += '&lstPre_Frete=' + encodeURIComponent(_lstPre_Frete);
        strCustomFields += '&lstPre_MeioTransporteID=' + encodeURIComponent(_lstPre_MeioTransporteID);
        strCustomFields += '&lstPre_FinanciamentoID=' + encodeURIComponent(_lstPre_FinanciamentoID);
        strCustomFields += '&lstPre_Quantidade=' + encodeURIComponent(_lstPre_Quantidade);
        strCustomFields += '&lstPre_MoedaConversaoID=' + encodeURIComponent(_lstPre_MoedaConversaoID);
        strCustomFields += '&lstPre_Estoque=' + encodeURIComponent(_lstPre_Estoque);
        strCustomFields += '&lstPre_Lote=' + encodeURIComponent(_lstPre_Lote);
        strCustomFields += '&lstPre_TransacaoID=' + encodeURIComponent(_lstPre_TransacaoID);
        strCustomFields += '&lstPre_FinalidadeID=' + encodeURIComponent(_lstPre_FinalidadeID);

        if (glb_nListagemEspecifica) {
            glb_nListagemEspecifica = false;
            strCustomFields += '&lstPre_ProdutoReferenciaID=' + encodeURIComponent(_lstPre_ProdutoReferenciaID);
            strCustomFields += '&lstPre_TipoRelacaoID=' + encodeURIComponent(_lstPre_TipoRelacaoID);
        }
        else {
            btnFindProduto_onclick(true);
            strCustomFields += '&lstPre_ProdutoReferenciaID=' + encodeURIComponent(_lstPre_ProdutoReferenciaID);
            strCustomFields += '&lstPre_TipoRelacaoID=' + encodeURIComponent(_lstPre_TipoRelacaoID);
        }

        strCustomFields += '&lstPre_FamiliaID=' + encodeURIComponent(_lstPre_FamiliaID);
        strCustomFields += '&lstPre_MarcaID=' + encodeURIComponent(_lstPre_MarcaID);
        strCustomFields += '&lstPre_LinhaProdutoID=' + encodeURIComponent(_lstPre_LinhaProdutoID);
        strCustomFields += '&lstPre_EmaClienteID=' + encodeURIComponent(_lstPre_EmaClienteID);
    }
    //Fonte cotador
    else {
        bSemPessoa = (selChavePessoaID.value == 2 ? true : false);

        _lstPre_ParceiroID = (selParceiroID.selectedIndex > 0 ? selParceiroID.value : 0/*_lstPre_ParceiroID*/);

        if (bSemPessoa)
            _lstPre_PessoaID = (glb_nCotadorPessoaID >= 0 ? glb_nCotadorPessoaID : 0);
        else
            _lstPre_PessoaID = (selPessoaID.selectedIndex >= 0 ? selPessoaID.value : 0);

        _lstPre_Pais = aEmpresa[1];
        _lstPre_Frete = 0;//selFrete.value;
        _lstPre_FinanciamentoID = (selFinanciamentoID.selectedIndex >= 0 ? selFinanciamentoID.value : 0/*_lstPre_FinanciamentoID*/);
        _lstPre_FinalidadeID = (selFinalidade.selectedIndex >= 0 ? selFinalidade.value : 0);
        _lstPre_Quantidade = (trimStr(txtQuantidade.value) == '' ? 1 : txtQuantidade.value);
        _lstPre_Estoque = (chkEstoque.checked ? 1 : 0);
        _lstPre_TransacaoID = (selTransacao.selectedIndex >= 0 ? selTransacao.value : 0);
        _lstPre_MarcaID = (((selMarcaID.value >= 0) && (selMarcaID.value != "")) ? selMarcaID.value : 0);
        _lstPre_LinhaProdutoID = (((selLinhaProdutoID.value >= 0) && (selLinhaProdutoID.value != "")) ? selLinhaProdutoID.value : 0);

        strCustomFields += '&lstPre_ParceiroID=' + encodeURIComponent(_lstPre_ParceiroID);
        strCustomFields += '&lstPre_PessoaID=' + encodeURIComponent(_lstPre_PessoaID);
        strCustomFields += '&lstPre_Pais=' + encodeURIComponent(_lstPre_Pais);
        strCustomFields += '&lstPre_Frete=' + encodeURIComponent(_lstPre_Frete);
        strCustomFields += '&lstPre_FinanciamentoID=' + encodeURIComponent(_lstPre_FinanciamentoID);
        strCustomFields += '&lstPre_FinalidadeID=' + encodeURIComponent(_lstPre_FinalidadeID);
        strCustomFields += '&lstPre_Quantidade=' + encodeURIComponent(_lstPre_Quantidade);
        strCustomFields += '&lstPre_Estoque=' + encodeURIComponent(_lstPre_Estoque);
        strCustomFields += '&lstPre_TransacaoID=' + encodeURIComponent(_lstPre_TransacaoID);
        strCustomFields += '&lstPre_MarcaID=' + encodeURIComponent(_lstPre_MarcaID);
        strCustomFields += '&lstPre_LinhaProdutoID=' + encodeURIComponent(_lstPre_LinhaProdutoID);

    }

    return strCustomFields;
}
/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function cotadorData() {
    var strParam = '';

    var sCurrency = glb_aCotacao[glb_aCotacao.length - 1];
    glb_aCotacao.pop();

    // Fonte Cotador para gera��o de proposta Estimate BOM ou Quote DID
    if ((selFonteID.value == 3) && (glb_aCotacao != null)) {
        for (j = 0; j < glb_aCotacao.length; j++) {
            if (j == 0) {
                strParam += 'CotadorPartNumber=' + encodeURIComponent(glb_aCotacao[j].PartNumber);
                strParam += (j == glb_aCotacao.length - 1) ? "" : ";";
            }
            else {
                strParam += '&CotadorPartNumber=' + encodeURIComponent(glb_aCotacao[j].PartNumber);
                strParam += (j == glb_aCotacao.length - 1) ? "" : ";";
            }
            strParam += '&CotadorLineNumber=' + encodeURIComponent(glb_aCotacao[j].LineNumber);
            strParam += (j == glb_aCotacao.length - 1) ? "" : ";";
            strParam += '&CotadorDescription=' + encodeURIComponent(glb_aCotacao[j].Description);
            strParam += (j == glb_aCotacao.length - 1) ? "" : ";";
            strParam += '&CotadorListPrice=' + encodeURIComponent(glb_aCotacao[j].ListPrice);
            strParam += (j == glb_aCotacao.length - 1) ? "" : ";";
            strParam += '&CotadorQuantity=' + encodeURIComponent(glb_aCotacao[j].Quantity);
            strParam += (j == glb_aCotacao.length - 1) ? "" : ";";
            strParam += '&CotadorDiscount=' + encodeURIComponent(glb_aCotacao[j].Discount);
            strParam += (j == glb_aCotacao.length - 1) ? "" : ";";
            strParam += '&ServiceDuration=' + encodeURIComponent(glb_aCotacao[j].ServiceDuration);
            strParam += (j == glb_aCotacao.length - 1) ? "" : ";";
        }
        strParam += '&QuoteBOM=' + glb_quoteBOM;
        strParam += '&QuoteMoeda=' + sCurrency;
    }
    glb_quoteBOM = null;

    return strParam;
}

/********************************************************************
Funcao criada pelo programador
Usuario clicou botao especifico de comprar

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function executePurchaseInModal() {
    var sErrorMsg = '';
    var bCotador = (selFonteID.value == 3 ? true : false);

    if ((glb_nPesAtendimentoID == null) || (glb_nPesAtendimentoID < 1)) {
        if (selChavePessoaID.value == 1) {
            window.top.overflyGen.Alert('O atendimento precisa estar iniciado.');
            return null;
        } else {
            window.top.overflyGen.Alert('Para inserir itens no carrinho, selecione uma pessoa.');
            return null;
        }
        return null;
    }

    if (fg.Rows == 1)
        sErrorMsg = 'Selecione um produto';
    else if (isNaN(txtQuantidade.value))
        sErrorMsg = 'Quantidade inv�lida';
    else if (parseInt(txtQuantidade.value) == 0)
        sErrorMsg = 'Quantidade inv�lida';
    else if (selMoedaConversaoID.selecteIndex == -1)
        sErrorMsg = 'Moeda inv�lida';
    // Quando for compra Sem Pessoa via Cotador deve-se ter a pessoa compat�vel grvada na variavel global
    else if ((selPessoaID.selectedIndex < 0) && !((bCotador) && (selChavePessoaID.value == 2) && (glb_nCotadorPessoaID > 0)))
        sErrorMsg = 'Nenhum cliente encontrado para simula��o';

    if (sErrorMsg != '') {
        if (window.top.overflyGen.Alert(sErrorMsg) == 0)
            return null;

        return null;
    }

    if (bCotador) {
        if (glb_bPaste) {
            var bOk = true;
            for (var i = 1; i < fg.Rows; i++) {
                if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Inconsistencia')).length > 0)
                    bOk = false;
            }
            if (bOk) {
                glb_bCotadorPadrao = false;
                sacolaAtual();
            }
            else {
                window.top.overflyGen.Alert('Produto(s) com inconsist�ncia, verifique por favor.');
                return null;
            }
        }
        else {
            for (var i = 1; i < fg.Rows; i++) {
                if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
                    if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Inconsistencia')).length == 0) {
                        glb_nRow = i;
                        glb_bCotadorPadrao = true;
                        sacolaAtual();
                        break;
                    }
                    else {
                        glb_bTemInconsistencia = true;
                    }
                }

                if (i == fg.Rows - 1) {

                    if (glb_bTemInconsistencia) {
                        window.top.overflyGen.Alert('Produto(s) com inconsist�ncia, verifique por favor.');
                        glb_bTemInconsistencia = false;
                        return null;
                    }

                    if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) == 0) {
                        window.top.overflyGen.Alert('Selecione um produto');
                        return null;
                    }
                }
            }
        }
    }
    else {
        for (var i = 1; i < fg.Rows; i++) {
            if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
                glb_nRow = i;
                prepareDataToClipBoard();
                purchaseItem('PL', glb_nRow);
                break;
            }

            if ((i == fg.Rows - 1) && (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) == 0)) {
                window.top.overflyGen.Alert('Selecione um produto');
                return null;
            }
        }
    }
}

/*
Inicia um novo atendimento a partir do lista de precos
*/
function setDadosParceirosFromSup(nI1, nI2, nI3, nI4, nI5, nI6, nI7, nI8, nI9, nI10) {
    glb_aDadosPessoa[0] = nI1;
    glb_aDadosPessoa[1] = nI2;
    glb_aDadosPessoa[2] = nI3;
    glb_aDadosPessoa[3] = nI4;
    glb_aDadosPessoa[4] = nI5;
    glb_aDadosPessoa[5] = nI6;
    glb_aDadosPessoa[6] = nI7;
    glb_aDadosPessoa[7] = nI8;
    glb_aDadosPessoa[8] = nI9;
    glb_aDadosPessoa[9] = nI10;

    window.top.focus();

    glb_sClipBoardBuffer = '';
    set_glb_IniciarFinalizarStatus(1);
    glb_TimerListaPrecoVar2 = window.setInterval('fillCmbParceiro()', 10, 'JavaScript');
    setTimerAgendamentoStatus(true);
}

function fg_KeyPressPesqList(KeyAscii) {
    if (KeyAscii == 13) {
        var Col = fg.Col;
        var Row = fg.Row;

        if (Col == getColIndexByColKey(fg, 'QuantidadeComprar')) {
            nQuantidade = fg.ValueMatrix(Row, Col);

            if ((!isNaN(nQuantidade)) && nQuantidade > 0) {
                executePurchaseInModal();
                return null;
            }
        }
    }
}

function fg_DblClick_Especific() {

    var nMCIndex = getColIndexByColKey(fg, 'MC');
    var nQuantidadeComprarIndex = getColIndexByColKey(fg, 'QuantidadeComprar');
    var nGuiaIndex = getColIndexByColKey(fg, 'Guia');
    var nICMSSTIndex = getColIndexByColKey(fg, 'ICMSST');

    if (fg.Col == getColIndexByColKey(fg, 'OK')) {
        if (!glb_bPaste) {
            for (var i = 1; i < fg.Rows; i++) {
                fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = (glb_EnableChkOK ? 1 : 0);
            }
            glb_EnableChkOK = !glb_EnableChkOK;
        }
        return;
    }


    if ((((fg.Col != nMCIndex) && (fg.Col != nGuiaIndex) && (fg.Col != nICMSSTIndex) && (fg.Col != nQuantidadeComprarIndex)) || (fg.Rows <= 1)) && (selFonteID.value != 3)) {
        fg_DblClick();
        return null;
    }
    else if ((fg.Col == nGuiaIndex) || (fg.Col == nICMSSTIndex)) {
        CalculaICMSST(fg.Row);
    }
    else {
        if ((fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'MC')) != '') || (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Preco')) != '')) {
            if (glb_bAtualizaMargem) {
                if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Preco')) == '')
                    glb_bAtualizaMargem = false;
            }
            else {
                if ((fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'MC')) == '') || (fg.CoL == nMCIndex))
                    glb_bAtualizaMargem = true;
            }
        }

        glb_RowEdit = fg.Row;

        if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Preco')) > 0)
            atualizaValores();
    }
}

function atualizaValores() {
    var nQuantidade;
    var nQuantidadeComprar;
    var nPrecoLista;
    var nMargemContribuicao;
    var nSujeitoID;
    var nObjetoID;
    var nParceiroID;
    var nPessoaID;
    var nPessoaID2;
    var lstPre_FinanciamentoID;
    var sCFOP;
    var nTransacao;
    var nFinalidade;
    var nIncluiFrete;
    var sMeioTransporteID;
    var lstPre_MoedaConversaoID;
    var lstPre_ImpostosIncidencia;
    var lstPre_ListaPreco;
    var nIncluiFrete;
    var sSQL;
    var sSQLMC;
    var sSQLEstoqueDisp1;
    var sSQLEstoqueDisp2;
    var sSQLEstoqueReserva;
    var sSQLDisponibilidade;
    var aEmpresa;
    var nEmpresaID;
    var nIdiomaID;
    var nClasse;
    var nTipoPessoa;
    var nFaturamentoDireto;
    var nContribuinte;
    var nSimplesNacional;
    var nSuframa;
    var nCidade;
    var nClassificacoes;
    var nCnae;
    var nIsencoes;
    var nPais;
    var nLotPedItemID;
    var sSQLProp = "";
    var GAREGNRE = "";
    var nProdutoID;
    var nNCM;
    var nPrecoFob;
    var nPrecoFobDesc;
    var nDesconto;
    var bCotador;
    var nMoedaID;
    var sTipoProdutoID;
    var nTipoProdutoID;
    var nMarcaID;
    var nTipoResultadoID;

    nQuantidade = '1';
    nQuantidadeComprar = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'QuantidadeComprar'));
    nSujeitoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'EmpresaID'));
    nObjetoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ConceitoID*'));
    nPessoaID = '';
    nPessoaID2 = 'NULL';
    nParceiroID = 'NULL';
    lstPre_FinanciamentoID = (selFinanciamentoID.selectedIndex >= 0 ? selFinanciamentoID.value : 0);
    nTransacao = (selTransacao.selectedIndex >= 0 ? selTransacao.value : 'NULL');
    nFinalidade = (selFinalidade.selectedIndex >= 0 ? selFinalidade.value : 'NULL');
    nIncluiFrete = 0;
    sMeioTransporteID = '';
    lstPre_MoedaConversaoID = selMoedaConversaoID.value;
    lstPre_ImpostosIncidencia = 'NULL';
    lstPre_ListaPreco = 0;
    nIncluiFrete = 0;//(selFrete.value == 0 ? 0 : 1);
    aEmpresa = getCurrEmpresaData();
    nEmpresaID = getCurrentEmpresaID();
    nLotPedItemID = (getColIndexByColKey(fg, 'Lote*') > 0 ? fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Lote*')) : 'NULL');
    bCotador = (selFonteID.value == 3 ? true : false);
    nTipoResultadoID = (bCotador ? 3 : 1);
    bSemPessoa = (selChavePessoaID.value == 2 ? true : false);

    if (selChavePessoaID.value == 2) {
        nClasse = encodeURIComponent(selClassificacoesClaID.selectedIndex >= 0 ? selClassificacoesClaID.value : 0);
        nTipoPessoa = encodeURIComponent(selTipoPessoa.value);
        nFaturamentoDireto = encodeURIComponent(chkFaturamentoDireto.checked ? 1 : 0);
        nContribuinte = encodeURIComponent(chkContribuinte.checked ? 1 : 0);
        nSimplesNacional = encodeURIComponent(chkSimplesNacional.checked ? 1 : 0);
        nSuframa = encodeURIComponent(chkSuframaID.checked ? 1 : 0);
        nUFID = selUFID.value;
        nCidade = encodeURIComponent(selCidadeId.selectedIndex >= 0 ? selCidadeId.value : 0);
        nClassificacoes = encodeURIComponent(selClassificacoesID.selectedIndex >= 0 ? selClassificacoesID.value : 0);
        nCnae = txtCnae.value;
        nIsencoes = selIsencoesID.value;
        nPais = encodeURIComponent(aEmpresa[1]);
    }
    else {
        nClasse = 'NULL';
        nTipoPessoa = 'NULL';
        nFaturamentoDireto = 'NULL';
        nContribuinte = 'NULL';
        nSimplesNacional = 'NULL';
        nSuframa = 'NULL';
        nUFID = 'NULL';
        nCidade = 'NULL';
        nClassificacoes = 'NULL';
        nCnae = 'NULL';
        nIsencoes = 'NULL';
        nPais = 'NULL';
    }

    if (nCnae == "")
        nCnae = 'NULL';

    if (nIsencoes == "")
        nIsencoes = 'NULL';

    nPrecoLista = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Preco'));
    nMargemContribuicao = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'MC'));

    if (!isNaN(txtQuantidade.value))
        nQuantidade = txtQuantidade.value;

    if ((!isNaN(selPessoaID.value)) && (selPessoaID.value > 0)) {
        nPessoaID = selPessoaID.value;
        nPessoaID2 = selPessoaID.value;
        nParceiroID = (selParceiroID.value > 0 ? selParceiroID.value : 'NULL');
        if (nParceiroID != 'NULL')
            lstPre_ListaPreco = -nParceiroID;
    }
    else
        nPessoaID = 'NULL';

    nIncluiFrete = 0;
    sMeioTransporteID = "NULL";

    if (selChavePessoaID.value == 1) {
        sCFOP = "dbo.fn_EmpresaPessoaTransacao_CFOP(" + nSujeitoID + "," + nPessoaID2 + ", NULL, NULL, NULL, NULL, NULL, " + nTransacao + "," + nFinalidade + "," + nObjetoID + ", NULL)";
    }
    else {
        sCFOP = "dbo.fn_EmpresaPessoaTransacao_CFOP(" + nSujeitoID + "," + nPessoaID2 + "," + nPais + "," + nContribuinte + "," + nSuframa + "," + nUFID + "," + nIsencoes + "," + nTransacao + "," + nFinalidade + "," + nObjetoID + ", NULL)";
    }

    nIdiomaID = aEmpresa[8];

    // mandar os parametros para o servidor
    // Esta modal fica sempre carregada e o que identifica
    // isto e o terceiro parametro da funcao showModalWin abaixo.
    // No strPars, o primeiro parametro e obrigatorio e e o UNICO!!!
    // que pode ser mandado

    setConnection(dsoCamposExtras);
    //setConnection(dsoProposta);
    //setConnection(dsoAtualizaGridCotador);

    if (nLotPedItemID == 0)
        nLotPedItemID = 'NULL';

    if (bCotador) {
        nEmpresaID = fg.ValueMatrix(glb_RowEdit, getColIndexByColKey(fg, 'EmpresaID'));
        nCorpProdutoID = fg.ValueMatrix(glb_RowEdit, getColIndexByColKey(fg, 'CorpProdutoID'));
        nPrecoFob = fg.ValueMatrix(glb_RowEdit, getColIndexByColKey(fg, 'PrecoFob'));
        nDesconto = fg.ValueMatrix(glb_RowEdit, getColIndexByColKey(fg, 'Desconto'));
        nMoedaID = fg.ValueMatrix(glb_RowEdit, getColIndexByColKey(fg, 'MoedaID'));  //(aEmpresa[1] == 130 ? 647 : 541);
        nNCM = fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'NCM'));
        sTipoProduto = fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'LinhaProduto'));
        nTipoProdutoID = (sTipoProduto == 'Hardware' ? 1132 : sTipoProduto == 'Software' ? 1133 : 1134);
        nMarcaID = selMarcaID.value;
        sOrigem = fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'Origem'));
        nOrigemID = fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'OrigemID'));
        sCFOP = fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'CFOP'));

        if (bSemPessoa) {
            nPessoaID = glb_nCotadorPessoaID;
            nParceiroID = (selParceiroID.value > 0 ? selParceiroID.value : 'NULL');
            nClassificacoes = 'NULL';
            nFaturamentoDireto = 'NULL';
            nContribuinte = 'NULL';
            nSimplesNacional = 'NULL';
            nUFID = 'NULL';
            nCidade = 'NULL';
            nCnae = 'NULL';
            nTipoPessoa = 'NULL';
        }

        if (nNCM.length == 0)
            nNCM = 'NULL';
        else
            nNCM = "'" + nNCM + "'";

        if (sCFOP.length == 0)
            sCFOP = 'NULL';
    }

    if (glb_bAtualizaMargem) {
        sSQL = "SELECT dbo.fn_Preco_MargemLista(" + nSujeitoID + ", " +
            (bCotador && nObjetoID == 0 ? "dbo.fn_Cotador_ProdutoCompativel(" + nSujeitoID + "," + nNCM + "," + nTipoProdutoID + "," + nMarcaID + "," + nOrigemID + ")" : nObjetoID) + ", " +
            nQuantidade + ", " + nPrecoLista + ", NULL, NULL, " +
            "dbo.fn_Produto_Aliquota(" + nSujeitoID + ", " +
            (bCotador && nObjetoID == 0 ? "dbo.fn_Cotador_ProdutoCompativel(" + nSujeitoID + "," + nNCM + "," + nTipoProdutoID + "," + nMarcaID + "," + nOrigemID + ")" : nObjetoID) + ", " +

            nPessoaID + "," + nContribuinte + "," + nUFID + "," + nCidade + ", NULL, " + sCFOP + "), " +
            lstPre_MoedaConversaoID + ", GETDATE(), " + lstPre_ImpostosIncidencia + ", " + nPessoaID2 + ", " + nClassificacoes + ", " + nFaturamentoDireto + ", " +
            nContribuinte + ", " + nSimplesNacional + ", " + nUFID + ", " + nCidade + ", " + nCnae + ", " + nTipoPessoa + ", " + nParceiroID + ", " +
            lstPre_FinanciamentoID + ", " + nIncluiFrete + ", " + sMeioTransporteID + ", NULL, " + nFinalidade + "," + sCFOP + "," + nTipoResultadoID + ", " +
            nLotPedItemID + ", " +
            (bCotador ? "dbo.fn_Cotador_CustoReposicao(" + nEmpresaID + "," + nCorpProdutoID + ", " + nDesconto + ", 1, NULL))" : "NULL)") + " AS MC";
    }
    else if ((bCotador) && (!glb_bAtualizaMargem)) {
        nProdutoID = fg.ValueMatrix(glb_RowEdit, getColIndexByColKey(fg, 'ConceitoID'));
        if (nProdutoID == '')
            nProdutoID = 'NULL';

        if (glb_bAtualizaDesconto) {
            nPrecoFobDesc = nPrecoFob * (1 - (nDesconto / 100));
            fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'PrecoFobDesc')) = nPrecoFobDesc;
        }
        else
            nPrecoFobDesc = fg.ValueMatrix(glb_RowEdit, getColIndexByColKey(fg, 'PrecoFobDesc'));

        sSQL = "DECLARE @OrigemID INT, @MoedaID INT, @TipoProdutoID INT, @ICMSST NUMERIC(11,2), @TipoPagamentoST INT, @Preco NUMERIC(11,2), @PrecoFobDesc NUMERIC(13,2) " +
            "SELECT @OrigemID = OrigemID, @OrigemID = OrigemID, @MoedaID = MoedaID, @TipoProdutoID = TipoProdutoID, " +
            "@PrecoFobDesc = dbo.fn_Cotador_CustoReposicao(" + nSujeitoID + ", CorpProdutoID, " + nDesconto + ", 0, NULL) " +
            "FROM CorpProdutos WITH(NOLOCK) " +
            "WHERE CorpProdutoID = " + fg.ValueMatrix(glb_RowEdit, getColIndexByColKey(fg, 'CorpProdutoID'));

        sSQL += " EXEC dbo.sp_Preco_Corporativo " + nProdutoID + ", " + nNCM + ", " + selMarcaID.value + ", @OrigemID," + (aEmpresa[1] == 130 ? "647" : "541") + "," + nQuantidadeComprar + ", " +
            nPessoaID + ", " + nParceiroID + ", " + nFinalidade + ", 0, " + '@PrecoFobDesc' + ", " + (nMargemContribuicao == 0 ? "NULL" : nMargemContribuicao) + " , " + lstPre_FinanciamentoID + ", " +
            nEmpresaID + ", " + " @TipoProdutoID, 1, NULL, NULL, @ICMSST OUTPUT, NULL, NULL, @TipoPagamentoST OUTPUT, NULL, @Preco OUTPUT, NULL, NULL, NULL, NULL, NULL " +

            "SELECT ISNULL(@ICMSST,0) AS ICMSST, CASE @TipoPagamentoST WHEN '1' THEN 'GARE' WHEN '2' THEN 'GNRE' ELSE '' END AS Guia, ISNULL(@Preco,0) AS Preco, ";

        sSQL += "dbo.fn_Preco_MargemLista(" + nSujeitoID + ", " +
            (bCotador && nObjetoID == 0 ? "dbo.fn_Cotador_ProdutoCompativel(" + nSujeitoID + "," + nNCM + "," + nTipoProdutoID + "," + nMarcaID + "," + nOrigemID + ")" : nObjetoID) + ", " +
            nQuantidade + ", " + "@Preco" + ", NULL, NULL, " +
            "dbo.fn_Produto_Aliquota(" + nSujeitoID + ", " +
            (bCotador && nObjetoID == 0 ? "dbo.fn_Cotador_ProdutoCompativel(" + nSujeitoID + "," + nNCM + "," + nTipoProdutoID + "," + nMarcaID + "," + nOrigemID + ")" : nObjetoID) + ", " +
            nPessoaID + "," + nContribuinte + "," + nUFID + "," + nCidade + ", NULL, " + sCFOP + "), " +
            lstPre_MoedaConversaoID + ", GETDATE(), " + lstPre_ImpostosIncidencia + ", " + nPessoaID2 + ", " + nClassificacoes + ", " + nFaturamentoDireto + ", " +
            nContribuinte + ", " + nSimplesNacional + ", " + nUFID + ", " + nCidade + ", " + nCnae + ", " + nTipoPessoa + ", " + nParceiroID + ", " +
            lstPre_FinanciamentoID + ", " + nIncluiFrete + ", " + sMeioTransporteID + ", NULL, " + nFinalidade + "," + sCFOP + "," + nTipoResultadoID + ", " +
            nLotPedItemID + ", " +
            (bCotador ? "dbo.fn_Cotador_CustoReposicao(" + nEmpresaID + "," + nCorpProdutoID + ", " + nDesconto + ", 1, NULL))" : "NULL)") + " AS MC";
    }
    else {
        nPrecoListaAjust = "ISNULL(dbo.fn_Preco_PrecoLista(" + nSujeitoID + "," + nObjetoID + ", NULL, " +
            "dbo.fn_Produto_Aliquota(" + nSujeitoID + ", " + nObjetoID + "," + nPessoaID + "," + nContribuinte + "," + nUFID + "," + nCidade + ", NULL, " + sCFOP + "), " +
            lstPre_MoedaConversaoID + ", GETDATE(), " + lstPre_ListaPreco + ", " + lstPre_ImpostosIncidencia + ", " +
            nQuantidade + ", " + "0," + nPessoaID2 + "," + nParceiroID + "," + nFaturamentoDireto + "," + nContribuinte + "," + nSimplesNacional + "," +
            nSuframa + "," + nUFID + "," + nCidade + "," + nCnae + "," + nIsencoes + ", 1," + nClasse + "," + nClassificacoes + "," + nTipoPessoa + ", NUll,  " + nMargemContribuicao + ", " + lstPre_FinanciamentoID + ", " + nIncluiFrete + ", " +
            sMeioTransporteID + ", NULL, " + nFinalidade + "," + sCFOP + "," + nLotPedItemID + "),0)";

        sSQL = "SELECT " + nPrecoListaAjust + " AS Preco, ";

        sSQL += "dbo.fn_Preco_MargemLista(" + nSujeitoID + ", " + nObjetoID + ", " + nQuantidade + ", " + nPrecoListaAjust + ", NULL, NULL, " +
            "dbo.fn_Produto_Aliquota(" + nSujeitoID + ", " + nObjetoID + "," + nPessoaID + "," + nContribuinte + "," + nUFID + "," + nCidade + ", NULL, " + sCFOP + "), " +
            lstPre_MoedaConversaoID + ", GETDATE(), " + lstPre_ImpostosIncidencia + ", " + nPessoaID2 + ", " + nClassificacoes + ", " + nFaturamentoDireto + ", " +
            nContribuinte + ", " + nSimplesNacional + ", " + nUFID + ", " + nCidade + ", " + nCnae + ", " + nTipoPessoa + ", " + nParceiroID + ", " +
            lstPre_FinanciamentoID + ", " + nIncluiFrete + ", " + sMeioTransporteID + ", NULL, " + nFinalidade + "," + sCFOP + ", 1, " + nLotPedItemID + ", NULL) AS MC ";
    }

    if (bCotador)
        sSQL += " , STR(dbo.fn_Produto_Estoque(" + nSujeitoID + ", " + nObjetoID + ", -356, " + nSujeitoID + ", NULL, NULL,375, " + nLotPedItemID + " ),6,0) AS Disponivel ";
    else
        sSQL += " , STR(dbo.fn_Produto_Estoque(" + nSujeitoID + ", " + nObjetoID + ", -356, " + nSujeitoID + ", NULL, NULL,375, " + nLotPedItemID + " ),6,0) AS Disponivel1, " +
            "STR(dbo.fn_Produto_Estoque(" + nSujeitoID + ", " + nObjetoID + ", -356, -1, NULL, NULL,375, " + nLotPedItemID + " ),6,0) AS Disponivel2, " +
            "STR(dbo.fn_Produto_Estoque(" + nSujeitoID + ", " + nObjetoID + ", 353, NULL, NULL, NULL,375, " + nLotPedItemID + "  ),6,0) AS Reserva, " +
            "dbo.fn_Produto_PrevisaoEntregaContratual(" + nObjetoID + "," + nSujeitoID + ", " + nQuantidadeComprar + ", " + nLotPedItemID + ", 0) AS PrevisaoEntrega " +
            // "dbo.fn_Produto_Disponibilidade(" + nSujeitoID + "," + nObjetoID + ",NULL,GETDATE(),NULL, " + nIdiomaID + ",1) AS dtPrevisaoDisponibilidade " +
            "OPTION (MAXDOP 8)";

    dsoCamposExtras.SQL = sSQL;
    dsoCamposExtras.ondatasetcomplete = dsoCamposExtras_DSC;
    dsoCamposExtras.refresh();
}

function dsoCamposExtras_DSC() {
    var nRow = glb_RowEdit;
    var aEmpresaData = getCurrEmpresaData();
    var bCotador = (selFonteID.value == 3 ? true : false);

    if ((nRow > 0) && (!((dsoCamposExtras.recordset.BOF) && (dsoCamposExtras.recordset.EOF)))) {
        if (!glb_bAtualizaMargem) {
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Preco')) = dsoCamposExtras.recordset['Preco'].value;
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'MC')) = dsoCamposExtras.recordset['MC'].value;
            //var bgColorRed = 0X000000;
            //fg.Cell(7, nRow, getColIndexByColKey(fg, 'MC'), nRow, getColIndexByColKey(fg, 'MC')) = bgColorRed;
        }
        else {
            // if ((!selFrete.disabled) && (dsoCamposExtras.recordset['MC'].value == null))
            if (dsoCamposExtras.recordset['MC'].value == null)
                window.top.overflyGen.Alert('N�o � poss�vel calcular a MC.'); // para esta modalidade de frete.');
            else {
                fg.TextMatrix(nRow, getColIndexByColKey(fg, 'MC')) = dsoCamposExtras.recordset['MC'].value;

                // Pintar Preco de vermelho caso MC fique abaixo da MargemMinima
                if ((glb_nA1 == 0) || (glb_nA2 == 0)) {
                    var nMC = parseFloat(replaceStr(fg.TextMatrix(nRow, getColIndexByColKey(fg, 'MC')), ',', '.'));
                    var nMargemMinima = parseFloat(replaceStr(fg.TextMatrix(nRow, getColIndexByColKey(fg, 'MargemMinima')), ',', '.'));


                    if (nMC >= nMargemMinima)
                        fg.Cell(6, nRow, getColIndexByColKey(fg, 'Preco'), nRow, getColIndexByColKey(fg, 'Preco')) = 0XFFFFFF;//branco
                    else
                        fg.Cell(6, nRow, getColIndexByColKey(fg, 'Preco'), nRow, getColIndexByColKey(fg, 'Preco')) = 0X0000FF;//vermelho
                }
            }
        }


        if ((bCotador) && (!glb_bAtualizaMargem)) {
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Disponivel')) = dsoCamposExtras.recordset['Disponivel'].value;

            if (dsoCamposExtras.recordset['Preco'].value == null)
                fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'Preco')) = 0;
            else
                fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'Preco')) = dsoCamposExtras.recordset['Preco'].value;

            if (dsoCamposExtras.recordset['ICMSST'].value == null)
                fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'ICMSST')) = 0;
            else
                fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'ICMSST')) = dsoCamposExtras.recordset['ICMSST'].value;

            if (dsoCamposExtras.recordset['Guia'].value == null)
                fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'Guia')) = '';
            else
                fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'Guia')) = dsoCamposExtras.recordset['Guia'].value;

            glb_bAtualizaDesconto = false;
        }
        else if (!bCotador) {
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Disponivel1*')) = dsoCamposExtras.recordset['Disponivel1'].value;
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Disponivel2*')) = dsoCamposExtras.recordset['Disponivel2'].value;
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Reserva*')) = dsoCamposExtras.recordset['Reserva'].value;
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'PrevisaoEntrega*')) = dsoCamposExtras.recordset['PrevisaoEntrega'].value;
        }
    }
    else if (nRow > 0) {
        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Preco')) = '';
        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'MC')) = '';
        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Reserva*')) = '';
        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'PrevisaoEntrega*')) = '';

        if (bCotador)
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Disponivel*')) = '';
        else {
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Disponivel1*')) = '';
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Disponivel2*')) = '';
        }
    }

    if ((aEmpresaData[1] == 130) && (!bCotador)) {
        CalculaICMSST(nRow);
    }
}

function dsoProposta_DSC() {
    var LineNumber;
    var vMC;
    var Quant;

    if ((!dsoProposta.recordset.BOF) && (!dsoProposta.recordset.EOF)) {
        for (y = 1; y < fg.Rows; y++) {
            dsoProposta.recordset.moveFirst();
            dsoProposta.recordset.Filter = 'LineNumber = \'' + y + '\'';

            while (!dsoProposta.recordset.EOF) {
                LineNumber = dsoProposta.recordset['LineNumber'].value;
                vMC = ((dsoProposta.recordset['MC'].value == null) ? 0 : dsoProposta.recordset['MC'].value);
                Quant = dsoProposta.recordset['QUANT'].value;

                fg.TextMatrix(parseInt(LineNumber), getColIndexByColKey(fg, 'MC')) = vMC;
                fg.TextMatrix(parseInt(LineNumber), getColIndexByColKey(fg, 'QuantidadeComprar')) = Quant;

                dsoProposta.recordset.moveNext();
            }
            dsoProposta.recordset.Filter = '';
        }
    }


    lockInterface(false);
}

function js_PesqList_AfterEditListaPreco(nRow, nCol) {
    if (selFonteID.value == 3) {
        if (glb_bPaste && fg.TextMatrix(nRow, getColIndexByColKey(fg, 'OK')) == 0) {
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'OK')) = 1;
        }

        if (nCol == getColIndexByColKey(fg, 'Desconto'))
            glb_bAtualizaDesconto = true;
        else
            glb_bAtualizaDesconto = false;

        if (nCol == getColIndexByColKey(fg, 'Preco') || nCol == getColIndexByColKey(fg, 'Desconto') || nCol == getColIndexByColKey(fg, 'MC')) {
            var sPartNumber, nQntComprar, nPreco, nLineNumber, nPadrao, nDesconto, nMoedaID, nCorpProdutoID, nEmpresaID;

            sPartNumber = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'PartNumber'));
            nQntComprar = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'QuantidadeComprar'));
            nCorpProdutoID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'CorpProdutoID'));
            nEmpresaID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'EmpresaID'));

            nDesconto = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Desconto')).replace(',', '.');

            if (glb_bPaste) {
                nLineNumber = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'LineNumber'));
                nPreco = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'PrecoFob')).replace(',', '.');
                nMoedaID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'MoedaID')).replace(',', '.');
                nPadrao = "2";
            }
            else {
                nLineNumber = "NULL";
                nPreco = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Preco')).replace(',', '.');
                nMoedaID = 'NULL';
                nPadrao = "1";
            }

            if (((nCol == getColIndexByColKey(fg, 'MC')) || (glb_bAtualizaDesconto)) /*|| (nCol == getColIndexByColKey(fg, 'Preco') && fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'MC')) > 0)*/)
                glb_bAtualizaMargem = false;
            else
                glb_bAtualizaMargem = true;

            setConnection(dsoInconsistencia);

            dsoInconsistencia.SQL = "SELECT dbo.fn_Cotador_Inconsistencias('" + sPartNumber + "'," + nQntComprar + "," + (nPreco == "" ? "NULL" : nPreco) + ",NULL," + nLineNumber + "," + nDesconto + "," + nMoedaID + "," + nCorpProdutoID + "," + nEmpresaID + ",NULL," + nPadrao + ",0) AS Inconsistencia";

            dsoInconsistencia.ondatasetcomplete = inconsistencia_DSC;
            dsoInconsistencia.refresh();
        }

        glb_RowEdit = nRow;
    }
    else {
        var nQuantidade = 0;
        var ICMSST, QuantidadeComprar;

        ICMSST = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'ICMSST_Unitario'));
        QuantidadeComprar = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'QuantidadeComprar'));

        glb_RowEdit = nRow;
        //CalculaICMSST(nRow);

        if ((fg.TextMatrix(nRow, getColIndexByColKey(fg, 'MC')) != '') || (fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Preco')) != '')) {
            if (nCol == getColIndexByColKey(fg, 'Preco')) {
                if (fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Preco')) == '')
                    glb_bAtualizaMargem = false;
                else
                    glb_bAtualizaMargem = true;

                // Grava pre�o digitado e limpa margem
                fg.TextMatrix(nRow, getColIndexByColKey(fg, 'PrecoDigitado')) = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Preco'));
                fg.TextMatrix(nRow, getColIndexByColKey(fg, 'MargemDigitada')) = '';
                fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Guia')) = '';
                fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ICMSST')) = '';
            }
            else if (nCol == getColIndexByColKey(fg, 'MC')) {
                if (fg.TextMatrix(nRow, getColIndexByColKey(fg, 'MC')) == '')
                    glb_bAtualizaMargem = true;
                else
                    glb_bAtualizaMargem = false;

                // Grava margem digitada e limpa pre�o
                fg.TextMatrix(nRow, getColIndexByColKey(fg, 'MargemDigitada')) = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'MC'));
                fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Guia')) = '';
                fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ICMSST')) = '';
                fg.TextMatrix(nRow, getColIndexByColKey(fg, 'PrecoDigitado')) = '';
                //CalculaICMSST(fg.nRow);
            }
            if ((ICMSST != 0) && (QuantidadeComprar > 0)) {
                fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ICMSST')) = (ICMSST * QuantidadeComprar);
            }
        }
    }

    if (nCol == getColIndexByColKey(fg, 'OK'))
        return;

    if (nCol == getColIndexByColKey(fg, 'QuantidadeComprar')) {
        if (fg.TextMatrix(nRow, getColIndexByColKey(fg, 'QuantidadeComprar')) == 0) {
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'OK')) = 0;
        }
        else {
            fg.TextMatrix(nRow, getColIndexByColKey(fg, 'OK')) = 1;
        }
        return;
    }

    if (nPreco != "")
        atualizaValores();
}

function inconsistencia_DSC() {
    var yellow = 0x00ffff;

    /*&& fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'Inconsistencia')).length > 0 &&
    fg.Cell(6, glb_RowEdit, getColIndexByColKey(fg, 'Produto'), glb_RowEdit, getColIndexByColKey(fg, 'Produto')) == yellow)*/
    if (fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'Inconsistencia')) != dsoInconsistencia.recordset.aRecords[0][0]) {
        fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'Inconsistencia')) = (dsoInconsistencia.recordset.aRecords[0][0] == null ? "" : dsoInconsistencia.recordset.aRecords[0][0]);
        pintaGrid();
    }
}

function set_glb_IniciarFinalizarStatus(status) {
    if (status != null) {
        glb_IniciarFinalizar = status;
    }

    //btnIniciarFinalizar.value = (status == 0 ? "Iniciar" : "Finalizar");
    //btnIniciarFinalizar.title = (status == 0 ? "Iniciar atendimento" : "Finalizar atendimento");

    if (status == 0) {
        glb_nPesAtendimentoID = null;
    }

    selParceiroID.disabled = (status == 1);
    btnFindCliente.disabled = (status == 1);
}

function atender() {
    if (glb_TimerModalClienteRetorno != null) {
        window.clearInterval(glb_TimerModalClienteRetorno);
        glb_TimerModalClienteRetorno = null;
    }

    var sAlert = '';
    var sdtAgendamento = '';
    var nPessoaID = selParceiroID.value;
    var empresaData = getCurrEmpresaData();
    glb_nTipoAtendimentoID = 282; //282-Receptivo

    strPars = '?nPessoaID=' + encodeURIComponent(nPessoaID);
    strPars += '&nStatus=' + encodeURIComponent('0');
    strPars += '&nEmpresaID=' + encodeURIComponent(empresaData[0]);
    strPars += '&nUserID=' + encodeURIComponent(getCurrUserID());
    strPars += '&nTipoAtendimentoID=' + encodeURIComponent(glb_nTipoAtendimentoID);

    try {
        dsoGravarAgendamento.URL = SYS_ASPURLROOT + '/serversidegenEx/gravaragendamento.aspx' + strPars;
        dsoGravarAgendamento.ondatasetcomplete = atender_DSC;
        dsoGravarAgendamento.refresh();
    }
    catch (e) {
        lockInterface(false);
        if (window.top.overflyGen.Alert('N�o foi poss�vel iniciar o atendimento, tente novamente.') == 0)
            return null;
    }
}

function atender_DSC() {
    var dtStartAtendimento = new Date();

    set_glb_IniciarFinalizarStatus(1);

    try {
        glb_nPesAtendimentoID = dsoGravarAgendamento.recordset.Fields['PesAtendimentoID'].value;
        glb_dtAtendimentoInicio = dtStartAtendimento.getTime();
        setTimerAgendamentoStatus(true);
    }
    catch (e) {
        if (window.top.overflyGen.Alert('N�o foi poss�vel iniciar o atendimento, tente novamente.') == 0)
            return null;
    }
}

function finalizarAtendimento() {
    var sdtAgendamento = '';

    lockInterface(true);

    strPars = '?nStatus=' + encodeURIComponent('1');
    strPars += '&nMeioAtendimentoID=' + encodeURIComponent('');
    strPars += '&sAtendimentoOK=' + encodeURIComponent(1);
    strPars += '&sObservacoes=' + encodeURIComponent('');
    strPars += '&sdtAgendamento=' + encodeURIComponent('');
    strPars += '&nPesAtendimentoID=' + encodeURIComponent(glb_nPesAtendimentoID);
    strPars += '&sProdutos=' + encodeURIComponent(glb_sClipBoardBuffer);

    try {
        dsoGravarAgendamento.URL = SYS_ASPURLROOT + '/serversidegenEx/gravaragendamento.aspx' + strPars;
        dsoGravarAgendamento.ondatasetcomplete = finalizarAtendimento_DSC;
        dsoGravarAgendamento.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('N�o foi poss�vel finalizar o atendimento, tente novamente.') == 0)
            return null;

        lockInterface(false);
    }
}

function finalizarAtendimento_DSC() {
    lockInterface(false);

    setTimerAgendamentoStatus(false);

    //destrava combo parceiro e selecione a op��o -1
    delOptCmbParceiro();
    set_glb_IniciarFinalizarStatus(0);
    glb_nPesAtendimentoID = null;
    glb_bCotadorCompra = false;
    glb_sClipBoardBuffer = '';
    selParceiroID.value = -1;
    selParceiroID_OnChange();
}

/*
Inicia/Finaliza o timer que controla o tempo de um atendimento
Parametros: bInicia: 1-Inicia, 0-Finaliza
*/
function setTimerAgendamentoStatus(bInicia) {
    if (glb_TimerTempodecorrido != null) {
        window.clearInterval(glb_TimerTempodecorrido);
        glb_TimerTempodecorrido = null;
    }

    //Verificar com Marco por que esta return null. DCS 22/03/2010 16:46
    //return null;

    if (bInicia)
        glb_AtendimentoTimer = window.setInterval('atualizaLabelTimer()', 1000, 'JavaScript');
    else if (glb_AtendimentoTimer != null) {
        window.clearInterval(glb_AtendimentoTimer);
        glb_AtendimentoTimer = null;
    }
}

function atualizaLabelTimer() {
    if (glb_dtAtendimentoInicio == null)
        window.top.document.title = 'Lista de Pre�os';
    else {
        var dtAtendimentoFim = new Date();
        glb_dtAtendimentoFim = dtAtendimentoFim.getTime();

        var sTexto = 'Lista de Pre�os';

        if (fg.Rows == 2)
            sTexto += ': ' + (fg.Rows - 1) + ' registro';
        else if (fg.Rows > 2)
            sTexto += ': ' + (fg.Rows - 1) + ' registros';

        sTexto = sTexto + '  [' + tempoDecorrido(glb_dtAtendimentoInicio, glb_dtAtendimentoFim) + ']';
        window.top.document.title = sTexto;

        var tempoValida = tempoDecorrido(glb_dtAtendimentoInicio, glb_dtAtendimentoFim).toString().split(':');

        var horas = 0, minutos = 0, segundos = 0;

        if (tempoValida.length == 2) {
            minutos = parseFloat(tempoValida[0]);
            segundos = parseFloat(tempoValida[1]);
        }
        else if (tempoValida.length == 3) {
            horas = parseFloat(tempoValida[0]);
            minutos = parseFloat(tempoValida[1]);
            segundos = parseFloat(tempoValida[2]);
        }

        minutos = minutos + horas * 60;

        //if ((minutos >= 20) && ((minutos % 5) == 0) && (!glb_bCarregaModoFinalizar) && (segundos == 0))
        if ((minutos >= 120) && ((minutos % 30) == 0) && (!glb_bCarregaModoFinalizar) && (segundos == 0)) {
            window.focus();
            //var _retMsg = window.top.overflyGen.Confirm('Atendimento iniciado a mais de 20 min.\nDeseja finalizar o atendimento?');
            var _retMsg = window.top.overflyGen.Confirm('Atendimento iniciado a mais de 2 horas.\nDeseja finalizar o atendimento?');

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
            else {
                glb_bCarregaModoFinalizar = true;
                btnCRM_onclick();
            }
        }
    }
}

function openModalAtendimento(nState) {
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];
    var userID = getCurrUserID();
    var htmlPath;
    var strPars = new String();
    var nA1 = getCurrRightValue('SUP', 'B10A1');
    var nA2 = getCurrRightValue('SUP', 'B10A2');
    var nB7A1 = getCurrRightValue('SUP', 'B7A1');
    var nB7A2 = getCurrRightValue('SUP', 'B7A2');
    var nParceiroID = ((selParceiroID.value > 0) ? selParceiroID.value : '');

    strPars = '?sCaller=' + encodeURIComponent('PL');
    strPars += '&sCallFrom=' + encodeURIComponent('ListaPreco');
    strPars += '&nState=' + encodeURIComponent(nState);
    strPars += '&sCurrDateFormat=' + encodeURIComponent(DATE_FORMAT);
    strPars += '&nEmpresaID=' + encodeURIComponent(nEmpresaID);
    strPars += '&nUserID=' + encodeURIComponent(userID);
    strPars += '&nA1=' + encodeURIComponent(nA1 ? 1 : 0);
    strPars += '&nA2=' + encodeURIComponent(nA2 ? 1 : 0);
    strPars += '&nB7A1=' + encodeURIComponent(nB7A1 ? 1 : 0);
    strPars += '&nB7A2=' + encodeURIComponent(nB7A2 ? 1 : 0);
    strPars += '&nParceiroID=' + encodeURIComponent(nParceiroID);

    // carregar modal - faz operacao de banco no carregamento    
    htmlPath = SYS_PAGESURLROOT + '/modalgen/modalatendimento.asp' + strPars;
    showModalWin(htmlPath, new Array(995, 550));

    if (userID == 1000) {
        var rectModal = new Array(0, 0, 775, 460);
        moveFrameInHtmlTop('frameModal', rectModal);
        var modalFrame = getFrameInHtmlTop('frameModal');

        if (modalFrame != null) {
            modalFrame.style.zIndex = 2;
        }

        showFrameInHtmlTop('frameModal', true);
    }
}

function atendimentoInicido() {
    var breturn = false;

    if (glb_nPesAtendimentoID > 0)
        breturn = true;
    else {
        window.top.overflyGen.Alert('O atendimento precisa estar iniciado.');
        return true;
    }

    return breturn;
}

function js_fg_AfterRowColChangePesqList(fg, oldRow, oldCol, newRow, newCol) {
    window.status = '';
    if (glb_bPaste && fg.TextMatrix(newRow, getColIndexByColKey(fg, 'OK')) == 0) {
        fg.TextMatrix(newRow, getColIndexByColKey(fg, 'OK')) = 1;
    }
}

function AjustaPesquisa() {
    if (selRegistros.value > 10)
        selRegistros.value = 20;

    if ((glb_nA1 == 0) || (glb_nA2 == 0)) {
        for (i = 1; i < fg.Rows; i++) {
            if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Estado*')) == 'O')
                fg.TextMatrix(i, getColIndexByColKey(fg, 'Estado*')) = 'A';
        }
    }
}

function preencheFamilia() {
    glb_nValue = selFamiliaID.value;

    setConnection(dsoFamilia);

    var sWHERE = '';

    /*
    sWHERE = ('AND ' + glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' + glb_sFiltro + ' ' + glb_sFiltroEspecifico + ' ' +
	    glb_sFiltroDireitos + ' ' + glb_sFiltroProprietario + ' ' +
	    'AND (a.SujeitoID = ' + glb_aEmpresaData[0] + ') ');
	*/

    if (selMarcaID.value != 0)
        sWHERE += (' AND (b.MarcaID = ' + selMarcaID.value + ') ');

    dsoFamilia.SQL = 'SELECT 0 AS FamiliaID, \'\' AS Familia ' +
        'UNION ' +
        'SELECT DISTINCT c.ConceitoID AS FamiliaID, c.Conceito AS Familia ' +
        'FROM RelacoesPesCon a WITH(NOLOCK) ' +
        'INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ObjetoID) ' +
        'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ProdutoID) ' +
        'WHERE ((a.TipoRelacaoID = 61) AND (a.EstadoID NOT IN (1, 3, 5))) ' + sWHERE +
        'ORDER BY Familia';

    dsoFamilia.ondatasetcomplete = preencheFamilia_DSC;
    dsoFamilia.Refresh();
}

function preencheFamilia_DSC() {

    clearComboEx(['selFamiliaID']);

    if (!(dsoFamilia.recordset.BOF || dsoFamilia.recordset.EOF)) {
        dsoFamilia.recordset.moveFirst();

        while (!dsoFamilia.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoFamilia.recordset['Familia'].value;
            oOption.value = dsoFamilia.recordset['FamiliaID'].value;
            selFamiliaID.add(oOption);
            dsoFamilia.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selFamiliaID', glb_nValue);

        selFamiliaID.disabled = ((selFamiliaID.length <= 1) ? true : false);
    }

}

function preencheMarca() {
    glb_nValue = selMarcaID.value;

    setConnection(dsoMarca);

    var sWHERE = '';

    /*
    sWHERE = ('AND ' + glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' + glb_sFiltro + ' ' + glb_sFiltroEspecifico + ' ' +
	    glb_sFiltroDireitos + ' ' + glb_sFiltroProprietario + ' ' +
	    'AND (a.SujeitoID = ' + glb_aEmpresaData[0] + ') ');
    */

    //Cotador
    if (selFonteID.value == 3) {
        dsoMarca.SQL = 'SELECT 0 AS MarcaID, \'\' AS Marca ' +
            'UNION ' +
            'SELECT DISTINCT b.ConceitoID AS MarcaID, b.Conceito AS Marca ' +
            'FROM CorpProdutos a WITH(NOLOCK) ' +
            'INNER JOIN Conceitos b WITH(NOLOCK)  ON (b.ConceitoID = a.MarcaID) ' +
            'ORDER BY Marca';
    }
    else {
        if (selFamiliaID.value != 0)
            sWHERE += (' AND (b.ProdutoID = ' + selFamiliaID.value + ') ');

        dsoMarca.SQL = 'SELECT 0 AS MarcaID, \'\' AS Marca ' +
            'UNION ' +
            'SELECT DISTINCT c.ConceitoID AS MarcaID, c.Conceito AS Marca ' +
            'FROM RelacoesPesCon a WITH(NOLOCK) ' +
            'INNER JOIN Conceitos b WITH(NOLOCK)  ON (b.ConceitoID = a.ObjetoID) ' +
            'INNER JOIN Conceitos c WITH(NOLOCK)  ON (c.ConceitoID = b.MarcaID) ' +
            'WHERE ((a.TipoRelacaoID = 61) AND (a.EstadoID = 2)) ' + sWHERE +
            'ORDER BY Marca';
    }

    dsoMarca.ondatasetcomplete = preencheMarca_DSC;
    dsoMarca.Refresh();
}

function preencheMarca_DSC() {
    clearComboEx(['selMarcaID']);

    if (!(dsoMarca.recordset.BOF || dsoMarca.recordset.EOF)) {
        dsoMarca.recordset.moveFirst();

        while (!dsoMarca.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoMarca.recordset['Marca'].value;
            oOption.value = dsoMarca.recordset['MarcaID'].value;
            selMarcaID.add(oOption);
            dsoMarca.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selMarcaID', glb_nValue);

        selMarcaID.disabled = ((selMarcaID.length <= 1) ? true : false);

        if (selMarcaID.length == 2 && selFonteID.value == 3) {
            selMarcaID.selectedIndex = 1;
            selMarca_onchange();
        }
    }
}

function preencheLinha() {
    glb_nValue = selLinhaProdutoID.value;

    setConnection(dsoLinha);

    var sWHERE = '';

    /*
    sWHERE = ('AND ' + glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' + glb_sFiltro + ' ' + glb_sFiltroEspecifico + ' ' +
	    glb_sFiltroDireitos + ' ' + glb_sFiltroProprietario + ' ' +
	    'AND (a.SujeitoID = ' + glb_aEmpresaData[0] + ') ' +
        'AND (b.MarcaID = ' + selMarca.value + ') ');
    */

    //cotador
    if (selFonteID.value == 3) {
        if (selMarcaID.value != 0)
            sWHERE = ' AND (MarcaID = ' + selMarcaID.value + ') ';

        dsoLinha.SQL = 'SELECT 0 AS LinhaID, \'\' AS Linha ' +
            'UNION ' +
            'SELECT DISTINCT TipoProdutoID AS LinhaID, dbo.fn_TipoAuxiliar_Item(TipoProdutoID, 0) AS Linha ' +
            'FROM CorpProdutos WITH(NOLOCK) ' +
            'WHERE EstadoID = 2 ' + sWHERE +
            'ORDER BY Linha ';
    }
    else {
        if (selFamiliaID.value != 0)
            sWHERE += (' AND (b.ProdutoID = ' + selFamiliaID.value + ') ');

        if (selMarcaID.value != 0)
            sWHERE += (' AND (b.MarcaID = ' + selMarcaID.value + ') ');

        dsoLinha.SQL = 'SELECT 0 AS LinhaID, \'\' AS Linha ' +
            'UNION ' +
            'SELECT DISTINCT c.ConceitoID AS LinhaID, c.Conceito AS Linha ' +
            'FROM RelacoesPesCon a WITH(NOLOCK) ' +
            'INNER JOIN Conceitos b WITH(NOLOCK)  ON (b.ConceitoID = a.ObjetoID) ' +
            'INNER JOIN Conceitos c WITH(NOLOCK)  ON (c.ConceitoID = b.LinhaProdutoID) ' +
            'WHERE ((a.TipoRelacaoID = 61) AND (a.EstadoID = 2)) ' + sWHERE +
            'ORDER BY Linha';
    }

    dsoLinha.ondatasetcomplete = preencheLinha_DSC;
    dsoLinha.Refresh();
}

function preencheLinha_DSC() {
    clearComboEx(['selLinhaProdutoID']);

    if (!(dsoLinha.recordset.BOF || dsoLinha.recordset.EOF)) {
        dsoLinha.recordset.moveFirst();

        while (!dsoLinha.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoLinha.recordset['Linha'].value;;
            oOption.value = dsoLinha.recordset['LinhaID'].value;
            selLinhaProdutoID.add(oOption);
            dsoLinha.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selLinhaProdutoID', glb_nValue);

        selLinhaProdutoID.disabled = ((selLinhaProdutoID.length <= 1) ? true : false);
    }
}

function filtraPessoa(valor, objSel) {
    if (glb_TimerPessoaOnChange != null) {
        window.clearInterval(glb_TimerPessoaOnChange);
        glb_TimerPessoaOnChange = null;
    }

    for (i = 0; i < objSel.length; i++) {
        if (objSel.options[i].text.toUpperCase().indexOf(valor.toUpperCase()) >= 0) {
            objSel.selectedIndex = i;
            break;
        }
    }

    glb_TimerPessoaOnChange = window.setInterval('selPessoaID_OnChange()', 3000, 'JavaScript');
}

function CalculaICMSST(linha) {
    var aEmpresa = getCurrEmpresaData();
    var ProdutoID, EmpresaID, Quantidade, Preco, PessoaID, SimplesNacional, Contribuinte, CidadeID, UFID, TransacaoID, FinalidadeID, CFOPID, TipoID, nChavePessoa;
    var strPars = "";
    glb_nRow = linha;

    if (aEmpresa[1] == 130 && selFonteID.value != 3) {
        EmpresaID = fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'EmpresaID'));
        ProdutoID = fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'ConceitoID*'));
        Quantidade = (fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'QuantidadeComprar')) == 0 ? 1 : fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'QuantidadeComprar')));

        CFOPID = fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'CFOP*'));

        nChavePessoa = selChavePessoaID.value;

        Contribuinte = (chkContribuinte.checked ? 1 : 0);
        SimplesNacional = (chkSimplesNacional.checked ? 1 : 0);

        var precoDigitado = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + glb_nRow + ", getColIndexByColKey(fg, 'PrecoDigitado'))");
        var margemDigitada = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + glb_nRow + ", getColIndexByColKey(fg, 'MargemDigitada'))");
        var margemContribuicao = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + glb_nRow + ", getColIndexByColKey(fg, 'MC'))").replace(',', '.');

        strPars += "?nEmpresaID=" + EmpresaID;
        strPars += "&nProdutoID=" + ProdutoID;
        strPars += "&nQuantidade=" + Quantidade;
        strPars += "&nAliquotaImposto=" + sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + glb_nRow + ", getColIndexByColKey(fg, 'Imp*'))");

        // Se foi configurada margem na lista, usa essa margem para chegar no mesmo pre�o (e n�o a margem recalculada MC, pois sempre dar� diverg�ncia)
        if ((margemDigitada != "") || (precoDigitado == ""))
            strPars += "&nMargemContribuicao=" + margemDigitada.replace(',', '.');
        else
            strPars += "&nMargemContribuicao=" + sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + glb_nRow + ", getColIndexByColKey(fg, 'MC'))").replace(',', '.');

        strPars += "&nTipoMargem= 1 ";
        strPars += "&nImpostoIncidencia=" + sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "lstPre_ImpostosIncidencia");
        strPars += "&sCFOP=" + sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + glb_nRow + ", getColIndexByColKey(fg, 'CFOP*'))");
        strPars += "&nLoteID=" + sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "(getColIndexByColKey(fg, 'Lote*') > 0 ? fg.TextMatrix(" + glb_nRow + ", getColIndexByColKey(fg, 'Lote*')) : 'null')");
        strPars += sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "specialClauseOfResearchEx()");

        margemContribuicao = (margemContribuicao == null ? 0 : margemContribuicao);

        if ((margemContribuicao < -999.99) || (margemContribuicao > 999.99))
            return;

        dsoICMSST.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/CalculaICMSSt.aspx' + strPars;
        dsoICMSST.ondatasetcomplete = CalculaICMSST_DSC;
        dsoICMSST.refresh();
    }
}

function CalculaICMSST_DSC() {
    var Icms_ST = dsoICMSST.recordset['ICMS_ST'].value;
    var TipoPagamento = dsoICMSST.recordset['TipoPagamentoST'].value;
    var GareGNRE;
    var nQuantidade = (fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'QuantidadeComprar')) == 0 ? 1 : fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'QuantidadeComprar')));

    if (TipoPagamento == 2)
        GareGNRE = "GNRE";
    else if (TipoPagamento == 1)
        GareGNRE = "GARE";
    else
        GareGNRE = " - ";

    fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'Guia')) = GareGNRE;
    fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'ICMSST')) = Icms_ST * nQuantidade;
    fg.TextMatrix(glb_nRow, getColIndexByColKey(fg, 'ICMSST_Unitario')) = Icms_ST;


}

function selecionarSacola(nSacolaID) {
    var strPars = '';
    var nVendedorID = getCurrUserID();

    glb_nSacolaID = nSacolaID;

    strPars = '?nSacolaID=' + escape(nSacolaID) +
        '&nVendedorID=' + escape(nVendedorID) +
        '&nTornarAtual=' + escape(1) +
        '&nDuplicarSacola=' + escape(0);

    setConnection(dsoSelecionaSacola);

    dsoSelecionaSacola.URL = SYS_ASPURLROOT + '/serversidegenEx/selecionaSacola.aspx' + strPars;
    dsoSelecionaSacola.ondatasetcomplete = dsoSelecionaSacola_DSC;
    dsoSelecionaSacola.refresh();
}

function dsoSelecionaSacola_DSC() {
    if (dsoSelecionaSacola.recordset['Resultado'].value == '')
        openModalTrolley('PL');
    else {
        if (window.top.overflyGen.Alert(dsoSelecionaSacola.recordset['Resultado'].value) == 0)
            return null;
    }

}

function anulaSacola() {
    glb_nSacolaID = 0;
}

function verificaCargaCotador() {
    if (selMarcaID.value != 0) {
        setConnection(dsoCargaCotador);

        dsoCargaCotador.SQL = 'SELECT dbo.fn_Cotador_Inconsistencias(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,' + selMarcaID.value + ', 4, 1) AS [Inconsistencia], ' +
            'dbo.fn_Cotador_Inconsistencias(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,' + selMarcaID.value + ', 4, 0) AS [MsgInconsistencia]';

        dsoCargaCotador.ondatasetcomplete = verificaCargaCotador_DSC;
        dsoCargaCotador.refresh();
    }
}

function verificaCargaCotador_DSC() {
    if (dsoCargaCotador.recordset['Inconsistencia'].value == 3)
        glb_bCargaCompletaAtualizada = false;
    else
        glb_bCargaCompletaAtualizada = true;

}

function buscaPessoaCotador() {
    var nTipoPessoaID = selTipoPessoa.value;
    var bContribuinte = (chkContribuinte.checked ? 1 : 0);
    var bSimplesNacional = (chkSimplesNacional.checked ? 1 : 0);
    var nUFID = selUFID.value;
    var nCidadeID = selCidadeId.value;

    setConnection(dsoBuscaPessoaCotador);

    dsoBuscaPessoaCotador.SQL = 'SELECT dbo.fn_Cotador_PessoaCompativel(' + bContribuinte + ', ' + bSimplesNacional + ', ' + nUFID + ', ' + nCidadeID + ', ' + nTipoPessoaID + ') AS [PessoaID]';

    dsoBuscaPessoaCotador.ondatasetcomplete = buscaPessoaCotador_DSC;
    dsoBuscaPessoaCotador.refresh();
}

function buscaPessoaCotador_DSC() {
    var nPessoaID = dsoBuscaPessoaCotador.recordset['PessoaID'].value;

    setConnection(dsoTipoSacola);

    dsoTipoSacola.SQL = 'SELECT a.SacolaID [SacolaAtual], dbo.fn_Cotador_Tipo_SacolaCompra(a.SacolaID, 1) AS [TipoSacolaAtual], a.UsuarioID AS [Vendedor] ' +
        'FROM SacolasCompras a WITH(NOLOCK) ' +
        'WHERE a.SacolaID = dbo.fn_SacolasCompras_Atual(' + selParceiroID.value + ', ' + nPessoaID + ', ' + getCurrUserID() + ', NULL) ';

    dsoTipoSacola.ondatasetcomplete = validaPessoaCotador_DSC;
    dsoTipoSacola.refresh();
}

function validaPessoaCotador_DSC() {
    var nSacolaID = dsoTipoSacola.recordset['SacolaAtual'].value;
    var nVendedorID = dsoTipoSacola.recordset['Vendedor'].value;

    if (nSacolaID != null && dsoTipoSacola.recordset['TipoSacolaAtual'].value == 1 && selChavePessoaID.value == 2) {
        var strPars = '?nSacolaID=' + escape(nSacolaID) +
            '&nVendedorID=' + escape(nVendedorID) +
            '&nTornarAtual=' + escape(0) +
            '&nDuplicarSacola=' + escape(0);

        setConnection(dsoSalvaSacola);

        dsoSalvaSacola.URL = SYS_ASPURLROOT + '/serversidegenEx/selecionaSacola.aspx' + strPars;
        dsoSalvaSacola.ondatasetcomplete = SalvaSacola_DSC;
        dsoSalvaSacola.refresh();
    }
    glb_nCotadorPessoaID = dsoBuscaPessoaCotador.recordset['PessoaID'].value;
}

function SalvaSacola_DSC() {
    glb_nCotadorPessoaID = dsoBuscaPessoaCotador.recordset['PessoaID'].value;
}

function sacolaAtual() {
    var bSemPessoa;
    var nParceiroID;
    var nUserID;
    var nPessoaID;
    var nSacolaID;

    bSemPessoa = (selChavePessoaID.value == 2 ? true : false);
    nParceiroID = selParceiroID.value;
    nUserID = getCurrUserID();
    nSacolaID = 'NULL';
    nPessoaID = null;

    if (bSemPessoa) {
        if (!(dsoBuscaPessoaCotador.recordset['PessoaID'] == undefined))
            nPessoaID = dsoBuscaPessoaCotador.recordset['PessoaID'].value;
    }
    else
        nPessoaID = selPessoaID.value;


    if ((nParceiroID > 0) && (nPessoaID > 0) && (nUserID > 0)) {
        setConnection(dsoSacolaAtual);

        dsoSacolaAtual.SQL = 'DECLARE @SacolaAtualID INT ' +
            'SELECT @SacolaAtualID = dbo.fn_SacolasCompras_Atual(' + nParceiroID + ',' + nPessoaID + ',' + nUserID + ',' + nSacolaID + ') ' +
            'SELECT @SacolaAtualID AS SacolaAtualID, Observacoes, OrigemSacolaID FROM SacolasCompras WITH(NOLOCK) WHERE SacolaID = @SacolaAtualID';

        dsoSacolaAtual.ondatasetcomplete = sacolaAtual_DSC;
        dsoSacolaAtual.refresh();
    }
}

function sacolaAtual_DSC() {
    var nSacolaAtualTipo = null; //0 - Estoque/Lote; 1 - Cotador Padr�o; 2 - Cotador Quote/BOM
    var sObservacoesCotador = dsoSacolaAtual.recordset['Observacoes'].value;

    if (sObservacoesCotador == null)
        sObservacoesCotador = '';

    if (dsoSacolaAtual.recordset['OrigemSacolaID'].value != 610 && dsoSacolaAtual.recordset['OrigemSacolaID'].value != null)
        nSacolaAtualTipo = 0;
    else if (dsoSacolaAtual.recordset['OrigemSacolaID'].value == 610 && sObservacoesCotador.indexOf('<PASTE:1>') < 0)
        nSacolaAtualTipo = 1;
    else if (dsoSacolaAtual.recordset['OrigemSacolaID'].value == 610 && sObservacoesCotador.indexOf('<PASTE:1>') >= 0)
        nSacolaAtualTipo = 2;

    if (glb_bCotadorPadrao) {
        if (nSacolaAtualTipo != 1 && nSacolaAtualTipo != null) {
            if (window.top.overflyGen.Alert('N�o � poss�vel comprar. Favor liberar a sacola Atual.') == 0)
                return null;
        }
        else
            purchaseItem('PC', glb_nRow);

    }
    else {
        if (nSacolaAtualTipo >= 0 && nSacolaAtualTipo != null) {
            if (window.top.overflyGen.Alert('N�o � poss�vel comprar. Favor liberar a sacola Atual.') == 0)
                return null;
        }
        else
            purchaseGPLEstimate();
    }

    glb_bCotadorPadrao = false;
}