/********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do registro desejado
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
	if ((window.top.__glb_ListOrDet == '') || (window.top.__glb_ListOrDet == 'PL'))
		cloneControlsContentsAndState();

    var sSQL, sStrSelect1, sStrSelect2, sStrFROM, sStrWhere, sStrEsqtoque, sStrSelectLote, sStrFromLote;
    var nEmpresaData = getCurrEmpresaData();
    var nEmpresaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + (glb_nPointer + 1) + ", getColIndexByColKey(fg, 'EmpresaID'))");
    var sAliquotaImposto = 'NULL';
    var sPessoa = 'NULL';
    var sParceiroID = 'NULL';
    var nListaPreco = 'NULL';
    var sQuantidade;
    var sFinanciamento;
    var sFrete;
    var sCFOPID = 'NULL';
    var sPrecoMargemMinima = '';
    var sMargemMinima = '';
    var sTransacao = (selTransacao.selectedIndex >= 0 ? selTransacao.value : 'NULL');
    var sFinalidade = (selFinalidade.selectedIndex >= 0 ? selFinalidade.value : 'NULL');
    var nLoteID = 'NULL';
    var Lote = (sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'chkLote.checked')) ? 1 : 0;

    // Parametros Sem Pessoa
    var nClasse = selClassificacoesClaID.value;
    var nTipoPessoa = selTipoPessoa.value;
    var nFaturamentoDireto = escape(chkFaturamentoDireto.checked ? 1 : 0);
    var nContribuinte = escape(chkContribuinte.checked ? 1 : 0);
    var nSimplesNacional = escape(chkSimplesNacional.checked ? 1 : 0);
    var nSuframa = escape(chkSuframaID.checked ? 1 : 0);
    var nUFID = selUFID.value;
    var nCidade = escape(selCidadeId.selectedIndex >= 0 ? selCidadeId.value : 0);
    var nClassificacoes = escape(selClassificacoesID.selectedIndex >= 0 ? selClassificacoesID.value : 0);
    var nCnae = txtCnae.value;
    var nIsencoes = selIsencoesID.value;
    var nPais = escape(nEmpresaData[1]);
    var glb_nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));
    var glb_nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
    var sEstadoID = 'NULL';

    sStrEsqtoque = "";

    if (nCnae == "") 
        nCnae = 'NULL';

    if (nIsencoes == "")
        nIsencoes = 'NULL';        
         
    if ((isNaN(parseInt(txtQuantidade.value,10))) || (parseInt(txtQuantidade.value,10) < 1))
        sQuantidade = 1;
    else
        sQuantidade = parseInt(txtQuantidade.value,10);

	if ((glb_btnCtlSup == 'SUPDET') || (glb_btnCtlSup == 'SUPANT') || (glb_btnCtlSup == 'SUPSEG'))
	{
        sQuantidade = ' ISNULL(a.MultiploVenda, 1) ';
		if (isNaN(parseInt(txtPrazoMedioPagamento.value,10)))
		    txtPrazoMedioPagamento.value = 0;
		prazo();
    }
    
    if (Lote == 1) 
    {
        sStrFromLote = ' INNER JOIN dbo.Lotes_Produtos Lote WITH(NOLOCK) ON (Lote.ProdutoID = a.ObjetoID ) ';
        sStrFromLote = sStrFromLote + ' INNER JOIN dbo.Lotes x WITH ( NOLOCK ) ON ( (x.EmpresaID = ' + nEmpresaID + ' ) AND( Lote.LoteID = x.LoteID)) ';
        sStrSelectLote = ' ,Lote.LoteID ';  
    }
    if (selPessoaID.value > 0 )//com pessoa
    {
        // Com Pessoa esses parametros sempre ser�o nulos
        nClasse = 'NULL';
        nTipoPessoa = 'NULL';
        nFaturamentoDireto = 'NULL';
        nContribuinte = 'NULL';
        nSimplesNacional = 'NULL';
        nSuframa = 'NULL';
        nUFID = 'NULL';
        nCidade = 'NULL';
        nClassificacoes = 'NULL';
        nCnae = 'NULL';
        nIsencoes = 'NULL';
        nPais = 'NULL';
        
        sImpostosIncidencia = 'dbo.fn_Pessoa_Incidencia(' + nEmpresaID + ', ' + selPessoaID.value + ')';
        sPessoa = selPessoaID.value;
        sCFOPID = "dbo.fn_EmpresaPessoaTransacao_CFOP(a.SujeitoID, " + sPessoa + ",NULL,NULL,NULL,NULL,NULL," + sTransacao + ", " + sFinalidade + ", a.ObjetoID, NULL)";
        sAliquotaImposto = 'dbo.fn_Produto_Aliquota(' + nEmpresaID + ', a.ObjetoID,' + sPessoa + ',NULL,NULL,NULL,NULL,' + sCFOPID + ')';
        sParceiroID = (selParceiroID.value > 0 ? selParceiroID.value : 'NULL');
        if (sParceiroID != 'NULL')
            nListaPreco = -sParceiroID;

        sStrEsqtoque = sStrEsqtoque +
         " CONVERT(VARCHAR(10), dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, -356, NULL, NULL, dbo.fn_Produto_EstoqueGlobal(a.ObjetoID, a.SujeitoID, " + sPessoa + ",NULL),375" + (sStrSelectLote ? sStrSelectLote : ",NULL" )+ ")) AS EstoqueDisponivel, " +
         " CONVERT(VARCHAR(10), dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, -356, a.SujeitoID, NULL, dbo.fn_Produto_EstoqueGlobal(a.ObjetoID, a.SujeitoID, " + sPessoa + ",NULL),375" + (sStrSelectLote ? sStrSelectLote : ",NULL") + ")) AS EstoqueDisponivel1, " +
         " CONVERT(VARCHAR(10), dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, -356, -1, NULL, dbo.fn_Produto_EstoqueGlobal(a.ObjetoID, a.SujeitoID, " + sPessoa + ",NULL),375" + (sStrSelectLote ? sStrSelectLote : ",NULL") + ")) AS EstoqueDisponivel2 ";
    }
    else //sem pessoa
    {
       sParceiroID = (selParceiroID.value > 0 ? selParceiroID.value : 'NULL');
       if (sParceiroID != 'NULL')
           nListaPreco = -sParceiroID;
            
        s    = "dbo.fn_EmpresaPessoaTransacao_CFOP(a.SujeitoID, NULL," + nPais + "," + nContribuinte + "," + nSuframa + "," + nUFID + "," + nIsencoes + "," + sTransacao + ", " + sFinalidade + ", a.ObjetoID, NULL)";

        sImpostosIncidencia = 1;
        
        if (selUFID.value > 0)
            sAliquotaImposto = 'dbo.fn_Produto_Aliquota(' + nEmpresaID + ', a.ObjetoID, NULL, ' + nContribuinte + ',' + nUFID + ',' + nCidade + ', NULL,' + sCFOPID + ')';        
        else
        {
            if (glb_bRetificaImposto)
                sAliquotaImposto = 'dbo.fn_Produto_Aliquota(' + nEmpresaID + ', a.ObjetoID, NULL, ' + nContribuinte + ',' + nUFID + ',' + nCidade + ',NULL,' + sCFOPID + ')';
            else {
                glb_bRetificaImposto = true;
                if (!isNaN(parseInt(txtImposto.value, 10))) {
                    sAliquotaImposto = txtImposto.value;
                }
                else {
                    sAliquotaImposto = 'dbo.fn_Produto_Aliquota(' + nEmpresaID + ', a.ObjetoID, NULL,' + nContribuinte + ',' + nUFID + ',' + nCidade + ',NULL, ' + sCFOPID + ')';
                }
            }
        }

        sStrEsqtoque = sStrEsqtoque +
         " CONVERT(VARCHAR(10), dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, -356, NULL, NULL, dbo.fn_Produto_EstoqueGlobal(a.ObjetoID, a.SujeitoID,NULL," + nTipoPessoa + "),375" + (sStrSelectLote ? sStrSelectLote : ",NULL") + ")) AS EstoqueDisponivel, " +
         " CONVERT(VARCHAR(10), dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, -356, a.SujeitoID, NULL, dbo.fn_Produto_EstoqueGlobal(a.ObjetoID, a.SujeitoID,NULL," + nTipoPessoa + "),375" + (sStrSelectLote ? sStrSelectLote : ",NULL") + ")) AS EstoqueDisponivel1, " +
         " CONVERT(VARCHAR(10), dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, -356, -1, NULL, dbo.fn_Produto_EstoqueGlobal(a.ObjetoID, a.SujeitoID,NULL," + nTipoPessoa + "),375" + (sStrSelectLote ? sStrSelectLote : ",NULL") + ")) AS EstoqueDisponivel2 ";  
    }

    if (selFinanciamentoID.value > 0 )
		sFinanciamento = selFinanciamentoID.value;
	else
		sFinanciamento = "NULL";

    sFrete = '0'; //selFrete.value;

    if (glb_bDireitoVerMargem)
        sMargemMinima = 'CONVERT(NUMERIC(5,2), dbo.fn_Produto_Margem(a.SujeitoID, a.ObjetoID, ' + nListaPreco + ',' + nClasse + ',' + nClassificacoes + ', ' + sQuantidade + ', 1, 1))';
    else
        sMargemMinima = 'CONVERT(NUMERIC(5,2), 0)';

    if (glb_nA1 == 0 || glb_nA2 == 0)
        sEstadoID = '(CASE WHEN a.EstadoID = 15 THEN 2 ELSE a.EstadoID END) ';
    else
        sEstadoID = 'a.EstadoID ';

    sStrSelect1 = 'SELECT ' +
        'b.ConceitoID AS ConceitoID, ' + sEstadoID + 'AS EstadoID, b.Conceito AS Conceito, b.PartNumber AS PartNumber, ' +
        'c.ItemAbreviado AS Unidade, CONVERT(NUMERIC(5,2),dbo.fn_Produto_PesosMedidas(b.ConceitoID,1,1)) AS PesoBruto, a.PrazoTroca AS PrazoTroca, ' +
        'a.PrazoGarantia AS PrazoGarantia,  SUBSTRING(CONVERT(VARCHAR(10),a.OrigemID),4,1) AS Origem, a.PrazoAsstec AS PrazoAsstec, ' +
        'dbo.fn_Produto_Descricao2(b.ConceitoID, ' + nEmpresaData[8] + ', 11) AS DescricaoProduto, ';
        //'dbo.fn_Tradutor(d.Conceito, ' + nEmpresaData[7] + ', ' + nEmpresaData[8] + ', NULL) AS ConceitoConcreto, e.Conceito AS Marca, b.Modelo AS Modelo, b.Descricao AS Descricao, ';                
                
    sStrSelect2 =
        sAliquotaImposto  + ' AS Imposto, ' +
        sImpostosIncidencia + ' AS ImpostosIncidencia, ' +
        ' CONVERT(VARCHAR(6), \'' + selMoedaConversaoID.options[selMoedaConversaoID.selectedIndex].innerText + '\') AS MoedaTabela, ' +
        'CONVERT(INT, ' + selMoedaConversaoID.value + ') AS MoedaTabelaID, ' +
        'dbo.fn_Produto_PrevisaoEntregaContratual(a.ObjetoID, a.SujeitoID ,' + sQuantidade + ', ' + nLoteID + ', 0) AS PrevisaoEntrega, ' +
        'h.Fantasia AS Empresa, h.PessoaID AS EmpresaID, ' +
		'dbo.fn_Preco_PrecoLista(a.SujeitoID,a.ObjetoID,NULL,' + sAliquotaImposto + ',' + selMoedaConversaoID.value + ',GETDATE(),0,' + sImpostosIncidencia + ', 1, NULL, ' + sPessoa + ',' + sParceiroID + ',' + nFaturamentoDireto + ',' + nContribuinte + ',' + nSimplesNacional + ',' + nSuframa + ',' + nUFID + ',' + nCidade + ',' + nCnae + ',' + nIsencoes + ', NULL, ' + nClasse + ',' + nClassificacoes + ', ' + nTipoPessoa + ', NULL, NULL, NULL, 0, NULL, NULL, ' + sFinalidade + ', ' + sCFOPID + ',' + nLoteID + ' ) AS PrecoTabela, ' +
        'dbo.fn_Preco_PrecoLista(a.SujeitoID,a.ObjetoID,NULL,' + sAliquotaImposto + ',' + selMoedaConversaoID.value + ',GETDATE(),' + nListaPreco + ',' + sImpostosIncidencia + ',' + sQuantidade + ', NULL, ' + sPessoa + ',' + sParceiroID + ',' + nFaturamentoDireto + ',' + nContribuinte + ',' + nSimplesNacional + ',' + nSuframa + ',' + nUFID + ',' + nCidade + ',' + nCnae + ',' + nIsencoes + ', NULL, ' + nClasse + ',' + nClassificacoes + ', ' + nTipoPessoa + ', NULL, NULL, NULL, 0, NULL, NULL, ' + sFinalidade + ', ' + sCFOPID + ',' + nLoteID + ') AS PrecoLista, ' +
		'CONVERT(NUMERIC(5,2), dbo.fn_Produto_Margem(a.SujeitoID, a.ObjetoID, ' + nListaPreco + ', NULL, NULL, ' + sQuantidade + ', 1, 2)) AS MargemLista, ' + sMargemMinima + ' AS MargemMinima, ' +
        'dbo.fn_Preco_PrecoLista(a.SujeitoID,a.ObjetoID,NULL,' + sAliquotaImposto + ',' + selMoedaConversaoID.value + ',GETDATE(),' + nListaPreco + ',' + sImpostosIncidencia + ',' + sQuantidade + ', NULL, ' + sPessoa + ',' + sParceiroID + ',' + nFaturamentoDireto + ',' + nContribuinte + ',' + nSimplesNacional + ',' + nSuframa + ',' + nUFID + ',' + nCidade + ',' + nCnae + ',' + nIsencoes + ', NULL, ' + nClasse + ',' + nClassificacoes + ', ' + nTipoPessoa + ', NULL, NULL, ' + sFinanciamento + ', ' + sFrete + ', NULL, NULL, ' + sFinalidade + ', ' + sCFOPID + ',' + nLoteID + ') AS PrecoConvertido, ' +
        'dbo.fn_Preco_PrecoLista(a.SujeitoID,a.ObjetoID,NULL,' + sAliquotaImposto + ',' + selMoedaConversaoID.value + ',GETDATE(),' + nListaPreco + ',' + sImpostosIncidencia + ',' + sQuantidade + ', NULL, ' + sPessoa + ',' + sParceiroID + ',' + nFaturamentoDireto + ',' + nContribuinte + ',' + nSimplesNacional + ',' + nSuframa + ',' + nUFID + ',' + nCidade + ',' + nCnae + ',' + nIsencoes + ', NULL, ' + nClasse + ',' + nClassificacoes + ', ' + nTipoPessoa + ', NULL , ' + sMargemMinima + ',NULL,' + sFrete + ', NULL, NULL, ' + sFinalidade + ', ' + sCFOPID + ',' + nLoteID + ') AS PrecoMinimo, ' +
		'dbo.fn_Produto_FatorTotalImpostos(a.SujeitoID, a.ObjetoID, ' + sAliquotaImposto + ', ' + sImpostosIncidencia + ', ' + sCFOPID + ', NULL) AS FatorTotalImpostos, ' + sQuantidade + ' AS Quantidade, ' +
		'i.Conceito AS NCM, ISNULL(i.Observacoes, SPACE(0)) AS  NCM_Observacoes, ' +
		
		//'CONVERT(VARCHAR(10), dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, -356, NULL, NULL, NULL)) AS EstoqueDisponivel, ' +
        //'CONVERT(VARCHAR(10), dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, -356, a.SujeitoID, NULL, NULL)) AS EstoqueDisponivel1, ' +
        //'CONVERT(VARCHAR(10), dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, -356, -1, NULL, NULL)) AS EstoqueDisponivel2, ' +
       
        'ISNULL(j.Conceito, SPACE(0)) AS [LinhaProduto], ' +
        '0 AS Prop1, 0 AS Prop2, ' + sCFOPID + ' AS CFOPID, ISNULL(dbo.fn_Operacoes(' + sCFOPID + ', 0), SPACE(0)) AS CFOP ,';

    sStrFROM = 'FROM RelacoesPesCon a WITH(NOLOCK) ' +
        'INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ObjetoID = b.ConceitoID) ' +
        'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (b.UnidadeID = c.ItemID) ' +
        'INNER JOIN Conceitos d WITH(NOLOCK) ON (b.ProdutoID = d.ConceitoID) ' +
        'INNER JOIN Conceitos e WITH(NOLOCK) ON (b.MarcaID = e.ConceitoID) ' +
        'INNER JOIN Pessoas h WITH(NOLOCK) ON (a.SujeitoID = h.PessoaID) ' +
        'INNER JOIN Recursos g WITH(NOLOCK) ON g.RecursoID=999 ' +
        'LEFT JOIN Conceitos i WITH(NOLOCK) ON (a.ClassificacaoFiscalID = i.ConceitoID) ' +
		'LEFT JOIN Conceitos j WITH(NOLOCK) ON (j.ConceitoID = b.LinhaProdutoID) ';
		

    sStrWhere = 'WHERE (b.ConceitoID = ' + nID + ' AND a.SujeitoID = ' + nEmpresaID + ' AND a.TipoRelacaoID=61)';

    //@@
    if (Lote == 1)
        sSQL = sStrSelect1 + sStrSelect2 + sStrEsqtoque + sStrSelectLote + sStrFROM + sStrFromLote + sStrWhere;
    else
        sSQL = sStrSelect1 + sStrSelect2 + sStrEsqtoque + sStrFROM + sStrWhere;

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;
    
    sql = 'SELECT * FROM TiposRelacoes WITH(NOLOCK) WHERE TipoRelacaoID = 0';
    dso.SQL = sql;          
}              
