/********************************************************************
pesqlistsuperior.js.js


Library javascript para o pesqlistsuperior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_tempRefresh = null;
var glb_callRefreshInterface = null;
var glb_DsoToReturn = null;
var glb_nProdutoID = null;
var glb_nQuantidade = null;
var glb_bCarregaReceptivo = false;
var glb_nSacolaAtual = 0;
var glb_nSacolasSalvas = 0;
var glb_bVerificaSacola = false;

var glb_nRow = 0;
var glb_sFromCaller = '';

dsoPurchaseItem = new CDatatransport("dsoPurchaseItem");
dsoCla = new CDatatransport('dsoCla');
dsoTipoPessoa = new CDatatransport('dsoTipoPessoa');
dsoUFCidades = new CDatatransport('dsoUFCidades');
dsoCanalID = new CDatatransport('dsoCanalID');
dsoUFID = new CDatatransport('dsoUFID');
dsoBalanceCampaign = new CDatatransport('dsoBalanceCampaign');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:


********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de parceiro 

Parametro:
glb_aDadosPessoa   Array: item 0->Nome
                           item 1->ID
                           item 2->UFID
                           item 3->Parceiro
                           item 4->Lista Pre�o
                           item 5->PMP
                           item 6->Numero Parcelas
                           item 7->Financiamento Padr�o
                           item 8->Classifica��o interna
                           item 9->Classifica��o externa

Retorno:
nenhum */

function Fillcmbcidade() 
{
    lockInterface(true);
        dsoUFCidades.SQL = 'SELECT LocalidadeID, Localidade ' +
						        'FROM Localidades WITH(NOLOCK) ' +
		                        'WHERE LocalizacaoID = ' + selUFID.value + ' AND Capital = 1 ' +
                            'UNION ALL ' +
                            'SELECT LocalidadeID, Localidade ' +
                                'FROM Localidades WITH(NOLOCK) ' +
		                        'WHERE LocalizacaoID = ' + selUFID.value + ' AND Capital <> 1';
        dsoUFCidades.ondatasetcomplete = dsoUFCidades_DSC;
        dsoUFCidades.refresh();
        
        lockInterface(false);
        
        if (selUFID.value == 0) 
        {
            selCidadeId.disabled = true;
        } 
        else 
        {
            selCidadeId.disabled = false;
        }
    
}
function dsoUFCidades_DSC() 
{
    var bCotador = (selFonteID.value == 3 ? true : false);

    clearComboEx(['selCidadeId']);
    while (!dsoUFCidades.recordset.EOF) 
    {
        optionStr = dsoUFCidades.recordset['Localidade'].value;
        optionValue = dsoUFCidades.recordset['LocalidadeID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selCidadeId.add(oOption);

        dsoUFCidades.recordset.MoveNext();
    }

    if ((selCidadeId.value > 0) && (bCotador))
        buscaPessoaCotador();
}

function fillCmbCanal() 
{
    setConnection(dsoCanalID);

    dsoCanalID.SQL = 'SELECT 65 AS ItemID, \'Consumidor final\' AS ItemMasculino ' +
                    'UNION ALL ' +
                    'SELECT ItemID, ItemMasculino ' +
                        'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                        'WHERE TipoID = 13 AND ' +
                            'Filtro LIKE \'%1221%\' ' +
                            'AND ItemID <> 65';

    dsoCanalID.ondatasetcomplete = dsoCanalID_DSC;
    dsoCanalID.refresh();
}
    
function dsoCanalID_DSC()
{
    clearComboEx(['selClassificacoesID']);
    while (!dsoCanalID.recordset.EOF) 
    {
        optionStr = dsoCanalID.recordset['ItemMasculino'].value;
        optionValue = dsoCanalID.recordset['ItemID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selClassificacoesID.add(oOption);
        dsoCanalID.recordset.MoveNext();
    }
}

/**************************************************
Presto
**************************************************/
function fillCmbTipoPessoa() {
    setConnection(dsoTipoPessoa);
    dsoTipoPessoa.SQL = 'SELECT ItemID, LEFT(ItemAbreviado, 3) AS ItemMasculino ' +
	                    'FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) ' +
	                    'WHERE TipoID = 12 AND EstadoID = 2 AND Filtro LIKE \'%(5221)%\'' +
                        'ORDER BY ItemID DESC';
    
    dsoTipoPessoa.ondatasetcomplete = dsoTipoPessoa_DSC;
    dsoTipoPessoa.refresh();
}

function dsoTipoPessoa_DSC() {
    clearComboEx(['selTipoPessoa']);
    while (!dsoTipoPessoa.recordset.EOF) {
        optionStr = dsoTipoPessoa.recordset['ItemMasculino'].value;
        optionValue = dsoTipoPessoa.recordset['ItemID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selTipoPessoa.add(oOption);
        dsoTipoPessoa.recordset.MoveNext();
    }
}
/**************************************************
**************************************************/
function fillClassificacao(Parceiro) 
{
if (selParceiroID.selectedIndex >= 1) 
{
sClassificacaoInterna = selParceiroID.options(selParceiroID.selectedIndex).getAttribute('ClassificacaoInterna', 1);
}

setConnection(dsoCla);
    
if (Parceiro == 1) 
{
dsoCla.SQL = 'SELECT ItemID, ItemMasculino ' +
'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
'WHERE TipoID = 29 AND EstadoID = 2 AND ItemMasculino = ' + "'" + sClassificacaoInterna + "'";

dsoCla.ondatasetcomplete = dsoCla_DSC;
dsoCla.refresh();
}
else 
{
dsoCla.SQL = 'SELECT ItemID, ItemMasculino ' +
'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
'WHERE TipoID = 29 AND EstadoID = 2';

dsoCla.ondatasetcomplete = dsoCla_DSC;
dsoCla.refresh();
}
}

function dsoCla_DSC() 
{
clearComboEx(['selClassificacoesClaID']);
while (!dsoCla.recordset.EOF) 
{
optionStr = dsoCla.recordset['ItemMasculino'].value;
optionValue = dsoCla.recordset['ItemID'].value;
var oOption = document.createElement("OPTION");
oOption.text = optionStr;
oOption.value = optionValue;
selClassificacoesClaID.add(oOption);
dsoCla.recordset.MoveNext();
}
}


function ChaveLimpaPessoa() 
{
    clearComboEx(['selPessoaID']);
    selPessoaID.disabled = true;
    txtFiltroPessoa.disabled = true;
}
/********************************************************************/

function fillCmbParceiro() {
    
    if ( glb_TimerListaPrecoVar2 != null )
    {
        window.clearInterval(glb_TimerListaPrecoVar2);
        glb_TimerListaPrecoVar2 = null;
    }
    
    clearComboEx(['selParceiroID']);
    
    while (! dsoParceiros.recordset.EOF) {
        var oOption = document.createElement("OPTION");
        oOption.text = (i==0 ? '' : dsoParceiros.recordset['Fantasia'].value);
        oOption.value = (i==0 ? 0 : dsoParceiros.recordset['fldID'].value);
		oOption.setAttribute('ListaPreco', dsoParceiros.recordset['ListaPreco'].value, 1);
		oOption.setAttribute('EmpresaSistema', dsoParceiros.recordset['EmpresaSistema'].value, 1);
		oOption.setAttribute('PMP', dsoParceiros.recordset['PMP'].value, 1);
		oOption.setAttribute('NumeroParcelas', dsoParceiros.recordset['NumeroParcelas'].value, 1);
		oOption.setAttribute('FinanciamentoPadrao', dsoParceiros.recordset['FinanciamentoPadrao'].value, 1);
		oOption.setAttribute('ClassificacaoInterna', dsoParceiros.recordset['ClassificacaoInterna'].value, 1);
		oOption.setAttribute('ClassificacaoExterna', dsoParceiros.recordset['ClassificacaoExterna'].value, 1);
		oOption.setAttribute('Deletar', '0', 1);

        selParceiroID.add(oOption);
        dsoParceiros.recordset.moveNext();
    }

    if (selParceiroID.options.length > 1) {
        selParceiroID.disabled = false;
        selParceiroID.selectedIndex = 0;
    }
    else
        selParceiroID.disabled = true;

    setLabelParceiro();
    selParceiroID_OnChange(false);
}

function addCmbParceiro()
{
    if ( glb_TimerListaPrecoVar2 != null )
    {
        window.clearInterval(glb_TimerListaPrecoVar2);
        glb_TimerListaPrecoVar2 = null;
    }
    var insereCmb;
    var i;
    var sFantasia = glb_aDadosPessoa[0];
    var nPessoaID = glb_aDadosPessoa[1];
    var nListaPreco = glb_aDadosPessoa[4];
    var nEmpresaSistema = glb_aDadosPessoa[3];
    var sPMP = glb_aDadosPessoa[5];
    var nNumeroParcelas = glb_aDadosPessoa[6];
    var nFinanciamentoPadrao = glb_aDadosPessoa[7];
    var sClassificacaoInterna = glb_aDadosPessoa[8];
    var sClassificacaoExterna = glb_aDadosPessoa[9];
    
    if ((nPessoaID==0) || (nPessoaID==null) || isNaN(nPessoaID))
        return null;

    for (i = 0; i < selParceiroID.length; i++) 
    {
        if (glb_aDadosPessoa[1] == selParceiroID.options[i].value) 
        {
            insereCmb = 1;
        }
    }
    
    if (insereCmb != 1) 
    {
        var oOption = document.createElement("OPTION");
        oOption.text = (i==0 ? '' : glb_aDadosPessoa[0]);
        oOption.value = (i==0 ? 0 : glb_aDadosPessoa[1]);
		oOption.setAttribute('ListaPreco', glb_aDadosPessoa[2], 1);
		oOption.setAttribute('EmpresaSistema', glb_aDadosPessoa[3], 1);
		oOption.setAttribute('PMP', glb_aDadosPessoa[4], 1);
		oOption.setAttribute('NumeroParcelas', glb_aDadosPessoa[5], 1);
		oOption.setAttribute('FinanciamentoPadrao', glb_aDadosPessoa[6], 1);
		oOption.setAttribute('ClassificacaoInterna', glb_aDadosPessoa[7], 1);
		oOption.setAttribute('ClassificacaoExterna', glb_aDadosPessoa[8], 1);
		oOption.setAttribute('Deletar', '1', 1);

		selParceiroID.add(oOption);
    }
    
    dsoParceiros.recordset.setFilter('');

    if ((selParceiroID.options.length > 1) && (glb_bEhCliente))
    {
        selParceiroID.disabled = false;
        //selOptByValueInSelect(getHtmlId(), 'selParceiroID', nPessoaID);
    }
    else 
    {
        selParceiroID.disabled = true;
    }

    selOptByValueInSelect(getHtmlId(), 'selParceiroID', nPessoaID);

    setLabelParceiro();
    selParceiroID_OnChange(false);
    txtFiltroPessoa.disabled = false;
}

function delOptCmbParceiro()
{
    var retVal = false;
    var cmb = eval('selParceiroID');
    var i, j, coll = document.all.tags('SELECT');
    
    for (i=0; i<coll.length; i++)
    {
         if (coll(i).id == cmb.id)
         {
            for (j=coll(i).length - 1; j>=0; j--)
            {
                if ( selParceiroID.options[j].getAttribute('Deletar', 1) == '1' )                
                {
                    coll(i).remove(j);
                }    
            }
         }
    }
}

function selParceiroID_OnChange(bLoadModalAtendimento) 
{
    if (glb_currMode == 'PL')
        zeraGrid();

    ChavePessoaCotacao_OnChangeParceiro();

    setLabelParceiro();

    clearComboEx(['selTransacao']);
    enableDisableFieldsByParceiro();

    fillComboFinanciamento();
    fillComboPessoa();

    if ((bLoadModalAtendimento) && (selParceiroID.value > 0))
        btnCRM_onclick();

    if (glb_IniciarFinalizar == 1) {
        selParceiroID.disabled = true;
    }

    if ((glb_nParceiroID != selParceiroID.value) /*&& (selParceiroID.value > 0) && (glb_nParceiroID != null)*/)
        glb_bCarregaReceptivo = true;
    else
        glb_bCarregaReceptivo = false;

    if (glb_bCarregaReceptivo) {
        for (var i = 0; i < glb_aDadosSelecionados.length; i++) {
            if (glb_aDadosSelecionados[i][0] == "chkAtivo") {
                glb_aDadosSelecionados[i][1] = false;
                //break;
            }
            else if (glb_aDadosSelecionados[i][0] == "txtPesquisa") {
                glb_aDadosSelecionados[i][1] = '';
                //break;
            }
        }

        glb_bCarregaReceptivo = false;
    }
}

//Presto
function selTipoPessoa_OnChange() 
{
    selTipoPessoa_Comportamento();
}
//Presto
function selTipoPessoa_Comportamento() 
{
    //Se for Pessoa Fisica
    if (selTipoPessoa.selectedIndex == 1) {
        chkContribuinte.disabled = true;
        chkSimplesNacional.disabled = true;
        chkSuframaID.disabled = true;

        chkContribuinte.checked = false;
        chkSimplesNacional.checked = false;
        chkSuframaID.checked = false;

        selPessoaSemPessoa_FinalidadeTransacao();
        
    }
    //Se for Pessoa Juridica
    else if (selTipoPessoa.selectedIndex == 0) {
        chkContribuinte.disabled = false;
        chkSimplesNacional.disabled = false;
        chkSuframaID.disabled = false;

        chkContribuinte.checked = true;
        chkSimplesNacional.checked = false;
        chkSuframaID.checked = false;

        selPessoaSemPessoa_FinalidadeTransacao();
    }
    else if (selChavePessoaID.value == 2) {
        selPessoaSemPessoa_FinalidadeTransacao();
    }
    
}

function ChavePessoaCotacao_OnChangeParceiro()
{
    /*
    if (selChavePessoaID.value == 1)
    {
        if (selParceiroID.selectedIndex >= 1) 
        {
            selFrete.disabled = false;
        }
        else 
        {
            selFrete.disabled = true;
            selFrete.selectedIndex = 0;
        }    
    }
    else 
    */
    if (selChavePessoaID.value != 1)
    {
        if (selParceiroID.selectedIndex >= 1) 
        {
            fillClassificacao(1);
            chkFaturamentoDireto.checked = true;
            chkFaturamentoDireto.disabled = true;
            chkContribuinte.checked = true;

            selClassificacoesClaID.disabled = true;

            selUFID.selectedIndex = 0;

            clearComboEx(['selCidadeId']);
            selCidadeId.disabled = true;
        }
        else
        {
            fillClassificacao(0);
            chkFaturamentoDireto.disabled = false;
            chkContribuinte.checked = true;

            selClassificacoesClaID.disabled = false;

            selUFID.selectedIndex = 0;

            clearComboEx(['selCidadeId']);
            selCidadeId.disabled = true;
        }
    }
}


function enableDisableFieldsByParceiro() 
{
    if (glb_currMode == 'DET') 
    {
        if (selChavePessoaID.value == 1) 
        {
            selUFID.selectedIndex = 0;
        }
        selParceiroID.disabled = true;
        selUFID.disabled = true;
        selIsencoesID.disabled = false;
        //selFrete.disabled = false;
        selCidadeId.disabled = false;
        selTransacao.disabled = false;
        selFinalidade.disabled = false;
    }
}

function fillComboFinanciamento()
{
	var nPMP = 0;
	var nNumeroParcelas = 0;
	var nFinanciamentoPadraoID = 2;
	var bParceiro = false;
	var dsoEstaticCmbs = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoEstaticCmbs');
	
	if ((selParceiroID.options.length != 0) && (selParceiroID.value > 0))
		bParceiro = true;

	if (bParceiro)
	{
		nPMP = parseInt(selParceiroID.options(selParceiroID.selectedIndex).getAttribute('PMP', 1), 10);
		nNumeroParcelas = parseInt(selParceiroID.options(selParceiroID.selectedIndex).getAttribute('NumeroParcelas', 1), 10);
		nFinanciamentoPadraoID = parseInt(selParceiroID.options(selParceiroID.selectedIndex).getAttribute('FinanciamentoPadrao', 1), 10);
	}

    clearComboEx(['selFinanciamentoID']);
    
    if (! ((dsoEstaticCmbs.recordset.BOF)&&(dsoEstaticCmbs.recordset.EOF)) )
    {
		dsoEstaticCmbs.recordset.moveFirst();

		if (bParceiro)
			dsoEstaticCmbs.recordset.setFilter('PMP <= ' + nPMP + ' AND Indice = ' + '\'' + 3 + '\'');
		else			
			dsoEstaticCmbs.recordset.setFilter('Indice = ' + '\'' + 3 + '\'');

        while (!dsoEstaticCmbs.recordset.EOF)
        {
			var oOption = document.createElement("OPTION");
			oOption.text = dsoEstaticCmbs.recordset['fldName'].value;
			oOption.value = dsoEstaticCmbs.recordset['fldID'].value;
			
			oOption.setAttribute('PMP', dsoEstaticCmbs.recordset['PMP'].value, 1);
			
			selFinanciamentoID.add(oOption);
			dsoEstaticCmbs.recordset.moveNext();
		}
        
        selOptByValueInSelect(getHtmlId(), 'selFinanciamentoID', nFinanciamentoPadraoID);
		setLabelOfControlEx(lblFinanciamentoID, selFinanciamentoID);

        dsoEstaticCmbs.recordset.setFilter('');
    }

	if (glb_currMode == 'DET')
		prazo();

    //lockBtnLupa(btnFindCliente, false);
}

function fillComboPessoa()
{
	if (glb_currMode == 'PL')
	{
		if (dsoPessoa == null)
			dsoPessoa = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoPessoa');
	}

    var nEmpresaSistema = 0;
    var nEmpresaData = getCurrEmpresaData();
    var nParceiroID = 0;

    clearComboEx(['selPessoaID']);
    
    if (selParceiroID.selectedIndex <= 0)
    {
		selUFID.selectedIndex = 0;
		
		if (glb_currMode == 'DET')
			refreshInterface();
		else
			selPessoaID_OnChange(true);
		
		return;
	}

	nEmpresaSistema = parseInt(selParceiroID.options(selParceiroID.selectedIndex).getAttribute('EmpresaSistema', 1), 10);
	
	nParceiroID = selParceiroID.value;
	
    lockInterface(true);
    setConnection(dsoPessoa);

    // dsoPessoa.SQL = 'SELECT 0 AS ParceiroID, SPACE(0) AS Fantasia, 0 AS UFID, 0 AS Parceiro, 0 AS Incidencia, SPACE(0) AS UF, 0 AS SacolaAtual, 0 AS SacolasSalvas UNION ';

    dsoPessoa.SQL = 'SELECT b.PessoaID AS ParceiroID, b.Fantasia + ISNULL(SPACE(1) + \'(\'+ dbo.fn_Pessoa_Documento(a.SujeitoID, NULL, NULL, 101, 111, 0), SPACE(0)) + \')\' AS Fantasia, e.UFID, 0 AS Parceiro, ' +
		    'dbo.fn_Pessoa_Incidencia(' + nEmpresaData[0] + ', b.PessoaID) AS Incidencia, dbo.fn_Localidade_Dado(e.UFID, 2) AS UF, ' +
            'ISNULL(dbo.fn_SacolasCompras_Atual(b.PessoaID, b.PessoaID, ' + glb_USERID + ', NULL), 0) AS SacolaAtual, ' +
            '(SELECT COUNT(1) ' +
                'FROM SacolasCompras aa WITH(NOLOCK) ' + 
                'WHERE (aa.ParceiroID = b.PessoaID) AND (aa.PessoaID = b.PessoaID) AND (aa.EstadoID IN (21, 22, 24)) ' + 
                    'AND (ISNULL(dbo.fn_SacolasCompras_Atual(aa.ParceiroID, aa.PessoaID, ' + glb_USERID + ', aa.SacolaID), 0) <> aa.SacolaID) AND (aa.SacolaFinalizada = 0)) AS SacolasSalvas ' +
		'FROM RelacoesPessoas a WITH(NOLOCK) ' +
            'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.SujeitoID = b.PessoaID) ' +
            'INNER JOIN Pessoas_Creditos bb WITH(NOLOCK) ON (bb.PessoaID = b.PessoaID) ' +
		    'INNER JOIN FinanciamentosPadrao c WITH(NOLOCK) ON (ISNULL(bb.FinanciamentoLimiteID, 2) = c.FinanciamentoID) ' +
		    'LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (b.PessoaID=e.PessoaID AND e.Ordem=1) ' +
		'WHERE (a.ObjetoID = ' + nEmpresaData[0] + ' AND a.SujeitoID = ' + nParceiroID + ' AND ' +
        'a.EstadoID = 2 AND a.TipoRelacaoID = 21 AND b.EstadoID = 2) AND bb.PaisID = ' + nEmpresaData[1];
	
	// Soh adiciona os clientes se o parceiro nao for empresa do grupo
	if (nEmpresaSistema != '1' || nParceiroID == 127521 || nParceiroID == 255534)
	{
		dsoPessoa.SQL += 'UNION ' +
		'SELECT b.PessoaID AS ParceiroID, b.Fantasia + ISNULL(SPACE(1) + \'(\'+ dbo.fn_Pessoa_Documento(a.SujeitoID, NULL, NULL, 101, 111, 0), SPACE(0)) + \')\' AS Fantasia, e.UFID, 1 AS Parceiro, ' +
		    'dbo.fn_Pessoa_Incidencia(' + nEmpresaData[0] + ', b.PessoaID) AS Incidencia, dbo.fn_Localidade_Dado(e.UFID, 2) AS UF, ' +
            'ISNULL(dbo.fn_SacolasCompras_Atual(a.ObjetoID, a.SujeitoID, ' + glb_USERID + ', NULL), 0) AS SacolaAtual, ' +
            '(SELECT COUNT(1) ' +
                'FROM SacolasCompras aa WITH(NOLOCK) ' +
                'WHERE (aa.ParceiroID = a.ObjetoID) AND (aa.PessoaID = a.SujeitoID) AND (aa.EstadoID IN (21, 22, 24)) ' +
                    'AND (ISNULL(dbo.fn_SacolasCompras_Atual(aa.ParceiroID, aa.PessoaID, ' + glb_USERID + ', aa.SacolaID), 0) <> aa.SacolaID)) AS SacolasSalvas ' +
		'FROM RelacoesPessoas a WITH(NOLOCK) ' +
            'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.SujeitoID = b.PessoaID) ' + 
            'INNER JOIN Pessoas_Creditos bb WITH(NOLOCK) ON (bb.PessoaID = b.PessoaID) ' +
		    'INNER JOIN FinanciamentosPadrao c WITH(NOLOCK) ON (ISNULL(bb.FinanciamentoLimiteID, 2) = c.FinanciamentoID) ' + 
		    'LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (b.PessoaID=e.PessoaID AND e.Ordem=1) ' +
		'WHERE (a.ObjetoID = ' + nParceiroID + ' AND ' +
            'a.EstadoID = 2 AND a.TipoRelacaoID = 21 AND b.EstadoID = 2) AND bb.PaisID = ' + nEmpresaData[1];
    }

	dsoPessoa.SQL += 'ORDER BY Parceiro, Fantasia ';

    dsoPessoa.ondatasetcomplete = fillComboPessoa_DSC;
    dsoPessoa.Refresh();
}

function fillComboPessoa_DSC()
{
    var aEmpresaData = getCurrEmpresaData();
	var nEmpresaID = aEmpresaData[0];
	//var sFreteDefault = 0;
	/*
	if ( (nEmpresaID == 2) || (nEmpresaID == 4) )
		sFreteDefault = 1;
	*/

    clearComboEx(['selPessoaID']);
    
    while (! dsoPessoa.recordset.EOF)
    {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoPessoa.recordset['Fantasia'].value;
        oOption.value = dsoPessoa.recordset['ParceiroID'].value;
        oOption.setAttribute('UFID', dsoPessoa.recordset['UFID'].value, 1);
        oOption.setAttribute('UF', dsoPessoa.recordset['UF'].value, 1);
        oOption.setAttribute('Incidencia', dsoPessoa.recordset['Incidencia'].value, 1);
        oOption.setAttribute('SacolaAtual', dsoPessoa.recordset['SacolaAtual'].value, 1);
        oOption.setAttribute('SacolasSalvas', dsoPessoa.recordset['SacolasSalvas'].value, 1);
        selPessoaID.add(oOption);
        dsoPessoa.recordset.moveNext();
    }

	lockInterface(false);

/*
	if (selFrete.options.length > 1) {
	    selFrete.selectedIndex = sFreteDefault;
	}
*/

	if ((selPessoaID.options.length != 0) && (glb_IniciarFinalizar != 1))
	{
		selPessoaID.selectedIndex = 0;
		selPessoaID.disabled = false;
		txtFiltroPessoa.disabled = false;
	}/*
	else {
	    txtFiltroPessoa.disabled = true;
	    selPessoaID.disabled = true;
	}*/
		

	if (glb_currMode == 'DET')
		refreshInterface();

    // Se veio da modal com EmailMarketing selecionado, lista todos os produtos do EmailMarketing
	if ((glb_nEmaClienteID != null) && (selChavePessoaID.value == 1) && (glb_bCarregaProdutosOferta))
	{
	    txtArgumento.value = '';
	    __btn_LIST('sup');
	    glb_bCarregaProdutosOferta = false;
	}

    selPessoaID_OnChange(false);

    if (selChavePessoaID.value == 2) 
    {
        ChaveLimpaPessoa();
    }
}

function selPessoaID_OnChange(bRefresh) 
{
    var bCotador;
    
    try
    {
        bCotador = (selFonteID.value == 3 ? true : false);
    }
    catch(e)
    {
        bCotador = false;
    }

    try
    {
        if (glb_TimerPessoaOnChange != null)
        {
            window.clearInterval(glb_TimerPessoaOnChange);
            glb_TimerPessoaOnChange = null;
        }
    }
    catch(e)
    {
        ;
    }

    setLabelUFInPessoa();
    
    //selPessoaID.disabled = ((selPessoaID.options.length == 0) || (glb_IniciarFinalizar == 1));
    //txtFiltroPessoa.disabled = ((selPessoaID.options.length == 0) || (glb_IniciarFinalizar == 1));
    selPessoaID.disabled = (selPessoaID.options.length == 0);

    if (glb_currMode != 'DET')
        txtFiltroPessoa.disabled = (selPessoaID.options.length == 0);

    if (selPessoaID.options.length > 0)
    {
        glb_nSacolaAtual = selPessoaID.options(selPessoaID.selectedIndex).getAttribute('SacolaAtual', 1);
        glb_nSacolasSalvas = selPessoaID.options(selPessoaID.selectedIndex).getAttribute('SacolasSalvas', 1);
        glb_bVerificaSacola = true;
    }

    glb_tempRefresh = bRefresh;
    selPessoaSemPessoa_FinalidadeTransacao();

    if ((bCotador) && (selCidadeId.value > 0))
        buscaPessoaCotador();
}
function selPessoaSemPessoa_FinalidadeTransacao() 
{
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];

    glb_DsoToReturn = 2;

    setConnection(dsoFinalidade);
    setConnection(dsoCFOP);

    var sVendaOrdem = '';
    var sContribuinte = '';
    var MudaTipoCotacao = 0;

    if (chkContribuinte.checked)
        MudaTipoCotacao = 1;

    if (((selPessoaID.selectedIndex >= 0) && (selChavePessoaID.value == 1)) || (selChavePessoaID.value == 2)) {
        if (selPessoaID.selectedIndex >= 0) {
            sVendaOrdem = 'OR (a.OperacaoID = 125 AND dbo.fn_Pessoa_Incidencia(' + nEmpresaID + ',-' + selPessoaID.value + ') = 0) ';
            sPessoaIncidencia = 'OR (a.OperacaoID = 125 AND dbo.fn_Pessoa_Incidencia(' + nEmpresaID + ',-' + selPessoaID.value + ') = 0) ';
        }
        else if (chkContribuinte.checked) {
            sVendaOrdem = 'OR (a.OperacaoID = 125)';
        }
        else {
            sVendaOrdem = '';
        }
        dsoCFOP.SQL = 'SELECT a.OperacaoID AS TransacaoID, dbo.fn_Tradutor(a.Operacao, ' + aEmpresaData[7] + ', ' + aEmpresaData[8] + ', NULL) AS Transacao ' +
                          'FROM Operacoes a WITH(NOLOCK) ' +
                            'WHERE  ((a.OperacaoID = 115) ' + sVendaOrdem +
                                    'AND a.EstadoID = 2 ' +
                                    'AND a.TipoOperacaoID = 652 ' +
                                    'AND a.Nivel = 3 ' +
                                    'AND a.Resultado = 1 ' +
                                    'AND a.Entrada = 0) ' +
                        'ORDER BY a.Ordem';
        sFrom = '';

        if (selChavePessoaID.value == 1) 
        {
            sFrom = 'INNER JOIN Pessoas b WITH(NOLOCK) ON a.Filtro LIKE \'%(\' + CONVERT(VARCHAR(16), b.ClassificacaoID) +\')%\' AND b.PessoaID = ' + selPessoaID.value;
            sContribuinte = '~dbo.fn_Pessoa_Incidencia(' + nEmpresaID + ',-b.PessoaID)';
        }
        else if (MudaTipoCotacao == 1) 
        {
            sContribuinte = 1;
        }
        else 
        {
            sContribuinte = escape(chkContribuinte.checked ? 1 : 0);
        }

        dsoFinalidade.SQL = 'SELECT a.EhDefault, a.ItemID AS ItemID,  ' + (glb_currMode == 'DET' ? 'a.ItemFeminino' : 'a.ItemMasculino') + '  AS Finalidade, a.Filtro ' +
                                'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' + sFrom +
                                'WHERE (a.EstadoID = 2 AND a.TipoID = 418) ' +
                                        'AND (((' + sContribuinte + ' = 1) AND ((a.Filtro LIKE \'%<CB1>%\') OR (a.Filtro LIKE \'%<CB@_>%\' ESCAPE \'@\'))) ' +
			                            'OR ((' + sContribuinte + ' = 0) AND ((a.Filtro LIKE \'%<CB0>%\') OR (a.Filtro LIKE \'%\<CB@_>%\' ESCAPE \'@\')))) ' +
                                'ORDER BY a.Ordem';
    }
    else 
    {
        dsoCFOP.SQL = 'SELECT 0 AS TransacaoID, SPACE(0) AS Transacao ' +
            'FROM Operacoes a WITH(NOLOCK) ' +
            'WHERE a.OperacaoID=-1';

        dsoFinalidade.SQL = 'SELECT a.EhDefault, a.ItemID AS ItemID,  a.ItemMasculino AS Finalidade ' +
            'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
            'WHERE a.ItemID = -1 ';
    }
    dsoFinalidade.ondatasetcomplete = dsoCFOP_DSC;
    dsoFinalidade.Refresh();

    dsoCFOP.ondatasetcomplete = dsoCFOP_DSC;
    dsoCFOP.Refresh();
}

function dsoCFOP_DSC() 
{
    var nOldValueTransacao = 0;

    glb_DsoToReturn--;

    if (glb_DsoToReturn > 0)
        return;

    if (selTransacao.selectedIndex >= 0)
        nOldValueTransacao = selTransacao.value;

    clearComboEx(['selTransacao']);

    if (dsoCFOP.recordset.fields.Count > 0) {
        if (!((dsoCFOP.recordset.BOF) && (dsoCFOP.recordset.EOF))) {
            while (!dsoCFOP.recordset.EOF) {
                var oOption = document.createElement("OPTION");
                oOption.text = dsoCFOP.recordset['Transacao'].value;
                oOption.value = dsoCFOP.recordset['TransacaoID'].value;

                if (nOldValueTransacao == oOption.value)
                    oOption.selected = true;

                selTransacao.add(oOption);
                dsoCFOP.recordset.moveNext();
            }
        }
    }

    selTransacao.disabled = (selTransacao.options.length == 0);

    fillComboFinalidade();

    if (!glb_tempRefresh) {
        enableDisableFieldsByParceiro();
    }
    else {
        lockInterface(false);

        if (glb_currMode == 'DET')
            glb_callRefreshInterface = window.setInterval('callRefreshInterface()', 50, 'JavaScript');
    }

    /*
    if (glb_bVerificaSacola)
    {
        glb_bVerificaSacola = false;

        if ((glb_nSacolaAtual == 0) && (glb_nSacolasSalvas > 0))
        {
            var retMsg = window.top.overflyGen.Confirm('Esta pessoa possui ' + glb_nSacolasSalvas + ' sacolas salvas.\nDeseja ver?');

            if (retMsg == 1)
            {
                openModalTrolley_SacolasSalvas('PL');
            }
        }
    }
    */

    selPessoaID.focus();
}

function fillComboFinalidade() 
{
    var nOldValueFinalidade = 0;
    var sFiltro = '';
    var nEhDefault;

    var nTransacaoID = selTransacao.value;

    if (selFinalidade.selectedIndex >= 0)
        nOldValueFinalidade = selFinalidade.value;

    clearComboEx(['selFinalidade']);

    if (dsoFinalidade.recordset.fields.Count > 0) 
    {
        if (!((dsoFinalidade.recordset.BOF) && (dsoFinalidade.recordset.EOF))) 
        {
            dsoFinalidade.recordset.moveFirst();

            while (!dsoFinalidade.recordset.EOF) 
            {
                if (dsoFinalidade.recordset['Filtro'] != null) 
                {
                    sFiltro = dsoFinalidade.recordset['Filtro'].value;
                }
                else 
                {
                    sFiltro = null;
                }

                if (sFiltro != null && sFiltro.indexOf('{' + nTransacaoID + '}') != -1) 
                {
                    var oOption = document.createElement("OPTION");

                    oOption.text = dsoFinalidade.recordset['Finalidade'].value;
                    oOption.value = dsoFinalidade.recordset['ItemID'].value;

                    if (glb_currMode == 'PL') 
                    {
                        nEhDefault = dsoFinalidade.recordset['EhDefault'].value;
                        if ((selFinalidade.selectedIndex < 0) || (nEhDefault == 1))
                            oOption.selected = true;
                    }
                    else 
                    {
                        if (nOldValueFinalidade == oOption.value)
                            oOption.selected = true;
                    }

                    selFinalidade.add(oOption);
                }

                dsoFinalidade.recordset.moveNext();
            }
        }
    }

    selFinalidade.disabled = (selFinalidade.options.length == 0);
}

function callRefreshInterface()
{
    if ( glb_callRefreshInterface != null )
    {
        window.clearInterval(glb_callRefreshInterface);
        glb_callRefreshInterface = null;
    }

    refreshInterface();
}

function cloneControlsContentsAndState()
{
	var strWindows_Pt = '';
	setlistOrDet();

	if (glb_currMode == 'DET') 
	{
	    strWindows_Pt = 'PESQLIST_HTML';
	}
	else 
	{
	    strWindows_Pt = 'SUP_HTML';
	    
	}
	
	var i=0;
	var j=0;
	var k=0;
	var strAttr = '';
	
	var lblParceiroID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'lblParceiroID');
	var selParceiroID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selParceiroID');
	var lblPessoaID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'lblPessoaID');
	var selPessoaID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selPessoaID');
	var lblTransacao_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'lblTransacao');
	var selTransacao_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selTransacao');
	var lblFinalidade_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'lblFinalidade');
	var selFinalidade_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selFinalidade');
	var lblUFID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'lblUFID');
	var selUFID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selUFID');
	var selCidadeId_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selCidadeId');
	//var lblFrete_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'lblFrete');
	var lblFinanciamentoID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'lblFinanciamentoID');
	var selFinanciamentoID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selFinanciamentoID');
	var lblQuantidade_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'lblQuantidade');
	var txtQuantidade_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'txtQuantidade');
	var lblMoedaConversaoID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'lblMoedaConversaoID');
	var selMoedaConversaoID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selMoedaConversaoID');
	//var selFrete_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selFrete');
	var selTipoPessoa_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selTipoPessoa');
	var chkContribuinte_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'chkContribuinte');
	var chkFaturamentoDireto_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'chkFaturamentoDireto');
	var chkSimplesNacional_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'chkSimplesNacional');
	var chkSuframaID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'chkSuframaID');
	var selClassificacoesClaID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selClassificacoesClaID');
	var selClassificacoesID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selClassificacoesID');
	var selIsencoesID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selIsencoesID');
	var txtCnae_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'txtCnae');
	var selChavePessoaID_Pt = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'selChavePessoaID');
	
	if (glb_currMode == 'PL')
	{
		if ((selParceiroID_Pt.value != selParceiroID.value) ||
			(selPessoaID_Pt.value != selPessoaID.value) ||
			(selFinanciamentoID_Pt.value != selFinanciamentoID.value) ||
			(selMoedaConversaoID_Pt.value != selMoedaConversaoID.value))/* ||
			(selFrete_Pt.value != selFrete.value))*/
			zeraGrid();
	}

	glb_aDadosPessoa = sendJSMessage(strWindows_Pt, JS_DATAINFORM, EXECEVAL, 'glb_aDadosPessoa');

	lblParceiroID.innerText = lblParceiroID_Pt.innerText;
	lblPessoaID.innerText = lblPessoaID_Pt.innerText;
	lblTransacao.innerText = lblTransacao_Pt.innerText;
	lblFinalidade.innerText = lblFinalidade_Pt.innerText;
	lblFinanciamentoID.innerText = lblFinanciamentoID_Pt.innerText;

	txtQuantidade.value = txtQuantidade_Pt.value;
	txtQuantidade.disabled = txtQuantidade.disabled;

	txtCnae.value = txtCnae_Pt.value;
	txtCnae.disabled = txtCnae.disabled;

	selChavePessoaID.value = selChavePessoaID_Pt.value;
	selChavePessoaID.disabled = selChavePessoaID.disabled;

	chkContribuinte.checked = (chkContribuinte_Pt.checked);
	chkFaturamentoDireto.checked = (chkFaturamentoDireto_Pt.checked);
	chkSimplesNacional.checked = (chkSimplesNacional_Pt.checked);
	chkSuframaID.checked = (chkSuframaID_Pt.checked);

	clearComboEx(['selParceiroID', 'selPessoaID', 'selTransacao', 'selFinalidade', 'selUFID', 'selCidadeId', 'selFinanciamentoID', 'selMoedaConversaoID', /*'selFrete',*/ 'selClassificacoesClaID', 'selClassificacoesID', 'selIsencoesID', 'selTipoPessoa']);

	var aCmbsDynamics = new Array(selParceiroID, selPessoaID, selTransacao, selFinalidade, selUFID, selCidadeId, selFinanciamentoID, selMoedaConversaoID, /*selFrete,*/ selClassificacoesClaID, selClassificacoesID, selIsencoesID, selTipoPessoa);
	var aCmbsDynamics_Pt = new Array(selParceiroID_Pt, selPessoaID_Pt, selTransacao_Pt, selFinalidade_Pt, selUFID_Pt, selCidadeId_Pt, selFinanciamentoID_Pt, selMoedaConversaoID_Pt, /*selFrete_Pt,*/ selClassificacoesClaID_Pt, selClassificacoesID_Pt, selIsencoesID_Pt, selTipoPessoa_Pt);
	var option_Pt = null;
    
    for (i=0; i<aCmbsDynamics.length; i++)
    {
        for (j=0; j<aCmbsDynamics_Pt[i].options.length; j++)
        {
			option_Pt = aCmbsDynamics_Pt[i].options[j];
            var oOption = document.createElement("OPTION");
            oOption.text = option_Pt.text;
            
            for (k=0; k<option_Pt.attributes.length; k++)
            {
				if (option_Pt.attributes[k].specified)
					oOption.setAttribute(option_Pt.attributes[k].nodeName, option_Pt.attributes[k].nodeValue, 1);
			}
			
			aCmbsDynamics[i].options.add(oOption);
        }

        aCmbsDynamics[i].selectedIndex = aCmbsDynamics_Pt[i].selectedIndex;
        aCmbsDynamics[i].disabled = aCmbsDynamics_Pt[i].disabled;
    }

    setLabelParceiro();
    setLabelUFInPessoa();
    setLabelOfControlEx(lblFinanciamentoID, selFinanciamentoID);

    selParceiroID.disabled = false;
    selClassificacoesID.disabled = false;
    selUFID.disabled = false;
    if (selParceiroID.selectedIndex < 1)
        selClassificacoesClaID.disabled = false;
    
}

function ChavePessoaCotacao(ZerarGrid) 
{
    if (ZerarGrid == 1) 
    {
        zeraGrid();
    }
    
    selTransacao.onchange = selTransacao_onchange;
    selFinalidade.onchange = selFinalidade_onchange;
    selTipoRelacaoID.onchange = selTipoRelacaoID_onchange;
    txtQuantidade.onkeydown = txtQuantidade_onkeydown;
    selProdutoReferenciaID.onchange = selProdutoReferenciaID_onchange;
    
    if (selChavePessoaID.value == 1) 
    {
        if (selParceiroID.selectedIndex < 1) 
        {
            //selFrete.selectedIndex = 0;
            fillComboPessoa();
            selChavePessoaID.onclick = ControleAtendimentoFinaliza;
        }
        else 
        {
            fillComboPessoa();
            /*
            if (aEmpresa[1] != 167)
            {
                selFrete.selectedIndex = 1;
            }
            else 
            {
                selFrete.selectedIndex = 0;
            }
            */
        }
        ChaveLimpaPessoa();
    }
    else 
    {
        chkContribuinte.checked = true;
        //selPessoaID_OnChange();
        setLabelUFInPessoa();
        selPessoaID.disabled = (selPessoaID.options.length == 0);

        if (glb_currMode != 'DET')
            txtFiltroPessoa.disabled = (selPessoaID.options.length == 0);

        glb_tempRefresh = undefined;

        selUFID.onchange = Fillcmbcidade;
        fillIsencoes();
        fillCmbCanal();
        fillCmbTipoPessoa();           
        ChaveLimpaPessoa();

        if (selParceiroID.selectedIndex >= 1)
        {
            if ((selUFID.selectedIndex >= 1) && (selCidadeId.selectedIndex >= 1)) 
            {
                selCidadeId.disabled = false;
            }
            else 
            {
                clearComboEx(['selCidadeId']);
                selCidadeId.disabled = true;
            }
        }
        else 
        {
            clearComboEx(['selCidadeId']);
            selCidadeId.disabled = true;
        }

        if (selParceiroID.selectedIndex < 1) 
        {
            /*
            if (aEmpresa[1] != 167) 
            {
                selFrete.disabled = false;
                selFrete.selectedIndex = 1;
            }
            else 
            {
                selFrete.disabled = false;
                selFrete.selectedIndex = 0;
            }
            */
            selTransacao.disabled = false;
            selFinalidade.disabled = false;

            chkFaturamentoDireto.checked = true;

            selClassificacoesClaID.disabled = false;
            fillClassificacao(0);
        }
        else 
        {
            /*
            if (aEmpresa[1] != 167) 
            {
                selFrete.disabled = false;
                selFrete.selectedIndex = 1;
            }
            else 
            {
                selFrete.disabled = false;
                selFrete.selectedIndex = 0;
            }
            */
            selTransacao.disabled = false;
            selFinalidade.disabled = false;

            chkFaturamentoDireto.checked = true;
            chkFaturamentoDireto.disabled = true;

            selClassificacoesClaID.disabled = true;
            fillClassificacao(1);
        }
    }
}

 function setLabelParceiro() 
    {
        var sClassificacaoInterna = '';
        var sClassificacaoExterna = '';

        lblParceiroID.style.width = '170px';
        lblParceiroID.innerHTML = 'Parceiro';

        if (selParceiroID.selectedIndex >= 1) 
        {
            lblParceiroID.innerHTML = lblParceiroID.innerText + ' ' + selParceiroID.value;

            /*sClassificacaoInterna = selParceiroID.options(selParceiroID.selectedIndex).getAttribute('ClassificacaoInterna', 1);
            sClassificacaoExterna = selParceiroID.options(selParceiroID.selectedIndex).getAttribute('ClassificacaoExterna', 1);

            if (sClassificacaoInterna != '')
                lblParceiroID.innerHTML = lblParceiroID.innerHTML + ' Int: <B>' + sClassificacaoInterna + '</B>';

            if (sClassificacaoExterna != '')
                lblParceiroID.innerHTML = lblParceiroID.innerHTML + '  Ext: <B>' + sClassificacaoExterna + '</B>';*/
        }
    }

    function setLabelUFInPessoa() 
    {
        UFIDLabel = '';
        lblPessoaID.innerHTML = '';

        lblPessoaID.style.width = '170px';
        lblPessoaID.innerHTML = 'Pessoa';

        if (selPessoaID.selectedIndex >= 0) 
        {
            lblPessoaID.innerHTML = lblPessoaID.innerText + ' ' + selPessoaID.value;
            UFIDLabel = selPessoaID.options(selPessoaID.selectedIndex).getAttribute('UF', 1);

            lblPessoaID.innerHTML = lblPessoaID.innerHTML + '&nbsp;&nbsp;&nbsp;&nbsp;UF: <b>' + UFIDLabel + '<b>';
        }
    }


function zeraGrid()
{
	fg.Rows = 1;
	//adjustSupInfControlsBar('PESQMODE');
}

/*******************************************************************
Informa se o form esta em lista ou detalhe
*******************************************************************/
function setlistOrDet()
{
	window.top.__glb_ListOrDet = glb_currMode;
}

/********************************************************************
Usuario clicou o botao especifico do sup - Comprar
********************************************************************/
function purchaseItem(sFromCaller, nRow) 
{
    var sCFOPID = 0;
    var sFinalidade = 0;
    var strPars = '';
    var nFrete = 0;
    var sMeioTransporteID = 0;
    var nEmpresaID = 0;
    var nProdutoID = 0;
    var sGuia = '';
    var nICMSST = 0;
    var nPrecoInterno = 0;
    var nPrecoRevenda = 0;
    var nPrecoUnitario = 0;
    var nQuantidade = 0;
    var nFinanciamentoID = 0;
    var nMoedaID = 0;
    var nPessoaID = 0;
    var nParceiroID = 0;
    var sFrete = '';
    var nLoteID = 0;
    var nCorpProdutoID = 0;
    var nDesconto = 0;
    var sSacolaObservacoes = '';
    var nEmpresaData = getCurrEmpresaData();
    glb_nRow = nRow;
    glb_sFromCaller = sFromCaller;

    if (isNaN(nRow))
        nRow = fg.Row;

    lockInterface(true);

    if (sFromCaller == 'S') 
    {
        nEmpresaID = dsoSup01.recordset['EmpresaID'].value;
        nProdutoID = txtRegistroID.value;
        
        if (txtLote.value != '')
            nLoteID = txtLote.value;
        else
            nLoteID = 0;
            
        nPrecoInterno = txtPrecoConvertido.value;
        nPrecoRevenda = txtPrecoRevenda.value;
        nPrecoUnitario = txtPrecoFaturamento.value;
        nQuantidade = (isNaN(txtQuantidade.value) ? 1 : txtQuantidade.value);
        sCFOPID = glb_CFOPID;
    }
    else if (sFromCaller == 'PL') 
    {
        nEmpresaID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'EmpresaID'));
        nProdutoID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ConceitoID*'));
        nCorpProdutoID = 0;
        
        if (getColIndexByColKey(fg, 'Lote*') >= 0)
            nLoteID = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'Lote*'));
            
        nPrecoInterno = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'Preco'));
        nPrecoRevenda = nPrecoInterno;
        nPrecoUnitario = nPrecoInterno;
        nQuantidade = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'QuantidadeComprar'));

        if (nEmpresaData[1] == 130) {
            sGuia = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Guia'));
            nICMSST = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'ICMSST_Unitario'));
            sCFOPID = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'CFOP*'));
        }

        if (isNaN(nQuantidade) || nQuantidade <= 0)
            nQuantidade = (isNaN(txtQuantidade.value) ? 1 : txtQuantidade.value);

        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'QuantidadeComprar')) = 0;
   }
   else if (sFromCaller == 'PP') 
   {
       nEmpresaID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'EmpresaID'));
       nProdutoID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ConceitoID*'));
       nCorpProdutoID = 0;

       if (getColIndexByColKey(fg, 'Lote*') >= 0)
           nLoteID = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'Lote*'));

       nPrecoInterno = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'Preco'));
       nPrecoRevenda = nPrecoInterno;
       nPrecoUnitario = nPrecoInterno;
       nQuantidade = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'QuantidadeComprar'));
       if (nEmpresaData[1] == 130) {
           sCFOPID = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'CFOP*'));
       } 

       fg.TextMatrix(nRow, getColIndexByColKey(fg, 'QuantidadeComprar')) = 0;
   }
   else if (sFromCaller == 'PC')
   {
       nEmpresaID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'EmpresaID'));

        if (fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ConceitoID*')) > 0)
            nProdutoID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'ConceitoID*'));
        else
            nProdutoID = 0;

        nCorpProdutoID = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'CorpProdutoID*'));
        nDesconto = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'Desconto'));
        nLoteID = 0;
        nPrecoInterno = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'Preco'));
        nPrecoRevenda = nPrecoInterno;
        nPrecoUnitario = nPrecoInterno;
        nQuantidade = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'QuantidadeComprar'));

        if (nEmpresaData[1] == 130) {
            sGuia = fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Guia'));
            nICMSST = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'ICMSST_Unitario'));
            sCFOPID = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'CFOP*'));
        }

        if (isNaN(nQuantidade) || nQuantidade <= 0)
            nQuantidade = (isNaN(txtQuantidade.value) ? 1 : txtQuantidade.value);

        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'QuantidadeComprar')) = 0;
   }
    else
        return;

    glb_nProdutoID = nProdutoID;
    glb_nQuantidade = nQuantidade;

    nFinanciamentoID = selFinanciamentoID.value;
    nMoedaID = selMoedaConversaoID.value;

    //Compra fonte Cotador sem Pessoa
    if ((sFromCaller == 'PC') && (selChavePessoaID.value == 2)) {
        nPessoaID = glb_nCotadorPessoaID;
        sSacolaObservacoes = '<CotadorSemPessoa>';
    }
    else
        nPessoaID = selPessoaID.value;

    nParceiroID = selParceiroID.value;
    //sFrete = selFrete.value;
    sFinalidade = (selFinalidade.selectedIndex >= 0 ? selFinalidade.value : 0);
    sCFOPID = (sCFOPID == null ? 0 : sCFOPID);
    nICMSST = (nICMSST == null ? 0 : nICMSST);

    nFrete = 0;
    sMeioTransporteID = 0;

    /*
    if (sFrete == 0)
    {
        nFrete = 0;
        sMeioTransporteID = 0;
    }
    else 
    {
        nFrete = 1;

        if (sFrete == 1)
            sMeioTransporteID = 0;
        else
            sMeioTransporteID = sFrete;
    }
    */

    if (nQuantidade > 0) 
    {
        strPars += '?nUserID=' + escape(glb_USERID);
        strPars += '&nEmpresaID=' + escape(nEmpresaID);
        strPars += '&nPessoaID=' + escape(nPessoaID);
        strPars += '&nParceiroID=' + escape(nParceiroID);
        strPars += '&nMoedaID=' + escape(nMoedaID);
        strPars += '&nFinanciamentoID=' + escape(nFinanciamentoID);
        strPars += '&nFrete=' + escape(nFrete);
        strPars += '&sMeioTransporteID=' + escape(sMeioTransporteID);
        strPars += '&nDataLen=' + escape(1);
        strPars += '&nProdutoID=' + escape(nProdutoID);
        strPars += '&nCorpProdutoID=' + escape(nCorpProdutoID);
        strPars += '&nDesconto=' + escape(nDesconto);
        strPars += '&nLoteID=' + escape(nLoteID);
        strPars += '&nQuantidade=' + escape(nQuantidade);
        strPars += '&sGuia=' + escape(sGuia);
        strPars += '&nICMSST=' + escape(nICMSST);
        strPars += '&nPrecoInterno=' + escape(nPrecoInterno);
        strPars += '&nPrecoRevenda=' + escape(nPrecoRevenda);
        strPars += '&nPrecoUnitario=' + escape(nPrecoUnitario);
        strPars += '&sCFOPID=' + escape(sCFOPID);
        strPars += '&sFinalidade=' + escape(sFinalidade);
        strPars += '&sSacolaObservacoes=' + escape(sSacolaObservacoes);

        dsoPurchaseItem.URL = SYS_ASPURLROOT + '/serversidegenEx/purchaseitemtrolley.aspx' + strPars;
        dsoPurchaseItem.ondatasetcomplete = purchaseItem_DSC;
        dsoPurchaseItem.refresh();
    }
}

/********************************************************************
Retorno de compra de item do servidor.
Usuario clicou o botao especifico do sup - Comprar.
********************************************************************/
function purchaseItem_DSC() 
{
    if (!glb_bPaste) {
        glb_nRow++;
        for (var i = glb_nRow; i < fg.Rows; i++) {
            if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
                if (selFonteID.value == 3 && fg.TextMatrix(i, getColIndexByColKey(fg, 'Inconsistencia')).length > 0) {
                    glb_nRow++;
                    glb_bTemInconsistencia = true;
                    continue;
                }

                if (glb_sFromCaller != 'PC') {
                    prepareDataToClipBoard();
                }
                purchaseItem(glb_sFromCaller, glb_nRow);

                break;
            }
            else {
                glb_nRow++;
            }
        }

        if (glb_nRow < fg.Rows)
            return;
    }
    else
        dsoPurchaseItem.parameters = null;

    if (glb_bTemInconsistencia) {
        window.top.overflyGen.Alert('Os produtos com inconsist�ncia n�o v�o para a sacola.');
        glb_bTemInconsistencia = false;
    }

    desmarcaOK();

    lockInterface(false);

    var sMensagem = '';
    var bCotador = (selFonteID.value == 3 ? true : false);

    if (!(dsoPurchaseItem.recordset.BOF && dsoPurchaseItem.recordset.EOF)) {
        sMensagem = dsoPurchaseItem.recordset['Mensagem'].value;

        if (bCotador)
            glb_bCotadorCompra = true;

        if (isNaN(sMensagem.substring(0, 1))) {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }
        else {
            window.status = sMensagem;

            BalanceCampaign();
        }
        return null;
    }
    else {
        if (window.top.overflyGen.Alert('Compra n�o efetuada.') == 0)
            return null;
    }
}

function purchaseGPLEstimate()
{
    var nPessoaID;
    var nParceiroID = selParceiroID.value;
    var nProdutoID = 0;
    var nMoedaID = selMoedaConversaoID.value;
    var sGuia = '';
    var nICMSST = (nICMSST == null ? 0 : nICMSST);
    var sFinalidade = (selFinalidade.selectedIndex >= 0 ? selFinalidade.value : 0);
    var sCFOPID = '';
    var nFinanciamentoID = selFinanciamentoID.value;
    var nFrete = 0;
    var sMeioTransporteID = 0;
    var nLoteID = 0;
    var bWeb = 0;
    var nEmpresaData = getCurrEmpresaData();
    var sSacolaObservacoes = '';
    var strParam = '';


    //Sem Pessoa
    if (selChavePessoaID.value == 2) {
        nPessoaID = glb_nCotadorPessoaID;
        sSacolaObservacoes = '<CotadorSemPessoa>';
    }
    else
        nPessoaID = selPessoaID.value;

    lockInterface(true);

    for (var i = 1; i < fg.Rows; i++) {
        var nEmpresaID = fg.TextMatrix(i, getColIndexByColKey(fg, 'EmpresaID'));

        var sLineNumber = fg.TextMatrix(i, getColIndexByColKey(fg, 'LineNumber'));

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'ConceitoID*')) > 0)
            nProdutoID = fg.TextMatrix(i, getColIndexByColKey(fg, 'ConceitoID*'));
        else
            nProdutoID = 0;

        var nCorpProdutoID = fg.TextMatrix(i, getColIndexByColKey(fg, 'CorpProdutoID*'));

        var sPartNumber = fg.TextMatrix(i, getColIndexByColKey(fg, 'PartNumber'));

        var sDescricaoProduto = fg.TextMatrix(i, getColIndexByColKey(fg, 'Produto'));

        var nVigencia = fg.TextMatrix(i, getColIndexByColKey(fg, 'Vigencia'));

        var nDesconto = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Desconto'));

        var nQuantidade = fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeComprar'));

        var nTipoProdutoID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoProdutoID'));

        if (nEmpresaData[1] == 130) {
            sGuia = fg.TextMatrix(i, getColIndexByColKey(fg, 'Guia'));
            nICMSST = fg.ValueMatrix(i, getColIndexByColKey(fg, 'ICMSST_Unitario'));
            sCFOPID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'CFOP*'));
        }
        else {
            sGuia = '';
            nICMSST = (nICMSST == null ? 0 : nICMSST);
            sCFOPID = '';
        }

        var nPrecoInterno = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Preco'));
        var nPrecoRevenda = nPrecoInterno;
        var nPrecoUnitario = nPrecoInterno;

        if (i == 1) {
            strParam += 'List=';
        }
        strParam += (sLineNumber == '' ? 'NULL' : encodeURIComponent(sLineNumber)) + '�';
        strParam += (nEmpresaID == '' ? '0' : encodeURIComponent(nEmpresaID)) + '�';
        strParam += (nPessoaID == '' ? '0' : encodeURIComponent(nPessoaID)) + '�';
        strParam += (nParceiroID == '' ? '0' : encodeURIComponent(nParceiroID)) + '�';
        strParam += (nProdutoID == '' ? '0' : encodeURIComponent(nProdutoID)) + '�';
        strParam += (nCorpProdutoID == '' ? '0' : encodeURIComponent(nCorpProdutoID)) + '�';
        strParam += (sPartNumber == '' ? 'NULL' : encodeURIComponent(sPartNumber)) + '�';
        strParam += (sDescricaoProduto == '' ? 'NULL' : encodeURIComponent(sDescricaoProduto)) + '�';
        strParam += (nVigencia == '' ? '0' : encodeURIComponent(nVigencia)) + '�';
        strParam += (nDesconto == '' ? '0' : encodeURIComponent(nDesconto)) + '�';
        strParam += (nQuantidade == '' ? '0' : encodeURIComponent(nQuantidade)) + '�';
        strParam += (nMoedaID == '' ? '0' : encodeURIComponent(nMoedaID)) + '�';
        strParam += (sGuia == '' ? 'NULL' : encodeURIComponent(sGuia)) + '�';
        strParam += (nICMSST == '' ? '0' : encodeURIComponent(nICMSST)) + '�';
        strParam += (nPrecoInterno == '' ? '0' : encodeURIComponent(nPrecoInterno)) + '�';
        strParam += (nPrecoRevenda == '' ? '0' : encodeURIComponent(nPrecoRevenda)) + '�';
        strParam += (nPrecoUnitario == '' ? '0' : encodeURIComponent(nPrecoUnitario)) + '�';
        strParam += (sFinalidade == '' ? 'NULL' : encodeURIComponent(sFinalidade)) + '�';
        strParam += (sCFOPID == '' ? '0' : encodeURIComponent(sCFOPID)) + '�';
        strParam += (nFinanciamentoID == '' ? '0' : encodeURIComponent(nFinanciamentoID)) + '�';
        strParam += (nFrete == '' ? '0' : encodeURIComponent(nFrete)) + '�';
        strParam += (sMeioTransporteID == '' ? '0' : encodeURIComponent(sMeioTransporteID)) + '�';
        strParam += (glb_USERID == '' ? '0' : encodeURIComponent(glb_USERID)) + '�';
        strParam += (glb_USERID == '' ? '0' : encodeURIComponent(glb_USERID)) + '�';
        strParam += (nLoteID == '' ? '0' : encodeURIComponent(nLoteID)) + '�';
        strParam += (bWeb == '' ? '0' : encodeURIComponent(bWeb)) + '�';
        strParam += (nTipoProdutoID == '' ? '0' : encodeURIComponent(nTipoProdutoID));
        strParam += (sSacolaObservacoes.length == 0 ? '' : encodeURIComponent(sSacolaObservacoes));
        strParam += (i == (fg.Rows - 1)) ? "" : "�";
    }

    dsoPurchaseItem.URL = SYS_ASPURLROOT + '/serversidegenEx/purchaseitemtrolley.aspx';
    dsoPurchaseItem.parameters = strParam;
    dsoPurchaseItem.ondatasetcomplete = purchaseItem_DSC;
    dsoPurchaseItem.refresh();
    
}

function desmarcaOK() {
    if (glb_bPaste) {
        fg.Rows = 1;
    }
    else {
        for (var i = 1; i < fg.Rows; i++) {
            fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = 0;
        }
    }
}

/********************************************************************
Funcao criada pelo programador
Vai ao servidor obter a qntd e o id do produto.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function BalanceCampaign()
{
    lockInterface(false);

    var strPars = '';

    strPars += '?nProdutoID=' + escape(glb_nProdutoID);
    strPars += '&nQuantidade=' + escape(glb_nQuantidade);

    dsoBalanceCampaign.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/balancecampaign.aspx' + strPars;
    dsoBalanceCampaign.ondatasetcomplete = BalanceCampaign_DSC;
    dsoBalanceCampaign.refresh();
}

/********************************************************************
Funcao criada pelo programador
Retorno do servidor da funcao BalanceCampaign()

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function BalanceCampaign_DSC() {
    var saldocampanha = dsoBalanceCampaign.recordset['saldocampanha'].value;

    //destrava interface
    lockInterface(false);

    // Verifica se h�
    if (!(saldocampanha == null))
        if (!(saldocampanha >= glb_nQuantidade))
            window.top.overflyGen.Alert('Haver� altera��o na Margem.' + "\n" + 'Quantidade maior que saldo campanha. Saldo m�ximo: ' + saldocampanha + "\n" + 'A compra esta no carrinho.');

    glb_nProdutoID = null;
    glb_nQuantidade = null;
}