/********************************************************************
listaprecossup01.js

Library javascript para o listaprecossup01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_financeiro = 0;
var glb_aDadosPessoa = null;
var glb_CFOPID = 0;
// controla se a modal vem do servidor ou esta carregada escondida
var glb_bWantPurchase = false;

// variavel de timer
var glb_TimerListaPrecoVar = null;
var glb_TimerListaPrecoVar2 = null;
var glb_bRetificaImposto = true;
var glb_bArg = null;

var glb_btnBarNotEspecClicked;
glb_btnBarNotEspecClicked = '';
var glb_currMode = 'DET';

var glb_nA1 = 0;
var glb_nA2 = 0;
/*Carlos solicitou para voltar a mostrar MC. DCS 21/01/2009 16:10*/
var glb_bDireitoVerMargem = true;

// Dados do registro corrente .SQL
var dsoSup01 = new CDatatransport("dsoSup01");
// Descricao do estado atual do registro .SQL
var dsoStateMachine = new CDatatransport("dsoStateMachine");
// Dados dos combos estaticos .URL
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// Dados   .URL
var dsoPrecoImposto = new CDatatransport("dsoPrecoImposto");
var dsoListaPreco = new CDatatransport("dsoListaPreco");
var dsoPrecoLista = new CDatatransport("dsoPrecoLista");
var dsoPrecoConvertido = new CDatatransport("dsoPrecoConvertido");
var dsoPessoa = new CDatatransport("dsoPessoa");

// Busca id da relacao no servidor .URL
var dsoCurrRelation = new CDatatransport("dsoCurrRelation");
var dsoCFOP = new CDatatransport("dsoCFOP");

var dsoFinalidade = new CDatatransport('dsoFinalidade');

//BJBN - Verifica se produto esta no emailMarketing atual
var dsoEmailMarketing = new CDatatransport('dsoEmailMarketing');

//TSA - Busca ID do proprietario e do alternativo do ProdutoID
var dsoPropAltConceito = new CDatatransport('dsoPropAltConceito');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prginterfacesup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selUFID', '1'],
                          ['selMoedaConversaoID', '2'],
                          ['selFinanciamentoID', '3']]);/*,
                          ['selFrete', '4']]);*/

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modcomercial/subatendimento/listaprecos/inferior.asp',
                              SYS_PAGESURLROOT + '/modcomercial/subatendimento/listaprecos/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'ConceitoID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

function setupPage() 
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup', [[1, 'divSup01_01'], [2, 'divSup02_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 7, 1],
                          ['lblEstadoID', 'txtEstadoID', 2, 1],
                          ['lblConceito', 'txtConceito', 23, 1],
                          ['lblPartNumber', 'txtPartNumber', 18, 1],
                          ['lblEmpresa', 'txtEmpresa', 11, 1],
                          ['lblLote', 'txtLote', 7, 1],
                          ['lblUnidade', 'txtUnidade', 4, 1],
                          ['lblPesoBruto', 'txtPesoBruto', 7, 1],
                          ['lblPrazoTroca', 'txtPrazoTroca', 3, 1],
                          ['lblPrazoGarantia', 'txtPrazoGarantia', 3, 1],
                          ['lblPrazoAsstec', 'txtPrazoAsstec', 3, 1, -7],
                          ['lblDescricaoProduto', 'txtDescricaoProduto', 96, 2]
                          ], null, null, true);

/*
['lblConceitoConcreto', 'txtConceitoConcreto', 20, 2],
                          ['lblMarca', 'txtMarca', 20, 2],
                          ['lblModelo', 'txtModelo', 18, 2],
                          ['lblDescricao', 'txtDescricao', 34, 2]
*/

    adjust_adjustElementsInForm();
        
}
// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    var i;
    var empresa = getCurrEmpresaData();
    var nAtendimentoState = 0;
    
    if (param1 == 'SETPESSOAPRICELIST')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
        
        // param2 - traz array:
        // o id da empresa do form que mandou
        // o id da pessoa (parceiro)
        if ( param2[0] != empresa[0])
            return null;
            
        if ((window.top.__glb_ListOrDet != '') && (window.top.__glb_ListOrDet != 'PL'))
            return null;

        nAtendimentoState = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL , 'btnFindCliente.state;');

        if (nAtendimentoState == 1)
            return null;
            
        if ( glb_aDadosPessoa == null )
            glb_aDadosPessoa = new Array();
        
        glb_aDadosPessoa[0] = param2[1][0];
        glb_aDadosPessoa[1] = param2[1][1];
        glb_aDadosPessoa[2] = param2[1][2];
        glb_aDadosPessoa[3] = param2[1][3];
        glb_aDadosPessoa[4] = param2[1][4];
        glb_aDadosPessoa[5] = param2[1][5];
        glb_aDadosPessoa[6] = param2[1][6];
        glb_aDadosPessoa[7] = param2[1][7];
        //glb_aDadosPessoa[8] = param2[1][8];
        //glb_aDadosPessoa[9] = param2[1][9];        
		
	    // Manda o ID do atendimento para o pesqlist
	    sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL , 'glb_nPesAtendimentoID = ' + param2[2] + ';');
	    sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL , 'glb_dtAtendimentoInicio = ' + param2[3] + ';');
	    sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL , "glb_nTipoAtendimentoID = '" + param2[4] + "';");
	    
		var strExec = 'setDadosParceirosFromSup(' + '\'' + param2[1][0] + '\'' + ', ' +
			param2[1][1] + ', ' +
			param2[1][2] + ', ' +
			param2[1][3] + ', ' +
			param2[1][4] + ', ' +
			param2[1][5] + ', ' +
			param2[1][6] + ', ' +
			param2[1][7] + ', \'' +
			param2[1][8] + '\', \'' +
			param2[1][9] + '\');';
			
		sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL , strExec);

        return new Array(getHtmlId(), 1, 1);
    }

    if (param1 == 'SHOWLISTAPRECOS')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        /*
        if (__currFormState() != 0)
            return null;
        */
            
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        //if ( param2[0] != empresa[0])
        //    return null;

        window.top.focus();

        // Exclusivo para exibir detalhe em form
        //showDetailByCarrier(param2[1]);

        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selecionarSacola(' + param2[1] + ')');
        
        return new Array(getHtmlId(), null, null);          
    }

    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

function adjust_adjustElementsInForm()
{
    if (selChavePessoaID.value == 1) 
    {
        //@@ *** Complemento - Div secundario superior divSup02_01 ***/
        adjustElementsInForm([['lblParceiroID', 'selParceiroID', 20, 1, 0, -3],
						  ['lblPessoaID', 'selPessoaID', 20, 1, -2],
                          //['lblFrete', 'selFrete', 8, 1, -5],
                          ['lblPrevisaoEntrega', 'txtPrevisaoEntrega', 15, 1, -5, 0],
                          ['lblFinanciamentoID', 'selFinanciamentoID', 15, 1, -5, 0],
                          ['lblQuantidade', 'txtQuantidade', 6, 1],
                          ['lblMoedaConversaoID', 'selMoedaConversaoID', 7, 1, 0, 0],
                          ['lblTransacao', 'selTransacao', 15, 2],
                          ['lblFinalidade', 'selFinalidade', 18, 2, -5],
                          ['lblCFOPID', 'txtCFOPID', 7, 2, -5],
                          ['lblMoedaTabelaID', 'txtMoedaTabelaID', 4, 2, -5],
                          ['lblPrecoTabela', 'txtPrecoTabela', 11, 2, -5],
                          ['lblPrecoLista', 'txtPrecoLista', 11, 2, -5],
                          ['lblMargemLista', 'txtMargemLista', 6, 2, -5],
                          ['lblPrecoWeb', 'txtPrecoWeb', 11, 2, -5],
                          ['lblImposto', 'txtImposto', 6, 2, -5],
                          ['btnImpostos', 'btn', 21, 2],
                          ['lblOrigem', 'txtOrigem', 6, 2, -5],
                          ['lblVariacao', 'txtVariacao', 6, 3],
                          ['lblPrecoConvertido', 'txtPrecoConvertido', 10, 3],
                          ['lblPrecoRevenda', 'txtPrecoRevenda', 10, 3, -12],
                          ['lblPrecoFaturamento', 'txtPrecoFaturamento', 10, 3],
                          ['lblComissaoInterna', 'txtValorComissaoInterna', 8, 3],
                          ['lblValorComissaoRevenda', 'txtValorComissaoRevenda', 8, 3],
                          ['lblPercentualSUP', 'txtPercentualSUP', 6, 3, -4],
                          ['lblContribuicao', 'txtContribuicao', 10, 3],
                          ['lblMargemContribuicao', 'txtMargemContribuicao', 8, 3, -4],
                          ['lblEstoqueDisponivel', 'txtEstoqueDisponivel', 7, 3],
                          ['lblEstoqueDisponivel1', 'txtEstoqueDisponivel1', 6, 3],
                          ['lblEstoqueDisponivel2', 'txtEstoqueDisponivel2', 6, 3],
                          ['lblNCM', 'txtNCM', 10, 3]], null, null, true);
                          
                            lblUFID.style.visibility = 'hidden';
                            selUFID.style.visibility = 'hidden';
    }
    else 
    {
        adjustElementsInForm([['lblParceiroID', 'selParceiroID', 20, 1, 0, -3],
						  ['lblPessoaID', 'selPessoaID', 20, 1, -2],
						  ['lblUFID', 'selUFID', 6, 1, -5],
                          //['lblFrete', 'selFrete', 8, 1, -5],
                          ['lblPrevisaoEntrega', 'txtPrevisaoEntrega', 15, 1, -5, 0],
                          ['lblFinanciamentoID', 'selFinanciamentoID', 15, 1, -5, 0],
                          ['lblQuantidade', 'txtQuantidade', 6, 1],
                          ['lblMoedaConversaoID', 'selMoedaConversaoID', 7, 1, 0, 0],
                          ['lblTransacao', 'selTransacao', 15, 2],
                          ['lblFinalidade', 'selFinalidade', 18, 2, -5],
                          ['lblCFOPID', 'txtCFOPID', 7, 2, -5],
                          ['lblMoedaTabelaID', 'txtMoedaTabelaID', 4, 2, -5],
                          ['lblPrecoTabela', 'txtPrecoTabela', 11, 2, -5],
                          ['lblPrecoLista', 'txtPrecoLista', 11, 2, -5],
                          ['lblMargemLista', 'txtMargemLista', 6, 2, -5],
                          ['lblPrecoWeb', 'txtPrecoWeb', 11, 2, -5],
                          ['lblImposto', 'txtImposto', 6, 2, -5],
                          ['btnImpostos', 'btn', 21, 2],
                          ['lblOrigem', 'txtOrigem', 6, 2, -5],
                          ['lblVariacao', 'txtVariacao', 6, 3],
                          ['lblPrecoConvertido', 'txtPrecoConvertido', 10, 3],
                          ['lblPrecoRevenda', 'txtPrecoRevenda', 10, 3, -12],
                          ['lblPrecoFaturamento', 'txtPrecoFaturamento', 10, 3],
                          ['lblComissaoInterna', 'txtValorComissaoInterna', 8, 3],
                          ['lblValorComissaoRevenda', 'txtValorComissaoRevenda', 8, 3],
                          ['lblPercentualSUP', 'txtPercentualSUP', 6, 3, -4],
                          ['lblContribuicao', 'txtContribuicao', 10, 3],
                          ['lblMargemContribuicao', 'txtMargemContribuicao', 8, 3, -4],
                          ['lblEstoqueDisponivel', 'txtEstoqueDisponivel', 7, 3],
                          ['lblEstoqueDisponivel1', 'txtEstoqueDisponivel1', 6, 3],
                          ['lblEstoqueDisponivel2', 'txtEstoqueDisponivel2', 6, 3],
                          ['lblNCM', 'txtNCM', 10, 3]], null, null, true);

                           lblUFID.style.visibility = 'inherit';
                           selUFID.style.visibility = 'inherit';
    }
        lblPrecoMinimo.style.top = lblMargemContribuicao.style.top;
        lblPrecoMinimo.style.left = lblMargemContribuicao.style.left;
        lblPrecoMinimo.style.width = 65;

        txtPrecoMinimo.style.top = txtMargemContribuicao.style.top;
        txtPrecoMinimo.style.left = txtMargemContribuicao.style.left;
        txtPrecoMinimo.style.width = txtMargemContribuicao.style.width;

        divSup01_01.style.height = parseInt(txtDescricaoProduto.currentStyle.top, 10) +
            parseInt(txtDescricaoProduto.currentStyle.height, 10) + 2;
        //divSup01_01.style.backgroundColor = 'yellow';    

        divSup02_01.style.top = parseInt(divSup01_01.currentStyle.top, 10) +
            parseInt(divSup01_01.currentStyle.height, 10) + 1;
        //divSup02_01.style.backgroundColor = 'red';        

        lblPrazoMedioPagamento.style.visibility = 'hidden';
        txtPrazoMedioPagamento.style.visibility = 'hidden';

        lblFaturamentoDireto.style.visibility = 'hidden';
        chkFaturamentoDireto.style.visibility = 'hidden';

        lblCon.style.visibility = 'hidden';
        chkContribuinte.style.visibility = 'hidden';

        lblClasseID.style.visibility = 'hidden';
        selClassificacoesClaID.style.visibility = 'hidden';

        lblTipoPessoa.style.visibility = 'hidden';
        selTipoPessoa.style.visibility = 'hidden';

        lblCidadeId.style.visibility = 'hidden';
        selCidadeId.style.visibility = 'hidden';

        lblClassificacoesID.style.visibility = 'hidden';
        selClassificacoesID.style.visibility = 'hidden';

        lblSimplesNacionalID.style.visibility = 'hidden';
        chkSimplesNacional.style.visibility = 'hidden';

        lblSuframaID.style.visibility = 'hidden';
        chkSuframaID.style.visibility = 'hidden';

        lblIsencoesID.style.visibility = 'hidden';
        selIsencoesID.style.visibility = 'hidden';

        lblChavePessoaID.style.visibility = 'hidden';
        selChavePessoaID.style.visibility = 'hidden';
        
        setLabelOfControlEx(lblFinanciamentoID, selFinanciamentoID);
        //Campo Lote ReadOnly
        txtLote.readOnly = true;
        
        txtPrecoWeb.readOnly = true;
        txtOrigem.readOnly = true;
        selTransacao.disabled = (selTransacao.options.length == 0);
        selFinalidade.disabled = (selFinalidade.options.length == 0);

        txtCnae.style.visibility = 'hidden';
}    

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    var i = 0;
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.

    txtImposto.setAttribute('prog_SHHD', true, 1);
    txtPrecoTabela.setAttribute('prog_SHHD', true, 1);
    txtQuantidade.setAttribute('prog_SHHD', true, 1);
    txtPrecoLista.setAttribute('prog_SHHD', true, 1);
    txtMargemLista.setAttribute('prog_SHHD', true, 1);
    selParceiroID.setAttribute('prog_SHHD', true, 1);
    txtVariacao.setAttribute('prog_SHHD', true, 1);
    txtPrecoConvertido.setAttribute('prog_SHHD', true, 1);
    txtPrecoRevenda.setAttribute('prog_SHHD', true, 1);    
    txtPrecoFaturamento.setAttribute('prog_SHHD', true, 1);
    txtValorComissaoRevenda.setAttribute('prog_SHHD', true, 1);
    txtPercentualSUP.setAttribute('prog_SHHD', true, 1);

    // trava as barras do sup e do inf para form read-only
    glb_BtnsIncAltEstExcl[0] = true;
    glb_BtnsIncAltEstExcl[1] = true;
    glb_BtnsIncAltEstExcl[2] = true;
    glb_BtnsIncAltEstExcl[3] = true;
    
    // labels coloridos
    lblPessoaID.style.color = 'blue';
    lblImposto.style.color = 'green';
    lblParceiroID.style.color = 'blue';
    lblQuantidade.style.color = 'blue';
    lblFinanciamentoID.style.color = 'green';
    lblVariacao.style.color = 'green';
    lblMoedaConversaoID.style.color = 'blue';
    lblPrecoConvertido.style.color = 'green';
    lblPrecoRevenda.style.color = 'green';
    lblPrecoFaturamento.style.color = 'green';
    lblMargemContribuicao.style.color = 'green';
    lblPercentualSUP.style.color = 'green';
    
    // eventos onChange para calculos
    txtImposto.onchange = calcula;
    txtImposto.onkeyup = focusNextFld;
    txtQuantidade.onchange = calcula;
    txtQuantidade.onkeyup = focusNextFld_ThisForm;
    txtPrazoMedioPagamento.onchange = calcula;
    txtPrazoMedioPagamento.onkeyup = focusNextFld_ThisForm;
    txtVariacao.onchange = calcula;
    txtVariacao.onkeyup = focusNextFld_ThisForm;
    txtPrecoConvertido.onchange = precoConvertido;
    txtPrecoConvertido.onkeyup = focusNextFld_ThisForm;

    txtPrecoRevenda.onkeyup = focusNextFld_ThisForm;
    txtPrecoRevenda.onchange = precoRevenda;
    txtPrecoFaturamento.onchange = precoFaturamento;
    txtPrecoFaturamento.onkeyup = focusNextFld_ThisForm;
    txtMargemContribuicao.onchange = margemContribuicao;
    txtMargemContribuicao.onkeyup = focusNextFld_ThisForm;
    txtPercentualSUP.onchange = PercentualSUP;
    txtPercentualSUP.onkeyup = focusNextFld_ThisForm;
    
    // campos numericos deslinkados e com max e minimos
    txtQuantidade.setAttribute('thePrecision', 5, 1);
    txtQuantidade.setAttribute('theScale', 0, 1);
    txtQuantidade.setAttribute('verifyNumPaste', 1);
    txtQuantidade.onkeypress = verifyNumericEnterNotLinked;
    txtQuantidade.setAttribute('minMax', new Array(0, 99999), 1);
    
    txtImposto.setAttribute('thePrecision', 5, 1);
    txtImposto.setAttribute('theScale', 2, 1);
    txtImposto.onkeypress = verifyNumericEnterNotLinked;
    txtImposto.setAttribute('verifyNumPaste', 1);
    txtImposto.setAttribute('minMax', new Array(0, 100), 1);
    
    txtPrazoMedioPagamento.setAttribute('thePrecision', 5, 1);
    txtPrazoMedioPagamento.setAttribute('theScale', 0, 1);
    txtPrazoMedioPagamento.setAttribute('verifyNumPaste', 1);
    txtPrazoMedioPagamento.onkeypress = verifyNumericEnterNotLinked;
    txtPrazoMedioPagamento.setAttribute('minMax', new Array(0, 999), 1);
    
    txtVariacao.setAttribute('thePrecision', 5, 1);
    txtVariacao.setAttribute('theScale', 2, 1);
    txtVariacao.onkeypress = verifyNumericEnterNotLinked;
    txtVariacao.setAttribute('minMax', new Array(-99.99, 99.99), 1);

    txtPrecoConvertido.setAttribute('thePrecision', 11, 1);
    txtPrecoConvertido.setAttribute('theScale', 2, 1);
    txtPrecoConvertido.setAttribute('verifyNumPaste', 1);
    txtPrecoConvertido.onkeypress = verifyNumericEnterNotLinked;
    txtPrecoConvertido.setAttribute('minMax', new Array(0, 99999999.99), 1);

    txtPrecoRevenda.setAttribute('thePrecision', 11, 1);
    txtPrecoRevenda.setAttribute('theScale', 2, 1);
    txtPrecoRevenda.setAttribute('verifyNumPaste', 1);
    txtPrecoRevenda.onkeypress = verifyNumericEnterNotLinked;
    txtPrecoRevenda.setAttribute('minMax', new Array(0, 99999999.99), 1);

    txtPrecoFaturamento.setAttribute('thePrecision', 11, 1);
    txtPrecoFaturamento.setAttribute('theScale', 2, 1);
    txtPrecoFaturamento.setAttribute('verifyNumPaste', 1);
    txtPrecoFaturamento.onkeypress = verifyNumericEnterNotLinked;
    txtPrecoFaturamento.setAttribute('minMax', new Array(0, 99999999.99), 1);

    txtMargemContribuicao.setAttribute('thePrecision', 5, 1);
    txtMargemContribuicao.setAttribute('theScale', 2, 1);
    txtMargemContribuicao.setAttribute('verifyNumPaste', 1);
    txtMargemContribuicao.onkeypress = verifyNumericEnterNotLinked;
    txtMargemContribuicao.setAttribute('minMax', new Array(-30.00, 30.00), 1);
    
    txtPercentualSUP.setAttribute('thePrecision', 5, 1);
    txtPercentualSUP.setAttribute('theScale', 2, 1);
    txtPercentualSUP.setAttribute('verifyNumPaste', 1);
    txtPercentualSUP.onkeypress = verifyNumericEnterNotLinked;
    txtPercentualSUP.setAttribute('minMax', new Array(0, 100.00), 1);
}

/********************************************************************
Muda foco de campo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function focusNextFld_ThisForm()
{
    if ( event.keyCode == 13 )
    {
        if ( this == txtPrecoConvertido )
            precoConvertido();
        else if (this == txtPrecoRevenda)
            precoRevenda();            
        else if ( this == txtPrecoFaturamento )
            precoFaturamento();
        else if ( this == txtMargemContribuicao )
            margemContribuicao();
		else if ( this == txtPercentualSUP )
		    PercentualSUP();
		else if (this == txtQuantidade)
		    txtPrecoConvertido.focus();
		else if (this == txtPrazoMedioPagamento)
		    txtPrecoConvertido.focus();
		else if (this == txtVariacao)
		    txtPrecoConvertido.focus(); 		    		    
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    //@@
    
    /*Carlos solicitou para voltar a mostrar MC. DCS 21/01/2009 16:10*/
    // tira do vendedor o acesso a MC na lista de pre�o
    //glb_bDireitoVerMargem = ((glb_nA1 == 1) || (glb_nA2 == 1));
    glb_bDireitoVerMargem = true;

	// Mover esta funcao para os finais de retornos de operacoes no
	// banco de dados
	finalOfSupCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    
    var nImpostosIncidencia;
    var nPrecoTabela;

    glb_nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));
    glb_nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));

    //lockBtnLupa(btnFindCliente, false);
    lockBtnLupa(btnImpostos, false);
    
    if (!glb_bDireitoVerMargem)
    {
        lblMargemLista.style.visibility = 'hidden';
        txtMargemLista.style.visibility = 'hidden';
    
	    lblContribuicao.style.visibility = 'hidden';
	    txtContribuicao.style.visibility = 'hidden';

	    lblMargemContribuicao.style.visibility = 'hidden';
	    txtMargemContribuicao.style.visibility = 'hidden';
    }
    else
    {
	    lblPrecoMinimo.style.visibility = 'hidden';
	    txtPrecoMinimo.style.visibility = 'hidden';

	    if (glb_nA1 == 0 || glb_nA1 == 0)
	    {
	        lblContribuicao.style.visibility = 'hidden';
	        txtContribuicao.style.visibility = 'hidden';

	        lblMargemContribuicao.style.visibility = 'hidden';
	        txtMargemContribuicao.style.visibility = 'hidden';

	        lblMargemLista.style.visibility = 'hidden';
	        txtMargemLista.style.visibility = 'hidden';
	    }
    }

    if ( selPessoaID.options.length != 0 )
        selPessoaID.disabled = false;
    else
        selPessoaID.disabled = true;
    
    enableDisableFieldsByParceiro();

    var nTipoCotacao = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selChavePessoaID.value;');
    
    // Se cotacao eh com pessoa
    if (nTipoCotacao == 1) 
        selPessoaID_OnChange(false);
        
    selFinanciamentoID.disabled = false;

    // Se cotacao eh com pessoa
    if (nTipoCotacao == 1)
        selUFID_OnChange(false);

    adjust_adjustElementsInForm();

    txtQuantidade.disabled = false;
    txtQuantidade.readOnly = false;
    txtVariacao.disabled = false;
    txtVariacao.readOnly = false;
    selMoedaConversaoID.disabled = false;
    txtPrecoConvertido.disabled = false;
    txtPrecoConvertido.readOnly = false;
    txtPrecoRevenda.disabled = false;
    txtPrecoRevenda.readOnly = false;
    txtPrecoFaturamento.disabled = false;
    txtPrecoFaturamento.readOnly = false;
    txtMargemContribuicao.disabled = false;
    txtMargemContribuicao.readOnly = false;
    txtPercentualSUP.disabled = false;
    txtPercentualSUP.readOnly = false;

    txtImposto.value = padNumReturningStr(roundNumber(dsoSup01.recordset['Imposto'].value,2),2);
    nImpostosIncidencia = dsoSup01.recordset['ImpostosIncidencia'].value;
   // chkImpostosIncidencia.checked = (nImpostosIncidencia == 1);

    nPrecoTabela = dsoSup01.recordset['PrecoTabela'].value;
    txtPrecoTabela.value = padNumReturningStr(roundNumber(nPrecoTabela, 2),2);
    glb_CFOPID = dsoSup01.recordset['CFOPID'].value;
    txtCFOPID.value = (glb_CFOPID == null ? '' : glb_CFOPID);    
    txtCFOPID.title = dsoSup01.recordset['CFOP'].value;
    

    txtQuantidade.value = dsoSup01.recordset['Quantidade'].value;

    txtPrecoLista.value = padNumReturningStr(roundNumber(dsoSup01.recordset['PrecoLista'].value,2),2);
    txtMargemLista.value = padNumReturningStr(roundNumber(dsoSup01.recordset['MargemLista'].value,2),2);
    
    if ( isNaN(parseInt(txtPrazoMedioPagamento.value,10)) )
        txtPrazoMedioPagamento.value = 0;

    if ( isNaN(parseInt(txtVariacao.value,10)) )
        txtVariacao.value = '0.00';
        
    txtNCM.title = dsoSup01.recordset['NCM_Observacoes'].value;        

    // Particular para este form mantem o div secundario sempre
    // visivel
    adjustComplementOfMainDiv();
    
    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================

    precoConvertido(true);
    
    //BJBN - Pinta txt do pre�o WEB caso produto esteja no EmailMarketing Atual
    precoEmailMKT();

    //TSA - Busca Proprietario e Alternativo do ProdutoID
    PropAltConceito();
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    
    if ( cmbID == 'selUFID' )
        selUFID_OnChange(true);
    /*else if ( cmbID == 'selFrete' )
        __btn_REFR('sup');*/
    else if ( cmbID == 'selMoedaConversaoID' )
    {
        prazo();
        __btn_REFR('sup');
    }
    else if ( cmbID == 'selPessoaID' )
        selPessoaID_OnChange(true);
    else if ( cmbID == 'selParceiroID' )
        selParceiroID_OnChange(true);
    else if ( cmbID == 'selFinanciamentoID' )
        selFinanciamentoID_OnChange(true);
    else if ( cmbID == 'selTransacao' )
    {
        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL , 'fg.Rows = 1;');       
        fillComboFinalidade();
        selFinanciamentoID_OnChange(true);
    }
    else if ( cmbID == 'selFinalidade' )
    {
        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL , 'fg.Rows = 1;');       
        selFinanciamentoID_OnChange(true);        
    }

    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    //@@ padrao do frame work
    return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
    if (btnClicked.id == 'btnFindCliente')
    {
        openModalCliente();
    }   
    else if (btnClicked.id == 'btnImpostos')
    {
		openModalImpostos();
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    if ( controlBar == 'SUP' )
    {
        if ((controlBar == 'SUP') && (btnClicked == 1)) {
            __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
        }

        // usuario clicou botao relatorios
        else if ( btnClicked == 2 )
            openModalPrint();                
        else if ( btnClicked == 4 )
        {
            // Busca o ID da RelacaoPesCon para detalhar
            openRelationByCarrier();
        }
        else if ( btnClicked == 6 )
            detalhaNCMSuperior();

        else if (btnClicked == 8) 
        {
            var nProdutoID = txtRegistroID.value;
            //var sProduto = txtConceitoConcreto.value;
            //var sMarca = txtMarca.value;
            //var sModelo = txtModelo.value;
            
            var sLinha = dsoSup01.recordset['LinhaProduto'].value;
            sLinha = (sLinha != null ? (sLinha != '' ? ' ' + sLinha + ' ' : '') : '');
            
            //var sDescricao = txtDescricao.value;
            var nEmpresaID = dsoSup01.recordset['EmpresaID'].value;
            nEmpresaID = (nEmpresaID != null ? nEmpresaID : '');
            
            var sEmpresa = txtEmpresa.value;            
            var nQuantidade = txtQuantidade.value; ;
            var sMoeda = selMoedaConversaoID.options(selMoedaConversaoID.selectedIndex).innerText;
            var nMoedaID = selMoedaConversaoID.value;
            var nPrecoUnitario = txtPrecoRevenda.value;
            var sAliquota = txtImposto.value;
            var sPMP = selFinanciamentoID.options(selFinanciamentoID.selectedIndex).innerText;
            var nPMP = selFinanciamentoID.options(selFinanciamentoID.selectedIndex).getAttribute('PMP', 1);

            var strExec = 'produtoDescricao()';
            var sProduto = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL , strExec);
            
            var strExec = 'copyDatatoClipBoard(' + nProdutoID + ',\'' + sProduto + '\',' + nEmpresaID + ',\'' + sEmpresa + '\',\'' + nQuantidade + '\',' +
                '\'' + sMoeda + '\',' + nMoedaID + ', ' + nPrecoUnitario + ', ' + sAliquota + ',\'' + sPMP + '\', ' + nPMP + ')';
            
            sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL , strExec);
        }
        else if (btnClicked == 9)
        {
            executePurchaseInModal();
        }
        else if (btnClicked == 10) {
            sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL , 'openModalTrolley(' + '\'' +'S' + '\'' + ')');
        }
    }
}

/********************************************************************
********************************************************************/
function detalhaNCMSuperior()
{
	NCM = parseInt(txtNCM.value);
	NCM = parseInt(NCM/10000);
	
	strPars = '?sNCM=' + NCM;
	htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/serverside/imagemncm.asp' + strPars;
	window.open(htmlPath);
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
	if (btnClicked == 'SUPLIST')
	{
		sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL , 'glb_callFromDet = true');
		glb_btnBarNotEspecClicked = 'SUPLIST';
	}
	else
		glb_btnBarNotEspecClicked = '';	
		
		
	
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCLIENTEHTML')
    {
        if ( param1 == 'OK' )                
        {
            restoreInterfaceFromModal();

            // Preenche o combo de cliente
            glb_aDadosPessoa = param2;
            glb_TimerListaPrecoVar2 = window.setInterval('fillCmbParceiro()', 10, 'JavaScript');
            // esta funcao que fecha a janela modal e destrava a interface
            // restoreInterfaceFromModal() foi movida para final das funcoes
            // chamadas pela funcao fillComboPessoa;    
            // escreve na barra de status
            //writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            //lockBtnLupa(btnFindCliente, false);
            // escreve na barra de status
            //writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }   
    // Modal de impressao
    else if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }    
    }
    // Modal do carrinho de compras
    else if ( idElement.toUpperCase() == 'MODALTROLLEYHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta aqui apenas para compatibilidade
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de impostos
    else if ( idElement.toUpperCase() == 'MODALIMPOSTOSHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Produto', 'Comprar', 'Sacola de Compra', 'NCM', 'Copiar dados do produto']);
}

/********************************************************************
Funcao desenvolvida pelo programador.
Particular para este form mantem o div secundario sempre visivel

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function adjustComplementOfMainDiv()
{
    divSup02_01.style.top = parseInt(divSup01_01.currentStyle.top, 10) +
        parseInt(divSup01_01.currentStyle.height, 10) + 1;
    divSup02_01.style.visibility = 'visible';

}
/********************************************************************
Funcao criada pelo programador.
Abre janela modal de cliente 

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalCliente()
{
    var htmlPath;
    var strPars = new Array();
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];
    
    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(dsoSup01.recordset['EmpresaID'].value);
    strPars += '&sCaller=' + escape('S');
       
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/subatendimento/listaprecos/modalpages/modalcliente.asp'+strPars;
    showModalWin(htmlPath, new Array(671,284));
}

function refreshInterface()
{
	__btn_REFR('sup');
}

/********************************************************************
Funcao criada pelo programador.

Parametro:  
bPrecoAtualizaPreco - null se o campo Preco nao deve ser alterado
                      true se o campo Preco deve ser alterado
Retorno:
nenhum
********************************************************************/
function precoConvertido(bPrecoAtualizaPreco, sCampo)
{
    if ( glb_TimerListaPrecoVar != null )
    {
        window.clearInterval(glb_TimerListaPrecoVar);
        glb_TimerListaPrecoVar = null;
    }

    if ( glb_btnBarNotEspecClicked == 'SUPLIST' )
    {
		glb_btnBarNotEspecClicked = '';
		return null;
    }

    var nEmpresaData = getCurrEmpresaData();
    var sPessoa = 'NULL';
    var sParceiroID = 'NULL';
    var nProdutoID = dsoSup01.recordset['ConceitoID'].value;
    var nAliquotaImposto = parseFloat(txtImposto.value);
    var nMoedaID = parseFloat(selMoedaConversaoID.value);
    
    var nFinanciamentoID = parseFloat(selFinanciamentoID.value);
    var nQuantidade = txtQuantidade.value;
    var nListaPreco = 0;
    var nTransacao = (selTransacao.selectedIndex >= 0 ? selTransacao.value : 'NULL');
    var nFinalidade = (selFinalidade.selectedIndex >= 0 ? selFinalidade.value : 'NULL');

    nQuantidade = (isNaN(nQuantidade) ? 1 : nQuantidade);
    
    txtQuantidade.value = nQuantidade;

    var nPrecoListaConvertido;
    var nPrecoConvertido;
    var nPrecoReal;
    var nVariacao;

    if (selPessoaID.value > 0)
    {
		sPessoa = selPessoaID.value;
		sParceiroID = (selParceiroID.value > 0 ? selParceiroID.value : 'NULL');

        if (sParceiroID != 'NULL')
            nListaPreco = -sParceiroID;
    }

	nPrecoListaConvertido = dsoSup01.recordset['PrecoConvertido'].value;

    if (bPrecoAtualizaPreco == null)
    {
        nPrecoConvertido = parseFloat(txtPrecoConvertido.value);
        nPrecoConvertido = (isNaN(nPrecoConvertido) ? nPrecoListaConvertido : nPrecoConvertido);
        txtPrecoConvertido.value = nPrecoConvertido;
        
        nVariacao = (((nPrecoConvertido/((isNaN(nPrecoListaConvertido) || nPrecoListaConvertido == null)? nPrecoConvertido : nPrecoListaConvertido))-1)*100);
        txtVariacao.value = padNumReturningStr(roundNumber(nVariacao,2),2);

    }
    else 
    {    
        if (sCampo == 'txtVariacao') 
        {
            nPrecoConvertido = parseFloat(txtPrecoConvertido.value);
            nVariacao = parseFloat(txtVariacao.value);
            nVariacao = (isNaN(nVariacao) ? '0.00' : nVariacao);
            txtVariacao.value = padNumReturningStr(roundNumber(nVariacao,2),2);
            nPrecoConvertido = nPrecoConvertido * (1 + (nVariacao / 100));
            txtPrecoConvertido.value = padNumReturningStr(roundNumber(nPrecoConvertido, 2), 2);
        }
        else 
        {
            nPrecoConvertido = nPrecoListaConvertido;
            txtPrecoConvertido.value = padNumReturningStr(roundNumber(nPrecoConvertido, 2), 2);
        }
    }
    
    txtMargemContribuicao.style.fontWeight = 'normal';
    txtMargemContribuicao.style.backgroundColor = '#FFFFFF';
    txtMargemContribuicao.title = 'Margem de Contribui��o';

    txtPrecoFaturamento.style.backgroundColor = '#FFFFFF';

    if ((!isNaN(nAliquotaImposto)) && (!isNaN(nMoedaID)) && (!isNaN(nPrecoConvertido)))
    {
        if (glb_bDireitoVerMargem)
        { 
            lockInterface(true);
            var strPars = new String();
            strPars += '?nEmpresaID=' + escape(dsoSup01.recordset['EmpresaID'].value);
            strPars += '&sPessoa=' + sPessoa;
            strPars += '&sParceiroID=' + sParceiroID;
            strPars += '&nProdutoID=' + escape(nProdutoID);
	        strPars += '&nQuantidade=' + escape(nQuantidade);
	        strPars += '&nListaPreco=' + escape(nListaPreco);
	        strPars += '&nPreco=' + escape(nPrecoConvertido);
	        strPars += '&nPrecoRevenda=' + escape(nPrecoConvertido);
	        strPars += '&nPrecoConvertido=' + escape(nPrecoConvertido);
            strPars += '&nAliquotaImposto=' + escape(nAliquotaImposto);
            strPars += '&nMoedaID=' + escape(nMoedaID);
           // strPars += '&nImpostosIncidencia=' + escape(chkImpostosIncidencia.checked ? 1 : 0);
            strPars += '&nFinanciamentoID=' + escape(nFinanciamentoID);
            strPars += '&nFrete=0'; // + escape(selFrete.value);
            strPars += '&nCalc=' + escape(1);
            strPars += '&nTransacao=' + escape(nTransacao);
            strPars += '&nFinalidade=' + escape(nFinalidade);            

            // Sem Pessoa 
           if (selChavePessoaID.value == 2)
           {
                strPars += '&nselClassificacoesClaID=' + escape(selClassificacoesClaID.selectedIndex >= 0 ? selClassificacoesClaID.value : 0);
                strPars += '&nselTipoPessoa=' + escape(selTipoPessoa.value);
                strPars += '&nchkFaturamentoDireto=' + escape(chkFaturamentoDireto.checked ? 1 : 0);
                strPars += '&nchkContribuinte=' + escape(chkContribuinte.checked ? 1 : 0);
                strPars += '&nchkSimplesNacional=' + escape(chkSimplesNacional.checked ? 1 : 0);
                strPars += '&nchkSuframaID=' + escape(chkSuframaID.checked ? 1 : 0);
                strPars += '&nselCidadeId=' + escape(selCidadeId.selectedIndex >= 0 ? selCidadeId.value : 0);
                strPars += '&nselClassificacoesID=' + escape(selClassificacoesID.selectedIndex >= 0 ? selClassificacoesID.value : 0);
                strPars += '&ntxtCnae=' + escape(txtCnae.value);
                strPars += '&nselIsencoesID=' + escape(selIsencoesID.value);
                strPars += '&nPaisID=' + escape(nEmpresaData[1]);
                strPars += '&nUFID=' + escape(selUFID.selectedIndex >= 0 ? selUFID.value : 0);
            }
            else 
            {
                strPars += '&nselClassificacoesClaID=' + escape("NULL");
                strPars += '&nselTipoPessoa=' + escape("NULL");
                strPars += '&nchkFaturamentoDireto=' + escape("NULL");
                strPars += '&nchkContribuinte=' + escape("NULL");
                strPars += '&nchkSimplesNacional=' + escape("NULL");
                strPars += '&nchkSuframaID=' + escape("NULL");
                strPars += '&nselCidadeId=' + escape("NULL");
                strPars += '&nselClassificacoesID=' + escape("NULL");
                strPars += '&ntxtCnae=' + escape("NULL");
                strPars += '&nselIsencoesID=' + escape("NULL");
                strPars += '&nPaisID=' + escape("NULL");
                strPars += '&nUFID=' + escape("NULL");
            }
            
            setConnection(dsoListaPreco);

            dsoListaPreco.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/preco.aspx'+strPars;
            dsoListaPreco.ondatasetcomplete = precoConvertido_DSC;
            dsoListaPreco.refresh();
        }
        else
        {
            precoConvertidoLocal_DSC();        
        }
    }
    else
    {
		if (dsoSup01.recordset['MargemMinima'].value != null)
		{
			txtMargemContribuicao.title = 'Margem de Contribui��o (Margem M�nima: ' + (dsoSup01.recordset['MargemMinima'].value).toString() + '%)';

			if (dsoSup01.recordset['MargemMinima'].value > 0)
			{
				txtMargemContribuicao.style.fontWeight = 'bold';
				txtMargemContribuicao.style.backgroundColor = '#FF7F50';
				txtMargemContribuicao.title = 'Margem de Contribui��o inferior a margem m�nima (Margem M�nima: ' + (dsoSup01.recordset['MargemMinima'].value).toString() + '%)';

				txtPrecoFaturamento.style.backgroundColor = '#FF7F50';
			}			
		}

        txtContribuicao.value = '0.00';
        txtMargemContribuicao.value = '0.00';
        window.focus();
        txtPrecoConvertido.focus();
    }
}
/********************************************************************
Funcao criada pelo programador.
Retorno da fun��o que Atualiza os resultados em fun��o do pre�o convertido

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function precoConvertido_DSC()
{
    //var nPrecoBase;
    //var nValorFinanceiro;
    var nContribuicao;
    var nMargem;

    if (! ((dsoListaPreco.recordset.BOF) && (dsoListaPreco.recordset.EOF)) )
    {
        nContribuicao = parseFloat(dsoListaPreco.recordset['Contribuicao'].value);
        nMargem = parseFloat(dsoListaPreco.recordset['MargemContribuicao'].value);
        txtContribuicao.value = padNumReturningStr(roundNumber(nContribuicao,2),2);
        txtMargemContribuicao.value = padNumReturningStr(roundNumber(nMargem,2),2);
        txtPrecoWeb.value = padNumReturningStr(roundNumber(parseFloat(dsoListaPreco.recordset['PrecoWeb'].value),2),2);
        glb_CFOPID = dsoListaPreco.recordset['CFOPID'].value;        
        txtCFOPID.value = (glb_CFOPID == null ? '' : glb_CFOPID);
        txtCFOPID.title = dsoListaPreco.recordset['CFOP'].value;
    }
    else
    {
        txtContribuicao.value = '0.00';
        txtMargemContribuicao.value = '0.00';
        nMargem = 0;
    }
    
    txtMargemContribuicao.style.fontWeight = 'normal';
    txtMargemContribuicao.style.backgroundColor = '#FFFFFF';
    txtMargemContribuicao.title = 'Margem de Contribui��o';

    txtPrecoFaturamento.style.backgroundColor = '#FFFFFF';

    if (dsoSup01.recordset['MargemMinima'].value != null)
    {
		txtMargemContribuicao.title = 'Margem de Contribui��o (Margem M�nima: ' + (dsoSup01.recordset['MargemMinima'].value).toString() + '%)';

		if (nMargem < dsoSup01.recordset['MargemMinima'].value)
		{
			txtMargemContribuicao.style.fontWeight = 'bold';
			txtMargemContribuicao.style.backgroundColor = '#FF7F50';
			txtMargemContribuicao.title = 'Margem de Contribui��o inferior a margem m�nima (Margem M�nima: ' + (dsoSup01.recordset['MargemMinima'].value).toString() + '%)';

			txtPrecoFaturamento.style.backgroundColor = '#FF7F50';
		}			
    }

	precoRevenda(true);
	
    lockInterface(false);

	try
	{
		window.focus();
		txtPrecoConvertido.focus();
    }
    catch(e)
    {
		;
    }
}

/********************************************************************
Funcao criada pelo programador.
Retorno da fun��o que Atualiza os resultados em fun��o do pre�o convertido

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function precoConvertidoLocal_DSC()
{
	var nPrecoConvertido = parseFloat(txtPrecoConvertido.value);
    var nPrecoMinimo = roundNumber(parseFloat(dsoSup01.recordset['PrecoMinimo'].value*(1+(glb_financeiro/100))),2);
	txtPrecoConvertido.style.fontWeight = 'normal';
	txtPrecoConvertido.style.backgroundColor = '#FFFFFF';

    if (nPrecoMinimo != null)
    {
        txtPrecoMinimo.value = nPrecoMinimo;
		if (nPrecoConvertido < nPrecoMinimo)
		{
			txtPrecoConvertido.style.fontWeight = 'bold';
			txtPrecoConvertido.style.backgroundColor = '#FF7F50';
		}			
    }

	precoRevenda(true);
	
	try
	{
		window.focus();
		txtPrecoConvertido.focus();
    }
    catch(e)
    {
		;
    }
}

/********************************************************************
Funcao criada pelo programador.

Parametro:  
bPrecoConvertido - null se o campo Preco nao deve ser alterado
                   true se veio do preco convertido
Retorno:
nenhum
********************************************************************/
function precoRevenda(bPrecoConvertido) 
{
    var nPrecoConvertido;
    var nPrecoRevenda;
    var nPrecoFaturamento;
    var nValorComissaoInterno;
    var nPercentualSUP;
    var nFatorTotalImpostos = dsoSup01.recordset['FatorTotalImpostos'].value;
    var strPars = new String();
    var nEmpresaData = getCurrEmpresaData();
    var sPessoa = 'NULL';
    var sParceiroID = 'NULL';
    var nProdutoID = dsoSup01.recordset['ConceitoID'].value;
    var nAliquotaImposto = parseFloat(txtImposto.value);
    var nMoedaID = parseFloat(selMoedaConversaoID.value);
    var nFinanciamentoID = parseFloat(selFinanciamentoID.value);
    var nQuantidade = txtQuantidade.value;
    var nListaPreco = 0;
    var nTransacao = (selTransacao.selectedIndex >= 0 ? selTransacao.value : 'NULL');
    var nFinalidade = (selFinalidade.selectedIndex >= 0 ? selFinalidade.value : 'NULL');
    var nEmpresaID = dsoSup01.recordset['EmpresaID'].value;
    var sCfopId = '';
    var sDespesas = '';

    nQuantidade = (isNaN(nQuantidade) ? 1 : nQuantidade);
    nPrecoConvertido = parseFloat(txtPrecoConvertido.value);

    //Chamada pela precoConvertido
    if (bPrecoConvertido == true) 
    {
        txtPrecoRevenda.value = padNumReturningStr(roundNumber(nPrecoConvertido, 2), 2);
        txtValorComissaoInterna.value = padNumReturningStr(roundNumber(0, 2), 2);
        precoFaturamento(true);
    }
    else 
    {
        nPrecoRevenda = parseFloat(txtPrecoRevenda.value);

        nPrecoRevenda = (isNaN(nPrecoRevenda) ? nPrecoConvertido : nPrecoRevenda);

        if (nPrecoRevenda <= nPrecoConvertido) 
        {
            txtPrecoConvertido.value = padNumReturningStr(roundNumber(nPrecoRevenda, 2), 2);
            precoConvertido();
        }
        else 
        {
            if (selPessoaID.value > 0) 
            {
                sPessoa = selPessoaID.value;
                sParceiroID = (selParceiroID.value > 0 ? selParceiroID.value : 'NULL');

                if (sParceiroID != 'NULL')
                    nListaPreco = -sParceiroID;
            }

            lockInterface(true);

            strPars += '?nEmpresaID=' + escape(nEmpresaID);
            strPars += '&sPessoa=' + sPessoa;
            strPars += '&sParceiroID=' + sParceiroID;
            strPars += '&nProdutoID=' + escape(nProdutoID);
            strPars += '&nQuantidade=' + escape(nQuantidade);
            strPars += '&nListaPreco=' + escape(nListaPreco);
            strPars += '&nPreco=' + escape(nPrecoRevenda);
            strPars += '&nPrecoRevenda=' + escape(nPrecoRevenda);
            strPars += '&nPrecoConvertido=' + escape(nPrecoConvertido);
            strPars += '&nAliquotaImposto=' + escape(nAliquotaImposto);
            strPars += '&nMoedaID=' + escape(nMoedaID);
            strPars += '&nFinanciamentoID=' + escape(nFinanciamentoID);
            strPars += '&nFrete=0'; // + escape(selFrete.value);
            strPars += '&nCalc=' + escape(1);
            strPars += '&nTransacao=' + escape(nTransacao);
            strPars += '&nFinalidade=' + escape(nFinalidade);

            // Sem Pessoa
            if (selChavePessoaID.value == 2) 
            {
                strPars += '&nselClassificacoesClaID=' + escape(selClassificacoesClaID.selectedIndex >= 0 ? selClassificacoesClaID.value : 0);
                strPars += '&nselTipoPessoa=' + escape(selTipoPessoa.value);
                strPars += '&nchkFaturamentoDireto=' + escape(chkFaturamentoDireto.checked ? 1 : 0);
                strPars += '&nchkContribuinte=' + escape(chkContribuinte.checked ? 1 : 0);
                strPars += '&nchkSimplesNacional=' + escape(chkSimplesNacional.checked ? 1 : 0);
                strPars += '&nchkSuframaID=' + escape(chkSuframaID.checked ? 1 : 0);
                strPars += '&nselCidadeId=' + escape(selCidadeId.selectedIndex >= 0 ? selCidadeId.value : 0);
                strPars += '&nselClassificacoesID=' + escape(selClassificacoesID.selectedIndex >= 0 ? selClassificacoesID.value : 0);
                strPars += '&ntxtCnae=' + escape(txtCnae.value);
                strPars += '&nselIsencoesID=' + escape(selIsencoesID.value);
                strPars += '&nPaisID=' + escape(nEmpresaData[1]);
                strPars += '&nUFID=' + escape(selUFID.selectedIndex >= 0 ? selUFID.value : 0);
            }
            else 
            {
                strPars += '&nselClassificacoesClaID=' + escape("NULL");
                strPars += '&nselTipoPessoa=' + escape("NULL");
                strPars += '&nchkFaturamentoDireto=' + escape("NULL");
                strPars += '&nchkContribuinte=' + escape("NULL");
                strPars += '&nchkSimplesNacional=' + escape("NULL");
                strPars += '&nchkSuframaID=' + escape("NULL");
                strPars += '&nselCidadeId=' + escape("NULL");
                strPars += '&nselClassificacoesID=' + escape("NULL");
                strPars += '&ntxtCnae=' + escape("NULL");
                strPars += '&nselIsencoesID=' + escape("NULL");
                strPars += '&nPaisID=' + escape("NULL");
                strPars += '&nUFID=' + escape("NULL");
            }

            setConnection(dsoListaPreco);

            dsoListaPreco.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/preco.aspx' + strPars;
            dsoListaPreco.ondatasetcomplete = precoRevenda_DSC;
            dsoListaPreco.refresh();
        }
    }
}
/********************************************************************
Funcao criada pelo programador.

Parametro:  
nenhum        

Retorno:
nenhum
********************************************************************/
function precoRevenda_DSC() 
{
    var nContribuicao;
    var nMargem;
    var nValorComissaoInterno = 0;
    var nPrecoRevenda = txtPrecoRevenda.value;
    var nPrecoConvertido = txtPrecoConvertido.value;
    var nPercentualSUP = 0;

    if (!((dsoListaPreco.recordset.BOF) && (dsoListaPreco.recordset.EOF))) 
    {
        nContribuicao = parseFloat(dsoListaPreco.recordset['Contribuicao'].value);
        nMargem = parseFloat(dsoListaPreco.recordset['MargemContribuicao'].value);
        nValorComissaoInterno = parseFloat(dsoListaPreco.recordset['ValorSUP'].value);

        txtContribuicao.value = padNumReturningStr(roundNumber(nContribuicao, 2), 2);
        txtMargemContribuicao.value = padNumReturningStr(roundNumber(nMargem, 2), 2);
        //txtPrecoWeb.value = padNumReturningStr(roundNumber(parseFloat(dsoListaPreco.recordset['PrecoWeb'].value), 2), 2);

        glb_CFOPID = dsoListaPreco.recordset['CFOPID'].value;

        txtCFOPID.value = (glb_CFOPID == null ? '' : glb_CFOPID);
        txtCFOPID.title = dsoListaPreco.recordset['CFOP'].value;
    }
    else 
    {
        txtContribuicao.value = '0.00';
        txtMargemContribuicao.value = '0.00';
        nMargem = 0;
    }

    if (nValorComissaoInterno > 0)
        nPercentualSUP = (((nPrecoRevenda / nPrecoConvertido) - 1) * 100);

    txtValorComissaoInterna.value = padNumReturningStr(roundNumber(nValorComissaoInterno, 2), 2);
    txtPercentualSUP.value = padNumReturningStr(roundNumber(nPercentualSUP, 2), 2);

    // Ajusta o Hint de Total
    txtMargemContribuicao.style.fontWeight = 'normal';
    txtMargemContribuicao.style.backgroundColor = '#FFFFFF';
    txtMargemContribuicao.title = 'Margem de Contribui��o';

    txtPrecoFaturamento.style.backgroundColor = '#FFFFFF';

    if (dsoSup01.recordset['MargemMinima'].value != null)
    {
        txtMargemContribuicao.title = 'Margem de Contribui��o (Margem M�nima: ' + (dsoSup01.recordset['MargemMinima'].value).toString() + '%)';

        if (nMargem < dsoSup01.recordset['MargemMinima'].value) {
            txtMargemContribuicao.style.fontWeight = 'bold';
            txtMargemContribuicao.style.backgroundColor = '#FF7F50';
            txtMargemContribuicao.title = 'Margem de Contribui��o inferior a margem m�nima (Margem M�nima: ' + (dsoSup01.recordset['MargemMinima'].value).toString() + '%)';

            txtPrecoFaturamento.style.backgroundColor = '#FF7F50';
        }
    }
    
    precoFaturamento(true);    

    lockInterface(false);

    try {
        window.focus();
        txtPrecoConvertido.focus();
    }
    catch (e) 
    {
        ;
    }
}

/********************************************************************
Funcao criada pelo programador.
Atualiza os hints dos campos calculados

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function setFieldsHints()
{
	var nQuantidade = parseInt(txtQuantidade.value, 10);
	var nPrecoConvertido = parseFloat(txtPrecoConvertido.value);
	var nPrecoRevenda = parseFloat(txtPrecoRevenda.value);
	var nPrecoFaturamento = parseFloat(txtPrecoFaturamento.value);
	var nValorComissaoInterna = parseFloat(txtValorComissaoInterna.value);
	var nValorComissaoRevenda = parseFloat(txtValorComissaoRevenda.value);
	var nContribuicao = parseFloat(txtContribuicao.value);
	var sTotal = 'Total ';
	var sNCM = dsoSup01.recordset['NCM'].value;
	
	if ( isNaN(nQuantidade) || isNaN(nPrecoConvertido) )
		txtPrecoConvertido.title = '';
	else	
		txtPrecoConvertido.title = sTotal + padNumReturningStr(roundNumber((nQuantidade * nPrecoConvertido),2),2);

    if (isNaN(nQuantidade) || isNaN(nPrecoRevenda))
        txtPrecoRevenda.title = '';
    else
        txtPrecoRevenda.title = sTotal + padNumReturningStr(roundNumber((nQuantidade * nPrecoRevenda), 2), 2);

	if ( isNaN(nQuantidade) || isNaN(nPrecoFaturamento) )
		txtPrecoFaturamento.title = '';
	else
		txtPrecoFaturamento.title = sTotal + padNumReturningStr(roundNumber((nQuantidade * nPrecoFaturamento),2),2);

    if (isNaN(nQuantidade) || isNaN(nValorComissaoInterna))
        txtValorComissaoInterna.title = '';
    else
        txtValorComissaoInterna.title = sTotal + padNumReturningStr(roundNumber((nQuantidade * nValorComissaoInterna), 2), 2);

    if (isNaN(nQuantidade) || isNaN(nValorComissaoRevenda))
	    txtValorComissaoRevenda.title = '';
	else
	    txtValorComissaoRevenda.title = sTotal + padNumReturningStr(roundNumber((nQuantidade * nValorComissaoRevenda), 2), 2);
	
	if ( isNaN(nQuantidade) || isNaN(nContribuicao) )
		txtContribuicao.title = '';
	else
		txtContribuicao.title = sTotal + padNumReturningStr(roundNumber((nQuantidade * nContribuicao),2),2);
	
	if ( (sNCM != null) && (sNCM != '') )
		txtImposto.title = 'Classifica��o Fiscal: ' + sNCM;
	else	
		txtImposto.title = '';
}

/********************************************************************
Funcao criada pelo programador.

Parametro:  
PrecoConvertido - null funcao foi chamada pela alteracao do campo txtPrecoFaturamento
				- true funcao foi chamada pela funcao precoConvertido

Retorno:
nenhum
********************************************************************/
function precoFaturamento(bPrecoRevenda)
{
    var nPrecoRevenda;
    var nPrecoFaturamento;
    var nPercentualSUP;
    var nFatorTotalImpostos = dsoSup01.recordset['FatorTotalImpostos'].value;
    var strPars = new String();
    var nEmpresaData = getCurrEmpresaData();
    var sPessoa = 'NULL';
    var sParceiroID = 'NULL';
    var nProdutoID = dsoSup01.recordset['ConceitoID'].value;
    var nAliquotaImposto = parseFloat(txtImposto.value);
    var nMoedaID = parseFloat(selMoedaConversaoID.value);
    var nFinanciamentoID = parseFloat(selFinanciamentoID.value);
    var nQuantidade = txtQuantidade.value;
    var nListaPreco = 0;
    var nTransacao = (selTransacao.selectedIndex >= 0 ? selTransacao.value : 'NULL');
    var nFinalidade = (selFinalidade.selectedIndex >= 0 ? selFinalidade.value : 'NULL');
    var nEmpresaID = dsoSup01.recordset['EmpresaID'].value;
    var sCfopId = '';
    var sDespesas = '';

    nQuantidade = (isNaN(nQuantidade) ? 1 : nQuantidade);

    nPrecoRevenda = parseFloat(txtPrecoRevenda.value);

    //Chamada pela precoRevenda
    if (bPrecoRevenda == true) 
    {
        txtPrecoFaturamento.value = padNumReturningStr(roundNumber(nPrecoRevenda, 2), 2);
        txtValorComissaoRevenda.value = padNumReturningStr(roundNumber(0, 2), 2);
        txtPercentualSUP.value = padNumReturningStr(roundNumber(0, 2), 2);

        // Ajusta o Hint de Total
        setFieldsHints();
    }
    else 
    {
        nPrecoFaturamento = parseFloat(txtPrecoFaturamento.value);

        nPrecoFaturamento = (isNaN(nPrecoFaturamento) ? nPrecoRevenda : nPrecoFaturamento);

        nPrecoConvertido = parseFloat(txtPrecoConvertido.value);

        if (nPrecoFaturamento <= nPrecoRevenda) 
        {
            txtPrecoRevenda.value = padNumReturningStr(roundNumber(nPrecoFaturamento, 2), 2);    
            precoRevenda(false);
        }
        else {
            if (selPessoaID.value > 0) {
                sPessoa = selPessoaID.value;
                sParceiroID = (selParceiroID.value > 0 ? selParceiroID.value : 'NULL');

                if (sParceiroID != 'NULL')
                    nListaPreco = -sParceiroID;
            }

            lockInterface(true);

            strPars += '?nEmpresaID=' + escape(nEmpresaID);
            strPars += '&sPessoa=' + sPessoa;
            strPars += '&sParceiroID=' + sParceiroID;
            strPars += '&nProdutoID=' + escape(nProdutoID);
            strPars += '&nQuantidade=' + escape(nQuantidade);
            strPars += '&nListaPreco=' + escape(nListaPreco);
            strPars += '&nPreco=' + escape(nPrecoFaturamento);
            strPars += '&nPrecoRevenda=' + escape(nPrecoRevenda);
            strPars += '&nPrecoConvertido=' + escape(nPrecoConvertido);
            strPars += '&nAliquotaImposto=' + escape(nAliquotaImposto);
            strPars += '&nMoedaID=' + escape(nMoedaID);
            strPars += '&nFinanciamentoID=' + escape(nFinanciamentoID);
            strPars += '&nFrete=0'; // + escape(selFrete.value);
            strPars += '&nCalc=' + escape(1);
            strPars += '&nTransacao=' + escape(nTransacao);
            strPars += '&nFinalidade=' + escape(nFinalidade);

            // Sem Pessoa
            if (selChavePessoaID.value == 2) 
            {
                strPars += '&nselClassificacoesClaID=' + escape(selClassificacoesClaID.selectedIndex >= 0 ? selClassificacoesClaID.value : 0);
                strPars += '&nselTipoPessoa=' + escape(selTipoPessoa.value);
                strPars += '&nchkFaturamentoDireto=' + escape(chkFaturamentoDireto.checked ? 1 : 0);
                strPars += '&nchkContribuinte=' + escape(chkContribuinte.checked ? 1 : 0);
                strPars += '&nchkSimplesNacional=' + escape(chkSimplesNacional.checked ? 1 : 0);
                strPars += '&nchkSuframaID=' + escape(chkSuframaID.checked ? 1 : 0);
                strPars += '&nselCidadeId=' + escape(selCidadeId.selectedIndex >= 0 ? selCidadeId.value : 0);
                strPars += '&nselClassificacoesID=' + escape(selClassificacoesID.selectedIndex >= 0 ? selClassificacoesID.value : 0);
                strPars += '&ntxtCnae=' + escape(txtCnae.value);
                strPars += '&nselIsencoesID=' + escape(selIsencoesID.value);
                strPars += '&nPaisID=' + escape(nEmpresaData[1]);
                strPars += '&nUFID=' + escape(selUFID.selectedIndex >= 0 ? selUFID.value : 0);
            }
            else 
            {
                strPars += '&nselClassificacoesClaID=' + escape("NULL");
                strPars += '&nselTipoPessoa=' + escape("NULL");
                strPars += '&nchkFaturamentoDireto=' + escape("NULL");
                strPars += '&nchkContribuinte=' + escape("NULL");
                strPars += '&nchkSimplesNacional=' + escape("NULL");
                strPars += '&nchkSuframaID=' + escape("NULL");
                strPars += '&nselCidadeId=' + escape("NULL");
                strPars += '&nselClassificacoesID=' + escape("NULL");
                strPars += '&ntxtCnae=' + escape("NULL");
                strPars += '&nselIsencoesID=' + escape("NULL");
                strPars += '&nPaisID=' + escape("NULL");
                strPars += '&nUFID=' + escape("NULL");
            }
            
            setConnection(dsoListaPreco);

            dsoListaPreco.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/preco.aspx' + strPars;
            dsoListaPreco.ondatasetcomplete = precoFaturamento_DSC;
            dsoListaPreco.refresh();
        }        
    }
}

/********************************************************************
Funcao criada pelo programador.

Parametro:  
nenhum        

Retorno:
nenhum
********************************************************************/
function precoFaturamento_DSC() 
{
    var nContribuicao;
    var nMargem;
    var nValorComissaoRevenda = 0;
    var nValorComissaoInterna = txtValorComissaoInterna.value;
    var nPrecoFaturamento = txtPrecoFaturamento.value;
    var nPrecoConvertido = txtPrecoConvertido.value;
    var nPercentualSUP = 0;

    if (!((dsoListaPreco.recordset.BOF) && (dsoListaPreco.recordset.EOF))) 
    {
        nContribuicao = parseFloat(dsoListaPreco.recordset['Contribuicao'].value);
        nMargem = parseFloat(dsoListaPreco.recordset['MargemContribuicao'].value);
        nValorComissaoRevenda = parseFloat(dsoListaPreco.recordset['ValorSUP'].value);

        txtContribuicao.value = padNumReturningStr(roundNumber(nContribuicao, 2), 2);
        txtMargemContribuicao.value = padNumReturningStr(roundNumber(nMargem, 2), 2);
        txtPrecoWeb.value = padNumReturningStr(roundNumber(parseFloat(dsoListaPreco.recordset['PrecoWeb'].value), 2), 2);

        glb_CFOPID = dsoListaPreco.recordset['CFOPID'].value;

        txtCFOPID.value = (glb_CFOPID == null ? '' : glb_CFOPID);
        txtCFOPID.title = dsoListaPreco.recordset['CFOP'].value;
    }
    else 
    {
        txtContribuicao.value = '0.00';
        txtMargemContribuicao.value = '0.00';
        nMargem = 0;
    }

    if ((nValorComissaoRevenda > 0) || (nValorComissaoInterna > 0))
        nPercentualSUP = (((nPrecoFaturamento / nPrecoConvertido) - 1) * 100);

    txtValorComissaoRevenda.value = padNumReturningStr(roundNumber(nValorComissaoRevenda, 2), 2);
    txtPercentualSUP.value = padNumReturningStr(roundNumber(nPercentualSUP, 2), 2);

    // Ajusta o Hint de Total
    txtMargemContribuicao.style.fontWeight = 'normal';
    txtMargemContribuicao.style.backgroundColor = '#FFFFFF';
    txtMargemContribuicao.title = 'Margem de Contribui��o';

    txtPrecoFaturamento.style.backgroundColor = '#FFFFFF';

    if (dsoSup01.recordset['MargemMinima'].value != null)
    {
        txtMargemContribuicao.title = 'Margem de Contribui��o (Margem M�nima: ' + (dsoSup01.recordset['MargemMinima'].value).toString() + '%)';

        if (nMargem < dsoSup01.recordset['MargemMinima'].value) {
            txtMargemContribuicao.style.fontWeight = 'bold';
            txtMargemContribuicao.style.backgroundColor = '#FF7F50';
            txtMargemContribuicao.title = 'Margem de Contribui��o inferior a margem m�nima (Margem M�nima: ' + (dsoSup01.recordset['MargemMinima'].value).toString() + '%)';

            txtPrecoFaturamento.style.backgroundColor = '#FF7F50';
        }
    }

    setFieldsHints();

    lockInterface(false);

    try 
    {
        window.focus();
        txtPrecoConvertido.focus();
    }
    catch (e) 
    {
        ;
    }    
}

/********************************************************************
Funcao criada pelo programador.

Parametro:  
nenhum        

Retorno:
nenhum
********************************************************************/
function margemContribuicao()
{
    var nEmpresaData = getCurrEmpresaData();
    var sPessoa = 'NULL';
    var nProdutoID = dsoSup01.recordset['ConceitoID'].value;
    var nAliquotaImposto = parseFloat(txtImposto.value);
    var nMoedaID = parseFloat(selMoedaConversaoID.value);
    var nMargemContribuicao = parseFloat(txtMargemContribuicao.value);
    var nQuantidade = txtQuantidade.value;
    var nListaPreco = 0;
    var nFinanciamentoID = parseFloat(selFinanciamentoID.value);
    var nTransacao = (selTransacao.selectedIndex >= 0 ? selTransacao.value : 'NULL');
    var nFinalidade = (selFinalidade.selectedIndex >= 0 ? selFinalidade.value : 'NULL');
        
    nQuantidade = (isNaN(nQuantidade) ? 1 : nQuantidade);
    
    txtQuantidade.value = nQuantidade;

    if (selPessoaID.value > 0)
		sPessoa = selPessoaID.value;

    nMargemContribuicao = (isNaN(nMargemContribuicao) ? '0.00' : nMargemContribuicao);
    txtMargemContribuicao.value = nMargemContribuicao;

    lockInterface(true);
    var strPars = new String();
    strPars += '?nEmpresaID=' + escape(dsoSup01.recordset['EmpresaID'].value);
    strPars += '&sPessoa=' + sPessoa;
    strPars += '&nProdutoID=' + escape(nProdutoID);
    strPars += '&nQuantidade=' + escape(nQuantidade);
    strPars += '&nMargem=' + escape(nMargemContribuicao);
    strPars += '&nListaPreco=' + escape(nListaPreco);
    strPars += '&nAliquotaImposto=' + escape(nAliquotaImposto);
    strPars += '&nMoedaID=' + escape(nMoedaID);
   // strPars += '&nImpostosIncidencia=' + escape(chkImpostosIncidencia.checked ? 1 : 0);
    strPars += '&nFinanciamentoID=' + escape(nFinanciamentoID);
    strPars += '&nFrete=0'; // + escape(selFrete.value);
    strPars += '&nCalc='+escape(2);
    strPars += '&nTransacao=' + escape(nTransacao);
    strPars += '&nFinalidade=' + escape(nFinalidade);


    // Sem Pessoa
    if (selChavePessoaID.value == 2) 
    {
        strPars += '&nselClassificacoesClaID=' + escape(selClassificacoesClaID.selectedIndex >= 0 ? selClassificacoesClaID.value : 0);
        strPars += '&nselTipoPessoa=' + escape(selTipoPessoa.value);
        strPars += '&nchkFaturamentoDireto=' + escape(chkFaturamentoDireto.checked ? 1 : 0);
        strPars += '&nchkContribuinte=' + escape(chkContribuinte.checked ? 1 : 0);
        strPars += '&nchkSimplesNacional=' + escape(chkSimplesNacional.checked ? 1 : 0);
        strPars += '&nchkSuframaID=' + escape(chkSuframaID.checked ? 1 : 0);
        strPars += '&nselCidadeId=' + escape(selCidadeId.selectedIndex >= 0 ? selCidadeId.value : 0);
        strPars += '&nselClassificacoesID=' + escape(selClassificacoesID.selectedIndex >= 0 ? selClassificacoesID.value : 0);
        strPars += '&ntxtCnae=' + escape(txtCnae.value);
        strPars += '&nselIsencoesID=' + escape(selIsencoesID.value);
        strPars += '&nPaisID=' + escape(nEmpresaData[1]);
        strPars += '&nUFID=' + escape(selUFID.selectedIndex >= 0 ? selUFID.value : 0);
    }
    else 
    {
        strPars += '&nselClassificacoesClaID=' + escape("NULL");
        strPars += '&nselTipoPessoa=' + escape("NULL");
        strPars += '&nchkFaturamentoDireto=' + escape("NULL");
        strPars += '&nchkContribuinte=' + escape("NULL");
        strPars += '&nchkSimplesNacional=' + escape("NULL");
        strPars += '&nchkSuframaID=' + escape("NULL");
        strPars += '&nselCidadeId=' + escape("NULL");
        strPars += '&nselClassificacoesID=' + escape("NULL");
        strPars += '&ntxtCnae=' + escape("NULL");
        strPars += '&nselIsencoesID=' + escape("NULL");
        strPars += '&nPaisID=' + escape("NULL");
        strPars += '&nUFID=' + escape("NULL");
    }

    setConnection(dsoListaPreco);
    
    dsoListaPreco.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/preco.aspx'+strPars;
    dsoListaPreco.ondatasetcomplete = margemContribuicao_DSC;
    dsoListaPreco.refresh();
}
/********************************************************************
Funcao criada pelo programador.
Retorno da fun��o que Atualiza os resultados em fun��o do pre�o convertido

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function margemContribuicao_DSC()
{
    if (! ((dsoListaPreco.recordset.BOF) && (dsoListaPreco.recordset.EOF)) )
    {
        txtPrecoConvertido.value = padNumReturningStr(roundNumber(parseFloat(dsoListaPreco.recordset['Preco'].value),2),2);
    }

    lockInterface(false);
    prazo();
    glb_TimerListaPrecoVar = window.setInterval('precoConvertido(null)', 10, 'JavaScript');
}

/********************************************************************
Funcao criada pelo programador.

Parametro:  
nenhum        

Retorno:
nenhum
********************************************************************/
function PercentualSUP()
{
	var nPrecoConvertido = parseFloat(txtPrecoConvertido.value);
	var nPercentualSUP = parseFloat(txtPercentualSUP.value);

	if ( (!isNaN(nPrecoConvertido)) && (!isNaN(nPercentualSUP)) )
	{
	    txtPrecoFaturamento.value = padNumReturningStr(roundNumber(parseFloat(nPrecoConvertido * (1 + (nPercentualSUP / 100))), 2), 2);
		precoFaturamento();
		//window.focus();
		//txtPrecoFaturamento.focus();
	}
}

/********************************************************************
Funcao criada pelo programador.
Atualiza a variacao em funcao do prazo

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function prazo()
{
    var nFinanciamentoID = selFinanciamentoID.value;
    var nPMP = 0;
    var nPrazo = 0;
    var nVariacao = 0;
  
    if (! ((dsoEstaticCmbs.recordset.BOF)&&(dsoEstaticCmbs.recordset.EOF)) )
    {
		dsoEstaticCmbs.recordset.moveFirst();
        dsoEstaticCmbs.recordset.setFilter('fldID = ' + nFinanciamentoID + ' AND Indice = ' + '\'' + 3 + '\'');

        if (!dsoEstaticCmbs.recordset.EOF)
            nPMP = dsoEstaticCmbs.recordset['PMP'].value;

        dsoEstaticCmbs.recordset.setFilter('');

        dsoEstaticCmbs.recordset.moveFirst();
        dsoEstaticCmbs.recordset.setFilter('fldID = ' + selMoedaConversaoID.value + ' AND Indice = ' + '\'' + 2 + '\'');

        if ( (! dsoEstaticCmbs.recordset.EOF) && (dsoEstaticCmbs.recordset['TaxaVenda'].value != null) )
        {
            // calcula a variacao em fun��o do prazo
            nVariacao = (dsoEstaticCmbs.recordset['TaxaVenda'].value * nPMP) / dsoEstaticCmbs.recordset['DiasBase'].value;
			nPrazo = nPMP;
        }

        dsoEstaticCmbs.recordset.setFilter('');

    }
    
    txtPrazoMedioPagamento.value = padNumReturningStr(roundNumber(nPrazo,2),2);
    txtVariacao.value = padNumReturningStr(roundNumber(nVariacao,2),2);
    glb_financeiro = nVariacao; 
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
        
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(dsoSup01.recordset['EmpresaID'].value);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/modalpages/modalprint.asp'+strPars;

    showModalWin(htmlPath, new Array(346,200 + 34));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:

Retorno:
nenhum
********************************************************************/
function aliquotaUF()
{
    var nUFID = parseInt(selUFID.value, 10);
    
    if ( nUFID!=0 )
    {
        if (! ((dsoEstaticCmbs.recordset.BOF)&&(dsoEstaticCmbs.recordset.EOF)) )
        {
            dsoEstaticCmbs.recordset.moveFirst();
            dsoEstaticCmbs.recordset.setFilter('fldID = ' + nUFID + ' AND Indice = ' + '\'' + 1 + '\'');

            if (!dsoEstaticCmbs.recordset.EOF)
            {
                // Aliquota do Estado
                txtImposto.value = dsoEstaticCmbs.recordset['Aliquota'].value;
            }
        	dsoEstaticCmbs.recordset.setFilter('');

        }
        __btn_REFR('sup');
    }
}

function selUFID_OnChange(bRefresh)
{
    if (selUFID.value > 0)
    {
        if (bRefresh)
            aliquotaUF();            
        else
            txtImposto.readOnly = true;
    }
    else
        txtImposto.readOnly = false;
}

function selFinanciamentoID_OnChange(bRefresh)
{
    setLabelOfControlEx(lblFinanciamentoID, selFinanciamentoID);
    prazo();
    precoConvertido(true);
}

function calcula()
{
    if ( this.id == 'txtImposto' )
    {
		glb_bRetificaImposto = false;
        var nImposto = parseFloat(this.value);
        if ( !isNaN(nImposto) )
            this.value.value = 0;
        __btn_REFR('sup');
    }        
//    else if (this.id == 'chkImpostosIncidencia')
//    {
//        __btn_REFR('sup');
//    }        
    else if (this.id == 'txtQuantidade' )
    {
        var nQuantidade = parseFloat(this.value);
        if ( (!isNaN(nQuantidade)) && (nQuantidade == 0) )
            this.value.value = 1;
        __btn_REFR('sup');
    }        
    else if (this.id == 'txtVariacao' )
        precoConvertido(true, 'txtVariacao');
}

/********************************************************************
Funcao criada pelo programador
Vai ao servidor obter o ID da relacao e no retorno dispara um carrier.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openRelationByCarrier()
{
    lockInterface(true);
    
    var strPars = new String();
    var empresaID = dsoSup01.recordset['EmpresaID'].value;
    
    strPars = '?nProdutoID=' + escape(getCurrDataInControl('sup', 'txtRegistroID'));
    strPars += '&nEmpresaID=' + escape(empresaID);
        
    dsoCurrRelation.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/currrelation.aspx' + strPars ;
	dsoCurrRelation.ondatasetcomplete = openRelationByCarrier_DSC;
	dsoCurrRelation.refresh();
}

/********************************************************************
Funcao criada pelo programador
Retorno do servidor da funcao openRelationByCarrier()

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openRelationByCarrier_DSC()
{
    var empresa = getCurrEmpresaData();
    
    // Manda o id da relacao entre a empresa e o produto a detalhar 
    if ( dsoCurrRelation.recordset.fields.count > 0 )
        sendJSCarrier(getHtmlId(), 'SHOWRELPESCON',
                      new Array(empresa[0], dsoCurrRelation.recordset.fields['fldID'].value));       
        
    lockInterface(false);    
}

/********************************************************************
Funcao criada pelo programador
Usuario clicou botao especifico de comprar

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function executePurchaseInModal()
{
    var sErrorMsg = '';
    
    if ( isNaN(txtQuantidade.value) )
        sErrorMsg = 'Quantidade inv�lida';
    else if (parseInt(txtQuantidade.value) == 0)
        sErrorMsg = 'Quantidade inv�lida';
    else if ( isNaN(txtPrecoConvertido.value) )
        sErrorMsg = 'Pre�o Convertido inv�lido';
    else if (parseInt(txtPrecoConvertido.value) == 0)
        sErrorMsg = 'Pre�o Convertido inv�lido';
    else if ( isNaN(txtPrazoMedioPagamento.value) )
        sErrorMsg = 'Prazo inv�lido';
    else if (selMoedaConversaoID.selecteIndex == -1)
        sErrorMsg = 'Moeda inv�lida';

    if (sErrorMsg != '')
    {
        if ( window.top.overflyGen.Alert (sErrorMsg) == 0 )
            return null;
            
        return null;                    
    }

    purchaseItem('S', null);    
}

/********************************************************************
Funcao criada pelo programador
Usuario clicou botao de lupa de impostos

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalImpostos()
{
    var Empresa = getCurrEmpresaData();
    var nEmpresaID = dsoSup01.recordset['EmpresaID'].value;
    var nProdutoID = parseInt(txtRegistroID.value, 10);
    var nPessoaID = parseInt(selPessoaID.value > 0 ? selPessoaID.value : null);
    var nPreco = parseFloat(txtPrecoFaturamento.value);
    var nQuantidade = parseInt(txtQuantidade.value, 10);
    var nCFOPID = txtCFOPID.value;
    var nFinalidade = (selFinalidade.selectedIndex >= 0 ? selFinalidade.value : 0);
    var nTransacao = parseInt(selTransacao.value, 10);
    var sErrorMsg = '';
    var strPars = new String();

    if ( (isNaN(nPreco)) || (nPreco == 0) )
        sErrorMsg = 'Pre�o';
    else if ( (isNaN(nQuantidade)) || (nQuantidade == 0) )
        nQuantidade = 1;

    if (sErrorMsg != '')
    {
        if ( window.top.overflyGen.Alert ('Preencha o campo ' + sErrorMsg) == 0 )
            return null;
            
        return null;                
    }
            
   // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nProdutoID=' + escape(nProdutoID);
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nPreco=' + escape(nPreco);
    strPars += '&nQuantidade=' + escape(nQuantidade);
    strPars += '&nCFOPID=' + escape(nCFOPID);
    strPars += '&nFinalidade=' + escape(nFinalidade);
    strPars += '&nTransacao=' + escape(nTransacao);

    // carregar modal - nao faz operacao de banco no carregamento
    showModalWin(SYS_PAGESURLROOT + '/modcomercial/subatendimento/listaprecos/modalpages/modalimpostos.asp' + strPars,
        new Array(345,230));
}

/********************************************************************
Funcao criada pelo programador
Retorna a empresa alternativa

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function empresaAlternativa()
{
	var nEmpresaAlternativaID = 0;

    if (! ((dsoEstaticCmbs.recordset.BOF)&&(dsoEstaticCmbs.recordset.EOF)) )
    {
        dsoEstaticCmbs.recordset.moveFirst();
        dsoEstaticCmbs.recordset.setFilter('Indice = ' + '\'' + 5 + '\'');

        if ( (! dsoEstaticCmbs.recordset.EOF) && (dsoEstaticCmbs.recordset['fldID'].value != null) )
        {
            nEmpresaAlternativaID = dsoEstaticCmbs.recordset['fldID'].value;
        }
        dsoEstaticCmbs.recordset.setFilter('');
    }

	return nEmpresaAlternativaID;
}

/**********************************************************************
BJBN - Pinta txt do pre�o WEB caso produto esteja no EmailMarketing Atual
**********************************************************************/
function precoEmailMKT() 
{
    var nEmpresaID = dsoSup01.recordset['EmpresaID'].value;
    var nParceiroID = selParceiroID.value;
    var nProdutoID = dsoSup01.recordset['ConceitoID'].value; ;

    if (nParceiroID != '') 
    {
        setConnection(dsoEmailMarketing);

        dsoEmailMarketing.SQL = 'SELECT COUNT(1) AS TemEmail, CONVERT(VARCHAR(10), a.dtEnvio, 103) AS dtEnvio, CONVERT(VARCHAR(10), a.dtValidade, 103) AS dtValidade, ' +
                                        'CONVERT(VARCHAR(10), a.dtValidade, 108) AS dtHorasValidade ' +
                                    'FROM EmailMarketing a WITH(NOLOCK) ' +
                                        'INNER JOIN EmailMarketing_Clientes b WITH(NOLOCK) ON (b.EmailID = a.EmailID) ' +
                                        'INNER JOIN EmailMarketing_Clientes_Produtos c WITH(NOLOCK) ON (c.EmaClienteID = b.EmaClienteID) ' +
                                    'WHERE (a.dtValidade >= GETDATE()) AND (b.PessoaID = ' + nParceiroID + ') AND (a.EstadoID = 63) AND ' +
                                            '(c.ProdutoID = ' + nProdutoID + ') AND (c.EmpresaID = ' + nEmpresaID + ') ' +
                                    'GROUP BY a.dtValidade, a.dtEnvio';

        dsoEmailMarketing.ondatasetcomplete = precoEmailMKT_DSC;
        dsoEmailMarketing.refresh();
    }
}

function precoEmailMKT_DSC() 
{
    var nTemEmail = 0;
    var sEnvio = '';
    var sValidade = '';
    var sHoras = '';
    
    if (dsoEmailMarketing.recordset['TemEmail'].value != '')
    {
        nTemEmail = dsoEmailMarketing.recordset['TemEmail'].value;
        sEnvio = dsoEmailMarketing.recordset['dtEnvio'].value;
        sValidade = dsoEmailMarketing.recordset['dtValidade'].value;
        sHoras = dsoEmailMarketing.recordset['dtHorasValidade'].value;
    }

    if (nTemEmail > 0) 
    {
        txtPrecoWeb.style.background = '#90EE90'; //Verde
        txtPrecoWeb.title = 'Pre�o enviado no Email Marketing do dia ' + sEnvio + ' com validade at� o dia ' + sValidade + ' �s ' + sHoras;
    }
    else 
    {
        txtPrecoWeb.style.background = '#FFFFFF';
        txtPrecoWeb.title = '';
    }
}

/**********************************************************************
TSA - Busca Proprietario e Alternativo do ProdutoID
**********************************************************************/
function PropAltConceito() {

    var nEmpresaID = dsoSup01.recordset['EmpresaID'].value;
    var nProdutoID = dsoSup01.recordset['ConceitoID'].value;

    if (nProdutoID != '') {
        setConnection(dsoPropAltConceito);

        dsoPropAltConceito.SQL = 'SELECT b.Fantasia AS Proprietario, c.Fantasia AS Alternativo, d.Numero AS TelPro, d.DDD AS PropDDD, e.Numero AS TelAlt, e.DDD AS AltDDD '+
	                            'FROM RelacoesPesCon a WITH(NOLOCK) ' +
		                            'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) ' +
		                            'INNER JOIN Pessoas c WITH(NOLOCK) ON (a.AlternativoID = c.PessoaID) ' +
		                            'INNER JOIN Pessoas_Telefones d WITH(NOLOCK) ON (b.PessoaID = d.PessoaID) ' +
		                            'INNER JOIN Pessoas_Telefones e WITH(NOLOCK) ON (c.PessoaID = e.PessoaID) ' +
	                            'WHERE ObjetoID = ' + nProdutoID + ' AND SujeitoID = ' + nEmpresaID + ' AND d.TipoTelefoneID = 119 AND e.TipoTelefoneID = 119 ';

        dsoPropAltConceito.ondatasetcomplete = PropAltConceito_DSC;
        dsoPropAltConceito.refresh();
    }
}


function PropAltConceito_DSC() {

    var nPro = dsoPropAltConceito.recordset['Proprietario'].value;
    var nAlt = dsoPropAltConceito.recordset['Alternativo'].value;
    var nNumPro = dsoPropAltConceito.recordset['TelPro'].value;
    var nNumAlt = dsoPropAltConceito.recordset['TelAlt'].value;
    var nPropDDD = dsoPropAltConceito.recordset['PropDDD'].value;
    var nAltDDD = dsoPropAltConceito.recordset['AltDDD'].value;
    var sLabelConceito = 'GP: ' + nPro + ' ' +'('+ nPropDDD +') '+ nNumPro + "\n" + 'Especialista: ' + nAlt + ' ' +'('+ nAltDDD +') '+ nNumAlt;
    
    txtConceito.title = sLabelConceito;
}
