﻿/***********************************************************************************************
CCotacao.js

Objeto que armazena uma cotacao que vem por Ctr+V de uma planilha de: BOM GPL ou Quote DID.
Pre-requesitos: Para o objeto ser montado, a string deve ter as seguintes colunas:

BOM - GPL:
    Line Number,
    Item Name,
    Description,
    Service Duration,
    Lead Time,
    Included Item,
    Quantity,
    ListPrice
    Extended ListPrice,
    Discount %,
    Selling Price,
    Service Type

Quote DID - Estimate
    #
    Part Number
    Part Description
    Service Duration
    Buy Method
    Unit List Price
    Quantity
    Lead Time (in Days)
    Extended List Price
    Discount %	Discount
    Credits
    Effective Discount %
    Extended Net Price
    Unit Net Price
    Included Item
    Purchase Adjustment
    Unit Net Price Before Credits
    Extended Net Price-CI Value
***********************************************************************************************/
function CCotacao(strCotacao) {
    var retVal = new Array();
    var tab = String.fromCharCode(9);
    var tmpCampos = null;
    var nContBColumns = 0;
    var nCountQuoteDIDColumns = 0;

    this.isHeader = false;
    this.isValid = false;
    this.tipo = null; // null: indefinido ainda, 1:BOM GPL, 2:Quote DID; 3:Registro de BOM GPL, 4:Registro de Quote DID
    this.PartNumber = null;
    this.LineNumber = null;
    this.Description = null;
    this.ListPrice = null;
    this.Quantity = null;
    this.Discount = null;
    this.Currency = null;

    var aBomGplColumns = new Array('Line Number',
        'Item Name',
        'Description',
        'Service Duration',
        'Lead Time',
        'Included Item',
        'Quantity',
        'ListPrice',
        'Extended ListPrice',
        'Discount %',
        'Selling Price',
        'Service Type');

    var aQuoteDIDColumns = new Array('#',
        'Part Number',
        'Part Description',
        'Service Duration',
        'Buy Method',
        'Unit List Price',
        'Quantity',
        'Lead Time (in Days)',
        'Extended List Price',
        'Discount %',
        'Discount',
        'Credits',
        'Effective Discount %',
        'Extended Net Price',
        'Unit Net Price',
        'Included Item',
        'Purchase Adjustment',
        'Unit Net Price Before Credits',
        'Extended Net Price-CI Value');

    tmpCampos = strCotacao.split(tab);

    if (tmpCampos.length <= 0)
        return;

    var nContBomGplColumns = 0;
    var nCountQuoteDIDColumns = 0;

    for (i = 0; i < tmpCampos.length; i++) {
        var sCampos = tmpCampos[i];
        var curr = null;

        if (tmpCampos[i].indexOf('Price') >= 0 && tmpCampos[i].indexOf('(') >= 0) {
            var ini = tmpCampos[i].indexOf('(');
            var fim = tmpCampos[i].indexOf(')');

            sCampos = tmpCampos[i].substr(0, ini - 1);
            curr = tmpCampos[i].substr(ini + 1, fim - ini - 1);
        }

        if (ascan(aBomGplColumns, trimStr(sCampos), 0) >= 0) {
            nContBomGplColumns++;

            if (curr != null)
                this.Currency = curr;
        }

        if (ascan(aQuoteDIDColumns, trimStr(sCampos), 0) >= 0) {
            nCountQuoteDIDColumns++;

            if (curr != null)
                this.Currency = curr;
        }
    }

    // A linha recebida é uma linha de header de um BOM GPL
    if (nContBomGplColumns == aBomGplColumns.length) {
        this.isHeader = true;
        this.tipo = 1;
        this.isValid = true;
        retVal = aBomGplColumns;
    }
    // A linha recebida é uma linha de header de um Quote DID
    else if (nCountQuoteDIDColumns == aQuoteDIDColumns.length) {
        this.isHeader = true;
        this.tipo = 2;
        this.isValid = true;
        retVal = aQuoteDIDColumns;
    }
    // Pode ser uma linha de detalhe de BOM GPL
    else if (tmpCampos.length == aBomGplColumns.length)
    {
        // Fazer aqui a validacao dos campos obrigatorios e se estao respeitando o tipo de dados corretamente
        // if ( ascan(aBomGplColumns, fg.TextMatrix(i, 0), false) == -1 )
        this.isHeader = false;
        this.tipo = 3;
        this.isValid = true;
        retVal = aQuoteDIDColumns;
    }
    // Pode ser uma linha de detalhe de Quote DID
    else if (tmpCampos.length == aQuoteDIDColumns.length) {
        // Fazer aqui a validacao dos campos obrigatorios e se estao respeitando o tipo de dados corretamente
        // if ( ascan(aBomGplColumns, fg.TextMatrix(i, 0), false) == -1 )
        this.isHeader = false;
        this.tipo = 4;
        this.isValid = true;
        retVal = aQuoteDIDColumns;
    }
    // Tratativa para o Service Duration
    var tempServiceDuration = tmpCampos[ascan(aBomGplColumns, 'Service Duration', false)];
    var tempServiceDuration2 = '';

    if (tempServiceDuration != undefined) {
        tempServiceDuration = tempServiceDuration.replace('month(s)', '');
        tempServiceDuration = tempServiceDuration.replace('month', '');
        tempServiceDuration = tempServiceDuration.replace('N/A', '');
        tempServiceDuration = tempServiceDuration.replace('-', '');
        tempServiceDuration = tempServiceDuration.replace('.', ',');

        for (var j = 0; j < tempServiceDuration.length; j++) {
            if (!isNaN(parseFloat(tempServiceDuration.substring(j, j + 1))))
                tempServiceDuration2 += tempServiceDuration.substring(j, j + 1);
            else
                break;
        }
    }
    if (tempServiceDuration2 == '')
        tempServiceDuration2 = 'NULL';

    // Se for um registro de header
    if ((this.tipo == 1) || (this.tipo == 2))
    {
        this.PartNumber = 'Part Number';
        this.LineNumber = 'Line Number';
        this.Description = 'Description';
        this.ListPrice = 'List Price';
        this.Quantity = 'Quantity';
        this.Discount = 'Discount';
        this.ServiceDuration = 'Service Duration';
    }
    // Registro de detalhe de BOM GPL
    else if (this.tipo == 3) {
        this.PartNumber = tmpCampos[ascan(aBomGplColumns, 'Item Name', false)];
        this.LineNumber = tmpCampos[ascan(aBomGplColumns, 'Line Number', false)];
        this.Description = tmpCampos[ascan(aBomGplColumns, 'Description', false)];
        this.ListPrice = tmpCampos[ascan(aBomGplColumns, 'ListPrice', false)];
        this.Quantity = tmpCampos[ascan(aBomGplColumns, 'Quantity', false)];
        this.Discount = tmpCampos[ascan(aBomGplColumns, 'Discount %', false)];
        this.ServiceDuration = tempServiceDuration2;
        glb_quoteBOM = 'BOMGPL';
    }
    // Registro de detalhe de Quote DID
    else if (this.tipo == 4) {
        this.PartNumber = tmpCampos[ascan(aQuoteDIDColumns, 'Part Number', false)];
        this.LineNumber = tmpCampos[ascan(aQuoteDIDColumns, '#', false)];
        this.Description = tmpCampos[ascan(aQuoteDIDColumns, 'Part Description', false)];
        this.ListPrice = tmpCampos[ascan(aQuoteDIDColumns, 'Unit List Price', false)];
        this.Quantity = tmpCampos[ascan(aQuoteDIDColumns, 'Quantity', false)];
        this.Discount = tmpCampos[ascan(aQuoteDIDColumns, 'Discount %', false)];
        this.ServiceDuration = tempServiceDuration2;
        glb_quoteBOM = 'Quote';
    }

    //Tratamento para os casos onde o número venha em formato brasileiro
    if (this.ListPrice != null)
        this.ListPrice = convertNumber(this.ListPrice);
    if(this.Quantity != null)
        this.Quantity = convertNumber(this.Quantity);
    if (this.Discount != null)
        this.Discount = convertNumber(this.Discount);

}
//Função para substituir em um trecho específico da string
function replaceSpecific(text, start, end, argOld, argNew) {
    var text1 = '';
    var text2 = '';
    var text3 = '';

    if (start !== 0)
        text1 = text.substring(0, start);

    text2 = text.substring(start, end);

    if (end !== text.length)
        text3 = text.substring(end, text.length);

    while (text2.indexOf(argOld) >= 0) {
        text2 = text2.replace(argOld, argNew);
    }

    return text1 + text2 + text3;
}
//Função para transformar os números para formato americano
function convertNumber(text) {
    var penultimoCaractere = text.substring(text.length - 2, text.length - 1);
    var antePenultimoCaractere = text.substring(text.length - 3, text.length - 2);
    var anteAntePenultimoCaractere = text.substring(text.length - 4, text.length - 3);

    //Sem ponto e sem vírgula
    if (text.indexOf(',') <= 0 && text.indexOf('.') <= 0) {
        ;
    }
   //Já está no formato americano
    else if (antePenultimoCaractere == '.' || penultimoCaractere == '.') {
        ;
    }
    //Exemplo: 1.123,40
    else if (antePenultimoCaractere == ',') {
        text = replaceSpecific(text, 0, text.length - 3, '.', ',');
        text = replaceSpecific(text, text.length - 3, text.length - 2, ',', '.');
    }
    //Exemplo: 1.123,4
    else if (penultimoCaractere == ',') {
        text = replaceSpecific(text, 0, text.length - 2, '.', ',');
        text = replaceSpecific(text, text.length - 2, text.length - 1, ',', '.');
    }
    //Exemplo: 1,123 - Já está no formato americano
    else if (anteAntePenultimoCaractere == ',') {
        ;
    }
    //Exemplo: 1.123
    else if (anteAntePenultimoCaractere == '.') {
        while (text.indexOf('.') >= 0) {
            text = text.replace('.', ',');
        }
    }
    return text;
}
