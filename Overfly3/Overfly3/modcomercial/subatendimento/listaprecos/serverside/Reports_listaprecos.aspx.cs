﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_listaprecos : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        private string mEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        private int formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);
        private string sEstados = Convert.ToString(HttpContext.Current.Request.Params["sEstados"]);
        private string nMarcaID = Convert.ToString(HttpContext.Current.Request.Params["nMarcaID"]);
        private string selMoeda = Convert.ToString(HttpContext.Current.Request.Params["selMoeda"]);
        private string nUFID = Convert.ToString(HttpContext.Current.Request.Params["nUFID"]);
        private string selLista = Convert.ToString(HttpContext.Current.Request.Params["selLista"]);
        private string nQuantidade = Convert.ToString(HttpContext.Current.Request.Params["nQuantidade"]);
        private string nFinanciamento = Convert.ToString(HttpContext.Current.Request.Params["nFinanciamento"]);
        private string bIncideImposto = Convert.ToString(HttpContext.Current.Request.Params["bIncideImposto"]);
        private string bEstoque = Convert.ToString(HttpContext.Current.Request.Params["bEstoque"]);
        private string bMargemPublicacao = Convert.ToString(HttpContext.Current.Request.Params["bMargemPublicacao"]);
        private string sFiltro = Convert.ToString(HttpContext.Current.Request.Params["sFiltro"]);

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                if (sRelatorioID == "40171")
                {
                    listaPrecos();
                }

            }
        }

        public void listaPrecos()
        {
            // Excel
            int Formato = 2;
            string Title = "Lista de Preços";

            string strSQL = "EXEC sp_ListaPrecos_Relatorio " + nEmpresaID + ", " + sEstados + ", " + nMarcaID + ", " + selMoeda + ", " + nUFID + ", " + selLista + ", " + nQuantidade + ", " +
                                nFinanciamento + ", " + bIncideImposto + ", " + bEstoque + ", " + bMargemPublicacao + ", " + sFiltro;

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "2.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "5.13834", "0.0", "11", "#0113a0", "B", Header_Body, "7.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "13.0", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }
    }
}
