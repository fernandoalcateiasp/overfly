using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.subatendimento.listaprecosEx.serverside
{
	public partial class impostosdata : System.Web.UI.OverflyPage
	{
        private string empresaId;
		private string produtoId;
		private string pessoaId;
		private string parceiroId;
		private string preco;
		private string finalidade;
        private string cfop;
		private string quantidade;

        private string contribuinte;
        private string ufid;
        private string cidadeid;
        private string simplesnacional;
        private string cnae;

        private string isencoes;
        private string suframa;
        private string pais;

        protected string nEmpresaID
		{
			set { empresaId = value; }
		}

		protected string nProdutoID
		{
			set { produtoId = value; }
		}

		protected string nPessoaID
		{
            set { pessoaId = value == ""? null: value; }
		}              

		protected string nParceiroID
		{            
            set { parceiroId = value == ""? null: value; }
		}

		protected string nPreco
		{
			set { preco = value; }
		}

        protected string nCFOPID
        {
            set { cfop = value; }
        }

		protected string nQuantidade
		{
			set { quantidade = value; }
		}
		
		protected string nFinalidade
		{
			set { finalidade = value; }
		}

        public string nContribuinte
        {            
            set { contribuinte = value == ""? null : value; }
        }

        public string nUFID
        {            
            set { ufid = value == ""? null : value; }
        }

        public string nCidade
        {
            set { cidadeid = value == ""? null : value; }
        }

        public string nSimplesNacional
        {
            set { simplesnacional = value == ""? null : value; }
        }

        public string nCnae
        {            
            set { cnae = value == ""? null : value; }
        }

        public string nIsencoes
        {
            set { isencoes = value == ""? null : value; }
        }

        public string nSuframa
        {
            set { suframa = value == ""? null : value; }
        }

        public string nPais
        {
            set { pais = value == ""? null : value; }
        }

		protected DataSet ListaPrecosImpostos()
		{
			// Manta os parametros da ProcedureParameters.
			ProcedureParameters[] listaPrecosImpostosProcParams =
				new ProcedureParameters[16];

			listaPrecosImpostosProcParams[0] = new ProcedureParameters(
				"@EmpresaID",
				System.Data.SqlDbType.Int,
				(empresaId != null ? (Object)int.Parse(empresaId) : DBNull.Value));

			listaPrecosImpostosProcParams[1] = new ProcedureParameters(
				"@ProdutoID",
				System.Data.SqlDbType.Int,
                produtoId != null ? (Object)int.Parse(produtoId) : DBNull.Value);

			listaPrecosImpostosProcParams[2] = new ProcedureParameters(
				"@PessoaID",
				System.Data.SqlDbType.Int,
                pessoaId != null ? (Object)int.Parse(pessoaId) : DBNull.Value);

            listaPrecosImpostosProcParams[3] = new ProcedureParameters(
                "@EhContribuinte",
                System.Data.SqlDbType.Bit,
                contribuinte != null ? (Object)int.Parse(contribuinte.ToString()) : DBNull.Value);

            listaPrecosImpostosProcParams[4] = new ProcedureParameters(
                "@UFID",
                System.Data.SqlDbType.Int,
                ufid != null ? (Object)int.Parse(ufid) : DBNull.Value);

            listaPrecosImpostosProcParams[5] = new ProcedureParameters(
                "@CidadeID",
                System.Data.SqlDbType.Int,
                cidadeid != null ? (Object)int.Parse(cidadeid) : DBNull.Value);

            listaPrecosImpostosProcParams[6] = new ProcedureParameters(
                "@SimplesNacional",
                System.Data.SqlDbType.Bit,
                simplesnacional != null ? (Object)int.Parse(simplesnacional.ToString()) : DBNull.Value);

            listaPrecosImpostosProcParams[7] = new ProcedureParameters(
                "@Cnae",
                System.Data.SqlDbType.Char,
                cnae != null ? (Object)cnae : DBNull.Value);

            listaPrecosImpostosProcParams[8] = new ProcedureParameters(
                "@Isencoes",
                System.Data.SqlDbType.Char,
                isencoes != null ? (Object)isencoes : DBNull.Value);

            listaPrecosImpostosProcParams[9] = new ProcedureParameters(
                "@Suframa",
                System.Data.SqlDbType.Bit,
                suframa != null ? (Object)int.Parse(suframa.ToString()) : DBNull.Value);

            listaPrecosImpostosProcParams[10] = new ProcedureParameters(
                "@PaisID",
                System.Data.SqlDbType.Int,
                pais != null ? (Object)int.Parse(pais) : DBNull.Value);

			listaPrecosImpostosProcParams[11] = new ProcedureParameters(
				"@ParceiroID",
				System.Data.SqlDbType.Int,
                parceiroId != null ? (Object)int.Parse(parceiroId) : DBNull.Value);

			listaPrecosImpostosProcParams[12] = new ProcedureParameters(
                "@Preco",
				System.Data.SqlDbType.Money,
                preco != null ? (Object)double.Parse(preco) : DBNull.Value);

			listaPrecosImpostosProcParams[13] = new ProcedureParameters(
				"@FinalidadeID",
				System.Data.SqlDbType.Int,
				finalidade != null ? (Object)int.Parse(finalidade) : DBNull.Value);

			listaPrecosImpostosProcParams[14] = new ProcedureParameters(
				"@CFOPID",
				System.Data.SqlDbType.Int,
				cfop != null && int.Parse(cfop) != 0 ? (Object)int.Parse(cfop) : DBNull.Value);

			listaPrecosImpostosProcParams[15] = new ProcedureParameters(
				"@Quantidade",
				System.Data.SqlDbType.Int,
                quantidade != null ? (Object)int.Parse(quantidade) : DBNull.Value);

			return DataInterfaceObj.execQueryProcedure(
					"sp_ListaPrecos_Impostos",
					listaPrecosImpostosProcParams
			);
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(ListaPrecosImpostos());
		}
	}
}
