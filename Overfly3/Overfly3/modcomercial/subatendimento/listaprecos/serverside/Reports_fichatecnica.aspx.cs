﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_fichatecnica : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        string RelatorioID;
        private int glb_nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        string glb_sEmpresaFantasia = HttpContext.Current.Request.Params["glb_sEmpresaFantasia"];
        string formato = HttpContext.Current.Request.Params["formato"];
        string nConceitoID = HttpContext.Current.Request.Params["nConceitoID"];
        string nIdiomaDeID = HttpContext.Current.Request.Params["nIdiomaDeID"];
        string nIdiomaParaID = HttpContext.Current.Request.Params["nIdiomaParaID"];
        public string[,] arrSqlQuery;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string nomeRel = Request.Params["RelatorioID"].ToString();

                switch (nomeRel)
                {
                    case "40111":
                        fichaTecnica();
                        break;
                }
            }
        }

        public void fichaTecnica()
        {
            string strSQL1 = "";
            string strSQL2 = "";

            strSQL1 = " SELECT " +
                        " b.ConceitoID AS ConceitoID, h.RecursoAbreviado AS Estado, b.Conceito AS Conceito, b.PartNumber AS PartNumber, c.ItemAbreviado AS Unidade, " +
                        " CONVERT(NUMERIC(5, 2), dbo.fn_Produto_PesosMedidas(b.ConceitoID, 1, 1)) AS PesoBruto, a.PrazoTroca AS PrazoTroca, a.PrazoGarantia AS PrazoGarantia, " +
                        " a.PrazoAsstec AS PrazoAsstec, dbo.fn_Tradutor(d.Conceito, " + nIdiomaDeID + ", " + nIdiomaParaID + ", NULL) AS ConceitoConcreto, e.Conceito AS Marca, " +
                        " b.Modelo AS Modelo, b.Descricao AS Descricao " +
                      " FROM " +
                        " RelacoesPesCon a WITH(NOLOCK) " +
                            " INNER JOIN Conceitos b WITH(NOLOCK) ON(b.ConceitoID = a.ObjetoID) " +
                            " INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON(c.ItemID = b.UnidadeID) " +
                            " INNER JOIN Conceitos d WITH(NOLOCK) ON(d.ConceitoID = b.ProdutoID) " +
                            " INNER JOIN Conceitos e WITH(NOLOCK) ON(e.ConceitoID = b.MarcaID) " +
                            " INNER JOIN Conceitos f WITH(NOLOCK) ON(f.ConceitoID = a.MoedaSaidaID) " +
                            " INNER JOIN Recursos h WITH(NOLOCK) ON(h.RecursoID = a.EstadoID) " +
                      " WHERE " +
                        " a.SujeitoID = " + glb_nEmpresaID + " AND a.TipoRelacaoID = 61 AND b.ConceitoID = " + nConceitoID;

            strSQL2 = " SELECT " +
                        " dbo.fn_Produto_CaracteristicaOrdem(a.ProdutoID, a.CaracteristicaID) AS Ordem, " +
                          " dbo.fn_Tradutor(b.Conceito, " + nIdiomaDeID + ", " + nIdiomaParaID + ", NULL) AS Caracteristica, a.Valor AS Valor, d.FichaTecnica, a.ProdutoID " +
                       " FROM " +
                        " Conceitos_Caracteristicas a WITH(NOLOCK) " +
                           " INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.CaracteristicaID) " +
                           " INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = a.ProdutoID) " +
                           " INNER JOIN RelacoesConceitos d WITH(NOLOCK) ON ((d.SujeitoID = a.CaracteristicaID) AND (d.ObjetoID = c.ProdutoID)) " +
                       " WHERE ((a.ProdutoID = " + nConceitoID + ") AND (a.Checado = 1) AND (d.FichaTecnica = 1)) " +
                       " ORDER BY Ordem ";

            arrSqlQuery = new string[,] {
                                        {"Query1", strSQL1},
                                        {"Query2", strSQL2}
                                      };


            double TamLinha = 19;

            double header_top = 0.45;
            double header_left = 0.1;

            double bloco1_top = 0.10;
            double bloco1ask_left = 0.1;
            double bloco1ans_left = 2.55;

            double bloco2_top = 0.10;
            double bloco2ask_left = 9.6;
            double bloco2ans_left = 14.7;


            Relatorio.CriarRelatório("Ficha Técnica", arrSqlQuery, "BRA", "Portrait", "1");

            //Labels Cabeçalho
            Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, header_left.ToString(), header_top.ToString(), "12", "black", "B", "Header", "2.5", "");
            Relatorio.CriarObjLabel("Fixo", "", "Ficha Técnica", (header_left + 8.12834).ToString(), header_top.ToString(), "12", "#0113a0", "B", "Header", "5", "");
            Relatorio.CriarObjLabelData("Now", "", "", (header_left + 15.0225).ToString(), (header_top).ToString(), "10", "black", "B", "Header", "3.72187", "Horas");

            Relatorio.CriarObjLinha(header_left.ToString(), (header_top + 0.5).ToString(), TamLinha.ToString(), "", "Header");


            //Bloco1  
            Relatorio.CriarObjLabel("Fixo", "", "ID:", bloco1ask_left.ToString(), bloco1_top.ToString(), "10", " ", "L", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "Query1", "ConceitoID", bloco1ans_left.ToString(), bloco1_top.ToString(), "10", " ", "L", "Body", "5.54792", "");

            Relatorio.CriarObjLabel("Fixo", "", "Produto:", bloco1ask_left.ToString(), (bloco1_top + 0.36).ToString(), "10", " ", "L", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "Query1", "Conceito", bloco1ans_left.ToString(), (bloco1_top + 0.36).ToString(), "10", " ", "L", "Body", "5.54792", "");

            Relatorio.CriarObjLabel("Fixo", "", "Família:", bloco1ask_left.ToString(), (bloco1_top + 0.77).ToString(), "10", " ", "L", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "Query1", "ConceitoConcreto", bloco1ans_left.ToString(), (bloco1_top + 0.77).ToString(), "10", " ", "L", "Body", "5.54792", "");

            Relatorio.CriarObjLabel("Fixo", "", "Marca:", bloco1ask_left.ToString(), (bloco1_top + 1.2).ToString(), "10", " ", "L", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "Query1", "Marca", bloco1ans_left.ToString(), (bloco1_top + 1.2).ToString(), "10", " ", "L", "Body", "5.54792", "");

            Relatorio.CriarObjLabel("Fixo", "", "Modelo:", bloco1ask_left.ToString(), (bloco1_top + 1.61).ToString(), "10", " ", "L", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "Query1", "Modelo", bloco1ans_left.ToString(), (bloco1_top + 1.61).ToString(), "10", " ", "L", "Body", "5.54792", "");

            Relatorio.CriarObjLabel("Fixo", "", "Descrição:", bloco1ask_left.ToString(), (bloco1_top + 2.03).ToString(), "10", " ", "L", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "Query1", "Descricao", bloco1ans_left.ToString(), (bloco1_top + 2.03).ToString(), "10", " ", "L", "Body", "5.54792", "");


            //Bloco2
            Relatorio.CriarObjLabel("Fixo", "", "Part Number:", bloco2ask_left.ToString(), bloco2_top.ToString(), "10", " ", "L", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PartNumber", bloco2ans_left.ToString(), bloco2_top.ToString(), "10", " ", "L", "Body", "2.84792", "");

            Relatorio.CriarObjLabel("Fixo", "", "Unidade:", bloco2ask_left.ToString(), (bloco2_top + 0.36).ToString(), "10", " ", "L", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "Query1", "Unidade", bloco2ans_left.ToString(), (bloco2_top + 0.36).ToString(), "10", " ", "L", "Body", "2.84792", "");

            Relatorio.CriarObjLabel("Fixo", "", "Peso Bruto (kg):", bloco2ask_left.ToString(), (bloco2_top + 0.77).ToString(), "10", " ", "L", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PesoBruto", bloco2ans_left.ToString(), (bloco2_top + 0.77).ToString(), "10", " ", "L", "Body", "2.84792", "Right");

            Relatorio.CriarObjLabel("Fixo", "", "Prazo p/ Troca (mês):", bloco2ask_left.ToString(), (bloco2_top + 1.2).ToString(), "10", " ", "L", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PrazoTroca", bloco2ans_left.ToString(), (bloco2_top + 1.2).ToString(), "10", " ", "L", "Body", "2.84792", "Right");

            Relatorio.CriarObjLabel("Fixo", "", "Prazo de Garantia (mês):", bloco2ask_left.ToString(), (bloco2_top + 1.61).ToString(), "10", " ", "L", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PrazoGarantia", bloco2ans_left.ToString(), (bloco2_top + 1.61).ToString(), "10", " ", "L", "Body", "2.84792", "Right");

            Relatorio.CriarObjLabel("Fixo", "", "Prazo de Asstec (mês):", bloco2ask_left.ToString(), (bloco2_top + 2.03).ToString(), "10", " ", "L", "Body", "5.54792", "");
            Relatorio.CriarObjLabel("Query", "Query1", "PrazoAsstec", bloco2ans_left.ToString(), (bloco2_top + 2.03).ToString(), "10", " ", "L", "Body", "2.84792", "Right");


            //Tabela
            Relatorio.CriarObjTabela("0.1", "3", "Query2", true);

            //Coluna1
            Relatorio.CriarObjColunaNaTabela("Característica", "Caracteristica", false, "7", "Default", "10");
            Relatorio.CriarObjCelulaInColuna("Query", "Caracteristica", "DetailGroup", "Null", "Null", 0, false, "10");

            //Coluna2
            Relatorio.CriarObjColunaNaTabela("Valor", "Valor", false, "13", "Default", "10");
            Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup", "Null", "Null", 0, false, "10");

            Relatorio.TabelaEnd();

            //Exporta 
            Relatorio.CriarPDF_Excel("Ficha Técnica", 1);
        }

        protected dataInterface DataInterfaceObj = new dataInterface(
        System.Configuration.ConfigurationManager.AppSettings["application"]);

        private DataTable RequestDatatable(string SQLQuery, string NomeQuery)
        {
            string strConn = "";

            Object oOverflySvrCfg = null;

            DataTable Dt = new DataTable();

            DataSet DsContainer = new DataSet();

            oOverflySvrCfg = comCreateObject("OverflySvrCfg.OverflyMTS");
            strConn = ((OVERFLYSVRCFGLib.OverflyMTS)oOverflySvrCfg).DotNetDataBaseStrConn(DataInterfaceObj.ApplicationName);

            SqlCommand cmd = new SqlCommand(SQLQuery);
            using (SqlConnection con = new SqlConnection(strConn))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    cmd.CommandTimeout = 0;
                    sda.SelectCommand = cmd;

                    sda.Fill(DsContainer, NomeQuery);
                }
            }

            Dt = DsContainer.Tables[NomeQuery];

            return Dt;
        }


        public string translateTherm(string sTherm, string[,] aTranslate)
        {
            int nIdiomaDeID = Convert.ToInt32(HttpContext.Current.Request.Params["nIdiomaDeID"]);
            int nIdiomaParaID = Convert.ToInt32(HttpContext.Current.Request.Params["nIdiomaParaID"]);
            string retVal = "";
            int nSeek;

            if ((sTherm == null) || (sTherm == "") || (nIdiomaDeID == nIdiomaParaID) || (nIdiomaParaID == 246))
                return sTherm;

            nSeek = Assek(aTranslate, sTherm);

            if (nSeek >= 0)
                retVal = aTranslate[nSeek, 1];
            else
                retVal = "";

            return retVal;
        }


        public int Assek(string[,] aArray, string strToSeek)
        {
            int nFinal = (((aArray.Length) / 2) - 1);
            int retVal = -1;

            for (int i = 0; i <= nFinal; i++)
            {
                if (aArray[i, 0].CompareTo(strToSeek) == 0)
                {
                    retVal = i;
                    break;
                }
            }
            return retVal;
        }

        private static Object comCreateObject(string sProgID)
        {
            Type oType = Type.GetTypeFromProgID(sProgID);
            if (oType != null)
            {
                return Activator.CreateInstance(oType);
            }
            return null;
        }
    }
}