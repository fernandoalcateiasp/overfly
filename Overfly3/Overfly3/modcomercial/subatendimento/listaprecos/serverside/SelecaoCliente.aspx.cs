using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.subatendimento.listaprecosEx.serverside
{
	public partial class SelecaoCliente : System.Web.UI.OverflyPage
	{
		private Integer empresaID;
		private Integer userID;
		private java.lang.Boolean meusClientes;
		private Integer selChavePesquisa;
		private string dadoPesquisa;

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(SelecaoClientes());
		}

		private DataSet SelecaoClientes()
		{
			// Manta os parametros da sp_SelecaoClientes.
			ProcedureParameters[] SelecaoClientesProcParams =
				new ProcedureParameters[5];

			SelecaoClientesProcParams[0] = new ProcedureParameters(
				"@EmpresaID",
				System.Data.SqlDbType.Int,
				EmpresaID.ToString());

			SelecaoClientesProcParams[1] = new ProcedureParameters(
				"@UserID",
				System.Data.SqlDbType.Int,
				UserID.ToString());

			SelecaoClientesProcParams[2] = new ProcedureParameters(
				"@MeusClientes",
				System.Data.SqlDbType.Bit,
				MeusClientes.ToString());

			SelecaoClientesProcParams[3] = new ProcedureParameters(
				"@SelChavePesquisa",
				System.Data.SqlDbType.Int,
				SelChavePesquisa.ToString());

			SelecaoClientesProcParams[4] = new ProcedureParameters(
				"@DadoPesquisa",
				System.Data.SqlDbType.VarChar,
				DadoPesquisa.ToString());
			SelecaoClientesProcParams[4].Length = DadoPesquisa.Length;

			return DataInterfaceObj.execQueryProcedure(
				"sp_SelecaoClientes",
				SelecaoClientesProcParams);
		}

		public Integer EmpresaID
		{
			get { return empresaID; }

			set
			{
				if (value == null)
				{
					throw new System.Exception("N�o foi enviado o id da empresa.");
				}

				empresaID = value;
			}
		}

		public Integer UserID
		{
			get { return userID; }

			set
			{
				if (value == null)
				{
					throw new System.Exception("N�o foi enviado o id do usu�rio.");
				}

				userID = value;
			}
		}

		public java.lang.Boolean MeusClientes
		{
			get { return meusClientes; }

			set
			{
				if (value == null)
				{
					throw new System.Exception("N�o foi informado se os clientes selecionados devem ser clientes do usu�rio ou n�o.");
				}

				meusClientes = value;
			}
		}

		public Integer SelChavePesquisa
		{
			get { return selChavePesquisa; }

			set
			{
				if (value == null)
				{
					throw new System.Exception("N�o foi informado como se deve filtrar os clientes.");
				}

				selChavePesquisa = value;
			}
		}

		public string DadoPesquisa
		{
			get { return dadoPesquisa; }

			set
			{
				if (value == null)
				{
					throw new System.Exception("N�o foi informado como se deve filtrar os clientes.");
				}

				dadoPesquisa = value;
			}
		}
	}
}