using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.subatendimento.listaprecosEx.serverside
{
	public partial class balancecampaign : System.Web.UI.OverflyPage
	{
        private static Integer[] emptyAInt = new Integer[0];
       
        protected Integer[] produtoID = emptyAInt;
        protected Integer[] quantidade = emptyAInt;

        protected Integer[] nProdutoID {
            set { produtoID = value != null ? value : emptyAInt; }
        }

        protected Integer[] nQuantidade
        {
            set { quantidade = value != null ? value : emptyAInt; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string sSQL = "";

            for (int i = 0; i < produtoID.Length; i++)
            {
                sSQL += (i == 0 ? "" : " UNION ALL ") +
                        "SELECT dbo.fn_CampanhaProduto_Totais(b.CamProdutoID, NULL, 7) AS saldocampanha, b.ProdutoID AS ProdutoID, " + quantidade[i] + " AS quantidade " +
                        "FROM Campanhas_Produtos b WITH(NOLOCK) " +
                        "WHERE b.Produtoid = " + (produtoID[i] == null ? "NULL" : produtoID[i].ToString()) +
                            " AND b.dtFim >= GETDATE() ";
            }
            WriteResultXML(DataInterfaceObj.getRemoteData(sSQL));
        }
	}
}