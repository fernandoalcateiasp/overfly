using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace Overfly3.modcomercial.subatendimento.listaprecos.serverside
{
    public partial class AcessaProposta : System.Web.UI.OverflyPage
    {
        private int nSacolaID = Convert.ToInt32(HttpContext.Current.Request.Params["nSacolaID"]);

        protected override void PageLoad(object sender, EventArgs e)
        {


            string sql = "SELECT PropostaURL FROM SacolasCompras WITH(NOLOCK) " +
                            "WHERE SacolaID = " + nSacolaID;

            try
            {                
                    WriteResultXML(DataInterfaceObj.getRemoteData(sql));
            }
            catch (System.Exception exception)
            {

                WriteResultXML(DataInterfaceObj.getRemoteData(
                    "select 'Filtro Inv�lido' as fldError"));

            }
        }
    }
}