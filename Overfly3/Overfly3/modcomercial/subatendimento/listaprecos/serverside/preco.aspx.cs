using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.modcomercial.subatendimento.listaprecosEx.serverside
{
	public partial class preco : System.Web.UI.OverflyPage
	{
		private string empresaId = "0";
		private string pessoa = " NULL ";
		private string parceiroId = " NULL ";
		private string produtoId;
		private string quantidade;
		private string precoFatura;
        private string precoRevenda;
        private string precoConvertido;
		private string margem;
		private string listaPreco;
		private string aliquotaImposto;
		private string moedaId;
		//private string impostosIncidencia;
		private string financiamentoId;
		private string frete;
		private string calc;
		private string transacao;
        private string finalidade;
		private string meioTransporteId;
		private string incluiFrete;
        private string LotpedItemID;
        // Sem Pessoa
        private string selClassificacoesClaID;
        private string selTipoPessoa;
        private string chkFaturamentoDireto;
        private string chkContribuinte;
        private string chkSimplesNacional;
        private  string chkSuframaID;
        private string selCidadeId;
        private string selClassificacoesID;
        private string txtCnae;
        private string selIsencoesID;
        private string PaisID;
        private string UFID;

		protected string nEmpresaID
		{
			set { empresaId = value != null ? value : "0"; }
		}
		
		protected string sPessoa
		{
			set { pessoa = value != null ? value : " NULL "; }
		}
		
		protected string sParceiroID
		{
			set { parceiroId = value != null ? value : " NULL "; }
		}

		protected string nProdutoID
		{
			set { produtoId = value != null ? value : "NULL"; }
		}

		protected string nQuantidade
		{
			set { quantidade = value != null ? value : "0"; }
		}

		protected string nPreco
		{
            set { precoFatura = value != null ? value : "0"; }
		}

        protected string nPrecoRevenda
        {
            set { precoRevenda = value != null ? value : "0"; }
        }
        protected string nPrecoConvertido
        {
            set { precoConvertido = value != null ? value : "0"; }
        }

		protected string nMargem
		{
			set { margem = value != null ? value : "0"; }
		}

		protected string nListaPreco
		{
			set { listaPreco = value != null ? value : "0"; }
		}

		protected string nAliquotaImposto
		{
			set { aliquotaImposto = value != null ? value : "0"; }
		}

		protected string nMoedaID
		{
			set { moedaId = value != null ? value : "0"; }
		}

		protected string nFinanciamentoID
		{
			set { financiamentoId = value != null ? value : "0"; }
		}

		protected string nFrete
		{
			set 
			{
				frete = value != null ? value : "0";

				if (int.Parse(frete) == 0)
				{
					incluiFrete = "0";
					meioTransporteId = "NULL";
				}
				else if (int.Parse(frete) == 1)
				{
					incluiFrete = "1";
					meioTransporteId = "NULL";
				}
				else
				{
					incluiFrete = "1";
					meioTransporteId = frete;
				}
			}
		}

		protected string nCalc
		{
			set { calc = value != null ? value : "0"; }
		}

		protected string nTransacao
		{
			set { transacao = value != null ? value : "NULL"; }
		}

        protected string nFinalidade
        {
            set { finalidade = value != null ? value : "NULL"; }
        }

        protected string nselClassificacoesClaID
        {
            set { selClassificacoesClaID = value != null ? value : "NULL"; }
        }

        protected string nselTipoPessoa
        {
            set { selTipoPessoa = value != null ? value : "NULL"; }
        }

        protected string nchkFaturamentoDireto
        {
            set { chkFaturamentoDireto = value != null ? value : "NULL"; }
        }

        protected string nchkContribuinte
        {
            set { chkContribuinte = value != null ? value : "NULL"; }
        }

        protected string nchkSimplesNacional
        {
            set { chkSimplesNacional = value != null ? value : "NULL"; }
        }

        protected string nchkSuframaID
        {
            set { chkSuframaID = value != null ? value : "NULL"; }
        }

        protected string nselCidadeId
        {
            set { selCidadeId = value != null ? value : "NULL"; }
        }

        protected string nselClassificacoesID
        {
            set { selClassificacoesID = value != null ? value : "NULL"; }
        }

        protected string ntxtCnae
        {
            set { txtCnae = value != "" ? value : "NULL"; }
        }

        protected string nselIsencoesID
        {
            set { selIsencoesID = value != "" ? value : "NULL"; }
        }

        protected string nPaisID
        {
            set { PaisID = value != null ? value : "NULL"; }
        }

        protected string nUFID
        {
            set { UFID = value != null ? value : "NULL"; }
        }

        protected string nLotpedItemID 
        {
            set { LotpedItemID = value != null ? value : "NULL"; }
        }


		protected DataSet GeraResultado()
		{
			System.Data.DataSet resultDataSet;

            string sDespesas = "NULL";
            string sDespesasGeral = "NULL";
            string sSQL = "";
            string sPrecoMaior = "";
            string sPrecoMenor = "";
            Double nPrecoFatura = Convert.ToDouble(precoFatura);
            Double nPrecoRevenda = Convert.ToDouble(precoRevenda);
            Double nPrecoConvertido = Convert.ToDouble(precoConvertido);
            string LotpedItemID = "NULL";

            string cfopId = "dbo.fn_EmpresaPessoaTransacao_CFOP(" + empresaId + "," + pessoa + ", " + PaisID + ", " + chkContribuinte + ", " + chkSuframaID + ", " + UFID + ", " + 
                                                                    selIsencoesID + ", " + transacao + ", " + finalidade + "," + produtoId + ", NULL)";

            if(calc == "1")
			{
                if (nPrecoConvertido != nPrecoFatura)
                    sDespesasGeral = "dbo.fn_ListaPreco_SUP(" + empresaId + "," + produtoId + ", " + pessoa + "," + chkContribuinte + "," + chkSimplesNacional + "," + chkSuframaID + "," +
                                                           UFID + "," + selCidadeId + "," + PaisID + "," + txtCnae + "," + selIsencoesID + ", " + parceiroId + "," + precoConvertido + "," +
                                                           precoFatura + "," + finalidade + "," + cfopId + ", 1)";

                if (nPrecoFatura > nPrecoRevenda)
                {
                    sPrecoMaior = precoFatura;
                    sPrecoMenor = precoRevenda;
                }
                else if (nPrecoRevenda > nPrecoConvertido)
                {
                    sPrecoMaior = precoRevenda;
                    sPrecoMenor = precoConvertido;
                }

                if (sPrecoMaior != sPrecoMenor)
                    sDespesas = "dbo.fn_ListaPreco_SUP(" + empresaId + "," + produtoId + ", " + pessoa + "," + chkContribuinte + "," + chkSimplesNacional + "," + chkSuframaID + "," + 
                                                           UFID + "," + selCidadeId + "," + PaisID + "," + txtCnae + "," + selIsencoesID + ", " + parceiroId + "," + sPrecoMenor + "," +
                                                           sPrecoMaior + "," + finalidade + "," + cfopId + ", 1)";

                sSQL = "SELECT dbo.fn_Preco_MargemLista(" + empresaId + ", " + produtoId + ", " + quantidade + ", " + precoFatura + ", NULL, " + sDespesasGeral + ", " +
                    aliquotaImposto + ", " + moedaId + ", GETDATE(), NULL, " + pessoa + " ," +
                    selClassificacoesID + ", " + chkFaturamentoDireto + ", " + chkContribuinte + ", " + chkSimplesNacional + ", " + UFID + ", " + selCidadeId + ", " + txtCnae + ", " + selTipoPessoa + ", " +
                    parceiroId + ", " + financiamentoId + ", " + incluiFrete + ", " + meioTransporteId + ", NULL, " + finalidade + ", " + cfopId + ", 1 ," + LotpedItemID + ", NULL) AS MargemContribuicao, " +
                    "dbo.fn_Preco_MargemLista(" + empresaId +

                    ", " + produtoId + ", " + quantidade + ", " + precoFatura + ", NULL, " + sDespesasGeral + ", " +
                        aliquotaImposto + ", " + moedaId + ", GETDATE(), NULL, " + pessoa + ", " +
                        selClassificacoesID + ", " + chkFaturamentoDireto + ", " + chkContribuinte + ", " + chkSimplesNacional + ", " + UFID + ", " + selCidadeId + ", " + txtCnae + ", " + selTipoPessoa + ", " +
                        parceiroId + ", " + financiamentoId + ", " + incluiFrete + ", " + meioTransporteId + ", NULL, " + finalidade + ", " + cfopId + ", 2," + LotpedItemID + ", NULL) AS Contribuicao, " +
                    "dbo.fn_Preco_PrecoLista(" + empresaId + ", " + produtoId + ", NULL, " + aliquotaImposto + ", " + moedaId + ", GETDATE(), " +
                        listaPreco + " , NULL ," + quantidade + ", 1, " + pessoa + ", " + parceiroId + ", " + chkFaturamentoDireto + ", " +
                        chkContribuinte + ", " + chkSimplesNacional + ", " + chkSuframaID + ", " + UFID + ", " + selCidadeId + ", " + txtCnae + ", " +
                        selIsencoesID + ", 2, " + selClassificacoesClaID + ", " + selClassificacoesID + ", " + selTipoPessoa + ", NULL, NULL, " + financiamentoId + " ," + incluiFrete + ", NULL, NULL, " +
                        finalidade + ", " + cfopId + "," + LotpedItemID + ") AS PrecoWeb, " + cfopId + " AS CFOPID, " +
                        "ISNULL(dbo.fn_Operacoes(" + cfopId + ", 0), SPACE(0)) AS CFOP, " + sDespesas + " AS ValorSUP ";
            }
			else
			{
                sSQL = "SELECT dbo.fn_Preco_PrecoLista(" + empresaId + "," + produtoId + ", NULL, " + aliquotaImposto + ", " + moedaId + ", GETDATE(), " +
                    listaPreco + ", NULL, " + quantidade + ", 0, " + pessoa + ", " + parceiroId + ", " +
                    chkFaturamentoDireto + ", " + chkContribuinte + ", " + chkSimplesNacional + ", " +
                    chkSuframaID + ", " + UFID + ", " + selCidadeId + ", " + txtCnae + ", " + selIsencoesID + ", 1, " +
                    selClassificacoesClaID + ", " + selClassificacoesID + ", " + selTipoPessoa + ", NULL ,  " + margem + ", " + financiamentoId + ", " + incluiFrete + ", " +
                    meioTransporteId + ", NULL, " + finalidade + "," + cfopId + "," + LotpedItemID + ") AS Preco ";
			}
            resultDataSet = DataInterfaceObj.getRemoteData(sSQL);
			return resultDataSet;
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(GeraResultado());
		}
	}
}
