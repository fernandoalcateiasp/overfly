using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modcomercial.subatendimento.listaprecosEx.serverside
{
    public partial class extratopreco : System.Web.UI.OverflyPage
	{
        private string empresaID;
        private string produtoID;
        //private string produtoAlternativoID;
        private string aliquotaImposto;
        private string moedaID;
        //private string dtAtualizacao;
        private string listaPreco;
        private string impostoIncidencia;
        //private string quantidade;
        //private string produtosRequeridos;
        private string pessoaID;
        private string parceiroID;
        private string faturamentoDireto;
        private string ehContribuinte;
        private string simplesNacional;
        private string suframa;
        private string UFID;
        private string cidadeID;
        private string CNAE;
        private string isencoes;
        private string tipoMargem;
        private string classificacaoID;
        private string canalID;
        private string tipoPessoaID;
        private string margemContribuicao;
        private string financiamentoID;
        private string incluiFrete;
        //private string meioTransporteID;
        //private string transportadoraID;
        private string finalidadeID;
        private string CFOPID;
        private string loteID;
        //private string resultado;
        private string chavePessoaID;

        protected string nEmpresaID { set { empresaID = value == "" ? null : value; } }
        protected string nProdutoID { set { produtoID = value == "" ? null : value; } }
        //sempre null protected string nprodutoAlternativoID { set { produtoAlternativoID = value; } }
        protected string nAliquotaImposto { set { aliquotaImposto = ((value == "") || (value == "null")) ? null : value; } }
        protected string lstPre_MoedaConversaoID { set { moedaID = ((value == "") || (value == "0")) ? null : value; } }
        //sempre null protected string nDtAtualizacao { set { dtAtualizacao = value; } }
        protected string lstPre_ListaPreco { set { listaPreco = ((value == "") || (value == "0") || (!(int.Parse(value) > 0))) ? null : value; } }
        protected string nImpostoIncidencia { set { impostoIncidencia = ((value == "") || (value == "null")) ? null : value; } }
        //sempre 1 protected string nQuantidade { set { quantidade = value; } }
        //sempre 0 protected string nProdutosRequeridos { set { produtosRequeridos = value; } }
        protected string lstPre_PessoaID { set { pessoaID = ((value == "") || (value == "0") || (!(int.Parse(value) > 0))) ? null : value; } }
        protected string lstPre_ParceiroID { set { parceiroID = ((value == "") || (value == "0") || (!(int.Parse(value) > 0))) ? null : value; } }
        protected string lstPre_FaturamentoDireto { set { faturamentoDireto = ((value == "") || (value == "0")) ? null : value; } }
        protected string lstPre_Contribuinte { set { ehContribuinte = ((value == "") || (value == "0")) ? null : value; } }
        protected string lstPre_SimplesNacional { set { simplesNacional = ((value == "") || (value == "0")) ? null : value; } }
        protected string lstPre_Suframa { set { suframa = ((value == "") || (value == "0")) ? null : value; } }
        protected string lstPre_UFID { set { UFID = ((value == "") || (value == "0") || (!(int.Parse(value) > 0))) ? null : value; } }
        protected string lstPre_Cidade { set { cidadeID = ((value == "") || (value == "0") || (!(int.Parse(value) > 0))) ? null : value; } }
        protected string lstPre_Cnae { set { CNAE = ((value == "") || (value == "0")) ? null : value; } }
        protected string lstPre_Isencoes { set { isencoes = ((value == "") || (value == "0")) ? null : value; } }
        protected string nTipoMargem { set { tipoMargem = (value == "") ? null : value; } }
        protected string lstPre_Classe { set { classificacaoID = ((value == "") || (value == "0")) ? null : value; } }
        protected string lstPre_Classificacoes { set { canalID = ((value == "") || (value == "0") || (!(int.Parse(value) > 0))) ? null : value; ; } }
        protected string lstPre_TipoPessoa { set { tipoPessoaID = ((value == "") || (value == "0")) ? null : value; } }
        protected string nMargemContribuicao { set { margemContribuicao = value == "" ? null : value; } }
        protected string lstPre_FinanciamentoID { set { financiamentoID = ((value == "") || (value == "0")) ? null : value; } }
        protected string lstPre_Frete { set { incluiFrete = ((value == "") || (value == "0") || (!(int.Parse(value) > 0))) ? null : value; } }
        //sempre null protected string nMeioTransporteID { set { meioTransporteID = value; } }
        //sempre null protected string nTransportadoraID { set { transportadoraID = value; } }
        protected string lstPre_FinalidadeID { set { finalidadeID = ((value == "") || (value == "0") || (!(int.Parse(value) > 0))) ? null : value; } }
        protected string sCFOP { set { CFOPID = ((value == "") || (value == "null")) ? null : value; } }
        protected string nLoteID { set { loteID = ((value == "") || (value == "null")) ? null : value; } }
        //sempre null protected string nResultado { set { resultado = value; } }

        protected string lstPre_ChavePessoaID { set { chavePessoaID = value; } }
        
        protected DataSet ExtratoPrecoLista()
        {
            // Sem pessoa
            if (chavePessoaID == "2")
            {
                pessoaID = null;
                
                if (faturamentoDireto == null)
                    faturamentoDireto = "0";

                if (ehContribuinte == null)
                    ehContribuinte = "0";

                if (simplesNacional == null)
                    simplesNacional = "0";

                if (suframa == null)
                    suframa = "0";
            }
            // Com pessoa
            else
            {
                faturamentoDireto = null;
                ehContribuinte = null;
                simplesNacional = null;
                suframa = null;
                UFID = null;
                cidadeID = null;
                CNAE = null;
                isencoes = null;
                classificacaoID = null;
                canalID = null;
                tipoPessoaID = null;
            }
            
            // Manta os parametros da ProcedureParameters.
	        ProcedureParameters[] extratoPrecoListaProcParams =
		        new ProcedureParameters[34];

	        extratoPrecoListaProcParams[0] = new ProcedureParameters(
		        "@EmpresaID",
		        System.Data.SqlDbType.Int,
		        (empresaID != null ? (Object)int.Parse(empresaID) : DBNull.Value));

	        extratoPrecoListaProcParams[1] = new ProcedureParameters(
		        "@ProdutoID",
		        System.Data.SqlDbType.Int,
                produtoID != null ? (Object)int.Parse(produtoID) : DBNull.Value);

            extratoPrecoListaProcParams[2] = new ProcedureParameters(
                "@ProdutoAlternativoID",
                System.Data.SqlDbType.Int,
                DBNull.Value);
                        
            extratoPrecoListaProcParams[3] = new ProcedureParameters(
                "@AliquotaImposto",
                System.Data.SqlDbType.Money,
                aliquotaImposto != null ? (Object)float.Parse(aliquotaImposto) : DBNull.Value);
            
            extratoPrecoListaProcParams[4] = new ProcedureParameters(
                "@MoedaID",
		        System.Data.SqlDbType.Int,
                moedaID != null ? (Object)int.Parse(moedaID) : DBNull.Value);

            extratoPrecoListaProcParams[5] = new ProcedureParameters(
                "@dtAtualizacao",
		        System.Data.SqlDbType.DateTime,
                DBNull.Value);

            extratoPrecoListaProcParams[6] = new ProcedureParameters(
                "@ListaPreco",
                System.Data.SqlDbType.Int,
                listaPreco != null ? (Object)int.Parse(listaPreco) : DBNull.Value);

            extratoPrecoListaProcParams[7] = new ProcedureParameters(
                "@ImpostoIncidencia",
                System.Data.SqlDbType.Bit,
                //impostoIncidencia != null ? (Object)int.Parse(impostoIncidencia.ToString()) : (Object)int.Parse("0"));
                impostoIncidencia != null ? true : false );

            extratoPrecoListaProcParams[8] = new ProcedureParameters(
                "@Quantidade",
                System.Data.SqlDbType.Int,
                (Object)int.Parse("1"));
            
            extratoPrecoListaProcParams[9] = new ProcedureParameters(
                "@ProdutosRequeridos",
                System.Data.SqlDbType.Bit,
                (tipoMargem.ToString() == "2" ? true : false));

            extratoPrecoListaProcParams[10] = new ProcedureParameters(
                "@PessoaID",
                System.Data.SqlDbType.Int,
                pessoaID != null ? (Object)int.Parse(pessoaID) : DBNull.Value);

            extratoPrecoListaProcParams[11] = new ProcedureParameters(
                "@ParceiroID",
                System.Data.SqlDbType.Int,
                parceiroID != null ? (Object)int.Parse(parceiroID) : DBNull.Value);

            extratoPrecoListaProcParams[12] = new ProcedureParameters(
                "@FaturamentoDireto",
                System.Data.SqlDbType.Bit,
                //faturamentoDireto != null ? (Object)int.Parse(faturamentoDireto.ToString()) : (Object)int.Parse("0"));
                faturamentoDireto == null ? DBNull.Value : (Object)int.Parse(faturamentoDireto));

            extratoPrecoListaProcParams[13] = new ProcedureParameters(
                "@EhContribuinte",
                System.Data.SqlDbType.Bit,
                //ehContribuinte != null ? (Object)int.Parse(ehContribuinte.ToString()) : (Object)int.Parse("0"));
                ehContribuinte == null ? DBNull.Value : (Object)int.Parse(ehContribuinte));

            extratoPrecoListaProcParams[14] = new ProcedureParameters(
                "@SimplesNacional",
                System.Data.SqlDbType.Bit,
                //simplesNacional != null ? (Object)int.Parse(simplesNacional.ToString()) : (Object)int.Parse("0"));
                simplesNacional == null ? DBNull.Value : (Object)int.Parse(simplesNacional));

            extratoPrecoListaProcParams[15] = new ProcedureParameters(
                "@Suframa",
                System.Data.SqlDbType.Bit,
                //suframa != null ? (Object)int.Parse(suframa.ToString()) : (Object)int.Parse("0"));
                suframa == null ? DBNull.Value : (Object)int.Parse(suframa));

            extratoPrecoListaProcParams[16] = new ProcedureParameters(
                "@UFID",
                System.Data.SqlDbType.Int,
                UFID != null ? (Object)int.Parse(UFID) : DBNull.Value);

            extratoPrecoListaProcParams[17] = new ProcedureParameters(
                "@CidadeID",
                System.Data.SqlDbType.Int,
                cidadeID != null ? (Object)int.Parse(cidadeID) : DBNull.Value);

            extratoPrecoListaProcParams[18] = new ProcedureParameters(
                "@CNAE",
                System.Data.SqlDbType.VarChar,
                (!(CNAE == null || CNAE.Length == 0)) ? (Object)CNAE : DBNull.Value);
            extratoPrecoListaProcParams[18].Length = 7;

            extratoPrecoListaProcParams[19] = new ProcedureParameters(
                "@Isencoes",
                System.Data.SqlDbType.VarChar,
                (!(isencoes == null || isencoes.Length == 0)) ? (Object)isencoes : DBNull.Value);
            extratoPrecoListaProcParams[19].Length = 128;

            extratoPrecoListaProcParams[20] = new ProcedureParameters(
                "@TipoMargem",
                System.Data.SqlDbType.Int,
                tipoMargem != null ? (Object)int.Parse(tipoMargem) : DBNull.Value);

            extratoPrecoListaProcParams[21] = new ProcedureParameters(
                "@ClassificacaoID",
                System.Data.SqlDbType.Int,
                classificacaoID != null ? (Object)int.Parse(classificacaoID) : DBNull.Value);

            extratoPrecoListaProcParams[22] = new ProcedureParameters(
                "@CanalID",
                System.Data.SqlDbType.Int,
                canalID != null ? (Object)int.Parse(canalID) : DBNull.Value);

            extratoPrecoListaProcParams[23] = new ProcedureParameters(
                "@TipoPessoaID",
                System.Data.SqlDbType.Int,
                tipoPessoaID != null ? (Object)int.Parse(tipoPessoaID) : DBNull.Value);

            extratoPrecoListaProcParams[24] = new ProcedureParameters(
                "@CustoReposicao",
                System.Data.SqlDbType.Money,
                /*margemContribuicao != null ? (Object)float.Parse(margemContribuicao) : */DBNull.Value);
            
            extratoPrecoListaProcParams[25] = new ProcedureParameters(
                 "@MargemContribuicao",
                 System.Data.SqlDbType.Money,
                 margemContribuicao != null ? (Object)float.Parse(margemContribuicao) : DBNull.Value);
            
            extratoPrecoListaProcParams[26] = new ProcedureParameters(
                "@FinanciamentoID",
                System.Data.SqlDbType.Int,
                financiamentoID != null ? (Object)int.Parse(financiamentoID) : DBNull.Value);

            extratoPrecoListaProcParams[27] = new ProcedureParameters(
                "@IncluiFrete",
                System.Data.SqlDbType.Bit,
                //incluiFrete != null ? (Object)int.Parse(incluiFrete.ToString()) : (Object)int.Parse("0"));
                incluiFrete != null ? true : false );

            extratoPrecoListaProcParams[28] = new ProcedureParameters(
                "@MeioTransporteID",
                System.Data.SqlDbType.Int,
                DBNull.Value);

            extratoPrecoListaProcParams[29] = new ProcedureParameters(
                "@TransportadoraID",
                System.Data.SqlDbType.Int,
                DBNull.Value);

            extratoPrecoListaProcParams[30] = new ProcedureParameters(
                "@FinalidadeID",
                System.Data.SqlDbType.Int,
                finalidadeID != null ? (Object)int.Parse(finalidadeID) : DBNull.Value);

            extratoPrecoListaProcParams[31] = new ProcedureParameters(
                "@CFOPID",
                System.Data.SqlDbType.Int,
                CFOPID != null ? (Object)int.Parse(CFOPID) : DBNull.Value);

            extratoPrecoListaProcParams[32] = new ProcedureParameters(
                "@LoteID",
                System.Data.SqlDbType.Int,
                loteID != null ? (Object)int.Parse(loteID) : DBNull.Value);

            extratoPrecoListaProcParams[33] = new ProcedureParameters(
                "@Resultado", System.Data.SqlDbType.Money, DBNull.Value, ParameterDirection.Output);

	        DataSet ds = DataInterfaceObj.execQueryProcedure( "sp_Preco_PrecoLista", extratoPrecoListaProcParams );

            return ds;
        }

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(ExtratoPrecoLista());
		}
	}
}
