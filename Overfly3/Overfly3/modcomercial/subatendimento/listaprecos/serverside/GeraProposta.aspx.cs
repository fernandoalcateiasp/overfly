using GDriveInterface;
using GSheetInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;
using System.Configuration;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;

namespace Overfly3.modcomercial.subatendimento.listaprecos.serverside
{
    public partial class GeraProposta : System.Web.UI.OverflyPage
    {
        private int nSacolaID = Convert.ToInt32(HttpContext.Current.Request.Params["nSacolaID"]);

        private int nLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["nLinguaLogada"]);

        private string sEmailContatos = HttpContext.Current.Request.Params["sEmailContatos"];

        private string[] aTipo = new string[3];

        private string[] aOrigem = new string[2];

        private string[] aOrigem2 = new string[2];

        private string fantasia;

        private string cidade;

        private string UF;

        private string cnpj;

        private string emailCliente;

        private string emailVendedor;

        private string telefoneVendedor;

        private string fantasiaVendedor;

        private string fantasiaParceiro;

        private string tipoEmpresa;

        private bool contribuinte;

        private string dtProposta;

        private double ICMSST_Alcateia;

        private double ICMSST_Cliente;

        private double precoTotalHardware;

        private double totalSoftware;

        private double totalICMSSTCliente;

        private double totalServico;
        protected double TotalServico { set { totalServico = value; } }

        private double dPrecoTotal;

        private string boletoPrazo;

        private string mailUsuario;

        private bool bSubItens;

        private string sMarca;

        protected override void PageLoad(object sender, EventArgs e)
        {
            SheetProposta();

            try
            {                
                    WriteResultXML(DataInterfaceObj.getRemoteData(
                    "select 'OK' as OK"));
            }
            catch (System.Exception exception)
            {

                WriteResultXML(DataInterfaceObj.getRemoteData(
                    "select 'Filtro Inv�lido' as fldError"));

            }
        }

        public void SheetProposta()
        {
            DriveInterface DI = new DriveInterface("desenvolvimento@overflygdriveinterface.iam.gserviceaccount.com", "overflygdriveinterface-04e1a6d3d471.p12");
            SheetInterface SI;

            string sqlDadosCliente = "SELECT dbo.fn_Cotador_Pessoa_Nome(a.SacolaID, 2) AS Nome, " +
                        "d.Localidade AS Cidade, " +
                        "e.CodigoLocalidade2 AS UF, " +
                        "dbo.fn_Pessoa_Documento(a.PessoaID,c.PaisID,c.UFID, 111,101,NULL) AS Documento, " +
                        "ISNULL(dbo.fn_Pessoa_URL(a.PessoaID, NULL, NULL), SPACE(0)) AS EmailCliente, " +
                        "f.ItemMasculino AS Classificacao, b.EhContribuinteICMS, " +
                        "g.Financiamento AS Prazo, " +
                        "ISNULL(dbo.fn_Pessoa_URL(a.ProprietarioID, NULL, NULL), SPACE(0)) AS EmailVendedor, " +
                        "ISNULL(dbo.fn_Pessoa_URL(a.UsuarioID, NULL, NULL), SPACE(0)) AS EmailUsuario, " +
                        "a.PropostaURL, " +
                        "dbo.fn_Cotador_Tipo_SacolaCompra(a.SacolaID,2) AS TipoSacola, " +
                        "dbo.fn_Cotador_Tipo_SacolaCompra(a.SacolaID, 1) AS TemPessoa, " +
                        "dbo.fn_Pessoa_Telefone(a.ProprietarioID, 119, 119, 1, 0, NULL) AS TelefoneVendedor, " +
                        "h.Fantasia AS FantasiaVendedor, " +
                        "i.Fantasia AS FantasiaParceiro " +
                        "FROM SacolasCompras a WITH(NOLOCK) " +
                            "INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.PessoaID) " +
                            "INNER JOIN Pessoas_Enderecos c WITH(NOLOCK) ON (c.PessoaID = a.PessoaID) " +
                            "INNER JOIN Localidades d WITH(NOLOCK) ON (d.LocalidadeID  = c.CidadeID) " +
                            "INNER JOIN Localidades e WITH(NOLOCK) ON (e.LocalidadeID = c.UFID) " +
                            "INNER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (f.ItemID = b.ClassificacaoID) " +
                            "INNER JOIN FinanciamentosPadrao g WITH(NOLOCK) ON (g.FinanciamentoID = a.FinanciamentoID) " +
                            "INNER JOIN Pessoas h WITH(NOLOCK) ON (h.PessoaID = a.ProprietarioID) " +
                            "INNER JOIN Pessoas i WITH(NOLOCK) ON i.PessoaID = a.ParceiroID " +
                        "WHERE c.Ordem = 1 AND a.SacolaID = " + nSacolaID;

            
            DataSet dsDadosCliente = DataInterfaceObj.getRemoteData(sqlDadosCliente);
            fantasia = dsDadosCliente.Tables[1].Rows[0]["Nome"].ToString();
            cidade = dsDadosCliente.Tables[1].Rows[0]["Cidade"].ToString();
            UF = dsDadosCliente.Tables[1].Rows[0]["UF"].ToString();
            cnpj = dsDadosCliente.Tables[1].Rows[0]["Documento"].ToString();
            emailCliente = dsDadosCliente.Tables[1].Rows[0]["EmailCliente"].ToString();
            emailVendedor = dsDadosCliente.Tables[1].Rows[0]["EmailVendedor"].ToString();
            telefoneVendedor = dsDadosCliente.Tables[1].Rows[0]["TelefoneVendedor"].ToString();
            fantasiaVendedor = dsDadosCliente.Tables[1].Rows[0]["FantasiaVendedor"].ToString();
            fantasiaParceiro = dsDadosCliente.Tables[1].Rows[0]["FantasiaParceiro"].ToString();
            tipoEmpresa = dsDadosCliente.Tables[1].Rows[0]["Classificacao"].ToString();
            contribuinte = (dsDadosCliente.Tables[1].Rows[0]["EhContribuinteICMS"].ToString() == "False" ? false : true);
            boletoPrazo = dsDadosCliente.Tables[1].Rows[0]["Prazo"].ToString();
            mailUsuario = dsDadosCliente.Tables[1].Rows[0]["EmailUsuario"].ToString();
            int tipoSacola = Convert.ToInt32(dsDadosCliente.Tables[1].Rows[0]["TipoSacola"]);
            int temPessoa = Convert.ToInt32(dsDadosCliente.Tables[1].Rows[0]["TemPessoa"]);
            string propostaURL = dsDadosCliente.Tables[1].Rows[0]["PropostaURL"].ToString();


            string sqlDadosSacola = "USE OVERFLY " +
                            "DECLARE @LinesNumber INT, @CurrLine INT, @PrevisaoEntrega1 INT, @PrevisaoEntrega2 VARCHAR(200), @EmpresaID INT, @TipoProdutoID INT, @OrigemID INT, @ProdutoID INT, " +
                                    "@PessoaID INT, @EhContribuinte BIT, @UFDestinoID INT, @UFOrigemID INT, @AliquotaInterna NUMERIC(25,2), @AliquotaInterna2 NUMERIC(25,2), " +
                                    "@Aliquota NUMERIC(25,2), @ParceiroID INT, @Preco NUMERIC(25,2), @FinalidadeID INT, @CFOPID INT, @Quantidade INT, @ISS VARCHAR(7), @IPI VARCHAR(7), @ICMS VARCHAR(7), " +
                                    "@dtPreco DATETIME, @ReducaoBaseICMS NUMERIC(25,2), @BaseCalculo NUMERIC(25,2), @Difal NUMERIC(25,2), @ValorICMSDestino NUMERIC(25,2), @ValorICMS NUMERIC(25,2), @NCM VARCHAR(25), " +
                                    "@MarcaID INT, @TransacaoID INT, @SacolaID INT = " + nSacolaID + " " +

                            "DECLARE @CotadorLineNumber TABLE(PKID INT, SacItemID INT, SacItemCotID INT, Codigo VARCHAR(20)) " +

                            "INSERT INTO @CotadorLineNumber " +
                                "EXEC sp_CotadorCorporativo_SacolaItensCodigo @SacolaID " +

                            "DECLARE @Cotador TABLE(IDCot INT IDENTITY, CorpProdutoID INT, PartNumber VARCHAR(200), Produto VARCHAR(400), Vigencia INT, NCM VARCHAR(25), Quantidade INT, " +
                            "Moeda VARCHAR(3), OrigemID INT, Origem VARCHAR(80), Finalidade VARCHAR(80), FinalidadeID INT, PrecoUnitario NUMERIC(25,2), PrecoTotal NUMERIC(25,2), IPI VARCHAR(7), ISS VARCHAR(7), " +
                            "ICMS VARCHAR(7), ICMSST NUMERIC(25,2), Guia VARCHAR(10), EmpresaID INT, Estoque VARCHAR(20), TipoProdutoID INT, LinhaProduto VARCHAR(400), PrevisaoEntrega VARCHAR(200), " +
                            "SacItemID INT, ProdutoID INT, CFOPID INT, dtPreco DATETIME, Difal NUMERIC(25,2), LineNumber VARCHAR(20), PKLineNumber INT, MarcaID INT, Marca VARCHAR(25)) " +

                            //Preenche os dados dos itens m�es
                            "INSERT INTO @Cotador(CorpProdutoID, PartNumber, Produto, Vigencia, NCM, Quantidade, Moeda, OrigemID, Origem, Finalidade, FinalidadeID, PrecoUnitario, PrecoTotal, ICMSST, " +
                                                   "Guia, EmpresaID, Estoque, TipoProdutoID, LinhaProduto, PrevisaoEntrega, SacItemID, ProdutoID, CFOPID, dtPreco, LineNumber, PKLineNumber, MarcaID, Marca) " +
                               "SELECT	a.CorpProdutoID, b.Produto AS PartNumber, b.DescricaoProduto, a.Vigencia, c.Conceito AS NCM, a.Quantidade, CASE WHEN b.OrigemID IN (2701,2706) THEN 'US$' ELSE 'R$' END AS Moeda, b.OrigemID, " +
                               "CASE WHEN b.OrigemID IN (2701,2706) THEN 'Importado' ELSE 'Nacional' END, f.ItemMasculino AS Finalidade, a.FinalidadeID,	dbo.fn_cotador_ProdutoPrecoMae(a.SacItemID) AS PrecoUnitario, " +
                               "(dbo.fn_cotador_ProdutoPrecoMae(a.SacItemID) * a.Quantidade) AS PrecoTotal, (a.ICMSST * a.Quantidade) AS ICMSST, a.Guia, a.EmpresaID, e.Fantasia AS Estoque, b.TipoProdutoID,  " +
                               "ISNULL(dbo.fn_TipoAuxiliar_Item(b.TipoProdutoID, 0), NULL) AS 'Linha Produto', '' AS PrevisaoEntrega, a.SacItemID, a.ProdutoID, a.CFOPID, a.dtPreco, CASE WHEN g.Codigo IS NULL THEN i.Codigo ELSE g.Codigo END, " + 
                               "CASE WHEN g.PKID IS NULL THEN i.PKID ELSE g.PKID END, b.MarcaID, j.Conceito " +
                                   "FROM SacolasCompras_Itens a WITH(NOLOCK) " +
                                     "INNER JOIN CorpProdutos b WITH(NOLOCK) ON b.CorpProdutoID = a.CorpProdutoID " +
                                     "LEFT JOIN Conceitos c WITH(NOLOCK) ON b.NCMID = c.ConceitoID " +
                                     "INNER JOIN Conceitos d WITH(NOLOCK) ON d.ConceitoID = a.MoedaID " +
                                     "INNER JOIN Pessoas e WITH(NOLOCK) ON e.PessoaID = a.EmpresaID " +
                                     "INNER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON f.ItemID = a.FinalidadeID " +
                                     "LEFT JOIN @CotadorLineNumber g ON g.SacItemID = a.SacItemID AND g.SacItemCotID IS NULL " +
                                     "LEFT JOIN SacolasCompras_Itens_CotadorDetalhes h WITH(NOLOCK) ON h.SacItemReferenciaID = a.SacItemID " + // LineNumber dos itens de servi�o e software
                                     "LEFT JOIN @CotadorLineNumber i ON i.SacItemCotID = h.SacItemCotID " +
                                     "INNER JOIN Conceitos j WITH(NOLOCK) ON j.ConceitoID = b.MarcaID " +
                                   "WHERE a.SacolaID = @SacolaID " +

                            //Atualiza a previs�o de entrega
                            " SET @LinesNumber = @@ROWCOUNT " +
                            "SET @CurrLine = 1 " +
                            "WHILE (@CurrLine <= @LinesNumber) " +
                            "BEGIN " +
                                "SELECT	@PrevisaoEntrega1 = dbo.fn_Produto_PrevisaoEntregaAtual(NULL,NULL,CorpProdutoID,NULL,NULL,NULL,NULL), @EmpresaID = EmpresaID, " +
                                        "@TipoProdutoID = TipoProdutoID, @OrigemID = OrigemID " +
                                     "FROM @Cotador " +
                                     "WHERE IDCot = @CurrLine " +
                                "IF(@PrevisaoEntrega1 IS NULL) " +
                                "BEGIN " +
                                    "SET @PrevisaoEntrega2 = 'N�o � possivel.' " +
                                "END " +
                                "ELSE " +
                                "BEGIN " +
                                    "IF((@EmpresaID=2 AND (@TipoProdutoID=1134 OR @TipoProdutoID=1133) AND (CASE @OrigemID WHEN 2701 THEN 1 WHEN 2706 THEN 1 ELSE 0 END) = 1)) " +
                                    "BEGIN " +
                                        "SET @PrevisaoEntrega2 = 'At� 10 dias �teis ap�s o HW' " +
                                    "END " +
                                    "ELSE IF(@PrevisaoEntrega1 <> 0) " +
                                    "BEGIN " +
                                        "SET @PrevisaoEntrega2 = 'At� ' + CONVERT(VARCHAR,@PrevisaoEntrega1) + ' dias �teis' " +
                                    "END " +
                                    "ELSE IF(@PrevisaoEntrega1 = 0) " +
                                    "BEGIN " +
                                        "SET @PrevisaoEntrega2 = '' " +
                                    "END " +
                                "END " +
                                "UPDATE @Cotador SET PrevisaoEntrega = @PrevisaoEntrega2 WHERE IDCot = @CurrLine " +
                                "SET @CurrLine = @CurrLine +1 " +
                            "END " +

                            //Insere os dados dos itens filhos
                            "INSERT INTO @Cotador(CorpProdutoID, PartNumber, Produto, Vigencia, NCM, Quantidade, Moeda, OrigemID, Origem, Finalidade, PrecoUnitario, PrecoTotal, " +
                                "EmpresaID, Estoque, TipoProdutoID, LinhaProduto, PrevisaoEntrega, SacItemID, CFOPID, ProdutoID, dtPreco, Guia, ICMSST, LineNumber, PKLineNumber) " +
                                "SELECT	NULL, a.Produto, a.DescricaoProduto, b.Vigencia, b.NCM, a.Quantidade, b.Moeda, b.OrigemID, b.Origem, b.Finalidade, a.PrecoUnitario, (a.PrecoUnitario * a.Quantidade) AS PrecoTotal, " +
                                    "b.EmpresaID, b.Estoque, b.TipoProdutoID, ISNULL(dbo.fn_TipoAuxiliar_Item(d.TipoProdutoID,0), b.LinhaProduto), b.PrevisaoEntrega, a.SacItemID, b.CFOPID, b.ProdutoID, b.dtPreco, a.Guia, (a.ICMSST * a.Quantidade), c.Codigo, c.PKID " +
                                    "FROM SacolasCompras_Itens_CotadorDetalhes a WITH(NOLOCK) " +
                                        "INNER JOIN @Cotador b ON b.SacItemID = a.SacItemID " +
                                        "INNER JOIN @CotadorLineNumber c ON c.SacItemCotID = a.SacItemCotID " +
                                        "LEFT JOIN CorpProdutos d WITH(NOLOCK) ON d.CorpProdutoID = a.CorpProdutoID " +
                                    "WHERE a.SacItemReferenciaID IS NULL " +


                            //Loop para buscar os impostos ICMS, IPI, ISS
                            "SELECT @LinesNumber = COUNT(1) FROM @Cotador " +
                            "SET @CurrLine = 1 " +
                            "SELECT	@PessoaID = a.PessoaID, @ParceiroID = a.ParceiroID, @EhContribuinte = ISNULL(@EhContribuinte,(~dbo.fn_Pessoa_Incidencia(a.EmpresaID, -a.PessoaID))), @UFDestinoID = c.UFID " +
                                "FROM SacolasCompras a WITH(NOLOCK) " +
                                    "INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.PessoaID) " +
                                    "INNER JOIN Pessoas_Enderecos c WITH(NOLOCK) ON (c.PessoaID = a.PessoaID) " +
                                "WHERE c.Ordem = 1 AND a.SacolaID = @SacolaID " +

                            " SELECT @AliquotaInterna = a.Aliquota " +
                                "FROM Conceitos_Aliquotas a WITH(NOLOCK) " +
                                "WHERE ((a.ImpostoID = 9) " +
                                    "AND (a.LocalidadeOrigemID = @UFDestinoID) " +
                                    "AND (a.LocalidadeDestinoID = @UFDestinoID)) " +

                            " CREATE TABLE #CotadorImpostos(ProdutoID INT, ImpostoID INT, Imposto VARCHAR(25), ImpostoAbrev VARCHAR(6), Aliquota NUMERIC(25,2), " +
                                                            "sAliquota VARCHAR(7), ValorImposto NUMERIC(25,2), BaseCalculo NUMERIC(25,2)) " +

                            "WHILE @CurrLine <= @LinesNumber " +
                            "BEGIN " +
                                "SELECT @EmpresaID = NULL, @ProdutoID = NULL, @Preco = NULL, @FinalidadeID = NULL, @CFOPID = NULL, @Quantidade = NULL, @dtPreco = NULL, @UFOrigemID = NULL, @IPI = NULL, " +
                                        "@ICMS = NULL, @ISS = NULL, @ReducaoBaseICMS = NULL, @ValorICMSDestino = NULL, @ValorICMS = NULL, @BaseCalculo = NULL, @Aliquota = NULL, @AliquotaInterna2 = NULL, " +
                                        "@NCM = NULL, @TipoProdutoID = NULL, @MarcaID = NULL, @OrigemID = NULL, @TransacaoID = NULL " +
                                "SELECT	@EmpresaID = EmpresaID, @ProdutoID = ProdutoID,  @Preco = PrecoUnitario, @FinalidadeID = FinalidadeID, @CFOPID = CFOPID, @Quantidade = Quantidade, @dtPreco = dtPreco, " +
                                        "@NCM = NCM, @TipoProdutoID = TipoProdutoID, @MarcaID = MarcaID, @OrigemID = OrigemID " +
                                    "FROM @Cotador WHERE IDCot = @CurrLine " +

                                "SELECT @UFOrigemID = UFID FROM Pessoas_Enderecos WITH(NOLOCK) WHERE PessoaID = @EmpresaID " +

                                //Caso n�o tenha produtoID seleciona um produto compat�vel
                                "IF(@ProdutoID IS NULL OR @ProdutoID = 0) " +
                                "BEGIN " +
                                    "SELECT @ProdutoID = dbo.fn_Cotador_ProdutoCompativel(@EmpresaID, @NCM, @TipoProdutoID, @MarcaID, @OrigemID) " +

                                    //Caso tipo do produto seja servi�o considera TransacaoID 215 caso contr�rio considera Venda (115)
                                    "IF(@TipoProdutoID = 1134) " +
                                        "SET @TransacaoID = 215 " +
                                    "ELSE " +
                                        "SET @TransacaoID = 115 " +

                                    "IF(@CFOPID IS NULL) " +
                                        "SELECT @CFOPID = dbo.fn_EmpresaPessoaTransacao_CFOP(@EmpresaID, @PessoaID, 130, NULL, NULL, NULL, NULL, @TransacaoID, @FinalidadeID, @ProdutoID, NULL) " +
                                "END " +

                                "IF(@Preco > 0) " +
                                "BEGIN " +
                                    "INSERT INTO #CotadorImpostos(ProdutoID, ImpostoID, Imposto, ImpostoAbrev, Aliquota, sAliquota, ValorImposto, BaseCalculo) "    +
                                        "EXEC sp_ListaPrecos_Impostos @EmpresaID, @ProdutoID, @PessoaID, NULL, NULL, NULL,NULL, NULL, NULL, " +
                                                                    "NULL, NULL, @ParceiroID, @Preco, @FinalidadeID, @CFOPID, @Quantidade " +

                                    "SELECT @IPI = sAliquota FROM #CotadorImpostos WHERE ImpostoID = 8 " +
                                    "SELECT @ICMS = sAliquota, @Aliquota = Aliquota, @BaseCalculo = BaseCalculo FROM #CotadorImpostos WHERE ImpostoID = 9 " +
                                    "SELECT @ISS = sAliquota FROM #CotadorImpostos WHERE ImpostoID = 10 " +

                                    "SET @Difal = 0 " +

                                    //C�lculo do Difal
                                    "IF((@UFDestinoID <> @UFOrigemID) AND (@FinalidadeID IN (783, 784)) AND (@EhContribuinte = 0)) " +
                                    "BEGIN " +
                                        "SET @AliquotaInterna2 = ISNULL(@AliquotaInterna, 0) " +

                                        "SELECT @ReducaoBaseICMS = dbo.fn_Produto_DecretosICMSST(@ProdutoID, @EmpresaID, @PessoaID, @FinalidadeID, @UFOrigemID, @UFDestinoID, " +
                                            "NULL, @AliquotaInterna2, @Aliquota, @dtPreco, NULL, NULL, 5) " +



                                        "SELECT @AliquotaInterna2 = (((@AliquotaInterna2 / 100.00) * (@ReducaoBaseICMS / 100.00)) * 100.00) " +

                                        "SET @ValorICMSDestino = (@BaseCalculo*@AliquotaInterna2)/100 " +

                                        "SET @ValorICMS = (@BaseCalculo*@Aliquota)/100 " +

                                        "IF(@ValorICMSDestino>@ValorICMS) " +
                                            "SET @Difal = @ValorICMSDestino - @ValorICMS " +
                                    "END " +

                                    "UPDATE @Cotador SET IPI = @IPI, ICMS = @ICMS, ISS = @ISS, Difal = @Difal WHERE IDCot = @CurrLine " +
                                "END " +
                                "TRUNCATE TABLE #CotadorImpostos " +
                                "SET @CurrLine = @CurrLine + 1 " +
                            "END " +

                            "DROP TABLE #CotadorImpostos " +

                            "SELECT	LinhaProduto, PartNumber, Produto, Vigencia, NCM, Quantidade, Moeda, ISNULL(PrecoUnitario,0) AS PrecoUnitario, ISNULL(PrecoTotal,0) AS PrecoTotal, IPI, ISS, ICMS, ISNULL(ICMSST,0) AS ICMSST, " +
                                   "Guia, ISNULL(Difal,0) AS Difal, PrevisaoEntrega, Estoque, Finalidade, LineNumber, Origem, Marca " +
                                "FROM @Cotador " +
                                "ORDER BY PKLineNumber ";


            DataSet dsDadosSacola = DataInterfaceObj.getRemoteData(sqlDadosSacola);

            // DataSet para obter valor de D�lar PTAX

            string sqlDolarPTAX = "SELECT TOP 1 " +
                "RelConCotacaoID, dtCotacao, ISNULL(CotacaoVenda,0) AS CotacaoVenda " +
                "FROM RelacoesConceitos_Cotacoes WITH(NOLOCK) " +
                "WHERE RelacaoID = 704 AND CONVERT(DATE, dtCotacao) <> CONVERT(DATE, GETDATE()) " +
                "ORDER BY dtCotacao DESC ";

            DataSet dsDolarPTAX = DataInterfaceObj.getRemoteData(sqlDolarPTAX);

            DateTime dtCotacao = Convert.ToDateTime(dsDolarPTAX.Tables[1].Rows[0]["dtCotacao"]);
            double CotacaoVenda = Convert.ToDouble(dsDolarPTAX.Tables[1].Rows[0]["CotacaoVenda"]);

            string sIdioma = "";

            if (nLinguaLogada == 130)
            {
                sIdioma = "pt-BR";
                dtProposta = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
            }

            else
            {
                sIdioma = "en-US";
                dtProposta = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss");
            }

            //Verifica��o se possui subitens para calcular o LineNumber da proposta. (Se n�o possui subitens considera LineNumbers para padr�o)
            string sqlVerificaSubItens = "SELECT COUNT(a.SacItemID) AS qtdeSubItens " +
                                            "FROM SacolasCompras_Itens a WITH(NOLOCK) " +
                                                "INNER JOIN SacolasCompras_Itens_CotadorDetalhes b WITH(NOLOCK) ON b.SacItemID = a.SacItemID " +
                                            "WHERE a.SacolaID = " + nSacolaID + " ";

            DataSet dsVerificaSubItens = DataInterfaceObj.getRemoteData(sqlVerificaSubItens);

            if (Convert.ToInt32(dsVerificaSubItens.Tables[1].Rows[0]["qtdeSubItens"].ToString()) > 0)
                bSubItens = true;
            else
                bSubItens = false;

            //- Sheet -
            SI = new SheetInterface(DI,propostaURL);

            SI.CreateSpreadsheet(fantasia + "_" + nSacolaID, sIdioma.Replace('-','_'));
            SI.CreateSheet("Proposta Comercial", true, "146,208,80,0",7,2);

            //-- Cabecalho --

            // Caso seja localhost, busca no banco do DEV2, para que seja poss�vel testar corretamente no GSheet;
            // GSheet n�o consegue mostrar imagem com link do localhost.
            string host = HttpContext.Current.Request.Url.Host;
            SI.WriteCell("=IMAGE(\"http://" + (host == "localhost" ? "dev2.overfly.com.br" : host) + "/commonpages/serversidegenEx/imageblob.aspx?nFormID=1210&nSubFormID=20100&nRegistroID=2&nTamanho=2\";3)",
                         "A1", 0, horizontalAlign: "CENTER", verticalAlign: "MIDDLE", endMerge: "B6");
            
            //--- Informa��es sobre o cliente ---
            SI.WriteCell(fantasia, "C1", 0, "0,102,204,0", fontSize: 22);
            SI.WriteCell(cidade + " - " + UF, "C2", 0, fontSize: 12);
            SI.WriteCell(temPessoa == 1 ?("CNPJ " + cnpj):"", "C3", 0, fontSize: 12);
            SI.WriteCell(temPessoa==1?emailCliente:"", "C4", 0, fontSize: 12);
            SI.WriteCell("(" + dsDadosSacola.Tables[1].Rows[0]["Finalidade"].ToString() + ")", "C5", 0, fontSize: 12);

            string _sContribuinte = (contribuinte == true ? "- Contribuinte" : "");
            SI.WriteCell("Faturamento para " + tipoEmpresa + " (" + fantasia + ") " + (temPessoa != 0 ?_sContribuinte : ""), "C6", 0, fontSize: 14);
            //--- Informa��es sobre o cliente ---
            
            //--- Informa��es sobre a proposta ---
            SI.WriteCell("Proposta Comercial", "I1", 0, fontSize: 26, horizontalAlign: "RIGHT");
            SI.WriteCell("#" + nSacolaID, "J1", 0, fontSize: 22, horizontalAlign: "RIGHT");
            SI.WriteCell(dtProposta, "Q1", 0, fontSize: 22, horizontalAlign: "RIGHT");
            //--- Informa��es sobre a proposta ---
            //-- Cabecalho --
            
            //-- Itens do pedido --
            //--- Cabecalho ---
            SI.WriteCell("Item", "A7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255", cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");
            
            SI.WriteCell("Part Number", "B7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("Produto", "C7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("NCM", "D7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("Quant", "E7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("$", "F7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("Pre�o\r\nUnit�rio", "G7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("Pre�o\r\nTotal", "H7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("IPI", "I7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("ICMS", "J7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("ISS", "K7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("ICMS-ST\r\nAlcateia", "L7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");
           
            SI.WriteCell("ICMS-ST\r\nCliente", "M7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("Difal", "N7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                        cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("Tempo", "O7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("Entrega", "P7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE");

            SI.WriteCell("Estoque", "Q7", 0, "255,255,255,255", fontSize: 12, border: "true,true,true,true", borderRGBA: "255,255,255,255",
                         cellRGBA: "128,128,128,0", horizontalAlign: "CENTER", verticalAlign: "MIDDLE", rowSize: 40);


            //--- Cabecalho ---

            //--- Tipo, totais e itens ---
            int iLinhaOrigem;
            int iLinha = 9;
            string sRGBA = null;
            string sRGBATipos = null;
            aOrigem[0] = "Importado";
            aOrigem2[0] = "IMPORTADOS";
            aOrigem[1] = "Nacional";
            aOrigem2[1] = "NACIONAIS";
            aTipo[0] = "Hardware";
            aTipo[1] = "Software";
            aTipo[2] = "Servi�o";


            int lineNumberPadrao = 0;

            bool bLinhaOrigem = false;
            bool bLinhaProduto  = false;

            //Loop por Origem (Nacional e Importado)
            for (int k = 0; k < aOrigem.Count(); k++)
            {
                iLinhaOrigem = iLinha - 1;
                bLinhaOrigem = false;

                precoTotalHardware = 0;
                totalICMSSTCliente = 0;
                totalSoftware = 0;
                totalServico = 0;

                for (int l = 0; l < dsDadosSacola.Tables[1].Rows.Count; l++)
                {
                    if (dsDadosSacola.Tables[1].Rows[l]["Origem"].ToString() == aOrigem[k])
                        bLinhaOrigem = true;
                }
                if (bLinhaOrigem)
                {
                    SI.WriteCell(aOrigem2[k], "A" + (iLinha), 0, fontRGBA: "255,0,0,0", fontSize: 21, verticalAlign: "MIDDLE", endMerge: "B" + (iLinha));
                    SI.WriteCell("", "R" + (iLinha), 0, rowSize: 40);
                    iLinha ++;

                    //Loop por tipo (Hardware, Software, Servi�o)
                    for (int i = 0; i < aTipo.Count(); i++)
                    {
                        sRGBA = "255,255,255,0";
                        sRGBATipos = "0,102,204,0";

                        for (int j = 0; j < dsDadosSacola.Tables[1].Rows.Count; j++)
                        {
                            if (dsDadosSacola.Tables[1].Rows[j]["LinhaProduto"].ToString().ToUpper().Replace("SERVICE", "SERVI�O") == aTipo[i].ToUpper() && dsDadosSacola.Tables[1].Rows[j]["Origem"].ToString() == aOrigem[k])
                                bLinhaProduto = true;
                        }
                        if (bLinhaProduto)
                        {
                            SI.WriteCell(aTipo[i], "A" + (iLinha), 0, fontRGBA: sRGBATipos, fontSize: 16, verticalAlign: "MIDDLE", endMerge: "B" + (iLinha));
                            SI.WriteCell("", "R" + (iLinha), 0, rowSize: 32);
                        }
                            
                        bLinhaProduto = false;
                        bool temItem = false;
                        for (int j = 0; j < dsDadosSacola.Tables[1].Rows.Count; j++)
                        {


                            if (dsDadosSacola.Tables[1].Rows[j]["LinhaProduto"].ToString().ToUpper().Replace("SERVICE", "SERVI�O") == aTipo[i].ToUpper() && dsDadosSacola.Tables[1].Rows[j]["Origem"].ToString() == aOrigem[k])
                            {
                                temItem = true;

                                iLinha++;

                                ICMSST_Cliente = 0;
                                ICMSST_Alcateia = 0;

                                if (dsDadosSacola.Tables[1].Rows[j]["Guia"].ToString().ToUpper() == "GARE")
                                    ICMSST_Cliente = Convert.ToDouble(dsDadosSacola.Tables[1].Rows[j]["ICMSST"].ToString());
                                else if (dsDadosSacola.Tables[1].Rows[j]["Guia"].ToString().ToUpper() == "GNRE")
                                    ICMSST_Alcateia = Convert.ToDouble(dsDadosSacola.Tables[1].Rows[j]["ICMSST"].ToString());

                                //Item
                                lineNumberPadrao++;
                                int[] posCell = SI.ConvertCell("A" + (iLinha));
                                string _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                if (bSubItens)
                                    SI.WriteCell(dsDadosSacola.Tables[1].Rows[j]["LineNumber"].ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA);
                                else
                                    SI.WriteCell((lineNumberPadrao).ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA);

                                //Part Number
                                posCell = SI.ConvertCell("B" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(dsDadosSacola.Tables[1].Rows[j]["PartNumber"].ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA);

                                //Produto
                                posCell = SI.ConvertCell("C" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(dsDadosSacola.Tables[1].Rows[j]["Produto"].ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA);

                                //NCM
                                posCell = SI.ConvertCell("D" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(dsDadosSacola.Tables[1].Rows[j]["NCM"].ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA);

                                //Quantidade
                                posCell = SI.ConvertCell("E" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(dsDadosSacola.Tables[1].Rows[j]["Quantidade"].ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA, horizontalAlign: "RIGHT", formatType: "NUMBER");

                                //$
                                posCell = SI.ConvertCell("F" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(dsDadosSacola.Tables[1].Rows[j]["Moeda"].ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA, horizontalAlign: "CENTER");

                                //Pre�o Unit�rio
                                string sPrecoUnitario = dsDadosSacola.Tables[1].Rows[j]["PrecoUnitario"].ToString();
                                if(aOrigem[k] == "Importado")
                                    sPrecoUnitario = (sPrecoUnitario == "" ? "" : (Convert.ToDouble(sPrecoUnitario)/CotacaoVenda).ToString());
                                else
                                    sPrecoUnitario = (sPrecoUnitario == "" ? "" : Convert.ToDouble(sPrecoUnitario).ToString());

                                posCell = SI.ConvertCell("G" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(sPrecoUnitario, _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA, horizontalAlign: "RIGHT", formatType: "NUMBER", pattern: "###,###,##0.00");

                                //Pre�o Total
                                dPrecoTotal = Convert.ToDouble(dsDadosSacola.Tables[1].Rows[j]["PrecoTotal"].ToString());
                                if(aOrigem[k] == "Importado")
                                    dPrecoTotal = dPrecoTotal / CotacaoVenda;

                                posCell = SI.ConvertCell("H" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(dPrecoTotal.ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA, horizontalAlign: "RIGHT", formatType: "NUMBER", pattern: "###,###,##0.00");

                                //IPI
                                posCell = SI.ConvertCell("I" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(dsDadosSacola.Tables[1].Rows[j]["IPI"].ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA, horizontalAlign: "RIGHT");

                                //ICMS
                                posCell = SI.ConvertCell("J" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(dsDadosSacola.Tables[1].Rows[j]["ICMS"].ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA, horizontalAlign: "RIGHT");

                                //ISS
                                posCell = SI.ConvertCell("K" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(dsDadosSacola.Tables[1].Rows[j]["ISS"].ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA, horizontalAlign: "RIGHT");

                                //ICMS-ST - Alcateia
                                posCell = SI.ConvertCell("L" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                if (aOrigem[k] == "Importado")
                                    ICMSST_Alcateia = ICMSST_Alcateia / CotacaoVenda;

                                SI.WriteCell(Convert.ToDouble(ICMSST_Alcateia).ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA, horizontalAlign: "RIGHT", formatType: "NUMBER", pattern: "###,###,##0.00");

                                //ICMS-ST - Cliente
                                posCell = SI.ConvertCell("M" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                if (aOrigem[k] == "Importado")
                                    ICMSST_Cliente = ICMSST_Cliente / CotacaoVenda;

                                SI.WriteCell((ICMSST_Cliente.ToString()), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA, horizontalAlign: "RIGHT", formatType: "NUMBER", pattern: "###,###,##0.00");

                                //Difal
                                double dDifal = Convert.ToDouble(dsDadosSacola.Tables[1].Rows[j]["Difal"].ToString());
                                if (aOrigem[k] == "Importado")
                                    dDifal = dDifal / CotacaoVenda;

                                posCell = SI.ConvertCell("N" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(dDifal.ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA, horizontalAlign: "RIGHT", formatType: "NUMBER", pattern: "###,###,##0.00");

                                //Vig�ncia
                                posCell = SI.ConvertCell("O" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(dsDadosSacola.Tables[1].Rows[j]["Vigencia"].ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA, horizontalAlign: "RIGHT");

                                //Entrega
                                posCell = SI.ConvertCell("P" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(dsDadosSacola.Tables[1].Rows[j]["PrevisaoEntrega"].ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA);

                                //Estoque
                                posCell = SI.ConvertCell("Q" + (iLinha));
                                _posCell = posCell[0].ToString() + "," + posCell[1].ToString();
                                SI.WriteCell(dsDadosSacola.Tables[1].Rows[j]["Estoque"].ToString(), _posCell, 0, fontSize: 12, border: "true,false,false,false", borderRGBA: "226,239,255,0", cellRGBA: sRGBA);

                                //Calculo dos totais
                                if (i == 0)
                                {
                                    precoTotalHardware += dPrecoTotal;
                                    totalICMSSTCliente += ICMSST_Cliente;
                                }
                                else if (i == 1)
                                    totalSoftware += dPrecoTotal;
                                else if (i == 2)
                                    totalServico += dPrecoTotal;
                            }
                        }

                        if (temItem)
                        {
                            iLinha++;

                            //-- Totais 
                            if (i == 0)
                            {
                                SI.WriteCell(precoTotalHardware.ToString(), "H" + (iLinha), 0, fontSize: 16, formatType: "NUMBER", horizontalAlign: "RIGHT", verticalAlign: "MIDDLE", fontRGBA: sRGBATipos, pattern: "###,###,##0.00");
                                SI.WriteCell(totalICMSSTCliente.ToString(), "M" + (iLinha), 0, fontSize: 16, formatType: "NUMBER", horizontalAlign: "RIGHT", verticalAlign: "MIDDLE", fontRGBA: sRGBATipos, pattern: "###,###,##0.00");
                            }

                            if (i == 0)
                            {
                                SI.WriteCell((precoTotalHardware + totalICMSSTCliente).ToString(), "Q" + (iLinha), 0, fontSize: 16, formatType: "NUMBER", horizontalAlign: "RIGHT", verticalAlign: "MIDDLE", fontRGBA: sRGBATipos, pattern: "###,###,##0.00");
                            }
                            else if (i == 1)
                            {
                                SI.WriteCell((totalSoftware).ToString(), "H" + (iLinha), 0, fontSize: 16, formatType: "NUMBER", horizontalAlign: "RIGHT", verticalAlign: "MIDDLE", fontRGBA: sRGBATipos, pattern: "###,###,##0.00");
                                SI.WriteCell((totalSoftware).ToString(), "Q" + (iLinha), 0, fontSize: 16, formatType: "NUMBER", horizontalAlign: "RIGHT", verticalAlign: "MIDDLE", fontRGBA: sRGBATipos, pattern: "###,###,##0.00");
                            }
                            else if (i == 2)
                            {
                                SI.WriteCell((totalServico).ToString(), "H" + (iLinha), 0, fontSize: 16, formatType: "NUMBER", horizontalAlign: "RIGHT", verticalAlign: "MIDDLE", fontRGBA: sRGBATipos, pattern: "###,###,##0.00");
                                SI.WriteCell((totalServico).ToString(), "Q" + (iLinha), 0, fontSize: 16, formatType: "NUMBER", horizontalAlign: "RIGHT", verticalAlign: "MIDDLE", fontRGBA: sRGBATipos, pattern: "###,###,##0.00");
                            }

                            SI.WriteCell("", "R" + (iLinha), 0, rowSize: 32);

                            iLinha = iLinha + 1;
                        }
                        //-- Totais 
                    }
                    iLinha ++;

                    //-- Totais por Origem --
                    if(aOrigem[k] == "Importado")
                        SI.WriteCell("TOTAIS (US$)", "A" + (iLinha), 0, fontRGBA: "255,0,0,0", fontSize: 22, verticalAlign: "MIDDLE", endMerge: "B" + (iLinha));
                    else
                        SI.WriteCell("TOTAIS(R$)", "A" + (iLinha), 0, fontRGBA: "255,0,0,0", fontSize: 22, verticalAlign: "MIDDLE", endMerge: "B" + (iLinha));

                    SI.WriteCell((precoTotalHardware + totalSoftware + totalServico).ToString(), "H" + (iLinha), 0, fontSize: 22, fontRGBA: "255,0,0,0", formatType: "NUMBER", horizontalAlign: "RIGHT", verticalAlign: "MIDDLE", pattern: "###,###,##0.00");
                    SI.WriteCell((totalICMSSTCliente).ToString(), "M" + (iLinha), 0, fontSize: 22, fontRGBA: "255,0,0,0", formatType: "NUMBER", horizontalAlign: "RIGHT", verticalAlign: "MIDDLE", pattern: "###,###,##0.00");
                    SI.WriteCell((precoTotalHardware + totalSoftware + totalServico + totalICMSSTCliente).ToString(), "Q" + (iLinha), 0, fontSize: 22, fontRGBA: "255,0,0,0", formatType: "NUMBER", horizontalAlign: "RIGHT", verticalAlign: "MIDDLE", pattern: "###,###,##0.00");
                    SI.WriteCell("", "R" + (iLinha), 0, rowSize: 40);
                    iLinha += 2;
                }
            }

            //-- Condi��es de Fornecimento --
            SI.WriteCell("Condi��es de Fornecimento", "A" + (iLinha), 0, fontSize: 18, verticalAlign: "MIDDLE");
            SI.WriteCell("", "R" + (iLinha), 0, rowSize: 32);
            SI.WriteCell("Pagamento", "A" + (iLinha + 2), 0, bold: true, fontSize: 12, verticalAlign: "MIDDLE");
            SI.WriteCell("� " + boletoPrazo, "A" + (iLinha + 3), 0, fontRGBA:"255,0,0,0", bold:true, fontSize: 12, verticalAlign: "MIDDLE");
            
            SI.WriteCell("Pre�os", "A" + (iLinha + 5), 0, bold: true, fontSize: 12, verticalAlign: "MIDDLE");
            SI.WriteCell("� Os valores desta proposta ser�o recalculados no dia do seu faturamento com base no D�lar PTAX do dia anterior.", "A" + (iLinha + 6), 0, fontSize: 12, verticalAlign: "MIDDLE");
            SI.WriteCell("� Todos impostos inclusos.", "A" + (iLinha + 7), 0, fontSize: 12, verticalAlign: "MIDDLE");
            SI.WriteCell("� Pre�os v�lidos somente para as quantidades acima.", "A" + (iLinha + 8), 0, fontSize: 12, verticalAlign: "MIDDLE");
            SI.WriteCell("� A lista de produtos desta proposta dever� ser validada tecnicamente pela revenda.", "A" + (iLinha + 9), 0, fontSize: 12, verticalAlign: "MIDDLE");
            
            SI.WriteCell("Entrega", "A" + (iLinha + 11), 0, bold: true, fontSize: 12, verticalAlign: "MIDDLE");
            SI.WriteCell("� A data de entrega � estimada, baseada em dados hist�ricos, v�lida ap�s a confirma��o do pedido.", "A" + (iLinha + 12), 0, fontSize: 12, verticalAlign: "MIDDLE");
            SI.WriteCell("� Atrasos na fabrica��o/disponibiliza��o dos produtos pelo fabricante, poder�o afetar o prazo de entrega.", "A" + (iLinha + 13), 0, fontSize: 12, verticalAlign: "MIDDLE");

            SI.WriteCell("Validade da Proposta", "A" + (iLinha + 15), 0, bold: true, fontSize: 12, verticalAlign: "MIDDLE");
            SI.WriteCell("1 dia.", "A" + (iLinha + 16), 0, fontSize: 12, verticalAlign: "MIDDLE");

            SI.WriteCell("Garantia", "A" + (iLinha + 18), 0, bold: true, fontSize: 12, verticalAlign: "MIDDLE");
            SI.WriteCell("� Prazo e as condi��es de garantia s�o de responsabilidade do fabricante.", "A" + (iLinha + 19), 0, fontSize: 12, verticalAlign: "MIDDLE");

            SI.WriteCell("Aceite", "A" + (iLinha + 21), 0, bold: true, fontSize: 12, verticalAlign: "MIDDLE");
            SI.WriteCell("� Esta proposta ser� considerada aceita mediante a assinatura dos termos e condi��es conforme link abaixo.", "A" + (iLinha + 22), 0, fontSize: 12, verticalAlign: "MIDDLE");
            SI.WriteCell("=HIPERLINK(\"https://drive.google.com/file/d/1xCQmm4FTI_HixCd2_PsnTuHcyemagjMS/view\";\"Termos e condi��es\")", "A" + (iLinha + 23), 0, fontRGBA: "17,85,204,0", underline: true, fontSize: 12, verticalAlign: "MIDDLE");
            //-- Condi��es de Fornecimento --
            SI.WriteCell("", "R1", 0, rowSize: 50);
            SI.WriteCell("", "R6", 0, rowSize: 30);

            //-- Formata��o do tamanho linhas/colunas --
            SI.WriteCell("", "A" + (iLinha + 24), 0, columnSize: 67);
            SI.WriteCell("", "B" + (iLinha + 24), 0, columnSize: 172);
            SI.WriteCell("", "C" + (iLinha + 24), 0, columnSize: 516);
            SI.WriteCell("", "D" + (iLinha + 24), 0, columnSize: 70);
            SI.WriteCell("", "E" + (iLinha + 24), 0, columnSize: 81);
            SI.WriteCell("", "F" + (iLinha + 24), 0, columnSize: 60);
            SI.WriteCell("", "G" + (iLinha + 24), 0, columnSize: 116);
            SI.WriteCell("", "H" + (iLinha + 24), 0, columnSize: 120);
            SI.WriteCell("", "I" + (iLinha + 24), 0, columnSize: 123);
            SI.WriteCell("", "J" + (iLinha + 24), 0, columnSize: 109);
            SI.WriteCell("", "K" + (iLinha + 24), 0, columnSize: 109);
            SI.WriteCell("", "L" + (iLinha + 24), 0, columnSize: 109);
            SI.WriteCell("", "M" + (iLinha + 24), 0, columnSize: 64);
            SI.WriteCell("", "N" + (iLinha + 24), 0, columnSize: 77);
            SI.WriteCell("", "O" + (iLinha + 24), 0, columnSize: 170);
            SI.WriteCell("", "P" + (iLinha + 24), 0, columnSize: 170);
            SI.WriteCell("", "Q" + (iLinha + 24), 0, columnSize: 170);
            SI.WriteCell("", "R" + (iLinha + 24), 0, columnSize: 2);
            //-- Formata��o do tamanho linhas/colunas --

            sMarca = dsDadosSacola.Tables[1].Rows[0]["Marca"].ToString();

            if (propostaURL == null || propostaURL == "")
            {
                string linkSheet = SI.GenerateSpreadsheet("\\Sistemas\\Alcateia\\Cotador", mailUsuario, "reader", "false", typePermission: "anyone");

                //Salva link da proposta na tabela SacolaCompras
                string sqlPropostaURL = "UPDATE SacolasCompras SET PropostaURL = '" + linkSheet + "' WHERE SacolaID = " + nSacolaID;
                DataInterfaceObj.ExecuteSQLCommand(sqlPropostaURL);

                sendEmail(linkSheet, emailVendedor, sEmailContatos, mailUsuario, sMarca);
            }
            else
            {
                SI.UpdateSpreadsheet();
                sendEmail(propostaURL, emailVendedor, sEmailContatos, mailUsuario, sMarca);
            }               
        }

        public void sendEmail(string linkSheet, string vendedorEmail, string emailCliente, string emailUsuario, string Marca)
        {
            string sBodyEmail;
            string sSubject;
            string sSQLEmail;
            string sFrom = vendedorEmail;
            string sTo = emailCliente;
            string sBCc = emailUsuario + ";" + vendedorEmail;

            sSubject = "Alcateia - Cota��o " + Marca + "#" + nSacolaID;

            sBodyEmail = "<HTML>" +
                        "<BODY>" +
                        fantasiaParceiro + ",<br/><br/>" +
                        "Veja a nossa proposta no link abaixo:  <br/><br/>" +
                        linkSheet +
                        "<br/><br/>" +
                        fantasiaVendedor + "<br/>" +
                        telefoneVendedor + "<br/>" +
                        "Skype: " +vendedorEmail + "<br/>" +
                        "facebook.com/alcateiadistribuidora<br/>" +
                        "</BODY>" + 
                        "</HTML>"; 

           
            sSQLEmail = "EXEC sp_Email_Gerador '" + sFrom + "','" + sTo + "',NULL, '" + sBCc + "','" + sSubject + "',0,0,0,'" + sBodyEmail + "',NULL,NULL,0,3,999,NULL";

            DataInterfaceObj.ExecuteSQLCommand(sSQLEmail);
        }
    }
}