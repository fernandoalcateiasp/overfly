using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.PrintJet
{
	public partial class ReportsGrid_ExtratoPreco : System.Web.UI.OverflyPage
	{
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey; 
        private int nLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["nLinguaLogada"]);
        private string controle = Convert.ToString(HttpContext.Current.Request.Params["controle"]);
        private int glb_ExtratoID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_ExtratoID"]);

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                switch (controle)
                {
                    case "Extrato de Pre�o":
                        ReportGrid_ExtratoPreco();
                        break;
                }
            }
        }
        public void ReportGrid_ExtratoPreco()
        {
            DateTime DataHoje = DateTime.Now;
            string _data = DataHoje.ToString("dd_MM_yyyy");

            if (nLinguaLogada != 246)
            {
                _data = DataHoje.ToString("MM_dd_yyyy");
            }

            string Title = "Extrato de Pre�o_" + _data;

            string strSQL = "SELECT a.Descricao, a.Percentual, a.BaseCalculo, b.SimboloMoeda AS Moeda, a.Valor " +
                                  "FROM ExtratosPrecos_Detalhes a WITH(NOLOCK) " +
                                      "INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.MoedaID) " +
                                  "WHERE (a.ExtratoID = " + glb_ExtratoID + ") " +
                                  "ORDER BY a.Ordem DESC, a.ExtDetalheID ";

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            Relatorio.CriarRelat�rio(Title.ToString(), arrSqlQuery, "BRA", "Excel");

            int Datateste = 0;
            bool nulo = false;

            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query est� vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            if (dsFin.Tables["Query1"].Rows.Count == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {
                Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, false);

                Relatorio.CriarObjColunaNaTabela("Descricao", "Descricao", true, "Descricao");
                Relatorio.CriarObjCelulaInColuna("Query", "Descricao", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Percentual", "Percentual", true, "Percentual");
                Relatorio.CriarObjCelulaInColuna("Query", "Percentual", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("BaseCalculo", "BaseCalculo", true, "BaseCalculo");
                Relatorio.CriarObjCelulaInColuna("Query", "BaseCalculo", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Moeda", "Moeda", true, "Moeda");
                Relatorio.CriarObjCelulaInColuna("Query", "Moeda", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Valor", "Valor", true, "Valor");
                Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");

                Relatorio.TabelaEnd();

                Relatorio.CriarPDF_Excel(Title, 2);
            }
        }
	}
}
