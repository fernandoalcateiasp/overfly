﻿/***********************************************************************************************
CCotacao.js

Retorna um list com objetos CCotacao instanciados.
************************************************************************************************/
function LCotacao(strCotacao) {
    var retVal = new Array();
    var tmpLinhas = null;
    var enter = String.fromCharCode(13) + String.fromCharCode(10);
    var nStartRow = -1;
    var i = 0;
    var oCotacao = null;
    var sCurrency = null;

    tmpLinhas = strCotacao.split(enter);

    if (tmpLinhas.length <= 0)
        return retVal;

    for (i=0; i<tmpLinhas.length; i++)
    {
        oCotacao = new CCotacao(tmpLinhas[i]);

        if ((oCotacao.isValid) && !(oCotacao.isHeader)) {
            retVal[retVal.length] = oCotacao;
        }

        else if ((oCotacao.isValid) && (oCotacao.isHeader)) {
            if (oCotacao.Currency != null) {
                sCurrency = oCotacao.Currency;
            }
        }
    }

    retVal[retVal.length] = sCurrency;

    return retVal;
}
