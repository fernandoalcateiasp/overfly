<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="listaprecossup01Html" name="listaprecossup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/subatendimento/listaprecos/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/subatendimento/listaprecos/superioresp.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/subatendimento/listaprecos/pesqlistsuperior.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="listaprecossup01Body" name="listaprecossup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->

    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" name="txtEstadoID" class="fldGeneral">
        <p id="lblEmpresa" name="lblEmpresa" class="lblGeneral">Empresa</p>
        <input type="text" id="txtEmpresa" name="txtEmpresa" DATASRC="#dsoSup01" DATAFLD="Empresa" class="fldGeneral">
        
        <p id="lblLote" name="lblLote" class="lblGeneral">Lote</p>
        <input type="text" id="txtLote" name="txtLote" DATASRC="#dsoSup01" DATAFLD="LoteID" class="fldGeneral">
        
        <p id="lblConceito" name="lblConceito" class="lblGeneral">Produto</p>
        <input type="text" id="txtConceito" name="txtConceito" DATASRC="#dsoSup01" DATAFLD="Conceito" class="fldGeneral">
        <p id="lblPartNumber" name="lblPartNumber" class="lblGeneral">Part Number</p>
        <input type="text" id="txtPartNumber" name="txtPartNumber" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PartNumber">
        <p id="lblUnidade" name="lblUnidade" class="lblGeneral">Un</p>
        <input type="text" id="txtUnidade" name="txtUnidade" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Unidade">
        <p id="lblPesoBruto" name="lblPesoBruto" class="lblGeneral">Peso</p>
        <input type="text" id="txtPesoBruto" name="txtPesoBruto" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PesoBruto" title="Peso Bruto do produto (Kg)">
        <p id="lblPrazoTroca" name="lblPrazoTroca" class="lblGeneral">Troca</p>
        <input type="text" id="txtPrazoTroca" name="txtPrazoTroca" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PrazoTroca" title="Prazo de Troca (em meses)"> 
        <p id="lblPrazoGarantia" name="lblPrazoGarantia" class="lblGeneral">Garantia</p>
        <input type="text" id="txtPrazoGarantia" name="txtPrazoGarantia" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PrazoGarantia" title="Prazo de Garantia (em meses)">
        <p id="lblPrazoAsstec" name="lblPrazoAsstec" class="lblGeneral">Asstec</p>
        <input type="text" id="txtPrazoAsstec" name="txtPrazoAsstec" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PrazoAsstec" title="Prazo de Assist�ncia T�cnica (em meses)">
<!-- 
        <p id="lblConceitoConcreto" name="lblConceitoConcreto" class="lblGeneral">Fam�lia</p>
        <input type="text" id="txtConceitoConcreto" name="txtConceitoConcreto" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ConceitoConcreto">
        <p id="lblMarca" name="lblMarca" class="lblGeneral">Marca</p>
        <input type="text" id="txtMarca" name="txtMarca" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Marca">
        <p id="lblModelo" name="lblModelo" class="lblGeneral">Modelo</p>
        <input type="text" id="txtModelo" name="txtModelo" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Modelo">
        <p id="lblDescricao" name="lblDescricao" class="lblGeneral">Descri��o</p>
        <input type="text" id="txtDescricao" name="txtDescricao" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Descricao">
-->
        <p id="lblDescricaoProduto" name="lblDescricaoProduto" class="lblGeneral">Descri��o do Produto</p>
        <input type="text" id="txtDescricaoProduto" name="txtDescricaoProduto" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DescricaoProduto">

    </div>

    <!-- Primeiro div secundario superior - complementa o principal -->
    <div id="divSup02_01" name="divSup02_01" class="divExtern">
        <p id="lblParceiroID" name="lblParceiroID" class="lblGeneral">Parceiro</p>
        <select id="selParceiroID" name="selParceiroID" class="fldGeneral"></select>
        <!-- 
        <input type="image" id="btnFindCliente" name="btnFindCliente" class="fldGeneral" title="Preencher Combo" WIDTH="24" HEIGHT="23">
        -->
        <p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">Pessoa</p>
        <select id="selPessoaID" name="selPessoaID" class="fldGeneral"></select>
        <p id="lblUFID" name="lblUFID" class="lblGeneral">UF</p>
        <select id="selUFID" name="selUFID" class="fldGeneral"></select>
        <p id="lblClasseID" name="lblClasseID" class="lblGeneral">Cla</p>
        <select id="selClassificacoesClaID" name="selClassificacoesClaID" class="fldGeneral"></select>
        
        <!-- Chave para pesquisa Com pessoa ou Sem pessoa -->
            <p id="lblChavePessoaID" name="lblChavePessoaID" class="lblGeneral">Tipo de cota��o</p>
            <select id="selChavePessoaID" name="selChavePessoaID" class="fldGeneral">
                <option value="1">Com pessoa</option>
			    <option value="2">Sem pessoa</option>
            </select>
		
		<p id="lblCidadeId" name="lblCidadeId" class="lblGeneral">Cidade</p>
		<select id="selCidadeId" name="selCidadeId" class="fldGeneral"></select>
        
	    <p id="lblClassificacoesID" name="lblClassificacoesID" class="lblGeneral">Classifica��es</p>
		<select id="selClassificacoesID" name="selClassificacoesID" class="fldGeneral"></select>
		
	    <p id="lblIsencoesID" name="lblIsencoesID" class="lblGeneral">Isen��es</p>
        <select id="selIsencoesID" name="selIsencoesID" class="fldGeneral" MULTIPLE>
        
		<p id="lblCnae" name="lblCnae" class="lblGeneral">CNAE</p>
		<input type="text" id="txtCnae" name="txtCnae" class="fldGeneral" maxLength="7">
		
        <p id="lblFaturamentoDireto" name="lblFaturamentoDireto" class="lblGeneral">FD</p>
        <input type="checkbox" id="chkFaturamentoDireto" name="chkFaturamentoDireto" class="fldGeneral" >
        
        <p id="lblTipoPessoa" name="lblTipoPessoa" class="lblGeneral">Fis/Jur</p>
		<select id="selTipoPessoa" name="selTipoPessoa" class="fldGeneral" title="Pessoa F�sica ou jur�dica?"></select>
        
        <p id="lblCon" name="lblCon" class="lblGeneral">Con</p>
		<input type="checkbox" id="chkContribuinte" name="chkContribuinte" class="fldGeneral">
		
		<p id="lblSimplesNacionalID" name="lblSimplesNacionalID" class="lblGeneral">SN</p>
		<input type="checkbox" id="chkSimplesNacional" name="chkSimplesNacional" class="fldGeneral" title="Optante do Simples Nacional?">
		
		<p id="lblSuframaID" name="lblSuframaID" class="lblGeneral">Su</p>
		<input type="checkbox" id="chkSuframaID" name="chkSuframaID" class="fldGeneral" title="Tem Suframa?">
        <!--
		<p id="lblFrete" name="lblFrete" class="lblGeneral">Frete</p>
		<select id="selFrete" name="selFrete" class="fldGeneral"></select>
        -->
        <p id="lblPrevisaoEntrega" name="lblPrevisaoEntrega" class="lblGeneral">Previs�o Entrega</p>
        <input type="text" id="txtPrevisaoEntrega" name="txtPrevisaoEntrega" DATASRC="#dsoSup01" DATAFLD="PrevisaoEntrega" class="fldGeneral">

        <p id="lblFinanciamentoID" name="lblFinanciamentoID" class="lblGeneral">Financiamento</p>
        <select id="selFinanciamentoID" name="selFinanciamentoID" class="fldGeneral"></select>
        <p id="lblQuantidade" name="lblQuantidade" class="lblGeneral">Quant</p>
        <input type="text" id="txtQuantidade" name="txtQuantidade" class="fldGeneral">
        <p id="lblMoedaConversaoID" name="lblMoedaConversaoID" class="lblGeneral">Moeda</p>
        <select id="selMoedaConversaoID" name="selMoedaConversaoID" class="fldGeneral" title="Moeda do Pre�o Convertido"></select>
        <p id="lblTransacao" name="lblTransacao" class="lblGeneral">Transacao</p>
        <select id="selTransacao" name="selTransacao" class="fldGeneral"></select>
        <p id="lblFinalidade" name="lblFinalidade" class="lblGeneral">Finalidade</p>
        <select id="selFinalidade" name="selFinalidade" class="fldGeneral"></select>
        <p id="lblCFOPID" name="lblCFOPID" class="lblGeneral">CFOP</p>
        <input type="text" id="txtCFOPID" name="txtCFOPID" class="fldGeneral">
        <p id="lblMoedaTabelaID" name="lblMoedaTabelaID" class="lblGeneral">Moeda</p>
        <input type="text" id="txtMoedaTabelaID" name="txtMoedaTabelaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="MoedaTabela" title="Moeda do Pre�o de Tabela">
        <p id="lblPrecoTabela" name="lblPrecoTabela" class="lblGeneral">Pre�o Tabela</p>
        <input type="text" id="txtPrecoTabela" name="txtPrecoTabela" class="fldGeneral">
        <p id="lblPrecoLista" name="lblPrecoLista" class="lblGeneral">Pre�o Lista</p>
        <input type="text" id="txtPrecoLista" name="txtPrecoLista" class="fldGeneral">
        <p id="lblMargemLista" name="lblMargemLista" class="lblGeneral" title="Margem Lista %">MC</p>
        <input type="text" id="txtMargemLista" name="txtMargemLista" class="fldGeneral" title="Margem Lista %">
        <p id="lblPrecoWeb" name="lblPrecoWeb" class="lblGeneral">Pre�o Web</p>
        <input type="text" id="txtPrecoWeb" name="txtPrecoWeb" class="fldGeneral">
        <p id="lblImposto" name="lblImposto" class="lblGeneral">Imp %</p>
        <input type="text" id="txtImposto" name="txtImposto" class="fldGeneral" title="Imposto">
        
        <p id="lblOrigem" name="lblOrigem" class="lblGeneral">Origem</p>
        <input type="text" id="txtOrigem" name="txtOrigem" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Origem">

        <input type="image" id="btnImpostos" name="btnImpostos" class="fldGeneral" title="Impostos" WIDTH="24" HEIGHT="23">
        <p id="lblPrazoMedioPagamento" name="lblPrazoMedioPagamento" class="lblGeneral">PMP</p>
        <input type="text" id="txtPrazoMedioPagamento" name="txtPrazoMedioPagamento" class="fldGeneral" title="Prazo M�dio de Pagamento">
        
        <p id="lblVariacao" name="lblVariacao" class="lblGeneral">Var %</p>
        <input type="text" id="txtVariacao" name="txtVariacao" class="fldGeneral" title="Varia��o (desconto ou acr�scimo)">        

        <p id="lblPrecoConvertido" name="lblPrecoConvertido" class="lblGeneral">Pre�o Convertido</p>
        <input type="text" id="txtPrecoConvertido" name="txtMoedaConversaoID" class="fldGeneral">
        
        <p id="lblPrecoRevenda" name="lblPrecoRevenda" class="lblGeneral">Pre�o Revenda</p>
        <input type="text" id="txtPrecoRevenda" name="txtPrecoRevenda" class="fldGeneral">
        
        <p id="lblPrecoFaturamento" name="lblPrecoFaturamento" class="lblGeneral">Pre�o Fatura</p>
        <input type="text" id="txtPrecoFaturamento" name="txtPrecoFaturamento" class="fldGeneral">
        
        <p id="lblComissaoInterna" name="lblComissaoInterna" class="lblGeneral" title="Valor comiss�o interna">CI</p>
        <input type="text" id="txtValorComissaoInterna" name="txtValorComissaoInterna" class="fldGeneral" title="Valor comiss�o interna">
        
        <p id="lblValorComissaoRevenda" name="lblValorComissaoRevenda" class="lblGeneral" title="Valor comiss�o revenda">CR</p>
        <input type="text" id="txtValorComissaoRevenda" name="txtValorComissaoRevenda" class="fldGeneral" title="Valor comiss�o revenda">
        
        <p id="lblPercentualSUP" name="lblPercentualSUP" class="lblGeneral">SUP %</p>
        <input type="text" id="txtPercentualSUP" name="txtPercentualSUP" class="fldGeneral">
        <p id="lblContribuicao" name="lblContribuicao" class="lblGeneral">Contribui��o</p>
        <input type="text" id="txtContribuicao" name="txtContribuicao" class="fldGeneral">
        <p id="lblMargemContribuicao" name="lblMargemContribuicao" class="lblGeneral">MC %</p>
        <input type="text" id="txtMargemContribuicao" name="txtMargemContribuicao" class="fldGeneral" title="Margem de Contribui��o">

        <p id="lblPrecoMinimo" name="lblPrecoMinimo" class="lblGeneral">Pre�o M�nimo</p>
        <input type="text" id="txtPrecoMinimo" name="txtPrecoMinimo" class="fldGeneral" title="Pre�o M�nimo">

        <p id="lblEstoqueDisponivel" name="lblEstoqueDisponivel" class="lblGeneral">Disp</p>
        <input type="text" id="txtEstoqueDisponivel" name="txtEstoqueDisponivel" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EstoqueDisponivel" title="Estoque Dispon�vel">
        <p id="lblEstoqueDisponivel1" name="lblEstoqueDisponivel1" class="lblGeneral">Disp 1</p>
        <input type="text" id="txtEstoqueDisponivel1" name="txtEstoqueDisponivel1" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EstoqueDisponivel1" title="Estoque Dispon�vel 1">
        <p id="lblEstoqueDisponivel2" name="lblEstoqueDisponivel2" class="lblGeneral">Disp 2</p>
        <input type="text" id="txtEstoqueDisponivel2" name="txtEstoqueDisponivel2" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EstoqueDisponivel2" title="Estoque Dispon�vel 2">
        
        <p id="lblNCM" name="lblNCM" class="lblGeneral">NCM</p>
        <input type="text" id="txtNCM" name="txtNCM" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="NCM">
    </div>

</body>

</html>
