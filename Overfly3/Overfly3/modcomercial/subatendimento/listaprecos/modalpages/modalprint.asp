
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<HTML id="modalprintHtml" name="modalprintHtml">
	<HEAD>
		<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcomercial/subatendimento/listaprecos/modalpages/modalprint.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf                
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_progressbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodalprint.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/subatendimento/listaprecos/modalpages/modalprint_listaprecos.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcomercial/subatendimento/listaprecos/modalpages/modalprint_fichatecnica.js" & Chr(34) & "></script>" & vbCrLf
%>
		<%
'Script de variaveis globais

Dim i, sCaller, nEmpresaID, sEmpresaFantasia, nContextoID, nUserID
Dim relatorioID, relatorio, EhDefault, relatorioFantasia, dirA1, dirA2, dirA1A2True

sCaller = ""
nEmpresaID = 0
sEmpresaFantasia = ""
nContextoID = 0
nUserID = 0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

'nEmpresaID - ID da empresa logada
For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

'sEmpresaFantasia - fantasia da empresa logada
For i = 1 To Request.QueryString("sEmpresaFantasia").Count    
    sEmpresaFantasia = Request.QueryString("sEmpresaFantasia")(i)
Next

'nContextoID - ID do contexto
For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

'nUserID - ID do usuario logado
For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & nEmpresaID & ";"
Response.Write vbcrlf

Response.Write "var glb_sEmpresaFantasia = " & Chr(39) & sEmpresaFantasia & Chr(39) & ";"
Response.Write vbcrlf

'Monta array dos relatorios
Response.Write "// Array dos relatorios, com seus ids, nomes"
Response.Write vbcrlf

Dim rsData, strSQL
Dim rsData1

If ( (sCaller) = "PL" ) Then
    i = 0
Else
    i = 1
End If        

strSQL = "SELECT DISTINCT d.RecursoID AS RelatorioID, d.Recurso AS Relatorio, " & _
         "c.EhDefault, d.RecursoFantasia AS RelatorioFantasia, c.Ordem, " & _
         "g.Alterar1 AS A1, g.Alterar2 AS A2 " & _
         "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), " & _
         "RelacoesPesRec e WITH(NOLOCK), RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " & _
         "WHERE a.ObjetoID= " & CStr(nContextoID) & " AND a.TipoRelacaoID=1 AND a.EstadoID=2 " & _
         "AND a.SujeitoID=b.RecursoID AND b.EstadoID=2 AND b.TipoRecursoID=4 " & _ 
         "AND b.Principal=1 AND b.RecursoID=c.ObjetoID AND c.EstadoID=2 " & _
         "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID " & _
         "AND d.EstadoID=2 AND d.TipoRecursoID=5 AND d.ClassificacaoID=14 AND d.EhDetalhe= " & CStr(i) & " " & _
         "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) WHERE a.RecursoID= " & CStr(nContextoID) & " )) " & _
         "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " & _
         "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " & _
         "AND e.SujeitoID IN ((SELECT " & CStr(nUserID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(nUserID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
         "AND e.RelacaoID=f.RelacaoID " & _
         "AND f.EmpresaID= " & CStr(nEmpresaID) & " AND g.ContextoID= " & CStr(nContextoID) & " " & _
         "AND c.ObjetoID=g.RecursoMaeID AND d.RecursoID=g.RecursoID " & _
         "AND (g.Consultar1=1 OR g.Consultar2=1) " & _
         "AND f.PerfilID=g.PerfilID AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " & _
         "ORDER BY c.Ordem"

Set rsData = Server.CreateObject("ADODB.Recordset")         

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write vbcrlf
Response.Write "var glb_arrayReports = new Array();"
Response.Write vbcrlf
Response.Write vbcrlf
'------------------
i = 0
Do While (Not rsData.EOF)
        
    relatorioID = rsData.Fields("RelatorioID").Value
    relatorio = rsData.Fields("Relatorio").Value
    EhDefault = rsData.Fields("EhDefault").Value
    relatorioFantasia = rsData.Fields("RelatorioFantasia").Value
    dirA1 = rsData.Fields("A1").Value
    dirA2 = rsData.Fields("A2").Value
    
    If ( dirA1 AND dirA2 ) Then
        dirA1A2True = TRUE        
    Else
        dirA1A2True = FALSE    
    End If
        
    rsData.MoveNext
        
    Do While (Not rsData.EOF)
        If ( CLng(relatorioID) = CLng(rsData.Fields("RelatorioID").Value) ) Then
            'dirX1 e dirX2 e o anterior sempre
            If ( (rsData.Fields("A1").Value) AND (rsData.Fields("A2").Value) ) Then
                dirA1A2True = TRUE        
            End If

            dirA1 = dirA1 OR rsData.Fields("A1").Value
            dirA2 = dirA2 OR rsData.Fields("A2").Value
            rsData.MoveNext
        Else
            Exit Do
        End If    
    Loop    
        
    If ( (dirA1) AND (dirA2) ) Then
        If ( NOT (dirA1A2True)  ) Then
            dirA1 = FALSE
            dirA2 = TRUE
        End If
    End If    

    Response.Write "glb_arrayReports[" & CStr(i) & "] = new Array("
    Response.Write Chr(39)
    Response.Write CStr(relatorio)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write CStr(relatorioID)
    Response.Write ","
    Response.Write LCase(CStr(EhDefault))
    Response.Write ","
    Response.Write Chr(39)
    Response.Write CStr(relatorioFantasia)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write LCase(CStr(dirA1))
    Response.Write ","
    Response.Write LCase(CStr(dirA2))
    Response.Write ");"
    i = i + 1

Loop

rsData.Close
Set rsData = Nothing

Response.Write vbcrlf

Response.Write "</script>"

%>
		<script ID="wndJSProc" LANGUAGE="javascript">
        <!--

        //-->
		</script>
	</HEAD>
	<body id="modalprintBody" name="modalprintBody" LANGUAGE="javascript" onload="return window_onload()">
		<!-- dso usado para envia e-mails -->

		
		<p id="lblReports" name="lblReports" class="paraNormal"></p>
		<select id="selReports" name="selReports" class="fldGeneral">
		</select>
		<!-- Divs dos relatorios do Lista de Precos -->
		<!-- Cria o Div para o relat�rio Lista de Preco -->
		<div id="divListaPreco" name="divListaPreco" class="divGeneral">
			<p id="lblMarca" name="lblMarca" class="lblGeneral">Marca</p>
			<select id="selMarca" name="selMarca" class="fldGeneral">
				<%
	'Marcas de produtos ativos da empresa logada 
	Set rsData1 = Server.CreateObject("ADODB.Recordset")         

	strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName " & _
			"UNION ALL " & _ 
			"SELECT DISTINCT c.ConceitoID AS fldID, c.Conceito AS fldName " & _
			"FROM RelacoesPesCon a WITH(NOLOCK) " & _
					"INNER JOIN Conceitos b WITH(NOLOCK) ON a.ObjetoID = b.ConceitoID " & _
					"INNER JOIN Conceitos c WITH(NOLOCK) ON b.MarcaID = c.ConceitoID " & _
			"WHERE a.TipoRelacaoID = 61 AND a.EstadoID NOT IN (4,5) AND a.SujeitoID = " & CStr(nEmpresaID) & " " & _
			"ORDER BY fldName"

	rsData1.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData1.BOF AND rsData1.EOF) ) Then

		While Not (rsData1.EOF)

			Response.Write( "<option value='" & rsData1.Fields("fldID").Value & "'>" )
			Response.Write( rsData1.Fields("fldName").Value & "</option>" )

			rsData1.MoveNext()

		WEnd

	End If

	rsData1.Close
	
%>
			</select>
			<p id="lblEstoqueDisponivel" name="lblEstoqueDisponivel" title="Somente produtos com estoque dispon�vel?"
				class="lblGeneral">Est</p>
			<input type="checkbox" id="chkEstoqueDisponivel" name="chkEstoqueDisponivel" title="Somente produtos com estoque dispon�vel?"
				class="fldGeneral"></input>
			<p id="lblLista" name="lblLista" class="lblGeneral">Lista</p>
			<select id="selLista" name="selLista" class="fldGeneral">
				<OPTION value="0" selected>0</OPTION>
				<OPTION value="1">1</OPTION>
				<OPTION value="2">2</OPTION>
				<OPTION value="3">3</OPTION>
				<OPTION value="4">4</OPTION>
				<OPTION value="5">5</OPTION>
			</select>
			<p id="lblQuantidade" name="lblQuantidade" class="lblGeneral">Quant</p>
			<input type="text" id="txtQuantidade" name="txtQuantidade" value="1" class="fldGeneral"></input>
			<p id="lblUF" name="lblUF" class="lblGeneral">UF</p>
			<select id="selUF" name="selUF" class="fldGeneral">
				<%
	Set rsData1 = Server.CreateObject("ADODB.Recordset")         

	'UFs do pais da empresa logada    
	strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName " & _
			"UNION ALL " & _ 
		"SELECT DISTINCT UFs.LocalidadeID AS fldID, UFs.CodigoLocalidade2 AS fldName " & _
			"FROM Conceitos Impostos WITH(NOLOCK) " & _ 
					"INNER JOIN Conceitos_Aliquotas Aliquotas WITH(NOLOCK) ON Impostos.ConceitoID = Aliquotas.ImpostoID " & _ 
					"INNER JOIN Pessoas_Enderecos EmpresaEndereco WITH(NOLOCK) ON EmpresaEndereco.UFID = Aliquotas.LocalidadeOrigemID " & _ 
					"INNER JOIN Localidades UFs WITH(NOLOCK) ON LocalidadeDestinoID = UFs.LocalidadeID " & _ 
			"WHERE Impostos.TipoConceitoID = 306 " & _ 
					"AND Impostos.IncidePreco = 1 " & _ 
					"AND Impostos.InclusoPreco = 1 " & _ 
					"AND Impostos.AlteraAliquota = 1 " & _ 
					"AND Impostos.DestacaNota = 1 " & _ 
					"AND EmpresaEndereco.PessoaID =  "& CStr(nEmpresaID) & " " & _
					"ORDER BY fldName"

	rsData1.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData1.BOF AND rsData1.EOF) ) Then

		While Not (rsData1.EOF)

			Response.Write( "<option value='" & rsData1.Fields("fldID").Value & "'>" )
			Response.Write( rsData1.Fields("fldName").Value & "</option>" )

			rsData1.MoveNext()

		WEnd

	End If

	rsData1.Close
	
%>
			</select>
			<p id="lblIncideImposto" name="lblIncideImposto" title="Incide imposto (ICMS/Tax) sobre a nota?"
				class="lblGeneral">Inc</p>
			<input type="checkbox" id="chkIncideImposto" name="chkIncideImposto" title="Incide imposto (ICMS/Tax) sobre a nota?"
				class="fldGeneral"></input>
			<p id="lblFinanciamento" name="lblFinanciamento" class="lblGeneral">Financiamento</p>
			<select id="selFinanciamento" name="selFinanciamento" class="fldGeneral">
				<%
	Set rsData1 = Server.CreateObject("ADODB.Recordset")         

	'Financiamento da empresa logada
	strSQL = "SELECT c.FinanciamentoID as fldID, " & _
			"(CONVERT(VARCHAR(50), CONVERT(INT, ROUND(dbo.fn_Financiamento_PMP(c.FinanciamentoID,NULL,NULL,NULL),2))) + SPACE(1) + CHAR(40) + c.Financiamento + CHAR(41)) AS fldName " & _
		"FROM	RelacoesPesRec a WITH(NOLOCK) " & _
			"INNER JOIN RelacoesPesRec_FinanciamentosPadrao b WITH(NOLOCK) ON a.RelacaoID = b.RelacaoID " & _  
			"INNER JOIN FinanciamentosPadrao c WITH(NOLOCK) ON b.FinanciamentoID = c.FinanciamentoID " & _
		"WHERE (a.SujeitoID =  " & CStr(nEmpresaID) & " " & _
			"AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 AND b.EstadoID = 2 AND c.FinanciamentoID <> 1)" 

	rsData1.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData1.BOF AND rsData1.EOF) ) Then

		While Not (rsData1.EOF)

			Response.Write( "<option value='" & rsData1.Fields("fldID").Value & "'>" )
			Response.Write( rsData1.Fields("fldName").Value & "</option>" )

			rsData1.MoveNext()

		WEnd

	End If

	rsData1.Close
	
%>
			</select>
			<p id="lblHomologacao" name="lblHomologacao" title="Homologa��o" class="lblGeneral">H</p>
			<input type="checkbox" id="chkHomologacao" name="chkHomologacao" title="Homologa��o" value="11"
				class="fldGeneral"></input>
			<p id="lblPreLancamento" name="lblPreLancamento" title="Pr�-Lan�amento" class="lblGeneral">R</p>
			<input type="checkbox" id="chkPreLancamento" name="chkPreLancamento" title="Pr�-Lan�amento"
				value="12" class="fldGeneral"></input>
			<p id="lblLancamento" name="lblLancamento" title="Lan�amento" class="lblGeneral">L</p>
			<input type="checkbox" id="chkLancamento" name="chkLancamento" title="Lan�amento" value="13"
				class="fldGeneral"></input>
			<p id="lblAtivo" name="lblAtivo" title="Ativo" class="lblGeneral">A</p>
			<input type="checkbox" id="chkAtivo" name="chkAtivo" title="Ativo" value="2" class="fldGeneral"></input>
			<p id="lblPromocao" name="lblPromocao" title="Promo��o" class="lblGeneral">P</p>
			<input type="checkbox" id="chkPromocao" name="chkPromocao" title="Promo��o" value="14"
				class="fldGeneral"></input>
			<p id="lblObsoleto" name="lblObsoleto" title="Obsoleto" class="lblGeneral">O</p>
			<input type="checkbox" id="chkObsoleto" name="chkObsoleto" title="Obsoleto" value="15"
				class="fldGeneral"></input>
			<p id="lblSuspenso" name="lblSuspenso" title="Suspenso" class="lblGeneral">S</p>
			<input type="checkbox" id="chkSuspenso" name="chkSuspenso" title="Suspenso" value="3" class="fldGeneral"></input>
			<p id="lblMoeda" name="lblMoeda" class="lblGeneral">Moeda</p>
			<select id="selMoeda" name="selMoeda" class="fldGeneral">
				<%
	Set rsData1 = Server.CreateObject("ADODB.Recordset")         

	'--- Moeda de Faturamento da Empresa 
	strSQL = "SELECT c.ConceitoID AS fldID, c.SimboloMoeda AS fldName " & _
         "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " & _
         "WHERE a.ObjetoID=999 AND a.TipoRelacaoID=12 AND a.SujeitoID=" & nEmpresaID & " " & _
         "AND a.RelacaoID=b.RelacaoID AND b.Faturamento=1 AND b.MoedaID=c.ConceitoID"


	rsData1.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData1.BOF AND rsData1.EOF) ) Then

		While Not (rsData1.EOF)

			Response.Write( "<option value='" & rsData1.Fields("fldID").Value & "'>" )
			Response.Write( rsData1.Fields("fldName").Value & "</option>" )

			rsData1.MoveNext()

		WEnd

	End If

	rsData1.Close
	Set rsData1 = Nothing
%>
			</select>
			<p id="lblMargemPublicacao" name="lblMargemPublicacao" title="Margem de publica��o?"
				class="lblGeneral">MP</p>
			<input type="checkbox" id="chkMargemPublicacao" name="chkMargemPublicacao" title="Margem de publica��o?"
				class="fldGeneral"></input>
			<p id="lblFiltro" name="lblFiltro" class="lblGeneral">Filtro</p>
			<input type="text" id="txtFiltro" name="txtFiltro" class="fldGeneral"></input>
		</div>
		<!-- Final de Divs dos relatorios do Lista de Precos  -->
		<input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)"
			class="btns"> <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript"
			onclick="return btn_onclick(this)" class="btns">
		<div id="divMod01" name="divMod01" class="divGeneral">
			<p id="paraMod01" name="paraMod01" class="paraNormal"></p>
		</div>

        <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>
	</body>
</HTML>
