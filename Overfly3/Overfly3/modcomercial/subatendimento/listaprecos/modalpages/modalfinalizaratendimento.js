/********************************************************************
modalfinalizaratendimento.js

Library javascript para o modalfinalizaratendimento.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_sResultado = '';
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_ResultadoID = 0;
var glb_MeioAtendimentoID = 0;


// Gravacao de dados do grid .RDS
var dsoAgendarHorario = new CDatatransport('dsoAgendarHorario');
var dsoGravarAgendamento = new CDatatransport('dsoGravarAgendamento');


// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalfinalizaratendimentoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	glb_bFirstFill = true;

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    var nTipoAtendimentoID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nTipoAtendimentoID');
    var sTipoAtendimento = '';    

    for (i=0; i<glb_aTiposAtendimento.length; i++)
    {
        if (glb_aTiposAtendimento[i][0] == nTipoAtendimentoID)
            sTipoAtendimento = glb_aTiposAtendimento[i][1];
    }
    
	secText(translateTerm('Finalizar atendimento ' + sTipoAtendimento, null), 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Ajusta o divFinalizarAtendimento
    adjustElementsInForm([['lblAdiarDias', 'txtAdiarDias', 3, 2, 383],
		['lblAdiarHoras', 'txtAdiarHoras', 3, 2, -55, 0],
		['lblAdiarMinutos', 'txtAdiarMinutos', 3, 2],
		['btnCalcularAgenda', 'btn', 78, 2, 10, 0],
		['lblAgendar','txtAgendar', 19, 3, 383, 0],
		['lblObservacoes','txtObservacoes',25,4, 0, 10]], null, null, true);
	btnCalcularAgenda.style.width = 46;
	
	txtObservacoes.style.width = 535;
	txtObservacoes.style.height = 150;
	txtObservacoes.maxLength = 1000;
	
    var sReturn = String.fromCharCode(13) + String.fromCharCode(10);
	
	btnOK.style.top = txtObservacoes.offsetTop + txtObservacoes.offsetHeight + 10;
	btnOK.style.left = ((txtObservacoes.offsetLeft + txtObservacoes.offsetWidth) /2) - btnOK.offsetWidth - 5;
	btnOK.value = 'OK';

	btnCanc.style.top = txtObservacoes.offsetTop + txtObservacoes.offsetHeight + 10;
	btnCanc.style.left = btnOK.offsetLeft + btnOK.offsetWidth + 10;
	btnCanc.value = 'Cancelar';
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // O usuario clicou o botao OK
    if (ctl.id == btnOK.id )
    {
        finalizarAtendimento();
	}
    // O usuario clicou o botao Agendar
    if (ctl.id == btnCalcularAgenda.id )
    {
        agendarHorario();
	}
    // O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    showExtFrame(window, true);
	window.focus();
}

function rdMeioAntedimentoOnclick(ctrl)
{
	eval("rd" + ctrl.registroID + ".checked=true;");
	glb_MeioAtendimentoID = ctrl.registroID;
}

function rdResultadoAtendimentoOnclick(ctrl)
{
    var nDias = 0;
    var nHoras = 0;
    var nMinutos = 0;
    var nRadioValue = ctrl.numero;
    
    eval('rd' + ctrl.registroID + '.checked=true;');
    glb_ResultadoID = ctrl.registroID;
    
    if(!isNaN(nRadioValue))
    {
        nDias = parseInt((nRadioValue/24), 10);
        nHoras = parseInt((nRadioValue - (nDias * 24)), 10);
        nMinutos = parseInt(roundNumber(((nRadioValue - (nDias * 24) - nHoras) * 60), 0), 10);
    }
                
    if (nDias > 0)
        txtAdiarDias.value = nDias;
    else
        txtAdiarDias.value = '';
    
    if (nHoras > 0)
        txtAdiarHoras.value = nHoras;
    else
        txtAdiarHoras.value = '';
                
    if (nMinutos > 0)
        txtAdiarMinutos.value = nMinutos;
    else
        txtAdiarMinutos.value = '';
        
    agendarHorario(); 
}

function agendarHorario()
{
    try
    {
        if (glb_ResultadoID == 255)
        {
            lockControlsInModalWin(false);
            txtAgendar.value = '';
            return;
        }
    }
    catch(e)
    {
        ;
    }
        
    lockControlsInModalWin(true);
    
    var sDias = (txtAdiarDias.value == '' ? '0' : txtAdiarDias.value);
    var sHoras = (txtAdiarHoras.value == '' ? '0' : txtAdiarHoras.value);
    var sMinutos = (txtAdiarMinutos.value == '' ? '0' : txtAdiarMinutos.value);

    setConnection(dsoAgendarHorario);

    dsoAgendarHorario.SQL = 'SELECT CONVERT(VARCHAR(10),(dbo.fn_Data_ProjetaHora(GETDATE(), ' + sDias + ', ' + sHoras + ', ' + sMinutos + ', 0, ' + glb_aEmpresaData[0] + ')),' + DATE_SQL_PARAM + ') + \' \' +   ' +
	                            'CONVERT(VARCHAR(5),(dbo.fn_Data_ProjetaHora(GETDATE(), ' + sDias + ', ' + sHoras + ', ' + sMinutos + ', 0, ' + glb_aEmpresaData[0] + ')),108) AS Data';
    
    dsoAgendarHorario.ondatasetcomplete = agendarHorario_DSC;
    dsoAgendarHorario.refresh();
}

function agendarHorario_DSC()
{
    if ( !((dsoAgendarHorario.recordset.BOF) || (dsoAgendarHorario.recordset.EOF)) )
    {
        if ((dsoAgendarHorario.recordset['Data'].value != null) && (dsoAgendarHorario.recordset['Data'].value) != '')
            txtAgendar.value = dsoAgendarHorario.recordset['Data'].value;            
    }
    
    lockControlsInModalWin(false);
}

function verificaData(sData)
{
	var sData = trimStr(sData);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(sData);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        lockControlsInModalWin(false);
		return false;
	}
	return true;
}


function finalizarAtendimento()
{
    var sAlert = '';
    var sdtAgendamento = '';    
    var sMeioAtendimentoID = glb_MeioAtendimentoID;
    var sResultadoID = glb_ResultadoID;
    var sObservacoes = trimStr(txtObservacoes.value);
    sObservacoes = replaceStr(sObservacoes, '\'', '');
    sObservacoes = replaceStr(sObservacoes, '"', '');
    
    // Finalizar atendimento
    if (!verificaData(txtAgendar.value))
	    return null;

    sdtAgendamento = normalizeDate_DateTime(txtAgendar.value, true);
    sdtAgendamento = (sdtAgendamento == null ? '' : sdtAgendamento);

    if ((sMeioAtendimentoID<=0) || (sMeioAtendimentoID==null) || (sMeioAtendimentoID==''))
        sAlert += '\nSelecione o meio de atendimento';

    if ((sResultadoID<=0) || (sResultadoID==null) || (sResultadoID==''))
        sAlert += '\nSelecione o resultado do atendimento';

    if ((glb_ResultadoID != null) && (glb_ResultadoID != 0) && (glb_ResultadoID != 255))
    {
        if ((sdtAgendamento=='')||(sdtAgendamento==null))
            sAlert += '\nAgende a data do pr�ximo atendimento';

        if ((sObservacoes == '') || (sObservacoes == null) || (sObservacoes.length < 2))
            sAlert += '\nRegistre observa��es do atendimento';
    }

    if (sAlert.length > 0)
    {
        if (window.top.overflyGen.Alert(sAlert) == 0 )
		    return null;

        //txtObservacoes.focus();
        			
        return null;	
    }
    
    var sProdutosCotados = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_sClipBoardBuffer');
    lockControlsInModalWin(true);
    
    strPars = '?nStatus=' + escape('1');//--0-Iniciar, 1-Finalizar
    strPars += '&nMeioAtendimentoID=' + escape(sMeioAtendimentoID);
    strPars += '&sResultadoID=' + escape(sResultadoID);
    strPars += '&sObservacoes=' + escape(sObservacoes);
    strPars += '&sdtAgendamento=' + escape(sdtAgendamento);	
    strPars += '&nPesAtendimentoID=' + escape(glb_nPesAtendimentoID);	
    strPars += '&sProdutos=' + escape(sProdutosCotados);	

    try
    {
        dsoGravarAgendamento.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/gravaragendamento.aspx' + strPars;
        dsoGravarAgendamento.ondatasetcomplete = finalizarAtendimento_DSC;
        dsoGravarAgendamento.refresh();
    }
    catch(e)
	{
	    if ( window.top.overflyGen.Alert('N�o foi poss�vel finalizar o atendimento, tente novamente.') == 0 )
			return null;
	}
}

function finalizarAtendimento_DSC() {
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
}
