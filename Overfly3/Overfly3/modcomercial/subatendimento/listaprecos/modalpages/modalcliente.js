/********************************************************************
modalcliente.js

Library javascript para o modalcliente.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var modPesqCli_TimerHandler = null;

var dsoPesq = new CDatatransport('dsoPesq');
var dsoAditionalParameters = new CDatatransport('dsoAditionalParameters');


// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modalclienteBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtCliente').disabled == false )
        txtCliente.focus();

    // Escreve na barra de status
    //writeInStatusBar('child', 'cellMode', 'Fornecedores');

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Cliente', 1);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    
    // ajusta o divCidade
    elem = window.document.getElementById('divCidade');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        temp = parseInt(width);
        height = 40;
        
    }

    adjustElementsInForm([['lblMeusClientes', 'chkMeusClientes', 3, 1, -10],
                          ['lblChavePesquisa', 'selChavePesquisa', 12, 1, -10],
                          ['lblCliente', 'txtCliente', 50, 1, -2]],
                          null,null,true);

    var aOptions = [[0,'ID'],
                    [1,'Nome'],
                    [2,'Fantasia'],
                    [3,'Documento']];

    for (i=0; i<aOptions.length; i++)
    {
        var oOption = document.createElement("OPTION");
        oOption.value = aOptions[i][0];
        oOption.text = aOptions[i][1];
        selChavePesquisa.add(oOption);
    }
    
    selChavePesquisa.selectedIndex = 1;

    lblMeusClientes.onclick = invertChkBox;
    txtCliente.onkeypress = txtCliente_onKeyPress;
    
    // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');
    elem.disabled = true;

    with (elem.style)
    {
        top = parseInt(document.getElementById('txtCliente').style.top);
        left = parseInt(document.getElementById('txtCliente').style.left) + parseInt(document.getElementById('txtCliente').style.width) + ELEM_GAP;
        width = 80;
        height = 24;
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divCidade').style.top) + parseInt(document.getElementById('divCidade').style.height) + ELEM_GAP;
        width = temp + 321;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);

    headerGrid(fg,['Nome',
                   'ID',
                   'Fantasia',
                   'Documento',
                   'Cidade',
                   'UF',
                   'Pa�s'], [2]);
    
    fg.Redraw = 2;
}

function txtCliente_onKeyPress()
{
    if ( event.keyCode == 13 )
        modPesqCli_TimerHandler = window.setInterval('btnFindPesquisa_onclick()', 30, 'JavaScript');
}

function txtCliente_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    if ( modPesqCli_TimerHandler != null )
    {
        window.clearInterval(modPesqCli_TimerHandler);
        modPesqCli_TimerHandler = null;
    }
    if (selChavePesquisa.selectedIndex == 0) 
    {
        if (isNaN(txtCliente.value)) 
        {
            window.top.overflyGen.Alert('A chave de pesquisa "ID" aceita apenas n�mero.');
            return true;
        }
    }
    txtCliente.value = trimStr(txtCliente.value);
    
    changeBtnState(txtCliente.value);

    if (btnFindPesquisa.disabled)
        return;
    
    startPesq(txtCliente.value);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModCliKeyPress(KeyAscii)
{
    if ( fg.Row < 1 )
        return true;
 
    if ( (KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false) )
        btn_onclick(btnOK);
 
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModCliDblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
	var strSQL = '';
	var nPessoaID = 0;
	var nFinanciamentoID = 0;
	var nObjetoID = 0;

    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
		nPessoaID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID'));
		nFinanciamentoID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FinanciamentoID'));
		nObjetoID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ObjetoID'));

		strSQL = 'SELECT dbo.fn_Empresa_Sistema(' + nPessoaID + ') AS EmpresaSistema, ' +
			'dbo.fn_Financiamento_PMP(' + nFinanciamentoID + ',NULL,NULL,NULL) AS PMP, ' +
			'dbo.fn_RelacoesPessoas_FinanciamentoPadrao(' + nPessoaID + ', ' + nObjetoID + ', 1) AS FinanciamentoPadrao,' +
			'ISNULL(dbo.fn_RelPessoa_Classificacao(' + nObjetoID + ', ' + nPessoaID + ', 0, GETDATE(),1), SPACE(0)) AS ClassificacaoInterna,' +
			'ISNULL(dbo.fn_RelPessoa_Classificacao(' + nObjetoID + ', ' + nPessoaID + ', 1, GETDATE(),1), SPACE(0)) AS ClassificacaoExterna';

		setConnection(dsoAditionalParameters);
		dsoAditionalParameters.SQL = strSQL;
		dsoAditionalParameters.ondatasetcomplete = btnOK_DSC;
		dsoAditionalParameters.refresh();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    

}

function btnOK_DSC() 
{
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, new Array(
                    fg.TextMatrix(fg.Row, 2), 
                    fg.TextMatrix(fg.Row, 1), 
                    fg.TextMatrix(fg.Row, 7),
                    dsoAditionalParameters.recordset['EmpresaSistema'].value,
                    fg.TextMatrix(fg.Row, 8),
                    dsoAditionalParameters.recordset['PMP'].value,
                    fg.TextMatrix(fg.Row, 10),
                    dsoAditionalParameters.recordset['FinanciamentoPadrao'].value,
                    dsoAditionalParameters.recordset['ClassificacaoInterna'].value,
                    dsoAditionalParameters.recordset['ClassificacaoExterna'].value));
}

var iniSQL;
var fimSQL;

function startPesq(strPesquisa)
{
	lockControlsInModalWin(true);
	
    var strPars =
		"?EmpresaID=" + glb_nEmpresaID + 
		"&UserID=" + glb_USERID  + 
		"&MeusClientes=" + (chkMeusClientes.checked ? 1 : 0) + 
		"&SelChavePesquisa=" + selChavePesquisa.value +
		"&DadoPesquisa=" + strPesquisa;
		
    setConnection(dsoPesq);

	dsoPesq.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/SelecaoCliente.aspx' + strPars;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.refresh();
}

function dsopesq_DSC() {
    startGridInterface(fg);
    fg.FrozenCols = 0;
    headerGrid(fg,['Nome',
                   'ID',
                   'Fantasia',
                   'Documento',
                   'Cidade',
                   'UF',
                   'Pa�s',
                   'UFID',
                   'ListaPreco',
                   'PessoaID',
                   'FinanciamentoID',
                   'ObjetoID'], [2,7,8,9,10,11]);
                  
    fillGridMask(fg,dsoPesq,['fldName',
                             'fldID',
                             'Fantasia',
                             'Documento',
                             'Cidade',
                             'UF',
                             'Pais',
                             'UFID',
                             'ListaPreco',
                             'PessoaID',
							 'FinanciamentoID',
							 'ObjetoID'],['','','','','','','','','','','','']);

    fg.FrozenCols = 1;

    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    
        btnOK.disabled = false;
    }    
    else
        btnOK.disabled = true;    
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    return true;
}
