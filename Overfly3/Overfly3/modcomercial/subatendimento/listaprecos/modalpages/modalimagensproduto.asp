<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'Script de variaveis globais

Dim i, strSQL, EmpresaID, ProdutoID, ArgumentosVenda
Dim rsData
EmpresaID = 0
ProdutoID = 0
strSQL = ""
ArgumentosVenda = ""

For i = 1 To Request.QueryString("EmpresaID").Count
    EmpresaID = Request.QueryString("EmpresaID")(i)
Next

For i = 1 To Request.QueryString("ProdutoID").Count
    ProdutoID = Request.QueryString("ProdutoID")(i)
Next

strSQL = "SELECT ISNULL(dbo.fn_Produto_ArgumentosVenda(a.ObjetoID, -a.SujeitoID), SPACE(0)) AS ArgumentosVenda " & _
	"FROM RelacoesPesCon a WITH(NOLOCK) " & _
	"WHERE a.SujeitoID=" & CStr(EmpresaID) & " AND a.ObjetoID=" & CStr(ProdutoID) & " AND a.TipoRelacaoID=61 "

Set rsData = Server.CreateObject("ADODB.Recordset")         

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

If (Not (rsData.BOF AND rsData.EOF)) Then

	ArgumentosVenda = rsData.Fields("ArgumentosVenda").Value

End If

rsData.Close()
Set rsData = Nothing

%>

<HTML>
<HEAD>
<TITLE></TITLE>
<%
Response.Write vbcrlf
Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write "<!--" & vbcrlf

Response.Write "var glb_URLRoot = " & Chr(39) & pagesURLRoot & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "//-->" & vbcrlf
Response.Write "</script>"
%>

<script ID="JSScripts" LANGUAGE="javascript">
<!--

function img_onload(imgTag)
{
	imgTag.style.visibility = 'inherit';
}
 
function img_error(imgTag)
{
	imgTag.style.visibility = 'hidden';
}

function imgMainLoad(imgTag)
{
	IMGMain.style.visibility = 'inherit';
	pMensagem.style.visibility = 'hidden';
	pMensagem.innerText = ""; 
}

function imgMainError(imgTag)
{
	IMGMain.style.visibility = 'hidden';
	pMensagem.style.visibility = 'inherit';
	pMensagem.innerText = "Imagem n�o dispon�vel."; 
}

function loadMainImage(produtoID, ordem)
{
	IMGMain.src = glb_URLRoot + '/serversidegen/imageblob.asp?nFormID=2110&nSubFormID=21000&nRegistroID=' + produtoID + 
		'&nTamanho=4&nOrdem=' + ordem;
}

//-->
</script>

</HEAD>

<BODY>

<TABLE> 
<TR>
	<TD vAlign="top">

		<TABLE>
		<TR>
			<TD COLSPAN=5>
				<IMG onerror='return imgMainError(this)' onload='imgMainLoad(this)' ID="IMGMain" SRC='<%Response.Write(pagesURLRoot)%>/serversidegen/imageblob.asp?nFormID=2110&nSubFormID=21000&nRegistroID=<% Response.Write(CStr(ProdutoID)) %>&nTamanho=4&nOrdem=1' STYLE='width:auto;height:560px'>
				<p id="pMensagem"></p>				
			</TD>
		</TR>
		<TR vAlign="bottom">
			<TD vAlign="bottom">
				<IMG onclick="JavaScript:loadMainImage(<% Response.Write(CStr(ProdutoID)) %>, 1)" border=none ID="IMG1" STYLE='width:69px;height:69px' onload='img_onload(this)' onerror='return img_error(this)' 
					SRC='<%Response.Write(pagesURLRoot)%>/serversidegen/imageblob.asp?nFormID=2110&nSubFormID=21000&nRegistroID=<% Response.Write(CStr(ProdutoID)) %>&nTamanho=4&nOrdem=1'>
			</TD>
			<TD vAlign="bottom">
				<IMG onclick="JavaScript:loadMainImage(<% Response.Write(CStr(ProdutoID)) %>, 2)" border=none  ID="IMG2" STYLE='width:69px;height:69px' onload='img_onload(this)' onerror='return img_error(this)' 
					SRC='<%Response.Write(pagesURLRoot)%>/serversidegen/imageblob.asp?nFormID=2110&nSubFormID=21000&nRegistroID=<% Response.Write(CStr(ProdutoID)) %>&nTamanho=4&nOrdem=2'>
			</TD>
			<TD vAlign="bottom">
				<IMG onclick="JavaScript:loadMainImage(<% Response.Write(CStr(ProdutoID)) %>, 3)" border=none  ID="IMG3" STYLE='width:69px;height:69px' onload='img_onload(this)' onerror='return img_error(this)' 
					SRC='<%Response.Write(pagesURLRoot)%>/serversidegen/imageblob.asp?nFormID=2110&nSubFormID=21000&nRegistroID=<% Response.Write(CStr(ProdutoID)) %>&nTamanho=4&nOrdem=3'>
			</TD>
			<TD vAlign="bottom">
				<IMG onclick="JavaScript:loadMainImage(<% Response.Write(CStr(ProdutoID)) %>, 4)" border=none  ID="IMG4" STYLE='width:69px;height:69px' onload='img_onload(this)' onerror='return img_error(this)' 
					SRC='<%Response.Write(pagesURLRoot)%>/serversidegen/imageblob.asp?nFormID=2110&nSubFormID=21000&nRegistroID=<% Response.Write(CStr(ProdutoID)) %>&nTamanho=4&nOrdem=4'>
			</TD>
			<TD vAlign="bottom">
				<IMG onclick="JavaScript:loadMainImage(<% Response.Write(CStr(ProdutoID)) %>, 5)" border=none ID="IMG5" STYLE='width:69px;height:69px' onload='img_onload(this)' onerror='return img_error(this)' 
					SRC='<%Response.Write(pagesURLRoot)%>/serversidegen/imageblob.asp?nFormID=2110&nSubFormID=21000&nRegistroID=<% Response.Write(CStr(ProdutoID)) %>&nTamanho=4&nOrdem=5'>
			</TD>
		</TR>
		</TABLE>
	</TD>

	<TD vAlign="top">
		<br>
		<FONT STYLE='FONT-SIZE:8pt;FONT-FAMILY:ARIAL'><% Response.Write(ArgumentosVenda) %></FONT>
	</TD>
</TR>
</TABLE>

</BODY>
</HTML>
