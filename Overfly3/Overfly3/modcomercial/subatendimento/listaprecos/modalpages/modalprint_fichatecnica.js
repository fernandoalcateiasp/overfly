/********************************************************************
modalprint_fichatecnica.js

Este arquivo contem somente a funcao que imprime o relatorio de Ficha
Tecnica, esta funcao esta neste arquivo separado porque o conceitos usa
este arquivo para imprimir o mesmo relatorio.
********************************************************************/

function fichaTecnica() {
    var nEmpresaData = getCurrEmpresaData();
    var nConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ConceitoID' + '\'' + '].value');
    var nIdiomaDeID = nEmpresaData[7];
    var nIdiomaParaID = nEmpresaData[8];
    var formato = 1;

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + nEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&formato=" + formato +
                        "&nConceitoID=" + nConceitoID + "&nIdiomaDeID=" + nIdiomaDeID + "&nIdiomaParaID=" + nIdiomaParaID;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/Reports_fichatecnica.aspx?' + strParameters;
}
