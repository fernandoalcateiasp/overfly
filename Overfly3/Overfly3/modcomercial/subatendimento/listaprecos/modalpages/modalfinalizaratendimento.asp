
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalfinalizaratendimentoHtml" name="modalfinalizaratendimentoHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL='stylesheet' HREF='" & pagesURLRoot & "/modcomercial/subatendimento/listaprecos/modalpages/modalfinalizaratendimento.css' type='text/css'>" & vbCrLf
    
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_sysbase.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_constants.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_htmlbase.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_strings.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_interface.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_statusbar.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_fastbuttonsbar.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_controlsbar.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_modalwin.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/formlibs/js_gridsex.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_rdsfacil.js'></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/modalgen/commonmodal.js'></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/modcomercial/subatendimento/listaprecos/modalpages/modalfinalizaratendimento.js'></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID='serverSideVars' LANGUAGE='javascript'>"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, sCurrDateFormat, nContextoID, nPesAtendimentoID

sCaller = ""
nContextoID = 0
nPesAtendimentoID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

For i = 1 To Request.QueryString("nPesAtendimentoID").Count    
    nPesAtendimentoID = Request.QueryString("nPesAtendimentoID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nPesAtendimentoID= " & CStr(nPesAtendimentoID) & ";"
Response.Write vbcrlf

If (nContextoID = 9111) Then
	Response.Write "var glb_nTipoFinanceiroID = 1001;"
Else
	Response.Write "var glb_nTipoFinanceiroID = 1002;"
End If

'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If


Dim rsData, strSQL
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName " & _
    "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _
    "WHERE (a.EstadoID = 2 AND a.TipoID = 51) " & _
    "ORDER BY a.Ordem "

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write("var glb_aTiposAtendimento = new Array();" & vbCrLf)

If (Not (rsData.BOF AND rsData.EOF) ) Then

	While Not (rsData.EOF)

        Response.Write("glb_aTiposAtendimento[glb_aTiposAtendimento.length] = new Array(" & _
            CStr(rsData.Fields("fldID").Value) & ", " & _
            "'" & CStr(rsData.Fields("fldName").Value) & "');" & vbCrLf)
		rsData.MoveNext()

	WEnd

End If

rsData.Close

Response.Write "</script>"
Response.Write vbcrlf
%>

<script id="vbFunctions" language="vbscript" type="text/vbscript">
<!--

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

//-->
</script>

</head>

<body id="modalfinalizaratendimentoBody" name="modalfinalizaratendimentoBody" language="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <!-- Div Controles -->
    <div id="divFinalizarAtendimento" name="divFinalizarAtendimento" class="divGeneral" style="top:-25px;">
        <p class="lblGeneral" id="lblMeioAtendimento" style="width:120px; height:17px; top:55px; left:10px;">
			<b>Meio de atendimento</b>
		</p>

<%
	Dim height
	Dim top
	Dim left
	
	height =  17
	top    =  55
	left   =  10

    strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName " & _
		"FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
		"WHERE TipoID = 52 AND EstadoID=2 " & _
		"ORDER BY Ordem"

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData.EOF)
        Response.Write "<input type='radio' " & _
            "id='rd" & rsData.Fields("fldID").Value & "' " & _
            "registroID='" & rsData.Fields("fldID").Value & "' " & _
            "onclick='return rdMeioAntedimentoOnclick(this)' " & _
			"name='rdMeioAtendimento' value='" & rsData.Fields("fldName").Value & "' " & _
			"class='fldGeneral' " & _
			"style='width:17px; height:17px; top:" & CStr(top+height) & "px; left:" & CStr(left) & "px;'/>" & vbcrlf
			
        Response.Write "<p class='lblGeneral' " & _
        "id='lbl" & rsData.Fields("fldID").Value & "' " & _
        "registroID='" & rsData.Fields("fldID").Value & "' " & _
        "meioAtendimentoID='" & rsData.Fields("fldID").Value & "' " & _
        "onclick='return rdMeioAntedimentoOnclick(this)' " & _
        "style='width:120px; height:17px; top:" & CStr(top+height) & "px; left:" & CStr(left + 17) & "px;'>" & _
        rsData.Fields("fldName").Value & "</p>" & vbcrlf

		rsData.MoveNext
		top = (top + 17)
	Wend

	rsData.Close
%>
        <p class="lblGeneral" id="lblResultadoAtendimento" style="width:160px; height:17px; top:55px; left:190px;">
			<b>Resultado do atendimento</b>
		</p>
<%
	height =  17
	top    =  55
	left   =  190

    strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, Numero " & _
		"FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
		"WHERE TipoID = 49 AND EstadoID=2 " & _
		"ORDER BY Ordem"

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData.EOF)
        Response.Write "<input type='radio' " & _
            "id='rd" & rsData.Fields("fldID").Value & "' " & _
            "registroID='" & rsData.Fields("fldID").Value & "' " & _
            "numero='" & rsData.Fields("Numero").Value & "' " & _
            "onclick='return rdResultadoAtendimentoOnclick(this)' " & _
			"name='rdResultadoAtendimento' " & _
			"class='fldGeneral' " & _
			"style='width:17px; height:17px; top:" & CStr(top+height) & "px; left:" & CStr(left) & "px;'/>" & vbcrlf
			
        Response.Write "<p class='lblGeneral' " & _
            "id='lbl" & rsData.Fields("fldID").Value & "' " & _
            "registroID='" & rsData.Fields("fldID").Value & "' " & _
            "numero='" & rsData.Fields("Numero").Value & "' " & _
            "onclick='return rdResultadoAtendimentoOnclick(this)' " & _
            "style='width:120px; height:17px; top:" & CStr(top+height) & "px; left:" & CStr(left + 17) & "px;'>" & _
            rsData.Fields("fldName").Value & _
        "</p>" & vbcrlf

		rsData.MoveNext
		top = (top + 17)
	Wend

	rsData.Close
	Set rsData = Nothing
%>

        <p class="lblGeneral" id="lblAdiarDias" name="lblAdiarDias">Adiar (d/h/m)</p>
        <input type="text" id="txtAdiarDias" name="txtAdiarDias" class="fldGeneral"/>
        <p class="lblGeneral" id="lblAdiarHoras" name="lblAdiarHoras"></p>
        <input type="text" id="txtAdiarHoras" name="txtAdiarHoras" class="fldGeneral"/>
        <p class="lblGeneral" id="lblAdiarMinutos" name="lblAdiarMinutos"></p>
        <input type="text" id="txtAdiarMinutos" name="txtAdiarMinutos" class="fldGeneral"/>

        <p class="lblGeneral" id="lblCalcularAgenda"></p>
		<input type="button" id="btnCalcularAgenda" name="btnCalcularAgenda" value="Calcular" onclick="return btn_onclick(this)" class="btns" title="Calcular data do agendamento"/>

        <p class="lblGeneral" id="lblAgendar">Agendar para</p>
        <input type="text" id="txtAgendar" name="txtAgendar" class="fldGeneral"/>

        <p class="lblGeneral" id="lblObservacoes">Observações</p>
        <textarea id="txtObservacoes" name="txtObservacoes" class="fldGeneral"></textarea>        
    
		<input type="button" id="btnOK" name="btnOK" value="OK" onclick="return btn_onclick(this)" class="btns"/>
		<input type="button" id="btnCanc" name="btnCanc" value="Cancelar" onclick="return btn_onclick(this)" class="btns"/>
    </div>    
</body>

</html>
