/********************************************************************
modalextratopreco.js

Library javascript para o modalextratopreco.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_ExtratoID = '';
var glb_nProdutoEmpresaID;

var dsoGen01 = new CDatatransport('dsoGen01');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    getDataInServer(selProduto.value);
        
    // mostrar a janela movido para o final de preenchimento do grid
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Extrato de pre�o', 1);
    
    loadDataAndTreatInterface();    
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;
        
    // Ajusta botoes OK/Cancel para esta janela
    // O botao cancela nao e usado neste asp
    btnOK.style.visibility = 'hidden';
    btnOK.disabled = true;
    btnCanc.style.visibility = 'hidden';
    btnCanc.disabled = true;
    
    // ajusta o divFG
    elem = window.document.getElementById('divFields');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - 2 * ELEM_GAP;
        height = 45;
    }

    adjustElementsInForm([['lblProduto', 'selProduto', 55, 1, -10, -10],
                          ['lblPrecoWeb', 'chkPrecoWeb', 3, 1],
                          ['btnExcel', 'btnExcel', 50, 1]],
                          null, null, true);

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        //top = topFree + parseInt(selProduto.currentStyle.height, 10) + (3 * ELEM_GAP) + 6;
        top = parseInt(divFields.currentStyle.top, 10) + parseInt(divFields.currentStyle.height, 10);
        width = modWidth - frameBorder - 2 * ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) +
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10);
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    fillCmbsData();
    selProduto.onchange = selProdutoChanged;
    chkPrecoWeb.onclick = chkPrecoWebChanged;

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_PL', null);
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        // sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_PL', null);
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_PL', null);
    }
    else if (ctl.id == btnExcel.id) {
        sendToExcel();
    }

    //lockControlsInModalWin(false);
}

/********************************************************************
Preenchimento do combo de produtos
********************************************************************/
function fillCmbsData() {
    clearComboEx(['selProduto']);

    var fg = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg");

    for (i = 1; i < fg.Rows; i++)
    {
        var oOption = document.createElement("OPTION");

        var produtoID = fg.TextMatrix(i, getColIndexByColKey(fg, 'ConceitoID*'));
        var descricao = fg.TextMatrix(i, getColIndexByColKey(fg, 'Conceito*'));
        var empresa;

        try {
            empresa = fg.TextMatrix(i, getColIndexByColKey(fg, 'Empresa*'));
        } catch (e) {
            empresa = fg.TextMatrix(i, getColIndexByColKey(fg, 'Empresa'));
        }

        var text = '(' + produtoID +') ' + descricao.substring(0, (64 - (empresa.length + 6) - (produtoID.length + 3))) + '... (' + empresa + ')';

        oOption.text = text;
        oOption.value = i; //fg.ValueMatrix(i, getColIndexByColKey(fg, 'ConceitoID*'));
        //oOption.setAttribute('', '', 1);
        selProduto.add(oOption);

        if (i == fg.Row)
            oOption.selected = true;
    }
}

/********************************************************************
onChange do selProduto
********************************************************************/
function selProdutoChanged()
{
    getDataInServer(selProduto.value);
}

/********************************************************************
onChange do chkPrecoWeb
********************************************************************/
function chkPrecoWebChanged()
{
    getDataInServer(selProduto.value);
}

/********************************************************************
Envia extrato de pre�o para excel
********************************************************************/
function sendToExcel() {

    var nLinguaLogada = getDicCurrLang();

    strPars = '?glb_ExtratoID=' + glb_ExtratoID;
    strPars += '&controle=' + 'Extrato de Pre�o';
    strPars += '&nLinguaLogada=' + nLinguaLogada;

    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/ReportsGrid_ExtratoPreco.aspx' + strPars;

    lockControlsInModalWin(false);
}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServer(row)
{
    lockControlsInModalWin(true);

    var strPars = "";

    // Busca empresa do produto selecionado para escrever t�tulo no excel
    // Quando empresa selecionada � Allplus, campos empresa n�o � readonly
    try {
        glb_nProdutoEmpresaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + row + ", getColIndexByColKey(fg, 'Empresa*'))");
    } catch (e) {
        glb_nProdutoEmpresaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + row + ", getColIndexByColKey(fg, 'Empresa'))");
    }
    
    var precoDigitado = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + row + ", getColIndexByColKey(fg, 'PrecoDigitado'))");
    var margemDigitada = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + row + ", getColIndexByColKey(fg, 'MargemDigitada'))");

    strPars += "?nEmpresaID=" + sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + row + ", getColIndexByColKey(fg, 'EmpresaID'))");
    strPars += "&nProdutoID=" + sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + row + ", getColIndexByColKey(fg, 'ConceitoID*'))");
    strPars += "&nAliquotaImposto=" + sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + row + ", getColIndexByColKey(fg, 'Imp*'))");

    // Se foi configurada margem na lista, usa essa margem para chegar no mesmo pre�o (e n�o a margem recalculada MC, pois sempre dar� diverg�ncia)
    if ((margemDigitada != "") || (precoDigitado == ""))
        strPars += "&nMargemContribuicao=" + margemDigitada.replace(',','.');
    else
        strPars += "&nMargemContribuicao=" + sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + row + ", getColIndexByColKey(fg, 'MC'))").replace(',', '.');

    strPars += "&nTipoMargem=" + ((chkPrecoWeb.checked) ? "2" : "1");
    strPars += "&nImpostoIncidencia=" + sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "lstPre_ImpostosIncidencia");
    strPars += "&sCFOP=" + sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.TextMatrix(" + row + ", getColIndexByColKey(fg, 'CFOP*'))");
    strPars += "&nLoteID=" + sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "(getColIndexByColKey(fg, 'Lote*') > 0 ? fg.TextMatrix(" + row + ", getColIndexByColKey(fg, 'Lote*')) : 'null')");
    strPars += sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "specialClauseOfResearchEx()");

    dsoGen01.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/extratopreco.aspx' + strPars;
    dsoGen01.ondatasetcomplete = fillModalGrid_DSC;
    dsoGen01.refresh();
}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGrid_DSC()
{
    fg.Redraw = 0;
    
    headerGrid(fg,['Descri��o',
                   '%',
				   'BC',
				   'Moeda', 
				   'Valor',
                   'Nivel',
				   'ExtratoID',
                   'ExtDetalheID'], [5, 6, 7]);
    
    fillGridMask(fg, dsoGen01, ['Descricao*',
                                'Percentual*',
                                'BaseCalculo*',
                                'Moeda*',
                                'Valor*',
                                'Nivel',
                                'ExtratoID',
                                'ExtDetalheID'],
                                ['', '999.99', '', '', '', '', '', ''],
                                ['', '##0.00', '###,###,##0.00', '', '###,###,##0.00', '', '', '']);

    glb_aCelHint = [[0, 2, 'Base de C�lculo']];

    for (i_extrato = 1; i_extrato < fg.Rows; i_extrato++)
    {
        var extDetalheID = fg.TextMatrix(i_extrato, getColIndexByColKey(fg, 'ExtDetalheID'));

        dsoGen01.recordset.moveFirst();
        dsoGen01.recordset.setFilter('ExtDetalheID = ' + extDetalheID);

        if (!dsoGen01.recordset.EOF)
        {
            fg.TextMatrix(i_extrato, getColIndexByColKey(fg, 'Descricao*')) = replicate(' ', (8 * (dsoGen01.recordset['Nivel'].value - 1))) + dsoGen01.recordset['Descricao'].value;
        }
        dsoGen01.recordset.setFilter('');
    }

    //alignColsInGrid(fg, [1, 2, 4]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 2);
    
    fg.Redraw = 2;
            
    with (modalextratoprecoBody)
    {
        style.backgroundColor = 'transparent';
        //scroll = 'no';
        style.visibility = 'visible';
    }
               
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    glb_ExtratoID = fg.ValueMatrix(1, getColIndexByColKey(fg, 'ExtratoID'));

    lockControlsInModalWin(false);

    selProduto.focus();
}

function reports_onreadystatechange() {

    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
        (frameReport.contentWindow.document.readyState == 'interactive') ||
        (frameReport.contentWindow.document.readyState == 'complete')) {

        lockControlsInModalWin(false);
    }
}

