/********************************************************************
modalprint_listaprecos.js

Library javascript para o modalprint.asp
Lista de Precos 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    linksDivAndReports()
    invertChkBox()
    chkBox_Clicked()
    selReports_Change()
    btnOK_Status()
    btnOK_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    noRights()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
var dsoEMail = new CDatatransport('dsoEMail');
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	glb_bUseProgressBar = true;
	asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    with (modalprintBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
    
    if ( selReports.disabled == false )
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{

    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {
		pb_StopProgressBar(true);
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
	}        
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Relat�rios', 1);

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;    
    
    // largura livre
    widthFree = modWidth - frameBorder;
    
    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    with (lblReports.style)
    {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta combo de relatorios
    with (selReports.style)
    {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
    
    // ajusta o divListaPreco
    with (divListaPreco.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // O estado do botao btnOK
    btnOK_Status();
    
    // O usuario tem direito ou nao
    noRights();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;
    
    selReports.onchange = selReports_Change;
    
    // carrega a lista de relatorios
    loadReportsInList();
    
    // linka os divs nos relatorios
    linkDivsAndReports();
    
    // ajusta os divs
    adjustDivListaPreco();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList()
{
    var i;
    
    if ( glb_arrayReports != null )
    {
        for ( i=0; i<glb_arrayReports.length; i++)    
        {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);
            
            if ( glb_arrayReports[i][2] )
                selReports.selectedIndex = i;
        }
        
        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports()
{
    divListaPreco.setAttribute('report', 40171 , 1);
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    // O estado do botao btnOK
    btnOK_Status();
    
    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();
    
    return true;
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change()
{
    // mostra o div correspondente ao relatorio
    showDivByReportID();
    
    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if ( this.disabled == false )
        this.focus();
    
    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status()
{
    var btnOKStatus = true;
    var i, elem;
    
    if ( selReports.selectedIndex != -1 )
    {
        // testa o corrente div visivel
        if ( divListaPreco.currentStyle.visibility == 'visible' )
        {
            for ( i=0; i<(divListaPreco.children.length); i++ )
            {
                elem = divListaPreco.children[i];
                
                if ( (elem.tagName).toUpperCase() == 'INPUT' )
                {
                    if ( (elem.type).toUpperCase() == 'CHECKBOX' )
                        if (elem.checked)
                        {
                            btnOKStatus = false;                
                            break;
                        }
                }
            }
        }   
    }
    
    // 0 div visivel deve ser coerente com o relatorio selecionado
    if ( selReports.value != getCurrDivReportAttr() )
        btnOKStatus = true;
    
    if ( selReports.selectedIndex != -1 )
    {
        // Ficha T�cnica
        if (selReports.value == 40111)
            btnOKStatus = false;
        
        //Lista de preco
        if (selReports.value == 40171)
            btnOKStatus = false;
    }
    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    if ( (selReports.value == 40171) && (glb_sCaller == 'PL') )
        listaPrecos();
    // Ficha T�cnica
    else if ( (selReports.value == 40111) && (glb_sCaller == 'S') )
        fichaTecnica();
    else        
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null);
}

/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID()
{
    var i, coll, attr, currRep;
    
    attr = null;
    currRep = 0;
    
    currRep = selReports.value;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( attr == currRep )
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';    
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr()
{
    var i, coll, attr, retVal;
    
    attr = null;
    retVal = 0;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( coll[i].style.visibility == 'visible' )
            {
                retVal = attr;
                break;
            }    
        }
    }
    return retVal;    
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights()
{
    if ( selReports.options.length != 0 )
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;
    
    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
            coll[i].style.visibility = 'hidden';    
    }
    
    // desabilita o combo de relatorios
    selReports.disabled = true;
    
    // desabilita o botao OK
    btnOK.disabled = true;
        
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];
    
    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);
       
    elem = document.getElementById('selReports');
    
    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
        
    // a altura livre    
    modHeight -= topFree;
        
    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt'; 
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';
    
    // acrescenta o elemento
    window.document.body.appendChild(elem);
        
    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);
        
    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;
        
    return null;
}

/********************************************************************
Ajusta os elementos labels e checkbox do divListaPreco
********************************************************************/
function adjustDivListaPreco()
{
    if (glb_sCaller != 'PL')
        return null;

    txtFiltro.maxLength = 4000;
    txtQuantidade.maxLength = 5;
        
        
    adjustElementsInForm([['lblMarca','selMarca',18,1,-10],
						  ['lblHomologacao','chkHomologacao',3,1],
						  ['lblPreLancamento','chkPreLancamento',3,1,-8],
						  ['lblLancamento','chkLancamento',3,1,-8],
						  ['lblAtivo','chkAtivo',3,1,-8],
						  ['lblPromocao','chkPromocao',3,1,-8],
						  ['lblObsoleto','chkObsoleto',3,1,-8],
                          ['lblSuspenso','chkSuspenso',3,1,-8],
						  ['lblEstoqueDisponivel','chkEstoqueDisponivel',3,1,-1],
						  ['lblMoeda','selMoeda',5,2,-10],
						  ['lblLista','selLista',4,2,-3],
						  ['lblQuantidade','txtQuantidade',5,2,-3],
						  ['lblUF','selUF',5,2,-3],
						  ['lblIncideImposto','chkIncideImposto',3,2,-5],
						  ['lblMargemPublicacao','chkMargemPublicacao',3,2,-10],
						  ['lblFinanciamento','selFinanciamento',17,2,-10],
                          ['lblFiltro','txtFiltro',45,3,-10]],null,null,true);	
		    
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    // O estado do botao btnOK
    btnOK_Status();
    
    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();
    
    return true;
}

/********************************************************************
Usuario clicou um check box
********************************************************************/
function chkBox_Clicked()
{
    ctl = this;
    
    // O estado do botao btnOK
    btnOK_Status();
    
    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();
}

/********************************************************************
Impressao do relatorio de listaPrecos (ID=40171)
********************************************************************/
function listaPrecos()
{
    var elem;
    var bFirst = true;
	var sEstados = '';
    var nMarcaID = (selMarca.value == 0 ? 'NULL' : selMarca.value);    
    var nUFID = (selUF.value == 0 ? 'NULL' : selUF.value);
    var nQuantidade = (txtQuantidade.value == '' ? 'NULL' : txtQuantidade.value);
    var nFinanciamento = (selFinanciamento.value == 0 ? 'NULL' : selFinanciamento.value);
    var bIncideImposto = (chkIncideImposto.checked ? 1 : 0);
    var bEstoque = (chkEstoqueDisponivel.checked ? 1 : 0);
    var bMargemPublicacao = (chkMargemPublicacao.checked ? 1 : 0);
	var sFiltro = trimStr(txtFiltro.value);
    	
	sFiltro = (sFiltro == '' ? 'NULL' : '\'' + sFiltro + '\'');

    for ( i=0; i<(divListaPreco.children.length); i++ )
    {
        elem = divListaPreco.children[i];
        
        if ( (elem.tagName).toUpperCase() == 'INPUT' )
        {
            if ( ((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked) && (elem.id != 'chkEstoqueDisponivel') && (elem.id != 'chkIncideImposto') && (elem.id != 'chkMargemPublicacao') )
            {
                if (bFirst)
                {
                    bFirst=false;
                    sEstados = elem.value;
                }                    
                else 
                {
                    sEstados += ',' + elem.value;
                }
            }    
        }
    }
    
    sEstados = (sEstados == '' ? 'NULL' : '\'' + sEstados + '\'');

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var formato = 2;

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&sEstados=" + sEstados + "&nMarcaID=" + nMarcaID + "&selMoeda=" + selMoeda.value + "&nUFID=" + nUFID + "&selLista=" + selLista.value + "&nQuantidade=" + nQuantidade +
                        "&nFinanciamento=" + nFinanciamento + "&bIncideImposto=" + bIncideImposto + "&bEstoque=" + bEstoque + "&bMargemPublicacao=" + bMargemPublicacao + "&sFiltro=" + sFiltro;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/Reports_listaprecos.aspx?' + strParameters;
}