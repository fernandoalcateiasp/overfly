/********************************************************************
modalimpostos.js

Library javascript para o modalimpostos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var nEmpresaData = getCurrEmpresaData();
var dsoDadosImpostos = new CDatatransport('dsoDadosImpostos');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    getDataInServer();

    // configuracao inicial do html
    setupPage();   
        
    // mostrar a janela movido para o final de preenchimento do grid
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S', null );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null );    

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Impostos', 1);
    
    loadDataAndTreatInterface();    
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;
        
    // Ajusta botoes OK/Cancel para esta janela
    // O botao cancela nao e usado neste asp
    with (btnCanc.style)
    {
        visibility = 'hidden';
        width = 0;
        height = 0;
    }
    
    btnCanc.disabled = true;
    
    // Por default o botao OK vem destravado e muda de posicao
    btnOK.disabled = false;
    // move o btnOK de posicao e muda seu caption
    btnOK.value = 'Fechar';
    
    with (btnOK.style)
    {
        left = (widthFree - parseInt(width, 10)) / 2;
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - 2 * ELEM_GAP;    
        height = parseInt(btnOK.currentStyle.top, 10) -
                 parseInt(top, 10) -
                 ELEM_GAP;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServer()
{
    var strPars = new String();

    // Recebe parametros Impostos sem Pessoa
    nContribuinte = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'chkContribuinte.checked');
    nUFID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selUFID.value');
    nCidade = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selCidadeId.value');
    nSimplesNacional = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'chkSimplesNacional.checked');
    nCnae = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'txtCnae.value');
    nIsencoes = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selIsencoesID.value');
    nSuframa = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'chkSuframaID.checked');
    nSelChavePessoaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selChavePessoaID.value');
    nPais = escape(nEmpresaData[1]);
    
    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(glb_nEmpresaID);
    strPars += '&nProdutoID=' + escape(glb_nProdutoID);
    strPars += '&nPessoaID=' + escape(nSelChavePessoaID == 1? glb_nPessoaID : '');
    strPars += '&nPreco=' + escape(glb_nPreco);
    strPars += '&nQuantidade=' + escape(glb_nQuantidade);
    strPars += '&nCFOPID=' + escape(glb_nCFOPID);
    strPars += '&nFinalidade=' + escape(glb_nFinalidade);

    // Tratamento dos paramentros Sem Pessoa
    if (nSelChavePessoaID == 2) 
    {
        nContribuinte = escape(nContribuinte ? 1 : 0);
        nSimplesNacional = escape(nSimplesNacional ? 1 : 0);
        nSuframa = escape(nSuframa ? 1 : 0);        

        strPars += '&nContribuinte=' + escape(nContribuinte);
        strPars += '&nUFID=' + escape(nUFID);
        strPars += '&nCidade=' + escape(nCidade);
        strPars += '&nSimplesNacional=' + escape(nContribuinte);
        strPars += '&nCnae=' + escape(nCnae);
        strPars += '&nIsencoes=' + escape(nIsencoes);
        strPars += '&nSuframa=' + escape(nSuframa);
        strPars += '&nPais=' + escape(nPais);
    }
    else 
    {
        strPars += '&nContribuinte=';
        strPars += '&nUFID=';
        strPars += '&nCidade=';
        strPars += '&nSimplesNacional=';
        strPars += '&nCnae=';
        strPars += '&nIsencoes=';
        strPars += '&nSuframa=';
        strPars += '&nPais=';
    }

    setConnection(dsoDadosImpostos);

    dsoDadosImpostos.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/impostosdata.aspx' + strPars;
    dsoDadosImpostos.ondatasetcomplete = fillModalGrid_DSC;
    dsoDadosImpostos.refresh();
}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGrid_DSC() {
    var i, j, nSumImp;
    var nAliquota = 0;

    fg.Redraw = 0;

    headerGrid(fg,['Imposto',
                   'Al�quota',
                   'Valor Imposto',
                   'Base C�lculo'],[]);

    fillGridMask(fg,dsoDadosImpostos,['ImpostoAbrev',
                              'Aliquota',
                              'ValorImposto',
                              'BaseCalculo'],
                              ['','','',''],
                              ['','##0.00','###,###,##0.00','###,###,##0.00']);

    alignColsInGrid(fg,[1,2,3]);                           

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    // fg.MergeCol(-1) = true;

    // linha de Totais
    gridHasTotalLine(fg, 'Total', 0xC0C0C0, null, true, [[1,'###,###,##0.00','S'],[2,'###,###,##0.00','S'],[3,'###,###,##0.00','S']]);
    
    if ( fg.Rows > 2 )
    {
        fg.TextMatrix(1, 3) = (glb_nPreco * glb_nQuantidade);
        if (fg.ValueMatrix(1, 3) > 0)
            fg.TextMatrix(1, 1) = (fg.ValueMatrix(1, 2) / fg.ValueMatrix(1, 3)) * 100;
        else            
            fg.TextMatrix(1, 1) = 0;
/*
        nSumImp = 0;
        j = 0;
        for ( i=2;i<fg.Rows;i++ )
        {
            nSumImp += fg.ValueMatrix(i, 2);
            j++;
        }
        
        fg.TextMatrix(1, 2) = nSumImp;
*/
    }

    fg.Redraw = 2;
            
    with (modalimpostosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
               
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();
}
