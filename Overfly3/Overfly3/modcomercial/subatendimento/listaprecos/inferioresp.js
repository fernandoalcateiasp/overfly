/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.

Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();
    
    setConnection(dso);

    //@@
    if (folderID == 24231) // Estoques
    {
       
        var nFiltroID = getCmbCurrDataInControlBar('inf', 2);
        if ( (nFiltroID != null) && (nFiltroID[1] == 41042) )
            sFiltro = sFiltro + ' a.SujeitoID=' + nEmpresaData[0] + ' AND ';

        dso.SQL = 'SELECT c.Fantasia, (STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,356,NULL,NULL,NULL,375, NULL),6,0)) AS Disponivel, ' +
                  '(STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,341,NULL,NULL,NULL,375, NULL),6,0)) AS EstoquePO, ' +
                  '(STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,351,NULL,NULL,NULL,375, NULL),6,0)) AS EstoqueRC, (STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,352,NULL,NULL,NULL,375, NULL),6,0)) AS EstoqueRCC, ' +
                  '(STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,353,NULL,NULL,NULL,375, NULL),6,0)) AS EstoqueRV, (STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,354,NULL,NULL,NULL,375, NULL),6,0)) AS EstoqueRVC, ' +
                  'd.PrazoReposicao AS PrazoReposicao, ' +
                  'dbo.fn_Produto_Disponibilidade(a.SujeitoID,a.ObjetoID,NULL,GETDATE(),NULL,' + nEmpresaData[8] + ', 1) AS dtPrevisaoDisponibilidade, ' +
                  'a.Observacao AS Observacao, Null as Lote ' +
                  'FROM RelacoesPesCon a WITH(NOLOCK) ' +
                  'LEFT OUTER JOIN RelacoesPesCon_Fornecedores d WITH(NOLOCK) ON (a.RelacaoID=d.RelacaoID AND (d.Ordem=1)), ' +
                  'RelacoesPesRec b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
                  'WHERE ' +  sFiltro +
                  'a.ObjetoID= ' + idToFind + ' AND a.TipoRelacaoID=61 AND a.SujeitoID=b.SujeitoID ' +
                  'AND b.TipoRelacaoID=12 AND b.ObjetoID=999 AND a.SujeitoID=c.PessoaID ' +
                  'ORDER BY c.Fantasia';

        return 'dso01Grid_DSC';
    }        
    else if (folderID == 24232) // Cota��o
    {
        dso.SQL = 'SELECT DISTINCT TOP 100 ' +
                'DataCotacao.dtCotacao AS Data, ' +
                'CASE WHEN (d.SujeitoID = c.MoedaID) THEN DataCotacao.CotacaoVenda ELSE NULL END AS MoedaComum, ' +
                'CASE WHEN (d.SujeitoID = c.MoedaAlternativaID) THEN DataCotacao.CotacaoVenda ELSE NULL END AS MoedaParalela, ' +
                'h.SimboloMoeda AS Comum, SPACE(0) AS Paralela ' +
          'FROM RelacoesPesRec a WITH(NOLOCK) ' +
                'INNER JOIN RelacoesPesRec_Moedas b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) ' +
                'INNER JOIN Recursos c WITH(NOLOCK) ON (a.ObjetoID = c.RecursoID) ' +
                'INNER JOIN RelacoesConceitos d WITH(NOLOCK) ON (d.SujeitoID IN (c.MoedaID, c.MoedaAlternativaID)) AND (d.ObjetoID = b.MoedaID) ' +
                'INNER JOIN RelacoesConceitos_Cotacoes DataCotacao WITH(NOLOCK) ON (d.RelacaoID = DataCotacao.RelacaoID) ' +
                'INNER JOIN Conceitos h WITH(NOLOCK) ON (c.MoedaID = h.ConceitoID) ' +
            'WHERE (a.TipoRelacaoID = 12) AND (a.SujeitoID = 2) AND (b.Faturamento = 1) AND (b.Ordem = 1) AND (d.EstadoID = 2) AND (d.TipoRelacaoID = 49) ' +
            'ORDER BY DataCotacao.dtCotacao DESC';
        
        /*
        dso.SQL = 'SELECT DISTINCT TOP 50 ' +
            'DataCotacao.dtCotacao AS Data , DataCotacao.CotacaoVenda AS MoedaComum, ' +
            'NULL AS MoedaParalela, h.SimboloMoeda AS Comum, SPACE(0) AS Paralela ' +
        'FROM RelacoesPesRec a ' +
            'INNER JOIN RelacoesPesRec_Moedas b ON (a.RelacaoID=b.RelacaoID) ' +
            'INNER JOIN Recursos c ON (a.ObjetoID=c.RecursoID) ' +
            'INNER JOIN RelacoesConceitos d ON (c.MoedaID=d.SujeitoID AND d.ObjetoID=b.MoedaID) ' +
            'INNER JOIN RelacoesConceitos_Cotacoes DataCotacao ON (d.RelacaoID=DataCotacao.RelacaoID) ' +
            'INNER JOIN Conceitos h ON (c.MoedaID=h.ConceitoID) ' +
        'WHERE ( ' +  sFiltro + 
            '(a.TipoRelacaoID=12 AND a.SujeitoID=' + nEmpresaData[0] + ') AND ' +
            '(b.Faturamento=1 AND b.Ordem=1) AND ' +
            '(d.EstadoID=2 AND d.TipoRelacaoID=49) ) ' +
        'UNION ALL ' +
        'SELECT DISTINCT TOP 50 ' +
            'DataCotacao.dtCotacao AS Data , NULL AS MoedaComum, ' +
            'DataCotacao.CotacaoVenda AS MoedaParalela, SPACE(0) AS Comum, h.SimboloMoeda AS Paralela ' +
        'FROM RelacoesPesRec a ' +
            'INNER JOIN RelacoesPesRec_Moedas b ON (a.RelacaoID=b.RelacaoID) ' +
            'INNER JOIN Recursos c ON (a.ObjetoID=c.RecursoID) ' +
            'INNER JOIN RelacoesConceitos d ON (c.MoedaAlternativaID=d.SujeitoID AND d.ObjetoID=b.MoedaID) ' +
            'INNER JOIN RelacoesConceitos_Cotacoes DataCotacao ON (d.RelacaoID=DataCotacao.RelacaoID) ' +
            'INNER JOIN Conceitos h ON (c.MoedaID=h.ConceitoID) ' +
        'WHERE ( ' +  sFiltro + 
            '(a.TipoRelacaoID=12 AND a.SujeitoID=' + nEmpresaData[0] + ') AND ' +
            '(b.Faturamento=1 AND b.Ordem=1) AND ' +
            '(d.EstadoID=2 AND d.TipoRelacaoID=49) ) ' +
        'ORDER BY ' +
            'Data DESC';
        */

        return 'dso01Grid_DSC';
    }
    else if (folderID == 24233) // Homologa��o
    {
        dso.SQL = 'SELECT Homologacao AS Homologacao '+
                  'FROM Conceitos WITH(NOLOCK) WHERE ' +
                  sFiltro + 'ConceitoID = '+idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 24234) // Ficha Tecnica
    {
        var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[\'EmpresaID\'].value');
        
        dso.SQL = 'SELECT dbo.fn_Produto_CaracteristicaOrdem(a.ProdutoID, a.CaracteristicaID) AS Ordem, ' +
                          'dbo.fn_Tradutor(b.Conceito, ' + nEmpresaData[7] + ', ' + nEmpresaData[8] + ', NULL) AS Caracteristica, ' +
                          'a.Valor AS Valor, ' +
                          'd.FichaTecnica ' +
	                   'FROM Conceitos_Caracteristicas a WITH(NOLOCK) ' +
		                   'INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.CaracteristicaID) ' +
		                   'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = a.ProdutoID) ' +
		                   'INNER JOIN RelacoesConceitos d WITH(NOLOCK) ON ((d.SujeitoID = a.CaracteristicaID) AND (d.ObjetoID = c.ProdutoID)) ' +
	                   'WHERE ( ' + sFiltro + ' (a.ProdutoID = ' + idToFind + ') AND (a.Checado = 1) AND (d.FichaTecnica = 1)) ' +
				  'UNION ALL ' +
				  'SELECT 1000 AS Ordem, ' + '\'' + 'Medidas do produto (L/A/P)' + '\'' + ' ' +
					'AS Caracteristica, dbo.fn_Produto_DadosFichaTecnica(' + idToFind + ', ' + nEmpresaData[0] + ', 1) AS Valor,' +
					'1 AS FichaTecnica ' +
				  'UNION ALL ' +
				  'SELECT 1001 AS Ordem, ' + '\'' + 'Medidas embalagem (L/A/P)' + '\'' + ' ' +
					'AS Caracteristica, dbo.fn_Produto_DadosFichaTecnica(' + idToFind + ', ' + nEmpresaData[0] + ', 2) AS Valor,' +
					'1 AS FichaTecnica ' +
				  'UNION ALL ' +
				  'SELECT 1002 AS Ordem, ' + '\'' + 'Pesos Bruto/L�quido' + '\'' + ' ' +
					'AS Caracteristica, dbo.fn_Produto_DadosFichaTecnica(' + idToFind + ', ' + nEmpresaData[0] + ', 3) AS Valor,' +
					'1 AS FichaTecnica ' +
				  'UNION ALL ' +
				  'SELECT 1003 AS Ordem, ' + '\'' + 'Garantia' + '\'' + ' ' +
					'AS Caracteristica, dbo.fn_Produto_DadosFichaTecnica(' + idToFind + ', ' + nEmpresaID + ', 4) AS Valor, ' +
					'1 AS FichaTecnica ' +
                  'ORDER BY Ordem';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24235) // Referencias Tecnicas
    {
        dso.SQL = 'SELECT e.Objeto AS Referencia, b.ConceitoID AS ConceitoID, d.RecursoAbreviado AS Estado, b.Conceito AS Conceito ' +
                  'FROM RelacoesConceitos a WITH(NOLOCK), Conceitos b WITH(NOLOCK), RelacoesPesCon c WITH(NOLOCK), Recursos d WITH(NOLOCK), TiposRelacoes e WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.SujeitoID=' + idToFind + ' AND a.EstadoID=2 AND ' +
                  'a.ObjetoID=b.ConceitoID AND a.ObjetoID=c.ObjetoID ' +
                  'AND c.TipoRelacaoID=61 AND c.EstadoID=d.RecursoID ' +
                  'AND c.SujeitoID = ' + nEmpresaData[0] + ' ' +
                  'AND a.TipoRelacaoID=e.TipoRelacaoID ' +
                  'UNION ALL ' +
                  'SELECT e.Sujeito AS Referencia, b.ConceitoID AS ConceitoID, d.RecursoAbreviado AS Estado, b.Conceito AS Conceito ' +
                  'FROM RelacoesConceitos a WITH(NOLOCK), Conceitos b WITH(NOLOCK), RelacoesPesCon c WITH(NOLOCK), Recursos d WITH(NOLOCK), TiposRelacoes e WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.ObjetoID=' + idToFind + ' AND a.EstadoID=2 AND ' +
                  'a.SujeitoID=b.ConceitoID AND a.SujeitoID=c.ObjetoID ' +
                  'AND c.TipoRelacaoID=61 AND c.EstadoID=d.RecursoID ' +
                  'AND c.SujeitoID = ' + nEmpresaData[0] + ' ' +
                  'AND a.TipoRelacaoID=e.TipoRelacaoID ' +
                  'ORDER BY Referencia,Conceito';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 24236) // Pedidos
    {
        var dir_nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')') );
        var dir_nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')') );
        var userID = getCurrUserID();
        var sProprietario = '';
        var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'EmpresaID' + '\'' + '].value');
        var nFiltroID = getCmbCurrDataInControlBar('inf', 2);
        var strSQL = '';
        
        // Alteracao para os vendedores nao visualizarem os pedidos de outros vendedores
        // de EstadoId >= 25
        if (nEmpresaID != 7)
        {
            if (!(dir_nA1) || !(dir_nA2))
                sProprietario = ' AND (a.ProprietarioID = ' + userID + ') ';
        }
    
        strSQL = 'SELECT TOP 300 a.dtPedido, a.PedidoID, Estados.RecursoAbreviado, Pessoas.Fantasia AS Pessoa, ' +
                'NotasFiscais.NotaFiscal, NotasFiscais.dtNotaFiscal, CFOPs.Codigo AS Codigo, Deposito.Fantasia AS Deposito, Colaboradores.Fantasia AS Colaborador, ' +
                'Itens.Quantidade, Moedas.SimboloMoeda AS Moeda, dbo.fn_PedidoItem_Totais(Itens.PedItemID, 7) AS Valor, ' +
                'dbo.fn_PedidoItem_Totais(Itens.PedItemID, 9) AS MargemContribuicao, a.PrazoMedioPagamento, a.Observacao ' +
            'FROM Pedidos a WITH(NOLOCK) ' +
                'INNER JOIN Operacoes Transacoes WITH(NOLOCK) ON (a.TransacaoID = Transacoes.OperacaoID) ' +
                'INNER JOIN Pedidos_Itens Itens WITH(NOLOCK) ON (a.PedidoID = Itens.PedidoID) ' +
                'LEFT JOIN Operacoes CFOPs WITH(NOLOCK) ON (Itens.CFOPID = CFOPs.OperacaoID) ' +
                'LEFT OUTER JOIN NotasFiscais WITH(NOLOCK) ON (a.NotaFiscalID = NotasFiscais.NotaFiscalID AND NotasFiscais.EstadoID=67) ' +
				'INNER JOIN Recursos Estados WITH(NOLOCK) ON (a.EstadoID = Estados.RecursoID) ' +
				'INNER JOIN Pessoas WITH(NOLOCK) ON (a.PessoaID = Pessoas.PessoaID) ' +
				'INNER JOIN Pessoas Colaboradores WITH(NOLOCK) ON (a.ProprietarioID = Colaboradores.PessoaID) ' +
				'INNER JOIN Conceitos Moedas WITH(NOLOCK) ON (a.MoedaID = Moedas.ConceitoID) ' +
				'INNER JOIN Pessoas Deposito WITH(NOLOCK) ON (a.DepositoID=Deposito.PessoaID) ' +
            'WHERE( ' + sFiltro + ' a.TipoPedidoID = 602 AND ' + 
                'Itens.ProdutoID = ' + idToFind + ' AND a.EmpresaID = ' + nEmpresaID + ' ' +
                ' AND ((SELECT COUNT(*) FROM Operacoes_Baixas yy WHERE yy.OperacaoID = Transacoes.OperacaoID) > 0) ' + sProprietario + ' AND a.EstadoID <> 5) ';

        // Filtro de Cliente
        if (nFiltroID[1] == '41044')
        {
            var nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selPessoaID.value');
            
            if ( isNaN(nPessoaID) || (nPessoaID==null) || (nPessoaID=='') )
                nPessoaID = '0';
        
            strSQL += ' AND (a.PessoaID=' + nPessoaID + ' OR a.ParceiroID=' + nPessoaID + ') ';
        }
                
        strSQL += 'ORDER BY a.dtPedido DESC, Colaboradores.Fantasia';
            
            
        dso.SQL = strSQL;
        return 'dso01Grid_DSC';            
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
    var aFolders = [[24231,'true'],  /* Estoques   */
                    [24236,'true'],  /* Pedidos */
                    [24232,'true'],  /* Cota��o */
                    [24233,'true'],  /* Homologacao */
                    [24234,'true'],  /* Caracteristicas */
                    [24235,'true']]; /* Referencias Tecnicas */

	cleanupSelInControlBar('inf',1);

    for (i=0; i<aFolders.length; i++)
    {
        if (eval(aFolders[i][1]))
        {
            vPastaName = window.top.getPastaName(aFolders[i][0]);
            if (vPastaName != '')
            	addOptionToSelInControlBar('inf', 1, vPastaName, aFolders[i][0]);
        }
    }

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;
    
    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (currLine < 0)
            return false;
    }   
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    var dTFormat2 = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
	{
        dTFormat = 'dd/mm/yyyy hh:mm';
        dTFormat2 = 'dd/mm/yyyy';
	}
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
	{
        dTFormat = 'mm/dd/yyyy hh:mm';
        dTFormat2 = 'mm/dd/yyyy';
	}

    fg.FrozenCols = 0;
    // Estoques
    if (folderID == 24231)
    {
        headerGrid(fg,['Empresa','Lote','Dispon�vel',
                       'F�sico','RC','RCC','RV','RVC',
                       'Reposi��o','Disponibilidade','Observa��o'], []);

        fillGridMask(fg,currDSO,['Fantasia','Lote','Disponivel','EstoquePO','EstoqueRC',
                                 'EstoqueRCC','EstoqueRV','EstoqueRVC',
                                 'PrazoReposicao','dtPrevisaoDisponibilidade','Observacao']
                                    ,['','','','','','','','','',''],
                                     ['','#####','#####','#####','#####','#####','#####','','','']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[2,'#####','S'], [3,'#####','S'], [4,'#####','S'], [5,'#####','S'], 
															  [6,'#####','S'], [7,'#####','S']]);

        alignColsInGrid(fg,[2,3,4,5,6,7,8]);
        fg.FrozenCols = 1;
    }    
    // Cota��o
    else if (folderID == 24232)
    {
        var sMoedaComum = 'Moeda Comum';
        var sMoedaParalela = 'Moeda Paralela';
        if ( !((currDSO.recordset.BOF)&&(currDSO.recordset.EOF)))
        {
            currDSO.recordset.moveFirst();
            if ((currDSO.recordset['Comum'].value!=null)&&(currDSO.recordset['Comum'].value!=''))
                sMoedaComum = currDSO.recordset['Comum'].value;
            if ((currDSO.recordset['Paralela'].value!=null)&&(currDSO.recordset['Paralela'].value!=''))
                sMoedaParalela = currDSO.recordset['Paralela'].value;
        }
        headerGrid(fg,['Data'+replicate(' ',19),sMoedaComum,sMoedaParalela], []);
        fillGridMask(fg,currDSO,['Data','MoedaComum','MoedaParalela'] ,
                     ['','',''],[dTFormat,'##,###,###.#######','##,###,###.#######']);
    }    
    // Ficha Tecnica
    else if (folderID == 24234)
    {
        headerGrid(fg,['Ordem','Caracter�stica','Valor'], [0]);
        fillGridMask(fg,currDSO,['Ordem','Caracteristica','Valor'],['','','']);
        alignColsInGrid(fg,[0]);                           
    }    
    // Referencias
    else if (folderID == 24235)
    {
        headerGrid(fg,['ID','Est','Conceito','Refer�ncia'], []);
        fillGridMask(fg,currDSO,['ConceitoID','Estado','Conceito','Referencia']
                                     ,['','','','']);
        
    }
    // Pedidos
    else if (folderID == 24236)
    {
        //Direitos A1 e A2 do lista de pre�o.
        var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));
        var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
        
        aHidenCols = new Array();
        aHidenCols = [];

        //Usu�rios sem direito A1 e A2 n�o devem visualizar a MC.
        if (!(nA1 && nA2))
        {
            var nColMc = 12;

            aHidenCols = [nColMc];
        }

        headerGrid(fg, ['Data',
                       'Pedido', 
                       'Est',
                       'Pessoa', 
					   'Nota',
					   'Data Nota',
					   'CFOP',
					   'Dep�sito',
                       'Colaborador', 
                       'Quant', 
					   'Moeda',
                       'Valor',
                       'MC',
                       'PMP',
                       'Observa��o'], aHidenCols);
        
        fillGridMask(fg,currDSO,['dtPedido', 
                                 'PedidoID', 
                                 'RecursoAbreviado',
                                 'Pessoa', 
                                 'NotaFiscal',
                                 'dtNotaFiscal',
                                 'Codigo',
                                 'Deposito',
                                 'Colaborador', 
                                 'Quantidade', 
                                 'Moeda',
                                 'Valor',
                                 'MargemContribuicao',
                                 'PrazoMedioPagamento',
                                 'Observacao'],
                                 ['','','','','','','','','','','','','','',''],
                                 [dTFormat2,'','','','',dTFormat2,'','','','','','###,###,###.00','###.00','','']);

        alignColsInGrid(fg,[1,4,6,9,11,12,13]);                           
    }
}
