
<%@ LANGUAGE=VBSCRIPT %>

<%
    'C�digo que for�a o n�o armazenamento de cache da pagina . SGP
    Option Explicit
    Response.CacheControl = "no-cache"
    Response.AddHeader "Pragma", "no-cache"
    Response.Expires = -1
    'Fim do c�digo de n�o armazenamento do cache.
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Response.Write "<script ID=" & Chr(34) & "serverCfgVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
    Response.Write vbCrLf
    
    Response.Write "var __APP_NAME__ = " & Chr(39) & Application("appName") & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __PAGES_DOMINIUM__ = " & Chr(39) & objSvrCfg.PagesDominium(Application("appName")) & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __PAGES_URLROOT__ = " & Chr(39) & objSvrCfg.PagesURLRoot(Application("appName")) & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __DATABASE_DOMINIUM__ = " & Chr(39) & objSvrCfg.DatabaseDominium(Application("appName")) & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __DATABASE_ASPURLROOT__ = " & Chr(39) & objSvrCfg.DatabaseASPPagesURLRoot(Application("appName")) & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __DATABASE_WSURLROOT__ = " & Chr(39) & objSvrCfg.PagesDominium(Application("appName")) & "/WSOverflyRDS" & Chr(39) & ";"
    Response.Write vbcrlf

    Response.Write "</script>"
    Response.Write vbCrLf    
    
    Set objSvrCfg = Nothing
%>

<html id="notificacoesHtml" name="notificacoesHtml">

<head>

<title>Notifica��es</title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/notificacoes/notificacoes.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
  
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_browsers.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_facilitiesnonforms.js" & Chr(34) & "></script>" & vbCrLf    
    'Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/betterinnerhtml.js" & Chr(34) & "></script>" & vbCrLf    
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/notificacoes/notificacoes.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
Response.Write "<script ID=" & Chr(34) & "formVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbCrLf
Response.Write "</script>"
Response.Write vbCrLf
%>

<%    
 'Script de variaveis globais
Dim i, j, nUsuarioID, LidasBit
Dim nAction, sAnchorID

LidasBit = 0

Dim rsSPCommand
Dim nRecsAffected, HTML, sTexto 
Dim rsData
Dim strSQL

sTexto = ""

For i = 1 To Request.QueryString("nUsuarioID").Count    
  nUsuarioID = Request.QueryString("nUsuarioID")(i)
Next 

For i = 1 To Request.QueryString("LidasBit").Count    
  LidasBit = Request.QueryString("LidasBit")(i)
Next  

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--
    
//-->
</script>

</head>
<body id="notificacoesBody" name="notificacoesBody" LANGUAGE="javascript" onload="return window_onload()" onbeforeunload="return window_onbeforeunload()" onunload="return window_onunload()">

    <!-- Objeto OverflyGen -->
    <object CLASSID="clsid:CBD8996E-E238-4A48-AB72-7A7250F700C1" ID="overflyGen" HEIGHT="0" WIDTH="0"></object> 
    
    <!-- Div do check box e botao -->
    <div id="divbarraopcoes" name="divbarraopcoes" class="divGeneral">
         <p id="lblLidas" name="lblLidas" class="lblGeneral">N�o lidas</p>
         <p id="lblnotificacoes" name="lblnotificacoes" class="lblTitle">Notifica��es</p>
         <input type="checkbox" id="chkLidas" name="chkLidas" class="checkbox" DATAFLD="Lidas" 
         <% 
            If (LidasBit = 1) Then
                Response.Write "CHECKED"
            End If
         %> 
         title="Somente notifica��es n�o lidas?">
         <input type="button" id="btnMarcarTodas" name="btnMarcarTodas" value="Todas lidas" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btnMarcarTodas">
    </div>
 
    
    <!-- Div do grid -->
    <div id="divNotificacoes" style="overflow-y: scroll; height:40px;" name="divNotificacoes" class="divGeneral">
    </div>    
              
    <input type="button" id="btnOK" name="btnOK" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <iframe id="frameModal" name="frameModal" class="theFrames" frameborder="no"></iframe>
</body>

</html>
