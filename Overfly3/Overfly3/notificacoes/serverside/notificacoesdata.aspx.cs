using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSOverflyRDS;
using WSData;
using java.lang;

namespace Overfly3.notificacoes.serverside
{
    public partial class notificacoesdata : System.Web.UI.OverflyPage
    {
        string formID = (HttpContext.Current.Request.Params["nFormID"]).ToString();
        string usuarioID = (HttpContext.Current.Request.Params["nUsuarioID"]).ToString();
        string notificacaoID = (HttpContext.Current.Request.Params["nNotificacaoID"]).ToString();

        // Executa procedure 
        protected void execPesqlistSql()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[3];

            procParams[0] = new ProcedureParameters(
                "@FormID",
                System.Data.SqlDbType.Int,
                formID != "null" ? (Object)int.Parse(formID) : DBNull.Value);

            procParams[1] = new ProcedureParameters(
                "@PessoaID",
                System.Data.SqlDbType.Int,
                usuarioID != "null" ? (Object)int.Parse(usuarioID) : DBNull.Value);

            procParams[2] = new ProcedureParameters(
                "@NotificacaoID",
                System.Data.SqlDbType.Int,
                notificacaoID != "null" ? (Object)int.Parse(notificacaoID) : DBNull.Value);

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Notificacao_Leitura",
                procParams);
        }

		protected override void PageLoad(object sender, EventArgs e)
		{
            execPesqlistSql();

            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT 'Null' AS Resultado "));
        }
    }
}
