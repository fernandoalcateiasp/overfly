/********************************************************************
notificacoes.js

Library javascript para o notificacoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// Variavel para exibir ou nao msg no beforeunload
var beforeUnloadMsg = true;
// Controla estado de travamento do browser filho
var __bInterfaceLocked = false;

var glb_notificacoesTimer = null;

var glb_LASTLINESELID = '';
var glb_aEmpresaData = null;
var glb_nIdiomaEmpresaID = null;
var glb_retMgs = null;
var glb_controlaEmail = null;
var dsoNotificacoesData = new CDatatransport('dsoNotificacoesData');
var dsoLeitura = new CDatatransport('dsoLeitura');

glb_aEmpresaData = getCurrEmpresaData();
glb_nIdiomaEmpresaID = glb_aEmpresaData[8];

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RESUMO DAS FUNCOES

window_onload()
window_onbeforeunload()
window_onunload()
sendJsCarrierFromForm(idElement, param1, param2, especChildBrowserName)
wndJSProc(idElement, msg, param1, param2)

setupPage()
adjustToobar()
adjustDivChamadas()
adjustDivCallbacks()
adjustDivCommonControls()

btnChamadasCBClick(ctrl)
modeChamadas()
modeCallbacks()
interfaceByGrid()
commonBtnsState()

btnDisponivelClick(ctrl)

btnPessoaClick(ctrl)
btnListaPrecosClick(ctrl)
listaPrecos_DSC()

rdsPadraoOnKeyDown(ctrl)
rdsPadraoOnMouseDown(ctrl)
rdsPadraoOnClick(ctrl)

btnDiscarOnClick(ctrl)
btnLigarOnClick(ctrl)

putHeaderLineInGrid(grid)

getCallbackData()
getCallbackData_DSC()
go_getCallbackData_DSC()

logInOnCentral()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    var elem;
	
	// Ze em 17/03/08
    dealWithObjects_Load();
	// Ze em 08/12/14 a funcao abaixo esta comentada porque esta janel nao tem grid FG
	//dealWithGrid_Load();
	// Fim de Ze em 17/03/08

	if (glb_nIdiomaEmpresaID == 245) 
	{
	    window.document.title = 'Notifications';
	}
	else 
	{
	    window.document.title = 'Notifica��es'; ;
	}
	
	// ajusta o body do html
	elem = document.getElementById('notificacoesBody');
	with (elem) {
	    style.backgroundColor = 'transparent';
	    scroll = 'no';
	    style.visibility = 'visible';
	    style.width = 721;
	}

	// A janela esta carregada
	sendJSMessage(getHtmlId(), JS_NOTIFICATIONCALL, 2, null);
    
	// configuracao inicial do html
	setupPage();

	// Forca scroll para o topo da pagina
	elem.scrollIntoView(true);

	// forca preenchimento do array de controle de travamento dos
	// elementos da interface
	lockControlsInModalWin(true);
	lockControlsInModalWin(false);

	startTimerLoadTableData();

}    

/********************************************************************
Confirma se deve ou nao fechar o browser
********************************************************************/
function window_onbeforeunload()
{
    if ( beforeUnloadMsg )
    {
        //event.returnValue = SYS_NAME;
    }
}

/********************************************************************
Diversos cleanup do sistema
********************************************************************/
function window_onunload()
{
	dealWithObjects_Unload();
	
    // o if abaixo deve estar aqui obrigatoriamente
    try
    {
        if ( window.opener && !window.opener.closed )
        {
           removeChildBrowserFromControl(window.name);

           glb_controlaEmail = 0;
           
           // no caso de interrupcao do carregamento do browser filho
           // destrava a interface do browser mae
           // implantacao em 07/08/2001 - remover se comportamento
           // estranho do sistema
           sendJSMessage(getHtmlId(), JS_CHILDBROWSERLOADED, null, null);

           // fechou a janela de notificacoes
           sendJSMessage(getHtmlId(), JS_NOMFORMCLOSE, 2, null);
           // A janela esta fechada
           sendJSMessage(getHtmlId(), JS_NOTIFICATIONCALL, 0, null);
        }   
    }
    catch(e)   
    {
        ;
    }
}

/********************************************************************
Funcao de comunicacao do carrier:
Chamada do arquivo js_sysbase.js.
Veio da funcao sendJSCarrier definida no arquivo js_sysbase.js e chamada
em qualquer arquivo de form.

Localizacao:
notificacoes.asp

Chama:
Funcao sendJsCarrierFromChildBrowser(window.top.name,
                                     idElement, param1, param2)

do arquivo overfly.asp do browser mae.

Parametros:
idElement             - Obrigatorio. Id do html que manda o carrier.
param1                - De uso do programador. Qualquer objeto ou null.
param2                - De uso do programador. Qualquer objeto ou null.
especChildBrowserName - De uso do programador. Nome de um browser filho
                        para receber o carrier. Se null o carrier sera
                        propagado para todos os browsers.

Retorno     - null se nao tem browser filho.
********************************************************************/
function sendJsCarrierFromForm(idElement, param1, param2, especChildBrowserName)
{
    var wndTop = window.opener;

	if (wndTop == null)
		return null;

    // Chamada do carrier. Chama funcao do browser mae no overfly.asp
    wndTop.sendJsCarrierFromChildBrowser(window.top.name, idElement,
                                         param1, param2,
                                         especChildBrowserName);
}

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
        // Msg do carrier
        case JS_CARRIERCAMING :
        {
            return null;
        }    
        break;
            
        case JS_CARRIERRET :
        {
            return null;
        }
        break;
        
        case JS_MAINBROWSERCLOSING :
        {
            window.top.overflyGen.ForceDlgModalClose();
            beforeUnloadMsg = false;
            window.close();
        }
        break;
        
        // retorna ou altera e retorna o status da flag de interface
        // interface travada do browser filho
        // param1 == false -> so retorna
        case JS_CHILDBROWSERLOCKED:
        {
            if ( param1 == false )
                return __bInterfaceLocked;
            else if ( param1 == true )    
            {
                __bInterfaceLocked = ! __bInterfaceLocked;
                return __bInterfaceLocked;
            }    
        }
        break;

        case JS_WIDEMSG:
        {
            if (idElement == 'fastbuttonsmainHtml') {
                if (param1 == JS_FOCUSFORM) {
                    window.focus();
                    return 1;
                }
            }
        }
        break;
                     
        default:
            return null;
    }
}

/********************************************************************
Ajusta interface da pagina
********************************************************************/
function setupPage()
{
    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    btnOK.disabled = true;
    btnCanc.disabled = true;
    
    chkLidas.onclick = chkLidas_onclick;
    lblLidas.onclick = lblLidas_onclick;

    with (btnOK.style) {
        //visibility = 'inherit';
        visibility = 'hidden';
    }

    with (btnCanc.style) {
        //visibility = 'inherit';
        visibility = 'hidden';
    }

    with (btnMarcarTodas.style) {
        if (glb_nIdiomaEmpresaID == 245) 
        {
            btnMarcarTodas.value = 'Read all';
        }
    }
    
    // ajusta o divNotificacoes
    with (divNotificacoes.style) {
        border = 'black';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = 1;
        top = 28;
        width = 716;
        height = 585;
    }
    // ajusta o divbarraopcoes
    with (divbarraopcoes.style) {
        border = 'black';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = 1;
        top = 0;
        width = 716;
        height = 27;
    }

    with (lblLidas.style) 
    {
        width = 70;
        height = 25;
        top = 5;
        left = 22;

        if (glb_nIdiomaEmpresaID == 245) 
        {
            //Tradu��o das labels, temporariamente comentadas
            document.all.lblLidas.innerText = "Unread";
        }
        
    }
    with (lblnotificacoes.style) {
        width = 100;
        height = 25;
        top = 5;
        left = 280;
        
        if (glb_nIdiomaEmpresaID == 245) 
        {
            //Tradu��o das labels, temporariamente comentadas
            document.all.lblnotificacoes.innerText = "Notifications";
        } 
       
    }   
    
    
}
/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {

    window.focus();
    if (ctl.id == btnOK.id)
        btnOK.focus();
    else if (ctl.id == btnCanc.id)
        btnCanc.focus();
    else if (ctl.id == btnMarcarTodas.id)
        btnMarcarTodas.focus();

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) 
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    else if (ctl.id == btnMarcarTodas.id) 
    {
        // 1. O usuario clicou no botao Marcar Todas
        btnMarcarTodas_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
}

/*******************************************************************************************
* Parametros para a chamada da fun��o, para verificar se detalha ou nao o form do registro
*param1 = FormID
*param2 = Usuario/Pessoa
*param3 = NotificacaoID
*param4 = RegistroID
*param5 = Atalho do Carrier - Ex. SHOWPEDIDO
*********************************************************************************************/
function leitura(param1, param2, param3, param4, param5, param6) 
{
    //Trava a janela para executar as tarefas
    lockControlsInModalWin(true);
    
    var nNotificacaoID = 0;
    var nFormID = 0;
    var nUsuarioID = 0;
    var nRegistroID = 0;
    var nNotificacaoEmpresaID = 0;
    var showCarrier = '';
    var strPars = new String();
    var _retMsg = null;

    nFormID = param1;
    nUsuarioID = param2;
    nNotificacaoID = param3;
    nRegistroID = param4;
    nNotificacaoEmpresaID = param5;
    showCarrier = param6;

    if (showCarrier != null) 
    {
        glb_retMgs = window.top.overflyGen.Confirm('Deseja detalhar a notifica��o?');

        if (glb_retMgs == 0)
        {
            return null;
        }
        else if (glb_retMgs == 1)
        {
            sendJSCarrier(getHtmlId(), showCarrier, new Array(nNotificacaoEmpresaID, nRegistroID));
        }
    }
    
    notificacaoLeitura(nFormID, nUsuarioID, nNotificacaoID);

}

function notificacaoLeitura(param1, param2, param3) 
{
    var nNotificacaoID = 0;
    var nFormID = 0;
    var nUsuarioID = 0;
    var strPars = new String();
    
    nFormID = param1;
    nUsuarioID = param2;
    nNotificacaoID = param3;

    strPars = '?nFormID=' + escape(nFormID);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);
    strPars += '&nNotificacaoID=' + escape(nNotificacaoID);
    
    // pagina asp no servidor saveitemspedido.asp
    dsoLeitura.URL = SYS_ASPURLROOT + '/notificacoes/serverside/notificacoesdata.aspx' + strPars;
    dsoLeitura.ondatasetcomplete = notificacaoLeitura_DSC;
    dsoLeitura.refresh();

}

function notificacaoLeitura_DSC() 
{
   lockControlsInModalWin(false);
   // sendJSMessage('notificacoesHtml', JS_NOTIFICATIONCALL, null, 1);

   glb_controlaEmail = 2;
   
   getTableData();
   //window.close();
   
}

function btnMarcarTodas_Clicked() 
{
    var nUsuarioID = getCurrUserID();

    notificacaoLeitura(null, nUsuarioID, null);

}

function chkLidas_onclick() 
{
    // esta funcao destrava o html contido na janela modal
    /*
    lockControlsInModalWin(false);

    if (chkLidas.checked) {
        sendJSMessage('notificacoesHtml', JS_NOTIFICATIONCALL, null, 2);
    }
    else 
    {
        sendJSMessage('notificacoesHtml', JS_NOTIFICATIONCALL, null, 1);
    }
  
    window.close();
    */

    glb_controlaEmail = 2;
    
    getTableData();

}

function lblLidas_onclick() 
{
    if (chkLidas.checked) 
    {
        chkLidas.checked = false;
    }
    else 
    {
        chkLidas.checked = true;
    }    
        
    chkLidas_onclick();
}

/************************************************************************************************************************************
** Uso do Objetos de dados novo.
*************************************************************************************************************************************/
function startTimerLoadTableData() 
{
    glb_notificacoesTimer = window.setInterval('getTableData()', (1000), 'JavaScript');
}

function stopTimerLoadTableData() {
    if (glb_notificacoesTimer != null) {
        window.clearInterval(glb_notificacoesTimer);
        glb_notificacoesTimer = null;
    }
}

function getTableData() 
{
    stopTimerLoadTableData();
    lockControlsInModalWin(true);

    var mydiv = document.getElementById('divNotificacoes');
    //while (mydiv.firstChild) mydiv.removeChild(mydiv.firstChild);
    mydiv.innerHTML = "";
    mydiv.innerHTML = "<p class=\"lblGeneral_1\">Carregando notifica��es, aguarde...</p>";
    
    var strPars = new String();
    var nUsuarioID = getCurrUserID();
    var nLidasBit = 0;
    var nHabilitaEmail = 0;

    if (chkLidas.checked)
    {
        nLidasBit = 1;
    }
    else 
    {
        nLidasBit = 0;
    }

    if ((glb_controlaEmail == null) || (glb_controlaEmail == 0)) 
    {
        nHabilitaEmail = 1;
    }
    else 
    {
        nHabilitaEmail = 0;
    }
    
    
    strPars = '?nUsuarioID=' + escape(nUsuarioID);
    strPars += '&nLidasBit=' + escape(nLidasBit);
    strPars += '&nHabilitaEmail=' + escape(nHabilitaEmail);

    dsoNotificacoesData.URL = SYS_ASPURLROOT + '/serversidegenEx/notificacoestabledata.aspx' + strPars;
    dsoNotificacoesData.ondatasetcomplete = getTableData_DSC;
    dsoNotificacoesData.refresh();

}

function getTableData_DSC() 
{
    lockControlsInModalWin(false);

    var tableData = '';

    tableData = dsoNotificacoesData.recordset['Texto'].value;

    var mydiv = document.getElementById('divNotificacoes');
    //while (mydiv.firstChild) mydiv.removeChild(mydiv.firstChild);

    mydiv.innerHTML = "";
    mydiv.innerHTML += tableData;
    
}

