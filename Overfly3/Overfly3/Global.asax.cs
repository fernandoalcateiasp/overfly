﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using WSData;

namespace Overfly3
{
    public class Global : System.Web.HttpApplication
    {
        OVERFLYSVRCFGLib.OverflyMTS objSvrCfg = new OVERFLYSVRCFGLib.OverflyMTS();

        protected void Application_Start(object sender, EventArgs e)
        { 
            //Variavel setada pelo programador quando 
            //'usar o projeto para desenvolvimento
            Application["appName"] = "overfly3";

             //'Raiz das paginas no IIS
            Application["appPagesURLRoot"] = objSvrCfg.PagesURLRoot(System.Configuration.ConfigurationManager.AppSettings["application"]);

        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["devMode"] = objSvrCfg.DevMode(System.Configuration.ConfigurationManager.AppSettings["application"]);
            //'variaveis globais da sessao
            //'userID e usado apenas no carregamento do browser mae
            //'e sua interface. Desta maneira, e irrelevante se a sessao
            //'fecha entre intervalos de acesso, mas esta variavel
            //'NAO DEVE ser usada a nao ser pela automacao.
            //'O PROGRAMADOR NAO DEVE USAR ESTA VARIAVEL NUNCA!!!
            Session["userID"] = 0;
        }       

        protected void Session_End(object sender, EventArgs e)
        {

        }
    }
}