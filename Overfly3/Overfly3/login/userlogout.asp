<%@ LANGUAGE=VBSCRIPT %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, dbURLRoot    
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    dbURLRoot = objSvrCfg.DatabaseASPPagesURLRoot(Application("appName"))
    
    Response.Write "<script ID=" & Chr(34) & "serverCfgVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
    Response.Write vbcrlf
    
    Response.Write "var __SYS_ASPURLROOT = " & Chr(39) & objSvrCfg.DatabaseASPPagesURLRoot(Application("appName")) & Chr(39) & ";"
    Response.Write vbcrlf
    
    Response.Write "</script>"
    Response.Write vbcrlf    

    Set objSvrCfg = Nothing
%>

<HTML id="userlogoutHtml" name="userlogoutHtml">

<HEAD>

<TITLE>Logout</TITLE>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/login/userlogout.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
        
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/login/userlogout.js" & Chr(34) & "></script>" & vbCrLf                
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nUserID, nCallbacks

nUserID = 0
nCallbacks = 0

For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

Response.Write "var glb_nUserID = " & CStr(nUserID) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("nCallbacks").Count    
    nCallbacks = Request.QueryString("nCallbacks")(i)
Next

Response.Write "var glb_nCallbacks = " & CStr(nCallbacks) & ";"
Response.Write vbcrlf

Response.Write vbcrlf
Response.Write "</script>"
Response.Write vbcrlf
%>

<SCRIPT ID=wndJSProc LANGUAGE=javascript>
<!--

//-->
</SCRIPT>

<SCRIPT ID=vbFunctions LANGUAGE=vbScript>
<!--

//-->
</SCRIPT>

</HEAD>

<BODY id="userlogoutBody" name="userlogoutBody" LANGUAGE=javascript onload="return window_onload()" onunload="return window_onunload()">   

	<DIV id="divLogout" name="divLogout" class="divGeneral" >
		<p id="lblCallbacks" name="lblCallbacks" class="lblGeneral"></p>
    
		<p id="lblFinalJornada" name="lblFinalJornada" class="lblGeneral">Final</p>
		<INPUT type="checkbox" id="chkFinalJornada" name="chkFinalJornada" class="fldGeneral" Title="Final da jornada de trabalho?"></INPUT>
		    
		<INPUT type="button" id="btnLogout" name="btnLogout" value="Logout" class="btnGeneral" LANGUAGE=javascript onclick="return btn_onclick(this)"></INPUT>
    </DIV>

</BODY>

</HTML>
