<%@ LANGUAGE=VBSCRIPT %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, dbURLRoot    
    Dim sAcessoriesPath
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    dbURLRoot = objSvrCfg.DatabaseASPPagesURLRoot(Application("appName"))
    sAcessoriesPath = dbURLRoot & "/OverSetup.msi"

    Set objSvrCfg = Nothing
%>

<HTML id="userloginHtml" name="userloginHtml">

<HEAD>

<TITLE></TITLE>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/login/userlogin.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf        
        
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/login/userlogin.js" & Chr(34) & "></script>" & vbCrLf                
%>

<%
'Insere facilidades para o desenvolvedor: btnDeveloper

If (Session("devMode")) Then
    
    '********************************************************************
    'Script para style do botao btnDeveloper
    '********************************************************************
    Response.Write "<style type=" & Chr(34) & "text/css" & Chr(34) & ">"
    Response.Write vbcrlf
    Response.Write "#btnDeveloper"
    Response.Write vbcrlf
    Response.Write "{"
    Response.Write vbcrlf
    Response.Write "visibility: hidden;"
    Response.Write vbcrlf
    Response.Write "top: 0px;"
    Response.Write vbcrlf
    Response.Write"left: 0px;" 
    Response.Write vbcrlf
    Response.Write "height: 24px;"
    Response.Write vbcrlf
    Response.Write "width: 126px;"
    Response.Write vbcrlf
    Response.Write "}"
    Response.Write vbcrlf
    Response.Write "</style>"
    Response.Write vbcrlf
    Response.Write vbcrlf
    '********************************************************************
    'Script para evento onmousemove
    '********************************************************************
    Response.Write "<SCRIPT LANGUAGE=javascript FOR=document EVENT=onmousemove>"
    Response.Write vbcrlf
    Response.Write "<!--"
    Response.Write vbcrlf
    Response.Write "document_onmousemove()"
    Response.Write vbcrlf
    Response.Write "//-->"
    Response.Write "</SCRIPT>"
    Response.Write vbcrlf
    Response.Write vbcrlf
    '********************************************************************
    'Script para funcoes de uso do desenvolvedor
    '********************************************************************
    Response.Write "<SCRIPT ID=devMode LANGUAGE=javascript>"
    Response.Write vbcrlf
    Response.Write "<!--"
    Response.Write vbcrlf

    '********************************************************************
    'Ajusta e mostra o botao do desenvolvedor
    '********************************************************************
    Response.Write "function document_onmousemove()"
    Response.Write vbcrlf
    Response.Write "{"
    Response.Write "try"
    Response.Write vbcrlf
    Response.Write "{"
    Response.Write vbcrlf
    
    Response.Write "window.btnDeveloper.style.left = " & _
                   "parseInt(divLogin.style.left, 10) + " & _
                   "parseInt(divLogin.style.width, 10) + ELEM_GAP;"
    Response.Write "window.btnDeveloper.style.top = " & _
                   "parseInt(divLogin.style.top, 10) + " & _
                   "parseInt(btnOK.style.top, 10);"
    
    Response.Write "window.btnDeveloper.style.visibility = 'visible';"
    Response.Write vbcrlf
    Response.Write "}"
    Response.Write vbcrlf
    Response.Write "catch(e)"
    Response.Write vbcrlf
    Response.Write "{"
    Response.Write vbcrlf
    Response.Write ";"
    Response.Write vbcrlf
    Response.Write "}"
    Response.Write vbcrlf
    Response.Write "}"
    Response.Write vbcrlf
    Response.Write vbcrlf

    '********************************************************************
    'Loga o usuario. Uso esclusivo do desenvolvedor.
    '********************************************************************
    Response.Write "function btnDeveloper_onclick()"
    Response.Write vbcrlf
    Response.Write "{"
    Response.Write vbcrlf
    Response.Write "lockThisInterface(true);"
    Response.Write vbcrlf
    Response.Write "var strPars = new String();"
    Response.Write vbcrlf
    Response.Write "strPars = '?';"
    Response.Write vbcrlf
    Response.Write "strPars += 'userName=';"
    Response.Write vbcrlf
    Response.Write "strPars += escape('desenvolvimento@alcateia.com.br');"
    Response.Write vbcrlf
    Response.Write "strPars += '&';"
    Response.Write vbcrlf
    Response.Write "strPars += 'senha=';"
    Response.Write vbcrlf
    Response.Write "strPars += escape('dev$15#ove');"
    Response.Write vbcrlf    
        
    Response.Write "strPars += '&entrada=';"
    Response.Write vbcrlf    
    Response.Write "if ( chkInicioJornada.checked )"
    Response.Write vbcrlf    
	Response.Write "strPars += escape(1);"
	Response.Write vbcrlf    
	Response.Write "else" 
	Response.Write vbcrlf    
	Response.Write "strPars += escape(0);"
    Response.Write vbcrlf    
    
    Response.Write "dsoLogUser.ondatasetcomplete = dsoRetUserValidationDev;"
    Response.Write vbcrlf
    Response.Write "dsoLogUser.URL=" & Chr(39) & dbURLRoot & Chr(39) & "+'/login/serverside/validateuser.aspx'+strPars;"
    Response.Write vbcrlf
    Response.Write "dsoLogUser.Refresh();"
    Response.Write vbcrlf
    Response.Write "}"
    Response.Write vbcrlf
    Response.Write vbcrlf

    '********************************************************************
    'Esta funcao recebe o retorno da validacao do usuario pelo controle dso

    'Parametros:
    'Nenhum

    'Retornos:
    'Nenhum
    '********************************************************************
    Response.Write "function dsoRetUserValidationDev()"
    Response.Write "{"
    Response.Write vbcrlf
    Response.Write "// guarda o PessoaID em variavel do browser mae"
    Response.Write vbcrlf
    Response.Write "set_Cookie(" & Chr(39) & "languageid" & Chr(39) & ", selLinguas.value);" & vbCrLf
	'Comentado por conta do modelo novo de senhas, agora a senha do desenvolvedor vai ser dura, para evitar a 
	'troca periodica de senha que o sistema passa a fazer.
    Response.Write "window.top._glb_currDicLang = selLinguas.value; " & vbCrLf
    Response.Write "window.top.glb_CURRUSERID = dsoLogUser.recordset[" & Chr(39) & "PessoaID" & Chr(39) & "].value;" & vbcrlf
    Response.Write "window.top.glb_CURRUSEREMAIL = dsoLogUser.recordset[" & Chr(39) & "PessoaEmail" & Chr(39) & "].value;" & vbcrlf
    Response.Write "window.top.glb_PRINTERBARCODE = dsoLogUser.recordset[" & Chr(39) & "ImpressoraCodigoBarra" & Chr(39) & "].value;" & vbcrlf

    Response.Write vbcrlf
    Response.Write "sendJSMessage('checkusersystemHtml', JS_USERLOAD, null, null);"
    Response.Write "}"

    Response.Write vbcrlf
    Response.Write "//-->"
    Response.Write vbcrlf
    Response.Write "</SCRIPT>"
End If    
%>

<SCRIPT ID=wndJSProc LANGUAGE=javascript>
<!--
/********************************************************************
Descritivo da interface
1. Nome da interface = 'USERLOGIN'
2. Componentes:
Composicao da interface:
html/asp               bin  hex    frame               observacao
--------               ---  ---    -----               ----------
userlogin.asp          001  0X1    frameGen01
statusbarlogin.asp     010  0X2    frameStatusBar

toda interface         111  0X3
********************************************************************/

//-->
</SCRIPT>

<SCRIPT ID=vbFunctions LANGUAGE=vbScript>
<!--

Function currTimeInSecs()
    Dim theTime, theHour, theMinute, theSecond
    
    theTime = Now
    theHour = Hour(theTime)
    theMinute = Minute(theTime)
    theSecond = Second(theTime)
    
    currTimeInSecs = theSecond + (theMinute * 60) + (theHour * 60 * 60)    
    
End Function

//-->
</SCRIPT>

</HEAD>

<BODY id="userloginBody" name="userloginBody" LANGUAGE=javascript onload="return window_onload()" onunload="return window_onunload()">


    <DIV id="divNotes" name="divNotes" class="divExtern" >
        <!-- ATENCAO: PARA MEXER NO TEXTO ABAIXO USAR SIMBOLOGIA DE ESPACO -->
        <P><STRONG>Requisitos</STRONG>:

        <BR>�&nbsp;Computador:
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Configura��o essencial:&nbsp;M�nimo 2GB RAM,&nbsp;Windows&nbsp;7&nbsp;e&nbsp;Internet&nbsp;Explorer&nbsp;9.0.
        <BR>�&nbsp;Acess�rios&nbsp;do&nbsp;Overfly&nbsp;atualizados:
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Fa�a&nbsp;download&nbsp;dos&nbsp;<A id = "overAccDownload" name = "overAccDownload" href="<% Response.Write sAcessoriesPath %>">
        <FONT color=blue><STRONG>Acess�rios</STRONG></FONT></A>,&nbsp;
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Execute&nbsp;o&nbsp;arquivo&nbsp;<FONT color=blue><STRONG>OverSetup.msi</STRONG>.</FONT>
        <BR>�&nbsp;Internet&nbsp;Explorer&nbsp;configurado&nbsp;para&nbsp;aceitar&nbsp;o&nbsp;</STRONG>Overfly</STRONG>&nbsp;como&nbsp;site&nbsp;seguro:
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;No&nbsp;menu&nbsp;do&nbsp;IE,&nbsp;clique&nbsp;em&nbsp;<STRONG>Ferramentas</STRONG>,&nbsp;<STRONG>Op��es da Internet...</STRONG>,&nbsp;<STRONG>Seguran�a</STRONG>,&nbsp;<STRONG>Sites&nbsp;confi�veis</STRONG>&nbsp;e&nbsp;<STRONG>Sites...</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Desmarque&nbsp;o&nbsp;checkbox&nbsp;<STRONG>Exigir&nbsp;verifica��o&nbsp;do&nbsp;servidor</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Digite&nbsp;<STRONG>http://www.overfly.com.br</STRONG>&nbsp;no&nbsp;campo&nbsp;<STRONG>Adicionar&nbsp;este&nbsp;site&nbsp;da&nbsp;Web&nbsp;�&nbsp;zona</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Clique&nbsp;em&nbsp;<STRONG>Adicionar</STRONG>,&nbsp;<STRONG>OK</STRONG>&nbsp;e&nbsp;<STRONG>OK</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Feche&nbsp;o&nbsp;browser&nbsp;e&nbsp;rode&nbsp;novamente.
        <P>
        <STRONG>Recomenda��es</STRONG>:
        <BR>�&nbsp;Semanalmente&nbsp;ou&nbsp;quando&nbsp;solicitado,&nbsp;limpe&nbsp;o&nbsp;cache&nbsp;do&nbsp;Internet&nbsp;Explorer:
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Feche&nbsp;todos&nbsp;os&nbsp;forms&nbsp;do&nbsp;Overfly,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;No&nbsp;menu&nbsp;do&nbsp;IE,&nbsp;clique&nbsp;em&nbsp;<STRONG>Ferramentas,&nbsp;Op��es&nbsp;da&nbsp;Internet...</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Clique&nbsp;no&nbsp;bot�o&nbsp;<STRONG>Excluir&nbsp;Arquivos...</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Marque&nbsp;o&nbsp;checkbox&nbsp;<STRONG>Apagar&nbsp;todo&nbsp;conte�do&nbsp;off-line</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Clique&nbsp<STRONG>OK</STRONG>&nbsp;e&nbsp<STRONG>OK</STRONG>.
        <!-- FINAL DE ATENCAO: PARA MEXER NO TEXTO ABAIXO USAR SIMBOLOGIA DE ESPACO -->
        <INPUT type="button" id="btnLogin" name="btnLogin" value="Voltar" class="btnGeneral" LANGUAGE=javascript onclick="return btnLoginInfo_onclick(this)"></INPUT>
    </DIV>


    <DIV id="divLogin" name="divLogin" class="divExtern" >
        <P id="lblSalut" name="lblSalut" class="lblGeneral">Bom dia!</P>
        <P id="lblUserName" name="lblUserName" class="lblGeneral">E-mail</P>
        <INPUT type="text" id="txtUserName" name="txtUserName" class="txtGeneral" LANGUAGE=javascript onkeydown="return txtUserName_onkeydown()"></INPUT>
        <P id="lblSenha" name="lblSenha" class="lblGeneral">Senha</P>
        <INPUT type="password" id="txtSenha" name="txtSenha" class="txtGeneral" LANGUAGE=javascript onkeydown="return txtSenha_onkeydown()"></INPUT>
        
        <p id="lblInicioJornada" name="lblInicioJornada" class="lblGeneral">In�cio</p>
	    <INPUT type="checkbox" id="chkInicioJornada" name="chkInicioJornada" class="fldGeneral" Title="Inicio da jornada de trabalho?"></INPUT>
	    
	    <select id="selLinguas" name="selLinguas" class="fldGeneral">
            <option value=246 selected>Portugu�s</option>
            <option value=245>Ingl�s</option>
        </select>
        
        <INPUT type="button" id="btnOK" name="btnOK" value="Login" class="btnGeneral" LANGUAGE=javascript onclick="return btnOK_onclick()"></INPUT>
        <INPUT type="button" id="btnInfo" name="btnInfo" value="Informa��es" class="btnGeneral" LANGUAGE=javascript onclick="return btnLoginInfo_onclick(this)"></INPUT>
        <INPUT type="button" id="btnTrocaSenha" name="btnTrocaSenha" value="Senha" title="Trocar senha" class="btnGeneral" LANGUAGE=javascript onclick="return btnTrocarSenha_onclick(this)"></INPUT>
    </DIV>

<%        
'Botao de uso do desenvolvedor para login direto
If (Session("devMode")) Then
    Response.Write "<input type=" & Chr(34) & "button" & Chr(34) & _
        "id=" & Chr(34) & "btnDeveloper" & Chr(34) & _
        "name=" & Chr(34) & "btnDeveloper" & Chr(34) & _
        "value=" & Chr(34) & "Desenvolvedor" & Chr(34) & _    
        "class=" & Chr(34) & "btnGeneral" & Chr(34) & _
        "LANGUAGE=javascript onclick=" & Chr(34) & "return btnDeveloper_onclick()" & Chr(34) & _
        ">"
    Response.Write vbcrlf
    Response.Write "</input>"
End If
%>

</BODY>

</HTML>
