/********************************************************************
userlogout.js

Library javascript para o userlogout.asp
********************************************************************/

var dsoLogoutUser = new CDatatransport('dsoLogoutUser');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	setupPage();
	
	// O retorno basico
    window.returnValue = false;
	
	// mostra a janela    
    userlogoutBody.style.visibility = 'visible';
}

/********************************************************************
Descarrega a janela
********************************************************************/
function window_onunload()
{
	
}

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
            case JS_PAGELOADED :
            {
				;
            }
            break;
        
            case JS_LOGIN :
            {
				;
            }
            break;
        
        default:
            return null;
    }
}

/********************************************************************
Ajusta interface da janela
********************************************************************/
function setupPage()
{
	var vGap = 0;
    var elem;

    elem = lblCallbacks;
    with (elem.style)
    {
		textAlign = 'center';
        backgroundColor = 'transparent';
        color = 'red';
        fontWeight = 'bold';
        fontSize = '10pt';
        left = 0;
        top = 0;
        width = 240;
        height = 20;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    if ( glb_nCallbacks > 1 )
		lblCallbacks.innerText = 'Voc� tem ' + glb_nCallbacks + ' liga��es em Callback';
    else if ( glb_nCallbacks == 1 )
		lblCallbacks.innerText = 'Voc� tem ' + glb_nCallbacks + ' liga��o em Callback';
    else
		lblCallbacks.innerText = '';
		
	elem = chkFinalJornada;
    elem.disabled = true;
    with (elem.style)
    {
        left = 0;
        top = vGap + 9;
        height = 19;
        width = 19;
        //vGap = parseInt(top, 10) + parseInt(height, 10);
        visibility = 'hidden';
    }
    
    elem = lblFinalJornada;
    elem.innerText = 'Fechar o Overfly';
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = chkFinalJornada.offsetLeft + chkFinalJornada.offsetWidth + 4;
        top = chkFinalJornada.offsetTop + 4;
        width = 100;
        height = 16;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    elem.onclick = lblFinalJornada_onclick;	
    
    elem = btnLogout;
    with (elem.style)
    {
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = 75;
        height = 24;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

	elem = divLogout;
	with (elem.style)
    {
		backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = ELEM_GAP;
        width = lblCallbacks.offsetLeft + lblCallbacks.offsetWidth;
        height = btnLogout.offsetTop + btnLogout.offsetHeight;
    }
    
    btnLogout.style.left = (divLogout.offsetWidth - btnLogout.offsetWidth) / 2;
    chkFinalJornada.style.left = btnLogout.style.left;
    //lblFinalJornada.style.left = chkFinalJornada.offsetLeft + chkFinalJornada.offsetWidth + 4;
    lblFinalJornada.style.left = (divLogout.offsetWidth - lblFinalJornada.offsetWidth) / 2 + ELEM_GAP;
    
}

/********************************************************************
Usuario clicou lblInicioJornada
********************************************************************/
function lblFinalJornada_onclick()
{
	chkFinalJornada.checked = !chkFinalJornada.checked;
}

/********************************************************************
Habilita ou desabilita os elementos da interface

Parametros:
cmdLock     - true desabilita, false habilita

Retornos:
Nenhum
********************************************************************/
function lockThisInterface(cmdLock)
{
    var i, j, coll;
    
    // Inputs
    coll = window.document.getElementsByTagName('INPUT');
    for ( i=0; i<coll.length; i++ )
    {
        coll.item(i).disabled = cmdLock;
    }    
    
    // A
    coll = window.document.getElementsByTagName('A');
    for ( i=0; i<coll.length; i++ )
    {
        coll.item(i).disabled = cmdLock;
    }    
}

/********************************************************************
Usuario clicou botao na interface
********************************************************************/
function btn_onclick(ctl)
{
	// O retorno basico
    window.returnValue = true;
    
    if ( !chkFinalJornada.checked )
        window.close();
    else
    {
		lockThisInterface(true);
	
		var strPars = new String();
		strPars = '?nUserID=' + escape(glb_nUserID);
		strPars += '&nSaida=' + escape(1);

		dsoLogoutUser.ondatasetcomplete = dsoLogoutUser_DSC;
		dsoLogoutUser.URL = __SYS_ASPURLROOT + '/login/serverside/logoutuser.asp' + strPars;
		dsoLogoutUser.Refresh();
    }
        
}

function dsoLogoutUser_DSC()
{
	window.close();
}