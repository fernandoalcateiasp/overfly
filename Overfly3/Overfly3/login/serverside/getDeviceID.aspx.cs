﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using OVERFLYSVRCFGLib;
using WSData;
using System.Net.NetworkInformation;

namespace Overfly3.login.serverside
{
    public partial class getDeviceID : System.Web.UI.OverflyPage
    {
        private string pagesURLRoot;
        private string resultado = null;
        
        private string UserName;

        public string userName
        {
            get { return UserName; }
            set { UserName = value; }
        }
        private string Senha;

        public string senha
        {
            get { return Senha; }
            set { Senha = value; }
        }

        private string ManterConectado;

        public string manterConectado
        {
            get { return ManterConectado; }
            set { ManterConectado = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            int nDeviceID = 0;

            OverflyMTS objSvrCfg = new OverflyMTS();

            pagesURLRoot = objSvrCfg.PagesURLRoot(
                System.Configuration.ConfigurationManager.AppSettings["application"]);

            ProcedureParameters[] procParams = new ProcedureParameters[1];
            procParams[0] = new ProcedureParameters(
                "@DeviceID",
                SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Sistema_DeviceID",
                procParams);

            nDeviceID = int.Parse(procParams[0].Data.ToString());

            WriteResultXML(
                            DataInterfaceObj.getRemoteData(
                                "SELECT " + nDeviceID + " AS DeviceID"
                            ));
        }

        /*public string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                // if (sMacAddress == String.Empty)// only return MAC Address from first card  
                if (adapter.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();

                    if (adapter.Name.ToUpper() == "ETHERNET")
                        continue;
                    //  break;
                }
            }
            return sMacAddress;
        }*/
    }
}