<%@ LANGUAGE=VBSCRIPT %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
    
    Dim rsData, strSQL
	Set rsData = Server.CreateObject("ADODB.Recordset")

	strSQL = "SELECT * FROM Recursos_Senhas WHERE RecursoID=999 AND Tipo=1"
	             
	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
%>

<HTML>
<HEAD>
<TITLE>Regras para altera��o de senha</TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
</HEAD>

<BODY>
<font face="Tahoma" size="2">
<BR>
&nbsp;<B>A nova senha N�O PODE:</B><BR>
&nbsp;&nbsp;&nbsp; ser igual a nenhuma das&nbsp;<% Response.Write rsData.Fields("NumeroHistoricoSenhas").Value %>&nbsp;senhas anteriores.<BR>
&nbsp;&nbsp;&nbsp; ser trocada antes de&nbsp;<% Response.Write rsData.Fields("DuracaoMinima").Value %>&nbsp;dias da �ltima troca.<BR> 
&nbsp;&nbsp;&nbsp; conter espa�o em branco ou um dos caracteres especiais:&nbsp;[]"\<BR>
&nbsp;&nbsp;&nbsp; conter&nbsp;<% Response.Write rsData.Fields("NumeroCaracteresRepetidos").Value %>&nbsp;caracteres repetidos em sequencia.<BR>
&nbsp;&nbsp;&nbsp; conter uma sequencia de&nbsp;<% Response.Write rsData.Fields("NumeroSequenciaCaracteres").Value %>&nbsp;caracteres.<BR> 
&nbsp;&nbsp;&nbsp; conter seu nome de usu�rio.<BR>
&nbsp;&nbsp;&nbsp; conter sua data de nascimento.<BR>
&nbsp;&nbsp;&nbsp; conter&nbsp;<% Response.Write rsData.Fields("NumeroCaracteresDocumentos").Value %>&nbsp;d�gitos em sequencia de seus n�meros de documentos.<BR>
&nbsp;&nbsp;&nbsp; conter&nbsp;<% Response.Write rsData.Fields("NumeroCaracteresTelefones").Value %>&nbsp;d�gitos em sequencia de seus n�meros de telefones.<BR><BR>
&nbsp;<B>A senha DEVE:</B><BR>
&nbsp;&nbsp;&nbsp; ser trocada a cada&nbsp;<% Response.Write rsData.Fields("DuracaoMaxima").Value %>&nbsp;dias. <BR>
&nbsp;&nbsp;&nbsp; conter no minimo&nbsp;<% Response.Write rsData.Fields("TamanhoMinimo").Value %>&nbsp;caracteres. <BR>
&nbsp;&nbsp;&nbsp; conter no maximo&nbsp;<% Response.Write rsData.Fields("TamanhoMaximo").Value %>&nbsp;caracteres. <BR>
&nbsp;&nbsp;&nbsp; ter no minimo o nivel de seguranca&nbsp;<% Response.Write rsData.Fields("NivelSegurancaSenha").Value %>.&nbsp;(*)<BR>
<BR>
&nbsp;<B>(*)&nbsp;Nivel de Seguran�a da Senha</B><BR>
&nbsp;O sistema verifica a presen�a de caracteres dos grupos de caracteres abaixo:<BR>
&nbsp;&nbsp;&nbsp; Grupo 1:&nbsp;letras minusculas&nbsp;(a-z), <BR>
&nbsp;&nbsp;&nbsp; Grupo 2:&nbsp;letras maiusculas&nbsp;(A-Z),<BR>
&nbsp;&nbsp;&nbsp; Grupo 3:&nbsp;numeros&nbsp;(0-9),<BR>
&nbsp;&nbsp;&nbsp; Grupo 4:&nbsp;caractres especiais:&nbsp;`~!@#$%^&*()-_=+{}|;:,<.>/?<BR>
<BR>
&nbsp;Se a senha tiver caracteres dos 4 grupos,&nbsp;ela tem nivel 4.&nbsp;Se tiver caracteres de 3 grupos,&nbsp;ela tem nivel 3 e assim por diante.<BR>
</font>
</BODY>
</HTML>
<%
	rsData.Close
	Set rsData = Nothing
%>