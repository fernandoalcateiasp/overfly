<%@ LANGUAGE=VBSCRIPT %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'********************************************************************
'Inicia o carregamento da interface do browser mae do usuario.
'Composicao da interface:
'html/asp           frame               observacao
'--------           -----               ----------
'mainmenu.asp       frameMainMenu
'statusbar.asp      frameStatusBar
'fastbuttons.asp    frameFastButtons
'********************************************************************
Response.Redirect( pagesURLRoot & "/mainmenu/mainmenu.aspx" )

%>

