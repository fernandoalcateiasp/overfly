﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="unlog01.aspx.cs" Inherits="Overfly3.login.serverside.unlog01" %>

<html id="unlog01Html" name="unlog01Html">

<head>
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">

    <title>Overfly</title>

    <%
        //Executa se o usuario fecha o browsers mae

        //'Zera o id do usuario
        Session["userID"] = 0;

        //'Fecha a sessao
        Session.Abandon();

    %>

    <script id="wndJSProc" language="javascript">
<!--

    /********************************************************************
    Configura o html
    ********************************************************************/
    function window_onload() {
        if (window.history.length != 0) {
            try {
                window.history.back();
            }
            catch (e) {
                ;
            }
        }
    }

    //-->
    </script>

</head>

<body language="javascript" onload="return window_onload()">
</body>

</html>
