﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.login.serverside
{
    public partial class validatenewpassword : System.Web.UI.OverflyPage
    {
        private Integer UserID;

        public Integer userID
        {
            get { return UserID; }
            set { UserID = value; }
        }
        private string UserName;

        public string userName
        {
            get { return UserName; }
            set { UserName = value; }
        }
        private string NovaSenha;

        public string novaSenha
        {
            get { return NovaSenha; }
            set { NovaSenha = value; }
        }
        private string GeraSenha;

        public string geraSenha
        {
            get { return GeraSenha; }
            set { GeraSenha = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            Session["userID"] = 0;
            DataSet Senha = new DataSet();
            string resultado = null;

            if (GeraSenha.ToString() == "0")
            {
                Session["userID"] = UserID;

                ProcedureParameters[] procParams = new ProcedureParameters[4];

                procParams[0] = new ProcedureParameters(
                    "@EmailLogin",
                    System.Data.SqlDbType.VarChar,
                    UserName != null ? (Object)UserName.ToString() : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                  "@Senha",
                  System.Data.SqlDbType.VarChar,
                   NovaSenha != null ? (Object)NovaSenha.ToString() : DBNull.Value);

                procParams[2] = new ProcedureParameters(
                  "@Tipo",
                  System.Data.SqlDbType.Int,
                   1);

                procParams[3] = new ProcedureParameters(
                  "@Resultado",
                  System.Data.SqlDbType.VarChar,
                  DBNull.Value,
                  ParameterDirection.Output);

                procParams[3].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                             "sp_Senha_Verifica",
                             procParams);

                if ((procParams[3].Data != null) && (procParams[3].Data.ToString() != ""))
                {
                    resultado = procParams[3].Data.ToString();
                }

            }

            if (resultado == null)
            {
                ProcedureParameters[] procParams = new ProcedureParameters[8];

                procParams[0] = new ProcedureParameters(
                 "@EmailLogin",
                 System.Data.SqlDbType.VarChar,
                 UserName != null ? (Object)UserName.ToString() : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                  "@Senha",
                  System.Data.SqlDbType.VarChar,
                   NovaSenha != null ? (Object)NovaSenha.ToString() : DBNull.Value);

                procParams[2] = new ProcedureParameters(
                  "@GerarSenha",
                  System.Data.SqlDbType.Bit,
                   (GeraSenha.ToString() == "1" ? true : false));

                procParams[3] = new ProcedureParameters(
                  "@ForcarTroca",
                  System.Data.SqlDbType.Bit,
                   false);

                procParams[4] = new ProcedureParameters(
                  "@CaminhoFTP",
                  System.Data.SqlDbType.VarChar,
                   DBNull.Value);

                procParams[5] = new ProcedureParameters(
                  "@Tipo",
                  System.Data.SqlDbType.Int,
                   1); 

                procParams[6] = new ProcedureParameters(
                  "@PesUrlID",
                  System.Data.SqlDbType.Int,
                   DBNull.Value);

                procParams[7] = new ProcedureParameters(
                  "@Resultado",
                  System.Data.SqlDbType.VarChar,
                  DBNull.Value,
                  ParameterDirection.Output);

                procParams[7].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                             "sp_Senha_Insert",
                             procParams);

            }


            WriteResultXML(
                    DataInterfaceObj.getRemoteData("select '" + resultado + "' as Mensagem"));


        }
    }
}