﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.login.serverside
{
    public partial class logoutuser : System.Web.UI.OverflyPage
    {
        private Integer UserID;

        public Integer nUserID
        {
            get { return UserID; }
            set { UserID = value; }
        }
        private Integer Saida;

        public Integer nSaida
        {
            get { return Saida; }
            set { Saida = value; }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            string ip = Request.ServerVariables["REMOTE_ADDR"];

            ProcedureParameters[] procParams = new ProcedureParameters[3];

            procParams[0] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                UserID != null ? (Object)UserID.ToString() : DBNull.Value);

            procParams[1] = new ProcedureParameters(
              "@Entrada",
              System.Data.SqlDbType.Bit,
              bool.FalseString);

            procParams[2] = new ProcedureParameters(
              "@EnderecoIP",
              System.Data.SqlDbType.Int,
              ip != null ? (Object)ip.ToString() : DBNull.Value);

            DataInterfaceObj.execQueryProcedure(
                        "sp_LOG_Presenca",
                        procParams);


            WriteResultXML(DataInterfaceObj.getRemoteData(
            "select 0 as OK"));
        }

    }
}
