﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using OVERFLYSVRCFGLib;
using WSData;
using System.Net.NetworkInformation;

namespace Overfly3.login.serverside
{
    public partial class validateuser : System.Web.UI.OverflyPage
    {
        private string pagesURLRoot;
        private string resultado = null;
        
        private string UserName;
        public string userName
        {
            get { return UserName; }
            set { UserName = value; }
        }

        private string Senha;
        public string senha
        {
            get { return Senha; }
            set { Senha = value; }
        }

        private string ManterConectado;
        public string manterConectado
        {
            get { return ManterConectado; }
            set { ManterConectado = value; }
        }

        private string DeviceID;
        public string deviceID
        {
            get { return DeviceID; }
            set { DeviceID = value; }
        }

        /*private string Entrada;

        public string entrada
        {
            get { return Entrada; }
            set { Entrada = value; }
        }
        
        private string Autologin;

        public string autologin
        {
            get { return Autologin; }
            set { Autologin = value; }
        }*/

        protected override void PageLoad(object sender, EventArgs e)
        {
            Session["userID"] = 0;
            
            DataSet Login = new DataSet();
            string ip = Request.ServerVariables["REMOTE_ADDR"];

            OverflyMTS objSvrCfg = new OverflyMTS();

            pagesURLRoot = objSvrCfg.PagesURLRoot(
                System.Configuration.ConfigurationManager.AppSettings["application"]);

            ProcedureParameters[] procParams = new ProcedureParameters[7];

            procParams[0] = new ProcedureParameters(
                "@EmailLogin",
                System.Data.SqlDbType.VarChar,
                UserName != null ? (Object)UserName.ToString() : DBNull.Value);

            procParams[1] = new ProcedureParameters(
              "@Senha",
              System.Data.SqlDbType.VarChar,
               senha != null ? (Object)senha.ToString() : DBNull.Value);

            procParams[2] = new ProcedureParameters(
              "@Url",
              System.Data.SqlDbType.VarChar,
               pagesURLRoot != null ? (Object)pagesURLRoot.ToString() : DBNull.Value);

            procParams[3] = new ProcedureParameters(
              "@IP",
              System.Data.SqlDbType.VarChar,
               ip != null ? (Object)ip.ToString() : DBNull.Value, 15);

            procParams[4] = new ProcedureParameters(
                "@DeviceID",
                SqlDbType.Int,
                this.deviceID.ToString());

            procParams[5] = new ProcedureParameters(
              "@ManterConectado",
              System.Data.SqlDbType.VarChar,
             (this.manterConectado == "1" ? bool.TrueString : bool.FalseString));

            procParams[6] = new ProcedureParameters(
              "@Log",
              System.Data.SqlDbType.VarChar,
             bool.TrueString);

           Login = DataInterfaceObj.execQueryProcedure(
                        "sp_Login",
                        procParams);

           if (Login.Tables[1].Rows[0]["AcessoLiberado"].ToString() == "True")
           {
               Session["userID"] = Convert.ToInt32(Login.Tables[1].Rows[0]["PessoaID"].ToString());
           }
           else
               resultado = Login.Tables[1].Rows[0]["Mensagem"].ToString();

           WriteResultXML(Login);
        }
    }
}