/********************************************************************
newpassword.js

Library javascript para o newpassword.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoNovaSenha = new CDatatransport('dsoNovaSenha');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES:
    wndJSProc(idElement, msg, param1, param2)
    window_onload()
    setupInterface()
    maxLengthData()
    lockThisInterface(cmdLock)
    onkeypress_fldText()
    onbeforepaste_fldText()
    onpaste_fldText()
    onbeforepaste_fldText_Verify(clipContent)        
    btnOKClicked()
    verifyNewPasswordInBanck()
    verifyNewPasswordInBanck_DSC()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
        default:
            return null;
    }
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    setupInterface();

    // ajusta o body do html
    with (modalnewpasswordBody)
    {
        style.backgroundColor = 'white';
        scroll = 'no';
        style.visibility = 'visible';
    }
    lblUsuario.innerHTML = '<B>' + window.top.glb_USERFANTASIA + '</B>';
    showExtFrame(window, true);    
    
    window.focus();
    txtSenha.focus();
}

/********************************************************************
Ajusta interface
********************************************************************/
function setupInterface()
{
    var winWidth = 238;
    var winHeight = 280;
    var rectFrameNewLogin = null;
    var rectFrameSTBar = null;
    var vGap = 0;
    var txtWidth = 215;
    var txtHeight = 24;
    var i, coll;

    rectFrameNewLogin = getRectFrameInHtmlTop('frameGen01');
    rectFrameSTBar = getRectFrameInHtmlTop('frameStatusBar');

    if ( rectFrameNewLogin == null )
        return null;
    
    // ajusta rect
    rectFrameNewLogin[0] = ((rectFrameNewLogin[2] - rectFrameNewLogin[0]) - winWidth) / 2;
    rectFrameNewLogin[1] = (((rectFrameNewLogin[3] - rectFrameNewLogin[1]) - winHeight) / 2) + (rectFrameSTBar[1] + rectFrameSTBar[3]);
    rectFrameNewLogin[2] = /*rectFrameNewLogin[0] +*/ winWidth;
    rectFrameNewLogin[3] = /*rectFrameNewLogin[1] +*/ winHeight;
    
    rectFrameNewLogin[0] = Math.abs(rectFrameNewLogin[0]);
    rectFrameNewLogin[1] = Math.abs(rectFrameNewLogin[1]);
    rectFrameNewLogin[2] = Math.abs(rectFrameNewLogin[2]);
    rectFrameNewLogin[3] = Math.abs(rectFrameNewLogin[3]);

    moveExtFrame(window, rectFrameNewLogin);
    
    with (divCaption.style)
    {
        left = 0;
        top = 0;
        height = 24;
        width = winWidth;//rectFrameNewLogin[2] - rectFrameNewLogin[0];
    }

    lblCaption.innerText = 'Nova Senha';
    with (lblCaption.style)
    {
        color = 'white';
        fontSize = '10pt'; 
        //fontWeight = 'bold';
        left = 4;
        top = 2;
        width = parseInt(divCaption.currentStyle.width) - 2 * parseInt(left, 10);
        height = 22;
    }
    
    with (divNotes.style)
    {
        backgroundColor = 'transparent';
        left = 10;
        top = parseInt(divCaption.currentStyle.top, 10) + parseInt(divCaption.currentStyle.height, 10) + ELEM_GAP;
        width = 228;
        height = 54;
    }
    
    with (divSenha.style)
    {
        backgroundColor = 'transparent';
        left = 10;
        top = parseInt(divNotes.currentStyle.top, 10) + parseInt(divNotes.currentStyle.height, 10) + ELEM_GAP;
        width = parseInt(divNotes.currentStyle.width, 10);
        height = 128;
    }
    
    with (bnAjuda.style)
    {
        backgroundColor = 'transparent';
        left = 192;
        top = 0;
        width = 24;
        height = 24;
    }

    with (lblSenha.style)
    {
        backgroundColor = 'transparent';
        left = 0;
        top = 0;
        width = lblSenha.innerText.length * FONT_WIDTH;
        height = 16;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    txtSenha.value = '';
    with (txtSenha.style)
    {
        left = 0;
        top = vGap;
        width = txtWidth;
        height = txtHeight;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    with (lblSenhaConf.style)
    {
        backgroundColor = 'transparent';
        left = 0;
        top = vGap + ELEM_GAP;
        width = lblSenhaConf.innerText.length * FONT_WIDTH;
        height = 16;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    txtSenhaConf.value = '';
    with (txtSenhaConf.style)
    {
        left = 0;
        top = vGap;
        width = txtWidth;
        height = txtHeight;
        vGap = parseInt(top, 10) + parseInt(height, 10) + 10;
    }
    
    with (lblGerarSenha.style)
    {
        backgroundColor = 'transparent';
        left = 25;
        top = vGap + ELEM_GAP - 3;
        width = lblGerarSenha.innerText.length * FONT_WIDTH;
        height = 16;
        //vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    chkGerarSenha.checked = false;
    with (chkGerarSenha.style)
    {
        left = -2;
        top = vGap;
        width = FONT_WIDTH * 3;
        height = txtHeight;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    with (btnOK.style)
    {
        left = 15;
        top = vGap + ELEM_GAP;
        width = 85;
        height = 24;
        //vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    with (btnCancela.style)
    {
        left = btnOK.offsetLeft + btnOK.offsetWidth + ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = 85;
        height = 24;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    maxLengthData();
    
    // Evento do botao OK
    btnOK.onclick = btnOKClicked;
    btnCancela.onclick = btnCancelaClicked;
    
    // selecionar conteudo do campos textos quando em foco
    // e controlar digitacao em campos textos
    coll = window.document.getElementsByTagName('INPUT');
    for ( i=0; i<coll.length; i++ )
    {
        if ( (coll.item(i).type.toUpperCase() == 'TEXT') || (coll.item(i).type.toUpperCase() == 'PASSWORD') )
        {
            coll.item(i).onfocus = selFieldContent;
            coll.item(i).onbeforepaste = onbeforepaste_fldText;
            coll.item(i).onpaste = onpaste_fldText;
            coll.item(i).onkeypress = onkeypress_fldText;
        }    
    }
}

/********************************************************************
Quantidade maxima de caracteres digitados
********************************************************************/
function maxLengthData()
{
    txtSenha.maxLength = glb_nTamanhoMaximo;
    txtSenhaConf.maxLength = glb_nTamanhoMaximo;
}

/********************************************************************
Habilita ou desabilita os elementos da interface

Parametros:
cmdLock     - true desabilita, false habilita

Retornos:
Nenhum
********************************************************************/
function lockThisInterface(cmdLock)
{
    var i, j, coll;
    
    // Inputs
    coll = window.document.getElementsByTagName('INPUT');
    for ( i=0; i<coll.length; i++ )
    {
        coll.item(i).disabled = cmdLock;
    }    
}

/********************************************************************
Controla digitacao nos campos de senha e confirmacao da senha
********************************************************************/
function onkeypress_fldText()
{
    var kCode = event.keyCode;
    var previousText = '';
    var repConsecutive = 0;
    var numerosRepetidos = glb_nNumeroCaracteresRepetidos + 1;
    /*********************    
    Deads keys:

    A    -> 192 a 197
    C    -> 199
    E    -> 200 a 203
    I    -> 204 a 207
    O    -> 210 a 214
    U    -> 227 a 220

    a    -> 224 a 229
    c    -> 231
    e    -> 232 a 235
    i    -> 236 a 239
    o    -> 242 a 246
    u    -> 249 a 252
    *********************/
    
    // So aceita caracteres alfa numericos, dead keys, teclas de
    // movimento e Enter
    /*
    if ( !(((kCode >= 48) && (kCode <= 57)) ||
           ((kCode >= 65) && (kCode <= 90)) ||
           ((kCode >= 97) && (kCode <= 122)) ||
           ((kCode >= 192) && (kCode <= 197)) ||
           (kCode >= 199) ||
           ((kCode >= 200) && (kCode <= 203)) ||
           ((kCode >= 204) && (kCode <= 207)) ||
           ((kCode >= 210) && (kCode <= 214)) ||
           ((kCode >= 2227) && (kCode <= 220)) ||
           ((kCode >= 224) && (kCode <= 229)) ||
           (kCode >= 231) ||
           ((kCode >= 232) && (kCode <= 235)) ||
           ((kCode >= 236) && (kCode <= 239)) ||
           ((kCode >= 242) && (kCode <= 246)) ||
           ((kCode >= 249) && (kCode <= 252)) ||
           (kCode >= 13)) )
        return false;
    */
/*
    if ( !(((kCode >= 48) && (kCode <= 57)) ||
           ((kCode >= 65) && (kCode <= 90)) ||
           ((kCode >= 97) && (kCode <= 122)) ||
           (kCode >= 13)) )
        return false;
*/
    // Nao pode ter 3 caracteres iguais consecutivos
    previousText = this.value;
    repConsecutive = 1;
    
    for ( i=(previousText.length - 1); i>(previousText.length - numerosRepetidos) ; i-- )
    {
        if ( previousText.charCodeAt(i) == kCode )
            repConsecutive++;
        else    
            repConsecutive--;

        if ( repConsecutive == numerosRepetidos )
            return false;
    }

    if ( kCode == 13 )
        btnOKClicked();
}

/********************************************************************
Controla validade da string em um paste
Se o conteudo do clipboard atender, por principio o conteudo atual
do campo sera sempre substituido pelo do clipboard, ou seja,
insercao nao sera permitido
********************************************************************/
function onbeforepaste_fldText()
{
    var retTest = false;
    var clipContent = window.clipboardData.getData('Text');
    
    // Conteudo do clipboard vazio ou de outro tipo
    if ( clipContent == null )
        return false;
    
    // Trima conteudo do clipboard    
    clipContent = trimStr(clipContent);
    
    if ( clipContent.length == 0 )
        return false;
                
    // Conteudo compativel, ajusta numero de caracteres e testa
    if ( clipContent.length > glb_nTamanhoMaximo )
        clipContent = clipContent.substr(0, glb_nTamanhoMaximo);
        
    retTest = onbeforepaste_fldText_Verify(clipContent);
    
    if ( retTest )
        this.value = clipContent;
    
    event.returnValue = false;
}

/********************************************************************
Controla validade da string em um paste
Os campos de senha e confirmacao de senha nao aceitam paste
********************************************************************/
function onpaste_fldText()
{
    event.returnValue = false;
}

/********************************************************************
Verifica texto a ser passado do clipboard
********************************************************************/
function onbeforepaste_fldText_Verify(clipContent)
{
    var kCode = '';
    var previousText = '';
    var repConsecutive = 0;
    var i, j;
    var numerosRepetidos = glb_nNumeroCaracteresRepetidos + 1;
    
    for (i=0; i<clipContent.length; i++)
    {
        kCode = (clipContent.substr(i, 1)).charCodeAt(0);
/*        
		if ( !(((kCode >= 48) && (kCode <= 57)) ||
			((kCode >= 65) && (kCode <= 90)) ||
			((kCode >= 97) && (kCode <= 122)) ||
			(kCode >= 13)) )
			return false;
*/
		// Nao pode ter glb_nNumeroCaracteresRepetidos caracteres iguais consecutivos
		previousText = clipContent.substr(0, i);
		repConsecutive = 1;
		for ( j=(previousText.length - 1); j>(previousText.length - numerosRepetidos) ; j-- )
		{
			if ( previousText.charCodeAt(j) == kCode )
				repConsecutive++;
			else    
				repConsecutive--;

			if ( repConsecutive == numerosRepetidos )
				return false;
		}
    }

    return true;
}

/********************************************************************
Usuario clicou o botao OK
Senha e contra senha sao trimadas
Senha e contra senha devem ter pelo menos glb_nTamanhoMinimo caracteres
Senha e contra senha devem ser iguais
********************************************************************/
function btnOKClicked()
{
    txtSenha.value = trimStr(txtSenha.value);
    txtSenhaConf.value = trimStr(txtSenhaConf.value);
    
    var senha = txtSenha.value;
    var senhaConf = txtSenhaConf.value;
    var oldSenha = sendJSMessage('LOGIN_HTML', JS_LOGIN, EXECEVAL, 'txtSenha.value');
    
    if (!chkGerarSenha.checked)
    {
		// Senha nao pode ser igual a atual
		if ( senha == oldSenha )    
		{
			alert('Senha deve ser diferente da atual!');
			if (!txtSenha.disabled)
			{
				window.focus();
				txtSenha.focus();
			}
			return true;        
		}
	    
		// Senha com pelo menos glb_nTamanhoMinimo caracteres
		if ( senha.length < glb_nTamanhoMinimo )
		{
			if (senha.length < glb_nTamanhoMinimo)
			{
				alert('Senha deve ter pelo menos ' + glb_nTamanhoMinimo + ' caracteres!');
				if (!txtSenha.disabled)
				{
					window.focus();
					txtSenha.focus();
				}
			}    
			return true;        
		}    
	    
		// Senha e sua confirmacao devem ser iguais
		if ( senha != senhaConf )
		{
			alert('Senha e sua redigita��o devem ser iguais!');
			if (!txtSenha.disabled)
			{
				window.focus();
				txtSenhaConf.focus();
			}
			return true;        
		}
    }
    
    // prossegue com operacoes de banco
    verifyNewPasswordInBanck();
}

/********************************************************************
Senha segue para verificacao no banco
********************************************************************/
function verifyNewPasswordInBanck()
{
    lockThisInterface(true);
    var userID = window.top.glb_CURRUSERID;
    var novaSenha = txtSenha.value;
    
    // IMPORTANTE - A PAGINA ASP DE VERIFICACAO DEVE GRAVAR
    // O ID DO USUARIO NA SECAO O IIS SE A NOVA SENHA E ACEITA,
    // IGUAL OCORRE NO ARQUIVO VALIDATEUSER.ASP
    
    // valida nova senha do lado do servidor
    var strPars = new String();
    strPars = '?userID=' + escape(userID);
    strPars += '&userName=' + escape(window.top.glb_USERNAME);
    strPars += '&novaSenha=' + escape(novaSenha);
    strPars += '&geraSenha=' + escape(chkGerarSenha.checked ? "1" : "0");
    
    dsoNovaSenha.ondatasetcomplete = verifyNewPasswordInBanck_DSC;
    dsoNovaSenha.URL= SYS_ASPURLROOT + '/login/serverside/validatenewpassword.aspx'+strPars;
    dsoNovaSenha.Refresh();
}

/********************************************************************
Senha volta verificada no banco
********************************************************************/
function verifyNewPasswordInBanck_DSC()
{
    var userID = window.top.glb_CURRUSERID;
    
    // Se tudo OK, loga o usuario
    if ( !(dsoNovaSenha.recordset.BOF && dsoNovaSenha.recordset.EOF) )
    {
        if ( dsoNovaSenha.recordset.Fields['Mensagem'].value == "" )
        {
			if (!chkGerarSenha.checked)
			{
				sendJSMessage('LOGIN_HTML', JS_LOGIN, EXECEVAL, 'txtSenha.value = ' + '\'' + txtSenha.value + '\'');
				sendJSMessage('LOGIN_HTML', JS_LOGIN, EXECEVAL, 'btnOK_onclick()');
				showExtFrame(window, false);
            }
            else
            {
				alert('Sua senha foi gerada pelo overlfy e enviada para o seu e-mail.');
				btnCancelaClicked();
			}
        }
        else
        {
			// Se nova senha nao foi aceita
			lockThisInterface(false);
	    
			if (dsoNovaSenha.recordset.Fields['Mensagem'].value != "") {
			    alert(dsoNovaSenha.recordset.Fields['Mensagem'].value);
			    if (!txtSenha.disabled) {
			        window.focus();
			        txtSenha.focus();
			    }
			}
        }
    }
    else
    {    
        // Se nova senha nao foi aceita
        lockThisInterface(false);
    
        alert('Nova senha � inv�lida!');
        if (!txtSenha.disabled)
        {
			window.focus();
			txtSenha.focus();
        }
    }
}

function chkGerarSenha_onclick()
{
	if (chkGerarSenha.checked)
	{
		txtSenha.value = '';
		txtSenhaConf.value = '';
		txtSenha.disabled = true;
		txtSenhaConf.disabled = true;
	}
	else
	{
		txtSenha.disabled = false;
		txtSenhaConf.disabled = false;
		window.focus();
		txtSenha.focus();
	}
}

function lblGerarSenha_onclick()
{
	chkGerarSenha.checked = !chkGerarSenha.checked;
	chkGerarSenha_onclick();
}

function btnAjudaClicked()
{
    var retVal = window.showModalDialog( glb_pagesURLRoot + '/login/serverside/mensagem.asp', '', 
		'dialogWidth:505px;dialogHeight:500px;status:no;help:no' );
}

function btnCancelaClicked()
{
	showExtFrame(window, false);
	sendJSMessage('LOGIN_HTML', JS_LOGIN, EXECEVAL, 'lockThisInterface(false)');
	sendJSMessage('LOGIN_HTML', JS_LOGIN, EXECEVAL, 'writeInStatusBar(' + '\'' + 'login' + '\'' + ', ' + '\'' + 'cellMode' + '\'' + ', ' + '\'' + '' + '\'' + ', false)');
}
