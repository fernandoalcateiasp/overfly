/********************************************************************
userlogin.js

Library javascript para o userlogin.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_HTMLID = '';
var glb_DIVID = '';
var glb_TXTUSERNAME = '';
var glb_DSO;
var __intLogID;
var glb_BTNDEV_WIDTH = 0;
var glb_BTNDEV_HEIGHT = 0;
var glb_TrocarSenha = false;
var dsoLogUser = new CDatatransport('dsoLogUser');
var dsoDeviceID = new CDatatransport('dsoDeviceID');
var glb_startLoginAutomatico = null;
var glb_nTempoLoginAutomatico = 5; // 6 segundos
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    // inicia o controle do carregamento de interface
    initInterfaceLoad('USERLOGIN', 0X3);

    var elemSalut = window.document.getElementById('lblSalut');
    elemSalut.style.visibility = 'hidden';
    
    elemSalut.style.fontWeight = 'bold';
    var thisTime = currTimeInSecs();
    // Bom dia das 06:01 as 12:00
    if ( (thisTime >= (6*60*60 + 0*60 + 1)) && (thisTime <= (12*60*60 + 0*60 + 0)) )
        elemSalut.innerText = 'Bom dia!';
    // Boa tarde de 12:01 as 18:00
    else if ( (thisTime >= (12*60*60 + 0*60 + 1)) && (thisTime <= (18*60*60 + 0*60 + 0)) )
        elemSalut.innerText = 'Boa tarde!';
    // Boa noite de 18:01 ate 23:59:59 e das 0:00 ate 06:00
    else if ( ((thisTime >= (18*60*60 + 0*60 + 1)) && (thisTime <= (23*60*60 + 59*60 + 59))) ||
              ((thisTime >= (0*60*60 + 0*60 + 0)) && (thisTime <= (6*60*60 + 0*60 + 0))) )
        elemSalut.innerText = 'Boa noite!';            

    // manda carregar o statusbarlogin.asp
    sendJSMessage('userlogin', JS_PAGELOAD, new Array(window.top.name.toString(), 'frameStatusBar', SYS_PAGESURLROOT + '/statusbar/statusbarlogin.asp'), null);

    if ((txtUserName.value.length > 3) && (chkManterConectado.checked))
        txtSenha.value = replicate('^', 10);

    // if (window.top.glb_CURRUSERID == 1181)
    var nDeviceID = localStorage.getItem("OVERFLY_DEVICEID");
    // alert(teste);

    // Device ainda nao tem ID
    if (nDeviceID == null)
    {
        dsoDeviceID.ondatasetcomplete = dsoDeviceID_datasetcomplete;
        dsoDeviceID.URL = SYS_ASPURLROOT + '/login/serverside/getDeviceID.aspx';
        dsoDeviceID.Refresh();
    }
    else
        window_onload_1stPart();
}

function dsoDeviceID_datasetcomplete()
{
    // Grava o identificador da maquina 
    if (dsoDeviceID.recordset.RecordCount() > 0)
        localStorage.setItem("OVERFLY_DEVICEID", dsoDeviceID.recordset['DeviceID'].value);

    window_onload_1stPart();
}

/********************************************************************
Controla o carregamento total (html + checkusersLib)
********************************************************************/
function finishLoad()
{
    // funcao onclick dos A
    var i, coll;
    coll = window.document.getElementsByTagName('A');
    for ( i=0; i<coll.length; i++ )
    {
        coll.item(i).onclick = arefClick;
    }        
    // selecionar conteudo do campos textos quando em foco
    // e o tab index
    coll = window.document.getElementsByTagName('INPUT');
    for ( i=0; i<coll.length; i++ )
    {
        coll.item(i).tabIndex = 0;
        if ( (coll.item(i).type.toUpperCase() == 'TEXT') || (coll.item(i).type.toUpperCase() == 'PASSWORD') )
            coll.item(i).onfocus = selFieldContent;
    }        

    userloginBody.tabIndex = -1;    

    if ((window.document.readyState == 'complete'))
    {
        sendJSMessage('userloginHtml', JS_PAGELOADED, new Array(window.top.name.toString(), 'USERLOGIN'), 0X1);
    }

    if (chkManterConectado.checked)
    {
        writeInStatusBar('login', 'cellMode', 'Logando em ' + glb_nTempoLoginAutomatico, true);
        glb_startLoginAutomatico = window.setInterval('startLoginAutomatico()', 1000, 'JavaScript');
    }
    else {
        stopLoginAutomatico();
    }
}

function startLoginAutomatico()
{
    glb_nTempoLoginAutomatico--;

    if (glb_nTempoLoginAutomatico > 0)
    {
        writeInStatusBar('login', 'cellMode', 'Logando em ' + glb_nTempoLoginAutomatico, true);
        return;
    }

    if (glb_startLoginAutomatico != null)
    {
        window.clearInterval(glb_startLoginAutomatico);
        glb_startLoginAutomatico = null;
    }

    // Se email valido
    if ((trimStr(txtUserName.value).length > 10) && (chkManterConectado.checked))
        validadeUserInServer(window.dsoLogUser, 'userloginHtml', 'divLogin', window.txtUserName, window.txtSenha, null, window.chkManterConectado);
}

function stopLoginAutomatico()
{
    if (glb_startLoginAutomatico != null)
    {
        window.clearInterval(glb_startLoginAutomatico);
        glb_startLoginAutomatico = null;
    }

    writeInStatusBar('login', 'cellMode', '', false);

    if (txtSenha.value == replicate('^', 10))
        txtSenha.value = '';
}

/********************************************************************
Previne navegacao por A se dasabilitado
********************************************************************/
function arefClick()
{
    if ( this.disabled )
        window.event.returnValue = false;
}

/********************************************************************
Limpa campos textos
********************************************************************/
function btnCancel_onclick()
{
    txtUserName.value="";
    txtSenha.value="";
    txtUserName.focus();
}

/********************************************************************
Limpa campos textos ao sair do html
********************************************************************/
function window_onunload()
{
    txtUserName.value="";
    txtSenha.value="";
}

/********************************************************************
Transfere keydown para btnOK_Click
********************************************************************/
function txtUserName_onkeydown()
{
    if ( event.keyCode == 13 )
        btnOK_onclick();
}

/********************************************************************
Transfere keydown para btnOK_Click
********************************************************************/
function txtSenha_onkeydown()
{
    if ( event.keyCode == 13 )
        btnOK_onclick();
}

/********************************************************************
Tenta logar o usuario
********************************************************************/
function btnOK_onclick()
{
    validadeUserInServer(window.dsoLogUser, 'userloginHtml', 'divLogin', window.txtUserName, window.txtSenha, null, window.chkManterConectado);
}

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
            case JS_PAGELOADED :
            {
                if ( param1[0].toUpperCase() == window.top.name.toUpperCase())
                {                
                    if ( param1[1].toUpperCase() == getInterfaceName() )
                    {
                        if ( componentOfInterfaceLoaded(param2) == true )
                        {
                            // encerra o timer disparado no window on load
                            if ( __intLogID != null )
                            {
                                window.clearInterval(__intLogID);    
                                __intLogID = null;
                            }
                            
                            var frameID;
                            var frameDim = new Array();
                            var y;
                            
                            // Configura a interface
                            // ajusta o statusbar
                            frameID = getFrameIdByHtmlId('statusbarloginHtml');
                            moveFrameInHtmlTop(frameID, new Array(0, 0, MAX_FRAMEWIDTH + ELEM_GAP, 24));
                            // ajusta o userlogin pelo statusbar                   
                            frameDim = getRectFrameInHtmlTop(frameID);
                            moveExtFrame(window, new Array(0, (frameDim[1] + frameDim[3] + 1), MAX_FRAMEWIDTH, (MAX_FRAMEHEIGHT - (frameDim[1] + frameDim[3] + 78))));
                            // escreve no statusBar
                            writeInStatusBar('login', 'cellPrgName', SYS_NAME);
                            writeInStatusBar('login', 'cellForm', 'Form Principal');
                            
                            // mostra o frame do statusbar
                            showFrameInHtmlTop(frameID, true);
                            // mostra o frame do user login
                            showExtFrame(window, true);
                                                                            
                            // libera a interface
                            window.document.getElementById('btnOK').disabled = false;
                                                    
                            // coloca foco no txtUserName
                            try
                            {
                                window.focus();
                                if ( (txtUserName.value == null) || (txtUserName.value == '') )
                                    window.document.getElementById('txtUserName').focus();
                                else
                                    window.document.getElementById('txtSenha').focus();
                                
                                return true;    
                            }
                            catch(e)    
                            {
                                return null;
                            }
                        }
                    }    
                }    
            }
            break;
        
            case JS_LOGIN :
            {
                if ((idElement == 'LOGIN_HTML') && (param1 == EXECEVAL))
                {
                    return eval(param2);
                }
            }
            break;
        
        default:
            return null;
    }
}

/********************************************************************
Funcao complementar do window_onload
********************************************************************/
function window_onload_1stPart()
{
    // esconde todos os frames
    showAllFramesInHtmlTop(false);

    // txtUserName.value="";
    // txtSenha.value="";
    
    var vGap = 0;
    var txtHeight = 24;
    var i;
    var nLangID = 0;

    // Incluida as proximas linhas para controle dos itens da tela
    var elem;

    elem = window.document.getElementById('lblSalut');
    with (elem.style)
    {
        backgroundColor = 'transparent';
        fontWeight = 'bold';
        fontSize = '10pt';
        left = 0;
        top = 0;
        width = 100;
        height = 16;
        vGap = parseInt(top, 10) /*+ parseInt(height, 10)*/;
    }
    
    elem = window.document.getElementById('lblUserName');
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = 0;
        top = vGap + ELEM_GAP;
        width = 100;
        height = 16;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    elem = window.document.getElementById('txtUserName');
    with (elem)
    {
        tabIndex = 1;
    }    
    
    with (elem.style)
    {
        left = 0;
        top = vGap;
        height = txtHeight;
        width = '244px';
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    elem = window.document.getElementById('lblSenha');
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = 0;
        top = vGap + ELEM_GAP;
        width = 100;
        height = 16;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    elem = window.document.getElementById('txtSenha');
    with (elem)
    {
        tabIndex = 1;
    }

    with (elem.style)
    {
        left = 0;
        top = vGap;
        height = txtHeight;
        width = '244px';
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    elem = window.document.getElementById('chkInicioJornada');
    elem.disabled = true;
    with (elem.style)
    {
        left = -2;
        // top = vGap + 9;
        top = vGap + 16;
        height = 19;
        width = 19;
        //vGap = parseInt(top, 10) + parseInt(height, 10);
        visibility = 'hidden';
    }
    
    elem = window.document.getElementById('lblInicioJornada');
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = chkInicioJornada.offsetLeft + chkInicioJornada.offsetWidth + 4;
        top = chkInicioJornada.offsetTop + 4;
        width = 100;
        height = 16;
        vGap = parseInt(top, 10) + parseInt(height, 10);
        visibility = 'hidden';
    }
    elem.onclick = lblInicioJornada_onclick;

/*
    elem = window.document.getElementById('btnOK').style;
    with (elem)
    {
        left = 0;
        top = vGap + ELEM_GAP;
        width = 75;
        height = 24;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    elem = window.document.getElementById('btnInfo').style;
    with (elem)
    {
        left = parseInt(btnOK.currentStyle.left, 10) + parseInt(btnOK.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(btnOK.currentStyle.top, 10);
        width = 75;
        height = 24;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    elem = window.document.getElementById('btnTrocaSenha').style;
    with (elem)
    {
        left = parseInt(btnInfo.currentStyle.left, 10) + parseInt(btnInfo.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(btnInfo.currentStyle.top, 10);
        width = 75;
        height = 24;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    */

	elem = window.document.getElementById('selLinguas');
	with (elem.style)
	{
		top = chkInicioJornada.offsetTop;
		width = 244;
		left = (divLogin.offsetWidth - divLogin.offsetWidth) / 2;
	}

	elem = window.document.getElementById('chkManterConectado');
	with (elem) {
	    tabIndex = 2;
	}

	with (elem.style) {
	    left = 0;
	    // top = selLinguas.offsetTop + ELEM_GAP;
	    top = vGap + ELEM_GAP + 3;
	    height = 19;
	    width = 19;
	    vGap = parseInt(top, 10) + parseInt(height, 10);
	}


	elem = window.document.getElementById('lblManterConectado');
	with (elem.style) {
	    backgroundColor = 'transparent';
	    left = chkManterConectado.offsetLeft + chkManterConectado.offsetWidth + 2;
	    top = chkManterConectado.offsetTop + 1;
	    // width = 200;
	    width = 105;
	    height = 16;
	    fontSize = '10pt';
	    cursor = 'hand';
	    vGap = parseInt(top, 10) + parseInt(height, 10);
	}

	elem = window.document.getElementById('btnOK').style;
	with (elem) {
	    left = 0;
	    // top = vGap + ELEM_GAP;
	    top = vGap + ELEM_GAP + 5;
	    width = 75;
	    height = 24;
	    vGap = parseInt(top, 10) + parseInt(height, 10);
	}

	elem = window.document.getElementById('btnInfo').style;
	with (elem) {
	    left = parseInt(btnOK.currentStyle.left, 10) + parseInt(btnOK.currentStyle.width, 10) + ELEM_GAP;
	    top = parseInt(btnOK.currentStyle.top, 10);
	    width = 75;
	    height = 24;
	    vGap = parseInt(top, 10) + parseInt(height, 10);
	}

	elem = window.document.getElementById('btnTrocaSenha').style;
	with (elem) {
	    left = parseInt(btnInfo.currentStyle.left, 10) + parseInt(btnInfo.currentStyle.width, 10) + ELEM_GAP;
	    top = parseInt(btnInfo.currentStyle.top, 10);
	    width = 75;
	    height = 24;
	    vGap = parseInt(top, 10) + parseInt(height, 10);
	}

    elem = window.document.getElementById('divLogin');
    with (elem.style)
    {
        backgroundColor = 'transparent';
        border = 'none';
        left = 10;
        top = 10;
        width = parseInt(btnTrocaSenha.currentStyle.left, 10) + parseInt(btnTrocaSenha.currentStyle.width, 10) + 1;
        height = vGap + 1;
        vGap = parseInt(top);
    }
    
    elem = window.document.getElementById('divNotes');
    with (elem.style)
    {
        backgroundColor = 'transparent';
        visibility = 'hidden';
        border = 'none';
        left = 10;
        top = 10;
        width = MAX_FRAMEWIDTH - (2 * parseInt(left));
        height = 406;
    }

    elem = window.document.getElementById('btnLogin').style;
    with (elem)
    {
        left = parseInt(btnOK.currentStyle.left, 10);
        top = parseInt(divNotes.currentStyle.height, 10) - 24;
        width = 75;
        height = 24;
    }

    maxLengthData();
    
    // Username cookie
    //if ( get_Cookie('username') != null )
		// txtUserName.value = get_Cookie('username');
    
    // LanguageCookie
    nLangID = get_Cookie('languageid');
    
    if ( nLangID != null )
    {
		for (i=0; i<selLinguas.options.length; i++)
		{
			if ( nLangID == selLinguas.options.item(i).value )
			{
				selLinguas.options.item(i).selected = true;
 		    	break;
			}
		}
		
		window.top._glb_currDicLang = nLangID;
    }
    
    // mostra a janela    
    elem = window.document.getElementById('userloginBody');
    elem.style.visibility = 'visible';
    
    __intLogID = window.setInterval('finishLoad()', 100, 'JavaScript');
}

function lblManterConectadoClicked()
{
    chkManterConectado.checked = !chkManterConectado.checked;
    chkManterConectadoOnClick();
}

function chkManterConectadoOnClick()
{
    stopLoginAutomatico();
}

/********************************************************************
Quantidade maxima de caracteres digitados
********************************************************************/
function maxLengthData()
{
    txtUserName.maxLength = MAX_USERNAME;
    txtSenha.maxLength = MAX_SENHA;
}

/********************************************************************
Usuario clicou lblInicioJornada
********************************************************************/
function lblInicioJornada_onclick()
{
	chkInicioJornada.checked = !chkInicioJornada.checked;
}

/********************************************************************
Esta funcao valida o usuario do lado do servidor

Parametros:
dso                     - controle RDS que valida o usuario
htmlID                  - id da tag HTML
divID                   - id do div a bloquear elementos durante a validacao
elemName                - campo texto do nome
elemSenha               - campo texto da senha
showInfo                - null ou true se for para mostrar o div de informacoes
manterConectado         - manter conectado

Retornos:
Nenhum

Se o usuario e validado, o programa carrega o menu principal do usuario.
Se o usuario nao e validado, o programa permanece na pagina de log
de usuario.
********************************************************************/
function validadeUserInServer(dso, htmlID, divID, elemName, elemSenha, showInfo, elemManterConectado)
{
    var strToTrim = new String();
    var bManterConectado = false;

    // trima os campos
    strToTrim = elemName.value;
    elemName.innerText = trimStr(strToTrim);
    
    strToTrim = elemSenha.value;
    elemSenha.innerText = trimStr(strToTrim);
    bManterConectado = elemManterConectado.checked;
    
    // verifica se os campos tem conteudo
    if (elemName.value.length == 0)
    {
        stopLoginAutomatico();
        alert("Preencher Usu�rio.");
        elemName.focus();
        return;
    }
    if (elemSenha.value.length == 0)
    {
        stopLoginAutomatico();
        alert('Preencher Senha.');
        elemSenha.focus();
        return;
    }
    
    // desabilita interface
    lockThisInterface(true);
    
    // if ((showInfo == null) && (!glb_TrocarSenha))
    //     writeInStatusBar('login', 'cellMode', 'Logando', true);
    
    // salva htmlID, divID e txtUserNameID
    glb_HTMLID = htmlID;
    glb_DIVID = divID;
    glb_TXTUSERNAME = elemName;
    glb_DSO = dso;
    
    // valida o usu�rio do lado do servidor
    var strPars = new String();

    strPars = '?userName=';
    strPars += escape(elemName.value);
    strPars += '&senha=';
    strPars += escape(elemSenha.value);
    strPars += '&manterConectado=' + escape(bManterConectado ? 1 : 0);
    strPars += '&deviceID=' + escape(localStorage.getItem("OVERFLY_DEVICEID"));

    if ( showInfo == null )
        dso.ondatasetcomplete = dsoRetUserValidation;
    else
        dso.ondatasetcomplete = dsoRetUserShowInfo;
    
    dso.URL = SYS_ASPURLROOT + '/login/serverside/validateuser.aspx'+strPars;
    dso.Refresh();
}

/********************************************************************
Esta funcao recebe o retorno da validacao do usuario pelo controle dso,
para login no Overfly e prosseguir com a abertura do sistema

Parametros:
Nenhum

Retornos:
Nenhum

Se usuario valido mostra o div de informacoes
Se usuario invalido chama funcao retypeUser abaixo
********************************************************************/
function dsoRetUserShowInfo()
{
    if (glb_DSO.recordset.RecordCount() == 0)
        retypeUser();
    else if (glb_DSO.recordset['AcessoLiberado'].value == 0)
	{
		// habilita interface
		lockThisInterface(false);
	    
		// Mensagem
		alert(glb_DSO.recordset['Mensagem'].value);
	    
		// Controle a receber foco
		window.glb_TXTUSERNAME.focus();
	}    
    else
    {
        showDivLoginOrDivInfo(btnInfo);
    }    
}

/********************************************************************
Esta funcao recebe o retorno da validacao do usuario pelo controle dso,
para login no Overfly e prosseguir com a abertura do sistema

Parametros:
Nenhum

Retornos:
Nenhum

Se usuario valido manda carregar o mainMenu do usuario
Se usuario invalido chama funcao retypeUser abaixo
********************************************************************/
function dsoRetUserValidation()
{
    if (glb_DSO.recordset.RecordCount() == 0)
        retypeUser();
    else if ((glb_DSO.recordset['AcessoLiberado'].value == 0) &&
			 (glb_DSO.recordset['ForcarTroca'].value  == 0))
	{
        stopLoginAutomatico();

        // habilita interface
		lockThisInterface(false);
	    
		// Mensagem
		alert(glb_DSO.recordset['Mensagem'].value );
	    
		// Controle a receber foco
		window.glb_TXTUSERNAME.focus();
	}
    else
    {
		// Cookie do user Name
        // set_Cookie('username', txtUserName.value);
        // Cookie da lingua
        set_Cookie('languageid', selLinguas.value);
        window.top._glb_currDicLang = selLinguas.value;

        // guarda o PessoaID em variavel do browser mae
        window.top.glb_USERNAME = window.glb_TXTUSERNAME.value;
        window.top.glb_USERFANTASIA = glb_DSO.recordset['Usuario'].value ;
        window.top.glb_CURRUSERID = glb_DSO.recordset['PessoaID'].value ;
        window.top.glb_CURRUSEREMAIL = glb_DSO.recordset['PessoaEmail'].value ;
        window.top.glb_PRINTERBARCODE = glb_DSO.recordset['ImpressoraCodigoBarra'].value;
        
        if ((glb_DSO.recordset['ForcarTroca'].value  == 1) || glb_TrocarSenha)
		{
			alert("Sua senha expirou.\nPor favor, altere-a.");		
			if (glb_TrocarSenha && (glb_DSO.recordset['PermiteTrocarSenha'].value  == 0))
			{
			    alert(glb_DSO.recordset['Mensagem'].value );
				lockThisInterface(false);
			}
			else
				userNeedNewPassword();
			
			glb_TrocarSenha = false;
		}
        else
			sendJSMessage('userloginHtml', JS_USERLOAD, null, null);    
    }    
}

/********************************************************************
Usuario necessita cadastrar nova senha

Parametros:
Nenhum

Retornos:
Nenhum
********************************************************************/
function userNeedNewPassword()
{
    lockThisInterface(true);
    sendJSMessage('userloginHtml', JS_NEWPASSWORD, 'SHOW_WIN', null);
    return null;
}

/********************************************************************
Habilita ou desabilita os elementos da interface

Parametros:
cmdLock     - true desabilita, false habilita

Retornos:
Nenhum
********************************************************************/
function lockThisInterface(cmdLock)
{
    var i, j, coll;
    
    // Inputs
    coll = window.document.getElementsByTagName('INPUT');
    for ( i=0; i<coll.length; i++ )
    {
        coll.item(i).disabled = cmdLock;
    }    
    
    // A
    coll = window.document.getElementsByTagName('A');
    for ( i=0; i<coll.length; i++ )
    {
        coll.item(i).disabled = cmdLock;
    }    
}

/********************************************************************
Esta funcao desbloqueia os controles de login do usuario
e pede novo userName e Senha.
Ela e chamada pela rotina de validacao de usuario caso o usuario
nao tenha sido validado.

Parametros:
divID                   - id do div a desbloquear os elementos
elemIDfocus             - campo a receber o foco

Retornos:
Nenhum
********************************************************************/
function retypeUser()
{
    // habilita interface
    lockThisInterface(false);
    
    // Mensagem
    alert('Usu�rio n�o autorizado.');
    
    // Controle a receber foco
    window.glb_TXTUSERNAME.focus();
}

/********************************************************************
Coloca cookies
********************************************************************/
function set_Cookie(cookieName, cookieValue)
{
    if (navigator.cookieEnabled)
    {
        var dayInterval = 24 * 60 * 60 * 1000;
        var i;
        
        var expDate = new Date();
        
        for ( i=0; i<30; i++ )
            expDate.setTime(expDate.getTime() + dayInterval);
        
        window.document.cookie = cookieName + '=' + escape(cookieValue) + ';' +
            'expires=' + expDate.toGMTString();
    }    
}

/********************************************************************
Recupera cookies
********************************************************************/
function get_Cookie(cookieName)
{
	var cookieValue = null;
	
    if (navigator.cookieEnabled)
    {
        if( getCookCount(cookieName) != 0 )
        {
            cookieValue = getCookieNum(cookieName, getCookCount(cookieName));
            if ( cookieValue != null )
                return cookieValue;
        }    
    }    
}

/********************************************************************
Conta numero de cookies
********************************************************************/
function getCookCount (name)
{
    var result = 0;
    var myCookie = ' ' + window.document.cookie + ';';
    var searchName = ' ' + name + '=';
    var nameLength = searchName.length;
    var startOfCookie = myCookie.indexOf(searchName);
        
    while ( startOfCookie != -1 )
    {
        result += 1;
        startOfCookie = myCookie.indexOf(searchName, startOfCookie + nameLength);
    }
        
    return result;
}

/********************************************************************
Obtem o um cookie
********************************************************************/
function getCookieNum(name, cookieNum)
{
    var result = null;
    
    if ( cookieNum >=1 )
    {
        var myCookie = ' ' + window.document.cookie + ';';
        var searchName = ' ' + name + '=';
        var nameLength = searchName.length;
        var startOfCookie = myCookie.indexOf(searchName);
        var cntr = 0;
        
        for ( cntr = 1; cntr < cookieNum; cntr++ )
            startOfCookie = myCookie.indexOf(searchName, startOfCookie + nameLength);
            
        if ( startOfCookie != -1 )    
        {
            startOfCookie += nameLength;
            var endOfCookie = myCookie.indexOf(';', startOfCookie);
            result = unescape(myCookie.substring(startOfCookie, endOfCookie));
        }
        
        return result;
    }
    
    return result;
}

/********************************************************************
Loga o usuario com a nova senha atravez da pagina newpassword
********************************************************************/
function logUserWithNewPassword()
{
    __intLogID = window.setInterval('logUserWithNewPassword_Efect()', 50, 'JavaScript');
    
    return true; 
}

/********************************************************************
Efetivamente loga o usuario com a nova senha atravez da pagina newpassword
********************************************************************/
function logUserWithNewPassword_Efect()
{
    if ( __intLogID != null )
    {
        window.clearInterval(__intLogID);
        __intLogID = null;
    }
    
    // set_Cookie('username', txtUserName.value);
    
    // Oculta o frame de nova senha e descarrega a pagina 
    var frameRef = getFrameInHtmlTop('frameGen02');
    showFrameInHtmlTop(frameRef, false);
    moveFrameInHtmlTop(frameRef, new Array(0,0,0,0));
    //frameRef.src = '';
    top.frames(frameRef.name).document.location.replace('');
        
    sendJSMessage('userloginHtml', JS_USERLOAD, null, null);    
}

/********************************************************************
Esconde o div de login e mostra o de informacoes.
Esconde o btnDeveloper se for o caso
********************************************************************/
function btnLoginInfo_onclick(ctl)
{
    stopLoginAutomatico();
    showDivLoginOrDivInfo(ctl);
    if ( ctl == btnInfo )
        showDivLoginOrDivInfo(ctl);
        // validadeUserInServer(window.dsoLogUser, 'userloginHtml', 'divLogin', window.txtUserName, window.txtSenha, false, true);
    else
        showDivLoginOrDivInfo(ctl);
}

/********************************************************************
Esconde o div de login e mostra o de informacoes.
Esconde o btnDeveloper se for o caso
********************************************************************/
function showDivLoginOrDivInfo(ctl)    
{
    var elem;
    
    // habilita interface
    lockThisInterface(false);
    
    if ( ctl.id == 'btnInfo' )
    {
        divLogin.style.visibility = 'hidden';

        elem = window.document.getElementById('btnDeveloper');
        if ( elem != null )
        {
            elem.disabled = true;
            glb_BTNDEV_WIDTH = parseInt(elem.currentStyle.width, 10);
            glb_BTNDEV_HEIGHT = parseInt(elem.currentStyle.height, 10);
            elem.style.width = 0;
            elem.style.height = 0;
        }    
    
        divNotes.style.visibility = 'inherit';    
        btnLogin.focus();
    }
    else if ( ctl.id == 'btnLogin' )
    {
        divNotes.style.visibility = 'hidden';

        elem = window.document.getElementById('btnDeveloper');
        if ( elem != null )
        {
            elem.disabled = false;
            elem.style.width = glb_BTNDEV_WIDTH;
            elem.style.height = glb_BTNDEV_HEIGHT;
        }    
    
        divLogin.style.visibility = 'inherit'; 
        
        txtUserName.focus();
    }
}

function btnTrocarSenha_onclick()
{
	glb_TrocarSenha = true;
	validadeUserInServer(window.dsoLogUser, 'userloginHtml', 'divLogin', window.txtUserName, window.txtSenha);
}
