<%@  language="VBScript" %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalnewpasswordHtml" name="modalpasswordHtml">

<head>
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">

    <title></title>

    <%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/login/newpassword.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
        
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/login/newpassword.js" & Chr(34) & "></script>" & vbCrLf

    Dim rsData, strSQL

    Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT TOP 1 TamanhoMinimo, TamanhoMaximo, NumeroCaracteresRepetidos " & _
			"FROM Recursos_Senhas " & _
			"WHERE RecursoID=999 AND Tipo=1"

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	Response.Write "<script LANGUAGE='JavaScript' ID='globalvars'>" & vbCrLf
	Response.Write "var glb_nTamanhoMinimo = 0;" & vbCrLf
	Response.Write "var glb_nTamanhoMaximo = 0;" & vbCrLf
	Response.Write "var glb_nNumeroCaracteresRepetidos = 0;" & vbCrLf
	Response.Write "var glb_pagesURLRoot = '" & pagesURLRoot & "';" & vbCrLf
		
    If (NOT(rsData.BOF AND rsData.EOF)) Then
		rsData.MoveFirst

		Response.Write "glb_nTamanhoMinimo = " & CStr(rsData.Fields("TamanhoMinimo").Value) & ";" & vbCrLf
		Response.Write "glb_nTamanhoMaximo = " & CStr(rsData.Fields("TamanhoMaximo").Value) & ";" & vbCrLf
		Response.Write "glb_nNumeroCaracteresRepetidos = " & CStr(rsData.Fields("NumeroCaracteresRepetidos").Value) & ";" & vbCrLf
	End If
	Response.Write "</script>" & vbCrLf 

    rsData.Close
    Set rsData = Nothing
    %>

    <script id="wndJSProc" language="javascript">
<!--

    //-->
    </script>

</head>

<body id="modalnewpasswordBody" name="modalnewpasswordBody" language="javascript" onload="return window_onload()">

    <div id="divCaption" name="divCaption" class="divCaptionGen">
        <p id="lblCaption" name="lblCaption" class="lblGeneral"></p>
    </div>

    <div id="divNotes" name="divNotes" class="divGeneral">
        <input type="image" id="bnAjuda" name="bnAjuda" src="../images/btnAjuda.gif" class="fldGeneral" title="Ajuda" onclick="javascript: return btnAjudaClicked()">
        <!-- ATENCAO: PARA MEXER NO TEXTO ABAIXO USAR SIMBOLOGIA DE ESPACO -->
        <p id="lblUsuario" name="lblUsuario"></p>
        <p>
            Digite&nbsp;sua&nbsp;nova&nbsp;senha.
        <!-- FINAL DE ATENCAO: PARA MEXER NO TEXTO ABAIXO USAR SIMBOLOGIA DE ESPACO -->
    </div>

    <div id="divSenha" name="divSenha" class="divGeneral">
        <p id="lblSenha" name="lblSenha" class="lblGeneral">Senha</p>
        <input type="password" id="txtSenha" name="txtSenha" class="fldGeneral"></input>
        <p id="lblSenhaConf" name="lblSenhaConf" class="lblGeneral">Redigite a senha</p>
        <input type="password" id="txtSenhaConf" name="txtSenhaConf" class="fldGeneral"></input>
        <p id="lblGerarSenha" name="lblGerarSenha" class="lblGeneral" onclick="lblGerarSenha_onclick()">Gerar automaticamente a nova senha</p>
        <input type="checkbox" id="chkGerarSenha" name="chkGerarSenha" class="fldGeneral" title="Deseja que o sistema gere a sua senha?" onclick="chkGerarSenha_onclick()">
        <input type="button" id="btnOK" name="btnOK" value="OK" class="btnGeneral"></input>
        <input type="button" id="btnCancela" name="btnCancela" value="Cancela" class="btnGeneral"></input>
    </div>

</body>

</html>
