﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Overfly3.login
{
    public partial class userlogin : System.Web.UI.OverflyPage
    //public partial class userlogin : System.Web.UI.Page
    {
        public bool bManterConectado;
        public string sEmail;
        public string sQuery;

        private string DeviceID;
        public string deviceID
        {
            get { return DeviceID; }
            set { DeviceID = value; }
        }

        //protected void Page_Load(object sender, EventArgs e)
        protected override void PageLoad(object sender, EventArgs e)
        {
            this.bManterConectado = false;
            this.sEmail = "";
            System.Data.DataSet dsLogin;
            sQuery = "";

            if ((deviceID != null) && (deviceID != "0"))
            {
                OVERFLYSVRCFGLib.OverflyMTS objSvrCfg = new OVERFLYSVRCFGLib.OverflyMTS();

                string pagesURLRoot = objSvrCfg.PagesURLRoot(
                       System.Configuration.ConfigurationManager.AppSettings["application"]);

                pagesURLRoot = pagesURLRoot.Replace("/commonpages", "");
                pagesURLRoot = pagesURLRoot.Replace("/com", "");
                pagesURLRoot = pagesURLRoot.Replace("/co", "");

                sQuery = "SELECT TOP 1 a.* " +
                    "FROM [LoginServer].[OVERFLY].[dbo].[LOGs_Login] a WITH(NOLOCK) " +
                    "WHERE (a.Web=0 AND a.URL='" + pagesURLRoot + "' AND a.DeviceID=" + this.DeviceID + ") " +
                    "ORDER BY a.Data DESC";
                dsLogin = DataInterfaceObj.getRemoteData(sQuery);

                if (dsLogin.Tables[1].Rows.Count > 0)
                {
                    this.bManterConectado = (bool)dsLogin.Tables[1].Rows[0]["ManterConectado"];
                    this.sEmail = dsLogin.Tables[1].Rows[0]["UserName"].ToString();
                }
            }
        }
    }
}