﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="userlogin.aspx.cs" Inherits="Overfly3.login.userlogin" %>

<!-- <!DOCTYPE html> -->

<%         
    string pagesURLRoot, dbURLRoot, strConn, aplicacaoRoot, sAcessoriesPath;
    bool DevMode;

    OVERFLYSVRCFGLib.OverflyMTS objSvrCfg = new OVERFLYSVRCFGLib.OverflyMTS();

    WSData.dataInterface oDataInterface = new WSData.dataInterface(
           System.Configuration.ConfigurationManager.AppSettings["application"]
       );
    pagesURLRoot = objSvrCfg.PagesURLRoot(
           System.Configuration.ConfigurationManager.AppSettings["application"]);

    dbURLRoot = objSvrCfg.DatabaseASPPagesURLRoot(
        System.Configuration.ConfigurationManager.AppSettings["application"]);

    sAcessoriesPath = dbURLRoot + "/OverSetup.msi";
    aplicacaoRoot = pagesURLRoot;

    DevMode = objSvrCfg.DevMode(System.Configuration.ConfigurationManager.AppSettings["application"]);
    strConn = objSvrCfg.DataBaseStrConn(System.Configuration.ConfigurationManager.AppSettings["application"]);
%>

<HTML id="userloginHtml" name="userloginHtml">

<head>
    <title></title>

<%
    //Links de estilo, bibliotecas da automacao e especificas
    Response.Write("<LINK REL=" + (char)34 + "stylesheet" + (char)34 + " HREF=" + (char)34 + pagesURLRoot + "/login/userlogin.css" + (char)34 + "type=" + (char)34 + "text/css" + (char)34 + ">" + "\r\n");

    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/Defines.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CRecordsetParser.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CDatatransportSystem.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CReturnParam.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CFieldStructure.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CField.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CFields.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CRecordset.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CDatatransport.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CReturnParam.js" + (char)34 + "></script>" + "\r\n");

    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_sysbase.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_constants.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_htmlbase.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_strings.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_interface.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_statusbar.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_fastbuttonsbar.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_controlsbar.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_modalwin.js" + (char)34 + "></script>" + "\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_rdsfacil.js" + (char)34 + "></script>" + "\r\n");

    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/login/userlogin.js" + (char)34 + "></script>" + "\r\n");
%>

<%
    //Insere facilidades para o desenvolvedor: btnDeveloper

    if (DevMode)
    {
        /*********************************************************************
        Script para style do botao btnDeveloper
        ********************************************************************/
        Response.Write("<style type=" + (char)34 + "text/css" + (char)34 + ">");


        Response.Write("\r\n");
        Response.Write("#btnDeveloper");


        Response.Write("\r\n");
        Response.Write("{");


        Response.Write("\r\n");
        Response.Write("visibility: hidden;");


        Response.Write("\r\n");
        Response.Write("top: 0px;");


        Response.Write("\r\n");
        Response.Write("left: 0px;");
        Response.Write("\r\n");
        Response.Write("height: 24px;");


        Response.Write("\r\n");
        Response.Write("width: 126px;");


        Response.Write("\r\n");
        Response.Write("}");


        Response.Write("\r\n");
        Response.Write("</style>");


        Response.Write("\r\n");
        Response.Write("\r\n");
        /********************************************************************
        Script para evento onmousemove
        ********************************************************************/
        Response.Write("<SCRIPT LANGUAGE=javascript FOR=document EVENT=onmousemove>");


        Response.Write("\r\n");
        Response.Write("<!--");


        Response.Write("\r\n");
        Response.Write("document_onmousemove()");


        Response.Write("\r\n");
        Response.Write("//-->");


        Response.Write("</SCRIPT>");


        Response.Write("\r\n");
        Response.Write("\r\n");
        /********************************************************************
        Script para funcoes de uso do desenvolvedor
        ********************************************************************/
        Response.Write("<SCRIPT ID=devMode LANGUAGE=javascript>");
        Response.Write("\r\n");
        Response.Write("<!--");
        Response.Write("\r\n");

        /********************************************************************
        Ajusta e mostra o botao do desenvolvedor
        ********************************************************************/
        Response.Write("function document_onmousemove()");
        Response.Write("\r\n");
        Response.Write("{");
        Response.Write("try");
        Response.Write("\r\n");
        Response.Write("{");
        Response.Write("\r\n");

        Response.Write("window.btnDeveloper.style.left = " +
                       "parseInt(divLogin.style.left, 10) + " +
                       "parseInt(divLogin.style.width, 10) + ELEM_GAP;");

        Response.Write("window.btnDeveloper.style.top = " +
                       "parseInt(divLogin.style.top, 10) + " +
                       "parseInt(btnOK.style.top, 10);");

        Response.Write("window.btnDeveloper.style.visibility = 'visible';");
        Response.Write("\r\n");
        Response.Write("}");
        Response.Write("\r\n");
        Response.Write("catch(e)");
        Response.Write("\r\n");
        Response.Write("{");
        Response.Write("\r\n");
        Response.Write(";");
        Response.Write("\r\n");
        Response.Write("}");
        Response.Write("\r\n");
        Response.Write("}");
        Response.Write("\r\n");
        Response.Write("\r\n");

        /********************************************************************
        Loga o usuario. Uso esclusivo do desenvolvedor.
        ********************************************************************/
        Response.Write("function btnDeveloper_onclick()");
        Response.Write("\r\n");
        Response.Write("{");
        Response.Write("\r\n");
        Response.Write("lockThisInterface(true);");
        Response.Write("\r\n");
        Response.Write("var strPars = new String();");
        Response.Write("\r\n");
        Response.Write("strPars = '?';");
        Response.Write("\r\n");
        Response.Write("strPars += 'userName=';");
        Response.Write("\r\n");
        Response.Write("strPars += escape('desenvolvimento@alcateia.com.br');");
        Response.Write("\r\n");
        Response.Write("strPars += '&';");
        Response.Write("\r\n");
        Response.Write("strPars += 'senha=';");
        Response.Write("\r\n");
        Response.Write("strPars += escape('dev$15#ove');");
        Response.Write("\r\n");

        Response.Write("strPars += '&entrada=';");
        Response.Write("\r\n");
        Response.Write("if ( chkInicioJornada.checked )");
        Response.Write("\r\n");
        Response.Write("strPars += escape(1);");
        Response.Write("\r\n");
        Response.Write("else");
        Response.Write("\r\n");
        Response.Write("strPars += escape(0);");
        Response.Write("\r\n");

        Response.Write("strPars += '&manterConectado=';");
        Response.Write("\r\n");
        Response.Write("if ( chkManterConectado.checked )");
        Response.Write("\r\n");
        Response.Write("strPars += escape(1);");
        Response.Write("\r\n");
        Response.Write("else");
        Response.Write("\r\n");
        Response.Write("strPars += escape(0);");
        Response.Write("\r\n");

        Response.Write("strPars += '&deviceID=0';");
        Response.Write("\r\n");

        Response.Write("dsoLogUser.ondatasetcomplete = dsoRetUserValidationDev;");
        Response.Write("\r\n");
        Response.Write("dsoLogUser.URL=" + (char)39 + dbURLRoot + (char)39 + "+'/login/serverside/validateuser.aspx'+strPars;");
        Response.Write("\r\n");
        Response.Write("dsoLogUser.Refresh();");
        Response.Write("\r\n");
        Response.Write("}");
        Response.Write("\r\n");
        Response.Write("\r\n");

        /********************************************************************
        Esta funcao recebe o retorno da validacao do usuario pelo controle dso

        Parametros:
        Nenhum

        Retornos:
        Nenhum
        ********************************************************************/
        Response.Write("function dsoRetUserValidationDev()");
        Response.Write("{");
        Response.Write("\r\n");
        Response.Write("// guarda o PessoaID em variavel do browser mae");
        Response.Write("\r\n");
        Response.Write("set_Cookie(" + (char)39 + "languageid" + (char)39 + ", selLinguas.value);" + "\r\n");
        //Comentado por conta do modelo novo de senhas, agora a senha do desenvolvedor vai ser dura, para evitar a 
        //troca periodica de senha que o sistema passa a fazer.
        Response.Write("window.top._glb_currDicLang = selLinguas.value; " + "\r\n");
        Response.Write("window.top.glb_CURRUSERID = dsoLogUser.recordset[" + (char)39 + "PessoaID" + (char)39 + "].value;" + "\r\n");
        Response.Write("window.top.glb_CURRUSEREMAIL = dsoLogUser.recordset[" + (char)39 + "PessoaEmail" + (char)39 + "].value;" + "\r\n");
        Response.Write("window.top.glb_PRINTERBARCODE = dsoLogUser.recordset[" + (char)39 + "ImpressoraCodigoBarra" + (char)39 + "].value;" + "\r\n");

        Response.Write("\r\n");
        Response.Write("sendJSMessage('checkusersystemHtml', JS_USERLOAD, null, null);");
        Response.Write("}");

        Response.Write("\r\n");
        Response.Write("//-->");
        Response.Write("\r\n");
        Response.Write("</SCRIPT>");
    }
%>

<SCRIPT ID=wndJSProc LANGUAGE=javascript>
<%
    // Response.Write("var glb_Query = '" + this.sQuery + "';");
%>
<!--
/********************************************************************
Descritivo da interface
1. Nome da interface = 'USERLOGIN'
2. Componentes:
Composicao da interface:
html/asp               bin  hex    frame               observacao
--------               ---  ---    -----               ----------
userlogin.asp          001  0X1    frameGen01
statusbarlogin.asp     010  0X2    frameStatusBar

toda interface         111  0X3
********************************************************************/

//-->
</SCRIPT>

<SCRIPT ID=vbFunctions LANGUAGE=vbScript>
<!--

Function currTimeInSecs()
    Dim theTime, theHour, theMinute, theSecond
    
    theTime = Now
    theHour = Hour(theTime)
    theMinute = Minute(theTime)
    theSecond = Second(theTime)
    
    currTimeInSecs = theSecond + (theMinute * 60) + (theHour * 60 * 60)    
    
End Function

//-->
</SCRIPT>

</head>

<BODY id="userloginBody" name="userloginBody" LANGUAGE=javascript onload="return window_onload()" onunload="return window_onunload()">

    <DIV id="divNotes" name="divNotes" class="divExtern" >
        <!-- ATENCAO: PARA MEXER NO TEXTO ABAIXO USAR SIMBOLOGIA DE ESPACO -->
        <P><STRONG>Requisitos</STRONG>:

        <BR>•&nbsp;Computador:
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Configuração essencial:&nbsp;Mínimo 2GB RAM,&nbsp;Windows&nbsp;7&nbsp;e&nbsp;Internet&nbsp;Explorer&nbsp;9.0.
        <BR>•&nbsp;Acessórios&nbsp;do&nbsp;Overfly&nbsp;atualizados:
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Faça&nbsp;download&nbsp;dos&nbsp;<A id = "overAccDownload" name = "overAccDownload" href="<% Response.Write(sAcessoriesPath);%>">
        <FONT color=blue><STRONG>Acessórios</STRONG></FONT></A>,&nbsp;
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Execute&nbsp;o&nbsp;arquivo&nbsp;<FONT color=blue><STRONG>OverSetup.msi</STRONG>.</FONT>
        <BR>•&nbsp;Internet&nbsp;Explorer&nbsp;configurado&nbsp;para&nbsp;aceitar&nbsp;o&nbsp;</STRONG>Overfly</STRONG>&nbsp;como&nbsp;site&nbsp;seguro:
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;No&nbsp;menu&nbsp;do&nbsp;IE,&nbsp;clique&nbsp;em&nbsp;<STRONG>Ferramentas</STRONG>,&nbsp;<STRONG>Opções da Internet...</STRONG>,&nbsp;<STRONG>Segurança</STRONG>,&nbsp;<STRONG>Sites&nbsp;confiáveis</STRONG>&nbsp;e&nbsp;<STRONG>Sites...</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Desmarque&nbsp;o&nbsp;checkbox&nbsp;<STRONG>Exigir&nbsp;verificação&nbsp;do&nbsp;servidor</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Digite&nbsp;<STRONG>http://www.overfly.com.br</STRONG>&nbsp;no&nbsp;campo&nbsp;<STRONG>Adicionar&nbsp;este&nbsp;site&nbsp;da&nbsp;Web&nbsp;à&nbsp;zona</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Clique&nbsp;em&nbsp;<STRONG>Adicionar</STRONG>,&nbsp;<STRONG>OK</STRONG>&nbsp;e&nbsp;<STRONG>OK</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Feche&nbsp;o&nbsp;browser&nbsp;e&nbsp;rode&nbsp;novamente.
        <P>
        <STRONG>Recomendações</STRONG>:
        <BR>•&nbsp;Semanalmente&nbsp;ou&nbsp;quando&nbsp;solicitado,&nbsp;limpe&nbsp;o&nbsp;cache&nbsp;do&nbsp;Internet&nbsp;Explorer:
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Feche&nbsp;todos&nbsp;os&nbsp;forms&nbsp;do&nbsp;Overfly,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;No&nbsp;menu&nbsp;do&nbsp;IE,&nbsp;clique&nbsp;em&nbsp;<STRONG>Ferramentas,&nbsp;Opções&nbsp;da&nbsp;Internet...</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Clique&nbsp;no&nbsp;botão&nbsp;<STRONG>Excluir&nbsp;Arquivos...</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Marque&nbsp;o&nbsp;checkbox&nbsp;<STRONG>Apagar&nbsp;todo&nbsp;conteúdo&nbsp;off-line</STRONG>,
        <BR>&nbsp;&nbsp;&nbsp;-&nbsp;Clique&nbsp<STRONG>OK</STRONG>&nbsp;e&nbsp<STRONG>OK</STRONG>.
        <!-- FINAL DE ATENCAO: PARA MEXER NO TEXTO ABAIXO USAR SIMBOLOGIA DE ESPACO -->
        <INPUT type="button" id="btnLogin" name="btnLogin" value="Voltar" class="btnGeneral" LANGUAGE=javascript onclick="return btnLoginInfo_onclick(this)"></INPUT>
    </DIV>


    <DIV id="divLogin" name="divLogin" class="divExtern" >
        <P id="lblSalut" name="lblSalut" class="lblGeneral">Bom dia!</P>
        <P id="lblUserName" name="lblUserName" class="lblGeneral">E-mail</P>
        <INPUT type="text" id="txtUserName" name="txtUserName" value="<% Response.Write(this.sEmail); %>" placeholder="E-mail" class="txtGeneral" LANGUAGE=javascript onkeydown="return txtUserName_onkeydown()"></INPUT>
        <P id="lblSenha" name="lblSenha" class="lblGeneral">Senha</P>
        <INPUT type="password" id="txtSenha" name="txtSenha" placeholder="Senha" class="txtGeneral" LANGUAGE=javascript onkeydown="return txtSenha_onkeydown()"></INPUT>
        
        <p id="lblInicioJornada" name="lblInicioJornada" class="lblGeneral">Início</p>
	    <INPUT type="checkbox" id="chkInicioJornada" name="chkInicioJornada" class="fldGeneral" Title="Inicio da jornada de trabalho?"></INPUT>
	    
	    <select id="selLinguas" name="selLinguas" class="fldGeneral">
            <option value=246 selected>Português</option>
            <option value=245>Inglês</option>
        </select>

        <p id="lblManterConectado" name="lblManterConectado" class="lblGeneral" LANGUAGE=javascript onclick="return lblManterConectadoClicked()">Manter conectado</p>
        <input type="checkbox" id="chkManterConectado" name="chkManterConectado" class="fldGeneral" onclick="return chkManterConectadoOnClick()" <% if (this.bManterConectado) Response.Write("checked"); %>></input>
        
        <INPUT type="button" id="btnOK" name="btnOK" value="Login" class="btnGeneral" LANGUAGE=javascript onclick="return btnOK_onclick()"></INPUT>
        <INPUT type="button" id="btnInfo" name="btnInfo" value="Informações" class="btnGeneral" LANGUAGE=javascript onclick="return btnLoginInfo_onclick(this)"></INPUT>
        <INPUT type="button" id="btnTrocaSenha" name="btnTrocaSenha" value="Senha" title="Trocar senha" class="btnGeneral" LANGUAGE=javascript onclick="return btnTrocarSenha_onclick(this)"></INPUT>
    </DIV>

<%        
// Botao de uso do desenvolvedor para login direto
if (DevMode)
{
    Response.Write("<input type=" + (char)34 + "button" + (char)34 +
        "id=" + (char)34 + "btnDeveloper" + (char)34 +
        "name=" + (char)34 + "btnDeveloper" + (char)34 +
        "value=" + (char)34 + "Desenvolvedor" + (char)34 +    
        "class=" + (char)34 + "btnGeneral" + (char)34 +
        "LANGUAGE=javascript onclick=" + (char)34 + "return btnDeveloper_onclick()" + (char)34 +
        ">");
    Response.Write("\r\n");
    Response.Write("</input>");
}
%>

</BODY>

</html>
