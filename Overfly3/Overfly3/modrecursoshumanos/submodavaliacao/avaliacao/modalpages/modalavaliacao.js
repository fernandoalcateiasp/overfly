/********************************************************************
modalavaliacao.js

Library javascript para o modalavaliacao.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_nDSOs2 = 0;
var glb_OcorrenciasTimerInt = null;
var glb_bFirstFill = false;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_nDivFGHeight = 0;
var glb_nDivFGWidth = 0;
//var glb_nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
//var glb_sReadOnly = (glb_nEstadoID == 66 ? '*' : '');
var glb_aDeletedActions = new Array();
var glb_aFuncionarios = new Array();
var glb_bFocusModo = false;
var glb_TextoAjuda = '';

// FINAL DE VARIAVEIS GLOBAIS ***************************************
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';


var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrid2 = new CDatatransport('dsoGrid2');
var dsoGrava = new CDatatransport('dsoGrava');
var dsoCombos = new CDatatransport('dsoCombos');
var dsoFatores = new CDatatransport('dsoFatores');
var dsoAvaliacao = new CDatatransport('dsoAvaliacao');
var dsoObservacoes = new CDatatransport('dsoObservacoes');
var dsoTipoOperacional = new CDatatransport('dsoTipoOperacional');
var dsoTipoComando = new CDatatransport('dsoTipoComando');
var dsoCmbResponsaveis = new CDatatransport('dsoCmbResponsaveis');
var dsoAvaliacaoTexto = new CDatatransport('dsoAvaliacaoTexto');
var dsoAviso = new CDatatransport('dsoAviso');
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtns()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalavaliacaoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalavaliacaoDblClick(grid, Row, Col)
js_modalavaliacaoKeyPress(KeyAscii)
js_modalavaliacao_ValidateEdit()
js_modalavaliacao_BeforeEdit(grid, row, col)
js_modalavaliacao_AfterEdit(Row, Col)
js_fg_AfterRowColmodalavaliacao (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalavaliacaoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 52;
    modalFrame.style.left = 0;    
    
    glb_bFirstFill = true;
    
	glb_nDSOs = 5;
    setConnection(dsoFatores);
    dsoFatores.SQL = 'SELECT ItemID as fldID, ItemMasculino AS fldName ' +
		'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
		'WHERE TipoID IN (1203) AND (EstadoID = 2)';

    dsoFatores.ondatasetcomplete = window_onload_DSC;
    dsoFatores.Refresh();

    setConnection(dsoAvaliacao);
    dsoAvaliacao.SQL = 'SELECT ItemID AS fldID, ItemAbreviado AS fldName , Observacoes as Observacoes ' +
		'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
		'WHERE TipoID=1205 ' +
		'ORDER BY fldID';

    dsoAvaliacao.ondatasetcomplete = window_onload_DSC;
    dsoAvaliacao.Refresh();

    setConnection(dsoTipoOperacional);
    dsoTipoOperacional.SQL = 'SELECT ItemID as fldID, ItemMasculino AS fldName ' +
		'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
		'WHERE TipoID=1203  AND (EstadoID = 2)' +
		'ORDER BY Ordem';

    dsoTipoOperacional.ondatasetcomplete = window_onload_DSC;
    dsoTipoOperacional.Refresh();

    setConnection(dsoCmbResponsaveis);
    dsoCmbResponsaveis.SQL = 'SELECT DISTINCT b.PessoaID AS fldID, b.Fantasia AS fldName ' +
		'FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
		'WHERE dbo.fn_Empresa_Sistema(a.ObjetoID)=1 AND a.TipoRelacaoID=31 AND a.EstadoID=2 AND a.SujeitoID=b.PessoaID ' +
		'ORDER BY fldName';

    dsoCmbResponsaveis.ondatasetcomplete = window_onload_DSC;
    dsoCmbResponsaveis.Refresh();

    setConnection(dsoAvaliacaoTexto);
    dsoAvaliacaoTexto.SQL = 'SELECT ItemID as ID, Observacoes as Observacoes ' +
		'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
		'WHERE TipoID=1205 ' +
		'ORDER BY Ordem';

    dsoAvaliacaoTexto.ondatasetcomplete = window_onload_DSC;
    dsoAvaliacaoTexto.Refresh();
    
}

function window_onload_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

	adjustLabelsCombos();
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Avalia��o de Desempenho', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
	
    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = btnOK.offsetLeft + btnOK.offsetWidth + ELEM_GAP;    
        height = 37;
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        glb_nDivFGWidth = modWidth - 2 * ELEM_GAP - 6;
        width = glb_nDivFGWidth;
        glb_nDivFGHeight = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
        height = glb_nDivFGHeight;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

	// ajusta o divFG
	with (divFG2.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		left = ELEM_GAP;
		top = parseInt(divFG.currentStyle.top, 10) + parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;
		width = modWidth - 2 * ELEM_GAP - 6;
		height = glb_nDivFGHeight;
	}

	with (fg2.style)
	{
		left = 0;
		top = 0;
		width = parseInt(divFG2.style.width, 10);
		height = parseInt(divFG2.style.height, 10);
	}

    with (txtObservacoes.style)
    {
		nHeight = (8 * ELEM_GAP) + 6;
        left = ELEM_GAP;
        top = divFG.offsetTop + divFG.offsetHeight - nHeight;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = nHeight;
    }
    
    // Reposiciona botao OK
    with (btnImprimir)
    {         
		style.width = 50;
		style.top = divControls.offsetTop + ELEM_GAP;
		style.left = modWidth - ELEM_GAP - parseInt(btnImprimir.currentStyle.width) - 4;
		title = '';
    }
    
    with (btnOK) {
        style.width = btnImprimir.currentStyle.width;
        style.height = btnImprimir.currentStyle.height;
        style.top = btnImprimir.currentStyle.top;
        style.left = parseInt(btnImprimir.currentStyle.left, 10) - parseInt(btnOK.currentStyle.width, 10) - 4;
    }

    with (btnListar)
    {         
        style.width = btnImprimir.currentStyle.width;
        style.height = btnImprimir.currentStyle.height;
        style.top = btnImprimir.currentStyle.top;
        style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnListar.currentStyle.width, 10) - 4;
    }



    with (btnNovo)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		// style.top = divFG.currentStyle.top + divFG.currentStyle.height;
    }

    with (btnDelete)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		// style.top = divFG.currentStyle.top + divFG.currentStyle.height;
    }

    with (btnGravarInf)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		// style.top = divFG.currentStyle.top + divFG.currentStyle.height;
    }
    
    with (btnEmailAvaliacao)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		// style.top = divFG.currentStyle.top + divFG.currentStyle.height;
    }

    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

	adjustElementsInForm([['lbldtInicio','txtdtInicio',9,1,-10,-10],
		['lbldtFim','txtdtFim',9,1,-5],
		['btnFillCmbs', 'btn', 21, 1],
		['lblGestor','selGestor',20,1],
		['lblFuncionario','selFuncionario',20,1],
		['lblFatores','chkFatores',3,1,-5],
		['lblModo','selModo',22,1,-10]/*,
		['lblAtores', 'selAtores', 15, 1, -5]*/], null, null, true);

	chkFatores.style.left = chkFatores.offsetLeft + 10;
	
	txtdtInicio.value = glb_dtInicio;
	txtdtInicio.maxLength = 10;
	txtdtFim.value = glb_dtFim;
	txtdtFim.maxLength = 10;

	btnFillCmbs.src = glb_LUPA_IMAGES[0].src;
	chkFatores.checked = false;
	chkFatores.onclick = chkFatores_onclick;
	selGestor.onchange = selGestor_onchange;
	fillCmbModo();
	fillCmbFuncionarios();
	selModo.onchange = selModo_onchange;
    selFuncionario.onchange = selFuncionario_onchange;
    selModo_onchange();
    selAtores.onchange = selAtores_onchange;
    
	selGestor.disabled = true;
	selFuncionario.disabled = true;

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;

    startGridInterface(fg2);
    fg2.Cols = 0;
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;    
    fg2.Redraw = 2;
}

function chkFatores_onclick()
{
	fillCmbModo();
	fg.Rows = 1;
	fg2.Rows = 1;
}

function selAtores_onchange()
{
	adjustObsFields();
}

function selGestor_onchange()
{
	fg.Rows = 1;
	fg2.Rows = 1;
	fillCmbFuncionarios();
	adjustLabelsCombos();
	setupBtns();
}

function selFuncionario_onchange()
{
	fg.Rows = 1;
	fg2.Rows = 1;
	txtObservacoes.value = '';
	chkLiberado.checked = false;
	chkAcordado.checked = false;
	chkOK.checked = false;
	adjustLabelsCombos();
	setupBtns();
}

function selModo_onchange()
{
	var nGap = 305;
	var nTopGap = 0;
	
	lblAtores.style.visibility = 'hidden';
	selAtores.style.visibility = 'hidden';
	lblLiberado.style.visibility = 'hidden';
	chkLiberado.style.visibility = 'hidden';
	lblAcordado.style.visibility = 'hidden';
	chkAcordado.style.visibility = 'hidden';
	lblOK.style.visibility = 'hidden';
	chkOK.style.visibility = 'hidden';
	divFG2.style.visibility = 'hidden';
	fg2.style.visibility = 'hidden';
	txtObservacoes.style.visibility = 'hidden';
	txtObservacoesAjuda.style.visibility = 'hidden';
	btnImprimir.style.visibility = 'inherit';
	btnNovo.style.visibility = 'hidden';
	btnDelete.style.visibility = 'hidden';
	btnGravarInf.style.visibility = 'hidden';
	btnEmailAvaliacao.style.visibility = 'hidden';
    // acompanhar/aprovar ou avaliar
	if ((selModo.value == 1) || (selModo.value == 2))
	{
	    divFG.style.height = glb_nDivFGHeight - nGap;
	    adjustObsFields();
	    // Acompanhar / Aprovar
	    //if (selModo.value == 1)
	    {
	        nTopGap = 35;
	        lblAtores.style.visibility = 'inherit';
	        selAtores.style.visibility = 'inherit';
	        lblLiberado.style.visibility = 'inherit';
	        chkLiberado.style.visibility = 'inherit';
	        lblAcordado.style.visibility = 'inherit';
	        chkAcordado.style.visibility = 'inherit';
	        lblOK.style.visibility = 'inherit';
	        chkOK.style.visibility = 'inherit';
	        //btnImprimir.style.visibility = 'hidden';
	        btnGravarInf.style.visibility = 'inherit';
	        btnEmailAvaliacao.style.visibility = 'inherit';
		
	        adjustElementsInForm([
                ['lblAtores', 'selAtores', 15, 1, 0, 210],
                ['lblLiberado', 'chkLiberado', 3, 1, 0],
				['lblAcordado','chkAcordado',3,1],
				['lblOK','chkOK',3,1],
				['btnGravarInf','btn',btnOK.offsetWidth,1,0,-7],
				['btnEmailAvaliacao', 'btn', btnOK.offsetWidth, 1]], null, null, true);

	        chkAcordado.style.left = chkAcordado.offsetLeft + 15;
	        chkLiberado.style.left = chkLiberado.offsetLeft + 10;
				
	        chkOK.onclick = chkOK_onclick;
	    }

	    txtObservacoes.style.top = parseInt(divFG.style.top,10) + parseInt(divFG.style.height, 10) + 5 + nTopGap;
	    txtObservacoes.style.height = nGap - nTopGap;

	    if (chkFatores.checked) {
	        txtObservacoes.style.width = (glb_nDivFGWidth / 2);
	        txtObservacoesAjuda.style.visibility = 'inherit';
	        txtObservacoesAjuda.style.top = txtObservacoes.offsetTop;
	        txtObservacoesAjuda.style.width = txtObservacoes.offsetWidth;
	        txtObservacoesAjuda.style.left = txtObservacoes.offsetWidth + 10;
	        txtObservacoesAjuda.style.height = txtObservacoes.offsetHeight;
	    }
	    else
	    {
	        txtObservacoes.style.width = glb_nDivFGWidth;
	    }
	    

	    txtObservacoes.style.visibility = 'inherit';

	    
	}
    //Plano de A��o
	else if (selModo.value == 3)
	{
		divFG.style.height = glb_nDivFGHeight - nGap;
		btnNovo.style.visibility = 'inherit';
		btnDelete.style.visibility = 'inherit';
		btnGravarInf.style.visibility = 'inherit';
		btnEmailAvaliacao.style.visibility = 'inherit';
		txtObservacoes.style.visibility = 'inherit';

		adjustElementsInForm([['btnNovo', 'btn', btnOK.offsetWidth, 1, (glb_nDivFGWidth / 2)+10, 210],
			['btnDelete','btn',btnOK.offsetWidth,1,2],
			['btnGravarInf','btn',btnOK.offsetWidth,1,2],
			['btnEmailAvaliacao','btn',btnOK.offsetWidth,1,2]], null, null, true);

		// ajusta o divFG
		with (divFG2.style)
		{
			top = parseInt(divFG.currentStyle.top, 10) + parseInt(divFG.currentStyle.height, 10) + ELEM_GAP + 30;
			height = 260;
			width = (glb_nDivFGWidth / 2);
			left = (glb_nDivFGWidth / 2) + 10;
		}

		with (fg2.style)
		{
		    height = parseInt(divFG2.style.height, 10);
		    width = divFG2.offsetWidth;//parseInt(divFG2.style.width, 10);
		    //left = parseInt(divFG2.style.width, 10);
		}

		divFG2.style.visibility = 'inherit';
		fg2.style.visibility = 'inherit';
		glb_bFocusModo = true;
		fillGridData2();

		txtObservacoes.style.top = divFG2.offsetTop;
		txtObservacoes.style.height = divFG2.offsetHeight;
		txtObservacoes.style.width = (glb_nDivFGWidth / 2);
	}
	else
	{
		divFG.style.height = glb_nDivFGHeight;
	}

	fg.style.height = divFG.offsetHeight;
	drawBordersAroundTheGrid(fg);
	setupBtns();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnListar')
    {
		fillGridData();
    }
    else if (controlID == 'btnImprimir')
    {
		printGrid();
    }
    else if (controlID == 'btnNovo')
    {
		inserirInf();
    }
    else  if (controlID == 'btnGravarInf')
    {
		gravaInf();
    }
    else if (controlID == 'btnEmailAvaliacao')
    {
		emailAvaliacao();
    }
    else if (controlID == 'btnDelete')
    {
		deleteInf();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar interface aqui
    setupBtns();
    
	showExtFrame(window, true);
	
	window.focus();
	
	if ((txtdtInicio.value != '') && (txtdtFim.value != ''))
		fillCombos();

    // preenche o grid da janela modal
    //fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtns()
{
    var i;
    var bHasRowsInGrid = fg.Rows > 2;
    var bEhProprietario = false;
	var nAvaliacaoID = 0;

	if (fg.Row > 0)
		nAvaliacaoID = getCellValueByColKey(fg, 'AvaliacaoID*', fg.Row);
		
	btnOK.disabled = !bHasRowsInGrid;

	// Acompanhar/Aprovar
	if (selModo.value == 1)
	{
		bEhProprietario = findObservacao(nAvaliacaoID, 2);

		chkLiberado.disabled = !bEhProprietario;
		chkAcordado.disabled = !bEhProprietario;
		chkOK.disabled = !bEhProprietario;
		btnGravarInf.disabled = !bEhProprietario;
		btnEmailAvaliacao.disabled = !bEhProprietario;
		txtObservacoes.readOnly = !bEhProprietario;
	}
	// Avaliar
	else if (selModo.value == 2)
	{
		;
	}
	// Plano de A��o
	else if (selModo.value == 3)
	{
		bEhProprietario = findObservacao(nAvaliacaoID, 2);
		btnGravarInf.disabled = (fg2.Rows < 2);
		btnDelete.disabled = (fg2.Row < 1);
	}
}

function setTotalColumns()
{
	;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }
    
    
    if (chkFatores.checked)
    {
		if ((selGestor.selectedIndex == 0) && (selFuncionario.selectedIndex == 0))
		{
			if ( window.top.overflyGen.Alert('Selecione Gestor imediato ou Funcion�rio.') == 0 )
				return null;

			return null;
		}
	}
    
    var sDadosAvaliacao = '';
	var aGrid = null;
	var i = 0;
	//var AvaliacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'AvaliacaoID' + '\'' + '].value');
	var sFiltro = '';
	
	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;

    // zera o grid
    fg.Rows = 1;
    fg2.Rows = 1;

    sFiltro += ' AND a.dtInicio >= dbo.fn_Data_Zero(' + '\'' + putDateInMMDDYYYY2(txtdtInicio.value) + '\'' + ') AND ' +
			'dbo.fn_Data_Zero(a.dtFim) <= ' + '\'' + putDateInMMDDYYYY2(txtdtFim.value) + '\'' + ' ';
    
    if (selFuncionario.selectedIndex > 0)
		sFiltro += ' AND a.FuncionarioID = ' + selFuncionario.value + ' ';
	else
	{
		sFiltro += ' AND a.FuncionarioID IN (';
		var bFirst = true;
		for (i=0; i<selFuncionario.options.length; i++)
		{
			if (selFuncionario.options(i).value != 0)
			{
				sFiltro += (bFirst ? '' : ',') + selFuncionario.options(i).value;
				bFirst = false;
			}
		}
		sFiltro += ') ';
	}

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	if (chkFatores.checked)
	{
        // Acompanhar/aprovar e gestor superior
        if ((selModo.value == 1) && (selAtores.value == 1813))
	        sFiltro += ' AND a.AlternativoID=' + getCurrUserID() + ' ';

	    //'((a.Peso * a.Avaliacao)/100) AS PesoAvaliacao, ' +
	    dsoGrid.SQL = 'SELECT ' +
	            'b.ItemMasculino AS TipoAvaliacao, a.dtInicio, a.AvaliacaoID, c.RecursoAbreviado AS Estado, a.FuncionarioID, d.Fantasia AS Funcionario, ' +
			    'dbo.fn_Pessoa_Data(a.FuncionarioID, a.EmpresaID, NULL, 6, NULL, NULL) AS Admissao, ' +
			    'e.RecursoFantasia AS Cargo, ' +
			    'ISNULL(CONVERT(VARCHAR(10), dbo.fn_Colaborador_Dado(d.PessoaID, NULL, NULL, 8)), 0) AS Tempo, ' +
			    'ISNULL(dbo.fn_Avaliacao_Nota(a.AvaliacaoID), 0) AS NotaFinal, ' +
			    'ISNULL((SELECT aa.ItemAbreviado ' +
			        'FROM TiposAuxiliares_Itens aa WITH(NOLOCK) ' +
			        'WHERE (aa.ItemID = dbo.fn_Avaliacao_Parecer(a.AvaliacaoID))), SPACE(1)) AS Parecer, ' +
			    'h.ItemMasculino AS Fator, g.Peso, g.NotaID, g.Observacao AS Observacao, h.Observacoes AS Ajuda, a.TipoAvaliacaoID, g.NotaID AS NotaIDHidden, ' +
			    'a.ProprietarioID AS GestorID, f.Fantasia AS Gestor, g.AvaFatorID, a.EstadoID  ' +   
			'FROM AvaliacoesDesempenho a WITH(NOLOCK) ' +
			    'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.TipoAvaliacaoID) ' +
			    'INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = a.EstadoID) ' +
			    'INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = a.FuncionarioID) ' +
			    'INNER JOIN Recursos e WITH(NOLOCK) ON (e.RecursoID = a.CargoID) ' +
			    'INNER JOIN Pessoas f WITH(NOLOCK) ON (f.PessoaID = a.ProprietarioID) ' +
			    'INNER JOIN AvaliacoesDesempenho_Fatores g WITH(NOLOCK) ON (g.AvaliacaoID = a.AvaliacaoID) ' +
			    'INNER JOIN TiposAuxiliares_Itens h WITH(NOLOCK) ON (h.ItemID = g.FatorID) ' +					    
		    'WHERE ((a.EmpresaID IN (SELECT ' + glb_aEmpresaData[0] + ' ' +
                    'UNION ALL ' +
                    'SELECT EmpresaAlternativaID ' +
                        'FROM FopagEmpresas WITH(NOLOCK) ' +
                        'WHERE (EmpresaID = ' + glb_aEmpresaData[0] + ' AND FuncionarioID IS NULL))) ' +
				sFiltro + ') ' +
			'ORDER BY a.TipoAvaliacaoID, d.Fantasia';			
	}
	else { 
	    dsoGrid.SQL = 'SELECT ' +
	            'b.ItemMasculino AS TipoAvaliacao, a.dtInicio, a.AvaliacaoID, c.RecursoAbreviado AS Estado, a.FuncionarioID, d.Fantasia AS Funcionario, ' +
			    'dbo.fn_Pessoa_Data(a.FuncionarioID, a.EmpresaID, NULL, 6, NULL, NULL) AS Admissao, ' +
			    'e.RecursoFantasia AS Cargo, ' +
			    'ISNULL(CONVERT(INT, dbo.fn_Colaborador_Dado(d.PessoaID, NULL, NULL, 8)), 0) AS Tempo, ' +
			    'ISNULL(dbo.fn_Avaliacao_Nota(a.AvaliacaoID), 0) AS NotaFinal, ' +
			    'ISNULL((SELECT aa.ItemAbreviado ' +
			        'FROM TiposAuxiliares_Itens aa WITH(NOLOCK) ' +
			        'WHERE (aa.ItemID = dbo.fn_Avaliacao_Parecer(a.AvaliacaoID))), SPACE(1)) AS Parecer, ' +
			    'SPACE(0) AS Fator, SPACE(0) AS Peso, 0 AS NotaID, SPACE(0) AS Observacao, SPACE(0) AS Ajuda, a.TipoAvaliacaoID, 0 AS NotaIDHidden, ' +
			    'a.ProprietarioID AS GestorID, f.Fantasia AS Gestor, 0 AS AvaFatorID, a.EstadoID ' +
			'FROM AvaliacoesDesempenho a WITH(NOLOCK) ' +
			    'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.TipoAvaliacaoID) ' +
			    'INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = a.EstadoID) ' +
			    'INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = a.FuncionarioID) ' +
			    'INNER JOIN Recursos e WITH(NOLOCK) ON (e.RecursoID = a.CargoID) ' +
			    'INNER JOIN Pessoas f WITH(NOLOCK) ON (f.PessoaID = a.ProprietarioID) ' +
		    'WHERE ((a.EmpresaID IN (SELECT ' + glb_aEmpresaData[0] + ' ' +
                    'UNION ALL ' +
                    'SELECT EmpresaAlternativaID ' +
                        'FROM FopagEmpresas WITH(NOLOCK) ' +
                        'WHERE (EmpresaID = ' + glb_aEmpresaData[0] + ' AND FuncionarioID IS NULL))) ' +
				sFiltro + ') ' +
			'ORDER BY a.TipoAvaliacaoID, d.Fantasia';
	}
	
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
    try
    {
		dsoGrid.Refresh();
	}	
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Filtro inv�lido.') == 0 )
            return null;

		lockControlsInModalWin(false);        
        window.focus();
	}
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

	if (glb_bFirstFill)
	{
		glb_bFirstFill = false;
		showExtFrame(window, true);
		
		window.focus();
	}

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
    var bTempValue;
    var aHoldCols = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	if (!chkFatores.checked)
	{
		aHoldCols[aHoldCols.length] = 12;
		aHoldCols[aHoldCols.length] = 13;
		aHoldCols[aHoldCols.length] = 14;
		aHoldCols[aHoldCols.length] = 15;
	}

	aHoldCols[aHoldCols.length] = 16;
	aHoldCols[aHoldCols.length] = 17;
	aHoldCols[aHoldCols.length] = 18;
	aHoldCols[aHoldCols.length] = 19;
	aHoldCols[aHoldCols.length] = 20;
	aHoldCols[aHoldCols.length] = 21;
	

    headerGrid(fg,['Avalia��o',
		'In�cio',
		'Gestor',
		'Av',
		'E',
		'ID',
		'Funcion�rio',
		'Admiss�o',
		'Cargo',
		'Meses',
		'Nota',		
		'Parecer',
		'Fator',
		'Peso',
		'Avalia��o',
		'Observa��o',
		'Ajuda',
		'TipoAvaliacaoID',
		'NotaIDHidden',
		'GestorID',
		'EstadoID',
		'AvaFatorID'], aHoldCols);

    fillGridMask(fg, dsoGrid, ['TipoAvaliacao*',
		'dtInicio*',
		'Gestor*',
		'AvaliacaoID*',
		'Estado*',
		'FuncionarioID*',
		'Funcionario*',
		'Admissao*',
		'Cargo*',
		'Tempo*',
		'NotaFinal*',
		'Parecer*',
		'Fator*',
		'Peso*',
		'NotaID',
		'Observacao',
		'Ajuda',
		'TipoAvaliacaoID',
		'NotaIDHidden',
		'GestorID',
		'EstadoID',
		'AvaFatorID'],
		['', '99/99/99', '', '', '', '', '', '99/99/9999', '', '', '999.99', '', '', '999', '', '', '', '', '', '', '', ''],
		['', dTFormat, '', '','', '', '', dTFormat, '', '', '##0.00', '', '', '##0', '', '', '', '', '', '', '', '']);

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'FuncionarioID*'),'###0','C']]);
    
    setGridTotalLabel();

	insertcomboData(fg, getColIndexByColKey(fg, 'NotaID'), dsoAvaliacao, 'fldName', 'fldID');

	alignColsInGrid(fg, [9, 10, 13]);

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;
    fg.MergeCol(5) = true;
    fg.MergeCol(6) = true;
    fg.MergeCol(7) = true;
    fg.MergeCol(8) = true;
    fg.MergeCol(9) = true;
    fg.MergeCol(10) = true;
    fg.MergeCol(11) = true;
    
    fg.Redraw = 0;
    //fg.FrozenCols = 4;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
	paintCellsSpecialyReadOnly(fg);
	    
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 2)
    {
		fg.Editable = chkFatores.checked;
        window.focus();
        fg.focus();
        fg.Row = 2;
        fg.Col = 1;
    }                
    else
    {
        ;
    }            

    fg.Redraw = 2;
    
    adjustObsFields();
    
    // ajusta estado dos botoes
    setupBtns();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;
	
    lockControlsInModalWin(true);
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
    
    for (i=2; i<fg.Rows; i++)
    {
		nBytesAcum=strPars.length;
		
		if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
		{
			nBytesAcum = 0;
			if (strPars != '')
			{
				glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
				strPars = '';
				nDataLen = 0;
			}
			
			strPars += '?AvaliacaoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'AvaliacaoID*')));
		}

		nDataLen++;
		strPars += '&AvaFatorID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'AvaFatorID')));
		
		if (trimStr(getCellValueByColKey(fg, 'NotaID', i)) != '')
			strPars += '&NotaID=' + escape(getCellValueByColKey(fg, 'NotaID', i));
		else
			strPars += '&NotaID=' + escape('NULL');

		if (trimStr(getCellValueByColKey(fg, 'Observacao', i)) != '')
			strPars += '&Observacao=' + escape('\'' + getCellValueByColKey(fg, 'Observacao', i) + '\'');
		else
			strPars += '&Observacao=' + escape('NULL');
	}

	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
		
	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
		    dsoGrava.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodavaliacao/avaliacao/serverside/gravaavaliacao.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
	}
}

function sendDataToServer_DSC()
{
	if ( !(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF))
	{
		dsoGrava.recordset.MoveFirst();
		if ( (dsoGrava.recordset['Resultado'].value != null) && 
			 (dsoGrava.recordset['Resultado'].value != '') )
		{
		    if ( window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0 )
				return null;
				
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  				
			return null;
		}
	}

	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);

	glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
}

function imprimirGrid()
{
	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	//var AvaliacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'AvaliacaoID' + '\'' + '].value');
	var sMsg = aEmpresaData[6] + '          Dados de funcion�rios          Fopag ' + AvaliacaoID + '          ' + getCurrDate();

	sMsg += '     ';
	fg.PrintGrid(sMsg, false, 2, 0, 450);

	fg.AutoSizeMode = 0;
	fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 1;
}

function getCurrDate()
{
   var d, s = "";
   d = new Date();
   
   if (DATE_SQL_PARAM == 103)
   {
		s += padL(d.getDate().toString(),2,'0') + "/";
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
   }
   else
   {
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
		s += padL(d.getDate().toString(),2,'0') + "/";
   }
   
   s += d.getYear() + " ";
   s += padL(d.getHours().toString(),2,'0') + ":";
   s += padL(d.getMinutes().toString(),2,'0');

   return(s);
}


/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly(grid)
{
	paintReadOnlyCols(grid);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalavaliacaoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
    ;
}


/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalavaliacao_ChangeEdit()
{
    var ID;

    if (fg.Col == getColIndexByColKey(fg, 'NotaID')) {
        // Simulando troca no combo
        fg.Col = fg.Col + 1;
        fg.Col = fg.Col - 1;

        // ID do combo selecionado
        ID = fg.ValueMatrix(fg.Row, fg.Col);

        // busca o texto e preenche para invocar funcao que preenche o nome.
        glb_TextoAjuda = TextoAjuda(ID);
        adjustObsFields();

        //variavel s� utilizada no momento de troca de combos no grid.
        glb_TextoAjuda = '';
    }
  
}
/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalavaliacaoDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    // trap so colunas editaveis
    if ( getColIndexByColKey(grid, 'OK') != Col)
        return true;
        
    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);
    
    glb_PassedOneInDblClick = true;
    
    // limpa coluna se tem um check box checado
    for ( i=2; i<grid.Rows; i++ )
    {
        if ( grid.ValueMatrix(i, Col) != 0 )
        {
            bFill = false;
            break;
        }
    }
    
    for ( i=2; i<grid.Rows; i++ )
    {
        if ( bFill )
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    
    setTotalColumns();
    
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtns();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalavaliacaoKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalavaliacao_ValidateEdit(grid, Row, Col)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalavaliacao_BeforeEdit(grid, row, col)
{
	if ((fg.Row == 1) && (fg.Rows > 2))
	    fg.Row = 2;

    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalavaliacao_AfterEdit(Row, Col)
{
	var sMsg = '';
	
    if ((fg.Editable) && (fg.EditText != ''))
    {
		if (Col == getColIndexByColKey(fg, 'NotaID'))
		{
		    if (fg.ValueMatrix(Row, getColIndexByColKey(fg, 'GestorID')) != glb_nUserID)
				sMsg = 'Somente o gestor imediato deve avaliar o funcion�rio.';

			if (fg.ValueMatrix(Row, getColIndexByColKey(fg, 'EstadoID')) != 41)
				sMsg = 'Esta avalia��o n�o est� aberta.';

			if (sMsg != '')
			{
				if ( window.top.overflyGen.Alert(sMsg) == 0 )
					return null;

				// Volta o valor original
				fg.TextMatrix(Row, Col) = getCellValueByColKey(fg, 'NotaIDHidden', Row);
			}
		}
	}
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalavaliacao(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return false;
	
	adjustObsFields();
	setupBtns();
}

function adjustObsFields() {

 
	if (fg.Row > 0)
	{
		// Acompanhar/Aprovar
	    if (selModo.value == 1) {
	        txtObservacoes.readOnly = false;
	        getObsInServer();
	    /*}
	    // Avaliar
	    else if (selModo.value == 2) 
	    {*/
	        //txtObservacoes.readOnly = true;
	        if (glb_TextoAjuda != '')
	            txtObservacoesAjuda.value = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Ajuda')) + glb_TextoAjuda;
	        else
	            txtObservacoesAjuda.value = (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Ajuda')) + TextoAjuda(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'NotaID'))));

	    }
	    // Plano de A��o
	    else if (selModo.value == 3) {
	        if (fg2.Rows > 0) {
	            if (getCellValueByColKey(fg, 'AvaliacaoID*', fg.Row) == getCellValueByColKey(fg2, 'AvaliacaoID', 1))
	                return null;
	        }
	        txtObservacoes.visibility = false;
	        txtObservacoesAjuda.visibility = false;

	        glb_bFocusModo = false;
	        fillGridData2();
	    }
	}
	else
	{
		fg2.Rows = 1;
		txtObservacoes.value = '';
		txtObservacoesAjuda.value = '';
		chkLiberado.checked = false;
		chkAcordado.checked = false;
		chkOK.checked = false;
	}
	setupBtns();
}

function getObsInServer()
{
	txtObservacoes.value = '';
	chkLiberado.checked = false;
	chkAcordado.checked = false;
	chkOK.checked = false;
	
	if (fg.Row < 1)
		return null;
		
	var nAvaliacaoID = getCellValueByColKey(fg, 'AvaliacaoID*', fg.Row);
	var findObs = findObservacao(nAvaliacaoID, 1);
	// Nao eh necessario ir ao servidor
	
	if (findObs == null)
	{
	    chkLiberado.checked = dsoObservacoes.recordset['Liberado'].value;
	    chkAcordado.checked = dsoObservacoes.recordset['Acordado'].value;
	    chkOK.checked = dsoObservacoes.recordset['OK'].value;
		setupBtns();
		return null;
	}

	lockControlsInModalWin(true);

    // parametrizacao do dso dsoGrid
    setConnection(dsoObservacoes);

    dsoObservacoes.SQL = 'SELECT a.*, (SELECT b.Fantasia FROM Pessoas b WITH(NOLOCK) WHERE b.PessoaID = a.PessoaID) AS Ator ' +
		'FROM AvaliacoesDesempenho_Observacoes a WITH(NOLOCK) ' +
		'WHERE a.AvaliacaoID=' + nAvaliacaoID + ' ' +
		'ORDER BY a.AvaliacaoID, a.PapelID';

    dsoObservacoes.ondatasetcomplete = getObsInServer_DSC;
    dsoObservacoes.Refresh();
}

function getObsInServer_DSC()
{
	if (fg.Row > 0)
	{
		var nAvaliacaoID = getCellValueByColKey(fg, 'AvaliacaoID*', fg.Row);
		var findObs = findObservacao(nAvaliacaoID, 1);
		
		if (findObs == true)
		{
		    if (dsoObservacoes.recordset['Observacoes'].value != null)
		        txtObservacoes.value = dsoObservacoes.recordset['Observacoes'].value;
			else
				txtObservacoes.value = '';

		    chkLiberado.checked = dsoObservacoes.recordset['Liberado'].value;
		    chkAcordado.checked = dsoObservacoes.recordset['Acordado'].value;
		    chkOK.checked = dsoObservacoes.recordset['OK'].value;
		}
		else if (findObs == null)
		{
		    chkLiberado.checked = dsoObservacoes.recordset['Liberado'].value;
		    chkAcordado.checked = dsoObservacoes.recordset['Acordado'].value;
		    chkOK.checked = dsoObservacoes.recordset['OK'].value;
		}
	}
	
	lockControlsInModalWin(false);
	setupBtns();
	window.focus();
	fg.focus();
}


/*
	Procura o campo observacoes no dso
	nTipoRetorno: 1:Direitos de visualizacao
				  2:Direitos de edicao
	Retorno: true:  achou o registro e o usuario tem direito de ver
			 false: nao achou o registro
			 null: achou o registro, mas usuario nao tem direito de ver
*/
function findObservacao(nAvaliacaoID, nTipoRetorno)
{
	var retVal = false;
	
	if (dsoObservacoes.recordset.fields.count > 0)
	{
		if (!dsoObservacoes.recordset.EOF)
		{
			dsoObservacoes.recordset.MoveFirst();
			
			while (!dsoObservacoes.recordset.EOF)
			{
			    if ((dsoObservacoes.recordset['AvaliacaoID'].value == nAvaliacaoID) &&
					((dsoObservacoes.recordset['PapelID'].value == selAtores.value) || (selModo.value == "3" && dsoObservacoes.recordset['PapelID'].value == 1811))) 
				{
					retVal = true;

					if (nTipoRetorno == 1)
					{
						//check para ver se o usuario logado tem direito de ler este campo
					    if (!dsoObservacoes.recordset['Liberado'].value)
						{
					        if (dsoObservacoes.recordset['PessoaID'].value != glb_nUserID)
								retVal = null;
						}
					}
					else if (nTipoRetorno == 2)
					{
					    retVal = ((dsoObservacoes.recordset['PessoaID'].value == glb_nUserID) ||
								  (glb_nUserID == 1142))
					}
					
					break;
				}
				dsoObservacoes.recordset.MoveNext();
			}
		}
	}
	
	return retVal;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData2()
{
	if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }
    
    if (fg.Row < 1)
		return null;
    
	var nAvaliacaoID = getCellValueByColKey(fg, 'AvaliacaoID*', fg.Row);

    var sDadosAvaliacao = '';
	var aGrid = null;
	var i = 0;
	var sFiltro = '';
	
	lockControlsInModalWin(true);
	
	glb_nDSOs2 = 1;

    // zera o grid
    fg2.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid2);

	dsoGrid2.SQL = 'SELECT a.* ' +
		'FROM AvaliacoesDesempenho_Acoes a WITH(NOLOCK) ' +
		'WHERE a.AvaliacaoID=' + nAvaliacaoID;

    dsoGrid2.ondatasetcomplete = fillGridData2_DSC;
    
    try
    {
		dsoGrid2.Refresh();
	}	
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Filtro inv�lido.') == 0 )
            return null;

		lockControlsInModalWin(false);        
        window.focus();
	}
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData2_DSC()
{
	glb_nDSOs2--;
	
	if (glb_nDSOs2 > 0)
		return null;

	if (glb_bFirstFill)
	{
		glb_bFirstFill = false;
		showExtFrame(window, true);
		
		window.focus();
	}

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
    var bTempValue;
    var aHoldCols = new Array();
	var nTipoAvaliacaoID = getCellValueByColKey(fg, 'TipoAvaliacaoID', fg.Row);
	
    fg2.Redraw = 0;
    fg2.Editable = true;
    startGridInterface(fg2);
    
    fg2.FontSize = '8';
    fg2.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	aHoldCols[aHoldCols.length] = 8;
	aHoldCols[aHoldCols.length] = 9;
	
	glb_aDeletedActions = new Array();

    headerGrid(fg2,['Fator',
		'A��o',
		'Respons�vel',
		'In�cio',
		'Fim',
		'Conclus�o',
		'Eficaz',
		'Observa��o',
		'AvaliacaoID',
		'AvaAcaoID'], aHoldCols);

    fillGridMask(fg2,dsoGrid2,['FatorID',
		'Acao',
		'ResponsavelID',
		'dtInicio',
		'dtFim',
		'dtConclusao',
		'Eficaz',
		'Observacao',
		'AvaliacaoID',
		'AvaAcaoID'],
		['','','','99/99/9999','99/99/9999','99/99/9999','','','',''],
		['','','',dTFormat,dTFormat,dTFormat,'','','','']);
 
	insertcomboData(fg2, getColIndexByColKey(fg2, 'ResponsavelID'), dsoCmbResponsaveis, 'fldName', 'fldID');
	
	// Operacional
	if (nTipoAvaliacaoID == 1801)
		insertcomboData(fg2, getColIndexByColKey(fg2, 'FatorID'), dsoTipoOperacional, 'fldName', 'fldID');
	
    fg2.Redraw = 0;
    //fg.FrozenCols = 4;
    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0,fg2.Cols-1);
    
    fg2.ColWidth(getColIndexByColKey(fg2, 'FatorID')) = 2000;
    fg2.ColWidth(getColIndexByColKey(fg2, 'Acao')) = 2700;
    fg2.ColWidth(getColIndexByColKey(fg2, 'ResponsavelID')) = 1550;
    fg2.ColWidth(getColIndexByColKey(fg2, 'dtInicio')) = 950;
    fg2.ColWidth(getColIndexByColKey(fg2, 'dtFim')) = 950;
    fg2.ColWidth(getColIndexByColKey(fg2, 'dtConclusao')) = 950;
    fg2.ColWidth(getColIndexByColKey(fg2, 'Eficaz')) = 650;
    fg2.ColWidth(getColIndexByColKey(fg2, 'Observacao')) = 1250;
	paintCellsSpecialyReadOnly(fg2);
	    
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    fg2.Editable = true;
    if (fg2.Rows > 1)
    {
        fg2.Row = 1;
        fg2.Col = 1;
    }                

    fg2.Redraw = 2;
    
    window.focus();
    
    if (glb_bFocusModo)
		selModo.focus();
    else
		fg.focus();
    
	// Plano de A��o
	if (selModo.value == 3)
		getObsInServer();
	else
		setupBtns();
}

function deleteInf()
{
	var sMsg = '';

	if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'GestorID')) != glb_nUserID)
		sMsg = 'Somente o gestor imediato pode apagar uma a��o.';

	if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'EstadoID')) != 41)
		sMsg = 'Esta avalia��o n�o est� aberta.';

	if (sMsg != '')
	{
		if ( window.top.overflyGen.Alert(sMsg) == 0 )
			return null;
		
		return null;
	}

	var nAvaAcaoID = 0;
	var nLineBefore = 0;
	if (fg2.Row > 0)
	{
		nAvaAcaoID = getCellValueByColKey(fg2, 'AvaAcaoID', fg2.Row);
		if (nAvaAcaoID != 0)
			glb_aDeletedActions[glb_aDeletedActions.length] = nAvaAcaoID;
		
		//nLineBefore = fg2.Row-1;
		deleteLineInGrid(fg2, fg2.Row, true);
		
		window.focus();
		fg2.focus();

		/*if (fg2.Rows-1 >= nLineBefore)
			fg2.Row = nLineBefore;
		else if (fg2.Rows > 1)
			fg2.Row = 1;*/
	}
}

function gravaInf()
{
	var sMsg = '';

	if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'EstadoID')) != 41)
		sMsg = 'Esta avalia��o n�o est� aberta.';

	if (sMsg != '')
	{
		if ( window.top.overflyGen.Alert(sMsg) == 0 )
			return null;

		return null;
	}
    // Acompanhar/Aprovar
	if (selModo.value == 1)
	{
		if (fg.Row < 1)
			return null;
			
		var nAvaliacaoID = getCellValueByColKey(fg, 'AvaliacaoID*', fg.Row);

		// Nao eh necessario ir ao servidor
		if (findObservacao(nAvaliacaoID, 2))
		{
			lockControlsInModalWin(true);

			dsoObservacoes.recordset['Observacoes'].value = txtObservacoes.value;
			dsoObservacoes.recordset['Liberado'].value = chkLiberado.checked;
			dsoObservacoes.recordset['Acordado'].value = chkAcordado.checked;
			dsoObservacoes.recordset['OK'].value = chkOK.checked;
			
			dsoObservacoes.SubmitChanges();
			dsoObservacoes.ondatasetcomplete = gravaInfOBS_DSC;
			dsoObservacoes.Refresh();
		}
	}
	// Plano de A��o
	else if (selModo.value == 3)
	{
		saveDataInGrid2();
	}
}

function gravaInfOBS_DSC()
{
	lockControlsInModalWin(false);
	setupBtns();
}

function inserirInf()
{
	var i=0;
	var sMsg = '';
	
	// Verifica se pode adicionar a linha nova
	for (i=1; i<fg2.Rows; i++)
	{
		if (!lineIngGrid2IsValid(i))
			return null;
	}
	
	if (fg.Row < 1)
		return null;

if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'GestorID')) != glb_nUserID)
		sMsg = 'Somente o gestor imediato pode inserir uma a��o.';

	if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'EstadoID')) != 41)
		sMsg = 'Esta avalia��o n�o est� aberta.';

	if (sMsg != '')
	{
		if ( window.top.overflyGen.Alert(sMsg) == 0 )
			return null;
		
		return null;
	}

	fg2.Rows = fg2.Rows + 1;
	fg2.Row = fg2.Rows -1;
	setupBtns();
}

function lineIngGrid2IsValid(nLine)
{
	if ((fg2.ValueMatrix(nLine, getColIndexByColKey(fg2, 'FatorID')) == 0) ||
		(fg2.ValueMatrix(nLine, getColIndexByColKey(fg2, 'ResponsavelID')) == 0) ||
		(getCellValueByColKey(fg2, 'Acao', nLine) == '') ||
		(fg2.ValueMatrix(nLine, getColIndexByColKey(fg2, 'dtInicio')) == 0) ||
		(fg2.ValueMatrix(nLine, getColIndexByColKey(fg2, 'dtFim')) == 0))
		return false;
	
	return true;
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid2()
{
	var sMsg = '';

	if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'GestorID')) != glb_nUserID)
		sMsg = 'Somente o gestor imediato pode alterar uma a��o.';

	if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'EstadoID')) != 41)
		sMsg = 'Esta avalia��o n�o est� aberta.';

	if (sMsg != '')
	{
		if ( window.top.overflyGen.Alert(sMsg) == 0 )
			return null;
		
		return null;
	}
	
	if (fg.Row < 1)
		return null;

	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;
	var nAvaliacaoID = getCellValueByColKey(fg, 'AvaliacaoID*', fg.Row);
	var sErrorMsg = '';
	var nErrorMsg = 0;
	var nLineError = 0;
	
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
    
    for (i=1; i<fg2.Rows; i++)
    {
		nLineError = i;
		
		if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'FatorID')) == 0)
		{
			sErrorMsg += 'Fator';
			nErrorMsg++;
		}
		
		if (trimStr(getCellValueByColKey(fg2, 'Acao', i)) == '')
		{
			sErrorMsg += (sErrorMsg == '' ? '' : ', ') + 'A��o';
			nErrorMsg++;
		}
			
		if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'ResponsavelID')) == 0)
		{
			sErrorMsg += (sErrorMsg == '' ? '' : ', ') + 'Respons�vel';
			nErrorMsg++;
		}
			
		if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'dtInicio')) == 0)
		{
			sErrorMsg += (sErrorMsg == '' ? '' : ', ') + 'In�cio';
			nErrorMsg++;
		}
			
		if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'dtFim')) == 0)
		{
			sErrorMsg += (sErrorMsg == '' ? '' : ', ') + 'Fim';
			nErrorMsg++;
		}
			
		if (sErrorMsg != '')
			break;
	}
	
	if (nErrorMsg > 0)
	{
		if (nErrorMsg == 1)
			sErrorMsg = 'O campo ' + sErrorMsg + ' deve ser preenchido.';
		else
			sErrorMsg = 'Os campos ' + sErrorMsg + ' devem ser preenchidos.';

		fg2.Row = nLineError;
		if ( window.top.overflyGen.Alert(sErrorMsg) == 0 )
            return null;
            
        return null;
	}
	
	lockControlsInModalWin(true);
	
    for (i=1; i<fg2.Rows; i++)
    {
		if (!lineIngGrid2IsValid(i))
			continue;

		nBytesAcum=strPars.length;
		
		if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
		{
			nBytesAcum = 0;
			if (strPars != '')
			{
				glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
				strPars = '';
				nDataLen = 0;
			}
			
			strPars += '?AvaliacaoID=' + escape(nAvaliacaoID);
		}

		nDataLen++;
		strPars += '&bDelete=' + escape(0);
		strPars += '&AvaAcaoID=' + escape(fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'AvaAcaoID')));
		strPars += '&FatorID=' + escape(getCellValueByColKey(fg2, 'FatorID', i));
		
		if (trimStr(getCellValueByColKey(fg2, 'Acao', i)) != '')
			strPars += '&Acao=' + escape('\'' + getCellValueByColKey(fg2, 'Acao', i) + '\'');
		else
			strPars += '&Acao=' + escape('NULL');
			
		strPars += '&ResponsavelID=' + escape(fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'ResponsavelID')));
		
		if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'dtInicio')) != 0)
			strPars += '&dtInicio=' + escape('\'' + normalizeDate_DateTime(getCellValueByColKey(fg2, 'dtInicio', i), true) + '\'');
		else
			strPars += '&dtInicio=' + escape('NULL');
		
		if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'dtFim')) != 0)
			strPars += '&dtFim=' + escape('\'' + normalizeDate_DateTime(getCellValueByColKey(fg2, 'dtFim', i), true) + '\'');
		else
			strPars += '&dtFim=' + escape('NULL');
		
		if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'dtConclusao')) != 0)
			strPars += '&dtConclusao=' + escape('\'' + normalizeDate_DateTime(getCellValueByColKey(fg2, 'dtConclusao', i), true) + '\'');
		else
			strPars += '&dtConclusao=' + escape('NULL');

		strPars += '&Eficaz=' + escape(fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'Eficaz')) == 0 ? 0 : 1);

		if (trimStr(getCellValueByColKey(fg2, 'Observacao', i)) != '')
			strPars += '&Observacao=' + escape('\'' + getCellValueByColKey(fg2, 'Observacao', i) + '\'');
		else
			strPars += '&Observacao=' + escape('NULL');
	}
	
	// Registro deletados
	for (i=0; i<glb_aDeletedActions.length; i++)
	{	
		nDataLen++;
		if (strPars == '')
			strPars += '?AvaliacaoID=' + escape(nAvaliacaoID);

		strPars += '&bDelete=' + escape(1);
		strPars += '&AvaAcaoID=' + escape(glb_aDeletedActions[i]);
		strPars += '&FatorID=' + escape(getCellValueByColKey(fg2, 'FatorID', i));
		strPars += '&Acao=' + escape('');
		strPars += '&ResponsavelID=' + escape(glb_nUserID);
		strPars += '&dtInicio=' + escape('');
		strPars += '&dtFim=' + escape('');
		strPars += '&dtConclusao=' + escape('');
		strPars += '&Eficaz=' + escape(0);
		strPars += '&Observacao=' + escape('');
	}

	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
		
	sendDataToServer2();
}

function sendDataToServer2()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
		    dsoGrava.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodavaliacao/avaliacao/serverside/gravaacao.asp' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer2_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_bFocusModo = false;
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData2()', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_bFocusModo = false;
		glb_OcorrenciasTimerInt = window.setInterval('fillGridData2()', 10, 'JavaScript');  
	}
}

function sendDataToServer2_DSC()
{
	if ( !(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF))
	{
		dsoGrava.recordset.MoveFirst();
		if ( (dsoGrava.recordset['Resultado'].value != null) && 
			 (dsoGrava.recordset['Resultado'].value != '') )
		{
		    if ( window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0 )
				return null;
			
			glb_bFocusModo = false;
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData2()', 10, 'JavaScript');  				
			return null;
		}
	}

	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer2()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

function fillCmbModo()
{
	clearComboEx(['selModo']);
	
    var oOption = document.createElement("OPTION");
    oOption.text = '';
    oOption.value = 0;
    selModo.add(oOption);

    var oOption = document.createElement("OPTION");
    oOption.text = 'Avalia��o';
    oOption.value = 1;
    selModo.add(oOption);

    /*if (chkFatores.checked)
    {
		var oOption = document.createElement("OPTION");
		oOption.text = 'Avaliar';
		oOption.value = 2;
		selModo.add(oOption);
    }*/

    var oOption = document.createElement("OPTION");
    oOption.text = 'Plano de Desenvolvimento';
    oOption.value = 3;
    selModo.add(oOption);
    
    selModo_onchange();
}

function fillCmbFuncionarios()
{
	var nGestorID = selGestor.value;
	var nUserID = getCurrUserID();
	var i = 0;
	clearComboEx(['selFuncionario']);
	
    var oOption = document.createElement("OPTION");
    oOption.text = '';
    oOption.value = 0;
    selFuncionario.add(oOption);

    if (selGestor.selectedIndex == 0)
    {
		var oOption = document.createElement("OPTION");
		oOption.text = glb_sUser;
		oOption.value = nUserID;
		selFuncionario.add(oOption);
    }
    
    if (glb_aFuncionarios.length > 1)
		asort(glb_aFuncionarios, 1);
    
    for (i=0; i<glb_aFuncionarios.length; i++)
    {
		if ((nGestorID == 0) || (glb_aFuncionarios[i][2]==nGestorID))
		{
			if ((glb_aFuncionarios[i][0] != nUserID) || (selGestor.selectedIndex > 0))
			{
				var oOption = document.createElement("OPTION");
				oOption.value = glb_aFuncionarios[i][0];
				oOption.text = glb_aFuncionarios[i][1];
				selFuncionario.add(oOption);
			}
		}
    }
}

function adjustLabelsCombos()
{
    setLabelOfControl(lblGestor, selGestor.value);
    setLabelOfControl(lblFuncionario, selFuncionario.value);
}

function btnFillCombos_onclick()
{
    if (btnFillCmbs.src == glb_LUPA_IMAGES[1].src)
        return;

	if (!chkDataEx(txtdtInicio.value))
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        window.focus();
		txtdtInicio.focus();
		return null;
	}

	if (!chkDataEx(txtdtFim.value))
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        window.focus();
		txtdtFim.focus();
		return null;
	}
	
	fillCombos();
}

function fillCombos()
{
	var strPars = '?EmpresaID=' + escape(glb_aEmpresaData[0]) +
		'&UsuarioID=' + escape(glb_nUserID) +
		'&dtInicio=' + escape(putDateInMMDDYYYY2(txtdtInicio.value)) +
		'&dtFim=' + escape(putDateInMMDDYYYY2(txtdtFim.value));

	lockControlsInModalWin(true);

	dsoCombos.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodavaliacao/avaliacao/serverside/fillcombosavaliacao.aspx' + strPars;
	dsoCombos.ondatasetcomplete = fillCombos_DSC;
	dsoCombos.refresh();
}

function fillCombos_DSC()
{
	var nGestorID = -999;
	var nFuncionarioID = -999;
	var nCounter = 0;
	var nUserID = getCurrUserID();
	
	clearComboEx(['selGestor','selFuncionario']);

	var oOption = document.createElement("OPTION");
	oOption.text = '';
	oOption.value = 0;
	selGestor.add(oOption);

	if (dsoCombos.recordset.fields.count > 0)
	{
		if ( !((dsoCombos.recordset.BOF)&&(dsoCombos.recordset.EOF)) )
		{
			while (!dsoCombos.recordset.EOF)
			{
			    if ((nGestorID != dsoCombos.recordset['GestorID'].value) &&
					(dsoCombos.recordset['GestorID'].value != 0))
				{
					var oOption = document.createElement("OPTION");
					oOption.text = dsoCombos.recordset['Gestor'].value;
					oOption.value = dsoCombos.recordset['GestorID'].value;
					
					if (dsoCombos.recordset['GestorID'].value == nUserID)
						oOption.selected = true;

					selGestor.add(oOption);
				}

			    glb_aFuncionarios[nCounter] = new Array(dsoCombos.recordset['FuncionarioID'].value,
					dsoCombos.recordset['Funcionario'].value,
					dsoCombos.recordset['GestorID'].value);

			    nGestorID = dsoCombos.recordset['GestorID'].value;
				nCounter++;
				dsoCombos.recordset.moveNext();
			}
		}
	}
	
	if (selGestor.selectedIndex >= 0)
		fillCmbFuncionarios();
		
	if ((selGestor.selectedIndex == 0) && (selFuncionario.options.length >= 2))
		selFuncionario.selectedIndex = 1;

	lockControlsInModalWin(false);
	selGestor.disabled = (selGestor.options.length == 0);
	selFuncionario.disabled = (selFuncionario.options.length == 0);
	setupBtns();
	window.focus();
	selFuncionario.focus();
	
	fillGridData();
}

function printGrid()
{
	if (fg.Row <= 0)
		return null;

	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	var sMsg = aEmpresaData[6] + '          Avalia��o de desempenho          ' + getCurrDate();
	fg.PrintGrid(sMsg, false, 2, 0, 450);

	fg.AutoSizeMode = 0;
	fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 1;
}

function emailAvaliacao()
{
	if (fg.Row <= 0)
	{
		if ( window.top.overflyGen.Alert('Selecione uma avalia��o.') == 0 )
            return null;
        
        return null;
    }

	var sMsg = '';

	if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'EstadoID')) != 41)
		sMsg = 'Esta avalia��o n�o est� aberta.';

	if (sMsg != '')
	{
		if ( window.top.overflyGen.Alert(sMsg) == 0 )
			return null;

		return null;
	}

	var nAvaliacaoID = getCellValueByColKey(fg, 'AvaliacaoID*', fg.Row);
	var bAprovou = false;
	var nPapelID = selAtores.value;
	var nPessoaID = 0;
	var nTipoAviso = 0; // 1:Manda para o proximo ator
						// 2:Manda para o ator anterior
						// 3:Manda para o GI informando que todos ja aprovaram
						// 4:Manda para o GI informando que nao aprova esta avaliacao
	var strPars = '';
	var sMsgConfirm = '';
	var sAtor = '';

    // Descobre para quem sera enviado o e-mail
    if (findObservacao(nAvaliacaoID, 2))
        bAprovou = dsoObservacoes.recordset['OK'].value;

	// Manda a avaliacao para frente. A orden eh: 1811(GI) -> 1812(Avaliado) -> 1813(Gestor Superior) -> 1814(RH)
	if (bAprovou)
	{
		dsoObservacoes.recordset.MoveFirst();
		while (!dsoObservacoes.recordset.EOF)
		{
			// Verifica se alguem com precedencia menor ainda nao deu OK
		    if ((dsoObservacoes.recordset['PapelID'].value < nPapelID) &&
				(dsoObservacoes.recordset['OK'].value != 1))
			{
		        nPessoaID = dsoObservacoes.recordset['PessoaID'].value;
				nTipoAviso = 2;
				sMsgConfirm = 'Solicito confirma��o (OK) e o seu parecer desta avalia��o, para darmos sequ�ncia no processo.';
				sAtor = dsoObservacoes.recordset['Ator'].value;
				break;
			}
		        //else if (dsoObservacoes.recordset['PapelID'].value >= nPapelID)
		    else if (dsoObservacoes.recordset['PapelID'].value > nPapelID)
			{
		        nPessoaID = dsoObservacoes.recordset['PessoaID'].value;
				nTipoAviso = 1;
				sMsgConfirm = 'Solicito confirma��o (OK) e o seu parecer desta avalia��o, para darmos sequ�ncia no processo.';
				sAtor = dsoObservacoes.recordset['Ator'].value;
				break;
			}
			dsoObservacoes.recordset.moveNext();
		}

		// Se todos ja aprovaram, avisa o GI que pode finalizar a avaliacao
		if (nPessoaID == 0)
		{
			nTipoAviso = 3;
			sMsgConfirm = 'Todos ja confirmaram esta avalia��o.\nSolicito aprova��o dela.';
		    //sAtor = dsoObservacoes.recordset['Ator'].value;
		}
	}

	if ((!bAprovou) || (nTipoAviso == 3))
	{
		dsoObservacoes.recordset.MoveFirst();
		while (!dsoObservacoes.recordset.EOF)
		{
		    if (dsoObservacoes.recordset['PapelID'].value == 1811)
			{
		        nPessoaID = dsoObservacoes.recordset['PessoaID'].value;
		        sAtor = dsoObservacoes.recordset['Ator'].value;
				if (!bAprovou)
				{
					nTipoAviso = 4;
					sMsgConfirm = 'Solicito revis�o desta avalia��o.';
				}
				break;
			}
			dsoObservacoes.recordset.moveNext();
		}
	}

	sMsgConfirm = 'Confirma o envio do e-mail para ' + sAtor + ' com o seguinte texto?\n\n\n' + sMsgConfirm;

    var _retMsg = window.top.overflyGen.Confirm(sMsgConfirm);

    if ( _retMsg != 1 )
		return null;

	lockControlsInModalWin(true);
	strPars = '?AvaliacaoID=' + escape(nAvaliacaoID) +
		'&PessoaID=' + escape(nPessoaID) +
		'&UsuarioID=' + escape(getCurrUserID()) +
		'&Aviso=' + escape(nTipoAviso);

	dsoAviso.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodavaliacao/avaliacao/serverside/avisoavaliacao.aspx' + strPars;
	dsoAviso.ondatasetcomplete = dsoAviso_DSC;
	dsoAviso.refresh();
}

function dsoAviso_DSC()
{
	lockControlsInModalWin(false);
	
	if (!((dsoAviso.recordset.BOF) && (dsoAviso.recordset.EOF))) {
	    if ((dsoAviso.recordset['Mensagem'].value != '') && (dsoAviso.recordset['Mensagem'].value != null)) {
	        if (window.top.overflyGen.Alert(dsoAviso.recordset['Mensagem'].value) == 0)
	            return null;
	    }
	}
}

function chkOK_onclick()
{
	btnEmailAvaliacao.disabled = true;
}

function setGridTotalLabel()
{
    var nFuncionarioID = 0;
    var nFuncionarios = 0;
    var i=0;
    
    if (fg.Rows > 1)
    {
        for (i=2; i<fg.Rows; i++)
        {
            if (nFuncionarioID != fg.ValueMatrix(i, getColIndexByColKey(fg, 'FuncionarioID*')))
            {
                nFuncionarios++;
                nFuncionarioID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'FuncionarioID*'));
            }
        }
        
        fg.TextMatrix(1, getColIndexByColKey(fg, 'FuncionarioID*')) = nFuncionarios;
    }
}

function TextoAjuda(ID) 
{
    if (ID != '') 
    {
        var TextoAvaliacao = '\n\n';
        dsoAvaliacaoTexto.recordset.setFilter('ID =' + ID);

        if (dsoAvaliacaoTexto.recordset.RecordCount() > 0) 
        {
            dsoAvaliacaoTexto.recordset.MoveFirst();

            while (!dsoAvaliacaoTexto.recordset.EOF) 
            {
                TextoAvaliacao += dsoAvaliacaoTexto.recordset['Observacoes'].value;
                dsoAvaliacaoTexto.recordset.MoveNext();
            }
        }
        dsoAvaliacaoTexto.recordset.setFilter('');
    }

    if (TextoAvaliacao == null)
        TextoAvaliacao = ' ';
    
    return TextoAvaliacao;
}