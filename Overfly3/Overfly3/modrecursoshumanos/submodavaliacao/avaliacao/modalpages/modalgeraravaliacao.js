/********************************************************************
modalgeraravaliacao.js

Library javascript para o modalgeraravaliacao.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_bFirstFill = false;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_nDivFGHeight = 0;

var dsoGrid2 = new CDatatransport('dsoGrid2');
var dsoGrava = new CDatatransport('dsoGrava');
var dsoCombos = new CDatatransport('dsoCombos');
var dsoFatores = new CDatatransport('dsoFatores');
var dsoAvaliacao = new CDatatransport('dsoAvaliacao');
var dsoObservacoes = new CDatatransport('dsoObservacoes');
var dsoTipoOperacional = new CDatatransport('dsoTipoOperacional');
var dsoTipoComando = new CDatatransport('dsoTipoComando');
var dsoCmbResponsaveis = new CDatatransport('dsoCmbResponsaveis');
var dsoAviso = new CDatatransport('dsoAviso');
/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtns()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalgeraravaliacaoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalgeraravaliacaoDblClick(grid, Row, Col)
js_modalgeraravaliacaoKeyPress(KeyAscii)
js_modalgeraravaliacao_ValidateEdit()
js_modalgeraravaliacao_BeforeEdit(grid, row, col)
js_modalgeraravaliacao_AfterEdit(Row, Col)
js_fg_AfterRowColmodalgeraravaliacao (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalgeraravaliacaoBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    glb_bFirstFill = true;

    adjustLabelsCombos();
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Avalia��o de Desempenho', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = btnOK.offsetLeft + btnOK.offsetWidth + ELEM_GAP;
        height = 37;
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        glb_nDivFGHeight = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
        height = glb_nDivFGHeight;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // Reposiciona botao OK
    with (btnOK) {
        style.width = 50;
        style.top = divControls.offsetTop + ELEM_GAP;
        style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
        title = '';
    }

    with (btnListar) {
        style.width = btnOK.currentStyle.width;
        style.height = btnOK.currentStyle.height;
        style.top = btnOK.currentStyle.top;
        style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnListar.currentStyle.width, 10) - 4;
    }

    // Esconde botao cancel
    with (btnCanc) {
        style.visibility = 'hidden';
    }

    adjustElementsInForm([['lbldtInicio', 'txtdtInicio', 9, 1, -10, -10],
		['lbldtFim', 'txtdtFim', 9, 1, -5]], null, null, true);

    txtdtInicio.value = glb_dtInicio;
    txtdtFim.value = glb_dtFim;

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        saveDataInGrid();
    }
    else if (controlID == 'btnListar') {
        fillGridData();
    }
        // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar interface aqui
    setupBtns();

    showExtFrame(window, true);

    window.focus();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtns() {
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    var sDadosAvaliacao = '';
    var aGrid = null;
    var i = 0;
    var nFopagID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FopagID' + '\'' + '].value');
    var sFiltro = '';

    lockControlsInModalWin(true);

    glb_nDSOs = 1;

    // zera o grid
    fg.Rows = 1;

    sFiltro += ' AND b.dtInicio >= dbo.fn_Data_Zero(' + '\'' + putDateInMMDDYYYY2(txtdtInicio.value) + '\'' + ') AND ' +
			'dbo.fn_Data_Zero(b.dtFim) <= ' + '\'' + putDateInMMDDYYYY2(txtdtFim.value) + '\'' + ' ';

    if (selFuncionario.selectedIndex > 0)
        sFiltro += ' AND b.FuncionarioID=' + selFuncionario.value + ' ';
    else {
        sFiltro += ' AND b.FuncionarioID IN (';
        var bFirst = true;
        for (i = 0; i < selFuncionario.options.length; i++) {
            if (selFuncionario.options(i).value != 0) {
                sFiltro += (bFirst ? '' : ',') + selFuncionario.options(i).value;
                bFirst = false;
            }
        }
        sFiltro += ') ';
    }

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT 0 AS AvaFatorID, b.dtInicio, b.FuncionarioID, c.Fantasia AS Funcionario, d.RecursoFantasia AS Cargo, e.ItemMasculino AS TipoAvaliacao,' +
			'dbo.fn_Avaliacao_Nota(b.AvaliacaoID) AS NotaFinal, ' +
			'ISNULL((SELECT g.ItemMasculino ' +
			'FROM TiposAuxiliares_Itens g WITH(NOLOCK) ' +
			'WHERE g.ItemID=dbo.fn_Avaliacao_Parecer(b.AvaliacaoID)), SPACE(1)) AS Parecer, ' +
			'SPACE(0) AS Fator, 0 AS Peso, 0 AS NotaID, SPACE(0) AS Observacao, ' +
			'0 AS PesoAvaliacao, SPACE(0) AS Ajuda, b.TipoAvaliacaoID, 0 AS NotaIDHidden, 0 AS AvaliadorID, ' +
			'b.EstadoID, b.AvaliacaoID ' +
		'FROM AvaliacoesDesempenho b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Recursos d WITH(NOLOCK), TiposAuxiliares_Itens e WITH(NOLOCK) ' +
		'WHERE b.EmpresaID = ' + glb_aEmpresaData[0] + ' AND b.FuncionarioID=c.PessoaID AND b.CargoID=d.RecursoID AND ' +
		'b.TipoAvaliacaoID=e.ItemID ' + sFiltro + ' ' +
		'ORDER BY b.TipoAvaliacaoID, c.Fantasia';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;

    try {
        dsoGrid.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;

    if (glb_nDSOs > 0)
        return null;

    if (glb_bFirstFill) {
        glb_bFirstFill = false;
        showExtFrame(window, true);

        window.focus();
    }

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yy';

    var i;
    var aLinesState = new Array();
    var bTempValue;
    var aHoldCols = new Array();

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    if (!chkFatores.checked) {
        aHoldCols[aHoldCols.length] = 8;
        aHoldCols[aHoldCols.length] = 9;
        aHoldCols[aHoldCols.length] = 10;
        aHoldCols[aHoldCols.length] = 11;
    }

    aHoldCols[aHoldCols.length] = 12;
    aHoldCols[aHoldCols.length] = 13;
    aHoldCols[aHoldCols.length] = 14;
    aHoldCols[aHoldCols.length] = 15;
    aHoldCols[aHoldCols.length] = 16;
    aHoldCols[aHoldCols.length] = 17;

    headerGrid(fg, ['Avalia��o',
		'In�cio',
		'ID',
		'Funcion�rio',
		'Cargo'], [4]);

    fillGridMask(fg, dsoGrid, ['TipoAvaliacao*',
		'dtInicio*',
		'FuncionarioID*',
		'Funcionario*',
		'Cargo*'],
		['', '99/99/99', '', '', ''],
		['', dTFormat, '', '', '']);

    alignColsInGrid(fg, [2]);

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;

    fg.Redraw = 0;
    //fg.FrozenCols = 4;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    paintCellsSpecialyReadOnly(fg);

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        fg.Editable = chkFatores.checked;
        window.focus();
        fg.focus();
        fg.Row = 1;
        fg.Col = 1;
    }
    else {
        ;
    }

    fg.Redraw = 2;

    adjustObsFields();

    // ajusta estado dos botoes
    setupBtns();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;

    lockControlsInModalWin(true);
    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    for (i = 1; i < fg.Rows; i++) {
        nBytesAcum = strPars.length;

        if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
            nBytesAcum = 0;
            if (strPars != '') {
                glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                strPars = '';
                nDataLen = 0;
            }

            strPars += '?AvaliacaoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'AvaliacaoID*')));
        }

        nDataLen++;
        strPars += '&AvaFatorID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'AvaFatorID')));

        if (trimStr(getCellValueByColKey(fg, 'NotaID', i)) != '')
            strPars += '&NotaID=' + escape(getCellValueByColKey(fg, 'NotaID', i));
        else
            strPars += '&NotaID=' + escape('NULL');

        if (trimStr(getCellValueByColKey(fg, 'Observacao', i)) != '')
            strPars += '&Observacao=' + escape('\'' + getCellValueByColKey(fg, 'Observacao', i) + '\'');
        else
            strPars += '&Observacao=' + escape('NULL');
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);

    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodavaliacao/avaliacao/serverside/gravaavaliacao.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            lockControlsInModalWin(false);
            glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        dsoGrava.recordset.MoveFirst();
        if ((dsoGrava.recordset['Resultado'].value != null) &&
			 (dsoGrava.recordset['Resultado'].value != '')) {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0)
                return null;

            glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
            return null;
        }
    }

    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    lockControlsInModalWin(false);

    glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly(grid) {
    paintReadOnlyCols(grid);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgeraravaliacaoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgeraravaliacaoDblClick(grid, Row, Col) {
    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if (getColIndexByColKey(grid, 'OK') != Col)
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);

    glb_PassedOneInDblClick = true;

    // limpa coluna se tem um check box checado
    for (i = 2; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 2; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtns();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgeraravaliacaoKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgeraravaliacao_ValidateEdit(grid, Row, Col) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgeraravaliacao_BeforeEdit(grid, row, col) {
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if (fldDet == null)   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ((fldDet[0] == 200) || (fldDet[0] == 129))  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgeraravaliacao_AfterEdit(Row, Col) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalgeraravaliacao(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return false;

    adjustObsFields();
    setupBtns();
}