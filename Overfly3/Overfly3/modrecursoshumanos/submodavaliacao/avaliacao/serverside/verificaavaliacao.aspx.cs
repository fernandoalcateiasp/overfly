﻿using System;
using System.Data.OleDb;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modrecursoshumanos.submodavaliacao.avaliacao.serverside
{
    public partial class verificaavaliacao : System.Web.UI.OverflyPage
    {

        protected DataSet Pendentes;
        private Integer nAvaliacaoID;

        public Integer AvaliacaoID
        {
            get { return nAvaliacaoID; }
            set { nAvaliacaoID = value; }
        }

        private string Verificacao() 
        {
            string sql = "SELECT b.Fantasia as Fantasia " +
                "FROM  AvaliacoesDesempenho_Observacoes a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " +
                "WHERE (a.AvaliacaoID = " + (AvaliacaoID) + " AND (a.OK = 0 OR a.OK IS NULL) AND a.PessoaID=b.PessoaID) ";


            return sql;
        }
        protected override void PageLoad(object sender, EventArgs e)
        {         

            WriteResultXML(DataInterfaceObj.getRemoteData(Verificacao()));
        }
    }
}