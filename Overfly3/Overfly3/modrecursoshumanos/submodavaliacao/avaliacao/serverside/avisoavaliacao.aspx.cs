﻿using System;
using System.Data.OleDb;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modrecursoshumanos.submodavaliacao.avaliacao.serverside
{
    public partial class avisoavaliacao : System.Web.UI.OverflyPage
    {

        private string erros = "";

        private Integer avaliacaoID;

        public Integer AvaliacaoID
        {
            get { return avaliacaoID; }
            set { avaliacaoID = value; }
        }
        private Integer pessoaID;

        public Integer PessoaID
        {
            get { return pessoaID; }
            set { pessoaID = value; }
        }
        private Integer usuarioID;

        public Integer UsuarioID
        {
            get { return usuarioID; }
            set { usuarioID = value; }
        }
        private Integer aviso;

        public Integer Aviso
        {
            get { return aviso; }
            set { aviso = value; }
        }

        private void AvaliacoesEmail()
        {
            try
            {
                ProcedureParameters[] procParams = new ProcedureParameters[4];

                procParams[0] = new ProcedureParameters(
                    "@AvaliacaoID",
                    System.Data.SqlDbType.Int,
                    avaliacaoID);

                procParams[1] = new ProcedureParameters(
                    "@PessoaID",
                    System.Data.SqlDbType.Int,
                    pessoaID);

                procParams[2] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                  usuarioID);

                procParams[3] = new ProcedureParameters(
                    "@TipoEmail",
                    System.Data.SqlDbType.Int,
                    aviso);

                DataInterfaceObj.execQueryProcedure(
                    "sp_Avaliacoes_Email",
                    procParams);
            }
            catch (System.Exception exception)
            {

                erros += exception.Message + " " + avaliacaoID+ " \r\n ";
            }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            AvaliacoesEmail();
            WriteResultXML(DataInterfaceObj.getRemoteData(
                    "select '" + erros + "' as Mensagem"));
        }
    }
}