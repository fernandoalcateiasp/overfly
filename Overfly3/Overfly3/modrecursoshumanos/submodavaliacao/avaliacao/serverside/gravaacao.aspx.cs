﻿using System;
using System.Data.OleDb;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modrecursoshumanos.submodavaliacao.avaliacao.serverside
{
    public partial class gravaacao : System.Web.UI.OverflyPage
    {
        private Integer DataLen;

        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }
        private Integer avaliacaoID;

        public Integer AvaliacaoID
        {
            get { return avaliacaoID; }
            set { avaliacaoID = value; }
        }
        private Integer[] Delete;

        public Integer[] bDelete
        {
            get { return Delete; }
            set { Delete = value; }
        }

        private Integer[] avaAcaoID;

        public Integer[] AvaAcaoID
        {
            get { return avaAcaoID; }
            set { avaAcaoID = value; }
        }

        private Integer[] fatorID;

        public Integer[] FatorID
        {
            get { return fatorID; }
            set { fatorID = value; }
        }
        private Integer[] acao;

        public Integer[] Acao
        {
            get { return acao; }
            set { acao = value; }
        }
        private Integer[] responsavelID;

        public Integer[] ResponsavelID
        {
            get { return responsavelID; }
            set { responsavelID = value; }
        }

        private Integer[] DtInicio;

        public Integer[] dtInicio
        {
            get { return DtInicio; }
            set { DtInicio = value; }
        }
        private Integer[] DtFim;

        public Integer[] dtFim
        {
            get { return DtFim; }
            set { DtFim = value; }
        }
        private Integer[] DtConclusao;

        public Integer[] dtConclusao
        {
            get { return DtConclusao; }
            set { DtConclusao = value; }
        }
        private Integer[] eficaz;

        public Integer[] Eficaz
        {
            get { return eficaz; }
            set { eficaz = value; }
        }
        private Integer[] observacao;

        public Integer[] Observacao
        {
            get { return observacao; }
            set { observacao = value; }
        }

        private string Error = "";
        private string strSql = " ";


        private void Avaliacoes()
        {
            for (int i = 0; i <= DataLen.intValue(); i++)
            {
                try
                {

                    if (Delete[i].ToString() == "1")
                    {
                        strSql += " DELETE AvaliacoesDesempenho_Acoes WHERE AvaAcaoID=" + (avaAcaoID[i]);

                    }
                    else if (avaAcaoID[i].ToString() != "0")
                    {

                        strSql = "UPDATE AvaliacoesDesempenho_Acoes SET FatorID=" + (fatorID[i].ToString()) + ", " +
                           "Acao=" + (Acao[i].ToString()) + ", " +
                           "ResponsavelID=" + (responsavelID[i].ToString()) + ", " +
                           "dtInicio=" + (DtInicio[i].ToString()) + ", " +
                           "dtFim=" + (DtFim[i].ToString()) + ", " +
                           "dtConclusao=" + (dtConclusao[i].ToString()) + ", " +
                           "Eficaz=" + (eficaz[i].ToString()) + ", " +
                           "Observacao=" + (observacao[i].ToString()) + " " +
                           "WHERE AvaAcaoID=" + (avaAcaoID[i].ToString());

                    }
                    else
                    {
                        strSql = "INSERT INTO AvaliacoesDesempenho_Acoes (AvaliacaoID, FatorID, Acao, ResponsavelID, dtInicio, dtFim, dtConclusao, Eficaz, Observacao) " +
                                        "VALUES (" + (avaliacaoID.ToString()) + ", " + (fatorID[i].ToString()) + ", " + (acao[i].ToString()) + ", " + (responsavelID[i].ToString()) + ", " +
                            (dtInicio[i].ToString()) + ", " + (dtFim[i].ToString()) + ", " + (dtConclusao[i].ToString()) + ", " +
                            (eficaz[i].ToString()) + ", " + (observacao[i].ToString()) + ")";                                            
                    
                    }

                    DataInterfaceObj.ExecuteSQLCommand(strSql);
                }
                catch (System.Exception exception)
                {

                    Error += exception.Message + " \r\n ";
                }

            }// fim do for            


        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            Avaliacoes();
            WriteResultXML(DataInterfaceObj.getRemoteData(
                 "select '" + Error + "' as Resultado"));
        }
    }
}