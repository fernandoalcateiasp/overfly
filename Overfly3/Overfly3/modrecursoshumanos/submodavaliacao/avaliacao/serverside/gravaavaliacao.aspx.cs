﻿using System;
using System.Data.OleDb;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modrecursoshumanos.submodavaliacao.avaliacao.serverside
{
    public partial class gravaavaliacao : System.Web.UI.OverflyPage
    {

        private static string[] emptyStr = new string[0];		
        
        private Integer DataLen;

        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }
        private Integer avaliacaoID;

        public Integer AvaliacaoID
        {
            get { return avaliacaoID; }
            set { avaliacaoID = value; }
        }
        private Integer[] avaFatorID;

        public Integer[] AvaFatorID
        {
            get { return avaFatorID; }
            set { avaFatorID = value; }
        }

        private string[] notaID;

        public string[] NotaID
        {
            get { return notaID; }
            set { notaID = value; }
        }

        private string[] observacao;

        public string[] Observacao
        {
            get { return observacao; }
            set { observacao = value != null ? value : emptyStr; }
        }

        private string Error = "";
        private string strSql = " ";


        private void Avaliacoes()
        {
            for (int i = 0; i < DataLen.intValue(); i++)
            {
                try
                {

                    strSql = "UPDATE AvaliacoesDesempenho_Fatores SET NotaID=" + (notaID[i].ToString()) + ", " +
                            "Observacao=" + (observacao[i]) + " " +
                            "WHERE AvaFatorID=" + (avaFatorID[i].ToString());

                    DataInterfaceObj.ExecuteSQLCommand(strSql);

                }
                catch (System.Exception exception)
                {

                    Error += exception.Message + " \r\n ";
                }

            }// fim do for            


        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            Avaliacoes();
            WriteResultXML(DataInterfaceObj.getRemoteData(
                 "select '" + Error + "' as Resultado"));
        }
    }
}