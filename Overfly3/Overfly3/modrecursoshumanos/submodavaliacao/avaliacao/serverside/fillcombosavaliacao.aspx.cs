﻿using System;
using System.Data.OleDb;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modrecursoshumanos.submodavaliacao.avaliacao.serverside
{
    public partial class fillcombosavaliacao : System.Web.UI.OverflyPage
    {
        private string erros = "";
        private string sSql = "";
        ProcedureParameters[] procParams = new ProcedureParameters[4];

        private Integer empresaID;

        public Integer EmpresaID
        {
            get { return empresaID; }
            set { empresaID = value; }
        }
        private Integer usuarioID;

        public Integer UsuarioID
        {
            get { return usuarioID; }
            set { usuarioID = value; }
        }
        private string DtInicio;

        public string dtInicio
        {
            get { return DtInicio; }
            set { DtInicio = value; }
        }
        private string DtFim;

        public string dtFim
        {
            get { return DtFim; }
            set { DtFim = value; }
        }

        private string AvaliacoesSelecao()
        {
            try
            {
                procParams[0] = new ProcedureParameters(
                    "@EmpresaID",
                    System.Data.SqlDbType.Int,
                    empresaID);

                procParams[1] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                    usuarioID);

                procParams[2] = new ProcedureParameters(
                    "@dtInicio",
                    System.Data.SqlDbType.DateTime,
                  dtInicio);

                procParams[3] = new ProcedureParameters(
                    "@dtFim",
                    System.Data.SqlDbType.DateTime,
                    dtFim);

                sSql = "sp_Avaliacoes_Selecao";
            }
            catch (System.Exception exception)
            {

                erros += exception.Message + " \r\n ";
            }

            return sSql;
        }


        protected override void PageLoad(object sender, EventArgs e)
        {

            WriteResultXML(DataInterfaceObj.execQueryProcedure(AvaliacoesSelecao(), procParams));
        }
    }
}