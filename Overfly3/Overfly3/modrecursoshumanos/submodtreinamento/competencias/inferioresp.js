/********************************************************************
inferioresp.js

Library javascript para o inferior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:	
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
	setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
	fillCmbPastasInCtlBarInf(nTipoRegistroID)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao filtroComplete_DSC(emptyFilter) do js_detailinf.js

Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e no retorno chama a funcao de
ondatasetcomplete de acordo com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.CompetenciaID,a.ProprietarioID,a.AlternativoID,a.Observacoes FROM Competencias a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.CompetenciaID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // Habilidades
    else if (folderID == 31121)
    {
        dso.SQL = 'SELECT * FROM Competencias_Habilidades a WITH(NOLOCK) ' +
        'WHERE ' + sFiltro + ' a.CompetenciaID=' + idToFind + ' ' +
        'ORDER BY (SELECT Competencia FROM Competencias WITH(NOLOCK) WHERE (CompetenciaID = a.HabilidadeID))';
        return 'dso01Grid_DSC';
    }
    // Fornecedores
    else if (folderID == 31122)
    {
        dso.SQL = 'SELECT * FROM Competencias_Fornecedores a WITH(NOLOCK) ' +
        'WHERE ' + sFiltro + ' a.CompetenciaID=' + idToFind + ' ' +
        'ORDER BY Ordem';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao dso01Grid_DSC() do js_detailinf.js

Monta string de select para combo de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
	var aEmpresaData = getCurrEmpresaData();
    setConnection(dso);

    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
	// Habilidades
    else if ( pastaID == 31121 )
    {
		if ( dso.id == 'dsoCmb01Grid_01' )
		{
		    dso.SQL = 'SELECT a.CompetenciaID, a.Competencia ' +
		              'FROM Competencias a WITH(NOLOCK) ' +
		              'WHERE (a.EstadoID=2 AND a.TipoCompetenciaID = 1703) ' +
			          'ORDER BY a.Competencia';
		}
	}
	// Fornecedores
    else if ( pastaID == 31122 )
    {
		if ( dso.id == 'dsoCmb01Grid_01' )
		{
		    dso.SQL = 'SELECT c.ConceitoID AS fldID, c.SimboloMoeda AS fldName ' +
				'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
				'WHERE a.SujeitoID = ' + aEmpresaData[0] + ' AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 AND ' +
					'a.RelacaoID = b.RelacaoID AND b.MoedaID = c.ConceitoID';

		}
		else if ( dso.id == 'dso01GridLkp' )
		{
		    dso.SQL = 'SELECT a.PessoaID, a.Fantasia ' +
		              'FROM Pessoas a WITH(NOLOCK), Competencias_Fornecedores b WITH(NOLOCK) ' +
		              'WHERE (a.PessoaID=b.FornecedorID AND b.CompetenciaID=' + nRegistroID + ')';
		}
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

	if (nTipoRegistroID == 1704)
	{
		vPastaName = window.top.getPastaName(31121);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 31121); //Habilidades

		vPastaName = window.top.getPastaName(31122);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 31122); //Fornecedores
	}

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 31121) // Habilidades
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['CompetenciaID', registroID]);

        if (folderID == 31122) // Fornecedores
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[1], ['CompetenciaID', registroID], [1]);

        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao buildGridFromFolder(folderID) do js_detailinf.js

Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Habilidades
    else if (folderID == 31121)
    {
        headerGrid(fg,['Habilidade',
					   'Observa��o',
                       'ComHabilidadeID'], [2]);

        fillGridMask(fg,currDSO,['HabilidadeID',
								 'Observacao',
								 'ComHabilidadeID'],
								 ['','',''],
                                 ['','','']);

		alignColsInGrid(fg,[]);

		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Fornecedores
    else if (folderID == 31122)
    {
        headerGrid(fg,['Ordem',
					   'FornecedorID',
					   'Fornecedor',
					   'CH',
					   'Moeda',
					   'Valor',
					   'Observa��o',
                       'ComFornecedorID'], [1, 7]);

        glb_aCelHint = [[0,3,'Carga hor�ria']];

        fillGridMask(fg,currDSO,['Ordem',
								 'FornecedorID',
								 '^FornecedorID^dso01GridLkp^PessoaID^Fantasia*',
								 'CargaHoraria',
								 'MoedaID',
								 'Valor',
								 'Observacao',
								 'ComFornecedorID'],
								 ['999','','','9999.99','','99999999.99','',''],
                                 ['###','','','###0.00','','#######0.00','','']);

		alignColsInGrid(fg,[0, 3, 5]);

		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************
