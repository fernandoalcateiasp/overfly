/********************************************************************
inferioresp.js

Library javascript para o inferior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:	
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
	setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
	fillCmbPastasInCtlBarInf(nTipoRegistroID)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao filtroComplete_DSC(emptyFilter) do js_detailinf.js

Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e no retorno chama a funcao de
ondatasetcomplete de acordo com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.RETID,a.ProprietarioID,a.AlternativoID,a.Observacoes FROM RET a WITH(NOLOCK) WHERE '+
        sFiltro + 'a.RETID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // Texto
    else if (folderID == 31151)
	{
	    dso.SQL = 'SELECT a.RETID,a.ProprietarioID,a.AlternativoID,a.Texto FROM RET a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RETID = ' + idToFind;
        return 'dso01JoinSup_DSC';
	}
    // Participantes
    else if (folderID == 31153)
    {
        dso.SQL = 'SELECT a.* FROM RET_Participantes a WITH(NOLOCK) WHERE ' +
			sFiltro + ' a.RETID=' + idToFind + ' ' +
			'ORDER BY (SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID = a.PessoaID)';
        return 'dso01Grid_DSC';
    }
    // Aulas
    else if (folderID == 31152)
    {
        dso.SQL = 'SELECT a.* FROM RET_Aulas a WITH(NOLOCK) WHERE ' +
			sFiltro + ' a.RETID=' + idToFind + ' ' +
			'ORDER BY a.Aula, a.Turma';
        return 'dso01Grid_DSC';
    }
    // Documentos Relacionados
    else if (folderID == 31154)
    {
        dso.SQL = 'EXEC sp_DocumentosRelacionados 620004, ' + idToFind;
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao dso01Grid_DSC() do js_detailinf.js

Monta string de select para combo de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Aulas
    else if (pastaID == 31152)
    {
		var strSQL = '';
		var nAulas = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['Aulas'].value");
		var nTurmas = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['Turmas'].value");
		var i=0;
		
        if ( dso.id == 'dso01GridLkp' )
        {
            strSQL = 'SELECT a.RETAulaID, CONVERT(NUMERIC(6,2), dbo.fn_DivideZero(DATEDIFF(mi, a.dtInicio, a.dtFim), 60)) AS CargaHoraria ' +
                      'FROM RET_Aulas a WITH(NOLOCK) ' +
                      'WHERE a.RETID=' + nRegistroID;
        }
		else if ( dso.id == 'dsoCmb01Grid_01' )
		{
			for (i=0; i<nAulas; i++)
			{
				if (i > 0)
					strSQL += ' UNION ALL ';

				strSQL += 'SELECT ' + (i+1).toString() + ' AS fldName, ' +
					(i+1).toString() + ' AS fldID ';
			}
		}
		else if ( dso.id == 'dsoCmb02Grid_01' )
		{
			for (i=0; i<nTurmas; i++)
			{
				if (i > 0)
					strSQL += ' UNION ALL ';

				strSQL += 'SELECT ' + (i+1).toString() + ' AS fldName, ' +
					(i+1).toString() + ' AS fldID ';
			}
		}
		
		dso.SQL = strSQL;
    }
    // Participantes
    else if (pastaID == 31153)
    {
		var aEmpresaData = getCurrEmpresaData();

		var strSQL = '';
		var nTurmas = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['Turmas'].value");
		var i=0;
		
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM RET_Participantes a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE a.ResponsavelID=b.PessoaID AND a.RETID=' + nRegistroID;
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT a.RETPessoaID, CONVERT(NUMERIC(5,2), dbo.fn_RETParticipantes_CargaHoraria(a.RETPessoaID, 2)) AS Frequencia ' +
                      'FROM RET_Participantes a WITH(NOLOCK) ' +
                      'WHERE a.RETID=' + nRegistroID;
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT a.RETPessoaID, CONVERT(NUMERIC(5,1), dbo.fn_RETParticipantes_CargaHoraria(a.RETPessoaID, 1)) AS CargaHoraria ' +
                      'FROM RET_Participantes a WITH(NOLOCK) ' +
                      'WHERE a.RETID=' + nRegistroID;
        }
		else if ( dso.id == 'dsoCmb01Grid_01' )
		{
		    dso.SQL = 'SELECT DISTINCT c.PessoaID, c.Fantasia ' +
				'FROM RelacoesPesRec_Perfis a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
				'WHERE (a.RelacaoID = b.RelacaoID AND ' +
					'b.EstadoID = 2 AND b.SujeitoID = c.PessoaID AND c.EstadoID = 2) ' +
				'UNION ' +
				'SELECT a.PessoaID, b.Fantasia ' +
				'FROM RET_Participantes a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
				'WHERE a.RETID=' + nRegistroID + ' AND a.PessoaID=b.PessoaID ' +
				'ORDER BY Fantasia';
		}
		else if ( dso.id == 'dsoCmb02Grid_01' )
		{
			for (i=0; i<nTurmas; i++)
			{
				if (i > 0)
					strSQL += ' UNION ALL ';

				strSQL += 'SELECT ' + (i+1).toString() + ' AS fldName, ' +
					(i+1).toString() + ' AS fldID ';
			}
			dso.SQL = strSQL;
		}
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(31151);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 31151); //Texto

    vPastaName = window.top.getPastaName(31152);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 31152); //Aulas

    vPastaName = window.top.getPastaName(31153);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 31153); //Participantes

    vPastaName = window.top.getPastaName(31154);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 31154); //Documentos Relacionados

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
		// Aulas
        if (folderID == 31152)
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0, 1, 2, 3], ['RETID', registroID]);

		// Participantes
        if (folderID == 31153)
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0, 1], ['RETID', registroID], [7]);

        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao buildGridFromFolder(folderID) do js_detailinf.js

Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    var i=0;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Aulas
    else if (folderID == 31152)
    {
        headerGrid(fg,['Aula',
					   'Turma',
					   'In�cio         Hora     ',
                       'Fim            Hora     ',
                       'CH',
                       'Tema',
                       'Local',
                       'Endere�o',
                       'Instrutor',
                       'RETAulaID'], [9]);

        fillGridMask(fg,currDSO,['Aula',
								 'Turma',
								 'dtInicio',
								 'dtFim',
								 '^RETAulaID^dso01GridLkp^RETAulaID^CargaHoraria*',
								 'Tema',
								 'Local',
								 'Endereco',
								 'Instrutor',
								 'RETAulaID'],
								 ['','','99/99/9999 99:99','99/99/9999 99:99','9999.9','','','','',''],
                                 ['','',dTFormat + ' hh:mm',dTFormat + ' hh:mm',    '###0.00','','','','','']);

		alignColsInGrid(fg,[0, 1, 4]);
		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Participantes
    else if (folderID == 31153)
    {
		var bAvaliacaoTreinandos = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['AvaliacaoTreinandos'].value");

        var aHoldCols = new Array();

		if (bAvaliacaoTreinandos)
		{
			aHoldCols = [7, 10]; 

			// array de celulas com hint
			glb_aCelHint = [[0,2,'Percentual de frequ�ncia'],
							[0,3,'Carga hor�ria'],
							[0,5,'Treinamento foi eficaz?']];
		}
		else			
		{
			aHoldCols = [4, 7, 10]; 

			// array de celulas com hint
			glb_aCelHint = [[0,2,'Percentual de frequ�ncia'],
							[0,5,'Treinamento foi eficaz?']];
		}
        
        headerGrid(fg,['Participante',
					   'Turma',
					   'Freq',
					   'CH',
					   'Nota',
                       'Eficaz',
                       'Avalia��o',
                       'RespontavelID',
                       'Respons�vel',
                       'Observa��o',
                       'RETPessoaID'], aHoldCols);

        fillGridMask(fg,currDSO,['PessoaID',
								 'Turma',
								 '^RETPessoaID^dso02GridLkp^RETPessoaID^Frequencia*',
								 '^RETPessoaID^dso03GridLkp^RETPessoaID^CargaHoraria*',
								 'Nota',
								 'Eficaz*',
								 'dtAvaliacao*',
								 'ResponsavelID',
								 '^ResponsavelID^dso01GridLkp^PessoaID^Fantasia*',
								 'Observacao',
								 'RETPessoaID'],
								 ['','','999.99','9999.9','99.9','','99/99/9999','','','',''],
                                 ['','','###.00','#,##0.0','##.0','',dTFormat    ,'','','','']);

		alignColsInGrid(fg,[1, 2, 3, 4]);
		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
        
        // Linha de Totais
        gridHasTotalLine(fg, 'Total', 0xC0C0C0, null, true, [[1,'#####0','C'], [2,'###.0%','M'], [3, '#,##0.00','M'], [4,'##.0','M']]);
    }
    // Documentos Relacionados
    else if (folderID == 31154)
    {
        headerGrid(fg,['Tipo',
					   'DocumentoID',
					   'Documento',
                       'ID',
                       'Est',
                       'Data',
                       'Vencimento',
                       'Conclus�o',
                       'TipoRelacionamentoID',
                       'DocRelacaoID'], [1, 8, 9]);

        fillGridMask(fg,currDSO,['TipoRelacionamento',
								 'DocumentoID',
								 'DocumentoAbreviado',
								 'RegistroID',
								 'Estado',
								 'dtEmissao',
								 'dtVencimento',
								 'dtConclusao',
								 'TipoRelacionamentoID',
								 'DocRelacaoID'],
								 ['','','','','','99/99/9999','99/99/9999','99/99/9999','',''],
                                 ['','','','','',dTFormat,dTFormat,dTFormat,'','']);
		
		alignColsInGrid(fg,[1, 3]);
		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }    
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************
