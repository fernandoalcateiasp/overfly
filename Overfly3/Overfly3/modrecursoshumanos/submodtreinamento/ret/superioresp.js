/********************************************************************
superioresp.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 *, ' +
			   'CONVERT(VARCHAR, dtData, '+DATE_SQL_PARAM+') AS V_dtData, ' +
               'CONVERT(VARCHAR, dtVencimento, '+DATE_SQL_PARAM+') AS V_dtVencimento, ' +
               'CONVERT(NUMERIC(5,1), dbo.fn_RET_CargaHoraria(RETID)) AS CargaHoraria, ' +
			   '(SELECT COUNT(*) FROM RET_Participantes b WITH(NOLOCK) WHERE b.RETID=RET.RETID) AS Participantes, ' +
			   '(SELECT Fantasia FROM Pessoas c WITH(NOLOCK) WHERE c.PessoaID=RET.ProprietarioID) AS Proprietario, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=RET.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM RET WITH(NOLOCK)  ' +
               'WHERE ProprietarioID = '+ nID + 'ORDER BY RETID DESC';
    else
        sSQL = 'SELECT *, ' + 
			   'CONVERT(VARCHAR, dtData, '+DATE_SQL_PARAM+') AS V_dtData, ' +
               'CONVERT(VARCHAR, dtVencimento, '+DATE_SQL_PARAM+') AS V_dtVencimento, ' +
               'CONVERT(NUMERIC(5,1), dbo.fn_RET_CargaHoraria(RETID)) AS CargaHoraria, ' +
			   '(SELECT COUNT(*) FROM RET_Participantes b WITH(NOLOCK) WHERE b.RETID=RET.RETID) AS Participantes, ' +
			   '(SELECT Fantasia FROM Pessoas c WITH(NOLOCK) WHERE c.PessoaID=RET.ProprietarioID) AS Proprietario, ' +
               'dbo.fn_RET_CargaHoraria(RETID) AS CargaHoraria, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=RET.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM RET WITH(NOLOCK) WHERE RETID = ' + nID + ' ORDER BY RETID DESC';

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);

    var sql;

    sql = 'SELECT *, ' +
		  'CONVERT(VARCHAR, dtData, '+DATE_SQL_PARAM+') AS V_dtData, ' +
          'CONVERT(VARCHAR, dtVencimento, '+DATE_SQL_PARAM+') AS V_dtVencimento, ' +
          'CONVERT(NUMERIC(5,1), dbo.fn_RET_CargaHoraria(RETID)) AS CargaHoraria, ' +
		  '(SELECT COUNT(*) FROM RET_Participantes b WHERE b.RETID=RET.RETID) AS Participantes, ' +
		  '(SELECT Fantasia FROM Pessoas c WHERE c.PessoaID=RET.ProprietarioID) AS Proprietario, ' +
          '0 AS Prop1, 0 AS Prop2 ' +
          'FROM RET WITH(NOLOCK) WHERE RETID = 0';

    dso.SQL = sql;
}
