/********************************************************************
modalparticipantes.js

Library javascript para o modalparticipantes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_nDSOs = 0;
var glb_FillGridTimer = null;

//  Dados do grid .RDS  
var dsoGrid = new CDatatransport("dsoGrid");
var dsoCmbGrid01 = new CDatatransport("dsoCmbGrid01");
//  Dados do grid .RDS  
var dsoGrava = new CDatatransport("dsoGrava");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin()
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalparticipantes.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalparticipantes.ASP

js_fg_modalparticipantesBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalparticipantesDblClick( grid, Row, Col)
js_modalparticipantesKeyPress(KeyAscii)
js_modalparticipantes_AfterRowColChange
js_modalparticipantes_ValidateEdit()
js_modalparticipantes_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalparticipantesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   

	showExtFrame(window, true);
	lockControlsInModalWin(false);    
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Participantes', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
    
    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblTipoOperacao','selTipoOperacao',10,1,-10,-10],
						  ['lblPessoaID','selPessoaID',20,1],
						  ['lblTurma','selTurma',4,1],
						  ['lblAula','selAula',5,1]], null, null, true);

	selTipoOperacao.selectedIndex = -1;
	selTipoOperacao.onchange = selTipoOperacao_onchange;
	selPessoaID.onchange = selPessoaID_onchange;
	selAula.onchange = selAula_onchange;
	selTurma.onchange = selTurma_onchange;
	
    // ajusta o divFields
    with (divFields.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = selPessoaID.offsetTop + selPessoaID.offsetHeight + 2;
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divFields.currentStyle.top, 10) + parseInt(divFields.currentStyle.height, 10) + 5;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - divFG.offsetTop - (ELEM_GAP * 1.5);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // Reposiciona botao Refresh
    with (btnRefresh)
    {         
		style.top = parseInt(divFields.currentStyle.top, 10) + (ELEM_GAP / 2);
		style.left = parseInt(selAula.currentStyle.left) + parseInt(selAula.currentStyle.width) + 2*ELEM_GAP;
		style.visibility = 'inherit';
    }

    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = parseInt(divFields.currentStyle.top, 10) + (ELEM_GAP / 2);
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		value = 'Gravar';
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
    
    selTipoOperacao_onchange();
}

function selTipoOperacao_onchange()
{
	// nTipoOperacao == 1 -> Incluir
	//				 == 2 -> Presenca
	//				 == 3 -> Nota
	var nTipoOperacao = selTipoOperacao.value;
	var sVisibility = (((nTipoOperacao==2) || (nTipoOperacao==3)) ? 'inherit' : 'hidden');

	lblPessoaID.style.visibility = sVisibility;
	selPessoaID.style.visibility = sVisibility;
	lblAula.style.visibility = sVisibility;
	selAula.style.visibility = sVisibility;
	lblTurma.style.visibility = sVisibility;
	selTurma.style.visibility = sVisibility;
	btnRefresh.style.visibility = sVisibility;

	if (nTipoOperacao == 1)
		refreshParamsAndDataAndShowModalWin();
	else
		fg.Rows = 1;
}

function selPessoaID_onchange()
{
	fg.Rows = 1;
}

function selAula_onchange()
{
	fg.Rows = 1;
}

function selTurma_onchange()
{
	fg.Rows = 1;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnRefresh')
    {
		refreshParamsAndDataAndShowModalWin();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.
********************************************************************/
function refreshParamsAndDataAndShowModalWin()
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    lockControlsInModalWin(true);
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var bHasRowsInGrid = fg.Rows > 1;
    
    btnOK.disabled = !(bHasRowsInGrid && glb_dataGridWasChanged);
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	var aGrid = null;
	var i = 0;
	var nEmpresaID = glb_aEmpresaData[0];
	var strSQL = '';

	lockControlsInModalWin(true);
	
	if (glb_FillGridTimer != null)
	{
	    window.clearInterval(glb_FillGridTimer);
	    glb_FillGridTimer = null;
	}

	// zera o grid
	fg.Rows = 1;

	glb_dataGridWasChanged = false;
	
	// Incluir
	if (selTipoOperacao.value == 1)
	{
		glb_nDSOs = 2;
    
		// parametrizacao do dso dsoGrid
		setConnection(dsoGrid);

		dsoGrid.SQL = 'SELECT DISTINCT c.PessoaID, c.Fantasia ' +
			'FROM RelacoesPesRec_Perfis a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
			'WHERE ((a.EmpresaID = ' + nEmpresaID + ' AND a.OK = 1 AND a.RelacaoID = b.RelacaoID AND ' +
				'b.EstadoID = 2 AND b.SujeitoID = c.PessoaID AND c.EstadoID = 2) AND ' +
				'(c.PessoaID NOT IN (SELECT PessoaID FROM RET_Participantes WITH(NOLOCK) WHERE RETID=' + glb_nRETID + '))) ' +
			'ORDER BY c.Fantasia';

		dsoGrid.ondatasetcomplete = fillGridData_DSC;
		dsoGrid.Refresh();
    
		for (i=0; i<glb_nTurmas; i++)
		{
			if (i==0)
				strSQL += 'SELECT SPACE(1) AS fldName, 0 AS fldID ';

			strSQL += ' UNION ALL ' +
				'SELECT ' + '\'' + (i+1).toString() + '\'' + ' AS fldName, ' +
					(i+1).toString() + ' AS fldID ';
		}

		setConnection(dsoCmbGrid01);
		dsoCmbGrid01.SQL = strSQL;
		dsoCmbGrid01.ondatasetcomplete = fillGridData_DSC;
		dsoCmbGrid01.Refresh();
	}
	// Presenca
	else if (selTipoOperacao.value == 2)
	{
		glb_nDSOs = 1;
    
		// parametrizacao do dso dsoGrid
		setConnection(dsoGrid);

		strSQL = 'EXEC sp_RET_ParticipantesSincroniza ' + glb_nRETID;

		if (selPessoaID.selectedIndex >= 1)
			strSQL += ', ' + selPessoaID.value + ' ';
		else
			strSQL += ', NULL ';
			
		if (selAula.selectedIndex >= 1)
			strSQL += ', ' + selAula.value + ' ';
		else
			strSQL += ', NULL ';

		if (selTurma.selectedIndex >= 1)
			strSQL += ', ' + selTurma.value + ' ';
		else
			strSQL += ', NULL ';
			
		dsoGrid.SQL = strSQL;
		dsoGrid.ondatasetcomplete = fillGridData_DSC;
		dsoGrid.Refresh();
	}
	// Nota
	else if (selTipoOperacao.value == 3)
	{
		glb_nDSOs = 1;
    
		// parametrizacao do dso dsoGrid
		setConnection(dsoGrid);

		strSQL = 'SELECT DISTINCT a.RETPessoaID, b.Fantasia, a.Nota ' +
			'FROM RET_Participantes a WITH(NOLOCK), Pessoas b WITH(NOLOCK), RET_Aulas c WITH(NOLOCK) ' +
			'WHERE a.RETID=' + glb_nRETID + ' AND a.PessoaID=b.PessoaID AND ' +
				'c.RETID=' + glb_nRETID + ' AND a.Turma=c.Turma ';

		if (selPessoaID.selectedIndex >= 1)
			strSQL += ' AND a.PessoaID=' + selPessoaID.value + ' ';
			
		if (selTurma.selectedIndex >= 1)
			strSQL += ' AND a.Turma=' + selTurma.value + ' ';

		if (selAula.selectedIndex >= 1)
			strSQL += ' AND c.Aula=' + selAula.value + ' ';
			
		strSQL += 'ORDER BY b.Fantasia';

		dsoGrid.SQL = strSQL;
		dsoGrid.ondatasetcomplete = fillGridData_DSC;
		dsoGrid.Refresh();
	}
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

    showExtFrame(window, true);
    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
    var bTempValue;
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    removeCombo(fg);
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	// Incluir
	if (selTipoOperacao.value == 1)
	{
		headerGrid(fg,['Fantasia',
		               'Turma',
		               'PessoaID'], [2]);

		fillGridMask(fg,dsoGrid,['Fantasia*',
								 '_calc_Turma_7',
								 'PessoaID'],
								 ['', '', ''],
		                         ['', '', '']);

		insertcomboData(fg,1,dsoCmbGrid01,'fldName','fldID');
		alignColsInGrid(fg,[1]);
    }
	// Presenca
	else if (selTipoOperacao.value == 2)
	{
		headerGrid(fg,['Colaborador',
		               'Turma',
		               'Aula',
		               'Presen�a',
		               'Observa��o',
		               '_calc_RegistroAlterado_1',
		               'RETPartPresencaID'], [5, 6]);

		fillGridMask(fg,dsoGrid,['Fantasia*',
								 'Turma*',
								 'Aula*',
								 'Presenca',
								 'Observacao',
								 '_calc_RegistroAlterado_1',
								 'RETPartPresencaID'],
								 ['', '9', '99', '', '', ''],
		                         ['', '#', '##', '', '', '']);

		alignColsInGrid(fg,[1, 2]);
    }
	// Nota
	else if (selTipoOperacao.value == 3)
	{
		headerGrid(fg,['Fantasia',
		               'Nota',
		               'RETPessoaID',
		               '_calc_RegistroAlterado_1'], [2, 3]);

		fillGridMask(fg,dsoGrid,['Fantasia*',
								 'Nota',
								 'RETPessoaID',
								 '_calc_RegistroAlterado_1'],
								 ['', '99.9', '', ''],
		                         ['', '#0.0', '', '']);

		alignColsInGrid(fg,[1]);
    }
    
    // Linha de Totais
	gridHasTotalLine(fg, 'Total', 0xC0C0C0, null, true, [[1,'#####0','C']]);
    
	if (fg.Rows >= 3)
	{
		fg.TextMatrix(1, 0) = fg.Rows - 2;
		fg.TextMatrix(1, 1) = '';
	}
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
	fg.FrozenCols = 1;
    fg.Redraw = 2;
    
    if (fg.Rows > 2)
    {
        fg.Editable = true;
        fg.Row = 2;
    }    
        
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nTurma = 0;
	var nRegistroAlterado = 0;
	var sNota = '';
	
    glb_dataGridWasChanged = false;
    lockControlsInModalWin(true);
    
	strPars = '?nTipoOperacao=' + escape(selTipoOperacao.value);
	
	//Incluir
	if (selTipoOperacao.value==1)
	{
		strPars += '&nRETID=' + escape(glb_nRETID);
    
		for (i=2; i<fg.Rows; i++)
		{
			nTurma = getCellValueByColKey(fg, '_calc_Turma_7', i);
			if ( (nTurma != 0) && (nTurma != '') && (nTurma != null))
			{
				nDataLen++;
				strPars += '&nPessoaID=' + escape(getCellValueByColKey(fg, 'PessoaID', i));
				strPars += '&nTurma=' + escape(nTurma);
			}
		}
	}
	//Presenca
	else if (selTipoOperacao.value==2)
	{
		for (i=2; i<fg.Rows; i++)
		{
			nRegistroAlterado = getCellValueByColKey(fg, '_calc_RegistroAlterado_1', i);
			if (nRegistroAlterado == 1)
			{
				nDataLen++;
				strPars += '&nRETPartPresencaID=' + escape(getCellValueByColKey(fg, 'RETPartPresencaID', i));
				strPars += '&bPresenca=' + escape((getCellValueByColKey(fg, 'Presenca', i) != 0 ? 1 : 0));
				strPars += '&sObservacao=' + escape(getCellValueByColKey(fg, 'Observacao', i));
			}
		}
	}
	//Nota
	else if (selTipoOperacao.value==3)
	{
		for (i=2; i<fg.Rows; i++)
		{
			nRegistroAlterado = getCellValueByColKey(fg, '_calc_RegistroAlterado_1', i);
			if (nRegistroAlterado == 1)
			{
				sNota = trimStr(getCellValueByColKey(fg, 'Nota', i));
				sLastChar = sNota.substr(sNota.length - 1, 1);
				
				if ((sLastChar == '.') || (sLastChar == ','))
				{
					if (sNota.length == 1)
						continue;
					
					sNota = sNota.substr(0, sNota.length - 2);
				}
					
				nDataLen++;
				strPars += '&nRETPessoaID=' + escape(getCellValueByColKey(fg, 'RETPessoaID', i));
				strPars += '&nNota=' + escape(sNota);
			}
		}
	}

	strPars += '&nDataLen=' + escape(nDataLen);

	if (nDataLen > 0)
	{
		dsoGrava.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodtreinamento/ret/serverside/gravaparticipantes.aspx' + strPars;
		dsoGrava.ondatasetcomplete = saveDataInGrid_DSC;
		dsoGrava.refresh();
    }
    else
		saveDataInGrid_DSC();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);

	glb_FillGridTimer = window.setInterval('fillGridData()', 10, 'JavaScript');  
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalparticipantesBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalparticipantesDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalparticipantesKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalparticipantes_ValidateEdit()
{
	var valInEditText = transformStringInNumeric(fg.EditText);
	
	if ( isNaN(valInEditText) )
		return null;
	
	if (parseFloat(valInEditText) > 10)	
	{
			fg.EditText = '';
	}	
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalparticipantes_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalparticipantes_AfterEdit(Row, Col)
{
	glb_dataGridWasChanged = true;
	
	if (fg.ColKey(Col).toUpperCase() == 'NOTA')
	{
		if ( isNaN(transformStringInNumeric(fg.TextMatrix(fg.Row, fg.Col))) )
			fg.TextMatrix(fg.Row, fg.Col) = '';
	}

	// Altera esta celula para informar a gravacao quais linhas foram
	// alteradas pelo usuario
	if (getColIndexByColKey(fg, '_calc_RegistroAlterado_1') >= 0)
		fg.TextMatrix(Row, getColIndexByColKey(fg, '_calc_RegistroAlterado_1')) = 1;

	setupBtnsFromGridState();
	return null;
}
// FINAL DE EVENTOS DE GRID *****************************************
