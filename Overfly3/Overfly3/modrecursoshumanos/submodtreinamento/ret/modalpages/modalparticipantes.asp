
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalparticipantesHtml" name="modalparticipantesHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modrecursoshumanos/submodtreinamento/ret/modalpages/modalparticipantes.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modrecursoshumanos/submodtreinamento/ret/modalpages/modalparticipantes.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nRETID, nAulas, nTurmas, nAvaliacaoTreinandos
Dim strSQL, rsData

sCaller = ""
nRETID = 0
nAulas = 0
nTurmas = 0
nAvaliacaoTreinandos = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nRETID").Count
    nRETID = Request.QueryString("nRETID")(i)
Next

For i = 1 To Request.QueryString("nAulas").Count
    nAulas = Request.QueryString("nAulas")(i)
Next

For i = 1 To Request.QueryString("nTurmas").Count
    nTurmas = Request.QueryString("nTurmas")(i)
Next

For i = 1 To Request.QueryString("nAvaliacaoTreinandos").Count
    nAvaliacaoTreinandos = Request.QueryString("nAvaliacaoTreinandos")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";" & vbcrlf

Response.Write "var glb_nRETID = " & nRETID & ";" & vbcrlf
Response.Write "var glb_nAulas = " & nAulas & ";" & vbcrlf
Response.Write "var glb_nTurmas = " & nTurmas & ";" & vbcrlf

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalparticipantes_ValidateEdit();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalparticipantes_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalparticipantesKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalparticipantes_BeforeEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modalparticipantesDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalparticipantesBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);

if ( (fg.Editable) && (fg.Rows >=3) && (NewRow < 2) ) 
{
	fg.Editable = false;
	fg.Row = 2;
	fg.Editable = true;
}
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

</head>

<body id="modalparticipantesBody" name="modalparticipantesBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
    <!-- Div do grid -->
    <div id="divFields" name="divFields" class="divGeneral">
        <p id="lblTipoOperacao" name="lblTipoOperacao" class="lblGeneral">Opera��o</p>
		<select id="selTipoOperacao" name="selTipoOperacao" class="fldGeneral">
<%
	Response.Write "<option value =" & Chr(39) & "1" & Chr(39) & ">" & _
               "Incluir</option>" & chr(13) & chr(10)        

	Response.Write "<option value =" & Chr(39) & "2" & Chr(39) & ">" & _
               "Presen�a</option>" & chr(13) & chr(10)        

	If (CLng(nAvaliacaoTreinandos) = 1) Then
		Response.Write "<option value =" & Chr(39) & "3" & Chr(39) & ">" & _
		           "Nota</option>" & chr(13) & chr(10)        
	End If
%>
		</select>	
        <p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">Colaborador</p>
		<select id="selPessoaID" name="selPessoaID" class="fldGeneral">
<%
	Set rsData = Server.CreateObject("ADODB.Recordset")

	    strSQL = "SELECT 0 AS PessoaID, SPACE(1) AS Fantasia " & _
				 "UNION ALL " & _
				 "SELECT a.PessoaID, a.Fantasia " & _
	             "FROM Pessoas a WITH(NOLOCK), RET_Participantes b WITH(NOLOCK) " & _
	             "WHERE b.RETID=" & CStr(nRETID) & " AND a.PessoaID=b.PessoaID " & _
	             "ORDER BY Fantasia"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	While (Not rsData.EOF)
		Response.Write "<option value =" & rsData.Fields("PessoaID").Value & ">" & _
                   rsData.Fields("Fantasia").Value & "</option>" & chr(13) & chr(10)        
	
		rsData.MoveNext
	WEnd

	rsData.Close
	Set rsData = Nothing
%>		
		</select>
        <p id="lblTurma" name="lblTurma" class="lblGeneral">Turma</p>
		<select id="selTurma" name="selTurma" class="fldGeneral">
<%
	Set rsData = Server.CreateObject("ADODB.Recordset")

	strSQL = ""
	For i=1 To nTurmas Step 1
		If (i = 1) Then
			strSQL = strSQL & "SELECT SPACE(1) AS fldName, 0 AS fldID "
		End If	

		strSQL = strSQL & " UNION ALL "

		strSQL = strSQL & "SELECT '" & CStr(i) & "' AS fldName, " & _
			CStr(i) & " AS fldID "
	Next

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	While (Not rsData.EOF)
		Response.Write "<option value =" & rsData.Fields("fldID").Value & ">" & _
                   rsData.Fields("fldName").Value & "</option>" & chr(13) & chr(10)        
	
		rsData.MoveNext
	WEnd

	rsData.Close
	Set rsData = Nothing
%>		
		</select>
        <p id="lblAula" name="lblAula" class="lblGeneral">Aula</p>
		<select id="selAula" name="selAula" class="fldGeneral">
<%
	Set rsData = Server.CreateObject("ADODB.Recordset")

	strSQL = ""
	For i=1 To nAulas Step 1
		If (i = 1) Then
			strSQL = strSQL & "SELECT SPACE(1) AS fldName, 0 AS fldID "
		End If	

		strSQL = strSQL & " UNION ALL "

		strSQL = strSQL & "SELECT '" & CStr(i) & "' AS fldName, " & _
			CStr(i) & " AS fldID "
	Next

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	While (Not rsData.EOF)
		Response.Write "<option value =" & rsData.Fields("fldID").Value & ">" & _
                   rsData.Fields("fldName").Value & "</option>" & chr(13) & chr(10)        
	
		rsData.MoveNext
	WEnd

	rsData.Close
	Set rsData = Nothing
%>		
		</select>
    </div>
    
    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
            
	<input type="button" id="btnRefresh" name="btnRefresh" value="Refresh" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
</body>

</html>
