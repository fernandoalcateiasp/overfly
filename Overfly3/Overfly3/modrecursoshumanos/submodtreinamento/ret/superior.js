/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_BtnFromFramWork = null;
var glb_CounterCmbsDynamics = 0;
var glb_bAlteracao = false;
var glb_backToAutomation = false;

//  Dados do registro corrente .SQL 
var dsoSup01 = new CDatatransport("dsoSup01");
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");
var dsoCmbDynamic02 = new CDatatransport("dsoCmbDynamic02");
//  Descricao do estado atual do registro .SQL 
var dsoStateMachine = new CDatatransport("dsoStateMachine");
//  Dados dos combos estaticos .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
var dsoVerificacao = new CDatatransport("dsoVerificacao");
var dsoEMailParticipante = new CDatatransport("dsoEMailParticipante");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
	window_onload()
	setupPage()
    putSpecialAttributesInControls()
    formFinishLoad()
    prgServerSup(btnClicked)
    finalOfSupCascade(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    
********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function showRideFields(bPaging, initEditMode)
{
	bShow = false;
	sShow = 'hidden';
	
	if (bPaging)
		bShow = (dsoSup01.recordset['AvaliacaoTreinandos'].value == true);
	else
		bShow = chkAvaliacaoTreinandos.checked;
		
	if (bShow)		
		sShow = 'inherit';
	else	
		sShow = 'hidden';
		
	lblNotaMinima.style.visibility = sShow;
	txtNotaMinima.style.visibility = sShow;
	
	if (initEditMode == true)
		txtNotaMinima.style.visibility = 'hidden';
}


// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Evento disparado ao final do carregamento da pagina.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    glb_aStaticCombos = ([['selTreinamentoID', '1'],
						  ['selMoedaID', '2']]);

    windowOnLoad_1stPart();

    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modrecursoshumanos/submodtreinamento/ret/inferior.asp',
                              SYS_PAGESURLROOT + '/modrecursoshumanos/submodtreinamento/ret/pesquisa.asp');

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'RETID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao windowOnLoad_2ndPart() do js_detailsup.js

Usar para ajustes graficos da interface.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
						  ['lblEstadoID','txtEstadoID',2,1],
						  ['lblTreinamentoID','selTreinamentoID',30,1],
						  ['lbldtData','txtdtData',10,1],
						  ['lbldtVencimento','txtdtVencimento',10,1],
						  ['lblAvaliacaoTreinandos','chkAvaliacaoTreinandos',3,1],
						  ['lblAvaliacaoTreinamento','chkAvaliacaoTreinamento',3,1,5],
						  ['lblAvaliacaoEficacia','chkAvaliacaoEficacia',3,1,5],
						  ['lblFornecedorID','selFornecedorID',22,2],
						  ['lblMoedaID','selMoedaID',7,2],
						  ['lblValor','txtValor',12,2],
						  ['lblCargaHoraria','txtCargaHoraria',6,2],
						  ['lblFrequenciaMinima','txtFrequenciaMinima',4,2],
						  ['lblNotaMinima','txtNotaMinima',4,2],
						  ['lblAulas','txtAulas',3,2],
						  ['lblTurmas','txtTurmas',2,2],
						  ['lblParticipantes','txtParticipantes', 4, 2],
						  ['lblProprietario','txtProprietario', 20, 3],
						  ['lblObservacao','txtObservacao',30,3]], null, null, true);

	chkAvaliacaoTreinandos.onclick = chkAvaliacaoTreinandos_onclick;
}

function chkAvaliacaoTreinandos_onclick()
{
	showRideFields(false, true);
}

/********************************************************************
Funcao disparada pelo frame work.
Chamado pela funcao windowOnLoad_2ndPart() do js_superior.js

Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    txtFrequenciaMinima.setAttribute('minMax', new Array(0, 100), 1);
    txtNotaMinima.setAttribute('minMax', new Array(0, 10), 1);
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    // especBtnIsPrintBtn('sup', 1);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Texto', 'E-mail de convoca��o dos participantes']);
    
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);
}

/********************************************************************
Funcao disparada pelo frame work.
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;

    if ( (btnClicked == 'SUPDET') ||
         (btnClicked == 'SUPREFR') ||
         (btnClicked == 'SUPANT') ||
         (btnClicked == 'SUPSEG') )
    {
		glb_backToAutomation = true;
        startDynamicCmbs(dsoSup01.recordset['TreinamentoID'].value);
    }    
    else    
		finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada ao final da funcao prgServerInf(btnClicked) que e
disparada pelo frame work,

Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    
    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    
    clearAndLockCmbsDynamics(btnClicked);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Texto', 'E-mail de convoca��o dos participantes']);
    
    if ( btnClicked == 'SUPINCL' )
    {
        // funcao que controla campos read-only
        controlReadOnlyFields();
	}    

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs(nTreinamentoID)
{
    // controla o retorno do servidor dos dados de combos dinamicos
    glb_CounterCmbsDynamics = 1;

    // parametrizacao do dso dsoCmbDynamic01 (designado para selSujeitoID)
    setConnection(dsoCmbDynamic01);
    
    dsoCmbDynamic01.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS MoedaID, -1 AS Valor ' +
						  'UNION ALL ' +
						  'SELECT b.PessoaID AS fldName, b.Fantasia AS fldName, a.MoedaID, a.Valor ' +
						  'FROM Competencias_Fornecedores a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
						  'WHERE (a.CompetenciaID = ' + nTreinamentoID + ' AND ' +
						  'a.FornecedorID = b.PessoaID AND b.EstadoID = 2)';

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();
}

/********************************************************************
Funcao disparada pelo programador.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC()
{
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selFornecedorID];
    var aDSOsDunamics = [dsoCmbDynamic01];

    clearComboEx(['selFornecedorID']);
    
    glb_CounterCmbsDynamics --;
    
    if (glb_CounterCmbsDynamics == 0)
    {
        for (i=0; i<=0; i++)
        {
            while (! aDSOsDunamics[i].recordset.EOF )
            {
                optionStr = aDSOsDunamics[i].recordset['fldName'].value;
	        	optionValue = aDSOsDunamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
				oOption.setAttribute('MoedaID', aDSOsDunamics[i].recordset['MoedaID'].value, 1);
				oOption.setAttribute('Valor', aDSOsDunamics[i].recordset['Valor'].value, 1);
                aCmbsDynamics[i].add(oOption);
                aDSOsDunamics[i].recordset.MoveNext();
            }
        }

        adjustLabelsCombos(true);
	    
		if (glb_backToAutomation)
		{
			glb_backToAutomation = false;
			
			// volta para a automacao
			finalOfSupCascade(glb_BtnFromFramWork);
		}
		else
		{
			selFornecedorID.disabled = selFornecedorID.options.length <= 1;
		}
	}
	return null;
}


/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao selChanged() do js_interfaceex.js

Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();
    else if (cmbID == 'selTreinamentoID')
    {
		glb_backToAutomation = false;
		startDynamicCmbs(selTreinamentoID.value);
	}
    else if (cmbID == 'selFornecedorID')
    {
		selMoedaID.selectedIndex = 0;
		
		var nMoedaFornecedor = selFornecedorID.options[selFornecedorID.selectedIndex].getAttribute('MoedaID', 1);
		var nValorFornecedor = selFornecedorID.options[selFornecedorID.selectedIndex].getAttribute('Valor', 1);
		var nSearchValue = lookUpValueInCmb(selMoedaID, nMoedaFornecedor);

		if (nValorFornecedor == -1)
			txtValor.value = '';
		else
			txtValor.value = nValorFornecedor;

		if ((nSearchValue != null) && (nSearchValue >= 0))
			selMoedaID.selectedIndex = lookUpValueInCmb(selMoedaID, nMoedaFornecedor);
	}

	adjustLabelsCombos(false);        

    // Final de Nao mexer - Inicio de automacao =====================
}

function adjustLabelsCombos(bPaging)
{
    if (bPaging)
    {
        setLabelOfControl(lblTreinamentoID, dsoSup01.recordset['TreinamentoID'].value);
        setLabelOfControl(lblFornecedorID, dsoSup01.recordset['FornecedorID'].value);
    }
    else
    {
        setLabelOfControl(lblTreinamentoID, selTreinamentoID.value);
        setLabelOfControl(lblFornecedorID, selFornecedorID.value);
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo evento onClickBtnLupa() do js_detailsup.js

Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	var nEstadoID = dsoSup01.recordset['EstadoID'].value;
	var nAnchorID = '';

	// Documentos
	if (btnClicked == 1) {
		__openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
	}
	if (btnClicked == 3)
	{
		if (nEstadoID == 1)
			nAnchorID = '222';
		else if (nEstadoID == 96)
			nAnchorID = '23';
		else if (nEstadoID == 97)
			nAnchorID = '232';
		else if (nEstadoID == 98)
			nAnchorID = '24';

		window.top.openModalControleDocumento('S', '', 620, null, nAnchorID, 'T');
	}
	else if (btnClicked == 4)
		window.top.openModalHTML(dsoSup01.recordset['RETID'].value, 'Texto', 'S', 7);
	else if (btnClicked == 5)
		enviaEMailParticipante();
}

function enviaEMailParticipante()
{
    lockInterface(true);
    var strPars = new String();

    strPars = '?nRETID=' + escape(dsoSup01.recordset['RETID'].value);
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    dsoEMailParticipante.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodtreinamento/ret/serverside/emailparticipante.aspx' + strPars;
    dsoEMailParticipante.ondatasetcomplete = enviaEMailParticipante_DSC;
    dsoEMailParticipante.refresh();
}

function enviaEMailParticipante_DSC()
{
    lockInterface(false);
    
    if ( !(dsoEMailParticipante.BOF && dsoEMailParticipante.EOF) )
    {
        dsoEMailParticipante.recordset.MoveFirst();
        if ( window.top.overflyGen.Alert ( dsoEMailParticipante.recordset['fldResponse'].value ) == 0 )
			return null;
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    var aEmpresaData = getCurrEmpresaData();
    var sTelefone = '';
    var sDDD = '';
    var i;
    var sAulas, sTurmas;

    if (btnClicked == 'SUPOK')
    {
		sAulas = trimStr(txtAulas.value);
		sTurmas = trimStr(txtTurmas.value);

		if ( (sAulas == '') || (isNaN(sAulas)) || (parseInt(sAulas) <= 0) )
		{
			if ( window.top.overflyGen.Alert('Preencha o campo Aulas com valor positivo') == 0 )
			    return null;
			
			window.focus();
			txtAulas.focus();
			
			return false;
		}
		if ( (sTurmas == '') || (isNaN(sTurmas)) || (parseInt(sTurmas) <= 0) )
		{
			if ( window.top.overflyGen.Alert('Preencha o campo Turmas com valor positivo') == 0 )
			    return null;
			    
			window.focus();
			txtTurmas.focus();

			return false;
		}

		if(dsoSup01.recordset[glb_sFldIDName].value == null)
		{
            // Grava o campo empresaID
            dsoSup01.recordset['EmpresaID'].value = aEmpresaData[0];
		}
	}
			
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALOPENHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
    	if (param1 == 'OK') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    	else if (param1 == 'CANCEL') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao editRegister() do js_superior.js

Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // funcao que controla campos read-only
    controlReadOnlyFields();

	// chamada abaixo necessaria para prosseguir a automacao
	// mover para final de operacoes de servidor se for o caso
	supInitEditMode_Continue();
	
	showRideFields(false, true);
}

function clearAndLockCmbsDynamics(btnClicked)
{
    if (btnClicked == 'SUPINCL')
    {
        clearComboEx(['selFornecedorID']);
        selFornecedorID.disabled = true;
	}
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************

// FUNCOES DO CARRIER ***********************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWRET')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
        
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
            
        window.top.focus();
                
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{
    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE FUNCOES DO CARRIER **************************************

// FUNCOES DA MAQUINA DE ESTADO *************************************

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
	verifyRetInServer(currEstadoID, newEstadoID);
	return true;
}

/********************************************************************
Verificacoes da Relacao
********************************************************************/
function verifyRetInServer(currEstadoID, newEstadoID)
{
    var nRETID = dsoSup01.recordset['RETID'].value;
    var strPars = new String();

    strPars = '?nRETID='+escape(nRETID);
    strPars += '&nEstadoDeID='+escape(currEstadoID);
    strPars += '&nEstadoParaID='+escape(newEstadoID);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodtreinamento/ret/serverside/verificacaoret.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyRetInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes da Relacao
********************************************************************/
function verifyRetInServer_DSC()
{
    if (dsoVerificacao.recordset['Verificacao'].value == null)
    {
        stateMachSupExec('OK');
    }    
    else
    {
        if ( window.top.overflyGen.Alert(dsoVerificacao.recordset['Verificacao'].value) == 0 )
            return null;
        stateMachSupExec('CANC');
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{

}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Read Only

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields()
{
	txtCargaHoraria.readOnly = true;
	txtParticipantes.readOnly = true;
	txtProprietario.readOnly = true;

	return null;
}

// FINAL DE FUNCOES DA MAQUINA DE ESTADO ****************************

// FUNCOES ESPECIFICAS DO FORM **************************************

// FINAL DE FUNCOES ESPECIFICAS DO FORM *****************************