<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="retsup01Html" name="retsup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modqualidade/commonqualidade/commonqualidade.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modrecursoshumanos/submodtreinamento/ret/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modrecursoshumanos/submodtreinamento/ret/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="retsup01Body" name="retsup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p id="lblTreinamentoID" name="lblTreinamentoID" class="lblGeneral">Treinamento</p>
		<select id="selTreinamentoID" name="selTreinamentoID" DATASRC="#dsoSup01" DATAFLD="TreinamentoID" class="fldGeneral"></select>
        <p id="lbldtData" name="lbldtData" class="lblGeneral">Data</p>
        <input type="text" id="txtdtData" name="txtdtData" DATASRC="#dsoSup01" DATAFLD="V_dtData" class="fldGeneral"></input>
        <p id="lbldtVencimento" name="lbldtVencimento" class="lblGeneral">Vencimento</p>
        <input type="text" id="txtdtVencimento" name="txtdtVencimento" DATASRC="#dsoSup01" DATAFLD="V_dtVencimento" class="fldGeneral"></input>
        <p  id="lblAvaliacaoTreinandos" name="lblAvaliacaoTreinandos" class="lblGeneral">Rea��o</p>
        <input type="checkbox" id="chkAvaliacaoTreinandos" name="chkAvaliacaoTreinandos" DATASRC="#dsoSup01" DATAFLD="AvaliacaoTreinandos" class="fldGeneral" title="Avalia��o dos treinandos pelo instrutor (Rea��o)?"></input>
        <p  id="lblAvaliacaoTreinamento" name="lblAvaliacaoTreinamento" class="lblGeneral">Evento</p>
        <input type="checkbox" id="chkAvaliacaoTreinamento" name="chkAvaliacaoTreinamento" DATASRC="#dsoSup01" DATAFLD="AvaliacaoTreinamento" class="fldGeneral" title="Avalia��o do treinamento pelos treinandos (Evento)?"></input>
        <p  id="lblAvaliacaoEficacia" name="lblAvaliacaoEficacia" class="lblGeneral">Efic�cia</p>
        <input type="checkbox" id="chkAvaliacaoEficacia" name="chkAvaliacaoEficacia" DATASRC="#dsoSup01" DATAFLD="AvaliacaoEficacia" class="fldGeneral" title="Avalia��o dos treinandos pelo superior (Efic�cia)?"></input>
        <p id="lblFornecedorID" name="lblFornecedorID" class="lblGeneral">Fornecedor</p>
		<select id="selFornecedorID" name="selFornecedorID" DATASRC="#dsoSup01" DATAFLD="FornecedorID" class="fldGeneral"></select>
        <p id="lblMoedaID" name="lblMoedaID" class="lblGeneral">Moeda</p>
		<select id="selMoedaID" name="selMoedaID" DATASRC="#dsoSup01" DATAFLD="MoedaID" class="fldGeneral"></select>
        <p id="lblValor" name="lblValor" class="lblGeneral">Valor</p>
        <input type="text" id="txtValor" name="txtValor" DATASRC="#dsoSup01" DATAFLD="Valor" class="fldGeneral"></input>
        <p id="lblCargaHoraria" name="lblCargaHoraria" class="lblGeneral">CH</p>
        <input type="text" id="txtCargaHoraria" name="txtCargaHoraria" DATASRC="#dsoSup01" DATAFLD="CargaHoraria" class="fldGeneral" title="Carga hor�ria"></input>
        <p id="lblFrequenciaMinima" name="lblFrequenciaMinima" class="lblGeneral">FM</p>
        <input type="text" id="txtFrequenciaMinima" name="txtFrequenciaMinima" DATASRC="#dsoSup01" DATAFLD="FrequenciaMinima" class="fldGeneral" title="Frequ�ncia m�nima (%)"></input>
        <p id="lblNotaMinima" name="lblNotaMinima" class="lblGeneral">NM</p>
        <input type="text" id="txtNotaMinima" name="txtNotaMinima" DATASRC="#dsoSup01" DATAFLD="NotaMinima" class="fldGeneral" title="Nota M�nima"></input>
        <p id="lblAulas" name="lblAulas" class="lblGeneral">Aulas</p>
        <input type="text" id="txtAulas" name="txtAulas" DATASRC="#dsoSup01" DATAFLD="Aulas" class="fldGeneral"></input>
        <p id="lblTurmas" name="lblTurmas" class="lblGeneral">Turmas</p>
        <input type="text" id="txtTurmas" name="txtTurmas" DATASRC="#dsoSup01" DATAFLD="Turmas" class="fldGeneral"></input>
        <p id="lblParticipantes" name="lblParticipantes" class="lblGeneral">Participantes</p>
        <input type="text" id="txtParticipantes" name="txtParticipantes" DATASRC="#dsoSup01" DATAFLD="Participantes" class="fldGeneral"></input>
        <p id="lblProprietario" name="lblProprietario" class="lblGeneral">Propriet�rio</p>
        <input type="text" id="txtProprietario" name="txtProprietario" DATASRC="#dsoSup01" DATAFLD="Proprietario" class="fldGeneral"></input>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
    </div>
    
</body>

</html>
