using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modrecursoshumanos.submodtreinamento.ret.serverside
{
	public partial class gravaparticipantes : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
		}

		protected Integer tipoOperacao = Constants.INT_ZERO;
		protected Integer nTipoOperacao
		{
			get { return tipoOperacao; }
			set { if(value != null) tipoOperacao = value; }
		}

		protected Integer retID = Constants.INT_ZERO;
		protected Integer nRETID
		{
			get { return retID; }
			set { if(value != null) retID = value; }
		}

		protected Integer[] pessoaID = Constants.INT_EMPTY_SET;
		protected Integer[] nPessoaID
		{
			get { return pessoaID; }
			set { if(value != null) pessoaID = value; }
		}

		protected Integer[] turma = Constants.INT_EMPTY_SET;
		protected Integer[] nTurma
		{
			get { return turma; }
			set { if(value != null) turma = value; }
		}

		protected Integer[] retPartPresencaID = Constants.INT_EMPTY_SET;
		protected Integer[] nRETPartPresencaID
		{
			get { return retPartPresencaID; }
			set { if(value != null) retPartPresencaID = value; }
		}

		protected java.lang.Boolean[] presenca = Constants.BOOL_EMPTY_SET;
		protected java.lang.Boolean[] bPresenca
		{
			get { return presenca; }
			set { if(value != null) presenca = value; }
		}

		protected string[] observacao = Constants.STR_EMPTY_SET;
		protected string[] sObservacao
		{
			get { return observacao; }
			set { if(value != null) observacao = value; }
		}

		protected Integer[] retPessoaID = Constants.INT_EMPTY_SET;
		protected Integer[] nRETPessoaID
		{
			get { return retPessoaID; }
			set { if(value != null) retPessoaID = value; }
		}

		protected Integer[] nota = Constants.INT_EMPTY_SET;
		protected Integer[] nNota
		{
			get { return nota; }
			set { if(value != null) nota = value; }
		}

		protected string SQL
		{
			get
			{
				string sql = "";
				string notaSQL;

				// Incluir
				if(nTipoOperacao.intValue() == 1)
				{
					for(int i = 0; i < nPessoaID.Length; i++)
					{
						sql += "INSERT INTO RET_Participantes (RETID, PessoaID, Turma) " +
							"SELECT " + nRETID + ", " + nPessoaID[i] + ", " + nTurma[i] + " " +
                            "WHERE ((SELECT COUNT(*) FROM RET_Participantes WITH(NOLOCK) WHERE RETID=" + nRETID + " AND " +
							"PessoaID=" + nPessoaID[i] + ") = 0) ";
					}
				}
				// Presenca
				else if (nTipoOperacao.intValue() == 2)
				{
					for(int i = 0; i < bPresenca.Length; i++)
					{
						sql += "UPDATE RET_Participantes_Presenca SET Presenca=" + bPresenca[i] + ", " +
							"Observacao='" + sObservacao[i] + "' " +
							"WHERE RETPartPresencaID=" + nRETPartPresencaID[i] + " ";
					}
				}
				// Nota
				else if (nTipoOperacao.intValue() == 3)
				{
					for (int i = 0; i < nNota.Length; i++)
					{
						notaSQL = nNota[i].ToString();
						notaSQL.Replace(",", ".");
						notaSQL.Replace(" ", "");

						sql += "UPDATE RET_Participantes SET Nota=" + notaSQL + " " +
							"WHERE RETPessoaID=" + nRETPessoaID[i] + " ";
					}
				}

				return sql;
			}
		}
	}
}
