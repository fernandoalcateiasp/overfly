using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modrecursoshumanos.submodtreinamento.ret.serverside
{
	public partial class verificacaoret : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"SELECT dbo.fn_RET_Verifica( " +
					nRETID + ", " +
					nEstadoDeID + ", " +
					nEstadoParaID + 
				") AS Verificacao"
			));
		}

		protected Integer retID = Constants.INT_ZERO;
		protected Integer nRETID
		{
			get { return retID; }
			set { if(value != null) retID = value; }
		}

		protected Integer estadoDeID = Constants.INT_ZERO;
		protected Integer nEstadoDeID
		{
			get { return estadoDeID; }
			set { if(value != null) estadoDeID = value; }
		}

		protected Integer estadoParaID = Constants.INT_ZERO;
		protected Integer nEstadoParaID
		{
			get { return estadoParaID; }
			set { if(value != null) estadoParaID = value; }
		}
	}
}
