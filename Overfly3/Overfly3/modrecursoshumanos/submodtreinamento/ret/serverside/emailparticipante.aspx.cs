using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modrecursoshumanos.submodtreinamento.ret.serverside
{
	public partial class emailparticipante : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			EmailRET();

			WriteResultXML(DataInterfaceObj.getRemoteData(
				"select 'Email enviado com sucesso.' as fldResponse"
			));
		}

		protected Integer retID = Constants.INT_ZERO;
		protected Integer nRETID
		{
			get { return retID; }
			set { if(value != null) retID = value; }
		}

		protected Integer usuarioID = Constants.INT_ZERO;
		protected Integer nUsuarioID
		{
			get { return usuarioID; }
			set { if (value != null) usuarioID = value; }
		}

		protected void EmailRET()
		{
			ProcedureParameters[] param = new ProcedureParameters[2];

			param[0] = new ProcedureParameters("@RETID", SqlDbType.Int, retID.intValue());
			param[1] = new ProcedureParameters("@UsuarioID", SqlDbType.Int, usuarioID.intValue());

			DataInterfaceObj.execNonQueryProcedure("sp_Email_RET", param);
		}
	}
}
