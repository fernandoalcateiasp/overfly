﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_ret : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        private string mEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);
        private int formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string sdtInicio = Convert.ToString(HttpContext.Current.Request.Params["sdtInicio"]);
        private string sdtFim = Convert.ToString(HttpContext.Current.Request.Params["sdtFim"]);
        private string chkCadastrados = Convert.ToString(HttpContext.Current.Request.Params["chkCadastrados"]);
        private string chkPlanejamento = Convert.ToString(HttpContext.Current.Request.Params["chkPlanejamento"]);
        private string chkExecucao = Convert.ToString(HttpContext.Current.Request.Params["chkExecucao"]);
        private string chkVerificacao = Convert.ToString(HttpContext.Current.Request.Params["chkVerificacao"]);
        private string chkFechados = Convert.ToString(HttpContext.Current.Request.Params["chkFechados"]);
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "40177":
                        relatorioRAT();
                        break;
                }

            }
        }

        public void relatorioRAT()
        {
            // Excel
            int Formato = 2;
            string Title = "Relatório de Treinamentos";
            string sEstados = "";
            string sInformation = "";

            sInformation = "Período: " + sdtInicio + " até: " + sdtFim + " Estados: ";

            if (chkCadastrados == "1")
            {
                sEstados += (sEstados == "" ? "" : ",") + "1";
                sInformation += " C";
            }

            if (chkPlanejamento == "1")
            {
                sEstados += (sEstados == "" ? "" : ",") + "96";
                sInformation += " P";
            }

            if (chkExecucao == "1")
            {
                sEstados += (sEstados == "" ? "" : ",") + "97";
                sInformation += " E";
            }

            if (chkVerificacao == "1")
            {
                sEstados += (sEstados == "" ? "" : ",") + "98";
                sInformation += " V";
            }

            if (chkFechados == "1")
            {
                sEstados += (sEstados == "" ? "" : ",") + "48";
                sInformation += " F";
            }

            string strSQL = "SELECT " +
                                " a.RETID AS [ID], c.RecursoAbreviado AS [Estado], b.Competencia AS [Treinamento], a.dtData AS [Data], a.dtVencimento AS [Vencimento], a.Observacao AS[Observação], " +
                                " (CASE a.AvaliacaoTreinandos WHEN 1 THEN CHAR(88) ELSE SPACE(0) END) AS [Reação], (CASE a.AvaliacaoTreinamento WHEN 1 THEN CHAR(88) ELSE SPACE(0) END) AS [Evento], " +
                                " (CASE a.AvaliacaoEficacia WHEN 1 THEN CHAR(88) ELSE SPACE(0) END) AS [Eficácia], d.Fantasia AS [Fornecedor], f.SimboloMoeda AS [Moeda], a.Valor, " +
                                " CONVERT(NUMERIC(5, 1), dbo.fn_RET_CargaHoraria(a.RETID)) AS [CH], a.FrequenciaMinima AS [FM], a.NotaMinima AS [NM], a.Aulas, a.Turmas, " +
                                " (SELECT COUNT(*) FROM RET_Participantes WITH(NOLOCK) WHERE RETID = a.RETID) AS [Participantes], e.Fantasia AS [Proprietário], 1 AS [_Registro] " +
                            " FROM " +
                                " RET a WITH(NOLOCK) " +
                                    " LEFT OUTER JOIN Pessoas d WITH(NOLOCK) ON (a.FornecedorID = d.PessoaID) " +
                                    " LEFT OUTER JOIN Pessoas e WITH(NOLOCK) ON (a.ProprietarioID = e.PessoaID) " +
                                    " LEFT OUTER JOIN Conceitos f WITH(NOLOCK) ON (a.MoedaID = f.ConceitoID) " +
                                    " INNER JOIN Competencias b WITH(NOLOCK) ON(a.TreinamentoID = b.CompetenciaID) " +
                                    " INNER JOIN Recursos c WITH(NOLOCK) ON(a.EstadoID = c.RecursoID) " +
                            " WHERE " +
                                " a.EmpresaID = " + nEmpresaID +
                            " AND dbo.fn_Data_Zero(dtData) >= " + sdtInicio +
                            " AND dbo.fn_Data_Zero(dtData) <= " + sdtFim +
                            " AND a.EstadoID IN (" + sEstados + ") " +
                            " ORDER BY a.dtData";

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "7.13834", "0.0", "11", "#0113a0", "B", Header_Body, "10.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "20.0", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");
                Relatorio.CriarObjLabel("Fixo", "", sInformation, "0.001", "0.4", "11", "#0113a0", "B", Header_Body, "10.51332", "");
                
                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.6", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }
            Relatorio.CriarPDF_Excel(Title, Formato);
        }
    }
}
