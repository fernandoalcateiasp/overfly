/********************************************************************
pesqlist.js

Library javascript para o pesquisa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
//  Dados da listagem da pesquisa 
var dsoListData01 = new CDatatransport("dsoListData01");
//  Dados dos combos de contexto e filtros 
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");
//  Dados dos proprietarios para o pesqlist .URL 
var dsoPropsPL = new CDatatransport("dsoPropsPL");
var dsoEMailParticipante = new CDatatransport("dsoEMailParticipante");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
	window_onload()
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Evento disparado ao final do carregamento da pagina.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Treinamento', 'Data', 'Vencimento', 'Observa��o', 'Rea��o', 'Evento', 'Efic�cia',
		'Fornecedor', 'Moeda', 'Valor', 'CH', 'FM', 'NM', 'Aulas', 'Turmas', 'Partic', 'Propriet�rio');

    var dTFormat = '';

    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    glb_aCOLPESQFORMAT = new Array('', '', '', '', dTFormat, '', '', '', '', '', '', '###,###,##0.00', '#,##0.00', '###', '#0.0', '##', '#', '###', '');

    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_pesqlist.js

O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
	var nCurrRETID = 0;
	
	showBtnsEspecControlBar('sup', true, [1,1,1,1,1]);
	tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Texto', 'E-mail de convoca��o dos participantes', '']);

	if (fg.Row < 1)
		setupEspecBtnsControlBar('sup', 'HHHDD');
	else
		setupEspecBtnsControlBar('sup', 'HHHHD');

    alignColsInGrid(fg,[0,11,12,13,14,15,16,17]);
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_pesqlist.js

Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	// Usuario clicou botao documentos
	if (btnClicked == 1) {
		if (fg.Rows > 1) {
			__openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
		}
		else {
		    window.top.overflyGen.Alert('Selecione um registro.');
		}
	}
	if (btnClicked == 2)
	{
		openmodalprint();
	}
	else if (btnClicked == 3)
	{
		window.top.openModalControleDocumento('PL', '', 620, null, null, 'T');
	}
	else if (btnClicked == 4)
	{
		nCurrRETID = getCellValueByColKey(fg, 'RETID', fg.Row);
		window.top.openModalHTML(nCurrRETID, 'Texto', 'S', 7);
	}
	else if (btnClicked == 5)
	{
		enviaEMailParticipante();
	}
}

function enviaEMailParticipante()
{
	if (fg.Row <= 0)
		return null;
	
    lockInterface(true);
    var strPars = new String();

    strPars = '?nRETID=' + escape(getCellValueByColKey(fg, 'RETID', fg.Row));
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    dsoEMailParticipante.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodtreinamento/ret/serverside/emailparticipante.aspx' + strPars;
    dsoEMailParticipante.ondatasetcomplete = enviaEMailParticipante_DSC;
    dsoEMailParticipante.refresh();
}

function enviaEMailParticipante_DSC()
{
    lockInterface(false);
    
    if ( !(dsoEMailParticipante.BOF && dsoEMailParticipante.EOF) )
    {
        dsoEMailParticipante.recordset.MoveFirst();
        if ( window.top.overflyGen.Alert ( dsoEMailParticipante.recordset['fldResponse'].value ) == 0 )
			return null;
    }
}

function openmodalprint()
{
    // por questoes esteticas, coloca foco no grid de pesquisa
    if ( fg.disabled == false )
        fg.focus();
    
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modrecursoshumanos/submodtreinamento/ret/modalpages/modalprint.asp'+strPars;
    
    showModalWin(htmlPath, new Array(370,200));
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em dois pontos do js_pesqlist.js

Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALPRINTHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
    	if (param1 == 'OK') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    	else if (param1 == 'CANCEL') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao goExecPesqShowList(formID) do js_pesqlist.js

Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{
    //@@ da automacao -> retorno padrao
    return null;
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************