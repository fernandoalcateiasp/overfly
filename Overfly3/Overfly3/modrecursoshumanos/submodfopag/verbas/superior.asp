<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="verbassup01Html" name="verbassup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modqualidade/commonqualidade/commonqualidade.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modrecursoshumanos/submodfopag/verbas/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modrecursoshumanos/submodfopag/verbas/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="verbassup01Body" name="verbassup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p id="lblVerba" name="lblVerba" class="lblGeneral">Verba</p>
        <input type="text" id="txtVerba" name="txtVerba" DATASRC="#dsoSup01" DATAFLD="Verba" class="fldGeneral"></input>
        <p  id="lblEhProvento" name="lblEhProvento" class="lblGeneral">Prov</p>
        <input type="checkbox" id="chkEhProvento" name="chkEhProvento" DATASRC="#dsoSup01" DATAFLD="EhProvento" class="fldGeneral" title="� verba de provento ou desconto?"></input>
        <p  id="lblEhAutomatica" name="lblEhAutomatica" class="lblGeneral">Auto</p>
        <input type="checkbox" id="chkEhAutomatica" name="chkEhAutomatica" DATASRC="#dsoSup01" DATAFLD="EhAutomatica" class="fldGeneral" title="� verba autom�tica ou de ocorr�ncia?"></input>
        <p  id="lblPermiteValorExterno" name="lblPermiteValorExterno" class="lblGeneral">PVE</p>
        <input type="checkbox" id="chkPermiteValorExterno" name="chkPermiteValorExterno" DATASRC="#dsoSup01" DATAFLD="PermiteValorExterno" class="fldGeneral" title="Verba permite valor externo?"></input>
        <p  id="lblEhBeneficio" name="lblEhBeneficio" class="lblGeneral">Benef�cio</p>
        <input type="checkbox" id="chkEhBeneficio" name="chkEhBeneficio" DATASRC="#dsoSup01" DATAFLD="EhBeneficio" class="fldGeneral" title="� verba de benef�cio?"></input>
        <p  id="lblEhParametro" name="lblEhParametro" class="lblGeneral">Par�metro</p>
        <input type="checkbox" id="chkEhParametro" name="chkEhParametro" DATASRC="#dsoSup01" DATAFLD="EhParametro" class="fldGeneral" title="� verba de par�metro?"></input>
        <p id="lblContaID" name="lblContaID" class="lblGeneral">ContaID</p>
		<select id="selContaID" name="selContaID" DATASRC="#dsoSup01" DATAFLD="ContaID" class="fldGeneral"></select>
        <p  id="lblEhDebito" name="lblEhDebito" class="lblGeneral">D�b</p>
        <input type="checkbox" id="chkEhDebito" name="chkEhDebito" DATASRC="#dsoSup01" DATAFLD="EhDebito" class="fldGeneral" title="� d�bito?"></input>
        <p id="lblImpostoID" name="lblImpostoID" class="lblGeneral">Imposto</p>
		<select id="selImpostoID" name="selImpostoID" DATASRC="#dsoSup01" DATAFLD="ImpostoID" class="fldGeneral"></select>
        <p id="lblMetodoDataReferenciaID" name="lblMetodoDataReferenciaID" class="lblGeneral">M�todo Data Rerer�ncia</p>
		<select id="selMetodoDataReferenciaID" name="selMetodoDataReferenciaID" DATASRC="#dsoSup01" DATAFLD="MetodoDataReferenciaID" class="fldGeneral" title=""></select>
        <p id="lblMetodoReferenciaID" name="lblMetodoReferenciaID" class="lblGeneral">M�todo Refer�ncia</p>
		<select id="selMetodoReferenciaID" name="selMetodoReferenciaID" DATASRC="#dsoSup01" DATAFLD="MetodoReferenciaID" class="fldGeneral" title=""></select>
        <p id="lblMetodoValorID" name="lblMetodoValorID" class="lblGeneral">M�todo Valor</p>
		<select id="selMetodoValorID" name="selMetodoValorID" DATASRC="#dsoSup01" DATAFLD="MetodoValorID" class="fldGeneral" title=""></select>
        <p id="lblTrataDataFopagInicio" name="lblTrataDataFopagInicio" class="lblGeneral">TDFI</p>
		<select id="selTrataDataFopagInicio" name="selTrataDataFopagInicio" DATASRC="#dsoSup01" DATAFLD="TrataDataFopagInicio" class="fldGeneral" title="M�todo para tratamento do campo Data Fopag In�cio">
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>
        <p id="lblTrataDataFopagFim" name="lblTrataDataFopagFim" class="lblGeneral">TDFF</p>
		<select id="selTrataDataFopagFim" name="selTrataDataFopagFim" DATASRC="#dsoSup01" DATAFLD="TrataDataFopagFim" class="fldGeneral" title="M�todo para tratamento do campo Data Fopag Fim">
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>
        <p id="lblTrataDataReferencia" name="lblTrataDataReferencia" class="lblGeneral">TDR</p>
		<select id="selTrataDataReferencia" name="selTrataDataReferencia" DATASRC="#dsoSup01" DATAFLD="TrataDataReferencia" class="fldGeneral" title="M�todo para tratamento do campo Data Refer�ncia">
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>
        <p id="lblTrataReferencia" name="lblTrataReferencia" class="lblGeneral">TR</p>
		<select id="selTrataReferencia" name="selTrataReferencia" DATASRC="#dsoSup01" DATAFLD="TrataReferencia" class="fldGeneral" title="M�todo para tratamento do campo Refer�ncia">
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>
        <p id="lblTrataValor" name="lblTrataValor" class="lblGeneral">TV</p>
		<select id="selTrataValor" name="selTrataValor" DATASRC="#dsoSup01" DATAFLD="TrataValor" class="fldGeneral" title="M�todo para tratamento do campo Valor">
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>
		
		<p id="lblEnviaVerbaPrestador" name="lblEnviaVerbaPrestador" class="lblGeneral" title="Envia esta verba na remessa para prestador?">EV</p>
		<input type="checkbox" id="chkEnviaVerbaPrestador" name="chkEnviaVerbaPrestador" DATASRC="#dsoSup01" DATAFLD="PrestadorEnviaVerba" class="fldGeneral" title="Envia esta verba na remessa para prestador?" />
		<p id="lblIncluiVerbaPrestador" name="lblIncluiVerbaPrestador" class="lblGeneral" title="Inclui na Fopag. se esta verba estiver presente no retorno do prestador?">IV</p>
		<input type="checkbox" id="chkIncluiVerbaPrestador" name="chkIncluiVerbaPrestador" DATASRC="#dsoSup01" DATAFLD="PrestadorIncluiVerba" class="fldGeneral" title="Inclui na Fopag, se esta verba estiver presente no retorno do prestador?" />
		<p id="lblExcluiVerbaPrestador" name="lblExcluiVerbaPrestador" class="lblGeneral" title="Exclui da Fopag. se esta verba estiver presente no retorno do prestador?">ExV</p>
		<input type="checkbox" id="chkExcluiVerbaPrestador" name="chkExcluiVerbaPrestador" DATASRC="#dsoSup01" DATAFLD="PrestadorExcluiVerba" class="fldGeneral" title="Exclui da Fopag, se esta verba estiver presente no retorno do prestador?" />
		<p id="lblAlteraVerbaPrestador" name="lblAlteraVerbaPrestador" class="lblGeneral" title="Altera a Fopag. se esta verba tiver valor diferente at� este percentual no retorno do prestador?">AV</p>
		<input type="text" id="txtAlteraVerbaPrestador" name="txtAlteraVerbaPrestador" DATASRC="#dsoSup01" DATAFLD="PrestadorAlteraVerba" class="fldGeneral" title="Altera a Fopag, se esta verba tiver valor diferente at� este percentual no retorno do prestador?" />
    </div>
</body>

</html>
