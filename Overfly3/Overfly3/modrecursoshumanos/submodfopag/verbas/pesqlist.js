/********************************************************************
pesqlist.js

Library javascript para o pesquisa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
//  Dados da listagem da pesquisa 
var dsoListData01 = new CDatatransport("dsoListData01");
//  Dados dos combos de contexto e filtros 
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");
//  Dados dos proprietarios para o pesqlist .URL 
var dsoPropsPL = new CDatatransport("dsoPropsPL");
var dsoEMailParticipante = new CDatatransport("dsoEMailParticipante");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
	window_onload()
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Evento disparado ao final do carregamento da pagina.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Verba', 'Prov', 'Auto', 'PVE', 'Benef�cio', 'Par�metro', 'MDR', 'MR','MV','TDFI','TDFF','TDR','TR',
		'TV','Observa��o');

    var dTFormat = '';

    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', '', '', '', '', '','','','','','','');
    windowOnLoad_2ndPart();
}


/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_pesqlist.js

O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
	var nCurrRETID = 0;
	
	// especBtnIsPrintBtn('sup', 1);
	showBtnsEspecControlBar('sup', true, [0,0,0,0]);
	tipsBtnsEspecControlBar('sup', ['', '', '', '']);
	setupEspecBtnsControlBar('sup', 'DDDD');

    alignColsInGrid(fg,[0]);
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_pesqlist.js

Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	if (btnClicked == 1)
	{
		openmodalprint();
	}
}

function openmodalprint()
{
	return null;
    // por questoes esteticas, coloca foco no grid de pesquisa
    if ( fg.disabled == false )
        fg.focus();
    
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modrecursoshumanos/submodtreinamento/ret/modalpages/modalprint.asp'+strPars;
    
    showModalWin(htmlPath, new Array(370,200));
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em dois pontos do js_pesqlist.js

Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
	if (idElement.toUpperCase() == 'MODALPRINTHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao goExecPesqShowList(formID) do js_pesqlist.js

Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{
    //@@ da automacao -> retorno padrao
    return null;
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************