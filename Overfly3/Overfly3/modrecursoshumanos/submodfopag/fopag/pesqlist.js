/********************************************************************
pesqlist.js

Library javascript para o pesquisa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_empresaData = getCurrEmpresaData();
var glb_empresaID = glb_empresaData[0];
var dsoListData01 = new CDatatransport('dsoListData01');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoPropsPL = new CDatatransport('dsoPropsPL');
var dsoEMailParticipante = new CDatatransport('dsoEMailParticipante');
var dsoUltimaFolha = new CDatatransport('dsoUltimaFolha');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
	window_onload()
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Evento disparado ao final do carregamento da pagina.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Tipo Fopag', 'PL', 'VN', 'VV', 'PS', 'SSF', 'AS', 'GFD', 'DAAS','DPAS','DUPF','DDV','DRV','Data Fopag',
		'Fopag in�cio', 'Fopag fim', 'Fopag pgto', 'Total Fopag', 'Observa��o', 'dtApuracaoInicio', 'dtApuracaoFim', 'EstadoID', 'dtUltimoFechamento', 'TipoFopagID');

    var dTFormat = '';

    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', '', '', '', '', '','','','','',dTFormat, dTFormat, dTFormat, dTFormat, '###,###,##0.00','', dTFormat, dTFormat, '', dTFormat, '');

    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_pesqlist.js

O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
	var nCurrRETID = 0;
	
	showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1]);
	tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Fopag', 'Resumo da Fopag', 'Ponto Eletr�nico']);
	setupEspecBtnsControlBar('sup', 'HHHHDH');
	
	fg.ColHidden(fg.Cols-1) = true;		
    fg.ColHidden(fg.Cols-2) = true;
    fg.ColHidden(fg.Cols-3) = true;
    fg.ColHidden(fg.Cols-4) = true;
    fg.ColHidden(fg.Cols-5) = true;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_pesqlist.js

Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	// Usuario clicou botao documentos
	if (btnClicked == 1) {
		if (fg.Rows > 1) {
			__openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
		}
		else {
		    window.top.overflyGen.Alert('Selecione um registro.');
		}
	}
	if (btnClicked == 2)
	{
		openmodalprint();
	}
	else if (btnClicked == 3)
	{
		window.top.openModalControleDocumento('PL', '', 910, null, '2113', 'T');
	}
	else if (btnClicked == 4) {
	    openModalOcorrenciasVerbas();
	}
	else if (btnClicked == 5)
	{
		if (fg.Row > 0)
		{
			var nCurrFopagID = getCellValueByColKey(fg, 'FopagID', fg.Row);
			window.top.openModalHTML(nCurrFopagID, 'Fopag ' + nCurrFopagID + ' - Resumo', 'PL', 9, 1);
        }
    }
    //rodrigo
    else if (btnClicked == 6) 
    {
        if (glb_empresaID == 2 || 10)
            parametrosApontamento();
        else
            window.top.overflyGen.Alert('Liberado somente na Alcateia e abano.');
    }
}

function openmodalprint()
{
	return null;
    // por questoes esteticas, coloca foco no grid de pesquisa
    if ( fg.disabled == false )
        fg.focus();
    
    var htmlPath;
    var strPars = new String();
    //var empresaData = getCurrEmpresaData();
    //var empresaID = empresaData[0];
    var empresaFantasia = glb_empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(glb_empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modrecursoshumanos/submodtreinamento/ret/modalpages/modalprint.asp'+strPars;
    
    showModalWin(htmlPath, new Array(370,200));
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em dois pontos do js_pesqlist.js

Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALPRINTHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALOPENHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
    	if (param1 == 'OK') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    	else if (param1 == 'CANCEL') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao goExecPesqShowList(formID) do js_pesqlist.js

Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{
    //@@ da automacao -> retorno padrao
    return null;
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************

/********************************************************************
Criado pelo programador Rodrigo Franklin
Abre a JM de Gerar Ocorrencias

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function parametrosApontamento() {
    var nCurrFopagID;
    var dDtApuracaoInicio;
    var dDtApuracaoFim;
    var dDtUltimoFechamento;
    var nUserID;
    var nEstadoID;

    if (fg.Row > 0) 
    {
        if (getCellValueByColKey(fg, 'TipoFopagID', fg.Row) != '1602')
            window.top.overflyGen.Alert('Selecione um folha normal!');
        else 
        {
            nCurrFopagID = getCellValueByColKey(fg, 'FopagID', fg.Row);
            dDtApuracaoInicio = getCellValueByColKey(fg, 'dtApuracaoInicio', fg.Row);
            dDtApuracaoFim = getCellValueByColKey(fg, 'dtApuracaoFim', fg.Row);
            nUserID = getCurrUserID();
            nEstadoID = getCellValueByColKey(fg, 'EstadoID', fg.Row);
            dDtUltimoFechamento = getCellValueByColKey(fg, 'dtUltimoFechamento', fg.Row);

            openModalApontamento(glb_empresaID, nCurrFopagID, dDtApuracaoInicio, dDtApuracaoFim, nUserID, nEstadoID, dDtUltimoFechamento);
        }
    }
    else 
    {
        setConnection(dsoUltimaFolha);

        dsoUltimaFolha.SQL = 'SELECT TOP 1 FopagID, CONVERT(VARCHAR(10), dbo.fn_Fopag_Data(FopagID, 4),' + DATE_SQL_PARAM + ') AS dtApuracaoInicio, '+
                             ' CONVERT(VARCHAR(10),dbo.fn_Fopag_Data(FopagID, 5),' + DATE_SQL_PARAM + ') AS dtApuracaoFim,  ' +
                             ' CONVERT(VARCHAR(10),dtUltimoFechamento,' + DATE_SQL_PARAM + ') AS dtUltimoFechamento, ' +
                             ' EstadoID as EstadoID' +
                             ' FROM Fopag WITH(NOLOCK) ' +
                             ' WHERE EstadoID IN (41,1) AND TipoFopagID = 1602 AND EmpresaID = ' + glb_empresaID + 
                             ' ORDER BY FopagID DESC' ;

        dsoUltimaFolha.ondatasetcomplete = parametrosApontamento_DSC;

        try 
        {
            dsoUltimaFolha.Refresh();
        }
        catch (e) {
            if (window.top.overflyGen.Alert('Erro ao ler apontamento') == 0)
                return null;
        }
    }
}

function parametrosApontamento_DSC()
{
    var nCurrFopagID = dsoUltimaFolha.recordset['FopagID'].value;
    var dDtApuracaoInicio = dsoUltimaFolha.recordset['dtApuracaoInicio'].value;
    var dDtApuracaoFim = dsoUltimaFolha.recordset['dtApuracaoFim'].value;
    var dDtUltimoFechamento = dsoUltimaFolha.recordset['dtUltimoFechamento'].value;
    var nEstadoID = dsoUltimaFolha.recordset['EstadoID'].value;
    var nUserID = getCurrUserID();

    openModalApontamento(glb_empresaID, nCurrFopagID, dDtApuracaoInicio, dDtApuracaoFim, nUserID, nEstadoID, dDtUltimoFechamento);

        
}
function openModalApontamento(empresaID, nCurrFopagID, dDtApuracaoInicio, dDtApuracaoFim, nUserID, nEstadoID, dDtUltimoFechamento)
{
    var htmlPath;
    var strPars = new String();
    var dirC1;
    var dirC2;
    var dirA1;
    var dirA2;

    dirC1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 
                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B6C1' + '\'' + ')'));

    dirC2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 
                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B6C2' + '\'' + ')'));

    dirA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 
                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B6A1' + '\'' + ')'));

    dirA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 
                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B6A2' + '\'' + ')'));

    strPars = '?sCaller=' + escape('S');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&nFopagID=' + escape(nCurrFopagID);
    strPars += '&sDataInicio=' + escape(dDtApuracaoInicio);
    strPars += '&sDataFim=' + escape(dDtApuracaoFim);
    strPars += '&sDataUltimoFechamento=' + escape(dDtUltimoFechamento);
    strPars += '&nEstadoID=' + escape(nEstadoID);
    strPars += '&nUserID=' + escape(nUserID);
    strPars += '&ndirC1=' + escape(dirC1);
    strPars += '&ndirC2=' + escape(dirC2);
    strPars += '&ndirA1=' + escape(dirA1);
    strPars += '&ndirA2=' + escape(dirA2);


    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modrecursoshumanos/submodfopag/fopag/modalpages/modalapontamento.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 590));

}
/********************************************************************
Criado pelo programador
Abre a JM de Gerar Ocorrencias

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalOcorrenciasVerbas() {
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];

    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('S');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&nFopagID=' + escape(getCellValueByColKey(fg, 'FopagID', fg.Row));

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modrecursoshumanos/submodfopag/fopag/modalpages/modalocorrenciasverbas.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 550));
}
