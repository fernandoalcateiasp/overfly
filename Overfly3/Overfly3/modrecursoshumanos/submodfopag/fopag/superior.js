/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_BtnFromFramWork = null;
var glb_CounterCmbsDynamics = 0;
var glb_bAlteracao = false;
var glb_backToAutomation = false;
var glb_aniversariantesTimer = null;
var glb_empresaData = getCurrEmpresaData();
var glb_empresaID = glb_empresaData[0];
var dsoSup01 = new CDatatransport('dsoSup01');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoCmbDynamic02 = new CDatatransport('dsoCmbDynamic02');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoVerificacao = new CDatatransport('dsoVerificacao');
var dsoEMailParticipante = new CDatatransport('dsoEMailParticipante');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
	window_onload()
	setupPage()
    putSpecialAttributesInControls()
    formFinishLoad()
    prgServerSup(btnClicked)
    finalOfSupCascade(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    
********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Evento disparado ao final do carregamento da pagina.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function window_onload() {
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);

    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    glb_aStaticCombos = ([['selTipoRegistroID', '1']]);

    windowOnLoad_1stPart();

    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modrecursoshumanos/submodfopag/fopag/inferior.asp',
                              SYS_PAGESURLROOT + '/modrecursoshumanos/submodfopag/fopag/pesquisa.asp');

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    //linkDivsAndSubForms('divSup01_01', null, null);
    //linkDivsAndSubForms('divSup01_01', ['divSup02_01', 'divSup02_02', 'divSup02_03'], [1602, 1605, 1606]);

    //Alterado para mudar forma de mostrar a interface da fopag. Solcitado pelo Eduardo. BJBN 27/01/2012
    linkDivsAndSubForms('divSup01_01', null, null);

    windowOnLoad_2ndPart();

    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'FopagID';

    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoRegistroID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoFopagID';
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao windowOnLoad_2ndPart() do js_detailsup.js

Usar para ajustes graficos da interface.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setupPage() {
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup', [[1, 'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    /*    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
            ['lblEstadoID','txtEstadoID',2,1],
            ['lblTipoRegistroID','selTipoRegistroID',16,1],
            ['lbldtFopag','txtdtFopag',10,1],
            ['lbldtFopagInicio','txtdtFopagInicio',10,1],
            ['lbldtFopagFim','txtdtFopagFim',10,1]], null, null, true);*/

    /*if ((selTipoRegistroID.value == 1602) || (selTipoRegistroID.value == ""))
    {*/
    adjustElementsInForm([
        ['lblRegistroID', 'txtRegistroID', 10, 1],
		['lblEstadoID', 'txtEstadoID', 2, 1],
		['lblTipoRegistroID', 'selTipoRegistroID', 16, 1],
		['lbldtFopag', 'txtdtFopag', 10, 1],
		['lbldtFopagInicio', 'txtdtFopagInicio', 10, 1],
		['lbldtFopagFim', 'txtdtFopagFim', 10, 1],
		['lblProcessamentoLiberado', 'chkProcessamentoLiberado', 3, 2],
		['lblIncluiVerbasNormais', 'chkIncluiVerbasNormais', 3, 2],
		['lblIncluiVerbasVendas', 'chkIncluiVerbasVendas', 3, 2],
		['lblSubtraiSalarioFixo', 'chkSubtraiSalarioFixo', 3, 2],
		['lblArredondaSalarios', 'chkArredondaSalarios', 3, 2],
		['lblGeraFinanceirosDistintos', 'chkGeraFinanceirosDistintos', 3, 2],
		['lblLimiteComissaoVendas', 'txtLimiteComissaoVendas', 5, 2],
		['lblPercentualPremioAdministrativo', 'txtPercentualPremioAdministrativo', 5, 2],
		['lblDiaAntecipacaoAviso', 'txtDiaAntecipacaoAviso', 3, 2],
		['lblDiaAntecipacaoPagamento', 'txtDiaAntecipacaoPagamento', 3, 2],
		['lblDiasUteisPagamento', 'txtDiasUteisPagamento', 4, 2],
		['lblDiasDesconsiderarVendas', 'txtDiasDesconsiderarVendas', 4, 2],
		['lblDiasRecuarVendas', 'txtDiasRecuarVendas', 4, 2],
		['lblDiaAvisoPontoEletronico', 'txtDiaAvisoPontoEletronico', 4, 2],
		['lbldtAntecipacaoAviso', 'txtdtAntecipacaoAviso', 10, 3, 0, -3],
		['lbldtAntecipacaoPagamento', 'txtdtAntecipacaoPagamento', 10, 3],
		['lbldtPagamento', 'txtdtPagamento', 10, 3, -9],
		['lbldtVendasInicio', 'txtdtVendasInicio', 10, 4, 0, -3],
		['lbldtVendasFim', 'txtdtVendasFim', 10, 4],
		['lbldtInadimplenciaInicio', 'txtdtInadimplenciaInicio', 10, 4],
		['lbldtInadimplenciaFim', 'txtdtInadimplenciaFim', 10, 4, -10],
		['lbldtObsolescencia', 'txtdtObsolescencia', 10, 4],
		['lblPercentual', 'txtPercentual', 10, 4],
		['lblTotalFopag', 'txtTotalFopag', 11, 5],
		['lblObservacao', 'txtObservacao', 30, 5]], null, null, true);
    /*}
    else 
    {
        adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
		['lblEstadoID', 'txtEstadoID', 2, 1],
		['lblTipoRegistroID', 'selTipoRegistroID', 16, 1],
		['lbldtFopag', 'txtdtFopag', 10, 1],
		['lbldtFopagInicio', 'txtdtFopagInicio', 10, 1],
		['lbldtFopagFim', 'txtdtFopagFim', 10, 1],
		['lblProcessamentoLiberado', 'chkProcessamentoLiberado', 3, 2],
		['lblIncluiVerbasNormais', 'chkIncluiVerbasNormais', 3, 2],
		['lblArredondaSalarios', 'chkArredondaSalarios', 3, 2],
		['lblGeraFinanceirosDistintos', 'chkGeraFinanceirosDistintos', 3, 2],
		['lblDiaAntecipacaoAviso', 'txtDiaAntecipacaoAviso', 3, 2],
		['lblDiaAntecipacaoPagamento', 'txtDiaAntecipacaoPagamento', 3, 2],
		['lblDiasUteisPagamento', 'txtDiasUteisPagamento', 4, 2],
		//Esses campos n�o apareceram na interface nesse caso - Inicio. BJBN
		['lblDiasDesconsiderarVendas', 'txtDiasDesconsiderarVendas', 4, 2],
		['lblDiasRecuarVendas', 'txtDiasRecuarVendas', 4, 2],
        ['lblIncluiVerbasVendas', 'chkIncluiVerbasVendas', 3, 2],
		['lblSubtraiSalarioFixo', 'chkSubtraiSalarioFixo', 3, 2],
        ['lblLimiteComissaoVendas', 'txtLimiteComissaoVendas', 5, 2],
		['lblPercentualPremioAdministrativo', 'txtPercentualPremioAdministrativo', 5, 2],
        //Esses campos n�o apareceram na interface nesse caso - Fim. BJBN
		['lbldtAntecipacaoAviso', 'txtdtAntecipacaoAviso', 10, 3, 0, -3],
		['lbldtAntecipacaoPagamento', 'txtdtAntecipacaoPagamento', 10, 3],
		['lbldtPagamento', 'txtdtPagamento', 10, 3, -9],
		['lblPercentualPLR', 'txtPercentualPLR', 10, 4],
        //Esses campos n�o apareceram na interface nesse caso - Inicio. BJBN
		['lbldtVendasInicio', 'txtdtVendasInicio', 10, 4, 0, -3],
		['lbldtVendasFim', 'txtdtVendasFim', 10, 4],
		['lbldtInadimplenciaInicio', 'txtdtInadimplenciaInicio', 10, 4],
		['lbldtInadimplenciaFim', 'txtdtInadimplenciaFim', 10, 4, -10],
		['lbldtObsolescencia', 'txtdtObsolescencia', 10, 4],
        //Esses campos n�o apareceram na interface nesse caso - Fim. BJBN
		['lblTotalFopag', 'txtTotalFopag', 11, 5],
		['lblObservacao', 'txtObservacao', 30, 5]], null, null, true);
    }*/
}

/********************************************************************
Funcao disparada pelo frame work.
Chamado pela funcao windowOnLoad_2ndPart() do js_superior.js

Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@

    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad() {
    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 0]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Fopag', 'Resumo da Fopag', 'Ponto Eletr�nico']);

}

/********************************************************************
Funcao disparada pelo frame work.
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked) {
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;
    finalOfSupCascade(glb_BtnFromFramWork);
    adjustLabelsCombos(true);
}

/********************************************************************
Funcao disparada ao final da funcao prgServerInf(btnClicked) que e
disparada pelo frame work,

Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked) {
    //@@

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 0]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Fopag', 'Resumo da Fopag', 'Ponto Eletr�nico']);


    if (btnClicked == 'SUPINCL') {
        // funcao que controla campos read-only
        controlReadOnlyFields();
    }

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================

    ajustaCampos(true);
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao selChanged() do js_interfaceex.js

Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selTipoRegistroID') {
        adjustSupInterface();
        ajustaCampos(false);
    }

    adjustLabelsCombos(false);

    // Final de Nao mexer - Inicio de automacao =====================
}

function adjustLabelsCombos(bPaging) {
    if (bPaging)
        setLabelOfControl(lblTipoRegistroID, dsoSup01.recordset['TipoFopagID'].value);
    else
        setLabelOfControl(lblTipoRegistroID, selTipoRegistroID.value);
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo evento onClickBtnLupa() do js_detailsup.js

Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    var nEstadoID = dsoSup01.recordset['EstadoID'].value;
    var nTipoFopagID = dsoSup01.recordset['TipoFopagID'].value;

    // Documentos
    if ((controlBar == 'SUP') && (btnClicked == 1)) {
        __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
    }

    if ((controlBar == 'SUP') && (btnClicked == 2)) {
        if ((glb_empresaID != 7) && (nTipoFopagID == 1602))
            openModalPrint();
        else if (glb_empresaID == 7)
            window.top.overflyGen.Alert('Sem relatorios para esta empresa.');
        else
            window.top.overflyGen.Alert('Relatorios somente para folhas normais.');
    }

    if (btnClicked == 3) {
        window.top.openModalControleDocumento('S', '', 910, null, '2113', 'T');
    }
    else if (btnClicked == 4) {
        openModalOcorrenciasVerbas();
    }
    else if (btnClicked == 5) {
        var nCurrFopagID = dsoSup01.recordset['FopagID'].value;
        var sConfirm = window.top.overflyGen.Confirm('Deseja abrir o resumo da Fopag ' + nCurrFopagID + '? \nEste processo pode demorar um pouco.');

        if ((sConfirm == 0) || (sConfirm == 2))
            return null;

        window.top.openModalHTML(nCurrFopagID, 'Fopag ' + nCurrFopagID + ' - Resumo', 'PL', 9, 1);
    }

}

/********************************************************************
Criado pelo programador
Abre a JM de Gerar Ocorrencias

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalOcorrenciasVerbas() {
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];

    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('S');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&nFopagID=' + escape(dsoSup01.recordset['FopagID'].value);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modrecursoshumanos/submodfopag/fopag/modalpages/modalocorrenciasverbas.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 550));
}


/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    var aEmpresaData = getCurrEmpresaData();

    if (btnClicked == 'SUPOK') {
        __glb_applyCheckBoxDefaultValue = false;
        //if ((typeof (dsoSup01.recordset[glb_sFldIDName].value)).toUpperCase() == 'UNDEFINED')
        if (dsoSup01.recordset[glb_sFldIDName].value == null) {
            // Grava o campo empresaID
            dsoSup01.recordset['EmpresaID'].value = aEmpresaData[0];
        }
    }

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALOPENHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALOCORRENCIASVERBASHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALOPENHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
        // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
        // Modal apontamento
    else if (idElement.toUpperCase() == 'MODALAPONTAMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de impressao
    if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao editRegister() do js_superior.js

Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode() {
    // funcao que controla campos read-only
    controlReadOnlyFields();

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************

// FUNCOES DO CARRIER ***********************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2) {
    if (param1 == 'SHOWRET') {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if (__currFormState() != 0)
            return null;

        // param2 - traz array:
        // o id da empresa do form que mandou
        // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();

        if (param2[0] != empresa[0])
            return null;

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);

        window.top.focus();

        return new Array(getHtmlId(), null, null);
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2) {
    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE FUNCOES DO CARRIER **************************************

// FUNCOES DA MAQUINA DE ESTADO *************************************

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK(currEstadoID, newEstadoID) {
    // Se estiver indo para processado
    if (newEstadoID == 66) {
        verifyFopagInServer(currEstadoID, newEstadoID);
        return true;
    }
    else
        return false;
}

/********************************************************************
Verificacoes da Relacao
********************************************************************/
function verifyFopagInServer(currEstadoID, newEstadoID) {
    var nFopagID = dsoSup01.recordset['FopagID'].value;
    var strPars = '?FopagID=' + escape(nFopagID) +
		'&ReprocessaVerbasVendas=' + escape(-1) +
		'&MantemVerbasAlteradas=' + escape(-1) +
		'&ColaboradorID=' + escape(-1) +
		'&nTipoProcessamentoID=' + escape(2);

    lockInterface(false);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/processarfopag.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyFopagInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes da Relacao
********************************************************************/
function verifyFopagInServer_DSC() {
    if ((dsoVerificacao.recordset.Fields['Resultado'].value == null) ||
		(trimStr(dsoVerificacao.recordset.Fields['Resultado'].value) == '')) {
        stateMachSupExec('OK');
    }
    else {
        if (window.top.overflyGen.Alert(dsoVerificacao.recordset.Fields['Resultado'].value) == 0)
            return null;
        stateMachSupExec('CANC');
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID, bSavedID) {

}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Read Only

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields() {
    var nEstadoID = dsoSup01.recordset['EstadoID'].value;

    chkProcessamentoLiberado.disabled = true;
    txtdtAntecipacaoAviso.readOnly = true;
    txtdtAntecipacaoPagamento.readOnly = true;
    txtdtPagamento.readOnly = false;
    txtdtVendasInicio.readOnly = true;
    txtdtVendasFim.readOnly = true;
    txtdtInadimplenciaInicio.readOnly = true;
    txtdtInadimplenciaFim.readOnly = true;
    txtdtObsolescencia.readOnly = true;
    txtTotalFopag.readOnly = true;

    // Processado
    if (nEstadoID == 66) {
        chkArredondaSalarios.disabled = true;

        txtDiaAntecipacaoAviso.readOnly = true;
        txtDiaAntecipacaoPagamento.readOnly = true;
        txtDiasUteisPagamento.readOnly = true;
        txtDiasDesconsiderarVendas.readOnly = true;
        txtDiasRecuarVendas.readOnly = true;
        txtdtFopag.readOnly = true;
        txtdtFopagInicio.readOnly = true;
        txtdtFopagFim.readOnly = true;
        txtdtPagamento.readOnly = true;
    }

    return null;
}

// FINAL DE FUNCOES DA MAQUINA DE ESTADO ****************************

// FUNCOES ESPECIFICAS DO FORM **************************************

// FINAL DE FUNCOES ESPECIFICAS DO FORM *****************************

function ajustaCampos(bPaging) {
    var TipoRegistro;

    if (bPaging)
        TipoRegistro = dsoSup01.recordset['TipoFopagID'].value;
    else
        TipoRegistro = selTipoRegistroID.value;

    lblIncluiVerbasVendas.style.visibility = 'inherit';
    chkIncluiVerbasVendas.style.visibility = 'inherit';

    lblSubtraiSalarioFixo.style.visibility = 'inherit';
    chkSubtraiSalarioFixo.style.visibility = 'inherit';

    lblLimiteComissaoVendas.style.visibility = 'inherit';
    txtLimiteComissaoVendas.style.visibility = 'inherit';
    //getCloneFromOriginal('txtLimiteComissaoVendas').style.visibility = 'inherit';

    lblPercentualPremioAdministrativo.style.visibility = 'inherit';
    txtPercentualPremioAdministrativo.style.visibility = 'inherit';
    //getCloneFromOriginal('txtPercentualPremioAdministrativo').style.visibility = 'inherit';

    lblDiasDesconsiderarVendas.style.visibility = 'inherit';
    txtDiasDesconsiderarVendas.style.visibility = 'inherit';
    //getCloneFromOriginal('txtDiasDesconsiderarVendas').style.visibility = 'inherit';

    lblDiasRecuarVendas.style.visibility = 'inherit';
    txtDiasRecuarVendas.style.visibility = 'inherit';
    //getCloneFromOriginal('txtDiasRecuarVendas').style.visibility = 'inherit';

    lblDiaAvisoPontoEletronico.style.visibility = 'inherit';
    txtDiaAvisoPontoEletronico.style.visibility = 'inherit';

    lbldtVendasInicio.style.visibility = 'inherit';
    txtdtVendasInicio.style.visibility = 'inherit';
    //getCloneFromOriginal('txtdtVendasInicio').style.visibility = 'inherit';

    lbldtVendasFim.style.visibility = 'inherit';
    txtdtVendasFim.style.visibility = 'inherit';
    //getCloneFromOriginal('txtdtVendasFim').style.visibility = 'inherit';

    lbldtInadimplenciaInicio.style.visibility = 'inherit';
    txtdtInadimplenciaInicio.style.visibility = 'inherit';
    //getCloneFromOriginal('txtdtInadimplenciaInicio').style.visibility = 'inherit';

    lbldtInadimplenciaFim.style.visibility = 'inherit';
    txtdtInadimplenciaFim.style.visibility = 'inherit';
    //getCloneFromOriginal('txtdtInadimplenciaFim').style.visibility = 'inherit';

    lbldtObsolescencia.style.visibility = 'inherit';
    txtdtObsolescencia.style.visibility = 'inherit';
    //getCloneFromOriginal('txtdtObsolescencia').style.visibility = 'inherit';

    lblPercentual.style.visibility = 'inherit';
    txtPercentual.style.visibility = 'inherit';
    //getCloneFromOriginal('txtPercentual').style.visibility = 'inherit';

    if ((TipoRegistro == 1602) || (TipoRegistro == "") || (TipoRegistro == null)) {
        adjustElementsInForm([
        ['lblRegistroID', 'txtRegistroID', 10, 1],
		['lblEstadoID', 'txtEstadoID', 2, 1],
		['lblTipoRegistroID', 'selTipoRegistroID', 16, 1],
		['lbldtFopag', 'txtdtFopag', 10, 1],
		['lbldtFopagInicio', 'txtdtFopagInicio', 10, 1],
		['lbldtFopagFim', 'txtdtFopagFim', 10, 1],
		['lblProcessamentoLiberado', 'chkProcessamentoLiberado', 3, 2],
		['lblIncluiVerbasNormais', 'chkIncluiVerbasNormais', 3, 2],
		['lblIncluiVerbasVendas', 'chkIncluiVerbasVendas', 3, 2],
		['lblSubtraiSalarioFixo', 'chkSubtraiSalarioFixo', 3, 2],
		['lblArredondaSalarios', 'chkArredondaSalarios', 3, 2],
		['lblGeraFinanceirosDistintos', 'chkGeraFinanceirosDistintos', 3, 2],
		['lblLimiteComissaoVendas', 'txtLimiteComissaoVendas', 5, 2],
		['lblPercentualPremioAdministrativo', 'txtPercentualPremioAdministrativo', 5, 2],
		['lblDiaAntecipacaoAviso', 'txtDiaAntecipacaoAviso', 3, 2],
		['lblDiaAntecipacaoPagamento', 'txtDiaAntecipacaoPagamento', 3, 2],
		['lblDiasUteisPagamento', 'txtDiasUteisPagamento', 4, 2],
		['lblDiasDesconsiderarVendas', 'txtDiasDesconsiderarVendas', 4, 2],
		['lblDiasRecuarVendas', 'txtDiasRecuarVendas', 4, 2],
		['lblDiaAvisoPontoEletronico', 'txtDiaAvisoPontoEletronico', 4, 2],
		['lbldtAntecipacaoAviso', 'txtdtAntecipacaoAviso', 10, 3, 0, -3],
		['lbldtAntecipacaoPagamento', 'txtdtAntecipacaoPagamento', 10, 3],
		['lbldtPagamento', 'txtdtPagamento', 10, 3, -9],
		['lbldtVendasInicio', 'txtdtVendasInicio', 10, 4, 0, -3],
		['lbldtVendasFim', 'txtdtVendasFim', 10, 4],
		['lbldtInadimplenciaInicio', 'txtdtInadimplenciaInicio', 10, 4],
		['lbldtInadimplenciaFim', 'txtdtInadimplenciaFim', 10, 4, -10],
		['lbldtObsolescencia', 'txtdtObsolescencia', 10, 4],
		['lblPercentual', 'txtPercentual', 10, 4],
		['lblTotalFopag', 'txtTotalFopag', 11, 5],
		['lblObservacao', 'txtObservacao', 30, 5]], null, null, true);

        lblPercentual.style.visibility = 'hidden';
        txtPercentual.style.visibility = 'hidden';
    }
    else {
        adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
		['lblEstadoID', 'txtEstadoID', 2, 1],
		['lblTipoRegistroID', 'selTipoRegistroID', 16, 1],
		['lbldtFopag', 'txtdtFopag', 10, 1],
		['lbldtFopagInicio', 'txtdtFopagInicio', 10, 1],
		['lbldtFopagFim', 'txtdtFopagFim', 10, 1],
		['lblProcessamentoLiberado', 'chkProcessamentoLiberado', 3, 2],
		['lblIncluiVerbasNormais', 'chkIncluiVerbasNormais', 3, 2],
		['lblArredondaSalarios', 'chkArredondaSalarios', 3, 2],
		['lblGeraFinanceirosDistintos', 'chkGeraFinanceirosDistintos', 3, 2],
		['lblDiaAntecipacaoAviso', 'txtDiaAntecipacaoAviso', 3, 2],
		['lblDiaAntecipacaoPagamento', 'txtDiaAntecipacaoPagamento', 3, 2],
		['lblDiasUteisPagamento', 'txtDiasUteisPagamento', 4, 2],
		['lbldtAntecipacaoAviso', 'txtdtAntecipacaoAviso', 10, 3, 0, -3],
		['lbldtAntecipacaoPagamento', 'txtdtAntecipacaoPagamento', 10, 3],
		['lbldtPagamento', 'txtdtPagamento', 10, 3, -9],
		['lblPercentual', 'txtPercentual', 10, 4], //Campo mostrado somente no caso de 1610
		//Campos escondidos - Inicio
		['lblIncluiVerbasVendas', 'chkIncluiVerbasVendas', 3, 4],
		['lblSubtraiSalarioFixo', 'chkSubtraiSalarioFixo', 3, 4],
        ['lblLimiteComissaoVendas', 'txtLimiteComissaoVendas', 5, 4],
        ['lblPercentualPremioAdministrativo', 'txtPercentualPremioAdministrativo', 5, 4],
        ['lblDiasDesconsiderarVendas', 'txtDiasDesconsiderarVendas', 4, 4],
        ['lblDiasRecuarVendas', 'txtDiasRecuarVendas', 4, 4],
        ['lblDiaAvisoPontoEletronico', 'txtDiaAvisoPontoEletronico', 4, 4],
		['lbldtVendasInicio', 'txtdtVendasInicio', 10, 4, 0, -3],
		['lbldtVendasFim', 'txtdtVendasFim', 10, 4],
		['lbldtInadimplenciaInicio', 'txtdtInadimplenciaInicio', 10, 4],
		['lbldtInadimplenciaFim', 'txtdtInadimplenciaFim', 10, 4, -10],
		['lbldtObsolescencia', 'txtdtObsolescencia', 10, 4],
        //Campos escondidos - Fim
		['lblTotalFopag', 'txtTotalFopag', 11, 5],
		['lblObservacao', 'txtObservacao', 30, 5]], null, null, true);

        lblIncluiVerbasVendas.style.visibility = 'hidden';
        chkIncluiVerbasVendas.style.visibility = 'hidden';

        lblSubtraiSalarioFixo.style.visibility = 'hidden';
        chkSubtraiSalarioFixo.style.visibility = 'hidden';

        lblLimiteComissaoVendas.style.visibility = 'hidden';
        txtLimiteComissaoVendas.style.visibility = 'hidden';


        lblPercentualPremioAdministrativo.style.visibility = 'hidden';
        txtPercentualPremioAdministrativo.style.visibility = 'hidden';
        //getCloneFromOriginal('txtPercentualPremioAdministrativo').style.visibility = 'hidden';

        lblDiasDesconsiderarVendas.style.visibility = 'hidden';
        txtDiasDesconsiderarVendas.style.visibility = 'hidden';
        //getCloneFromOriginal('txtDiasDesconsiderarVendas').style.visibility = 'hidden';

        lblDiasRecuarVendas.style.visibility = 'hidden';
        txtDiasRecuarVendas.style.visibility = 'hidden';
        //getCloneFromOriginal('txtDiasRecuarVendas').style.visibility = 'hidden';

        lblDiaAvisoPontoEletronico.style.visibility = 'hidden';
        txtDiaAvisoPontoEletronico.style.visibility = 'hidden';
        //getCloneFromOriginal('txtDiaAvisoPontoEletronico').style.visibility = 'hidden';

        lbldtVendasInicio.style.visibility = 'hidden';
        txtdtVendasInicio.style.visibility = 'hidden';
        //getCloneFromOriginal('txtdtVendasInicio').style.visibility = 'hidden';

        lbldtVendasFim.style.visibility = 'hidden';
        txtdtVendasFim.style.visibility = 'hidden';
        //getCloneFromOriginal('txtdtVendasFim').style.visibility = 'hidden';

        lbldtInadimplenciaInicio.style.visibility = 'hidden';
        txtdtInadimplenciaInicio.style.visibility = 'hidden';
        //getCloneFromOriginal('txtdtInadimplenciaInicio').style.visibility = 'hidden';

        lbldtInadimplenciaFim.style.visibility = 'hidden';
        txtdtInadimplenciaFim.style.visibility = 'hidden';
        //getCloneFromOriginal('txtdtInadimplenciaFim').style.visibility = 'hidden';

        lbldtObsolescencia.style.visibility = 'hidden';
        txtdtObsolescencia.style.visibility = 'hidden';
        //getCloneFromOriginal('txtdtObsolescencia').style.visibility = 'hidden';

        if (!((selTipoRegistroID.value == 1605) || (selTipoRegistroID.value == 1606) || (selTipoRegistroID.value == 1601))) {
            lblPercentual.style.visibility = 'hidden';
            txtPercentual.style.visibility = 'hidden';
            //getCloneFromOriginal('txtPercentual').style.visibility = 'hidden';
        }
    }
}

function openModalPrint() {
    var htmlPath;
    var strPars = new String();
    var empresaFantasia = glb_empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    var nFopagID = dsoSup01.recordset['FopagID'].value;
    var nEstadoID = dsoSup01.recordset['EstadoID'].value;

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(glb_empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&nFopagID=' + escape(nFopagID);
    strPars += '&nEstadoID=' + escape(nEstadoID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(100, 50));
}
