/********************************************************************
modalprint_fopag.js

Library javascript para o modalprint.asp
fopag
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    selReports_Change()
    btnOK_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    noRights()
    
EVENTOS DO OBJETO oREL PARA USO DE ENVIO DO ARQUIVO DO Fopag
    oRel_OnBuildComplete()
    oREL_OnNotBuildComplete()
    oRel_OnRELError()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	glb_bUseProgressBar = true;
	asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    with (modalprintBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    showExtFrame(window, true);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {
		pb_StopProgressBar(true);

		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);    
    }    
}


function oRel_OnBuildComplete()
{
    if ( window.top.overflyGen.Alert('Arquivo gerado com sucesso.') == 0 )
        return null;

    lockControlsInModalWin(false);
}

function oRel_OnNotBuildComplete()
{
    lockControlsInModalWin(false);
}

function oRel_OnRELError()
{
    if ( window.top.overflyGen.Alert(oRel.ErrorDescription) == 0 )
        return null;

    lockControlsInModalWin(false);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Relat�rios', 1);

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var vGap, hGap;
    var estadoID;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;    
    
    // largura livre
    widthFree = modWidth - frameBorder;
    
    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    
    adjustElementsInForm([['lblReports', 'selReports', 25, 1, 0, 20],
                          ['lblGeraArquivo', 'chkGeraArquivo', 10, 1, 0]], null, null, true);
    
    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
   
    // O usuario tem direito ou nao
    noRights();

    // N�o libera gerar o arquivo para folha fechada.
    if (glb_nEstadoID == 41) //aberto 
    {
        lblGeraArquivo.style.visibility = 'visible';
        chkGeraArquivo.style.visibility = 'visible';
        chkGeraArquivo.checked = false;
    }
    else 
    {
        lblGeraArquivo.style.visibility = 'hidden';
        chkGeraArquivo.style.visibility = 'hidden';
        chkGeraArquivo.checked = false;
    }
        
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // Por default selReports botao OK vem travados
    /*selReports.disabled = true;
    btnOK.disabled = true;*/
    
    selReports.onchange = selReports_Change;
    
    // carrega a lista de relatorios
    loadReportsInList();
    
    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList()
{
    var i;
    
    if ( glb_arrayReports != null )
    {
        for ( i=0; i<glb_arrayReports.length; i++)    
        {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);
            
            if ( glb_arrayReports[i][2] )
                selReports.selectedIndex = i;
        }
        
        selReports.disabled = false;
    }
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change()
{
    // mostra o div correspondente ao relatorio
    showDivByReportID();
    
    window.focus();
    if ( this.disabled == false )
        this.focus();

    if (selReports.value == 40314) // Confere VT 
    {
        lblGeraArquivo.style.visibility = 'hidden';
        chkGeraArquivo.style.visibility = 'hidden';
        chkGeraArquivo.checked = false;
    }
    else {
        lblGeraArquivo.style.visibility = 'visible';
        chkGeraArquivo.style.visibility = 'visible';
        chkGeraArquivo.checked = false;
    }
    return true;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    buildTxtFopag();        

    return true;    
}

/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID()
{
    var i, coll, attr, currRep;
    
    attr = null;
    currRep = 0;
    
    currRep = selReports.value;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( attr == currRep )
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';    
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr()
{
    var i, coll, attr, retVal;
    
    attr = null;
    retVal = 0;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( coll[i].style.visibility == 'visible' )
            {
                retVal = attr;
                break;
            }    
        }
    }
    return retVal;    
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights()
{
   if ( selReports.options.length != 0 )
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;
    
    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
            coll[i].style.visibility = 'hidden';    
    }
    
    // desabilita o combo de relatorios
    selReports.disabled = true;
    
    // desabilita o botao OK
    btnOK.disabled = true;
        
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];
    
    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);
       
    elem = document.getElementById('selReports');
    
    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
        
    // a altura livre    
    modHeight -= topFree;
        
    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt'; 
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';
    
    // acrescenta o elemento
    window.document.body.appendChild(elem);
        
    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);
        
    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;
        
    return null;
}


function buildTxtFopag()
{
    var TipoID;
    var nomeRel;
        
    lockControlsInModalWin(true);

    if (selReports.value == 40308) //Ponto eletronico
    {
        TipoID = 1;
        nomeRel = 'Ponto Eletr�nico';
    }
    else if (selReports.value == 40309)//Comissao
    {
        TipoID = 2;
        nomeRel = 'Comiss�o Fopag';
    }
    else if (selReports.value == 40310)//Assist. Medica
    {
        TipoID = 3;
        nomeRel = 'Assist�ncia M�dica';
    }
    else if (selReports.value == 40311)//Assist. Odonto
    {
        TipoID = 4;
        nomeRel = 'Assist�ncia Odontologico';
    }
    else if (selReports.value == 40312)//Emprestimo Consignado
    {
        TipoID = 5;
        nomeRel = 'Emprestimo Consignado';
    }
    else if (selReports.value == 40313)//Vale Transporte
    {
        TipoID = 6;
        nomeRel = 'Vale Transporte';
    }
    else if (selReports.value == 40314)//Confere VT
    {
        TipoID = 7;
        nomeRel = 'Confere VT';
        chkGeraArquivo.checked = false;
    }
    else if (selReports.value == 40316)//Co-participa��o Conv�nio M�dico
    {
        TipoID = 8;
        nomeRel = 'Co-participa��o Conv�nio M�dico';
    }


    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var formato = 2;

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&glb_nFopagID=" + glb_nFopagID + "&chkGeraArquivo=" + chkGeraArquivo.checked + "&TipoID=" + TipoID + "&nomeRel=" + nomeRel;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/Reports_fopag.aspx?' + strParameters;

}

