/********************************************************************
modalfuncionarios.js

Library javascript para o modalfuncionarios.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_bFirstFill = false;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;

var glb_nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
var glb_sReadOnly = (glb_nEstadoID == 66 ? '*' : '');

var dsoGrava = new CDatatransport('dsoGrava');
var dsoReset = new CDatatransport('dsoReset');
var dsoGrid = new CDatatransport('dsoGrid');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalfuncionariosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalfuncionariosDblClick(grid, Row, Col)
js_modalfuncionariosKeyPress(KeyAscii)
js_modalfuncionarios_ValidateEdit()
js_modalfuncionarios_BeforeEdit(grid, row, col)
js_modalfuncionarios_AfterEdit(Row, Col)
js_fg_AfterRowColmodalfuncionarios (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalfuncionariosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    glb_bFirstFill = true;
    
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    var nFopagID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FopagID' + '\'' + '].value');
	secText('Fopag ' + nFopagID + ' - Funcion�rios', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;
	btnReset.disabled = true;
	btnImprimir.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
	
    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
			  // btnReset.offsetTop + btnReset.offsetHeight + 2;
        width = btnOK.offsetLeft + btnOK.offsetWidth + ELEM_GAP;    
        height = 37;
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    with (txtObservacoes.style)
    {
		nHeight = (8 * ELEM_GAP) + 6;
        left = ELEM_GAP;
        top = divFG.offsetTop + divFG.offsetHeight - nHeight;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = nHeight;
    }
    
    txtObservacoes.onkeyup = txtObservacoes_onkeyup;
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + ELEM_GAP;
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		title = '';
    }
    
    with (btnReset)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		style.top = btnOK.currentStyle.top;
		style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnReset.currentStyle.width, 10) - 4;
    }

    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

	adjustElementsInForm([['lblDadosAdicionais','chkDadosAdicionais',3,1,-10,-10],
		['lblAvaliacao','chkAvaliacao',3,1],
		['lblMetasVendas','chkMetasVendas',3,1],
		['lblFuncionariosDiretos','chkFuncionariosDiretos',3,1],
		['lblFuncionariosIndiretos','chkFuncionariosIndiretos',3,1,-24],
		['lblPessoalComercial','chkPessoalComercial',3,1],
		['lblPessoalAdministrativo','chkPessoalAdministrativo',3,1,-20],
		['lblObservacao','chkObservacao',3,1]], null, null, true);

    with (btnListar)
    {         
		style.width = btnReset.currentStyle.width;
		style.height = btnReset.currentStyle.height;
		style.top = btnReset.currentStyle.top;
		style.left = parseInt(chkObservacao.currentStyle.left, 10) + parseInt(chkObservacao.currentStyle.width, 10) + 
			(ELEM_GAP * 2);
    }
    
    with (btnImprimir)
    {         
		style.width = btnReset.currentStyle.width;
		style.height = btnReset.currentStyle.height;
		style.top = btnReset.currentStyle.top;
		style.left = parseInt(btnListar.currentStyle.left, 10) + parseInt(btnListar.currentStyle.width, 10) + (ELEM_GAP / 2);
    }

	chkDadosAdicionais.checked = true;
	chkAvaliacao.checked = false;
	chkMetasVendas.checked = false;

	chkDadosAdicionais.onclick = chkDadosAdicionais_onclick;
	chkAvaliacao.onclick = chkAvaliacao_onclick;
	chkMetasVendas.onclick = chkMetasVendas_onclick;
	
	chkFuncionariosDiretos.onclick = chkFuncionariosDiretos_onclick;
	chkFuncionariosIndiretos.onclick = chkFuncionariosDiretos_onclick;

	chkPessoalComercial.onclick =  chkPessoalComercial_onclick;
	chkPessoalAdministrativo.onclick =  chkPessoalComercial_onclick;
	
	chkObservacao.onclick = chkObservacao_onclick;

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

function txtObservacoes_onkeyup()
{
	if (fg.Row <= 0)
		return null;
		
	fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Observacoes')) = txtObservacoes.value;
	fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Gravar')) = 1;
}

function chkDadosAdicionais_onclick()
{
	fg.Rows = 1;
}

function chkAvaliacao_onclick()
{
	fg.Rows = 1;
}

function chkMetasVendas_onclick()
{
	fg.Rows = 1;
}

function chkFuncionariosDiretos_onclick()
{
	fg.Rows = 1;

	if ((this.id == chkFuncionariosDiretos.id) && (!chkFuncionariosDiretos.checked))
		chkFuncionariosIndiretos.checked = true;
	else if ((this.id == chkFuncionariosIndiretos.id) && (!chkFuncionariosIndiretos.checked))
		chkFuncionariosDiretos.checked = true;	
}

function chkPessoalComercial_onclick()
{
	fg.Rows = 1;
	
	if ((this.id == chkPessoalComercial.id) && (!chkPessoalComercial.checked))
		chkPessoalAdministrativo.checked = true;
	else if ((this.id == chkPessoalAdministrativo.id) && (!chkPessoalAdministrativo.checked))
		chkPessoalComercial.checked = true;	
}

function chkObservacao_onclick()
{
	var nGap = 168;
	if (chkObservacao.checked)
	{
		divFG.style.height = divFG.offsetHeight - nGap;
		txtObservacoes.style.top = parseInt(divFG.style.top,10) + parseInt(divFG.style.height, 10) + 5;
		txtObservacoes.style.height = nGap;
		txtObservacoes.style.visibility = 'inherit';
	}	
	else
	{
		divFG.style.height = divFG.offsetHeight + nGap;
		txtObservacoes.style.visibility = 'hidden';
	}

	fg.style.height = divFG.offsetHeight;
	drawBordersAroundTheGrid(fg);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnReset')
    {
		resetFuncionarios();
    }
    else if (controlID == 'btnListar')
    {
		fillGridData();
    }
    else if (controlID == 'btnImprimir')
    {
		imprimirGrid();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
	showExtFrame(window, true);
	
	window.focus();

    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
	
	btnReset.disabled = (nEstadoID == 66);
	btnOK.disabled = ((!bHasRowsInGrid) || (nEstadoID == 66));
	btnImprimir.disabled = (!bHasRowsInGrid);
}

function setTotalColumns()
{
	return null;
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    var nValue = 0;
    var nCounter = 0;
    
	if (bHasRowsInGrid)
	{
		for (i=2; i<fg.Rows; i++)
		{
			if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) != 0)
			{
				nValue += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor*'));
				nCounter++;
			}				
		}
	}
	
	fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor*')) = nValue;
	fg.TextMatrix(1, getColIndexByColKey(fg, 'FormaPagamento*')) = nCounter;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }
    
    var sDadosAvaliacao = '';
	var aGrid = null;
	var i = 0;
	var nFopagID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FopagID' + '\'' + '].value');
	var sFiltro = '';
	
	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;

    // zera o grid
    fg.Rows = 1;
    
	if (!(chkFuncionariosDiretos.checked && chkFuncionariosIndiretos.checked))
	{
		if (chkFuncionariosDiretos.checked)
			sFiltro += ' AND dbo.fn_Fopag_Funcionario(a.FopagID, a.FuncionarioID, 1) = 1 ';
		else if (chkFuncionariosIndiretos.checked)
			sFiltro += ' AND dbo.fn_Fopag_Funcionario(a.FopagID, a.FuncionarioID, 1) = 0 ';
	}

	if (!(chkPessoalComercial.checked && chkPessoalAdministrativo.checked))
	{
		if (chkPessoalComercial.checked)
			sFiltro += ' AND dbo.fn_Fopag_Funcionario(a.FopagID, a.FuncionarioID, 3) IN (1, 2) ';
		else if (chkPessoalAdministrativo.checked)
			sFiltro += ' AND dbo.fn_Fopag_Funcionario(a.FopagID, a.FuncionarioID, 3) = 0 ';
	}
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	dsoGrid.SQL = 'SELECT ISNULL(f.ItemMasculino, ' + '\'' + 'Administrativo' + '\'' + ') AS Grupo, e.RecursoFantasia AS Cargo, c.Fantasia AS Funcionario, a.*, ' +
					'd.dtAdmissao, CONVERT(BIT, a.Area) AS Comercial, g.SimboloMoeda AS SimboloMoedaSalario, h.SimboloMoeda AS SimboloMoedaVenda, ' + 
					'0 AS Gravar ' +
                  'FROM Fopag_Funcionarios a WITH(NOLOCK) ' +
                    'LEFT OUTER JOIN Conceitos g WITH(NOLOCK) ON (a.MoedaSalarioID = g.ConceitoID) ' +
                    'LEFT OUTER JOIN Conceitos h WITH(NOLOCK) ON (a.MoedaVendaID = h.ConceitoID) ' +
                    'INNER JOIN Fopag b WITH(NOLOCK) ON (a.FopagID=b.FopagID) ' +
                    'INNER JOIN Pessoas c WITH(NOLOCK) ON (a.FuncionarioID=c.PessoaID) ' +
                    'INNER JOIN RelacoesPessoas d WITH(NOLOCK) ON (c.PessoaID=d.SujeitoID AND d.ObjetoID=b.EmpresaID) ' +
                    'INNER JOIN Recursos e WITH(NOLOCK) ON (d.CargoID = e.RecursoID) ' +
                    'LEFT OUTER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (e.TipoPerfilComercialID = f.ItemID) ' +
                  'WHERE a.FopagID = '+ nFopagID + ' AND d.TipoRelacaoID=31 AND d.EstadoID = 2 ' +	' ' + sFiltro +
                  'ORDER BY ISNULL(f.ItemID, 9999), e.RecursoID, Funcionario';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
    try
    {
		dsoGrid.Refresh();
	}	
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Filtro inv�lido.') == 0 )
            return null;

		lockControlsInModalWin(false);        
        window.focus();
	}
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

	if (glb_bFirstFill)
	{
		glb_bFirstFill = false;
		showExtFrame(window, true);
		
		window.focus();
	}

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
    var bTempValue;
    var aHoldCols = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	if (!chkDadosAdicionais.checked)
	{
		aHoldCols[aHoldCols.length] = 4;
		aHoldCols[aHoldCols.length] = 5;
		aHoldCols[aHoldCols.length] = 6;
		aHoldCols[aHoldCols.length] = 7;
		aHoldCols[aHoldCols.length] = 8;
		aHoldCols[aHoldCols.length] = 9;
	}

	if (!chkMetasVendas.checked)	
	{
		aHoldCols[aHoldCols.length] = 10;
		aHoldCols[aHoldCols.length] = 11;
		aHoldCols[aHoldCols.length] = 12;
		aHoldCols[aHoldCols.length] = 13;
		aHoldCols[aHoldCols.length] = 14;
	}

	if (!chkAvaliacao.checked)
	{
		aHoldCols[aHoldCols.length] = 15;
		aHoldCols[aHoldCols.length] = 16;
	}

	aHoldCols[aHoldCols.length] = 18;
	aHoldCols[aHoldCols.length] = 19;
	aHoldCols[aHoldCols.length] = 20;
	
    headerGrid(fg,['Grupo',
				   'Cargo',
				   'ID',
				   'Funcion�rio',
				   'Admiss�o',
				   'Dir',
				   'Coml',
				   '$',
				   'Sal�rio Carteira',
				   'Sal�rio Fixo',
				   'Sal�rio Meta',
				   '$',
				   'Faturamento',
				   'Contribui��o',
				   'Repasse',
                   'Peso',
                   'Avalia��o',
                   'Observa��o',
                   'Observacoes',
                   'Gravar',
                   'FopFuncionarioID'], aHoldCols);

    glb_aCelHint = [[0,5,'Funcion�rio direto?'],
					[0,6,'�rea comercial']];

    fillGridMask(fg,dsoGrid,['Grupo*',
							 'Cargo*',
							 'FuncionarioID*',
							 'Funcionario*',
							 'dtAdmissao*',
							 'FuncionarioDireto*',
							 'Comercial*',
							 'SimboloMoedaSalario*',
							 'SalarioCarteira*',
							 'SalarioFixo*',
							 'MetaSalario' + glb_sReadOnly,
							 'SimboloMoedaVenda*',
							 'MetaFaturamento' + glb_sReadOnly,
							 'MetaContribuicao' + glb_sReadOnly,
							 'RepasseContribuicao' + glb_sReadOnly,
							 'Peso*',
							 'Avaliacao' + glb_sReadOnly,
							 'Observacao' + glb_sReadOnly,
							 'Observacoes',
							 'Gravar',
							 'FopFuncionarioID'],
							 ['','','','','99/99/9999','','','','999999999.99',    '999999999.99',  '999999999.99','',  '999999999.99',  '999999999.99',  '999999999.99',  '999999999.99','999.99','','',''],
							 ['','','','',dTFormat    ,'','','','###,###,##0.00','###,###,##0.00','###,###,##0.00','','###,###,##0.00','###,###,##0.00','###,###,##0.00','###,###,##0.00','##0.00','','','']);

	gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[2,'####0','C'], [8,'###,###,###.00','S'], [9,'###,###,###.00','S'],
		[10,'###,###,###.00','S'], [12,'###,###,###.00','S'], [13,'###,###,###.00','S'], [14,'###,###,###.00','S'],
		[15,'###,###,###.00','M'],[16,'###,###,###.00','M']]);
	
	alignColsInGrid(fg,[2,8,9,10,12,13,14,15,16]);

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;

    fg.Redraw = 0;
    fg.FrozenCols = 4;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
	paintCellsSpecialyReadOnly();
	    
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
		fg.Editable = true;
        window.focus();
        fg.focus();
        fg.Col = 1;
    }                
    else
    {
        ;
    }            

    fg.Redraw = 2;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;
	
	var nFopagID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FopagID' + '\'' + '].value');
    lockControlsInModalWin(true);
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
    
    for (i=2; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'Gravar', i) != 0)
		{
			nBytesAcum=strPars.length;
			
			if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
			{
				nBytesAcum = 0;
				if (strPars != '')
				{
					glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
					strPars = '';
					nDataLen = 0;
				}
				
				strPars += '?FopagID=' + escape(nFopagID);
			}
		
			nDataLen++;
			strPars += '&FopFuncionarioID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'FopFuncionarioID')));
			strPars += '&MetaSalario=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'MetaSalario')));
			strPars += '&MetaFaturamento=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'MetaFaturamento')));
			strPars += '&MetaContribuicao=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'MetaContribuicao')));
			strPars += '&Repasse=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'RepasseContribuicao')));
			strPars += '&Avaliacao=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Avaliacao')));
			
			if (getCellValueByColKey(fg, 'Observacao', i) != '')
				strPars += '&Observacao=' + escape('\'' + getCellValueByColKey(fg, 'Observacao', i) + '\'');
			else
				strPars += '&Observacao=' + escape('NULL');

			if (getCellValueByColKey(fg, 'Observacoes', i) != '')
				strPars += '&Observacoes=' + escape('\'' + getCellValueByColKey(fg, 'Observacoes', i) + '\'');
			else
				strPars += '&Observacoes=' + escape('NULL');
		}
	}
	
	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
		
	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
		    dsoGrava.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/alterafuncionarios.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
	}
}

function sendDataToServer_DSC()
{
	if ( !(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF))
	{
		dsoGrava.recordset.MoveFirst();
		if ( (dsoGrava.recordset['fldErrorText'].value != null) && 
			 (dsoGrava.recordset['fldErrorText'].value != '') )
		{
		    if ( window.top.overflyGen.Alert(dsoGrava.recordset['fldErrorText'].value) == 0 )
				return null;
				
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  				
			return null;
		}
	}

	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);

	glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
}

function resetFuncionarios()
{
	var sMsg = 'Os funcion�rios ser�o sincronizados e seus dados atualizados.';
    
    if (chkAvaliacao.checked)
		sMsg += '\nAs avalia��es ser�o resetadas (igualadas a 100%).';

    if (chkMetasVendas.checked)
		sMsg += '\nAs metas de vendas ser�o resetadas (igualadas ao m�s anterior).';
    
    sMsg += '\n\nTem certeza?';

	var _retMsg = window.top.overflyGen.Confirm(sMsg);

    if ( _retMsg != 1 )
		return null;

    var nFopagID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FopagID' + '\'' + '].value');
	var strPars = '?FopagID=' + escape(nFopagID) +
		'&ResetAvaliacao=' + (chkAvaliacao.checked ? '1' : '0') +
		'&ResetMetasVendas=' + (chkMetasVendas.checked ? '1' : '0');

	lockControlsInModalWin(true);

	dsoReset.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/resetfuncionarios.aspx' + strPars;
	dsoReset.ondatasetcomplete = resetFuncionarios_DSC;
	dsoReset.refresh();
}

function resetFuncionarios_DSC()
{
	lockControlsInModalWin(false);
	
	if (dsoReset.recordset.fields.count > 0)
	{
		if ( !((dsoReset.recordset.BOF)&&(dsoReset.recordset.EOF)) )
		{
		    if ((dsoReset.recordset['Resultado'].value != null) &&
				(dsoReset.recordset['Resultado'].value != ''))
			{
		        if (window.top.overflyGen.Alert(dsoReset.recordset['Resultado'].value) == 0)
					return null;
					
				return null;
			}	
		}
	}
	
	refreshParamsAndDataAndShowModalWin(true);
}

function imprimirGrid()
{
	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	var nFopagID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'FopagID' + '\'' + '].value');
	var sMsg = aEmpresaData[6] + '          Dados de funcion�rios          Fopag ' + nFopagID + '          ' + getCurrDate();

	sMsg += '     ';
	fg.PrintGrid(sMsg, false, 2, 0, 450);

	fg.AutoSizeMode = 0;
	fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 1;
}

function getCurrDate()
{
   var d, s = "";
   d = new Date();
   
   if (DATE_SQL_PARAM == 103)
   {
		s += padL(d.getDate().toString(),2,'0') + "/";
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
   }
   else
   {
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
		s += padL(d.getDate().toString(),2,'0') + "/";
   }
   
   s += d.getYear() + " ";
   s += padL(d.getHours().toString(),2,'0') + ":";
   s += padL(d.getMinutes().toString(),2,'0');

   return(s);
}


/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalfuncionariosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalfuncionariosDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    // trap so colunas editaveis
    if ( getColIndexByColKey(grid, 'OK') != Col)
        return true;
        
    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);
    
    glb_PassedOneInDblClick = true;
    
    // limpa coluna se tem um check box checado
    for ( i=2; i<grid.Rows; i++ )
    {
        if ( grid.ValueMatrix(i, Col) != 0 )
        {
            bFill = false;
            break;
        }
    }
    
    for ( i=2; i<grid.Rows; i++ )
    {
        if ( bFill )
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    
    setTotalColumns();
    
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalfuncionariosKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalfuncionarios_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalfuncionarios_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalfuncionarios_AfterEdit(Row, Col)
{
	if (fg.Editable)
	{
		if ( (Col == getColIndexByColKey(fg, 'MetaSalario')) ||
			(Col == getColIndexByColKey(fg, 'MetaFaturamento')) ||
			(Col == getColIndexByColKey(fg, 'MetaContribuicao')) ||
			(Col == getColIndexByColKey(fg, 'RepasseContribuicao')) ||
			(Col == getColIndexByColKey(fg, 'Avaliacao')) )
			fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

		fg.TextMatrix(Row, getColIndexByColKey(fg, 'Gravar')) = 1;
	}
	
	setupBtnsFromGridState();
	setTotalColumns();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalfuncionarios(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return false;

	txtObservacoes.value = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Observacoes'));
	setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************
