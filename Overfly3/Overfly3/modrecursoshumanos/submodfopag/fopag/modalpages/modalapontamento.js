/********************************************************************
modalapontamento.js

Library javascript para o modalapontamento.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_TodosMarcados = false;
var glb_aEmpresaData = null;
var glb_nUserID = 0;
var glb_Nivel = null;
//var glb_dFopagInicio = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['dtFopagInicio'].value");
//var glb_dFopagFim = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['dtFopagFim'].value");
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_bFirstFill = false;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 800 * 1;
var glb_bFirstTime = true;
var glb_bGravou = false;
var glb_bFirstGestor = true;
var glb_aFuncionarios = new Array();
var glbCampoGrid = null;
var glb_bDireitoGravacao = true;
var glb_bDireitoJustificativa = false;
var glb_bListar = false;

var dsoGrava = new CDatatransport('dsoGrava');
var dsoGestor = new CDatatransport('dsoGestor');
var dsoGrid = new CDatatransport('dsoGrid');
var dsoGridFechamento = new CDatatransport('dsoGridFechamento');
var dsoCmb01Grid = new CDatatransport('dsoCmb01Grid');
var dsoColaborador = new CDatatransport('dsoColaborador');
var dsoFuncionario = new CDatatransport('dsoFuncionario');
var dsoApontamentosOverfly = new CDatatransport('dsoApontamentosOverfly');
var dsoSincFechamento = new CDatatransport('dsoSincFechamento');
var dsoAprovaFechamento = new CDatatransport('dsoAprovaFechamento');
var dsoTxtBoxAprv = new CDatatransport('dsoTxtBoxAprv');


// Dados do grid .RDS
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
btnBarClicked(controlBar, btnClicked)
fillGridApontamento()
fillGridApontamento_DSC()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalapontamentoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalapontamentoDblClick(grid, Row, Col)
js_modalapontamentoKeyPress(KeyAscii)
js_modalapontamento_ValidateEdit()
js_modalapontamento_BeforeEdit(grid, row, col)
js_modalapontamento_AfterEdit(Row, Col)
js_fg_AfterRowColmodalapontamento (grid, OldRow, OldCol, NewRow, NewCol)

********************************************************************/

/********************************************************************
Loop de mensagens
********************************************************************/

function wndJSProc(idElement, msg, param1, param2) {
    switch (msg) {

        default:
            return null;
    }
}

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {

    // Garante inicializacao do lockControls
    //lockControls(true);
    //lockControls(false);
    window_onload_1stPart();
    
    // ajusta parametros fixos dos dsos da pagina
    dsoFixedParams();

    // configuracao inicial do html
    var sPag = setupPage();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 0;
    modalFrame.style.left = 0;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // se retorna false o usuario esta navegando pelo browser
    if (sPag == false)
        return null;

    // FG - grid
    // coloca classe no grid
    elem = window.document.getElementById('fg');

    if (elem != null)
        elem.className = 'fldGeneral';
    
    

    // ajusta o body do html
    elem = document.getElementById('modalapontamentoBody');
    
    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    //resetDateFormat();

    // a janela carregou
    sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_APONTAMENTO', 'LOADED');

    // mostrar aqui a pagina de direitos
    sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_APONTAMENTO', 'SHOW');
    
    //setFocus();
    //teste

    if ((glb_ndirC1 == 1) && (glb_ndirC2 == 1))
        glb_bDireitoGravacao = true;

    if ((glb_ndirA1 == 1) && (glb_ndirA2 == 1))
        glb_bDireitoJustificativa = true;

    refreshParamsAndDataAndShowModalWin(true);
    fillCmbGestor();
}

function fillCmbGestor() {

    var strSQL = '';
    var aEmpresaData = getCurrEmpresaData();

    lockControlsInModalWin(true);
    setConnection(dsoGestor);

    strPars = '?nEmpresaID=' + escape(glb_aEmpresaData[0]);
    strPars += '&nUsuarioID=' + escape(glb_USERID);
    strPars += '&nFopagID=' + escape(glb_nFopagID);

    dsoGestor.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/apontamentoselecao.aspx' + strPars;
    dsoGestor.ondatasetcomplete = dsoGestor_DSC;
    dsoGestor.Refresh();

}

function dsoGestor_DSC() {
    var nGestorID = -999;
    var nFuncionarioID = -999;
    var nCounter = 0;
    var nUserID = getCurrUserID();

    clearComboEx(['selGestor', 'selFuncionario']);

    if (!((dsoGestor.recordset.BOF) && (dsoGestor.recordset.EOF))) {
        while (!dsoGestor.recordset.EOF) {
            if ((nGestorID != dsoGestor.recordset['GestorID'].value) &&
				(dsoGestor.recordset['GestorID'].value != 0)) {
                var oOption = document.createElement("OPTION");
                oOption.text = dsoGestor.recordset['Gestor'].value;
                oOption.value = dsoGestor.recordset['GestorID'].value;
                

                if (dsoGestor.recordset['GestorID'].value == nUserID)
                    oOption.selected = true;

                selGestor.add(oOption);
            }

            glb_aFuncionarios[nCounter] = new Array(dsoGestor.recordset['FuncionarioID'].value,
				dsoGestor.recordset['Funcionario'].value,
				dsoGestor.recordset['GestorID'].value,
				dsoGestor.recordset['Sincronizado'].value);

            nGestorID = dsoGestor.recordset['GestorID'].value;
            nCounter++;
            dsoGestor.recordset.moveNext();
        }
    }

    if (selGestor.selectedIndex >= 0)
        selGestor_onchange();

    if ((selGestor.selectedIndex == 0) && (selFuncionario.options.length >= 2))
    {
        selFuncionario.selectedIndex = 1;
        setLabelOfControlEx(lblFuncionario, selFuncionario);


    }

    lockControlsInModalWin(false);
    selGestor.disabled = (selGestor.options.length == 0);
    selFuncionario.disabled = (selFuncionario.options.length == 0);
    //setupBtns();
    window.focus();
}


/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() 
{
    secText('Apontamento', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;

    // o frame da pagina
    var frameID = getExtFrameID(window);

    var rectFrame = new Array();

    /*if (frameID)
        rectFrame = getRectFrameInHtmlTop(frameID);
    */
    // Nao mostra se o frame esta com retangulo zerado
    // significa que o usuario esta navegando pelo browser
    /*if (rectFrame[0] == 0)
        return false;
    */
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) 
    {
        modWidth = frameRect[2];
    }

    eLeft = 2;
    eTop = 1;
    rQuote = 0;

    // desabilita o botao OK
    btnOK.disabled = true;
    btnCanc.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Ajusta o divControls
    with (divControls.style) {
        border = 5;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = btnOK.offsetLeft + btnOK.offsetWidth + ELEM_GAP;
        height = 85;
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 1;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = btnListar.offsetTop + ELEM_GAP * 9;
        //width = MAX_FRAMEWIDTH_OLD - parseInt(left, 10) * (ELEM_GAP - 10);
        width = modWidth - 2 * ELEM_GAP - 5;
        //height = modHeight - parseInt(top, 10) - (ELEM_GAP + 15);
        height = MAX_FRAMEHEIGHT_OLD  - parseInt(top, 10) - (ELEM_GAP - 67);
        
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // Esconde botao cancel
    with (btnCanc) {
        style.visibility = 'hidden';
    }

    // Esconde botao cancel
    with (btnOK) {
        style.visibility = 'hidden';
    }

    changeModal(btnApontFecha);
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnListar' || controlID == 'btnExcel')
    {
        if (selGestor.value == 0) {
            window.top.overflyGen.Alert('Selecione um gestor!');
            return null;
        }
        
        if (btnApontFecha.value == 'Fechamento')
            fillGridApontamento(controlID);
        else
            fillGridFechamento(controlID);
    }
    else if (controlID == 'btnApontFecha') {
        changeModal(ctl);
    }
    else if ((controlID == 'btnGravar') && (btnApontFecha.value == 'Fechamento')) 
    {
        if (selFuncionario.value != glb_USERID && glb_bDireitoGravacao)
            gravarApontamento();
        else {
            window.top.overflyGen.Alert('Voc� n�o tem permiss�o para alterar este apontamento.');
            return null;
        }
        
    }
    
    else if (controlID == 'btnAprovar') {
        aprovaFechamento();
        dtAprovacoes();
        
    }
    // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
- null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    showExtFrame(window, true);

    window.focus();

    // preenche o grid da janela modal
    // fillGridApontamento();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridApontamento(controlID) {
    var sEmpresa = getCurrEmpresaData();
    var _controlID = controlID;
    var sDadosAvaliacao = '';
    var aGrid = null;
    var i = 0;
    var sLinguaLogada = getDicCurrLang();
    glb_bListar = true;

    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    if (selFuncionario.value == 0) {
        window.top.overflyGen.Alert('Selecione um funcion�rio!');
        return null;
    }

    if (!verificaFiltros())
        return null;
    
    lockControlsInModalWin(true);

    if (glb_bFirstTime)
        glb_nDSOs = 2;
    else
        glb_nDSOs = 1;


    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    strPars = '?nFopagID=' + escape(glb_nFopagID);
    strPars += '&nFuncionarioID=' + escape(selFuncionario.value);
    strPars += '&sDataInicio=' + normalizeDate_DateTime(txtInicio.value, true);
    strPars += '&sDataFim=' + normalizeDate_DateTime(txtFim.value, true);
    strPars += '&sInconsistencia=' + escape(chkInconsistencia.checked);
    strPars += '&controlID=' + _controlID;
    strPars += '&sEmpresaFantasia=' + sEmpresa[3];
    strPars += '&sLinguaLogada=' + sLinguaLogada;

    if (controlID == "btnExcel") {
        lockControlsInModalWin(true);
        var frameReport = document.getElementById("frmReport");
        frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/apontamentolistar.aspx' + strPars;
        lockControlsInModalWin(false);
    }
    else {
        // zera o grid
        fg.Rows = 1;

        //Envia os dados para o servidor
        dsoGrid.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/apontamentolistar.aspx' + strPars;
        dsoGrid.ondatasetcomplete = fillGridApontamento_DSC;

        try {
            dsoGrid.Refresh();
        }
        catch (e) {
            if (window.top.overflyGen.Alert('Filtro inv�lido.' + e.message) == 0)
                return null;

            lockControlsInModalWin(false);
            window.focus();
        }

        if (glb_bFirstTime) {
            setConnection(dsoCmb01Grid);

            dsoCmb01Grid.SQL = 'SELECT -1 AS fldID, \' \' AS fldName UNION ALL ' +
                                ' SELECT SituacaoID AS fldID, Situacao AS fldName FROM SituacoesFopag WITH(NOLOCK) WHERE ehJustificativa = 0 AND EstadoID = 2 ';

            if (!glb_bDireitoJustificativa)
                dsoCmb01Grid.SQL = dsoCmb01Grid.SQL + 'AND Observacao like \'%{Gestor}%\'';

            dsoCmb01Grid.ondatasetcomplete = fillGridApontamento_DSC;
            dsoCmb01Grid.Refresh();
            glb_bFirstTime = false;
        }
    }
}
/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridApontamento_DSC() {

    var sReadOnly = '*';
    var vInconsistencia;
    var bgColorYellow;
    var bgColorYellow = 0x00ffff;
    var bgColorOrage = 0x00A5FF;
    var i;
    var aLinesState = new Array();
    var bTempValue;
    var dTFormat = '';
    var sMensagem = '';

    glb_nDSOs--;

    if (glb_nDSOs > 0)
        return null;

    glb_currDSO = dsoGrid;
    sMensagem = (dsoGrid.recordset.Fields['Mensagem'].value == null ? '' : dsoGrid.recordset.Fields['Mensagem'].value);
    
    //nao conseguiu montar a listagem
    if (sMensagem != '') 
    {
        window.top.overflyGen.Alert(sMensagem);
        //libera o grid
        lockControlsInModalWin(false);
        glb_GridIsBuilding = false;

        return null;
    }

    if (glb_bFirstFill) {
        glb_bFirstFill = false;
        showExtFrame(window, true);

        window.focus();
    }

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg, ['Data',
                    'DU',
                    'Ap 1',
                    'Ap 2',
                    'Ap 3',
                    'Ap 4',
                    'Ap 5',
                    'Ap 6',
                    'Ap 7',
                    'Ap 8',
                    'HE',
                    'Abono',
                    'AN',
                    'Abono',
                    'DSR',
                    'Abono',
                    'Falta',
                    'Abono',
                    'Atraso',
                    'Abono',
                    'Situa��oID',
                    'Justificativa',
                    'Inc',
                    'FopApontamentoID',
                    'DiferencaAlmoco'], [9, 20, 23, 24]);

    glb_aCelHint = [[0, 1, 'Dia �til'],
          			[0, 2, 'Apontamento 1'],
          			[0, 3, 'Apontamento 2'],
          			[0, 4, 'Apontamento 3'],
          			[0, 5, 'Apontamento 4'],
          			[0, 6, 'Apontamento 5'],
          			[0, 7, 'Apontamento 6'],
          			[0, 8, 'Apontamento 7'],
          			[0, 9, 'Apontamento 8'],
          			[0, 10, 'Hora Extra'],
          			[0, 12, 'Adicional Noturno'],
          			[0, 14, 'Descan�o Semanal Remunerado'],
          			[0, 22, 'Inconsist�ncia']];

    fillGridMask(fg, dsoGrid, ['DataApontamento*',
                                '_EhDiadeTrabalho*',
                                'Apontamento1',
                                'Apontamento2',
                                'Apontamento3',
                                'Apontamento4',
                                'Apontamento5',
                                'Apontamento6',
                                'Apontamento7',
                                'Apontamento8',
                                'HoraExtra*',
                                'HoraExtraAbono*',
                                'AdicNoturno*',
                                'AdicNoturnoAbono*',
                                'DSR*',
                                'DSRAbono*',
                                'Falta*',
                                'FaltaAbono*',
                                'Atraso*',
                                'AtrasoAbono*',
                                '_SituacaoID',
                                'Justificativa',
                                '_Inconsistencia*',
                                'FopApontamentoID',
                                'DiferencaAlmoco'],
							 ['', '', '##:##', '##:##', '##:##', '##:##', '##:##', '##:##', '##:##', '##:##', '##:##', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
							 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);


    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[10, '###:##', 'C'], [11, '###:##', 'C'], [12, '###:##', 'C'], [13, '###:##', 'C'], [14, '###:##', 'C'],
                                                          [15, '###:##', 'C'], [16, '###:##', 'C'], [17, '###:##', 'C'], [18, '###:##', 'C'], [22, '99', 'I']]);

    insertcomboData(fg, getColIndexByColKey(fg, 'Justificativa'), dsoCmb01Grid, 'fldName', 'fldID');
    

    fg.ColDataType(getColIndexByColKey(fg, '_EhDiadeTrabalho*')) = 11;
    fg.ColDataType(getColIndexByColKey(fg, '_Inconsistencia*')) = 11;

    // Merge de Colunas
    //fg.MergeCells = 0;
    //fg.MergeCells = 1;
    //fg.MergeCol(0) = true;
    //fg.MergeCol(1) = true;

    fg.Redraw = 0;
    fg.FrozenCols = 6;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 0;

    for (y = 1; y <= (fg.Rows - 1); y++) {
        vInconsistencia = fg.ValueMatrix(y, getColIndexByColKey(fg, '_Inconsistencia*'));

        if (vInconsistencia < 0) {
            fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento1'), y, getColIndexByColKey(fg, 'Apontamento1')) = bgColorYellow;
            fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento2'), y, getColIndexByColKey(fg, 'Apontamento2')) = bgColorYellow;
            fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento3'), y, getColIndexByColKey(fg, 'Apontamento3')) = bgColorYellow;
            fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento4'), y, getColIndexByColKey(fg, 'Apontamento4')) = bgColorYellow;
            fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento5'), y, getColIndexByColKey(fg, 'Apontamento5')) = bgColorYellow;
            fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento6'), y, getColIndexByColKey(fg, 'Apontamento6')) = bgColorYellow;
            fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento7'), y, getColIndexByColKey(fg, 'Apontamento7')) = bgColorYellow;
            fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento8'), y, getColIndexByColKey(fg, 'Apontamento8')) = bgColorYellow;
        }
        
        if ((fg.TextMatrix(y, getColIndexByColKey(fg, 'DiferencaAlmoco'))) < 60 && (fg.TextMatrix(y, getColIndexByColKey(fg, 'DiferencaAlmoco'))) != 0)
            fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento3*'), y, getColIndexByColKey(fg, 'Apontamento3*')) = bgColorOrage;

        if (fg.ValueMatrix(y, getColIndexByColKey(fg, 'Falta*')) != 0 && fg.ValueMatrix(y, getColIndexByColKey(fg, 'FaltaAbono*')) == 0) 
            fg.Cell(6, y, getColIndexByColKey(fg, 'Falta*'), y, getColIndexByColKey(fg, 'Falta*')) = bgColorYellow;
        
        if (fg.TextMatrix(y, getColIndexByColKey(fg, 'Atraso*')) != "00:00" && fg.TextMatrix(y, getColIndexByColKey(fg, 'AtrasoAbono*')) == "00:00") 
            fg.Cell(6, y, getColIndexByColKey(fg, 'Atraso*'), y, getColIndexByColKey(fg, 'Atraso*')) = bgColorYellow;

        if (fg.TextMatrix(y, getColIndexByColKey(fg, 'HoraExtra*')) != "00:00" && fg.TextMatrix(y, getColIndexByColKey(fg, 'HoraExtraAbono*')) == "00:00")
            fg.Cell(6, y, getColIndexByColKey(fg, 'HoraExtra*'), y, getColIndexByColKey(fg, 'HoraExtra*')) = bgColorYellow;
        
        if (y == 1) 
        {
            fg.TextMatrix(y, getColIndexByColKey(fg, 'HoraExtra*')) = fg.TextMatrix(y + 1, getColIndexByColKey(fg, 'HoraExtra*'));
            fg.TextMatrix(y, getColIndexByColKey(fg, 'HoraExtraAbono*')) = fg.TextMatrix(y + 1, getColIndexByColKey(fg, 'HoraExtraAbono*'));
            fg.TextMatrix(y, getColIndexByColKey(fg, 'AdicNoturno*')) = fg.TextMatrix(y + 1, getColIndexByColKey(fg, 'AdicNoturno*'));
            fg.TextMatrix(y, getColIndexByColKey(fg, 'AdicNoturnoAbono*')) = fg.TextMatrix(y + 1, getColIndexByColKey(fg, 'AdicNoturnoAbono*'));
            fg.TextMatrix(y, getColIndexByColKey(fg, 'DSR*')) = fg.TextMatrix(y + 1, getColIndexByColKey(fg, 'DSR*'));
            fg.TextMatrix(y, getColIndexByColKey(fg, 'DSRAbono*')) = fg.TextMatrix(y + 1, getColIndexByColKey(fg, 'DSRAbono*'));
            fg.TextMatrix(y, getColIndexByColKey(fg, 'Falta*')) = fg.TextMatrix(y + 1, getColIndexByColKey(fg, 'Falta*'));
            fg.TextMatrix(y, getColIndexByColKey(fg, 'FaltaAbono*')) = fg.TextMatrix(y + 1, getColIndexByColKey(fg, 'FaltaAbono*'));
            fg.TextMatrix(y, getColIndexByColKey(fg, 'Atraso*')) = fg.TextMatrix(y + 1, getColIndexByColKey(fg, 'Atraso*'));
            fg.TextMatrix(y, getColIndexByColKey(fg, 'AtrasoAbono*')) = fg.TextMatrix(y + 1, getColIndexByColKey(fg, 'AtrasoAbono*'));

        }

    }
    //deleta a segunda linha de totais
    fg.RemoveItem(2);

    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);

    fg.Redraw = 2;

    if (selFuncionario.value != glb_USERID && glb_bDireitoGravacao) {
        btnGravar.style.visibility = 'inherit';
        fg.editable = true;
    }
    else {
        btnGravar.style.visibility = 'hidden';
        fg.editable = false;
    }

    //dsoCmb01Grid.recordset.setFilter('GestorAltera = 1');
    
    pinta_apontamentos_overfly();

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {

        fg.Editable = true;
        window.focus();
        fg.focus();
        //fg.Col = 1;
    }
    else {
        fg.Editable = false;
        window.focus();
        fg.focus();
    }

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly() {
    paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalapontamentoBeforeMouseDown(grid, Row, Col) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalapontamentoDblClick(grid, Row, Col) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalapontamentoKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalapontamento_ValidateEdit(fg, Row, Col) {
    ;
    
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalapontamento_BeforeEdit(grid, row, col) {
    
    
    if (grid.TextMatrix(row, col))
    {
        if (((grid.ColKey(col) == "Apontamento1") || (grid.ColKey(col) == "Apontamento2") ||
             (grid.ColKey(col) == "Apontamento3") || (grid.ColKey(col) == "Apontamento4") ||
             (grid.ColKey(col) == "Apontamento5") || (grid.ColKey(col) == "Apontamento6") ||
             (grid.ColKey(col) == "Apontamento7") || (grid.ColKey(col) == "Apontamento8"))) 
        {
            
            if (grid.CellBackColor == 8388564 & grid.TextMatrix(row, col) == "" & glb_bListar != false)
            {
                dsoGrid.recordset.setFilter('FopApontamentoID = ' + grid.ValueMatrix(row, getColIndexByColKey(fg, 'FopApontamentoID')));
            }
        }
        else
            glbCampoGrid = null;
    }
    else
        glbCampoGrid = null;

    glb_bListar = false;
    
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalapontamento_AfterEdit(Row, Col) {

    var hora = fg.TextMatrix(Row, Col);
    var horaminuto = hora.split(":");
    dsoGrid.recordset.setFilter( 'FopApontamentoID = ' + fg.ValueMatrix(Row, getColIndexByColKey(fg, 'FopApontamentoID')));
    glbCampoGrid = dsoGrid.recordset[fg.ColKey(Col)].value;
    
    if ((fg.ColKey(Col) == "Apontamento1") || (fg.ColKey(Col) == "Apontamento2") ||
        (fg.ColKey(Col) == "Apontamento3") || (fg.ColKey(Col) == "Apontamento4") ||
        (fg.ColKey(Col) == "Apontamento5") || (fg.ColKey(Col) == "Apontamento6") ||
        (fg.ColKey(Col) == "Apontamento7") || (fg.ColKey(Col) == "Apontamento8")) 
    {
        if (horaminuto[0] > 23 || horaminuto[0] < 0 || horaminuto[1] < 0 || horaminuto[1] > 59) 
        {
            window.top.overflyGen.Alert('Digite uma hora v�lida!');
            fg.TextMatrix(Row, Col) = '';
            fg.Row = Row;
            fg.Col = Col;
            return false;
        }

    }

    
    if (trimStr(fg.TextMatrix(Row, Col)) == ":")
        fg.TextMatrix(Row, Col) = '';
        
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
grid
OldRow
OldCol
NewRow
NewCol
********************************************************************/
function js_fg_AfterRowColmodalapontamento(grid, OldRow, OldCol, NewRow, NewCol) {
    var firstLine = 1;

    if (glb_totalCols__ != false)
        firstLine = 2;

    if (NewRow >= firstLine)
        fg.Row = NewRow;
    else if ((NewRow < firstLine) && (fg.Rows > firstLine))
        fg.Row = firstLine;
}

function changeModal(ctl) {

    var tamlblColaborador, tamlblInicio, tamlblFim;

    //sempre zera o grid na troca de botao
    fg.Rows = 1;
    fg.Cols = 0;

    if (ctl.id == 'btnApontFecha' && ctl.value == 'Fechamento') {
        secText('Fechamento', 1);
        ctl.value = 'Apontamento';

        lblInicio.style.visibility = 'hidden';
        txtInicio.style.visibility = 'hidden';

        lblFim.style.visibility = 'hidden';
        txtFim.style.visibility = 'hidden';

        lblInconsistencia.style.visibility = 'hidden';
        chkInconsistencia.style.visibility = 'hidden';
        btnGravar.style.visibility = 'hidden';
        
        if (glb_nEstadoID == 41)
            btnAprovar.style.visibility = 'inherit';
        else
            btnAprovar.style.visibility = 'hidden';
        
        lblSincSim.style.visibility = 'inherit';
        chkSincSim.style.visibility = 'inherit';
        lblSincNao.style.visibility = 'inherit';
        chkSincNao.style.visibility = 'inherit';
        lblAprFuncionario.style.visibility = 'inherit';
        txtAprFuncionario.style.visibility = 'inherit';
        lblAprGestor.style.visibility = 'inherit';
        txtAprGestor.style.visibility = 'inherit';
        
        txtAprGestor.readOnly = true;
        txtAprFuncionario.readOnly = true;
        
       
        chkSincSim.onclick = chkSinc_onclick;
        chkSincNao.onclick = chkSinc_onclick;
        
        adjustElementsInForm([['btnApontFecha', 'btnApontFecha', 80, 1, 1,-10],
                          ['lblGestor', 'selGestor', 21, 1, 10],
                          ['lblFuncionario', 'selFuncionario', 21, 1, 10],
                          ['lblSincSim', 'chkSincSim', 3, 1, -5],
						  ['lblSincNao', 'chkSincNao', 3, 1, -20],
	                      ['btnListar', 'btnListar', 60, 1, 10],
                          ['btnExcel', 'btnExcel', 60, 1, 10],
                          ['btnAprovar', 'btnAprovar', 60, 1, 10],
                          ['lblAprFuncionario', 'txtAprFuncionario', 14, 1, 10],
                          ['lblAprGestor', 'txtAprGestor', 14, 1, -6]], null, null, true);
        
        if (selFuncionario.value != "")
            fillGridFechamento("btnListar");
        
    }
    else if (ctl.id == 'btnApontFecha' && ctl.value == 'Apontamento') {

        secText('Apontamentos', 1);
        ctl.value = 'Fechamento';
        lblInicio.style.visibility = 'inherit';
        txtInicio.style.visibility = 'inherit';

        lblFim.style.visibility = 'inherit';
        txtFim.style.visibility = 'inherit';

        lblInconsistencia.style.visibility = 'inherit';
        chkInconsistencia.style.visibility = 'inherit';

        btnGravar.style.visibility = 'inherit';
        btnAprovar.style.visibility = 'hidden';

        lblSincSim.style.visibility = 'hidden';
        chkSincSim.style.visibility = 'hidden';
        lblSincNao.style.visibility = 'hidden';
        chkSincNao.style.visibility = 'hidden';
        lblAprFuncionario.style.visibility = 'hidden';
        txtAprFuncionario.style.visibility = 'hidden';
        lblAprGestor.style.visibility = 'hidden';
        txtAprGestor.style.visibility = 'hidden';
        
        

        adjustElementsInForm([['btnApontFecha', 'btnApontFecha', 80, 1, 1,-10],
                          ['lblGestor', 'selGestor', 21, 1, 10],
                          ['lblFuncionario', 'selFuncionario', 21, 1, 10],
	                      ['lblInicio', 'txtInicio', 10, 1, 10],
	                      ['lblFim', 'txtFim', 10, 1, 5],
	                      ['lblInconsistencia', 'chkInconsistencia', 3, 1, -3],
	                      ['btnListar', 'btnListar', 60, 1, 10],
	                      ['btnGravar', 'btnGravar', 60, 1, 10],
                          ['btnExcel', 'btnExcel', 60, 1, 10]], null, null, true);

        if (selFuncionario.value != "")
            fillGridApontamento('btnListar');
        
    }
    
    btnApontFecha.style.height = selFuncionario.offsetHeight + 2;
    btnListar.style.height = selFuncionario.offsetHeight + 2;
    btnGravar.style.height = selFuncionario.offsetHeight + 2;
    btnExcel.style.height = selFuncionario.offsetHeight + 2;
    btnAprovar.style.height = selFuncionario.offsetHeight + 2;
}

function fillGridFechamento(controlID) {

    var sEmpresa = getCurrEmpresaData();
    var _controlID = controlID;
    var sDadosAvaliacao = '';
    var aGrid = null;
    var i = 0;
    var sFiltro = '';
    var sLinguaLogada = getDicCurrLang();

    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    lockControlsInModalWin(true);
    glb_nDSOs = 1;



    // parametrizacao do dso dsoGrid

    setConnection(dsoGridFechamento);

    strPars = '?nFopagID=' + escape(glb_nFopagID);
    strPars += '&nUsuarioID=' + escape(glb_USERID);
    strPars += '&nGestorID=' + escape(selGestor.value);
    strPars += '&nFuncionarioID=' + escape(selFuncionario.value);
    strPars += '&controlID=' + _controlID;
    strPars += '&sEmpresaFantasia=' + sEmpresa[3];
    strPars += '&sLinguaLogada=' + sLinguaLogada;
        
    if (chkSincSim.checked && chkSincNao.checked)
        strPars += '&bSincronizado=' + escape(null);
    else if (chkSincSim.checked)
        strPars += '&bSincronizado=' + escape(1);
    else
        strPars += '&bSincronizado=' + escape(0);
    
    //Gera o relat�rio
    if (controlID == "btnExcel") {
        lockControlsInModalWin(true);
        var frameReport = document.getElementById("frmReport");
        frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/fechamentolistar.aspx' + strPars;
        lockControlsInModalWin(false);
    }

    //Lista no grid
    else {
        // zera o grid
        fg.Rows = 1;

        //Envia os dados para o servidor
        dsoGridFechamento.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/fechamentolistar.aspx' + strPars;
        dsoGridFechamento.ondatasetcomplete = fillGridFechamento_DSC;

        try {
            dsoGridFechamento.Refresh();
        }
        catch (e) {
            if (window.top.overflyGen.Alert('N�o possui fechamento para este colaborador.') == 0)
                return null;

            lockControlsInModalWin(false);
            window.focus();
        }

        dtAprovacoes();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridFechamento_DSC() {

    var sReadOnly = '*';
    var sComboNivel = "#";
    var sComboNivelMax = "";
    var nComboNivelCount;
    var dTFormat = '';
    var i;
    var aLinesState = new Array();
    var bTempValue;
    var sMensagem = '';

    glb_nDSOs--;

    if (glb_nDSOs > 0)
        return null;

    glb_currDSO = dsoGridFechamento;
    sMensagem = (dsoGridFechamento.recordset.Fields['Mensagem'].value == null ? '' : dsoGridFechamento.recordset.Fields['Mensagem'].value);

    //nao conseguiu montar a listagem
    if (sMensagem != '') {
        window.top.overflyGen.Alert(sMensagem);
        //libera o grid
        lockControlsInModalWin(false);
        glb_GridIsBuilding = false;

        return null;
    }


    if (glb_bFirstFill) {
        glb_bFirstFill = false;
        showExtFrame(window, true);

        window.focus();
    }

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.Redraw = 0;
    fg.Editable = true;
    startGridInterface(fg);
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg, ['Sinc',
                    'ID',
                    'Colaborador',
                    'Aprov Gestor',
                    'Aprov Func',
                    'VerbaID',
                    'Verba',
                    'Total',
                    'C�digo',
                    'Evento',
                    'Unidade',
                    'N�mero',
                    'FopFechamentoID'], [12]);

    fillGridMask(fg, dsoGridFechamento, 
                  [
                    'Sincronizado',
                    'ID',
                    'Colaborador',
                    'DtAprovacaoGestor',
                    'DtAprovacaoFunc',
                    'VerbaID',
                    'Verba',
                    'Total',
                    'ItemID',
                    'Evento',
                    'Unidade',
                    'Numero',
                    'FopFechamentoID'],
				 ['', '', '', '', '', '', '', '', '', '', '', '', ''],
				 ['', '', '', '', '', '', '', '', '', '', '', '', '']);

    for (i = 2; i < fg.Rows; i++) 
    {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Numero')) < 0) 
        {
            fg.Select(i, getColIndexByColKey(fg, 'Numero'), i, getColIndexByColKey(fg, 'Numero'));
            fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }
    }
    alignColsInGrid(fg, [8]);
    
    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;
    fg.MergeCol(5) = true;
    fg.MergeCol(6) = true;
    fg.MergeCol(7) = true;
        
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) 
    {

        fg.Editable = false;
        window.focus();
        fg.focus();
        fg.Col = 1;
    }
    else 
    {
        fg.Editable = false;
        window.focus();
        fg.focus();
    }

    if (fg.Rows > 1) 
    {
        fg.Row = 1;
        fg.TopRow = 1;
    }

    fg.Redraw = 2;
}


function js_fg_BeforeRowColChangeApontamento(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
}

function gravarApontamento() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var apont1 = new String();
    var apont2 = new String();
    var apont3 = new String();
    var apont4 = new String();
    var apont5 = new String();
    var apont6 = new String();
    var apont7 = new String();
    var apont8 = new String();
    var atraso = new String();
    var falta = new String();
    var dataApontamento = new String();
    var nBytesAcum = 0;
    
    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    glb_bGravou = false;
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrava);

    lockControlsInModalWin(true);

    nDataLen = 0;
    strPars = '';
    strPars = '?nFopagID=' + escape(glb_nFopagID);
    strPars += '&nPessoaID=' + escape(selFuncionario.value);

    for (y = 2; y <= (fg.Rows - 1); y++) {
        
        //dsoGrid.recordset.moveFirst();
        dsoGrid.recordset.setFilter('FopApontamentoID = ' + fg.ValueMatrix(y, getColIndexByColKey(fg, 'FopApontamentoID')));
        
        //dsoGrid.recordset.MoveNext();

        if ((fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento1')) != dsoGrid.recordset['Apontamento1'].value) ||
            (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento2')) != dsoGrid.recordset['Apontamento2'].value) ||
        (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento3')) != dsoGrid.recordset['Apontamento3'].value) ||
        (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento4')) != dsoGrid.recordset['Apontamento4'].value) ||
        (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento5')) != dsoGrid.recordset['Apontamento5'].value) ||
        (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento6')) != dsoGrid.recordset['Apontamento6'].value) ||
        (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento7')) != dsoGrid.recordset['Apontamento7'].value) ||
        (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento8')) != dsoGrid.recordset['Apontamento8'].value) ||
        (fg.TextMatrix(y, getColIndexByColKey(fg, 'Justificativa')) != dsoGrid.recordset['Justificativa'].value)) 
        {
            apont1 = "";
            apont2 = "";
            apont3 = "";
            apont4 = "";
            apont5 = "";
            apont6 = "";
            apont7 = "";
            apont8 = "";

            dataApontamento = fg.TextMatrix(y, getColIndexByColKey(fg, 'DataApontamento*'));
            apont1 = (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento1')) == '') ? "" :
                                            normalizeDate_DateTime(dataApontamento + ' ' +
                                                                   fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento1')), true);
                                                                   
            apont2 = (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento2')) == '') ? "" :
                                            normalizeDate_DateTime(dataApontamento + ' ' +
                                                                   fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento2')), true);

            apont3 = (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento3')) == '') ? "" :
                                            normalizeDate_DateTime(dataApontamento + ' ' +
                                                                   fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento3')), true);

            apont4 = (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento4')) == '') ? "" :
                                            normalizeDate_DateTime(dataApontamento + ' ' +
                                                                   fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento4')), true);

            apont5 = (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento5')) == '') ? "" :
                                            normalizeDate_DateTime(dataApontamento + ' ' +
                                                                   fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento5')), true);

            apont6 = (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento6')) == '') ? "" :
                                            normalizeDate_DateTime(dataApontamento + ' ' +
                                                                   fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento6')), true);

            apont7 = (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento7')) == '') ? "" :
                                            normalizeDate_DateTime(dataApontamento + ' ' +
                                                                   fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento7')), true);

            apont8 = (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento8')) == '') ? "" :
                                            normalizeDate_DateTime(dataApontamento + ' ' +
                                                                   fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento8')), true);

            strPars += '&sDataApontamento=' + escape(normalizeDate_DateTime(dataApontamento, true));
            strPars += '&sDataApontamento1=' + escape(apont1);
            strPars += '&sDataApontamento2=' + escape(apont2);
            strPars += '&sDataApontamento3=' + escape(apont3);
            strPars += '&sDataApontamento4=' + escape(apont4);
            strPars += '&sDataApontamento5=' + escape(apont5);
            strPars += '&sDataApontamento6=' + escape(apont6);
            strPars += '&sDataApontamento7=' + escape(apont7);
            strPars += '&sDataApontamento8=' + escape(apont8);
            strPars += '&nJustificativaID=' + escape(fg.ValueMatrix(y, getColIndexByColKey(fg, 'Justificativa')));
            strPars += '&nUsuarioID=' + escape(glb_nUserID);

            nDataLen++;


            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) 
            {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                    strPars = '';
                    strPars = '?nFopagID=' + escape(glb_nFopagID);
                    strPars += '&nPessoaID=' + escape(selFuncionario.value);
                    nDataLen = 0;
                }
            }
            
        }
        dsoGrid.recordset.setFilter('');
    }
    if (nDataLen>0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);

    sendDataToServer();
    
    
}
function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
		    dsoGrava.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/gravaapontamento.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = dsoGravarApontamento_DSC;
            dsoGrava.Refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			fillGridApontamento('btnListar');
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Erro. Tente novamente.') == 0 )
			return null;


        lockControlsInModalWin(false);
        fillGridApontamento('btnListar');
		
	}
}


function dsoGravarApontamento_DSC() {
    glb_nPointToaSendDataToServer++;
    lockControlsInModalWin(false);
    var sMensagem = (dsoGrava.recordset['Resultado'].value == null ? '' : dsoGrava.recordset['Resultado'].value);

    if (sMensagem != '')
        window.top.overflyGen.Alert(sMensagem);
    
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
    /*fillGridApontamento();
    return null;
    */
}

function pinta_apontamentos_overfly() {

    setConnection(dsoApontamentosOverfly);
    
    dsoApontamentosOverfly.SQL = ' SELECT CONVERT(VARCHAR, Data,' + DATE_SQL_PARAM + ') AS Data, ISNULL(CONVERT(VARCHAR(5), Data, 108),\'\') AS DataApontamento ' +
							 ' FROM  [OVERFLYBASE_EXT2].MultiBanco.NewDMP.IMPORTACAO_CONTEUDO  WITH(NOLOCK) ' +
						     ' WHERE CRACHA = ' + selFuncionario.value +
							 ' AND CODIGOEMPRESA = ' + glb_aEmpresaData[0] +
							 ' AND DATA >= \'' + normalizeDate_DateTime(glb_sDataInicio,true) + '\'' +
							 ' AND DATA <= \'' + normalizeDate_DateTime(glb_sDataFim,true) + '\'' + 
							 ' AND TIPOREGISTRO = \'I\'';

    dsoApontamentosOverfly.ondatasetcomplete = dsoApontamentosOverfly_DSC;
    dsoApontamentosOverfly.Refresh();


}

function dsoApontamentosOverfly_DSC() {
    var bgColorBlue = 0X7FFFD4;

    if ((!dsoApontamentosOverfly.recordset.BOF) && (!dsoApontamentosOverfly.recordset.EOF)) {
        for (y = 1; y <= (fg.Rows - 1); y++) {
            strPars = '';

            dsoApontamentosOverfly.recordset.moveFirst();
            dsoApontamentosOverfly.recordset.setFilter('Data = \'' + fg.TextMatrix(y, getColIndexByColKey(fg, 'DataApontamento*')) + '\'');

            while (!dsoApontamentosOverfly.recordset.EOF) {
                if (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento1')) == dsoApontamentosOverfly.recordset['DataApontamento'].value) {
                    fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento1'), y, getColIndexByColKey(fg, 'Apontamento1')) = bgColorBlue;
                }
                if (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento2')) == dsoApontamentosOverfly.recordset['DataApontamento'].value) {
                    fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento2'), y, getColIndexByColKey(fg, 'Apontamento2')) = bgColorBlue;
                }
                if (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento3')) == dsoApontamentosOverfly.recordset['DataApontamento'].value) {
                    fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento3'), y, getColIndexByColKey(fg, 'Apontamento3')) = bgColorBlue;
                }
                if (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento4')) == dsoApontamentosOverfly.recordset['DataApontamento'].value) {
                    fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento4'), y, getColIndexByColKey(fg, 'Apontamento4')) = bgColorBlue;
                }
                if (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento5')) == dsoApontamentosOverfly.recordset['DataApontamento'].value) {
                    fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento5'), y, getColIndexByColKey(fg, 'Apontamento5')) = bgColorBlue;
                }
                if (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento6')) == dsoApontamentosOverfly.recordset['DataApontamento'].value) {
                    fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento6'), y, getColIndexByColKey(fg, 'Apontamento6')) = bgColorBlue;
                }
                if (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento7')) == dsoApontamentosOverfly.recordset['DataApontamento'].value) {
                    fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento7'), y, getColIndexByColKey(fg, 'Apontamento7')) = bgColorBlue;
                }
                if (fg.TextMatrix(y, getColIndexByColKey(fg, 'Apontamento8')) == dsoApontamentosOverfly.recordset['DataApontamento'].value) {
                    fg.Cell(6, y, getColIndexByColKey(fg, 'Apontamento8'), y, getColIndexByColKey(fg, 'Apontamento8')) = bgColorBlue;
                }
                dsoApontamentosOverfly.recordset.moveNext();
            }
            dsoApontamentosOverfly.recordset.setFilter('');
        }
    }
}

function selGestor_onchange() {

    setLabelOfControlEx(lblGestor, selGestor);
    fg.Rows = 1;
    fillCmbFuncionarios();
    setLabelOfControlEx(lblFuncionario, selFuncionario);

}

function selFuncionario_onchange() 
{
    
    fg.Rows = 1;
    setLabelOfControlEx(lblFuncionario, selFuncionario);
    
    if (selFuncionario.options.item(selFuncionario.selectedIndex).getAttribute('Sincronizado', 1))
        lblFuncionario.style.color = 'black';
    else
        lblFuncionario.style.color = 'red';
    
    if (btnApontFecha.value == 'Fechamento')
        fillGridApontamento('btnListar');
    else
        fillGridFechamento('btnListar');
       
    
}


function fillCmbFuncionarios() {

    var nGestorID = selGestor.value;
    var nUserID = getCurrUserID();
    var i = 0;

    clearComboEx(['selFuncionario']);

    if (glb_aFuncionarios.length > 1) {
        var oOption = document.createElement("OPTION");
        oOption.text = '';
        oOption.value = 0;
        selFuncionario.add(oOption);
        asort(glb_aFuncionarios, 1);
    }
    
    if (selGestor.selectedIndex == 0) {
        var oOption = document.createElement("OPTION");
        oOption.text = glb_sUser;
        oOption.value = nUserID;
        selFuncionario.add(oOption);
    }

    for (i = 0; i < glb_aFuncionarios.length; i++) {
        if ((nGestorID == 0) || (glb_aFuncionarios[i][2] == nGestorID)) {
            if ((glb_aFuncionarios[i][0] != nUserID) || (selGestor.selectedIndex > 0)) {
                var oOption = document.createElement("OPTION");
                oOption.value = glb_aFuncionarios[i][0];
                oOption.text = glb_aFuncionarios[i][1];
                oOption.setAttribute('Sincronizado', glb_aFuncionarios[i][3], 1);
                 selFuncionario.add(oOption);
            }
        }
    }

    if (selFuncionario.options.item(selFuncionario.selectedIndex).getAttribute('Sincronizado', 1))
        lblFuncionario.style.color = 'black';
    else
        lblFuncionario.style.color = 'red';
    
    
}

/*function sincronizaFechamento() 
{
    //faz pergunta para saber se o usuario realmente quer sincronizar
    lockInterface(false);
    var _retMsg = window.top.overflyGen.Confirm('Deseja realmente Sincronizar?');
    
    if (_retMsg == 0)
        return null;
    else if (_retMsg == 1) 
    {
       // parametrizacao do dso dsoSincFechamento
       setConnection(dsoSincFechamento);

       strPars = '?nFopagID=' + escape(glb_nFopagID);
       strPars += '&nUsuarioID=' + escape(glb_USERID);

       dsoSincFechamento.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/sincronizaFechamento.aspx' + strPars;
       dsoSincFechamento.ondatasetcomplete = sincronizaFechamento_DSC;
       dsoSincFechamento.Refresh();
            

    }
    else
        return true;

}

function sincronizaFechamento_DSC() 
{
    var sMensagem = (dsoSincFechamento.recordset['Mensagem'].value == null ? '' : dsoSincFechamento.recordset['Mensagem'].value);

    if (sMensagem != '') 
    {
        if (window.top.overflyGen.Alert(sMensagem) == 0)
            return null;
    }            
}
*/
function aprovaFechamento() 
{
    lockInterface(true);

    // parametrizacao do dso dsoAprovaFechamento
    setConnection(dsoAprovaFechamento);

    strPars = '?nFopagID=' + escape(glb_nFopagID);
    strPars += '&nUsuarioID=' + escape(glb_USERID);
    strPars += '&nFuncionarioID=' + escape(selFuncionario.value);

    dsoAprovaFechamento.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/aprovaFechamento.aspx' + strPars;
    dsoAprovaFechamento.ondatasetcomplete = aprovaFechamento_DSC;
    dsoAprovaFechamento.Refresh();
    
}

function aprovaFechamento_DSC() 
{
    lockInterface(false);
    var sMensagem = (dsoAprovaFechamento.recordset['Mensagem'].value == null ? '' : dsoAprovaFechamento.recordset['Mensagem'].value);

    if (sMensagem != '') {
        if (window.top.overflyGen.Alert(sMensagem) == 0)
            return null;
    }

}

function verificaFiltros() {
    var dataAtual = getCurrDate();
    
    if (trimStr(txtInicio.value) != "") {
        if (chkDataEx(txtInicio.value) == false) {
            window.top.overflyGen.Alert('Data inv�lida.');
            return null;
        }
    }
    else
        txtInicio.value = glb_sDataInicio;

    if (trimStr(txtFim.value) != "") {
        if (chkDataEx(txtFim.value) == false) {
            window.top.overflyGen.Alert('Data inv�lida.');
            return null;
        }
    }
    else
        txtFim.value = glb_sDataFim;

    if (daysBetween(txtInicio.value, txtFim.value) < 0) {
        window.top.overflyGen.Alert('Data inicio deve ser menor que a data fim');
        return null;
    }

    if (daysBetween(txtFim.value, dataAtual) < 0)
    {
        window.top.overflyGen.Alert('Data fim n�o pode ser maior que ' + dataAtual);
        txtFim.value = dataAtual;
    }
    if (daysBetween(txtInicio.value, txtFim.value) > 90) {
        window.top.overflyGen.Alert('O per�odo m�ximo � de 90 dias');
        return null;
    }
    /*if ((trimStr(txtInicio.value) != "" && (!((daysBetween(glb_sDataInicio, txtInicio.value) >= -90) && (daysBetween(glb_sDataFim, txtInicio.value) <= -90)))) ||
        (trimStr(txtFim.value) != "" && (!((daysBetween(glb_sDataInicio, txtFim.value) >= -90) && (daysBetween(glb_sDataFim, txtFim.value) <= -90))))) {
        window.top.overflyGen.Alert('Escolha uma data entre ' + glb_sDataInicio + ' e ' + glb_sDataFim);
        return null;
    }*/
    
    return true;
}

function chkSinc_onclick() 
{
    if ((this.id == chkSincSim.id) && (!chkSincSim.checked))
        chkSincNao.checked = true;
    else if ((this.id == chkSincNao.id) && (!chkSincNao.checked))
        chkSincSim.checked = true;
}
//Preenche os TxtBox de datas de aprova��o da folha de ponto
function dtAprovacoes() 
{
    setConnection(dsoTxtBoxAprv);
    if (selFuncionario.value != 0 ) {
        dsoTxtBoxAprv.SQL = 'SELECT CONVERT(VARCHAR,DtAprovacaoColaborador, ' + DATE_SQL_PARAM + ') AS AprFuncionario, CONVERT(VARCHAR,DtAprovacaoGestor, ' + DATE_SQL_PARAM + ') AS AprGestor FROM Fopag_Funcionarios WITH(NOLOCK) ' +
                            'WHERE FopagID = ' + escape(glb_nFopagID) + 'AND FuncionarioID = ' + escape(selFuncionario.value);

        dsoTxtBoxAprv.ondatasetcomplete = dtAprovacoes_DSC;
        dsoTxtBoxAprv.Refresh();
    }
    else {
        txtAprFuncionario.value = '';
        txtAprGestor.value = '';
    }
        
}

function dtAprovacoes_DSC() {

    txtAprFuncionario.value =(dsoTxtBoxAprv.recordset['AprFuncionario'].value == null ? '' : dsoTxtBoxAprv.recordset['AprFuncionario'].value);
    txtAprGestor.value = (dsoTxtBoxAprv.recordset['AprGestor'].value == null ? '' : dsoTxtBoxAprv.recordset['AprGestor'].value);

}
function getCurrDate() {
    var d, s = "";
    d = new Date();

    if (DATE_SQL_PARAM == 103) {
        s += padL(d.getDate().toString(), 2, '0') + "/";
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
    }
    else {
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
        s += padL(d.getDate().toString(), 2, '0') + "/";
    }

    s += d.getYear();

    return (s);
}
