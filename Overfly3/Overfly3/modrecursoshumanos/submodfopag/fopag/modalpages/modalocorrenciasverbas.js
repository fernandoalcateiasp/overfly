/********************************************************************
modalocorrenciasverbas.js

Library javascript para o modalocorrenciasverbas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_bOcorrenciasMode = true;
var glb_InserirOcorrenciasTimer = null;
var glb_txtValorKeyUpTimer = null;


var dsoGrava = new CDatatransport('dsoGrava');
var dsoProcessaFopag = new CDatatransport('dsoProcessaFopag');
var dsoGrid = new CDatatransport('dsoGrid');
var dsoVerbas = new CDatatransport('dsoVerbas');
var dsoFuncionarios = new CDatatransport('dsoFuncionarios');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridDataOcorrencias()
fillGridDataOcorrencias_DSC()
fillGridDataVerbas()
fillGridDataVerbas_DSC()
saveDataInGridOcorrencias()
saveDataInGridOcorrencias_DSC()
saveDataInGridVerbas()
saveDataInGridVerbas_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalocorrenciasverbasBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalocorrenciasverbasDblClick(grid, Row, Col)
js_modalocorrenciasverbasKeyPress(KeyAscii)
js_modalocorrenciasverbas_ValidateEdit()
js_modalocorrenciasverbas_BeforeEdit(grid, row, col)
js_modalocorrenciasverbas_AfterEdit(Row, Col)
js_fg_AfterRowColmodalocorrenciasverbas (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/
/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalocorrenciasverbasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	glb_bFirstFill = true;
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
    fillCmbVerbas();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	chkFuncionarioVerba_onClick(true);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 41;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    // desabilita o botao OK
    btnOK.disabled = true;
	
    // troca label botao Cancelar
    btnCanc.value = 'Cancelar';

    if (glb_aEmpresaData[0] == 10)
        chkPessoalEstagio.title = "Pessoal estoque?";
    else
        chkPessoalEstagio.title = "Pessoal estagio?";

    // Aqui ajusta os controles do divControls

	txtdtFopagInicio.onfocus = selFieldContent;
	txtdtFopagInicio.maxLength = 10;
	txtdtFopagInicio.onkeypress = txtListByKeyPress;

	txtdtFopagFim.onfocus = selFieldContent;
	txtdtFopagFim.maxLength = 10;
	txtdtFopagFim.onkeypress = txtListByKeyPress;

	txtdtReferencia.onfocus = selFieldContent;
	txtdtReferencia.maxLength = 10;
	txtdtReferencia.onkeypress = txtListByKeyPress;

	txtReferencia.onfocus = selFieldContent;
	txtReferencia.maxLength = 30;
	txtReferencia.onkeypress = txtListByKeyPress;

	txtValor.onfocus = selFieldContent;
	txtValor.maxLength = 30;
	txtValor.onkeypress = verifyNumericEnterNotLinked;
	txtValor.onkeyup = txtListByKeyPress;
	txtValor.setAttribute('thePrecision', 11, 1);
	txtValor.setAttribute('theScale', 2, 1);
	txtValor.setAttribute('verifyNumPaste', 1);
	txtValor.setAttribute('minMax', new Array(-999999999, 999999999), 1);

	chkFuncionarioVerba.onclick = chkFuncionarioVerba_onClick;
	chkNomeInvertido.checked = glb_bNomeInvertido;
	chkNomeInvertido.onclick = chkNomeInvertido_onclick;
	
	secText('Fopag ' + glb_nFopagID + ' - Ocorr�ncias', 1);
	selFuncionarios.onchange = CMB_onchange;
	selVerbas.onchange = selVerbas_onchange; 
	adjustLabelsCombos();
	
	adjustElementsInForm([['btnOcorrencias','btn',btnOK.offsetWidth,1,0,-10],
						['btnGravarVerbas','btn',btnOK.offsetWidth,1,13],
						['lblReprocessarVerbasVendas','chkReprocessarVerbasVendas',3,1],
						['lblManterVerbasAlteradas','chkManterVerbasAlteradas',3,1,-7],
						['btnProcessarFopag','btn',btnOK.offsetWidth,1,3],
						['btnImprimirVerbas','btn',btnOK.offsetWidth,1,13],
						['btnExcelVerbas','btn',btnOK.offsetWidth,1,3],
						['btnListarVerbas','btn',btnOK.offsetWidth,1,196]], null, null, true);

	chkReprocessarVerbasVendas.onclick = chkReprocessarVerbasVendas_onclick;
	btnOcorrencias.style.height = btnOK.offsetHeight;
	btnGravarVerbas.style.height = btnOK.offsetHeight;
	btnProcessarFopag.style.height = btnOK.offsetHeight;
	btnImprimirVerbas.style.height = btnOK.offsetHeight;
	btnExcelVerbas.style.height = btnOK.offsetHeight;
	btnListarVerbas.style.height = btnOK.offsetHeight;
	
	
	adjustElementsInForm([['btnVerbas','btn',btnOK.offsetWidth,1,0,-10],
						['btnGravarOcorrencias','btn',btnOK.offsetWidth,1,13],
						['btnDeletarOcorrencias','btn',btnOK.offsetWidth,1,3],
						['btnImprimirOcorrencias','btn',btnOK.offsetWidth,1,13],
						['btnExcelOcorrencias','btn',btnOK.offsetWidth,1,3],
						['lblModo','selModo',12,1,141],
						['btnListarOcorrencias', 'btn', btnOK.offsetWidth, 1,10 ]], null, null, true);

	chkVerbasUsoCotidiano.onclick = fillCmbVerbas;
	btnVerbas.style.height = btnOK.offsetHeight;
	btnGravarOcorrencias.style.height = btnOK.offsetHeight;
	btnDeletarOcorrencias.style.height = btnOK.offsetHeight;
	btnImprimirOcorrencias.style.height = btnOK.offsetHeight;
	btnExcelOcorrencias.style.height = btnOK.offsetHeight;
	selModo.onchange = selModo_onchange;
	btnListarOcorrencias.style.height = btnOK.offsetHeight;

    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
			  btnOK.offsetTop + btnOK.offsetHeight + 2;
        width = txtValor.offsetLeft + txtValor.offsetWidth + ELEM_GAP;    
        height = txtValor.offsetTop + txtValor.offsetHeight;    
    }
    
    with (divBnsOcorrencias.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        //top = divFG.offsetTop + divFG.offsetHeight + 1;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + 1;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = 40;
    }

    with (divBnsVerbas.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + 1;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = 40;
    }
        
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divBnsVerbas.currentStyle.top, 10) + parseInt(divBnsVerbas.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - ELEM_GAP - 4;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + txtValor.offsetTop;
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		value = 'Listar';
		style.visibility = 'hidden';
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

	adjustInterfaceByMode();
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

function chkReprocessarVerbasVendas_onclick()
{
	//if (chkReprocessarVerbasVendas.checked)
		//chkIncluirVerbasVendas.checked = true;
}

function selModo_onchange()
{
	fg.Rows = 1;
	// Pesquisa
	if (selModo.value == 1)
	{
		txtdtFopagInicio.value = glb_dtFopagInicio;
		txtdtFopagFim.value = glb_dtFopagFim;
	}
	else
	{
		txtdtFopagInicio.value = '';
		txtdtFopagFim.value = '';
	}
	
	selVerbas_onchange();
	setupBtnsFromGridState();
	setHeaderGrid();
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtListByKeyPress()
{
    if ( event.keyCode == 13 )
	{
		fg.Rows = 1;
		if (glb_bOcorrenciasMode)
		{
			txtValor.onkeyup = null;
			setNextFocus(this);
			glb_txtValorKeyUpTimer = window.setInterval('settxtValorKeyUp()', 100, 'JavaScript');  
		}
		else
			fillGridDataVerbas(false);
	}
}

function chkFuncionarioVerba_onClick(bCarregamento)
{
	adjustInterfaceByMode();
}

function chkNomeInvertido_onclick()
{
	fillCmbFuncionarios();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    var sMensagemOcorrencias = '';
    var sMensagemVerbas = '';
    var sVerbas = '';
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        ;
    }
    else if (controlID == 'btnOcorrencias')    
    {
		btnOcorrencias_Clicked();
	}
    else if (controlID == 'btnVerbas')    
    {
		fg.Rows = 1;
		glb_bOcorrenciasMode = false;
		adjustInterfaceByMode();
		setHeaderGrid();
	}
	else if (controlID == 'btnGravarOcorrencias')
	{
		saveDataInGridOcorrencias(false);
	}
	else if (controlID == 'btnDeletarOcorrencias')
	{
        var _retMsg = window.top.overflyGen.Confirm('Deletar - tem certeza?');
    
        if ( _retMsg != 1 )
			return null;
	
		saveDataInGridOcorrencias(true);
	}
    else if (controlID == 'btnListarOcorrencias')    
    {
		btnListarOcorrencias_Clicked(false);
    }
    else if (controlID == 'btnImprimirOcorrencias')    
    {
        var dtFopag = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'V_dtFopag' + '\'' + '].value');
		sMensagemOcorrencias = 'Ocorr�ncias Fopag ' + glb_nFopagID + '   ' + dtFopag;

		if (chkFuncionarioVerba.checked)
		{
			if (selFuncionarios.selectedIndex > 0)
				sMensagemOcorrencias += '   Func: ' + selFuncionarios.value + ' ' +
					selFuncionarios.options[selFuncionarios.selectedIndex].innerText;
				
			if (selVerbas.selectedIndex > 0)
				sMensagemOcorrencias += '   Verba: ' + selVerbas.options[selVerbas.selectedIndex].innerText;
		}
		else
		{
			if (selVerbas.selectedIndex > 0)
				sMensagemOcorrencias += '   Verba: ' + selVerbas.options[selVerbas.selectedIndex].innerText;

			if (selFuncionarios.selectedIndex > 0)
				sMensagemOcorrencias += '   Func: ' + selFuncionarios.value + ' ' +
					selFuncionarios.options[selFuncionarios.selectedIndex].innerText;
		}

		if (trimStr(txtdtFopagInicio.value) != '')
			sMensagemOcorrencias += '   In�cio: ' + trimStr(txtdtFopagInicio.value);

		if (trimStr(txtdtFopagFim.value) != '')
			sMensagemOcorrencias += '   Fim: ' + trimStr(txtdtFopagFim.value);

		if (trimStr(txtdtReferencia.value) != '')
			sMensagemOcorrencias += '   Data: ' + trimStr(txtdtReferencia.value);

		if (trimStr(txtReferencia.value) != '')
			sMensagemOcorrencias += '   Ref: ' + trimStr(txtReferencia.value);

		if (trimStr(txtValor.value) != '')
			sMensagemOcorrencias += '   Valor: ' + trimStr(txtValor.value);
			
		imprimeGrid(sMensagemOcorrencias);
    }
    else if (controlID == 'btnExcelOcorrencias')
    {
		btnListarOcorrencias_Clicked(true);
    }
    else if (controlID == 'btnGravarVerbas')
    {
		saveDataInGridVerbas(false);
    }
    else if (controlID == 'btnProcessarFopag')
    {
		processarFopag();
    }
    else if (controlID == 'btnImprimirVerbas')    
    {
        var dtFopag = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'V_dtFopag' + '\'' + '].value');
		sMensagemVerbas = 'Verbas Fopag ' + glb_nFopagID + '   ' + dtFopag;

		if (chkFuncionarioVerba.checked)
		{
			if (selFuncionarios.selectedIndex > 0)
				sMensagemVerbas += '   Func: ' + selFuncionarios.value + ' ' +
					selFuncionarios.options[selFuncionarios.selectedIndex].innerText;
				
			if (selVerbas.selectedIndex > 0)
				sMensagemVerbas += '   Verba: ' + selVerbas.options[selVerbas.selectedIndex].innerText;
		}
		else
		{
			if (selVerbas.selectedIndex > 0)
				sMensagemVerbas += '   Verba: ' + selVerbas.options[selVerbas.selectedIndex].innerText;

			if (selFuncionarios.selectedIndex > 0)
				sMensagemVerbas += '   Func: ' + selFuncionarios.value + ' ' +
					selFuncionarios.options[selFuncionarios.selectedIndex].innerText;
		}

		if (trimStr(txtdtReferencia.value) != '')
			sMensagemVerbas += '   Data: ' + trimStr(txtdtReferencia.value);

		if (trimStr(txtReferencia.value) != '')
			sMensagemVerbas += '   Ref: ' + trimStr(txtReferencia.value);

		if (trimStr(txtValor.value) != '')
			sMensagemVerbas += '   Valor: ' + trimStr(txtValor.value);
			
		imprimeGrid(sMensagemVerbas);
    }
    else if (controlID == 'btnExcelVerbas')    
    {
		fillGridDataVerbas(true);
    }
    else if (controlID == 'btnListarVerbas')    
    {
		fillGridDataVerbas(false);
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

function btnOcorrencias_Clicked()
{
	fg.Rows = 1;
	glb_bOcorrenciasMode = true;
	adjustInterfaceByMode();
	setHeaderGrid();
}

function btnListarOcorrencias_Clicked(bExcel)
{
	// Inclusao
	if (selModo.value == 0)
	{
		lockControlsInModalWin(true);
		glb_InserirOcorrenciasTimer = window.setInterval('InsereOcorrencias()', 10, 'JavaScript');  
	}
	else
		fillGridDataOcorrencias(bExcel);
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    //lockControlsInModalWin(true);
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    showExtFrame(window, true);
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var i;
    var bHasRowsInGrid = (fg.Rows > 1);
    var bOK = false;
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
    
	btnOK.disabled = true;

	if (bHasRowsInGrid)
	{
		for (i=1; i<fg.Rows; i++)
		{
			if (getCellValueByColKey(fg, 'OK', i) != 0)
			{
				bOK = true;
				break;
			}	
		}
	}

	if (glb_bOcorrenciasMode)
	{
		fg.Editable = ((fg.Editable) && (nEstadoID==41));
		btnListarOcorrencias.disabled = ((nEstadoID!=41) && (selModo.value == 0));
		btnGravarOcorrencias.disabled = ((!bOK) || (nEstadoID!=41));
		btnDeletarOcorrencias.disabled = ((!bOK) || (nEstadoID!=41) || (selModo.value == 0));
		btnImprimirOcorrencias.disabled = (!bHasRowsInGrid);
		btnExcelOcorrencias.disabled = ((!bHasRowsInGrid) || (selModo.value == 0));
	}
	else
	{
		fg.Editable = ((fg.Editable) && (nEstadoID==41));
		btnListarVerbas.disabled = false;
		btnGravarVerbas.disabled = ((!bOK) || (nEstadoID!=41));
		btnProcessarFopag.disabled = (nEstadoID!=41);
		btnImprimirVerbas.disabled = (!bHasRowsInGrid);
		btnExcelVerbas.disabled = (!bHasRowsInGrid);
	}
}

function validFileds()
{
	var sError = '';
	var fldError = null;
	var bRetVal = true;
	
	if ((glb_bOcorrenciasMode) && (trimStr(txtdtFopagInicio.value) != '') && (chkDataEx(txtdtFopagInicio.value) == false))
	{
		sError = 'Fopag In�cio';
		fldError = txtdtFopagInicio;
	}
	else if ((glb_bOcorrenciasMode) && (trimStr(txtdtFopagFim.value) != '') && (chkDataEx(txtdtFopagFim.value) == false))
	{
		sError = 'Fopag Fim';
		fldError = txtdtFopagFim;
	}
	else if ((trimStr(txtdtReferencia.value) != '') && (chkDataEx(txtdtReferencia.value) == false))
	{
		sError = 'Data';
		fldError = txtdtReferencia;
	}
	
	if (sError != '')
	{
		lockControlsInModalWin(false);
        if ( window.top.overflyGen.Alert('O campo '+sError+' cont�m uma data inv�lida.') == 0 )
            return null;
        bRetVal = false;
        window.focus();
        fldError.focus();
	}
	
	return bRetVal;
}

function InsereOcorrencias()
{
	if (glb_InserirOcorrenciasTimer != null)
    {
        window.clearInterval(glb_InserirOcorrenciasTimer);
        glb_InserirOcorrenciasTimer = null;
    }

    if (!validFileds())
    {
		lockControlsInModalWin(false);
		return true;
	}

	var TrataDataFopagInicio = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataFopagInicio', 1);
	var TrataDataFopagFim = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataFopagFim', 1);
	var TrataDataReferencia = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataReferencia', 1);
	var TrataReferencia = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataReferencia', 1);
	var TrataValor = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataValor', 1);

	if (selVerbas.selectedIndex <= 0)
	{
        if ( window.top.overflyGen.Alert('Selecione uma verba.') == 0 )
            return null;
        
        lockControlsInModalWin(false);
        return null;
	}
	
    var dTFormat = '';
    var lineData = '';
    var nFuncionarioID = 0;
    var sFuncionario = '';
    var nVerbaID = 0;
    var sVerba = '';
    var PermiteAlteracao = 0;
	var TrataDataFopagFim = 0;
	var TrataDataFopagInicio = 0;
	var TrataDataReferencia = 0;
	var TrataReferencia = 0;
	var TrataValor = 0;

    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	setHeaderGrid();
	fg.Redraw = 0;
	
    fg.ColKey(0) = (chkFuncionarioVerba.checked ? 'FuncionarioID*' : 'VerbaID*');
    fg.ColKey(1) = (chkFuncionarioVerba.checked ? 'Funcionario*' : 'Verba*');
    fg.ColKey(2) = (chkFuncionarioVerba.checked ? 'VerbaID*' : 'FuncionarioID*');
    fg.ColKey(3) = (chkFuncionarioVerba.checked ? 'Verba*' : 'Funcionario*');
    fg.ColKey(4) = 'OK';
    fg.ColKey(5) = 'dtFopagInicio';
    fg.ColKey(6) = 'dtFopagFim';
    fg.ColKey(7) = 'dtReferencia';
    fg.ColKey(8) = 'Referencia';
    fg.ColKey(9) = 'Valor';
    fg.ColKey(10)= 'VerbaOK';
    fg.ColKey(11)= 'Confirmado*';
    fg.ColKey(12)= 'PermiteAlteracao';
    fg.ColKey(13)= 'TrataDataFopagInicio';
	fg.ColKey(14)= 'TrataDataFopagFim';
	fg.ColKey(15)= 'TrataDataReferencia';
	fg.ColKey(16)= 'TrataReferencia';
	fg.ColKey(17)= 'TrataValor';
	fg.ColKey(18)= 'FopOcorrenciaID';
	
	for (i=1; i< selFuncionarios.length; i++)
	{
		nFuncionarioID = (selFuncionarios.selectedIndex > 0 ? selFuncionarios.value : selFuncionarios.options[i].value);
		sFuncionario = selFuncionarios.options[(selFuncionarios.selectedIndex > 0 ? selFuncionarios.selectedIndex : i)].innerText;
		nVerbaID = selVerbas.value;

		// Portugues
		if (glb_aEmpresaData[8] == 246)
			sVerba = selVerbas.options[selVerbas.selectedIndex].getAttribute('Verba', 1);
		else
			sVerba = selVerbas.options[selVerbas.selectedIndex].getAttribute('VerbaTraduzida', 1);

		PermiteAlteracao = selVerbas.options[selVerbas.selectedIndex].getAttribute('PermiteAlteracao', 1);
		TrataDataFopagInicio = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataFopagInicio', 1);
		TrataDataFopagFim = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataFopagFim', 1);
		TrataDataReferencia = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataReferencia', 1);
		TrataReferencia = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataReferencia', 1);
		TrataValor = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataValor', 1);
		VerbaOK = selVerbas.options[selVerbas.selectedIndex].getAttribute('VerbaOK', 1);

		lineData = '';
		lineData += (chkFuncionarioVerba.checked ? nFuncionarioID : nVerbaID) + '\t';
		lineData += (chkFuncionarioVerba.checked ? sFuncionario : sVerba) + '\t';
		lineData += (chkFuncionarioVerba.checked ? nVerbaID : nFuncionarioID) + '\t';
		lineData += (chkFuncionarioVerba.checked ? sVerba : sFuncionario) + '\t';
		lineData += (trimStr(txtValor.value) != '' ? '1' : '0') + '\t';
		lineData += txtdtFopagInicio.value + '\t';
		lineData += txtdtFopagFim.value + '\t';
		lineData += txtdtReferencia.value + '\t';
		lineData += txtReferencia.value + '\t';
		
		if (trimStr(txtValor.value) != '')
		{
			if (DECIMAL_SEP == ',')
				lineData += replaceStr(txtValor.value, '.', ',') + '\t';
			else
				lineData += txtValor.value + '\t';
		}
		else
			lineData += '\t';
		
		lineData += VerbaOK + '\t';
		lineData += '0\t';
		lineData += PermiteAlteracao + '\t';
		lineData += TrataDataFopagInicio + '\t';
		lineData += TrataDataFopagFim + '\t';
		lineData += TrataDataReferencia + '\t';
		lineData += TrataReferencia + '\t';
		lineData += TrataValor + '\t';
		lineData += '0\t';
		
		//addAndFillineInGrid(fg, lineData, fg.Rows);
		fg.AddItem(lineData);
		
		if (selFuncionarios.selectedIndex > 0)
			break;
	}
	
	// Check Box
	fg.ColDataType(4) = 11;
	fg.ColDataType(10) = 11;
	fg.ColDataType(11) = 11;
    putMasksInGrid(fg, ['9999999999','','9999999999','','','99/99/9999','99/99/9999','99/99/9999','','999999999.99','','','','','','',''],
					   ['##########','','##########','','',dTFormat,    dTFormat,    dTFormat,    '','###,###,##0.00','','','','','','','']);
	alignColsInGrid(fg,[0, 2, 9]);
	
    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = false;
    fg.MergeCol(5) = false;
		
	fg.Redraw = 0;

	//gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, (chkFuncionarioVerba.checked ? 'VerbaID*' : 'FuncionarioID*')),'##########','C'],
		//[getColIndexByColKey(fg, 'Valor'),'###,###,###.00','S']]);
	paintCellsSpecialyReadOnly();
	
	setTotalColuns();
	
    for ( i=2; i<fg.Rows; i++ )
    {
		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }

        fg.Cell(6, i, 5, i, 5) = getColor(true, fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataDataFopagInicio')),
			fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao')));

        fg.Cell(6, i, 6, i, 6) = getColor(true, fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataDataFopagFim')),
			fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao')));

        fg.Cell(6, i, 7, i, 7) = getColor(true, fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataDataReferencia')),
			fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao')));

        fg.Cell(6, i, 8, i, 8) = getColor(true, fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataReferencia')),
			fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao')));

        fg.Cell(6, i, 9, i, 9) = getColor(true, fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataValor')),
			fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao')));
	}

	if (fg.Rows > 1)
	{
		fg.Row = 1;
		fg.Col = -1;
	}

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

	fg.FrozenCols = 5;

	//paintCellsSpecialyReadOnly();

	fg.ExplorerBar = 5;
	fg.ColWidth(getColIndexByColKey(fg, 'Valor')) = 1200;
	fg.ColWidth(getColIndexByColKey(fg, 'Referencia')) = 1300;
	fg.ColWidth(getColIndexByColKey(fg, 'dtFopagFim')) = fg.ColWidth(getColIndexByColKey(fg, 'dtFopagInicio'));
	fg.ColWidth(getColIndexByColKey(fg, 'dtReferencia')) = fg.ColWidth(getColIndexByColKey(fg, 'dtFopagInicio'));
	
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
	lockControlsInModalWin(false);
	fg.Redraw = 2;
    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
        
        var aRowCol = getFirstRowColAllowed();
        
        if (aRowCol != null)
        {
			fg.Editable = true;
			fg.Row = aRowCol[0];
			fg.Col = aRowCol[1];
        }
        else
        {
			fg.Editable = false;
			fg.Row = -1;
			fg.Col = -1;
        }
    }
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

function verbaOK()
{
	var retVal = '';
	
	if ((chkVerbaOK0.checked) && (!chkVerbaOK1.checked))
		retVal = '0';
	else if ((!chkVerbaOK0.checked) && (chkVerbaOK1.checked))
		retVal = '1';
	else
		retVal = 'NULL';
	
	return retVal;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridDataOcorrencias(bExcel)
{
	if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }
    
    if (!validFileds())
		return true;

	lockControlsInModalWin(true);
	var empresaData = getCurrEmpresaData();
	var nLinguaLogada = getDicCurrLang();
    var sVerbaOK = verbaOK();
    var sFiltro = '';
    var nTipoFopagID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoFopagID' + '\'' + '].value');

    setConnection(dsoGrid);

    strPars = '?nFopagID=' + escape(glb_nFopagID);
    strPars += '&bExcel=' + bExcel;
    strPars += '&empresaID=' + empresaData[0];
    strPars += '&sEmpresaFantasia=' + empresaData[3];
    strPars += '&nLinguaLogada=' + nLinguaLogada;
    strPars += '&nPaisEmpresaID=' + glb_nPaisEmpresaID;
    strPars += '&glb_aEmpresaData=' + glb_aEmpresaData[8];   
    strPars += '&sVerbaOK=' + sVerbaOK;
    strPars += '&sFiltro=' + sFiltro;
    strPars += '&chkNomeInvertido=' + (chkNomeInvertido.checked ? "1" : "0");
    strPars += '&selVerbas=' + selVerbas.value;
    strPars += '&selVerbasIndex=' + selVerbas.selectedIndex;
    strPars += '&selFuncionarios=' + selFuncionarios.value;
    strPars += '&selFuncionariosIndex=' + selFuncionarios.selectedIndex;
    strPars += '&txtdtFopagInicio=' + (trimStr(putDateInMMDDYYYY2(txtdtFopagInicio.value)));
    strPars += '&txtdtFopagFim=' + (trimStr(putDateInMMDDYYYY2(txtdtFopagFim.value)));
    strPars += '&txtdtReferencia=' + (trimStr(putDateInMMDDYYYY2(txtdtReferencia.value)));
    strPars += '&txtReferencia=' + txtReferencia.value;
    strPars += '&txtValor=' + txtValor.value;
    strPars += '&selModo=' + selModo.value;
    strPars += '&controlID=' + 'ocorrencias';
    strPars += '&chkFuncionarioVerba=' + chkFuncionarioVerba.checked;

		
    if (bExcel)
    {
        lockControlsInModalWin(true);
        var frameReport = document.getElementById("frmReport");
        frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/ocorrenciasverbas.aspx' + strPars;
        lockControlsInModalWin(false);
    }
    else
    {
        fg.Rows = 1;
        dsoGrid.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/ocorrenciasverbas.aspx' + strPars;
		dsoGrid.ondatasetcomplete = fillGridDataOcorrencias_DSC;

		try
		{
			dsoGrid.Refresh();
		}	
		catch(e)
		{
			if ( window.top.overflyGen.Alert('Filtro inv�lido.') == 0 )
				return null;

			lockControlsInModalWin(false);        
			window.focus();
			txtValor.focus();
		}
	}
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridDataOcorrencias_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	setHeaderGrid();
	fg.Redraw = 0;

    fillGridMask(fg,dsoGrid,[(chkFuncionarioVerba.checked ? 'FuncionarioID*' : 'VerbaID*'),
				   (chkFuncionarioVerba.checked ? 'Funcionario*' : 'Verba*'),
				   (chkFuncionarioVerba.checked ? 'VerbaID*' : 'FuncionarioID*'),
				   (chkFuncionarioVerba.checked ? 'Verba*' : 'Funcionario*'),
				   'OK',
				   'dtFopagInicio',
				   'dtFopagFim',
				   'dtReferencia',
				   'Referencia',
				   'Valor',
				   'VerbaOK',
				   'Confirmado*',
				   'Consistencia*',
				   'PermiteAlteracao',
				   'TrataDataFopagInicio',
				   'TrataDataFopagFim',
				   'TrataDataReferencia',
				   'TrataReferencia',
				   'TrataValor',
				   'FopOcorrenciaID',
				   'FuncNome'],
					['9999999999','','9999999999','','','99/99/9999','99/99/9999','99/99/9999','','#999999999.99','','','','','','','','','','',''],
					['##########','','##########','','',dTFormat    ,dTFormat    ,dTFormat    ,'','###,###,##0.00','','','','','','','','','','','']);

	fg.Redraw = 0;
	paintCellsSpecialyReadOnly();
	setTotalColuns();

    for ( i=2; i<fg.Rows; i++ )
    {
		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }

		// paintBackgroundOfACell(fg, i, nColIsReadOnly, 0XDCDCDC);
        fg.Cell(6, i, 5, i, 5) = getColor(true, fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataDataFopagInicio')),
			fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao')));
			
        fg.Cell(6, i, 6, i, 6) = getColor(true, fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataDataFopagFim')),
			fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao')));
			
        fg.Cell(6, i, 7, i, 7) = getColor(true, fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataDataReferencia')),
			fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao')));
			
        fg.Cell(6, i, 8, i, 8) = getColor(true, fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataReferencia')),
			fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao')));
			
        fg.Cell(6, i, 9, i, 9) = getColor(true, fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataValor')),
			fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao')));
	}

	if (fg.Rows > 1)
	{
		fg.Row = 1;
		fg.Col = 0;
		fg.Col = -1;
	}
    
    alignColsInGrid(fg,[0, 2, 9]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
	fg.FrozenCols = 5;

	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
        
        var aRowCol = getFirstRowColAllowed();
        
        if (aRowCol != null)
        {
			fg.Editable = true;
			fg.Row = aRowCol[0];
			fg.Col = aRowCol[1];
        }
        else
        {
			fg.Editable = false;
			fg.Row = -1;
			fg.Col = -1;
        }
    }

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = false;
    fg.MergeCol(5) = false;
    fg.Redraw = 2;

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

function getColor(bFromGrid, nConsistencia, bPermiteAlteracao)
{
	if (glb_bOcorrenciasMode)
		bPermiteAlteracao = true;

	// Default branco
	var nRetVal = 0XFFFFFF;

	// Grafite
	if ((nConsistencia == 0) || (bPermiteAlteracao == 0))
	{
		if (glb_bOcorrenciasMode)
			nRetVal = 0X919191;
		else
			nRetVal = 0XDCDCDC;
	}
	else if (glb_bOcorrenciasMode)
	{
		// Cinza
		if (nConsistencia == 1)
			nRetVal = 0XDCDCDC;
		// Amarelo
		else if (nConsistencia == 3)
		{
			if (bFromGrid)
				nRetVal = 0XE1FFFF;
			else
				nRetVal = 0XFFFFE1;
		}
	}
		
	return nRetVal;
}

function fillGridDataVerbas(bExcel)
{
	if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    if (!validFileds())
		return true;
		
	var sVerbaOK = verbaOK();
	
    if (sVerbaOK == 'NULL')
		sVerbaOK = '';
	else
		sVerbaOK = ' AND a.VerbaOK=' + sVerbaOK;
    
    lockControlsInModalWin(true);
    var empresaData = getCurrEmpresaData();
    var nLinguaLogada = getDicCurrLang();
    var sFiltro = '';

    setConnection(dsoGrid);

    strPars = '?nFopagID=' + escape(glb_nFopagID);
    strPars += '&bExcel=' + bExcel;
    strPars += '&empresaID=' + empresaData[0];
    strPars += '&sEmpresaFantasia=' + empresaData[3];
    strPars += '&nLinguaLogada=' + nLinguaLogada;
    strPars += '&nPaisEmpresaID=' + glb_nPaisEmpresaID;
    strPars += '&glb_aEmpresaData=' + glb_aEmpresaData[8];
    strPars += '&sVerbaOK=' + sVerbaOK;
    strPars += '&sFiltro=' + sFiltro;
    strPars += '&chkNomeInvertido=' + (chkNomeInvertido.checked ? "1" : "0");
    strPars += '&selVerbasIndex=' + selVerbas.selectedIndex;
    strPars += '&selVerbas=' + selVerbas.value;
    strPars += '&selFuncionariosIndex=' + selFuncionarios.selectedIndex;
    strPars += '&selFuncionarios=' + selFuncionarios.value;
    strPars += '&txtdtReferencia=' + (trimStr(putDateInMMDDYYYY2(txtdtReferencia.value)));
    strPars += '&txtReferencia=' + txtReferencia.value;
    strPars += '&txtValor=' + txtValor.value;
    strPars += '&controlID=' + 'verbas';
    strPars += '&chkFuncionarioVerba=' + chkFuncionarioVerba.checked;
    strPars += '&chkFuncionariosDiretos=' + chkFuncionariosDiretos.checked;
    strPars += '&chkFuncionariosIndiretos=' + chkFuncionariosIndiretos.checked;
    strPars += '&chkPessoalComercial=' + chkPessoalComercial.checked;
    strPars += '&chkPessoalAdministrativo=' + chkPessoalAdministrativo.checked;
    strPars += '&chkPessoalEstagio=' + chkPessoalEstagio.checked;
    strPars += '&chkSohTotalVerbas=' + chkSohTotalVerbas.checked;
    strPars += '&sVerbaOK2=' + verbaOK();
    
    if (bExcel)
    {
        lockControlsInModalWin(true);
        var frameReport = document.getElementById("frmReport");
        frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/ocorrenciasverbas.aspx' + strPars;
        lockControlsInModalWin(false);
    }
    else
    {
        fg.Rows = 1;
        dsoGrid.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/ocorrenciasverbas.aspx' + strPars;
		dsoGrid.ondatasetcomplete = fillGridDataVerbas_DSC;
	    
		try
		{
			dsoGrid.Refresh();
		}	
		catch(e)
		{
			if ( window.top.overflyGen.Alert('Filtro inv�lido.') == 0 )
				return null;

			lockControlsInModalWin(false);        
			window.focus();
			txtValor.focus();		
		}
	}
}

function fillGridDataVerbas_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	setHeaderGrid();
	fg.Redraw = 0;
	
    fillGridMask(fg,dsoGrid,[(chkFuncionarioVerba.checked ? 'FuncionarioID*' : 'VerbaID*'),
				   (chkFuncionarioVerba.checked ? 'Funcionario*' : 'Verba*'),
				   'Total*',
				   (chkFuncionarioVerba.checked ? 'VerbaID*' : 'FuncionarioID*'),
				   (chkFuncionarioVerba.checked ? 'Verba*' : 'Funcionario*'),
				   'OK',
				   'dtReferencia*',
				   'Referencia',
				   'Valor',
				   'VerbaOK*',
				   'AlteracaoManual*',
				   'PermiteAlteracao',
				   'TrataDataReferencia',
				   'TrataReferencia',
				   'TrataValor',
				   'FopVerbaID',
				   'FuncNome'],
					['9999999999','','999999999.99','9999999999','','','99/99/9999','','#999999999.99','','','','','','','',''],
					['##########','','###,###,##0.00','##########','','',dTFormat    ,'','###,###,##0.00','','','','','','','','']);

	fg.Redraw = 0;

	paintCellsSpecialyReadOnly();
	//gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, (chkFuncionarioVerba.checked ? 'VerbaID*' : 'FuncionarioID*')),'##########','C'],
		//[getColIndexByColKey(fg, 'Valor'),'###,###,###.00','S']]);
	setTotalColuns();
	
    for ( i=2; i<fg.Rows; i++ )
    {
		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'Total*')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'Total*'), i, getColIndexByColKey(fg, 'Total*'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }

		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }
        
        fg.Cell(6, i, 7, i, 7) = getColor(true, fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataReferencia')),
			fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao')));
        fg.Cell(6, i, 8, i, 8) = getColor(true, fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataValor')),
			fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao')));
	}
	
    alignColsInGrid(fg,[0, 2, 3, 8]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
	fg.FrozenCols = 6;

	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
        
        var aRowCol = getFirstRowColAllowed();
        
        if (aRowCol != null)
        {
			fg.Editable = true;
			fg.Row = aRowCol[0];
			fg.Col = aRowCol[1];
        }
        else
        {
			fg.Editable = false;
			fg.Row = -1;
			fg.Col = -1;
        }
    }

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;
    fg.MergeCol(5) = false;
    fg.Redraw = 2;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGridOcorrencias(bDelete)
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;
	var TrataDataFopagInicio;
	var TrataDataFopagFim;
	var TrataDataReferencia;
	var TrataReferencia;
	var TrataValor;
	var nTipoTratamento = 0;
	var sMsgErro = '';
	var nLineError = 0;

	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;

	if (!bDelete)
	{
		// Valida se os campos obrigatorios foram preenchidos
		for (i=2; i<fg.Rows; i++)
		{
			if (getCellValueByColKey(fg, 'OK', i) != 0)
			{
				TrataDataFopagInicio = fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataDataFopagInicio'));
				TrataDataFopagFim = fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataDataFopagFim'));
				TrataDataReferencia = fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataDataReferencia'));
				TrataReferencia = fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataReferencia'));
				TrataValor = fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataValor'));
				nLineError = i;
				nTipoTratamento = 0;
				sMsgErro = '';
				
				if ((TrataDataFopagInicio == 3) && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'dtFopagInicio'))==0))
					sMsgErro = 'Fopag In�cio';
				
				if ((TrataDataFopagFim == 3) && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'dtFopagFim'))==0))
					sMsgErro += (sMsgErro != '' ? ', ' : '') + 'Fopag Fim';
				
				if ((TrataDataReferencia == 3) && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'dtReferencia'))==0))
				{
					// Permite nao preencher data de referencia no caso de vale lista com referencia = 'Vale lista'
					if (!((selVerbas.value == 220) && (fg.TextMatrix(i, getColIndexByColKey(fg, 'Referencia'))=='Vale lista')))
						sMsgErro += (sMsgErro != '' ? ', ' : '') + 'Data Ref';
				}
				
				if ((TrataReferencia == 3) && (trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'Referencia')))==''))
					sMsgErro += (sMsgErro != '' ? ', ' : '') + 'Refer�ncia';
				
				if ((TrataValor == 3) && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor'))==0))
				{
					// Permite nao preencher valor no caso de vale lista com referencia = 'Vale lista'
					if (!((selVerbas.value == 220) && (fg.TextMatrix(i, getColIndexByColKey(fg, 'Referencia'))=='Vale lista')))
						sMsgErro += (sMsgErro != '' ? ', ' : '') + 'Valor';
				}
					
				if (sMsgErro != '')
					break;

				if ((fg.ValueMatrix(i, getColIndexByColKey(fg, 'dtFopagInicio')) != 0) &&
					(fg.ValueMatrix(i, getColIndexByColKey(fg, 'dtFopagFim')) != 0))
				{
					if ( putDateInYYYYMMDD(fg.TextMatrix(i, getColIndexByColKey(fg, 'dtFopagInicio'))) > putDateInYYYYMMDD(fg.TextMatrix(i, getColIndexByColKey(fg, 'dtFopagFim'))) )
					{
						nTipoTratamento = 1;
						sMsgErro = 'O campo Fopag In�cio deve ser menor ou igual a Fopag Fim';
						break;
					}
				}
			}
		}
	}

	if (sMsgErro != '')
	{
		fg.Row = nLineError;
		
		if (nTipoTratamento == 0)
		{
			if ( window.top.overflyGen.Alert('O(s) campo(s) ' + sMsgErro + ' deve(m) ser preenchido(s).') == 0 )
				return null;
		}
		else if (nTipoTratamento == 1)
		{
			if ( window.top.overflyGen.Alert(sMsgErro) == 0 )
				return null;
		}
		
		return null;
	}

	lockControlsInModalWin(true);
	
    for (i=1; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'OK', i) != 0)
		{
			nBytesAcum=strPars.length;
			
			if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
			{
				nBytesAcum = 0;
				if (strPars != '')
				{
					glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
					strPars = '';
					nDataLen = 0;
				}
				
				strPars += '?bDelete=' + escape(bDelete ? 1 : 0);
				strPars += '&FopagID=' + escape(glb_nFopagID);
			}

			strPars += '&FopOcorrenciaID=' + escape(getCellValueByColKey(fg, 'FopOcorrenciaID', i));
			strPars += '&FuncionarioID=' + escape(getCellValueByColKey(fg, 'FuncionarioID*', i));
			strPars += '&VerbaID=' + escape(getCellValueByColKey(fg, 'VerbaID*', i));
			
			if (fg.ValueMatrix(i, getColIndexByColKey(fg,'dtFopagInicio')) == 0)
				strPars += '&dtFopagInicio=NULL';
			else 
				strPars += '&dtFopagInicio=' + escape('\'' + normalizeDate_DateTime(getCellValueByColKey(fg, 'dtFopagInicio', i), true) + '\'');
				
			if (fg.ValueMatrix(i, getColIndexByColKey(fg,'dtFopagFim')) == 0)
				strPars += '&dtFopagFim=NULL';
			else
				strPars += '&dtFopagFim=' + escape('\'' + normalizeDate_DateTime(getCellValueByColKey(fg, 'dtFopagFim', i), true) + '\'');
				
			if (fg.ValueMatrix(i, getColIndexByColKey(fg,'dtReferencia')) == 0)
				strPars += '&dtReferencia=NULL';
			else
				strPars += '&dtReferencia=' + escape('\'' + normalizeDate_DateTime(getCellValueByColKey(fg, 'dtReferencia', i), true) + '\'');
			
			if (trimStr(fg.TextMatrix(i, getColIndexByColKey(fg,'Referencia'))) == '')
				strPars += '&Referencia=NULL';
			else
				strPars += '&Referencia=' + escape('\'' + getCellValueByColKey(fg, 'Referencia', i) + '\'');

			if (fg.ValueMatrix(i, getColIndexByColKey(fg,'Valor')) == 0)
				strPars += '&Valor=NULL';
			else
				strPars += '&Valor=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg,'Valor')));
			
			if (fg.ValueMatrix(i, getColIndexByColKey(fg,'VerbaOK')) != 0)
				strPars += '&VerbaOK=' + escape(1);
			else
				strPars += '&VerbaOK=' + escape(0);
				
			nDataLen++;				
		}	
	}
	
	if (nDataLen>0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);

	sendDataToServerOcorrencias();
}

function sendDataToServerOcorrencias()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
		    dsoGrava.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/alteraocorrencias.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServerOcorrencias_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			
			// Inclusao
			if (selModo.value == 0)
			{
				fg.Rows = 1;
				lockControlsInModalWin(false);
			}
			else
				glb_OcorrenciasTimerInt = window.setInterval('fillGridDataOcorrencias(false)', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_OcorrenciasTimerInt = window.setInterval('fillGridDataOcorrencias(false)', 10, 'JavaScript');  
	}
}

function sendDataToServerOcorrencias_DSC()
{
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServerOcorrencias()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}


/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGridVerbas(bDelete)
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;
	var nLineError = 0;
	var sMsgErro = '';


/*
	O Dudu, em 28/03/2006, pediu para nao criticar preenchimento de campo obrigatorio no caso de verbas
	// Valida se os campos obrigatorios foram preenchidos
	for (i=2; i<fg.Rows; i++)
	{
		TrataDataReferencia = fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataDataReferencia'));
		TrataReferencia = fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataReferencia'));
		TrataValor = fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataValor'));
		nLineError = i;

		if ((TrataDataReferencia == 3) && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'dtReferencia*'))==0))
		{
			sMsgErro = 'Data Ref';
			break;
		}
		else if ((TrataReferencia == 3) && (trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'Referencia*')))==''))
		{
			sMsgErro = 'Refer�ncia';
			break;
		}
		else if ((TrataValor == 3) && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor'))==0))
		{
			sMsgErro = 'Valor';
			break;
		}
	}

	if (sMsgErro != '')
	{
		fg.Row = nLineError;
		if ( window.top.overflyGen.Alert('O campo ' + sMsgErro + ' deve ser preenchido.') == 0 )
			return null;

		return null;
	}
*/
    lockControlsInModalWin(true);
    
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;

    for (i=1; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'OK', i) != 0)
		{
			nBytesAcum=strPars.length;
			
			if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
			{
				nBytesAcum = 0;
				if (strPars != '')
				{
					glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
					strPars = '';
					nDataLen = 0;
				}
				
				strPars += '?FopagID=' + escape(glb_nFopagID);
			}

			strPars += '&FopVerbaID=' + escape(getCellValueByColKey(fg, 'FopVerbaID', i));

			if (trimStr(fg.TextMatrix(i, getColIndexByColKey(fg,'Referencia'))) == '')
				strPars += '&Referencia=NULL';
			else
				strPars += '&Referencia=' + escape('\'' + fg.TextMatrix(i, getColIndexByColKey(fg,'Referencia')) + '\'');

			if (fg.ValueMatrix(i, getColIndexByColKey(fg,'Valor')) == 0)
				strPars += '&Valor=NULL';
			else
				strPars += '&Valor=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg,'Valor')));
			nDataLen++;				
		}	
	}
	
	if (nDataLen>0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);

	sendDataToServerVerbas();
}

function sendDataToServerVerbas()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
		    dsoGrava.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/alteraverbas.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServerVerbas_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_OcorrenciasTimerInt = window.setInterval('fillGridDataVerbas(false)', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_OcorrenciasTimerInt = window.setInterval('fillGridDataVerbas(false)', 10, 'JavaScript');  
	}
}

function sendDataToServerVerbas_DSC()
{
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServerVerbas()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

function processarFopag()
{
    var bVerbasNormais = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'IncluiVerbasNormais' + '\'' + '].value');
    var bVerbasVendas = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'IncluiVerbasVendas' + '\'' + '].value');

	if (chkReprocessarVerbasVendas.checked)
	{
        var _retMsg = window.top.overflyGen.Confirm('Deseja processar as verbas de vendas, inadimpl�ncia e obsolesc�ncia?\n' +
			'Isto pode demorar alguns minutos.');
    
        if ( _retMsg != 1 )
			return null;
	}

	else if ((!bVerbasNormais) && (!bVerbasVendas))
	{
        if ( window.top.overflyGen.Alert('Habilite Verbas Normais ou Verbas de Vendas\npara poder processar a Fopag') == 0 )
            return null;
            
        return null;
	}

	var strPars = '?FopagID=' + escape(glb_nFopagID) +
		'&ReprocessaVerbasVendas=' + escape(chkReprocessarVerbasVendas.checked ? 1 : 0) +
		'&MantemVerbasAlteradas=' + escape(chkManterVerbasAlteradas.checked ? 1 : 0) +
	    '&ColaboradorID=' + escape(selFuncionarios.value) +
		'&nTipoProcessamentoID=' + escape(1);

	lockControlsInModalWin(true);

	dsoProcessaFopag.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/fopag/serverside/processarfopag.aspx' + strPars;
	dsoProcessaFopag.ondatasetcomplete = dsoProcessaFopag_DSC;
	dsoProcessaFopag.refresh();
}

function dsoProcessaFopag_DSC()
{
	lockControlsInModalWin(false);
	
	if (dsoProcessaFopag.recordset.fields.count > 0)
	{
		if ( !((dsoProcessaFopag.recordset.BOF)&&(dsoProcessaFopag.recordset.EOF)) )
		{
		    if ((dsoProcessaFopag.recordset['Resultado'].value != null) &&
				(dsoProcessaFopag.recordset['Resultado'].value != ''))
			{
		        if ( window.top.overflyGen.Alert(dsoProcessaFopag.recordset['Resultado'].value) == 0 )
					return null;
					
				selModo.selectedIndex = 2;
				btnOcorrencias_Clicked();
				selModo_onchange();
				btnListarOcorrencias_Clicked(false);
				return null;
			}	
		}
	}
	
	fillGridDataVerbas(false);
}

function fillCmbVerbas()
{
	lockControlsInModalWin(true);
	fg.Rows = 1;
	var strSQL = '';

    strSQL = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName, SPACE(0) AS Verba, SPACE(0) AS VerbaTraduzida, 0 AS VerbaOK, ' +
			 '0 AS PermiteAlteracao, 0 AS TrataDataFopagFim, 0 AS TrataDataFopagInicio, 0 AS TrataDataReferencia, 0 AS TrataReferencia, ' +
			 '0 AS TrataValor ' +
			 'UNION ALL ' +
			 'SELECT 1 AS Indice, a.VerbaID AS fldID, (CONVERT(VARCHAR(10), a.VerbaID) + SPACE(1) + dbo.fn_Tradutor(a.Verba, 246, ' + glb_aEmpresaData[8] + ', NULL)) AS fldName, ' +
			 'a.Verba, dbo.fn_Tradutor(a.Verba, 246, ' + glb_aEmpresaData[8] + ', NULL) AS VerbaTraduzida, ' +
			 'b.VerbaOK, b.PermiteAlteracao, ISNULL(a.TrataDataFopagFim, -1) AS TrataDataFopagFim, ISNULL(a.TrataDataFopagInicio, -1) AS TrataDataFopagInicio, ' +
			 'ISNULL(a.TrataDataReferencia,-1) AS TrataDataReferencia, ISNULL(a.TrataReferencia,-1) AS TrataReferencia, ' +
			 'ISNULL(a.TrataValor,-1) AS TrataValor ' +
			 'FROM VerbasFopag a WITH(NOLOCK), VerbasFopag_Paises b WITH(NOLOCK) ' +
			 'WHERE a.EstadoID=2 AND a.VerbaID=b.VerbaID AND b.PaisID=' + glb_nPaisEmpresaID;
			 
	if (chkVerbasUsoCotidiano.checked)			 
		strSQL += ' AND (EhAutomatica = 0 OR EhAutomatica IS NULL) ';

	strSQL += 'ORDER BY Indice, fldName';

    setConnection(dsoVerbas);
	dsoVerbas.SQL = strSQL;
    dsoVerbas.ondatasetcomplete = fillCmbVerbas_DSC;
	dsoVerbas.Refresh();
}

function fillCmbVerbas_DSC()
{
    clearComboEx(['selVerbas']);
    
    while (! dsoVerbas.recordset.EOF )
    {
        optionStr = dsoVerbas.recordset['fldName'].value;
        optionValue = dsoVerbas.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        
        oOption.setAttribute('Verba', dsoVerbas.recordset['Verba'].value, 1);
        oOption.setAttribute('VerbaTraduzida', dsoVerbas.recordset['VerbaTraduzida'].value, 1);
        oOption.setAttribute('VerbaOK', dsoVerbas.recordset['VerbaOK'].value, 1);
        oOption.setAttribute('PermiteAlteracao', dsoVerbas.recordset['PermiteAlteracao'].value, 1);
        oOption.setAttribute('TrataDataFopagFim', dsoVerbas.recordset['TrataDataFopagFim'].value, 1);
        oOption.setAttribute('TrataDataFopagInicio', dsoVerbas.recordset['TrataDataFopagInicio'].value, 1);
        oOption.setAttribute('TrataDataReferencia', dsoVerbas.recordset['TrataDataReferencia'].value, 1);
        oOption.setAttribute('TrataReferencia', dsoVerbas.recordset['TrataReferencia'].value, 1);
        oOption.setAttribute('TrataValor', dsoVerbas.recordset['TrataValor'].value, 1);
        
        selVerbas.add(oOption);
        dsoVerbas.recordset.MoveNext();
    }
    
	if (glb_bFirstFill)
	{
		fillCmbFuncionarios();
	}
	else
	{
		lockControlsInModalWin(false);
		window.focus();
		selVerbas.focus();
	}
}

function fillCmbFuncionarios()
{
	lockControlsInModalWin(true);
	var strSQL = '';
	var sFiltro = '';

	if (chkAplicarFiltroFuncionario.checked)
	{
		if (!(chkFuncionariosDiretos.checked && chkFuncionariosIndiretos.checked))
		{
			if (chkFuncionariosDiretos.checked)
				sFiltro += ' AND dbo.fn_Fopag_Funcionario(' + glb_nFopagID + ', b.PessoaID, 1) = 1 ';
			else if (chkFuncionariosIndiretos.checked)
				sFiltro += ' AND dbo.fn_Fopag_Funcionario(' + glb_nFopagID + ', b.PessoaID, 1) = 0 ';
		}

		if (!(chkPessoalComercial.checked && chkPessoalAdministrativo.checked && chkPessoalEstagio.checked))
		{
			if (chkPessoalComercial.checked)
				sFiltro += ' AND dbo.fn_Fopag_Funcionario(' + glb_nFopagID + ', b.PessoaID, 3) IN (1, 2) ';
			else if (chkPessoalAdministrativo.checked)
				sFiltro += ' AND dbo.fn_Fopag_Funcionario(' + glb_nFopagID + ', b.PessoaID, 3) = 0 ';
            else if (chkPessoalEstagio.checked) 
			{
			    if (glb_aEmpresaData[0] == 10)
			        sFiltro += ' AND dbo.fn_Fopag_Funcionario(a.FopagID, a.FuncionarioID, 2) IN (639,641,677,678,696,753) ';
			    else
			        sFiltro += ' AND dbo.fn_Fopag_Funcionario(a.FopagID, a.FuncionarioID, 2) = 759 ';
			}
		}
	}		 
/*
    strSQL = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +
			 'UNION ALL ' +
			 'SELECT 1 AS Indice, b.PessoaID AS fldID, ' +
			 'dbo.fn_Pessoa_Fantasia(b.PessoaID, ' + (chkNomeInvertido.checked ? '1' : '0') + ') AS fldName ' +
			 'FROM RelacoesPessoas a, Pessoas b ' +
			 'WHERE a.ObjetoID=' + glb_aEmpresaData[0] + ' AND a.TipoRelacaoID=31 AND a.EstadoID=2 AND ' +
			 'a.SujeitoID=b.PessoaID ' + sFiltro +
			 'ORDER BY Indice, fldName';
*/

    strSQL = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +
			 'UNION ALL ' +
			 'SELECT 1 AS Indice, a.FuncionarioID AS fldID, ' +
			 'dbo.fn_Pessoa_Fantasia(a.FuncionarioID, ' + (chkNomeInvertido.checked ? '1' : '0') + ') AS fldName ' +
			 'FROM Fopag_Funcionarios a WITH(NOLOCK) ' +
			 'WHERE a.FopagID=' + glb_nFopagID + ' ' + sFiltro +
			 'ORDER BY Indice, fldName';

    setConnection(dsoFuncionarios);
	dsoFuncionarios.SQL = strSQL;
    dsoFuncionarios.ondatasetcomplete = fillCmbFuncionarios_DSC;
	dsoFuncionarios.Refresh();
}

function fillCmbFuncionarios_DSC()
{
    clearComboEx(['selFuncionarios']);
    
    while (! dsoFuncionarios.recordset.EOF )
    {
        optionStr = dsoFuncionarios.recordset['fldName'].value;
        optionValue = dsoFuncionarios.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selFuncionarios.add(oOption);
        dsoFuncionarios.recordset.MoveNext();
    }
    
	if (glb_bFirstFill)
	{
		glb_bFirstFill = false;
		showExtFrame(window, true);
		lockControlsInModalWin(false);
		window.focus();
		selModo_onchange();

		if ( !txtdtReferencia.disabled )
			txtdtReferencia.focus();
	}
	else
	{
		lockControlsInModalWin(false);
		window.focus();
		selVerbas.focus();
	}
}

function selVerbas_onchange()
{
	var bPermiteAlteracao = selVerbas.options[selVerbas.selectedIndex].getAttribute('PermiteAlteracao',1);
	
	if (glb_bOcorrenciasMode)
		bPermiteAlteracao = true;
	
	// Inclusao
	if ((glb_bOcorrenciasMode) && (selModo.value == 0) && (selVerbas.selectedIndex > 0))
	{
		txtdtFopagInicio.style.backgroundColor = getColor(false, selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataFopagInicio',1),
			bPermiteAlteracao);
			
		txtdtFopagFim.style.backgroundColor = getColor(false, selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataFopagFim',1),
			bPermiteAlteracao);

		txtdtReferencia.style.backgroundColor = getColor(false, selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataReferencia',1),
			bPermiteAlteracao);
			
		txtReferencia.style.backgroundColor = getColor(false, selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataReferencia',1),
			bPermiteAlteracao);
			
		txtValor.style.backgroundColor = getColor(false, selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataValor',1),
			bPermiteAlteracao);

		txtdtFopagInicio.disabled = (selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataFopagInicio',1) == 0) ||
			(bPermiteAlteracao == 0);

		txtdtFopagFim.disabled = (selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataFopagFim',1) == 0) ||
			(bPermiteAlteracao == 0);

		txtdtReferencia.disabled = (selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataReferencia',1) == 0) ||
			(bPermiteAlteracao == 0);
			
		txtReferencia.disabled = (selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataReferencia',1) == 0) ||
			(bPermiteAlteracao == 0);
			
		txtValor.disabled = (selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataValor',1) == 0) ||
			(bPermiteAlteracao == 0);

		if ((bPermiteAlteracao == 0) || (selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataFopagInicio',1) == 0))
			txtdtFopagInicio.value = '';
			
		if ((bPermiteAlteracao == 0) || (selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataFopagFim',1) == 0))
			txtdtFopagFim.value = '';
		
		if ((bPermiteAlteracao == 0) || (selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataReferencia',1) == 0))
			txtdtReferencia.value = '';
		
		if ((bPermiteAlteracao == 0) || (selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataReferencia',1) == 0))
			txtReferencia.value = '';
		
		if ((bPermiteAlteracao == 0) || (selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataValor',1) == 0))
			txtValor.value = '';
	}
	else
	{
		txtdtFopagInicio.style.backgroundColor = 'transparent';	
		txtdtFopagFim.style.backgroundColor = 'transparent';	
		txtdtReferencia.style.backgroundColor = 'transparent';	
		txtReferencia.style.backgroundColor = 'transparent';	
		txtValor.style.backgroundColor = 'transparent';	

		txtdtFopagInicio.disabled = false;
		txtdtFopagFim.disabled = false;
		txtdtReferencia.disabled = false;
		txtReferencia.disabled = false;
		txtValor.disabled = false;
	}

	adjustLabelsCombos();
	fg.Rows = 1;
	// ajusta estado dos botoes
	setupBtnsFromGridState();
}

function CMB_onchange()
{
	adjustLabelsCombos();
	fg.Rows = 1;
	// ajusta estado dos botoes
	setupBtnsFromGridState();
}

function adjustLabelsCombos()
{
    // setLabelOfControl(lblFuncionarios, selFuncionarios.value);
    ;
}

function verificaData()
{
	var sData = trimStr(txtdtReferencia.value);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(txtdtReferencia.value);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        window.focus();
        
        if ( sData != '' )    
			txtdtReferencia.focus();
		
		return false;
	}
	return true;
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGridOcorrencias_DSC()
{
    lockControlsInModalWin(false);

	glb_OcorrenciasTimerInt = window.setInterval('fillGridDataOcorrencias(false)', 10, 'JavaScript');  
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalocorrenciasverbasBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalocorrenciasverbasDblClick(grid, Row, Col)
{
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
	
	if (nEstadoID != 41)
		return true;

	if (!fg.Editable)
		return true;

    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;
    
    // trap so colunas editaveis
    if ( getColIndexByColKey(grid, 'OK') != Col)
        return true;
        
    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);
    
    glb_PassedOneInDblClick = true;
    
    // limpa coluna se tem um check box checado
    for ( i=2; i<grid.Rows; i++ )
    {
        if ( grid.ValueMatrix(i, Col) != 0 )
        {
            bFill = false;
            break;
        }
    }
    
    for ( i=2; i<grid.Rows; i++ )
    {
		if ((!glb_bOcorrenciasMode) && (!fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao'))))
			continue;

        if ( bFill )
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    setTotalColuns();
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalocorrenciasverbasKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalocorrenciasverbas_ValidateEdit()
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalocorrenciasverbas_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalocorrenciasverbas_AfterEdit(Row, Col)
{
	var currRow = 0;
	var currTopRow = 0;
	
	var bCheckOK = 0;
    if ((fg.Editable) && (fg.EditText != ''))
    {
		if (glb_bOcorrenciasMode)
		{
			if (Col == getColIndexByColKey(fg, 'OK'))
				bCheckOK = 0;
			else if ((Col == getColIndexByColKey(fg, 'dtFopagInicio')) ||
				(Col == getColIndexByColKey(fg, 'dtFopagFim')) ||
				(Col == getColIndexByColKey(fg, 'dtReferencia')) )
			{
				if (trimStr(fg.EditText) == '/  /')
					fg.TextMatrix(Row, Col) = '';
				else if (chkDataEx(fg.EditText) == false)
				{
					if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
						return null;

					fg.TextMatrix(Row, Col) = '';
				}
				bCheckOK = 1;
			}
			else if (Col == getColIndexByColKey(fg, 'Referencia'))
			{
				bCheckOK = 1;
			}
			else if (Col == getColIndexByColKey(fg, 'Valor'))
			{
				fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
				bCheckOK = 1;
			}
			else if (Col == getColIndexByColKey(fg, 'VerbaOK'))
			{
				bCheckOK = 1;
			}
			else if (Col == getColIndexByColKey(fg, 'Confirmado*'))
			{
				if (fg.ValueMatrix(Row, Col) == 0)
					fg.TextMatrix(Row, Col) = 1;
				else
					fg.TextMatrix(Row, Col) = 0;
					
				bCheckOK = 0;
			}

			if (bCheckOK)
				fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = '1';
        }
        else
        {
			if (Col == getColIndexByColKey(fg, 'OK'))
			{
				if (fg.ValueMatrix(Row, getColIndexByColKey(fg, 'PermiteAlteracao')) == 0)
				{
					if (fg.ValueMatrix(Row, Col) == 0)
						fg.TextMatrix(Row, Col) = 1;
					else
						fg.TextMatrix(Row, Col) = 0;
				}
			}
			else if (Col == getColIndexByColKey(fg, 'dtReferencia*'))
			{
				if (trimStr(fg.EditText) == '/  /')
					fg.TextMatrix(Row, Col) = '';
				else if (chkDataEx(fg.EditText) == false)
				{
					if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
						return null;

					fg.TextMatrix(Row, Col) = '';
				}
				fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = '1';
			}
			else if (Col == getColIndexByColKey(fg, 'Referencia'))
			{
				fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = '1';
			}
			else if (Col == getColIndexByColKey(fg, 'Valor'))
			{
				fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
				fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = '1';
			}
			else if (Col == getColIndexByColKey(fg, 'AlteracaoManual*'))
			{
				if (fg.ValueMatrix(Row, Col) == 0)
					fg.TextMatrix(Row, Col) = 1;
				else
					fg.TextMatrix(Row, Col) = 0;
			}
        }
    }

	currRow = fg.Row;
	currTopRow = fg.TopRow;
	setupBtnsFromGridState();
	setTotalColuns();
	fg.Row = currRow;
	fg.TopRow = currTopRow;
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalocorrenciasverbas(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return true;
		
	if ((NewRow < 2) && (grid.Rows > 2))
	{
		grid.Row = 2;
	}

	setupBtnsFromGridState();
}

function js_BeforeRowColChangeModalocorrenciasverbas(grid, OldRow, OldCol, NewRow, NewCol, Cancel)
{
	//Forca celula read-only    
	if (!glb_GridIsBuilding)
	{
		if ( glb_FreezeRolColChangeEvents )
			return true;

		if (currCellIsLocked(grid, NewRow, NewCol))
		{
			glb_validRow = OldRow;
			glb_validCol = OldCol;
		}
		else
		    js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);

    }
}

function currCellIsLocked(grid, nRow, nCol)
{
	var bRetVal = false;

	if ((!glb_bOcorrenciasMode) && (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'PermiteAlteracao')) == 0))
		bRetVal = true;
	else if (glb_bOcorrenciasMode)
	{
		if (nCol == getColIndexByColKey(grid, 'dtFopagInicio'))
			bRetVal = (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'TrataDataFopagInicio')) == 0);
		else if (nCol == getColIndexByColKey(grid, 'dtFopagFim'))
			bRetVal = (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'TrataDataFopagFim')) == 0);
		else if (nCol == getColIndexByColKey(grid, 'dtReferencia'))
			bRetVal = (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'TrataDataReferencia')) == 0);
		else if (nCol == getColIndexByColKey(grid, 'Referencia'))
			bRetVal = (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'TrataReferencia')) == 0);
		else if (nCol == getColIndexByColKey(grid, 'Valor'))
			bRetVal = (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'TrataValor')) == 0);
	}

	return bRetVal;
}

function imprimeGrid(sCaller)
{
	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	var sMsg = aEmpresaData[6] + '     ' + sCaller + '     ' + getCurrDate();

	sMsg += '     ';
	fg.PrintGrid(sMsg, false, 2, 0, 450);

	fg.AutoSizeMode = 0;
	fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 1;
}

function getCurrDate()
{
   var d, s = "";
   d = new Date();
   
   if (DATE_SQL_PARAM == 103)
   {
		s += padL(d.getDate().toString(),2,'0') + "/";
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
   }
   else
   {
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
		s += padL(d.getDate().toString(),2,'0') + "/";
   }
   
   s += d.getYear() + " ";
   s += padL(d.getHours().toString(),2,'0') + ":";
   s += padL(d.getMinutes().toString(),2,'0');

   return(s);
}

function setTotalColuns()
{
	var nOldRow = fg.Row;
	
	if (glb_bOcorrenciasMode)
		gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [
			[getColIndexByColKey(fg, (chkFuncionarioVerba.checked ? 'VerbaID*' : 'FuncionarioID*')),'##########','C'],
			[getColIndexByColKey(fg, 'Valor'),'###,###,###.00','S']]);
	else
		gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [
			[getColIndexByColKey(fg, (chkFuncionarioVerba.checked ? 'VerbaID*' : 'FuncionarioID*')),'##########','C'],
			[getColIndexByColKey(fg, 'Total*'),'###,###,###.00','S'],
			[getColIndexByColKey(fg, 'Valor'),'###,###,###.00','S']]);
	
	if (nOldRow >= 2)
		fg.Row = nOldRow;

    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    var nValue = 0;
    var nCounter = 0;
    var nKey = '';

	if (bHasRowsInGrid)
	{
		for (i=2; i<fg.Rows; i++)
		{
			if (nKey != fg.TextMatrix(i, getColIndexByColKey(fg, (chkFuncionarioVerba.checked ? 'FuncionarioID*' : 'VerbaID*'))))
			{
				nCounter++;
				nKey = fg.TextMatrix(i, getColIndexByColKey(fg, (chkFuncionarioVerba.checked ? 'FuncionarioID*' : 'VerbaID*')));
			}
			/*if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) != 0)
			{
				nValue += fg.ValueMatrix(i, getColIndexByColKey(fg, sColuna));
				nCounter++;
			}*/
		}

		if (!glb_bOcorrenciasMode)
		{
			if (!chkSohTotalVerbas.checked)
				fg.TextMatrix(1, getColIndexByColKey(fg, 'Total*')) = fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor'));
		}
			
		fg.TextMatrix(1, getColIndexByColKey(fg, (chkFuncionarioVerba.checked ? 'Funcionario*' : 'Verba*'))) = nCounter;
	}
}

function adjustInterfaceByMode()
{

	// Ocorrencias
	if (glb_bOcorrenciasMode)
	{
		secText('Fopag ' + glb_nFopagID + ' - Ocorr�ncias', 1);
		lbldtFopagInicio.style.visibility = 'inherit';
		txtdtFopagInicio.style.visibility = 'inherit';
		lbldtFopagFim.style.visibility = 'inherit';
		txtdtFopagFim.style.visibility = 'inherit';
		divBnsOcorrencias.style.visibility = 'inherit';
		divBnsVerbas.style.visibility = 'hidden';
		chkAplicarFiltroFuncionario.style.width = FONT_WIDTH * 3;
		chkVerbasUsoCotidiano.style.width = FONT_WIDTH * 3;
		lblSohTotalVerbas.style.visibility = 'hidden';
		chkSohTotalVerbas.style.visibility = 'hidden';
		lblNomeInvertido.style.visibility = 'hidden';
		chkNomeInvertido.style.visibility = 'hidden';
		lblFuncionariosDiretos.style.visibility = 'hidden';
		chkFuncionariosDiretos.style.visibility = 'hidden';
		lblFuncionariosIndiretos.style.visibility = 'hidden';
		chkFuncionariosIndiretos.style.visibility = 'hidden';
		lblPessoalComercial.style.visibility = 'hidden';
		chkPessoalComercial.style.visibility = 'hidden';
		lblPessoalAdministrativo.style.visibility = 'hidden';
		chkPessoalAdministrativo.style.visibility = 'hidden';
		
		lblPessoalEstagio.style.visibility = 'hidden';
		chkPessoalEstagio.style.visibility = 'hidden';

		if (chkFuncionarioVerba.checked)
		{
			adjustElementsInForm([['lblFuncionarioVerba','chkFuncionarioVerba',3,1,-10,-10],
								['lblFuncionarios','selFuncionarios',16,1,-10],
								['lblVerbas','selVerbas',19,1,-5],
								['lbldtFopagInicio','txtdtFopagInicio',9,1,-5],
								['lbldtFopagFim','txtdtFopagFim',9,1,-5],
								['lbldtReferencia','txtdtReferencia',9,1,-5],
								['lblReferencia','txtReferencia',12,1,-5],
								['lblValor','txtValor',9,1,-5],
								['lblVerbaOK1','chkVerbaOK1',3,1,-5],
								['lblVerbaOK0','chkVerbaOK0',3,1,-20]], null, null, true);
		}						
		else
		{
			adjustElementsInForm([['lblFuncionarioVerba','chkFuncionarioVerba',3,1,-10,-10],
								['lblVerbas','selVerbas',19,1,-10],
								['lblFuncionarios','selFuncionarios',16,1,-5],
								['lbldtFopagInicio','txtdtFopagInicio',9,1,-5],
								['lbldtFopagFim','txtdtFopagFim',9,1,-5],
								['lbldtReferencia','txtdtReferencia',9,1,-5],
								['lblReferencia','txtReferencia',12,1,-5],
								['lblValor','txtValor',9,1,-5],
								['lblVerbaOK1','chkVerbaOK1',3,1,-5],
								['lblVerbaOK0','chkVerbaOK0',3,1,-20]], null, null, true);
		}
	}
	//Verbas
	else
	{
		secText('Fopag ' + glb_nFopagID + ' - Verbas', 1);

		lbldtFopagInicio.style.visibility = 'hidden';
		txtdtFopagInicio.style.visibility = 'hidden';
		lbldtFopagFim.style.visibility = 'hidden';
		txtdtFopagFim.style.visibility = 'hidden';
		divBnsOcorrencias.style.visibility = 'hidden';
		divBnsVerbas.style.visibility = 'inherit';
		lblSohTotalVerbas.style.visibility = 'inherit';
		chkSohTotalVerbas.style.visibility = 'inherit';
		lblNomeInvertido.style.visibility = 'inherit';
		chkNomeInvertido.style.visibility = 'inherit';
		lblFuncionariosDiretos.style.visibility = 'inherit';
		chkFuncionariosDiretos.style.visibility = 'inherit';
		lblFuncionariosIndiretos.style.visibility = 'inherit';
		chkFuncionariosIndiretos.style.visibility = 'inherit';
		lblPessoalComercial.style.visibility = 'inherit';
		chkPessoalComercial.style.visibility = 'inherit';
		lblPessoalAdministrativo.style.visibility = 'inherit';
		chkPessoalAdministrativo.style.visibility = 'inherit';
		lblPessoalEstagio.style.visibility = 'inherit';
		chkPessoalEstagio.style.visibility = 'inherit';

		if (chkFuncionarioVerba.checked)
			adjustElementsInForm([['lblFuncionarioVerba','chkFuncionarioVerba',3,1,-10,-10],
								['lblFuncionarios','selFuncionarios',16,1,-10],
								['lblVerbas','selVerbas',21,1,-5],
								['lbldtReferencia','txtdtReferencia',9,1,-5],
								['lblReferencia','txtReferencia',14,1,-5],
								['lblValor','txtValor',9,1,-5],
								['lblFuncionariosDiretos','chkFuncionariosDiretos',3,1,-5],
								['lblFuncionariosIndiretos','chkFuncionariosIndiretos',3,1,-23],
								['lblPessoalComercial','chkPessoalComercial',3,1,-10],
								['lblPessoalAdministrativo', 'chkPessoalAdministrativo', 3, 1, -20],
								['lblPessoalEstagio', 'chkPessoalEstagio', 3, 1, -20],
								['lblVerbaOK1','chkVerbaOK1',3,1,-10],
								['lblVerbaOK0','chkVerbaOK0',3,1,-20],
								['lblSohTotalVerbas','chkSohTotalVerbas',3,1,-10],
								['lblNomeInvertido','chkNomeInvertido',3,1,-10]], null, null, true);
		else
			adjustElementsInForm([['lblFuncionarioVerba','chkFuncionarioVerba',3,1,-10,-10],
								['lblVerbas','selVerbas',21,1,-10],
								['lblFuncionarios','selFuncionarios',16,1,-5],
								['lbldtReferencia','txtdtReferencia',9,1,-5],
								['lblReferencia','txtReferencia',14,1,-5],
								['lblValor','txtValor',9,1,-5],
								['lblFuncionariosDiretos','chkFuncionariosDiretos',3,1,-5],
								['lblFuncionariosIndiretos','chkFuncionariosIndiretos',3,1,-23],
								['lblPessoalComercial','chkPessoalComercial',3,1,-10],
								['lblPessoalAdministrativo', 'chkPessoalAdministrativo', 3, 1, -20],
								['lblPessoalEstagio', 'chkPessoalEstagio', 3, 1, -20],
								['lblVerbaOK1','chkVerbaOK1',3,1,-10],
								['lblVerbaOK0','chkVerbaOK0',3,1,-20],
								['lblSohTotalVerbas','chkSohTotalVerbas',3,1,-10],
								['lblNomeInvertido','chkNomeInvertido',3,1,-10]], null, null, true);
	}

	chkAplicarFiltroFuncionario.style.left = lblFuncionarios.offsetLeft + lblFuncionarios.offsetWidth - (ELEM_GAP * 3) - 5;
	chkAplicarFiltroFuncionario.style.top = lblFuncionarios.offsetTop - 7;
	chkAplicarFiltroFuncionario.onclick = chkAplicarFiltroFuncionario_onclick;

	chkVerbasUsoCotidiano.style.left = lblVerbas.offsetLeft + lblVerbas.offsetWidth - 15;
	chkVerbasUsoCotidiano.style.top = lblVerbas.offsetTop - 7;
	
	chkFuncionariosDiretos.onclick = chkFuncionariosDiretos_onclick;
	chkFuncionariosIndiretos.onclick = chkFuncionariosDiretos_onclick;
	
	chkPessoalComercial.onclick =  chkPessoalComercial_onclick;
	chkPessoalAdministrativo.onclick = chkPessoalComercial_onclick;
	chkPessoalEstagio.onclick = chkPessoalComercial_onclick;
	
	chkVerbaOK0.onclick = chkVerbaOK_onclick;
	chkVerbaOK1.onclick = chkVerbaOK_onclick;

	setupBtnsFromGridState();
}

function chkAplicarFiltroFuncionario_onclick()
{
	fillCmbFuncionarios();
}

function chkFuncionariosDiretos_onclick()
{
	if ((this.id == chkFuncionariosDiretos.id) && (!chkFuncionariosDiretos.checked))
		chkFuncionariosIndiretos.checked = true;
	else if ((this.id == chkFuncionariosIndiretos.id) && (!chkFuncionariosIndiretos.checked))
		chkFuncionariosDiretos.checked = true;
		
	if (chkAplicarFiltroFuncionario.checked)
		fillCmbFuncionarios();
}

function chkPessoalComercial_onclick()
{
    if ((this.id == chkPessoalComercial.id) && (!chkPessoalComercial.checked) && (!chkPessoalEstagio.checked))
		chkPessoalAdministrativo.checked = true;
    else if ((this.id == chkPessoalAdministrativo.id) && (!chkPessoalAdministrativo.checked) && (!chkPessoalEstagio.checked))
        chkPessoalComercial.checked = true;
    else if ((this.id == chkPessoalEstagio.id) && (!chkPessoalComercial.checked) && (!chkPessoalAdministrativo.checked))
        chkPessoalComercial.checked = true;	
		

	if (chkAplicarFiltroFuncionario.checked)
		fillCmbFuncionarios();
}

function chkVerbaOK_onclick()
{
	if ((this.id == chkVerbaOK0.id) && (!chkVerbaOK0.checked))
		chkVerbaOK1.checked = true;
	else if ((this.id == chkVerbaOK1.id) && (!chkVerbaOK1.checked))
		chkVerbaOK0.checked = true;	
}

function setHeaderGrid()
{
	var aHoldCols = null;
	var bFuncionarioVerba = chkFuncionarioVerba.checked;
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	if (glb_bOcorrenciasMode)
	{
		// Consistencia
		if (selModo.value == 2)
			aHoldCols = [13,14,15,16,17,18,19,20];
		else
			aHoldCols = [12,13,14,15,16,17,18,19,20];

		headerGrid(fg,['ID',
						(bFuncionarioVerba ? 'Funcionario' : 'Verba'),
						'ID',
						(bFuncionarioVerba ? 'Verba' : 'Funcionario'),
						'OK',
						'Fopag In�cio',
						'Fopag Fim',
						'Data Ref',
						'Refer�ncia',
						'Valor',
						'*',
						'C',
						'Consist�ncia',
						'PermiteAlteracao',
						'TrataDataFopagInicio',
						'TrataDataFopagFim',
						'TrataDataReferencia',
						'TrataReferencia',
						'TrataValor',
						'FopOcorrenciaID',
						'FuncNome'], aHoldCols);
		
		glb_aCelHint = [[0,10,'VerbaOK'], [0,11,'Confirmado']];
	}
	else
	{
		if (chkSohTotalVerbas.checked)
			aHoldCols = [3,4,5,6,7,8,9,10,11,12,13,14,15,16];
		else
			aHoldCols = [11,12,13,14,15,16];
	
	    headerGrid(fg,['ID',
					(bFuncionarioVerba ? 'Funcionario' : 'Verba'),
					'Total',
					'ID',
					(bFuncionarioVerba ? 'Verba' : 'Funcionario'),
					'OK',
				    'Data',
				    'Refer�ncia',
				    'Valor',
				    '*',
				    'AM',
				    'PermiteAlteracao',
				    'TrataDataReferencia',
				    'TrataReferencia',
				    'TrataValor',
				    'FopVerbaID',
				    'FuncNome'], aHoldCols);
		
		glb_aCelHint = [[0,9,'VerbaOK'],[0,10,'Altera��o Manual']];
	}
	fg.Redraw = 2;
}

/*
	Procura pela 1a celula disponivel p/ edicao.
	Retorno: null, nao encontrou nenhuma celular disponivel
	         [Row, Col], linha o coluna disponivel
*/
function getFirstRowColAllowed()
{
	var i;
	var nValidRow = 0;
	var nValidCol = 0;
	
	// Modo de ocorrencias
	if (glb_bOcorrenciasMode)
	{
		for (i=2; i<fg.Rows; i++)
		{
			if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataDataFopagInicio')) != 0)
				nValidCol = getColIndexByColKey(fg, 'dtFopagInicio');
			else if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataDataFopagFim')) != 0)
				nValidCol = getColIndexByColKey(fg, 'dtFopagFim');
			else if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataDataReferencia')) != 0)
				nValidCol = getColIndexByColKey(fg, 'dtReferencia');
			else if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataReferencia')) != 0)
				nValidCol = getColIndexByColKey(fg, 'Referencia');
			else if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TrataValor')) != 0)
				nValidCol = getColIndexByColKey(fg, 'Valor');

			if (nValidCol != 0)
			{
				nValidRow = i;
				break;
			}
		}
	}
	else
	{
		for (i=2; i<fg.Rows; i++)
		{
			// Nao permite alteracao
			if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteAlteracao')) == 0)
				continue;
			else
			{
				nValidCol = getColIndexByColKey(fg, 'OK');
				nValidRow = i;
				break;
			}
		}
	}

	if ((nValidCol + nValidRow) > 0)
		return new Array(nValidRow, nValidCol);
	else
		return null;
}

function setNextFocus(oText)
{
	var aTexts = [txtdtFopagInicio, txtdtFopagFim, txtdtReferencia, txtReferencia, txtValor];
	var nIndex = findText(aTexts, oText.id);
	var nTratamento = null;
	
	if (nIndex < 0)
		return null;

	else if ((selVerbas.selectedIndex <= 0) && (selModo.value == 0))
		return null;
		
	else if (nIndex == (aTexts.length - 1))
		btnListarOcorrencias_Clicked(false);
	
	else if (selModo.value != 0)
	{
		if (! aTexts[nIndex + 1].disabled)
		{
			window.focus();
			aTexts[nIndex + 1].focus();
		}
	}
	else
	{
		if (aTexts[nIndex + 1].id == txtdtFopagInicio.id)
			nTratamento = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataFopagInicio', 1);
			
		else if (aTexts[nIndex + 1].id == txtdtFopagFim.id)
			nTratamento = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataFopagFim', 1);

		else if (aTexts[nIndex + 1].id == txtdtReferencia.id)
			nTratamento = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataDataReferencia', 1);

		else if (aTexts[nIndex + 1].id == txtReferencia.id)
			nTratamento = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataReferencia', 1);

		else if (aTexts[nIndex + 1].id == txtValor.id)
			nTratamento = selVerbas.options[selVerbas.selectedIndex].getAttribute('TrataValor', 1);
			
		// Nao editavel
		if (nTratamento == 0)
			setNextFocus(aTexts[nIndex + 1]);
		else if (! aTexts[nIndex + 1].disabled)
		{
			window.focus();
			aTexts[nIndex + 1].focus();
		}
	}
}

function findText(aTexts, nTextID)
{
	var retVal = -1;
	var i;
	for (i=0; i<aTexts.length; i++)
	{
		if (aTexts[i].id == nTextID)
		{
			retVal = i;
			break;
		}
	}

	return retVal;
}

function settxtValorKeyUp()
{
	if (glb_txtValorKeyUpTimer != null)
    {
        window.clearInterval(glb_txtValorKeyUpTimer);
        glb_txtValorKeyUpTimer = null;
    }

	txtValor.onkeyup = txtListByKeyPress;
}

// FINAL DE EVENTOS DE GRID *****************************************