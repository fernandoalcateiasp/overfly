<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    Dim rsData, strSQL
    
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalapontamentoHtml" name="modalapontamentoHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modrecursoshumanos/submodfopag/fopag/modalpages/modalapontamento.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
       
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modrecursoshumanos/submodfopag/fopag/modalpages/modalapontamento.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, sCurrDateFormat
Dim nEmpresaID, nFopagID, sDataInicio, sDataFim, nUserID, sDataUltimoFechamento, nEstadoID, ndirC1, ndirC2, ndirA1, ndirA2

sCaller = ""
nEmpresaID = 0
nFopagID = 0
nUserID = 0
sDataInicio = ""
sDataFim = ""
sDataUltimoFechamento = ""
nEstadoID = 0
ndirC1=0
ndirC2=0
ndirA1=0
ndirA2=0


For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

'nEmpresaID - ID da empresa logada
For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

'nFopagID - ID da Fopag
For i = 1 To Request.QueryString("nFopagID").Count    
    nFopagID = Request.QueryString("nFopagID")(i)
Next

For i = 1 To Request.QueryString("sDataInicio").Count    
    sDataInicio = Request.QueryString("sDataInicio")(i)
Next

For i = 1 To Request.QueryString("sDataFim").Count    
    sDataFim = Request.QueryString("sDataFim")(i)
Next

For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

For i = 1 To Request.QueryString("sDataUltimoFechamento").Count    
    sDataUltimoFechamento = Request.QueryString("sDataUltimoFechamento")(i)
Next

For i = 1 To Request.QueryString("nEstadoID").Count    
    nEstadoID = Request.QueryString("nEstadoID")(i)
Next

'ndirC1 Direito do usu�rio
For i = 1 To Request.QueryString("ndirC1").Count    
    ndirC1 = Request.QueryString("ndirC1")(i)
Next

'ndirC2 Direito do usu�rio
For i = 1 To Request.QueryString("ndirC2").Count    
    ndirC2 = Request.QueryString("ndirC2")(i)
Next

'ndirA1 Direito do usu�rio
For i = 1 To Request.QueryString("ndirA1").Count    
    ndirA1 = Request.QueryString("ndirA1")(i)
Next

'ndirA2 Direito do usu�rio
For i = 1 To Request.QueryString("ndirA2").Count    
    ndirA2 = Request.QueryString("ndirA2")(i)
Next


Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf


Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT Fantasia FROM Pessoas a WITH(NOLOCK) " & _
	"WHERE a.PessoaID=" & CStr(nUserID)

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write "var glb_sUser = '';"

If (Not rsData.EOF) Then
	Response.Write "var glb_sUser = " & Chr(39) & rsData.Fields("Fantasia").Value & Chr(39) & ";" & vbcrlf
End If

rsData.Close


'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";" & vbcrlf
Response.Write "var glb_nFopagID = " & CStr(nFopagID) & ";" & vbcrlf
Response.Write "var glb_sDataInicio = '" & CStr(sDataInicio) & "';" & vbcrlf
Response.Write "var glb_sDataFim = '" & CStr(sDataFim) & "';" & vbcrlf
Response.Write "var glb_sDataUltimoFechamento = '" & CStr(sDataUltimoFechamento) & "';" & vbcrlf
Response.Write "var glb_nEstadoID = '" & CStr(nEstadoID) & "';" & vbcrlf
Response.Write "var glb_ndirC1 = " &  CStr(ndirC1) & ";" & vbcrlf
Response.Write "var glb_ndirC2 = " &  CStr(ndirC2) & ";" & vbcrlf
Response.Write "var glb_ndirA1 = " &  CStr(ndirA1) & ";" & vbcrlf
Response.Write "var glb_ndirA2 = " &  CStr(ndirA2) & ";" & vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalapontamento_ValidateEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalapontamento_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalapontamentoKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalapontamento_BeforeEdit(fg, fg.Row, fg.Col)
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modalapontamentoDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalapontamentoBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
 
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
<!--
 js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Atualizar linha de mensagens
js_fg_AfterRowColmodalapontamento (fg, arguments[0], arguments[1], arguments[2], arguments[3]);

//Para linhas read only
js_fg_AfterRowColChange2 (fg, arguments[0], arguments[1], arguments[2], arguments[3], true);

//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 //Atencao programador:
 //Setimo parametro abaixo = true, permite ao usuario mudar de linha em grid
 //clicando em celula read only.
 js_fg_BeforeRowColChangeApontamento (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], false);
//-->
</SCRIPT>

<script ID="vbFunctions" LANGUAGE="vbscript">
<!--

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

//-->
</script>

</head>

<body id="modalapontamentoBody" name="modalapontamentoBody" LANGUAGE="javascript" onload="return window_onload()"> 
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    <!-- Div Controles -->
    <div id="divControls" name="divControls" class="divGeneral">
        <input type="button" id="btnApontFecha" name="btnApontFecha" value="Apontamento" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <p id="lblGestor" name="lblGestor" class="lblGeneral">Gestor imediato</p>
		<select id="selGestor" name="selGestor" class="fldGeneral" onchange="return selGestor_onchange()"></select>
        <p id="lblFuncionario" name="lblFuncionario" class="lblGeneral">Funcion�rio</p>
		<select id="selFuncionario" name="selFuncionario" class="fldGeneral" onchange="return selFuncionario_onchange()"></select>
        <p id="lblInicio" name="lblInicio" class="lblGeneral">Inicio</p>
        <input type="text" id="txtInicio" name="txtInicio" class="fldGeneral" title="Inicio">
        <p id="lblFim" name="lblFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtFim" name="txtFim" class="fldGeneral" title="Fim">
        <p id="lblAprFuncionario" name="lblAprFuncionario" class="lblGeneral">Aprova��o Funcion�rio</p>
        <input type="text" id="txtAprFuncionario" name="txtAprFuncionario" class="fldGeneral" title="Aprov. Funcion�rio">
        <p id="lblAprGestor" name="lblAprGestor" class="lblGeneral">Aprova��o Gestor</p>
        <input type="text" id="txtAprGestor" name="txtAprGestor" class="fldGeneral" title="Aprov. Gestor">
        
        <p id="lblInconsistencia" name="lblInconsistencia" class="lblGeneral">Inc</p>
		<input type="checkbox" id="chkInconsistencia" name="chkInconsistencia" class="fldGeneral" title="Mostrar somente somente as inconsist�ncias?">
		
		<p id="lblSincSim" name="lblSincSim" class="lblGeneral">Sinc</p>
        <input type="checkbox" id="chkSincSim" name="chkSincSim" class="fldGeneral" title="Somente os sincronizados" checked>
        <p id="lblSincNao" name="lblSincNao" class="lblGeneral"></p>
        <input type="checkbox" id="chkSincNao" name="chkSincNao" class="fldGeneral" title="Somente os n�o sincronizados" checked>
        
			
        <input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnExcel" name="btnExcel" value="Excel" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnAprovar" name="btnAprovar" value="Aprovar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        
        
    </div>    
    
    <input type="button" id="btnOK" name="btnOK" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
            

    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>
    
</body>

</html>
