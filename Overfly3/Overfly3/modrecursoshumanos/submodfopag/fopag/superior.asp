<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="fopagsup01Html" name="fopagsup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf      
       
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modqualidade/commonqualidade/commonqualidade.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modrecursoshumanos/submodfopag/fopag/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modrecursoshumanos/submodfopag/fopag/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="fopagsup01Body" name="fopagsup01Body" LANGUAGE="javascript" onload="return window_onload()">
   <!-- //@@ Os dsos sao definidos de acordo com o form -->
        
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo Fopag</p>
		<select id="selTipoRegistroID" name="selTipoRegistroID" DATASRC="#dsoSup01" DATAFLD="TipoFopagID" class="fldGeneral"></select>
        <p id="lbldtFopag" name="lbldtFopag" class="lblGeneral">Data Fopag</p>
        <input type="text" id="txtdtFopag" name="txtdtFopag" DATASRC="#dsoSup01" DATAFLD="V_dtFopag" class="fldGeneral" title="Data da fopag"></input>
        <p id="lbldtFopagInicio" name="lbldtFopagInicio" class="lblGeneral">Fopag In�cio</p>
        <input type="text" id="txtdtFopagInicio" name="txtdtFopagInicio" DATASRC="#dsoSup01" DATAFLD="V_dtFopagInicio" class="fldGeneral" title="Data do in�cio do per�odo da fopag"></input>
        <p id="lbldtFopagFim" name="lbldtFopagFim" class="lblGeneral">Fopag Fim</p>
        <input type="text" id="txtdtFopagFim" name="txtdtFopagFim" DATASRC="#dsoSup01" DATAFLD="V_dtFopagFim" class="fldGeneral" title="Data do fim do per�odo da fopag"></input>

        <p  id="lblProcessamentoLiberado" name="lblProcessamentoLiberado" class="lblGeneral">PL</p>
        <input type="checkbox" id="chkProcessamentoLiberado" name="chkProcessamentoLiberado" DATASRC="#dsoSup01" DATAFLD="ProcessamentoLiberado" class="fldGeneral" title="Processamento liberado?"></input>
        <p  id="lblIncluiVerbasNormais" name="lblIncluiVerbasNormais" class="lblGeneral">VN</p>
        <input type="checkbox" id="chkIncluiVerbasNormais" name="chkIncluiVerbasNormais" DATASRC="#dsoSup01" DATAFLD="IncluiVerbasNormais" class="fldGeneral" title="Inclui verbas normais?"></input>
        <p  id="lblIncluiVerbasVendas" name="lblIncluiVerbasVendas" class="lblGeneral">VV</p>
        <input type="checkbox" id="chkIncluiVerbasVendas" name="chkIncluiVerbasVendas" DATASRC="#dsoSup01" DATAFLD="IncluiVerbasVendas" class="fldGeneral" title="Inclui verbas de vendas?"></input>
        <p  id="lblSubtraiSalarioFixo" name="lblSubtraiSalarioFixo" class="lblGeneral">SSF</p>
        <input type="checkbox" id="chkSubtraiSalarioFixo" name="chkSubtraiSalarioFixo" DATASRC="#dsoSup01" DATAFLD="SubtraiSalarioFixo" class="fldGeneral" title="Subtrai sal�rio fixo?"></input>
        <p  id="lblArredondaSalarios" name="lblArredondaSalarios" class="lblGeneral">AS</p>
        <input type="checkbox" id="chkArredondaSalarios" name="chkArredondaSalarios" DATASRC="#dsoSup01" DATAFLD="ArredondaSalarios" class="fldGeneral" title="Arredonda sal�rios?"></input>
        <p  id="lblGeraFinanceirosDistintos" name="lblGeraFinanceirosDistintos" class="lblGeneral">GFD</p>
        <input type="checkbox" id="chkGeraFinanceirosDistintos" name="chkGeraFinanceirosDistintos" DATASRC="#dsoSup01" DATAFLD="GeraFinanceirosDistintos" class="fldGeneral" title="Gera financeiros distintos?"></input>
        <p  id="lblLimiteComissaoVendas" name="lblLimiteComissaoVendas" class="lblGeneral">LCV</p>
        <input type="text" id="txtLimiteComissaoVendas" name="txtLimiteComissaoVendas" DATASRC="#dsoSup01" DATAFLD="LimiteComissaoVendas" class="fldGeneral" title="Limite para efeito de c�lculo de comiss�o de vendas"></input>
        <p  id="lblPercentualPremioAdministrativo" name="lblPercentualPremioAdministrativo" class="lblGeneral">PPA</p>
        <input type="text" id="txtPercentualPremioAdministrativo" name="txtPercentualPremioAdministrativo" DATASRC="#dsoSup01" DATAFLD="PercentualPremioAdministrativo" class="fldGeneral" title="Percentual de pr�mio administrativo"></input>
        <p id="lblDiaAntecipacaoAviso" name="lblDiaAntecipacaoAviso" class="lblGeneral">DAAS</p>
        <input type="text" id="txtDiaAntecipacaoAviso" name="txtDiaAntecipacaoAviso" DATASRC="#dsoSup01" DATAFLD="DiaAntecipacaoAviso" class="fldGeneral" title="Dia do aviso da antecipa��o salarial"></input>
        <p id="lblDiaAntecipacaoPagamento" name="lblDiaAntecipacaoPagamento" class="lblGeneral">DPAS</p>
        <input type="text" id="txtDiaAntecipacaoPagamento" name="txtDiaAntecipacaoPagamento" DATASRC="#dsoSup01" DATAFLD="DiaAntecipacaoPagamento" class="fldGeneral" title="Dia do pagamento da antecipa��o salarial"></input>
        <p id="lblDiasUteisPagamento" name="lblDiasUteisPagamento" class="lblGeneral">DUPF</p>
        <input type="text" id="txtDiasUteisPagamento" name="txtDiasUteisPagamento" DATASRC="#dsoSup01" DATAFLD="DiasUteisPagamento" class="fldGeneral" title="Dias �teis para pagamento da fopag"></input>
        <p id="lblDiasDesconsiderarVendas" name="lblDiasDesconsiderarVendas" class="lblGeneral">DDV</p>
        <input type="text" id="txtDiasDesconsiderarVendas" name="txtDiasDesconsiderarVendas" DATASRC="#dsoSup01" DATAFLD="DiasDesconsiderarVendas" class="fldGeneral" title="Dias a desconsiderar nas vendas"></input>
        <p id="lblDiasRecuarVendas" name="lblDiasRecuarVendas" class="lblGeneral">DRV</p>
        <input type="text" id="txtDiasRecuarVendas" name="txtDiasRecuarVendas" DATASRC="#dsoSup01" DATAFLD="DiasRecuarVendas" class="fldGeneral" title="Dias a recuar nas vendas"></input>
        <p id="lblDiaAvisoPontoEletronico" name="lblDiaAvisoPontoEletronico" class="lblGeneral">DAPE</p>
        <input type="text" id="txtDiaAvisoPontoEletronico" name="txtDiaAvisoPontoEletronico" DATASRC="#dsoSup01" DATAFLD="DiaAvisoPontoEletronico" class="fldGeneral" title="Dia do aviso do Ponto Eletr�nico"></input>
        <p id="lbldtAntecipacaoAviso" name="lbldtAntecipacaoAviso" class="lblGeneral">Antec Aviso</p>
        <input type="text" id="txtdtAntecipacaoAviso" name="txtdtAntecipacaoAviso" DATASRC="#dsoSup01" DATAFLD="dtAntecipacaoAviso" class="fldGeneral" title="Data para o aviso de antecipa��o salarial"></input>
        <p id="lbldtAntecipacaoPagamento" name="lbldtAntecipacaoPagamento" class="lblGeneral">Antec Pagamento</p>
        <input type="text" id="txtdtAntecipacaoPagamento" name="txtdtAntecipacaoPagamento" DATASRC="#dsoSup01" DATAFLD="dtAntecipacaoPagamento" class="fldGeneral" title="Data para o pagamento da antecipa��o salarial"></input>
        <p id="lbldtPagamento" name="lbldtPagamento" class="lblGeneral">Fopag Pagamento</p>
        <input type="text" id="txtdtPagamento" name="txtdtPagamento" DATASRC="#dsoSup01" DATAFLD="V_dtFopagPagamento" class="fldGeneral" title="Data para o pagamento da fopag"></input>
        <p id="lbldtVendasInicio" name="lbldtVendasInicio" class="lblGeneral">Vendas In�cio</p>
        <input type="text" id="txtdtVendasInicio" name="txtdtVendasInicio" DATASRC="#dsoSup01" DATAFLD="dtVendasInicio" class="fldGeneral" title="Data do in�cio do per�odo das vendas"></input>
        <p id="lbldtVendasFim" name="lbldtVendasFim" class="lblGeneral">Vendas Fim</p>
        <input type="text" id="txtdtVendasFim" name="txtdtVendasFim" DATASRC="#dsoSup01" DATAFLD="dtVendasFim" class="fldGeneral" title="Data do fim do per�odo das vendas"></input>
        <p id="lbldtInadimplenciaInicio" name="lbldtInadimplenciaInicio" class="lblGeneral">Inadimpl In�cio</p>
        <input type="text" id="txtdtInadimplenciaInicio" name="txtdtInadimplenciaInicio" DATASRC="#dsoSup01" DATAFLD="dtInadimplenciaInicio" class="fldGeneral" title="Data do in�cio do per�odo da inadimpl�ncia"></input>
        <p id="lbldtInadimplenciaFim" name="lbldtInadimplenciaFim" class="lblGeneral">Inadimpl Fim</p>
        <input type="text" id="txtdtInadimplenciaFim" name="txtdtInadimplenciaFim" DATASRC="#dsoSup01" DATAFLD="dtInadimplenciaFim" class="fldGeneral" title="Data do fim do per�odo da inadimpl�ncia"></input>
        <p id="lbldtObsolescencia" name="lbldtObsolescencia" class="lblGeneral">Obsolesc�ncia</p>
        <input type="text" id="txtdtObsolescencia" name="txtdtObsolescencia" DATASRC="#dsoSup01" DATAFLD="dtObsolescencia" class="fldGeneral" title="Data para c�lculo da obsolesc�ncia dos produtos"></input>

        <p id="lblTotalFopag" name="lblTotalFopag" class="lblGeneral">Total Fopag</p>
        <input type="text" id="txtTotalFopag" name="txtTotalFopag" DATASRC="#dsoSup01" DATAFLD="TotalFopag" class="fldGeneral"></input>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>

        <p  id="lblPercentual" name="lblPercentual" class="lblGeneral">Perc(%)</p>
        <input type="text" id="txtPercentual" name="txtPercentual" DATASRC="#dsoSup01" DATAFLD="Percentual" class="fldGeneral" title="Percentual"></input>
	</div>
</body>

</html>
