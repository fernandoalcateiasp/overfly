/********************************************************************
inferioresp.js

Library javascript para o inferior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:	
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
	setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
	fillCmbPastasInCtlBarInf(nTipoRegistroID)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao filtroComplete_DSC(emptyFilter) do js_detailinf.js

Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e no retorno chama a funcao de
ondatasetcomplete de acordo com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.FopagID, a.ProprietarioID,a.AlternativoID,a.Observacoes FROM Fopag a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.FopagID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // Metas Vendas
    else if (folderID == 32001)
    {
        dso.SQL = 'SELECT ISNULL(f.ItemMasculino, ' + '\'' + 'Administrativo' + '\'' + ') AS Grupo, e.RecursoFantasia AS Cargo, c.Fantasia AS Funcionario, a.*, ' +
								'd.dtAdmissao, CONVERT(BIT, a.Area) AS Comercial, g.SimboloMoeda AS SimboloMoedaSalario, h.SimboloMoeda AS SimboloMoedaVenda ' +
					'FROM Fopag_Funcionarios a WITH(NOLOCK) ' +
					    'LEFT OUTER JOIN Conceitos g WITH(NOLOCK) ON (a.MoedaSalarioID=g.ConceitoID) ' +
					    'LEFT OUTER JOIN Conceitos h WITH(NOLOCK) ON (a.MoedaVendaID=h.ConceitoID) ' +
					    'INNER JOIN Fopag b WITH(NOLOCK) ON (a.FopagID=b.FopagID) ' +
					    'INNER JOIN Pessoas c WITH(NOLOCK) ON (a.FuncionarioID=c.PessoaID) ' +
					    'INNER JOIN RelacoesPessoas d WITH(NOLOCK) ON (c.PessoaID=d.SujeitoID AND d.ObjetoID=b.EmpresaID) ' +
					    'INNER JOIN Recursos e WITH(NOLOCK) ON (d.CargoID = e.RecursoID) ' +
					    'LEFT OUTER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (e.TipoPerfilComercialID = f.ItemID) ' +
					'WHERE ' + sFiltro + ' a.FopagID = '+ idToFind + ' AND d.TipoRelacaoID=31 AND d.EstadoID = 2' +
					'ORDER BY ISNULL(f.ItemID, 9999), e.RecursoID, Funcionario';
        return 'dso01Grid_DSC';
    }
    // Vendas
    else if (folderID == 32002)
    {
        dso.SQL = 'SELECT a.*, b.Fantasia, c.Verba ' +
                  'FROM Fopag_Vendas a WITH(NOLOCK), Pessoas b WITH(NOLOCK), VerbasFopag c WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.FopagID = '+ idToFind + ' ' +
                  'AND a.FuncionarioID=b.PessoaID AND a.VerbaID=c.VerbaID ' +
                  'ORDER BY c.VerbaID, b.Fantasia';
        return 'dso01Grid_DSC';
    }
    // Valores Externos
    else if (folderID == 32003)
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Fopag_ValoresExternos a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.FopagID = '+ idToFind + ' ' +                  
                  'ORDER BY a.VerbaID, a.MarcaID, a.GeraFinanceiro, a.Valor, a.FopValorExternoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 32004) // Lancamentos
    {
		dso.SQL = 'EXEC sp_EventosContabeis_Lancamentos ' + glb_nFormID + ', NULL, ' + idToFind;
		return 'dso01Grid_DSC';
    }
    // Par�metros
    else if (folderID == 32005) {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Fopag_Parametros a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.FopagID = ' + idToFind + ' ' +
                  'ORDER BY a.VerbaID';
        return 'dso01Grid_DSC';
    }
    
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao dso01Grid_DSC() do js_detailinf.js

Monta string de select para combo de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Valores Externos
    if (pastaID == 32003)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT  a.VerbaID AS fldID, a.Verba AS fldName ' +
                        'FROM VerbasFopag a WITH(NOLOCK) ' +
                        'WHERE (EstadoID=2 AND PermiteValorExterno = 1) ' +
                        'ORDER BY VerbaID';
        }
        else if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT  VerbaID, Verba ' +
                        'FROM VerbasFopag a WITH(NOLOCK) ' +
                        'WHERE (EstadoID=2 AND PermiteValorExterno = 1) ' +
                        'ORDER BY VerbaID';
        }
        else if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT SPACE(0) AS Conceito, 0 AS ConceitoID ' + 
                        'UNION ALL ' +
                        'SELECT a.Conceito, a.ConceitoID ' +
                      'FROM Conceitos a WITH(NOLOCK) ' +
                      'WHERE a.EstadoID = 2 AND a.TipoConceitoID = 304 ' +
                      'ORDER BY Conceito';
        }
    }
    // Par�metros
    if (pastaID == 32005)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT  a.VerbaID AS fldID, a.Verba AS fldName ' +
                        'FROM VerbasFopag a WITH(NOLOCK) ' +
                        'WHERE (EstadoID=2 AND ehParametro = 1) ' +
                        'ORDER BY VerbaID';
        }
        else if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT  VerbaID, Verba ' +
                        'FROM VerbasFopag a WITH(NOLOCK) ' +
                        'WHERE (EstadoID=2 AND ehParametro = 1) ' +
                        'ORDER BY VerbaID';
        }
    }
    
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(32005);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 32005); //Parametros

    vPastaName = window.top.getPastaName(32001);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 32001); //Metas Vendas

    vPastaName = window.top.getPastaName(32002);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 32002); //Vendas
		
	vPastaName = window.top.getPastaName(32003);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 32003); //Valores Externos

    vPastaName = window.top.getPastaName(32004);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 32004); //Lancamentos

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
		// Aulas
        //if (folderID == 31152)
            //currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0, 1, 2, 3], ['RETID', registroID]);

        // Valores Externos
        if (folderID == 32003)
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0, 4], ['FopagID', registroID]);

        // Par�metros aqui
        if (folderID == 32005)
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 2, 3], ['FopagID', registroID]);
        
        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao buildGridFromFolder(folderID) do js_detailinf.js

Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    var i=0;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);
    }
    // Funcionario
    else if (folderID == 32001)
    {
		headerGrid(fg,['Grupo',
					'Cargo',
					'ID',
					'Funcion�rio',
					'Admiss�o',
					'Dir',
					'Coml',
					'$',
					'Sal�rio Carteira',
					'Sal�rio Fixo',
					'Sal�rio Meta',
					'$',
					'Faturamento',
					'Contribui��o',
					'Repasse',
					'Peso',
					'Avalia��o',
					'Observa��o',
					'Observacoes',
					'FopFuncionarioID'], [18, 19]);

		glb_aCelHint = [[0,5,'Funcion�rio direto?'],
						[0,6,'�rea comercial']];

		fillGridMask(fg,currDSO,['Grupo',
								'Cargo',
								'FuncionarioID',
								'Funcionario',
								'dtAdmissao',
								'FuncionarioDireto',
								'Comercial',
								'SimboloMoedaSalario',
								'SalarioCarteira',
								'SalarioFixo',
								'MetaSalario',
								'SimboloMoedaVenda',
								'MetaFaturamento',
								'MetaContribuicao',
								'RepasseContribuicao',
								'Peso',
								'Avaliacao',
								'Observacao',
								'Observacoes',
								'FopFuncionarioID'],
								['','','','','99/99/9999','','','','999999999.99','999999999.99','999999999.99','','999999999.99','999999999.99','999999999.99','999','',''],
								['','','','',dTFormat    ,'','','','###,###,##0.00','###,###,##0.00','###,###,##0.00','','###,###,##0.00','###,###,##0.00','###,###,##0.00','##0','','']);

		gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[2,'####0','C'], [8,'###,###,###.00','S'], [9,'###,###,###.00','S'],
			[10,'###,###,###.00','S'], [12,'###,###,###.00','S'], [13,'###,###,###.00','S'], [14,'###,###,###.00','S'], [15,'###,###,###.00','M'],
			[16,'###,###,###.00','M']]);
		
		alignColsInGrid(fg,[2,8,9,10,12,13,14,15,16]);
		
		// Merge de Colunas
		fg.MergeCells = 4;
		fg.MergeCol(0) = true;
		fg.MergeCol(1) = true;
		fg.FrozenCols = 4;
    }
    // Vendas
    else if (folderID == 32002)
    {
        headerGrid(fg,['ID',
					   'Verba',
					   'Funcion�rio',
					   'Quant',
                       'Valor',
                       'Valor2',
                       'Valor3',
                       'Observa��o',
                       'FopVendaID'], [8]);

        fillGridMask(fg,currDSO,['VerbaID',
								 'Verba',
								 'Fantasia',
								 'Quantidade',
								 'Valor',
								 'Valor2',
								 'Valor3',
								 'Observacao',
								 'FopVendaID'],
								 ['9999999999','','','9999999999','999999999.99','999999999.99','999999999.99','',''],
								 ['##########','','','##########','###,###,##0.00','###,###,##0.00','###,###,##0.00','','']);

		alignColsInGrid(fg,[0, 3, 4, 5, 6]);
		gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[2,'########','C']]);
															  
        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Valores Externo
    else if (folderID == 32003)
    {
        headerGrid(fg,['VerbaID',
                       'Verba',
                       'Marca',
                       'GF',
                       'Valor',
                       'Observa��o',
                       'FopValorExternoID'], [6]);

        fillGridMask(fg,currDSO,['VerbaID',
                                 '^VerbaID^dso01GridLkp^fldID^fldName*',
                                 'MarcaID',
                                 'GeraFinanceiro',
                                 'Valor',
                                 'Observacao',
                                 'FopValorExternoID'],
								 ['','','','','#999999999.99','',''],
								 ['','','','','(###,###,##0.00)','','']);

		alignColsInGrid(fg,[ 4]);
		gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[4,'(###,###,##0.00)','S']]);
															  
        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(0) = true;
        fg.MergeCol(1) = true;
    }
    else if (folderID == 32004) // Lancamentos contabeis
    {
        headerGrid(fg,['Lan�amento',
                        'Est',
                        'Data',
                        'Balancete',
                        'Est',
                        'Tipo',
                        'Quant',
                        'Hist�rico Padr�o',
                        'Hist�rico Complementar',
                        'Detalhe',
                        'D',
                        'C',
                        'Moeda',
                        'Total Lan�amento',
                        'Taxa',
                        'Total Convertido',
                        'Observa��o',
                        'EventoID'], [17]);
                       
        glb_aCelHint = [[0,4,'� estorno?'],
						[0,6,'Quantidade de detalhes'],
						[0,10,'N�mero de D�bitos'],
						[0,11,'N�mero de Cr�ditos'],
						[0,11,'Taxa de convers�o para moeda comum']];

        fillGridMask(fg,currDSO,['LancamentoID',
                                 'Estado',
                                 'dtLancamento',
                                 'dtBalancete',
                                 'EhEstorno',
                                 'TipoLancamento',
                                 'Quantidade',
                                 'HistoricoPadrao',
                                 'HistoricoComplementar',
                                 'Detalhe',
                                 'NumeroDebitos',
                                 'NumeroCreditos',
                                 'Moeda',
                                 'ValorTotalLancamento',
                                 'TaxaMoeda',
                                 'ValorTotalConvertido',
                                 'Observacao',
                                 'EventoID'],
                                 ['9999999999','','99/99/9999','99/99/9999','','','999999','','','','99','99','',
									'999999999.99','9999999999999.99','999999999.99','',''],
								 ['##########','',dTFormat,dTFormat,'','','######','','','','##','##','',
									'###,###,##0.00','#,###,###,###,##0.0000000','###,###,##0.00','','']);

        alignColsInGrid(fg,[0,6,10,11,13,14,15]);
        fg.FrozenCols = 2;
    }
    // Par�metros
    else if (folderID == 32005) {
        headerGrid(fg, ['VerbaID',
                       'Verba',
                       'Multiplicador',
                       'Divisor',
                       'Observa��o',
                       'FopParametroID'], [5]);

        fillGridMask(fg, currDSO, ['VerbaID',
                                 '^VerbaID^dso01GridLkp^fldID^fldName*',
                                 'Multiplicador',
                                 'Divisor',
                                 'Observacao',
                                 'FopParametroID'],
								 ['', '', '#999.99', '#999.99', '', ''],
								 ['', '', '##0.00', '##0.00', '', '']);

        alignColsInGrid(fg, [4]);

    }
    
    
    
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************
