﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modrecursoshumanos.submodfopag.fopagEx.serverside
{
    public partial class apontamentoselecao : System.Web.UI.OverflyPage 
    {

        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private Integer FopagID;

        public Integer nFopagID
        {
            get { return FopagID; } 
            set { FopagID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(ApontamentoSelecao());
        }

        protected DataSet ApontamentoSelecao()
        {
            // Roda a procedure sp_Fopag_Apontamento_Selecao
            ProcedureParameters[] procParams = new ProcedureParameters[3];

            procParams[0] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                EmpresaID != null ? (Object)EmpresaID : DBNull.Value);

            procParams[1] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                UsuarioID != null ? (Object)UsuarioID : DBNull.Value);

            procParams[2] = new ProcedureParameters(
                "@FopagID",
                System.Data.SqlDbType.Int,
                FopagID != null ? (Object)FopagID : DBNull.Value);

            return DataInterfaceObj.execQueryProcedure(
                "sp_Fopag_Apontamento_Selecao",
                procParams);
        }
    }
}