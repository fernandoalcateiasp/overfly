﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modrecursoshumanos.submodfopag.fopagEx.serverside
{
    public partial class gravaapontamento : System.Web.UI.OverflyPage
    {
        private string Mensagem = "";
        private Integer FopagID;

        public Integer nFopagID
        {
            get { return FopagID; }
            set { FopagID = value; }
        }
        private Integer PessoaID;

        public Integer nPessoaID
        {
            get { return PessoaID; }
            set { PessoaID = value; }
        }
        private Integer DataLen;

        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }
        private string[] DataApontamento;

        public string[] sDataApontamento
        {
            get { return DataApontamento; }
            set { DataApontamento = value; }
        }
        private string[] DataApontamento1;

        public string[] sDataApontamento1
        {
            get { return DataApontamento1; }
            set { DataApontamento1 = value; }
        }
        private string[] DataApontamento2;

        public string[] sDataApontamento2
        {
            get { return DataApontamento2; }
            set { DataApontamento2 = value; }
        }
        private string[] DataApontamento3;

        public string[] sDataApontamento3
        {
            get { return DataApontamento3; }
            set { DataApontamento3 = value; }
        }
        private string[] DataApontamento4;

        public string[] sDataApontamento4
        {
            get { return DataApontamento4; }
            set { DataApontamento4 = value; }
        }
        private string[] DataApontamento5;

        public string[] sDataApontamento5
        {
            get { return DataApontamento5; }
            set { DataApontamento5 = value; }
        }
        private string[] DataApontamento6;

        public string[] sDataApontamento6
        {
            get { return DataApontamento6; }
            set { DataApontamento6 = value; }
        }
        private string[] DataApontamento7;

        public string[] sDataApontamento7
        {
            get { return DataApontamento7; }
            set { DataApontamento7 = value; }
        }
        private string[] DataApontamento8;

        public string[] sDataApontamento8
        {
            get { return DataApontamento8; }
            set { DataApontamento8 = value; }
        }
        private Integer[] JustificativaID;

        public Integer[] nJustificativaID
        {
            get { return JustificativaID; }
            set { JustificativaID = value; }
        }
        private Integer[] UsuarioID;    

        public Integer[] nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }


        protected DataSet Fopag_Apontamento_Inserir()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[14];

            for (int i = 0; i < DataLen.intValue(); i++)
            {
                procParams[0] = new ProcedureParameters(
                    "@FopagID",
                    System.Data.SqlDbType.Int,
                    (FopagID != null) ? (Object)FopagID.ToString() : DBNull.Value);


                procParams[1] = new ProcedureParameters(
                    "@PessoaID",
                    System.Data.SqlDbType.Int,
                    (PessoaID != null) ? (Object)PessoaID.ToString() : DBNull.Value);


                procParams[2] = new ProcedureParameters(
                    "@DataApontamento",
                    System.Data.SqlDbType.Date,
                    (DataApontamento[i].Length > 0) ?
                        (Object)DateTime.Parse(DataApontamento[i], System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat) :
                        DBNull.Value);

                procParams[3] = new ProcedureParameters(
                    "@Apont1",
                    System.Data.SqlDbType.DateTime,
                    (DataApontamento1[i].Length > 0) ?
                        (Object)DateTime.Parse(DataApontamento1[i], System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat) :
                        DBNull.Value);

                procParams[4] = new ProcedureParameters(
                    "@Apont2",
                    System.Data.SqlDbType.DateTime,
                    (DataApontamento2[i].Length > 0) ?
                        (Object)DateTime.Parse(DataApontamento2[i], System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat) :
                        DBNull.Value);

                procParams[5] = new ProcedureParameters(
                    "@Apont3",
                    System.Data.SqlDbType.DateTime,
                    (DataApontamento3[i].Length > 0) ?
                        (Object)DateTime.Parse(DataApontamento3[i], System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat) :
                        DBNull.Value);

                procParams[6] = new ProcedureParameters(
                    "@Apont4",
                    System.Data.SqlDbType.DateTime,
                    (DataApontamento4[i].Length > 0) ?
                        (Object)DateTime.Parse(DataApontamento4[i], System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat) :
                        DBNull.Value);

                procParams[7] = new ProcedureParameters(
                    "@Apont5",
                    System.Data.SqlDbType.DateTime,
                    (DataApontamento5[i].Length > 0) ?
                        (Object)DateTime.Parse(DataApontamento5[i], System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat) :
                        DBNull.Value);


                procParams[8] = new ProcedureParameters(
                    "@Apont6",
                    System.Data.SqlDbType.DateTime,
                    (DataApontamento6[i].Length > 0) ?
                        (Object)DateTime.Parse(DataApontamento6[i], System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat) :
                        DBNull.Value);


                procParams[9] = new ProcedureParameters(
                    "@Apont7",
                    System.Data.SqlDbType.DateTime,
                    (DataApontamento7[i].Length > 0) ?
                        (Object)DateTime.Parse(DataApontamento7[i], System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat) :
                        DBNull.Value);


                procParams[10] = new ProcedureParameters(
                    "@Apont8",
                    System.Data.SqlDbType.DateTime,
                    (DataApontamento8[i].Length > 0) ?
                        (Object)DateTime.Parse(DataApontamento8[i], System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat) :
                        DBNull.Value);

                procParams[11] = new ProcedureParameters(
                    "@JustificativaID",
                    System.Data.SqlDbType.Int,
                    (JustificativaID != null) ? (Object)Convert.ToInt32(JustificativaID[i].ToString()) : DBNull.Value);

                procParams[12] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                    (UsuarioID != null) ? (Object)Convert.ToInt32(nUsuarioID[i].ToString()) : DBNull.Value);

                procParams[13] = new ProcedureParameters(
                    "@Mensagem",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[13].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_Fopag_Apontamento_Inserir",
                    procParams);

                Mensagem += procParams[13].Data.ToString();
            }

                return DataInterfaceObj.getRemoteData(
                     "select " +
                     (Mensagem != null ? "'" + Mensagem + "' " : "NULL") +
                     " as Resultado ");
            
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(Fopag_Apontamento_Inserir());


        }
    }
}