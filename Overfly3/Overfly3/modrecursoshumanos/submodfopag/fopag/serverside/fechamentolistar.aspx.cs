﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class fechamentolistar : System.Web.UI.OverflyPage
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private string controlID = Convert.ToString(HttpContext.Current.Request.Params["controlID"]);
        private int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["sEmpresaFantasia"]);
        private string glb_nUsuarioID = Convert.ToString(HttpContext.Current.Request.Params["glb_nUsuarioID"]);
        private int sLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["sLinguaLogada"]);

        private string Resultado = "";
        private Integer FopagID;

        private string Mensagem = "";
        DataSet Apontamento = new DataSet();

        public Integer nFopagID
        {
            get { return FopagID; }
            set { FopagID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private Integer GestorID;

        public Integer nGestorID
        {
            get { return GestorID; }
            set { GestorID = value; }
        }
        private Integer FuncionarioID;

        public Integer nFuncionarioID
        {
            get { return FuncionarioID; }
            set { FuncionarioID = value; }
        }
        private string Sincronizado;

        public string bSincronizado
        {
            get { return Sincronizado; }
            set { Sincronizado = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                switch (controlID)
                {
                    case "btnListar":
                        listaExcelFechamento();
                        break;

                    case "btnExcel":
                        listaExcelFechamento();
                        break;
                }
            }
        }
        public void listaExcelFechamento()
        {
            DateTime DataHoje = DateTime.Now;
            string param = "<Format>dd/mm/yyy</Format>";
            string _data = DataHoje.ToString("dd_MM_yyyy");

            Sincronizado = (Sincronizado != null ? (Sincronizado == "0" ? false : true) : false).ToString();

            if (sLinguaLogada != 246)
            {
                param = "<Format>mm/dd/yyy</Format>";
                _data = DataHoje.ToString("MM_dd_yyyy");
            }
            
            string Title = "Relatório de Fechamento_" + sEmpresaFantasia + "_" + _data;

            string strSQL = " EXEC sp_Fopag_Fechamento_Listar " + FopagID + "," + UsuarioID + "," + GestorID + "," + FuncionarioID + "," + Sincronizado + ",NULL";

            if (controlID == "btnExcel")//SE EXCEL
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                string[,] arrIndexKey = new string[,] { { "" } };

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                int Datateste = 0;
                bool nulo = false;

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, false);

                    Relatorio.CriarObjColunaNaTabela("Sincronizado", "Sincronizado", true, "Sincronizado");
                    Relatorio.CriarObjCelulaInColuna("Query", "Sincronizado", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("ID", "ID", true, "ID");
                    Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("DtAprovacaoGestor", "DtAprovacaoGestor", true, "DtAprovacaoGestor");
                    Relatorio.CriarObjCelulaInColuna("Query", "DtAprovacaoGestor", "DetailGroup", Decimais: param);

                    Relatorio.CriarObjColunaNaTabela("DtAprovacaoFunc", "DtAprovacaoFunc", true, "DtAprovacaoFunc");
                    Relatorio.CriarObjCelulaInColuna("Query", "DtAprovacaoFunc", "DetailGroup", Decimais: param);

                    Relatorio.CriarObjColunaNaTabela("VerbaID", "VerbaID", true, "VerbaID");
                    Relatorio.CriarObjCelulaInColuna("Query", "VerbaID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Verba", "Verba", true, "Verba");
                    Relatorio.CriarObjCelulaInColuna("Query", "Verba", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("ItemID", "ItemID", true, "ItemID");
                    Relatorio.CriarObjCelulaInColuna("Query", "ItemID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Evento", "Evento", true, "Evento");
                    Relatorio.CriarObjCelulaInColuna("Query", "Evento", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Unidade", "Unidade", true, "Unidade");
                    Relatorio.CriarObjCelulaInColuna("Query", "Unidade", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Numero", "Numero", true, "Numero");
                    Relatorio.CriarObjCelulaInColuna("Query", "Numero", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Total", "Total", true, "Total");
                    Relatorio.CriarObjCelulaInColuna("Query", "Total", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("FopFechamentoID", "FopFechamentoID", true, "FopFechamentoID");
                    Relatorio.CriarObjCelulaInColuna("Query", "FopFechamentoID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Mensagem", "Mensagem", true, "Mensagem");
                    Relatorio.CriarObjCelulaInColuna("Query", "Mensagem", "DetailGroup");

                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, 2);
                }
            }
            else if (controlID == "btnListar")//Se Lista
            {
                ProcedureParameters[] procParams = new ProcedureParameters[6];

                procParams[0] = new ProcedureParameters(
                    "@FopagID",
                    System.Data.SqlDbType.Int,
                    FopagID.ToString());

                procParams[1] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                    UsuarioID.ToString());

                procParams[2] = new ProcedureParameters(
                     "@GestorID",
                     System.Data.SqlDbType.Int,
                     GestorID.ToString());

                procParams[3] = new ProcedureParameters(
                    "@FuncionarioID",
                    System.Data.SqlDbType.Int,
                   FuncionarioID != null ? (Object)FuncionarioID : DBNull.Value);

                procParams[4] = new ProcedureParameters(
                     "@Sincronizado",
                     System.Data.SqlDbType.Bit,
                    Sincronizado);

                procParams[5] = new ProcedureParameters(
                      "@Mensagem",
                      System.Data.SqlDbType.VarChar,
                      DBNull.Value,
                      ParameterDirection.InputOutput);
                procParams[5].Length = 8000;

                Apontamento = DataInterfaceObj.execQueryProcedure(
                     "sp_Fopag_Fechamento_Listar",
                     procParams);

                if (procParams[5].Data != DBNull.Value)
                {
                    Resultado = " Select '" + procParams[5].Data.ToString() + "' as Mensagem ";
                    WriteResultXML(DataInterfaceObj.getRemoteData(Resultado));

                }
                else
                    WriteResultXML(Apontamento);
            }
        }
    }
}