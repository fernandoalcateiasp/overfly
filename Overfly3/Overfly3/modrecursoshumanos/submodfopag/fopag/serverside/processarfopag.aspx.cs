﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modrecursoshumanos.submodfopag.fopagEx.serverside
{
    public partial class processarfopag : System.Web.UI.OverflyPage
    {
        private string Mensagem = "''";

        private Integer fopagID;

        public Integer FopagID
        {
            get { return fopagID; }
            set { fopagID = value; }
        }
        private string reprocessaVerbasVendas;

        public string ReprocessaVerbasVendas
        {
            get { return reprocessaVerbasVendas; }
            set
            {
                reprocessaVerbasVendas = value;

                if (reprocessaVerbasVendas.ToString() == "-1")
                    reprocessaVerbasVendas = "0";

            }
        }
        private string mantemVerbasAlteradas;

        public string MantemVerbasAlteradas
        {
            get { return mantemVerbasAlteradas; }
            set
            {
                mantemVerbasAlteradas = value;

                if (mantemVerbasAlteradas.ToString() == "-1")
                    mantemVerbasAlteradas = "0";

            }
        }
        private Integer colaboradorID;

        public Integer ColaboradorID
        {
            get { return colaboradorID; }
            set
            {
                colaboradorID = value;

                if (colaboradorID.ToString() == "-1")
                    colaboradorID = null;
            }
        }
        private Integer TipoProcessamentoID;

        public Integer nTipoProcessamentoID
        {
            get { return TipoProcessamentoID; }
            set { TipoProcessamentoID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            try
            {
                ProcedureParameters[] procParams = new ProcedureParameters[6];

                procParams[0] = new ProcedureParameters(
                    "@FopagID",
                    System.Data.SqlDbType.Int,
                    FopagID.ToString());

                procParams[1] = new ProcedureParameters(
                    "@ReprocessaVerbasVendas",
                    System.Data.SqlDbType.Bit,
                   (reprocessaVerbasVendas.ToString() == "0" ? false : true));

                procParams[2] = new ProcedureParameters(
                     "@MantemVerbasAlteradas",
                     System.Data.SqlDbType.Bit,
                     (mantemVerbasAlteradas.ToString() == "0" ? false : true));

                procParams[3] = new ProcedureParameters(
                    "@ColaboradorID",
                    System.Data.SqlDbType.Int,
                    colaboradorID != null ? (Object)colaboradorID.ToString() : DBNull.Value);

                procParams[4] = new ProcedureParameters(
                     "@TipoProcessamentoID",
                     System.Data.SqlDbType.Int,
                    TipoProcessamentoID != null ? (Object)TipoProcessamentoID.ToString() : DBNull.Value);

                procParams[5] = new ProcedureParameters(
                      "@Resultado",
                      System.Data.SqlDbType.VarChar,
                      DBNull.Value,
                      ParameterDirection.InputOutput);
                procParams[5].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_Fopag_Gerador",
                    procParams);

                if (procParams[5].Data != DBNull.Value)
                {
                    Mensagem = "'" + procParams[5].Data.ToString() + "'";
                }
            }
            catch (System.Exception exception)
            {
                //Mensagem += "'" + exception.Message + "'  \r\n ";
                Mensagem = "''";
            }

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " + Mensagem + " as Resultado"
            ));
        }
    }
}