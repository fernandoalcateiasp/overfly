﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class ocorrenciasverbas : System.Web.UI.OverflyPage
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private bool bExcel = Convert.ToBoolean(HttpContext.Current.Request.Params["bExcel"]);
        private int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["sEmpresaFantasia"]);
        private int empresaID = Convert.ToInt32(HttpContext.Current.Request.Params["empresaID"]);
        private string glb_nUsuarioID = Convert.ToString(HttpContext.Current.Request.Params["glb_nUsuarioID"]);
        private int nLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["nLinguaLogada"]);
        private int nFopagID = Convert.ToInt32(HttpContext.Current.Request.Params["nFopagID"]);
        private int nPaisEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["nPaisEmpresaID"]);
        private int glb_aEmpresaData = Convert.ToInt32(HttpContext.Current.Request.Params["glb_aEmpresaData"]);
        private string chkNomeInvertido = Convert.ToString(HttpContext.Current.Request.Params["chkNomeInvertido"]);
        private string sVerbaOK = Convert.ToString(HttpContext.Current.Request.Params["sVerbaOK"]);
        private string sFiltro = Convert.ToString(HttpContext.Current.Request.Params["sFiltro"]);
        private int selVerbas = Convert.ToInt32(HttpContext.Current.Request.Params["selVerbas"]);
        private int selVerbasIndex = Convert.ToInt32(HttpContext.Current.Request.Params["selVerbasIndex"]);
        private int selFuncionarios = Convert.ToInt32(HttpContext.Current.Request.Params["selFuncionarios"]);
        private int selFuncionariosIndex = Convert.ToInt32(HttpContext.Current.Request.Params["selFuncionariosIndex"]);
        private string txtdtFopagInicio = Convert.ToString(HttpContext.Current.Request.Params["txtdtFopagInicio"]);
        private string txtdtFopagFim = Convert.ToString(HttpContext.Current.Request.Params["txtdtFopagFim"]);
        private string txtdtReferencia = Convert.ToString(HttpContext.Current.Request.Params["txtdtReferencia"]);
        private string txtReferencia = Convert.ToString(HttpContext.Current.Request.Params["txtReferencia"]);
        private string txtValor = Convert.ToString(HttpContext.Current.Request.Params["txtValor"]);
        private int selModo = Convert.ToInt32(HttpContext.Current.Request.Params["selModo"]);
        private string controlID = Convert.ToString(HttpContext.Current.Request.Params["controlID"]);
        private bool chkFuncionarioVerba = Convert.ToBoolean(HttpContext.Current.Request.Params["chkFuncionarioVerba"]);
        private bool chkFuncionariosDiretos = Convert.ToBoolean(HttpContext.Current.Request.Params["chkFuncionariosDiretos"]);
        private bool chkFuncionariosIndiretos = Convert.ToBoolean(HttpContext.Current.Request.Params["chkFuncionariosIndiretos"]);
        private bool chkPessoalComercial = Convert.ToBoolean(HttpContext.Current.Request.Params["chkPessoalComercial"]);
        private bool chkPessoalAdministrativo = Convert.ToBoolean(HttpContext.Current.Request.Params["chkPessoalAdministrativo"]);
        private bool chkPessoalEstagio = Convert.ToBoolean(HttpContext.Current.Request.Params["chkPessoalEstagio"]);
        private bool chkSohTotalVerbas = Convert.ToBoolean(HttpContext.Current.Request.Params["chkSohTotalVerbas"]);
        private string sVerbaOK2 = Convert.ToString(HttpContext.Current.Request.Params["sVerbaOK2"]);

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                switch (controlID)
                {
                    case "ocorrencias":
                        listaExcelOcorrencias();
                        break;

                    case "verbas":
                        listaExcelVerbas();
                        break;
                }
            }
        }

        public void listaExcelOcorrencias()
        {
            DateTime DataHoje = DateTime.Now;
            string param = "<Format>dd/mm/yyy</Format>";
            string _data = DataHoje.ToString("dd_MM_yyyy");

            if (nLinguaLogada != 246)
            {
                param = "<Format>mm/dd/yyy</Format>";
                _data = DataHoje.ToString("MM_dd_yyyy");
            }
                
            string Title = "Ocorrências - Fopag "+ nFopagID + " " + sEmpresaFantasia + "_" + _data;

            if (sVerbaOK == "NULL")
                sVerbaOK = "";
            else
                sVerbaOK = " AND a.VerbaOK=" + sVerbaOK;

            if (selVerbasIndex > 0)
                sFiltro += " AND a.VerbaID=" + selVerbas;

            if (selFuncionariosIndex > 0)
                sFiltro += " AND a.FuncionarioID=" + selFuncionarios;

            if (txtdtFopagInicio != "")
                sFiltro += " AND ('" + txtdtFopagInicio + "' <= a.dtFopagFim OR a.dtFopagFim IS NULL) ";

            if (txtdtFopagFim != "")
                sFiltro += " AND ('" + txtdtFopagFim + "' >= a.dtFopagInicio OR a.dtFopagInicio IS NULL) ";

            if (txtdtReferencia != "")
                sFiltro += " AND dbo.fn_Data_Zero(a.dtReferencia) = '" + txtdtReferencia + "' ";

            if (txtReferencia != "")
                sFiltro += " AND a.Referencia LIKE '%" + txtReferencia + "%' ";

            if (txtValor != "")
                sFiltro += " AND a.Valor = " + txtValor + " ";

            // Consistencia
            if (selModo == 2)
                sFiltro += " AND (dbo.fn_Fopag_OcorrenciaConsistencia(a.FopOcorrenciaID, GETDATE(), 1) IS NOT NULL) ";

            string strSQL = "SELECT a.FopOcorrenciaID, CONVERT(BIT,0) AS OK, a.FuncionarioID, " +
                                "dbo.fn_Pessoa_Fantasia(b.PessoaID, " + chkNomeInvertido +") AS Funcionario, a.VerbaID, dbo.fn_Tradutor(c.Verba, 246, " + glb_aEmpresaData + ", NULL) AS Verba, " +
                                "a.dtFopagInicio, a.dtFopagFim, a.dtReferencia, a.Referencia, a.Valor, a.VerbaOK, " +
                                "CONVERT(BIT,(CASE WHEN dbo.fn_Fopag_OcorrenciaConsistencia(a.FopOcorrenciaID, GETDATE(), 1) IS NULL THEN 1 ELSE 0 END)) AS Confirmado, " +
                                "dbo.fn_Fopag_OcorrenciaConsistencia(a.FopOcorrenciaID, GETDATE(), 100) AS Consistencia, e.PermiteAlteracao, " +
                                "ISNULL(c.TrataDataFopagFim, -1) AS TrataDataFopagFim, ISNULL(c.TrataDataFopagInicio, -1) AS TrataDataFopagInicio, " +
                                "ISNULL(c.TrataDataReferencia, -1) AS TrataDataReferencia, ISNULL(c.TrataReferencia,-1) AS TrataReferencia, " +
                                "ISNULL(c.TrataValor,-1) AS TrataValor, " +
                                "b.Nome AS FuncNome " +
                                "FROM Fopag_Ocorrencias a WITH(NOLOCK), Pessoas b WITH(NOLOCK), VerbasFopag c WITH(NOLOCK), Fopag d WITH(NOLOCK), VerbasFopag_Paises e WITH(NOLOCK) " +
                                "WHERE a.FuncionarioID = b.PessoaID AND a.VerbaID=c.VerbaID AND a.FopagID=d.FopagID AND " +
                                    "d.EmpresaID=" + empresaID + " AND c.VerbaID=e.VerbaID AND e.PaisID=" + nPaisEmpresaID + " AND " +
                                    "dbo.fn_Fopag_Filtro(" + nFopagID + ", a.FopOcorrenciaID, 2) = 1 " + sVerbaOK + sFiltro;
            
            if (chkFuncionarioVerba) {
                    if (nPaisEmpresaID != 130)
                    strSQL += "ORDER BY Funcionario, a.VerbaID";
                    else
                    strSQL += "ORDER BY b.Nome, a.VerbaID";
                }
	        else {
                    if (nPaisEmpresaID != 130)
                    strSQL += "ORDER BY a.VerbaID, Funcionario";
                    else
                    strSQL += "ORDER BY a.VerbaID, b.Nome";
                }

                if (bExcel)//SE EXCEL
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                string[,] arrIndexKey = new string[,] { { "" } };

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                int Datateste = 0;
                bool nulo = false;

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, false);

                    Relatorio.CriarObjColunaNaTabela("FopOcorrenciaID", "FopOcorrenciaID", true, "FopOcorrenciaID");
                    Relatorio.CriarObjCelulaInColuna("Query", "FopOcorrenciaID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("OK", "OK", true, "OK");
                    Relatorio.CriarObjCelulaInColuna("Query", "OK", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("FuncionarioID", "FuncionarioID", true, "FuncionarioID");
                    Relatorio.CriarObjCelulaInColuna("Query", "FuncionarioID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Funcionario", "Funcionario", true, "Funcionario");
                    Relatorio.CriarObjCelulaInColuna("Query", "Funcionario", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("VerbaID", "VerbaID", true, "VerbaID");
                    Relatorio.CriarObjCelulaInColuna("Query", "VerbaID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Verba", "Verba", true, "Verba");
                    Relatorio.CriarObjCelulaInColuna("Query", "Verba", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("dtFopagInicio", "dtFopagInicio", true, "dtFopagInicio");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtFopagInicio", "DetailGroup", Decimais: param);

                    Relatorio.CriarObjColunaNaTabela("dtFopagFim", "dtFopagFim", true, "dtFopagFim");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtFopagFim", "DetailGroup", Decimais: param);

                    Relatorio.CriarObjColunaNaTabela("dtReferencia", "dtReferencia", true, "dtReferencia");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtReferencia", "DetailGroup", Decimais: param);

                    Relatorio.CriarObjColunaNaTabela("Referencia", "Referencia", true, "Referencia");
                    Relatorio.CriarObjCelulaInColuna("Query", "Referencia", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Valor", "Valor", true, "Valor");
                    Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("VerbaOK", "VerbaOK", true, "VerbaOK");
                    Relatorio.CriarObjCelulaInColuna("Query", "VerbaOK", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Confirmado", "Confirmado", true, "Confirmado");
                    Relatorio.CriarObjCelulaInColuna("Query", "Confirmado", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Consistencia", "Consistencia", true, "Consistencia");
                    Relatorio.CriarObjCelulaInColuna("Query", "Consistencia", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("PermiteAlteracao", "PermiteAlteracao", true, "PermiteAlteracao");
                    Relatorio.CriarObjCelulaInColuna("Query", "PermiteAlteracao", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("TrataDataFopagFim", "TrataDataFopagFim", true, "TrataDataFopagFim");
                    Relatorio.CriarObjCelulaInColuna("Query", "TrataDataFopagFim", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("TrataDataFopagInicio", "TrataDataFopagInicio", true, "TrataDataFopagInicio");
                    Relatorio.CriarObjCelulaInColuna("Query", "TrataDataFopagInicio", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("TrataDataReferencia", "TrataDataReferencia", true, "TrataDataReferencia");
                    Relatorio.CriarObjCelulaInColuna("Query", "TrataDataReferencia", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("TrataReferencia", "TrataReferencia", true, "TrataReferencia");
                    Relatorio.CriarObjCelulaInColuna("Query", "TrataReferencia", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("TrataValor", "TrataValor", true, "TrataValor");
                    Relatorio.CriarObjCelulaInColuna("Query", "TrataValor", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("FuncNome", "FuncNome", true, "FuncNome");
                    Relatorio.CriarObjCelulaInColuna("Query", "FuncNome", "DetailGroup");

                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, 2);
                }
            }
            else//Se Lista
            {
                DataSet dtTeste = DataInterfaceObj.getRemoteData(strSQL);
                WriteResultXML(dtTeste);
            }
        }
        public void listaExcelVerbas()
        {
            DateTime DataHoje = DateTime.Now;
            string param = "<Format>dd/mm/yyy</Format>";
            string _data = DataHoje.ToString("dd_MM_yyyy");
            string sFuncionarioNome = "";
            string sFuncionarioDireto = "";
            string sPessoal = "";

            if (nLinguaLogada != 246)
            {
                param = "<Format>mm/dd/yyy</Format>";
                _data = DataHoje.ToString("MM_dd_yyyy");
            }

            string Title = "Verbas - Fopag " + nFopagID + " " + sEmpresaFantasia + "_" + _data;

            string strSQL = "";

            if (selVerbasIndex > 0)
                sFiltro += " AND a.VerbaID=" + selVerbas;

            if (selFuncionariosIndex > 0)
                sFiltro += " AND a.FuncionarioID=" + selFuncionarios;

            if (txtdtReferencia != "")
                sFiltro += " AND dbo.fn_Data_Zero(a.dtReferencia) = '" + txtdtReferencia + "' ";

            if (txtReferencia != "")
                sFiltro += " AND a.Referencia LIKE '%" + txtReferencia + "%' ";

            if (txtValor != "")
                sFiltro += " AND a.Valor = " + txtValor + " ";

            if (!(chkFuncionariosDiretos && chkFuncionariosIndiretos))
            {
                if (chkFuncionariosDiretos)
                {
                    sFiltro += " AND dbo.fn_Fopag_Funcionario(a.FopagID, a.FuncionarioID, 1) = 1 ";
                }
                else if (chkFuncionariosIndiretos)
                {
                    sFiltro += " AND dbo.fn_Fopag_Funcionario(a.FopagID, a.FuncionarioID, 1) = 0 ";
                }
            }

            if (!(chkPessoalComercial && chkPessoalAdministrativo && chkPessoalEstagio))
            {
                if (chkPessoalComercial)
                {
                    sFiltro += ((chkPessoalAdministrativo || chkPessoalEstagio) ? " AND (" : " AND ") + "dbo.fn_Fopag_Funcionario(a.FopagID, a.FuncionarioID, 3) IN (1, 2) ";
                }
                //Somente funcionarios do setor administrativo
                if (chkPessoalAdministrativo)
                {
                    if (chkPessoalComercial)
                        sFiltro += " OR ";
                    else
                        sFiltro += ((chkPessoalComercial || chkPessoalEstagio) ? " AND (" : " AND ");

                    if (empresaID == 10)
                        sFiltro += "dbo.fn_Fopag_Funcionario(a.FopagID, a.FuncionarioID, 2) IN (635, 666, 667, 672, 690, 696, 752) AND a.funcionarioID NOT IN (1667, 1774) ";
                    else
                        sFiltro += "dbo.fn_Fopag_Funcionario(a.FopagID, a.FuncionarioID, 2) IN (635, 666, 667, 672, 696, 752)";

                    if (chkPessoalComercial)
                        sFiltro += ")";
                }
                if (chkPessoalEstagio)
                {
                    if (empresaID == 10)
                        sFiltro += ((chkPessoalComercial || chkPessoalAdministrativo) ? " OR " : " AND ") + "(dbo.fn_Fopag_Funcionario(a.FopagID, a.FuncionarioID, 2) IN (639, 677, 680, 689, 696,750, 753, 762) OR a.FuncionarioID IN (1774)) AND FuncionarioID NOT IN (1467) ";
                    else
                        sFiltro += ((chkPessoalComercial || chkPessoalAdministrativo) ? " OR " : " AND ") + "dbo.fn_Fopag_Funcionario(a.FopagID, a.FuncionarioID, 2) = 759 ";

                    if (chkPessoalComercial || chkPessoalAdministrativo)
                        sFiltro += ")";
                }
            }

            if (chkSohTotalVerbas)
            {
                if (chkFuncionarioVerba)
                {
                    strSQL = "SELECT DISTINCT a.FuncionarioID, dbo.fn_Pessoa_Fantasia(b.PessoaID, " + chkNomeInvertido +") AS Funcionario, ";
                    sFuncionarioNome = " b.Nome AS FuncNome ";
                }
                else
                {
                    strSQL = "SELECT DISTINCT 0 AS FuncionarioID, SPACE(0) AS Funcionario, ";
                    sFuncionarioNome = " NULL AS FuncNome ";
                }
            }
            else
            {
                strSQL = "SELECT DISTINCT a.FuncionarioID, dbo.fn_Pessoa_Fantasia(b.PessoaID, " + chkNomeInvertido +") AS Funcionario, ";
                sFuncionarioNome = " b.Nome AS FuncNome ";
            }

            if ((chkFuncionariosDiretos) && (chkFuncionariosIndiretos))
                sFuncionarioDireto = "NULL";
            else if (chkFuncionariosDiretos)
                sFuncionarioDireto = "1";
            else
                sFuncionarioDireto = "0";

            if ((chkPessoalComercial) && (chkPessoalAdministrativo))
                sPessoal = "NULL";
            else if (chkPessoalAdministrativo)
                sPessoal = "0";
            else
                sPessoal = "2";

            if (chkFuncionarioVerba)
                strSQL += "dbo.fn_Fopag_Totais(" + nFopagID + ", a.FuncionarioID, " + sFuncionarioDireto + ", NULL, NULL, " + (sVerbaOK2 == null ? "NULL": sVerbaOK2) + ", 1, 1) AS Total, ";
            else
                strSQL += "dbo.fn_Fopag_Totais(" + nFopagID + ", NULL, " + sFuncionarioDireto + ", " + sPessoal + ", a.VerbaID, " + (sVerbaOK2 == null ? "NULL" : sVerbaOK2) + ", 1, 1) AS Total, ";

            // Lista somente o valor liquido total de verbas por funcionario
            if (chkSohTotalVerbas)
            {
                if (chkFuncionarioVerba)
                    strSQL += "0 AS FopVerbaID, 0 AS VerbaID, SPACE(0) AS Verba, ";
                else
                    strSQL += "0 AS FopVerbaID, a.VerbaID, dbo.fn_Tradutor(c.Verba, 246, " + glb_aEmpresaData + ", NULL) AS Verba, ";

                strSQL += "CONVERT(BIT,0) AS OK, NULL AS dtReferencia, NULL AS Referencia, 0 AS Valor, " +
                                "CONVERT(BIT,0) AS VerbaOK, CONVERT(BIT,0) AS AlteracaoManual, CONVERT(BIT,0) AS PermiteAlteracao, " +
                                "-1 AS TrataDataReferencia, -1 AS TrataReferencia, -1 AS TrataValor, " + sFuncionarioNome +
                            "FROM Fopag_Verbas a WITH(NOLOCK), Pessoas b WITH(NOLOCK), VerbasFopag c WITH(NOLOCK) " +
                            "WHERE a.FopagID=" + nFopagID + " AND a.FuncionarioID = b.PessoaID AND a.VerbaID=c.VerbaID " +
                            sVerbaOK + sFiltro + " ";
            }
            else
            {
                strSQL += "a.FopVerbaID, a.VerbaID, dbo.fn_Tradutor(c.Verba, 246, " + glb_aEmpresaData + ", NULL) AS Verba, CONVERT(BIT,0) AS OK, " +
                                "a.dtReferencia, a.Referencia, a.Valor, a.VerbaOK, a.AlteracaoManual, d.PermiteAlteracao, " +
                                "ISNULL(c.TrataDataReferencia, -1) AS TrataDataReferencia, " +
                                "ISNULL(c.TrataReferencia, -1) AS TrataReferencia, ISNULL(c.TrataValor, -1) AS TrataValor, " +
                                sFuncionarioNome +
                            "FROM Fopag_Verbas a WITH(NOLOCK), Pessoas b WITH(NOLOCK), VerbasFopag c WITH(NOLOCK), VerbasFopag_Paises d WITH(NOLOCK) " +
                            "WHERE a.FopagID=" + nFopagID + " AND a.FuncionarioID = b.PessoaID AND a.VerbaID=c.VerbaID AND " +
                            "c.VerbaID=d.VerbaID AND d.PaisID=" + nPaisEmpresaID + " " + sVerbaOK + sFiltro + " ";
            }

            if (chkFuncionarioVerba)
            {
                if (nPaisEmpresaID != 130)
                    strSQL += "ORDER BY Funcionario, VerbaID";
                else
                    strSQL += "ORDER BY b.Nome, VerbaID";
            }
            else
            {
                if (nPaisEmpresaID != 130)
                    strSQL += "ORDER BY VerbaID, Funcionario";
                else
                    strSQL += "ORDER BY VerbaID, b.Nome";
            }
            

            if (bExcel)//SE EXCEL
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                string[,] arrIndexKey = new string[,] { { "" } };

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                int Datateste = 0;
                bool nulo = false;

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, false);

                    Relatorio.CriarObjColunaNaTabela("FuncionarioID", "FuncionarioID", true, "FuncionarioID");
                    Relatorio.CriarObjCelulaInColuna("Query", "FuncionarioID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Funcionario", "Funcionario", true, "Funcionario");
                    Relatorio.CriarObjCelulaInColuna("Query", "Funcionario", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Total", "Total", true, "Total");
                    Relatorio.CriarObjCelulaInColuna("Query", "Total", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("FopVerbaID", "FopVerbaID", true, "FopVerbaID");
                    Relatorio.CriarObjCelulaInColuna("Query", "FopVerbaID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("VerbaID", "VerbaID", true, "VerbaID");
                    Relatorio.CriarObjCelulaInColuna("Query", "VerbaID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Verba", "Verba", true, "Verba");
                    Relatorio.CriarObjCelulaInColuna("Query", "Verba", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("OK", "OK", true, "OK");
                    Relatorio.CriarObjCelulaInColuna("Query", "OK", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("dtReferencia", "dtReferencia", true, "dtReferencia");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtReferencia", "DetailGroup", Decimais: param);

                    Relatorio.CriarObjColunaNaTabela("Referencia", "Referencia", true, "Referencia");
                    Relatorio.CriarObjCelulaInColuna("Query", "Referencia", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Valor", "Valor", true, "Valor");
                    Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("VerbaOK", "VerbaOK", true, "VerbaOK");
                    Relatorio.CriarObjCelulaInColuna("Query", "VerbaOK", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("AlteracaoManual", "AlteracaoManual", true, "AlteracaoManual");
                    Relatorio.CriarObjCelulaInColuna("Query", "AlteracaoManual", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("PermiteAlteracao", "PermiteAlteracao", true, "PermiteAlteracao");
                    Relatorio.CriarObjCelulaInColuna("Query", "PermiteAlteracao", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("TrataDataReferencia", "TrataDataReferencia", true, "TrataDataReferencia");
                    Relatorio.CriarObjCelulaInColuna("Query", "TrataDataReferencia", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("TrataReferencia", "TrataReferencia", true, "TrataReferencia");
                    Relatorio.CriarObjCelulaInColuna("Query", "TrataReferencia", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("TrataValor", "TrataValor", true, "TrataValor");
                    Relatorio.CriarObjCelulaInColuna("Query", "TrataValor", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("FuncNome", "FuncNome", true, "FuncNome");
                    Relatorio.CriarObjCelulaInColuna("Query", "FuncNome", "DetailGroup");

                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, 2);
                }
            }
            else//Se Lista
            {
                DataSet dtTeste = DataInterfaceObj.getRemoteData(strSQL);
                WriteResultXML(dtTeste);
            }
        }
    }
}
