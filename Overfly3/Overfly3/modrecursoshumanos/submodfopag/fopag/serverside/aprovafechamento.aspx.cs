﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modrecursoshumanos.submodfopag.fopagEx.serverside
{
    public partial class aprovafechamento : System.Web.UI.OverflyPage
    {
        private string Mensagem;
        private Integer FopagID;

        public Integer nFopagID
        {
            get { return FopagID; }
            set { FopagID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private Integer FuncionarioID;

        public Integer nFuncionarioID
        {
            get { return FuncionarioID; }
            set { FuncionarioID = value; }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            ProcedureParameters[] procParams = new ProcedureParameters[4];

            procParams[0] = new ProcedureParameters(
                "@FopagID",
                System.Data.SqlDbType.Int,
                FopagID.ToString());
            
            procParams[1] = new ProcedureParameters(
                "@FuncionarioID",
                System.Data.SqlDbType.Int,
                FuncionarioID.ToString());

            procParams[2] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                UsuarioID.ToString());

            procParams[3] = new ProcedureParameters(
                  "@Mensagem",
                  System.Data.SqlDbType.VarChar,
                  DBNull.Value,
                  ParameterDirection.InputOutput);
            procParams[3].Length = 8000;


            DataInterfaceObj.execNonQueryProcedure(
            "sp_Fopag_AprovaFechamento",
            procParams);

            Mensagem = procParams[3].Data.ToString();
            
            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select '" + Mensagem + "' as Mensagem"
            ));

        }
    }
}