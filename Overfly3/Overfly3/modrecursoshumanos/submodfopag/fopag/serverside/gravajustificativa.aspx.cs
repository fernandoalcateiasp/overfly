﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modrecursoshumanos.submodfopag.fopagEx.serverside
{
    public partial class gravajustificativa : System.Web.UI.OverflyPage
    {

        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private Integer PessoaID;

        public Integer nPessoaID
        {
            get { return PessoaID; }
            set { PessoaID = value; }
        }
        private string DataApontamento;

        public string sDataApontamento
        {
            get { return DataApontamento; }
            set { DataApontamento = value; }
        }
        private Integer Falta;

        public Integer nFalta
        {
            get { return Falta; }
            set { Falta = value; }
        }
        private string Atraso;

        public string sAtraso
        {
            get { return Atraso; }
            set { Atraso = value; }
        }
        private Integer JustificativaID;

        public Integer nJustificativaID
        {
            get { return JustificativaID; }
            set { JustificativaID = value; }
        }
        private Integer FopagID;

        public Integer nFopagID
        {
            get { return FopagID; }
            set { FopagID = value; }
        }


        protected override void PageLoad(object sender, EventArgs e)
        {
            ProcedureParameters[] procParams = new ProcedureParameters[6];

            procParams[0] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                EmpresaID != null ? (Object)EmpresaID.ToString() : DBNull.Value );

            procParams[1] = new ProcedureParameters(
                "@PessoaID",
                System.Data.SqlDbType.Int,
                PessoaID != null ? (Object)PessoaID.ToString() : DBNull.Value);

            procParams[2] = new ProcedureParameters(
                 "@DataApontamento",
                 System.Data.SqlDbType.Date,
                (DataApontamento != null) ?
                (Object)DateTime.Parse(DataApontamento, System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat) :
                DBNull.Value);

            procParams[3] = new ProcedureParameters(
                "@Falta",
                System.Data.SqlDbType.Int,
                Falta != null ? (Object)Falta.ToString(): DBNull.Value);

            procParams[4] = new ProcedureParameters(
                 "@Atraso",
                 System.Data.SqlDbType.VarChar,
                 Atraso != null ? (Object)Atraso.ToString() : DBNull.Value);

            procParams[5] = new ProcedureParameters(
                 "@JustificativaID",
                 System.Data.SqlDbType.Int,
                 JustificativaID != null ? (Object)JustificativaID.ToString() : DBNull.Value);

            DataInterfaceObj.execNonQueryProcedure(
                "sp_insere_justificativa_dimep",
                procParams);

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select 0 as Resultado, 0 as Resultado2"
            ));

        }
    }
}