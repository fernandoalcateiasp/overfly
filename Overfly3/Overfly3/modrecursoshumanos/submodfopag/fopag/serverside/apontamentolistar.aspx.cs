﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class apontamentolistar : System.Web.UI.OverflyPage
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private string controlID = Convert.ToString(HttpContext.Current.Request.Params["controlID"]);
        private int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["sEmpresaFantasia"]);
        private string glb_nUsuarioID = Convert.ToString(HttpContext.Current.Request.Params["glb_nUsuarioID"]);
        private int sLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["sLinguaLogada"]);

        private string Resultado = "";
        private Integer FopagID;

        public Integer nFopagID
        {
            get { return FopagID; }
            set { FopagID = value; }
        }
        private Integer FuncionarioID;

        public Integer nFuncionarioID
        {
            get { return FuncionarioID; }
            set { FuncionarioID = value; }
        }
        private string DataInicio;

        public string sDataInicio
        {
            get { return DataInicio; }
            set { DataInicio = value; }
        }
        private string DataFim;

        public string sDataFim
        {
            get { return DataFim; }
            set { DataFim = value; }
        }
        private string Inconsistencia;

        public string sInconsistencia
        {
            get { return Inconsistencia; }
            set { Inconsistencia = value; }
        }


        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                switch (controlID)
                {
                    case "btnListar":
                        listaExcelApontamento();
                        break;

                    case "btnExcel":
                        listaExcelApontamento();
                        break;
                }
            }
        }

        public void listaExcelApontamento()
        {
            DateTime DataHoje = DateTime.Now;
            string param = "<Format>dd/mm/yyy</Format>";
            string _data = DataHoje.ToString("dd_MM_yyyy");

            if (sLinguaLogada != 246)
            {
                param = "<Format>mm/dd/yyy</Format>";
                _data = DataHoje.ToString("MM_dd_yyyy");
            }
                
            string Title = "Relatório de Apontamentos_"+ sEmpresaFantasia + "_" + _data;

            string strSQL = " EXEC sp_Fopag_Apontamento_Listar " + FopagID + "," + FuncionarioID + ",'" + DataInicio + "','" + DataFim + "'," + Inconsistencia + ",NULL";

            if (controlID == "btnExcel")//SE EXCEL
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                string[,] arrIndexKey = new string[,] { { "" } };

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                int Datateste = 0;
                bool nulo = false;

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, false);

                    Relatorio.CriarObjColunaNaTabela("FuncionarioID", "FuncionarioID", true, "FuncionarioID");
                    Relatorio.CriarObjCelulaInColuna("Query", "FuncionarioID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Colaborador", "Colaborador", true, "Colaborador");
                    Relatorio.CriarObjCelulaInColuna("Query", "Colaborador", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Justificativa", "Justificativa", true, "Justificativa");
                    Relatorio.CriarObjCelulaInColuna("Query", "Justificativa", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("DataApontamento", "DataApontamento", true, "DataApontamento");
                    Relatorio.CriarObjCelulaInColuna("Query", "DataApontamento", "DetailGroup", Decimais:param);

                    Relatorio.CriarObjColunaNaTabela("EhDiadeTrabalho", "EhDiadeTrabalho", true, "EhDiadeTrabalho");
                    Relatorio.CriarObjCelulaInColuna("Query", "EhDiadeTrabalho", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Apontamento1", "Apontamento1", true, "Apontamento1");
                    Relatorio.CriarObjCelulaInColuna("Query", "Apontamento1", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Apontamento2", "Apontamento2", true, "Apontamento2");
                    Relatorio.CriarObjCelulaInColuna("Query", "Apontamento2", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Apontamento3", "Apontamento3", true, "Apontamento3");
                    Relatorio.CriarObjCelulaInColuna("Query", "Apontamento3", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Apontamento4", "Apontamento4", true, "Apontamento4");
                    Relatorio.CriarObjCelulaInColuna("Query", "Apontamento4", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Apontamento5", "Apontamento5", true, "Apontamento5");
                    Relatorio.CriarObjCelulaInColuna("Query", "Apontamento5", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Apontamento6", "Apontamento6", true, "Apontamento6");
                    Relatorio.CriarObjCelulaInColuna("Query", "Apontamento6", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Apontamento7", "Apontamento7", true, "Apontamento7");
                    Relatorio.CriarObjCelulaInColuna("Query", "Apontamento7", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Apontamento8", "Apontamento8", true, "Apontamento8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Apontamento8", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("HoraExtra", "HoraExtra", true, "HoraExtra");
                    Relatorio.CriarObjCelulaInColuna("Query", "HoraExtra", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("AdicNoturno", "AdicNoturno", true, "AdicNoturno");
                    Relatorio.CriarObjCelulaInColuna("Query", "AdicNoturno", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Falta", "Falta", true, "Falta");
                    Relatorio.CriarObjCelulaInColuna("Query", "Falta", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Atraso", "Atraso", true, "Atraso");
                    Relatorio.CriarObjCelulaInColuna("Query", "Atraso", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("DSR", "DSR", true, "DSR");
                    Relatorio.CriarObjCelulaInColuna("Query", "DSR", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("HoraExtraAbono", "HoraExtraAbono", true, "HoraExtraAbono");
                    Relatorio.CriarObjCelulaInColuna("Query", "HoraExtraAbono", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("AdicNoturnoAbono", "AdicNoturnoAbono", true, "AdicNoturnoAbono");
                    Relatorio.CriarObjCelulaInColuna("Query", "AdicNoturnoAbono", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("FaltaAbono", "FaltaAbono", true, "FaltaAbono");
                    Relatorio.CriarObjCelulaInColuna("Query", "FaltaAbono", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("AtrasoAbono", "AtrasoAbono", true, "AtrasoAbono");
                    Relatorio.CriarObjCelulaInColuna("Query", "AtrasoAbono", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("DSRAbono", "DSRAbono", true, "DSRAbono");
                    Relatorio.CriarObjCelulaInColuna("Query", "DSRAbono", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Inconsistencia", "Inconsistencia", true, "Inconsistencia");
                    Relatorio.CriarObjCelulaInColuna("Query", "Inconsistencia", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("FopApontamentoID", "FopApontamentoID", true, "FopApontamentoID");
                    Relatorio.CriarObjCelulaInColuna("Query", "FopApontamentoID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Mensagem", "Mensagem", true, "Mensagem");
                    Relatorio.CriarObjCelulaInColuna("Query", "Mensagem", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("DiferencaAlmoco", "DiferencaAlmoco", true, "DiferencaAlmoco");
                    Relatorio.CriarObjCelulaInColuna("Query", "DiferencaAlmoco", "DetailGroup");

                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, 2);
                }
            }
            else if (controlID == "btnListar")//Se Lista
            {
                DataSet Apontamento = new DataSet();
                ProcedureParameters[] procParams = new ProcedureParameters[6];

                procParams[0] = new ProcedureParameters(
                    "@FopagID",
                    System.Data.SqlDbType.Int,
                    FopagID != null ? (Object)FopagID.ToString() : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                    "@FuncionarioID",
                    System.Data.SqlDbType.Int,
                    FuncionarioID != null ? (Object)FuncionarioID.ToString() : DBNull.Value);

                procParams[2] = new ProcedureParameters(
                    "@dtApuracaoInicio",
                    System.Data.SqlDbType.Date,
                    DataInicio != null ? (Object)DataInicio.ToString() : DBNull.Value);

                procParams[3] = new ProcedureParameters(
                    "@dtApuracaoFim",
                    System.Data.SqlDbType.Date,
                    DataFim != null ? (Object)DataFim.ToString() : DBNull.Value);

                procParams[4] = new ProcedureParameters(
                    "@Inconsistencia",
                    System.Data.SqlDbType.VarChar,
                    Inconsistencia != null ? (Object)Inconsistencia.ToString() : DBNull.Value);

                procParams[5] = new ProcedureParameters(
                   "@Mensagem",
                   System.Data.SqlDbType.VarChar,
                   System.DBNull.Value,
                   ParameterDirection.Output);
                procParams[5].Length = 8000;

                Apontamento = DataInterfaceObj.execQueryProcedure(
                                  "sp_Fopag_Apontamento_Listar",
                                  procParams);

                if (procParams[5].Data != DBNull.Value)
                {
                    Resultado = " Select '" + procParams[5].Data.ToString() + "' as Mensagem ";
                    WriteResultXML(DataInterfaceObj.getRemoteData(Resultado));

                }
                else
                    WriteResultXML(Apontamento);
            }
        }
    }
}
