﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_fopag : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int mEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        private string mEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);
        private int formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string glb_nFopagID = Convert.ToString(HttpContext.Current.Request.Params["glb_nFopagID"]);
        private Boolean chkGeraArquivo = Convert.ToBoolean(HttpContext.Current.Request.Params["chkGeraArquivo"]);
        private string TipoID = Convert.ToString(HttpContext.Current.Request.Params["TipoID"]);
        private string nomeRel = Convert.ToString(HttpContext.Current.Request.Params["nomeRel"]);

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                if (sRelatorioID == "40308" || sRelatorioID == "40309" || sRelatorioID == "40310" || sRelatorioID == "40311" || sRelatorioID == "40312" || sRelatorioID == "40313" || sRelatorioID == "40314" || sRelatorioID == "40316")
                {
                    RelatoriosFopag();
                }

            }
        }

        public void RelatoriosFopag()
        {
            // Excel
            int Formato = 2;
            string Title = nomeRel;

            string strSQL = "EXEC sp_Fopag_Relatorio " + glb_nFopagID + ", " + chkGeraArquivo + ", " + TipoID;

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "2.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "5.13834", "0.0", "11", "#0113a0", "B", Header_Body, "7.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "13.0", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }
    }
}
