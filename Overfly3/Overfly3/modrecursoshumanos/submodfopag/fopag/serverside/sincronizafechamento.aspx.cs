﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modrecursoshumanos.submodfopag.fopagEx.serverside
{
    public partial class sincronizafechamento : System.Web.UI.OverflyPage
    {
        private Integer fopagID;

        public Integer FopagID
        {
            get { return fopagID; }
            set { fopagID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        string Mensagem;

        protected override void PageLoad(object sender, EventArgs e)
        {

            string Resultado = "";

            ProcedureParameters[] procParams = new ProcedureParameters[3];

            procParams[0] = new ProcedureParameters(
                "@FopagID",
                System.Data.SqlDbType.Int,
                FopagID.ToString());

            procParams[1] = new ProcedureParameters(
                  "@FuncionarioID",
                  System.Data.SqlDbType.Int,
                  UsuarioID.ToString());

            procParams[2] = new ProcedureParameters(
                  "@Resultado",
                  System.Data.SqlDbType.VarChar,
                  DBNull.Value,
                  ParameterDirection.InputOutput);
            procParams[2].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Fopag_SincronizaFechamento",
                procParams);

            Resultado = procParams[2].ToString();

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " + Resultado + " as Mensagem"
            ));
        }
    }
}