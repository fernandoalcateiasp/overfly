﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modrecursoshumanos.submodfopag.fopagEx.serverside
{
    public partial class alterafuncionarios : System.Web.UI.OverflyPage
    {

        private Integer dataLen;

        public Integer nDataLen
        {
            get { return dataLen; }
            set { dataLen = value; }
        }
        private Integer nFopgaID;

        public Integer FopgaID
        {
            get { return nFopgaID; }
            set { nFopgaID = value; }
        }

        private Integer[] fopFuncionarioID;

        public Integer[] FopFuncionarioID
        {
            get { return fopFuncionarioID; }
            set { fopFuncionarioID = value; }
        }
        private string[] metaSalario;

        public string[] MetaSalario
        {
            get { return metaSalario; }
            set { metaSalario = value; }
        }


        private string[] metaFaturamento;

        public string[] MetaFaturamento
        {
            get { return metaFaturamento; }
            set { metaFaturamento = value; }
        }
        private string[] metaContribuicao;

        public string[] MetaContribuicao
        {
            get { return metaContribuicao; }
            set { metaContribuicao = value; }
        }
        private string[] repasse;

        public string[] Repasse
        {
            get { return repasse; }
            set { repasse = value; }
        }
        private string[] avaliacao;

        public string[] Avaliacao
        {
            get { return avaliacao; }
            set { avaliacao = value; }
        }
        private string[] observacao;

        public string[] Observacao
        {
            get { return observacao; }
            set { observacao = value; }
        }
        private string[] observacoes;

        public string[] Observacoes
        {
            get { return observacoes; }
            set { observacoes = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            string strSQL = "";
            string Error = "";
            for (int i = 0; i < dataLen.intValue(); i++)
            {

                strSQL = "UPDATE Fopag_Funcionarios SET MetaSalario=" + (metaSalario[i]) + ", " +
                   "MetaFaturamento=" + (metaFaturamento[i]) + ", " +
                   "MetaContribuicao=" + (metaContribuicao[i]) + ", " +
                   "RepasseContribuicao=" + (repasse[i]) + ", " +
                   "Avaliacao=" + (avaliacao[i]) + ", " +
                   "Observacao=" + (observacao[i]) + ", " +
                   "Observacoes=" + (observacoes[i]) + " " +
                   "WHERE FopFuncionarioID=" + (fopFuncionarioID[i]);
                try
                {

                    int linhas = DataInterfaceObj.ExecuteSQLCommand(strSQL);

                }
                catch (System.Exception exception)
                {
                    Error += exception.Message + " as fldErrorText, " +
                         " 0 as fldErrorNumber ";

                }

            }
            WriteResultXML(DataInterfaceObj.getRemoteData(
                         "select " + (Error == "" ? "'' as fldErrorText, 0 as fldErrorNumber " : Error)));

        }
    }
}