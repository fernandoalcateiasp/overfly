﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modrecursoshumanos.submodfopag.fopagEx.serverside
{
    public partial class resetfuncionarios : System.Web.UI.OverflyPage
    {
        private Integer fopagID;

        public Integer FopagID
        {
            get { return fopagID; }
            set { fopagID = value; }
        }
        private string resetAvaliacao;

        public string ResetAvaliacao
        {
            get { return resetAvaliacao; }
            set { resetAvaliacao = value; }
        }
        private string resetMetasVendas;

        public string ResetMetasVendas
        {
            get { return resetMetasVendas; }
            set { resetMetasVendas = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            string Resultado = "";

            ProcedureParameters[] procParams = new ProcedureParameters[4];

            procParams[0] = new ProcedureParameters(
                "@FopagID",
                System.Data.SqlDbType.Int,
                FopagID.ToString());

            procParams[1] = new ProcedureParameters(
                "@ResetAvaliacao",
                System.Data.SqlDbType.Bit,
               ((Object)resetAvaliacao.ToString() == "0" ? false : true));

            procParams[2] = new ProcedureParameters(
                 "@ResetMetasVendas",
                 System.Data.SqlDbType.Bit,
                 ((Object)resetMetasVendas.ToString() == "0" ? false : true));

            procParams[3] = new ProcedureParameters(
                  "@Resultado",
                  System.Data.SqlDbType.VarChar,
                  DBNull.Value,
                  ParameterDirection.InputOutput);
            procParams[3].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Fopag_Funcionarios",
                procParams);

            Resultado = procParams[3].Data.ToString();

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select '" + Resultado + "' as Resultado"
            ));
        }
    }
}