﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modrecursoshumanos.submodfopag.fopagEx.serverside
{
    public partial class alteraocorrencias : System.Web.UI.OverflyPage
    {
        private Integer DataLen;

        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }
        private string Delete;

        public string bDelete
        {
            get { return Delete; }
            set { Delete = (value == "0" ? "false" : "true"); }
        }
        private Integer fopagID;

        public Integer FopagID
        {
            get { return fopagID; }
            set { fopagID = value; }
        }
        private Integer[] fopOcorrenciaID;

        public Integer[] FopOcorrenciaID
        {
            get { return fopOcorrenciaID; }
            set { fopOcorrenciaID = value; }
        }
        private Integer[] funcionarioID;

        public Integer[] FuncionarioID
        {
            get { return funcionarioID; }
            set { funcionarioID = value; }
        }
        private string[] verbaID;

        public string[] VerbaID
        {
            get { return verbaID; }
            set { verbaID = value; }
        }
        private string[] DtFopagInicio;

        public string[] dtFopagInicio
        {
            get { return DtFopagInicio; }
            set { DtFopagInicio = value; }
        }
        private string[] DtFopagFim;

        public string[] dtFopagFim
        {
            get { return DtFopagFim; }
            set { DtFopagFim = value; }
        }
        private string[] DtReferencia;

        public string[] dtReferencia
        {
            get { return DtReferencia; }
            set { DtReferencia = value; }
        }
        private string[] referencia;

        public string[] Referencia
        {
            get { return referencia; }
            set { referencia = value; }
        }
        private string[] valor;

        public string[] Valor
        {
            get { return valor; }
            set { valor = value; }
        }
        private string[] verbaOK;

        public string[] VerbaOK
        {
            get { return verbaOK; }
            set { verbaOK = value; }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            string strSQL = "";
            string Error = "";
            for (int i = 0; i < DataLen.intValue(); i++)
            {
                // Deleção
                if (bDelete.ToString() == "true")
                {
                    strSQL = "DELETE Fopag_Ocorrencias " +
                             "WHERE FopOcorrenciaID=" + (fopOcorrenciaID[i]);
                }
                else
                    //Inclusao
                    if (fopOcorrenciaID[i].intValue() == 0)
                    {
                        strSQL = "INSERT INTO Fopag_Ocorrencias (FuncionarioID, VerbaID, dtFopagInicio, dtFopagFim, dtReferencia, " +
                                "Referencia, Valor, VerbaOK, dtOcorrencia, FopagID) " +
                                "VALUES (" + (funcionarioID[i]) + ", " + (verbaID[i]) + ", " + (dtFopagInicio[i].ToString()) + ", " +
                                (dtFopagFim[i].ToString()) + ", " + (dtReferencia[i].ToString()) + ", " + (referencia[i].ToString()) + ", " +
                                (valor[i]) + ", " + (verbaOK[i].ToString()) + ", GETDATE(), " + (fopagID.ToString()) + ")";
                    }
                    else
                    {
                        strSQL = "UPDATE Fopag_Ocorrencias SET FuncionarioID=" + (funcionarioID[i].ToString()) + ", " +
                            "VerbaID=" + (verbaID[i]) + ", dtFopagInicio=" + (dtFopagInicio[i].ToString()) + ", " +
                            "dtFopagFim=" + (DtFopagFim[i].ToString()) + ", dtReferencia=" + (dtReferencia[i].ToString()) + ", " +
                            "Referencia=" + (referencia[i].ToString()) + ", Valor=" + (valor[i].ToString()) + ", " +
                            "VerbaOK=" + (verbaOK[i]) + ", dtOcorrencia=GETDATE() " +
                            "WHERE FopOcorrenciaID=" + (fopOcorrenciaID[i]);
                    }
                try
                {
                    int linhas = DataInterfaceObj.ExecuteSQLCommand(strSQL);
                }
                catch (System.Exception exception)
                {
                    Error += exception.Message + " as fldErrorText, " +
                         " 0 as fldErrorNumber ";
                }
            }
            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " + (Error == "" ?  "'' as fldErrorText, 0 as fldErrorNumber ": Error)));
        }
    }
}