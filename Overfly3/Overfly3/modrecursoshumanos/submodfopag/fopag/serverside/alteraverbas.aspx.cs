﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modrecursoshumanos.submodfopag.fopagEx.serverside
{
    public partial class alteraverbas : System.Web.UI.OverflyPage
    {
        private Integer DataLen;
        private int linhas;

        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }
        private Integer fopgaID;

        public Integer FopgaID
        {
            get { return fopgaID; }
            set { fopgaID = value; }
        }
        private Integer[] fopVerbaID;

        public Integer[] FopVerbaID
        {
            get { return fopVerbaID; }
            set { fopVerbaID = value; }
        }
        private string[] referencia;

        public string[] Referencia
        {
            get { return referencia; }
            set { referencia = value; }
        }
        private string[] valor;

        public string[] Valor
        {
            get { return valor; }
            set { valor = value; }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            string strSQL = "";
            string Error = "";
            for (int i = 0; i < DataLen.intValue(); i++)
            {
                strSQL = "UPDATE Fopag_Verbas SET Valor=" + (valor[i].ToString()) + ", Referencia=" + (referencia[i].ToString()) + ", " +
                        "AlteracaoManual=1 " +
                        "WHERE FopVerbaID=" + (fopVerbaID[i].ToString());
                try
                {
                    linhas = DataInterfaceObj.ExecuteSQLCommand(strSQL);

                }
                catch (System.Exception exception)
                {
                    Error += exception.Message + " as fldresp ";
                }
            }
            WriteResultXML(DataInterfaceObj.getRemoteData(
                 "select " + linhas + " as fldresp "));
        }
    }
}