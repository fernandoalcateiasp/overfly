/********************************************************************
antecipacaoSalarial.js

Library javascript para o antecipacaoSalarial.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// Ultima linha selecionada
var glb_Local_DATE_FORMAT = "DD/MM/YYYY";
var glb_Local_DATE_SQL_PARAM = 103;
var dsoGrid = new CDatatransport('dsoGrid');
var dsoDataToSave = new CDatatransport('dsoDataToSave');
var dsoCmbCol = new CDatatransport('dsoCmbCol');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
        default:
            return null;
    }
}

/********************************************************************
Window onload
********************************************************************/
function window_onload()
{
	// Garante inicializacao do lockControls
	lockControls(true);
	lockControls(false);
	
	// ajusta parametros fixos dos dsos da pagina
    dsoFixedParams();
	
    var elem;
    
	// configuracao inicial do html
    var sPag = setupPage();   
    
    // se retorna false o usuario esta navegando pelo browser
    if ( sPag == false )
        return null;

    // ajusta o body do html
    elem = document.getElementById('antecipacaoSalarialBody');

    with (elem)
    {
        style.border = 'none'; 
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

	resetDateFormat();

    // a janela carregou
    sendJSMessage( getHtmlId(), JS_MSGRESERVED, 'PAGE_VALE', 'LOADED');
    
    // mostrar aqui a pagina de direitos
    sendJSMessage( getHtmlId(), JS_MSGRESERVED, 'PAGE_VALE', 'SHOW');
    
	setFocus();
	
	if (glb_nValorVale != null)
		txtValorVale.value = glb_nValorVale;
}

/*****************************************************************************************
Funcao criada para ser usada no antecipacaoSalarial.js, pois no carregamento da pagina
o overflygen ainda nao tinha sido criado, entao a constante DATE_FORMAT ficou com
o valor default que eh o formato brasileiro. Mostrar p/ o Ze
*****************************************************************************************/
function resetDateFormat()
{
	DATE_FORMAT = overflyGen.ShortDateFormat();
	
	if ( DATE_FORMAT == "DD/MM/YYYY" )
	{
		glb_Local_DATE_FORMAT = "DD/MM/YYYY";
	    glb_Local_DATE_SQL_PARAM = 103;
	}    
	else if ( DATE_FORMAT == "MM/DD/YYYY" )
	{
		glb_Local_DATE_FORMAT = "MM/DD/YYYY";
	    glb_Local_DATE_SQL_PARAM = 101;
	}    
}

/********************************************************************
Configuracao inicial da pagina
********************************************************************/
function setupPage()
{
	var elem;
	var nNextLeft, nNextTop;
    
    // o frame da pagina
    var frameID = getExtFrameID(window);
    
    var rectFrame = new Array();
    if (frameID)
        rectFrame = getRectFrameInHtmlTop(frameID);
        
    // Nao mostra se o frame esta com retangulo zerado
    // significa que o usuario esta navegando pelo browser
    if ( rectFrame[0] == 0 )    
        return false;
    
    // ajusta o divControls
    elem = window.document.getElementById('divControls');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = ELEM_GAP;
        width = rectFrame[2] - 2 * ELEM_GAP;    
        height = 40;
    }
    
    // ajusta o titulo
    elem = window.document.getElementById('lblTitle');
    with (elem.style)
    {
		textAlign = 'left';
		fontSize = '10 pt';
		fontWeight = 'bold';
		backgroundColor = 'transparent';
        
        top = ELEM_GAP;
        width = 500;
        height = 18;
        left = 0;        
    }
    
    // ajusta o titulo
    elem = window.document.getElementById('lblMensagem');
    with (elem.style)
    {
		textAlign = 'left';
		fontSize = '8 pt';
		fontWeight = 'normal';
		backgroundColor = 'transparent';
        
        top = ELEM_GAP * 6;
        width = 500;
        height = 'auto';
        left = 0;        
    }

    // ajusta o label do valor
    elem = window.document.getElementById('lblValorVale');
    with (elem.style)
    {
        top = lblMensagem.offsetTop + lblMensagem.offsetHeight + (ELEM_GAP * 3);
        width = FONT_WIDTH * 13;
        left = 0;
        visibility = (glb_nFopOcorrenciaID == null ? 'hidden' : 'inherit');
    }

    // ajusta o titulo
    elem = window.document.getElementById('txtValorVale');
    with (elem.style)
    {
        top = lblValorVale.offsetTop + 16;
        width = FONT_WIDTH * 13;
        left = 0;
        visibility = (glb_nFopOcorrenciaID == null ? 'hidden' : 'inherit');
    }

    elem.setAttribute('thePrecision', 11, 1);
    elem.setAttribute('theScale', 2, 1);
    elem.setAttribute('verifyNumPaste', 1);
    elem.onkeypress = verifyNumericEnterNotLinked;
    elem.onkeyup = txtValorVale_onkeyup;
    elem.setAttribute('minMax', new Array(0, 99999999999), 1);
    elem.onfocus = selFieldContent;
    elem.maxLenght = 11;

    var btnsTop = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + 
		ELEM_GAP + 302;

    // ajusta o botao Gravar
    elem = window.document.getElementById('btnGravar');
    with (elem.style)
    {
        height = 24;
        width = 80;
        left = 331;
        top = btnsTop;
    }
    elem.disabled = (glb_nFopOcorrenciaID == null);
    
    // ajusta o botao Fechar
    elem = window.document.getElementById('btnFechar');
    with (elem.style)
    {
        height = 24;
        width = 80;
        left = parseInt(btnGravar.currentStyle.left, 10) + parseInt(btnGravar.currentStyle.width, 10) + ELEM_GAP;
        top = btnsTop;
    }
    
    setFocus();
}

/********************************************************************
Insere focu no campo de valor
********************************************************************/
function setFocus()
{
    window.focus();
/*
    if ((txtValorVale.style.visibility == 'inherit') &&	(!txtValorVale.disabled))
		txtValorVale.focus();
*/
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado
        
    // O usuario clicou o botao Refresh
    if (ctl.id == btnGravar.id )
    {
		gravarVale();
    }
    // O usuario clicou o botao de Fechar
    else if (ctl.id == btnFechar.id )
    {
        lockControls(true);
        
        // volta para e-mails
        sendJSMessage( getHtmlId(), JS_MSGRESERVED, 'PAGE_VALE', 'HIDE');
    }
}

function gravarVale()
{

	if (!glb_nPermiteAlteracao)
	{
		if (overflyGen.Alert('Altera��o n�o permitida.') == 0)
			return null;

		return null;
	}

	var sValor = txtValorVale.value;
	
	if (trimStr(sValor) == '')
		sValor = '*';
	else if (! isNaN(sValor))
	{
		if (sValor > glb_nLimiteVale)
		{
			if ( overflyGen.Alert ('Valor do vale maior que o limite.') == 0 )
                    return null;
			
			return null;
		}
	}
	else
		return null;

	lockControls(true);
	
	var strPars = '?FopOcorrenciaID=' + escape(glb_nFopOcorrenciaID) +
		'&ValorVale=' + escape(sValor);
	
	dsoDataToSave.URL = SYS_ASPURLROOT + '/modrecursoshumanos/submodfopag/antecipacaoSalarial/serverside/alterarvale.aspx' + strPars;
	dsoDataToSave.ondatasetcomplete = dsoGrava_DSC;
	dsoDataToSave.refresh();
}

function dsoGrava_DSC()
{
    lockControls(true);
    
    // volta para e-mails
    sendJSMessage( getHtmlId(), JS_MSGRESERVED, 'PAGE_VALE', 'HIDE');
}

function txtValorVale_onkeyup()
{
    if (event.keyCode == 13)
		gravarVale();
}
