﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modrecursoshumanos.submodfopag.antecipacaosalarial.serverside
{
    public partial class alterarvale : System.Web.UI.OverflyPage
    {
        private Integer fopOcorrenciaID;

        protected Integer FopOcorrenciaID
        {
            get { return fopOcorrenciaID; }
            set { fopOcorrenciaID = value; }
        }

        private string valorVale;

        protected string ValorVale
        {
            get { return valorVale; }
            set { valorVale = (value == "*" ? "NULL" : value); }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            int resultado = DataInterfaceObj.ExecuteSQLCommand("UPDATE Fopag_Ocorrencias SET Valor=" + valorVale+ " " +
                            "WHERE FopOcorrenciaID=" + (fopOcorrenciaID));

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select " + resultado + " as fldresp"
                )
            );
        }
    }
}