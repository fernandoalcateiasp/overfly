<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="antecipacaoSalarialHtml" name="antecipacaoSalarialHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modrecursoshumanos/submodfopag/antecipacaoSalarial/antecipacaoSalarial.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
            
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
                      
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modrecursoshumanos/submodfopag/antecipacaoSalarial/antecipacaoSalarial.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
Dim nEmpresaID, nUsuarioID
Dim i, nFopOcorrenciaID, sMensagem, nValorVale, nLimiteVale, nPermiteAlteracao

i = 0
nEmpresaID = 0
nUsuarioID = 0
nFopOcorrenciaID = 0
sMensagem = ""
nValorVale = 0
nLimiteVale = 0
nPermiteAlteracao = False

For i = 1 To Request.QueryString("nEmpresaID").Count
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nUsuarioID").Count
    nUsuarioID = Request.QueryString("nUsuarioID")(i)
Next

Dim rsSPCommand
Set rsSPCommand = Server.CreateObject("ADODB.Command")
  
With rsSPCommand
	.CommandTimeout = 60 * 10
    .ActiveConnection = strConn
    .CommandText = "sp_Fopag_AntecipacaoDados"
    .CommandType = adCmdStoredProc
      
    .Parameters.Append( .CreateParameter("@FuncionarioID", adInteger, adParamInput) )
    .Parameters("@FuncionarioID").Value = CStr(nUsuarioID)
    
    .Parameters.Append( .CreateParameter("@EmpresaID", adInteger, adParamInput) )
    .Parameters("@EmpresaID").Value = CStr(nEmpresaID)
    
	.Parameters.Append( .CreateParameter("@FopOcorrenciaID", adInteger, adParamOutput) )
	.Parameters.Append( .CreateParameter("@Mensagem", adVarChar, adParamOutput, 8000) )
	.Parameters.Append( .CreateParameter("@ValorVale", adCurrency, adParamOutput) )
	.Parameters.Append( .CreateParameter("@LimiteVale", adCurrency, adParamOutput) )
	.Parameters.Append( .CreateParameter("@PermiteAlteracao", adBoolean, adParamOutput) )

    .Execute

	nFopOcorrenciaID = rsSPCommand.Parameters("@FopOcorrenciaID").Value
	sMensagem = rsSPCommand.Parameters("@Mensagem").Value
	nValorVale = rsSPCommand.Parameters("@ValorVale").Value
	nLimiteVale = rsSPCommand.Parameters("@LimiteVale").Value
	nPermiteAlteracao = rsSPCommand.Parameters("@PermiteAlteracao").Value
End With

Set rsSPCommand = Nothing
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

If (IsNull(nFopOcorrenciaID)) Then
	nFopOcorrenciaID = "null"
End If

Response.Write "var glb_nFopOcorrenciaID = " & CStr(nFopOcorrenciaID) & ";"
Response.Write vbcrlf

If (IsNull(nValorVale)) Then
	nValorVale = "null"
End If

Response.Write "var glb_nValorVale = " & CStr(nValorVale) & ";"
Response.Write vbcrlf

If (IsNull(nLimiteVale)) Then
	nLimiteVale = "null"
End If

Response.Write "var glb_nLimiteVale = " & CStr(nLimiteVale) & ";"
Response.Write vbcrlf

'Response.Write "var glb_nPermiteAlteracao = " & CStr(nPermiteAlteracao) & ";"
Response.Write "var glb_nPermiteAlteracao = 0;"
Response.Write vbcrlf

If (nPermiteAlteracao) Then
	Response.Write "glb_nPermiteAlteracao = 1;"
	Response.Write vbcrlf
End If

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
if (fg_ExecEvents == true)
        js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
if (fg_ExecEvents == true)
{
    js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    js_fg_delegDirsAfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
}
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
if (fg_ExecEvents == true)
   js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=CellChanged>
<!--
js_fg_delegDirsCellChanged (fg, arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_delegDirsBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<script LANGUAGE="javascript" FOR=fg EVENT=BeforeSort>
<!--
fg_BeforeSort(arguments[0]);
//-->
</script>

<script LANGUAGE="javascript" FOR=fg EVENT=AfterSort>
<!--
fg_AfterSort(arguments[0]);
//-->
</script>

<!-- //@@ Translados de Eventos de grid -->
<SCRIPT ID=clientEventVbJs LANGUAGE=vbscript>
<!--

Function DateAndTimeNow()
    DateAndTimeNow = Now()
End Function

//-->
</SCRIPT>

</head>

<body id="antecipacaoSalarialBody" name="antecipacaoSalarialBody" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Objeto OverflyGen -->
    <object CLASSID="clsid:CBD8996E-E238-4A48-AB72-7A7250F700C1" ID="overflyGen" HEIGHT="0" WIDTH="0" VIEWASTEXT></object>
    
	<div id="divControls" name="divControls" class="divGeneral">
		<p id="lblTitle" name="lblTitle" class="lblGeneral">Antecipação Salarial</p>
		<p id="lblMensagem" name="lblMensagem" class="lblGeneral"><% Response.Write sMensagem %></p>
		<p id="lblValorVale" name="lblValorVale" class="lblGeneral">Valor do Vale</p>
		<input type="text" id="txtValorVale" name="txtValorVale" class="fldGeneral"></input>
    </div>
        
    <input type="button" id="btnGravar" name="btnGravar" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
    <input type="button" id="btnFechar" name="btnFechar" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>

</body>

</html>
