/********************************************************************
lancamentosinf01.js

Library javascript para o recursosinf01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dso01JoinSup = new CDatatransport('dso01JoinSup');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoDynamicCmbs = new CDatatransport('dsoDynamicCmbs');
var dsoStateMachineLkp = new CDatatransport('dsoStateMachineLkp');
var dsoFiltroInf = new CDatatransport('dsoFiltroInf');
var dso01Grid = new CDatatransport('dso01Grid');
var dsoCmbsPropAlt = new CDatatransport('dsoCmbsPropAlt');
var dsoCmb01Grid_01 = new CDatatransport('dsoCmb01Grid_01');
var dsoCmb02Grid_01 = new CDatatransport('dsoCmb02Grid_01');
var dsoCmb03Grid_01 = new CDatatransport('dsoCmb03Grid_01');
var dso01GridLkp = new CDatatransport('dso01GridLkp');
var dso02GridLkp = new CDatatransport('dso02GridLkp');
var dso03GridLkp = new CDatatransport('dso03GridLkp');
var dso04GridLkp = new CDatatransport('dso04GridLkp');
var dso05GridLkp = new CDatatransport('dso05GridLkp');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    adjustElementsInDivsNonSTD()
    putSpecialAttributesInControls()
    optChangedInCmb(cmb)
    prgServerInf(btnClicked)
    prgInterfaceInf(btnClicked, pastaID, dso)
    finalOfInfCascade(btnClicked)
    treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, currDSO)
    btnLupaClicked(btnClicked)
    addedLineInGrid(folderID, grid, nLineInGrid, dso)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    btnAltPressedInGrid( folderID )
    btnAltPressedInNotGrid( folderID )
    fillCmbFiltsInfAutomatic(folderID)
    selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
    frameIsAboutToApplyRights(folderID)

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG
    fg_AfterRowColChange_Prg()
    fg_DblClick_Prg()
    fg_ChangeEdit_Prg()
    fg_ValidateEdit_Prg()
    fg_BeforeRowColChange_Prg()
    fg_EnterCell_Prg()
    fg_MouseUp_Prg()
    fg_MouseDown_Prg()
    fg_BeforeEdit_Prg()

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    // Ze em 17/03/08
    dealWithGrid_Load();

    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array dos ids dos dsos que tenham campos linkados
    // a controles HTMLs.
    // Estes controles HTMls estao em divs e sao linkados a campos do dso
    // que sejam: obrigatorios, numericos ou de datas, nos dsos.
    // Se nao aplicavel ao form este array deve ser null
    glb_aDsosToCloneFieldsEtc = ['dso01JoinSup'];
    
    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;
    
    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 

    glb_aGridStaticCombos = [ [20010, [[0, 'dso01GridLkp'   , ''             , ''],
                                       [0, 'dso02GridLkp'   , ''             , ''],
                                       [0, 'dso03GridLkp'   , ''             , ''],
                                       [0, 'dso04GridLkp'   , ''             , '']]],
                              [29061, [[1, 'dsoCmb01Grid_01', '*ContaID|ContaIdentada|TipoDetalhamento', 'ContaID'], 
                                       [9, 'dsoCmb02Grid_01', '*ContaNome|Empresa|ContaComplemento', 'RelPesContaID'],
                                       [10, 'dsoCmb03Grid_01', '*ImpostoAbrev|Imposto', 'ImpostoID'],
                                       [0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', ''],
                                       [0, 'dso04GridLkp', '', ''],
                                       [0, 'dso05GridLkp', '', '']]]];

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
        
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03'],
                               [20008, 20009, [20010, 29061, 29062, 29063]] );

    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();
    
}

function setupPage()
{
    //@@ Ajustar os divs
    // Nada a codificar
}

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg()
{

}

function fg_DblClick_Prg()
{
    var empresa = getCurrEmpresaData();
 
    if ( (keepCurrFolder() == 29061) && (fg.Rows > 1) && (fg.Editable == false) )
        sendJSCarrier(getHtmlId(), 'SHOWCONTA', new Array(empresa[0], getCellValueByColKey(fg, 'ContaID', fg.Row)));
    if ( (keepCurrFolder() == 29062) && (fg.Rows > 1) && (fg.Editable == false) )
        sendJSCarrier(getHtmlId(), 'SHOWLANCAMENTO', new Array(empresa[0], getCellValueByColKey(fg, 'LancamentoID', fg.Row)));       
    if ( (keepCurrFolder() == 29063) && (fg.Rows > 1) && (fg.Editable == false) )
		detalharDetalhe();
}

function fg_ChangeEdit_Prg()
{

}
function fg_ValidateEdit_Prg()
{
    // Se for a Pasta de Contas
    if ((keepCurrFolder() == 29061) && (fg.Editable) && (fg.Col == getColIndexByColKey(fg, 'ContaID')))
	{
        validateConta();
	}
}
function fg_BeforeRowColChange_Prg()
{

}

function fg_EnterCell_Prg()
{

}

function fg_MouseUp_Prg()
{

}

function fg_MouseDown_Prg()
{

}

function fg_BeforeEdit_Prg()
{

}

// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD()
{
    //@@
    // Este form nao tem este caso    
}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    glb_pastaDefault = 29061;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

     
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div

	// Mover esta funcao para os finais retornos de operacoes no
	// banco de dados
	finalOfInfCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    showBtnsEspecControlBar('inf', false, [1,1,1,1]);
    
    // Pasta de Contas
	if ( pastaID == 29061 )
    {
        showBtnsEspecControlBar('inf', true, [1,1,1,0]);
        tipsBtnsEspecControlBar('inf', ['Preencher Pessoa','Detalhar Conta','Extratos conciliados','']);
    }

    // Pasta de Lancamentos Filhos
	else if ( pastaID == 29062 )
    {
        showBtnsEspecControlBar('inf', true, [1,0,0,0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Lan�amento','','','']);
    }

    // Pasta de Detalhes
	else if ( pastaID == 29063 )
    {
        showBtnsEspecControlBar('inf', true, [1,1,0,0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Detalhe','Detalhar Contas','','']);
    }

    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ( (btnClicked.toUpperCase()).indexOf('SUP') >= 0 )
        // usario clicou botao no control bar sup
	    sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
	else
	    // usario clicou botao no control bar inf
	    sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
	    
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
{
	var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
	var nProprietarioID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ProprietarioID' + '\'' + '].value');
	var bHPUsoSistema = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'HPUsoSistema' + '\'' + '].value');
	var nLancamentoMaeID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'LancamentoMaeID' + '\'' + '].value');
	var nSaldoLancMae = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'SaldoLancMae' + '\'' + '].value');
	var sClonarBtn = ((!bHPUsoSistema) && ((nEstadoID == 2) || (nEstadoID == 81) )) ? 'H' : 'D';
	var sRatearBtn = ((nLancamentoMaeID == null) && (nSaldoLancMae == 0)) ? 'H' : 'D';
	var sDetalharLancamentoMaeBtn = ((nLancamentoMaeID == null) || (nLancamentoMaeID == 0)) ? 'D' : 'H';
	var bDireito = false;
	var nE1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		                        ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E1' + '\'' + ')') );

	var nE2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		                        ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E2' + '\'' + ')') );
    //DireitoEspecifico
    //Lancamentos->Lancametos->SUP
    //29060 SFS-Grupo Lancamento -> 
    //((nEstadoID != null)&&(nEstadoID != 1)&&(nEstadoID != 82))||((nProprietarioID==999)&&(!(E1&&E2))) -> Ajsuta os botoes da barra superior para 'HDHDHDDDHHHDDDD'. Se a pasta inferior for a pasta contas e tem linhas no gride a barra inferior ficar�: 'DDDDDDDDHHHHDHD', se n�o: 'DDDDDDDDHDDDDDD'.
	bDireito = (nE1 && nE2);
	
	if ((btnClicked!='SUPALT') && (btnClicked!='SUPINCL'))
	{
		if ( ((nEstadoID != null) && (nEstadoID != 1) && (nEstadoID != 82)) || ((nProprietarioID==999)&&(!bDireito)) )
		{
			adjustControlBarEx(window,'sup',  'HDHDHDDDHHHDDDD');
			
			if (folderID == 29061)
			{
				if (fg.Rows > 1)
					adjustControlBarEx(window,'inf',  'DDDDDDDDHHHHDHD');
				else
					adjustControlBarEx(window,'inf',  'DDDDDDDDHDDDDDD');
			}	
		}
	}
	
    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR') 
          || (btnClicked == 'SUPOK') || (btnClicked == 'INFREFR')
          || (btnClicked == 'INFOK') || (btnClicked == 'INFCANC') )
    {      
        setupEspecBtnsControlBar('sup', 'HDH' + sClonarBtn + 'D' + sRatearBtn + 'D' + sDetalharLancamentoMaeBtn);
    }    
    else    
        setupEspecBtnsControlBar('sup', 'HDH' + sClonarBtn + 'D' + sRatearBtn + 'D');

	setupEspecBtnsControlBar('inf', 'DDDD');

	if (folderID == 29061)
	{
		if (fg.Rows > 1)
			setupEspecBtnsControlBar('inf', 'DHHD');
		else			
			setupEspecBtnsControlBar('inf', 'DDDD');
	}
	if (folderID == 29062)
	{
		if (fg.Rows > 1)
			setupEspecBtnsControlBar('inf', 'HDDD');
		else			
			setupEspecBtnsControlBar('inf', 'DDDD');
	}
	if (folderID == 29063)
	{
		if (fg.Rows > 1)
			setupEspecBtnsControlBar('inf', 'HHDD');
		else			
			setupEspecBtnsControlBar('inf', 'DDDD');
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso)
{
	if (folderID == 29061)
		setupEspecBtnsControlBar('inf', 'HDDD');
	else		
		setupEspecBtnsControlBar('inf', 'DDDD');

    // Pasta de Contas
    if ( folderID == 29061 )
    {
        // Merge de Colunas
        fg.MergeCells = 0;
    }

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    var empresa = getCurrEmpresaData();

    // Pasta Contas
    if ( (keepCurrFolder() == 29061) && (btnClicked == 1) )
        openModalPessoa();
    else if ( (keepCurrFolder() == 29061) && (btnClicked == 2) )
        sendJSCarrier(getHtmlId(), 'SHOWCONTA', new Array(empresa[0], getCellValueByColKey(fg, 'ContaID', fg.Row)));
	else if ( (keepCurrFolder() == 29061) && (btnClicked == 3) )
		openModalExtratosConciliados();
	else if ( (keepCurrFolder() == 29062) && (btnClicked == 1) )
        sendJSCarrier(getHtmlId(), 'SHOWLANCAMENTO', new Array(empresa[0], getCellValueByColKey(fg, 'LancamentoID', fg.Row)));
	else if ( (keepCurrFolder() == 29063) && (btnClicked == 1) )
		detalharDetalhe();
	else if ( (keepCurrFolder() == 29063) && (btnClicked == 2) )
		openModalDetalharContas();
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Pessoa

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPessoa()
{
	if ( (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ContaID')) == null) || (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ContaID')) == 0) )
	{
		if ( window.top.overflyGen.Alert('Selecione a conta primeiro.') == 0 )
		    return null;
		    
		return null;		    
	}

	var aEmpresa = getCurrEmpresaData();
	var strPars = '?nContaID=' + escape(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ContaID')));
	strPars += '&nEmpresaID=' + escape(aEmpresa[0]);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/lancamentos/modalpages/modalpessoa.asp' + strPars;
    showModalWin(htmlPath, new Array(717,284));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Extratos Conciliados

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalExtratosConciliados()
{
	var strPars = '?nLanContaID=' + escape(getCellValueByColKey(fg, 'LanContaID', fg.Row));

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/lancamentos/modalpages/modalextratosconciliados.asp' + strPars;
    showModalWin(htmlPath, new Array(400,280));
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALPESSOAHTML')
    {
        if ( param1 == 'OK' )                
        {
            // Escreve ID, Label e TipoRelacaoID no grid de relacoes
            writePessoaInGrid(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
	else if (idElement.toUpperCase() == 'MODALEXTRATOSCONCILIADOSHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    // Modal Eventos Contabeis
	else if (idElement.toUpperCase() == 'MODALEVENTOSCONTABEIS_CONTASHTML')
	{
		if ( param1 == 'OK' )                
		{
			// esta funcao fecha a janela modal e destrava a interface
			restoreInterfaceFromModal();
			// escreve na barra de status
			writeInStatusBar('child', 'cellMode', 'Detalhe');

			return 0;
		}
		else if (param1 == 'CANCEL')
		{
			// esta funcao fecha a janela modal e destrava a interface
			restoreInterfaceFromModal();
			// escreve na barra de status
			writeInStatusBar('child', 'cellMode', 'Detalhe');
		    
			return 0;
		}
	}
}

/********************************************************************
Funcao criada pelo programador.
Escreve Pessoa e PessoaID.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writePessoaInGrid(aPessoa)
{
    // o array aPessoa contem: PessoaID, Fantasia
    
    fg.Redraw = 0;

    // Escreve nos campos
    fg.TextMatrix(keepCurrLineInGrid(),getColIndexByColKey(fg, 'PessoaID*')) = aPessoa[0];
    fg.TextMatrix(keepCurrLineInGrid(),getColIndexByColKey(fg, '^PessoaID^dso02GridLkp^PessoaID^Fantasia*')) = aPessoa[1];
    
    fg.Redraw = 2;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid( folderID )
{
	if (folderID == 29061)
		setupEspecBtnsControlBar('inf', 'HDDD');
	else		
		setupEspecBtnsControlBar('inf', 'DDDD');
		
    // Pasta de Contas
    if ( folderID == 29061 )
    {
        // Merge de Colunas
        fg.MergeCells = 0;
        fg.ColWidth(getColIndexByColKey(fg, 'Valor')) = (fg.ColWidth(getColIndexByColKey(fg, 'Valor')) * 1.5);
    }
		
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid( folderID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID)
{
    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
{
    //@@ retornar null para prosseguir a automacao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra infeior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID)
{
    // Pasta de LOG Read Only
    if ( folderID == 20010 )
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    // Pasta de Lancamentos Filhos/Detalhes ReadOnly
    if ( (folderID == 29062) || (folderID == 29063) )
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else
    {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Usada pela funcao execValidadeEdit_Prg() na pasta de Contas

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function validateConta()
{
	if(!((dsoCmb01Grid_01.recordset.BOF)&&(dsoCmb01Grid_01.recordset.EOF)))
    {
        dsoCmb01Grid_01.recordset.MoveFirst();

        while (!dsoCmb01Grid_01.recordset.EOF)
        {
            if (dsoCmb01Grid_01.recordset['ContaID'].value == parseInt(fg.ComboData, 10))
			{
                fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, '^ContaID^dso01GridLkp^ContaID^Det*')) = dsoCmb01Grid_01.recordset['Det'].value;
                fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, '^ContaID^dso01GridLkp^ContaID^Conta*')) = dsoCmb01Grid_01.recordset['ContaIdentada'].value;
                if (!dsoCmb01Grid_01.recordset['AceitaLancamento'].value)
				{
					if ( window.top.overflyGen.Alert('Esta conta n�o permite lan�amento.') == 0 )
					    return null;
				}		
			}
            dsoCmb01Grid_01.recordset.MoveNext();
        }
    }       
}

function detalharDetalhe()
{
    var empresa = getCurrEmpresaData();
	var nFormID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FormID'));
	var nSubFormID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'SubFormID'));
	var nRegistroID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RegistroID'));
	var nFinanceiroID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FinanceiroID'));

	if ( (nFormID == 5110) && (nSubFormID == 24000) )
		sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], nRegistroID));       
	else if ( (nFormID == 9110) && (nSubFormID == 28000) )
		sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(empresa[0], nRegistroID));       
	else if ( (nFormID == 9110) && (nSubFormID == 28001) )
		sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(empresa[0], nFinanceiroID));       
	else if ( (nFormID == 9130) && (nSubFormID == 28040) )
		sendJSCarrier(getHtmlId(), 'SHOWVALORID', new Array(empresa[0], nRegistroID));       
	else if ( (nFormID == 9150) && (nSubFormID == 28080) )
		sendJSCarrier(getHtmlId(), 'SHOWDEPOSITOBANCARIO', new Array(empresa[0], nRegistroID));       
}

function openModalDetalharContas()
{
    if (fg.Row <= 0)
		return null;

    var htmlPath;
    var strPars = new String();
    var nEventoID = getCellValueByColKey(fg, 'EventoID', fg.Row);

    // mandar os parametros para o servidor
    strPars = '?nEventoID=' + escape(nEventoID);
    
    // carregar modal - nao faz operacao de banco no carregamento           
    htmlPath = SYS_PAGESURLROOT + '/modalgen/modaleventoscontabeis_contas.asp' + strPars;
    showModalWin(htmlPath, new Array(765,460));
}
