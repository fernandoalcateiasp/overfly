﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modcontabil.submodcontabilidade.lancamentos.serverside
{
    public partial class verificacaolancamento : System.Web.UI.OverflyPage
    {
        private Integer LancamentoID;

        public Integer nLancamentoID
        {
            get { return LancamentoID; }
            set { LancamentoID = value; }
        }
        private Integer EstadoDeID;

        public Integer nEstadoDeID
        {
            get { return EstadoDeID; }
            set { EstadoDeID = value; }
        }
        private Integer EstadoParaID;

        public Integer nEstadoParaID
        {
            get { return EstadoParaID; }
            set { EstadoParaID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string strSql = "";
            int Verificacao=0;

            strSql = "SELECT dbo.fn_Lancamento_Verifica( " + (LancamentoID) + ", " +
                                                (EstadoDeID) + ", " +
                                                (EstadoParaID) + ", GETDATE()) AS Verificacao";
            WriteResultXML(DataInterfaceObj.getRemoteData(strSql));
        }
    }
}