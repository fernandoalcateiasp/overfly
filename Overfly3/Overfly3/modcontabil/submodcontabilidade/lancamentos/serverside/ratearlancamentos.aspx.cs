﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.lancamentos.serverside
{
    public partial class ratearlancamentos : System.Web.UI.OverflyPage
    {
        private string Resultado = "";
        private string LancamentoCompensacaoID = "";


        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private Integer LancamentoID;

        public Integer nLancamentoID
        {
            get { return LancamentoID; }
            set { LancamentoID = value; }
        }
        private string Empresas;

        public string sEmpresas
        {
            get { return Empresas; }
            set { Empresas = value; }
        }


        protected DataSet sp_Lancamento_Ratear()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[5];

            procParams[0] = new ProcedureParameters(
                "@LancamentoID",
                System.Data.SqlDbType.Int,
                (LancamentoID != null) ? (Object)LancamentoID.ToString() : DBNull.Value);

            procParams[1] = new ProcedureParameters(
                "@Empresas",
                System.Data.SqlDbType.VarChar,
                (Empresas != null) ? (Object)Empresas.ToString() : DBNull.Value);

            procParams[2] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                (UsuarioID != null) ? (Object)UsuarioID.ToString() : DBNull.Value);

            procParams[3] = new ProcedureParameters(
              "@LancamentoCompensacaoID",
              System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);

            procParams[4] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[4].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Lancamento_Ratear",
                procParams);

            LancamentoCompensacaoID = procParams[3].Data.ToString();
            Resultado = procParams[4].Data.ToString();


            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (LancamentoCompensacaoID != null ? "'" + LancamentoCompensacaoID + "' " : "NULL") +
                " as LancamentoID , " +
                 (Resultado != null ? "'" + Resultado + "' " : "NULL") +
                 " as Resultado ");

        }


        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(sp_Lancamento_Ratear());
        }
    }
}