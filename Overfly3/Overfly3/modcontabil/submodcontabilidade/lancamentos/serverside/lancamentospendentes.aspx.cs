﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.lancamentos.serverside
{
    public partial class lancamentospendentes : System.Web.UI.OverflyPage
    {
        private string Resultado = "";
        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private Integer HpID;

        public Integer nHPID
        {
            get { return HpID; }
            set { HpID = value; }
        }
        private string Eventos;

        public string sEventos
        {
            get { return Eventos; }
            set { Eventos = value; }
        }

        protected void LancamentoAutomatico()
        {

            ProcedureParameters[] procParams = new ProcedureParameters[4];

            procParams[0] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                (EmpresaID != null) ? (Object)EmpresaID.ToString() : DBNull.Value);

            procParams[1] = new ProcedureParameters(
                "@HistoricoPadraoID",
                System.Data.SqlDbType.Int,
                (HpID != null) ? (Object)HpID.ToString() : DBNull.Value);

            procParams[2] = new ProcedureParameters(
                "@Eventos",
                System.Data.SqlDbType.VarChar,
                (Eventos != "") ? (Object)Eventos.ToString() : DBNull.Value);

            procParams[3] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[3].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Lancamento_Automatico",
                procParams);

            Resultado += procParams[3].Data.ToString() + "";


        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            LancamentoAutomatico();

            WriteResultXML(DataInterfaceObj.getRemoteData(
            "select " +
            (Resultado != null ? "'" + Resultado + "' " : "NULL") +
            " as Resultado "));

        }
    }
}