﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WSData;
using java.lang;

namespace Overfly3.modcontabil.submodcontabilidade.lancamentos.serverside
{
    public partial class clonarlancamento : System.Web.UI.OverflyPage
    {
        private Integer LancamentoID;

        public Integer nLancamentoID
        {
            get { return LancamentoID; }
            set { LancamentoID = value; }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            string strSql = "";

            strSql = "SELECT a.EmpresaID, e.Fantasia AS Empresa, a.EhEstorno, a.TipoLancamentoID, a.HistoricoPadraoID, " +
                    "a.HistoricoComplementar, b.ContaID, b.Valor, b.Documento, b.PessoaID, " +
                    "c.Fantasia AS Pessoa, b.RelPesContaID, dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) AS ContaBancaria, " +
                    "b.ImpostoID, d.Imposto, f.Conta, f.TipoDetalhamentoID, f.DetalhamentoObrigatorio, i.EhDebito, " +
                    "ISNULL(f.UsoEspecialID, 0) AS UsoEspecialID, " +
                    "dbo.fn_Conta_EhFilha(f.ContaID, (SELECT TOP 1 ContaID FROM PlanoContas WITH(NOLOCK) WHERE (EstadoID = 2 AND UsoEspecialID = 1511)), 1) AS EhFilhaDisp, " +
                    "0 AS Excluir, i.EhObrigatoria, g.ItemAbreviado AS Det, b.UnidadeID AS LanContaEmpresaID " +
                "FROM Lancamentos a WITH(NOLOCK) " +
                    "INNER JOIN Lancamentos_Contas b WITH(NOLOCK) ON (a.LancamentoID = b.LancamentoID) " +
                    "LEFT OUTER JOIN Pessoas c WITH(NOLOCK) ON (b.PessoaID = c.PessoaID) " +
                    "LEFT OUTER JOIN Conceitos d WITH(NOLOCK) ON (b.ImpostoID = d.ConceitoID) " +
                    "INNER JOIN Pessoas e WITH(NOLOCK) ON (a.EmpresaID = e.PessoaID) " +
                    "INNER JOIN PlanoContas f WITH(NOLOCK) ON (b.ContaID = f.ContaID) " +
                    "INNER JOIN TiposAuxiliares_Itens g WITH(NOLOCK) ON (f.TipoDetalhamentoID = g.ItemID) " +
                    "INNER JOIN HistoricosPadrao h WITH(NOLOCK) ON (a.HistoricoPadraoID = h.HistoricoPadraoID) " +
                    "INNER JOIN HistoricosPadrao_Contas i WITH(NOLOCK) ON (h.HistoricoPadraoID = i.HistoricoPadraoID AND a.TipoLancamentoID = i.TipoLancamentoID) " +
                "WHERE (a.LancamentoID = " + LancamentoID.ToString() + " AND " +
                    "dbo.fn_Conta_EhFilha(f.ContaID, i.ContaID, 1) = 1 AND " +
                    "((b.Valor > 0 AND i.EhDebito = 0) OR (b.Valor < 0 AND i.EhDebito = 1))) " +
                "ORDER BY (CASE WHEN b.Valor < 0 THEN 0 ELSE 1 END), b.ContaID, c.Fantasia, " +
                    "dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4), d.Imposto, " +
                    "ABS(b.Valor), b.Documento ";


            WriteResultXML(DataInterfaceObj.getRemoteData(strSql));
        }
    }
}
