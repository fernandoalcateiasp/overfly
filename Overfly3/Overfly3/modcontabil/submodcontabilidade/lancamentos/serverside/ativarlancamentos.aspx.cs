﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.lancamentos.serverside
{
    public partial class ativarlancamentos : System.Web.UI.OverflyPage
    {
        private string Resultado;

        private Integer UserID;

        public Integer nUserID
        {
            get { return UserID; }
            set { UserID = value; }
        }
        private Integer dataLen;

        public Integer nDataLen
        {
            get { return dataLen; }
            set { dataLen = value; }
        }

        private Integer[] LancamentoID;

        public Integer[] nLancamentoID
        {
            get { return LancamentoID; }
            set { LancamentoID = value; }
        }
        protected DataSet AtivarLancamentos()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[4];

            for (int i = 0; i < dataLen.intValue(); i++)
            {

                procParams[0] = new ProcedureParameters(
                    "@LancamentoID",
                    System.Data.SqlDbType.Int,
                    (LancamentoID.Length > 0) ? (Object)LancamentoID[i].ToString() : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                    "@EstadoParaID",
                    System.Data.SqlDbType.Int,
                    2);

                procParams[2] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                    (UserID != null) ? (Object)UserID.ToString() : DBNull.Value);

                procParams[3] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[3].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_Lancamento_EstadoAlterar",
                    procParams);

                Resultado += procParams[3].Data.ToString() + " \r\n ";
            }

            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (Resultado != null ? "'" + Resultado + "' " : "NULL") +
                 " as Resultado ");

        }


        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(AtivarLancamentos());
        }
    }
}