﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.lancamentos.serverside
{
    public partial class pesqpessoa : System.Web.UI.OverflyPage
    {
        private string Find;

        public string strToFind
        {
            get { return Find; }
            set { Find = value.Trim(); }
        }
        private string ContaID;

        public string nContaID
        {
            get { return ContaID; }
            set { ContaID = value; }
        }
        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private string Ativos;

        public string bAtivos
        {
            get { return Ativos; }
            set { Ativos = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            ProcedureParameters[] procParams = new ProcedureParameters[4];
            procParams[0] = new ProcedureParameters(
                 "@Pesquisa",
                 System.Data.SqlDbType.VarChar,
                 (Find != null) ? (Object)Find.ToString() : DBNull.Value);

            procParams[1] = new ProcedureParameters(
                "@ContaID",
                System.Data.SqlDbType.BigInt,
                (ContaID != null) ? (Object)ContaID.ToString() : DBNull.Value);

            procParams[2] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                (EmpresaID != null) ? (Object)EmpresaID.ToString() : DBNull.Value);

            procParams[3] = new ProcedureParameters(
               "@FiltroAtivos",
               System.Data.SqlDbType.VarChar,
               (DBNull.Value));

            WriteResultXML(DataInterfaceObj.execQueryProcedure("sp_Lancamento_ListarPessoa", procParams));
        }
    }
}