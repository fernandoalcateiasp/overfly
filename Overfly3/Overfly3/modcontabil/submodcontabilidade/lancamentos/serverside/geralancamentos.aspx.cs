﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;


namespace Overfly3.modcontabil.submodcontabilidade.lancamentos.serverside
{
    public partial class geralancamentos : System.Web.UI.OverflyPage
    {
        private string Resultado;
        private int Lancamento;
        protected static Integer Zero = new Integer(0);

        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private string EhEstorno;

        public string sEhEstorno
        {
            get { return EhEstorno; }
            set { EhEstorno = value; }
        }
        private Integer TipoLancamentoID;

        public Integer nTipoLancamentoID
        {
            get { return TipoLancamentoID; }
            set { TipoLancamentoID = value; }
        }
        private Integer HistoricoPadraoID;

        public Integer nHistoricoPadraoID
        {
            get { return HistoricoPadraoID; }
            set { HistoricoPadraoID = value; }
        }
        private string HistoricoComplementar;

        public string sHistoricoComplementar
        {
            get { return HistoricoComplementar; }
            set { HistoricoComplementar = value; }
        }
        private Integer[] LancamentoID;

        public Integer[] nLancamentoID
        {
            get { return LancamentoID; }
            set
            {
                LancamentoID = value;

                for (int i = 0; i < LancamentoID.Length; i++)
                {
                    if (LancamentoID[i] == null)
                        LancamentoID[i] = Zero;

                    if (i == 0)
                    {
                        Lancamento = Convert.ToInt32(LancamentoID[i]);
                    }
                }
            }
        }
        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private Integer[] LanContaEmpresaID;

        public Integer[] nLanContaEmpresaID
        {
            get { return LanContaEmpresaID; }
            set
            {
                LanContaEmpresaID = value;

                for (int i = 0; i < LanContaEmpresaID.Length; i++)
                {
                    if (LanContaEmpresaID[i] == null)
                        LanContaEmpresaID[i] = Zero;
                }
            }
        }
        private Integer[] ContaID;

        public Integer[] nContaID
        {
            get { return ContaID; }
            set
            {
                ContaID = value;

                for (int i = 0; i < ContaID.Length; i++)
                {
                    if (ContaID[i] == null)
                        ContaID[i] = Zero;
                }
            }
        }
        private Integer[] RelPesContaID;

        public Integer[] nRelPesContaID
        {
            get { return RelPesContaID; }
            set
            {
                RelPesContaID = value;

                for (int i = 0; i < RelPesContaID.Length; i++)
                {
                    if (RelPesContaID[i] == null)
                        RelPesContaID[i] = Zero;
                }
            }
        }
        private Integer[] ImpostoID;

        public Integer[] nImpostoID
        {
            get { return ImpostoID; }
            set
            {
                ImpostoID = value;

                for (int i = 0; i < ImpostoID.Length; i++)
                {
                    if (ImpostoID[i] == null)
                        ImpostoID[i] = Zero;
                }
            }
        }
        private Integer[] MoedaID;

        public Integer[] nMoedaID
        {
            get { return MoedaID; }
            set
            {
                MoedaID = value;

                for (int i = 0; i < MoedaID.Length; i++)
                {
                    if (MoedaID[i] == null)
                        MoedaID[i] = Zero;
                }
            }
        }
        private string[] Documento;

        public string[] sDocumento
        {
            get { return Documento; }
            set
            {
                Documento = value;

                for (int i = 0; i < Documento.Length; i++)
                {
                    if (Documento[i] == null)
                        Documento[i] = "";
                }
            }
        }
        private Integer[] PessoaID;

        public Integer[] nPessoaID
        {
            get { return PessoaID; }
            set
            {
                PessoaID = value;

                for (int i = 0; i < PessoaID.Length; i++)
                {
                    if (PessoaID[i] == null)
                        PessoaID[i] = Zero;
                }
            }
        }
        private string[] Valor;

        public string[] nValor
        {
            get { return Valor; }
            set
            {
                Valor = value;

                for (int i = 0; i < Valor.Length; i++)
                {
                    if (Valor[i] == null)
                        Valor[i] = "";
                }
            }
        }

        protected void GeraLancamentos()
        {
            int RetLancamentoID = 0;
            ProcedureParameters[] procParams = new ProcedureParameters[10];
            ProcedureParameters[] ContaGeradorParams = new ProcedureParameters[11];

            if (Lancamento == 0)
            {

                procParams[0] = new ProcedureParameters(
                    "@EmpresaID",
                    System.Data.SqlDbType.Int,
                    (EmpresaID != null ? (Object)Convert.ToInt32(EmpresaID.ToString()) : DBNull.Value));

                procParams[1] = new ProcedureParameters(
                    "@dtBalancete",
                    System.Data.SqlDbType.DateTime,
                    DBNull.Value);

                procParams[2] = new ProcedureParameters(
                    "@EhEstorno",
                    System.Data.SqlDbType.Bit,
                    (EhEstorno == "0") ? false : true);

                procParams[3] = new ProcedureParameters(
                    "@TipoLancamentoID",
                    System.Data.SqlDbType.Int,
                    (TipoLancamentoID != null ? (Object)Convert.ToInt32(TipoLancamentoID.ToString()) : DBNull.Value));

                procParams[4] = new ProcedureParameters(
                    "@HistoricoPadraoID",
                    System.Data.SqlDbType.Int,
                    (HistoricoPadraoID != null ? (Object)Convert.ToInt32(HistoricoPadraoID.ToString()) : DBNull.Value));

                procParams[5] = new ProcedureParameters(
                    "@HistoricoComplementar",
                    System.Data.SqlDbType.VarChar,
                    (HistoricoComplementar != null) ? (Object)HistoricoComplementar.ToString() : DBNull.Value);

                procParams[6] = new ProcedureParameters(
                    "@Gerador",
                    System.Data.SqlDbType.Int,
                    2);

                procParams[7] = new ProcedureParameters(
                    "@ExtLancamentoID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value);

                procParams[8] = new ProcedureParameters(
                   "@ProprietarioID",
                   System.Data.SqlDbType.Int,
                   (UsuarioID != null ? (Object)Convert.ToInt32(UsuarioID.ToString()) : DBNull.Value));

                procParams[9] = new ProcedureParameters(
                    "@LancamentoID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[9].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_Lancamento_Gerador",
                    procParams);

                if (procParams[9].Data != DBNull.Value)
                {
                    RetLancamentoID = Convert.ToInt32(procParams[9].Data.ToString());
                    Lancamento = Convert.ToInt32(RetLancamentoID);
                }

            }            // fim do IF lancamento=0          
            
            if (Lancamento > 0)
            {
                for (int i = 0; i < nContaID.Length; i++)
                {
                    ContaGeradorParams[0] = new ProcedureParameters(
                        "@LancamentoID",
                        System.Data.SqlDbType.Int,
                        Lancamento);

                    ContaGeradorParams[1] = new ProcedureParameters(
                        "@UnidadeID",
                        System.Data.SqlDbType.Int,                        
                        ((!LanContaEmpresaID[i].Equals(Zero)) ? (Object)Convert.ToInt32(LanContaEmpresaID[i]) : DBNull.Value));

                    ContaGeradorParams[2] = new ProcedureParameters(
                        "@ContaID",
                        System.Data.SqlDbType.Int,                        
                        ((!ContaID[i].Equals(Zero)) ? (Object)Convert.ToInt32(ContaID[i]) : DBNull.Value));

                    ContaGeradorParams[3] = new ProcedureParameters(
                         "@PessoaID",
                         System.Data.SqlDbType.Int,
                        ((!PessoaID[i].Equals(Zero)) ? (Object)Convert.ToInt32(PessoaID[i]) : DBNull.Value));

                    ContaGeradorParams[4] = new ProcedureParameters(
                       "@RelPesContaID",
                       System.Data.SqlDbType.Int,                       
                      ((!RelPesContaID[i].Equals(Zero)) ? (Object)Convert.ToInt32(RelPesContaID[i]) : DBNull.Value));

                    ContaGeradorParams[5] = new ProcedureParameters(
                       "@ImpostoID",
                       System.Data.SqlDbType.Int,                       
                       ((!ImpostoID[i].Equals(Zero)) ? (Object)Convert.ToInt32(ImpostoID[i]) : DBNull.Value));

                    ContaGeradorParams[6] = new ProcedureParameters(
                       "@MoedaID",
                       System.Data.SqlDbType.Int,                       
                        ((!MoedaID[i].Equals(Zero)) ? (Object)Convert.ToInt32(MoedaID[i]) : DBNull.Value));

                    ContaGeradorParams[7] = new ProcedureParameters(
                       "@Valor",
                       System.Data.SqlDbType.Decimal,
                       (Valor != null) ? (Object)Convert.ToDouble(Valor[i]) : DBNull.Value);

                    ContaGeradorParams[8] = new ProcedureParameters(
                       "@Documento",
                       System.Data.SqlDbType.VarChar,
                       (Documento[i].ToString() != "") ? (Object)Documento.ToString() : DBNull.Value);

                    ContaGeradorParams[9] = new ProcedureParameters(
                         "@LanContaID",
                         System.Data.SqlDbType.Int,
                         DBNull.Value,
                         ParameterDirection.InputOutput);

                    ContaGeradorParams[10] = new ProcedureParameters(
                          "@Resultado",
                          System.Data.SqlDbType.VarChar,
                          DBNull.Value,
                          ParameterDirection.InputOutput);
                    ContaGeradorParams[10].Length = 8000;

                    DataInterfaceObj.execNonQueryProcedure(
                                "sp_LancamentoConta_Gerador",
                                ContaGeradorParams);

                    Resultado = ContaGeradorParams[10].Data.ToString();

                }// fim do for 

            }   // fim do if lancamento > 0
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            GeraLancamentos();
            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " +
                (Lancamento != null ? "'" + Lancamento + "' " : "NULL") +
                " as LancamentoID,  "
                +
                (Resultado != null ? "'" + Resultado + "' " : "NULL") +
                " as Resultado  "));
        }
    }
}