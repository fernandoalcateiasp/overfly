/********************************************************************
especificinf.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.LancamentoID, a.ProprietarioID, a.Observacoes, a.AlternativoID FROM Lancamentos a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.LancamentoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // Contas
    else if (folderID == 29061)
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Lancamentos_Contas a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.LancamentoID = '+ idToFind + ' ' +
                  'ORDER BY (SELECT aa.Grupo ' +
                                'FROM HistoricosPadrao_Contas aa WITH(NOLOCK) ' +
                                    'INNER JOIN Lancamentos bb WITH(NOLOCK) ON (bb.HistoricoPadraoID = aa.HistoricoPadraoID) ' +
                                'WHERE ((bb.LancamentoID = a.LancamentoID) AND (aa.ContaID = a.ContaID) AND (aa.TipoLancamentoID = bb.TipoLancamentoID) AND aa.EhDebito = CASE WHEN (a.Valor < 0) THEN 1 ELSE 0 END)), ' +
                    '(CASE WHEN a.Valor < 0 THEN 0 ELSE 1 END), a.ContaID, ' +
					'(SELECT b.Fantasia FROM Pessoas b WITH(NOLOCK) WHERE a.PessoaID = b.PessoaID), ' +
					'dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4), ' +
					'(SELECT c.Imposto FROM Conceitos c WITH(NOLOCK) WHERE a.ImpostoID = c.ConceitoID), ABS(a.Valor), a.Documento ';
        return 'dso01Grid_DSC';
    }

    // Lan�amentos Filhos
    else if (folderID == 29062)
    {
        dso.SQL = 'EXEC sp_Lancamento_Filhos ' + idToFind;
        return 'dso01Grid_DSC';
    }

    // Detalhes
    else if (folderID == 29063)
    {
        dso.SQL = 'EXEC sp_Lancamento_Detalhes ' + idToFind + ', NULL, 1';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Contas
    else if (pastaID == 29061)
    {
        var nEmpresaData = getCurrEmpresaData();
        var nEmpresaID = nEmpresaData[0]; 
		var nPaisID = nEmpresaData[1];

        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT a.ContaID, dbo.fn_Conta_Identada(a.ContaID, ' + getDicCurrLang() + ', NULL, NULL) AS ContaIdentada, ' +
				'dbo.fn_Conta_AceitaLancamento(a.ContaID) AS AceitaLancamento, ' +
				'b.ItemMasculino AS TipoDetalhamento, b.ItemAbreviado AS Det ' +
                'FROM PlanoContas a WITH(NOLOCK) ' +
                    'LEFT OUTER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (a.TipoDetalhamentoID = b.ItemID) ' +
                'WHERE a.EstadoID=2 ' +
                'ORDER BY a.ContaID';
        }
        else if ( dso.id == 'dsoCmb02Grid_01' )
        {
/*
            dso.SQL = 'SELECT 0 AS RelPesContaID, SPACE(0) AS ContaNome, SPACE(0) AS ContaComplemento ' +
					  'UNION ALL SELECT b.RelPesContaID, dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) AS ContaNome, ' +
					  'c.Fantasia AS ContaComplemento ' +
                      'FROM RelacoesPessoas a, RelacoesPessoas_Contas b, Pessoas c, Bancos d ' +
					  'WHERE a.TipoRelacaoID = 24 AND a.EstadoID = 2 AND a.SujeitoID= ' + nEmpresaID + ' ' + 
					  'AND a.RelacaoID=b.RelacaoID AND b.EstadoID = 2 AND a.ObjetoID = c.PessoaID AND c.EstadoID = 2 ' +
					  'AND c.BancoID = d.BancoID AND d.EstadoID = 2 ' +
                      'ORDER BY ContaNome';
*/                      
            dso.SQL = 'SELECT 0 AS RelPesContaID, SPACE(0) AS ContaNome, SPACE(0) AS ContaComplemento, 0 AS EmpresaID, SPACE(0) AS Empresa ' +
					          'UNION ALL ' +
                'SELECT Contas.RelPesContaID, dbo.fn_ContaBancaria_Nome(Contas.RelPesContaID, 4) AS ContaNome, Agencia.Fantasia AS ContaComplemento, ' +
	                'Empresa.PessoaID AS EmpresaID, Empresa.Fantasia AS Empresa ' +
                'FROM RelacoesPessoas RelacaoContas WITH(NOLOCK) ' +
	                'INNER JOIN RelacoesPessoas_Contas Contas WITH(NOLOCK) ON RelacaoContas.RelacaoID=Contas.RelacaoID ' +
	                'INNER JOIN Pessoas Agencia WITH(NOLOCK) ON RelacaoContas.ObjetoID = Agencia.PessoaID ' +
	                'INNER JOIN Bancos WITH(NOLOCK) ON Agencia.BancoID = Bancos.BancoID ' +
	                'INNER JOIN Pessoas Empresa WITH(NOLOCK) ON Empresa.PessoaID=RelacaoContas.SujeitoID ' +
                'WHERE RelacaoContas.TipoRelacaoID = 24 AND RelacaoContas.EstadoID = 2 AND Contas.EstadoID = 2 AND Agencia.EstadoID = 2 AND Bancos.EstadoID = 2 AND ' +
	                'RelacaoContas.SujeitoID IN  ' +
		                '(SELECT dbo.fn_Empresa_Matriz(' + nEmpresaID + ', 1, NULL) ' +
		                'UNION ' +
		                'SELECT SujeitoID ' +
		                'FROM RelacoesPessoas WITH(NOLOCK) ' +
		                'WHERE EstadoID=2 AND TipoRelacaoID=30 AND ObjetoID=dbo.fn_Empresa_Matriz(' + nEmpresaID + ', 1, NULL)) ' +
                'ORDER BY ContaNome';
        }
        else if ( dso.id == 'dsoCmb03Grid_01' )
        {

            dso.SQL = 'SELECT 0 AS ImpostoID, SPACE(0) AS ImpostoAbrev, SPACE(0) AS Imposto ' +
					  'UNION ALL SELECT ConceitoID AS ImpostoID, Imposto AS ImpostoAbrev, Conceito AS Imposto ' +
                      'FROM Conceitos WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoConceitoID=306 ' +
                      'AND PaisID= ' +  nPaisID + ' ' +
                      'ORDER BY ImpostoAbrev';
        }
        else if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.ContaID, b.Conta, c.ItemAbreviado AS Det, b.TipoDetalhamentoID ' +
                      'FROM Lancamentos_Contas a WITH(NOLOCK), PlanoContas b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK) ' +
                      'WHERE (a.LancamentoID = ' + nRegistroID + ' AND a.ContaID = b.ContaID AND b.TipoDetalhamentoID = c.ItemID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM Lancamentos_Contas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.LancamentoID = ' + nRegistroID + ' AND a.PessoaID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.LanContaID, dbo.fn_DivideZero(b.Valor, a.TaxaMoeda) AS ValorConvertido ' +
                      'FROM Lancamentos a WITH(NOLOCK), Lancamentos_Contas b WITH(NOLOCK) ' +
                      'WHERE (a.LancamentoID = ' + nRegistroID + ' AND a.LancamentoID = b.LancamentoID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.LanContaID, c.Fantasia AS Empresa ' +
                      'FROM Lancamentos a WITH(NOLOCK), Lancamentos_Contas b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
                      'WHERE (a.LancamentoID = ' + nRegistroID + ' AND a.LancamentoID = b.LancamentoID AND b.UnidadeID=c.PessoaID)';
        }
        else if (dso.id == 'dso05GridLkp')
        {
            dso.SQL = 'SELECT b.LanContaID, c.Grupo ' +
                        'FROM Lancamentos a WITH(NOLOCK) ' +
                            'INNER JOIN Lancamentos_Contas b WITH(NOLOCK) ON (a.LancamentoID = b.LancamentoID) ' +
                            'INNER JOIN HistoricosPadrao_Contas c WITH(NOLOCK) ON (c.HistoricoPadraoID = a.HistoricoPadraoID) ' +
                        'WHERE ((a.LancamentoID = ' + nRegistroID + ') AND (b.ContaID = ISNULL(c.ContaID, b.ContaID)) AND (c.TipoLancamentoID = a.TipoLancamentoID))';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(29061);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 29061); //Contas

    vPastaName = window.top.getPastaName(29063);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 29063); //Detalhes

    vPastaName = window.top.getPastaName(29062);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 29062); //Lancamentos Filhos


    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 29061) // Contas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,2], ['LancamentoID', registroID],[6]);
        
        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Contas
    else if (folderID == 29061)
    {
        var aHoldCols = new Array();

		var nHitoricoPadraoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'HistoricoPadraoID' + '\'' + '].value');

		if (parseInt(nHitoricoPadraoID, 10) == 1)
			aHoldCols = [7, 14, 15, 16]; 
		else
			aHoldCols = [7, 13, 14, 15, 16]; 
 
        headerGrid(fg,['Unidade',
                       'Grupo',
                       'ContaID',
					   'Conta',
					   'Valor',
                       'Documento',
                       'Det',
                       'PessoaID',
                       'Pessoa',
                       'Bco/Ag/Cta',
					   'Imposto',
					   'Valor Conv',
					   'Cont',
                       'SI',
                       'TipoDetalhamentoID',
                       'UnidadeID',
                       'LanContaID'], aHoldCols);

        // array de celulas com hint
        glb_aCelHint = [[0,12,'Controle para efeito de emiss�o de cheque'],
						[0,13,'Saldo Inicial OK?']];

        fillGridMask(fg,currDSO,['^LanContaID^dso04GridLkp^LanContaID^Empresa*',
                                 '^LanContaID^dso05GridLkp^LanContaID^Grupo*',
                                 'ContaID',
								 '^ContaID^dso01GridLkp^ContaID^Conta*',
								 'Valor',
								 'Documento',
								 '^ContaID^dso01GridLkp^ContaID^Det*',
								 'PessoaID*',
								 '^PessoaID^dso02GridLkp^PessoaID^Fantasia*',
								 'RelPesContaID',
								 'ImpostoID',
								 '^LanContaID^dso03GridLkp^LanContaID^ValorConvertido*',
								 'Controle',
								 'SaldoInicialOK',
								 '^ContaID^dso01GridLkp^ContaID^TipoDetalhamentoID',
								 'UnidadeID',
								 'LanContaID'],
								 ['','','','','#999999999.99','','','','','','','999999999.99','','','','',''],
                                 ['','','','','(###,###,##0.00)','','','','','','','(###,###,##0.00)','','','','','']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[4,'(###,###,###.00)','S'],[11,'(###,###,###.00)','S']]);

		fg.Redraw = 0;
		
		for ( i=1; i<fg.Rows; i++ )
		{
			if ( (fg.Rows>2) && (i==1) )
			{
			
				fg.TextMatrix(1, getColIndexByColKey(fg, 'ContaID')) = 'Diferen�a';
				fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor')) = - parseFloat(fg.ValueMatrix(1, getColIndexByColKey(fg, 'Valor')));
				fg.TextMatrix(1, getColIndexByColKey(fg, '^LanContaID^dso03GridLkp^LanContaID^ValorConvertido*')) = - 
					parseFloat(fg.ValueMatrix(1, getColIndexByColKey(fg, '^LanContaID^dso03GridLkp^LanContaID^ValorConvertido*')));
			}
			if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')) < 0 )
			{
				fg.Select(i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor'));
				fg.FillStyle = 1;
		        fg.CellForeColor = 0X0000FF;

				fg.Select(i, getColIndexByColKey(fg, '^LanContaID^dso03GridLkp^LanContaID^ValorConvertido*'), 
				    i, getColIndexByColKey(fg, '^LanContaID^dso03GridLkp^LanContaID^ValorConvertido*'));
				fg.FillStyle = 1;
		        fg.CellForeColor = 0X0000FF;

		    }
		}

        alignColsInGrid(fg,[1,4,7,11]);                           
		fg.FrozenCols = 2;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
        
        if ( fg.Rows > 1 )
        {
			fg.Row = 2;
			fg.Col = getColIndexByColKey(fg, 'Valor');
		}	
		
		fg.Redraw = 2;
    }
    // Lancamentos Filhos
    else if (folderID == 29062)
    {
        headerGrid(fg,['Empresa',
					   'Lan�amento',
                       '%',
                       'Valor',
                       'Taxa',
                       'Valor Conv'], []);

        fillGridMask(fg,currDSO,['Empresa',
								 'LancamentoID',
								 'Percentual',
								 'ValorTotalLancamento',
								 'TaxaMoeda',
								 'ValorConvertido'],
								 ['','','','','',''],
                                 ['','','##0.00','###,###,##0.00','###,###,###,##0.0000000','###,###,##0.00']);

        alignColsInGrid(fg,[1,2,3,4,5]);                           

		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Detalhes
    else if (folderID == 29063)
    {
        headerGrid(fg,['Form',
					   'SubForm',
					   'Tipo',
					   'Subtipo',
                       'Registro',
                       'Est',
                       'Data',
                       'Pessoa',
					   'Valor',
					   'Observa��o',
					   'FormID',
					   'SubFormID',
					   'FinanceiroID',
					   'EventoID'], [10, 11, 12, 13]);

        fillGridMask(fg,currDSO,['Form',
								 'SubForm',
								 'Tipo',
								 'SubTipo',
								 'RegistroID',
								 'Estado',
								 'Data',
								 'Pessoa',
								 'Valor',
								 'Observacao',
								 'FormID',
								 'SubFormID',
								 'FinanceiroID',
								 'EventoID'],
								 ['','','','','','','','','','','','','',''],
                                 ['','','','','','','','','(###,###,##0.00)','','','','','']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[4,'(###,###,###)','C'], [8,'(###,###,###.00)','S']]);

		for ( i=1; i<fg.Rows; i++ )
		{
			if ( fg.ValueMatrix(i, 8) < 0 )
			{
				fg.Select(i, 8, i, 8);
				fg.FillStyle = 1;
		        fg.CellForeColor = 0X0000FF;

		    }
		}

		if (fg.Rows > 2)
			fg.Row = 2;

        alignColsInGrid(fg,[4,8]);

		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
}

function js_Lancamentos_AfterEdit(Row, Col)
{
    if (fg.Editable)
    {
        if (Col == getColIndexByColKey(fg, 'RelPesContaID'))
        {
            dsoCmb02Grid_01.recordset.Find('RelPesContaID', fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RelPesContaID')));
            
            if (!((dsoCmb02Grid_01.recordset.BOF) && (dsoCmb02Grid_01.recordset.EOF)) )
                fg.TextMatrix(Row, getColIndexByColKey(fg, 'EmpresaID')) = dsoCmb02Grid_01.recordset['EmpresaID'].value;
        }
    }            
}
