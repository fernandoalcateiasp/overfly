<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="lancamentossup01Html" name="lancamentossup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/lancamentos/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/lancamentos/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="lancamentossup01Body" name="lancamentossup01Body" LANGUAGE="javascript" onload="return window_onload()">

    <!-- //@@ Os dsos sao definidos de acordo com o form -->
        
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p id="lbldtLancamento" name="lbldtLancamento" class="lblGeneral">Data</p>
        <input type="text" id="txtdtLancamento" name="txtdtLancamento" DATASRC="#dsoSup01" DATAFLD="V_dtLancamento" class="fldGeneral"></input>
        <p id="lbldtBalancete" name="lbldtBalancete" class="lblGeneral">Balancete</p>
        <input type="text" id="txtdtBalancete" name="txtdtBalancete" DATASRC="#dsoSup01" DATAFLD="V_dtBalancete" class="fldGeneral"></input>
        <p id="lblLancamentoMaeID" name="lblLancamentoMaeID" class="lblGeneral">Lan�amento M�e</p>
        <input type="text" id="txtLancamentoMaeID" name="txtLancamentoMaeID" DATASRC="#dsoSup01" DATAFLD="LancamentoMaeID" class="fldGeneral"></input>
        <p  id="lblLancamentoConciliado" name="lblLancamentoConciliado" class="lblGeneral">Conc</p>
        <input type="checkbox" id="chkLancamentoConciliado" name="chkLancamentoConciliado" class="fldGeneral" title="Lan�amento conciliado?"></input>
        <p  id="lblEhEstorno" name="lblEhEstorno" class="lblGeneral">Est</p>
        <input type="checkbox" id="chkEhEstorno" name="chkEhEstorno" DATASRC="#dsoSup01" DATAFLD="EhEstorno" class="fldGeneral" title="� Estorno"></input>
		<p id="lblTipoLancamentoID" name="lblTipoLancamentoID" class="lblGeneral">Tipo</p>
		<select id="selTipoLancamentoID" name="selTipoLancamentoID" DATASRC="#dsoSup01" DATAFLD="TipoLancamentoID" class="fldGeneral"></select>
        <p id="lblQuantidade" name="lblQuantidade" class="lblGeneral">Quant</p>
        <input type="text" id="txtQuantidade" name="txtQuantidade" class="fldGeneral" title="Quantidade de Detalhes"></input>
		<p id="lblHistoricoPadraoID" name="lblHistoricoPadraoID" class="lblGeneral">Hist�rico Padr�o</p>
		<select id="selHistoricoPadraoID" name="selHistoricoPadraoID" DATASRC="#dsoSup01" DATAFLD="HistoricoPadraoID" class="fldGeneral"></select>
        <p id="lblHistoricoComplementar" name="lblHistoricoComplementar" class="lblGeneral">Hist�rico Complementar</p>
        <input type="text" id="txtHistoricoComplementar" name="txtHistoricoComplementar" DATASRC="#dsoSup01" DATAFLD="HistoricoComplementar" class="fldGeneral"></input>

		<p id="lblDetalhe" name="lblDetalhe" class="lblGeneral">Detalhe</p>
		<p id="txtDetalhe" name="txtDetalhe" class="fldGeneral"></p>

        <p id="lblNumeroDebitos" name="lblNumeroDebitos" class="lblGeneral" title="N�mero de D�bitos">D</p>
        <input type="text" id="txtNumeroDebitos" name="txtNumeroDebitos" DATASRC="#dsoSup01" DATAFLD="NumeroDebitos" class="fldGeneral" title="N�mero de D�bitos"></input>
        <p id="lblNumeroCreditos" name="lblNumeroCreditos" class="lblGeneral" title="N�mero de Cr�ditos">C</p>
        <input type="text" id="txtNumeroCreditos" name="txtNumeroCreditos" DATASRC="#dsoSup01" DATAFLD="NumeroCreditos" class="fldGeneral" title="N�mero de Cr�ditos"></input>
		<p id="lblMoedaID" name="lblMoedaID" class="lblGeneral">Moeda</p>
		<select id="selMoedaID" name="selMoedaID" DATASRC="#dsoSup01" DATAFLD="MoedaID" class="fldGeneral"></select>
        <p id="lblValorTotalLancamento" name="lblValorTotalLancamento" class="lblGeneral">Total Lan�amento</p>
        <input type="text" id="txtValorTotalLancamento" name="txtValorTotalLancamento" DATASRC="#dsoSup01" DATAFLD="ValorTotalLancamento" class="fldGeneral"></input>
        <p id="lblTaxaMoeda" name="lblTaxaMoeda" class="lblGeneral" title="Taxa de convers�o para moeda comum">Taxa</p>
        <input type="text" id="txtTaxaMoeda" name="txtTaxaMoeda" DATASRC="#dsoSup01" DATAFLD="TaxaMoeda" class="fldGeneral" title="Taxa de convers�o para moeda comum"></input>
        <p id="lblValorTotalConvertido" name="lblValorTotalConvertido" class="lblGeneral" title="Taxa de convers�o dos valores do pedido para moeda comum">Total Convertido</p>
        <input type="text" id="txtValorTotalConvertido" name="txtValorTotalConvertido" DATASRC="#dsoSup01" DATAFLD="ValorTotalConvertido" class="fldGeneral"></input>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
    </div>
    
</body>

</html>
