/********************************************************************
js_especificsup.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        /*sSQL = 'SELECT TOP 1 *, ' +
			   'dbo.fn_Lancamento_Conciliado(-LancamentoID) AS LancamentoConciliado, ' +
               'CONVERT(VARCHAR, dtLancamento, '+DATE_SQL_PARAM+') AS V_dtLancamento, ' +
               'CONVERT(VARCHAR, dtBalancete, '+DATE_SQL_PARAM+') AS V_dtBalancete, ' +
			   'dbo.fn_Lancamento_Totais(LancamentoID, 6) AS ValorTotalConvertido, ' +
			   'CONVERT(NUMERIC(9), dbo.fn_Lancamento_Totais(LancamentoID, 1)) AS NumeroDebitos, ' +
			   'CONVERT(NUMERIC(9), dbo.fn_Lancamento_Totais(LancamentoID, 2)) AS NumeroCreditos, ' +
			   'CONVERT(NUMERIC(11), dbo.fn_Lancamento_Totais(LancamentoID, 7)) AS Quantidade, ' +
			   'CONVERT(NUMERIC(9), dbo.fn_Lancamento_Totais(LancamentoID, 11)) AS SaldoLancMae, ' +
			   'CONVERT(BIT, ISNULL((SELECT UsoSistema FROM HistoricosPadrao WHERE (HistoricoPadraoID = Lancamentos.HistoricoPadraoID)), 0)) AS HPUsoSistema, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=Lancamentos.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'dbo.fn_Lancamento_Detalhe(-1) AS Detalhe ' +
               'FROM Lancamentos  ' +
               'WHERE ProprietarioID = '+ nID + 'ORDER BY LancamentoID DESC';*/
		sSQL = 'SELECT TOP 1 *, ' +
				'dbo.fn_Lancamento_Conciliado(-LancamentoID) AS LancamentoConciliado, ' +
				'CONVERT(VARCHAR, dtLancamento, '+DATE_SQL_PARAM+') AS V_dtLancamento, ' +
				'CONVERT(VARCHAR, dtBalancete, '+DATE_SQL_PARAM+') AS V_dtBalancete, ' +
				'dbo.fn_Lancamento_Totais(LancamentoID, 6) AS ValorTotalConvertido, ' +
				'CONVERT(NUMERIC(9), dbo.fn_Lancamento_Totais(LancamentoID, 1)) AS NumeroDebitos, ' +
				'CONVERT(NUMERIC(9), dbo.fn_Lancamento_Totais(LancamentoID, 2)) AS NumeroCreditos, ' +
				'CONVERT(NUMERIC(11), dbo.fn_Lancamento_Totais(LancamentoID, 7)) AS Quantidade, ' +
				'CONVERT(NUMERIC(9), dbo.fn_Lancamento_Totais(LancamentoID, 11)) AS SaldoLancMae, ' +
				'CONVERT(BIT, ISNULL((SELECT UsoSistema FROM HistoricosPadrao WITH(NOLOCK) WHERE (HistoricoPadraoID = Lancamentos.HistoricoPadraoID)), 0)) AS HPUsoSistema, ' +
				'0 AS Prop1, 0 AS Prop2 ' +
                'FROM Lancamentos WITH(NOLOCK)  ' +
                'WHERE ProprietarioID = '+ nID + ' ORDER BY LancamentoID DESC';
    else
        sSQL = 'SELECT *, ' + 
			   'dbo.fn_Lancamento_Conciliado(-LancamentoID) AS LancamentoConciliado, ' +
               'CONVERT(VARCHAR, dtLancamento, '+DATE_SQL_PARAM+') AS V_dtLancamento, ' +
               'CONVERT(VARCHAR, dtBalancete, '+DATE_SQL_PARAM+') AS V_dtBalancete, ' +
			   'dbo.fn_Lancamento_Totais(LancamentoID, 6) AS ValorTotalConvertido, ' +
			   'CONVERT(NUMERIC(9), dbo.fn_Lancamento_Totais(LancamentoID, 1)) AS NumeroDebitos, ' +
			   'CONVERT(NUMERIC(9), dbo.fn_Lancamento_Totais(LancamentoID, 2)) AS NumeroCreditos, ' +
			   'CONVERT(NUMERIC(11), dbo.fn_Lancamento_Totais(LancamentoID, 7)) AS Quantidade, ' +
			   'CONVERT(NUMERIC(9), dbo.fn_Lancamento_Totais(LancamentoID, 11)) AS SaldoLancMae, ' +
			   'CONVERT(BIT, ISNULL((SELECT UsoSistema FROM HistoricosPadrao WITH(NOLOCK) WHERE (HistoricoPadraoID = Lancamentos.HistoricoPadraoID)), 0)) AS HPUsoSistema, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=Lancamentos.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'dbo.fn_Lancamento_Detalhe(' + nID +  ') AS Detalhe ' +
               'FROM Lancamentos WITH(NOLOCK) WHERE LancamentoID = ' + nID + ' ORDER BY LancamentoID DESC';

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;
    
    sql = 'SELECT *, ' +
		  'dbo.fn_Lancamento_Conciliado(-LancamentoID) AS LancamentoConciliado, ' +
          'CONVERT(VARCHAR, dtLancamento, ' + DATE_SQL_PARAM + ') AS V_dtLancamento, ' +
          'CONVERT(VARCHAR, dtBalancete, '+DATE_SQL_PARAM+') AS V_dtBalancete, ' +
		  'dbo.fn_Lancamento_Totais(LancamentoID, 6) AS ValorTotalConvertido, ' +
		  'CONVERT(NUMERIC(9), dbo.fn_Lancamento_Totais(LancamentoID, 1)) AS NumeroDebitos, ' +
		  'CONVERT(NUMERIC(9), dbo.fn_Lancamento_Totais(LancamentoID, 2)) AS NumeroCreditos, ' +
	      'CONVERT(NUMERIC(11), dbo.fn_Lancamento_Totais(LancamentoID, 7)) AS Quantidade, ' +
		  'CONVERT(NUMERIC(9), dbo.fn_Lancamento_Totais(LancamentoID, 11)) AS SaldoLancMae, ' +
		  'CONVERT(BIT, ISNULL((SELECT UsoSistema FROM HistoricosPadrao WHERE (HistoricoPadraoID = Lancamentos.HistoricoPadraoID)), 0)) AS HPUsoSistema, ' +
          '0 AS Prop1, 0 AS Prop2 ' +
          'FROM Lancamentos WITH(NOLOCK) WHERE LancamentoID = 0';
    
    dso.SQL = sql;          
}              
