/********************************************************************
modalratearlancamentos.js

Library javascript para o modalratearlancamentos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoCmbStatic01 = new CDatatransport('dsoCmbStatic01');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoGrid = new CDatatransport('dsoGrid');
var dsoCmb01Grid_01 = new CDatatransport('dsoCmb01Grid_01');
var dsoCmb02Grid_01 = new CDatatransport('dsoCmb02Grid_01');
var dsoCmb03Grid_01 = new CDatatransport('dsoCmb03Grid_01');
var dsoPesq = new CDatatransport('dsoPesq');
var dsoRatearLanc = new CDatatransport('dsoRatearLanc');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES:

window_onload()
setupPage()
btn_onclick(ctl)
js_gerarLancamentos_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
js_gerarLancamentos_AfterEdit(Row, Col)
js_gerarLancamentos_ChangeEdit()
js_gerarLancamentos_ValidateEdit()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    btnCanc.style.visibility = 'hidden';

    var elem;
    // FG - grid
    // coloca classe no grid
    elem = window.document.getElementById('fg');

    if (elem != null)
        elem.className = 'fldGeneral';

    startGridInterface(fg);

    window_onload_1stPart();

    // ajusta o body do html
    with (modalratearlancamentosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    window.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Ratear Lanšamentos', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    var midPos = 0;

    adjustElementsInForm([['lblLancamentoID', 'txtLancamentoID', 10, 1, 0, -10],
						  ['lblEstado', 'txtEstado', 2, 1, -5],
						  ['lblData', 'txtData', 10, 1, -5],
						  ['lblEhEstorno', 'chkEhEstorno', 3, 1, -5],
						  ['lblTipoLancamento', 'txtTipoLancamento', 5, 1, -5],
						  ['lblHistoricoPadrao', 'txtHistoricoPadrao', 22, 1, -5],
						  ['lblMoeda', 'txtMoeda', 4, 1, -5],
						  ['lblValorLancamento', 'txtValorLancamento', 11, 1, -5]], null, null, true);

    btnOK.disabled = false;
    btnOK.style.top = parseInt(txtValorLancamento.currentStyle.top, 10);
    btnOK.style.left = parseInt(txtValorLancamento.currentStyle.left, 10) + parseInt(txtValorLancamento.currentStyle.width, 10) + ELEM_GAP - 5;

    btnRefresh.style.height = parseInt(btnOK.currentStyle.height, 10);
    btnRefresh.style.width = parseInt(btnOK.currentStyle.width, 10);
    btnRefresh.style.top = parseInt(btnOK.currentStyle.top, 10);
    btnRefresh.style.left = parseInt(btnOK.currentStyle.left, 10) + parseInt(btnOK.currentStyle.width, 10) + ELEM_GAP - 5;

    txtLancamentoID.disabled = true;
    txtEstado.disabled = true;
    txtData.disabled = true;
    chkEhEstorno.disabled = true;
    txtTipoLancamento.disabled = true;
    txtHistoricoPadrao.disabled = true;
    txtMoeda.disabled = true;
    txtValorLancamento.disabled = true;

    txtLancamentoID.value = glb_nLancamentoID;
    txtEstado.value = glb_sEstado;
    txtData.value = glb_sData;
    chkEhEstorno.checked = glb_bEhEstorno;
    txtTipoLancamento.value = glb_sTipoLancamento;
    txtHistoricoPadrao.value = glb_sHistoricoPadrao;
    txtMoeda.value = glb_sMoeda;
    txtValorLancamento.value = glb_nValorTotalLancamento;

    // ajusta o divPesquisa
    with (divPesquisa.style) {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        //left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = parseInt(btnRefresh.currentStyle.height, 10) + ELEM_GAP;
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.style.top, 10) + parseInt(divPesquisa.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - divFG.offsetTop - (2 * ELEM_GAP);
        //height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(top);
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    startGridInterface(fg);

    fillGridData();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        rateiaLancamento();
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        // esta funcao trava o html contido na janela modal
        lockControlsInModalWin(true);

        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
    else if ((ctl.id).toUpperCase() == 'BTNREFRESH')
        fillGridData();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    lockControlsInModalWin(true);

    // zera o grid
    fg.Rows = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT DISTINCT b.PessoaID, b.Fantasia, ' +
				'CONVERT(NUMERIC(11,2), 0) AS Valor, CONVERT(NUMERIC(5,2), 0) AS Percentual ' +
		 'FROM RelacoesPesRec a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
		 'WHERE (a.TipoRelacaoID = 12 AND a.EstadoID = 2 AND a.ObjetoID = 999 AND ' +
			'a.dtContabilidade IS NOT NULL AND a.SujeitoID = b.PessoaID AND b.EstadoID = 2 AND ' +
			'b.PessoaID <> ' + glb_nEmpresaID + ') ' +
		 'ORDER BY b.Fantasia';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '10';
    fg.FrozenCols = 0;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg, ['Empresa',
				   '%',
				   'Valor',
				   'EmpresaID'], [3]);

    fillGridMask(fg, dsoGrid, ['Fantasia*',
							 'Percentual',
							 'Valor',
							 'PessoaID'],
							 ['', '999.99', '999999999.99', ''],
							 ['', '##0.00', '###,###,##0.00', '']);

    gridHasTotalLine(fg, glb_sEmpresa, 0XC0C0C0, null, true, [[getColIndexByColKey(fg, 'Percentual'), '##0.00', 'S'],
															 [getColIndexByColKey(fg, 'Valor'), '###,###,##0.00', 'S']]);

    fg.Redraw = 0;

    alignColsInGrid(fg, [1, 2]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.ColWidth(1) = FONT_WIDTH * 150;
    fg.ColWidth(2) = FONT_WIDTH * 200;

    fg.FrozenCols = 1;

    fg.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        fg.Editable = true;
        window.focus();
        fg.focus();
    }
    else {
        ;
    }

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;
    fg.Redraw = 2;
}

function rateiaLancamento() {
    var i, j;

    var strPars = new String();
    var sEmpresas = '';

    lockControlsInModalWin(true);

    strPars += '?nLancamentoID=' + escape(glb_nLancamentoID);

    for (i = 2; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Percentual')) > 0)
            sEmpresas += '/' + fg.ValueMatrix(i, getColIndexByColKey(fg, 'PessoaID')) +
						 '*' + fg.ValueMatrix(i, getColIndexByColKey(fg, 'Percentual'));
    }

    sEmpresas += '/';

    strPars += '&sEmpresas=' + escape(sEmpresas);
    strPars += '&nUsuarioID=' + escape(glb_nUserID);

    dsoRatearLanc.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/lancamentos/serverside/ratearlancamentos.aspx' + strPars;
    dsoRatearLanc.ondatasetcomplete = rateiaLancamento_DSC;
    dsoRatearLanc.refresh();
}

function rateiaLancamento_DSC() {
    lockControlsInModalWin(false);

    if (!(dsoRatearLanc.recordset.BOF && dsoRatearLanc.recordset.EOF)) {
        if ((dsoRatearLanc.recordset['Resultado'].value != null) &&
			 (dsoRatearLanc.recordset['Resultado'].value != '')) {
            if (window.top.overflyGen.Alert(dsoRatearLanc.recordset['Resultado'].value) == 0)
                return null;

            return null;
        }
        else if ((dsoRatearLanc.recordset['LancamentoID'].value != null) &&
			 (dsoRatearLanc.recordset['LancamentoID'].value != '')) {
            // esta funcao trava o html contido na janela modal
            lockControlsInModalWin(true);

            sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, dsoRatearLanc.recordset['LancamentoID'].value);
        }
    }
}

/********************************************************************
Onclick dos checkbox
********************************************************************/
function chkOnClick() {
    if ((this.id).toUpperCase() == 'CHKEHESTORNO') {
        startDynamicCmbs();
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_gerarLancamentos_AfterRowColChange(grid, OldRow, OldCol, NewRow, NewCol) {
    var firstLine = 0;

    if (glb_totalCols__ != false)
        firstLine = 1;

    if (NewRow > firstLine) {
        grid.Row = NewRow;
    }

    if (grid.Editable) {
        if (OldRow != NewRow) {
            ;
        }
    }

    // grid tem primeira linha de totalizacao
    if (glb_totalCols__) {
        if (grid.Row == 1) {
            if (grid.Rows > 2)
                grid.Row = 2;
        }
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_gerarLancamentos_AfterEdit(Row, Col) {
    var i = 0;
    var nFirstRow = (glb_totalCols__ ? 2 : 1);
    var nPercent = 0;

    if (fg.Editable) {
        if ((fg.col == getColIndexByColKey(fg, 'Percentual')) ||
			 (fg.col == getColIndexByColKey(fg, 'Valor'))) {
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

            for (i = nFirstRow; i < fg.Rows; i++) {
                nPercent += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Percentual'));
            }

            fg.TextMatrix(1, getColIndexByColKey(fg, 'Percentual')) = 100 - nPercent;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor')) = (glb_nValorTotalLancamento / 100) * (100 - nPercent);
        }
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_gerarLancamentos_ChangeEdit() {
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_gerarLancamentos_ValidateEdit() {
    var fg_EditText = '';
    var gridStatus = fg.Editable;
    var nFirstRow = (glb_totalCols__ ? 2 : 1);
    var nPercentual = 0;
    var nValor = 0;

    if ((glb_nValorTotalLancamento == null) || (isNaN(glb_nValorTotalLancamento)) || (glb_nValorTotalLancamento == 0)) {
        fg.EditText = 0;
    }
    else if (gridStatus) {
        fg.Editable = false;

        if (fg.Col == getColIndexByColKey(fg, 'Percentual')) {
            nPercentual = parseFloat(getNumAlphaStripedEx(trimStr(fg.EditText), DECIMAL_SEP, false));
            if ((nPercentual == null) || isNaN(nPercentual) || (nPercentual == 0) || (nPercentual <= 0) || (nPercentual > 100)) {
                fg.EditText = 0;
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Valor')) = 0;
            }
            else {
                if (chkOverflowValue(fg.Row, getColIndexByColKey(fg, 'Percentual'), nFirstRow, nPercentual, 100)) {
                    if (window.top.overflyGen.Alert('Percentual excedeu o permitido.') == 0)
                        return null;

                    fg.EditText = 0;
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Valor')) = 0;
                }
                else {
                    fg.EditText = getNumAlphaStripedEx(trimStr(fg.EditText), DECIMAL_SEP, false);
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Valor')) = (glb_nValorTotalLancamento / 100) * nPercentual;
                }
            }
        }
        else if (fg.Col == getColIndexByColKey(fg, 'Valor')) {
            nValor = parseFloat(getNumAlphaStripedEx(trimStr(fg.EditText), DECIMAL_SEP, false));
            if ((nValor == null) || isNaN(nValor) || (nValor == 0) || (nValor > glb_nValorTotalLancamento)) {
                fg.EditText = 0;
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Percentual')) = 0;
            }
            else {
                if (chkOverflowValue(fg.Row, getColIndexByColKey(fg, 'Valor'), nFirstRow, nValor, glb_nValorTotalLancamento)) {
                    if (window.top.overflyGen.Alert('Valor excedeu o permitido.') == 0)
                        return null;

                    fg.EditText = 0;
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Percentual')) = 0;
                }
                else {
                    fg.EditText = getNumAlphaStripedEx(trimStr(fg.EditText), DECIMAL_SEP, false);
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Percentual')) = (nValor / glb_nValorTotalLancamento) * 100;
                }
            }
        }
    }
    fg.Editable = gridStatus;
}

function chkOverflowValue(nRow, nCol, nFirstRow, nCurrValue, nSupValue) {
    var nAcumul = 0;
    var i = 0;

    nAcumul += nCurrValue;
    for (i = nFirstRow; i < fg.Rows; i++) {
        if (i != nRow)
            nAcumul += fg.ValueMatrix(i, nCol);
    }

    return (nAcumul > nSupValue);
}
