/********************************************************************
modalgerarlancamentospendentes.js

Library javascript para o modalgerarlancamentospendentes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;


var dsoGrid = new CDatatransport('dsoGrid');
var dsoTipoLancamento = new CDatatransport('dsoTipoLancamento');
var dsoCmbHPData = new CDatatransport('dsoCmbHPData');
var dsoGrava = new CDatatransport('dsoGrava');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalgerarlancamentospendentesBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalgerarlancamentospendentesDblClick(grid, Row, Col)
js_modalgerarlancamentospendentesKeyPress(KeyAscii)
js_modalgerarlancamentospendentes_ValidateEdit()
js_modalgerarlancamentospendentes_BeforeEdit(grid, row, col)
js_modalgerarlancamentospendentes_AfterEdit(Row, Col)
js_fg_AfterRowColmodalgerarlancamentospendentes (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalgerarlancamentospendentesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a modal
    // refreshParamsAndDataAndShowModalWin(true);
    loadDsoCmbHPData();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Gerar Lan�amentos Pendentes', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK
    btnOK.disabled = true;
	btnFillGrid.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblEhEstorno','chkEhEstorno',3,1,-10,-10],
						  ['lblTipoLancamentoID','selTipoLancamentoID',10,1,-4],
						  ['lblHistoricoPadraoID','selHistoricoPadraoID',25,1]], null, null, true);

	selTipoLancamentoID.selectedIndex = -1;
	selTipoLancamentoID.onchange = selTipoLancamentoID_onchange;
	selHistoricoPadraoID.selectedIndex = -1;
	selHistoricoPadraoID.onchange = selHistoricoPadraoID_onchange;
	chkEhEstorno.onclick = chkEhEstorno_onclick;
	
	adjustLabelsCombos();

    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
			  btnFillGrid.offsetTop + btnFillGrid.offsetHeight + 2;
        width = selHistoricoPadraoID.offsetLeft + selHistoricoPadraoID.offsetWidth + ELEM_GAP;    
        height = selHistoricoPadraoID.offsetTop + selHistoricoPadraoID.offsetHeight;    
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + selHistoricoPadraoID.offsetTop;
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		value = 'Lan�amento';
		title = 'Gerar lan�amento?';
    }
    
    with (btnFillGrid)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		style.top = btnOK.currentStyle.top;
		style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnFillGrid.currentStyle.width, 10) - 4;
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtDataFiltro_onKeyPress()
{
    if ( event.keyCode == 13 )
	{
		fg.Rows = 1;
		// ajusta estado dos botoes
		setupBtnsFromGridState();
	}
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnFillGrid')
    {
		listar();
    }
    // codigo privado desta janela
    else
    {        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    lockControlsInModalWin(true);
    
    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    
	btnOK.disabled = true;

	if (bHasRowsInGrid)
	{
		for (i=1; i<fg.Rows; i++)
		{
			if (getCellValueByColKey(fg, 'OK', i) != 0)
			{
				btnOK.disabled = false;
				break;
			}	
		}
	}
	
	btnFillGrid.disabled = false;
}

function loadDsoCmbHPData()
{
	glb_nDSOs = 2;
    // parametrizacao do dso dsoGrid
    setConnection(dsoCmbHPData);

	dsoCmbHPData.SQL = 'SELECT DISTINCT b.HistoricoPadraoID AS fldID, b.HistoricoPadrao AS fldName, ' +
					'CONVERT(BIT, (SELECT COUNT(*) FROM EventosContabeis WITH(NOLOCK) WHERE (HistoricoPadraoID = a.HistoricoPadraoID AND a.TipoLancamentoID = 1571 AND a.LancamentoID IS NULL))) AS Pagamento, ' +
					'CONVERT(BIT, (SELECT COUNT(*) FROM EventosContabeis WITH(NOLOCK) WHERE (HistoricoPadraoID = a.HistoricoPadraoID AND a.TipoLancamentoID = 1572 AND a.LancamentoID IS NULL))) AS Recebimento, ' +
					'CONVERT(BIT, (SELECT COUNT(*) FROM EventosContabeis WITH(NOLOCK) WHERE (HistoricoPadraoID = a.HistoricoPadraoID AND a.TipoLancamentoID = 1573 AND a.LancamentoID IS NULL))) AS Importe ' +
				'FROM EventosContabeis a WITH(NOLOCK), HistoricosPadrao b WITH(NOLOCK) ' +
				'WHERE (a.HistoricoPadraoID=b.HistoricoPadraoID AND ' +
					'a.LancamentoID IS NULL AND dbo.fn_EventosContabeis_Empresa(a.EventoID) = ' + glb_nEmpresaID + ' AND ' +
					'b.AnteciparLancamento = 1)  ' +
				'ORDER BY b.HistoricoPadrao';

    dsoCmbHPData.ondatasetcomplete = loadDsoCmbHPData_DSC;
    
	dsoCmbHPData.Refresh();

    // parametrizacao do dso dsoGrid
    setConnection(dsoTipoLancamento);

	dsoTipoLancamento.SQL = 'SELECT DISTINCT c.ItemID AS fldID, c.ItemAbreviado AS fldName, c.Ordem ' +
				'FROM EventosContabeis a WITH(NOLOCK), HistoricosPadrao b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK) ' +
				'WHERE (a.HistoricoPadraoID=b.HistoricoPadraoID AND ' +
					'a.LancamentoID IS NULL AND dbo.fn_EventosContabeis_Empresa(a.EventoID) = ' + glb_nEmpresaID + ' AND ' +
					'b.AnteciparLancamento = 1 AND a.TipoLancamentoID = c.ItemID) ' +
				'ORDER BY c.Ordem';

    dsoTipoLancamento.ondatasetcomplete = loadDsoCmbHPData_DSC;
    
	dsoTipoLancamento.Refresh();
	
}

function loadDsoCmbHPData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs==0)
	{
		fg.Rows = 1;
		clearComboEx(['selTipoLancamentoID']);
		
		if (! ((dsoTipoLancamento.recordset.BOF)&&(dsoTipoLancamento.recordset.EOF)) )
		{
			dsoTipoLancamento.recordset.MoveFirst();
			while (! dsoTipoLancamento.recordset.EOF )
			{
			    optionStr = dsoTipoLancamento.recordset['fldName'].value;
			    optionValue = dsoTipoLancamento.recordset['fldID'].value;
				var oOption = document.createElement("OPTION");
				oOption.text = optionStr;
				oOption.value = optionValue;
				selTipoLancamentoID.add(oOption);
				dsoTipoLancamento.recordset.MoveNext();
			}
		}

		selTipoLancamentoID.selectedIndex = -1;
		adjustLabelsCombos();
		setupBtnsFromGridState();

		showExtFrame(window, true);
		lockControlsInModalWin(false);
		
		selTipoLancamentoID.disabled = (selTipoLancamentoID.options.length == 0);
		selHistoricoPadraoID.disabled = true;
		
		window.focus();

		if ( !chkEhEstorno.disabled )
			chkEhEstorno.focus();
	}
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

	if (selTipoLancamentoID.selectedIndex == -1)
	{
		if ( window.top.overflyGen.Alert('Selecione um Tipo de Lan�amento.') == 0 )
            return null;
		
		lockControlsInModalWin(false);
		return null;            
	}
	else if (selHistoricoPadraoID.selectedIndex == -1)
	{
		if ( window.top.overflyGen.Alert('Selecione um Hist�rico Padr�o.') == 0 )
            return null;

		lockControlsInModalWin(false);
		return null;            
	}

	var aGrid = null;
	var i = 0;
	
	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;
    
    // zera o grid
    fg.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	dsoGrid.SQL = 'SELECT b.RecursoFantasia AS Form, b.RecursoFantasia AS SubForm, a.RegistroID, ' +
			'CONVERT(BIT, 0) AS OK, a.EventoID,	a.dtData, a.dtBalancete ' +
		'FROM EventosContabeis a WITH(NOLOCK), Recursos b WITH(NOLOCK), Recursos c WITH(NOLOCK) ' +
		'WHERE a.LancamentoID IS NULL AND dbo.fn_EventosContabeis_Empresa(a.EventoID) = ' + glb_nEmpresaID + ' AND ' + 
			'a.EhEstorno = ' + (chkEhEstorno.checked ? '1' : '0') + ' AND ' +
			'a.TipoLancamentoID = ' + selTipoLancamentoID.value + ' AND ' +
			'a.HistoricoPadraoID = ' + selHistoricoPadraoID .value + ' AND ' +
		'a.FormID=b.RecursoID AND a.SubFormID=c.RecursoID ' +
		'ORDER BY Form, SubForm, RegistroID';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
	dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

    headerGrid(fg,['Form',
				   'Subform',
				   'Registro',
				   'OK',
				   'Evento',
				   'Data',
				   'Balancete'], []);

    fillGridMask(fg,dsoGrid,['Form*',
							 'SubForm*',
							 'RegistroID*',
							 'OK',
							 'EventoID*',
							 'dtData*',
							 'dtBalancete*'],
							 ['','','','','','99/99/9999','99/99/9999'],
							 ['','','','','',dTFormat,dTFormat]);

    fg.Redraw = 0;
    
    alignColsInGrid(fg,[2, 4]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
	fg.FrozenCols = 1;

	paintCellsSpecialyReadOnly();
	    
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
		fg.Editable = true;
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;
    fg.Redraw = 2;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var sEventos = '';

    lockControlsInModalWin(true);
    
    strPars += '?nEmpresaID=' + escape(glb_nEmpresaID);
    strPars += '&nHPID=' + escape(selHistoricoPadraoID.value);
    strPars += '&sEventos=';
    
    for (i=1; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'OK', i) != 0)
			sEventos += '/' + escape(getCellValueByColKey(fg, 'EventoID*', i));
	}
	
	if (sEventos != '')
		sEventos += '/';
	else
	{
		if ( window.top.overflyGen.Alert('Selecione um evento.') == 0 )
            return null;
		
		lockControlsInModalWin(false);
		return null;            
	}

	strPars += escape(sEventos);
	
	try
	{
		dsoGrava.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/lancamentos/serverside/lancamentospendentes.aspx' + strPars;
		dsoGrava.ondatasetcomplete = saveDataInGrid_DSC;
		dsoGrava.refresh();
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
            return null;

		lockControlsInModalWin(false);
		glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
	}
}

function chkEhEstorno_onclick()
{
	fg.Rows = 1;
}

function selHistoricoPadraoID_onchange()
{
	adjustLabelsCombos();
}

function selTipoLancamentoID_onchange()
{
	var sFilter = '';
	fg.Rows = 1;
	clearComboEx(['selHistoricoPadraoID']);
	
	if (selTipoLancamentoID.value == 1571)
		sFilter = 'Pagamento=1';
	else if (selTipoLancamentoID.value == 1572)
		sFilter = 'Recebimento=1';
	else if (selTipoLancamentoID.value == 1573)
		sFilter =  'Importe = 1';
		
	dsoCmbHPData.recordset.setFilter(sFilter);
	dsoCmbHPData.recordset.MoveFirst();
	
    while (! dsoCmbHPData.recordset.EOF )
    {
        optionStr = dsoCmbHPData.recordset['fldName'].value;
        optionValue = dsoCmbHPData.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selHistoricoPadraoID.add(oOption);
        dsoCmbHPData.recordset.MoveNext();
    }

	selHistoricoPadraoID.selectedIndex = -1;
	selHistoricoPadraoID.disabled = (selHistoricoPadraoID.options.length == 0);
	adjustLabelsCombos();
	
	// ajusta estado dos botoes
	setupBtnsFromGridState();
}

function adjustLabelsCombos()
{
    setLabelOfControl(lblTipoLancamentoID, selTipoLancamentoID.value);
    setLabelOfControl(lblHistoricoPadraoID, selHistoricoPadraoID.value);
}

function listar()
{
	refreshParamsAndDataAndShowModalWin(false);
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);

    if ((dsoGrava.recordset['Resultado'].value != null) && (dsoGrava.recordset['Resultado'].value != ''))
	{
	    if ( window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0 )
			return null;			
	}

	glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerarlancamentospendentesBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerarlancamentospendentesDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    // trap so colunas editaveis
    if ( getColIndexByColKey(grid, 'OK') != Col)
        return true;
        
    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);
    
    glb_PassedOneInDblClick = true;
    
    // limpa coluna se tem um check box checado
    for ( i=1; i<grid.Rows; i++ )
    {
        if ( grid.ValueMatrix(i, Col) != 0 )
        {
            bFill = false;
            break;
        }
    }
    
    for ( i=1; i<grid.Rows; i++ )
    {
        if ( bFill )
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarlancamentospendentesKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarlancamentospendentes_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarlancamentospendentes_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarlancamentospendentes_AfterEdit(Row, Col)
{
	setupBtnsFromGridState();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalgerarlancamentospendentes(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return true;

	setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************
