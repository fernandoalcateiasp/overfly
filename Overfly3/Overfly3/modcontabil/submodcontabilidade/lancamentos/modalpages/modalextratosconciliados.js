/********************************************************************
modalextratosconciliados.js

Library javascript para o modalextratosconciliados.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoGen01 = new CDatatransport('dsoGen01');
// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    getDataInServer();
        
    // mostrar a janela movido para o final de preenchimento do grid
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', null );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Extrato Conciliado', 1);
    
    loadDataAndTreatInterface();    
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;
        
    // Ajusta botoes OK/Cancel para esta janela
    // O botao cancela nao e usado neste asp
    with (btnCanc.style)
    {
        visibility = 'hidden';
        width = 0;
        height = 0;
    }
    
    // O botao OK nao e usado neste asp
    with (btnOK.style)
    {
        visibility = 'hidden';
        width = 0;
        height = 0;
    }
    
    btnOK.disabled = true;
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - (2 * ELEM_GAP);    
        height = heightFree - (2 * ELEM_GAP);    
    }
    
    hr_L_FGBorder.style.visibility = 'hidden';
    hr_R_FGBorder.style.visibility = 'hidden';
    hr_B_FGBorder.style.visibility = 'hidden';
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width) - 1;
        height = parseInt(document.getElementById('divFG').currentStyle.height) - 1;
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServer()
{
    // parametrizacao do dso dsoCmbDynamic02 (designado para selObjetoID)
    setConnection(dsoGen01);
    
    dsoGen01.SQL = 'SELECT a.ExtratoID, c.RecursoAbreviado, a.dtLancamento, a.Historico ' +
		'FROM ExtratosBancarios_Lancamentos a WITH(NOLOCK), ExtratosBancarios b WITH(NOLOCK), Recursos c WITH(NOLOCK) ' +
		'WHERE (a.LanContaID = ' + glb_nLanContaID + ' AND a.ExtratoID = b.ExtratoID AND ' +
		'b.EstadoID = c.RecursoID) ' +
		'ORDER BY a.ExtratoID';

    dsoGen01.ondatasetcomplete = fillModalGrid_DSC;
    dsoGen01.Refresh();
}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGrid_DSC()
{
    var i, j, nSumImp;
    var nAliquota = 0;
    var dTFormat = '';

    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    fg.Redraw = 0;

    headerGrid(fg,['ID',
                   'Est',
                   'Data',
                   'Histórico'],[]);
    
    fillGridMask(fg,dsoGen01,['ExtratoID',
							  'RecursoAbreviado',
							  'dtLancamento',
							  'Historico'],
                              ['','','99/99/9999',''],
                              ['','',dTFormat,'']);

    alignColsInGrid(fg,[0]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    fg.Redraw = 2;
            
    with (modalextratosconciliadosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
               
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();
}

function fg_ExtratosConciliadosDblClick()
{
	if (fg.Row <= 0)
		return true;
		
	var empresa = getCurrEmpresaData();
	var nExtratoID = 0;
	var sExecCarrier = '';
	
	nExtratoID = getCellValueByColKey(fg, 'ExtratoID', fg.Row);
	sExecCarrier = "sendJSCarrier(getHtmlId(), 'SHOWEXTRATOBANCARIO', new Array(" + empresa[0] + "," +  nExtratoID + "))";
	sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, sExecCarrier);
}