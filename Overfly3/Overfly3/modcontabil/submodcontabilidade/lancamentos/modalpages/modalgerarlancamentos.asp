
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>
<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalgerarlancamentosHtml" name="modalgerarlancamentosHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/lancamentos/modalpages/modalgerarlancamentos.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/lancamentos/modalpages/modalgerarlancamentos.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf

Dim i, sCaller, nEmpresaID, nEmpresaPaisID, nUserID, nMoedaID, nLancamentoID

nEmpresaID = 0
nEmpresaPaisID = 0
nUserID = 0
nLancamentoID = 0

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaPaisID").Count    
    nEmpresaPaisID = Request.QueryString("nEmpresaPaisID")(i)
Next

For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

For i = 1 To Request.QueryString("nLancamentoID").Count    
    nLancamentoID = Request.QueryString("nLancamentoID")(i)
Next

For i = 1 To Request.QueryString("sCaller").Count
    sCaller = Request.QueryString("sCaller")(i)
Next

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_sCaller = " & Chr(34) & CStr(sCaller) & Chr(34) & ";"
Response.Write vbcrlf

Response.Write "var glb_nLancamentoID = " & CStr(nLancamentoID) & ";"
Response.Write vbcrlf

Dim strSQL, rsData
Dim strSQL2, rsData2

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT DISTINCT b.PessoaID, b.Fantasia, " & _
		 "(SELECT TOP 1 d.ConceitoID " & _
				"FROM RelacoesPesRec_Moedas c WITH(NOLOCK), Conceitos d WITH(NOLOCK) " & _
				"WHERE (c.RelacaoID = a.RelacaoID AND c.MoedaID = d.ConceitoID AND d.EstadoID = 2) " & _
				"ORDER BY c.Faturamento DESC, c.Ordem) AS MoedaID " & _
		 "FROM RelacoesPesRec a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " & _
		 "WHERE (a.TipoRelacaoID = 12 AND a.EstadoID = 2 AND a.ObjetoID = 999 AND " & _
			"a.dtContabilidade IS NOT NULL AND a.SujeitoID = b.PessoaID AND b.EstadoID = 2 AND " & _
			"b.PessoaID <> " & CStr(nEmpresaID) & ") " & _
		 "ORDER BY b.Fantasia "
				
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText


Set rsData2 = Server.CreateObject("ADODB.Recordset")

strSQL2 = "SELECT DISTINCT b.PessoaID, b.Fantasia, " & _
		 "(SELECT TOP 1 d.ConceitoID " & _
				"FROM RelacoesPesRec_Moedas c WITH(NOLOCK), Conceitos d WITH(NOLOCK) " & _
				"WHERE (c.RelacaoID = a.RelacaoID AND c.MoedaID = d.ConceitoID AND d.EstadoID = 2) " & _
				"ORDER BY c.Faturamento DESC, c.Ordem) AS MoedaID " & _
		 "FROM RelacoesPesRec a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " & _
		 "WHERE (a.TipoRelacaoID = 12 AND a.EstadoID = 2 AND a.ObjetoID = 999 AND " & _
			"a.dtContabilidade IS NOT NULL AND a.SujeitoID = b.PessoaID AND b.EstadoID = 2 AND " & _
			"b.PessoaID = " & CStr(nEmpresaID) & ") " & _
		 "ORDER BY b.Fantasia "
				
rsData2.Open strSQL2, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write "var glb_nMoedaID = 0;"

If (Not (rsData2.BOF AND rsData2.EOF) ) Then

	Response.Write "var glb_nMoedaID = " & CStr(rsData2.Fields("MoedaID").Value) & ";"
	Response.Write vbcrlf

End If

Set rsData2 = Nothing

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
js_gerarLancamentos_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_gerarLancamentos_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ChangeEdit>
<!--
 js_gerarLancamentos_ChangeEdit();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_gerarLancamentos_ValidateEdit();
//-->
</SCRIPT>

</head>

<body id="modalgerarlancamentosBody" name="modalgerarlancamentosBody" LANGUAGE="javascript" onload="return window_onload()">
     <div id="divPesquisa" name="divPesquisa" class="divGeneral">
        <p  id="lblEhEstorno" name="lblEhEstorno" class="lblGeneral">Est</p>
        <input type="checkbox" id="chkEhEstorno" name="chkEhEstorno" class="fldGeneral" title="� Estorno?" onclick="return chkOnClick(this)"></input>
		<p id="lblTipoLancamentoID" name="lblTipoLancamentoID" class="lblGeneral">Tipo</p>
		<select id="selTipoLancamentoID" name="selTipoLancamentoID" class="fldGeneral" LANGUAGE="javascript" onchange="return cmbOnChange(this)">
		</select>
        <p  id="lblUsoCotidiano" name="lblUsoCotidiano" class="lblGeneral">UC</p>
        <input type="checkbox" id="chkUsoCotidiano" name="chkUsoCotidiano" class="fldGeneral" title="Somente HP de uso cotidiano ou todos dispon�veis?" onclick="return chkOnClick(this)"></input>
		<p id="lblHistoricoPadraoID" name="lblHistoricoPadraoID" class="lblGeneral">Hist�rico Padr�o</p>
		<select id="selHistoricoPadraoID" name="selHistoricoPadraoID" class="fldGeneral"  onchange="return cmbOnChange(this)"></select>
        <p id="lblHistoricoComplementar" name="lblHistoricoComplementar" class="lblGeneral">Hist�rico Complementar</p>
        <input type="text" id="txtHistoricoComplementar" name="txtHistoricoComplementar" class="fldGeneral"></input>
        <p id="lblValorTotalLancamento" name="lblValorTotalLancamento" class="lblGeneral">Valor</p>
        <input type="text" id="txtValorTotalLancamento" name="txtValorTotalLancamento" class="fldGeneral"></input>
		<p id="lblEhDefault" name="lblEhDefault" class="lblGeneral" LANGUAGE=javascript onclick="return invertChkBox(chkRefrInf)" title="Somente contas default?">Def</p>
        <input type="checkbox" id="chkEhDefault" name="chkEhDefault" class="fldGeneral" title="Somente contas default?"></input>
		<input type="button" id="btnFillGrid" name="btnFillGrid" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Listar contas">
	</div>		

     <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
	
	<div id="divFields" name="divFields" class="divGeneral">		
        <p id="lblEmpresas" name="lblEmpresas" class="lblGeneral">Empresas</p>
        <select id="selEmpresas" name="selEmpresas" class="fldGeneral" MULTIPLE onchange="return cmbOnChange(this)">
<%        

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("PessoaID").Value & "'")
			
			If (CLng(nEmpresaID) = CLng(rsData.Fields("PessoaID").Value)) Then
				Response.Write( " SELECTED " )
			End If

			Response.Write( ">" & rsData.Fields("Fantasia").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
	Set rsData = Nothing
%>        
        </select>
		<input type="button" id="btnRatear" name="btnRatear" value="Ratear" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Ratear lan�amento entre empresas"></input>
        <p id="lblArgumento" name="lblArgumento" class="lblGeneral">Argumento</p>
        <input type="text" id="txtArgumento" name="txtArgumento" class="fldGeneral"></input>
		<p id="lblAtivo" name="lblAtivo" class="lblGeneral" LANGUAGE=javascript onclick="return invertChkBox(chkRefrInf)" title="Somente pessoas ativas?">Ativas</p>
        <input type="checkbox" id="chkAtivo" name="chkAtivo" class="fldGeneral" title="Somente pessoas ativas?"></input>
		<input type="image" id="btnListarDetalhes" name="btnListarDetalhes" class="fldGeneral" title="Preencher combo" onclick="javascript:return btnLupaClicked()" WIDTH="24" HEIGHT="23">
		<p id="lblDetalhes" name="lblDetalhes" class="lblGeneral">Detalhes</p>
		<select id="selDetalhes" name="selDetalhes" class="fldGeneral" MULTIPLE LANGUAGE="javascript" onchange="return cmbOnChange(this)"></select>
		<input type="button" id="btnPreencher" name="btnPreencher" value="Preencher" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Preencher Detalhes"></input>
		<input type="button" id="btnLimpar" name="btnLimpar" value="Limpar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"title="Limpar Detalhe"></input>
		<input type="button" id="btnExcluir" name="btnExcluir" value="Excluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Excluir Conta"></input>    
		<input type="button" id="btnLancamento" name="btnLancamento" value="Lan�amento" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Gerar lan�amento"></input>    
    </div>    

    <input type="button" id="btnOK" name="btnOK" value="Lan�amento" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
