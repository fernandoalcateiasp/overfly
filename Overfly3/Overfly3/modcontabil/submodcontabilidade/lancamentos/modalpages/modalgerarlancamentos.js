/********************************************************************
modalgerarlancamentos.js

Library javascript para o modalgerarlancamentos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var modPesqRel_TimerHandler = null;
var glb_CounterCmbsDynamics = 0;
var glb_CounterCmbsStatics = 0;
var glb_FirstTime = false;
var glb_nQtdCmbsLoaded = 0;

var glb_nMaxStringSize = 1024 * 1;
var glb_nCurrLineInSave = 0;

var glb_nSaveTimerInterval = null;
var glb_nCmbsTimerInterval = null;
var glb_nLancamentoID = 0;
var glb_nLancamentoIDEmpresaLog = 0;

var glb_nNextEmpresaIndexInList = 0;
var glb_nCurrEmpresaIDInList = 0;

var selHistoricoPadraoID_changByUser = false;
var bCapaLancamentoEstaEmpresa = true;

var glb_nContaCompensacaoID = 0;
var glb_sContaCompensacao = '';
var dsoCmbStatic01 = new CDatatransport('dsoCmbStatic01');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoGrid = new CDatatransport('dsoGrid');
var dsoCmb01Grid_01 = new CDatatransport('dsoCmb01Grid_01');
var dsoCmb02Grid_01 = new CDatatransport('dsoCmb02Grid_01');
var dsoCmb03Grid_01 = new CDatatransport('dsoCmb03Grid_01');
var dsoPesq = new CDatatransport('dsoPesq');
var dsoClonar = new CDatatransport('dsoClonar');
var dsoGerarLanc = new CDatatransport('dsoGerarLanc');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';

/********************************************************************
INDICE DAS FUNCOES:

window_onload()
setupPage()
txtArgumento_onkeydown()
txtArgumento_onkeyup()
btn_onclick(ctl)
geraLancamentos()
geraLancamentosInGrid()
geraLancamentosInGrid_DSC()
insertDetalheInGrid()
adjustWidthCellsInGrid()
clearDetalheInGrid()
startPesqDetalhes()
startPesqDetalhes_DSC()
cmbOnChange(cmb)
enableBtnFillGrid()
startStaticCmbs()
dsoCmbStatic_DSC()
startDynamicCmbs()
dsoCmbDynamic_DSC()
fillDSOGrid()
unlockModalWin()
fillCellsInGrid()
filtAndFillGrid()
showEmpresasCtrls(bShow)
showDetalhesCtrls(bShow)
js_gerarLancamentos_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
clearListDetalhe()
js_gerarLancamentos_AfterEdit(Row, Col)
js_gerarLancamentos_ChangeEdit()
js_gerarLancamentos_ValidateEdit()
validateConta()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    lockBtnLupa(btnListarDetalhes, true);
    btnPreencher.disabled = true;
    btnExcluir.disabled = true;
    btnLimpar.disabled = true;

    var elem;
    // FG - grid
    // coloca classe no grid
    elem = window.document.getElementById('fg');

    if (elem != null)
        elem.className = 'fldGeneral';

    startGridInterface(fg);

    window_onload_1stPart();

    showDetalhesCtrls(false);
    showEmpresasCtrls(false);

    // ajusta o body do html
    with (modalgerarlancamentosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    glb_FirstTime = true;

    startStaticCmbs();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Gerar Lan�amentos', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
    }

    var nBtnsWidth = 72;
    var midPos = 0;
    adjustElementsInForm([['lblEhEstorno', 'chkEhEstorno', 3, 1, -10, -10],
                          ['lblTipoLancamentoID', 'selTipoLancamentoID', 7, 1, -5],
						  ['lblUsoCotidiano', 'chkUsoCotidiano', 3, 1],
                          ['lblHistoricoPadraoID', 'selHistoricoPadraoID', 25, 1, -5],
                          ['lblHistoricoComplementar', 'txtHistoricoComplementar', 24, 1],
                          ['lblValorTotalLancamento', 'txtValorTotalLancamento', 11, 1],
                          ['lblEhDefault', 'chkEhDefault', 3, 1],
                          ['btnFillGrid', 'btn', nBtnsWidth, 1, 12, -1]], null, null, true);

    adjustElementsInForm([['lblAtivo', 'chkAtivo', 3, 1, -10],
						  ['lblArgumento', 'txtArgumento', 20, 1, -5],
						  ['btnListarDetalhes', 'btn', 24, 1],
						  ['lblDetalhes', 'selDetalhes', 25, 1, -2],
						  ['btnPreencher', 'btn', nBtnsWidth, 1, -21],
						  ['btnLimpar', 'btn', nBtnsWidth, 1, 2],
						  ['btnExcluir', 'btn', nBtnsWidth, 1, 2],
						  ['btnLancamento', 'btn', nBtnsWidth, 1, 2]], null, null, true);

    chkUsoCotidiano.checked = true;

    btnPreencher.style.height = btnOK.offsetHeight;
    btnLimpar.style.height = btnOK.offsetHeight;
    btnExcluir.style.height = btnOK.offsetHeight;
    btnLancamento.style.height = btnOK.offsetHeight;

    chkAtivo.checked = true;

    btnFillGrid.style.left = btnFillGrid.offsetLeft - 10;
    btnFillGrid.style.height = parseInt(btnOK.currentStyle.height, 10);
    btnFillGrid.style.width = parseInt(btnOK.currentStyle.width, 10) - 28;

    chkEhDefault.checked = true;

    // ajusta o divPesquisa
    with (divPesquisa.style) {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = parseInt(btnFillGrid.currentStyle.top, 10) +
                 parseInt(btnFillGrid.currentStyle.height, 10) + ELEM_GAP;
    }

    selDetalhes.disabled = true;

    lblEmpresas.style.left = 2;
    lblEmpresas.style.top = (ELEM_GAP / 2);

    with (selEmpresas.style) {
        left = 2;
        top = lblEmpresas.offsetTop + 14;
        width = 173;
        height = 102;
    }

    lblDetalhes.style.left = parseInt(btnListarDetalhes.currentStyle.left, 10) +
			parseInt(btnListarDetalhes.currentStyle.width, 10) + ELEM_GAP;
    lblDetalhes.style.top = parseInt(lblEmpresas.currentStyle.top, 10);

    with (selDetalhes.style) {
        left = parseInt(lblDetalhes.currentStyle.left, 10);
        top = parseInt(selEmpresas.currentStyle.top, 10);
        width = 173;
        height = 102;
    }
    selDetalhes.ondblclick = selDetalhes_ondlbclick;

    txtHistoricoComplementar.onfocus = selFieldContent;
    txtHistoricoComplementar.maxLength = 30;

    txtValorTotalLancamento.setAttribute('thePrecision', 11, 2);
    txtValorTotalLancamento.setAttribute('theScale', 2, 1);
    txtValorTotalLancamento.onkeypress = verifyNumericEnterNotLinked;
    txtValorTotalLancamento.onkeyup = txtValorTotalLancamento_onkeyup;
    txtValorTotalLancamento.onfocus = selFieldContent;

    txtArgumento.maxLength = 20;
    txtArgumento.onkeydown = txtArgumento_onkeydown;
    txtArgumento.onkeyup = txtArgumento_onkeyup;
    txtArgumento.onfocus = selFieldContent;

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.style.top, 10) + parseInt(divPesquisa.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = 225;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // ajusta o divFields
    with (divFields.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divFG.style.top, 10) + parseInt(divFG.style.height, 10);
        width = modWidth - 2 * ELEM_GAP - 6;
        height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(top);
    }

    startGridInterface(fg);

    fg.FontSize = '8';

    headerGrid(fg, ['EmpresaID',
				   'Empresa',
                   'ContaID',
                   'Valor',
                   'Documento',
                   'PessoaID',
                   'Pessoa',
				   'Bco/Ag/Cta',
				   'Imposto',
                   'Conta',
                   'Det'], [0, 5]);

    fg.Redraw = 2;

    btnFillGrid.disabled = true;
    disableEnable_btnLancamento(true);
    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';
}

function txtValorTotalLancamento_onkeyup() {
    enableBtnFillGrid();

    if ((event.keyCode == 13) && (btnFillGrid.disabled == false))
        btn_onclick(btnFillGrid);
}

function txtArgumento_onkeydown() {
    ;
}

function txtArgumento_onkeyup() {
    if (event.keyCode == 13) {
        btn_onclick(btnListarDetalhes);
    }
}

function btnLupaClicked() {
    btn_onclick(btnListarDetalhes);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    if (ctl.id == btnOK.id) {
        ;
    }
    else if (ctl.id == btnLancamento.id) {
        // 1. O usuario clicou o botao OK
        geraLancamentos();
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        // esta funcao trava o html contido na janela modal
        lockControlsInModalWin(true);

        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
    else if ((ctl.id).toUpperCase() == 'BTNFILLGRID')
        filtAndFillGrid();
    else if ((ctl.id).toUpperCase() == 'BTNRATEAR')
        ratear();
    else if ((ctl.id).toUpperCase() == 'BTNLISTARDETALHES')
        startPesqDetalhes();
    else if ((ctl.id).toUpperCase() == 'BTNPREENCHER')
        insertDetalheInGrid();
    else if ((ctl.id).toUpperCase() == 'BTNLIMPAR')
        clearDetalheInGrid();
    else if ((ctl.id).toUpperCase() == 'BTNEXCLUIR')
        removeLineInGrid();
}

function geraLancamentos() {
    var i, i_init;
    var sHistoricoComplementar;
    var sHistoricoComplementarDefault;
    var nTipoDetalhamentoID, nDetalhamentoObrigatorio, nPessoaID, nRelPesContaID, nImpostoID;
    var nEmpresaID = 0;
    var sEmpresa = '';
    var nEmpresaAnteriorID = 0;
    var nValor = 0;
    var sErro = '';

    // consulta a variavel da automacao
    glb_totalCols__ ? i_init = 2 : i_init = 1;

    if (fg.Rows < i_init) {
        if (window.top.overflyGen.Alert('N�o existem contas selecionadas.\nPreencha o grid.') == 0)
            return null;

        return null;
    }

    if (!isNaN(fg.ValueMatrix((i_init - 1), getColIndexByColKey(fg, 'Valor')))) {
        if (roundNumber(fg.ValueMatrix((i_init - 1), getColIndexByColKey(fg, 'Valor')), 2) != 0) {
            sErro = 'Lan�amento com diferen�a.' + '\n';
        }
    }

    fg.Editable = false;

    for (i = i_init; i < fg.Rows; i++) {
        fg.TextMatrix(i, getColIndexByColKey(fg, 'Documento')) =
			trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'Documento')));
    }

    fg.Editable = true;

    for (i = i_init; i < fg.Rows; i++) {
        if ((fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')) == 0) &&
		      (fg.ValueMatrix(i, getColIndexByColKey(fg, 'EhObrigatoria')) != 0)) {
            sErro += 'S� � permitido lan�amentos com valor.' + '\n';
        }

        nTipoDetalhamentoID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoDetalhamentoID'));
        nDetalhamentoObrigatorio = (fg.ValueMatrix(i, getColIndexByColKey(fg, 'DetalhamentoObrigatorio')) != 0 ? 1 : 0);
        nEmpresaID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'EmpresaID'));

        if (nEmpresaID != nEmpresaAnteriorID) {
            if (nValor != 0) {
                sEmpresa = fg.TextMatrix(i, getColIndexByColKey(fg, 'Empresa*'));

                sErro += 'Lan�amento da empresa ' + sEmpresa + '.' + '\n';
            }
            else {
                nEmpresaAnteriorID = nEmpresaID;
                nValor = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor'));
            }
        }
        else {
            nValor += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor'));
        }

        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')) != 0) {
            if (nDetalhamentoObrigatorio == 1) {
                // Pessoa
                if ((nTipoDetalhamentoID >= 1532) && (nTipoDetalhamentoID <= 1541)) {
                    nPessoaID = fg.TextMatrix(i, getColIndexByColKey(fg, 'PessoaID*'));

                    if ((nPessoaID == null) || (nPessoaID == '')) {
                        sErro += 'Informar a pessoa para a conta: ' + fg.TextMatrix(i, getColIndexByColKey(fg, 'Conta*')) + '.' + '\n';
                    }
                }
                    // Conta
                else if (nTipoDetalhamentoID == 1542) {
                    nRelPesContaID = fg.TextMatrix(i, getColIndexByColKey(fg, 'RelPesContaID'));

                    if ((nRelPesContaID == null) || (nRelPesContaID == '')) {
                        sErro += 'Informar a conta banc�ria para a conta: ' + fg.TextMatrix(i, getColIndexByColKey(fg, 'Conta*')) + '.' + '\n';
                    }
                }
                    // Imposto
                else if (nTipoDetalhamentoID == 1543) {
                    nImpostoID = fg.TextMatrix(i, getColIndexByColKey(fg, 'ImpostoID'));

                    if ((nImpostoID == null) || (nImpostoID == '')) {
                        sErro += 'Informar o imposto para a conta: ' + fg.TextMatrix(i, getColIndexByColKey(fg, 'Conta*')) + '.' + '\n';
                    }
                }
            }
        }
    }

    txtHistoricoComplementar.value = trimStr(txtHistoricoComplementar.value);

    sHistoricoComplementar = txtHistoricoComplementar.value;
    sHistoricoComplementarDefault = selHistoricoPadraoID.options.item(selHistoricoPadraoID.selectedIndex).getAttribute('HistoricoComplementar', 1);

    if ((sHistoricoComplementarDefault != null) && (sHistoricoComplementarDefault != '')) {
        if ((sHistoricoComplementar == null) ||
			 (sHistoricoComplementar == '') ||
		     (sHistoricoComplementar == sHistoricoComplementarDefault)) {
            sErro += 'Corrigir Hist�rico Complementar.' + '\n';
        }
    }

    if (sErro != '') {
        if (window.top.overflyGen.Alert(sErro) == 0)
            return null;

        window.focus();
        return null;
    }

    var _retConf = window.top.overflyGen.Confirm('Gerar Lan�amento?');

    if (_retConf == 1) {
        glb_nCurrLineInSave = 0;

        glb_nLancamentoID = 0;

        glb_nNextEmpresaIndexInList = 0;

        geraLancamentosInGrid();
    }
    else {
        return null;
    }
}

function geraLancamentosInGrid() {
    if (glb_nSaveTimerInterval != null) {
        window.clearInterval(glb_nSaveTimerInterval);
        glb_nSaveTimerInterval = null;
    }

    var i, j;

    var strPars = new String();
    var strParsLen = 0;
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');

    // If abaixo so ocorre para:
    // a primeira empresa e a primeira linha do grid
    if (glb_nNextEmpresaIndexInList <= (selEmpresas.options.length - 1)) {
        if (glb_nCurrLineInSave == 0) {
            glb_nCurrEmpresaIDInList = glb_nEmpresaID;
        }
    }

    if (glb_nCurrLineInSave >= fg.Rows - 1) {
        // If abaixo so ocorre para:
        // qualquer empresa e ja leu a ultima linha do grid
        if (glb_nNextEmpresaIndexInList <= (selEmpresas.options.length - 1)) {
            glb_nCurrLineInSave = 0;

            glb_nCurrEmpresaIDInList = selEmpresas.options(glb_nNextEmpresaIndexInList).value;
            glb_nNextEmpresaIndexInList++;
            bCapaLancamentoEstaEmpresa = true;
            glb_nLancamentoID = 0;
        }
        else {
            lockControlsInModalWin(false);

            fg.Rows = 1;

            // ajusta a variavel da automacao para grid com linha de total
            glb_totalCols__ = false;

            var _retConf = window.top.overflyGen.Confirm('Deseja detalhar o lan�amento ' + glb_nLancamentoIDEmpresaLog + '\ngerado para a Empresa logada?', 1);

            if (_retConf == 1)
                sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, glb_nLancamentoIDEmpresaLog);

            clearComboEx(['selDetalhes']);
            showDetalhesCtrls(false);
            showEmpresasCtrls(false);

            txtValorTotalLancamento.value = '';
            txtHistoricoComplementar.value = selHistoricoPadraoID.options.item(selHistoricoPadraoID.selectedIndex).getAttribute('HistoricoComplementar', 1);

            disableEnable_btnLancamento(true);
            return null;
        }
    }

    lockControlsInModalWin(true);

    var nMoedaID = glb_nMoedaID;

    for (i = (glb_nCurrLineInSave + 1) ; i < fg.Rows; i++) {
        glb_nCurrLineInSave = i;

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'EmpresaID')) != glb_nCurrEmpresaIDInList)
            continue;

        if ((fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')) == null) ||
		     (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')) == 0))
            continue;

        // Parametros que so vao uma vez
        if (bCapaLancamentoEstaEmpresa) {
            strPars += (strPars == '' ? '?' : '&');
            strPars += 'nUsuarioID=' + escape(nUserID);
            strPars += '&bEhEstorno=' + escape((chkEhEstorno.checked ? 1 : 0));
            strPars += '&nTipoLancamentoID=' + escape(selTipoLancamentoID.options(selTipoLancamentoID.selectedIndex).value);
            strPars += '&nHistoricoPadraoID=' + escape(selHistoricoPadraoID.options(selHistoricoPadraoID.selectedIndex).value);
            strPars += '&sHistoricoComplementar=' + escape(txtHistoricoComplementar.value);
        }
        else {
            strPars += (strPars == '' ? '?' : '&');
            strPars += 'nUsuarioID=' + escape(nUserID);
        }

        strPars += '&nEmpresaID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'EmpresaID')));
        strPars += '&nContaID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ContaID')));
        strPars += '&nRelPesContaID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'RelPesContaID')));
        strPars += '&nImpostoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ImpostoID')));
        strPars += '&sDocumento=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'Documento')));

        if (!isNaN(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PessoaID*'))))
            strPars += '&nPessoaID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PessoaID*')));
        else
            strPars += '&nPessoaID=' + escape(0);

        strPars += '&nMoedaID=' + escape(nMoedaID);
        strPars += '&nValor=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')));
        strPars += '&nLancamentoID=' + escape(glb_nLancamentoID);
        strPars += '&nLanContaEmpresaID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'LanContaEmpresaID')));

        strParsLen = strPars.length;

        if (bCapaLancamentoEstaEmpresa) {
            bCapaLancamentoEstaEmpresa = false;
            break;
        }
        else if (strParsLen >= glb_nMaxStringSize)
            break;
    }

    if (strParsLen > 0) {
        dsoGerarLanc.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/lancamentos/serverside/geralancamentos.aspx' + strPars;
        dsoGerarLanc.ondatasetcomplete = geraLancamentosInGrid_DSC;
        dsoGerarLanc.refresh();
    }
    else
        geraLancamentosInGrid_DSC();

}

function geraLancamentosInGrid_DSC() {
    if (!(dsoGerarLanc.recordset.BOF && dsoGerarLanc.recordset.EOF)) {
        if ((dsoGerarLanc.recordset['Resultado'].value != null) && (dsoGerarLanc.recordset['Resultado'].value != "")) {
            lockControlsInModalWin(false);

            fg.Rows = 1;

            // ajusta a variavel da automacao para grid com linha de total
            glb_totalCols__ = false;

            clearComboEx(['selDetalhes']);
            showDetalhesCtrls(false);
            showEmpresasCtrls(false);

            disableEnable_btnLancamento(true);

            if (window.top.overflyGen.Alert(dsoGerarLanc.recordset['Resultado'].value) == 0)
                return null;

            return null;
        }

        glb_nLancamentoID = dsoGerarLanc.recordset['LancamentoID'].value;

        if (glb_nCurrEmpresaIDInList == glb_nEmpresaID)
            glb_nLancamentoIDEmpresaLog = dsoGerarLanc.recordset['LancamentoID'].value;
    }

    glb_nSaveTimerInterval = window.setInterval('geraLancamentosInGrid()', 10, 'JavaScript');
}

function insertDetalheInGrid() {
    var nTipoDetalhamentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoDetalhamentoID'));
    var i, j;

    var i_init;
    var sPessoa = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Pessoa*'));
    var sContaBancaria = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RelPesContaID'));
    var sImposto = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ImpostoID'));

    // consulta a variavel da automacao
    glb_totalCols__ ? i_init = 2 : i_init = 1;

    if ((fg.Row >= i_init) && (selDetalhes.selectedIndex >= 0)) {
        fg.Redraw = 0;
        fg.Editable = false;
        var lineData = '';

        var optionSelectedIndex = -1;

        var rowPosition = fg.Row;
        var oldRowPosition = fg.Row;

        for (i = 0; i < selDetalhes.options.length; i++) {
            if (selDetalhes.options(i).selected) {
                optionSelectedIndex = i;
                break;
            }
        }

        for (i = 0; i < selDetalhes.options.length; i++) {
            if (selDetalhes.options(i).selected) {
                lineData = '';

                if ((i > optionSelectedIndex) ||
				     (sPessoa != '') || (sContaBancaria != '') || (sImposto != '')) {
                    for (j = 0; j < fg.Cols; j++) {
                        if (j != getColIndexByColKey(fg, 'Excluir')) {
                            if (lineData != '')
                                lineData = lineData + '\t' + fg.TextMatrix(rowPosition, j);
                            else
                                lineData = fg.TextMatrix(rowPosition, j);
                        }
                        else
                            lineData = lineData + '\t' + 1;
                    }

                    rowPosition++;

                    addAndFillineInGrid(fg, lineData, rowPosition);
                    reSomaValores();
                }

                // Pessoa
                if ((nTipoDetalhamentoID >= 1532) && (nTipoDetalhamentoID <= 1541)) {
                    fg.TextMatrix(rowPosition, getColIndexByColKey(fg, 'PessoaID*')) = selDetalhes.options(i).value;
                    fg.TextMatrix(rowPosition, getColIndexByColKey(fg, 'Pessoa*')) = selDetalhes.options(i).innerText;
                    btnLimpar.disabled = (fg.TextMatrix(rowPosition, getColIndexByColKey(fg, 'PessoaID*')) == '');
                }
                    // Conta
                else if (nTipoDetalhamentoID == 1542) {
                    fg.TextMatrix(rowPosition, getColIndexByColKey(fg, 'RelPesContaID')) = selDetalhes.options(i).value;
                    fg.TextMatrix(rowPosition, getColIndexByColKey(fg, 'LanContaEmpresaID')) = selDetalhes.options(i).getAttribute('EmpresaID', 1);
                    btnLimpar.disabled = (fg.TextMatrix(rowPosition, getColIndexByColKey(fg, 'RelPesContaID')) == '');
                }
                    // Imposto
                else if (nTipoDetalhamentoID == 1543) {
                    fg.TextMatrix(rowPosition, getColIndexByColKey(fg, 'ImpostoID')) = selDetalhes.options(i).value;
                    btnLimpar.disabled = (fg.TextMatrix(rowPosition, getColIndexByColKey(fg, 'ImpostoID')) == '');
                }
            }
        }

        paintDetailCells();
        fg.Row = oldRowPosition;

        fg.Editable = true;

        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
        adjustWidthCellsInGrid();
        fg.Redraw = 2;
    }
}

function adjustWidthCellsInGrid() {
    var nCurrRow = 0;
    var sLabel4, sLabel5, sLabel8, sLabel10, sLabel11;

    sLabel4 = fg.TextMatrix(0, getColIndexByColKey(fg, 'Valor'));
    sLabel5 = fg.TextMatrix(0, getColIndexByColKey(fg, 'Documento'));
    sLabel8 = fg.TextMatrix(0, getColIndexByColKey(fg, 'RelPesContaID'));
    sLabel10 = fg.TextMatrix(0, getColIndexByColKey(fg, 'Conta*'));
    sLabel11 = fg.TextMatrix(0, getColIndexByColKey(fg, 'Det*'));

    if (fg.Row > 0)
        nCurrRow = fg.Row;

    fg.Redraw = 0;

    fg.TextMatrix(0, getColIndexByColKey(fg, 'Valor')) = '99999999999';
    fg.TextMatrix(0, getColIndexByColKey(fg, 'Documento')) = '999999999999999';
    fg.TextMatrix(0, getColIndexByColKey(fg, 'RelPesContaID')) = '9999999999999999';
    fg.TextMatrix(0, getColIndexByColKey(fg, 'Conta*')) = '9999999999';
    fg.TextMatrix(0, getColIndexByColKey(fg, 'Det*')) = '999999';

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.TextMatrix(0, getColIndexByColKey(fg, 'Valor')) = sLabel4;
    fg.TextMatrix(0, getColIndexByColKey(fg, 'Documento')) = sLabel5;
    fg.TextMatrix(0, getColIndexByColKey(fg, 'RelPesContaID')) = sLabel8;
    fg.TextMatrix(0, getColIndexByColKey(fg, 'Conta*')) = sLabel10;
    fg.TextMatrix(0, getColIndexByColKey(fg, 'Det*')) = sLabel11;

    fg.Redraw = 2;
}

function clearDetalheInGrid() {
    var nTipoDetalhamentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoDetalhamentoID'));

    var i, i_init;

    // consulta a variavel da automacao
    glb_totalCols__ ? i_init = 2 : i_init = 1;

    if (fg.Row >= i_init) {
        fg.Redraw = 0;

        fg.Editable = false;

        // Pessoa
        if ((nTipoDetalhamentoID >= 1532) && (nTipoDetalhamentoID <= 1541)) {
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*')) = '';
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Pessoa*')) = '';
            btnLimpar.disabled = (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*')) == '');
        }
            // Conta
        else if (nTipoDetalhamentoID == 1542) {
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RelPesContaID')) = '';
            btnLimpar.disabled = (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RelPesContaID')) == '');
        }
            // Imposto
        else if (nTipoDetalhamentoID == 1543) {
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ImpostoID')) = '';
            btnLimpar.disabled = (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ImpostoID')) == '');
        }

        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);

        paintDetailCells();

        adjustWidthCellsInGrid();
        fg.Redraw = 2;

        fg.Editable = true;
    }
}

function startPesqDetalhes() {
    var nTipoDetalhamentoID = 0;

    clearComboEx(['selDetalhes']);

    if (fg.Row <= 0)
        return null;

    lockControlsInModalWin(true);

    nTipoDetalhamentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoDetalhamentoID'));

    if ((nTipoDetalhamentoID >= 1532) && (nTipoDetalhamentoID <= 1541)) {
        var strPas = '?strToFind=' + escape(txtArgumento.value);
        strPas += '&nContaID=' + escape(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ContaID')));
        strPas += '&nEmpresaID=' + escape(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'EmpresaID')));
        strPas += '&bAtivos=' + escape(chkAtivo.checked ? 1 : 0);

        dsoPesq.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/lancamentos/serverside/pesqpessoa.aspx' + strPas;
        dsoPesq.ondatasetcomplete = startPesqDetalhes_DSC;
        dsoPesq.Refresh();
    }
    else
        startPesqDetalhes_DSC();
}

function startPesqDetalhes_DSC() {
    var nTipoDetalhamentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoDetalhamentoID'));

    var oldDataSrc = selDetalhes.dataSrc;
    var oldDataFld = selDetalhes.dataFld;
    selDetalhes.dataSrc = '';
    selDetalhes.dataFld = '';
    var lFirstRecord = true;

    var dso = null;
    var sInnerText = '';
    var sValue = '';

    if ((nTipoDetalhamentoID >= 1532) && (nTipoDetalhamentoID <= 1541)) {
        dso = dsoPesq;
        sInnerText = 'Fantasia';
        sValue = 'fldID';
    }
    else if (nTipoDetalhamentoID == 1542) {
        dso = dsoCmb02Grid_01;

        if (!(dso.recordset.BOF && dso.recordset.EOF))
            dso.recordset.MoveFirst();

        sInnerText = 'ContaNome';
        sValue = 'RelPesContaID';
    }
    else if (nTipoDetalhamentoID == 1543) {
        dso = dsoCmb03Grid_01;

        if (!(dso.recordset.BOF && dso.recordset.EOF))
            dso.recordset.MoveFirst();

        sInnerText = 'ImpostoAbrev';
        sValue = 'ImpostoID';
    }

    while (!dso.recordset.EOF) {
        if ((dso.recordset[sValue].value == null) || (dso.recordset[sValue].value == 0)) {
            dso.recordset.MoveNext();
            continue;
        }

        var optionStr = dso.recordset[sInnerText].value;

        if (nTipoDetalhamentoID == 1542) {
            optionStr = optionStr + '-' + dso.recordset['Empresa'].value;
        }

        var optionValue = dso.recordset[sValue].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;

        if (nTipoDetalhamentoID == 1542) {
            oOption.setAttribute('EmpresaID', dso.recordset['EmpresaID'].value, 1);
        }

        selDetalhes.add(oOption);
        dso.recordset.MoveNext();
        lFirstRecord = false;
    }

    selDetalhes.dataSrc = oldDataSrc;
    selDetalhes.dataFld = oldDataFld;

    lockControlsInModalWin(false);

    selDetalhes.disabled = (selDetalhes.options.length == 0);
    btnPreencher.disabled = (selDetalhes.selectedIndex == -1);

    if (!selDetalhes.disabled) {
        window.focus();
        selDetalhes.focus();
    }
}

/********************************************************************
Onchange dos combos
********************************************************************/
function cmbOnChange(cmb) {
    var sObservacoes = '';

    if ((cmb.id).toUpperCase() == 'SELTIPOLANCAMENTOID') {
        startDynamicCmbs();
    }
    else if ((cmb.id).toUpperCase() == 'SELHISTORICOPADRAOID') {
        selHistoricoPadraoID_changByUser = true;

        sObservacoes = selHistoricoPadraoID.options.item(selHistoricoPadraoID.selectedIndex).getAttribute('Observacoes', 1);

        lblHistoricoPadraoID.title = sObservacoes;
        selHistoricoPadraoID.title = sObservacoes;

        txtHistoricoComplementar.value = selHistoricoPadraoID.options.item(selHistoricoPadraoID.selectedIndex).getAttribute('HistoricoComplementar', 1);

        fg.Rows = 1;

        // ajusta a variavel da automacao para grid com linha de total
        glb_totalCols__ = false;

        clearComboEx(['selDetalhes']);

        showDetalhesCtrls(false);
        showEmpresasCtrls(false);

        adjustLabelsCombos();

        enableBtnFillGrid();
        if (selHistoricoPadraoID.value != 0)
            fillDSOGrid();
    }
    else if ((cmb.id).toUpperCase() == 'SELEMPRESAS') {
        showEmpresasCtrls(true);
    }
    else if ((cmb.id).toUpperCase() == 'SELDETALHES') {
        showDetalhesCtrls(true);
    }
}

/********************************************************************
Onclick dos checkbox
********************************************************************/
function chkOnClick(chk) {
    if ((chk.id).toUpperCase() == 'CHKEHESTORNO') {
        startDynamicCmbs();
    }
    else if ((chk.id).toUpperCase() == 'CHKUSOCOTIDIANO') {
        startDynamicCmbs();
    }
}

function enableBtnFillGrid() {
    if (glb_nLancamentoID > 0)
        btnFillGrid.disabled = true;
    else {
        if ((selTipoLancamentoID.selectedIndex >= 0) &&
			(selHistoricoPadraoID.selectedIndex > 0) && (trimStr(txtValorTotalLancamento.value) != '') &&
			(parseFloat(trimStr(txtValorTotalLancamento.value)) > 0))
            btnFillGrid.disabled = false;
        else
            btnFillGrid.disabled = true;
    }
}

/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos combos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startStaticCmbs() {
    // parametrizacao do dso dsoCmbStatic01 (designado para selTipoLancamentoID)
    setConnection(dsoCmbStatic01);

    glb_CounterCmbsStatics = 1;

    dsoCmbStatic01.SQL = 'SELECT ItemID AS fldID, ItemAbreviado AS fldName, Aplicar ' +
			 'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
			 'WHERE (EstadoID=2 AND TipoID=907) ' +
			 'ORDER BY Ordem';

    dsoCmbStatic01.ondatasetcomplete = dsoCmbStatic01_DSC;
    dsoCmbStatic01.Refresh();
}

/********************************************************************
Funcao disparada pelo programado.
Preenchimento dos combos
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbStatic01_DSC() {
    var optionStr, optionValue;
    var aCmbsStatics = [selTipoLancamentoID];
    var aDSOsStatics = [dsoCmbStatic01];
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var i;
    var lFirstRecord;
    var nQtdCmbs = 1;

    // Inicia o carregamento de combos staticos (selTipoLancamentoID)
    clearComboEx(['selTipoLancamentoID']);

    glb_CounterCmbsStatics--;

    if (glb_CounterCmbsStatics == 0) {
        for (i = 0; i < nQtdCmbs; i++) {
            oldDataSrc = aCmbsStatics[i].dataSrc;
            oldDataFld = aCmbsStatics[i].dataFld;
            aCmbsStatics[i].dataSrc = '';
            aCmbsStatics[i].dataFld = '';
            lFirstRecord = true;
            while (!aDSOsStatics[i].recordset.EOF) {
                optionStr = aDSOsStatics[i].recordset['fldName'].value;
                optionValue = aDSOsStatics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                oOption.setAttribute('Aplicar', aDSOsStatics[i].recordset['Aplicar'].value, 1);
                aCmbsStatics[i].add(oOption);
                aDSOsStatics[i].recordset.MoveNext();
                lFirstRecord = false;
            }
            aCmbsStatics[i].dataSrc = oldDataSrc;
            aCmbsStatics[i].dataFld = oldDataFld;
        }
    }

    if (glb_nLancamentoID > 0) {
        chkEhEstorno.disabled = true;
        selTipoLancamentoID.disabled = true;
        chkUsoCotidiano.disabled = true;
        selHistoricoPadraoID.disabled = true;
        txtValorTotalLancamento.disabled = true;
        chkEhDefault.disabled = true;
        clonarLancamento();
    }
    else
        startDynamicCmbs();
}

/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos combos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs() {
    if (glb_nCmbsTimerInterval != null) {
        window.clearInterval(glb_nCmbsTimerInterval);
        glb_nCmbsTimerInterval = null;
    }

    showEmpresasCtrls(false);
    showDetalhesCtrls(false);

    // controla o retorno do servidor dos dados de combos dinamicos
    // (selHistoricoPadraoID)
    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var nTipoLancamentoID = selTipoLancamentoID.value;
    var sFiltro = '';

    if (chkUsoCotidiano.checked)
        sFiltro = ' AND a.UsoCotidiano = 1 ';

    if (isNaN(parseInt(nTipoLancamentoID, 10)))
        nTipoLancamentoID = 0;

    glb_CounterCmbsDynamics = 1;

    // parametrizacao do dso dsoCmbDynamic01 (designado para selHistoricoPadraoID)
    setConnection(dsoCmbDynamic01);

    lockControlsInModalWin(true);

    dsoCmbDynamic01.SQL = 'SELECT 0 AS Indice, 0 as fldID, SPACE(0) as fldName, SPACE(0) as HistoricoComplementar, ' +
						  'SPACE(0) AS HistoricoPadrao, 1 AS MultiplasEmpresas, SPACE(0) AS Observacoes ' +
						  'UNION ALL ' +
						  'SELECT 1 AS Indice, a.HistoricoPadraoID as fldID, a.HistoricoPadrao as fldName, ' +
	                      'ISNULL(a.HistoricoComplementar, SPACE(0)) AS HistoricoComplementar, a.HistoricoPadrao, a.MultiplasEmpresas, ' +
	                      'ISNULL(CONVERT(VARCHAR(8000), a.Observacoes), SPACE(0)) AS Observacoes ' +
	                      'FROM HistoricosPadrao a WITH(NOLOCK) ' +
	                      'WHERE (a.EstadoID = 2 AND a.UsoSistema = 0 ' + sFiltro + ' AND ' +
							'a.HistoricoPadraoID IN ' +
							'(SELECT HistoricoPadraoID FROM HistoricosPadrao_Contas WITH(NOLOCK) ' +
							'WHERE (HistoricoPadraoID = a.HistoricoPadraoID AND TipoLancamentoID = ' + nTipoLancamentoID + '))) ' +
						  'ORDER BY Indice, HistoricoPadrao';

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();
}

/********************************************************************
Funcao disparada pelo programado.
Preenchimento dos combos
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    var optionStr, optionValue;
    var aCmbsDynamics = [selHistoricoPadraoID];
    var aDSOsDynamics = [dsoCmbDynamic01];
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var i;
    var lFirstRecord;
    var nQtdCmbs = 1;

    // Inicia o carregamento de combos dinamicos (selHistoricoPadraoID)
    clearComboEx(['selHistoricoPadraoID']);

    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics == 0) {
        for (i = 0; i < nQtdCmbs; i++) {
            oldDataSrc = aCmbsDynamics[i].dataSrc;
            oldDataFld = aCmbsDynamics[i].dataFld;
            aCmbsDynamics[i].dataSrc = '';
            aCmbsDynamics[i].dataFld = '';
            lFirstRecord = true;
            aDSOsDynamics[i].recordset.MoveFirst();
            while (!aDSOsDynamics[i].recordset.EOF) {
                optionStr = aDSOsDynamics[i].recordset['fldName'].value;
                optionValue = aDSOsDynamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                oOption.setAttribute('HistoricoComplementar', aDSOsDynamics[i].recordset['HistoricoComplementar'].value, 1);
                oOption.setAttribute('MultiplasEmpresas', aDSOsDynamics[i].recordset['MultiplasEmpresas'].value, 1);
                oOption.setAttribute('Observacoes', aDSOsDynamics[i].recordset['Observacoes'].value, 1);
                aCmbsDynamics[i].add(oOption);
                aDSOsDynamics[i].recordset.MoveNext();
                lFirstRecord = false;
            }
            aCmbsDynamics[i].dataSrc = oldDataSrc;
            aCmbsDynamics[i].dataFld = oldDataFld;
        }

        //sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'adjustLabelsCombos(' + '\'' + 'selHistoricoPadraoID' + '\'' + ', true)');

        lockControlsInModalWin(false);

        if ((glb_nLancamentoID == 0) && (selHistoricoPadraoID.options.length > 0)) {
            selHistoricoPadraoID.disabled = false;

            if ((!glb_FirstTime) && (!selHistoricoPadraoID.disabled)) {
                window.focus();
                selHistoricoPadraoID.focus();
            }
        }

        if (glb_FirstTime) {
            // mostra a janela modal com o arquivo carregado
            showExtFrame(window, true);

            if (!selHistoricoPadraoID.disabled) {
                window.focus();
                selHistoricoPadraoID.focus();
            }
        }
        else {
            fg.Rows = 1;

            // ajusta a variavel da automacao para grid com linha de total
            glb_totalCols__ = false;
        }

        adjustLabelsCombos();
    }

    if (glb_nLancamentoID > 0)
        fillgridClonar();

    enableBtnFillGrid();

    return null;
}

function fillDSOGrid() {
    if (glb_nCmbsTimerInterval != null) {
        window.clearInterval(glb_nCmbsTimerInterval);
        glb_nCmbsTimerInterval = null;
    }

    glb_nQtdCmbsLoaded = 4;
    var aEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var sEmpresaFantasia = aEmpresaID[3];
    var nPaisID = aEmpresaID[1];
    var nEmpresaID = aEmpresaID[0];
    var sEhEstorno = '';

    if (chkEhEstorno.checked) {
        sEhEstorno = '~';
    }

    lockControlsInModalWin(true);

    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT ' + glb_nEmpresaID + ' AS EmpresaID, ' + '\'' + sEmpresaFantasia + '\'' + ' AS Empresa, ' +
					'a.HistoricoPadraoID, b.TipoLancamentoID, ' + sEhEstorno + 'b.EhDebito AS EhDebito, b.ContaID, c.Conta, ' +
					'b.EhDefault, c.TipoDetalhamentoID, c.DetalhamentoObrigatorio, d.ItemAbreviado AS Det, ' +
					'b.EhObrigatoria, NULL AS RelPesContaID, NULL AS ImpostoID, ' +
					'ISNULL(c.UsoEspecialID, 0) AS UsoEspecialID, dbo.fn_Conta_EhFilha(c.ContaID, (SELECT TOP 1 ContaID FROM PlanoContas WITH(NOLOCK) WHERE (EstadoID = 2 AND UsoEspecialID = 1511)), 1) AS EhFilhaDisp, 0 AS Excluir, ' +
				    'NULL AS PessoaID, NULL AS Pessoa, NULL AS Documento, NULL AS Valor, ' + glb_nEmpresaID + ' AS LanContaEmpresaID ' +
				  'FROM HistoricosPadrao a WITH(NOLOCK), HistoricosPadrao_Contas b WITH(NOLOCK), PlanoContas c WITH(NOLOCK), TiposAuxiliares_Itens d WITH(NOLOCK) ' +
				  'WHERE (a.EstadoID = 2 AND a.HistoricoPadraoID = b.HistoricoPadraoID AND ' +
					'b.ContaID=c.ContaID AND c.TipoDetalhamentoID=d.ItemID) ' +
				  'ORDER BY a.HistoricoPadraoID, b.TipoLancamentoID, EhDebito DESC, b.ContaID ';

    dsoGrid.ondatasetcomplete = fillDSOGrid_DSC;
    dsoGrid.Refresh();

    setConnection(dsoCmb01Grid_01);

    dsoCmb01Grid_01.SQL = 'SELECT b.Conta, b.ContaID AS ContaID, dbo.fn_Conta_Identada(b.ContaID, ' + getDicCurrLang() + ', NULL, NULL) AS ContaIdentada, ' +
			'dbo.fn_Conta_AceitaLancamento(b.ContaID) AS AceitaLancamento, ' +
			'c.ItemAbreviado AS Det, c.ItemMasculino AS TipoDetalhamento, c.ItemID AS TipoDetalhamentoID, ' +
	        'ISNULL(b.UsoEspecialID, 0) AS UsoEspecialID, dbo.fn_Conta_EhFilha(b.ContaID, (SELECT TOP 1 ContaID FROM PlanoContas WITH(NOLOCK) WHERE (EstadoID = 2 AND UsoEspecialID = 1511)), 1) AS EhFilhaDisp, ' + sEhEstorno + 'a.EhDebito AS EhDebito ' +
	        'FROM HistoricosPadrao_Contas a WITH(NOLOCK), PlanoContas b WITH(NOLOCK) ' +
	                'LEFT OUTER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (b.TipoDetalhamentoID = c.ItemID) ' +
	        'WHERE a.HistoricoPadraoID = ' + selHistoricoPadraoID.value + ' AND a.TipoLancamentoID = ' + selTipoLancamentoID.value + ' AND ' +
		    'dbo.fn_Conta_EhFilha(b.ContaID, a.ContaID, 1) = 1 AND b.EstadoID=2 ' +
			'UNION ALL ' +
			'SELECT b.Conta, b.ContaID, dbo.fn_Conta_Identada(b.ContaID, ' + getDicCurrLang() + ', NULL, NULL) AS ContaIdentada, ' +
			'dbo.fn_Conta_AceitaLancamento(b.ContaID) AS AceitaLancamento, ' +
			'c.ItemAbreviado AS Det, c.ItemMasculino AS TipoDetalhamento, c.ItemID AS TipoDetalhamentoID, ' +
	        'ISNULL(b.UsoEspecialID, 0) AS UsoEspecialID, dbo.fn_Conta_EhFilha(b.ContaID, (SELECT TOP 1 ContaID FROM PlanoContas WITH(NOLOCK) WHERE (EstadoID = 2 AND UsoEspecialID = 1511)), 1) AS EhFilhaDisp, ' + sEhEstorno + 'CONVERT(BIT, 1) AS EhDebito ' +
	        'FROM HistoricosPadrao a WITH(NOLOCK), PlanoContas b WITH(NOLOCK) ' +
	                'LEFT OUTER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (b.TipoDetalhamentoID = c.ItemID) ' +
	        'WHERE a.HistoricoPadraoID = ' + selHistoricoPadraoID.value + ' AND ' +
	        'a.MultiplasEmpresas = 1 AND b.EstadoID=2 AND ISNULL(b.UsoEspecialID, 0) = 1514 ' +
			'ORDER BY b.ContaID';

    dsoCmb01Grid_01.ondatasetcomplete = fillDSOGrid_DSC;
    dsoCmb01Grid_01.Refresh();

    setConnection(dsoCmb02Grid_01);

    /*
    'SELECT NULL AS RelPesContaID, SPACE(0) AS ContaNome, SPACE(0) AS ContaComplemento, CONVERT(INT, 0) AS EmpresaID, ' +
        'SPACE(0) AS Empresa ' +
          'UNION ALL ' +
          'SELECT b.RelPesContaID, dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) AS ContaNome, ' +
              'c.Fantasia AS ContaComplemento, e.PessoaID AS EmpresaID, e.Fantasia AS Empresa ' +
          'FROM RelacoesPessoas a, RelacoesPessoas_Contas b, Pessoas c, Bancos d, Empresa e, RelacoesPessoas f ' +
          'WHERE a.TipoRelacaoID = 24 AND a.EstadoID = 2 AND a.SujeitoID = f.SujeitoID ' + 
              'AND a.RelacaoID=b.RelacaoID AND b.EstadoID = 2 AND a.ObjetoID = c.PessoaID AND c.EstadoID = 2 ' +
              'AND c.BancoID = d.BancoID AND d.EstadoID = 2 ' +
              'AND e.PessoaID=a.SujeitoID ' +
              'AND f.EstadoID=2 AND f.TipoRelacaoID=30 AND f.ObjetoID=dbo.fn_Empresa_Matriz(' + nEmpresaID + ', 1, NULL) ' +
          'ORDER BY ContaNome';
    */

    dsoCmb02Grid_01.SQL = 'SELECT Contas.RelPesContaID, dbo.fn_ContaBancaria_Nome(Contas.RelPesContaID, 4) AS ContaNome, Agencia.Fantasia AS ContaComplemento, ' +
	        'Empresa.PessoaID AS EmpresaID, Empresa.Fantasia AS Empresa ' +
        'FROM RelacoesPessoas RelacaoContas WITH(NOLOCK) ' +
	        'INNER JOIN RelacoesPessoas_Contas Contas WITH(NOLOCK) ON RelacaoContas.RelacaoID=Contas.RelacaoID ' +
	        'INNER JOIN Pessoas Agencia WITH(NOLOCK) ON RelacaoContas.ObjetoID = Agencia.PessoaID ' +
	        'INNER JOIN Bancos WITH(NOLOCK) ON Agencia.BancoID = Bancos.BancoID ' +
	        'INNER JOIN Pessoas Empresa WITH(NOLOCK) ON Empresa.PessoaID=RelacaoContas.SujeitoID ' +
        'WHERE RelacaoContas.TipoRelacaoID = 24 AND RelacaoContas.EstadoID = 2 AND Contas.EstadoID = 2 AND Agencia.EstadoID = 2 AND Bancos.EstadoID = 2 AND ' +
	        'RelacaoContas.SujeitoID IN  ' +
		        '(SELECT dbo.fn_Empresa_Matriz(' + nEmpresaID + ', 1, NULL) ' +
		        'UNION ' +
		        'SELECT SujeitoID ' +
		        'FROM RelacoesPessoas WITH(NOLOCK) ' +
		        'WHERE EstadoID=2 AND TipoRelacaoID=30 AND ObjetoID=dbo.fn_Empresa_Matriz(' + nEmpresaID + ', 1, NULL)) ' +
        'ORDER BY ContaNome';

    dsoCmb02Grid_01.ondatasetcomplete = fillDSOGrid_DSC;
    dsoCmb02Grid_01.Refresh();

    setConnection(dsoCmb03Grid_01);

    dsoCmb03Grid_01.SQL = 'SELECT NULL AS ImpostoID, SPACE(0) AS ImpostoAbrev, SPACE(0) AS Imposto ' +
			  'UNION ALL SELECT ConceitoID AS ImpostoID, Imposto AS ImpostoAbrev, Conceito AS Imposto ' +
              'FROM Conceitos WITH(NOLOCK) ' +
              'WHERE EstadoID=2 AND TipoConceitoID=306 ' +
              'AND PaisID= ' + nPaisID + ' ' +
              'ORDER BY ImpostoAbrev';

    dsoCmb03Grid_01.ondatasetcomplete = fillDSOGrid_DSC;
    dsoCmb03Grid_01.Refresh();
}

function fillDSOGrid_DSC() {
    glb_nQtdCmbsLoaded--;

    if (glb_nQtdCmbsLoaded != 0)
        return null;

    glb_FirstTime = false;

    if (glb_nLancamentoID > 0)
        filtAndFillGrid();
    else {
        lockControlsInModalWin(false);

        if (selHistoricoPadraoID_changByUser) {
            selHistoricoPadraoID_changByUser = false;

            if (!selHistoricoPadraoID.disabled) {
                window.focus();
                selHistoricoPadraoID.focus();
            }
        }
    }
}

function fillCellsInGrid(dso) {
    var i, i_init;

    fg.Editable = false;
    startGridInterface(fg);

    fg.FrozenCols = 0;

    fg.FontSize = '8';

    headerGrid(fg, ['EmpresaID',
				   'Empresa',
                   'ContaID',
                   'Valor',
                   'Documento',
                   'Det',
                   'PessoaID',
                   'Pessoa',
				   'Bco/Ag/Cta',
				   'Imposto',
                   'Conta',
                   'TipoDetalhamentoID',
                   'DetalhamentoObrigatorio',
                   'EhDebito',
				   'UsoEspecialID',
				   'EhFilhaDisp',
                   'Excluir',
                   'EhObrigatoria',
                   'LanContaEmpresaID'], [0, 6, 11, 12, 13, 14, 15, 16, 17, 18]);

    fillGridMask(fg, dso, ['EmpresaID',
                             'Empresa*',
                             'ContaID',
							 'Valor',
                             'Documento',
                             'Det*',
                             'PessoaID*',
                             'Pessoa*',
                             'RelPesContaID',
                             'ImpostoID',
                             'Conta*',
							 'TipoDetalhamentoID',
							 'DetalhamentoObrigatorio',
							 'EhDebito',
							 'UsoEspecialID',
							 'EhFilhaDisp',
							 'Excluir',
							 'EhObrigatoria',
							 'LanContaEmpresaID'],
							 ['', '', '', '#99999999.99', '&&&&&&&&&&&&&&&', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
				             ['', '', '', '(###,###,##0.00)', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);

    alignColsInGrid(fg, [2, 3]);

    fg.FrozenCols = 4;

    // ajusta a variavel da automacao para grid com linha de total
    glb_totalCols__ = true;

    // forcar i_init para 2 sempre;
    i_init = 2;

    if (glb_nLancamentoID == 0) {
        if (trimStr(txtValorTotalLancamento.value) == '')
            txtValorTotalLancamento.value = 1;
    }
    /********************************************************************
        Prepara grid para linha de totalizacao
        Parametros:
        grid            referencia ao grid
        lineLabel       label da linha de titulos
        lineBkColor     cor de fundo da linha de totais (usar hexa invertido 0X000000)
                        ou null
        lineFColor      cor dos caracteres da linha de totais  (usar hexa invertido 0X000000)
                        ou null
        useBold         caracteres em bold (usar false ou true)
        aTotLine        Array de parametros para linha de totalizacao
                        1 elemento do array - indice da linha
                        2 em diante - mascara e operacao a usar na coluna    
                        As operacoes possiveis sao 'S' soma e 'M' media
    ********************************************************************/
    gridHasTotalLine(fg, 'Diferen�a', 0XC0C0C0, null, true, [[getColIndexByColKey(fg, 'Valor'), '###,###,##0.00', 'S']]);

    if (glb_nLancamentoID == 0) {
        for (i = i_init; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'EhDebito')) != 0)
                fg.TextMatrix(i, getColIndexByColKey(fg, 'Valor')) = (parseFloat(txtValorTotalLancamento.value) * -1);
            else
                fg.TextMatrix(i, getColIndexByColKey(fg, 'Valor')) = parseFloat(txtValorTotalLancamento.value);
        }
    }

    paintNegativeNumbers();

    paintDetailCells();

    insertcomboData(fg, 2, dsoCmb01Grid_01, 'ContaID|ContaIdentada|TipoDetalhamento', 'ContaID');
    insertcomboData(fg, 8, dsoCmb02Grid_01, 'ContaNome|Empresa|ContaComplemento', 'RelPesContaID');
    insertcomboData(fg, 9, dsoCmb03Grid_01, 'ImpostoAbrev|Imposto', 'ImpostoID');

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;

    if (fg.Rows > i_init) {
        fg.Row = i_init;
        fg.Col = getColIndexByColKey(fg, 'Documento');

        fg.TextMatrix(1, 1) = 'Diferen�a';
    }

    reSomaValores();

    adjustWidthCellsInGrid();

    fg.Editable = true;
}

function filtAndFillGrid() {
    showDetalhesCtrls(false);
    showEmpresasCtrls(false);
    selEmpresas.selectedIndex = -1;

    fg.Rows = 1;

    // ajusta a variavel da automacao para grid com linha de total
    glb_totalCols__ = false;

    var nTipoDetalhamentoID = 0;
    var i, i_init;

    if (!((selTipoLancamentoID.selectedIndex >= 0) && (selHistoricoPadraoID.selectedIndex >= 0)))
        return null;

    if (glb_nLancamentoID > 0) {
        if (dsoClonar.recordset.BOF && dsoClonar.recordset.EOF)
            return null;

        fillCellsInGrid(dsoClonar);
    }
    else {
        if (dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)
            return null;

        lockControlsInModalWin(true);

        dsoGrid.recordset.SetFilter('TipoLancamentoID = ' +
			selTipoLancamentoID.options(selTipoLancamentoID.selectedIndex).value + ' AND ' +
			'HistoricoPadraoID = ' +
			selHistoricoPadraoID.options(selHistoricoPadraoID.selectedIndex).value + ' ' +
			(chkEhDefault.checked ? ' AND EhDefault = 1 ' : ''));

        if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)) {
            dsoGrid.recordset.MoveFirst();
            fillCellsInGrid(dsoGrid);
        }

        dsoGrid.recordset.SetFilter('');
    }

    lockControlsInModalWin(false);

    // consulta a variavel da automacao
    glb_totalCols__ ? i_init = 2 : i_init = 1;

    if (fg.Row >= i_init) {
        window.focus();
        fg.focus();

        if (!(isNaN(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoDetalhamentoID')))))
            nTipoDetalhamentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoDetalhamentoID'));

        showDetalhesCtrls((nTipoDetalhamentoID >= 1532) && (nTipoDetalhamentoID <= 1543));
        showEmpresasCtrls((selHistoricoPadraoID.options.item(selHistoricoPadraoID.selectedIndex).getAttribute('MultiplasEmpresas', 1) == '1') &&
		                   (selTipoLancamentoID.options.item(selTipoLancamentoID.selectedIndex).getAttribute('Aplicar', 1) == '1'));

        disableEnable_btnLancamento(false);

        for (i = 1; i < fg.Rows; i++) {
            if (isNaN(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Valor'))) || fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Valor')) == 0) {
                disableEnable_btnLancamento(true);
                break;
            }
        }
    }
}

function showEmpresasCtrls(bShow) {
    var i, j, i_init;
    var nEmpresaID = 0;
    var nPreviousEmpresaID = 0;

    // lblEmpresas.style.visibility = (bShow ? 'inherit' : 'hidden');
    // selEmpresas.style.visibility = (bShow ? 'inherit' : 'hidden');
    lblEmpresas.style.visibility = 'hidden';
    selEmpresas.style.visibility = 'hidden';
    selEmpresas.disabled = (!bShow) || (selEmpresas.options.length == 0);

    //	btnRatear.style.visibility = (bShow ? 'inherit' : 'hidden');
    btnRatear.style.visibility = 'hidden';

    btnRatear.disabled = true;

    // consulta a variavel da automacao
    glb_totalCols__ ? i_init = 2 : i_init = 1;

    if (fg.Row >= i_init) {
        if (bShow) {
            if (selEmpresas.selectedIndex >= 0)
                btnRatear.disabled = false;

            for (i = i_init; i < fg.Rows; i++) {
                nEmpresaID = fg.TextMatrix(i, getColIndexByColKey(fg, 'EmpresaID'));

                // btnRatear
                if (btnRatear.disabled == false) {
                    for (j = 0; j < selEmpresas.options.length; j++) {
                        if (selEmpresas.options[j].selected) {
                            if (parseInt(selEmpresas.options[j].value, 10) == parseInt(nEmpresaID, 10)) {
                                btnRatear.disabled = true;
                                break;
                            }
                        }
                    }
                }
                nPreviousEmpresaID = nEmpresaID;
            }
        }
        else {
            btnRatear.disabled = true;
        }
    }
    else {
        btnRatear.disabled = true;
    }
}

function showDetalhesCtrls(bShow) {
    var nTipoDetalhamentoID = 0;
    var nExcluir = 0;
    var bTemContaCompensacao = false;

    var i, i_init;

    lblArgumento.style.visibility = (bShow ? 'inherit' : 'hidden');

    txtArgumento.style.visibility = (bShow ? 'inherit' : 'hidden');
    txtArgumento.disabled = !bShow;

    btnListarDetalhes.style.visibility = (bShow ? 'inherit' : 'hidden');

    btnListarDetalhes.disabled = !bShow;

    lblDetalhes.style.visibility = (bShow ? 'inherit' : 'hidden');

    selDetalhes.style.visibility = (bShow ? 'inherit' : 'hidden');
    selDetalhes.disabled = (!bShow) || (selDetalhes.options.length == 0);

    lblAtivo.style.visibility = 'hidden';
    chkAtivo.style.visibility = 'hidden';

    btnPreencher.style.visibility = (bShow ? 'inherit' : 'hidden');

    if (selDetalhes.selectedIndex < 0)
        btnPreencher.disabled = true;
    else
        btnPreencher.disabled = !bShow;

    btnExcluir.style.visibility = 'inherit';
    btnExcluir.disabled = true;

    btnLimpar.style.visibility = (bShow ? 'inherit' : 'hidden');

    lblDetalhes.innerText = 'Detalhes';

    // consulta a variavel da automacao
    glb_totalCols__ ? i_init = 2 : i_init = 1;

    if (fg.Row >= i_init) {
        if (!(isNaN(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Excluir')))))
            nExcluir = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Excluir'));

        if (glb_nContaCompensacaoID != 0) {
            for (i = i_init; i < fg.Rows; i++) {
                if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ContaID')) == glb_nContaCompensacaoID);
                {
                    bTemContaCompensacao = true;
                    break;
                }
            }
        }

        if ((nExcluir == 1) || (bTemContaCompensacao))
            btnExcluir.disabled = false;

        if (bShow) {
            if (!(isNaN(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoDetalhamentoID')))))
                nTipoDetalhamentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoDetalhamentoID'));

            // Pessoa
            if ((nTipoDetalhamentoID >= 1532) && (nTipoDetalhamentoID <= 1541)) {
                lblDetalhes.innerText = 'Pessoas';

                if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*')) != '')
                    btnLimpar.disabled = false;
                else
                    btnLimpar.disabled = true;

                lblAtivo.style.visibility = (bShow ? 'inherit' : 'hidden');
                chkAtivo.style.visibility = (bShow ? 'inherit' : 'hidden');
            }
                // Conta
            else if (nTipoDetalhamentoID == 1542) {
                lblDetalhes.innerText = 'Bco/Ag/Cta';

                if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RelPesContaID')) != '')
                    btnLimpar.disabled = false;
                else
                    btnLimpar.disabled = true;
            }
                // Imposto
            else if (nTipoDetalhamentoID == 1543) {
                lblDetalhes.innerText = 'Impostos';

                if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ImpostoID')) != '')
                    btnLimpar.disabled = false;
                else
                    btnLimpar.disabled = true;
            }
        }
        else
            btnLimpar.disabled = true;
    }
    else
        btnLimpar.disabled = true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_gerarLancamentos_AfterRowColChange(grid, OldRow, OldCol, NewRow, NewCol) {
    var firstLine = 0;
    var nTipoDetalhamentoID = 0;

    if (glb_totalCols__ != false)
        firstLine = 1;

    if (NewRow > firstLine) {
        grid.Row = NewRow;
    }

    if (grid.Editable) {
        if (OldRow != NewRow) {
            clearListDetalhe();

            btnLimpar.disabled = (grid.TextMatrix(NewRow, getColIndexByColKey(fg, 'PessoaID*')) == '');
        }

        if (!(isNaN(grid.ValueMatrix(NewRow, getColIndexByColKey(fg, 'TipoDetalhamentoID')))))
            nTipoDetalhamentoID = grid.ValueMatrix(NewRow, getColIndexByColKey(grid, 'TipoDetalhamentoID'));

        showDetalhesCtrls((nTipoDetalhamentoID >= 1532) && (nTipoDetalhamentoID <= 1543));
    }

    // grid tem primeira linha de totalizacao
    if (glb_totalCols__) {
        if (grid.Row == 1) {
            if (grid.Rows > 2)
                grid.Row = 2;
        }
    }
}

function clearListDetalhe() {
    clearComboEx(['selDetalhes']);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_gerarLancamentos_AfterEdit(Row, Col) {
    if (fg.Editable) {
        if (Col == getColIndexByColKey(fg, 'Valor')) {
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

            if (fg.TextMatrix(Row, Col) == '' || (isNaN(fg.ValueMatrix(Row, Col))))
                fg.TextMatrix(Row, Col) = '0.00';

            reSomaValores();

            disableEnable_btnLancamento(false);
        }
        else if (Col == getColIndexByColKey(fg, 'RelPesContaID')) {
            dsoCmb02Grid_01.recordset.Find('RelPesContaID', fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RelPesContaID')));

            if (!((dsoCmb02Grid_01.recordset.BOF) && (dsoCmb02Grid_01.recordset.EOF)))
                fg.TextMatrix(Row, getColIndexByColKey(fg, 'LanContaEmpresaID')) = dsoCmb02Grid_01.recordset['EmpresaID'].value;
        }
        else
            paintDetailCells();
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_gerarLancamentos_ChangeEdit() {
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_gerarLancamentos_ValidateEdit() {
    var fg_EditText = '';

    var gridStatus = fg.Editable;

    if (gridStatus) {
        fg.Editable = false;

        if (fg.Col == getColIndexByColKey(fg, 'ContaID')) {
            validateConta();
            fg.Editable = gridStatus;
            return null;
        }
        else if (fg.Col == getColIndexByColKey(fg, 'Valor')) {
            disableEnable_btnLancamento(false);

            fg_EditText = getNumAlphaStripedEx(trimStr(fg.EditText), DECIMAL_SEP, false);

            if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'EhDebito')) != 0)
                fg.EditText = '-' + fg_EditText;
            else
                fg.EditText = fg_EditText;

            var i, i_init;

            // consulta a variavel da automacao
            glb_totalCols__ ? i_init = 2 : i_init = 1;

            for (i = i_init; i < fg.Rows; i++) {
                if (isNaN(fg_EditText) || (fg_EditText == 0)) {
                    disableEnable_btnLancamento(true);
                    break;
                }
            }

            fg.Select(fg.Row, fg.Col, fg.Row, fg.Col);

            // Se negativo pinta de vermelho
            if (parseInt(fg.EditText, 10) < 0)
                fg.CellForeColor = 0X0000FF;
                // Senao de preto	
            else
                fg.CellForeColor = 0;
        }
        else if ((fg.Col == getColIndexByColKey(fg, 'Documento')) ||
		          (fg.Col == getColIndexByColKey(fg, 'RelPesContaID')) ||
		          (fg.Col == getColIndexByColKey(fg, 'ImpostoID'))) {
            if (fg.EditText != '')
                fg.Cell(6, fg.Row, fg.Col, fg.Row, fg.Col) = 0;
        }
    }
    fg.Editable = gridStatus;
}

/********************************************************************
Funcao criada pelo programador.
Usada pela funcao js_gerarLancamentos_ValidateEdit()

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function validateConta() {
    var nTipoDetalhamentoID = 0;

    var nEmpresaID = parseInt(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EmpresaID')), 10);

    if (!((dsoCmb01Grid_01.recordset.BOF) && (dsoCmb01Grid_01.recordset.EOF))) {
        dsoCmb01Grid_01.recordset.MoveFirst();

        while (!dsoCmb01Grid_01.recordset.EOF) {
            if (dsoCmb01Grid_01.recordset['ContaID'].value == parseInt(fg.ComboData, 10)) {
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Conta*')) = dsoCmb01Grid_01.recordset['Conta'].value;
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Det*')) = dsoCmb01Grid_01.recordset['Det'].value;
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'TipoDetalhamentoID')) = dsoCmb01Grid_01.recordset['TipoDetalhamentoID'].value;

                if (parseInt(dsoCmb01Grid_01.recordset['UsoEspecialID'].value, 10) == 1514) {
                    // Pagamento
                    if (parseInt(selTipoLancamentoID.value, 10) == 1571) {
                        if (!chkEhEstorno.checked)
                            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EhDebito')) = (glb_nEmpresaID == nEmpresaID ? '1' : '0');
                        else
                            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EhDebito')) = (glb_nEmpresaID == nEmpresaID ? '0' : '1');
                    }
                        // Recebimento
                    else {
                        if (!chkEhEstorno.checked)
                            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EhDebito')) = (glb_nEmpresaID == nEmpresaID ? '0' : '1');
                        else
                            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EhDebito')) = (glb_nEmpresaID == nEmpresaID ? '1' : '0');
                    }
                }
                else
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EhDebito')) = dsoCmb01Grid_01.recordset['EhDebito'].value;

                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'UsoEspecialID')) = dsoCmb01Grid_01.recordset['UsoEspecialID'].value;
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EhFilhaDisp')) = dsoCmb01Grid_01.recordset['EhFilhaDisp'].value;

                if (!dsoCmb01Grid_01.recordset['AceitaLancamento'].value) {
                    if (window.top.overflyGen.Alert('Esta conta n�o permite lan�amento.') == 0)
                        return null;
                }

                break;
            }
            dsoCmb01Grid_01.recordset.MoveNext();
        }

        if (fg.Editable) {
            if (!(isNaN(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoDetalhamentoID')))))
                nTipoDetalhamentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoDetalhamentoID'));

            showDetalhesCtrls((nTipoDetalhamentoID >= 1532) && (nTipoDetalhamentoID <= 1541));
        }
    }
}

function ratear() {
    var aContaID = new Array();
    var nEmpresaAtualID = 0;
    var sEmpresaAtual = '';
    var nContaAtualID = 0;
    var sDocumento = '';
    var nDiferenca, nValor, nLastValor;
    var i, j, i_init;
    var numEmprSel = 0;

    // consulta a variavel da automacao
    glb_totalCols__ ? i_init = 2 : i_init = 1;

    for (i = 0; i < selEmpresas.options.length; i++) {
        if (selEmpresas.options(i).selected)
            numEmprSel++;
    }

    if (fg.Rows < i_init) {
        if (window.top.overflyGen.Alert('N�o existem contas selecionadas.\nPreencha o grid.') == 0)
            return null;

        return null;
    }

    if (!isNaN(fg.ValueMatrix((i_init - 1), getColIndexByColKey(fg, 'Valor')))) {
        nDiferenca = Math.abs(fg.ValueMatrix((i_init - 1), getColIndexByColKey(fg, 'Valor')));

        if (nDiferenca == 0) {
            if (window.top.overflyGen.Alert('Lan�amento sem diferen�a.') == 0)
                return null;

            return null;
        }
    }
    else
        return null;

    nValor = nDiferenca / numEmprSel;

    for (i = 0; i < selEmpresas.options.length; i++) {
        if (selEmpresas.options(i).selected) {
            nEmpresaAtualID = selEmpresas.options(i).value;
            sEmpresaAtual = selEmpresas.options(i).innerText;
            aContaID = [];

            for (j = i_init; j < fg.Rows; j++) {
                if ((parseInt(glb_nEmpresaID, 10) == parseInt(fg.TextMatrix(j, getColIndexByColKey(fg, 'EmpresaID')), 10)) &&
					 (fg.ValueMatrix(j, getColIndexByColKey(fg, 'EhFilhaDisp')) == 0) &&
					 (fg.ValueMatrix(j, getColIndexByColKey(fg, 'UsoEspecialID')) != 1514)) {
                    nContaAtualID = parseInt(fg.TextMatrix(j, getColIndexByColKey(fg, 'ContaID')), 10);
                    sDocumento = fg.TextMatrix(j, getColIndexByColKey(fg, 'Documento'));

                    if (ascan(aContaID, nContaAtualID) == -1) {
                        aContaID[aContaID.length] = nContaAtualID;

                        if (glb_nContaCompensacaoID == 0) {
                            if (!(dsoCmb01Grid_01.recordset.BOF && dsoCmb01Grid_01.recordset.EOF))
                                dsoCmb01Grid_01.recordset.MoveFirst();

                            while (!dsoCmb01Grid_01.recordset.EOF) {
                                if (parseInt(dsoCmb01Grid_01.recordset.Fields['UsoEspecialID'].value, 10) == 1514) {
                                    glb_nContaCompensacaoID = dsoCmb01Grid_01.recordset.Fields['ContaID'].value;
                                    glb_sContaCompensacao = dsoCmb01Grid_01.recordset.Fields['Conta'].value;
                                    break;
                                }
                                dsoCmb01Grid_01.recordset.MoveNext();
                            }
                        }
                        ratear_2ndpart(nEmpresaAtualID, sEmpresaAtual, nContaAtualID, glb_nContaCompensacaoID, nValor, sDocumento);
                    }
                }
            }
            reSomaValores();
        }
    }
}

function ratear_2ndpart(nEmpresaAtualID, sEmpresaAtual, nContaAtualID, glb_nContaCompensacaoID, nValor, sDocumento) {
    var aEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var sEmpresaFantasia = aEmpresaID[3];

    fg.Editable = false;
    fg.Redraw = 0;

    var nSinal = 1;

    // Pagamento
    if (parseInt(selTipoLancamentoID.value, 10) == 1571) {
        if (!chkEhEstorno.checked) {
            nSinal = 1;
        }
        else {
            nSinal = -1;
        }
    }
        // Recebimento
    else {
        if (!chkEhEstorno.checked) {
            nSinal = -1;
        }
        else {
            nSinal = 1;
        }
    }

    // Conta de Compensacao da Empresa Logada 
    insertContaInGrid(glb_nEmpresaID, sEmpresaFantasia, nEmpresaAtualID, sEmpresaAtual, glb_nContaCompensacaoID, (-1 * nValor * nSinal), '');

    // Conta de Compensacao da Empresa Lista 
    insertContaInGrid(nEmpresaAtualID, sEmpresaAtual, glb_nEmpresaID, sEmpresaFantasia, glb_nContaCompensacaoID, (nValor * nSinal), '');

    // Conta da Comum da Empresa Lista 
    insertContaInGrid(nEmpresaAtualID, sEmpresaAtual, '', '', nContaAtualID, (-1 * nValor * nSinal), sDocumento);

    paintNegativeNumbers();
    paintDetailCells();

    fg.Redraw = 2;
    fg.Editable = true;

    showEmpresasCtrls(true);
    showDetalhesCtrls(true);
}

function insertContaInGrid(nEmpresaID, sEmpresa, nPessoaID, sPessoa, nContaID, nValor, sDocumento) {
    var i;
    var rowPosition = -1;
    var sPesquisa, sPosicaoAtual, lineData;

    // consulta a variavel da automacao
    glb_totalCols__ ? i_init = 2 : i_init = 1;

    // Decobre se a Conta � Devedora ou Credora
    if (!((dsoCmb01Grid_01.recordset.BOF) && (dsoCmb01Grid_01.recordset.EOF))) {
        dsoCmb01Grid_01.recordset.MoveFirst();

        while (!dsoCmb01Grid_01.recordset.EOF) {
            if (dsoCmb01Grid_01.recordset['ContaID'].value == parseInt(nContaID, 10)) {
                sPesquisa = (glb_nEmpresaID == nEmpresaID ? '0' : '1');
                sPesquisa += padR(sEmpresa, 20, ' ');

                if (parseInt(dsoCmb01Grid_01.recordset['UsoEspecialID'].value, 10) == 1514) {
                    // Pagamento
                    if (parseInt(selTipoLancamentoID.value, 10) == 1571) {
                        if (!chkEhEstorno.checked)
                            sPesquisa += (glb_nEmpresaID == nEmpresaID ? '0' : '1');
                        else
                            sPesquisa += (glb_nEmpresaID == nEmpresaID ? '1' : '0');
                    }
                        // Recebimento
                    else {
                        if (!chkEhEstorno.checked)
                            sPesquisa += (glb_nEmpresaID == nEmpresaID ? '1' : '0');
                        else
                            sPesquisa += (glb_nEmpresaID == nEmpresaID ? '0' : '1');
                    }
                }
                else
                    sPesquisa += (dsoCmb01Grid_01.recordset['EhDebito'].value == 1 ? '0' : '1');

                sPesquisa += padL(nContaID.toString(), 10, '0');
                sPesquisa += padL(nPessoaID.toString(), 10, '0');

                break;
            }
            dsoCmb01Grid_01.recordset.MoveNext();
        }
    }

    for (i = i_init; i < fg.Rows; i++) {
        sPosicaoAtual = (glb_nEmpresaID == parseInt(fg.TextMatrix(i, getColIndexByColKey(fg, 'EmpresaID')), 10) ? '0' : '1');
        sPosicaoAtual += padR(fg.TextMatrix(i, getColIndexByColKey(fg, 'Empresa*')), 20, ' ');
        sPosicaoAtual += (fg.ValueMatrix(i, getColIndexByColKey(fg, 'EhDebito')) != 0 ? '1' : '0');
        sPosicaoAtual += padL(fg.TextMatrix(i, getColIndexByColKey(fg, 'ContaID')), 10, '0');
        sPosicaoAtual += padL(fg.TextMatrix(i, getColIndexByColKey(fg, 'PessoaID*')), 10, '0');

        if (sPesquisa <= sPosicaoAtual) {
            rowPosition = i;
            break;
        }
    }

    if (rowPosition == -1)
        rowPosition = fg.Rows;

    if (!((dsoCmb01Grid_01.recordset.BOF) && (dsoCmb01Grid_01.recordset.EOF))) {
        lineData = '';
        lineData = nEmpresaID + '\t';
        lineData += sEmpresa + '\t';
        lineData += nContaID + '\t';
        lineData += nValor.toString() + '\t';
        lineData += sDocumento + '\t';
        lineData += dsoCmb01Grid_01.recordset['Det'].value + '\t';
        lineData += nPessoaID + '\t';
        lineData += sPessoa + '\t';
        lineData += '' + '\t';
        lineData += '' + '\t';
        lineData += dsoCmb01Grid_01.recordset['Conta'].value + '\t';
        lineData += dsoCmb01Grid_01.recordset['TipoDetalhamentoID'].value + '\t';

        if (parseInt(dsoCmb01Grid_01.recordset['UsoEspecialID'].value, 10) == 1514) {
            // Pagamento
            if (parseInt(selTipoLancamentoID.value, 10) == 1571) {
                if (!chkEhEstorno.checked)
                    lineData += (glb_nEmpresaID == nEmpresaID ? '1' : '0') + '\t';
                else
                    lineData += (glb_nEmpresaID == nEmpresaID ? '0' : '1') + '\t';
            }
                // Recebimento
            else {
                if (!chkEhEstorno.checked)
                    lineData += (glb_nEmpresaID == nEmpresaID ? '0' : '1') + '\t';
                else
                    lineData += (glb_nEmpresaID == nEmpresaID ? '1' : '0') + '\t';
            }
        }
        else
            lineData += dsoCmb01Grid_01.recordset['EhDebito'].value + '\t';

        lineData += dsoCmb01Grid_01.recordset['UsoEspecialID'].value + '\t';
        lineData += dsoCmb01Grid_01.recordset['EhFilhaDisp'].value + '\t';
        lineData += 1 + '\t';

        if (lineData != '') {
            addAndFillineInGrid(fg, lineData, rowPosition);
            reSomaValores();
        }
    }
}

function recalcular() {
    ;
}

function removeLineInGrid() {
    var i_init;

    // consulta a variavel da automacao
    glb_totalCols__ ? i_init = 2 : i_init = 1;

    var _retConf = window.top.overflyGen.Confirm('Excluir conta?');

    if (_retConf == 1) {
        if ((fg.Rows > i_init) && (fg.Row > 0)) {
            fg.Editable = false;
            deleteLineInGrid(fg, fg.Row, true);
            reSomaValores();
            fg.Editable = true;

            showEmpresasCtrls(true);
            showDetalhesCtrls(true);
        }

        if (fg.Rows <= 1)
            glb_totalCols__ = false;
    }
    else {
        return null;
    }
}

function paintNegativeNumbers() {
    var i, i_init;
    var nCurrCellRow = -1;
    var nCurrCellCol = -1;
    var nColValor = getColIndexByColKey(fg, 'Valor');

    var gridStatus = fg.Editable;

    fg.Editable = false;

    // consulta a variavel da automacao
    glb_totalCols__ ? i_init = 2 : i_init = 1;

    if (fg.Row > i_init)
        nCurrCellRow = fg.Row;

    if (fg.Col >= 0)
        nCurrCellCol = fg.Col;

    for (i = i_init; i < fg.Rows; i++) {
        fg.Select(i, nColValor, i, nColValor);

        if (parseFloat(fg.TextMatrix(i, getColIndexByColKey(fg, 'Valor'))) < 0)
            fg.CellForeColor = 0X0000FF;
        else
            fg.CellForeColor = 0;
    }

    if (nCurrCellRow > -1)
        fg.Row = nCurrCellRow;

    if (nCurrCellCol > -1)
        fg.Col = nCurrCellCol;

    fg.Editable = gridStatus;
}

function paintDetailCells() {
    var bgColorYellow = 0X8CE6F0;
    var nColDocumento = getColIndexByColKey(fg, 'Documento');
    var nColPessoa = getColIndexByColKey(fg, 'Pessoa*');
    var nColContaBancaria = getColIndexByColKey(fg, 'RelPesContaID');
    var nColImposto = getColIndexByColKey(fg, 'ImpostoID');
    var nTipoDetalhamentoID = 0;

    var i, i_init;

    var nCurrCellRow = -1;
    var nCurrCellCol = -1;

    var gridStatus = fg.Editable;

    fg.Editable = false;

    // consulta a variavel da automacao
    glb_totalCols__ ? i_init = 2 : i_init = 1;

    if (fg.Row > i_init)
        nCurrCellRow = fg.Row;

    if (fg.Col >= 0)
        nCurrCellCol = fg.Col;

    for (i = i_init; i < fg.Rows; i++) {
        nTipoDetalhamentoID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoDetalhamentoID'));
        fg.FillStyle = 1;

        if ((fg.TextMatrix(i, nColDocumento) == null) || (trimStr(fg.TextMatrix(i, nColDocumento)) == ''))
            fg.Cell(6, i, nColDocumento, i, nColDocumento) = bgColorYellow;
        else
            fg.Cell(6, i, nColDocumento, i, nColDocumento) = 0;

        // Pessoa
        if ((nTipoDetalhamentoID >= 1532) && (nTipoDetalhamentoID <= 1541) && (fg.TextMatrix(i, nColPessoa) == ''))
            fg.Cell(6, i, nColPessoa, i, nColPessoa) = bgColorYellow;
            // Conta
        else if ((nTipoDetalhamentoID == 1542) && (fg.TextMatrix(i, nColContaBancaria) == ''))
            fg.Cell(6, i, nColContaBancaria, i, nColContaBancaria) = bgColorYellow;
            // Imposto
        else if ((nTipoDetalhamentoID == 1543) && (fg.TextMatrix(i, nColImposto) == ''))
            fg.Cell(6, i, nColImposto, i, nColImposto) = bgColorYellow;
        else {
            fg.Cell(6, i, nColPessoa, i, nColPessoa) = 0XDCDCDC;
            fg.Cell(6, i, nColContaBancaria, i, nColImposto) = 0;
        }
    }

    if (nCurrCellRow > -1)
        fg.Row = nCurrCellRow;

    if (nCurrCellCol > -1)
        fg.Col = nCurrCellCol;

    fg.Editable = gridStatus;
}

function disableEnable_btnLancamento(action) {
    // Desabilita sempre que solicitado
    if (action) {
        btnLancamento.disabled = action;
        return null;
    }

    // Para habilitar tem regras
    var i, i_init;

    // Consulta a variavel da automacao
    glb_totalCols__ ? i_init = 2 : i_init = 1;

    // Trap - nao tem linhas suficientes no grid	
    if (fg.Rows <= i_init) {
        btnLancamento.disabled = true;
        return null;
    }

    btnLancamento.disabled = action;
}

function reSomaValores() {
    var i, i_init;
    var nTotal = 0;
    var nCol = getColIndexByColKey(fg, 'Valor');

    // consulta a variavel da automacao
    glb_totalCols__ ? i_init = 2 : i_init = 1;

    for (i = i_init; i < fg.Rows; i++) {
        if (!isNaN(fg.ValueMatrix(i, nCol)))
            nTotal += fg.ValueMatrix(i, nCol);
    }

    if (fg.Rows > 2)
        fg.TextMatrix(1, nCol) = -nTotal;
}

function adjustLabelsCombos() {
    setLabelOfControl(lblTipoLancamentoID, selTipoLancamentoID.value);

    if ((selHistoricoPadraoID.value != null) && (parseInt(selHistoricoPadraoID.value, 10) != 0))
        setLabelOfControl(lblHistoricoPadraoID, selHistoricoPadraoID.value);
    else
        setLabelOfControl(lblHistoricoPadraoID, '');
}

function selDetalhes_ondlbclick() {
    if ((selDetalhes.selectedIndex >= 0) && (btnPreencher.disabled == false))
        btn_onclick(btnPreencher);
}

function clonarLancamento() {
    var strPas = '?nLancamentoID=' + escape(glb_nLancamentoID);

    lockControlsInModalWin(true);

    dsoClonar.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/lancamentos/serverside/clonarlancamento.aspx' + strPas;
    dsoClonar.ondatasetcomplete = clonarLancamento_DSC;
    dsoClonar.Refresh();
}

function clonarLancamento_DSC() {
    if (!dsoClonar.recordset.EOF) {
        dsoClonar.recordset.MoveFirst();
        selOptByValueInSelect(getHtmlId(), 'selTipoLancamentoID', dsoClonar.recordset['TipoLancamentoID'].value);
        chkUsoCotidiano.checked = false;

        if (dsoClonar.recordset['HistoricoComplementar'].value != null)
            txtHistoricoComplementar.value = dsoClonar.recordset['HistoricoComplementar'].value;
        else
            txtHistoricoComplementar.value = '';

        chkEhDefault.checked = false;
    }

    glb_nCmbsTimerInterval = window.setInterval('startDynamicCmbs()', 10, 'JavaScript');
}

function fillgridClonar() {
    if (!dsoClonar.recordset.EOF) {
        selOptByValueInSelect(getHtmlId(), 'selHistoricoPadraoID', dsoClonar.recordset['HistoricoPadraoID'].value);
        adjustLabelsCombos();
    }

    glb_nCmbsTimerInterval = window.setInterval('fillDSOGrid()', 10, 'JavaScript');
}