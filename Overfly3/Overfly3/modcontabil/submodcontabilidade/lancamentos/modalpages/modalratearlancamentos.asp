
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>
<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalratearlancamentosHtml" name="modalratearlancamentosHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/lancamentos/modalpages/modalratearlancamentos.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/lancamentos/modalpages/modalratearlancamentos.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf

Dim i, sCaller, nLancamentoID, nUserID

nLancamentoID = 0
nUserID = 0

For i = 1 To Request.QueryString("nLancamentoID").Count    
    nLancamentoID = Request.QueryString("nLancamentoID")(i)
Next

For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

For i = 1 To Request.QueryString("sCaller").Count
    sCaller = Request.QueryString("sCaller")(i)
Next

Response.Write "var glb_nUserID = " & CStr(nUserID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nLancamentoID = " & CStr(nLancamentoID) & ";"
Response.Write vbcrlf

Response.Write "var glb_sCaller = " & Chr(34) & CStr(sCaller) & Chr(34) & ";"
Response.Write vbcrlf

Dim strSQL, rsData

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT a.EmpresaID, f.Fantasia AS Empresa, b.RecursoAbreviado AS Estado, CONVERT(VARCHAR(10), a.dtLancamento, 103) AS Data, a.EhEstorno, " & _
			"c.ItemAbreviado AS TipoLancamento, d.HistoricoPadrao AS HistoricoPadrao, " & _
			"e.SimboloMoeda AS Moeda, replace(dbo.fn_Numero_Formata(a.ValorTotalLancamento, 2, 1, 101),',','') AS ValorTotalLancamento " & _
		 "FROM Lancamentos a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), HistoricosPadrao d WITH(NOLOCK), Conceitos e WITH(NOLOCK), Pessoas f WITH(NOLOCK) " & _
		 "WHERE (a.LancamentoID = " & CStr(nLancamentoID) & " AND a.EstadoID = b.RecursoID AND " & _
			"a.TipoLancamentoID = c.ItemID AND a.HistoricoPadraoID = d.HistoricoPadraoID AND " & _
			"a.MoedaID = e.ConceitoID AND a.EmpresaID = f.PessoaID) "
				
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

If (Not (rsData.BOF AND rsData.EOF) ) Then

	Response.Write "var glb_nEmpresaID = " & CStr(rsData.Fields("EmpresaID").Value) & ";"
	Response.Write vbcrlf
	Response.Write "var glb_sEmpresa = " & Chr(39) & CStr(rsData.Fields("Empresa").Value) & Chr(39) & ";"
	Response.Write vbcrlf
	Response.Write "var glb_sEstado = " & Chr(39) & CStr(rsData.Fields("Estado").Value) & Chr(39) & ";"
	Response.Write vbcrlf
	Response.Write "var glb_sData = " & Chr(39) & CStr(rsData.Fields("Data").Value) & Chr(39) & ";"
	Response.Write vbcrlf

	If (rsData.Fields("EhEstorno").Value = True) Then
		Response.Write "var glb_bEhEstorno = true;"
	Else
		Response.Write "var glb_bEhEstorno = false;"
	End If

	Response.Write vbcrlf
	Response.Write "var glb_sTipoLancamento = " & Chr(39) & CStr(rsData.Fields("TipoLancamento").Value) & Chr(39) & ";"
	Response.Write vbcrlf
	Response.Write "var glb_sHistoricoPadrao = " & Chr(39) & CStr(rsData.Fields("HistoricoPadrao").Value) & Chr(39) & ";"
	Response.Write vbcrlf
	Response.Write "var glb_sMoeda = " & Chr(39) & CStr(rsData.Fields("Moeda").Value) & Chr(39) & ";"
	Response.Write vbcrlf
	Response.Write "var glb_nValorTotalLancamento = '" & CStr(rsData.Fields("ValorTotalLancamento").Value) & "';"
	'Response.Write "var glb_nValorTotalLancamento = " & CStr(100) & ";"
	Response.Write vbcrlf

End If

rsData.Close
Set rsData = Nothing

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
js_gerarLancamentos_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_gerarLancamentos_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ChangeEdit>
<!--
 js_gerarLancamentos_ChangeEdit();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_gerarLancamentos_ValidateEdit();
//-->
</SCRIPT>

</head>

<body id="modalratearlancamentosBody" name="modalratearlancamentosBody" LANGUAGE="javascript" onload="return window_onload()">
     <div id="divPesquisa" name="divPesquisa" class="divGeneral">
        <p id="lblLancamentoID" name="lblLancamentoID" class="lblGeneral">ID</p>
        <input type="text" id="txtLancamentoID" name="txtLancamentoID" class="fldGeneral"></input>
        <p id="lblEstado" name="lblEstado" class="lblGeneral">Est</p>
        <input type="text" id="txtEstado" name="txtEstado" class="fldGeneral"></input>
        <p id="lblData" name="lblData" class="lblGeneral">Data</p>
        <input type="text" id="txtData" name="txtData" class="fldGeneral"></input>
        <p  id="lblEhEstorno" name="lblEhEstorno" class="lblGeneral">Est</p>
        <input type="checkbox" id="chkEhEstorno" name="chkEhEstorno" class="fldGeneral" title="� Estorno?"></input>
        <p id="lblTipoLancamento" name="lblTipoLancamento" class="lblGeneral">Tipo</p>
        <input type="text" id="txtTipoLancamento" name="txtTipoLancamento" class="fldGeneral"></input>
        <p id="lblHistoricoPadrao" name="lblHistoricoPadrao" class="lblGeneral">Hist�rico Padr�o</p>
        <input type="text" id="txtHistoricoPadrao" name="txtHistoricoPadrao" class="fldGeneral"></input>
        <p id="lblMoeda" name="lblMoeda" class="lblGeneral">$</p>
        <input type="text" id="txtMoeda" name="txtMoeda" class="fldGeneral"></input>
        <p id="lblValorLancamento" name="lblValorLancamento" class="lblGeneral">Valor</p>
        <input type="text" id="txtValorLancamento" name="txtValorLancamento" class="fldGeneral"></input>
		<input type="button" id="btnRefresh" name="btnRefresh" value="Refresh" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>    
		<input type="button" id="btnOK" name="btnOK" value="Ratear" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    </div>    

     <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
	
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
