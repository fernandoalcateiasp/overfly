/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form lancamentos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoPropsPL = new CDatatransport('dsoPropsPL');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoListData01 = new CDatatransport('dsoListData01');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Data', 'Balancete', 'Conc', 'Est',
								 'Tipo', 'Quant', 'Hist�rico Padr�o', 'Hist�rico Complementar', 
								 'Detalhe', 'Total Lan�amento', 'Total Convertido', 'Observa��o');

    var dTFormat = '';

    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    glb_aCOLPESQFORMAT = new Array('', '', dTFormat, dTFormat, '','',
                                   '', '', '', '', '', 
                                   '###,###,###,###.00', '###,###,###,###.00', '');
    
    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
	showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1,1,1]);
	tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Clonar lan�amento', 'Gerar lan�amentos pendentes', 'Ratear lan�amento', 'Ativar lan�amentos', 'Detalhar lan�amento m�e']);
	
    alignColsInGrid(fg,[0,7,11,12]);

	setupEspecBtnsControlBar('sup', 'HHHHHDH');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	// Usuario clicou botao documentos
	if ((controlBar == 'SUP') && (btnClicked == 1)) {
		if (fg.Rows > 1) {
			__openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
		}
		else {
		    window.top.overflyGen.Alert('Selecione um registro.');
		}
	}
	// Procedimento
    if ((controlBar == 'SUP') && (btnClicked == 3))
		window.top.openModalControleDocumento('PL', '', 910, null, '221', 'T');
	if ((controlBar == 'SUP') && (btnClicked==4))
		openModalGeraLancamentos();
	else if ((controlBar == 'SUP') && (btnClicked==5))
		openModalGeraLancamentosPendentes();
	else if ((controlBar == 'SUP') && (btnClicked==7))
		openModalAtivarLancamentos();
}

/********************************************************************
Criado pelo programador
Abre a JM de Gerar Ocorrencias

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalAtivarLancamentos()
{
    var htmlPath;
	var aEmpresa = getCurrEmpresaData();
    var strPars = new String();

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
	strPars += '&nEmpresaID=' + escape(aEmpresa[0]);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/lancamentos/modalpages/modalativarlancamentos.asp'+strPars;
    showModalWin(htmlPath, new Array(775,460));
}

function openModalGeraLancamentosPendentes()
{
    var htmlPath;
	var aEmpresa = getCurrEmpresaData();
    var strPars = new String();

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
	strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
	strPars += '&nLancamentoID=' + escape(0);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/lancamentos/modalpages/modalgerarlancamentospendentes.asp' + strPars;
    showModalWin(htmlPath, new Array(775,460));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Gerar Lancamentos.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalGeraLancamentos()
{
	var aEmpresa = getCurrEmpresaData();
	var strPars = '?sCaller=' + escape('PL');
	strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
	strPars += '&nEmpresaPaisID=' + escape(aEmpresa[1]);
	strPars += '&nUserID=' + escape(getCurrUserID());

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/lancamentos/modalpages/modalgerarlancamentos.asp' + strPars;
    //showModalWin(htmlPath, new Array(717,460));
    //showModalWin(htmlPath, new Array(787,462));
    showModalWin(htmlPath, new Array(771,462));
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALGERARLANCAMENTOSHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            
            if ( param2 != null )
				detalharLancamento(param2);
                        
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALGERARLANCAMENTOSPENDENTESHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALATIVARLANCAMENTOSHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            
            if ( param2 != null )
				detalharLancamento(param2);
                        
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
    	if (param1 == 'OK') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    	else if (param1 == 'CANCEL') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}

/********************************************************************
Funcao do programador, detalha um lancamento

Parametro:
ID do lancamento a detalhar

Retorno:
a string ou null se nao tem
********************************************************************/
function detalharLancamento(lancamentoID)
{
	lockInterface(true);
	
	sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWDETAIL', 
                  [lancamentoID, null, null]);	
}