/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_BtnFromFramWork = null;
var glb_bAlteracao = false;
var glb_bBtnClicked = false;
var glb_sLastBtn = '';
var glb_nTimerCallAutomation = null;
var dsoSup01 = new CDatatransport('dsoSup01');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoCurrData = new CDatatransport('dsoCurrData');
var dsoVerificacao = new CDatatransport('dsoVerificacao');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    glb_aStaticCombos = ([['selTipoLancamentoID', '1'],
						  ['selMoedaID', '2']]);

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/lancamentos/inferior.asp',
                              SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/lancamentos/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'LancamentoID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

function setupPage()
{
	var elemBase, elemBase1, elem;
	
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID','txtEstadoID',2,1],
                          ['lbldtLancamento','txtdtLancamento',10,1],
                          ['lbldtBalancete','txtdtBalancete',10,1],
                          ['lblLancamentoMaeID','txtLancamentoMaeID',10,1],
                          ['lblLancamentoConciliado','chkLancamentoConciliado',3,1],
                          ['lblEhEstorno','chkEhEstorno',3,1],
                          ['lblTipoLancamentoID','selTipoLancamentoID',7,2],
                          ['lblQuantidade','txtQuantidade',4,2],
                          ['lblHistoricoPadraoID','selHistoricoPadraoID',25,2],
                          ['lblHistoricoComplementar','txtHistoricoComplementar',29,2],
                          ['lblNumeroDebitos','txtNumeroDebitos',5,3],
                          ['lblNumeroCreditos','txtNumeroCreditos',5,3],
                          ['lblMoedaID','selMoedaID',8,3],
                          ['lblValorTotalLancamento','txtValorTotalLancamento',11,3],
                          ['lblTaxaMoeda','txtTaxaMoeda',11,3],
                          ['lblValorTotalConvertido','txtValorTotalConvertido',11,3],
                          ['lblObservacao','txtObservacao',30,3]], null, null, true);
                          
	
	//lblDetalhe
	elem = lblDetalhe;
	elemBase = lblHistoricoComplementar;
	elemBase1 = txtHistoricoComplementar;
	with (elem.style)
	{
		left = elemBase1.offsetLeft + elemBase1.offsetWidth + ELEM_GAP;
		top = elemBase.offsetTop;
		width = FONT_WIDTH * (elem.innerText.length);
		height = elemBase.offsetHeight;
	}
     
	elem = txtDetalhe;
	elemBase = txtHistoricoComplementar;
	elemBase1 = txtHistoricoComplementar;
	with (elem.style)
	{
		left = elemBase1.offsetLeft + elemBase1.offsetWidth + ELEM_GAP;
		top = elemBase.offsetTop;
		width = FONT_WIDTH * 22;
		height = elemBase.offsetHeight;
		borderStyle = 'inset';
		borderWidth = 2;
		fontSize = '10pt';
		paddingLeft = '1px';
		paddingTop = '1px';
	}                      

}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWLANCAMENTO')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
        
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
            
        window.top.focus();
                
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;
    
    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR'))
	{
		glb_bAlteracao = false;
        startDynamicCmbs();
	}
    else
	    finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Clonar lan�amento', 'Gerar lan�amentos pendentes', 'Ratear lan�amento', 'Ativar lan�amentos','Detalhar lan�amento m�e']);

    adjustLabelsCombos('selTipoLancamentoID', true);
    adjustLabelsCombos('selMoedaID', true);

    if ( btnClicked == 'SUPINCL' )
    {
        // funcao que controla campos read-only
        controlReadOnlyFields(false);
        
        txtQuantidade.innerText = '';
        txtDetalhe.innerText = '';
    }
    else
    {
        txtQuantidade.innerText = dsoSup01.recordset.Fields['Quantidade'].value;
        txtDetalhe.innerText = dsoSup01.recordset.Fields['Detalhe'].value;
    }
    
    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();
	else if ( (cmbID == 'selTipoLancamentoID') || 
			  (cmbID == 'selHistoricoPadraoID') || 
			  (cmbID == 'selMoedaID') )
	{
		adjustLabelsCombos(cmbID, false);
		if (cmbID == 'selTipoLancamentoID')
		{
			selHistoricoPadraoID.disabled = true;
			glb_bAlteracao = true;
			startDynamicCmbs();
		}
		else if (cmbID == 'selHistoricoPadraoID')
		{
			fillHistoricoComplementar();
		}
	}    
    
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
	verifyLancamentoInServer(currEstadoID, newEstadoID);
	return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	// Documentos
	if (btnClicked == 1) {
	    __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
	}
	// Procedimento
    if ( btnClicked == 3 )
		window.top.openModalControleDocumento('S', '', 910, null, '221', 'T');
	else if (btnClicked == 4)
		openModalGeraLancamentos();
	else if (btnClicked == 6)
		openModalRatearLancamentos();
	else if (btnClicked == 8)
		detalharLancamentoMae();
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Gerar Lancamentos.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalGeraLancamentos()
{
	var aEmpresa = getCurrEmpresaData();
	var nLancamentoID = dsoSup01.recordset.Fields['LancamentoID'].value;

	var strPars = '?sCaller=' + escape('S');
	strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
	strPars += '&nEmpresaPaisID=' + escape(aEmpresa[1]);
	strPars += '&nUserID=' + escape(getCurrUserID());
	strPars += '&nLancamentoID=' + escape(nLancamentoID);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/lancamentos/modalpages/modalgerarlancamentos.asp' + strPars;
    //showModalWin(htmlPath, new Array(717,460));
    //showModalWin(htmlPath, new Array(787,462));
    showModalWin(htmlPath, new Array(771,462));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Ratear Lancamentos.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalRatearLancamentos()
{
	var strPars = '?sCaller=' + escape('S');
	strPars += '&nLancamentoID=' + escape(dsoSup01.recordset['LancamentoID'].value);
	strPars += '&nUserID=' + escape(getCurrUserID());

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/lancamentos/modalpages/modalratearlancamentos.asp' + strPars;
    showModalWin(htmlPath, new Array(771,280));
}

function detalharLancamentoMae()
{
	sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL', 
		        [dsoSup01.recordset['LancamentoMaeID'].value, null, null]);
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    var aEmpresaData = getCurrEmpresaData();
	glb_sLastBtn = '';
    
    if (btnClicked == 'SUPOK')
    {
		// Pedido esta em inclusao
        //if ( (typeof(dsoSup01.recordset[glb_sFldIDName].value)).toUpperCase() == 'UNDEFINED' )
        if (dsoSup01.recordset[glb_sFldIDName].value == null) 
		{
            // Grava o campo empresaID, Gerador
            dsoSup01.recordset['EmpresaID'].value = aEmpresaData[0];
            dsoSup01.recordset['Gerador'].value= 1;
		}
	}
	else if ( (btnClicked == 'SUPINCL') || (btnClicked == 'SUPALT') )
	{
        glb_bAlteracao = true;
        glb_bBtnClicked = true;
        glb_sLastBtn = btnClicked;
        startDynamicCmbs();
	}
			
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALGERARLANCAMENTOSHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            
            if ( param2 != null )
				sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'detalharLancamento(' + '\'' + param2 + '\'' + ')');

            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALRATEARLANCAMENTOSHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            
            if ( param2 != null )
				sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'detalharLancamento(' + '\'' + param2 + '\'' + ')');

            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');

            if ( param2 != null )
			{
				var _retConf = window.top.overflyGen.Confirm('Deseja detalhar o lan�amento ' + param2 + '\ngerado para a Empresa logada?', 1);
			        
				if ( _retConf == 1 )
					sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'detalharLancamento(' + '\'' + param2 + '\'' + ')');
				else
					return null;
			}
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
    	if (param1 == 'OK') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    	else if (param1 == 'CANCEL') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // funcao que controla campos read-only
    controlReadOnlyFields(true);

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    // mostra dois botoes especificos desabilitados
	showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1]);
	tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Clonar lan�amento', 'Gerar lan�amentos pendentes', 'Ratear lan�amento', 'Ativar lan�amentos']);
}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Read Only

Parametro:
bAlteracao

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields(bAlteracao) {
    txtdtLancamento.readOnly = true;
    chkLancamentoConciliado.disabled = true;
    txtQuantidade.readOnly = true;
    txtLancamentoMaeID.readOnly = true;
    txtNumeroDebitos.readOnly = true;
    txtNumeroCreditos.readOnly = true;
    if (bAlteracao) {
        txtValorTotalLancamento.readOnly = true;
    }
    else {
        txtValorTotalLancamento.readOnly = false;

    }

    txtTaxaMoeda.readOnly = true;
    txtValorTotalConvertido.readOnly = true;
}

function adjustLabelsCombos(cmbID, bPaging)
{
    if (bPaging)
    {
		if (cmbID == 'selTipoLancamentoID')
		    setLabelOfControl(lblTipoLancamentoID, dsoSup01.recordset['TipoLancamentoID'].value);
		else if (cmbID == 'selHistoricoPadraoID')
		    setLabelOfControl(lblHistoricoPadraoID, dsoSup01.recordset['HistoricoPadraoID'].value);
		else if (cmbID == 'selMoedaID')
		    setLabelOfControl(lblMoedaID, dsoSup01.recordset['MoedaID'].value);
			
		if (dsoSup01.recordset['LancamentoConciliado'].value == true)
			chkLancamentoConciliado.checked = true;
		else
			chkLancamentoConciliado.checked = false;
	}
    else
	{
		if (cmbID == 'selTipoLancamentoID')
	        setLabelOfControl(lblTipoLancamentoID, selTipoLancamentoID.value);
		else if (cmbID == 'selHistoricoPadraoID')
		    setLabelOfControl(lblHistoricoPadraoID, selHistoricoPadraoID.value);
		else if (cmbID == 'selMoedaID')
			setLabelOfControl(lblMoedaID, selMoedaID.value);
	}
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selHistoricoPadraoID)
    var nEmpresaID = getCurrEmpresaData();
    var nTipoLancamentoID = selTipoLancamentoID.value;

    if ( isNaN(parseInt(nTipoLancamentoID, 10)) )
		nTipoLancamentoID = 0;

    glb_CounterCmbsDynamics = 1;
    
    // parametrizacao do dso dsoCmbDynamic01 (designado para selHistoricoPadraoID)
    setConnection(dsoCmbDynamic01);
    
	if (glb_bAlteracao)
	{
		lockInterface(true);

		dsoCmbDynamic01.SQL = 'SELECT a.HistoricoPadraoID as fldID, a.HistoricoPadrao as fldName, ' +
		                      'ISNULL(a.HistoricoComplementar, SPACE(0)) AS HistoricoComplementar ' +
		                      'FROM HistoricosPadrao a WITH(NOLOCK) ' +
		                      'WHERE (a.EstadoID = 2 AND ' +
								'a.HistoricoPadraoID IN ' +
								'(SELECT HistoricoPadraoID FROM HistoricosPadrao_Contas WITH(NOLOCK) ' +
								'WHERE (HistoricoPadraoID = a.HistoricoPadraoID AND TipoLancamentoID = ' + nTipoLancamentoID + '))) ' +
							  'ORDER BY a.HistoricoPadrao';
	}
	else
	{
		dsoCmbDynamic01.SQL = 'SELECT HistoricoPadraoID as fldID,HistoricoPadrao as fldName, ' +
		                      'ISNULL(HistoricoComplementar, SPACE(0)) AS HistoricoComplementar ' +
		                      'FROM HistoricosPadrao WITH(NOLOCK) ' +
		                      'WHERE (HistoricoPadraoID = ' +  dsoSup01.recordset['HistoricoPadraoID'].value + ') ' +
		                      'ORDER BY HistoricoPadrao';
	}

	dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();
}

/********************************************************************
Funcao disparada pelo programado.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC()
{
    var optionStr, optionValue;
    var aCmbsDynamics = [selHistoricoPadraoID];
    var aDSOsDynamics = [dsoCmbDynamic01];
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var i;
    var lFirstRecord;
    var nQtdCmbs = 1;
    
    // Inicia o carregamento de combos dinamicos (selHistoricoPadraoID)
    //
    clearComboEx(['selHistoricoPadraoID']);
    
    glb_CounterCmbsDynamics --;
    
    if (glb_CounterCmbsDynamics == 0)
    {
        for (i=0; i<nQtdCmbs; i++)
        {
            oldDataSrc = aCmbsDynamics[i].dataSrc;
            oldDataFld = aCmbsDynamics[i].dataFld;
            aCmbsDynamics[i].dataSrc = '';
            aCmbsDynamics[i].dataFld = '';
            lFirstRecord = true;
            while (! aDSOsDynamics[i].recordset.EOF )
            {
                optionStr = aDSOsDynamics[i].recordset['fldName'].value;
                optionValue = aDSOsDynamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                oOption.setAttribute('HistoricoComplementar', aDSOsDynamics[i].recordset['HistoricoComplementar'].value, 1);
                aCmbsDynamics[i].add(oOption);
                aDSOsDynamics[i].recordset.MoveNext();
                lFirstRecord = false;
            }
            aCmbsDynamics[i].dataSrc = oldDataSrc;
            aCmbsDynamics[i].dataFld = oldDataFld;
        }
        
		adjustLabelsCombos('selHistoricoPadraoID', true);

		if (!glb_bAlteracao)
		{		
			glb_bAlteracao = false;
			// volta para a automacao
			finalOfSupCascade(glb_BtnFromFramWork);
		}
		else
		{
			glb_bAlteracao = false;
			lockInterface(false);
			if ( selHistoricoPadraoID.options.length > 0 )
			{
				selHistoricoPadraoID.disabled = false;
				window.focus();
				selHistoricoPadraoID.focus();
			}
			if ( selHistoricoPadraoID.options.length == 1 )
			{
			    selHistoricoPadraoID.selectedIndex = 0;
			    dsoSup01.recordset['HistoricoPadraoID'].value = selHistoricoPadraoID.options(0).value;
			}
			
			if (glb_bBtnClicked)
			{
				glb_bBtnClicked = false;
				glb_nTimerCallAutomation = window.setInterval('callAutomation()', 20, 'JavaScript');
				return null;
			}
		}
    }
	return null;
}

function callAutomation()
{
    if ( glb_nTimerCallAutomation != null )
    {
        window.clearInterval(glb_nTimerCallAutomation);
        glb_nTimerCallAutomation = null;
    }

	// Chamadas de automacao
	if (glb_sLastBtn == 'SUPINCL')
		;//lockAndOrSvrSup_SYS();
	else if (glb_sLastBtn == 'SUPALT')
		editRegister();
		
	glb_sLastBtn = '';
}

/********************************************************************
Funcao disparada pelo programado.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function fillHistoricoComplementar()
{
	var sHistoricoComplementar = selHistoricoPadraoID.options.item(selHistoricoPadraoID.selectedIndex).getAttribute('HistoricoComplementar', 1);
	txtHistoricoComplementar.value = sHistoricoComplementar;
}

/********************************************************************
Verificacoes do Lancamento
********************************************************************/
function verifyLancamentoInServer(currEstadoID, newEstadoID)
{
    var nLancamentoID = dsoSup01.recordset['LancamentoID'].value;
    var strPars = new String();

    strPars = '?nLancamentoID='+escape(nLancamentoID);
    strPars += '&nEstadoDeID='+escape(currEstadoID);
    strPars += '&nEstadoParaID='+escape(newEstadoID);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/lancamentos/serverside/verificacaolancamento.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyLancamentoInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes do Lancamento
********************************************************************/
function verifyLancamentoInServer_DSC()
{
    if ((dsoVerificacao.recordset.Fields['Verificacao'].value == null) || (dsoVerificacao.recordset.Fields['Verificacao'].value == -1))
    {
        stateMachSupExec('OK');
    }    
    else
    {
        if ( window.top.overflyGen.Alert(dsoVerificacao.recordset.Fields['Verificacao'].value) == 0 )
            return null;
        stateMachSupExec('CANC');
    }
}
