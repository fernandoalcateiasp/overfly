<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="planocontassup01Html" name="planocontassup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
   
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
        
  
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/planocontas/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/planocontas/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="planocontassup01Body" name="planocontassup01Body" LANGUAGE="javascript" onload="return window_onload()">
   <!-- //@@ Os dsos sao definidos de acordo com o form -->
        
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p id="lblConta" name="lblConta" class="lblGeneral">Conta</p>
        <input type="text" id="txtConta" name="txtConta" DATASRC="#dsoSup01" DATAFLD="Conta" class="fldGeneral"></input>
		<p id="lblTipoContaID" name="lblTipoContaID" class="lblGeneral">Tipo</p>
		<select id="selTipoContaID" name="selTipoContaID" DATASRC="#dsoSup01" DATAFLD="TipoContaID" class="fldGeneral"></select>
        <p id="lblNivel" name="lblNivel" class="lblGeneral">N�vel</p>
        <input type="text" id="txtNivel" name="txtNivel" DATASRC="#dsoSup01" DATAFLD="Nivel" class="fldGeneral"></input>
		<p id="lblContaMaeID" name="lblContaMaeID" class="lblGeneral">Conta M�e</p>
		<select id="selContaMaeID" name="selContaMaeID" DATASRC="#dsoSup01" DATAFLD="ContaMaeID" class="fldGeneral"></select>
        <p id="lblContaAnterior" name="lblContaAnterior" class="lblGeneral">Conta Anterior</p>
        <input type="text" id="txtContaAnterior" name="txtContaAnterior" DATASRC="#dsoSup01" DATAFLD="ContaAnterior" class="fldGeneral"></input>
		<p id="lblUsoEspecialID" name="lblUsoEspecialID" class="lblGeneral">Uso Especial</p>
		<select id="selUsoEspecialID" name="selUsoEspecialID" DATASRC="#dsoSup01" DATAFLD="UsoEspecialID" class="fldGeneral"></select>
		<p id="lblTipoDetalhamentoID" name="lblTipoDetalhamentoID" class="lblGeneral">Detalhamento</p>
		<select id="selTipoDetalhamentoID" name="selTipoDetalhamentoID" DATASRC="#dsoSup01" DATAFLD="TipoDetalhamentoID" class="fldGeneral"></select>
		<p  id="lblDetalha" name="lblDetalha" class="lblGeneral">DC</p>
        <input type="checkbox" id="chkDetalha" name="chkDetalha"  DATASRC="#dsoSup01" DATAFLD="Detalha" class="fldGeneral" title="Detalha a conta em lan�amentos autom�ticos?"></input>
		<p  id="lblDetalhamentoObrigatorio" name="lblDetalhamentoObrigatorio" class="lblGeneral">DO</p>
        <input type="checkbox" id="chkDetalhamentoObrigatorio" name="chkDetalhamentoObrigatorio"  DATASRC="#dsoSup01" DATAFLD="DetalhamentoObrigatorio" class="fldGeneral" title="O detalhamento � obrigat�rio"></input>
        <p  id="lblAceitaLancamento" name="lblAceitaLancamento" class="lblGeneral">AL</p>
        <input type="checkbox" id="chkAceitaLancamento" name="chkAceitaLancamento" class="fldGeneral" title="Conta aceita lan�amento ou � totalizadora?"></input>
        <p  id="lblLancamentoManual" name="lblLancamentoManual" class="lblGeneral">LM</p>
        <input type="checkbox" id="chkLancamentoManual" name="chkLancamentoManual" DATASRC="#dsoSup01" DATAFLD="LancamentoManual" class="fldGeneral" title="Conta aceita lan�amento manual?"></input>
        <p  id="lblSaldoDevedor" name="lblSaldoDevedor" class="lblGeneral">SD</p>
        <input type="checkbox" id="chkSaldoDevedor" name="chkSaldoDevedor" DATASRC="#dsoSup01" DATAFLD="SaldoDevedor" class="fldGeneral" title="Conta de natureza devedora?"></input>
        <p  id="lblSaldoCredor" name="lblSaldoCredor" class="lblGeneral">SC</p>
        <input type="checkbox" id="chkSaldoCredor" name="chkSaldoCredor" DATASRC="#dsoSup01" DATAFLD="SaldoCredor" class="fldGeneral" title="Conta de natureza credora?"></input>
        <p  id="lblUsoRestrito" name="lblUsoRestrito" class="lblGeneral">UR</p>
        <input type="checkbox" id="chkUsoRestrito" name="chkUsoRestrito" DATASRC="#dsoSup01" DATAFLD="UsoRestrito" class="fldGeneral" title="Conta de uso restrito?"></input>
        <p  id="lblUSA" name="lblUSA" class="lblGeneral">USA</p>
        <input type="text" id="txtUSA" name="txtUSA" DATASRC="#dsoSup01" DATAFLD="USA" class="fldGeneral"></input>
        <p  id="lblTemOrcamento" name="lblTemOrcamento" class="lblGeneral">Orc</p>
        <input type="checkbox" id="chkTemOrcamento" name="chkTemOrcamento" DATASRC="#dsoSup01" DATAFLD="TemOrcamento" class="fldGeneral" title="Conta tem or�amento?"></input>
		<p id="lblTipoVariacaoID" name="lblTipoVariacaoID" class="lblGeneral">Varia��o</p>
		<select id="selTipoVariacaoID" name="selTipoVariacaoID" DATASRC="#dsoSup01" DATAFLD="TipoVariacaoID" class="fldGeneral"></select>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
    </div>
    
</body>

</html>
