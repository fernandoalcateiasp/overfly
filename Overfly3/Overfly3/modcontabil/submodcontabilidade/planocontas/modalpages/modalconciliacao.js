/********************************************************************
modalconciliacao.js

Library javascript para modalconciliacao.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************

var modPesqRel_TimerHandler = null;
var glb_aEmpresaData = getCurrEmpresaData();
var glb_nRow = 0;
var glb_nTotalRows = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_bFirstFill = false;

// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_refreshGridInterval = null;
var glb_Row;
var glb_sReporError;
var glb_geraExcel = null;
var glb_ContExcel = 0;

var dsoGrid = new CDatatransport("dsoGrid");
var dsoGridErro = new CDatatransport("dsoGridErro");
var dsoDataPesquisa = new CDatatransport("dsoDataPesquisa");
var dsoEmpresa = new CDatatransport("dsoEmpresa");
var dsoTipo = new CDatatransport("dsoTipo");
var dsoTipoCusto = new CDatatransport("dsoTipoCusto");
var dsoCombos = new CDatatransport("dsoCombos");
var dsoCorrecao = new CDatatransport("dsoCorrecao");
var dsoCorretor = new CDatatransport("dsoCorretor");
var dsoTipoErro = new CDatatransport("dsoTipoErro");
var dsoTipoErroCombo = new CDatatransport("dsoTipoErroCombo");
var dsoGrava = new CDatatransport("dsoGrava");

var glbCombo = null;
var glb_bResumo = true;
var glb_sTipoRegistro = '';
var glb_bautomatico = 1;
var glb_nIndexEmpresa;
var glb_nDSO = 0;
var glb_duplicaGridRows = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // ajusta o body do html
    with (modalconciliacaoBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    //
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 49;
    modalFrame.style.left = 0;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    // texto da secao01
    secText('Concilia��o Cont�bil', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 1;
    var grid_height = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // ajusta o divPesquisa
    with (divPesquisa.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        height = 90;
    }

    adjustElementsInForm([['lblInicio', 'txtInicio', 10, 1, -13, -10],
    	                    ['lblFim', 'txtFim', 10, 1, -2],
                            /*['lblIndice', 'chkIndice', 3, 1, -1],*/
                            ['lblTipoErro', 'selTipoErro', 6, 1, -2],
                            ['lblEmpresaID', 'selEmpresaID', 18, 1, -2],
                            ['btnListar', 'btn', btn_width, 1, 5],
                            ['lblDetalhes', 'chkDetalhes', 3, 1, -2],
                            ['btnGravar', 'btn', btn_width, 1, -2],
                            ['lblCorretor', 'selCorretor', 8, 1, -2],
                            ['lblCorrecao', 'selCorrecao', 25, 1, -2],
                            ['btnRegOrig', 'btn', btn_width, 1, 3],
                            ['btnExcel', 'btn', btn_width, 1, 3],
                            ['lblBusca', 'txtBusca', 28, 2, -13],
                            ['lblRegistro', 'txtRegistro', 15, 2]], null, null, true);

    // txtInicio.onkeypress = verifyDateTimeNotLinked;
    txtInicio.onkeypress = txtData_onkeypress;
    txtInicio.setAttribute('verifyNumPaste', 1);
    txtInicio.setAttribute('thePrecision', 10, 1);
    txtInicio.setAttribute('theScale', 0, 1);
    txtInicio.setAttribute('minMax', new Array(0, 999999999), 1);
    txtInicio.onfocus = selFieldContent;
    txtInicio.maxLength = 10;

    txtBusca.onkeypress = txtData_onkeypress2;

    //txtFim.onkeypress = verifyDateTimeNotLinked;
    txtFim.onkeypress = txtData_onkeypress;
    txtFim.setAttribute('verifyNumPaste', 1);
    txtFim.setAttribute('thePrecision', 10, 1);
    txtFim.setAttribute('theScale', 0, 1);
    txtFim.setAttribute('minMax', new Array(0, 999999999), 1);
    txtFim.onfocus = selFieldContent;
    txtFim.maxLength = 10;

    // chkIndice.onclick = chkIndice_onclick;

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP + 88;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = parseInt(btnOK.currentStyle.top, 10) + 10 - (ELEM_GAP * 13); // -parseInt(top, 10)
    }

    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // ajusta o divFG
    with (divFGErros.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = (parseInt(divFG.currentStyle.height, 10) / 2) + ELEM_GAP + 150;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = 150; //parseInt(btnOK.currentStyle.top, 10) + 10 - (ELEM_GAP * 20); // -parseInt(top, 10)
    }

    with (fgErros.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFGErros.style.width, 10);
        height = parseInt(divFGErros.style.height, 10);
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    btnRegOrig.style.visibility = 'hidden';
    btnExcel.style.visibility = 'hidden';
    lblBusca.style.visibility = 'hidden';
    txtBusca.style.visibility = 'hidden';
    lblRegistro.style.visibility = 'hidden';
    txtRegistro.style.visibility = 'hidden';
    txtRegistro.readOnly = true;
    lblDetalhes.style.visibility = 'hidden';
    chkDetalhes.style.visibility = 'hidden';
    btnGravar.style.visibility = 'hidden';
    lblCorretor.style.visibility = 'hidden';
    selCorretor.style.visibility = 'hidden';
    lblCorrecao.style.visibility = 'hidden';
    selCorrecao.style.visibility = 'hidden';
    divFGErros.style.visibility = 'hidden';
    //selTipoErro.disabled = true;

    chkDetalhes.onclick = chkDetalhes_onclick;
    
    startGridInterface(fg);

    headerGrid(fg, ['Tipo Registro',
                    'Registros',
                    'Diverg�ncias',
                    '%'], []);

    fg.Redraw = 2;

    startGridInterface(fgErros);

    headerGrid(fgErros, ['Erros'], []);

    fgErros.Redraw = 2;

    DataPesquisa();

    preencherEmpresa();    

    // preencherCorrecao();

    if (glb_bautomatico == 0) {
        fillGrid();
    }

    preencherTipoErro();
}

function txtData_onkeypress()
{
    if (event.keyCode == 13)
        btnListar_onclick(this);
}

function txtData_onkeypress2() {
    if (event.keyCode == 13)
        fillGrid();
}



function js_modalconciliacao_BeforeEdit(grid, Row, Col)
{
    if (glb_Row != Row)
        fgErros.Rows = 1;

    glb_Row = Row;
}

function js_modalconciliacao_AfterEdit(grid, Row, Col)
{
    fg.TextMatrix(Row, getColIndexByColKey(fg, 'Gravar')) = 1;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('OK_CALLFORM_PL'), null);
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_PL'), null);
    }
}

function fillGrid()
{
    if (glb_refreshGridInterval != null)
    {
        window.clearInterval(glb_refreshGridInterval);
        glb_refreshGridInterval = null;
    }

    var dtInicio = (txtInicio.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtInicio.value, true) + '\'');
    var dtFim = (txtFim.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtFim.value, true) + '\'');
    var nEmpresaID = selEmpresaID.value;
    var nTipoErro = selTipoErro.value;
    var sCorretor = selCorretor.value;
    var sCorrecao = selCorrecao.value;
    var sWhereData = '';
    glb_nDSO = 3;

    //Default
    if (nTipoErro == '')
        nTipoErro = -2;

    if ((dtInicio != 'NULL') && (dtFim != 'NULL'))
        sWhereData = ((glb_bResumo) ? 'WHERE ' : 'AND ') + '(dtBalancete BETWEEN ' + dtInicio + ' AND ' + dtFim + ') ';
    else if (dtInicio != 'NULL')
        sWhereData = ((glb_bResumo) ? 'WHERE ' : 'AND ') + '(dtBalancete >= ' + dtInicio + ') ';
    else if (dtFim != 'NULL')
        sWhereData = ((glb_bResumo) ? 'WHERE ' : 'AND ') + '(dtBalancete <= ' + dtFim + ') ';

    if (!glb_bResumo)
    {
        if (sCorretor != 0)
            sWhereData += 'AND (Corretor = \'' + sCorretor + '\') ';

        if (txtBusca.value != '')
            sWhereData += 'AND ' +
                          '(ISNULL(CONVERT(VARCHAR(100),TipoRegistro),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Contexto),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Indice),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Numero),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),PessoaID),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Pessoa),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),dtDocumento),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),dtBalancete),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Tipo),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Valor),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),IntegracaoID),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Fonte),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),ContadorID),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Automatico),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Erro),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Inconsistencias),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Corretor),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Correcao),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),EstadoID),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),dtVencimento),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Layout),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),ContaBancaria),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Impostos),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Retencoes),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Itens),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),Parcelas),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),CodigoOperacao),SPACE(0)) + SPACE(1) + ' +
                          'ISNULL(CONVERT(VARCHAR(100),ChaveAcesso),SPACE(0)) LIKE \'%' + txtBusca.value + '%\')';

        if (sCorrecao != 0)
            sWhereData += 'AND (Correcao = \'' + sCorrecao + '\') ';
    }

    if (selEmpresaID.selectedIndex >= 1)
    {
        if (sWhereData.length > 0)
            sWhereData += (nEmpresaID.length > 0 ? ' AND (EmpresaID = ' + nEmpresaID + ') ' : '');
        else
            sWhereData += (nEmpresaID.length > 0 ? ' WHERE (EmpresaID = ' + nEmpresaID + ') ' : '');
    }

    /*
    if (chkIndice.checked)
    {
        if (sWhereData.length > 0)
            sWhereData += ' AND (a.Inconsistencias = ' + '\'' + '|Indice|' + '\'' + ') ';
        else
            sWhereData += ' WHERE (a.Inconsistencias = ' + '\'' + '|Indice|' + '\'' + ') ';
    }
    */

    if ((nTipoErro >= 0) && (!glb_bResumo))
    {
        if ((sWhereData.length > 0) || (!glb_bResumo))
            sWhereData += (nTipoErro.length > 0 ? ' AND (TipoErro = ' + nTipoErro + ') ' : '');
        else
            sWhereData += (nTipoErro.length > 0 ? ' WHERE (TipoErro = ' + nTipoErro + ') ' : '');
    }
    if ((nTipoErro == -1) && (!glb_bResumo))
    {
        if ((sWhereData.length > 0) || (!glb_bResumo))
            sWhereData += (nTipoErro.length > 0 ? ' AND (TipoErro <> 0) ' : '');
        else
            sWhereData += (nTipoErro.length > 0 ? ' WHERE (TipoErro <> 0) ' : '');
    }

    lockControlsInModalWin(true);

    setConnection(dsoGrid);

    if (glb_bResumo)
    {
        dsoGrid.SQL = 'DECLARE @Resumo TABLE (TipoRegistro VARCHAR(50), IntegracaoID INT, Contexto VARCHAR(30), Indice INT, Numero VARCHAR(30), Inconsistencias VARCHAR(MAX), TipoErro INT) ' +
                        'INSERT INTO @Resumo ' +
                            'SELECT DISTINCT TipoRegistro, ISNULL(ISNULL(Indice, IntegracaoID), ContadorID), Contexto, Indice, Numero, Inconsistencias, TipoErro ' +
                                'FROM _ContabilidadeConciliacao a WITH(NOLOCK) ' +
                                 sWhereData + ' ' +

                        'SELECT TipoRegistro, COUNT(1) AS Registros, ' +
                                '(CASE WHEN (' + nTipoErro + ' = -2) THEN SUM((CASE WHEN (Inconsistencias IS NOT NULL) THEN 1 ELSE 0 END)) ' +
                                    'WHEN (' + nTipoErro + ' = -1) THEN SUM((CASE WHEN ((Inconsistencias IS NOT NULL) AND (TipoErro <> 0)) THEN 1 ELSE 0 END)) ' +
                                    'ELSE SUM((CASE WHEN ((Inconsistencias IS NOT NULL) AND (TipoErro = ' + nTipoErro + ')) THEN 1 ELSE 0 END)) END) AS Divergencia, ' +
                                'CONVERT(NUMERIC(11, 2), (((CASE WHEN (' + nTipoErro + ' = -2) THEN SUM((CASE WHEN (Inconsistencias IS NOT NULL) THEN 1 ELSE 0 END)) ' +
                                    'WHEN (' + nTipoErro + ' = -1) THEN SUM((CASE WHEN ((Inconsistencias IS NOT NULL) AND (TipoErro <> 0)) THEN 1 ELSE 0 END)) ' +
                                    'ELSE SUM((CASE WHEN ((Inconsistencias IS NOT NULL) AND (TipoErro = ' + nTipoErro + ')) THEN 1 ELSE 0 END)) END) / ' +
                                        'CONVERT(NUMERIC(11, 2), COUNT(1))) * 100)) AS Percentual ' +
                        'FROM @Resumo ' +
                        'GROUP BY TipoRegistro ';
    }
    else
    {
        dsoGrid.SQL = 'DECLARE @Conciliacao TABLE (Empresa VARCHAR(20), Contexto VARCHAR(30), Indice INT, Numero VARCHAR(30), Pessoa VARCHAR(40), dtBalancete DATETIME, Tipo VARCHAR(60), Valor NUMERIC(11, 2), ' +
                        'IntegracaoID INT, Fonte VARCHAR(10), ContadorID INT, Automatico BIT, Inconsistencias VARCHAR(MAX), Corretor VARCHAR(10), Correcao VARCHAR(80), Estado VARCHAR(40), ' +
                        'dtVencimento DATETIME, Layout VARCHAR(20), ContaBancaria VARCHAR(30), Impostos NUMERIC(11, 2), Retencoes NUMERIC(11, 2), Itens INT, Parcelas INT, CodigoOperacao VARCHAR(10), ' +
                        'ChaveAcesso VARCHAR(44), Erro BIT, PermiteEditar BIT, Gravar BIT, Erros BIT, ConciliacaoID INT, TipoErro INT) ' +

        'INSERT INTO @Conciliacao ' +
            'SELECT dbo.fn_Pessoa_Fantasia(a.EmpresaID, 0) AS Empresa, a.Contexto, a.Indice, a.Numero, a.Pessoa, a.dtBalancete, a.Tipo, ' +
                    'a.Valor, a.IntegracaoID, a.Fonte, a.ContadorID, a.Automatico, a.Inconsistencias, a.Corretor, a.Correcao, dbo.fn_Recursos_Campos(a.EstadoID, 1) AS Estado, ' +
                    'a.dtVencimento, a.Layout, a.ContaBancaria, a.Impostos, a.Retencoes, a.Itens, a.Parcelas, a.CodigoOperacao, a.ChaveAcesso, a.Erro, ' + 
                    '(CASE WHEN (dbo.fn_Conciliacao_Correcao(a.Fonte, a.Automatico, a.IntegracaoID, a.Inconsistencias, NULL, 1) IS NULL) THEN 1 ELSE 0 END) AS PermiteEditar, 0 AS Gravar, ' +
                    '(SELECT COUNT(1) ' +
                        'FROM _ContabilidadeConciliacao_Erros aa WITH(NOLOCK) ' +
                        'WHERE ((aa.TipoRegistro = a.TipoRegistro) AND (aa.IntegracaoID = a.IntegracaoID))) AS Erros, a.ConciliacaoID, a.TipoErro ' +
                'FROM _ContabilidadeConciliacao a WITH(NOLOCK) ' +
                'WHERE ' + (nEmpresaID > 0 ? 'a.EmpresaID = ' + nEmpresaID + ' AND' : '') + ' (a.Erro = 1) AND (a.TipoRegistro = \'' + glb_sTipoRegistro + '\') ' + sWhereData +

        'INSERT INTO @Conciliacao ' +
            'SELECT dbo.fn_Pessoa_Fantasia(a.EmpresaID, 0) AS Empresa, a.Contexto, a.Indice, a.Numero, a.Pessoa, a.dtBalancete, a.Tipo, ' +
                    'a.Valor, a.IntegracaoID, a.Fonte, a.ContadorID, a.Automatico, a.Inconsistencias, a.Corretor, a.Correcao, dbo.fn_Recursos_Campos(a.EstadoID, 1) AS Estado, ' +
                    'a.dtVencimento, a.Layout, a.ContaBancaria, a.Impostos, a.Retencoes, a.Itens, a.Parcelas, a.CodigoOperacao, a.ChaveAcesso, a.Erro, ' +
                    '(CASE WHEN (dbo.fn_Conciliacao_Correcao(a.Fonte, a.Automatico, a.IntegracaoID, a.Inconsistencias, NULL, 1) IS NULL) THEN 1 ELSE 0 END) AS PermiteEditar, 0 AS Gravar, ' +
                    '(SELECT COUNT(1) ' +
                        'FROM _ContabilidadeConciliacao_Erros aa WITH(NOLOCK) ' +
                        'WHERE ((aa.TipoRegistro = a.TipoRegistro) AND (aa.IntegracaoID = a.IntegracaoID))) AS Erros, a.ConciliacaoID, a.TipoErro ' +
                'FROM _ContabilidadeConciliacao a WITH(NOLOCK) ' +
                    'INNER JOIN @Conciliacao b ON (b.Indice = a.Indice) ' +
                'WHERE ((SELECT COUNT(1) FROM @Conciliacao aa WHERE (aa.ConciliacaoID = a.ConciliacaoID)) = 0) ' +

        'SELECT a.* ' +
            'FROM @Conciliacao a ' +
            'ORDER BY (CASE ' +
                        'WHEN ((a.Fonte = \'Overfly\') OR (a.Indice IS NULL)) THEN a.Empresa ' +
                        'ELSE (SELECT aa.Empresa ' +
                                'FROM @Conciliacao aa ' +
                                'WHERE (aa.Indice = a.Indice) AND (aa.Fonte <> a.Fonte)) END), ' +
                        'a.Contexto, ISNULL(a.Indice, 99999), a.Fonte, a.dtBalancete ASC, a.Indice ASC';
    }

    dsoGrid.ondatasetcomplete = fillGrid_DSC;
    dsoGrid.refresh();

    dsoCorretor.SQL = 'SELECT SPACE(0) AS CorretorID, SPACE(0) AS Corretor ' +
        'UNION SELECT ' + '\'' + 'Empresa' + '\'' + ' AS CorretorID, ' + '\'' + 'Empresa' + '\'' + ' AS Corretor ' +
        'UNION SELECT ' + '\'' + 'Contador' + '\'' + ' AS CorretorID, ' + '\'' + 'Contador' + '\'' + ' AS Corretor';
    dsoCorretor.ondatasetcomplete = fillGrid_DSC;
    dsoCorretor.refresh();

    dsoTipoErroCombo.SQL = 'SELECT DISTINCT TipoErro ' +
                                'FROM _ContabilidadeConciliacao WITH(NOLOCK) ' +
                                'ORDER BY TipoErro ASC';

    dsoTipoErroCombo.ondatasetcomplete = fillGrid_DSC;
    dsoTipoErroCombo.Refresh();
}

function fillGrid_DSC() 
{

    glb_nDSO--;

    if (glb_nDSO > 0)
        return;

    if (glb_sTipoRegistro == 'Nota Fiscal')
        aHidenCols = (selEmpresaID.value > 0 ? [0, 17, 19, 26, 27, 28, 29, 30] : [17,  19, 26, 27, 28, 29, 30]);
    else if (glb_sTipoRegistro == 'CTE')
        aHidenCols = (selEmpresaID.value > 0 ? [0, 17, 18, 19, 21, 22, 23, 26, 27, 28, 29, 30] : [17, 18, 19, 21, 22, 23, 26, 27, 28, 29, 30]);
    else if (glb_sTipoRegistro == 'Baixas')
        aHidenCols = (selEmpresaID.value > 0 ? [0, 3, 4, 16, 17, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30] : [3, 4, 16, 17, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]);
    else
        aHidenCols = (selEmpresaID.value > 0 ? [0, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30] : [20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]);

    startGridInterface(fg);
    fg.FrozenCols = 0;
    fg.FontSize = '8';

    if (glb_bResumo)
    {
        headerGrid(fg, ['Tipo Registro',
                        'Registros',
                        'Inconsist�ncias',
                        '%'], []);

        fillGridMask(fg, dsoGrid, ['TipoRegistro',
                                   'Registros',
                                   'Divergencia',
                                   'Percentual'],
                                   ['99999999999', '99999999999', '99999999999', '999999999.99'],
                                   ['###,###,###', '###,###,###', '###,###,###', '###,###,##0.00']);

        alignColsInGrid(fg, [1, 2, 3]);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1, '#########', 'S'],
                                                              [2, '#########', 'S']]);


        if (fg.ValueMatrix(1, 1) > 0)
            fg.TextMatrix(1, 3) = ((fg.ValueMatrix(1, 2) / fg.ValueMatrix(1, 1)) * 100);
        else
            fg.TextMatrix(1, 3) = 0;


        fg.Editable = true;
    }
    else
    {
        headerGrid(fg, ['Empresa', // 0
                        'Contexto', // 1
                        'Indice', // 2
                        'Numero', // 3
                        'Pessoa', // 4
                        'Data ', // 5
                        'Tipo', // 6
                        'Valor', // 7
                        'IntegracaoID', // 8
                        'Fonte', // 9
                        'ContadorID', // 10
                        'Auto', // 11
                        'Inconsist�ncias', // 12
                        'Erro', // 13
                        'Corretor', // 14
                        'Corre��o', // 15
                        'Estado', // 16
                        'Vencimento', // 17
                        'Layout', // 18
                        'Conta Bancaria', // 19
                        'Impostos', // 20
                        'Reten��es', // 21
                        'Itens', // 22
                        'Parcelas', // 23
                        'Codigo Opera��o', // 24
                        'Chave Acesso', // 25
                        'Erro', // 26
                        'PermiteEditar', // 27
                        'Gravar', // 28
                        'Erros', // 29
                        'ConciliacaoID'], aHidenCols); // 30

        fillGridMask(fg, dsoGrid, ['Empresa*',
                                   'Contexto*',
                                   'Indice*',
                                   'Numero*',
                                   'Pessoa*',
                                   'dtBalancete*',
                                   'Tipo*',
                                   'Valor*',
                                   'IntegracaoID*',
                                   'Fonte*',
                                   'ContadorID*',
                                   'Automatico*',
                                   'Inconsistencias*',
                                   'TipoErro',
                                   'Corretor',
                                   'Correcao',
                                   'Estado*',
                                   'dtVencimento*',
                                   'Layout*',
                                   'ContaBancaria*',
                                   'Impostos*',
                                   'Retencoes*',
                                   'Itens*',
                                   'Parcelas*',
                                   'CodigoOperacao*',
                                   'ChaveAcesso*',
                                   'Erro',
                                   'PermiteEditar',
                                   'Gravar',
                                   'Erros',
                                   'ConciliacaoID'],
                                   ['', '', '', '', '', '', '', '999999999.99', '', '', '', '', '', '', '', '', '', '', '', '', '999999999.99', '999999999.99', '', '', '', '', '', '', '', '', ''],
                                   ['', '', '', '', '', '', '', '###,###,##0.00', '', '', '', '', '', '', '', '', '', '', '', '', '###,###,##0.00', '###,###,##0.00', '', '', '', '', '', '', '', '', '']);

        insertcomboData(fg, getColIndexByColKey(fg, 'Corretor'), dsoCorretor, 'CorretorID', 'Corretor');

        insertcomboData(fg, getColIndexByColKey(fg, 'TipoErro'), dsoTipoErroCombo, 'TipoErro', 'TipoErro');

        glb_aCelHint = [[0, 11, 'Registro gerado automaticamente por EDI ou digitado/alterado manualmente']];

        alignColsInGrid(fg, [2, 7, 20, 21, 22, 23]);

        fg.Editable = true;

        fg.FrozenCols = 3;

        // Merge das celulas
        fg.MergeCells = 4;
        fg.MergeCol(0) = true;
        fg.MergeCol(1) = true;
        fg.MergeCol(2) = true;
        fg.MergeCol(3) = true;
        fg.MergeCol(4) = true;
        fg.MergeCol(5) = true;
        fg.MergeCol(6) = true;
        fg.MergeCol(7) = true;
        fg.MergeCol(8) = true;
        fg.MergeCol(9) = true;
        fg.MergeCol(10) = true;
        fg.MergeCol(11) = true;
        fg.MergeCol(12) = true;

        fg.Col = getColIndexByColKey(fg, 'Correcao');

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true,
                    [
                        [getColIndexByColKey(fg, 'Indice*'), '######', 'C']
                    ], true);

        if (fg.Rows > 1)
            fg.TextMatrix(1, getColIndexByColKey(fg, 'Indice*')) = fg.Rows - 2;

        pintaCelulas();
    }

    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);

    if (!glb_bResumo)
    {
        if (glb_sTipoRegistro == "Nota Fiscal")
            fg.ColWidth(getColIndexByColKey(fg, 'Tipo*')) = 2500;

        fg.ColWidth(getColIndexByColKey(fg, 'Corretor')) = 1250;
        // fg.ColWidth(getColIndexByColKey(fg, 'Correcao')) = 2200;
    }

    fg.Redraw = 2;

    fg.ExplorerBar = 5;

    fg.Redraw = 2;

    // fg.Editable = false;

    lockControlsInModalWin(false);

    selEmpresaID.disabled = ((selEmpresaID.length <= 1) ? true : false);

    if (fg.Rows >= 3)
        fg.Row = 2;
}

function btnListar_onclick(ctl)
{
    //selEmpresaID.disabled = false;

    if (!glb_bResumo)
    {
        glb_bResumo = true;
        selCorretor.selectedIndex = -1;
        selCorrecao.selectedIndex = -1;
        txtInicio.disabled = false;
        txtFim.disabled = false;
        btnListar.innerText = 'Listar';

        btnRegOrig.style.visibility = 'hidden';
        btnExcel.style.visibility = 'hidden';
        lblDetalhes.style.visibility = 'hidden';
        chkDetalhes.style.visibility = 'hidden';
        btnGravar.style.visibility = 'hidden';
        lblCorretor.style.visibility = 'hidden';
        selCorretor.style.visibility = 'hidden';
        lblCorrecao.style.visibility = 'hidden';
        selCorrecao.style.visibility = 'hidden';
        chkDetalhes.checked = 0;

        ajustaGrids();

        
        glb_sTipoRegistro = '';
        secText('Concilia��o Cont�bil', 1);
        //preencherEmpresa();
        txtBusca.value = '';
        
        lblBusca.style.visibility = 'hidden';
        txtBusca.style.visibility = 'hidden';
        lblRegistro.style.visibility = 'hidden';
        txtRegistro.style.visibility = 'hidden';
        fillGrid();

        
    }
    else
    {
       

        glb_nIndexEmpresa = selEmpresaID.value;
        preencherEmpresa();

        fillGrid();

    }        
}

function btnRegOrig_onclick(ctl) {
    geraExcel(3);
}

function btnExcel_onclick(ctl)
{
    geraExcel(1);
}

function pintaCelulas()
{
    var i = 0;
    var y = 0;
    var sCampos = '';
    var bgColorYellow = 0x00ffff;
    var bgColorGray = 0XDCDCDC;
    var nVerdeClaro = 0xBCD9B0;
    var nColunaCorretor = getColIndexByColKey(fg, 'Corretor');
    var nColunaInconsistencias = getColIndexByColKey(fg, 'Inconsistencias');
    var nColunaFonte = getColIndexByColKey(fg, 'Fonte*');
    var nColunaIndice = getColIndexByColKey(fg, 'Indice*');

    for (i = 2; i < fg.Rows; i++)
    {
        sCampos = '';
        sCampos = fg.TextMatrix(i, getColIndexByColKey(fg, 'Inconsistencias'));

        for (y = 0; y < fg.Cols; y++)
        {
            // coluna escondida
            if (fg.ColHidden(y))
                continue;

            if (sCampos.toUpperCase().indexOf(replaceStr(fg.ColKey(y).toUpperCase(), '*', '')) >= 0)
                fg.Cell(6, i, y, i, y) = bgColorYellow;

            if (sCampos.toUpperCase().indexOf('EMPRESA') >= 0)
                fg.Cell(6, i, nColunaIndice, i, nColunaIndice) = nVerdeClaro;
        }

        if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'Fonte*')) == 'NC') && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Automatico*')) == 0))
            fg.Cell(6, i, nColunaFonte, i, nColunaFonte) = nVerdeClaro;

        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'PermiteEditar')) == 0)
            fg.Cell(6, i, nColunaCorretor, i, nColunaCorretor) = bgColorGray;

        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Erros')) != 0)
            fg.Cell(6, i, nColunaInconsistencias, i, nColunaInconsistencias) = bgColorYellow;


    }
}

function js_fg_modalconciliacaoDblClick(grid, Row, Col)
{
    if (glb_bResumo)
    {
        var nRegistros = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Divergencia'));

        if (nRegistros >= 500)
        {
            var sConfirm = window.top.overflyGen.Confirm('Isso pode demorar um pouco.\nVoc� pode reduzir o per�odo de tempo.\n\nDeseja continuar?');

            if ((sConfirm == 0) || (sConfirm == 2))
                return null;
        }

        glb_bResumo = false;
        txtInicio.disabled = true;
        txtFim.disabled = true;
        btnListar.innerText = 'Voltar';

        btnRegOrig.style.visibility = 'inherit';
        btnExcel.style.visibility = 'inherit';
        lblBusca.style.visibility = 'inherit';
        txtBusca.style.visibility = 'inherit';
        lblRegistro.style.visibility = 'inherit';
        txtRegistro.style.visibility = 'inherit';
        lblDetalhes.style.visibility = 'inherit';
        lblTipoErro.style.visibility = 'inherit';
        selTipoErro.style.visibility = 'inherit';
        chkDetalhes.style.visibility = 'inherit';
        btnGravar.style.visibility = 'inherit';
        lblCorretor.style.visibility = 'inherit';
        selCorretor.style.visibility = 'inherit';
        lblCorrecao.style.visibility = 'inherit';
        selCorrecao.style.visibility = 'inherit';

        glb_sTipoRegistro = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'TipoRegistro'));

        secText('Concilia��o Cont�bil - ' + glb_sTipoRegistro, 1);

        preencherCorrecao();

        //primeira linha do grid selecionado.
        glb_Row = 2;

        fillGrid();
    }
    else
    {
        if (fg.TextMatrix(glb_Row, getColIndexByColKey(fg, 'Fonte')) == 'Overfly')
        {
            if (!(chkDetalhes.checked))
            {
                chkDetalhes.checked = 1;
                ajustaGrids();
            }
            else
            {
                fillGridErro();
            }
        }
        else if ((fg.TextMatrix(glb_Row, getColIndexByColKey(fg, 'Fonte')) == 'NC') && (fg.TextMatrix(glb_Row, getColIndexByColKey(fg, 'IntegracaoID*')) != ''))
        {
            if (!(chkDetalhes.checked))
            {
                chkDetalhes.checked = 1;
                ajustaGrids();
            }
            else
            {
                fillGridLog();
            }
        }
    }
}

function js_BeforeRowColChangeModalConciliacao(grid, OldRow, OldCol, NewRow, NewCol, Cancel)
{
    //Forca celula read-only    
    if (!glb_GridIsBuilding) {
        if (glb_FreezeRolColChangeEvents)
            return true;

        if (currCellIsLocked(grid, NewRow, NewCol)) {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
    }
}

function currCellIsLocked(grid, nRow, nCol)
{
    var bRetVal = false;

    if (nRow <= 1)
        return true;

    if (getColIndexByColKey(grid, 'PermiteEditar') < 0)
        return bRetVal;

    bRetVal = ((grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'PermiteEditar')) == 0) && (nCol == getColIndexByColKey(grid, 'Corretor')));

    return bRetVal;
}

function geraExcel(nTipo)
{
    if (glb_geraExcel != null) {
        window.clearInterval(glb_geraExcel);
        glb_geraExcel = null;
    }
    
    lockControlsInModalWin(true);

    var sSQL;
    var sCamposEspecificos = '';
    var nTipoErro = selTipoErro.value;

    var dtInicio = (txtInicio.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtInicio.value, true) + '\'');
    var dtFim = (txtFim.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtFim.value, true) + '\'');
    var sCorretor = selCorretor.value;
    var sCorrecao = selCorrecao.value;
    var sWhereData = '';
    var geraExcelID = null;
    var integracaoID = null;

    glb_ContExcel += 1;

    //Default
    if (nTipoErro == '')
        nTipoErro = -2;

    if ((nTipo == 1)||(nTipo == 2)) {
        geraExcelID = 1;
        integracaoID = 0;
    }

    if (nTipo == 3) {
        geraExcelID = 3;
        glb_ContExcel = 0;
        integracaoID = fg.textmatrix(fg.row, getColIndexByColKey(fg, 'IntegracaoID'));
    }

    var formato = 2;
    var sLinguaLogada = getDicCurrLang();

    var frameReport = document.getElementById("frmReport");
    frameReport.onreadystatechange = reports_onreadystatechange;

    var strParameters = "RelatorioID=" + geraExcelID + "&Formato=" + formato + "&nTipo=" + nTipo + "&sLinguaLogada=" + sLinguaLogada + "&glb_sEmpresaFantasia=" + glb_aEmpresaData[3] +
                        "&dtInicio=" + dtInicio + "&dtFim=" + dtFim + "&sCorretor=" + sCorretor + "&sCorrecao=" + sCorrecao + "&nTipoErro=" + nTipoErro + "&glb_sTipoRegistro=" + glb_sTipoRegistro +
                        "&glb_bResumo=" + glb_bResumo + "&IntegracaoID=" + integracaoID;

    frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/planocontas/serverside/ReportsGrid_conciliacao.aspx?' + strParameters;
}

function DataPesquisa()
{
    if (glb_bautomatico)
    {
        setConnection(dsoDataPesquisa);

        var sWhere = (selEmpresaID.value > 0 ? ' WHERE EmpresaID = ' + selEmpresaID.value + ' ' : '');

        dsoDataPesquisa.SQL = 'SELECT CONVERT(VARCHAR, MIN(dtBalancete), 103) AS DtInicio, CONVERT(VARCHAR, MAX(dtBalancete), 103) AS DtFim ' +
                                'FROM _ContabilidadeConciliacao WITH(NOLOCK) ' +
                                 sWhere;

        dsoDataPesquisa.ondatasetcomplete = DataPesquisa_DSC;
        dsoDataPesquisa.refresh();
    }
    else
        fillGrid();
}

function DataPesquisa_DSC()
{
    if (glb_bautomatico)
    {
        txtInicio.value = dsoDataPesquisa.recordset['DtInicio'].value;
        txtFim.value = dsoDataPesquisa.recordset['DtFim'].value;
    }

    fillGrid();
}

function onchange_selEmpresaID()
{
    DataPesquisa();
}

function txtInicio_onkeydown()
{
    glb_bautomatico = 0;

}

function txtFim_onkeydown() {

    glb_bautomatico = 0;
}

function preencherEmpresa()
{
    var dtInicio = (txtInicio.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtInicio.value, true) + '\'');
    var dtFim = (txtFim.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtFim.value, true) + '\'');
    var nTipoErro = selTipoErro.value;
    var sWhereData = '';

    //Default
    if (nTipoErro == '')
        nTipoErro = -2;

    clearComboEx(['selEmpresaID']);

    selEmpresaID.disabled = true;

    if ((dtInicio != 'NULL') && (dtFim != 'NULL'))
        sWhereData = 'WHERE  (dtBalancete BETWEEN ' + dtInicio + ' AND ' + dtFim + ') ';
    else if (dtInicio != 'NULL')
        sWhereData = 'WHERE (dtBalancete >= ' + dtInicio + ') ';
    else if (dtFim != 'NULL')
        sWhereData = 'WHERE (dtBalancete <= ' + dtFim + ') ';

    dsoEmpresa.SQL = 'DECLARE @Empresa TABLE (EmpresaID INT, Empresa VARCHAR(50), TipoRegistro VARCHAR(50), IntegracaoID INT, Contexto VARCHAR(30), Indice INT, Numero VARCHAR(30), ' +
                                            'Inconsistencias VARCHAR(MAX), TipoErro INT) ' +

                        'INSERT INTO @Empresa ' +
	                        'SELECT DISTINCT EmpresaID, dbo.fn_Pessoa_Fantasia(EmpresaID, 0), TipoRegistro, ISNULL(ISNULL(Indice, IntegracaoID), ContadorID), Contexto, Indice, Numero, Inconsistencias, TipoErro ' +
		                        'FROM _ContabilidadeConciliacao WITH(NOLOCK) ' +
		                        sWhereData +

                        'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
                        'UNION ' +
                        'SELECT EmpresaID AS fldID, Empresa + SPACE(1) + ' + '\'' + '\(' + '\'' + 
                                ' + CONVERT(VARCHAR, (CASE WHEN (' + nTipoErro + ' = -2) THEN SUM((CASE WHEN (Inconsistencias IS NOT NULL) THEN 1 ELSE 0 END)) ' +
                                    'WHEN (' + nTipoErro + ' = -1) THEN SUM((CASE WHEN ((Inconsistencias IS NOT NULL) AND (TipoErro <> 0)) THEN 1 ELSE 0 END)) ' +
                                    'ELSE SUM((CASE WHEN ((Inconsistencias IS NOT NULL) AND (TipoErro = ' + nTipoErro + ')) THEN 1 ELSE 0 END)) END)) + ' + '\'' + '\) ' + '\'' + ' AS fldName ' +
                            'FROM @Empresa ' +
                            'GROUP BY EmpresaID, Empresa ' +
                            'ORDER BY fldName';
 
    try
    {
        dsoEmpresa.ondatasetcomplete = preencherEmpresa_DSC;
        dsoEmpresa.Refresh();
    }
    catch(e)
    {
        selEmpresaID.disabled = false;
    }

}

function preencherEmpresa_DSC()
{
    var optionStr, optionValue;

    clearComboEx(['selEmpresaID']);

    dsoEmpresa.recordset.moveFirst();

    while (!dsoEmpresa.recordset.EOF) {
        optionStr = dsoEmpresa.recordset['fldName'].value;
        optionValue = dsoEmpresa.recordset['fldID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;

        selEmpresaID.add(oOption);

        dsoEmpresa.recordset.moveNext();

        //selEmpresaID.disabled = false;
    }

    selEmpresaID.disabled = ((selEmpresaID.length <= 1) ? true : false);

    selEmpresaID.value = glb_nIndexEmpresa;
}

function chkDetalhes_onclick()
{
    ajustaGrids();
}

/*
function chkIndice_onclick()
{
    fillGrid();
}
*/

function onchange_selTipoErro()
{
    preencherEmpresa();
    fillGrid();
} 

function ajustaGrids()
{
    if (!chkDetalhes.checked)
    {
        divFGErros.style.visibility = 'hidden';

        // ajusta o divFG
        with (divFG.style)
        {
            //height = parseInt(btnOK.currentStyle.top, 10) + 10 - (ELEM_GAP * 7); // -parseInt(top, 10)
            height = parseInt(btnOK.currentStyle.top, 10) + 10 - (ELEM_GAP * 13); // -parseInt(top, 10)
        }
    }
    else
    {
        if (fg.TextMatrix(glb_Row, getColIndexByColKey(fg, 'Fonte')) == 'Overfly')
            fillGridErro();
        else if ((fg.TextMatrix(glb_Row, getColIndexByColKey(fg, 'Fonte')) == 'NC') && (fg.TextMatrix(glb_Row, getColIndexByColKey(fg, 'IntegracaoID*')) != ''))
            fillGridLog();

        divFGErros.style.visibility = 'inherit';

        with (divFG.style)
        {
            height = parseInt(btnOK.currentStyle.top, 10) - (ELEM_GAP * 13) - parseInt(divFGErros.currentStyle.height, 10); // -parseInt(top, 10)
        }
    }

    with (fg.style)
    {
        height = parseInt(divFG.style.height, 10);
    }
}

function btnGravar_onclick(ctl)
{
    var strPars = '';
    var sCorretor = '';
    var sCorrecao = '';
    var sTipoErro = '';

    lockControlsInModalWin(true);

    for (i = 1; i < fg.Rows; i++)
    {
        if (getCellValueByColKey(fg, 'Gravar', i) != 1)
            continue;

        strPars +=  (strPars == '' ? '?' : '&') + 'nConciliacaoID=' + escape(getCellValueByColKey(fg, 'ConciliacaoID', i));

        sCorretor = getCellValueByColKey(fg, 'Corretor', i);
        if (sCorretor == '')
            sCorretor = 'NULL';
        else
            sCorretor = '\'' + sCorretor + '\'';

        sCorrecao = getCellValueByColKey(fg, 'Correcao', i);
        if (sCorrecao == '')
            sCorrecao = 'NULL';
        else
            sCorrecao = '\'' + sCorrecao + '\'';

        sTipoErro = getCellValueByColKey(fg, 'TipoErro', i);

        strPars += '&sCorretor=' + escape(sCorretor);
        strPars += '&sCorrecao=' + escape(sCorrecao);
        strPars += '&sTipoErro=' + escape(sTipoErro);
    }

    if (strPars == "")
    {
        if (window.top.overflyGen.Alert('N�o h� dados a serem gravados') == 0)
            return null;

        lockControlsInModalWin(false);
        return;
    }

    dsoGrava.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/planocontas/serverside/gravarconciliacao.aspx' + strPars;
    dsoGrava.ondatasetcomplete = dsoGrava_DSC;
    dsoGrava.refresh();
}

function dsoGrava_DSC()
{
    dsoCorrecao.ondatasetcomplete = preencherCorrecao_DSC;
    dsoCorrecao.Refresh();
    glb_refreshGridInterval = window.setInterval('fillGrid()', 10, 'JavaScript');
}

function onchange_selCorrecao(ctl)
{
    fillGrid();
}

function onchange_selCorretor(ctl)
{
    fillGrid();
}

function preencherCorrecao()
{
    var dtInicio = (txtInicio.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtInicio.value, true) + '\'');
    var dtFim = (txtFim.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtFim.value, true) + '\'');
    var nEmpresaID = selEmpresaID.value;
    var sCorretor = selCorretor.value;
    var sTipoRegistro = '';
    var sWhereData = '';

    if ((dtInicio != 'NULL') && (dtFim != 'NULL'))
        sWhereData += ((sWhereData.length == 0) ? 'WHERE ' : 'AND ') + '(dtBalancete BETWEEN ' + dtInicio + ' AND ' + dtFim + ') ';
    else if (dtInicio != 'NULL')
        sWhereData += ((sWhereData.length == 0) ? 'WHERE ' : 'AND ') + '(dtBalancete >= ' + dtInicio + ') ';
    else if (dtFim != 'NULL')
        sWhereData += ((sWhereData.length == 0) ? 'WHERE ' : 'AND ') + '(dtBalancete <= ' + dtFim + ') ';

    if (sCorretor != 0)
        sWhereData += ((sWhereData.length == 0) ? 'WHERE ' : 'AND ') + '(Corretor = \'' + sCorretor + '\') ';

    if (fg.Rows > 1)
    {
        sTipoRegistro = getCellValueByColKey(fg, 'TipoRegistro', fg.Row);
        sWhereData += ((sWhereData.length == 0) ? 'WHERE ' : 'AND ') + '(TipoRegistro = \'' + sTipoRegistro + '\') ';
    }

    if (sWhereData.length > 0)
        sWhereData += (nEmpresaID.length > 0 ? ' AND (EmpresaID = ' + nEmpresaID + ') ' : '');
    else
        sWhereData += (nEmpresaID.length > 0 ? ' WHERE (EmpresaID = ' + nEmpresaID + ') ' : '');

    sWhereData += ((sWhereData.length == 0) ? 'WHERE ' : 'AND ') + '(Correcao IS NOT NULL) ';

    dsoCorrecao.SQL = 'SELECT 0 AS Indice, ' + '\'' + '0' + '\'' + ' AS fldID, SPACE(0) AS fldName UNION ' +
        'SELECT DISTINCT 1 AS Indice, Correcao AS fldID, Correcao AS fldName ' +
        'FROM _ContabilidadeConciliacao WITH(NOLOCK) ' + sWhereData + ' ' +
        'ORDER BY Indice, fldName';

    dsoCorrecao.ondatasetcomplete = preencherCorrecao_DSC;
    dsoCorrecao.Refresh();
}

function preencherCorrecao_DSC()
{
    var optionStr, optionValue;

    clearComboEx(['selCorrecao']);

    dsoCorrecao.recordset.moveFirst();

    while (!dsoCorrecao.recordset.EOF)
    {
        optionStr = dsoCorrecao.recordset['fldName'].value;
        optionValue = dsoCorrecao.recordset['fldID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;

        selCorrecao.add(oOption);

        dsoCorrecao.recordset.moveNext();
    }

    selCorrecao.disabled = (selCorrecao.options.length <= 0);
}

function fillGridErro()
{
    var nIntegracaoID, nTipoID;

    lockControlsInModalWin(true);

    nIntegracaoID = fg.TextMatrix(glb_Row, getColIndexByColKey(fg, 'IntegracaoID*'));

    setConnection(dsoGridErro);

    dsoGridErro.SQL = 'SELECT LogID, dtData, (' + '\'' + 'Erro:' + '\'' + ' + SPACE(1) + ' + ' + Erro + ISNULL(CHAR(13) + CHAR(10) +' + '\'' + 'Doc:' + '\'' + ' + SPACE(1) + ' + '+ Documento, SPACE(0))) AS ErroDocumento, Numero, Arquivo ' +
                            'FROM _ContabilidadeConciliacao_Erros WITH(NOLOCK) ' +
                            'WHERE TipoRegistro = ' + '\'' + glb_sTipoRegistro + '\'' + ' AND IntegracaoID = ' + nIntegracaoID + ' ' +
                            'ORDER BY LogID';
    //glb_sReporError = dsoGridErro.SQL;

    dsoGridErro.ondatasetcomplete = fillGridErro_DSC;
    dsoGridErro.refresh();
}

function fillGridErro_DSC() {

    startGridInterface(fgErros);
    fgErros.FrozenCols = 0;
    fgErros.FontSize = '8';

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy hh:mm';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy hh:mm';

    headerGrid(fgErros, ['LogID',
                        'Data',
                        'Erro / Documento',
                        'N�mero',
                        'Arquivo'], []);

    fillGridMask(fgErros, dsoGridErro, ['LogID',
                                        'dtData',
                                        'ErroDocumento',
                                        'Numero',
                                        'Arquivo'],
                                        ['', '', '', '', ''],
                                        ['', dTFormat, '', '', '']);
    alignColsInGrid(fgErros, []);

    fgErros.Editable = false;

    // Merge das celulas
    fgErros.MergeCells = 4;
    fgErros.MergeCol(0) = true;
    fgErros.MergeCol(1) = true;
    fgErros.MergeCol(2) = true;
    fgErros.MergeCol(3) = true;
    fgErros.MergeCol(4) = true;

    //Ordena��o
    fgErros.ExplorerBar = 5;

    lockControlsInModalWin(false);

    fgErros.FrozenCols = 1;

    fgErros.AutoSize(0, fgErros.Cols - 1, false, 0);
    fgErros.ColWidth(getColIndexByColKey(fgErros, 'ErroDocumento')) = 8800;

    if (fgErros.Rows >= 2) {
        fgErros.Row = 1;
        fgErros.Col = 0;
    }

    glb_duplicaGridRows = window.setInterval('duplicaGridRows()', 50, 'JavaScript');
}

// Duplica a altura da linha para comportar a descricao do Servico Federal
function duplicaGridRows() {
    var i = 0;
    if (glb_duplicaGridRows != null) {
        window.clearInterval(glb_duplicaGridRows);
        glb_duplicaGridRows = null;
    }

    for (i = 2; i < fgErros.Rows; i++)
        fgErros.RowHeight(i) = 540;

    return null;
}

function preencherTipoErro()
{
    dsoTipoErro.SQL = 'SELECT -2 AS fldID, SPACE(0) AS fldName UNION ' +
        'SELECT -1 AS fldID, \'!0\' AS fldName UNION ' +
        'SELECT DISTINCT TipoErro AS fldID, CONVERT(VARCHAR, TipoErro) AS fldName ' +
            'FROM _ContabilidadeConciliacao WITH(NOLOCK) ' +
            'ORDER BY fldID ASC';

    dsoTipoErro.ondatasetcomplete = preencherTipoErro_DSC;
    dsoTipoErro.Refresh();
}

function preencherTipoErro_DSC()
{
    var optionStr, optionValue;

    clearComboEx(['selTipoErro']);

    dsoTipoErro.recordset.moveFirst();

    while (!dsoTipoErro.recordset.EOF) {
        optionStr = dsoTipoErro.recordset['fldName'].value;
        optionValue = dsoTipoErro.recordset['fldID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;

        selTipoErro.add(oOption);

        dsoTipoErro.recordset.moveNext();
    }

    selTipoErro.value = -2;

    selTipoErro.disabled = (selTipoErro.options.length <= 0);
}

function fillGridLog()
{
    var nIntegracaoID;
    var nFormID;
    var nSubFormID;
    var sWhere = '';
    var nEmpresaData = getCurrEmpresaData();

    lockControlsInModalWin(true);

    nIntegracaoID = fg.TextMatrix(glb_Row, getColIndexByColKey(fg, 'IntegracaoID*'));

    sContexto = fg.TextMatrix(glb_Row, getColIndexByColKey(fg, 'Contexto*'));

    if ((glb_sTipoRegistro == 'Nota Fiscal') || (glb_sTipoRegistro == 'Duplicata'))
    {
        nFormID = 5120;
        nSubFormID = 24030;
    }
    else if (glb_sTipoRegistro == 'CTE')
    {
        nFormID = 5410;
        nSubFormID = 24330;
    }
    else if (glb_sTipoRegistro == 'Lancamento Bancario')
    {
        nFormID = 10140;
        nSubFormID = 29090;
    }
    else
    {
        nFormID = 9110;
        nSubFormID = 28000;
    }

    if ((glb_sTipoRegistro == 'Baixa') || ((glb_sTipoRegistro == 'Adiantamento') & (sContexto = 'Cliente')))
        sWhere = ' AND (a.RegistroID IN (SELECT aa.FinanceiroID FROM Financeiro_Ocorrencias aa WITH(NOLOCK) WHERE (aa.FinOcorrenciaID = ' + nIntegracaoID + ')))';
    else
        sWhere = ' AND (a.RegistroID = ' + nIntegracaoID + ') ';

    setConnection(dsoGridErro);

    dsoGridErro.SQL = 'SELECT e.RecursoFantasia AS SubForm, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso, b.Fantasia AS Colaborador, c.ItemAbreviado AS Evento, ' +
                            'd.RecursoAbreviado AS Estado, a.Motivo, a.LOGID ' +
                        'FROM LOGs a WITH(NOLOCK) ' +
                            'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.UsuarioID) ' +
                            'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.ItemID = a.EventoID) ' +
                            'INNER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = a.EstadoID) ' +
                            'INNER JOIN Recursos e WITH(NOLOCK) ON (e.RecursoID = a.SubFormID) ' +
                          'WHERE (a.FormID=' + nFormID + ') AND (a.SubFormID=' + nSubFormID + ') ' + sWhere +
                          'ORDER BY a.Data DESC';

    dsoGridErro.ondatasetcomplete = fillGridLog_DSC;
    dsoGridErro.refresh();
}

function fillGridLog_DSC()
{
    startGridInterface(fgErros);
    fgErros.FrozenCols = 0;
    fgErros.FontSize = '8';

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy hh:mm';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy hh:mm';

    headerGrid(fgErros, ['SubForm',
                         'Data           Hora     ',
                         'Colaborador',
                         'Evento',
                         'Est',
                         'Motivo',
                         'LOGID'], [6]);

    fillGridMask(fgErros, dsoGridErro, ['SubForm',
                                        'DataFuso',
                                        'Colaborador',
                                        'Evento',
                                        'Estado',
                                        'Motivo',
                                        'LOGID'],
                                        ['', '', '', '', '', '', ''],
                                        ['', dTFormat, '', '', '', '', '']);
    alignColsInGrid(fgErros, []);

    fgErros.Editable = false;

    // Merge das celulas
    fgErros.MergeCells = 4;
    fgErros.MergeCol(0) = true;
    fgErros.MergeCol(1) = true;
    fgErros.MergeCol(2) = true;
    fgErros.MergeCol(3) = true;
    fgErros.MergeCol(4) = true;
    fgErros.MergeCol(5) = true;

    //Ordena��o
    fgErros.ExplorerBar = 5;

    lockControlsInModalWin(false);

    fgErros.FrozenCols = 1;
    fgErros.AutoSize(0, fgErros.Cols - 1, false, 0);

    if (fgErros.Rows >= 2)
    {
        fgErros.Row = 1;
        fgErros.Col = 0;
    }
}

function reports_onreadystatechange() {
    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
    (frameReport.contentWindow.document.readyState == 'interactive') ||
    (frameReport.contentWindow.document.readyState == 'complete')) {
        lockControlsInModalWin(false);
        if (glb_ContExcel == 1)
            glb_geraExcel = window.setInterval('geraExcel(2)', 10, 'JavaScript');
        else {
            glb_ContExcel = 0;
            window.clearInterval(glb_geraExcel);
        }
    }
}

function js_fg_modalconciliacaoAfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {
    if (!glb_bResumo) {
        if (fg.TextMatrix(NewRow, getColIndexByColKey(fg, 'Fonte')) == 'Overfly') {
            txtRegistro.value = fg.TextMatrix(NewRow, getColIndexByColKey(fg, 'IntegracaoID'));
        }
        else if ((fg.TextMatrix(NewRow, getColIndexByColKey(fg, 'Fonte')) == 'NC') && (fg.TextMatrix(NewRow, getColIndexByColKey(fg, 'Numero')) != '')) {
            txtRegistro.value = fg.TextMatrix(NewRow, getColIndexByColKey(fg, 'Numero'));
        }
    }
}
