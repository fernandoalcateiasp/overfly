/********************************************************************
modalrazao.js

Library javascript para o modalrazao.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;

var glb_PassedOneInDblClick = false;
var	glb_nContaID;
var glb_nEstadoID;
var	glb_nDetalheID;
var glb_bConciliado;
var	glb_sData;
var	glb_sFiltro;
var glb_aMoedas = new Array();
var glb_bCarregamento = true;

var glb_divFGExtended = 349;
var glb_divFGCollapse = 0;
var glb_divFG1Fornecedor = 169;
var glb_divFG2Fornecedor = 169;
var glb_bDivFGIsCollapse = false;

// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_excel.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_excel_dis.gif';


var dsoCombo = new CDatatransport('dsoCombo');
var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');
var dsoEMail = new CDatatransport('dsoEMail');
var dsoLancamentoDetalhes = new CDatatransport('dsoLancamentoDetalhes');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
chks_onclick()
sels_onchange()
btn_onclick(ctl)
fillGridData()
fillGridData_DSC()

js_fg_modalrazaoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalrazaoDblClick(grid, Row, Col)
js_modalrazaoKeyPress(KeyAscii)
js_modalrazao_ValidateEdit()
js_modalrazao_BeforeEdit(grid, row, col)
js_modalrazao_AfterEdit(Row, Col)
js_fg_AfterRowColmodalrazao (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	glb_nContaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nContaID');
	glb_nEstadoID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nEstadoID');
	glb_nDetalheID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nDetalheID');
	glb_bConciliado	= sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bConciliado');
	glb_sData = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_sData');
	glb_sFiltro = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_sFiltro');

	fillGlbArrays();
	fillCmbMoeda();

	window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalrazaoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    showExtFrame(window, true);
    
	txtDataFim.onfocus = selFieldContent;
	txtFiltro.onfocus = selFieldContent;

	if ( (glb_nContaSelecionadaID == 0) && (glb_nContaID != null) && (glb_nContaID > 0) )
		selConta.value = glb_nContaID;

	if ( glb_nEstadoID != null )
		selEstado.value = glb_nEstadoID;
	
	/*if ( (glb_bConciliado != null) && (glb_bConciliado == 1) )
		chkLancConciliados.checked = true;
	else
		chkLancConciliados.checked = false;*/

	if ( (glb_sFiltro != null) && (glb_sFiltro != '') )
		txtFiltro.value = glb_sFiltro;

	selConta.disabled = selConta.options.length <= 1;
    selEstado.disabled = selEstado.options.length <= 1;
    selDetalhe.disabled = selDetalhe.options.length <= 1;
    selConcilia.disabled = selConcilia.options.length <= 1;
    txtDataInicio.focus();
    txtDataInicio.select();
	adjustBtnsStatus();
    fillComboData();
	adjustLabelCombos();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Raz�o', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    glb_aEmpresaData = getCurrEmpresaData();
    
    // largura e altura da janela modal
    //frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    frameRect = [10, 57, 990, 540];
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // reajusta dimensoes e reposiciona a janela
    redimAndReposicionModalWin(modWidth, modHeight, false);

    moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;

	elem = lblEmpresas;
	with (elem.style)
	{
		left = 0;
		top = -10;
		width = FONT_WIDTH * 18;
    }
    
    elem = selEmpresas;
    elem1 = lblEmpresas;
    with (elem.style)
    {
		left = 0;
		top = elem1.offsetTop + elem1.offsetHeight;
		width = elem1.offsetWidth;
		height = 54;
    }

    adjustElementsInForm([['lblMoeda','selMoeda',7,1, 140, -17],
						  ['lblEstado','selEstado',5,1],
						  ['lblConcilia', 'selConcilia', 7, 1],
						  ['lblDetalheLancamento','chkDetalheLancamento',3,1],
						  ['lblColunas','chkColunas',3,1],
						  ['lblDataInicio','txtDataInicio',10,1],
						  ['lblDataFim','txtDataFim',10,1],
						  ['lblFiltro','txtFiltro',24,1],
    					  ['lblConta','selConta',37,2,140],
						  ['lblDetalhe','selDetalhe',24,2,-5],
						  ['btnOK','btn',40,2],
						  ['btnImprime','btn',44,2,1],
						  ['btnExcel', 'btn', 21, 2,1]], null, null, true);
	
    btnExcel.src = glb_LUPA_IMAGES[1].src;
	btnExcel.style.height = 21;
	btnImprime.style.height = 22;
	btnImprime.style.top = parseInt(btnImprime.style.top,10) - 2;
	btnImprime.disabled = true;
	btnOK.style.top = parseInt(btnOK.style.top,10) - 2;
	btnOK.style.height = 22;
	
	lblConta.style.width = selConta.offsetWidth;
	lblEstado.style.width = selEstado.offsetWidth;
	lblDetalhe.style.width = selDetalhe.offsetWidth;
	
	selConta.onchange = sels_onchange;
	selEstado.onchange = sels_onchange;
	selDetalhe.onchange = sels_onchange;
	selEmpresas.onchange = selEmpresas_onchange;
	selMoeda.onchange = selMoeda_onchange;
	selConcilia.onchange = sels_onchange;
	
	/*chkLancConciliados.onclick = chks_onclick;
    chkLancConciliados.checked = false;
    chkDetalheLancamento.onclick = collapseExtendedDivFG;
    chkColunas.onclick = chkColunas_onclick;
    chkColunas.checked = false;*/
    
    txtDataInicio.maxLength = 10;
    txtDataFim.maxLength = 10;
    txtDataFim.onkeydown = txt_onkeydown;
    txtFiltro.onkeydown = txt_onkeydown;
	
    if ( DATE_FORMAT == "DD/MM/YYYY" )
    {
		txtDataInicio.value = glb_sDataDDMMYYYY;
		txtDataFim.value = glb_sDataFimDDMMYYYY;
	}	
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
    {
		txtDataInicio.value = glb_sDataMMDDYYYY;
		txtDataFim.value = glb_sDataFimMMDDYYYY;
	}	

	// ajusta o divFields
    elem = divFields;
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = selConta.offsetTop + selConta.offsetHeight + 2;
    }
    
    // ajusta o divFG
    elem = divFG;
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP - 2;
        top = parseInt(divFields.currentStyle.top, 10) + parseInt(divFields.currentStyle.height, 10) + 1;
        width = (modWidth - 2 * ELEM_GAP - 6) + 4;    
        //height = modHeight - divFields.offsetTop - divFields.offsetHeight - (ELEM_GAP + 6);
        height = glb_divFGExtended + 70;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // ajusta o divFG2
    with (divFG2.style)
    {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divFG.offsetTop;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = glb_divFG2Fornecedor;
        visibility = 'hidden';
    }
    
    with (fg2.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10) - 2;
        height = parseInt(divFG2.style.height, 10) - 2;
    }
    	
    // Reposiciona e troca label botao OK
    elem = btnOK;
    with (elem)
    {         
		//style.top = parseInt(divFields.currentStyle.top, 10) + parseInt(selDetalhe.currentStyle.top, 10) - 3;
		//style.left = modWidth - parseInt(btnOK.currentStyle.width) - 30;
		//style.left = 645;
		style.width = 40;
		value = 'Listar';
		disabled = false;
    }
        
    // Reposiciona botao Fechar
    elem = btnCanc;
    elem1 = btnOK;
    
    with (elem)
    {
		style.top = elem1.offsetTop;
		style.left = elem1.offsetLeft - elem.offsetWidth - ELEM_GAP;
		style.visibility = 'hidden';
		disabled = false;
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
    
    startGridInterface(fg2);
    fg2.Cols = 0;
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;    
    fg2.Redraw = 2;
}

/********************************************************************
Usuario teclou Enter em campo texto
********************************************************************/
function txt_onkeydown()
{
	if ( event.keyCode == 13 )	
		fillGridData();
}

/********************************************************************
Usuario clicou em checkbox
********************************************************************/
function chks_onclick()
{
	;
}

/********************************************************************
Usuario alterou selecao de combo
********************************************************************/
function sels_onchange()
{
	adjustLabelCombos(this);

	if (this == selConta)
	{
		adjustBtnsStatus();
		fillComboData();
	}
}

function adjustLabelCombos(elem)
{
	if ( (elem == null) || (elem == selConta) )
	{
		if (selConta.options.length > 0)
			setLabelOfControl(lblConta, selConta.value);
		else
			setLabelOfControl(lblConta, '');
	}
	if ( (elem == null) || (elem == selEstado) )
	{
		if (selEstado.options.length > 0)
			setLabelOfControl(lblEstado, selEstado.value);
		else
			setLabelOfControl(lblEstado, '');
	}
	if ( (elem == null) || (elem == selDetalhe) )
	{
		if (selDetalhe.options.length > 0)
			setLabelOfControl(lblDetalhe, selDetalhe.value);
		else
			setLabelOfControl(lblDetalhe, '');
	}
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		fillGridData();
    }
    else if (controlID == 'btnCanc')
    {
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller,
				new Array(selConta.value, 
						  selEstado.value, 
						  selDetalhe.value,
						  selConcilia.value,
						  txtDataFim.value,
						  txtFiltro.value)); 
    }
    else if (controlID == 'btnImprime')
    {
		imprimeGrid();
    }
}

/********************************************************************
Solicitar dados dos combos ao servidor
********************************************************************/
function fillComboData()
{
    lockControlsInModalWin(true);
    
    var nContaID, nTipodetalhamentoID;
    var sEmpresas = buildsEmpresas(',', false);
	nContaID = selConta.value;
	nTipoDetalhamentoID = selConta.options.item(selConta.selectedIndex).getAttribute('TipoDetalhamentoID', 1);

	clearComboEx(['selDetalhe']);        
    fg.Rows = 1;
    fg2.Rows = 1;

	// Nenhum
	if ((nTipoDetalhamentoID == 1531) || (sEmpresas == ''))
	{
		lockControlsInModalWin(false);    
		adjustBtnsStatus();
    
		selDetalhe.disabled = selDetalhe.options.length <= 1;
		txtDataInicio.focus();
		txtDataInicio.select();
		glb_bCarregamento = false;
		return null;
	}
	//Pessoas
	else if ( (nTipoDetalhamentoID >= 1532) && (nTipoDetalhamentoID <= 1541) ) 
	{	    
	    strSQL = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName UNION ALL ' +
				 'SELECT DISTINCT 1 AS Indice, c.PessoaID AS fldID, c.Fantasia AS fldName ' +
					'FROM Lancamentos_Contas a WITH(NOLOCK), Lancamentos b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
					'WHERE (a.ContaID = ' + nContaID + ' AND a.LancamentoID = b.LancamentoID AND ' +
						'b.EmpresaID IN (' + sEmpresas + ') AND b.EstadoID IN (2, 81, 82) AND a.PessoaID = c.PessoaID AND ' + 
						'dbo.fn_Pessoa_Tipo(c.PessoaID, b.EmpresaID, 2, ' + nTipoDetalhamentoID + ') = 1) ' +
				 'ORDER BY Indice, fldName';
	}
	//Conta Bancaria
	else if ( nTipoDetalhamentoID == 1542)
	{
	    strSQL = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName UNION ALL ' +
				 'SELECT DISTINCT 1 AS Indice, a.RelPesContaID AS fldID, dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) AS fldName ' +
					'FROM Lancamentos_Contas a WITH(NOLOCK), Lancamentos b WITH(NOLOCK) ' +
					'WHERE (a.ContaID = ' + nContaID + ' AND a.LancamentoID = b.LancamentoID AND ' +
					'b.EmpresaID IN (' + sEmpresas + ') AND ' +
					'b.EstadoID IN (2, 81, 82)) ' +
				 'ORDER BY Indice, fldName';
	}
	//Impostos
	else if ( nTipoDetalhamentoID == 1543)
	{	    
	    strSQL = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName UNION ALL ' +
				 'SELECT DISTINCT 1 AS Indice, a.ImpostoID AS fldID, c.Imposto AS fldName ' +
					'FROM Lancamentos_Contas a WITH(NOLOCK), Lancamentos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
					'WHERE (a.ContaID = ' + nContaID + ' AND a.LancamentoID = b.LancamentoID AND ' +
					'b.EmpresaID IN (' + sEmpresas + ') AND ' +
					'b.EstadoID IN (2, 81, 82) AND a.ImpostoID = c.ConceitoID) ' +
				'ORDER BY Indice, fldName';

	}

	glb_nDSOs = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoCombo);

	dsoCombo.SQL = strSQL;
    dsoCombo.ondatasetcomplete = fillComboData_DSC;
    dsoCombo.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados dos combos
********************************************************************/
function fillComboData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

    var oOption;

    while (! dsoCombo.recordset.EOF )
    {
        oOption = document.createElement("OPTION");
        oOption.text = dsoCombo.recordset['fldName'].value;
        oOption.value = dsoCombo.recordset['fldID'].value;
        selDetalhe.add(oOption);
        dsoCombo.recordset.MoveNext();
    }
   
	if ( (glb_nDetalheID != null) && (glb_nDetalheID > 0) )
	{
		selDetalhe.value = glb_nDetalheID;
		glb_nDetalheID = null;
	}

	lockControlsInModalWin(false);    
	adjustBtnsStatus();
    
	selDetalhe.disabled = selDetalhe.options.length <= 1;
    txtDataInicio.focus();
    txtDataInicio.select();
    glb_bCarregamento = false;
}

function buildStrSQL(bToExcel)
{
    txtDataFim.value = trimStr(txtDataFim.value);
	txtFiltro.value = trimStr(txtFiltro.value);

	var i = 0;
	var nEstadoID, nDetalheID, sDataInicio, sDataFim, sFiltro, strSQL;

	nEstadoID = selEstado.value;
	nDetalheID = selDetalhe.value;
	
    var sEmpresas = buildsEmpresas('/', true);
    
    if (sEmpresas == '')
    {
        if ( window.top.overflyGen.Alert('Selecione pelo menos uma empresa.') == 0 )
            return null;
            
		return '';
    }

	sDataInicio = normalizeDate_DateTime(txtDataInicio.value, 1);
	
	if (chkDataEx(trimStr(txtDataInicio.value))!=true)
	{
		lockControlsInModalWin(false);
		adjustBtnsStatus();
		
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        window.focus();
        
        if ( txtDataInicio.value != '' )    
			txtDataInicio.focus();
		
		return '';
	}
	
	sDataFim = normalizeDate_DateTime(txtDataFim.value, 1);

	if (chkDataEx(trimStr(txtDataFim.value))!=true)
	{
		lockControlsInModalWin(false);
		adjustBtnsStatus();
		
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        window.focus();
        
        if ( txtDataFim.value != '' )    
			txtDataFim.focus();
		
		return '';
	}
	
	if (window.top.daysBetween(txtDataInicio.value, txtDataFim.value) > 732)
	{
		lockControlsInModalWin(false);
		adjustBtnsStatus();
		
        if ( window.top.overflyGen.Alert('Excedido per�odo m�ximo de 24 meses.') == 0 )
            return null;
        
        window.focus();
        
        if ( txtDataFim.value != '' )    
			txtDataFim.focus();
		
		return '';
	}

	if (window.top.daysBetween(txtDataInicio.value, txtDataFim.value) < 0)
	{
		lockControlsInModalWin(false);
		adjustBtnsStatus();
		
        if ( window.top.overflyGen.Alert('A data final deve ser maior ou igual a data inicial.') == 0 )
            return null;
        
        window.focus();
		txtDataFim.focus();
		
		return '';
	}

	if (sDataFim == null)
		sDataFim = '';
	
	sFiltro = txtFiltro.value;

	if (nEstadoID==0)
		nEstadoID = 'NULL';

	if (nDetalheID == 0)
		nDetalheID = 'NULL';

	if ((sDataInicio == null)||(sDataInicio == ''))
		sDataInicio = 'NULL';
	else
		sDataInicio = '\'' + sDataInicio + '\'';	

	if ((sDataFim == null)||(sDataFim == ''))
		sDataFim = 'NULL';
	else	
		sDataFim = '\'' + sDataFim + '\'';
		
	if (sFiltro == '')
		sFiltro = 'NULL';
	else
		sFiltro = '\'' + sFiltro + '\'';
    
	var geraExcelID = 1;
	var formato = 2; //Excel
	var sLinguaLogada = getDicCurrLang();

	var strParameters = "RelatorioID=" + geraExcelID + "&Formato=" + formato + "&bToExcel=" + (bToExcel ? true : false) + "&sLinguaLogada=" + sLinguaLogada + "&glb_sEmpresaFantasia=" + glb_aEmpresaData[3] +
                        "&sEmpresas=" + sEmpresas + "&selMoeda=" + selMoeda.value + "&selConta=" + (selConta.value == 0 ? 'NULL' : selConta.value) + "&nEstadoID=" + nEstadoID + "&nDetalheID=" + nDetalheID +
                        "&selConcilia=" + selConcilia.value + "&sDataInicio=" + sDataInicio + "&sDataFim=" + sDataFim + "&sFiltro=" + sFiltro;

	return SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/planocontas/serverside/ReportsGrid_razao.aspx?' + strParameters;
    
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	var strSQL = buildStrSQL(false);
    if (strSQL=='')
		return null;

	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;
    
    // zera o grid
    fg.Rows = 1;
    fg2.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	dsoGrid.URL = strSQL;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
    try
    {
        dsoGrid.Refresh();
    }
    catch(e)
    {
		lockControlsInModalWin(false);
		adjustBtnsStatus();
		
        if ( window.top.overflyGen.Alert('Filtro inv�lido.') == 0 )
            return null;
        
        window.focus();
        
        if ( txtFiltro.value != '' )    
			txtFiltro.focus();
    }  
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	lockControlsInModalWin(false);
	adjustBtnsStatus();

	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

    var dTFormat = '';
    var dTFormat2 = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd';

	dTFormat2 = dTFormat + '/yyyy';

    var i;
    var aLinesState = new Array();
    var bTempValue;

    var aHoldCols = new Array();

	if (selConta.value > 0)
		aHoldCols = [0, 18]; 
	else
		aHoldCols = [18]; 
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;

    headerGrid(fg,['Conta',
				   'Balancete',
				   'Data',
                   'Lan�',
				   'E',
				   'Conc',
				   'Est',
				   '$',
				   'Valor',
				   'Saldo',
				   'Documento',
				   'Contrapart',
				   'Hist�rico',
				   'Detalhe',
				   'Pessoa',
				   'Bco/Ag/Cta',
				   'Imposto',
				   'Empresa',
				   'LanContaID'], [aHoldCols]);
	
    fillGridMask(fg,dsoGrid,['Conta',
							 'dtBalancete',
							 'dtLancamento',
							 'LancamentoID',
							 'Estado',
							 'Conciliado',
							 'EhEstorno',
							 'SimboloMoeda',
							 'Valor',
							 'Saldo',
							 'Documento',
							 'Contrapartida',
							 'Historico',
							 'Detalhe',
							 'Fantasia',
							 'ContaBancaria',
							 'Imposto',	
							 'Empresa',
							 'LanContaID'],
							 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                             ['', dTFormat2, dTFormat, '', '', '', '', '', '(###,###,##0.00)', '(###,###,##0.00)', '', '', '', '', '', '', '', '', '']);

	gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'LancamentoID'),'#####','C']]);	
	
	if (fg.Rows > 2)
	{
		fg.TextMatrix(1,getColIndexByColKey(fg, 'Historico')) = translateTerm('Saldo Inicial', null);
		
		try
		{
			fg.TextMatrix(1,getColIndexByColKey(fg, 'Saldo')) = fg.ValueMatrix(2,getColIndexByColKey(fg, 'Saldo')) -
				fg.ValueMatrix(2,getColIndexByColKey(fg, 'Valor'));
		}
		catch(e)
		{
			fg.TextMatrix(1,getColIndexByColKey(fg, 'Saldo')) = 0;
		}
	}
	
	
	fg.ColHidden(getColIndexByColKey(fg, 'Contrapartida')) = !chkColunas.checked;
	fg.ColHidden(getColIndexByColKey(fg, 'Fantasia')) = !chkColunas.checked;
	fg.ColHidden(getColIndexByColKey(fg, 'ContaBancaria')) = !chkColunas.checked;
	fg.ColHidden(getColIndexByColKey(fg, 'Imposto')) = !chkColunas.checked;
	fg.ColHidden(getColIndexByColKey(fg, 'LanContaID')) = !chkColunas.checked;

	fg.Redraw = 0;
	for ( i=2; i<fg.Rows; i++ )
	{
		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor'));
			fg.FillStyle = 1;
	        fg.CellForeColor = 0X0000FF;
	    }

		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'Saldo')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'Saldo'), i, getColIndexByColKey(fg, 'Saldo'));
			fg.FillStyle = 1;
	        fg.CellForeColor = 0X0000FF;
	    }
	}

    alignColsInGrid(fg,[3, 8, 9]);                           

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
	fg.FrozenCols = 3;
    
    fg.Redraw = 2;
    
    if ( fg.Rows > 1 )
		fg.Row = 2;

    fg.Editable = false;
        
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);
	adjustBtnsStatus();

    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 2)
    {
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
}

function detalharLancamento()
{
	var nLancamentoID;

	if (fg.Row < 2)
		return true;
		
	nLancamentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'LancamentoID'));		

	if (chkDetalheLancamento.checked)
		fillgridDetalhes(nLancamentoID);
	else
		sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, ('detalharLancamento(' + nLancamentoID + ')') );
}


function fillgridDetalhes(nLancamentoID)
{
	lockControlsInModalWin(true);
    setConnection(dsoLancamentoDetalhes);

	dsoLancamentoDetalhes.SQL = 'EXEC sp_Lancamento_Detalhes ' + nLancamentoID + ', NULL, 1';
    dsoLancamentoDetalhes.ondatasetcomplete = dsoLancamentoDetalhes_DSC;
    dsoLancamentoDetalhes.Refresh();
}

function dsoLancamentoDetalhes_DSC()
{
	fg2.Editable = false;
	fg2.FontSize = '8';
    headerGrid(fg2,['Form',
					'SubForm',
					'Tipo',
					'Subtipo',
                    'Registro',
                    'Est',
                    'Data',
                    'Pessoa',
					'Valor',
					'Observa��o',
					'FormID',
					'SubFormID',
					'FinanceiroID'], [10, 11, 12]);

    fillGridMask(fg2,dsoLancamentoDetalhes,['Form',
								'SubForm',
								'Tipo',
								'Subtipo',
								'RegistroID',
								'Estado',
								'Data',
								'Pessoa',
								'Valor',
								'Observacao',
								'FormID',
								'SubFormID',
								'FinanceiroID'],
								['','','','','','','','','','','','',''],
                                ['','','','','','','','','(###,###,##0.00)','','','','']);

    //gridHasTotalLine(fg2, 'Totais', 0xC0C0C0, null, true, [[4,'(###,###,###)','C'], [8,'(###,###,###.00)','S']]);

	for ( i=1; i<fg2.Rows; i++ )
	{
		if ( fg2.ValueMatrix(i, 8) < 0 )
		{
			fg2.Select(i, 8, i, 8);
			fg2.FillStyle = 1;
		    fg2.CellForeColor = 0X0000FF;

		}
	}

	if (fg2.Rows > 1)
		fg2.Row = 1;

    alignColsInGrid(fg2,[4,8]);
	fg2.FrozenCols = 1;

    // Merge de Colunas
    fg2.MergeCells = 4;
    fg2.MergeCol(-1) = true;
    fg2.Redraw = 0;
    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0,fg2.Cols-1);
    fg2.Redraw = 2;
    
    lockControlsInModalWin(false);
}

function adjustBtnsStatus()
{
	var nAceitaLancamento;
	nAceitaLancamento = selConta.options.item(selConta.selectedIndex).getAttribute('AceitaLancamento', 1);
	btnOK.disabled = (nAceitaLancamento == 1 ? false : true);
	btnExcel.src = glb_LUPA_IMAGES[btnOK.disabled ? 1 : 0].src;
	btnImprime.disabled = (fg.Rows <= 2);
}

/********************************************************************
Usuario cliclou o botao esquerdo do mouse
********************************************************************/
function btn_onmousedownExcel()
{
	if (event.button == 1)
		sendToExcel();
}

function sendToExcel()
{
	var strSQL = buildStrSQL(true);
	
	if (strSQL=='')
		return null;
		
	if (btnExcel.src == glb_LUPA_IMAGES[1].src)
		return null;
		
	lockControlsInModalWin(true);
	
	var frameReport = document.getElementById("frmReport");

	frameReport.contentWindow.location = strSQL;

	lockControlsInModalWin(false);
}


function fillGlbArrays()
{
	var i=0;
	var nCounter = 0;

	for (i=0; i<glb_aMoedasEmpresas.length; i++)
	{
		if ( ascan(glb_aMoedas, glb_aMoedasEmpresas[i][1], false) < 0 )
		{
			glb_aMoedas[nCounter] = glb_aMoedasEmpresas[i][1];
			nCounter++;
		}
	}
}

function fillCmbMoeda()
{
	selMoeda.disabled = true;
	selEmpresas_onchange();
}

function selMoeda_onchange()
{
	fg.Rows = 1;
	fg2.Rows = 1;
}

function selEmpresas_onchange()
{
	var bAddMoeda = true;
	var i=0;
	var j=0;
	var nFind=0;
	var nMoedaSelected = null;
	var nCount = 0;
	
	fg.Rows = 1;
	fg2.Rows = 1;
	
	if (selMoeda.selectedIndex >= 0)
		nMoedaSelected = selMoeda.value;
		
	clearComboEx(['selMoeda']);		
	
	if (selEmpresas.selectedIndex < 0)
	{
		selMoeda.disabled = true;
		btnOK.disabled = true;
		return true;
	}
	else
	{
		selMoeda.disabled = false;
		btnOK.disabled = false;
	}
	btnExcel.src = glb_LUPA_IMAGES[btnOK.disabled ? 1 : 0].src;
	
	for (i=0; i<glb_aMoedas.length; i++)
	{
		bAddMoeda = true;
		for (j=0; j<selEmpresas.length; j++)
		{
			if (selEmpresas.options[j].selected == true )
			{
				bAddMoeda = (bAddMoeda && (bFindMoedaEmpresa(glb_aMoedas[i],selEmpresas.options[j].value) >= 0));
			}
		}
		
		if (bAddMoeda)
		{
			nFind = bFindMoedaEmpresa(glb_aMoedas[i]);
			var optionStr = glb_aMoedasEmpresas[nFind][2];
			var optionValue = glb_aMoedasEmpresas[nFind][1];
			var oOption = document.createElement("OPTION");
			oOption.text = optionStr;
			oOption.value = optionValue;
			selMoeda.add(oOption);
			
			if (optionValue == nMoedaSelected)
				nMoedaSelected = nCount;
				
			nCount++;
		}
	}
	
	selMoeda.disabled = (selMoeda.options.length == 0);

	if (selMoeda.options.length == 1)
		selMoeda.selectedIndex = 0;
	else if (nMoedaSelected != null)
		selMoeda.selectedIndex = nMoedaSelected;

    if (!glb_bCarregamento)
    {
		fillComboData();
		adjustLabelCombos();
	}
}

function buildsEmpresas(sSeparador, bInicioFim)
{
    var sEmpresas = '';
    
    for (i=0; i<selEmpresas.length; i++)
    {
		if (selEmpresas.options[i].selected == true )
		{
			if ((sEmpresas == '') && (bInicioFim))
				sEmpresas = sSeparador;

			sEmpresas += selEmpresas.options[i].value + sSeparador;
		}
    }
	
	if ((sEmpresas != '') && (!bInicioFim))
		sEmpresas = sEmpresas.substring(0, sEmpresas.length - 1);
    
	return sEmpresas;
}

function bFindMoedaEmpresa(nMoedaID, nEmpresaID)
{
	var i=0;
	var nRetVal = -1;
	
	for (i=0; i<glb_aMoedasEmpresas.length; i++)
	{
		if (nEmpresaID == null)
		{
			if (glb_aMoedasEmpresas[i][1] == nMoedaID)
			{
				nRetVal = i;
				break;
			}
		}
		else if ( (glb_aMoedasEmpresas[i][0] == nEmpresaID) && (glb_aMoedasEmpresas[i][1] == nMoedaID) )
		{
			nRetVal = i;
			break;
		}
		
	}
	
	return nRetVal;
}


// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalrazaoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalrazaoDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
	
	glb_PassedOneInDblClick = true;
    
    detalharLancamento();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalrazaoKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalrazao_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalrazao_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalrazao_AfterEdit(Row, Col)
{
	;
}

/********************************************************************
Atualiza linha de mensagens abaixo do grid.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalrazao (grid, OldRow, OldCol, NewRow, NewCol)
{
	if (OldRow != NewRow)
		fg2.Rows = 1;
}

// FINAL DE EVENTOS DE GRID *****************************************

function getCurrDate()
{
   var d, s = "";
   d = new Date();
   
   if (DATE_SQL_PARAM == 103)
   {
		s += padL(d.getDate().toString(),2,'0') + "/";
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
   }
   else
   {
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
		s += padL(d.getDate().toString(),2,'0') + "/";
   }
   
   s += d.getYear() + " ";
   s += padL(d.getHours().toString(),2,'0') + ":";
   s += padL(d.getMinutes().toString(),2,'0');

   return(s);
}

function chkColunas_onclick()
{
	fg.Rows = 1;
	fg2.Rows = 1;
}

function imprimeGrid()
{
	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	var sMsg = aEmpresaData[6] + '     ' + translateTerm('Raz�o', null, 'A');
	var gridLine = fg.gridLines;
	fg.gridLines = 2;

	if (selConcilia.value == 3000) //N�o
	    sMsg += ' (' + translateTerm('N�o Conciliado', null) + ')';
	else if(selConcilia.value == 3001) //Sim
	    sMsg += ' (' + translateTerm('Conciliado', null) + ')';
     
		
	sMsg += '     ';
	
	if (selConta.selectedIndex > 0)
	{
		sMsg += translateTerm('Conta', null) + ': ' + 
		selConta.options(selConta.selectedIndex).Conta + ' ' +
		translateTerm(selConta.options(selConta.selectedIndex).NomeConta, null, 'A') + '     ';
		
		if (selDetalhe.selectedIndex > 0)
			sMsg += translateTerm('Detalhe', null) + ': ' + selDetalhe.options(selDetalhe.selectedIndex).innerText + 
			'     ';
	}

	sMsg += txtDataInicio.value + ' - ' + txtDataFim.value + '     ' + getCurrDate();

	fg.ColWidth(getColIndexByColKey(fg, 'Valor')) = (fg.ColWidth(getColIndexByColKey(fg, 'Valor')) * 1.35);
	fg.ColWidth(getColIndexByColKey(fg, 'Saldo')) = (fg.ColWidth(getColIndexByColKey(fg, 'Saldo')) * 1.35);
	fg.ColWidth(getColIndexByColKey(fg, 'Historico')) = (fg.ColWidth(getColIndexByColKey(fg, 'Historico')) * 1.20);
	fg.ColWidth(getColIndexByColKey(fg, 'Detalhe')) = (fg.ColWidth(getColIndexByColKey(fg, 'Detalhe')) * 1.20);

	fg.PrintGrid(sMsg, false, 2, 0, 450);
	fg.gridLines = gridLine;
	fg.AutoSizeMode = 0;
	fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 1;
}

function collapseExtendedDivFG()
{
	fg.Redraw = 0;
	
	if (!chkDetalheLancamento.checked)
	{
		fg.style.height = glb_divFGExtended - 2;
		divFG.style.height = glb_divFGExtended;
		divFG2.style.visibility = 'hidden';
	}
	else
	{
		fg.style.height = glb_divFG1Fornecedor - 2;
		divFG.style.height = glb_divFG1Fornecedor;
		divFG2.style.top = divFG.offsetTop + divFG.offsetHeight + ELEM_GAP;
		divFG2.style.visibility = 'inherit';
	}

	fg.Redraw = 2;
	
	glb_bDivFGIsCollapse = !glb_bDivFGIsCollapse;
}    
