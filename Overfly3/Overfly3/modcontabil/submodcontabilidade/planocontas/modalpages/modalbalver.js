/********************************************************************
modalbalver.js

Library javascript para o modalbalver.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_timerUnloadWin = null;
var glb_bCarregamento = true;
var glb_bLancamento = false;
var glb_bLanConta = false;
var glb_bAgrupamento = false;
var glb_aMoedas = new Array();
var glb_nAgruparPorClicked = 0;
var glb_nFixedColumns = 12;
var glb_nFixedColumnsDetail = 8;
var glb_bDetalhesLancamento = false;

// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();
var glb_LUPA_contabilidaderazaoIMAGES = new Array();
glb_LUPA_contabilidaderazaoIMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_excel.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_excel_dis.gif';

var dsoGrid = new CDatatransport('dsoGrid');
var dsoDetalhamento = new CDatatransport('dsoDetalhamento');
var dsoEMail = new CDatatransport('dsoEMail');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
setupBtns()
cmb_onchange()
btn_onclick(ctl)
getCurrEmpresaData()
fillGridData()
fillGridData_DSC()
findRowByContaMae(nContaMaeID)

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalbalver.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

js_fg_DblClick()

FINAL DE DEFINIDAS NO ARQUIVO modalbalver.ASP
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();

    fillGlbArrays();

    fillCmbMoeda();

    // ajusta o body do html
    with (modalbalverBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();
    with (btnCanc.style) {
        left = (parseInt(divFG.style.width, 10) / 2) - (parseInt(btnCanc.style.width, 10) / 2);
        visibility = 'hidden';
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Balancete de Verifica��o', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();

    // largura e altura da janela modal
    //frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    frameRect = [10, 57, 990, 540];

    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';

    var oOption = document.createElement("OPTION");
    oOption.text = '';
    oOption.value = 0;
    selAgruparPor.add(oOption);

    var oOption = document.createElement("OPTION");
    oOption.text = 'Dia';
    oOption.value = 1;
    selAgruparPor.add(oOption);

    var oOption = document.createElement("OPTION");
    oOption.text = 'Semana';
    oOption.value = 2;
    selAgruparPor.add(oOption);

    var oOption = document.createElement("OPTION");
    oOption.text = 'M�s';
    oOption.value = 3;
    selAgruparPor.add(oOption);

    var oOption = document.createElement("OPTION");
    oOption.text = 'Quarter';
    oOption.value = 4;
    selAgruparPor.add(oOption);

    var i;

    for (i = 1; i <= 6; i++) {
        var oOption = document.createElement("OPTION");
        oOption.text = i;
        oOption.value = i;
        selNivel.add(oOption);
    }

    selNivel.selectedIndex = 5;

    elem = lblEmpresas;
    with (elem.style) {
        left = 0;
        top = -10;
        width = FONT_WIDTH * 18;
    }

    elem = selEmpresas;
    elem1 = lblEmpresas;
    with (elem.style) {
        left = 0;
        top = elem1.offsetTop + elem1.offsetHeight;
        width = elem1.offsetWidth;
        height = 54;
    }

    adjustElementsInForm([['lblMoeda', 'selMoeda', 7, 1, (lblEmpresas.offsetLeft + lblEmpresas.offsetWidth) + 2, -17],
						  ['lblAgruparPor', 'selAgruparPor', 9, 1, -5],
						  ['lblNivel', 'selNivel', 5, 1],
						  ['lblDica', 'chkDica', 3, 1],
						  ['lblLancConciliados', 'chkLancConciliados', 3, 1, -5],
						  ['lblSaldo', 'chkSaldo', 3, 1],
						  ['lblLancamentos', 'chkLancamentos', 3, 1],
						  ['lblContasPatrimoniais', 'chkContasPatrimoniais', 3, 1, -3],
						  ['lblContasResultado', 'chkContasResultado', 3, 1],
						  ['lblMilhar', 'chkMilhar', 3, 1, -5],
						  ['lblDataInicio', 'txtDataInicio', 9, 1, -10],
						  ['lblDataFim', 'txtDataFim', 9, 1, -3],
						  ['lblTipoDetalhamentoID', 'selTipoDetalhamentoID', 22, 2, (lblEmpresas.offsetLeft + lblEmpresas.offsetWidth) + 2],
						  ['btnConta', 'btn', btnOK.offsetWidth, 2, 24],
						  ['btnDetalhar', 'btn', btnOK.offsetWidth, 2, 10],
						  ['btnFillGrid', 'btn', btnOK.offsetWidth, 2, 10],
						  ['btnImprime', 'btn', btnOK.offsetWidth, 2, 10],
						  ['btnExcel', 'btn', 21, 2, 11]], null, null, true);

    btnExcel.src = glb_LUPA_IMAGES[0].src;
    btnExcel.style.height = 21;
    btnFillGrid.style.height = btnOK.offsetHeight;
    btnImprime.style.height = btnOK.offsetHeight;
    btnImprime.disabled = true;
    btnConta.style.height = btnOK.offsetHeight;
    btnDetalhar.style.height = btnOK.offsetHeight;
    selAgruparPor.onchange = selAgruparPor_onchange;
    selEmpresas.onchange = selEmpresas_onchange;
    selMoeda.onchange = selMoeda_onchange;
    selTipoDetalhamentoID.onchange = selTipoDetalhamentoID_onchange;

    chkDica.checked = false;
    chkDica.onclick = chkDica_onclick;
    chkSaldo.checked = true;
    chkSaldo.onclick = chkSaldo_onclick;

    chkContasPatrimoniais.checked = false;
    chkContasPatrimoniais.onclick = chkContasPatrimoniais_onclick;
    chkContasResultado.checked = true;
    chkContasResultado.onclick = chkContasResultado_onclick;
    chkMilhar.checked = true;

    txtDataInicio.maxLength = 10;
    txtDataInicio.onfocus = selFieldContent;
    txtDataInicio.onkeypress = txtData_onKeyPress;
    txtDataInicio.value = glb_dCurrDate;

    txtDataFim.maxLength = 10;
    txtDataFim.onfocus = selFieldContent;
    txtDataFim.onkeypress = txtData_onKeyPress;
    txtDataFim.value = glb_dCurrDate;

    selAgruparPor_onchange();

    // ajusta o divControles
    with (divControles.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = parseInt(btnFillGrid.currentStyle.top, 10) + parseInt(btnFillGrid.currentStyle.height, 10);
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControles.offsetTop, 10) +
			  parseInt(divControles.offsetHeight, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        //height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) -
        // parseInt(top, 10) - (ELEM_GAP * 9);
        height = parseInt(btnOK.offsetTop, 10) + parseInt(btnOK.offsetHeight, 10) -
				 parseInt(top, 10) + 84;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10) - 4;
    }

    with (txtObservacoes.style) {
        nHeight = (8 * ELEM_GAP) + 6;
        left = ELEM_GAP;
        top = divFG.offsetTop + divFG.offsetHeight - nHeight;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = nHeight;
    }

    txtObservacoes.readOnly = true;

    drawHeaderGrid();

    // reajusta dimensoes e reposiciona a janela
    (getFrameInHtmlTop(getExtFrameID(window))).style.top =
		(getFrameInHtmlTop('frameSup01')).offsetTop;

    redimAndReposicionModalWin(modWidth, modHeight, false);

    moveFrameInHtmlTop(getExtFrameID(window), frameRect);

    /////////////////////////////////////////
    showExtFrame(window, true);

    setupBtns();
    window.focus();
    txtDataInicio.focus();
}

function selAgruparPor_onchange() {
    lblMilhar.style.visibility = (selAgruparPor.value > 0 ? 'inherit' : 'hidden');
    chkMilhar.style.visibility = (selAgruparPor.value > 0 ? 'inherit' : 'hidden');
}

function drawHeaderGrid() {
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;

    with (fg) {
        FontName = 'Tahoma, Arial, MS SansSerif';
        FontSize = '8';
        Editable = false;
        AllowUserResizing = 1;
        VirtualData = true;
        Ellipsis = 1;
        SelectionMode = 1;
        AllowSelection = false;
        Rows = 1;
        Cols = 2;
        FixedRows = 1;
        FixedCols = 1;
        ScrollBars = 3;
        OutLineBar = 5;
        // TreeColor = 0X000000;
        GridLines = 0;
        FormatString = 'Conta' + '\t' + 'N�vel' + '\t' + 'Lan�' + '\t' + '$' + '\t' + 'Valor' + '\t' + 'Saldo' + '\t' + 'Observa��o' + '\t' + 'ContaMaeID' + '\t' +
			'TipoDetalhamentoID' + '\t' + 'ContaExpandida' + '\t' + 'AceitaLancamento' + '\t' + 'ContaID' + '\t' + 'TipoRegistro' + '\t' + 'DetalheID' + '\t' + 'LancamentoID' + '\t' + 'dtInicio' + '\t' + 'dtFim' + '\t' + 'Observacoes';
        OutLineBar = 1;
        // cores
        // TreeColor = 0X000000;
        //BackColorFixed = 0XFFFFFF;
        // linhas
        // Coloca botoes na barra
        GridLinesFixed = 13;
        GridLines = 1;
        GridColor = 0X000000;
        ColHidden(7) = true;
        ColHidden(8) = true;
        ColHidden(9) = true;
        ColHidden(10) = true;
        ColHidden(11) = true;
        ColHidden(12) = true;
        ColHidden(13) = true;
        ColHidden(14) = true;
        ColHidden(15) = true;
        ColHidden(16) = true;
        ColHidden(17) = true;
        ColKey(0) = 'Conta';
        ColKey(1) = 'Nivel';
        ColKey(2) = 'Lan�amentos';
        ColKey(3) = 'SimboloMoeda';
        ColKey(4) = 'Valor';
        ColKey(5) = 'Saldo';
        ColKey(6) = 'Observacao';
        ColKey(7) = 'ContaMaeID';
        ColKey(8) = 'TipoDetalhamentoID';
        ColKey(9) = 'ContaExpandida';
        ColKey(10) = 'AceitaLancamento';
        ColKey(11) = 'ContaID';
        ColKey(12) = 'TipoRegistro';
        ColKey(13) = 'DetalheID';
        ColKey(14) = 'LancamentoID';
        ColKey(15) = 'dtInicio';
        ColKey(16) = 'dtFim';
        ColKey(17) = 'Observacoes';
    }
    /* Coluna TipoRegistro
        1-Conta Original
        2-Conta Detalhe (Banco, Imposto, Pessoa)
        3-Lancamentos Agrupados (Dia, Semana, M�s, Quarter)
        4-Lancamentos
        5-Lancamentos_Contas
    */

    var aCols = [1, 2, 4, 5];

    if (dsoGrid.recordset.Fields.Count > glb_nFixedColumns) {
        for (i = glb_nFixedColumns; i < dsoGrid.recordset.Fields.Count ; i++) {
            fg.FormatString += '\t' + dsoGrid.recordset.Fields[i].Name;
            fg.ColKey(i + 6) = dsoGrid.recordset.Fields[i].Name;
            aCols[aCols.length] = i + 6;
        }
    }

    alignColsInGrid(fg, aCols);

    fg.Redraw = 2;
}

function chkSaldo_onclick() {
    if (chkSaldo.checked) {
        lblLancamentos.style.visibility = 'inherit';
        chkLancamentos.style.visibility = 'inherit';
    }
    else {
        chkLancamentos.checked = false;
        lblLancamentos.style.visibility = 'hidden';
        chkLancamentos.style.visibility = 'hidden';
    }
}

function chkDica_onclick() {
    var nGap = 168;
    if (chkDica.checked) {
        divFG.style.height = divFG.offsetHeight - nGap;
        txtObservacoes.disabled = false;
        txtObservacoes.style.top = parseInt(divFG.style.top, 10) + parseInt(divFG.style.height, 10) + 5;
        txtObservacoes.style.height = nGap;
        txtObservacoes.style.visibility = 'inherit';
    }
    else {
        divFG.style.height = divFG.offsetHeight + nGap;
        txtObservacoes.disabled = true;
        txtObservacoes.style.visibility = 'hidden';
    }


    fg.style.height = divFG.offsetHeight;
    drawBordersAroundTheGrid(fg);
}

function chkContasPatrimoniais_onclick() {
    if (!chkContasPatrimoniais.checked) {
        if (!chkContasResultado.checked)
            chkContasResultado.checked = true;
    }
}

function chkContasResultado_onclick() {
    if (!chkContasResultado.checked) {
        if (!chkContasPatrimoniais.checked)
            chkContasPatrimoniais.checked = true;
    }
}


/********************************************************************
Enter no campo data
********************************************************************/
function txtData_onKeyPress() {
    fg.Rows = 1;
    setupBtns();
    if (event.keyCode == 13)
        btn_onclick(btnFillGrid);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        ;
    }
    else if (controlID == 'btnCanc') {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
    else if (controlID == 'btnFillGrid') {
        glb_nAgruparPorClicked = selAgruparPor.value;
        btnFillGrid_onclick(controlID);
    }
    else if (controlID == 'btnImprime')
        imprimeGrid();
    else if (controlID == 'btnConta')
        detalharConta();
    else if (controlID == 'btnDetalhar')
        detalharLancamento();
}

function btnFillGrid_onclick(sControlID) {
    if (!verificaData())
        return null;

    if (window.top.daysBetween(txtDataInicio.value, txtDataFim.value) < 0) {
        lockControlsInModalWin(false);

        if (window.top.overflyGen.Alert('A data final deve ser maior ou igual a data inicial.') == 0)
            return null;

        window.focus();
        txtDataFim.focus();

        return null;
    }

    if (selAgruparPor.value > 0) {
        var nPeriodos = 0;

        // Dia
        if (selAgruparPor.value == 1)
            nPeriodos = periodo('d', txtDataInicio.value, txtDataFim.value);

            // Semana
        else if (selAgruparPor.value == 2)
            nPeriodos = periodo('w', txtDataInicio.value, txtDataFim.value);

            // Mes
        else if (selAgruparPor.value == 3)
            nPeriodos = periodo('m', txtDataInicio.value, txtDataFim.value);

            // Quarter
        else if (selAgruparPor.value == 4)
            nPeriodos = periodo('q', txtDataInicio.value, txtDataFim.value);

        if (nPeriodos > 52) {
            if (window.top.overflyGen.Alert('N�mero m�ximo de 52 per�odos excedido') == 0)
                return null;

            window.focus();
            txtDataInicio.focus();

            return null;
        }
    }

    if (sControlID.toUpperCase() == 'BTNEXCEL')
        sendToExcel();
    else
        fillGridData();
}

function verificaData() {
    var sDataInicio = trimStr(txtDataInicio.value);
    var sDataFim = trimStr(txtDataFim.value);
    var bDataIsValid = true;

    if (sDataInicio != '')
        bDataIsValid = chkDataEx(sDataInicio);

    if (!bDataIsValid || (sDataInicio == '')) {
        if (window.top.overflyGen.Alert('Data de in�cio inv�lida.') == 0)
            return null;

        window.focus();

        if (sDataInicio != '')
            txtDataInicio.focus();

        return false;
    }

    if (sDataFim != '')
        bDataIsValid = chkDataEx(sDataFim);

    if (!bDataIsValid || (sDataFim == '')) {
        if (window.top.overflyGen.Alert('Data de fim inv�lida.') == 0)
            return null;

        window.focus();

        if (sDataFim != '')
            txtDataFim.focus();

        return false;
    }

    return true;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    strSQL = buildStrSQL(1);

    if (strSQL == '')
        return null;

    lockControlsInModalWin(true);

    fg.Rows = 1;
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);
    dsoGrid.URL = strSQL;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

function buildStrSQL(nTipo) {
    var strSQL = '';
    var strPars = new String();
    var sDataInicio = trimStr(txtDataInicio.value);
    var sDataFim = trimStr(txtDataFim.value);
    var i = 0;

    if (sDataInicio != '') {
        sDataInicio = normalizeDate_DateTime(sDataInicio, 1);

        if (nTipo == 2)
            sDataInicio = '\'' + sDataInicio + '\'';
    }

    if (sDataFim != '') {
        sDataFim = normalizeDate_DateTime(sDataFim, 1);

        if (nTipo == 2)
            sDataFim = '\'' + sDataFim + '\'';
    }

    var sEmpresas = '/';
    var strPars = new String();

    for (i = 0; i < selEmpresas.length; i++) {
        if (selEmpresas.options[i].selected == true)
            sEmpresas += selEmpresas.options[i].value + '/';
    }

    if (sEmpresas == '/') {
        if (window.top.overflyGen.Alert('Selecione pelo menos uma empresa.') == 0)
            return null;

        return '';
    }

    if (nTipo == 1) {
        strPars = '?sEmpresas=' + escape(sEmpresas);
        strPars += '&nMoedaID=' + escape(selMoeda.value);
        strPars += '&nNivel=' + escape(selNivel.value);
        strPars += '&nLancamentosConciliados=' + (chkLancConciliados.checked ? '1' : '0');
        strPars += '&nTemLancamentos=' + (chkLancamentos.checked ? '1' : '0');
        strPars += '&nTemSaldo=' + (chkSaldo.checked ? '1' : '0');
        strPars += '&nContasPatrimoniais=' + (chkContasPatrimoniais.checked ? '1' : '0');
        strPars += '&nContasResultado=' + (chkContasResultado.checked ? '1' : '0');
        strPars += '&nTipoDetalhamentoID=' + escape(selTipoDetalhamentoID.value);
        strPars += '&nMilhar=' + (chkMilhar.checked ? '1' : '0');
        strPars += '&sDataInicio=' + escape(sDataInicio);
        strPars += '&sDataFim=' + escape(sDataFim);
        strPars += '&nAgruparPor=' + escape(selAgruparPor.value);
        strPars += '&nIdiomaID=' + escape(getDicCurrLang());

        strSQL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/planocontas/serverside/contabilidadebalancete.aspx' + strPars;
    }
    else {

        var geraExcelID = 1;
	    var formato = 2; //Excel
	    var sLinguaLogada = getDicCurrLang();

	    var strParameters = "RelatorioID=" +geraExcelID + "&Formato=" + formato + "&nTipo=" + nTipo + "&sLinguaLogada=" + sLinguaLogada + "&glb_sEmpresaFantasia=" + glb_aEmpresaData[3] +
                            "&sEmpresas=" + sEmpresas + "&selMoeda=" + selMoeda.value + "&selNivel=" + selNivel.value + "&chkLancConciliados=" + (chkLancConciliados.checked ? '1': '0') +
                            "&chkLancamentos=" + (chkLancamentos.checked ? '1' : '0') + "&chkSaldo=" +(chkSaldo.checked ? '1': '0') + "&chkContasPatrimoniais=" +(chkContasPatrimoniais.checked ? '1': '0') +
                            "&chkContasResultado=" + (chkContasResultado.checked ? '1' : '0') + "&selTipoDetalhamentoID=" + (selTipoDetalhamentoID.value == 0 ? 'NULL': selTipoDetalhamentoID.value) + 
                            "&chkMilhar=" + (chkMilhar.checked ? '1' : '0') + "&sDataInicio=" + sDataInicio + "&sDataFim=" + sDataFim + "&selAgruparPor=" + (selAgruparPor.value == 0 ? 'NULL': selAgruparPor.value);

	    strSQL = SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/planocontas/serverside/ReportsGrid_balver.aspx?' +strParameters;
    }

    return strSQL;
}

/********************************************************************
Fecha a janela se mostra-la
********************************************************************/
function unloadThisWin() {
    if (glb_timerUnloadWin != null) {
        window.clearInterval(glb_timerUnloadWin);
        glb_timerUnloadWin = null;
    }

    if (window.top.overflyGen.Alert('Nenhum registro dispon�vel.') == 0)
        return null;

    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, 'NOREG');
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    var sFldName;
    if (dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) {
        if (window.top.overflyGen.Alert('Nenhum registro selecionado.') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    drawHeaderGrid();

    var dTFormat = '';
    var i, j;
    var bgColorGreen, bgColorYellow, bgColorRed;
    var nTipoDetalhamentoID = 0;

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.Redraw = 0;

    if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)) {
        glb_GridIsBuilding = true;
        while (!dsoGrid.recordset.EOF) {
            fg.Row = fg.Rows - 1;

            fg.AddItem('', fg.Row + 1);

            if (fg.Row < (fg.Rows - 1))
                fg.Row++;

            fg.ColDataType(getColIndexByColKey(fg, 'Conta')) = 12;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Conta')) = dsoGrid.recordset['ID'].value + ' ' + trimStr(dsoGrid.recordset['Conta'].value);

            fg.ColDataType(getColIndexByColKey(fg, 'Nivel')) = 12;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Nivel')) = dsoGrid.recordset['Nivel'].value;

            fg.ColDataType(getColIndexByColKey(fg, 'Lan�amentos')) = 12;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Lan�amentos')) = (dsoGrid.recordset['Lan�amentos'].value == null ? '' : dsoGrid.recordset['Lan�amentos'].value);

            fg.ColDataType(getColIndexByColKey(fg, 'SimboloMoeda')) = 12;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'SimboloMoeda')) = (dsoGrid.recordset['Lan�amentos'].value == null ? '' : dsoGrid.recordset['SimboloMoeda'].value);

            fg.ColDataType(getColIndexByColKey(fg, 'Valor')) = 12;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Valor')) = (dsoGrid.recordset['Valor'].value == null ? '' : dsoGrid.recordset['Valor'].value);

            fg.ColDataType(getColIndexByColKey(fg, 'Saldo')) = 12;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Saldo')) = (dsoGrid.recordset['Saldo'].value == null ? '' : dsoGrid.recordset['Saldo'].value);

            fg.ColDataType(getColIndexByColKey(fg, 'Observacao')) = 12;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Observacao')) = (dsoGrid.recordset['Observacao'].value == null ? '' : dsoGrid.recordset['Observacao'].value);

            fg.ColDataType(getColIndexByColKey(fg, 'ContaMaeID')) = 12;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ContaMaeID')) = (dsoGrid.recordset['ContaMaeID'].value == null ? 0 : dsoGrid.recordset['ContaMaeID'].value);

            fg.ColDataType(getColIndexByColKey(fg, 'TipoDetalhamentoID')) = 12;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'TipoDetalhamentoID')) = dsoGrid.recordset['TipoDetalhamentoID'].value;

            fg.ColDataType(getColIndexByColKey(fg, 'AceitaLancamento')) = 11;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'AceitaLancamento')) = dsoGrid.recordset['AceitaLancamento'].value;

            fg.ColDataType(getColIndexByColKey(fg, 'ContaID')) = 12;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ContaID')) = dsoGrid.recordset['ID'].value;

            fg.ColDataType(getColIndexByColKey(fg, 'TipoRegistro')) = 12;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'TipoRegistro')) = 1;

            fg.ColDataType(getColIndexByColKey(fg, 'Observacoes')) = 12;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Observacoes')) = (dsoGrid.recordset['Observacoes'].value == null ? '' : dsoGrid.recordset['Observacoes'].value);

            for (i = glb_nFixedColumns; i < dsoGrid.recordset.fields.count; i++) {
                sFldName = dsoGrid.recordset.Fields[i].Name;
                fg.ColDataType(getColIndexByColKey(fg, sFldName)) = 12;
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, sFldName)) = (dsoGrid.recordset[sFldName].value == null ? '' : dsoGrid.recordset[sFldName].value);
            }

            fg.IsSubTotal(fg.Row) = true;
            fg.RowOutlineLevel(fg.Row) = dsoGrid.recordset['Nivel'].value - 1;

            dsoGrid.recordset.MoveNext();
        }
    }

    fg.GridColor = 0XC0C0C0;

    paintGrid();

    glb_GridIsBuilding = false;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if (fg.Rows > 1)
        fg.Row = 1;

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;

    lockControlsInModalWin(false);

    setupBtns();
    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
    }
    else {
        ;
    }
}

function paintGrid() {
    var nTipoDetalhamentoID;
    var nTipoRegistro;
    var aMask1 = ['', '99', '999999', '', '999999999.99', '999999999.99', '', '', '', '', '', '', '', '', '', '', ''];
    var aMask2 = ['', '##', '######', '', '(###,###,##0.00)', '(###,###,##0.00)', '', '', '', '', '', '', '', '', '', '', ''];
    var i, j;
    var sFldName = '';

    if (dsoGrid.recordset.Fields.Count > glb_nFixedColumns) {
        for (i = glb_nFixedColumns; i < dsoGrid.recordset.Fields.Count; i++) {
            aMask1[aMask1.length] = '999999999.99';
            aMask2[aMask2.length] = '(###,###,##0.00)';
        }
    }

    putMasksInGrid(fg, aMask1, aMask2);

    //alignColsInGrid(fg,[1,2,3,4]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);

    for (i = 1; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Saldo')) < 0) {
            fg.Select(i, getColIndexByColKey(fg, 'Saldo'), i, getColIndexByColKey(fg, 'Saldo'));
            fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }

        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')) < 0) {
            fg.Select(i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor'));
            fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }


        for (j = glb_nFixedColumns; j < dsoGrid.recordset.Fields.Count; j++) {
            sFldName = fg.ColKey(j + 6);

            if (fg.ValueMatrix(i, getColIndexByColKey(fg, sFldName)) < 0) {
                fg.Select(i, getColIndexByColKey(fg, sFldName), i, getColIndexByColKey(fg, sFldName));
                fg.FillStyle = 1;
                fg.CellForeColor = 0X0000FF;
            }
        }

        nTipoDetalhamentoID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoDetalhamentoID'));
        nTipoRegistro = fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoRegistro'));

        if ((nTipoRegistro == 1) &&
			 ((nTipoDetalhamentoID >= 1532 && nTipoDetalhamentoID <= 1543) ||
			 (fg.ValueMatrix(i, getColIndexByColKey(fg, 'AceitaLancamento')) != 0))) {
            fg.Select(i, getColIndexByColKey(fg, 'Conta'), i, getColIndexByColKey(fg, 'Conta'));
            fg.FillStyle = 1;
            fg.CellForeColor = 0XFF0000;
        }
        else if ((nTipoRegistro == 2) && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'AceitaLancamento')) != 0)) {
            fg.Select(i, getColIndexByColKey(fg, 'Conta'), i, getColIndexByColKey(fg, 'Conta'));
            fg.FillStyle = 1;
            fg.CellForeColor = 0X008000;
        }
        else if ((nTipoRegistro == 3) && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'AceitaLancamento')) != 0)) {
            fg.Select(i, getColIndexByColKey(fg, 'Conta'), i, getColIndexByColKey(fg, 'Conta'));
            fg.FillStyle = 1;
            fg.CellForeColor = 0X808080;
        }
        else if ((nTipoRegistro == 4) && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'AceitaLancamento')) != 0)) {
            fg.Select(i, getColIndexByColKey(fg, 'Conta'), i, getColIndexByColKey(fg, 'Conta'));
            fg.FillStyle = 1;
            fg.CellForeColor = 0X000080;
        }
    }
}

/********************************************************************
Localiza a linha do grid por ContaMaeID
********************************************************************/
function findRowByContaMae(nContaMaeID) {
    var i = 0;

    for (i = 0; i < fg.Rows; i++) {
        if (parseInt(fg.TextMatrix(i, getColIndexByColKey(fg, 'ContaID')), 10) == parseInt(nContaMaeID))
            return i;
    }

    return fg.Rows - 1;
}

function detalharLancamento() {
    var nTipoRegistro = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoRegistro'));
    var nLancamentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'LancamentoID'));

    if ((nLancamentoID != null) && (nLancamentoID != ''))
        sendJSCarrier(getHtmlId(), 'SHOWLANCAMENTO', new Array(glb_aEmpresaData[0], nLancamentoID));
}

function js_fg_DblClick() {
    if (fg.Row < 1)
        return true;
    else
        detalharConta();
}

function fg_modalbalver_AfterRowColChange() {
    if (glb_GridIsBuilding)
        return true;

    txtObservacoes.value = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Observacoes'));
    setupBtns();
}

function detalharConta() {
    var nTipoRegistro = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoRegistro'));
    var nLancamentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'LancamentoID'));
    var nAgruparPor = 0;
    var nTipo = 0;

    if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'AceitaLancamento')) == 0) {
        var node = fg.GetNode();

        try {
            node.Expanded = !node.Expanded;
        }
        catch (e) {
            ;
        }
    }
    else {
        if (!verificaData())
            return null;

        var nTipoDetalhamentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoDetalhamentoID'));

        if ((nTipoRegistro == 2) || (nTipoRegistro == 3) || (nTipoRegistro == 4)) {
            var nLancamentos = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Lan�amentos'));

            if ((!isNaN(nLancamentos)) && (nLancamentos > 1000)) {
                var _retMsg = window.top.overflyGen.Confirm('Esta conta possui ' + nLancamentos.toString() +
					' lan�amentos.\nO detalhamento pode demorar um pouco.\n' +
					'Deseja detalhar assim mesmo?');

                if (_retMsg != 1) {
                    lockControlsInModalWin(false);
                    return null;
                }
            }
        }

        // consulta dado na linha se ja expandida
        if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ContaExpandida')) == 1)
            deletaContasFilhas();

        lockControlsInModalWin(true);
        setConnection(dsoDetalhamento);

        glb_bLancamento = false;
        glb_bLanConta = false;
        glb_bAgrupamento = false;

        var nContaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ContaID'));
        var nDetalheID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'DetalheID'));
        var nLancamentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'LancamentoID'));
        var nAgrupamento = parseInt(selAgruparPor.value, 10);

        if ((nDetalheID == null) || (nDetalheID == ''))
            nDetalheID = 0;

        if ((nLancamentoID == null) || (nLancamentoID == ''))
            nLancamentoID = 0;

        var sDataInicio;
        var sDataFim;

        if (nTipoRegistro != 3) {
            sDataInicio = trimStr(txtDataInicio.value);
            sDataFim = trimStr(txtDataFim.value);
        }
        else {
            sDataInicio = trimStr(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'dtInicio')));
            sDataFim = trimStr(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'dtFim')));
        }

        if (sDataInicio != '')
            sDataInicio = normalizeDate_DateTime(sDataInicio, 1);

        if (sDataFim != '')
            sDataFim = normalizeDate_DateTime(sDataFim, 1);

        var sEmpresas = '/';
        var strPars = new String();

        for (i = 0; i < selEmpresas.length; i++) {
            if (selEmpresas.options[i].selected == true)
                sEmpresas += selEmpresas.options[i].value + '/';
        }

        var strSQL = '';

        var strPars = '?sEmpresas=' + escape(sEmpresas);
        strPars += '&nContaID=' + escape(nContaID);
        strPars += '&sDataInicio=' + escape(sDataInicio);
        strPars += '&sDataFim=' + escape(sDataFim);
        strPars += '&nDetalheID=' + escape(nDetalheID);
        strPars += '&nLancamentoID=' + escape(nLancamentoID);
        //Se tiver marcado, passa o valor 3001 (sim), se n�o estiver passa vazio (para trazer todos)
        //Isso foi feito para manter a compatibilidade da l�gica da procedure tamb�m utilizada na modal Raz�o FSM - 25/08/2012
        strPars += '&nLancamentosConciliados=' + escape((chkLancConciliados.checked ? '3001' : ''));
        strPars += '&nTemLancamentos=' + escape((chkLancamentos.checked ? '1' : '0'));
        strPars += '&nTemSaldo=' + escape((chkSaldo.checked ? '1' : '0'));

        // Conta normal
        if (nTipoRegistro == 1) {
            if (nTipoDetalhamentoID >= 1532 && nTipoDetalhamentoID <= 1543) {
                nTipo = 3;
                nAgruparPor = glb_nAgruparPorClicked;
            }
            else {
                if (nAgrupamento == 0) {
                    nTipo = 1;
                    glb_bLancamento = true;
                }
                else {
                    nTipo = nAgrupamento + 3;
                    glb_bAgrupamento = true;
                }
            }
        }
            // Conta Detalhe
        else if (nTipoRegistro == 2) {
            if (nAgrupamento == 0) {
                nTipo = 1;
                glb_bLancamento = true;
            }
            else {
                nTipo = nAgrupamento + 3;
                glb_bAgrupamento = true;
            }
        }
            // Conta Agrupada
        else if (nTipoRegistro == 3) {
            nTipo = 1;
            glb_bLancamento = true;
        }
            // Lancamento
        else if (nTipoRegistro == 4) {
            nTipo = 2;
            glb_bLanConta = true;
            glb_bLancamento = true;
        }

        strPars += '&nTipo=' + escape(nTipo);
        strPars += '&nAgruparPor=' + escape(nAgruparPor);
        strPars += '&nMoedaID=' + escape(selMoeda.value);
        strPars += '&nMilhar=' + escape((chkMilhar.checked ? 1 : 0));

        // guarda dado na linha se ja expandida
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ContaExpandida')) = 1;

        if (nTipo == 2) {
            glb_bDetalhesLancamento = true;
            dsoDetalhamento.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/planocontas/serverside/lancamentodetalhe.aspx' + strPars;
        }
        else
            dsoDetalhamento.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/planocontas/serverside/contabilidaderazao.aspx' + strPars;

        dsoDetalhamento.ondatasetcomplete = dsoDetalhamento_DSC;
        dsoDetalhamento.refresh();
    }
}

function dsoDetalhamento_DSC() {
    var sLineData = '';
    var nRowToInsInGrid = fg.Row + 1;
    var nOutLineLevel = fg.RowOutlineLevel(fg.Row) + 1;
    var nNivelToIns = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Nivel')) + 1;
    var nContaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ContaID'));
    var nDetalheID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'DetalheID'));
    var nSaldo = (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Saldo')) == '' ? 0 : fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Saldo')));

    if (!(dsoDetalhamento.recordset.BOF && dsoDetalhamento.recordset.EOF)) {
        glb_GridIsBuilding = true;
        fg.Redraw = 0;
        while (!dsoDetalhamento.recordset.EOF) {
            // Ocorreu erro no servidor
            if (dsoDetalhamento.recordset.Fields[0].name == 'Msg') {
                if (window.top.overflyGen.Alert(dsoDetalhamento.recordset['Msg'].value) == 0)
                    return null;

                break;
            }

            //nSaldo -= (dsoDetalhamento.recordset['Valor'].value == null ? 0 : dsoDetalhamento.recordset['Valor'].value);
            fg.AddItem('', nRowToInsInGrid);

            fg.ColDataType(getColIndexByColKey(fg, 'Conta')) = 12;
            fg.ColDataType(getColIndexByColKey(fg, 'Saldo')) = 12;
            fg.ColDataType(getColIndexByColKey(fg, 'TipoRegistro')) = 12;
            fg.ColDataType(getColIndexByColKey(fg, 'AceitaLancamento')) = 11;
            fg.ColDataType(getColIndexByColKey(fg, 'DetalheID')) = 12;

            if (!glb_bLancamento) {
                fg.ColDataType(getColIndexByColKey(fg, 'Lan�amentos')) = 12;
                fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'Lan�amentos')) = dsoDetalhamento.recordset['Lan�amentos'].value;

                fg.ColDataType(getColIndexByColKey(fg, 'SimboloMoeda')) = 12;
                fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'SimboloMoeda')) = dsoDetalhamento.recordset['Moeda'].value;

                fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'AceitaLancamento')) = 1;

                fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'Conta')) = dsoDetalhamento.recordset['Detalhe'].value == null ? '' :
					dsoDetalhamento.recordset['Detalhe'].value;

                if (!glb_bAgrupamento) {
                    fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'TipoRegistro')) = 2;

                    fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'DetalheID')) = dsoDetalhamento.recordset['DetalheID'].value == null ? '' :
						dsoDetalhamento.recordset['DetalheID'].value;

                    if (glb_nAgruparPorClicked > 0) {
                        for (i = glb_nFixedColumnsDetail; i < dsoDetalhamento.recordset.Fields.Count; i++) {
                            sFldName = dsoDetalhamento.recordset.Fields[i].Name;
                            fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, sFldName)) = (dsoDetalhamento.recordset[sFldName].value == null ? '' : dsoDetalhamento.recordset[sFldName].value);

                            if (fg.ValueMatrix(nRowToInsInGrid, getColIndexByColKey(fg, sFldName)) < 0) {
                                fg.Select(nRowToInsInGrid, getColIndexByColKey(fg, sFldName), nRowToInsInGrid, getColIndexByColKey(fg, sFldName));
                                fg.FillStyle = 1;
                                fg.CellForeColor = 0X0000FF;
                            }
                        }
                    }
                }
                else {
                    fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'TipoRegistro')) = 3;

                    fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'DetalheID')) = nDetalheID;

                    fg.ColDataType(getColIndexByColKey(fg, 'dtInicio')) = 12;
                    fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'dtInicio')) = dsoDetalhamento.recordset['dtInicio'].value;
                    fg.ColDataType(getColIndexByColKey(fg, 'dtFim')) = 12;
                    fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'dtFim')) = dsoDetalhamento.recordset['dtFim'].value;
                }
            }
            else {
                fg.ColDataType(getColIndexByColKey(fg, 'Lan�amentos')) = 12;
                fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'DetalheID')) = nDetalheID;

                fg.ColDataType(getColIndexByColKey(fg, 'SimboloMoeda')) = 12;
                fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'SimboloMoeda')) = dsoDetalhamento.recordset['Moeda'].value;

                if (!glb_bLanConta) {
                    fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'Lan�amentos')) = dsoDetalhamento.recordset['Lan�amentos'].value;
                    fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'AceitaLancamento')) = 1;
                    fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'TipoRegistro')) = 4;
                }
                else {
                    fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'Lan�amentos')) = 0;
                    fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'AceitaLancamento')) = 0;
                    fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'TipoRegistro')) = 5;
                }

                fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'Conta')) = dsoDetalhamento.recordset['Lancamento'].value == null ? '' :
					dsoDetalhamento.recordset['Lancamento'].value;

                fg.ColDataType(getColIndexByColKey(fg, 'LancamentoID')) = 12;
                fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'LancamentoID')) = dsoDetalhamento.recordset['LancamentoID'].value == null ? '' :
					dsoDetalhamento.recordset['LancamentoID'].value;
            }

            fg.ColDataType(getColIndexByColKey(fg, 'Nivel')) = 12;
            fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'Nivel')) = nNivelToIns.toString();

            fg.ColDataType(getColIndexByColKey(fg, 'Valor')) = 12;
            fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'Valor')) = dsoDetalhamento.recordset['Valor'].value;

            fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'Saldo')) = dsoDetalhamento.recordset['Saldo'].value == null ? '' :
				dsoDetalhamento.recordset['Saldo'].value;

            if (glb_bDetalhesLancamento) {
                fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'Observacao')) = dsoDetalhamento.recordset['Observacao'].value == null ? '' :
					dsoDetalhamento.recordset['Observacao'].value;
            }

            fg.ColDataType(getColIndexByColKey(fg, 'ContaID')) = 12;
            fg.TextMatrix(nRowToInsInGrid, getColIndexByColKey(fg, 'ContaID')) = nContaID;

            fg.IsSubTotal(nRowToInsInGrid) = true;
            fg.RowOutlineLevel(nRowToInsInGrid) = nOutLineLevel;

            nRowToInsInGrid++;

            dsoDetalhamento.recordset.MoveNext();
        }

        fg.Redraw = 2;
    }

    if (glb_bDetalhesLancamento)
        glb_bDetalhesLancamento = false;

    paintGrid();

    glb_GridIsBuilding = false;

    lockControlsInModalWin(false);

    setupBtns();
}

function setupBtns() {
    var nTipoRegistro;
    var nLancamentoID;

    btnConta.disabled = true;
    btnDetalhar.disabled = true;
    btnImprime.disabled = (fg.Rows <= 1);
    if (fg.Rows > 1) {
        nTipoRegistro = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoRegistro'));
        nLancamentoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'LancamentoID'));

        if (nTipoRegistro != 5)
            btnConta.disabled = false;

        if ((nLancamentoID != null) && (nLancamentoID != ''))
            btnDetalhar.disabled = false;
    }
}
function deletaContasFilhas() {
    var nRow, nTipoRegistro;

    nRow = fg.Row;
    nTipoRegistro = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoRegistro'));

    if (nRow <= fg.Rows - 2) {
        nRow++;

        while ((nRow <= fg.Rows - 1) && (nTipoRegistro < fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'TipoRegistro'))))
            fg.RemoveItem(nRow);
    }
}

function fillGlbArrays() {
    var i = 0;
    var nCounter = 0;

    for (i = 0; i < glb_aMoedasEmpresas.length; i++) {
        if (ascan(glb_aMoedas, glb_aMoedasEmpresas[i][1], false) < 0) {
            glb_aMoedas[nCounter] = glb_aMoedasEmpresas[i][1];
            nCounter++;
        }
    }
}

function fillCmbMoeda() {
    selMoeda.disabled = true;
    selEmpresas_onchange();
}

function selMoeda_onchange() {
    fg.Rows = 1;
    setupBtns();
    adjustLabelsCombos();
}

function selTipoDetalhamentoID_onchange() {
    adjustLabelsCombos();
}

function selEmpresas_onchange() {
    var bAddMoeda = true;
    var i = 0;
    var j = 0;
    var nFind = 0;
    var nMoedaSelected = null;
    var nCount = 0;

    fg.Rows = 1;

    if (selMoeda.selectedIndex >= 0)
        nMoedaSelected = selMoeda.value;

    clearComboEx(['selMoeda']);

    if (selEmpresas.selectedIndex < 0) {
        selMoeda.disabled = true;
        btnFillGrid.disabled = true;
        setupBtns();
        return true;
    }
    else {
        selMoeda.disabled = false;
        btnFillGrid.disabled = false;
    }
    btnExcel.src = glb_LUPA_IMAGES[btnFillGrid.disabled ? 1 : 0].src;

    for (i = 0; i < glb_aMoedas.length; i++) {
        bAddMoeda = true;
        for (j = 0; j < selEmpresas.length; j++) {
            if (selEmpresas.options[j].selected == true) {
                bAddMoeda = (bAddMoeda && (bFindMoedaEmpresa(glb_aMoedas[i], selEmpresas.options[j].value) >= 0));
            }
        }

        if (bAddMoeda) {
            nFind = bFindMoedaEmpresa(glb_aMoedas[i]);
            var optionStr = glb_aMoedasEmpresas[nFind][2];
            var optionValue = glb_aMoedasEmpresas[nFind][1];
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            selMoeda.add(oOption);

            if (optionValue == nMoedaSelected)
                nMoedaSelected = nCount;

            nCount++;
        }
    }

    selMoeda.disabled = (selMoeda.options.length == 0);

    if (selMoeda.options.length == 1)
        selMoeda.selectedIndex = 0;
    else if (nMoedaSelected != null)
        selMoeda.selectedIndex = nMoedaSelected;

    adjustLabelsCombos();
    setupBtns();
}

function bFindMoedaEmpresa(nMoedaID, nEmpresaID) {
    var i = 0;
    var nRetVal = -1;

    for (i = 0; i < glb_aMoedasEmpresas.length; i++) {
        if (nEmpresaID == null) {
            if (glb_aMoedasEmpresas[i][1] == nMoedaID) {
                nRetVal = i;
                break;
            }
        }
        else if ((glb_aMoedasEmpresas[i][0] == nEmpresaID) && (glb_aMoedasEmpresas[i][1] == nMoedaID)) {
            nRetVal = i;
            break;
        }

    }

    return nRetVal;
}

/********************************************************************
Usuario cliclou o botao esquerdo do mouse
********************************************************************/
function btn_onmousedownExcel() {
    if (event.button == 1)
        btnFillGrid_onclick('btnExcel');
}

function sendToExcel() {
    var strSQL = buildStrSQL(2);

    if (strSQL == '')
        return null;

    if (btnExcel.src == glb_LUPA_IMAGES[1].src)
        return null;

    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.onreadystatechange = reports_onreadystatechange;

    frameReport.contentWindow.location = strSQL;
}

function adjustLabelsCombos() {
    setLabelOfControl(lblMoeda, selMoeda.value);
    setLabelOfControl(lblTipoDetalhamentoID, selTipoDetalhamentoID.value);
}

function getCurrDate() {
    var d, s = "";
    d = new Date();

    if (DATE_SQL_PARAM == 103) {
        s += padL(d.getDate().toString(), 2, '0') + "/";
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
    }
    else {
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
        s += padL(d.getDate().toString(), 2, '0') + "/";
    }

    s += d.getYear() + " ";
    s += padL(d.getHours().toString(), 2, '0') + ":";
    s += padL(d.getMinutes().toString(), 2, '0');

    return (s);
}

function imprimeGrid() {
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var sMsg = aEmpresaData[6] + '     ' + translateTerm('Balancete de Verifica��o', null) + '     ' +
		txtDataInicio.value + ' - ' + txtDataFim.value + '     ' + getCurrDate();
    var gridLine = fg.gridLines;
    fg.gridLines = 2;

    var oldColWidthValor = fg.ColWidth(getColIndexByColKey(fg, 'Valor'));
    var oldColWidthSaldo = fg.ColWidth(getColIndexByColKey(fg, 'Saldo'));
    fg.ColWidth(getColIndexByColKey(fg, 'Valor')) = oldColWidthValor * 1.30;
    fg.ColWidth(getColIndexByColKey(fg, 'Saldo')) = oldColWidthSaldo * 1.30;

    for (i = glb_nFixedColumns; i < fg.Cols - 6; i++)
        fg.ColWidth(i + 6) = 1600;

    fg.PrintGrid(sMsg, false, 2, 0, 450);

    fg.ColWidth(getColIndexByColKey(fg, 'Valor')) = oldColWidthValor;
    fg.ColWidth(getColIndexByColKey(fg, 'Saldo')) = oldColWidthSaldo;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.gridLines = gridLine;
    fg.Redraw = 1;
}

function reports_onreadystatechange()
{
    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
    (frameReport.contentWindow.document.readyState == 'interactive') ||
    (frameReport.contentWindow.document.readyState == 'complete'))
    {
        lockControlsInModalWin(false);
    }
}