
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalbalverHtml" name="modalbalverHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/planocontas/modalpages/modalbalver.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
 
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/planocontas/modalpages/modalbalver.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nModalType, sCurrDateFormat, nUserID, nEmpresaID
Dim rsData, strSQL

sCaller = ""
nModalType = 0
nUserID = 0
nEmpresaID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nModalType").Count
    nModalType = Request.QueryString("nModalType")(i)
Next

For i = 1 To Request.QueryString("nUserID").Count
    nUserID = Request.QueryString("nUserID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nModalType = " & CStr(nModalType) & ";"
Response.Write vbcrlf


Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT DISTINCT b.EmpresaID, d.MoedaID, e.SimboloMoeda " & _
		 "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), RelacoesPesRec c WITH(NOLOCK), RelacoesPesRec_Moedas d WITH(NOLOCK), Conceitos e WITH(NOLOCK) " & _
		 "WHERE ((a.SujeitoID = " & CStr(nUserID) & ") AND (a.TipoRelacaoID = 11) AND (a.ObjetoID = 999) AND (a.RelacaoID = b.RelacaoID) AND " & _
			"(b.EmpresaID = c.SujeitoID) AND (c.TipoRelacaoID = 12) AND (c.ObjetoID = 999) AND (c.EstaEmOperacao = 1) AND (c.RelacaoID = d.RelacaoID) AND " & _
			"(d.MoedaID = e.ConceitoID)) " & _
		 "ORDER BY b.EmpresaID, e.SimboloMoeda"

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write "var glb_aMoedasEmpresas = new Array();"
Response.Write vbcrlf

i = 0
While Not rsData.EOF
        
    'glb_arrayEstado[i] = new Array(12, 'Bahia', 'BA');
        
    Response.Write "glb_aMoedasEmpresas[" & CStr(i) & "] = new Array("
    Response.Write CStr(rsData.Fields("EmpresaID").Value)
    Response.Write ","
    Response.Write CStr(rsData.Fields("MoedaID").Value)
    Response.Write ","
    Response.Write Chr(34)
    Response.Write CStr(rsData.Fields("SimboloMoeda").Value)
    Response.Write Chr(34)
    Response.Write ");"
    Response.Write vbcrlf
    
    rsData.MoveNext
    i = i + 1
Wend

rsData.Close

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
 fg_AfterRowColChange();
 fg_modalbalver_AfterRowColChange();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
	js_fg_DblClick();
//-->
</SCRIPT>

<script ID="vbFunctions" LANGUAGE="vbscript">
<!--
'Parametro tipo
'yyyy Year 
'q Quarter 
'm Month 
'y Day of year 
'd Day 
'w Weekday 
'ww Week of year 
'h Hour 
'n Minute 
's Second
Function periodo(tipo, date1, date2)
    periodo = DateDiff(tipo,date1,date2)
End Function
//-->
</script>

</head>

<body id="modalbalverBody" name="modalbalverBody" LANGUAGE="javascript" onload="return window_onload()">

    
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <!-- Div dos controles -->
    <div id="divControles" name="divControles" class="divGeneral">
		<p id="lblEmpresas" name="lblEmpresas" class="lblGeneral">Empresas</p>
        <select id="selEmpresas" name="selEmpresas" class="fldGeneral" MULTIPLE>
<%
	strSQL = "SELECT DISTINCT b.EmpresaID, d.Fantasia " & _
			"FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), RelacoesPesRec c WITH(NOLOCK), Pessoas d WITH(NOLOCK) " & _
			"WHERE ((a.SujeitoID = " & CStr(nUserID) & ") AND (a.TipoRelacaoID = 11) AND (a.ObjetoID = 999) AND " & _ 
				"(a.RelacaoID = b.RelacaoID) AND (b.EmpresaID = c.SujeitoID) AND " & _
				"(c.EstadoID = 2) AND (c.TipoRelacaoID = 12) AND (c.ObjetoID = 999) AND (c.EstaEmOperacao = 1) AND " & _
				"(c.SujeitoID = d.PessoaID)) " & _
			"ORDER BY d.Fantasia"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	While (Not rsData.EOF)
		Response.Write "<option value =" & rsData.Fields("EmpresaID").Value 
		
		If (CLng(nEmpresaID) = CLng(rsData.Fields("EmpresaID").Value)) Then
			Response.Write " SELECTED "
		End If
		
		Response.Write ">" & rsData.Fields("Fantasia").Value & "</option>" & _
			chr(13) & chr(10)

		rsData.MoveNext
	WEnd

	rsData.Close
%>		
        </select>
        <p id="lblMoeda" name="lblMoeda" class="lblGeneral">Moeda</p>
        <select id="selMoeda" name="selMoeda" class="fldGeneral"></select>
    
		<p id="lblAgruparPor" name="lblAgruparPor" class="lblGeneral" title="Agrupar lan�amentos por">Agrupar por</p>
        <select id="selAgruparPor" name="selAgruparPor" class="fldGeneral"></select>
		<p id="lblNivel" name="lblNivel" class="lblGeneral">N�vel</p>
        <select id="selNivel" name="selNivel" class="fldGeneral"></select>
        <p id="lblDica" name="lblDica" class="lblGeneral" title="Dicas de uso da conta?">Dicas</p>
        <input type="checkbox" id="chkDica" name="chkDica" class="fldGeneral" title="Dicas de uso da conta?"></input>
        <p id="lblLancConciliados" name="lblLancConciliados" class="lblGeneral" title="S� lan�amentos conciliados?">Conc</p>
        <input type="checkbox" id="chkLancConciliados" name="chkLancConciliados" class="fldGeneral" title="S� lan�amentos conciliados?"></input>
        <p id="lblSaldo" name="lblSaldo" class="lblGeneral">Sald</p>
        <input type="checkbox" id="chkSaldo" name="chkSaldo" class="fldGeneral" title="S� contas com saldo direfente de zero?">
        <p id="lblLancamentos" name="lblLancamentos" class="lblGeneral">Lan�</p>
        <input type="checkbox" id="chkLancamentos" name="chkLancamentos" class="fldGeneral" title="S� contas que tiveram lan�amentos no per�odo?">
        <p id="lblContasPatrimoniais" name="lblContasPatrimoniais" class="lblGeneral">Patr</p>
        <input type="checkbox" id="chkContasPatrimoniais" name="chkContasPatrimoniais" class="fldGeneral" title="Contas Patrimoniais?">
        <p id="lblContasResultado" name="lblContasResultado" class="lblGeneral">Res</p>
        <input type="checkbox" id="chkContasResultado" name="chkContasResultado" class="fldGeneral" title="Contas de Resultado?">
        <p id="lblMilhar" name="lblMilhar" class="lblGeneral">Mil</p>
        <input type="checkbox" id="chkMilhar" name="chkMilhar" class="fldGeneral" title="Agrupar em milhar?">
        <p id="lblDataInicio" name="lblDataInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio" name="txtDataInicio" class="fldGeneral" LANGUAGE="javascript" onfocus="return __selFieldContent(this)">
        <p id="lblDataFim" name="lblDataFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim" name="txtDataFim" class="fldGeneral" LANGUAGE="javascript" onfocus="return __selFieldContent(this)">
		<p id="lblTipoDetalhamentoID" name="lblTipoDetalhamentoID" class="lblGeneral">Detalhamento</p>
        <select id="selTipoDetalhamentoID" name="selTipoDetalhamentoID" class="fldGeneral">
<%
	strSQL = "SELECT 0 AS ItemID, SPACE(0) AS ItemMasculino, 0 AS Ordem " & _
			"UNION ALL SELECT ItemID, ItemMasculino, Ordem " & _
			"FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
			"WHERE (EstadoID=2 AND TipoID=904) " & _
			"ORDER BY Ordem"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	While (Not rsData.EOF)
		Response.Write "<option value =" & rsData.Fields("ItemID").Value 
		
		Response.Write ">" & rsData.Fields("ItemMasculino").Value & "</option>" & _
			chr(13) & chr(10)

		rsData.MoveNext
	WEnd

	rsData.Close
	Set rsData = Nothing
%>		
        </select>
        <input type="button" id="btnConta" name="btnConta" value="Conta" LANGUAGE="javascript" onclick="return btn_onclick(this)" title="Detalhar conta" class="btns"></input>
        <input type="button" id="btnDetalhar" name="btnDetalhar" value="Lan�amento" LANGUAGE="javascript" onclick="return btn_onclick(this)" title="Detalhar lan�amento" class="btns"></input>
        <input type="button" id="btnFillGrid" name="btnFillGrid" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnImprime" name="btnImprime" value="Imprimir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="image" id="btnExcel" name="btnExcel" class="fldGeneral" title="Exportar para Excel" LANGUAGE="javascript" onmousedown="return btn_onmousedownExcel()" WIDTH="24" HEIGHT="23"></input>
    </div>    
    
    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
	<textarea id="txtObservacoes" name="txtObservacoes" class="fldGeneral"></textarea>

    <input type="button" id="btnOK" name="btnOK" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">

    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>

</body>

</html>
