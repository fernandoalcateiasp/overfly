<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing

%>

<html id="modalconciliacaoHtml" name="modalconciliacaoHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/planocontas/modalpages/modalconciliacao.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
        
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/planocontas/modalpages/modalconciliacao.js" & Chr(34) & "></script>" & vbCrLf
%>


<%

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nInvoiceID, sCaller, rsData, strSQL

sCaller = ""
nInvoiceID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nInvoice").Count    
    nInvoiceID = Request.QueryString("nInvoice")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nInvoiceID = " & CStr(nInvoiceID) & ";"
Response.Write vbcrlf

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>


<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
    js_modalconciliacao_BeforeEdit(fg, arguments[0], arguments[1]);
    //-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
    js_modalconciliacao_AfterEdit(fg, arguments[0], arguments[1]);
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
    js_fg_modalconciliacaoDblClick(fg, fg.Row, fg.Col);
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
<!--
    js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
//Atencao programador:
//Setimo parametro abaixo = true, permite ao usuario mudar de linha em grid
//clicando em celula read only.
//Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
    js_BeforeRowColChangeModalConciliacao (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
    js_fg_modalconciliacaoAfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
    
<script LANGUAGE="javascript" FOR="txtInicio" EVENT="onkeydown">
<!--
    txtInicio_onkeydown();
    //-->
</script>

<script LANGUAGE="javascript" FOR="txtFim" EVENT="onkeydown">
<!--
    txtFim_onkeydown();
    //-->
</script>

</SCRIPT>

</head>

<body id="modalconciliacaoBody" name="modalconciliacaoBody" LANGUAGE="javascript" onload="return window_onload()">

    <div id="divPesquisa" name="divPesquisa" class="divGeneral">
         <p id="lblInicio" name="lblInicio" class="lblGeneral">Inicio</p> 
        <input type="text" id="txtInicio" name="txtInicio" class="fldGeneral" title="Inicio"></input> 
        <p id="lblFim" name="lblFim" class="lblGeneral">Fim</p> 
        <input type="text" id="txtFim" name="txtFim" class="fldGeneral" title="Fim"></input> 

        <p id="lblBusca" name="lblBusca" class="lblGeneral">Busca</p> 
        <input type="text" id="txtBusca" name="txtBusca" class="fldGeneral" title="Busca"></input> 

        <p id="lblRegistro" name="lblRegistro" class="lblGeneral">Registro</p> 
        <input type="text" id="txtRegistro" name="txtRegistro" class="fldGeneral" title="Registro "></input> 
        
        <p id="lblTipoErro" name="lblTipoErro" class="lblGeneral" title="Erro: 0-Irrelevante, 1-Usu�rio, 2-TI">Erro</p>
        <select id="selTipoErro" name="selTipoErro" class="fldGeneral" title="Erro: 0-Irrelevante, 1-Usu�rio, 2-TI" onchange="return onchange_selTipoErro(this)"></select>
        
        <p id="lblEmpresaID" name="lblEmpresaID" class="lblGeneral">Empresa</p>
        <select id="selEmpresaID" name="selEmpresaID" class="fldGeneral" onchange="return onchange_selEmpresaID(this)"></select>
        <input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btnListar_onclick(this)" class="btns">
        <p id="lblDetalhes" name="lblDetalhes" title="Mostrar detalhes de integra��o?" class="lblGeneral">Det</p>
        <input type=checkbox id="chkDetalhes" name="chkDetalhes" title="Mostrar detalhes de integra��o?" class="fldGeneral"></input>
        <!--
         <p id="lblIndice" name="lblIndice" title="S� inconsist�ncias de �ndice ou todas?" class="lblGeneral">Ind</p>
        <input type=checkbox id="chkIndice" name="chkIndice" title="S� inconsist�ncias de �ndice ou todas?" class="fldGeneral"></input>
        -->
        <input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" onclick="return btnGravar_onclick(this)" class="btns">
        <p id="lblCorretor" name="lblCorretor" class="lblGeneral">Corretor</p>
        <select id="selCorretor" name="selCorretor" class="fldGeneral" onchange="return onchange_selCorretor(this)">
            <option selected value=0></option>
            <option value=Empresa>Empresa</option>
            <option value=Contador>Contador</option>
        </select>
        <p id="lblCorrecao" name="lblCorrecao" class="lblGeneral">Corre��o</p>
        <select id="selCorrecao" name="selCorrecao" class="fldGeneral" onchange="return onchange_selCorrecao(this)"></select>
        <input type="button" id="btnRegOrig" name="btnRegOrig" value="Registro" LANGUAGE="javascript" onclick="return btnRegOrig_onclick(this)" class="btns">
        <input type="button" id="btnExcel" name="btnExcel" value="Excel" LANGUAGE="javascript" onclick="return btnExcel_onclick(this)" class="btns">
    </div>    

     <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>
    
     <div id="divFGErros" name="divFGErros" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgErros" name="fgErros" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
    
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>

</body>

</html>
