
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalprintHtml" name="modalprintHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/planocontas/modalpages/modalprint.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_progressbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodalprint.js" & Chr(34) & "></script>" & vbCrLf

    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/planocontas/modalpages/modalprint_planocontas.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Dim i, sCaller, nEmpresaID, sEmpresaFantasia, nContextoID, nUserID
Dim relatorioID, relatorio, EhDefault, relatorioFantasia, dirA1, dirA2, dirA1A2True, rsData3
Dim currDateFormat, nIdiomaID

sCaller = ""
nEmpresaID = 0
sEmpresaFantasia = ""
nContextoID = 0
nUserID = 0
nIdiomaID = 0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

'nEmpresaID - ID da empresa logada
For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

'sEmpresaFantasia - fantasia da empresa logada
For i = 1 To Request.QueryString("sEmpresaFantasia").Count    
    sEmpresaFantasia = Request.QueryString("sEmpresaFantasia")(i)
Next

'nContextoID - ID do contexto
For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

'nUserID - ID do usuario logado
For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

'Formato corrente de data
For i = 1 To Request.QueryString("currDateFormat").Count
    currDateFormat = Request.QueryString("currDateFormat")(i)
Next

For i = 1 To Request.QueryString("nIdiomaID").Count
    nIdiomaID = Request.QueryString("nIdiomaID")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & nEmpresaID & ";"
Response.Write vbcrlf

Response.Write "var glb_sEmpresaFantasia = " & Chr(39) & sEmpresaFantasia & Chr(39) & ";"
Response.Write vbcrlf

If (currDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';"
    Response.Write "var glb_dCurrDateIni = '" & Day(DateAdd("d", -3, Date)) & "/" & Month(DateAdd("d", -3, Date))& "/" & Year(DateAdd("d", -3, Date)) & "';"
ElseIf (currDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';"
    Response.Write "var glb_dCurrDateIni = '" & Month(DateAdd("d", -3, Date)) & "/" & Day(DateAdd("d", -3, Date))& "/" & Year(DateAdd("d", -3, Date)) & "';"
ElseIf (currDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';"
    Response.Write "var glb_dCurrDateIni = '" & Year(DateAdd("d", -3, Date)) & "/" & Month(DateAdd("d", -3, Date)) & "/" & Day(DateAdd("d", -3, Date)) & "';"
Else
    Response.Write "var glb_dCurrDate = '';"
End If

Response.Write vbcrlf

'Monta array dos relatorios
Response.Write "// Array dos relatorios, com seus ids, nomes"
Response.Write vbcrlf

Dim rsData, strSQL
Dim rsData1
Dim Matriz

If ( (sCaller) = "PL" ) Then
    i = 0
Else
    i = 1
End If        

' Pega a matriz da empresa logada.
Matriz = -1

Set rsData = Server.CreateObject("ADODB.Recordset")         

strSQL = "select convert(varchar(5), dbo.fn_Empresa_Matriz(" & nEmpresaID & ", 1, NULL)) as Matriz"

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Matriz = rsData.Fields("Matriz").Value

rsData.Close

' Pega as defini��es dos relat�rios.
strSQL = "SELECT DISTINCT d.RecursoID AS RelatorioID, dbo.fn_Tradutor(d.Recurso, 246, " & CStr(nIdiomaID) & ", 'A') AS Relatorio, " & _
         "c.EhDefault, d.RecursoFantasia AS RelatorioFantasia, c.Ordem, " & _
         "g.Alterar1 AS A1, g.Alterar2 AS A2 " & _
         "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), " & _
         "RelacoesPesRec e WITH(NOLOCK), RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK) " & _
         "WHERE a.ObjetoID= " & CStr(nContextoID) & " AND a.TipoRelacaoID=1 AND a.EstadoID=2 " & _
         "AND a.SujeitoID=b.RecursoID AND b.EstadoID=2 AND b.TipoRecursoID=4 " & _ 
         "AND b.Principal=1 AND b.RecursoID=c.ObjetoID AND c.EstadoID=2 " & _
         "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID " & _
         "AND d.EstadoID=2 AND d.TipoRecursoID=5 AND d.ClassificacaoID=14 AND d.EhDetalhe= " & CStr(i) & " " & _
         "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) WHERE a.RecursoID= " & CStr(nContextoID) & " )) " & _
         "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " & _
         "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " & _
         "AND e.SujeitoID IN ((SELECT " & CStr(nUserID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(nUserID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
         "AND e.RelacaoID=f.RelacaoID " & _
         "AND f.EmpresaID= " & CStr(nEmpresaID) & " AND g.ContextoID= " & CStr(nContextoID) & " " & _
         "AND c.ObjetoID=g.RecursoMaeID AND d.RecursoID=g.RecursoID " & _
         "AND (g.Consultar1=1 OR g.Consultar2=1) " & _
         "AND f.PerfilID=g.PerfilID " & _
         "ORDER BY c.Ordem"

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write vbcrlf
Response.Write "var glb_arrayReports = new Array();"
Response.Write vbcrlf
Response.Write vbcrlf
'------------------
i = 0
Do While (Not rsData.EOF)
	relatorioID = rsData.Fields("RelatorioID").Value
	relatorio = rsData.Fields("Relatorio").Value
	EhDefault = rsData.Fields("EhDefault").Value
	relatorioFantasia = rsData.Fields("RelatorioFantasia").Value
	dirA1 = rsData.Fields("A1").Value
	dirA2 = rsData.Fields("A2").Value
    
	If ( dirA1 AND dirA2 ) Then
		dirA1A2True = TRUE        
	Else
		dirA1A2True = FALSE    
	End If
        
	rsData.MoveNext
        
	Do While (Not rsData.EOF)
		If ( CLng(relatorioID) = CLng(rsData.Fields("RelatorioID").Value) ) Then
			'dirX1 e dirX2 e o anterior sempre
			If ( (rsData.Fields("A1").Value) AND (rsData.Fields("A2").Value) ) Then
				dirA1A2True = TRUE        
			End If

			dirA1 = dirA1 OR rsData.Fields("A1").Value
			dirA2 = dirA2 OR rsData.Fields("A2").Value
			rsData.MoveNext
		Else
			Exit Do
		End If    
	Loop    
        
	If ( (dirA1) AND (dirA2) ) Then
		If ( NOT (dirA1A2True)  ) Then
			dirA1 = FALSE
			dirA2 = TRUE
		End If
	End If    

	If ((Matriz <> nEmpresaID) AND ((relatorioID = 40302) OR (relatorioID = 40303) OR (relatorioID = 40304))) Then
	Else
		Response.Write "glb_arrayReports[" & CStr(i) & "] = new Array("
		Response.Write Chr(39)
		Response.Write CStr(relatorio)
		Response.Write Chr(39)
		Response.Write ","
		Response.Write CStr(relatorioID)
		Response.Write ","
		Response.Write LCase(CStr(EhDefault))
		Response.Write ","
		Response.Write Chr(39)
		Response.Write CStr(relatorioFantasia)
		Response.Write Chr(39)
		Response.Write ","
		Response.Write LCase(CStr(dirA1))
		Response.Write ","
		Response.Write LCase(CStr(dirA2))
		Response.Write ");"
		i = i + 1
	End If
Loop

rsData.Close

Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "</script>"

Response.Write vbcrlf

'--- Interface dos relatorios do form Relacoes entre Pessoas 
    
'strSQL = ""

'Set rsData1 = Server.CreateObject("ADODB.Recordset")         

'rsData1.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

'--- Final de Interface do Relatorios do form Relacoes entre Pessoas  

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>
    
</head>

<body id="modalprintBody" name="modalprintBody" LANGUAGE="javascript" onload="return window_onload()">
    
    
    <div id="divPlanoContas" name="divPlanoContas" class="divGeneral">
        <p id="lblEstado1" name="lblEstado1" class="lblGeneral">C</p>
        <input type="checkbox" id="chkEstado1" name="chkEstado1" class="fldGeneral" title="Cadastrado">
        <p id="lblEstado2" name="lblEstado2" class="lblGeneral">A</p>
        <input type="checkbox" id="chkEstado2" name="chkEstado2" class="fldGeneral" title="Ativo" checked>
        <p id="lblEstado3" name="lblEstado3" class="lblGeneral">S</p>
        <input type="checkbox" id="chkEstado3" name="chkEstado3" class="fldGeneral" title="Suspenso">
        <p id="lblEstado4" name="lblEstado4" class="lblGeneral">D</p>
        <input type="checkbox" id="chkEstado4" name="chkEstado4" class="fldGeneral" title="Desativo">
        <p id="lblNivel" name="lblNivel" class="lblGeneral">N�vel</p>
        <select id="selNivel" name="selNivel" class="fldGeneral"></select>
        <p id="lblDetalhePL" name="lblDetalhePL" class="lblGeneral">Det</p>
        <input type="checkbox" id="chkDetalhePL" name="chkDetalhePL" class="fldGeneral" title="Detalhar colunas extras?" checked>

        <p id="lblFormatoSolicitacao2" name="lblFormatoSolicitacao2" class="lblGeneral">Formato</p>
        <select id="selFormatoSolicitacao2" name="selFormatoSolicitacao2" class="fldGeneral">
            <option value="1">PDF</option>	
            <option value="2">Excel</option>
        </select>
    </div>
    
    <div id="divDiario" name="divDiario" class="divGeneral">
        <p id="lblDataInicio" name="lblDataInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio" name="txtDataInicio" class="fldGeneral" LANGUAGE="javascript" onfocus="return __selFieldContent(this)">
        <p id="lblDataFim" name="lblDataFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim" name="txtDataFim" class="fldGeneral" LANGUAGE="javascript" onfocus="return __selFieldContent(this)">
        <p id="lblFormatoSolicitacao" name="lblFormatoSolicitacao" class="lblGeneral">Formato</p>
        <select id="selFormatoSolicitacao" name="selFormatoSolicitacao" class="fldGeneral">
            <option value="2">Excel</option>		
            <option value="1">PDF</option>	
        </select>
        <p id="lblUndsDiario" name="lblUndsDiario" class="lblGeneral">Unidades</p>
        <select id="selUndsDiario" name="selUndsDiario" class="fldGeneral" multiple>
        <%
			strSQL = "select " & _
					"Empresas.PessoaID as fldID, Empresas.Fantasia as fldName " & _
				"from RelacoesPesRec Relacoes WITH(NOLOCK) " & _
					"INNER JOIN Pessoas Empresas WITH(NOLOCK) ON Relacoes.SujeitoID = Empresas.PessoaID " & _
				"where " & _
					"Relacoes.TipoRelacaoID=12 AND  " & _
					"Relacoes.EstadoID=2 AND " & _
					"dbo.fn_Empresa_Matriz(Relacoes.SujeitoID, 1, NULL) = " & nEmpresaID

			rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

			If (Not (rsData.BOF AND rsData.EOF) ) Then
				While Not (rsData.EOF)
					Response.Write "<option value='" & CStr(rsData.Fields("fldID").Value) & "'> " 
					Response.Write rsData.Fields("fldName").Value & "</option>" 
					rsData.MoveNext()
				WEnd
			End If

			rsData.Close
        %>
        </select>
    </div>

    <div id="divRazao" name="divRazao" class="divGeneral">
		<p id="lblConta" name="lblConta" class="lblGeneral">Conta</p>
        <select id="selConta" name="selConta" class="fldGeneral">
<%
	strSQL ="SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName, 0 AS TipoDetalhamentoID " & _
			"UNION ALL " & _
		"SELECT 1 AS Indice, ContaID AS fldID, " & _
			"CONVERT(VARCHAR(10), ContaID) + SPACE(1) + dbo.fn_Conta_Identada(ContaID, " & CStr(nIdiomaID) & ", NULL, NULL) AS fldName, " & _
			"TipoDetalhamentoID " & _
		"FROM PlanoContas WITH(NOLOCK) " & _
		"WHERE (EstadoID = 2) " & _
		"ORDER BY Indice, fldID "

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write "<option value='" & CStr(rsData.Fields("fldID").Value) & "' " 
			Response.Write "TipoDetalhamentoID='" & CStr(rsData.Fields("TipoDetalhamentoID").Value) & "' >"
			Response.Write rsData.Fields("fldName").Value & "</option>" 

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
%>
</select>

        <p id="lblImprimeDetalhe" name="lblImprimeDetalhe" class="lblGeneral">Det</p>
        <input type="checkbox" id="chkImprimeDetalhe" name="chkImprimeDetalhe" class="fldGeneral" title="Imprimir detalhe?" checked>

		<p id="lblDetalhe" name="lblDetalhe" class="lblGeneral">Detalhe</p>
        <select id="selDetalhe" name="selDetalhe" class="fldGeneral"></select>
        <p id="lblEstado" name="lblEstado" class="lblGeneral">Est</p>
        <select id="selEstado" name="selEstado" class="fldGeneral">
        <%
	        Dim optSel
        	
	        strSQL = "SELECT 0 AS fldID, '' AS fldName, -1 AS Ordem " & _
			        "UNION ALL " & _
			        "SELECT d.RecursoID AS fldID, d.RecursoAbreviado AS fldName, c.Ordem AS Ordem " & _
			        "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK) " & _
			        "WHERE (a.TipoRelacaoID = 1 AND a.EstadoID = 2 AND " & _
				        "a.ObjetoID = 10131 AND " & _
				        "a.SujeitoID = b.RecursoID AND b.Principal = 1 AND " & _
				        "a.MaquinaEstadoID = c.ObjetoID AND " & _
				        "c.TipoRelacaoID = 3 AND c.SujeitoID = d.RecursoID) " & _
				        "ORDER BY Ordem"

	        rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	        While Not rsData.EOF
		        If (CLng(rsData.Fields("fldID").Value)) = 2 Then
			        optSel = "selected"
		        Else
			        optSel = ""
		        End If        
		        Response.Write "<option value =" & CLng(rsData.Fields("fldID").Value) & " " & _
				                optSel & ">" & _
				                rsData.Fields("fldName").Value & _
				                "</option>" & chr(13) & chr(10)        
		        rsData.MoveNext
	        Wend

	        rsData.Close
        %>
        </select>
        <!--<p id="lblLancConciliados" name="lblLancConciliados" class="lblGeneral" title="S� lan�amentos conciliados?">Conc</p>
        <input type="checkbox" id="chkLancConciliados" name="chkLancConciliados" class="fldGeneral" title="S� lan�amentos conciliados?"></input>
        -->
        <p id="lblConcilia" name="lblConcilia" class="lblGeneral" title="Lan�amentos conciliados, n�o conciliados ou todos?">Conciliado</p>
        <select id="selConcilia" name="selConcilia" class="fldGeneral" >
        <%
        Set rsData3 = Server.CreateObject("ADODB.Recordset")

        strSQL = "SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName " & _
			     "UNION ALL SELECT 1 AS Indice, ItemID AS fldID, ItemMasculino AS fldName " & _
			     "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
			     "WHERE TipoID = 3000" & _
			     "ORDER BY Indice, fldID DESC"

	    rsData3.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	    If (Not (rsData3.BOF AND rsData3.EOF) ) Then
        
		    While Not (rsData3.EOF)
        
		        Response.Write( "<option value='" & rsData3.Fields("fldID").Value & "'>" )
			    Response.Write( rsData3.Fields("fldName").Value & "</option>" )
      
		    rsData3.MoveNext()
      
		    WEnd
     
 	    End If

	    rsData3.Close
	    Set rsData3 = Nothing
        %>		
        </select>
        
        <p id="lblDataInicio2" name="lblDataInicio2" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio2" name="txtDataInicio2" class="fldGeneral" LANGUAGE="javascript" onfocus="return __selFieldContent(this)">
        <p id="lblDataFim2" name="lblDataFim2" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim2" name="txtDataFim2" class="fldGeneral" LANGUAGE="javascript" onfocus="return __selFieldContent(this)">
        <p id="lblFiltro" name="lblFiltro" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro" name="txtFiltro" class="fldGeneral" LANGUAGE="javascript">
        <p id="lblUndsRazao" name="lblUndsRazao" class="lblGeneral">Unidades</p>
        <select id="selUndsRazao" name="selUndsRazao" class="fldGeneral" multiple>
        <%
			strSQL = "SELECT " & _
					"Empresas.PessoaID as fldID, Empresas.Fantasia as fldName " & _
				"FROM RelacoesPesRec Relacoes WITH(NOLOCK) " & _
					"INNER JOIN Pessoas Empresas WITH(NOLOCK) ON Relacoes.SujeitoID = Empresas.PessoaID " & _
				"WHERE " & _
					"Relacoes.TipoRelacaoID=12 AND  " & _
					"Relacoes.EstadoID=2 AND " & _
					"dbo.fn_Empresa_Matriz(Relacoes.SujeitoID, 1, NULL) = " & nEmpresaID

			rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

			If (Not (rsData.BOF AND rsData.EOF) ) Then
				While Not (rsData.EOF)
					Response.Write "<option value='" & CStr(rsData.Fields("fldID").Value) & "'> " 
					Response.Write rsData.Fields("fldName").Value & "</option>" 

					rsData.MoveNext()
				WEnd
			End If

			rsData.Close
        %>
        </select>
    </div>

    <div id="divBalanceteVerificacao" name="divBalanceteVerificacao" class="divGeneral">
		<p id="lblNivel3" name="lblNivel3" class="lblGeneral">N�vel</p>
        <select id="selNivel3" name="selNivel3" class="fldGeneral"></select>
        <p id="lblLancamentos" name="lblLancamentos" class="lblGeneral">Lan�</p>
        <input type="checkbox" id="chkLancamentos" name="chkLancamentos" class="fldGeneral" title="S� contas que tiveram lan�amentos no per�odo">
        <p id="lblSaldo" name="lblSaldo" class="lblGeneral">Saldo</p>
        <input type="checkbox" id="chkSaldo" name="chkSaldo" class="fldGeneral" title="S� contas com saldo direfente de zero">
        <p id="lblSaldoEmNiveis" name="lblSaldoEmNiveis" class="lblGeneral">N�veis</p>
        <input type="checkbox" id="chkSaldoEmNiveis" name="chkSaldoEmNiveis" class="fldGeneral" title="Saldo por n�vel de conta">
        <p id="lblPadraoUSA" name="lblPadraoUSA" class="lblGeneral">USA</p>
        <input type="checkbox" id="chkPadraoUSA" name="chkPadraoUSA" class="fldGeneral" title="Padr�o USA?">
        <p id="lblDataInicio3" name="lblDataInicio3" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio3" name="txtDataInicio3" class="fldGeneral" LANGUAGE="javascript" onfocus="return __selFieldContent(this)">
        <p id="lblDataFim3" name="lblDataFim3" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim3" name="txtDataFim3" class="fldGeneral" LANGUAGE="javascript" onfocus="return __selFieldContent(this)">
        <p id="lblUndsBalancete" name="lblUndsBalancete" class="lblGeneral">Unidades</p>
        <select id="selUndsBalancete" name="selUndsBalancete" class="fldGeneral" multiple>
        <%
			strSQL = "select " & _
					"Empresas.PessoaID as fldID, Empresas.Fantasia as fldName " & _
				"from RelacoesPesRec Relacoes WITH(NOLOCK) " & _
					"INNER JOIN Pessoas Empresas WITH(NOLOCK) ON Relacoes.SujeitoID = Empresas.PessoaID " & _
				"where " & _
					"Relacoes.TipoRelacaoID=12 AND  " & _
					"Relacoes.EstadoID=2 AND " & _
					"dbo.fn_Empresa_Matriz(Relacoes.SujeitoID, 1, NULL) = " & nEmpresaID

			rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

			If (Not (rsData.BOF AND rsData.EOF) ) Then
				While Not (rsData.EOF)          
					Response.Write "<option value='" & CStr(rsData.Fields("fldID").Value) & "'> " 
					Response.Write rsData.Fields("fldName").Value & "</option>" 

					rsData.MoveNext()
				WEnd
			End If

			rsData.Close
			Set rsData = Nothing
        %>
        </select>
        <p id="lblFormatoBalancete" name="lblFormatoBalancete" class="lblGeneral">Formato</p>
        <select id="selFormatoBalancete" name="selFormatoBalancete" class="fldGeneral">
            <option value="1">PDF</option>		
            <option value="2">Excel</option>		            			
        </select>
    </div>

    <p id="lblReports" name="lblReports" class="paraNormal"></p>
    <select id="selReports" name="selReports" class="fldGeneral">
    </select>

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <iframe id="frmReport" name="frmReport" style="display:none"  frameborder="no"></iframe>

</body>

</html>
