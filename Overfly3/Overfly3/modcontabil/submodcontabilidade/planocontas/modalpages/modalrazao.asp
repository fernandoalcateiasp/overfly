
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Function ReplaceInText(baseStr, patrn, replStr)
    Dim regEx                         ' Create variables.
    Set regEx = New RegExp            ' Create regular expression.
    regEx.Pattern = patrn             ' Set pattern.
    regEx.IgnoreCase = True           ' Make case insensitive.
    regEx.Global = True               ' Replace globally
    ReplaceInText = regEx.Replace(baseStr, replStr)   ' Make replacement.
End Function
%>

<html id="modalrazaoHtml" name="modalrazaoHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/planocontas/modalpages/modalrazao.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/planocontas/modalpages/modalrazao.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nContaID, nEmpresaID, nUserID, nContextoID, nIdiomaID

sCaller = ""
nContaID = 0
nEmpresaID = 0
nUserID = 0
nContextoID = 0
nIdiomaID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("nContaID").Count    
    nContaID = Request.QueryString("nContaID")(i)
Next

Response.Write "var glb_nContaSelecionadaID = " & CStr(nContaID) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

Response.Write "var glb_nEmpresaID = " &  CLng(nEmpresaID) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

Response.Write "var glb_nUserID = " &  CLng(nUserID) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

Response.Write "var glb_nContextoID = " &  CLng(nContextoID) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("nIdiomaID").Count    
    nIdiomaID = Request.QueryString("nIdiomaID")(i)
Next

'DADOS DOS COMBOS
Dim rsData, rsData1, rsData2, rsData3, strSQL, sDataDDMMYYYY, sDataMMDDYYYY, sDataFimDDMMYYYY, sDataFimMMDDYYYY
Dim optSel, idNum, nAceitaLancamento
nAceitaLancamento = 0

strSQL = "SELECT TOP 1 CONVERT(VARCHAR, DATEADD(dd, -3, GETDATE()), 103) AS sDataDDMMYYYY," & _
		 "CONVERT(VARCHAR, DATEADD(dd, -3, GETDATE()), 101) AS sDataMMDDYYYY ," & _
		 "CONVERT(VARCHAR, GETDATE(), 103) AS sDataFimDDMMYYYY, " & _
		 "CONVERT(VARCHAR, GETDATE(), 101) AS sDataFimMMDDYYYY"

Set rsData = Server.CreateObject("ADODB.Recordset")         
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

While Not rsData.EOF
	sDataDDMMYYYY = CStr(rsData.Fields("sDataDDMMYYYY").Value)
	sDataMMDDYYYY = CStr(rsData.Fields("sDataMMDDYYYY").Value)
	sDataFimDDMMYYYY = CStr(rsData.Fields("sDataFimDDMMYYYY").Value)
	sDataFimMMDDYYYY = CStr(rsData.Fields("sDataFimMMDDYYYY").Value)
	rsData.MoveNext
Wend
        
Response.Write "var glb_sDataDDMMYYYY = " & Chr(39) & CStr(sDataDDMMYYYY) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_sDataMMDDYYYY = " & Chr(39) & CStr(sDataMMDDYYYY) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_sDataFimDDMMYYYY = " & Chr(39) & CStr(sDataFimDDMMYYYY) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_sDataFimMMDDYYYY = " & Chr(39) & CStr(sDataFimMMDDYYYY) & Chr(39) & ";"
Response.Write vbcrlf

rsData.Close

strSQL = "SELECT DISTINCT b.EmpresaID, d.MoedaID, e.SimboloMoeda " & _
		 "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), RelacoesPesRec c WITH(NOLOCK), RelacoesPesRec_Moedas d WITH(NOLOCK), Conceitos e WITH(NOLOCK) " & _
		 "WHERE ((a.SujeitoID = " & CStr(nUserID) & ") AND (a.TipoRelacaoID = 11) AND (a.ObjetoID = 999) AND (a.RelacaoID = b.RelacaoID) AND " & _
			"(b.EmpresaID = c.SujeitoID) AND (c.TipoRelacaoID = 12) AND (c.ObjetoID = 999) AND (c.EstaEmOperacao = 1) AND (c.RelacaoID = d.RelacaoID) AND " & _
			"(d.MoedaID = e.ConceitoID)) " & _
		 "ORDER BY b.EmpresaID, e.SimboloMoeda"

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write "var glb_aMoedasEmpresas = new Array();"
Response.Write vbcrlf

i = 0
While Not rsData.EOF
        
    'glb_arrayEstado[i] = new Array(12, 'Bahia', 'BA');
        
    Response.Write "glb_aMoedasEmpresas[" & CStr(i) & "] = new Array("
    Response.Write CStr(rsData.Fields("EmpresaID").Value)
    Response.Write ","
    Response.Write CStr(rsData.Fields("MoedaID").Value)
    Response.Write ","
    Response.Write Chr(34)
    Response.Write CStr(rsData.Fields("SimboloMoeda").Value)
    Response.Write Chr(34)
    Response.Write ");"
    Response.Write vbcrlf
    
    rsData.MoveNext
    i = i + 1
Wend

rsData.Close

Response.Write "</script>"
Response.Write vbcrlf

strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName, 1531 AS TipoDetalhamentoID, CONVERT(BIT, 1) AS AceitaLancamento, " & _
		 "SPACE(0) AS Conta, SPACE(0) AS NomeConta " & _
		 "UNION ALL SELECT ContaID AS fldID, " & _
				"CONVERT(VARCHAR(10), ContaID) + SPACE(1) + dbo.fn_Conta_Identada(ContaID, " & CStr(nIdiomaID) & ", NULL, NULL) AS fldName, " & _
				"TipoDetalhamentoID, dbo.fn_Conta_AceitaLancamento(ContaID) AS AceitaLancamento, " & _
				"CONVERT(VARCHAR(10), ContaID) AS Conta, LTRIM(RTRIM(dbo.fn_Conta_Identada(ContaID, " & CStr(nIdiomaID) & ", NULL, NULL))) AS NomeConta " & _
			"FROM PlanoContas WITH(NOLOCK) " & _
			"WHERE (EstadoID IN (2,4)) " & _
		"ORDER BY fldID "

Set rsData1 = Server.CreateObject("ADODB.Recordset")         
rsData1.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

strSQL = "SELECT 0 AS fldID, '' AS fldName, -1 AS Ordem " & _
	     "UNION ALL " & _
         "SELECT d.RecursoID AS fldID, d.RecursoAbreviado AS fldName, c.Ordem AS Ordem " & _
		 "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK) " & _
		 "WHERE (a.TipoRelacaoID = 1 AND a.EstadoID = 2 AND " & _
			"a.ObjetoID = 10131 AND " & _
			"a.SujeitoID = b.RecursoID AND b.Principal = 1 AND " & _
			"a.MaquinaEstadoID = c.ObjetoID AND " & _
			"c.TipoRelacaoID = 3 AND c.SujeitoID = d.RecursoID) " & _
		    "ORDER BY Ordem"

Set rsData2 = Server.CreateObject("ADODB.Recordset")         
rsData2.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalrazao_ValidateEdit();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalrazao_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalrazaoKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalrazao_BeforeEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modalrazaoDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalrazaoBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Atualizar linha de mensagens
js_fg_AfterRowColmodalrazao (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4])
//-->
</SCRIPT>

</head>

<body id="modalrazaoBody" name="modalrazaoBody" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
    <!-- Div dos campos -->
    <div id="divFields" name="divFields" class="divGeneral">
		<p id="lblEmpresas" name="lblEmpresas" class="lblGeneral">Empresas</p>
        <select id="selEmpresas" name="selEmpresas" class="fldGeneral" MULTIPLE>
<%
	strSQL = "SELECT DISTINCT b.EmpresaID, d.Fantasia " & _
			"FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), RelacoesPesRec c WITH(NOLOCK), Pessoas d WITH(NOLOCK) " & _
			"WHERE ((a.SujeitoID = " & CStr(nUserID) & ") AND (a.TipoRelacaoID = 11) AND (a.ObjetoID = 999) AND " & _ 
				"(a.RelacaoID = b.RelacaoID) AND (b.EmpresaID = c.SujeitoID) AND " & _
				"(c.EstadoID = 2) AND (c.TipoRelacaoID = 12) AND (c.ObjetoID = 999) AND (c.EstaEmOperacao = 1) AND " & _
				"(c.SujeitoID = d.PessoaID)) " & _
			"ORDER BY d.Fantasia"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	While (Not rsData.EOF)
		Response.Write "<option value =" & rsData.Fields("EmpresaID").Value 
		
		If (CLng(nEmpresaID) = CLng(rsData.Fields("EmpresaID").Value)) Then
			Response.Write " SELECTED "
		End If
		
		Response.Write ">" & rsData.Fields("Fantasia").Value & "</option>" & _
			chr(13) & chr(10)

		rsData.MoveNext
	WEnd

	rsData.Close
	Set rsData = Nothing
%>		
        </select>
        <p id="lblMoeda" name="lblMoeda" class="lblGeneral">Moeda</p>
        <select id="selMoeda" name="selMoeda" class="fldGeneral"></select>
        <p id="lblEstado" name="lblEstado" class="lblGeneral">Est</p>
        <select id="selEstado" name="selEstado" class="fldGeneral">
        <%
			idNum = 1
			While Not rsData2.EOF
				If (CDbl(rsData2.Fields("fldID").Value)) = 2 Then
				    optSel = "selected"
				Else
				    optSel = ""
				End If        
				Response.Write "<option value =" & CDbl(rsData2.Fields("fldID").Value) & " " & _
				               "id=optEstado" & CStr(idNum) & " " & _
				               optSel & ">" & _
				               rsData2.Fields("fldName").Value & _
				               "</option>" & chr(13) & chr(10)        
				idNum = idNum + 1
				rsData2.MoveNext
			Wend
        
			rsData2.Close
			Set rsData2 = Nothing 
        %>
        </select>
		<p id="lblConta" name="lblConta" class="lblGeneral">Conta</p>
        <select id="selConta" name="selConta" class="fldGeneral">
        <%
			Dim nSelected
			nSelected = 0
			
			idNum = 1
			While Not rsData1.EOF

				If (CDbl(nContaID) > 0) Then
					If (CDbl(rsData1.Fields("fldID").Value) = CDbl(nContaID)) Then
						optSel = "selected"
					Else
						optSel = ""
					End If        
				Else
					If ( (nSelected = 0) AND (CDbl(rsData1.Fields("fldID").Value) > 0) ) Then
						nSelected = 1
						optSel = "selected"
					Else
						optSel = ""
					End If        
				End If

				If (CBool(rsData1.Fields("AceitaLancamento").Value)) Then
					nAceitaLancamento = 1
				Else					
					nAceitaLancamento = 0
				End If

				Response.Write "<option value =" & CDbl(rsData1.Fields("fldID").Value) & " " & _
							   "Conta = '" & CStr(rsData1.Fields("Conta").Value) & "' " & _
							   "NomeConta = '" & CStr(rsData1.Fields("NomeConta").Value) & "' " & _
							   "TipoDetalhamentoID = " & CDbl(rsData1.Fields("TipoDetalhamentoID").Value) & " " & _
							   "AceitaLancamento = " & nAceitaLancamento & " " & _
				               "id='optEstado" & CStr(idNum) & "' " & _
				               optSel & ">" & _
				               ReplaceInText(rsData1.Fields("fldName").Value, " ", "&nbsp") & _
				               "</option>" & chr(13) & chr(10)        
				idNum = idNum + 1
				rsData1.MoveNext
			Wend
        
			rsData1.Close
			Set rsData1 = Nothing 
        %>
        </select>
        <p id="lblDetalhe" name="lblDetalhe" class="lblGeneral">Detalhe</p>
        <select id="selDetalhe" name="selDetalhe" class="fldGeneral">
        </select>
        <!--
        <p id="lblLancConciliados" name="lblLancConciliados" class="lblGeneral" title="S� lan�amentos conciliados?">Conc</p>
        <input type="checkbox" id="chkLancConciliados" name="chkLancConciliados" class="fldGeneral" title="S� lan�amentos conciliados?"></input>
		-->
		<p id="lblConcilia" name="lblConcilia" class="lblGeneral" title="Lan�amentos conciliados, n�o conciliados ou todos?">Conciliado</p>
        <select id="selConcilia" name="selConcilia" class="fldGeneral" >
        <%
        Set rsData3 = Server.CreateObject("ADODB.Recordset")

        strSQL = "SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName " & _
			     "UNION ALL SELECT 1 AS Indice, ItemID AS fldID, ItemMasculino AS fldName " & _
			     "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
			     "WHERE TipoID = 3000" & _
			     "ORDER BY Indice, fldID DESC"

	    rsData3.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	    If (Not (rsData3.BOF AND rsData3.EOF) ) Then
        
		    While Not (rsData3.EOF)
        
		        Response.Write( "<option value='" & rsData3.Fields("fldID").Value & "'>" )
			    Response.Write( rsData3.Fields("fldName").Value & "</option>" )
      
		    rsData3.MoveNext()
      
		    WEnd
     
 	    End If

	    rsData3.Close
	    Set rsData3 = Nothing
        %>		
        </select>
        <p id="lblDetalheLancamento" name="lblDetalheLancamento" class="lblGeneral" title="Exibir detalhes de lan�amento?">Det</p>
        <input type="checkbox" id="chkDetalheLancamento" name="chkDetalheLancamento" class="fldGeneral" title="Exibir detalhes de lan�amento?"></input>
        
        <p id="lblColunas" name="lblColunas" class="lblGeneral" title="Exibir colunas complementares?">Col</p>
        <input type="checkbox" id="chkColunas" name="chkColunas" class="fldGeneral" title="Exibir colunas complementares?"></input>
        <p id="lblDataInicio" name="lblDataInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio" name="txtDataInicio" class="fldGeneral"></input>
        <p id="lblDataFim" name="lblDataFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim" name="txtDataFim" class="fldGeneral"></input>
        <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnImprime" name="btnImprime" value="Imprimir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
        <input type="image" id="btnExcel" name="btnExcel" class="fldGeneral" title="Exportar para Excel" LANGUAGE="javascript" onmousedown="return btn_onmousedownExcel()" WIDTH="24" HEIGHT="23"></input>
        <p id="lblFiltro" name="lblFiltro" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro" name="txtFiltro" class="fldGeneral"></input>
    </div>
    
    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <!-- <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img> -->
    </div>
    
    <!-- Div do grid -->
    <div id="divFG2" name="divFG2" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg2" name="fg2" VIEWASTEXT>
        </object>
        <!-- <img id="Img1" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="Img2" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="Img3" name="hr_B_FGBorder" class="lblGeneral"></img> -->
    </div>    

    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>
</body>

</html>
