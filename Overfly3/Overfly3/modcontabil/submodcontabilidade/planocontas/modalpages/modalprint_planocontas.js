/********************************************************************
modalprint_planocontas.js

Library javascript para o modalprint.asp
Valores a Localizar
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_currDate = '';
var glb_aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
var dsoEMail = new CDatatransport('dsoEMail');
var dsoCurrData = new CDatatransport('dsoCurrData');
var dsoCombo = new CDatatransport('dsoCombo');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    linksDivAndReports()
    invertChkBox()
    chkBox_Clicked()
    selReports_Change()
    btnOK_Status()
    btnOK_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    noRights()
    
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	glb_bUseProgressBar = true;
	asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    with (modalprintBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

	glb_sTermUse__ = 'A';
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
    
    if ( selReports.disabled == false )
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {
		pb_StopProgressBar(true);
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
	}        
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Relat�rios', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;    
    
    // largura livre
    widthFree = modWidth - frameBorder;
    
    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    with (lblReports.style)
    {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta combo de relatorios
    with (selReports.style)
    {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta o div PlanoContas
    with (divPlanoContas.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }
    
    // ajusta o div DivDiario
    with (divDiario.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }
    
    // ajusta o div divRazao
    with (divRazao.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }
    
    // ajusta o div divBalanceteVerificacao
    with (divBalanceteVerificacao.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }    

    // O estado do botao btnOK
    btnOK_Status();
    selUndsDiario.selectedIndex = 0;
    selUndsRazao.selectedIndex = 0;
    selUndsBalancete.selectedIndex = 0;
    
    // O usuario tem direito ou nao
    noRights();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;
    
    selReports.onchange = selReports_Change;
    
    // carrega a lista de relatorios
    loadReportsInList();
    
    // linka os divs nos relatorios
    linkDivsAndReports();
    
    // ajusta os divs

    // ajusta o divPlanoContas
    adjustdivPlanoContas();

    // ajusta o divDiario
    adjustdivDiario();
    
    // ajusta o divRazao
    adjustdivRazao();
    
    // ajusta o divBalanceteVerificacao
    adjustdivBalanceteVerificacao();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList()
{
    var i;
    
    if ( glb_arrayReports != null )
    {
        for ( i=0; i<glb_arrayReports.length; i++)    
        {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);
            
            if ( glb_arrayReports[i][2] )
                selReports.selectedIndex = i;
        }
        
        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports()
{
    divPlanoContas.setAttribute('report', 40301 , 1);
    divDiario.setAttribute('report', 40302 , 1);
    
    if (glb_sCaller == 'PL')
		divRazao.setAttribute('report', 40303 , 1);
	else
		divRazao.setAttribute('report', 40307 , 1);
		
    divBalanceteVerificacao.setAttribute('report', 40304 , 1);
    divBalanceteVerificacao.setAttribute('report', 40305 , 1);
    divBalanceteVerificacao.setAttribute('report', 40306 , 1);
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    // O estado do botao btnOK
    btnOK_Status();
    
    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();
    
    return true;
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change()
{
	if (selReports.value == 40303)
		divRazao.setAttribute('report', 40303 , 1);
	else if (selReports.value == 40304)
		divBalanceteVerificacao.setAttribute('report', 40304 , 1);		
	else if (selReports.value == 40305)
		divBalanceteVerificacao.setAttribute('report', 40305 , 1);		
	else if (selReports.value == 40306)
		divBalanceteVerificacao.setAttribute('report', 40306 , 1);
	else if (selReports.value == 40307)
		divRazao.setAttribute('report', 40307 , 1);		

    // mostra o div correspondente ao relatorio
    showDivByReportID();
    
    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if ( this.disabled == false )
        this.focus();
    
    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status()
{
    var btnOKStatus = true;
    var i, elem;
    
    if ( selReports.selectedIndex != -1 )
    {
        // Relatorio de Plano de Contas
        if (selReports.value == 40301)
            btnOKStatus = false;
		else if (selReports.value == 40302)
            btnOKStatus = false;
		else if (selReports.value == 40303)
            btnOKStatus = false;
		else if (selReports.value == 40304)
            btnOKStatus = false;
		else if (selReports.value == 40305)
            btnOKStatus = false;
		else if (selReports.value == 40306)
            btnOKStatus = false;
		else if (selReports.value == 40307)
            btnOKStatus = false;
    }

    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    // Relatorio de Plano de Contas
    if ( (selReports.value == 40301) && (glb_sCaller == 'PL') )
        relatorioPlanoContas();
	else if ( (selReports.value == 40302) && (glb_sCaller == 'PL') )
        relatorioDiario();
	else if ( (selReports.value == 40303) || (selReports.value == 40307) )
        relatorioRazao();
	else if ( (selReports.value == 40304) && (glb_sCaller == 'PL') )
        relatoriosBalanceteVerificacao(1);
	else if ( (selReports.value == 40305) && (glb_sCaller == 'PL') )
        relatoriosBalanceteVerificacao(2);
	else if ( (selReports.value == 40306) && (glb_sCaller == 'PL') )
        relatoriosBalanceteVerificacao(3);
    else
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null);
}

/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID()
{
    var i, coll, attr, currRep;
    
    attr = null;
    currRep = 0;
    
    currRep = selReports.value;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( attr == currRep )
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';    
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr()
{
    var i, coll, attr, retVal;
    
    attr = null;
    retVal = 0;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( coll[i].style.visibility == 'visible' )
            {
                retVal = attr;
                break;
            }    
        }
    }
    return retVal;    
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights()
{
    if ( selReports.options.length != 0 )
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;
    
    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
            coll[i].style.visibility = 'hidden';    
    }
    
    // desabilita o combo de relatorios
    selReports.disabled = true;
    
    // desabilita o botao OK
    btnOK.disabled = true;
        
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];
    
    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);
       
    elem = document.getElementById('selReports');
    
    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
        
    // a altura livre    
    modHeight -= topFree;
        
    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = translateTerm('Relat�rios n�o liberados', null);
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt'; 
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';
    
    // acrescenta o elemento
    window.document.body.appendChild(elem);
        
    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);
        
    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;
        
    return null;
}

function adjustdivPlanoContas()
{
    var i;

    for (i=1;i<=6;i++)
    {
        var oOption = document.createElement("OPTION");
        oOption.text = i;
        oOption.value = i;
        selNivel.add(oOption);
    }
    
    selNivel.selectedIndex = 5;

    adjustElementsInForm([['lblEstado1','chkEstado1',3,1,-10,-10],
						  ['lblEstado2','chkEstado2',3,1],
						  ['lblEstado3','chkEstado3',3,1],
						  ['lblEstado4','chkEstado4',3,1],
						  ['lblNivel','selNivel',10,2,-10],
						  ['lblDetalhePL', 'chkDetalhePL', 3, 2],
                          ['lblFormatoSolicitacao2', 'selFormatoSolicitacao2', 10, 3, -8, 0]], null, null, true);
						  
	chkDetalhePL.checked = true;
}

function adjustdivDiario()
{
    adjustElementsInForm([['lblDataInicio','txtDataInicio',10,1,-10,-10],
						  ['lblDataFim', 'txtDataFim', 10, 1],                       
						  ['lblUndsDiario', 'selUndsDiario', 20, 1],
                          ['lblFormatoSolicitacao', 'selFormatoSolicitacao', 10, 2, -8, 0]], null, null, true);

    txtDataInicio.maxLength = 10;
    txtDataFim.maxLength = 10;
    txtDataInicio.onfocus = selFieldContent;
    txtDataFim.onfocus = selFieldContent;

	selUndsDiario.style.height = 100;
	
    // busca data corrente do servidor de paginas
	dsoCurrData.URL = SYS_PAGESURLROOT + '/serversidegenEx/currdate.aspx?currDateFormat='+escape(DATE_FORMAT);
	dsoCurrData.ondatasetcomplete = dsoCurrDataComplete_DSC;
	dsoCurrData.Refresh();
}

function dsoCurrDataComplete_DSC()
{
    if (! (dsoCurrData.recordset.BOF && dsoCurrData.recordset.EOF) )
        glb_currDate = dsoCurrData.recordset['currDate'].value;

    txtDataInicio.value = glb_currDate;
    txtDataFim.value = glb_currDate;
    txtDataInicio2.value = glb_dCurrDateIni;
    txtDataFim2.value = glb_currDate;
}

function adjustdivRazao()
{
	var nContaID = 0;
    adjustElementsInForm([['lblConta','selConta',56,1,-10,-10],
						  ['lblImprimeDetalhe','chkImprimeDetalhe',3,2,-15],
						  ['lblDetalhe','selDetalhe',17,2],
						  ['lblEstado', 'selEstado', 5, 2],
						  ['lblConcilia', 'selConcilia', 7, 2],
						  ['lblDataInicio2','txtDataInicio2',9,2],
						  ['lblDataFim2','txtDataFim2',9,2],
						  ['lblFiltro','txtFiltro',56,3,-10],
						  ['lblUndsRazao','selUndsRazao',20,4, -10]
						  ],null,null,true);

	selUndsRazao.style.height = 100;

    txtDataInicio2.maxLength = 10;
    txtDataFim2.maxLength = 10;
    txtDataInicio2.onfocus = selFieldContent;
    txtDataFim2.onfocus = selFieldContent;
    selDetalhe.disabled = true;
    selConta.onchange = selConta_onchange;
    chkImprimeDetalhe.checked = true;
    chkImprimeDetalhe.onclick = chkImprimeDetalhe_onclick;
    
    if (glb_sCaller == 'S')
    {
        nContaID  = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'ContaID' + '\'' + '].value');
		
		if ((nContaID != null) && (nContaID != 0))
		{
			selOptByValueInSelect(getHtmlId(), 'selConta', nContaID);
			selConta.disabled = true;
			selConta_onchange();
		}
    }
}

function chkImprimeDetalhe_onclick()
{
	lblDetalhe.style.visibility = (chkImprimeDetalhe.checked ? 'inherit' : 'hidden');
	selDetalhe.style.visibility = (chkImprimeDetalhe.checked ? 'inherit' : 'hidden');
	selDetalhe.selectedIndex = 0;
}

/********************************************************************
Solicitar dados dos combos ao servidor
********************************************************************/
function selConta_onchange()
{
    lockControlsInModalWin(true);
    
    var nContaID, nTipodetalhamentoID;
	nContaID = selConta.value;
	nTipoDetalhamentoID = selConta.options.item(selConta.selectedIndex).getAttribute('TipoDetalhamentoID', 1);

	clearComboEx(['selDetalhe']);        

	// Nenhum
	if ((nTipoDetalhamentoID == 0) || (nTipoDetalhamentoID == 1531))
	{
		lockControlsInModalWin(false);    
		selDetalhe.disabled = true;

		if ( ((txtDataInicio2.style.visibility == 'inherit') || (txtDataInicio2.style.visibility == 'visible')) && (txtDataInicio2.disabled = false))
		{	
			txtDataInicio2.focus();
			txtDataInicio2.select();
		}
		
		btnOK_Status();

		return null;
	}
	//Pessoas
	else if ( (nTipoDetalhamentoID >= 1532) && (nTipoDetalhamentoID <= 1541) ) 
	{
	    strSQL = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName UNION ALL ' +
				 'SELECT DISTINCT 1 AS Indice, c.PessoaID AS fldID, c.Fantasia AS fldName ' +
					'FROM Lancamentos_Contas a WITH(NOLOCK), Lancamentos b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
					'WHERE (a.ContaID = ' + nContaID + ' AND a.LancamentoID = b.LancamentoID AND ' +
						'b.EmpresaID = dbo.fn_Empresa_Matriz(' + glb_nEmpresaID + ', 1, NULL) AND b.EstadoID IN (2, 81, 82) AND a.PessoaID = c.PessoaID AND ' + 
						'dbo.fn_Pessoa_Tipo(c.PessoaID, ' + glb_nEmpresaID + ', 2, ' + nTipoDetalhamentoID + ') = 1) ' +
				 'ORDER BY Indice, fldName';
	}
	//Conta Bancaria
	else if ( nTipoDetalhamentoID == 1542)
	{
	    strSQL = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName UNION ALL ' +
				 'SELECT DISTINCT 1 AS Indice, a.RelPesContaID AS fldID, dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) AS fldName ' +
					'FROM Lancamentos_Contas a WITH(NOLOCK), Lancamentos b WITH(NOLOCK) ' +
					'WHERE (a.ContaID = ' + nContaID + ' AND a.LancamentoID = b.LancamentoID AND b.EmpresaID = ' + glb_nEmpresaID + ' AND ' +
					'b.EstadoID IN (2, 81, 82)) ' +
				 'ORDER BY Indice, fldName';
	}
	//Impostos
	else if ( nTipoDetalhamentoID == 1543)
	{
	    strSQL = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName UNION ALL ' +
				 'SELECT DISTINCT 1 AS Indice, a.ImpostoID AS fldID, c.Imposto AS fldName ' +
					'FROM Lancamentos_Contas a WITH(NOLOCK), Lancamentos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
					'WHERE (a.ContaID = ' + nContaID + ' AND a.LancamentoID = b.LancamentoID AND b.EmpresaID = ' + glb_nEmpresaID + ' AND ' +
						'b.EstadoID IN (2, 81, 82) AND a.ImpostoID = c.ConceitoID) ' +
				'ORDER BY Indice, fldName';

	}

    // parametrizacao do dso dsoGrid
    setConnection(dsoCombo);

	dsoCombo.SQL = strSQL;
    dsoCombo.ondatasetcomplete = fillComboData_DSC;
    dsoCombo.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados dos combos
********************************************************************/
function fillComboData_DSC()
{
    var oOption;

    while (! dsoCombo.recordset.EOF )
    {
        oOption = document.createElement("OPTION");
        oOption.text = dsoCombo.recordset['fldName'].value;
        oOption.value = dsoCombo.recordset['fldID'].value;
        selDetalhe.add(oOption);
        dsoCombo.recordset.MoveNext();
    }
   
	lockControlsInModalWin(false);    
    
	selDetalhe.disabled = selDetalhe.options.length <= 1;
	if ((selDetalhe.disabled) || (selDetalhe.style.visibility == 'hidden'))
	{
		txtDataInicio2.focus();
		txtDataInicio2.select();
	}
	else
		selDetalhe.focus();
		
	btnOK_Status();		
}


function adjustdivBalanceteVerificacao()
{
    var i;

    for (i=1;i<=6;i++)
    {
        var oOption = document.createElement("OPTION");
        oOption.text = i;
        oOption.value = i;
        selNivel3.add(oOption);
    }

    selNivel3.selectedIndex = 5;
    adjustElementsInForm([['lblNivel3','selNivel3',5,1,-10,-10],
		['lblLancamentos','chkLancamentos',3,1],
		['lblSaldo','chkSaldo',3,1],
		['lblSaldoEmNiveis','chkSaldoEmNiveis',3,1],
		['lblPadraoUSA','chkPadraoUSA',3,1],
		['lblDataInicio3','txtDataInicio3',10,1,-5],
		['lblDataFim3','txtDataFim3',10,1],
		['lblUndsBalancete', 'selUndsBalancete', 20, 2, -10],
        ['lblFormatoBalancete', 'selFormatoBalancete', 10, 2]
		],null,null,true);

	selUndsBalancete.style.height = 100;

	chkSaldo.checked = true;
    txtDataInicio3.maxLength = 10;
    txtDataFim3.maxLength = 10;
    txtDataInicio3.onfocus = selFieldContent;
    txtDataFim3.onfocus = selFieldContent;
}

function relatorioPlanoContas()
{
    var title = translateTerm(selReports.options[selReports.selectedIndex].innerText, null, 'A');
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var formato = selFormatoSolicitacao2.value;

    var i, sInformation = '';
    
    for (i=1; i<=4; i++)
    {
		if (eval('chkEstado' + i + '.checked'))
		{
			sInformation += (sInformation == '' ? translateTerm('Estado',null) + '(s): ' : ' ');
			sInformation += translateTerm(eval('lblEstado' + i + '.innerText'), null);
		}
	}
	
    sInformation += '   ' + translateTerm('N�vel', null) + ': ' + selNivel.value;

	var strEstados = '';
	for (i=1; i<=4; i++)
	{
		if (eval('chkEstado' + i + '.checked'))
		{
			strEstados += (strEstados == '' ? '(' : ',');
			strEstados += i.toString();
		}	
	}
	
	strEstados += (strEstados == '' ? '(0)' : ')');


	var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&glb_sEmpresaRazao=" + aEmpresaData[6] + "&title=" + title + "&nEmpresaLogadaIdioma=" + getDicCurrLang() +
                        "&sInformation=" + sInformation + "&selNivel=" + selNivel.value + "&strEstados=" + strEstados + "&chkDetalhePL=" + chkDetalhePL.checked;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modcontabil/submodcontabilidade/planocontas/serverside/Reports_planocontas.aspx?" + strParameters;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
}

function SelectToInClause(sel)
{
	var result = "(";
	
	if(sel.options.length > 0)
	{
		for(i = 0; i < sel.options.length; i++)
		{
			if(sel.options[i].selected)
			{
			    if (result != "(")
			    {
			        result += ",";
			    };

				result += sel.options[i].value;				
			}
		}
		
		result += ")";
	}
	
	return result;
}

function SelectToPipeBar(sel)
{
	var result = "'/";
	
	if(sel.options.length > 0)
	{
		for(i = 0; i < sel.options.length; i++)
		{
			if(sel.options[i].selected)
			{
				result += sel.options[i].value;
				
				if(i + 1 < sel.options.length)
				{
					result += "/";
				}
			}
		}
		
		result += "/'";
	}
	
	return result;
}

function relatorioDiario()
{
	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	//var nEmpresaID = aEmpresaData[0];
	var nEmpresaID = SelectToInClause(selUndsDiario);
	
	// 
    // campo txtDataInicio e txtDataFim devem ser trimados
    txtDataInicio.value = trimStr(txtDataInicio.value);
    txtDataFim.value = trimStr(txtDataFim.value);
    
    // verifica os campos datas
    if (txtDataInicio.value != '')
    {
        if ( !chkDataEx(txtDataInicio.value) )
        {
            if ( window.top.overflyGen.Alert ('Data inicial inv�lida') == 0 )
                return null;
                
            lockControlsInModalWin(false);
            txtDataInicio.focus();
            return false;
        }
    }

    if (txtDataFim.value != '')
    {
        if ( !chkDataEx(txtDataFim.value) )
        {
            if ( window.top.overflyGen.Alert ('Data final inv�lida') == 0 )
                return null;
            lockControlsInModalWin(false);
            txtDataFim.focus();
            return false;
        }
    }

    var Formato = selFormatoSolicitacao.value;

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia +
                    "&getDicCurrLang=" + getDicCurrLang() + "&nEmpresaID2=" + nEmpresaID + "&DataInicio=" + putDateInMMDDYYYY2(trimStr(txtDataInicio.value)) +
                    "&DataFim=" + putDateInMMDDYYYY2(trimStr(txtDataFim.value)) + "&Formato=" + Formato + "&DataInicio2=" + txtDataInicio.value +
                    "&DataFim2=" + txtDataFim.value;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modcontabil/submodcontabilidade/planocontas/serverside/Reports_planocontas.aspx?" + strParameters + "&via2=false";

    glbContRel = 1;


}

function relatorioRazao()
{
	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	//var nEmpresaID = aEmpresaData[0];
	var nEmpresaID = SelectToPipeBar(selUndsRazao);
	var sFiltro = txtFiltro.value;
	
	sFiltro = (trimStr(sFiltro) == '' ? 'NULL' : sFiltro);

    // campo txtDataInicio2 e txtDataFim devem ser trimados
    txtDataInicio2.value = trimStr(txtDataInicio2.value);
    txtDataFim2.value = trimStr(txtDataFim2.value);
    
    // verifica os campos datas
    if (txtDataInicio2.value != '')
    {
        if ( !chkDataEx(txtDataInicio2.value) )
        {
            if ( window.top.overflyGen.Alert ('Data inicial inv�lida') == 0 )
                return null;
                
            lockControlsInModalWin(false);
            txtDataInicio2.focus();
            return false;
        }
    }

    if (txtDataFim2.value != '')
    {
        if ( !chkDataEx(txtDataFim2.value) )
        {
            if ( window.top.overflyGen.Alert ('Data final inv�lida') == 0 )
                return null;
            lockControlsInModalWin(false);
            txtDataFim2.focus();
            return false;
        }
    }

    var Formato = "2";

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia +
                    "&selConta=" + (selConta.value == 0 ? 'NULL' : selConta.value) + "&selEstado=" + (selEstado.value == 0 ? 'NULL' : selEstado.value) +
                    "&selDetalhe=" + (selDetalhe.value == 0 ? 'NULL' : selDetalhe.value) + "&selConcilia=" + selConcilia.value + "&Formato=" + Formato + 
                    "&DataInicio2=" + '\'' + putDateInMMDDYYYY2(trimStr(txtDataInicio2.value)) + '\'' + "&DataFim2=" + '\'' + putDateInMMDDYYYY2(trimStr(txtDataFim2.value)) + '\'' +
                    "&sFiltro=" + sFiltro + "&nEmpresaID2=" + nEmpresaID + "&chkImprimeDetalhe=" + (chkImprimeDetalhe.checked == true ? 1 : 0);

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modcontabil/submodcontabilidade/planocontas/serverside/Reports_planocontas.aspx?" + strParameters + "&via2=false";

    glbContRel = 1;
    

}

/*******************************************************************
Imprime 3 relatorios de Balancete de Verificacao
*******************************************************************/
function relatoriosBalanceteVerificacao(nTipoRel)
{
    var nEmpresaLogada = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var sDataInicio = 'NULL';
    var sDataFim = 'NULL';
    
    // verifica os campos datas
    if (txtDataInicio3.value != '')
    {
        if ( !chkDataEx(txtDataInicio3.value) )
        {
            if ( window.top.overflyGen.Alert ('Data inicial inv�lida') == 0 )
                return null;
                
            lockControlsInModalWin(false);
            txtDataInicio3.focus();
            return false;
        }
    }

    if (txtDataFim3.value != '')
    {
        if ( !chkDataEx(txtDataFim3.value) )
        {
            if ( window.top.overflyGen.Alert ('Data final inv�lida') == 0 )
                return null;
            lockControlsInModalWin(false);
            txtDataFim3.focus();
            return false;
        }
    }

	if (trimStr(txtDataInicio3.value) != '')
		sDataInicio = '\'' + normalizeDate_DateTime(txtDataInicio3.value, true) + '\'';

	if (trimStr(txtDataFim3.value) != '')
		sDataFim = '\'' + normalizeDate_DateTime(txtDataFim3.value, true) + '\'';

    var sInformation = '';
    
	sInformation += translateTerm('N�vel', null) + ': ' + selNivel3.value; 
	
	if (txtDataInicio3.value != '')
	{
		if (txtDataFim3.value != '')
			sInformation += '   ' + translateTerm('Per�odo', null) + ': ';
		else
			sInformation += '   ' + translateTerm('Data inicial', null) + ': ';

		sInformation +=	txtDataInicio3.value;

		if (txtDataFim3.value != '')
			sInformation += ' ' + translateTerm('at�', null) + ' ' + txtDataFim3.value;
	}
	else if (txtDataFim3.value != '')
	    sInformation += ' ' + translateTerm('Data final', null) + ': ' + txtDataFim3.value;


	//Envio dos par�metros pro lado servidor	
	var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + nEmpresaLogada[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + selFormatoBalancete.value +
                        "&nEmpresaLogadaIdioma=" + getDicCurrLang() + "&selNivel3=" + (selNivel3.value == null ? "0" : selNivel3.value) + "&chkLancamentos=" + (chkLancamentos.checked ? "1" : "0") +
	                    "&chkSaldo=" + (chkSaldo.checked ? "1" : "0") + "&chkSaldoEmNiveis=" + (chkSaldoEmNiveis.checked ? "1" : "0") + "&sDataInicio=" + sDataInicio +
	                    "&sDataFim=" + sDataFim + "&chkPadraoUSA=" + (chkPadraoUSA.checked ? "1" : "0") + "&sInformation=" + sInformation;

	pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
	lockControlsInModalWin(true);

	var frameReport = document.getElementById("frmReport");
	frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
	frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modcontabil/submodcontabilidade/planocontas/serverside/Reports_planocontas.aspx?" + strParameters;

	pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
}

