﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.planocontas.serverside
{
    public partial class contabilidadebalancete : System.Web.UI.OverflyPage
    {

        protected static Integer Zero = new Integer(0);
        private string Empresas;

        public string sEmpresas
        {
            get { return Empresas; }
            set { Empresas = value; }
        }
        private Integer MoedaID;

        public Integer nMoedaID
        {
            get { return MoedaID; }
            set { MoedaID = value; }
        }
        private Integer Nivel;

        public Integer nNivel
        {
            get { return Nivel; }
            set { Nivel = value; }
        }
        private string LancamentosConciliados;

        public string nLancamentosConciliados
        {
            get { return LancamentosConciliados; }
            set { LancamentosConciliados = value; }
        }
        private string TemLancamentos;

        public string nTemLancamentos
        {
            get { return TemLancamentos; }
            set { TemLancamentos = value; }
        }
        private string TemSaldo;

        public string nTemSaldo
        {
            get { return TemSaldo; }
            set { TemSaldo = value; }
        }
        private string ContasPatrimoniais;

        public string nContasPatrimoniais
        {
            get { return ContasPatrimoniais; }
            set { ContasPatrimoniais = value; }
        }
        private string ContasResultados;

        public string nContasResultado
        {
            get { return ContasResultados; }
            set { ContasResultados = (value == null ? "0" : value); }
        }
        private Integer TipoDetalhamentoID;

        public Integer nTipoDetalhamentoID
        {
            get { return TipoDetalhamentoID; }
            set { TipoDetalhamentoID = value; }
        }
        private string Milhar;

        public string nMilhar
        {
            get { return Milhar; }
            set { Milhar = value; }
        }
        private string DataInicio;

        public string sDataInicio
        {
            get { return DataInicio; }
            set { DataInicio = value; }
        }
        private string DataFim;

        public string sDataFim
        {
            get { return DataFim; }
            set { DataFim = value; }
        }
        private Integer AgruparPor;

        public Integer nAgruparPor
        {
            get { return AgruparPor; }
            set { AgruparPor = (value == Zero ? null : value) ; }
        }
        private Integer IdiomaID;

        public Integer nIdiomaID
        {
            get { return IdiomaID; }
            set { IdiomaID = value; }
        }


        private DataSet Balancete()
        {
            ProcedureParameters[] parameters = new ProcedureParameters[18];

            parameters[0] = new ProcedureParameters(
                "@Empresas",
                System.Data.SqlDbType.VarChar,
                (Empresas != null ? (object)Empresas.ToString() : DBNull.Value));

            parameters[1] = new ProcedureParameters(
                "@MoedaID",
                System.Data.SqlDbType.Int,
                (MoedaID != null ? (object)MoedaID : DBNull.Value));

            parameters[2] = new ProcedureParameters(
                "@TipoResultado",
                System.Data.SqlDbType.Int,
                1);

            parameters[3] = new ProcedureParameters(
                "@ColunasControle",
                System.Data.SqlDbType.Bit,
                true);

            parameters[4] = new ProcedureParameters(
                "@Nivel",
                System.Data.SqlDbType.Int,
                (Nivel != null ? (object)Nivel : DBNull.Value));

            parameters[5] = new ProcedureParameters(
                "@LancamentoConciliado",
                System.Data.SqlDbType.Bit,
                (LancamentosConciliados.ToString() == "0" ? false : true));

            parameters[6] = new ProcedureParameters(
                "@TemLancamentos",
                System.Data.SqlDbType.Bit,
                (TemLancamentos.ToString() == "0" ? false : true));

            parameters[7] = new ProcedureParameters(
                "@TemSaldo",
                System.Data.SqlDbType.Bit,
                (TemSaldo.ToString() == "0" ? false : true));

            parameters[8] = new ProcedureParameters(
                "@ContasPatrimonio",
                System.Data.SqlDbType.Bit,
                (ContasPatrimoniais.ToString() == "0" ? false : true));

            parameters[9] = new ProcedureParameters(
                "@ContasResultado",
                System.Data.SqlDbType.Bit,
                (ContasResultados.ToString() == "0" ? false : true));

            parameters[10] = new ProcedureParameters(
                "@TipoDetalhamentoID",
                System.Data.SqlDbType.Int,
                (TipoDetalhamentoID.intValue() != 0 ? (object)TipoDetalhamentoID : DBNull.Value));

            parameters[11] = new ProcedureParameters(
                "@Milhar",
                System.Data.SqlDbType.Bit,
                (Milhar.ToString() == "0" ? false : true));

            parameters[12] = new ProcedureParameters(
                "@SaldoEmNiveis",
                System.Data.SqlDbType.Bit,
                false);

            parameters[13] = new ProcedureParameters(
                "@dtInicio",
                System.Data.SqlDbType.DateTime,
                (DataInicio != null ? (object)DataInicio : DBNull.Value));

            parameters[14] = new ProcedureParameters(
                "@dtFim",
                System.Data.SqlDbType.DateTime,
                (DataFim != null ? (object)DataFim : DBNull.Value));

            parameters[15] = new ProcedureParameters(
                "@AgruparPor",
                System.Data.SqlDbType.Int,
                (AgruparPor != null ? (object)AgruparPor : DBNull.Value));

            parameters[16] = new ProcedureParameters(
                "@IdiomaID",
                System.Data.SqlDbType.Int,
                (IdiomaID != null ? (object)IdiomaID : DBNull.Value));

            parameters[17] = new ProcedureParameters(
                "@USA",
                System.Data.SqlDbType.Bit,
                DBNull.Value);


            return DataInterfaceObj.execQueryProcedure( "sp_Contabilidade_Balancete", parameters);
        }
       
        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(Balancete()); 
        }
    }
}