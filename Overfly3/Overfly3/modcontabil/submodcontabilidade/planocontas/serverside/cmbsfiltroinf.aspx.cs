﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;


namespace Overfly3.modcontabil.submodcontabilidade.planocontas.serverside
{
    public partial class cmbsfiltroinf : System.Web.UI.OverflyPage
    {

        private string StrSQL = "";
        private string strFiltro = "";

        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private string ContaID;

        public string nContaID
        {
            get { return ContaID; }
            set { ContaID = value; }
        }
        private Integer AceitaLancamento;

        public Integer nAceitaLancamento
        {
            get { return AceitaLancamento; }
            set { AceitaLancamento = value; }
        }
        private Integer TipoDetalhamentoID;

        public Integer nTipoDetalhamentoID
        {
            get { return TipoDetalhamentoID; }
            set { TipoDetalhamentoID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            if (AceitaLancamento.intValue() == 1)
            {
                strFiltro = "a.ContaID = " + ContaID + " AND ";
            }
            else
            {
                strFiltro = "dbo.fn_Conta_EhFilha(a.ContaID, " + ContaID + ", 1) = 1 AND ";
            }

            if (TipoDetalhamentoID.intValue() >= 1532 && nTipoDetalhamentoID.intValue() <= 1541)
            {
                StrSQL = "SELECT 0 AS Indice, 0 AS fldID, 'Todos' AS fldName UNION ALL " +
                 "SELECT 1 AS Indice, c.PessoaID AS fldID, c.Fantasia AS fldName " +
                    "FROM Lancamentos_Contas a WITH(NOLOCK), Lancamentos b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " +
                    "WHERE (" + strFiltro + " a.LancamentoID = b.LancamentoID AND " +
                        "b.EmpresaID = dbo.fn_Empresa_Matriz(" + EmpresaID + ", 1, NULL) AND b.EstadoID IN (2, 81, 82) AND a.PessoaID = c.PessoaID AND " +
                        "dbo.fn_Pessoa_Tipo(c.PessoaID, " + EmpresaID + ", 2, " + TipoDetalhamentoID + ") = 1) " +
                 "GROUP BY c.PessoaID, c.Fantasia " +
                 "ORDER BY Indice, fldName";
            }


            // Conta Bancaria

            else if (TipoDetalhamentoID.intValue() == 1542)
            {
                StrSQL = "SELECT 0 AS Indice, 0 AS fldID, 'Todos' AS fldName UNION ALL " +
                         "SELECT 1 AS Indice, a.RelPesContaID AS fldID, dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) AS fldName " +
                            "FROM Lancamentos_Contas a WITH(NOLOCK), Lancamentos b WITH(NOLOCK) " +
                            "WHERE (" + strFiltro + " a.LancamentoID = b.LancamentoID AND b.EmpresaID = " + EmpresaID + " AND " +
                            "b.EstadoID IN (2, 81, 82)) " +
                         "GROUP BY a.RelPesContaID " +
                         "ORDER BY Indice, fldName";
            }
            //'Impostos
            else if (TipoDetalhamentoID.intValue() == 1543)
            {

                StrSQL = "SELECT 0 AS Indice, 0 AS fldID, 'Todos' AS fldName UNION ALL " +
                         "SELECT 1 AS Indice, a.ImpostoID AS fldID, c.Imposto AS fldName " +
                            "FROM Lancamentos_Contas a WITH(NOLOCK), Lancamentos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " +
                            "WHERE (" + strFiltro + " a.LancamentoID = b.LancamentoID AND b.EmpresaID = " + EmpresaID + " AND " +
                                "b.EstadoID IN (2, 81, 82) AND a.ImpostoID = c.ConceitoID) " +
                        "GROUP BY a.ImpostoID, c.Imposto " +
                        "ORDER BY Indice, fldName ";
            }
            else
                StrSQL = "SELECT -1 AS Indice, -1 AS fldID, SPACE(0) AS fldName ";

            WriteResultXML(DataInterfaceObj.getRemoteData(StrSQL));
        }
    }
}