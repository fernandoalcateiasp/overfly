﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_planocontas : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        private string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        private string nEmpresaLogadaIdioma = Convert.ToString(HttpContext.Current.Request.Params["nEmpresaLogadaIdioma"]);
        private string glb_sEmpresaRazao = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaRazao"]);
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);
        private int formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string sDicCurrLang = Convert.ToString(HttpContext.Current.Request.Params["getDicCurrLang"]);
        private string nEmpresaID2 = Convert.ToString(HttpContext.Current.Request.Params["nEmpresaID2"]);
        private string DataInicio = Convert.ToString(HttpContext.Current.Request.Params["DataInicio"]);
        private string DataFim = Convert.ToString(HttpContext.Current.Request.Params["DataFim"]);
        private string DataInicio2 = Convert.ToString(HttpContext.Current.Request.Params["DataInicio2"]);
        private string DataFim2 = Convert.ToString(HttpContext.Current.Request.Params["DataFim2"]);
        private string selConta = Convert.ToString(HttpContext.Current.Request.Params["selConta"]);
        private string selEstado = Convert.ToString(HttpContext.Current.Request.Params["selEstado"]);
        private string selDetalhe = Convert.ToString(HttpContext.Current.Request.Params["selDetalhe"]);
        private string selConcilia = Convert.ToString(HttpContext.Current.Request.Params["selConcilia"]);
        private string sFiltro = Convert.ToString(HttpContext.Current.Request.Params["sFiltro"]);
        private string chkImprimeDetalhe = Convert.ToString(HttpContext.Current.Request.Params["chkImprimeDetalhe"]);
        private string selNivel3 = Convert.ToString(HttpContext.Current.Request.Params["selNivel3"]);
        private string chkLancamentos = Convert.ToString(HttpContext.Current.Request.Params["chkLancamentos"]);
        private string chkSaldo = Convert.ToString(HttpContext.Current.Request.Params["chkSaldo"]);
        private string chkSaldoEmNiveis = Convert.ToString(HttpContext.Current.Request.Params["chkSaldoEmNiveis"]);
        private string sDataInicio = Convert.ToString(HttpContext.Current.Request.Params["sDataInicio"]);
        private string sDataFim = Convert.ToString(HttpContext.Current.Request.Params["sDataFim"]);
        private string chkPadraoUSA = Convert.ToString(HttpContext.Current.Request.Params["chkPadraoUSA"]);
        private string sInformation = Convert.ToString(HttpContext.Current.Request.Params["sInformation"]);

        string SQLImagem = string.Empty;
        string SQLDetail1 = string.Empty;
        string SQLDetail2 = string.Empty;
        string SQLDetail3 = string.Empty;
        string SQLDetail4 = string.Empty;

        int Datateste = 0;
        bool nulo = false;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "40301":
                        relatorioPlanoContas();
                        break;

                    case "40302":
                        relatorioDiario();
                        break;

                    case "40303":
                        relatorioRazao();
                        break;

                    case "40307":
                        relatorioRazao();
                        break;

                    case "40304":
                        relatoriosBalanceteVerificacao("1");
                        break;

                    case "40305":
                        relatoriosBalanceteVerificacao("2");
                        break;

                    case "40306":
                        relatoriosBalanceteVerificacao("3");
                        break;
                }

            }
        }

        public void relatorioDiario()
        {
            string Title = "Diário";



            string strSQL1 = "SELECT DISTINCT CONVERT(VARCHAR(10),b.dtBalancete,103) AS Data, b.LancamentoID AS [Lançamento], " +
                                "dbo.fn_Lancamento_Historico(b.LancamentoID, 0) AS [Histórico], " +
                                "dbo.fn_Tradutor(dbo.fn_Lancamento_Detalhe(a.LancamentoID), 246, " + sDicCurrLang + ", " + "'" + "A" + "'" + ") AS Detalhe " +
                                "FROM Lancamentos_Contas a WITH(NOLOCK), Lancamentos b WITH(NOLOCK), PlanoContas c WITH(NOLOCK) " +
                            "WHERE ((a.LancamentoID = b.LancamentoID) AND (a.ContaID = c.ContaID) AND " +
                                "(b.EmpresaID in " + nEmpresaID2 + ") AND " +
                                "(b.dtBalancete >= " + "'" + DataInicio + "'" + " AND " +
                                "b.dtBalancete <= " + "'" + DataFim + "'" + ")) " +
                            "ORDER BY Data,b.LancamentoID";


            string strSQL2 = "SELECT CONVERT(VARCHAR(10),b.dtBalancete,103) AS Data, b.LancamentoID AS [Lançamento], dbo.fn_Lancamento_Historico(b.LancamentoID, 0) AS [Histórico]," +
                                "a.ContaID AS ContaID, dbo.fn_Conta_Identada(a.ContaID, " + sDicCurrLang + ", 0, 0) AS Conta, " +
                                "dbo.fn_Lancamento_Detalhe(-a.LanContaID) AS Detalhe, " +
                                "a.Documento AS Documento, a.Valor AS Valor " +
                             "FROM Lancamentos_Contas a WITH(NOLOCK), Lancamentos b WITH(NOLOCK), PlanoContas c WITH(NOLOCK) " +
                             "WHERE ((a.LancamentoID = b.LancamentoID) AND (a.ContaID = c.ContaID) AND " +
                                "(b.EmpresaID in " + nEmpresaID2 + ") AND " +
                                "(b.dtBalancete >= " + "'"+DataInicio + "'" +" AND " +
                                "b.dtBalancete <= " + "'" + DataFim + "'" +")) " +
                             "ORDER BY Data, b.LancamentoID, a.Valor";

            var Header_Body = (formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] {{ "Query1", strSQL1 },
                                         { "Query2", strSQL2 }};


            string[,] arrIndexKey = new string[,] {{ "Query2", "Lançamento;ContaID", "Lançamento" } };

            if (formato == 1)
            {
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape", "1.5", "Default", "Default", true);

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query2"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {

                    double PosYHeader = 0;

                    Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, "0.2", PosYHeader.ToString(), "12", "Black", "B", "Header", "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Diário", "13.5", PosYHeader.ToString(), "12", "Black", "B", "Header", "6", "");
                    Relatorio.CriarObjLabelData("Now", "", "", "24", PosYHeader.ToString(), "11", "black", "B", "Header", "3.72187", "Horas");
                    Relatorio.CriarObjLabel("Fixo", "", "Início:", "0.2", (PosYHeader+=0.5).ToString(), "11", "Black", "B", "Header", "1.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", DataInicio2, "1.5", PosYHeader.ToString(), "11", "Black", "B", "Header", "2.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Fim:", "4", PosYHeader.ToString(), "11", "Black", "B", "Header", "1", "");
                    Relatorio.CriarObjLabel("Fixo", "", DataFim2, "5", PosYHeader.ToString(), "11", "Black", "B", "Header", "2.5", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Data", "0.2", (PosYHeader += 0.5).ToString(), "9", "Black", "B", "Header", "2.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Lançamento", "2.7", PosYHeader.ToString(), "9", "Black", "B", "Header", "3", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Histórico", "5.7", PosYHeader.ToString(), "9", "Black", "B", "Header", "5.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "ID", "11.2", PosYHeader.ToString(), "9", "Black", "B", "Header", "2.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Conta", "13.7", PosYHeader.ToString(), "9", "Black", "B", "Header", "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Detalhe", "19.7", PosYHeader.ToString(), "9", "Black", "B", "Header", "4", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Documento", "23.7", PosYHeader.ToString(), "9", "Black", "B", "Header", "3.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Valor", "27.2", PosYHeader.ToString(), "9", "Black", "B", "Header", "2", "");

                    Relatorio.CriarObjLinha("0.2", "1", "29.6", "", "Header");

                    Relatorio.CriarObjTabela("0.2", "0", arrIndexKey, false, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "Data", false, "2.5","Default","1","0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(" ", "Lançamento", false, "3", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Lançamento", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(" ", "Histórico", false, "5.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Histórico", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(" ", "ContaID", false, "2.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ContaID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(" ", "Conta", false, "6", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Conta", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(" ", "Detalhe", false, "4", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Detalhe", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(" ", "Documento", false, "3.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Documento", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela(" ", "Valor", false, "2", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");


                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, formato);
                }
            }

            else if (formato == 2) // Se Excel
            {

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query2"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    //Cria Tabela           
                    Relatorio.CriarObjTabela("0", "0", arrIndexKey, true, true);


                    Relatorio.CriarObjColunaNaTabela("Data", "Data", true, "Data");
                    Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Lançamento", "Lançamento", true, "Lançamento");
                    Relatorio.CriarObjCelulaInColuna("Query", "Lançamento", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Histórico", "Histórico", true, "Histórico");
                    Relatorio.CriarObjCelulaInColuna("Query", "Histórico", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("ContaID", "ContaID", true, "ContaID");
                    Relatorio.CriarObjCelulaInColuna("Query", "ContaID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Conta", "Conta", true, "Conta");
                    Relatorio.CriarObjCelulaInColuna("Query", "Conta", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Detalhe", "Detalhe", true, "Detalhe");
                    Relatorio.CriarObjCelulaInColuna("Query", "Detalhe", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Documento", "Documento", true, "Documento");
                    Relatorio.CriarObjCelulaInColuna("Query", "Documento", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Valor", "Valor", true, "Valor");
                    Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");

                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, formato);
                }              
            }            
        }
        public void relatorioRazao()
        {
            string Title = "Razão";

            string strSQL2 = "";


            string strSQL1 = "EXEC sp_Contabilidade_Razao " + nEmpresaID2 + ", NULL, " + selConta + ", " +
                    selEstado + ", " + selDetalhe + ", NULL, " + selConcilia + ", 0, 0, NULL, 8, 1, " + DataInicio2 + ", " +
                    DataFim2 + ", NULL, " + sFiltro;

            if (chkImprimeDetalhe == "1") //Detalhe marcado
            {
                strSQL2 = "EXEC sp_Contabilidade_Razao " + nEmpresaID2 + ", NULL, " + selConta + ", " + selEstado + ", " + selDetalhe +
                    ", NULL, " + selConcilia + ", 0, 0, NULL, 11, 1, " + DataInicio2 + ", " + DataFim2 + ", NULL, " + sFiltro;
            }
            else //Detalhe desmarcado
            {
                strSQL2 = "EXEC sp_Contabilidade_Razao " + nEmpresaID2 + ", NULL, " + selConta + ", " +
                        selEstado + ", " + selDetalhe + ", NULL, " + selConcilia + ", 0, 0, NULL, 10, 1, " + DataInicio2 + ", " +
                        DataFim2 + ", NULL, " + sFiltro;
            }


            var Header_Body = (formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] {{ "Query1", strSQL1 },
                                         { "Query2", strSQL2 }};


            string[,] arrIndexKey = new string[,] { { "" } };

            if (formato == 1)
            {
                ;
            }

            else if (formato == 2) // Se Excel
            {

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query2"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    //Cria Tabela 1    
                    Relatorio.CriarObjTabela("0", "0", "Query1", true, false);

                    Relatorio.CriarObjColunaNaTabela("C/Partida", "ContraPartida", false, "2");
                    Relatorio.CriarObjCelulaInColuna("Query", "ContraPartida", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Histórico", "Historico", false, "3");
                    Relatorio.CriarObjCelulaInColuna("Query", "Historico", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Detalhe", "Detalhe", false, "3");
                    Relatorio.CriarObjCelulaInColuna("Query", "Detalhe", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Documento", "Documento", false, "5");
                    Relatorio.CriarObjCelulaInColuna("Query", "Documento", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Valor", "Valor", false, "3");
                    Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Saldo", "Saldo", false, "4");
                    Relatorio.CriarObjCelulaInColuna("Query", "Saldo", "DetailGroup");

                    Relatorio.TabelaEnd();

                    if (chkImprimeDetalhe == "1")
                    {
                        //Cria Tabela 2
                        Relatorio.CriarObjTabela("0", "0.5", "Query2", true, false);

                        Relatorio.CriarObjColunaNaTabela("ContaID", "ContaID", false, "2");
                        Relatorio.CriarObjCelulaInColuna("Query", "ContaID", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Data", "Data", false, "3");
                        Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Lançamento", "Lançamento", false, "3");
                        Relatorio.CriarObjCelulaInColuna("Query", "Lançamento", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Histórico", "Historico", false, "5");
                        Relatorio.CriarObjCelulaInColuna("Query", "Historico", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Detalhe", "Detalhe", false, "3");
                        Relatorio.CriarObjCelulaInColuna("Query", "Detalhe", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("C/Partida", "ContraPartida", false, "4");
                        Relatorio.CriarObjCelulaInColuna("Query", "ContraPartida", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Documento", "Documento", false, "4");
                        Relatorio.CriarObjCelulaInColuna("Query", "Documento", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Valor", "Valor", false, "3");
                        Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Saldo", "Saldo", false, "3");
                        Relatorio.CriarObjCelulaInColuna("Query", "Saldo", "DetailGroup");
                    }
                    else
                    {
                        //Cria Tabela 2
                        Relatorio.CriarObjTabela("0", "0.5", "Query2", true, false);

                        Relatorio.CriarObjColunaNaTabela("ContaID", "ContaID", false, "2");
                        Relatorio.CriarObjCelulaInColuna("Query", "ContaID", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Data", "Data", false, "3");
                        Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Lançamento", "Lançamento", false, "3");
                        Relatorio.CriarObjCelulaInColuna("Query", "Lançamento", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Histórico", "Historico", false, "5");
                        Relatorio.CriarObjCelulaInColuna("Query", "Historico", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Detalhe", "Detalhe", false, "3");
                        Relatorio.CriarObjCelulaInColuna("Query", "Detalhe", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Valor", "Valor", false, "4");
                        Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Saldo", "Saldo", false, "3");
                        Relatorio.CriarObjCelulaInColuna("Query", "Saldo", "DetailGroup");
                    }


                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, formato);
                }
            }
        }

        public void relatorioPlanoContas()
        {
            string title = Convert.ToString(HttpContext.Current.Request.Params["title"]);
            string sInformation = Convert.ToString(HttpContext.Current.Request.Params["sInformation"]);
            string strEstados = Convert.ToString(HttpContext.Current.Request.Params["strEstados"]);
            int selNivel = Convert.ToInt32(HttpContext.Current.Request.Params["selNivel"]);
            bool chkDetalhePL = Convert.ToBoolean(HttpContext.Current.Request.Params["chkDetalhePL"]);


            string Title = title;

            string sqlParm = "103";
            if (nEmpresaLogadaIdioma == "245")
            {
                sqlParm = "101";
            }

            string strSQL = " SELECT " +
                                " a.ContaID AS ID, c.RecursoAbreviado AS Est, dbo.fn_Conta_Identada(a.ContaID, " + nEmpresaLogadaIdioma + ", NULL, NULL) AS Conta, " +
                                " dbo.fn_Tradutor(e.ItemMasculino, 246, " + nEmpresaLogadaIdioma + " , 'A') AS Tipo, dbo.fn_Conta_Nivel(a.ContaID) AS Nivel, d.Iniciais AS Resp, " +
                                " a.ContaMaeID AS ContaMae, dbo.fn_Tradutor(b.ItemMasculino, 246, " + nEmpresaLogadaIdioma + " , 'A') AS Detalhamento, " +
                                " CASE dbo.fn_Conta_AceitaLancamento(a.ContaID) WHEN 1 THEN CHAR(88) ELSE SPACE(0) END AS AL, " +
                                " CASE a.LancamentoManual WHEN 1 THEN CHAR(88) ELSE SPACE(0) END AS ALM, CASE a.SaldoDevedor WHEN 1 THEN CHAR(88) ELSE SPACE(0) END AS SD, " +
                                " CASE a.SaldoCredor WHEN 1 THEN CHAR(88) ELSE SPACE(0) END AS SC, CASE a.UsoRestrito WHEN 1 THEN CHAR(88) ELSE SPACE(0) END AS UR, " +
                                " CONVERT(VARCHAR,dbo.fn_Conta_Data(a.ContaID, 1), " + sqlParm + ") AS Cadastro, CONVERT(VARCHAR,dbo.fn_Conta_Data(a.ContaID, 2), " + sqlParm + ") AS UltMov, " + 
                                " 1 AS Registro_ " +
                            " FROM " +
                                " PlanoContas a WITH(NOLOCK) " +
                                    " LEFT OUTER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (a.TipoContaID = e.ItemID) " +
                                    " INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (a.TipoDetalhamentoID = b.ItemID) " +
                                    " INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = a.EstadoID) " +
                                    " INNER JOIN RelacoesPesRec d WITH(NOLOCK) ON (d.SujeitoID = a.ProprietarioID) " +
                            " WHERE ((d.TipoRelacaoID = 11 AND d.ObjetoID = 999) AND (dbo.fn_Conta_Nivel(a.ContaID) <= " + selNivel + " )) ";

            strSQL += (strEstados == "" ? "" : " AND a.EstadoID IN " + strEstados + " ");

            strSQL += " ORDER BY a.ContaID ";


            arrSqlQuery = new string[,] { { "strSQL", strSQL } };


            string Portrait_Landscape = "Portrait";
            if (chkDetalhePL)
                Portrait_Landscape = "Landscape";

            if (formato == 1)
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", Portrait_Landscape, "1.5", "Default", "Default", true);
            else if (formato == 2)
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape");


            DataSet dsFin = new DataSet();
            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            int linhas = dsFin.Tables["strSQL"].Rows.Count;

            if (linhas == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {
                if (formato == 1) //SE PDF
                {
                    double headerX = 0.2;
                    double headerY = 0.2;
                    double tabelaTitulo = 1.2;

                    double tabelaX = 0.2;
                    double tabelaY = 0;

                    double bodyX = 0.2;
                    double bodyY = 0.0;

                    if (Portrait_Landscape == "Portrait")
                    {
                        //Header
                        Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaRazao, (headerX).ToString(), headerY.ToString(), "11", "black", "B", "Header", "8", "");
                        Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 10).ToString(), headerY.ToString(), "11", "#0113a0", "B", "Header", "9", "");
                        Relatorio.CriarObjLabelData("Now", "", "", (headerX + 15).ToString(), headerY.ToString(), "9", "black", "B", "Header", "3.72187", "Horas");
                        Relatorio.CriarNumeroPagina((headerX + 18.4).ToString(), (headerY).ToString(), "9", "Black", "B", false);

                        //Header Parametros
                        Relatorio.CriarObjLabel("Fixo", "", sInformation, (headerX).ToString(), (headerY + 0.5).ToString(), "9", "black", "B", "Header", "19", "");

                        //Linha
                        Relatorio.CriarObjLinha((headerX).ToString(), (headerY + 0.9).ToString(), "20.5", "", "Header");
                    }
                    else if (Portrait_Landscape == "Landscape")
                    {
                        Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaRazao, (headerX).ToString(), headerY.ToString(), "12", "black", "B", "Header", "8", "");
                        Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 12).ToString(), headerY.ToString(), "12", "#0113a0", "B", "Header", "9", "");
                        Relatorio.CriarObjLabelData("Now", "", "", (headerX + 23.5).ToString(), headerY.ToString(), "10", "black", "B", "Header", "3.72187", "Horas");
                        Relatorio.CriarNumeroPagina((headerX + 27).ToString(), (headerY).ToString(), "10", "Black", "B", false);

                        //Header Parametros
                        Relatorio.CriarObjLabel("Fixo", "", sInformation, (headerX).ToString(), (headerY + 0.5).ToString(), "8", "black", "B", "Header", "29", "");

                        //Linha
                        Relatorio.CriarObjLinha((headerX).ToString(), (headerY + 0.9).ToString(), "29", "", "Header");
                    }
                    
                    //Header titulo da tabela
                    Relatorio.CriarObjLabel("Fixo", "", "ID", (headerX).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Est", (headerX + 2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Conta", (headerX + 2.9).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Tipo", (headerX + 10.8).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Nível", (headerX + 12.3).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    if (chkDetalhePL)
                    {
                        Relatorio.CriarObjLabel("Fixo", "", "Resp", (headerX + 13.6).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Conta Mãe", (headerX + 14.8).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Detalhamento", (headerX + 16.6).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "AL", (headerX + 20.2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "ALM", (headerX + 21.2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "SD", (headerX + 22.2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "SC", (headerX + 23.2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "UR", (headerX + 24.2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Cadastro", (headerX + 25.2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                        Relatorio.CriarObjLabel("Fixo", "", "Últ Mov", (headerX + 26.9).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    }
                    
                    //Tabela
                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "strSQL", false, false, false);

                    Relatorio.CriarObjColunaNaTabela(" ", "ID", false, "2", "Default","1","0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Est", false, "0.9", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Est", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Conta", false, "7.9", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Conta", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Tipo", false, "1.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Tipo", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Nivel", false, "1.3", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Nivel", "DetailGroup", "Null", "Null", 0, false, "8");

                    if (chkDetalhePL)
                    {
                        Relatorio.CriarObjColunaNaTabela(" ", "Resp", false, "1.2", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Resp", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "ContaMae", false, "1.8", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "ContaMae", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "Detalhamento", false, "3.6", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Detalhamento", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "AL", false, "1", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "AL", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "ALM", false, "1", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "ALM", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "SD", false, "1", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "SD", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "SC", false, "1", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "SC", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "UR", false, "1", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "UR", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "Cadastro", false, "1.7", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Cadastro", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.CriarObjColunaNaTabela(" ", "UltMov", false, "1.7", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "UltMov", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Separador totais
                        Relatorio.CriarObjLinha((bodyX).ToString(), (bodyY + 0.5).ToString(), "29", "", "Body");

                        Relatorio.CriarObjLabel("Fixo", "", ""+linhas, (bodyX).ToString(), (bodyY + 0.8).ToString(), "8", "black", "B", "Body", "15", "");
                    }
                    Relatorio.TabelaEnd();
                    

                }
                else if (formato == 2) // Se Excel
                {
                    double headerX = 0;
                    double headerY = 0;

                    double tabelaX = 0;
                    double tabelaY = 0.3;

                    //Header
                    Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaRazao, (headerX).ToString(), headerY.ToString(), "8", "black", "B", "Body", "7", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 7.9).ToString(), headerY.ToString(), "8", "#0113a0", "B", "Body", "7.9", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 17.3).ToString(), headerY.ToString(), "8", "black", "B", "Body", "2.5", "");

                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "strSQL", false, false, false);

                    Relatorio.CriarObjColunaNaTabela("ID", "ID", false, "7", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Est", "Est", false, "0.9", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Est", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Conta", "Conta", false, "7.9", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Conta", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Tipo", "Tipo", false, "1.5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Tipo", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Nivel", "Nivel", false, "2.5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Nivel", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Resp", "Resp", false, "1.2", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Resp", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Conta Mãe", "ContaMae", false, "1.8", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "ContaMae", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Detalhamento", "Detalhamento", false, "3.6", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Detalhamento", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("AL", "AL", false, "1", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "AL", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("ALM", "ALM", false, "1", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "ALM", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("SD", "SD", false, "1", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "SD", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("SC", "SC", false, "1", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "SC", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("UR", "UR", false, "1", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "UR", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Cadastro", "Cadastro", false, "1.7", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Cadastro", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Últ Mov", "UltMov", false, "1.7", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "UltMov", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.TabelaEnd();
                }
            }
            Relatorio.CriarPDF_Excel(Title, formato);
        }
        public void relatoriosBalanceteVerificacao(string nTipoRel)
        {
            string Title = "";

            if(nTipoRel == "1")
            {
                Title = "Balancete de Verificação";
            }
            else if(nTipoRel == "2")
            {
                Title = "Balanço Patrimonial";
            }
            else if (nTipoRel == "3")
            {
                Title = "Demonstração de Resultados";
            }


            string strSQL1 = "EXEC sp_Contabilidade_Balancete '" + "/" + nEmpresaID + "/" +  "', NULL, " + nTipoRel + ", 1," + selNivel3 + ", NULL, " +
                               chkLancamentos +", " + chkSaldo +", 1, 1, NULL, NULL, " + chkSaldoEmNiveis +"," + sDataInicio + "," + sDataFim + ", NULL, " + 
                               nEmpresaLogadaIdioma + ", " + chkPadraoUSA;


            var Header_Body = (formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] {{ "Query1", strSQL1 }};


            string[,] arrIndexKey = new string[,] { { } };

            if (formato == 1)
            {
                var orientacao = "Landscape";
                double PosYHeader = 0;
                double PosYHeader1 = 0.5;
                double PosXHeader = 0.2;
                double PosXHeader2 = 13.3;
                double PosXHeader3 = 23.5;
                double PosXHeader4 = 27;
                double PosXHeader5 = 0;

                if (chkPadraoUSA == "1" && (Convert.ToInt32(selNivel3) <= 5 || chkSaldoEmNiveis == "0"))
                {
                    orientacao = "Portrait";
                    PosXHeader2 = 8.14;
                    PosXHeader3 = 15.2;
                    PosXHeader4 = 18.5;
                }

                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", orientacao , "1.5", "Default", "Default", true);

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    //Cabeçalho*************************************************************************************
                    Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, PosXHeader.ToString(), PosYHeader.ToString(), "12", "Black", "B", "Header", "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (PosXHeader + PosXHeader2).ToString(), PosYHeader.ToString(), "12", "Black", "B", "Header", "6", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (PosXHeader + PosXHeader3).ToString(), PosYHeader.ToString(), "11", "black", "B", "Header", "3.72187", "Horas");

                    Relatorio.CriarNumeroPagina((PosXHeader + PosXHeader4).ToString(), PosYHeader.ToString(), "10", "Black", "B", false);

                    Relatorio.CriarObjLabel("Fixo", "", sInformation, PosXHeader.ToString(), (PosYHeader += 0.5).ToString(), "11", "Black", "B", "Header", "18", "");

                    if(chkPadraoUSA == "0")
                    {
                        Relatorio.CriarObjLabel("Fixo", "", "ID", PosXHeader.ToString(), (PosYHeader += PosYHeader1).ToString(), "9", "Black", "B", "Header", (PosXHeader5 = 2).ToString(), "");
                        PosYHeader1 = 0;
                    }
                                  
                    Relatorio.CriarObjLabel("Fixo", "", "Conta", (PosXHeader += PosXHeader5).ToString(), (PosYHeader += PosYHeader1).ToString(), "9", "Black", "B", "Header", (PosXHeader5 = 8).ToString(), "");

                    if (chkPadraoUSA == "0")
                    {
                        Relatorio.CriarObjLabel("Fixo", "", "Nível", (PosXHeader += PosXHeader5).ToString(), PosYHeader.ToString(), "9", "Black", "B", "Header", (PosXHeader5 = 1.5).ToString(), "");
                        Relatorio.CriarObjLabel("Fixo", "", "Lançamentos", (PosXHeader += PosXHeader5).ToString(), PosYHeader.ToString(), "9", "Black", "B", "Header", (PosXHeader5 = 2.1).ToString(), "Right");

                    }

                    if (chkSaldoEmNiveis == "1")
                    {
                        if (Convert.ToInt32(selNivel3) >= 6)
                        {
                            Relatorio.CriarObjLabel("Fixo", "", "Saldo-6", (PosXHeader += PosXHeader5).ToString(), PosYHeader.ToString(), "9", "Black", "B", "Header", (PosXHeader5 = 2.5).ToString(), "Right");
                        }
                        if (Convert.ToInt32(selNivel3) >= 5)
                        {
                            Relatorio.CriarObjLabel("Fixo", "", "Saldo-5", (PosXHeader += PosXHeader5).ToString(), PosYHeader.ToString(), "9", "Black", "B", "Header", (PosXHeader5 = 2.5).ToString(), "Right");
                        }
                        if (Convert.ToInt32(selNivel3) >= 4)
                        {
                            Relatorio.CriarObjLabel("Fixo", "", "Saldo-4", (PosXHeader += PosXHeader5).ToString(), PosYHeader.ToString(), "9", "Black", "B", "Header", (PosXHeader5 = 2.5).ToString(), "Right");
                        }
                        if (Convert.ToInt32(selNivel3) >= 3)
                        {
                            Relatorio.CriarObjLabel("Fixo", "", "Saldo-3", (PosXHeader += PosXHeader5).ToString(), PosYHeader.ToString(), "9", "Black", "B", "Header", (PosXHeader5 = 2.5).ToString(), "Right");
                        }
                        if (Convert.ToInt32(selNivel3) >= 2)
                        {
                            Relatorio.CriarObjLabel("Fixo", "", "Saldo-2", (PosXHeader += PosXHeader5).ToString(), PosYHeader.ToString(), "9", "Black", "B", "Header", (PosXHeader5 = 2.5).ToString(), "Right");
                        }
                        if (Convert.ToInt32(selNivel3) >= 1)
                        {
                            Relatorio.CriarObjLabel("Fixo", "", "Saldo-1", (PosXHeader += PosXHeader5).ToString(), PosYHeader.ToString(), "9", "Black", "B", "Header", (PosXHeader5 = 2.5).ToString(), "Right");
                        }

                    }
                    else
                    {
                        Relatorio.CriarObjLabel("Fixo", "", "Saldo", (PosXHeader += PosXHeader5).ToString(), PosYHeader.ToString(), "9", "Black", "B", "Header", (PosXHeader5 = 2.5).ToString(), "Right");
                    }
                    
                    Relatorio.CriarObjLinha("0.2", "1", "29.6", "", "Header");


                    //Tabela*************************************************************************************
                    Relatorio.CriarObjTabela("0.2", "0", "Query1", false, false);

                    if (chkPadraoUSA == "0")
                    {
                        Relatorio.CriarObjColunaNaTabela(" ", "ID", false, "2", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup");
                    }



                    Relatorio.CriarObjColunaNaTabela(" ", "Conta", false, "8", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Conta", "DetailGroup");

                    if (chkPadraoUSA == "0")
                    {
                        Relatorio.CriarObjColunaNaTabela(" ", "Nivel", false, "1.5", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Nivel", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela(" ", "Lançamentos", false, "2.1", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Lançamentos", "DetailGroup");
                    }
                    
                    if (chkSaldoEmNiveis == "1")
                    {
                        if (Convert.ToInt32(selNivel3) >= 6)
                        {
                            Relatorio.CriarObjColunaNaTabela(" ", "Saldo6", false, "2.5", "Right", "1", "0");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo6", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                        }

                        if (Convert.ToInt32(selNivel3) >= 5)
                        {
                            Relatorio.CriarObjColunaNaTabela(" ", "Saldo5", false, "2.5", "Right", "1", "0");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo5", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                        }

                        if (Convert.ToInt32(selNivel3) >= 4)
                        {
                            Relatorio.CriarObjColunaNaTabela(" ", "Saldo4", false, "2.5", "Right", "1", "0");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo4", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                        }

                        if (Convert.ToInt32(selNivel3) >= 3)
                        {
                            Relatorio.CriarObjColunaNaTabela(" ", "Saldo3", false, "2.5", "Right", "1", "0");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo3", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                        }

                        if (Convert.ToInt32(selNivel3) >= 2)
                        {
                            Relatorio.CriarObjColunaNaTabela(" ", "Saldo2", false, "2.5", "Right", "1", "0");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo2", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                        }

                        if (Convert.ToInt32(selNivel3) >= 2)
                        {
                            Relatorio.CriarObjColunaNaTabela(" ", "Saldo1", false, "2.5", "Right", "1", "0");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo1", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                        }
                    }
                    else
                    {
                        Relatorio.CriarObjColunaNaTabela(" ", "Saldo", false, "2.5", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Saldo", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                    }

                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, formato);
                }
            }

            else if (formato == 2) // Se Excel
            {

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    //Cabeçalho
                    Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "2.51332", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, "5.13834", "0.0", "11", "#0113a0", "B", Header_Body, "7.51332", "");
                    Relatorio.CriarObjLabelData("Now", "", "", "13.0", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                    //Cria Tabela           
                    Relatorio.CriarObjTabela("0", "1", "Query1", false, false);

                    if (chkPadraoUSA == "0")
                    {
                        Relatorio.CriarObjColunaNaTabela("ID", "ID", true, "ID");
                        Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup");
                    }

                    Relatorio.CriarObjColunaNaTabela("Conta", "Conta", true, "Conta");
                    Relatorio.CriarObjCelulaInColuna("Query", "Conta", "DetailGroup");

                    if (chkPadraoUSA == "0")
                    {
                        Relatorio.CriarObjColunaNaTabela("Nivel", "Nivel", true, "Nivel");
                        Relatorio.CriarObjCelulaInColuna("Query", "Nivel", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Lançamentos", "Lançamentos", true, "Lançamentos");
                        Relatorio.CriarObjCelulaInColuna("Query", "Lançamentos", "DetailGroup");
                    }

                    if (chkSaldoEmNiveis == "1")
                    {
                        if (Convert.ToInt32(selNivel3) >= 6)
                        {
                            Relatorio.CriarObjColunaNaTabela("Saldo-6", "Saldo6", true, "Saldo-6");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo6", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                        }

                        if (Convert.ToInt32(selNivel3) >= 5)
                        {
                            Relatorio.CriarObjColunaNaTabela("Saldo-5", "Saldo5", true, "Saldo-5");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo5", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                        }

                        if (Convert.ToInt32(selNivel3) >= 4)
                        {
                            Relatorio.CriarObjColunaNaTabela("Saldo-4", "Saldo4", true, "Saldo-4");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo4", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                        }

                        if (Convert.ToInt32(selNivel3) >= 3)
                        {
                            Relatorio.CriarObjColunaNaTabela("Saldo3", "Saldo3", true, "Saldo3");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo3", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                        }

                        if (Convert.ToInt32(selNivel3) >= 2)
                        {
                            Relatorio.CriarObjColunaNaTabela("Saldo-2", "Saldo2", true, "Saldo-2");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo2", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                        }

                        if (Convert.ToInt32(selNivel3) >= 2)
                        {
                            Relatorio.CriarObjColunaNaTabela("Saldo-1", "Saldo1", true, "Saldo-1");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo1", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                        }
                    }
                    else
                    {
                        Relatorio.CriarObjColunaNaTabela("Saldo", "Saldo", true, "Saldo");
                        Relatorio.CriarObjCelulaInColuna("Query", "Saldo", "DetailGroup", Decimais: "<Format>#,0.00</Format>");
                    }

                    Relatorio.CriarObjColunaNaTabela("Observacao", "Observacao", true, "Observacao");
                    Relatorio.CriarObjCelulaInColuna("Query", "Observacao", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Observacoes", "Observacoes", true, "Observacoes");
                    Relatorio.CriarObjCelulaInColuna("Query", "Observacoes", "DetailGroup");

                    Relatorio.TabelaEnd();


                    Relatorio.CriarPDF_Excel(Title, formato);
                }
            }
        }
    }
}