using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.planocontas.serverside
{
    public partial class gravarconciliacao : System.Web.UI.OverflyPage
    {
        private Integer[] ConciliacaoID;

        public Integer[] nConciliacaoID
        {
            get { return ConciliacaoID; }
            set { ConciliacaoID = value; }
        }
        private String[] Corretor;

        public String[] sCorretor
        {
            get { return Corretor; }
            set { Corretor = value; }
        }

        private String[] Correcao;
        public String[] sCorrecao
        {
            get { return Correcao; }
            set { Correcao = value; }
        }

        private String[] TipoErro;
        public String[] sTipoErro
        {
            get { return TipoErro; }
            set { TipoErro = value; }
        }

        protected int Executar()
        {
            int fldresp;
            string sql = "";
            int nDataLen = ConciliacaoID.Length;

            for (int i = 0; i < nDataLen; i++)
            {
                sql = sql + "UPDATE _ContabilidadeConciliacao " +
                    "SET Corretor = " + Corretor[i].ToString() + ", " +
                        "Correcao = " + Correcao[i].ToString() + ", " +
                        "TipoErro = " + TipoErro[i].ToString() + " " +
                    "WHERE ConciliacaoID = " + ConciliacaoID[i].ToString() + " ";
            }

            fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);

            return fldresp;
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            int fldresp = Executar();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "SELECT " + fldresp + " as FldResp"
                )
            );
        }
    }
}
