﻿using System;
using System.Web;
using System.Data;

namespace Overfly3.PrintJet
{
    public partial class ReportsGrid_conciliacao : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;

        private int RelatorioID = Convert.ToInt32(HttpContext.Current.Request.Params["RelatorioID"]);
        private int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["formato"]);
        private int nTipo = Convert.ToInt32(HttpContext.Current.Request.Params["nTipo"]);
        private int sLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["sLinguaLogada"]);
        private string glb_sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        private int IntegracaoID = Convert.ToInt32(HttpContext.Current.Request.Params["IntegracaoID"]);

        int Datateste = 0;
        bool nulo = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "1": //1
                        relatorioConciliacao();
                        break;

                    case "2": //2
                        relatorioConciliacao();
                        break;
                    
                    case "3": //3
                        relatorioConciliacao2();
                        break;

                }
            }
        }

        public void relatorioConciliacao()
        {
            //Parametros da procedure
            string dtInicio = Convert.ToString(HttpContext.Current.Request.Params["dtInicio"]);
            string dtFim = Convert.ToString(HttpContext.Current.Request.Params["dtFim"]);
            string sCorretor = Convert.ToString(HttpContext.Current.Request.Params["sCorretor"]);
            string sCorrecao = Convert.ToString(HttpContext.Current.Request.Params["sCorrecao"]);
            int nTipoErro = Convert.ToInt32(HttpContext.Current.Request.Params["nTipoErro"]);
            string glb_sTipoRegistro = Convert.ToString(HttpContext.Current.Request.Params["glb_sTipoRegistro"]);
            bool glb_bResumo = Convert.ToBoolean(HttpContext.Current.Request.Params["glb_bResumo"]);

            string strSQL = "";

            string sWhereData = "";
            string sCamposEspecificos = "";


            //Define formato de data de acordo com a lingua logada
            string param = "dd/MM/yyy";
            if (sLinguaLogada != 246)
                param = "MM/dd/yyy";

            //Recebe data atual para colocar no titulo do Excel
            DateTime DataHoje = DateTime.Now;
            string _data = DataHoje.ToString(param);

            param = "<Format>" + param + "</Format>";

            string Title = "Conciliação Contábil " + (nTipo != 1 ? "(Erros) - " : " - ") + glb_sTipoRegistro + "__" + glb_sEmpresaFantasia + "_" + _data + "_" + nTipo;


            if (nTipo == 1)
            {
                if ((dtInicio != "NULL") && (dtFim != "NULL"))
                    sWhereData = "AND (dtBalancete BETWEEN " + dtInicio + " AND " + dtFim + ") ";
                else if (dtInicio != "NULL")
                    sWhereData = "AND (dtBalancete >= " + dtInicio + ") ";
                else if (dtFim != "NULL")
                    sWhereData = "AND (dtBalancete <= " + dtFim + ") ";

                if (sCorretor != "" && sCorretor != "0")
                    sWhereData += ((sWhereData.Length == 0) ? "WHERE " : " AND ") + "(Corretor = '" + sCorretor + "') ";

                if (sCorrecao != "" && sCorrecao != "0")
                    sWhereData += ((sWhereData.Length == 0) ? "WHERE " : " AND ") + "(Correcao = '" + sCorrecao + "') ";

                if (nTipoErro >= 0)
                {
                    if ((sWhereData.Length > 0) || (!glb_bResumo))
                        sWhereData += (nTipoErro.ToString().Length > 0 ? " AND (TipoErro = " + nTipoErro + ") " : "");
                    else
                        sWhereData += (nTipoErro.ToString().Length > 0 ? " WHERE (TipoErro = " + nTipoErro + ") " : "");
                }
                else if (nTipoErro == -1)
                {
                    if ((sWhereData.Length > 0) || (!glb_bResumo))
                        sWhereData += (nTipoErro.ToString().Length > 0 ? " AND (TipoErro <> 0) " : "");
                    else
                        sWhereData += (nTipoErro.ToString().Length > 0 ? " WHERE (TipoErro <> 0) " : "");
                }

                if (glb_sTipoRegistro == "ACP")
                    sCamposEspecificos = "dbo.fn_Recursos_Campos(EstadoID, 1) AS Estado, dtVencimento ";
                else if (glb_sTipoRegistro == "Adiantamento")
                    sCamposEspecificos = "dbo.fn_Recursos_Campos(EstadoID, 1) AS Estado, dtVencimento ";
                else if (glb_sTipoRegistro == "Baixa")
                    sCamposEspecificos = "Layout, ContaBancaria ";
                else if (glb_sTipoRegistro == "CTE")
                    sCamposEspecificos = "dbo.fn_Recursos_Campos(EstadoID, 1) AS Estado, Impostos, CodigoOperacao, ChaveAcesso ";
                else if (glb_sTipoRegistro == "Duplicata")
                    sCamposEspecificos = "dbo.fn_Recursos_Campos(EstadoID, 1) AS Estado, dtVencimento ";
                else if (glb_sTipoRegistro == "Lancamento Bancario")
                    sCamposEspecificos = "dbo.fn_Recursos_Campos(EstadoID, 1) AS Estado, dtVencimento, ContaBancaria ";
                else if (glb_sTipoRegistro == "Nota de Debito e Credito")
                    sCamposEspecificos = "dbo.fn_Recursos_Campos(EstadoID, 1) AS Estado, dtVencimento, ContaBancaria ";
                else if (glb_sTipoRegistro == "Nota Fiscal")
                    sCamposEspecificos = "dbo.fn_Recursos_Campos(EstadoID, 1) AS Estado, Impostos, Retencoes, Itens, Parcelas, CodigoOperacao, ChaveAcesso ";

                if (glb_bResumo)
                {
                    strSQL = "DECLARE @Resumo TABLE (TipoRegistro VARCHAR(50), Contexto VARCHAR(30), Indice INT, Numero VARCHAR(30), Erro BIT) " +
                                "INSERT INTO @Resumo " +
                                    "SELECT DISTINCT TipoRegistro, Contexto, Indice, Numero, Erro " +
                                        "FROM _ContabilidadeConciliacao a WITH(NOLOCK) " +

                                "SELECT TipoRegistro, COUNT(1) AS Registros, SUM(CONVERT(INT, ISNULL(Erro, 0))) AS Divergencia, " +
                                        "CONVERT(NUMERIC(11, 2), ((SUM(CONVERT(NUMERIC(11, 2), ISNULL(Erro, 0))) / CONVERT(NUMERIC(11, 2), COUNT(1))) * 100)) AS Percentual " +
                                "FROM @Resumo " +
                                "GROUP BY TipoRegistro ";
                }
                else
                {
                    strSQL = "SELECT ConciliacaoID, dbo.fn_pessoa_fantasia(EmpresaID, 0) AS Empresa, TipoRegistro, Contexto, Indice, Numero, PessoaID, Pessoa, dtBalancete AS Data, Tipo, Valor, " +
                                    "IntegracaoID, Fonte, ContadorID AS ContadorID, Inconsistencias, Corretor, Correcao, " +
                                    sCamposEspecificos +
                                "FROM _ContabilidadeConciliacao WITH(NOLOCK) " +
                                "WHERE (Erro = 1) AND (TipoRegistro = '" + glb_sTipoRegistro + "') " + sWhereData +
                                "ORDER BY Contexto, ISNULL(Indice, 99999), dtBalancete ASC, Indice ASC, Fonte";
                }
            }
            else
            {
                if ((dtInicio != "NULL") && (dtFim != "NULL"))
                    sWhereData = "AND (b.dtBalancete BETWEEN " + dtInicio + " AND " + dtFim + ") ";
                else if (dtInicio != "NULL")
                    sWhereData = "AND (b.dtBalancete >= " + dtInicio + ") ";
                else if (dtFim != "NULL")
                    sWhereData = "AND (b.dtBalancete <= " + dtFim + ") ";

                if (nTipoErro >= 0)
                {
                    sWhereData += (nTipoErro.ToString().Length > 0 ? " AND (b.TipoErro = " + nTipoErro + ") " : "");
                }
                else if (nTipoErro == -1)
                {
                    sWhereData += (nTipoErro.ToString().Length > 0 ? " AND (b.TipoErro <> 0) " : "");
                }

                strSQL = "SELECT a.* " +
                            "FROM _ContabilidadeConciliacao_Erros a WITH(NOLOCK) " +
                                "INNER JOIN _ContabilidadeConciliacao b WITH(NOLOCK) ON (b.IntegracaoID = a.IntegracaoID) " +
                            "WHERE a.TipoRegistro = '" + glb_sTipoRegistro + "' " + sWhereData + " " +
                            "ORDER BY a.LogID ";
            }


            arrSqlQuery = new string[,] { { "Query" + nTipo, strSQL } };

            Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

            DataSet dsFin = new DataSet();
            dsFin = (DataSet)HttpContext.Current.Session["Query"];
            Datateste = dsFin.Tables.Count;

            //Verifica se a Query está vazia e retorna mensagem de erro
            if (dsFin.Tables["Query" + nTipo].Rows.Count == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
            }
            else
            {
                if (Formato == 1) //Se formato PDF
                {
                    ;
                }

                else if (Formato == 2) //Se formato Excel
                {
                    if (nTipo == 1)
                    {
                        Relatorio.CriarObjTabela("0.00", "0.00", "Query1", false, false, false);

                        Relatorio.CriarObjColunaNaTabela("ConciliacaoID", "ConciliacaoID", true, "ConciliacaoID");
                        Relatorio.CriarObjCelulaInColuna("Query", "ConciliacaoID", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Empresa", "Empresa", true, "Empresa");
                        Relatorio.CriarObjCelulaInColuna("Query", "Empresa", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("TipoRegistro", "TipoRegistro", true, "TipoRegistro");
                        Relatorio.CriarObjCelulaInColuna("Query", "TipoRegistro", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Contexto", "Contexto", true, "Contexto");
                        Relatorio.CriarObjCelulaInColuna("Query", "Contexto", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Indice", "Indice", true, "Indice");
                        Relatorio.CriarObjCelulaInColuna("Query", "Indice", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Numero", "Numero", true, "Numero");
                        Relatorio.CriarObjCelulaInColuna("Query", "Numero", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("PessoaID", "PessoaID", true, "PessoaID");
                        Relatorio.CriarObjCelulaInColuna("Query", "PessoaID", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Pessoa", "Pessoa", true, "Pessoa");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pessoa", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Data", "Data", true, "Data");
                        Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup", Decimais: param);

                        Relatorio.CriarObjColunaNaTabela("Tipo", "Tipo", true, "Tipo");
                        Relatorio.CriarObjCelulaInColuna("Query", "Tipo", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Valor", "Valor", true, "Valor");
                        Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("IntegracaoID", "IntegracaoID", true, "IntegracaoID");
                        Relatorio.CriarObjCelulaInColuna("Query", "IntegracaoID", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Fonte", "Fonte", true, "Fonte");
                        Relatorio.CriarObjCelulaInColuna("Query", "Fonte", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("ContadorID", "ContadorID", true, "ContadorID");
                        Relatorio.CriarObjCelulaInColuna("Query", "ContadorID", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Inconsistencias", "Inconsistencias", true, "Inconsistencias");
                        Relatorio.CriarObjCelulaInColuna("Query", "Inconsistencias", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Corretor", "Corretor", true, "Corretor");
                        Relatorio.CriarObjCelulaInColuna("Query", "Corretor", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Correcao", "Correcao", true, "Correcao");
                        Relatorio.CriarObjCelulaInColuna("Query", "Correcao", "DetailGroup");

                        if (glb_sTipoRegistro == "ACP" || glb_sTipoRegistro == "Duplicata" || glb_sTipoRegistro == "Adiantamento" || glb_sTipoRegistro == "Lancamento Bancario" || glb_sTipoRegistro == "Nota de Debito e Credito" || glb_sTipoRegistro == "CTE" || glb_sTipoRegistro == "Nota Fiscal")
                        {
                            Relatorio.CriarObjColunaNaTabela("Estado", "Estado", true, "Estado");
                            Relatorio.CriarObjCelulaInColuna("Query", "Estado", "DetailGroup");
                        }

                        if (glb_sTipoRegistro == "ACP" || glb_sTipoRegistro == "Duplicata" || glb_sTipoRegistro == "Adiantamento" || glb_sTipoRegistro == "Lancamento Bancario" || glb_sTipoRegistro == "Nota de Debito e Credito")
                        {
                            Relatorio.CriarObjColunaNaTabela("dtVencimento", "dtVencimento", true, "dtVencimento");
                            Relatorio.CriarObjCelulaInColuna("Query", "dtVencimento", "DetailGroup", Decimais: param);
                        }

                        if(glb_sTipoRegistro == "Baixa")
                        {
                            Relatorio.CriarObjColunaNaTabela("Layout", "Layout", true, "Layout");
                            Relatorio.CriarObjCelulaInColuna("Query", "Layout", "DetailGroup");
                        }

                        if (glb_sTipoRegistro == "Baixa" || glb_sTipoRegistro == "Lancamento Bancario" || glb_sTipoRegistro == "Nota de Debito e Credito")
                        {
                            Relatorio.CriarObjColunaNaTabela("ContaBancaria", "ContaBancaria", true, "ContaBancaria");
                            Relatorio.CriarObjCelulaInColuna("Query", "ContaBancaria", "DetailGroup");
                        }

                        if (glb_sTipoRegistro == "CTE" || glb_sTipoRegistro == "Nota Fiscal")
                        {
                            Relatorio.CriarObjColunaNaTabela("Impostos", "Impostos", true, "Impostos");
                            Relatorio.CriarObjCelulaInColuna("Query", "Impostos", "DetailGroup");
                        }

                        if(glb_sTipoRegistro == "Nota Fiscal")
                        {
                            Relatorio.CriarObjColunaNaTabela("Retencoes", "Retencoes", true, "Retencoes");
                            Relatorio.CriarObjCelulaInColuna("Query", "Retencoes", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Itens", "Itens", true, "Itens");
                            Relatorio.CriarObjCelulaInColuna("Query", "Itens", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Parcelas", "Parcelas", true, "Parcelas");
                            Relatorio.CriarObjCelulaInColuna("Query", "Parcelas", "DetailGroup");
                        }

                        if(glb_sTipoRegistro == "Nota Fiscal" || glb_sTipoRegistro == "CTE")
                        {
                            Relatorio.CriarObjColunaNaTabela("CodigoOperacao", "CodigoOperacao", true, "CodigoOperacao");
                            Relatorio.CriarObjCelulaInColuna("Query", "CodigoOperacao", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ChaveAcesso", "ChaveAcesso", true, "ChaveAcesso");
                            Relatorio.CriarObjCelulaInColuna("Query", "ChaveAcesso", "DetailGroup");
                        }
                        
                        Relatorio.TabelaEnd();
                    }
                    else
                    {
                        Relatorio.CriarObjTabela("0.00", "0.00", "Query2", false, false, false);

                            Relatorio.CriarObjColunaNaTabela("ConErroID", "ConErroID", true, "ConErroID");
                            Relatorio.CriarObjCelulaInColuna("Query", "ConErroID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("TipoRegistro", "TipoRegistro", true, "TipoRegistro");
                            Relatorio.CriarObjCelulaInColuna("Query", "TipoRegistro", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("IntegracaoID", "IntegracaoID", true, "IntegracaoID");
                            Relatorio.CriarObjCelulaInColuna("Query", "IntegracaoID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("LogID", "LogID", true, "LogID");
                            Relatorio.CriarObjCelulaInColuna("Query", "LogID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("dtData", "dtData", true, "dtData");
                            Relatorio.CriarObjCelulaInColuna("Query", "dtData", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("Erro", "Erro", true, "Erro");
                            Relatorio.CriarObjCelulaInColuna("Query", "Erro", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Documento", "Documento", true, "Documento");
                            Relatorio.CriarObjCelulaInColuna("Query", "Documento", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Numero", "Numero", true, "Numero");
                            Relatorio.CriarObjCelulaInColuna("Query", "Numero", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Arquivo", "Arquivo", true, "Arquivo");
                            Relatorio.CriarObjCelulaInColuna("Query", "Arquivo", "DetailGroup");

                        Relatorio.TabelaEnd();
                    }
                }
                Relatorio.CriarPDF_Excel(Title, Formato);
            }
        }
        
        public void relatorioConciliacao2()
        {
            //Parametros da procedure
            string dtInicio = Convert.ToString(HttpContext.Current.Request.Params["dtInicio"]);
            string glb_sTipoRegistro = Convert.ToString(HttpContext.Current.Request.Params["glb_sTipoRegistro"]);
            
            //Define formato de data de acordo com a lingua logada
            string param = "dd/MM/yyy";
            if (sLinguaLogada != 246)
                param = "MM/dd/yyy";

            //Recebe data atual para colocar no titulo do Excel
            DateTime DataHoje = DateTime.Now;
            string _data = DataHoje.ToString(param);

            param = "<Format>" + param + "</Format>";
            
            string Title = "Relatório " + "__" + glb_sEmpresaFantasia + "_" + _data;
            string strSQL =  null;
            string strSQL1 = null;
            string strSQL2 = null;
            string nivelDetalhe = null;

            if (glb_sTipoRegistro == "ACP") {
                strSQL =  "EXEC [dbo].[sp_Mazars_Registro] 4, 1, " + IntegracaoID;
                strSQL1 = "EXEC [dbo].[sp_Mazars_Registro] 4, 2, " + IntegracaoID;
                Title = "ACP" + "__" + glb_sEmpresaFantasia + "_" + _data +"_" + IntegracaoID;
                nivelDetalhe = "2";
            }
            if (glb_sTipoRegistro == "Adiantamento") {
                strSQL = "EXEC [dbo].[sp_Mazars_Registro] 7, 1, " + IntegracaoID;
                Title = "Adiantamento" + "__" + glb_sEmpresaFantasia + "_" + _data + "_" + IntegracaoID;
                nivelDetalhe = "1";
            }
            if (glb_sTipoRegistro == "Baixa") {
                strSQL =  "EXEC [dbo].[sp_Mazars_Registro] 9, 1, " + IntegracaoID;
                strSQL1 = "EXEC [dbo].[sp_Mazars_Registro] 9, 2, " + IntegracaoID;
                Title = "Baixa" + "__" + glb_sEmpresaFantasia + "_" + _data + "_" + IntegracaoID;
                nivelDetalhe = "2";
            }
            if (glb_sTipoRegistro == "CTE") {
                strSQL =  "EXEC [dbo].[sp_Mazars_Registro] 3, 1, " + IntegracaoID;
                strSQL1 = "EXEC [dbo].[sp_Mazars_Registro] 3, 2, " + IntegracaoID;
                Title = "CTE" + "__" + glb_sEmpresaFantasia + "_" + _data + "_" + IntegracaoID;
                nivelDetalhe = "2";
            }
            if (glb_sTipoRegistro == "Duplicata") {
                strSQL = "EXEC [dbo].[sp_Mazars_Registro] 8, 1, " + IntegracaoID;
                Title = "Duplicatas" + "__" + glb_sEmpresaFantasia + "_" + _data + "_" + IntegracaoID;
                nivelDetalhe = "1";
            }
            if (glb_sTipoRegistro == "Lancamento Bancario") {
                strSQL = "EXEC [dbo].[sp_Mazars_Registro] 6, 1, " + IntegracaoID;
                Title = "LancBanc" + "__" + glb_sEmpresaFantasia + "_" + _data + "_" + IntegracaoID;
                nivelDetalhe = "1";
            }
            if (glb_sTipoRegistro == "Nota de Debito e Credito") {
                strSQL = "EXEC [dbo].[sp_Mazars_Registro] 5, 1, " + IntegracaoID;
                Title = "NotaDebito" + "__" + glb_sEmpresaFantasia + "_" + _data + "_" + IntegracaoID;
                nivelDetalhe = "1";
            }
            if (glb_sTipoRegistro == "Nota Fiscal") {
                strSQL =  "EXEC [dbo].[sp_Mazars_Registro] 1, 1, " + IntegracaoID;
                strSQL1 = "EXEC [dbo].[sp_Mazars_Registro] 1, 2, " + IntegracaoID;
                strSQL2 = "EXEC [dbo].[sp_Mazars_Registro] 1, 3, " + IntegracaoID;
                Title = "NotaFiscal" + "__" + glb_sEmpresaFantasia + "_" + _data + "_" + IntegracaoID;
                nivelDetalhe = "3";
            }

            //Title = "teste";
            
            if(nivelDetalhe == "1")
            {
                arrSqlQuery = new string[,] {{ "Query1", strSQL }};
            }

            else if (nivelDetalhe == "2")
            {
                arrSqlQuery = new string[,] {{ "Query1", strSQL }, { "Query2", strSQL1 }};
            }
            else if (nivelDetalhe == "3")
            {
                arrSqlQuery = new string[,] {{ "Query1", strSQL }, { "Query2", strSQL1 }, { "Query3", strSQL2 }};
            }
   
            Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

            DataSet dsFin = new DataSet();
            dsFin = (DataSet)HttpContext.Current.Session["Query"];
            Datateste = dsFin.Tables.Count;
       
            if (dsFin.Tables[0].Rows.Count == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
            }
            else
            {
                if (Formato == 1) //Se formato PDF
                {
                    ;
                }

                else if (Formato == 2) //Se formato Excel
                {
                    if (nTipo == 3)
                    {

                        if (nivelDetalhe == "1")
                        {
                            Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, true, LargCol: "2.00");
                            Relatorio.TabelaEnd();
                        }

                        if (nivelDetalhe == "2")
                        {
                            Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, true, LargCol: "2.00");
                            Relatorio.TabelaEnd();

                            Relatorio.CriarObjTabela("0.00", "0.5", "Query2", true, true, LargCol: "2.00");
                            Relatorio.TabelaEnd();
                        }

                        if (nivelDetalhe == "3")
                        {
                            Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, true, LargCol: "2.00");
                            Relatorio.TabelaEnd();

                            Relatorio.CriarObjTabela("0.00", "0.5", "Query2", true, true, LargCol: "2.00");
                            Relatorio.TabelaEnd();

                            Relatorio.CriarObjTabela("0.00", "1.00", "Query3", true, true, LargCol: "2.00");
                            Relatorio.TabelaEnd();
                        }
                    }
                }

                Relatorio.CriarPDF_Excel(Title, Formato);
            }
        }
    }
}