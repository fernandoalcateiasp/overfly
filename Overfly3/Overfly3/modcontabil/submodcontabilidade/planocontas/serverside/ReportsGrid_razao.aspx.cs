﻿using System;
using System.Web;
using System.Data;

namespace Overfly3.PrintJet
{
    public partial class ReportsGrid_razao : System.Web.UI.OverflyPage
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;

        private int RelatorioID = Convert.ToInt32(HttpContext.Current.Request.Params["RelatorioID"]);
        private int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["formato"]);
        private bool bToExcel = Convert.ToBoolean(HttpContext.Current.Request.Params["bToExcel"]);
        private int sLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["sLinguaLogada"]);
        private string glb_sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);

        int Datateste = 0;
        bool nulo = false;

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "1":
                        relatorioRazao();
                        break;
                }
            }
        }

        public void relatorioRazao()
        {
            //Parametros da procedure
            string sEmpresas = Convert.ToString(HttpContext.Current.Request.Params["sEmpresas"]);
            string selMoeda = Convert.ToString(HttpContext.Current.Request.Params["selMoeda"]);
            string selConta = Convert.ToString(HttpContext.Current.Request.Params["selConta"]);
            string nEstadoID = Convert.ToString(HttpContext.Current.Request.Params["nEstadoID"]);
            string nDetalheID = Convert.ToString(HttpContext.Current.Request.Params["nDetalheID"]);
            string selConcilia = Convert.ToString(HttpContext.Current.Request.Params["selConcilia"]);
            string sDataInicio = Convert.ToString(HttpContext.Current.Request.Params["sDataInicio"]);
            string sDataFim = Convert.ToString(HttpContext.Current.Request.Params["sDataFim"]);
            string sFiltro = Convert.ToString(HttpContext.Current.Request.Params["sFiltro"]);


            //Define formato de data de acordo com a lingua logada
            string param = "dd/MM/yyy";
            if (sLinguaLogada != 246)
                param = "MM/dd/yyy";

            //Recebe data atual para colocar no titulo do Excel
            DateTime DataHoje = DateTime.Now;
            string _data = DataHoje.ToString(param);

            param = "<Format>" + param + "</Format>";

            string Title = "Lançamentos_" + glb_sEmpresaFantasia + "_" + _data;

            string strSQL = "EXEC sp_Contabilidade_Razao " + "'" + sEmpresas + "'" + ", " + selMoeda + ", " + selConta + ", " + nEstadoID + ", " +
                            nDetalheID + ", NULL, " + selConcilia + ", 0, 0, NULL, NULL, " + (bToExcel ? "0" : "1") + ", " + sDataInicio + ", " + sDataFim + ", NULL, " + sFiltro;

            //Se Lista btnListar
            if (!bToExcel)
            {
                //Cria DataSet com retorno da procedure
                DataSet dsLista = DataInterfaceObj.getRemoteData(strSQL);

                //Escreve XML com o DataSet
                WriteResultXML(dsLista);
            }

            //SE EXCEL btnExcel
            else if (bToExcel)
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");


                DataSet dsFin = new DataSet();
                dsFin = (DataSet)HttpContext.Current.Session["Query"];
                Datateste = dsFin.Tables.Count;

                //Verifica se a Query está vazia e retorna mensagem de erro
                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    return;
                }
                else
                {
                    if (Formato == 1) //Se formato PDF
                    {
                        ;
                    }

                    else if (Formato == 2) //Se formato Excel
                    {
                        Relatorio.CriarObjTabela("0.00", "0.00", "Query1", false, false, false);

                            Relatorio.CriarObjColunaNaTabela("ContaID", "ContaID", true, "ContaID");
                            Relatorio.CriarObjCelulaInColuna("Query", "ContaID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Conta", "Conta", true, "Conta");
                            Relatorio.CriarObjCelulaInColuna("Query", "Conta", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("dtBalancete", "dtBalancete", true, "dtBalancete");
                            Relatorio.CriarObjCelulaInColuna("Query", "dtBalancete", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("dtLancamento", "dtLancamento", true, "dtLancamento");
                            Relatorio.CriarObjCelulaInColuna("Query", "dtLancamento", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("LancamentoID", "LancamentoID", true, "LancamentoID");
                            Relatorio.CriarObjCelulaInColuna("Query", "LancamentoID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Estado", "Estado", true, "Estado");
                            Relatorio.CriarObjCelulaInColuna("Query", "Estado", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Conciliado", "Conciliado", true, "Conciliado");
                            Relatorio.CriarObjCelulaInColuna("Query", "Conciliado", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("EhEstorno", "EhEstorno", true, "EhEstorno");
                            Relatorio.CriarObjCelulaInColuna("Query", "EhEstorno", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Moeda", "Moeda", true, "Moeda");
                            Relatorio.CriarObjCelulaInColuna("Query", "Moeda", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Valor", "Valor", true, "Valor");
                            Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Saldo", "Saldo", true, "Saldo");
                            Relatorio.CriarObjCelulaInColuna("Query", "Saldo", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Documento", "Documento", true, "Documento");
                            Relatorio.CriarObjCelulaInColuna("Query", "Documento", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Contrapartida", "Contrapartida", true, "Contrapartida");
                            Relatorio.CriarObjCelulaInColuna("Query", "Contrapartida", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Historico", "Historico", true, "Historico");
                            Relatorio.CriarObjCelulaInColuna("Query", "Historico", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("DetalheID", "DetalheID", true, "DetalheID");
                            Relatorio.CriarObjCelulaInColuna("Query", "DetalheID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Detalhe", "Detalhe", true, "Detalhe");
                            Relatorio.CriarObjCelulaInColuna("Query", "Detalhe", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Fantasia", "Fantasia", true, "Fantasia");
                            Relatorio.CriarObjCelulaInColuna("Query", "Fantasia", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ContaBancaria", "ContaBancaria", true, "ContaBancaria");
                            Relatorio.CriarObjCelulaInColuna("Query", "ContaBancaria", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Imposto", "Imposto", true, "Imposto");
                            Relatorio.CriarObjCelulaInColuna("Query", "Imposto", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Empresa", "Empresa", true, "Empresa");
                            Relatorio.CriarObjCelulaInColuna("Query", "Empresa", "DetailGroup");

                        Relatorio.TabelaEnd();
                    }
                    Relatorio.CriarPDF_Excel(Title, Formato);
                }
            }
        }
    }
}