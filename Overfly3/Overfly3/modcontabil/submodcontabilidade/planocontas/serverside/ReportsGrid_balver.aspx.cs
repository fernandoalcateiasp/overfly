﻿using System;
using System.Web;
using System.Data;

namespace Overfly3.PrintJet
{
    public partial class ReportsGrid_balver : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;

        private int RelatorioID = Convert.ToInt32(HttpContext.Current.Request.Params["RelatorioID"]);
        private int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["formato"]);
        private int nTipo = Convert.ToInt32(HttpContext.Current.Request.Params["nTipo"]);
        private int sLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["sLinguaLogada"]);
        private string glb_sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);

        int Datateste = 0;
        bool nulo = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "1":
                        relatorioBalver();
                        break;
                }
            }
        }

        public void relatorioBalver()
        {
            //Parametros da procedure
            string sEmpresas = Convert.ToString(HttpContext.Current.Request.Params["sEmpresas"]);
            string selMoeda = Convert.ToString(HttpContext.Current.Request.Params["selMoeda"]);
            string selNivel = Convert.ToString(HttpContext.Current.Request.Params["selNivel"]);
            string chkLancConciliados = Convert.ToString(HttpContext.Current.Request.Params["chkLancConciliados"]);
            string chkLancamentos = Convert.ToString(HttpContext.Current.Request.Params["chkLancamentos"]);
            string chkSaldo = Convert.ToString(HttpContext.Current.Request.Params["chkSaldo"]);
            string chkContasPatrimoniais = Convert.ToString(HttpContext.Current.Request.Params["chkContasPatrimoniais"]);
            string chkContasResultado = Convert.ToString(HttpContext.Current.Request.Params["chkContasResultado"]);
            string selTipoDetalhamentoID = Convert.ToString(HttpContext.Current.Request.Params["selTipoDetalhamentoID"]);
            string chkMilhar = Convert.ToString(HttpContext.Current.Request.Params["chkMilhar"]);
            string sDataInicio = Convert.ToString(HttpContext.Current.Request.Params["sDataInicio"]);
            string sDataFim = Convert.ToString(HttpContext.Current.Request.Params["sDataFim"]);
            string selAgruparPor = Convert.ToString(HttpContext.Current.Request.Params["selAgruparPor"]);

            //Define formato de data de acordo com a lingua logada
            string param = "dd/MM/yyy";
            if (sLinguaLogada != 246)
                param = "MM/dd/yyy";

            //Recebe data atual para colocar no titulo do Excel
            DateTime DataHoje = DateTime.Now;
            string _data = DataHoje.ToString(param);

            param = "<Format>" + param + "</Format>";

            string Title = "Balancete de Verificação_" + glb_sEmpresaFantasia + "_" + _data;

            string strSQL = "EXEC sp_Contabilidade_Balancete " + "'" + sEmpresas + "', " + selMoeda + ", NULL, 0, " + selNivel + ", " + chkLancConciliados + ", " + chkLancamentos + ", " +
                            chkSaldo + ", " + chkContasPatrimoniais + ", " + chkContasResultado + ", " + selTipoDetalhamentoID + ", " + chkMilhar + ", 0, " + sDataInicio + ", " + sDataFim + ", " +
                            selAgruparPor + ", " + sLinguaLogada + ", NULL";

            if (nTipo == 2)
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                string nameAgruparPor = "";
                DataSet dsFin = new DataSet();
                dsFin = (DataSet)HttpContext.Current.Session["Query"];
                Datateste = dsFin.Tables.Count;
                
                //if (selAgruparPor != "NULL")
                //{
                //    nameAgruparPor = dsFin.Tables["Query1"].Columns[7].ToString();
                    
                //    //Tratativa se começar com numero ou ID especial
                //    if (selAgruparPor == "1")
                //    {
                //        dsFin.Tables["Query1"].Columns[7].ColumnName = "ID" + nameAgruparPor;
                //        nameAgruparPor = dsFin.Tables["Query1"].Columns[7].ToString();
                //    }
                //}

                //Verifica se a Query está vazia e retorna mensagem de erro
                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    return;
                }
                else
                {
                    if (Formato == 1) //Se formato PDF
                    {
                        ;
                    }

                    else if (Formato == 2) //Se formato Excel
                    {
                        Relatorio.CriarObjTabela("0.00", "0.00", "Query1", false, false, false);

                        Relatorio.CriarObjColunaNaTabela("ID", "ID", true, "ID");
                        Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Conta", "Conta", true, "Conta");
                        Relatorio.CriarObjCelulaInColuna("Query", "Conta", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Nivel", "Nivel", true, "Nivel");
                        Relatorio.CriarObjCelulaInColuna("Query", "Nivel", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Lançamentos", "Lançamentos", true, "Lançamentos");
                        Relatorio.CriarObjCelulaInColuna("Query", "Lançamentos", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Moeda", "Moeda", true, "Moeda");
                        Relatorio.CriarObjCelulaInColuna("Query", "Moeda", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Valor", "Valor", true, "Valor");
                        Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup");

                        Relatorio.CriarObjColunaNaTabela("Saldo", "Saldo", true, "Saldo");
                        Relatorio.CriarObjCelulaInColuna("Query", "Saldo", "DetailGroup");

                        if (selAgruparPor != "NULL")
                        {

                            for (int x = 7; x < dsFin.Tables[0].Columns.Count; x++)
                            {

                                nameAgruparPor = dsFin.Tables["Query1"].Columns[x].ToString();


                                //Tratativa se começar com numero ou ID especial
                                if (selAgruparPor == "1")
                                {

                                    dsFin.Tables["Query1"].Columns[x].ColumnName = "ID" + nameAgruparPor;
                                    nameAgruparPor = dsFin.Tables["Query1"].Columns[x].ToString();

                                }


                                //if (selAgruparPor == "1")
                                //{
                                Relatorio.CriarObjColunaNaTabela(nameAgruparPor.ToString(), nameAgruparPor.ToString(), true, nameAgruparPor.ToString());
                                Relatorio.CriarObjCelulaInColuna("Query", (nameAgruparPor).ToString(), "DetailGroup");

                                //}
                                //else {
                                //    Relatorio.CriarObjColunaNaTabela(nameAgruparPor.ToString(), nameAgruparPor.ToString(), true, nameAgruparPor.ToString());
                                //    Relatorio.CriarObjCelulaInColuna("Query", (nameAgruparPor).ToString(), "DetailGroup");
                                //}
                            }                      
                        }

                        Relatorio.TabelaEnd();
                    }

                    Relatorio.CriarPDF_Excel(Title, Formato);
                }
            }
        }
    }
}