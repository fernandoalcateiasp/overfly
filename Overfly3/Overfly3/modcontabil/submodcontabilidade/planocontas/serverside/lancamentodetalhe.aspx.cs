﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.planocontas.serverside
{
    public partial class lancamentodetalhe : System.Web.UI.OverflyPage
    {
       private Integer DetalheID;

        public Integer nDetalheID
        {
            get { return DetalheID; }
            set { DetalheID = value; }
        }
        private Integer LancamentoID;

        public Integer nLancamentoID
        {
            get { return LancamentoID; }
            set { LancamentoID = value; }
        }
        private DataSet Detalhe()
        {
            ProcedureParameters[] parameters = new ProcedureParameters[3];

            parameters[0] = new ProcedureParameters(
                "@LancamentoID",
                System.Data.SqlDbType.Int,
                (LancamentoID != null ? (object)LancamentoID.ToString() : DBNull.Value));

            parameters[1] = new ProcedureParameters(
                "@DetalheID",
                System.Data.SqlDbType.Int,
                (DetalheID != null ? (object)DetalheID : DBNull.Value));

            parameters[2] = new ProcedureParameters(
                "@TipoResultado",
                System.Data.SqlDbType.Int,
                2);         

            return DataInterfaceObj.execQueryProcedure("sp_Lancamento_Detalhes", parameters);
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(Detalhe());
        }
    }
}