﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.planocontas.serverside
{
    public partial class contabilidaderazao : System.Web.UI.OverflyPage
    {
        protected static Integer Zero = new Integer(0);
        private string Empresas;

        public string sEmpresas
        {
            get { return Empresas; }
            set { Empresas = value; }
        }
        private Integer MoedaID;

        public Integer nMoedaID
        {
            get { return MoedaID; }
            set { MoedaID = value; }
        }

        private string ContaID;

        public string nContaID
        {
            get { return ContaID; }
            set { ContaID = value; }
        }
        private string DetalheID;

        public string nDetalheID
        {
            get { return DetalheID; }
            set { DetalheID = (value == "0" ? null : value); }
        }
        private Integer LancamentoID;

        public Integer nLancamentoID
        {
            get { return LancamentoID; }
            set { LancamentoID = value; }
        }
        private string LancamentosConciliados;

        public string nLancamentosConciliados
        {
            get { return LancamentosConciliados; }
            set { LancamentosConciliados = (value == "" ? null : value);  }
        }
        private string TemLancamentos;

        public string nTemLancamentos
        {
            get { return TemLancamentos; }
            set { TemLancamentos = value; }
        }
        private string TemSaldo;

        public string nTemSaldo
        {
            get { return TemSaldo; }
            set { TemSaldo = value; }
        }
        private string Milhar;

        public string nMilhar
        {
            get { return Milhar; }
            set { Milhar = value; }
        }
        private Integer Tipo;

        public Integer nTipo
        {
            get { return Tipo; }
            set { Tipo = value; }
        }
        private string DataInicio;

        public string sDataInicio
        {
            get { return DataInicio; }
            set { DataInicio = value; }
        }
        private string DataFim;

        public string sDataFim
        {
            get { return DataFim; }
            set { DataFim = value; }
        }
        private Integer AgruparPor;

        public Integer nAgruparPor
        {
            get { return AgruparPor; }
            set { AgruparPor = value; }
        }       

        private DataSet Razao()
        {
            ProcedureParameters[] parameters = new ProcedureParameters[16];

            parameters[0] = new ProcedureParameters(
                "@Empresas",
                System.Data.SqlDbType.VarChar,
                (Empresas != null ? (Object)Empresas.ToString() : System.DBNull.Value));

            parameters[1] = new ProcedureParameters(
                "@MoedaID",
                System.Data.SqlDbType.Int,
                (MoedaID != null ? (Object)int.Parse(MoedaID.ToString()) : System.DBNull.Value));

            parameters[2] = new ProcedureParameters(
                "@ContaID",
                System.Data.SqlDbType.BigInt,
               (Object)Int64.Parse(ContaID));

            parameters[3] = new ProcedureParameters(
                "@EstadoID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            parameters[4] = new ProcedureParameters(
                "@DetalheID",
                System.Data.SqlDbType.Int,
                (DetalheID != null ? (Object)int.Parse(DetalheID.ToString()) : System.DBNull.Value));

            parameters[5] = new ProcedureParameters(
                "@LancamentoID",
                System.Data.SqlDbType.Int,
                (LancamentoID.intValue() != 0 ? (Object)int.Parse(LancamentoID.ToString()) : System.DBNull.Value));

            parameters[6] = new ProcedureParameters(
                "@LancamentoConciliado",
                System.Data.SqlDbType.Int,
                (LancamentosConciliados != null ? (Object)int.Parse(LancamentosConciliados): System.DBNull.Value));

            parameters[7] = new ProcedureParameters(
                "@TemLancamentos",
                System.Data.SqlDbType.Bit,
                ((Object)TemLancamentos.ToString() == "0" ? false : true));

            parameters[8] = new ProcedureParameters(
                "@TemSaldo",
                System.Data.SqlDbType.Bit,
                ((Object)TemSaldo.ToString() == "0" ? false : true));
            
            parameters[9] = new ProcedureParameters(
                "@Milhar",
                System.Data.SqlDbType.Bit,
                ((Object)Milhar.ToString() == "0" ? false : true));

            parameters[10] = new ProcedureParameters(
                "@Tipo",
                System.Data.SqlDbType.Int,
                (Object)int.Parse(Tipo.ToString()));

            parameters[11] = new ProcedureParameters(
                "@ColunasControle",
                System.Data.SqlDbType.Bit,
                true);

            parameters[12] = new ProcedureParameters(
                "@dtInicio",
                System.Data.SqlDbType.DateTime,
                (DataInicio != null ?
                  (Object)DateTime.Parse(DataInicio) :
                    System.DBNull.Value));

            parameters[13] = new ProcedureParameters(
                "@dtFim",
                System.Data.SqlDbType.DateTime,
                (DataFim != null ? 
                    (Object)DateTime.Parse(DataFim) :
                      System.DBNull.Value));

            parameters[14] = new ProcedureParameters(
                "@AgruparPor",
                System.Data.SqlDbType.Int,
                (AgruparPor.intValue() != 0 ? (Object)int.Parse(AgruparPor.ToString()) : System.DBNull.Value));

            parameters[15] = new ProcedureParameters(
                "@Filtro",
                System.Data.SqlDbType.VarChar,
                System.DBNull.Value);

            return DataInterfaceObj.execQueryProcedure("sp_Contabilidade_Razao", parameters);
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(Razao());
        }
    }
}