/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoSup01 = new CDatatransport('dsoSup01');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoCurrData = new CDatatransport('dsoCurrData');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    glb_aStaticCombos = ([['selTipoContaID', '1'],
						  ['selUsoEspecialID', '2'],
						  ['selContaMaeID', '3'],
						  ['selTipoDetalhamentoID', '4'],
						  ['selTipoVariacaoID', '5']]);

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/planocontas/inferior.asp',
                              SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/planocontas/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'ContaID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID','txtEstadoID',2,1],
                          ['lblConta','txtConta',40,1],
                          ['lblTipoContaID','selTipoContaID',20,1],
                          ['lblNivel','txtNivel',2,1],
                          ['lblContaMaeID','selContaMaeID',14,1],
                          ['lblContaAnterior','txtContaAnterior',11,2],
                          ['lblUsoEspecialID','selUsoEspecialID',20,2],
                          ['lblTipoDetalhamentoID','selTipoDetalhamentoID',22,2],
                          ['lblDetalha','chkDetalha',3,2],
                          ['lblDetalhamentoObrigatorio','chkDetalhamentoObrigatorio',3,2],
                          ['lblAceitaLancamento','chkAceitaLancamento',3,2],
                          ['lblLancamentoManual','chkLancamentoManual',3,2],
                          ['lblSaldoDevedor','chkSaldoDevedor',3,2],
                          ['lblSaldoCredor','chkSaldoCredor',3,2],
                          ['lblUsoRestrito','chkUsoRestrito',3,2],
                          ['lblUSA','txtUSA',3,2],
                          ['lblTemOrcamento','chkTemOrcamento',3,3],
                          ['lblTipoVariacaoID','selTipoVariacaoID',10,3],
                          ['lblObservacao','txtObservacao',30,3]], null, null, true);
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWCONTA')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
        
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
            
        window.top.focus();
                
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    //@@
    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR'))
	{
        chkDetalhamentoObrigatorio.checked = dsoSup01.recordset['DetalhamentoObrigatorio'].value;
        chkAceitaLancamento.checked = dsoSup01.recordset['AceitaLancamento'].value;
		
		adjustLabelsCombos(true);
	}
    
	// Mover esta funcao para os finais de retornos de operacoes no
	// banco de dados
	finalOfSupCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    if ( btnClicked == 'SUPINCL' )
    {
        // funcao que controla campos read-only
        controlReadOnlyFields();
    }

	controlHintContaMaeID(0);

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();
    else if ( cmbID == 'selContaMaeID' )
        controlHintContaMaeID(1);
        
	adjustLabelsCombos(false);

    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    //@@ padrao do frame work
    return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    if ( controlBar == 'SUP' )
    {
    	// Documentos
    	if (btnClicked == 1) {
    	    __openModalDocumentos(controlBar, dsoSup01.recordset.Fields(0).value);
    	}
    	// usuario clicou botao imprimir
		if (btnClicked == 2)
		    openModalPrint();
		    
		// usuario clicou o botao lancamentos
		if (btnClicked == 4)
			openModalRazao(btnClicked);    
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
	if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de lancamentos
    else if ( idElement.toUpperCase() == 'MODALRAZAOHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

			sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nContaID = ' + param2[0] );
			sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nEstadoID = ' + param2[1] );
			sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nDetalheID = ' + param2[2] );
			sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bConciliado = ' + param2[3] );
			sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_sData = ' + param2[4] );
			sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_sFiltro = ' + param2[5] );

	        // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
    	if (param1 == 'OK') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    	else if (param1 == 'CANCEL') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // funcao que controla campos read-only
    controlReadOnlyFields();

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    // mostra tres botoes especificos desabilitados
	showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 0]);
	tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Raz�o', 'Balancete de Verifica��o', '']);
}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Read Only

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields()
{
    txtNivel.readOnly = true;    
    chkAceitaLancamento.disabled = true;
}

/********************************************************************
Funcao criada pelo programador.
Controla o hint do combo ContaMaeID

Parametro:
nTipoChamada - 0 - Carregamento, 1 - Mudanca no Combo

Retorno:
nenhum
********************************************************************/
function controlHintContaMaeID(nTipoChamada)
{
    var sContaMae = dsoSup01.recordset['ContaMae'].value;

	if (nTipoChamada == 0 && sContaMae != null && sContaMae != '')
    {
		selContaMaeID.title = sContaMae;
		lblContaMaeID.title = sContaMae;	
    }
	else
	{
		selContaMaeID.title = '';
		lblContaMaeID.title = '';	
	}
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       
    strPars += '&currDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nIdiomaID=' + escape(getDicCurrLang());

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/planocontas/modalpages/modalprint.asp'+strPars;
    
    showModalWin(htmlPath, new Array(476,410));
}

function adjustLabelsCombos(bPaging)
{
    if (bPaging)
    {
        setLabelOfControl(lblTipoContaID, dsoSup01.recordset['TipoContaID'].value);
        setLabelOfControl(lblContaMaeID, dsoSup01.recordset['ContaMaeID'].value);
        setLabelOfControl(lblUsoEspecialID, dsoSup01.recordset['UsoEspecialID'].value);
        setLabelOfControl(lblTipoDetalhamentoID, dsoSup01.recordset['TipoDetalhamentoID'].value);
	}        
    else
    {
        setLabelOfControl(lblTipoContaID, selTipoContaID.value);
        setLabelOfControl(lblContaMaeID, selContaMaeID.value);
        setLabelOfControl(lblUsoEspecialID, selUsoEspecialID.value);
        setLabelOfControl(lblTipoDetalhamentoID, selTipoDetalhamentoID.value);
    }
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal do balancete de verificacao

Parametro:
btnClicked

Retorno:
nenhum
********************************************************************/
function openModalRazao(btnClicked)
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 800;
    var nHeight = 440;

	var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var userID = getCurrUserID();
    
    var aItemSelected = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = aItemSelected[1];

	 // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nContaID=' + escape(dsoSup01.recordset.Fields['ContaID'].value);
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&nContextoID=' + escape(nContextoID);
    strPars += '&nIdiomaID=' + escape(getDicCurrLang());

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/planocontas/modalpages/modalrazao.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth,nHeight));
}

function detalharLancamento(nLancamentoID)
{
	var aEmpresaData = getCurrEmpresaData();

	sendJSCarrier(getHtmlId(), 'SHOWLANCAMENTO', new Array(aEmpresaData[0], nLancamentoID));
}
