/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form planocontas
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_nContaID = null;
var glb_nEstadoID = null;
var glb_nDetalheID = null;
var glb_bConciliado = null;
var glb_sData = null;
var glb_sFiltro = null;
var dsoListData01 = new CDatatransport('dsoListData01');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoPropsPL = new CDatatransport('dsoPropsPL');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Conta', 'Tipo', 'N�vel', 'Conta M�e', 'Uso Especial', 
		'Detalhamento', 'DC', 'DO', 'AL', 'LM', 'SD', 'SC', 'UR', 'USA', 'Orc', 'Observa��o');
                                 
    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked() {
	showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1]);
	tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Raz�o', 'Balancete de Verifica��o', 'Concilia��o Contabil']);
	setupEspecBtnsControlBar('sup', 'HHHHHH');
        
	alignColsInGrid(fg,[0, 4, 5, 15]);        
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    if ( controlBar == 'SUP' )
    {
    	// Usuario clicou botao documentos
    	if (btnClicked == 1) {
    		if (fg.Rows > 1) {
    			__openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
    		}
    		else {
    		    window.top.overflyGen.Alert('Selecione um registro.');
    		}
    	}
    	// usuario clicou botao imprimir
		if (btnClicked == 2)
		    openModalPrint();
    
		if (btnClicked == 4)
			openModalRazao(btnClicked);	

		if (btnClicked == 5)
		    openModalBalanceteVerificacao(btnClicked);

		if (btnClicked == 6)
		    openModalConciliacao();			
    }
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    // por questoes esteticas, coloca foco no grid de pesquisa
    if ( fg.disabled == false )
        fg.focus();
    
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       
    strPars += '&currDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nIdiomaID=' + escape(getDicCurrLang());

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/planocontas/modalpages/modalprint.asp'+strPars;
    
    showModalWin(htmlPath, new Array(476,410));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal do balancete de verificacao

Parametro:
btnClicked

Retorno:
nenhum
********************************************************************/
function openModalBalanceteVerificacao(btnClicked)
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 772;
    var nHeight = 462;
    var userID = getCurrUserID();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nModalType=' + escape(btnClicked);
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&nEmpresaID=' + escape(empresaID);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/planocontas/modalpages/modalbalver.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth,nHeight));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal do balancete de verificacao

Parametro:
btnClicked

Retorno:
nenhum
********************************************************************/
function openModalRazao(btnClicked)
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 800;
    var nHeight = 440;
    
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var userID = getCurrUserID();
    
    var aItemSelected = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = aItemSelected[1];

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

	if (fg.Rows > 1)
		strPars += '&nContaID=' + escape(getCellValueByColKey(fg, 'ContaID', fg.Row));
	else
		strPars += '&nContaID=' + escape(0);

    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&nContextoID=' + escape(nContextoID);
    strPars += '&nIdiomaID=' + escape(getDicCurrLang());

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/planocontas/modalpages/modalrazao.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth,nHeight));
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if ( idElement.toUpperCase() == 'MODALBALVERHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            
            // nao mexer
            return 0;
        }
    }
    // Modal de impressao
    else if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    // Modal de lancamentos
    else if ( idElement.toUpperCase() == 'MODALRAZAOHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

			glb_nContaID = param2[0];
			glb_nEstadoID = param2[1];
			glb_nDetalheID = param2[2];
			glb_bConciliado = param2[3];
			glb_sData = param2[4];
			glb_sFiltro = param2[5];

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
    	if (param1 == 'OK') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    	else if (param1 == 'CANCEL') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    }
    // Modal de Concilia��o
    else if (idElement.toUpperCase() == 'MODALCONCILIACAOHTML') 
    {
        if (param1 == 'OK')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}

function openModalConciliacao()
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 543;
   
    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/planocontas/modalpages/modalconciliacao.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}