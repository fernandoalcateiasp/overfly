/********************************************************************
especificinf.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();
	var nTipoDetalhamentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'TipoDetalhamentoID' + '\'' + '].value');

    setConnection(dso);
    
    // Ordem Normal
    glb_bCheckInvert = false;
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT * FROM PlanoContas a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.ContaID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // Lan�amentos
    else if (folderID == 29001)
    {
		glb_bCheckInvert = true;
		
        // Paginacao Inferior
        glb_currGridTable = 'Lancamentos_Contas';
        glb_aCmbKeyPesq = [['Lan�amento','DbdtLancamento'], ['Balancete','DbdtBalancete']];
        glb_vOptParam1 = nTipoDetalhamentoID;
        
        if (getCmbCurrDataInControlBar('inf', 2) != null)
        {
			sFiltro = getCmbCurrDataInControlBar('inf', 2)[1];
			
			if (sFiltro != 0)
			{
				if ( (nTipoDetalhamentoID >= 1532) && (nTipoDetalhamentoID <= 1541) )
					sFiltro = ' AND e.PessoaID=' + sFiltro + ' ';
				else if (nTipoDetalhamentoID == 1542)
					sFiltro = ' AND a.RelPesContaID=' + sFiltro + ' ';
				else if (nTipoDetalhamentoID == 1543)
					sFiltro = ' AND f.ConceitoID=' + sFiltro + ' ';
			}
			else
				sFiltro = '';
		}
		else
			sFiltro = '';
        
        // Complemento do strPars que sera enviado a pagina de pesquisa.
        // O valor deste campo eh enviado separadamente pelo motivo
        // desta janela ser especifica deste form, e este campo tambem
        strPars = '&bSohConciliacao=' + escape( (glb_bSohConciliados ? 1 : 0) );
        
        // paginacao de grid
        return execPaging(dso, 'ContaID', idToFind, folderID, sFiltro, strPars);
    }
    // Historicos Padrao
    else if (folderID == 29002)
    {
        dso.SQL = 'SELECT DISTINCT b.HistoricoPadraoID, c.RecursoAbreviado, b.HistoricoPadrao ' +
                  'FROM HistoricosPadrao_Contas a WITH(NOLOCK), HistoricosPadrao b WITH(NOLOCK), Recursos c WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.ContaID = '+ idToFind + ' ' +
                  'AND a.HistoricoPadraoID = b.HistoricoPadraoID AND b.EstadoID = c.RecursoID ' +
                  'ORDER BY b.HistoricoPadraoID ';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(29001);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 29001); //Lan�amentos

    vPastaName = window.top.getPastaName(29002);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 29002); //HistoricosPadrao

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Lan�amentos
    else if (folderID == 29001)
    {
        headerGrid(fg,['Data',
					   'Balancete',
                       'Lan�',
                       'E',
                       'Contrapart',
                       '$',
                       'Valor',
                       'Saldo',
                       'Documento',
                       'Pessoa',
                       'Bco/Ag/Cta',
                       'Imposto',
                       'Hist�rico'], [7]);

        fillGridMask(fg,currDSO,['dtLancamento',
 								 'dtBalancete',
								 'LancamentoID',
								 'Estado',
								 'Contrapartida',
								 'SimboloMoeda',
								 'Valor',
								 'Saldo',
								 'Documento',
								 'Fantasia',
								 'ContaBancaria',
								 'Imposto',
								 'Historico'],
                                 ['99/99/9999','99/99/9999','','','','','','','','','','',''],
                                 [dTFormat,dTFormat,'','','','','(###,###,##0.00)','(###,###,###,##0.00)','','','','','']);

		// Merge de Colunas
		fg.MergeCells = 4;
		fg.MergeCol(0) = true;
		fg.MergeCol(1) = true;
		fg.MergeCol(2) = true;
		fg.MergeCol(3) = true;
		fg.MergeCol(4) = true;
		
		var nColValor = getColIndexByColKey(fg, 'Valor');
		var nColSaldo = getColIndexByColKey(fg, 'Saldo');
		
		fg.Redraw = 0;
		
		for ( i=1; i<fg.Rows; i++ )
		{
			if ( fg.ValueMatrix(i, nColValor) < 0 )
			{
				fg.Select(i, nColValor, i, nColValor);
				fg.FillStyle = 1;
		        fg.CellForeColor = 0X0000FF;
			}

			if ( fg.ValueMatrix(i, nColSaldo) < 0 )
			{
				fg.Select(i, nColSaldo, i, nColSaldo);
				fg.FillStyle = 1;
		        fg.CellForeColor = 0X0000FF;
			}
		}

		if (fg.Rows > 1)
			fg.Row = 1;

		fg.Redraw = 2;
		alignColsInGrid(fg,[2,4,6,7]);
    }
    // HistoricosPadrao
    else if (folderID == 29002)
    {
        headerGrid(fg,['ID',
					   'Est',
                       'Hist�rico Padr�o'], []);

        fillGridMask(fg,currDSO,['HistoricoPadraoID',
 								 'RecursoAbreviado',
								 'HistoricoPadrao'],
                                 ['','',''],
                                 ['','','']);

		alignColsInGrid(fg,[0]);
    }
}