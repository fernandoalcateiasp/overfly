﻿using System;
using java.lang;

namespace Overfly3.modcontabil.submodcontabilidade.historicospadrao.serverside
{
    public partial class verificacaohistoricopadrao : System.Web.UI.OverflyPage
    {
        private Integer RegistroID;

        public Integer nRegistroID
        {
            get { return RegistroID; }
            set { RegistroID = value; }
        }
        private Integer EstadoDeID;

        public Integer nEstadoDeID
        {
            get { return EstadoDeID; }
            set { EstadoDeID = value; }
        }
        private Integer EstadoParaID;

        public Integer nEstadoParaID
        {
            get { return EstadoParaID; }
            set { EstadoParaID = value; }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                   "SELECT dbo.fn_HistoricoPadrao_Verifica( " + RegistroID.ToString() + ", " +
                                                EstadoDeID.ToString() + ", " +
                                                EstadoParaID.ToString() + ", GETDATE()) AS Verificacao"));
        }
    }
}