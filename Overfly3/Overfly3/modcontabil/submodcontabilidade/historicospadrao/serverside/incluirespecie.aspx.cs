﻿using System;
using java.lang;

namespace Overfly3.modcontabil.submodcontabilidade.historicospadrao.serverside
{
    public partial class incluirespecie : System.Web.UI.OverflyPage
    {

        private Integer Codigo;

        public Integer nCodigo
        {
            get { return Codigo; }
            set { Codigo = value; }
        }
        private Integer RegistroID;

        public Integer nRegistroID
        {
            get { return RegistroID; }
            set { RegistroID = value; }
        }
        private Integer UserID;

        public Integer nUserID
        {
            get { return UserID; }
            set { UserID = value; }
        }
        private string Observacao;

        public string sObservacao
        {
            get { return Observacao; }
            set { Observacao = value; }
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            int fldresp;
            string sql = "";

            sql += "INSERT INTO Atributos " +
            "SELECT 10120, 29030," + (RegistroID.ToString()) + ", GETDATE()," + (UserID.ToString()) + ", 2503," + (Codigo.ToString()) + ", '" + (Observacao) + "'";
             fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select '" + fldresp + "' as Mensagem"
                )
            );

        }
    }
}