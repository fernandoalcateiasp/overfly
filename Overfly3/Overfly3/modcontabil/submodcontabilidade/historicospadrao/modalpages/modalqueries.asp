<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalqueriesHtml" name="modalqueriesHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/historicospadrao/modalpages/modalqueries.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
                   
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
            
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
            
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, btnOKState, dso, fieldName, gridID, colQuery
Dim sConta

btnOKState = "D"  'desabilitado
dso = ""
fieldName = ""
gridID = ""
colQuery = 0
sConta = ""

For i = 1 To Request.QueryString("btnOKState").Count
    btnOKState = Request.QueryString("btnOKState")(i)
Next

For i = 1 To Request.QueryString("dso").Count
    dso = Request.QueryString("dso")(i)
Next

For i = 1 To Request.QueryString("fieldName").Count
    fieldName = Request.QueryString("fieldName")(i)
Next

For i = 1 To Request.QueryString("gridID").Count
    gridID = Request.QueryString("gridID")(i)
Next

For i = 1 To Request.QueryString("colQuery").Count
    colQuery = Request.QueryString("colQuery")(i)
Next

For i = 1 To Request.QueryString("sConta").Count
    sConta = Request.QueryString("sConta")(i)
Next

If (btnOKState = "D") Then
    Response.Write "var glb_btnOKState = 'D';"
Else
    Response.Write "var glb_btnOKState = 'H';"
End If

Response.Write "var glb_dso = " & Chr(39) & dso & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_fieldName = " & Chr(39) & fieldName & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_gridID = " & Chr(39) & gridID & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_colQuery = " & CStr(colQuery) & ";"
Response.Write vbcrlf
Response.Write "var glb_sConta = " & Chr(39) & sConta & Chr(39) & ";"

Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    var elem = document.getElementById('modalqueriesBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    if (! txtQuery.readOnly )
        txtQuery.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	var sQuery = '';
	
    // texto da secao01
    secText('Query - ' + glb_sConta, 1);

    // reajusta dimensoes e reposiciona a janela
    (getFrameInHtmlTop( getExtFrameID(window))).style.top = (getFrameInHtmlTop( 'frameSup01')).offsetTop;

    // reajusta dimensoes e reposiciona a janela
	//redimAndReposicionModalWin(740, 490, false);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    var nMemoHeight = 425; //375;
    
    // ajusta o divObservacoes
    elem = window.document.getElementById('divObservacoes');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = MAX_FRAMEWIDTH - (ELEM_GAP * 12);
        temp = parseInt(width);
        height = (nMemoHeight * 2) + (ELEM_GAP) + (16 * 2);
    }

    elem = document.getElementById('txtQuery');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = MAX_FRAMEWIDTH - (ELEM_GAP * 12);
        height = nMemoHeight;
    }

	sQuery = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL ,
                               glb_gridID + '.TextMatrix(' + glb_gridID + '.Row,' +
                               glb_colQuery + ')');

	if (sQuery == null)
		sQuery = '';

    btnOK.disabled = (glb_btnOKState == 'D');
    
    txtQuery.maxLength = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL ,
                              glb_dso + '.recordset[' + '\'' + glb_fieldName + '\'' + '].definedSize');
    txtQuery.readOnly = btnOK.disabled;
    txtQuery.value = sQuery;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
	var tempStr = '';
	var match = 0;
	
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
		// Procura pela palavra DROP
		tempStr = (txtQuery.value).toUpperCase();
		
		match = tempStr.indexOf("DROP");
		
		if (match != -1)
		{	
			
			if ( window.top.overflyGen.Alert ('Query n�o aceita cl�usula DROP') == 0 )
                    return null;
			
			lockControlsInModalWin(false);
			
			window.focus();
			
			if ( match == -1 )
				if ( !txtQuery.readOnly )
					txtQuery.focus();

			return true;
		}
    
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , txtQuery.value);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

//-->
</script>

</head>

<body id="modalqueriesBody" name="modalqueriesBody" LANGUAGE="javascript" onload="return window_onload()">
   
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

     <div id="divObservacoes" name="divObservacoes" class="divGeneral">
        <textarea id="txtQuery" name="txtQuery" VIEWASTEXT LANGUAGE="javascript" onfocus="return __selFieldContent(this)" class="fldGeneral"></textarea>
    </div>    

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        

</body>

</html>
