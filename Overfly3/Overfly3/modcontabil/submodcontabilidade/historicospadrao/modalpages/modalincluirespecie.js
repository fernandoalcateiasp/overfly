/********************************************************************
modalincluirespecie.js

Library javascript para o modalincluirespecie.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// Dados para clonar registro
var glb_sMensagem = '';
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_refreshGrid = null;
var dsoGrid = new CDatatransport('dsoGrid');
var dsoIncluirEspecie = new CDatatransport('dsoIncluirEspecie');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {

    window_onload_1stPart();

    // ajusta o body do html
    with (modalincluirespecieBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.Redraw = 2;

    /*if ( document.getElementById('txtLocalidade').disabled == false )
        txtLocalidade.focus();
        */
    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Cidades');
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Atributos', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // desabilita o botao OK
    btnOK.disabled = false;
    //btnFillGrid.disabled = false;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    //adjustElementsInForm([['lblMotivo', 'txtMotivo', 41, 1, -10, -10];

    with (divEspecie.style) {
        border = 'none';
        backgroundColor = 'transparent';
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = (parseInt(divEspecie.currentStyle.top, 10) + parseInt(divEspecie.currentStyle.height, 10)) - 11;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = modHeight - parseInt(top, 10) - ((ELEM_GAP + 6) + 60);
    }

    with (fg.style) {
        left = 0;
        top = 20;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    startGridInterface(fg);

    fg.Redraw = 2;

    with (btnOK) {
        style.top = divEspecie.offsetTop + 220;  //+ txtFiltro.offsetTop;
        style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 205;
        value = 'Incluir';
    }

    with (btnCanc) {
        style.visibility = 'hidden';
    }

    fillGridData();
}

function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        incluir();
    }

        // codigo privado desta janela
    else {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
    }
}

function refreshParamsAndDataAndShowModalWin(fromServer) {
    if (glb_refreshGrid != null) {
        window.clearInterval(glb_refreshGrid);
        glb_refreshGrid = null;
    }

    // @@ atualizar interface aqui
    //setupBtnsFromGridState();

    // mostra a janela modal
    // fillGridData();

    showExtFrame(window, true);

    window.focus();

}

function fillGridData() {
    var sSQL = 'SELECT CONVERT(INT,Numero) AS Codigo, ItemMasculino AS Descricao ' +
                    'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                    'WHERE a.TipoID = 908 AND a.EstadoID = 2 ' +
                    'ORDER BY CONVERT(INT,Numero)';


    setConnection(dsoGrid);
    dsoGrid.SQL = sSQL;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

function fillGridData_DSC() {
    headerGrid(fg, ['C�digo',
                    'Descri��o'], []);

    fillGridMask(fg, dsoGrid, ['Codigo',
                               'Descricao'],
                               ['', ''],
                               ['', '']);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    //    fg.FrozenCols = 3;

    lockControlsInModalWin(false);
    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
    }

    fg.Redraw = 2;
    window.focus();
}

function incluir() {
    lockControlsInModalWin(true);

    var sMensagem = '';
    var strPars = new String();
    var nBytesAcum = 0;
    var nRegistroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'HistoricoPadraoID\').value');
    var nUserID = getCurrUserID();
    var nCodigo = 0;
    var sObservacao = '';

    strPars = '';

    if (fg.Row > 0) {
        _retMsg = window.top.overflyGen.Confirm("Deseja inserir este valor?");

        if (_retMsg == 0)
            return null;
        else if (_retMsg == 1)
            // Trava interface
            lockInterface(true);
    }


    nCodigo = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Codigo'));
    sObservacao = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Descricao'));

    strPars = '?nCodigo=' + escape(nCodigo);
    strPars += '&nRegistroID=' + escape(nRegistroID);
    strPars += '&nUserID=' + escape(nUserID);
    strPars += '&sObservacao=' + escape(sObservacao);

    lockControlsInModalWin(false);

    setConnection(dsoIncluirEspecie);
    dsoIncluirEspecie.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/historicospadrao/serverside/incluirespecie.asp' + strPars;
    dsoIncluirEspecie.ondatasetcomplete = incluir_DSC;
    dsoIncluirEspecie.Refresh();
}

function incluir_DSC() {
    if (!(dsoIncluirEspecie.recordset.BOF && dsoIncluirEspecie.recordset.EOF)) {
        if (dsoIncluirEspecie.recordset['Mensagem').value != null && dsoIncluirEspecie.recordset['Mensagem').value.length > 0)
            glb_sMensagem = dsoIncluirEspecie.recordset['Mensagem').value;

    }
}