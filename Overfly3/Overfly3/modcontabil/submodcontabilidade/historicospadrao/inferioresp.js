/********************************************************************
especificinf.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();
    setConnection(dso);

    //@@
    if ((folderID == 20008) || (folderID == 20009)) // Observacoes/Prop/Altern
    {
        dso.SQL = 'SELECT a.HistoricoPadraoID, a.ProprietarioID , a.AlternativoID, a.Observacoes ' +
            'FROM HistoricosPadrao a WITH(NOLOCK) ' +
            'WHERE ' + sFiltro + ' a.HistoricoPadraoID = ' +idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' +nEmpresaData[2]+ ') AS DataFuso ' +
                    'FROM LOGs a WITH(NOLOCK) ' +
                    'WHERE ' +sFiltro + ' a.RegistroID = ' + idToFind + ' ' +
                    'AND a.FormID=' + glb_nFormID + ' ' +
            'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // Contas
    else if (folderID == 29031)
    {
        dso.SQL = 'SELECT *, LEFT(CONVERT(VARCHAR(8000),Query),100) + ' +
                    '(CASE WHEN LEN(Query) <= 100 THEN SPACE(0) ELSE ' + '\'' + '...' + '\'' + ' END) AS QueryAbreviada ' +
                'FROM HistoricosPadrao_Contas a WITH(NOLOCK) ' +
                'WHERE ' +sFiltro + ' a.HistoricoPadraoID = ' + idToFind + ' ' +
                'ORDER BY a.TipoLancamentoID, a.Grupo, a.EhDebito DESC, a.ContaID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 29032)
    {
        dso.SQL = 'SELECT * FROM Atributos a WITH(NOLOCK) ' +
                'WHERE a.FormID = 10120 AND a.SubFormID = 29030 AND a.RegistroID = ' + idToFind;
        return 'dso01Grid_DSC';
    }
    // Layouts
    else if (folderID == 32034)
    {
        dso.SQL = 'SELECT a.*, a.dtInclusao AS Data, a.EhEstorno AS [Estorno] ' +
	            'FROM HistoricosPadrao_PrestadorLayout a WITH(NOLOCK) ' +
	            'WHERE  a.HistoricoPadraoID = ' + idToFind;
        return 'dso01Grid_DSC';
    }
}
/********************************************************************
Monta string de select para combo dentro de grid
            
Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente
            
Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID) {
    setConnection(dso);

    // LOG
    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
                      }
                      else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' +nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
                      }
                      else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
        // Contas
    else if (pastaID == 29031) {
        var nEmpresaData = getCurrEmpresaData();
        var nEmpresaID = nEmpresaData[0];
        var nPaisID = nEmpresaData[1];
        var nTipoLancamentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'TipoLancamentoID' + '\'' + '].value');

        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID,ItemAbreviado,ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE (EstadoID=2 AND TipoID=907 AND ((ItemID = 1573) OR ' +
						'(ItemID = ' + nTipoLancamentoID + '))) ' +
			          'ORDER BY Ordem';
        }
        else if (dso.id == 'dsoCmb02Grid_01') {
            dso.SQL = 'SELECT 0 AS ContaID, SPACE(0) AS ContaIdentada ' +
            'UNION ALL ' +
            'SELECT ContaID, dbo.fn_Conta_Identada(ContaID, ' + getDicCurrLang() + ', NULL, NULL) AS ContaIdentada ' +
                'FROM PlanoContas WITH(NOLOCK) ' +
                'WHERE EstadoID=2 ' +
                'ORDER BY ContaID';
        }
        else if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.ContaID, b.Conta, dbo.fn_Conta_AceitaLancamento(b.ContaID) AS AceitaLancamento ' +
                      'FROM HistoricosPadrao_Contas a WITH(NOLOCK), PlanoContas b WITH(NOLOCK) ' +
                      'WHERE (a.HistoricoPadraoID = ' + nRegistroID + ' AND a.ContaID = b.ContaID)';
        }
    }
    else if (pastaID == 29032) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.RegistroID, a.AtributoID, a.FormID, a.SubFormID, CONVERT (VARCHAR,a.Data,103) as Data, dbo.fn_TipoAuxiliar_Item(a.TipoAtributoID,0) as Atributo, a.valor as Valor, a.Observacao ' +
                      'FROM Atributos a WITH(NOLOCK) ' +
                         'INNER JOIN HistoricosPadrao b WITH(NOLOCK) ON (b.HistoricoPadraoID = a.RegistroID) ' +
                      'WHERE a.FormID = 10120 AND a.SubFormID = 29030 AND b.HistoricoPadraoID =' + nRegistroID;
        }
    }

    else if (pastaID == 32034) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT PessoaID, Fantasia FROM Pessoas WITH(NOLOCK) WHERE ((Observacao LIKE \'%<Contabilidade>%\') AND (ClassificacaoID = 70)) ORDER BY PessoaID '; //Inserir o ID dos poss�veis prestadores.
        }

        if (dso.id == 'dsoCmb02Grid_01') {
            dso.SQL = 'SELECT ItemID, ItemMasculino FROM TiposAuxiliares_Itens  WITH(NOLOCK) WHERE ((TipoID = 909) AND (Filtro LIKE \'%<Contabilidade>%\')) ORDER BY ItemID';
        }

        if (dso.id == 'dsoCmb03Grid_01') {
            dso.SQL = 'SELECT ItemID, CASE WHEN (ItemID = 1001) THEN \'Pag\' ELSE \'Rec\' END AS Item FROM TiposAuxiliares_Itens  WITH(NOLOCK) WHERE ItemID IN (1001,1002) ORDER BY ItemID';
        }

        if (dso.id == 'dsoCmb04Grid_01') {
            dso.SQL = 'SELECT ItemID, CASE WHEN (ItemID = 1001) THEN \'Pag\' ELSE \'Rec\' END AS Item FROM TiposAuxiliares_Itens  WITH(NOLOCK) WHERE ItemID IN (1001,1002) ORDER BY ItemID';
        }

    }

}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID) {
    var i = 0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

    cleanupSelInControlBar('inf', 1);

    vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(29031);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 29031); //Contas

    vPastaName = window.top.getPastaName(29032);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 29032); //Atributos

    vPastaName = window.top.getPastaName(32034);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 32034); //Layout

    // Nao mexer - Inicio de automacao ============================== 	
    // Tenta selecionar novamente a pasta anterior selecionada no combo
    // Se nao mais existe a pasta anteriormente selecionada no combo
    // seleciona a pasta default

    stripFoldersByRight(currSelection);

    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID) {
    //@@ return true se tudo ok, caso contrario return false;

    var currLine = 0;

    // A pasta e grid
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {

        if (folderID == 29031) // Contas
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['HistoricoPadraoID', registroID], [13, 15]);

            if (currLine < 0)
                return false;
        }

        if (folderID == 29032) // Atributos
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['RegistroID', registroID], []);

            if (currLine < 0)
                return false;
        }

        if (folderID == 32034) // Layouts
        {
            var nUserID = getCurrUserID();

            fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'UsuarioID')) = nUserID;

            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['HistoricoPadraoID', registroID], [12, 13]);
            if (currLine < 0)
                return false;
        }
    }

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO) {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.FrozenCols = 0;

    // LOG
    if (folderID == 20010) {
        headerGrid(fg, ['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
        fillGridMask(fg, currDSO, ['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['', '99/99/9999', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm:ss', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
        // Contas
    else if (folderID == 29031) {
        headerGrid(fg, ['Tipo',
                       'Grupo',
                       'D�b',
                       'ID',
                       'Conta',
                       'AL',
                       'Pri',
                       'Def',
                       'Obr',
                       'Ped',
                       'Fin',
                       'GL',
                       'Query',
                       'Query',
                       'Observa��o',
                       'HisContaID'], [13, 15]);

        // array de celulas com hint
        glb_aCelHint = [[0, 2, '� D�bito?'],
						[0, 5, 'Esta conta aceita lan�amento ou � totalizadora?'],
						[0, 6, 'Esta conta � a principal neste hist�rico padr�o?'],
						[0, 7, 'Esta conta � default neste hist�rico padr�o?'],
						[0, 8, 'Esta conta � obrigatoria neste hist�rico padr�o?'],
						[0, 9, 'Esta conta � utilizada em pedido?'],
						[0, 10, 'Esta conta � utilizada em financeiro?'],
						[0, 11, 'Esta conta � utilizada na concilia��o banc�ria quando gerar lan�amento?']];

        fillGridMask(fg, currDSO, ['TipoLancamentoID',
								 'Grupo',
								 'EhDebito',
								 'ContaID',
								 '^ContaID^dso01GridLkp^ContaID^Conta*',
								 '^ContaID^dso01GridLkp^ContaID^AceitaLancamento*',
								 'EhPrincipal',
								 'EhDefault',
								 'EhObrigatoria',
								 'Pedido',
								 'Financeiro',
								 'GeraLancamento',
								 '_calc_QueryAbreviada_100*',
								 'Query',
								 'Observacao',
								 'HisContaID'],
								 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                                 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);

      for (icount = 1; icount < fg.Rows; icount++) {
            currDSO.recordset.MoveFirst();
            var currID = fg.TextMatrix(icount, fg.Cols - 1);

            currDSO.recordset.find['HisContaID = ' + currID];

            if (!currDSO.recordset.EOF) {
                if (currDSO.recordset['QueryAbreviada'].value != null)
                    fg.TextMatrix(icount, getColIndexByColKey(fg, '_calc_QueryAbreviada_100*')) = currDSO.recordset['QueryAbreviada'].value;
                else
                    fg.TextMatrix(icount, getColIndexByColKey(fg, '_calc_QueryAbreviada_100*')) = '';

            }
            else {
                fg.TextMatrix(icount, getColIndexByColKey(fg, '_calc_QueryAbreviada_100*')) = '';
            }
        }

        alignColsInGrid(fg, [3]);
        fg.FrozenCols = 4;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
        fg.Redraw = 0;
        fg.ColWidth(getColIndexByColKey(fg, '^ContaID^dso01GridLkp^ContaID^AceitaLancamento*')) = fg.ColWidth(getColIndexByColKey(fg, 'EhDefault'));
        fg.Redraw = 2;

    }
    if (folderID == 29032) {
        headerGrid(fg, ['FormID',
                        'SubFormID',
                        'RegistroID',
                        'Data',
                        'Atributo',
                        'Valor',
                        'Observa��o',
                        'AtributoID'], [0, 1, 2, 7]);
        fillGridMask(fg, currDSO, ['FormID',
                                   'SubFormID',
                                   'RegistroID',
                                   '^AtributoID^dso01GridLkp^AtributoID^Data*',
                                   '^AtributoID^dso01GridLkp^AtributoID^Atributo*',
                                   '^AtributoID^dso01GridLkp^AtributoID^Valor*',
                                   '^AtributoID^dso01GridLkp^AtributoID^Observacao*',
                                   'AtributoID'],
                                   ['', '', '', '99/99/9999', '', '', '', ''],
                                   ['', '', '', dTFormat, '', '', '', '']);

        // Merge de Colunas
        //fg.MergeCells = 4;
        //fg.MergeCol(-1) = true;
    }
    // Layouts
    if (folderID == 32034) {
        headerGrid(fg, ['HistoricoPadraoID',
                        'Data',
                        'Prestador',
                        'Layout',
                        'HP Tipo',
                        'Tipo Fin',
                        'Importe',
                        'Fin Ped',
                        'Estorno',
                        'Ativa��o',
                        'Observa��o',
                        'UsuarioID',
                        'HisPresLayoutID'], [0, 11, 12]);

        // array de celulas com hint
        glb_aCelHint = [[0, 4, '� Rec ou Pag?'],
                        [0, 5, 'Financeiro � Pag ou Rec?'],
                        [0, 6, 'Financeiro � do tipo Importe?'],
                        [0, 7, 'Financeiro possui pedido?'],
                        [0, 8, '� layout de estorno?'],
                        [0, 9, 'Reporta ativa��o do financeiro?']];

        fillGridMask(fg, currDSO, ['HistoricoPadraoID',
                                   'Data',
                                   'PrestadorID',
                                   'PrestadorLayoutID',
                                   'HpTipo',
                                   'FinTipo',
                                   'EhImporte',
                                   'FinPed',
                                   'Estorno',
                                   'Ativacao',
                                   'Observacao',
                                   'UsuarioID',
                                   'HisPresLayoutID'],
                                   ['', '99/99/9999', '', '', '', '', '', '', '', '', '', '', ''],
                                   ['', dTFormat, '', '', '', '', '', '', '', '', '', '', '']);

        fg.Redraw = 0;
        fg.autoSizeMode = 0;
        fg.autoSize(0, fg.Cols - 1);
        fg.Redraw = 2;

        // '^ContaID^dso01GridLkp^ContaID^Conta*',     		             

    }
}
