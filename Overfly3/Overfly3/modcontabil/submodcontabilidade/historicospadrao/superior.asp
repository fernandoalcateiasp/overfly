<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="historicospadraosup01Html" name="historicospadraosup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
              
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/historicospadrao/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/historicospadrao/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="historicospadraosup01Body" name="historicospadraosup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- //@@ Os dsos sao definidos de acordo com o form -->
        
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p id="lblHistoricoPadrao" name="lblHistoricoPadrao" class="lblGeneral">Hist�rico Padr�o</p>
        <input type="text" id="txtHistoricoPadrao" name="txtHistoricoPadrao" DATASRC="#dsoSup01" DATAFLD="HistoricoPadrao" class="fldGeneral"></input>
        <p id="lblHistoricoComplementar" name="lblHistoricoComplementar" class="lblGeneral">Hist�rico Complementar</p>
        <input type="text" id="txtHistoricoComplementar" name="txtHistoricoComplementar" DATASRC="#dsoSup01" DATAFLD="HistoricoComplementar" class="fldGeneral"></input>
        <p id="lblHistoricoAbreviado" name="lblHistoricoAbreviado" class="lblGeneral">Hist�rico Abrev</p>
        <input type="text" id="txtHistoricoAbreviado" name="txtHistoricoAbreviado" DATASRC="#dsoSup01" DATAFLD="HistoricoAbreviado" class="fldGeneral"></input>
		<p id="lblTipoLancamentoID" name="lblTipoLancamentoID" class="lblGeneral">Tipo</p>
		<select id="selTipoLancamentoID" name="selTipoLancamentoID" DATASRC="#dsoSup01" DATAFLD="TipoLancamentoID" class="fldGeneral"></select>
        <p  id="lblUsoCotidiano" name="lblUsoCotidiano" class="lblGeneral">UC</p>
        <input type="checkbox" id="chkUsoCotidiano" name="chkUsoCotidiano" DATASRC="#dsoSup01" DATAFLD="UsoCotidiano" class="fldGeneral" title="Uso cotidiano ou somente em certas ocasi�es?"></input>
        <p  id="lblUsoSistema" name="lblUsoSistema" class="lblGeneral">US</p>
        <input type="checkbox" id="chkUsoSistema" name="chkUsoSistema" DATASRC="#dsoSup01" DATAFLD="UsoSistema" class="fldGeneral" title="Uso exclusivo do sistema"></input>
        <p  id="lblEhRetorno" name="lblEhRetorno" class="lblGeneral">Ret</p>
        <input type="checkbox" id="chkEhRetorno" name="chkEhRetorno" DATASRC="#dsoSup01" DATAFLD="EhRetorno" class="fldGeneral" title="Este hist�rico padr�o � retorno?"></input>
        <p  id="lblAnteciparLancamento" name="lblAnteciparLancamento" class="lblGeneral">AL</p>
        <input type="checkbox" id="chkAnteciparLancamento" name="chkAnteciparLancamento" DATASRC="#dsoSup01" DATAFLD="AnteciparLancamento" class="fldGeneral" title="Permite antecipar lan�amento?"></input>
        <p  id="lblMultiplasEmpresas" name="lblMultiplasEmpresas" class="lblGeneral">ME</p>
        <input type="checkbox" id="chkMultiplasEmpresas" name="chkMultiplasEmpresas" DATASRC="#dsoSup01" DATAFLD="MultiplasEmpresas" class="fldGeneral" title="Permite m�ltiplas empresas?"></input>
        <p  id="lblPedido" name="lblPedido" class="lblGeneral">Ped</p>
        <input type="checkbox" id="chkPedido" name="chkPedido" class="fldGeneral" title="Este hist�rico padr�o � usado em pedido?"></input>
        <p  id="lblFinanceiro" name="lblFinanceiro" class="lblGeneral">Fin</p>
        <input type="checkbox" id="chkFinanceiro" name="chkFinanceiro" class="fldGeneral" title="Este hist�rico padr�o � usado em financeiro?"></input>
        <p  id="lblConciliacaoBancaria" name="lblConciliacaoBancaria" class="lblGeneral">CB</p>
        <input type="checkbox" id="chkConciliacaoBancaria" name="chkConciliacaoBancaria" class="fldGeneral" title="Na concilia��o banc�ria, considera lan�amentos com este hist�rico padr�o?"></input>
        <p  id="lblGeraLancamento" name="lblGeraLancamento" class="lblGeneral">GL</p>
        <input type="checkbox" id="chkGeraLancamento" name="chkGeraLancamento" class="fldGeneral" title="Na concilia��o banc�ria, utiliza este hist�rico padr�o para gerar lan�amento?"></input>
        <p  id="lblMarketing" name="lblMarketing" class="lblGeneral">Mkt</p>
        <input type="checkbox" id="chkMarketing" name="chkMarketing" DATASRC="#dsoSup01" DATAFLD="Marketing" class="fldGeneral" title="Permite associar a��es de marketing � financeiros com este hist�rico padr�o?"></input>
        <p id="lblPesoContabilizacao" name="lblPesoContabilizacao" class="lblGeneral">Peso</p>
        <input type="text" id="txtPesoContabilizacao" name="txtPesoContabilizacao" DATASRC="#dsoSup01" DATAFLD="PesoContabilizacao" class="fldGeneral" title="Peso para efeito de contabiliza��o"></input>
        <p id="lblGrupo" name="lblGrupo" class="lblGeneral">Grupo</p>
        <input type="text" id="txtGrupo" name="txtGrupo" DATASRC="#dsoSup01" DATAFLD="Grupo" class="fldGeneral"></input>
        <p id="lblRequerDocumentoFiscal" name="lblRequerDocumentoFiscal" class="lblGeneral">Fis</p>
        <input type="checkbox" id="chkRequerDocumentoFiscal" name="chkRequerDocumentoFiscal" DATASRC="#dsoSup01" DATAFLD="RequerDocumentoFiscal" class="fldGeneral" title="Este hist�rico padr�o requer documento fiscal?"></input>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
	    <p id="lblLimiteObservacao" name="lblLimiteObservacao" class="lblGeneral">Limite Observa��o</p>
        <input type="text" id="txtLimiteObservacao" name="txtLimiteObservacao" DATASRC="#dsoSup01" DATAFLD="LimiteObservacao" class="fldGeneral"></input>
   </div>
    
</body>

</html>
