/********************************************************************
js_especificsup.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 *, ' +
               'CONVERT(VARCHAR, dtInicial, '+DATE_SQL_PARAM+') AS V_dtInicial, ' +
               'CONVERT(VARCHAR, dtFinal, '+DATE_SQL_PARAM+') AS V_dtFinal, ' +
               'CONVERT(VARCHAR, dtEmissao, ' + DATE_SQL_PARAM + ') AS V_dtEmissao, ' +
			   'CONVERT(NUMERIC(9), dbo.fn_ExtratoBancario_Totais(ExtratoID, NULL, 1)) AS NumeroLancamentos, ' +
			   'CASE WHEN ISNULL(Concilia, 0) = 1 THEN dbo.fn_ExtratoBancario_Saldo(dbo.fn_ContaBancaria_Extrato_Data(RelPesContaID, NULL, 1, NULL, ExtratoID), 0, dbo.fn_Data_Zero(dtInicial)) ELSE 0 END AS SaldoConciliaAnterior, ' +
			   'CASE WHEN ISNULL(Concilia, 0) = 1 THEN dbo.fn_ContaBancaria_Extrato_Data(RelPesContaID, NULL, 1, NULL, ExtratoID) ELSE 0 END AS ExtratoConciliaAnteriorID, ' +
			   'dbo.fn_ExtratoBancario_Totais(ExtratoID, NULL, 2) AS ValorTotalDebitos, ' +
			   'dbo.fn_ExtratoBancario_Totais(ExtratoID, NULL, 3) AS ValorTotalCreditos, ' +
			   'dbo.fn_ExtratoBancario_Totais(ExtratoID, dtInicial, 5) AS SaldoInicial, ' +
			   'CASE WHEN ISNULL(Concilia, 0) = 1 THEN dbo.fn_ExtratoBancario_Saldo(dbo.fn_ContaBancaria_Extrato_Data(RelPesContaID, dtInicial, 0, NULL, NULL), 0, dbo.fn_Data_Zero(dtInicial)) ELSE 0 END AS SaldoNaoConcilia, ' +
			   'CASE WHEN ISNULL(Concilia, 0) = 1 THEN dbo.fn_ContaBancaria_Extrato_Data(RelPesContaID, dtInicial, 0, NULL, NULL) ELSE 0 END AS ExtratoNaoConciliaID, ' +		   
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=ExtratosBancarios.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM ExtratosBancarios WITH(NOLOCK)  ' +
               'WHERE ProprietarioID = '+ nID + 'ORDER BY ExtratoID DESC';
    else
        sSQL = 'SELECT *, ' + 
               'CONVERT(VARCHAR, dtInicial, '+DATE_SQL_PARAM+') AS V_dtInicial, ' +
               'CONVERT(VARCHAR, dtFinal, '+DATE_SQL_PARAM+') AS V_dtFinal, ' +
               'CONVERT(VARCHAR, dtEmissao, ' + DATE_SQL_PARAM + ') AS V_dtEmissao, ' +
			   'CONVERT(NUMERIC(9), dbo.fn_ExtratoBancario_Totais(ExtratoID, NULL, 1)) AS NumeroLancamentos, ' +
			   'CASE WHEN ISNULL(Concilia, 0) = 1 THEN dbo.fn_ExtratoBancario_Saldo(dbo.fn_ContaBancaria_Extrato_Data(RelPesContaID, NULL, 1, NULL, ExtratoID), 0, dbo.fn_Data_Zero(dtInicial)) ELSE 0 END AS SaldoConciliaAnterior, ' +
			   'CASE WHEN ISNULL(Concilia, 0) = 1 THEN dbo.fn_ContaBancaria_Extrato_Data(RelPesContaID, NULL, 1, NULL, ExtratoID) ELSE 0 END AS ExtratoConciliaAnteriorID, ' +
			   'dbo.fn_ExtratoBancario_Totais(ExtratoID, NULL, 2) AS ValorTotalDebitos, ' +
			   'dbo.fn_ExtratoBancario_Totais(ExtratoID, NULL, 3) AS ValorTotalCreditos, ' +
			   'dbo.fn_ExtratoBancario_Totais(ExtratoID, dtInicial, 5) AS SaldoInicial, ' +
			   'CASE WHEN ISNULL(Concilia, 0) = 1 THEN dbo.fn_ExtratoBancario_Saldo(dbo.fn_ContaBancaria_Extrato_Data(RelPesContaID, dtInicial, 0, NULL, NULL), 0, dbo.fn_Data_Zero(dtInicial)) ELSE 0 END AS SaldoNaoConcilia, ' +
			   'CASE WHEN ISNULL(Concilia, 0) = 1 THEN dbo.fn_ContaBancaria_Extrato_Data(RelPesContaID, dtInicial, 0, NULL, NULL) ELSE 0 END AS ExtratoNaoConciliaID, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=ExtratosBancarios.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM ExtratosBancarios WITH(NOLOCK) WHERE ExtratoID = ' + nID + ' ORDER BY ExtratoID DESC';

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;
    
    sql = 'SELECT *, ' +
          'CONVERT(VARCHAR, dtInicial, '+DATE_SQL_PARAM+') AS V_dtInicial, ' +
          'CONVERT(VARCHAR, dtFinal, '+DATE_SQL_PARAM+') AS V_dtFinal, ' +
          'CONVERT(VARCHAR, dtEmissao, ' + DATE_SQL_PARAM + ') AS V_dtEmissao, ' +
  		  'CONVERT(NUMERIC(9), dbo.fn_ExtratoBancario_Totais(ExtratoID, NULL, 1)) AS NumeroLancamentos, ' +
          'CASE WHEN ISNULL(Concilia, 0) = 1 THEN dbo.fn_ExtratoBancario_Saldo(dbo.fn_ContaBancaria_Extrato_Data(RelPesContaID, NULL, 1, NULL, ExtratoID), 0, dbo.fn_Data_Zero(dtInicial)) ELSE 0 END AS SaldoConciliaAnterior, ' +
	      'CASE WHEN ISNULL(Concilia, 0) = 1 THEN dbo.fn_ContaBancaria_Extrato_Data(RelPesContaID, NULL, 1, NULL, ExtratoID) ELSE 0 END AS ExtratoConciliaAnteriorID, ' +
		  'dbo.fn_ExtratoBancario_Totais(ExtratoID, NULL, 2) AS ValorTotalDebitos, ' +
		  'dbo.fn_ExtratoBancario_Totais(ExtratoID, NULL, 3) AS ValorTotalCreditos, ' +
	      'dbo.fn_ExtratoBancario_Totais(ExtratoID, dtInicial, 5) AS SaldoInicial, ' +
          'CASE WHEN ISNULL(Concilia, 0) = 1 THEN dbo.fn_ExtratoBancario_Saldo(dbo.fn_ContaBancaria_Extrato_Data(RelPesContaID, dtInicial, 0, NULL, NULL), 0, dbo.fn_Data_Zero(dtInicial)) ELSE 0 END AS SaldoNaoConcilia, ' +
          'CASE WHEN ISNULL(Concilia, 0) = 1 THEN dbo.fn_ContaBancaria_Extrato_Data(RelPesContaID, dtInicial, 0, NULL, NULL) ELSE 0 END AS ExtratoNaoConciliaID, ' +
          '0 AS Prop1, 0 AS Prop2 ' +
          'FROM ExtratosBancarios WHERE ExtratoID = 0';
    
    dso.SQL = sql;          
}              
