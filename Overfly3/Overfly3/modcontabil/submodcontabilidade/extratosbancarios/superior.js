/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_insertExtrato = false;
var glb_nLastExtratorInserted = 0;
var dsoSup01 = new CDatatransport('dsoSup01');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoCurrData = new CDatatransport('dsoCurrData');
var dsoVerificacao = new CDatatransport('dsoVerificacao');
var dsoTransferirVinculada = new CDatatransport('dsoTransferirVinculada');
//var glb_nTipoExtratoID;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    glb_aStaticCombos = ([['selRelPesContaID', '1']]);

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/inferior.asp',
                              SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'ExtratoID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID', 'txtEstadoID', 2, 1],
                          ['lblConcilia', 'chkConcilia', 3, 1],
                          ['lbldtEmissao', 'txtdtEmissao', 10, 1],
                          ['lbldtInicial','txtdtInicial',10,1],
                          ['lbldtFinal','txtdtFinal',10,1],                          
                          ['lblRelPesContaID','selRelPesContaID',24,2],
                          ['lblBanco','txtBanco',9,2],
                          ['lblContaCorrente','txtContaCorrente',20,2],
                          ['lblTipoConta','txtTipoConta',15,2],
                          ['lblNumeroLancamentos','txtNumeroLancamentos',5,3],
                          ['lblSaldoConciliaAnterior', 'txtSaldoConciliaAnterior', 22, 3],                          
                          ['lblValorTotalDebitos','txtValorTotalDebitos',11,3],
                          ['lblValorTotalCreditos','txtValorTotalCreditos',11,3],
                          ['lblSaldoFinal', 'txtSaldoFinal', 11, 3],
                          ['lblSaldoNaoConcilia', 'txtSaldoNaoConcilia', 22, 3],                          
                          ['lblArquivo','txtArquivo',30,4],
                          ['lblObservacao','txtObservacao',30,4]], null, null, true);
    chkConcilia.readOnly = true;
    chkConcilia.disabled = true;
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWEXTRATOBANCARIO')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
        
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
            
        window.top.focus();
                
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@

    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    //@@
    
    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR'))
		adjustLabelsCombos(true);
    
	// Mover esta funcao para os finais de retornos de operacoes no
	// banco de dados
	finalOfSupCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    if (btnClicked == 'SUPINCL')
        showHideData(true);

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    
    if ( btnClicked == 'SUPINCL' )
    {
        // funcao que controla campos read-only
        controlReadOnlyFields();
    }

    showHideData(true);
    treatBalanceFields();

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    adjustLabelsCombos(false);
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();

    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
	verifyExtratoInServer(currEstadoID, newEstadoID);
	return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	var aEmpresaData = getCurrEmpresaData();

	// Documentos
	if ((controlBar == 'SUP') && (btnClicked == 1)) {
	    __openModalDocumentos(controlBar, dsoSup01.recordset['ExtratoID'].value);
	}
	// Procedimento
	if (btnClicked == 3)
	    window.top.openModalControleDocumento('S', '', 910, null, '222', 'T');
	else if (btnClicked == 4)
	    sendJSCarrier(getHtmlId(), 'SHOWCONTA', new Array(aEmpresaData[0], 111020100));
	else if (btnClicked == 5)
	    openModalAssociaLancamentoConcilia();
	else if (btnClicked == 6)
	    openModalAssociaValorLocalizar();
	else if (btnClicked == 7)
	    TransferirVinculada();
		//openModalConciliaExtratos();
		
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
	else if (idElement.toUpperCase() == 'MODALASSOCIAVALORLOCALIZARHTML' )
    {
        if ( param1 == 'OK' )                
        {
			// esta funcao fecha a janela modal e destrava a interface
			restoreInterfaceFromModal();    
			// escreve na barra de status
			writeInStatusBar('child', 'cellMode', 'Detalhe');
			// nao mexer
			return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALASSOCIALANCAMENTOCONCILIAHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            // nao mexer
            return 0;
        }
    }
	else if (idElement.toUpperCase() == 'MODALCONCILIAEXTRATOSHTML' )
    {
        if ( param1 == 'OK' )                
        {
			// esta funcao fecha a janela modal e destrava a interface
			restoreInterfaceFromModal();    
			// escreve na barra de status
			writeInStatusBar('child', 'cellMode', 'Detalhe');
			// nao mexer
			return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALPOSICAOEXTRATOSHTML') {
        if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // Simula cancelamento de inclusao
            __btn_CANC('SUP');

            // nao mexer
            return 0;
        }
    }
}


/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // funcao que controla campos read-only
    controlReadOnlyFields();
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Conta', 'Associar Lan�amentos Concilia', 'Associar Valor a Localizar', 'Transferir valor vinculado']);
}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Read Only

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields()
{    
    txtdtInicial.readOnly = true;   
    txtdtFinal.readOnly = true;   
    txtBanco.readOnly = true;
    txtContaCorrente.readOnly = true;
    txtTipoConta.readOnly = true;
    txtNumeroLancamentos.readOnly = true;
    txtValorTotalDebitos.readOnly = true;
    txtValorTotalCreditos.readOnly = true;
    txtSaldoFinal.readOnly = true;
    txtArquivo.readOnly = true;
}

/********************************************************************
Criada pelo programador
Esta funcao abre a janela modal de bancos
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function openModalFiles()
{
    var htmlPath;
    var strPars = new String();
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];
    
    // mandar os parametros para o servidor
    strPars = '?sCaller=' + escape('S');   
    strPars += '&nEmpresaID=' + escape(nEmpresaID);   
               
    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/modalpages/modalposicaoextratos.asp'+strPars;
    showModalWin(htmlPath, new Array(500,450));
        
    // Prossegue a automacao
    return null;
}

/********************************************************************
Criada pelo programador
Esta funcao abre a janela modal de conciliacao de extratos bancarios
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function openModalAssociaValorLocalizar()
{
	var htmlPath;
    var strPars = new String();
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];
    var nExtratoID = dsoSup01.recordset['ExtratoID'].value;
    var sBanco = selRelPesContaID.options[selRelPesContaID.selectedIndex].innerText;
    var nRelPesContaID = dsoSup01.recordset['RelPesContaID'].value;
    var bConcilia = dsoSup01.recordset['Concilia'].value;
    
    // mandar os parametros para o servidor
    strPars = '?sCaller=' + escape('S');   
    strPars += '&nEmpresaID=' + escape(nEmpresaID);   
    strPars += '&nExtratoID=' + escape(nExtratoID);   
    strPars += '&sBanco=' + escape(sBanco);
    strPars += '&nRelPesContaID=' + escape(nRelPesContaID);

    strPars += '&bConcilia=' + escape(bConcilia);
               
    htmlPath = SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/modalpages/modalassociavalorlocalizar.asp'+strPars;
    showModalWin(htmlPath, new Array(990, 475));
        
    // Prossegue a automacao
    return null;
}

/********************************************************************
Criada pelo programador
Esta funcao abre a janela modal de associa��o de lan�amentos concilia
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function openModalAssociaLancamentoConcilia() {
    var htmlPath;
    var strPars = new String();
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];
    var nExtratoID = dsoSup01.recordset['ExtratoID'].value;
    var sBanco = selRelPesContaID.options[selRelPesContaID.selectedIndex].innerText;
    var nRelPesContaID = dsoSup01.recordset['RelPesContaID'].value;

    // mandar os parametros para o servidor
    strPars = '?sCaller=' + escape('S');
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nExtratoID=' + escape(nExtratoID);
    strPars += '&sBanco=' + escape(sBanco);
    strPars += '&nRelPesContaID=' + escape(nRelPesContaID);

    htmlPath = SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/modalpages/modalassocialancamentoconcilia.asp' + strPars;
    showModalWin(htmlPath, new Array(990, 475));

    // Prossegue a automacao
    return null;
}

/********************************************************************
Criada pelo programador
Esta funcao abre a janela modal de conciliacao de extratos bancarios
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function openModalConciliaExtratos()
{
	var htmlPath;
    var strPars = new String();
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];
    var nExtratoID = dsoSup01.recordset['ExtratoID'].value;
    var sBanco = selRelPesContaID.options[selRelPesContaID.selectedIndex].innerText;
    var nRelPesContaID = dsoSup01.recordset['RelPesContaID'].value;
    
    // mandar os parametros para o servidor
    strPars = '?sCaller=' + escape('S');   
    strPars += '&nEmpresaID=' + escape(nEmpresaID);   
    strPars += '&nExtratoID=' + escape(nExtratoID);   
    strPars += '&sBanco=' + escape(sBanco);   
    strPars += '&nRelPesContaID=' + escape(nRelPesContaID);   
               
    htmlPath = SYS_PAGESURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/modalpages/modalconciliaextratos.asp'+strPars;
    showModalWin(htmlPath, new Array(771,462));
        
    // Prossegue a automacao
    return null;
}

/********************************************************************
Criada pelo programador
Esta detalha um lancamento
           
Parametros: 
nLancamentoID

Retorno:
nenhum
********************************************************************/
function detalhaLancamento(nLancamentoID)
{
    var empresa = getCurrEmpresaData();
	
	if ( (nLancamentoID != null) && (nLancamentoID != '') )
	    sendJSCarrier(getHtmlId(), 'SHOWLANCAMENTO', new Array(empresa[0], nLancamentoID));
}

/********************************************************************
Verificacoes do Extrato
********************************************************************/
function verifyExtratoInServer(nCurrEstadoID, nNewEstadoID)
{
    var nExtratoID = dsoSup01.recordset['ExtratoID'].value;
    var strPars = new String();

    strPars = '?nExtratoID='+escape(nExtratoID);
    strPars += '&nCurrEstadoID='+escape(nCurrEstadoID);
    strPars += '&nNewEstadoID='+escape(nNewEstadoID);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/verificacaoextrato.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyExtratoInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes do Extrato
********************************************************************/
function verifyExtratoInServer_DSC()
{
    if (dsoVerificacao.recordset.Fields['Verificacao'].value == null)
    {
        stateMachSupExec('OK');
    }    
    else
    {
        if ( window.top.overflyGen.Alert(dsoVerificacao.recordset.Fields['Verificacao'].value) == 0 )
			return null;

        stateMachSupExec('CANC');
    }
}

function adjustLabelsCombos(bPaging)
{
    if (bPaging) {
        setLabelOfControl(lblRelPesContaID, dsoSup01.recordset['RelPesContaID'].value);
        setLabelOfControl(lblSaldoNaoConcilia, (dsoSup01.recordset['ExtratoNaoConciliaID'].value) + ')');
        setLabelOfControl(lblSaldoConciliaAnterior, (dsoSup01.recordset['ExtratoConciliaAnteriorID'].value) + ')');        
    }
    else {
        setLabelOfControl(lblRelPesContaID, selRelPesContaID.value);
        //setLabelOfControl(lblSaldoNaoConcilia, (txtSaldoNaoConcilia.value) + ')');
        //setLabelOfControl(lblSaldoConciliaAnterior, (txtSaldoConciliaAnterior.value) + ')');        
    }
}

function showHideData(bPaging) {
    var bConcilia;
    
    if (bPaging == true)
        bConcilia = dsoSup01.recordset['Concilia'].value;
    else
        bConcilia = chkConcilia.checked;

    if (!bConcilia) {
        lblSaldoNaoConcilia.style.visibility = 'hidden';
        txtSaldoNaoConcilia.style.visibility = 'hidden';
        lblSaldoConciliaAnterior.style.visibility = 'hidden';
        txtSaldoConciliaAnterior.style.visibility = 'hidden';
    }
    else {
        lblSaldoNaoConcilia.style.visibility = 'inherit';
        txtSaldoNaoConcilia.style.visibility = 'inherit';
        lblSaldoConciliaAnterior.style.visibility = 'inherit';
        txtSaldoConciliaAnterior.style.visibility = 'inherit';
    }
    
}

function detalharLancamento(nLancamentoID)
{
	var aEmpresaData = getCurrEmpresaData();

	sendJSCarrier(getHtmlId(), 'SHOWLANCAMENTO', new Array(aEmpresaData[0], nLancamentoID));
}

function detalharValorLocalizar(nValorID)
{
	var aEmpresaData = getCurrEmpresaData();

	sendJSCarrier(getHtmlId(), 'SHOWVALORID', new Array(aEmpresaData[0], nValorID));
}

function getCurrDate() {
    var d, s = "";
    d = new Date();

    if (DATE_SQL_PARAM == 103) {
        s += padL(d.getDate().toString(), 2, '0') + "/";
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
    }
    else {
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
        s += padL(d.getDate().toString(), 2, '0') + "/";
    }

    s += d.getYear();

    return (s);
}

function treatBalanceFields() {
    var nSaldoFinal = dsoSup01.recordset['SaldoFinal'].value;
    var nSaldoNaoConcilia = dsoSup01.recordset['SaldoNaoConcilia'].value;
    var nSaldoConciliaAnterior = dsoSup01.recordset['SaldoConciliaAnterior'].value;
    var nTotalDebitos = dsoSup01.recordset['ValorTotalDebitos'].value;
    var nTotalCreditos = dsoSup01.recordset['ValorTotalCreditos'].value;

    if (nSaldoNaoConcilia == nSaldoFinal)
        txtSaldoNaoConcilia.style.color = 'green';
    else
        txtSaldoNaoConcilia.style.color = 'red';

    if ((Math.round((nSaldoConciliaAnterior + nTotalDebitos + nTotalCreditos) * 100) / 100) == nSaldoFinal)
        txtSaldoConciliaAnterior.style.color = 'green';
    else
        txtSaldoConciliaAnterior.style.color = 'red';
}

function TransferirVinculada()
{
    var nExtratoID = dsoSup01.recordset['ExtratoID'].value;
    var str = '?nExtratoID=' + escape(nExtratoID);

    dsoTransferirVinculada.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/transferenciaVinculada.aspx' + str;
    dsoTransferirVinculada.ondatasetcomplete = TransferirVinculada_DSC;
    dsoTransferirVinculada.refresh();
}

function TransferirVinculada_DSC()
{
    var _retMsg = '';

    if (!(dsoTransferirVinculada.recordset.BOF && dsoTransferirVinculada.recordset.EOF))
    {
        if ((dsoTransferirVinculada.recordset['Resultado'].value != null) && (dsoTransferirVinculada.recordset['Resultado'].value != ''))
        {
            if (window.top.overflyGen.Alert(dsoTransferirVinculada.recordset['Resultado'].value) == 0)
            {
                return null;
            }
        }
        else if (dsoTransferirVinculada.recordset['FinanceiroDeID'].value > 0)
        {
            _retMsg = window.top.overflyGen.Confirm('Transfer�ncia Banc�ria criada com sucesso: ' +
                      '\n  Financeiro Origem: ' + dsoTransferirVinculada.recordset['FinanceiroDeID'].value +
                      '\n  Financeiro Destino: ' + dsoTransferirVinculada.recordset['FinanceiroParaID'].value +
                      '\n\nDetalhar Financeiro Origem?');

            var detalhar = dsoTransferirVinculada.recordset['FinanceiroDeID'].value;

            if (_retMsg != 1)
            {
                return null;
            }
            else if (_retMsg == 1)
            {
                lockInterface(true);

                sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWDETAIL',
                                [detalhar, null, null]);
            }
        }
        else
        {
            if (window.top.overflyGen.Alert('N�o h� transfer�ncias') == 0)
            {
                return null;
            }
        }

    }
}