/********************************************************************
especificinf.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.ProprietarioID,a.AlternativoID,a.Observacoes FROM ExtratosBancarios a WHERE '+
        sFiltro + 'a.ExtratoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // Lan�amentos Extratos
    else if (folderID == 29091) {
        dso.SQL = 'SELECT a.ExtLancamentoID, a.ExtratoID, a.dtLancamento, ' +
				  'a.Historico, a.Documento, a.Valor, dbo.fn_ExtratoBancario_Saldo(a.ExtratoID, a.ExtLancamentoID, NULL) AS Saldo, ' +
                  'b.LancamentoID, a.Identificador, a.TipoLancamento, a.NaoConcilia, ' +
                  'dbo.fn_ExtratoBancarioLancamento_Tipo(a.ExtLancamentoID) AS TipoID, ' +
                  'a.ValorID AS ValorID ' +
                  'FROM ExtratosBancarios_Lancamentos a WITH(NOLOCK) ' +
                        'LEFT OUTER JOIN Lancamentos_Contas b WITH(NOLOCK) ON (a.LanContaID = b.LanContaID) ' +
                  'WHERE ' + sFiltro + ' a.ExtratoID = ' + idToFind + ' ' +
                  'ORDER BY a.dtLancamento, (CASE WHEN a.ExtLancamentoMaeID IS NULL THEN a.ExtLancamentoID ELSE a.ExtLancamentoMaeID END), a.ExtLancamentoID ';
        return 'dso01Grid_DSC';
    }
    // Lan�amentos Cont�beis
    else if (folderID == 29092) 
    {
        dso.SQL = 'EXEC sp_EventosContabeis_Lancamentos ' + glb_nFormID + ', NULL, ' + idToFind;
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(29091);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 29091); //Lancamentos Ex
    
    vPastaName = window.top.getPastaName(29092);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 29092); //Lancamentos Con

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO) {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.FrozenCols = 0;

    // LOG
    if (folderID == 20010) {
        headerGrid(fg, ['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);

        fillGridMask(fg, currDSO, ['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['', '99/99/9999', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm:ss', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Lancamentos Extratos
    else if (folderID == 29091) {
        var nTipoID, sColor;

        headerGrid(fg, ['Data',
                       'Hist�rico',
                       'Documento',
                       'Valor',
                       'Saldo',
					   'Identificador',
                       'Tipo',
                       'NC',
                       'TipoID',
                       'ExtLancamentoID',
                       'Lan�',
                       'ValorID'], [8, 9]);

        fillGridMask(fg, currDSO, ['dtLancamento',
								 'Historico',
								 'Documento',
								 'Valor',
								 'Saldo',
								 'Identificador',
								 'TipoLancamento',
								 'NaoConcilia',
								 'TipoID',
								 'ExtLancamentoID',
								 'LancamentoID',
								 'ValorID'],
								 ['', '', '', '', '', '', '', '', '', '', '', ''],
                                 [dTFormat, '', '', '(###,###,##0.00)', '(###,###,##0.00)', '', '', '', '', '', '', '']);


        gridHasTotalLine(fg, 'Saldo Inicial', 0xC0C0C0, null, true, [[4, '(###,###,##0.00)', 'S']]);

        for (i = 1; i < fg.Rows; i++) {
            if ((fg.Rows > 2) && (i == 1)) {
                fg.TextMatrix(1, 0) = '';
                fg.TextMatrix(1, 1) = 'Saldo Inicial';

                var nSaldoInicial = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				    'dsoSup01.recordset[' + '\'' + 'SaldoInicial' + '\'' + '].value');

                if (nSaldoInicial != null)
                    fg.TextMatrix(1, 4) = nSaldoInicial;
            }
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')) < 0) {
                fg.Select(i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor'));
                fg.FillStyle = 1;
                fg.CellForeColor = 0X0000FF;
            }
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Saldo')) < 0) {
                fg.Select(i, getColIndexByColKey(fg, 'Saldo'), i, getColIndexByColKey(fg, 'Saldo'));
                fg.FillStyle = 1;
                fg.CellForeColor = 0X0000FF;
            }

            nTipoID = parseInt(fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoID')), 10);

            sColor = null;

            // Amarelo
            if (nTipoID == 1)
                sColor = 0X8CE6F0;
            // Verde
            else if (nTipoID == 2)
                sColor = 0X90EE90;
            // Rosa
            else if (nTipoID == 3)
                sColor = 0X7280FA;

            if (sColor != null)
                fg.Cell(6, i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor')) = sColor;
        }

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;

        //alignColsInGrid(fg, [3, 4, 5]);
       // fg.FrozenCols = 1;

        if (fg.Rows > 2)
            fg.Row = 2;
    }
    else if (folderID == 29092) 
    {
        headerGrid(fg, ['Lan�amento',
                        'Est',
                        'Data',
                        'Balancete',
                        'Est',
                        'Tipo',
                        'Quant',
                        'Hist�rico Padr�o',
                        'Hist�rico Complementar',
                        'Detalhe',
                        'D',
                        'C',
                        'Moeda',
                        'Total Lan�amento',
                        'Taxa',
                        'Total Convertido',
                        'Observa��o',
                        'EventoID'], [17]);

        glb_aCelHint = [[0, 4, '� estorno?'],
						[0, 6, 'Quantidade de detalhes'],
						[0, 10, 'N�mero de D�bitos'],
						[0, 11, 'N�mero de Cr�ditos'],
						[0, 12, 'Taxa de convers�o para moeda comum']];

        fillGridMask(fg, currDSO,['LancamentoID',
                                 'Estado',
                                 'dtLancamento',
                                 'dtBalancete',
                                 'EhEstorno',
                                 'TipoLancamento',
                                 'Quantidade',
                                 'HistoricoPadrao',
                                 'HistoricoComplementar',
                                 'Detalhe',
                                 'NumeroDebitos',
                                 'NumeroCreditos',
                                 'Moeda',
                                 'ValorTotalLancamento',
                                 'TaxaMoeda',
                                 'ValorTotalConvertido',
                                 'Observacao',
                                 'EventoID'],
                                 ['9999999999', '', '99/99/9999', '99/99/9999', '', '', '999999', '', '', '', '99', '99', '',
									'999999999.99', '9999999999999.99', '999999999.99', '', ''],
								 ['##########', '', dTFormat, dTFormat, '', '', '######', '', '', '', '##', '##', '',
									'###,###,##0.00', '#,###,###,###,##0.0000000', '###,###,##0.00', '', '']);

        alignColsInGrid(fg, [0, 6, 10, 11, 13, 14, 15]);
        fg.FrozenCols = 2;
    }
}