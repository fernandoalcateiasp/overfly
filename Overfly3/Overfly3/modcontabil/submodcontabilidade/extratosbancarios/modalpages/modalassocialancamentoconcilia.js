/********************************************************************
modalassocialancamentoconcilia.js

Library javascript para o modalassocialancamentoconcilia.asp
Pessoas 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var teste = '';
var glb_FirstTime = true;
var glb_LASTLINESELID = null;
var glb_PassedOneInDblClick = false;
var glb_nSaldoExtrato = 0;
var glb_bDissociar = null;
var glb_nCounter = 0;
var glb_bConcilia = false;
var glb_TimerCallRowColChange = null;
var glb_bChecks = [];
var glb_nTam = null;
var glb_FinalDate1 = '';
var glb_FinalDate2 = '';
var dsoHistoricoPadrao = new CDatatransport('dsoHistoricoPadrao');
var dsoGridExtrato = new CDatatransport('dsoGridExtrato');
var dsoGridExtConcilia = new CDatatransport('dsoGridExtConcilia');
var dsoAssociarLancamento = new CDatatransport('dsoAssociarLancamento');
var dsoDissociarLancamento = new CDatatransport('dsoDissociarLancamento');
var dsoAssociacaoAutomatica = new CDatatransport('dsoAssociacaoAutomatica');
var dsoExcluirLancamento = new CDatatransport('dsoExcluirLancamento');
var dsoIncluirLancamento = new CDatatransport('dsoIncluirLancamento');
var dsoContabiliza = new CDatatransport('dsoContabiliza');
/********************************************************************
Objeto controlador do carregamento dos dsos de grids e
preenchimento destes dsos 

Instanciado pela variavel glb_gridControlFill
********************************************************************/
function __gridControlFill()
{
	this.bFollowOrder = false;
    this.nDsosToArrive = 0;
    this.dso1_Ref = null;
    this.dso2_Ref = null;
    this.grid1_Ref = null;
    this.grid2_Ref = null;
}

var glb_gridControlFill = new __gridControlFill();

/********************************************************************
Objeto controlador de execucao dos eventos dos grids e de grid com
linha de totais

Nota:
nColDataType = 0 (String), 1 (Numerico), 2 (Data), 3 (Boolean)
ncolUsedToSort = -1 nenhuma coluna
ncolOrderUsedToSort = 1 ascendente, -1 descendente

Instanciado pela variavel glb_gridsEvents
********************************************************************/

function __resetOrderParams()
{
	this.ncolUsedToSort_fgExtrato = -1;
	this.ncolOrderUsedToSort_fgExtrato = 0;
	this.ncolUsedToSort_fgLacContas = -1;
	this.ncolOrderUsedToSort_fgExtConcilia = 0;
}

function __gridsEvents()
{
	this.bTotalLine_fgExtrato = false;
	this.bTotalLine_fgExtConcilia = false;
	this.bBlockEvents_fgExtrato = false;
	this.bBlockEvents_fgExtConcilia = false;
	this.ncolUsedToSort_fgExtrato = -1;
	this.ncolOrderUsedToSort_fgExtrato = 0;
	this.ncolUsedToSort_fgLacContas = -1;
	this.ncolOrderUsedToSort_fgExtConcilia = 0;
	
	this.resetOrderParams = __resetOrderParams;
}
 
var glb_gridsEvents = new __gridsEvents();

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
   
window_onload()
btn_onclick(ctl)
setupPage()
adjustControlsInInterface(topFree, heightFree, widthFree, frameBorder, mode)
startDynamicGrids()
startDynamicGrids_DSC()
showModalAfterDataLoad()
interfaceControlsState()
adjustWidthCellsInGrid(grid)

execAssociarValorLocalizar()
execAssociarValorLocalizar_DSC
geraValorLocalizar()
execSPValorLocalizar_Gerador()
execSPValorLocalizar_Gerador_DSC()

cmbOnChange(cmb)
chkOnClick()

js_fg_ModConcExtrBeforeRowColChange(grid, oldRow, oldCol, newRow, newCol)
js_fg_ModConcExtrAfterRowColChangePesqList(grid, oldRow, oldCol, newRow, newCol)
fg_ModConcExtrBeforeSort(grid, col)
fg_ModConcExtrAfterSort(grid, col)
js_fg_ModConcExtrDblClick(grid, row, col)

lineChangedInGrid_Extrato(grid, nRow)
lineChangedInGrid_ValoresLocalizar(grid, nRow)

controlsInterfaceState()
gridSortParams(grid, col)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    AssociacaoAutomatica();

    // configuracao inicial do html
    setupPage();
    
    (getFrameInHtmlTop( getExtFrameID(window))).style.top = 
		(getFrameInHtmlTop( 'frameSup01')).offsetTop;

    startDynamicGrids();
    startDynamicCmbs();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    try {
        window.focus();
    }
    catch (e) {
         ;
    }
     
    if (ctl.id == btnOK.id) {
        try {
            btnOK.focus();
        }
        catch (e) {
            ;
        }
    }
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    else if (ctl.id == btnAssociar.id) {
        if (btnAssociar.value == 'Associar')
            Associa_Lancamentos();
        else
            Dissocia_Lancamentos();
    }
    else if (ctl.id == btnExcluir.id)
        Exclui_Lancamentos();
    else if (ctl.id == btnIncluir.id)
        Inclui_Lancamentos();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	// btnOK nao e usado nesta modal
	with (btnOK)
	{
		disabled = true;
		style.visibility = 'hidden';
	}
	
	// btnCanc nao e usado nesta modal
	with (btnCanc)
	{
		style.visibility = 'hidden';
	}
	
	glb_bConcilia = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'Concilia' + '\'' + '].value');

	chkLancamentosPendentes.onclick = chkOnClick;
	chkLancamentosPendentes.checked = true;

    // texto da secao01
	secText('Associar Lançamento Concilia - Extrato ' + glb_nExtratoID + ' (' + glb_sBanco + ')', 1);

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 5;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    // altura livre da janela modal (descontando apenas a barra azul)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - (2 * frameBorder);
    
    adjustControlsInInterface(topFree, heightFree, widthFree, frameBorder);

	glb_gridControlFill.bFollowOrder = false;
	glb_gridControlFill.nDsosToArrive = 2;
	glb_gridControlFill.dso1_Ref = dsoGridExtrato;
	glb_gridControlFill.dso2_Ref = dsoGridExtConcilia;
	glb_gridControlFill.grid1_Ref = fgExtrato;
	glb_gridControlFill.grid2_Ref = fgExtConcilia;

}

/********************************************************************
Posiciona os campos na interface, define labels e hints,
controla digitacao em campos numéricos, define eventos associados
a controles, etc

Parametros:
	topFree	- inicio da altura da janela (posicao onde termina o div do
	          titulo da janela )
	heightFree - altura livre da janela (do top free ate o botao OK,
	          supondo o btnOK em baixo, que e o padrao de modal)
	widthFree - largura livre da janela
	frameBorder - largura da borda da janela
	          
	mode (integer) - opcional -> null implica em mode 1:
		1 - dois grid em cima e botoes em baixo
		2 - dois grids em baixo e botoes em cima
	
	gridPos (integer) - opcional -> null implica em gridPos 1:
		1 - um grid ao lado do outro
		2 - um grid em cima do outro
		
Retorno:
	nenhum
********************************************************************/
function adjustControlsInInterface(topFree, heightFree, widthFree, frameBorder, mode, gridPos)
{
	var elem;
	var x_leftWidth = 0;
	var y_topHeight = 0;
	
	var elemLbl;
	var elemGrid;
	var lbl_height = 16;
	var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
	var btn_height = parseInt(btnOK.currentStyle.height, 10);
	var nDivControlsHeight = 0;
	
	if ( mode == null )
		mode = 1;
		
	if ( gridPos == null )
		gridPos = 1;

	// Posiciona os controles do divControles
    adjustElementsInForm([['lblLancamentosPendentes', 'chkLancamentosPendentes', 3, 1, -5],
						  ['btnIncluir', 'btn', btn_width, 1, ELEM_GAP - 4],
						  ['btnAssociar', 'btn', btn_width, 1, ELEM_GAP - 4],
						  ['lblLog','txtLog', 25, 1, ELEM_GAP - 4],
						  ['btnExcluir', 'btn', btn_width, 1, ELEM_GAP - 4]], null, null, true);

    btnIncluir.style.height = btn_height;
    btnExcluir.style.height = btn_height;
    btnAssociar.style.height = btn_height;
    txtLog.maxLength = 50;
	nDivControlsHeight = parseInt(btnAssociar.currentStyle.top, 10) +
	                     parseInt(btnAssociar.currentStyle.height, 10);
	// Dimensiona os divs em funcao dos parametros
	
	// o div de controles
	elem = divControles;
	with (elem.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		width = widthFree - (3 * ELEM_GAP / 2);
		height = nDivControlsHeight;
	}
	
	// os divs de grids
	if ( gridPos == 1)
	{
		elem = divGridExtrato;
		with (elem.style)
		{
			border = 'none';
			backgroundColor = 'transparent';
			width = (widthFree - (3 * ELEM_GAP / 2)) / 2 - (ELEM_GAP / 2);
			height = heightFree - 2 * ELEM_GAP - nDivControlsHeight;
		}

		elem = divGridExtConcilia;
		with (elem.style)
		{
			border = 'none';
			backgroundColor = 'transparent';
			width = (widthFree - (3 * ELEM_GAP / 2)) / 2 - (ELEM_GAP / 2);
			height = heightFree - 2 * ELEM_GAP - nDivControlsHeight;
		}	
	}
	else if ( gridPos == 2)
	{
		;
	}
	
	// Dimensiona e posiciona os grids e seus labels
	elemLbl = lblGridExtrato;
	elemGrid = divGridExtrato;
	with (elemLbl.style)
	{
		backgroundColor = 'transparent';
		left = 0;
		top = ELEM_GAP / 2;
		width = parseInt(elemGrid.currentStyle.width, 10) - 2;
		height = lbl_height;
	}
	
	grid = fgExtrato;
	elem = divGridExtrato;
	with (grid.style)
	{
		left = 0;
		top = parseInt(elemLbl.currentStyle.top, 10) +
		      parseInt(elemLbl.currentStyle.height, 10);
		width = parseInt(elem.currentStyle.width, 10);
		height = parseInt(elem.currentStyle.height, 10) -
		         (parseInt(elemLbl.currentStyle.top, 10));
	}
	
	startGridInterface(grid , 1, null);
	grid.FontSize = '7';
	grid.Cols = 1;
    grid.ColWidth(0) = parseInt(elem.currentStyle.width, 10) * 18;
	
	elemLbl = lblGridExtConcilia;
	elemGrid = divGridExtConcilia;
	with (elemLbl.style)
	{
		backgroundColor = 'transparent';
		left = 0;
		top = ELEM_GAP / 2;
		width = parseInt(elemGrid.currentStyle.width, 10) - 2;
		height = lbl_height;
	}
	
	grid = fgExtConcilia;
	elem = divGridExtConcilia;
	with (grid.style)
	{
		left = 0;
		top = parseInt(elemLbl.currentStyle.top, 10) +
		      parseInt(elemLbl.currentStyle.height, 10);
		width = parseInt(elem.currentStyle.width, 10);
		height = parseInt(elem.currentStyle.height, 10) -
		         (parseInt(elemLbl.currentStyle.top, 10));
	}

	startGridInterface(grid , 1, null);
	grid.FontSize = '7';
	grid.Cols = 1;
    grid.ColWidth(0) = parseInt(elem.currentStyle.width, 10) * 18;
	
	// Posiciona os divs na horizontal e vertical
	if ( mode == 1 )
	{
		if ( gridPos == 1 )
		{
			elem = divGridExtrato;
			elem.style.left = ELEM_GAP;
			elem.style.top = topFree;
			y_topHeight = parseInt(elem.currentStyle.top, 10) +
			                  parseInt(elem.currentStyle.height, 10);
			x_leftWidth = parseInt(elem.currentStyle.left, 10) +
			                  parseInt(elem.currentStyle.width, 10);
		
			elem = divGridExtConcilia;
			elem.style.left = ELEM_GAP + x_leftWidth;
			elem.style.top = topFree;
			y_topHeight = parseInt(elem.currentStyle.top, 10) +
			                  parseInt(elem.currentStyle.height, 10);
		
			elem = divControles;
			elem.style.left = ELEM_GAP;
			elem.style.top = y_topHeight + ELEM_GAP;
		}
		else if ( gridPos == 2 )
		{
			;
		}
	}
	else if ( mode == 2 )
	{
		if ( gridPos == 1 )
		{
			elem = divControles;
			elem.style.left = ELEM_GAP;
			elem.style.top = topFree;
			y_topHeight = parseInt(elem.currentStyle.top, 10) ;
		
			elem = divGridExtrato;
			elem.style.left = ELEM_GAP;
			elem.style.top = y_topHeight + ELEM_GAP;
			x_leftWidth = parseInt(elem.currentStyle.left, 10) +
			                  parseInt(elem.currentStyle.width, 10);
		
			elem = divGridExtConcilia;
			elem.style.left = ELEM_GAP + x_leftWidth;
			elem.style.top = y_topHeight + ELEM_GAP;
		}
		else if ( gridPos == 2 )
		{
			;
		}
    }

    elem = lblLog;
    with (elem.style) {
        left = parseInt(fgExtConcilia.currentStyle.width, 10) + ELEM_GAP;
    }
    
    elem = txtLog;
    with (elem.style) {
        left = parseInt(fgExtConcilia.currentStyle.width, 10) + ELEM_GAP;
    }

    elem = btnExcluir;
    with (elem.style) {
        left = parseInt(fgExtConcilia.currentStyle.width, 10) + ELEM_GAP + parseInt(txtLog.currentStyle.width, 10) + ELEM_GAP;
    }
    
        
}

/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos combos
           
Parametros: 
bAutomatico

Retorno:
nenhum
********************************************************************/
function execAssociarValorLocalizar(bAutomatico)
{
    ;
}

/********************************************************************
Funcao do programador
Retorno do servidor apos executar procedure de conciliacao bancaria
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function execAssociarValorLocalizar_DSC()
{
    ;
}

/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos combos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function conciliaExtrato()
{
   ;
}

/********************************************************************
Funcao do programador
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function contabilizarLancamentos() {
    ;
}

/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos combos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function limparIdentificadores()
{ 
    ;
}

/********************************************************************
Funcao do programador
Limpa Identificadores
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function limparIdentificadoresEx() {
    ;
}

/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos grids
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicGrids()
{
	if ( !glb_FirstTime)
		lockControlsInModalWin(true);
    		
	var dso; 
	var sFiltro = '';
	var sData = '';
	var nValor = '';

	if ( glb_gridControlFill.dso1_Ref != null )
	{
	    if (chkLancamentosPendentes.checked)
		    sFiltro = ' AND ExtLancamentoConciliaID IS NULL AND NaoConcilia = 0 ';
		
		dso = glb_gridControlFill.dso1_Ref;
    
    	setConnection(dso);

    	dso.SQL = 'SELECT 0 AS Indice, NULL AS ExtLancamentoID, NULL AS dtLancamento, NULL AS Documento, NULL AS Valor, NULL AS NaoConcilia, ' +
				    	'NULL AS Historico, NULL AS Identificador, NULL AS TipoID, ' +
						'dbo.fn_ExtratoBancario_Saldo(' + glb_nExtratoID + ', 0, NULL) AS SaldoFinal, ' +
						'NULL ExtLancamentoMaeID, NULL AS ExtLancamentoConciliaID, NULL AS GerouEvento, NULL AS ExtLancamentoConciliaID_Bit ' +
						'UNION ALL SELECT 1 AS Indice, ExtLancamentoID, dtLancamento, Documento, Valor, NaoConcilia, Historico, Identificador, ' +
						'dbo.fn_ExtratoBancarioLancamento_Tipo(ExtLancamentoID) AS TipoID, ' +
						'NULL AS SaldoFinal, ' +
						'(CASE WHEN ExtLancamentoMaeID IS NULL THEN ExtLancamentoID ELSE ExtLancamentoMaeID END) AS ExtLancamentoMaeID, ' +
						'ExtLancamentoConciliaID AS ExtLancamentoConciliaID,  ' + 
						'(CASE WHEN (SELECT COUNT(*) FROM EventosContabeis WHERE RegistroID = ExtLancamentoID AND FormID = 10140 AND SubFormID = 29091) = 0 THEN 0 ELSE 1 END), ' +
						'(CASE WHEN ExtLancamentoConciliaID IS NULL THEN 0 ELSE 1 END) ExtLancamentoConciliaID_Bit ' +
						'FROM ExtratosBancarios_Lancamentos WITH(NOLOCK) ' +
						'WHERE (ExtratoID = ' + glb_nExtratoID + ' AND Valor <> 0 ' + sFiltro + ' ) ' +
						'ORDER BY Indice, dtLancamento, ExtLancamentoMaeID, ExtLancamentoID';

		dso.ondatasetcomplete = startDynamicGrids_DSC;
		dso.Refresh();
		
    }
    
    if ( glb_gridControlFill.dso2_Ref != null )
	{
	    GetDateSQL();
	    
	    if (chkLancamentosPendentes.checked)
	        sFiltro = ' NOT EXISTS(SELECT * FROM ExtratosBancarios_Lancamentos C WHERE C.ExtLancamentoConciliaID = a.ExtLancamentoID AND C.ExtratoID = ' + glb_nExtratoID + ' ) AND ';

	    sFiltro += ' a.dtLancamento BETWEEN (' + '\'' + glb_FinalDate1 + '\'' + ') AND ' +
	               ' (' + '\'' + glb_FinalDate2 + '\'' + ') ' +
	               ' AND b.Concilia = 1 AND b.EstadoID NOT IN (5) AND b.RelPesContaID = ' + glb_nRelPesContaID + ' ' +
	               ' AND a.ExtratoID <> ' + glb_nExtratoID + ' AND ' +
				   ' dbo.fn_ExtratoBancarioLancamento_Tipo(a.ExtLancamentoID) <> 1 ';
	    
		dso = glb_gridControlFill.dso2_Ref;
    
    	setConnection(dso);

    	dso.SQL = 'SELECT 0 AS Indice, NULL AS ExtratoID, NULL AS ExtLancamentoID, NULL AS dtLancamento, NULL AS Documento, NULL AS Valor, NULL AS OK, NULL AS NaoConcilia, ' +
				    	'NULL AS Historico, NULL AS Identificador, NULL AS TipoID, ' +
						'dbo.fn_ExtratoBancario_Saldo(' + glb_nExtratoID + ', 0, NULL) AS SaldoFinal, ' +
						'NULL ExtLancamentoMaeID ' +
						'UNION ALL SELECT 1 AS Indice, a.ExtratoID, a.ExtLancamentoID, a.dtLancamento, a.Documento, a.Valor, NULL AS OK, a.NaoConcilia, a.Historico, a.Identificador, ' +
						'dbo.fn_ExtratoBancarioLancamento_Tipo(a.ExtLancamentoID) AS TipoID, ' +
						'NULL AS SaldoFinal, ' +
						'(CASE WHEN a.ExtLancamentoMaeID IS NULL THEN a.ExtLancamentoID ELSE a.ExtLancamentoMaeID END) AS ExtLancamentoMaeID ' +
						'FROM ExtratosBancarios_Lancamentos a WITH(NOLOCK) ' +
                        'INNER JOIN ExtratosBancarios b WITH(NOLOCK) ON a.ExtratoID = b.ExtratoID ' +
						'WHERE ( '+ sFiltro + ' ) ' +
						'ORDER BY Indice, dtLancamento, ExtLancamentoMaeID, ExtLancamentoID';

		dso.ondatasetcomplete = startDynamicGrids_DSC;
		dso.Refresh();
    }
}

/********************************************************************
Preenche os grids e mostra a janela
********************************************************************/
function startDynamicGrids_DSC()
{
	glb_gridControlFill.nDsosToArrive--;
	
	if (glb_gridControlFill.nDsosToArrive > 0)
		return null;

	var i;
	var dso, grid;
	var dTFormat = '';
	var nTipoID, sColor;
	var bPreencheuExtrato = false;
	    
	if ( DATE_FORMAT == "DD/MM/YYYY" )
	    dTFormat = 'dd/mm';
	else if ( DATE_FORMAT == "MM/DD/YYYY" )
	    dTFormat = 'mm/dd';
	    
	// Eventos dos grids - bloqueia/desbloqueia
	glb_gridsEvents.bBlockEvents_fgExtrato = true;
	glb_gridsEvents.bBlockEvents_fgExtConcilia = true;

	if (glb_gridControlFill.dso1_Ref != null)
	{
	    glb_nSaldoExtrato = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'SaldoFinal' + '\'' + '].value');
		
		dso = glb_gridControlFill.dso1_Ref;
		grid = glb_gridControlFill.grid1_Ref;
		
		if (!(dso.recordset.BOF && dso.recordset.EOF))
		{
			// move o cursor do dso para o primeiro registro
			dso.recordset.MoveFirst();
    
			dso.recordset.Filter = 'Indice = 1';
		}

		glb_gridsEvents.bTotalLine_fgExtrato = false;
		
		grid.ExplorerBar = 0;
		startGridInterface(grid , 1, null);
		grid.FontSize = '7';
		grid.Editable = false;
		grid.BorderStyle = 1;
		bPreencheuExtrato = true;
		
		headerGrid(grid,['Data',
		               'Doc',
		               'Valor',
		               'NC',
		               'Histórico',
		               'Identificador',		         
		               'ExtLancamentoID',
		               'TipoID',
		               'ExtLancamentoConciliaID',
		               'GerouEvento',
		               'C'], [6, 7, 8, 9]);
		
						                   
		fillGridMask(grid,dso,['dtLancamento',
		                       'Documento',
		                       'Valor',
		                       'NaoConcilia',
		                       'Historico',
		                       'Identificador',
		                       'ExtLancamentoID',
		                       'TipoID',
		                       'ExtLancamentoConciliaID',
		                       'GerouEvento',
		                       'ExtLancamentoConciliaID_Bit'],
		                       ['','','','','','','','','','',''],
		                       [dTFormat, '', '(###,###,##0.00)', '', '', '', '', '', '','','']);

		// Coloca tipo de dado nas colunas
		with (grid) {
		    ColDataType(0) = 7;
		    ColDataType(1) = 8;
		    ColDataType(2) = 6;
		    ColDataType(3) = 11;
		    ColDataType(4) = 8;
		    ColDataType(5) = 8;
		    ColDataType(6) = 3;
		    ColDataType(7) = 3;
		    ColDataType(8) = 3;
		    ColDataType(10) = 11;
		}

		if (!(dso.recordset.BOF && dso.recordset.EOF))
		{
			dso.recordset.Filter = '';

			// move o cursor do dso para o primeiro registro
			dso.recordset.MoveFirst();
		}

		alignColsInGrid(grid,[2]);
		
		
		
		if ( (glb_gridsEvents.ncolUsedToSort_fgExtrato != -1) && (grid.Rows >= 3) )
			grid.Select(2, glb_gridsEvents.ncolUsedToSort_fgExtrato);
	    
	    if ( glb_gridsEvents.ncolOrderUsedToSort_fgExtrato == 1 )
			grid.Sort = 1;
	    else if ( glb_gridsEvents.ncolOrderUsedToSort_fgExtrato == -1 )
			grid.Sort = 2;
		
        // Linha de Totais
        gridHasTotalLine(grid, 'Total', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(grid, 'Documento'), '######', 'C'],
                         [getColIndexByColKey(grid, 'Valor'), '###,###,###,##0.00', 'S']]);

        if ( grid.Rows >=3 )
        {
			if(!((glb_gridControlFill.dso1_Ref.recordset.BOF)&&(glb_gridControlFill.dso1_Ref.recordset.EOF)))
			{
			    glb_gridControlFill.dso1_Ref.recordset.MoveFirst();
				glb_gridControlFill.dso1_Ref.recordset.Filter = 'Indice = 0';
				glb_nSaldoExtrato = glb_gridControlFill.dso1_Ref.recordset['SaldoFinal'].value;
				glb_gridControlFill.dso1_Ref.recordset.Filter = '';
			    glb_gridControlFill.dso1_Ref.recordset.MoveFirst();

			}

			if (glb_nSaldoExtrato != null)
				grid.TextMatrix(1, getColIndexByColKey(grid, 'Valor')) = glb_nSaldoExtrato;
			else
				grid.TextMatrix(1, getColIndexByColKey(grid, 'Valor')) = 0;

			grid.FillStyle = 1;
			grid.TextMatrix(1, getColIndexByColKey(grid, 'Documento')) = grid.Rows - 2;
			grid.Select(1, getColIndexByColKey(grid, 'Documento'), 1, getColIndexByColKey(grid, 'Documento'));

			// Alinhamento da celula a direita
			grid.CellAlignment = 7;
			
			for (i=1; i<grid.Rows; i++)
			{
				if (grid == fgExtrato)
				{
					nTipoID = parseInt(grid.ValueMatrix(i, getColIndexByColKey(grid, 'TipoID')), 10);
						
					sColor = null;

					// Amarelo
					if (nTipoID == 1)									
						sColor = 0X8CE6F0;
					// Verde
					else if (nTipoID == 2)									
						sColor = 0X90EE90;
					// Rosa
					else if (nTipoID == 3)									
						sColor = 0X7280FA;

					if (sColor != null)
						grid.Cell(6, i, getColIndexByColKey(grid, 'Valor'), i, getColIndexByColKey(grid, 'Valor')) = sColor;
				}

				grid.Select(i, getColIndexByColKey(grid, 'Valor'), i, getColIndexByColKey(grid, 'Valor'));
				if (grid.ValueMatrix(i, getColIndexByColKey(grid, 'Valor')) < 0)
					grid.CellForeColor = 0X0000FF;
				else
					grid.CellForeColor = 0;
            }
			
		}	
        
		grid.FrozenCols = 4;
		grid.MergeCells = 4;
		grid.MergeCol(0) = true;
		grid.MergeCol(1) = true;
		
		grid.ExplorerBar = 5;
		
		if ( glb_gridsEvents.bTotalLine_fgExtrato )
		{
			if (grid.Rows > 2)
				grid.Row = 2;
		}	
		else
		{
			if (grid.Rows > 1)
				grid.Row = 2;
		}

		if ( (grid.Rows > 0) && grid.Row > 0 ) {
		    grid.TopRow = grid.Row;
		}

        grid.Editable = false;

        adjustWidthCellsInGrid(grid);

        for ( i=3; i<=grid.Rows; i++ )
        {
            glb_bChecks[i] = grid.ValueMatrix(i-1, getColIndexByColKey(grid, 'NaoConcilia'));
            glb_nTam = i;
        }

	}
	
	if (glb_gridControlFill.dso2_Ref != null)
	{
	    dso = glb_gridControlFill.dso2_Ref;
	    grid = glb_gridControlFill.grid2_Ref;
	    
	    if (!(dso.recordset.BOF && dso.recordset.EOF)) {
	        // move o cursor do dso para o primeiro registro
	        dso.recordset.MoveFirst();

	        dso.recordset.Filter = 'Indice = 1';
	    }

	    glb_gridsEvents.bTotalLine_fgExtConcilia = true;

	    grid.ExplorerBar = 0;
	    startGridInterface(grid, 1, null);
	    grid.FontSize = '7';
	    grid.Editable = false;
	    grid.BorderStyle = 1;
	    bPreencheuExtrato = true;

	    headerGrid(grid, ['Extrato',
	                   'Data',
		               'Doc',
		               'Valor',
		               'OK',
		               'NC',
		               'Histórico',
		               'Identificador',
		               'ExtLancamentoID',
		               'TipoID'], [8, 9]);


	    fillGridMask(grid, dso, ['ExtratoID',
	                           'dtLancamento',
		                       'Documento',
		                       'Valor',
		                       'OK',
		                       'NaoConcilia',
		                       'Historico',
		                       'Identificador',
		                       'ExtLancamentoID',
		                       'TipoID'],
		                       ['', '', '', '', '', '', '', '', '', ''],
		                       ['', dTFormat, '', '(###,###,##0.00)', '', '', '', '', '', '']);
	    
	    // Coloca tipo de dado nas colunas
	    with (grid) {
	        ColDataType(0) = 3;
	        ColDataType(1) = 7;
	        ColDataType(2) = 8;
	        ColDataType(3) = 6;
	        ColDataType(4) = 11;
	        ColDataType(5) = 11;
	        ColDataType(6) = 8;
	        ColDataType(7) = 8;
	        ColDataType(8) = 3;
	        ColDataType(9) = 3;
	    }

	    if (!(dso.recordset.BOF && dso.recordset.EOF)) {
	        dso.recordset.Filter = '';

	        // move o cursor do dso para o primeiro registro
	        dso.recordset.MoveFirst();
	    }

	    alignColsInGrid(grid, [2]);

	    if ((glb_gridsEvents.ncolUsedToSort_fgExtConcilia != -1) && (grid.Rows >= 3))
	        grid.Select(2, glb_gridsEvents.ncolUsedToSort_fgExtConcilia);

	    if (glb_gridsEvents.ncolOrderUsedToSort_fgExtConcilia == 1)
	        grid.Sort = 1;
	    else if (glb_gridsEvents.ncolOrderUsedToSort_fgExtConcilia == -1)
	        grid.Sort = 2;

	    // Linha de Totais
	    gridHasTotalLine(grid, 'Total', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(grid, 'Documento'), '######', 'C'],
                         [getColIndexByColKey(grid, 'Valor'), '###,###,###,##0.00', 'S']]);

	    if (grid.Rows >= 3) {
	        if (!((glb_gridControlFill.dso2_Ref.recordset.BOF) && (glb_gridControlFill.dso2_Ref.recordset.EOF))) {
	            glb_gridControlFill.dso2_Ref.recordset.MoveFirst();
	            glb_gridControlFill.dso2_Ref.recordset.Filter = 'Indice = 0';
	            glb_gridControlFill.dso2_Ref.recordset.Filter = '';
	            glb_gridControlFill.dso2_Ref.recordset.MoveFirst();

	        }

	      /*  if (glb_nSaldoExtrato != null)
	            grid.TextMatrix(1, getColIndexByColKey(grid, 'Valor')) = glb_nSaldoExtrato;
	        else
	            grid.TextMatrix(1, getColIndexByColKey(grid, 'Valor')) = 0;*/

	        grid.FillStyle = 1;
	        grid.TextMatrix(1, getColIndexByColKey(grid, 'Documento')) = grid.Rows - 2;
	        grid.Select(1, getColIndexByColKey(grid, 'Documento'), 1, getColIndexByColKey(grid, 'Documento'));

	        // Alinhamento da celula a direita
	        grid.CellAlignment = 7;

	        if (grid == fgExtConcilia) {
	            for (i = 1; i < grid.Rows; i++) {
	                nTipoID = parseInt(grid.ValueMatrix(i, getColIndexByColKey(grid, 'TipoID')), 10);

	                sColor = null;

	                // Amarelo
	                if (nTipoID == 1)
	                    sColor = 0X8CE6F0;
	                // Verde
	                else if (nTipoID == 2)
	                    sColor = 0X90EE90;
	                // Rosa
	                else if (nTipoID == 3)
	                    sColor = 0X7280FA;

	                if (sColor != null)
	                    grid.Cell(6, i, getColIndexByColKey(grid, 'Valor'), i, getColIndexByColKey(grid, 'Valor')) = sColor;
                    
	                grid.Select(i, getColIndexByColKey(grid, 'Valor'), i, getColIndexByColKey(grid, 'Valor'));
	                if (grid.ValueMatrix(i, getColIndexByColKey(grid, 'Valor')) < 0)
	                    grid.CellForeColor = 0X0000FF;
	                else
	                    grid.CellForeColor = 0;
	            }
	        }
	        
	    }

	    grid.FrozenCols = 4;
	    grid.MergeCells = 4;
	    grid.MergeCol(0) = true;
	    grid.MergeCol(1) = true;

	    grid.ExplorerBar = 5;

	    if (glb_gridsEvents.bTotalLine_fgExtConcilia) {
	        if (grid.Rows > 2)
	            grid.Row = 2;
	    }
	    else {
	        if (grid.Rows > 1)
	            grid.Row = 2;
	    }

	    if ((grid.Rows > 0) && grid.Row > 0) {
	        grid.TopRow = grid.Row;
	    }

	    grid.Editable = true;

	    adjustWidthCellsInGrid(grid);
	}
	// Reinicializa objeto que controla dsos e grids
	glb_gridControlFill.bFollowOrder = false;
	glb_gridControlFill.nDsosToArrive = 0;
	glb_gridControlFill.dso1_Ref = null;
	glb_gridControlFill.dso2_Ref = null;
	glb_gridControlFill.grid1_Ref = null;
	glb_gridControlFill.grid2_Ref = null;
	
	// Eventos dos grids - bloqueia/desbloqueia
	glb_gridsEvents.bBlockEvents_fgExtrato = false;
	glb_gridsEvents.bBlockEvents_fgExtConcilia = false;

    // Mostra janela se for o caso
    if (glb_FirstTime)
    {
		glb_FirstTime = false;
		// A janela esta carregando
		glb_gridControlFill.bFollowOrder = false;
		glb_gridControlFill.nDsosToArrive = 2;
		glb_gridControlFill.dso1_Ref = dsoGridExtrato;
		glb_gridControlFill.dso2_Ref = dsoGridExtConcilia;
		glb_gridControlFill.grid1_Ref = fgExtrato;
		glb_gridControlFill.grid2_Ref = fgExtConcilia;    
		adjustLabelsCombos();
		showModalAfterDataLoad();
	}
	else
		lockControlsInModalWin(false);    
	
	if (fgExtrato.Rows > 2)
	{
		try
		{
          fgExtrato. focus();
        }
        catch (e)
        {
            ;
        }
		
		if (bPreencheuExtrato)
			glb_TimerCallRowColChange = window.setInterval('callAfterRowColChange()', 500, 'JavaScript');  
	}

	controlInterfaceState();
}

function callAfterRowColChange()
{
    if (glb_TimerCallRowColChange != null )
    {
        window.clearInterval(glb_TimerCallRowColChange);
        glb_TimerCallRowColChange = null;
    }
	
	glb_gridsEvents.bBlockEvents_fgExtrato = false;
	js_fg_ModConcExtrAfterRowColChangePesqList(fgExtrato, fgExtrato.Row - 1, fgExtrato.Col, fgExtrato.Row, fgExtrato.Col);
}

/********************************************************************
Ajusta largura de colunas dos grid
********************************************************************/
function adjustWidthCellsInGrid(grid)
{
	var nCurrRow = 0;
	var sLabel1;
	
	sLabel1 = grid.TextMatrix(0, getColIndexByColKey(grid, 'Valor'));
		
	if (grid.Row > 0)
		nCurrRow = grid.Row;
		
    grid.Redraw = 0;
    
	grid.TextMatrix(0, getColIndexByColKey(grid, 'Valor')) = '9999999';
	    
    grid.AutoSizeMode = 0;
    grid.AutoSize(0,grid.Cols-1);

	grid.TextMatrix(0, getColIndexByColKey(grid, 'Valor')) = sLabel1;
	
    grid.Redraw = 2;
}

/********************************************************************
Preenche os grids e mostra a janela
********************************************************************/
function geraValorLocalizar()
{
    ;
}
/********************************************************************
Funcao do programador
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function execSPValorLocalizar_Gerador()
{
    ;
}

/********************************************************************
Funcao do programador
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function execSPValorLocalizar_Gerador_DSC()
{

    ;
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modalassocialancamentoconciliaBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
    
    interfaceControlsState();
}

/********************************************************************
Inverte o check de um checkbox associado a um label
********************************************************************/
function invertChkBox(ctl)
{
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    return true;
}    

/********************************************************************
Altera o estado dos controles da interface
********************************************************************/
function interfaceControlsState()
{
    ;
}

/********************************************************************
Evento onchange dos combos
********************************************************************/
function cmbOnChange(cmb)
{
    ;
}

/********************************************************************
Onclick dos checkbox
********************************************************************/
function chkOnClick()
{
	if (((this.id).toUpperCase() == 'CHKLANCAMENTOSPENDENTES') )
	{
			// A janela esta carregando
			glb_gridControlFill.bFollowOrder = false;
			glb_gridControlFill.nDsosToArrive = 2;
			glb_gridControlFill.dso1_Ref = dsoGridExtrato;
			glb_gridControlFill.dso2_Ref = dsoGridExtConcilia;
			glb_gridControlFill.grid1_Ref = fgExtrato;
			glb_gridControlFill.grid2_Ref = fgExtConcilia;    
	    
			startDynamicGrids();
	}
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function js_fg_ModConcExtrBeforeRowColChange(grid, oldRow, oldCol, newRow, newCol)
{
    ;
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function js_fg_ModConcExtrAfterRowColChangePesqList(grid, oldRow, oldCol, newRow, newCol)
{
    ;
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function fg_ModConcExtrBeforeSort(grid, col)
{
    ;
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function fg_ModConcExtrAfterSort(grid, col)
{
    var i, keyToSelectRow, bBlockEvents;
    
    keyToSelectRow = '';
    gridSortParams(grid, col);
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function js_fg_ModConcExtrDblClick(grid, row, col)
{
    ;
}
/********************************************************************
Examina sort order dos grids e guarda parametros
********************************************************************/
function gridSortParams(grid, col)
{
	// nColDataType = 0 (String), 1 (Numerico), 2 (Data), 3 (Boolean)
	
	var i_init;
	
	var dataFirstRow = 0;
	var dataLastRow = 0;
	var dataDif = 0;
	var dataType = 0;
	var ncolOrderUsedToSort = -1;

	if ( grid == fgExtrato )
	{
		if ( glb_gridsEvents.bTotalLine_fgExtrato )
			i_init = 2;
		else
			i_init = 1;
			
		glb_gridsEvents.ncolUsedToSort_fgExtrato = col;	
		
		// inicialmente considera sempre sort ascendente
		glb_gridsEvents.ncolOrderUsedToSort_fgExtrato = 1;
			
	}		

	if ( grid == fgExtConcilia )
	{
		if ( glb_gridsEvents.bTotalLine_fgExtConcilia )
			i_init = 2;
		else
			i_init = 1;
			
		glb_gridsEvents.ncolUsedToSort_fgExtConcilia = col;	
		
		// inicialmente considera sempre sort ascendente
		glb_gridsEvents.ncolOrderUsedToSort_fgExtConcilia = 1;
	}
	
	dataType = grid.ColDataType(col);		
	
	if ( grid.Rows > i_init )
	{
		// data ou string
		if ( (dataType == 7) ||
		     (dataType == 8) || 
		     (dataType == 30) || 
		     (dataType == 31) )
		{
			dataFirstRow = '';
			dataLastRow = '';
			dataFirstRow = grid.TextMatrix(i_init, col);
			dataLastRow = grid.TextMatrix((grid.Rows - 1), col);
		}
		// numericos
		else
		{
			dataFirstRow = 0;
			dataLastRow = 0;
			dataFirstRow = grid.ValueMatrix(i_init, col);
			dataLastRow = grid.ValueMatrix((grid.Rows - 1), col);
		}
	}

	if ( grid.Rows > (i_init + 1) )
	{
		// String 8, 30, 31
		if ( (dataType == 8) || (dataType == 30) || (dataType == 31) )
		{
			if ( dataFirstRow == null )
				dataFirstRow = '';
			if ( dataLastRow == null )
				dataLastRow = '';	
				
			if ( (dataFirstRow == '') && (dataLastRow == '') )
				ncolOrderUsedToSort = 1;
			else if ( (dataFirstRow != '') && (dataLastRow == '') )
				ncolOrderUsedToSort = -1;
			else if ( (dataFirstRow == '') && (dataLastRow != '') )
				ncolOrderUsedToSort = 1;	
			else
			{
				if ( dataFirstRow < dataLastRow )
					ncolOrderUsedToSort = 1;	
				else
					ncolOrderUsedToSort = -1;	
			}
		}
		// Numero 2, 3, 4, 5, 6, 14, 20
		else if ( (dataType == 2) || (dataType == 3) ||
		          (dataType == 4) || (dataType == 5) ||
		          (dataType == 6) || (dataType == 14) || 
		          (dataType == 20) )
		{
			if ( dataFirstRow == null )
				dataFirstRow = 0;
			if ( dataLastRow == null )
				dataLastRow = 0;	

			dataDif = dataLastRow - dataFirstRow;
					
			if ( dataDif >= 0 )
				ncolOrderUsedToSort = 1;	
			else
				ncolOrderUsedToSort = -1;	
		}
		// Data 7
		else if ( dataType == 7 )
		{
			if ( dataFirstRow == null )
				dataFirstRow = '';
			if ( dataLastRow == null )
				dataLastRow = '';	
				
			if ( (dataFirstRow == '') && (dataLastRow == '') )
				ncolOrderUsedToSort = 1;
			else if ( (dataFirstRow != '') && (dataLastRow == '') )
				ncolOrderUsedToSort = -1;
			else if ( (dataFirstRow == '') && (dataLastRow != '') )
				ncolOrderUsedToSort = 1;	
			else
			{
				dataDif = window.top.daysBetween(dataFirstRow, dataLastRow);
					
				if ( dataDif >= 0 )
					ncolOrderUsedToSort = 1;	
				else
					ncolOrderUsedToSort = -1;	
			}
		}
	}

	if ( grid == fgExtrato )
		glb_gridsEvents.ncolOrderUsedToSort_fgExtrato = ncolOrderUsedToSort;
	else if ( grid == fgExtConcilia )	
		glb_gridsEvents.ncolOrderUsedToSort_fgExtConcilia = ncolOrderUsedToSort;
		
}

/********************************************************************
Usuario mudou a linha do grid de extratos
********************************************************************/
function lineChangedInGrid_Extrato(grid, nRow) {
    controlInterfaceState();   
}

/********************************************************************
Usuario mudou a linha do grid de lancamento de contas
********************************************************************/
function lineChangedInGrid_ValoresLocalizar(grid, nRow)
{
    controlInterfaceState();
}

function adjustLabelsCombos(bPaging)
{
    ;
}

function fgExtConcilia_AfterEdit(grid, row, col)
{
    ;
}



/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos combos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs() {
    ;
}

/********************************************************************
Funcao disparada pelo programado.
Preenchimento dos combos
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    ;
}

function fgExtrato_AfterEdit(grid, row, col) {
    ;
}


function fgExtrato_ValidateEdit(grid, Row, Col) {
    ;
}


function fgExtrato_AfterSelChange(grid, oldRow, oldCol, newRow, newCol) {

    if (!glb_FirstTime && fgExtConcilia.Rows > 1) {
        var nLinha;
        var nExtLancamentoConciliaID_NC = fgExtrato.ValueMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'ExtLancamentoConciliaID'));

        for (i = 1; i < fgExtConcilia.Rows; i++) {
            var nExtLancamentoID_C = fgExtConcilia.ValueMatrix(i, getColIndexByColKey(fgExtConcilia, 'ExtLancamentoID'));

            if (nExtLancamentoID_C == nExtLancamentoConciliaID_NC)
                nLinha = i;
        }

        fgExtConcilia.Row = nLinha;
        fgExtConcilia.TopRow = nLinha;
        try
        {
            fgExtConcilia.focus();
        }
        catch (e)
        {
            ;
        }
            
    }
}

function js_fg_EnterCell(grid) {
    ;

}

function js_fg_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol)
{
    //js_fg_AfterRowColChange fgExtrato, OldRow, OldCol, NewRow, NewCol
    if (!glb_FirstTime)
    {
        controlInterfaceState();

        if (fg.Rows > 2) {
            if (NewRow == 1) {
                fg.Row = 2; 
                fg.TopRow = 2;
                try 
                {
                    fg.focus;
                }
                catch (e)
                {
                    ;
                }
            }
        }
     }   
}


/********************************************************************
Funcao do programador
Ida ao servidor para associar/dissociar lançamentos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function Associa_Lancamentos() {
    lockControlsInModalWin(true);

    for (i = 1; i < fgExtConcilia.Rows; i++) {
        if (fgExtConcilia.ValueMatrix(i, getColIndexByColKey(fgExtConcilia, 'OK')) != 0) {
            AssociarLinha = i;
        }
    }        
    
    var AssociarLinha;
    var strPars = new String();
    var nExtLancamentoID_NC = fgExtrato.ValueMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'ExtLancamentoID'));
    var nExtLancamentoID_C = fgExtConcilia.ValueMatrix(AssociarLinha, getColIndexByColKey(fgExtConcilia, 'ExtLancamentoID'));

    strPars += '?nExtLancamentoID_NC=' + escape(nExtLancamentoID_NC);
    strPars += '&nExtLancamentoID_C=' + escape(nExtLancamentoID_C);
    
        dsoAssociarLancamento.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/associarlancamento.aspx' + strPars;
        dsoAssociarLancamento.ondatasetcomplete = Associa_Lancamentos_DSC;
        dsoAssociarLancamento.refresh();
}

/********************************************************************
Funcao do programador
Retorno do servidor para associar/dissociar lançamentos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function Associa_Lancamentos_DSC() {
    lockControlsInModalWin(false);
    var nValorID, sResultado;

    if (!(dsoAssociarLancamento.recordset.BOF && dsoAssociarLancamento.recordset.EOF)) {
        sResultado = dsoAssociarLancamento.recordset['Resultado'].value;

        if ((sResultado != null) && (sResultado != '')) {
            if (window.top.overflyGen.Alert(sResultado) == 0)
                return null;
        }
        else {
            window.top.overflyGen.Alert('Lançamentos associados');

            fgExtrato.Rows = 1;
            fgExtConcilia.Rows = 1;

        }

        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 2;
        glb_gridControlFill.dso1_Ref = dsoGridExtrato;
        glb_gridControlFill.dso2_Ref = dsoGridExtConcilia;
        glb_gridControlFill.grid1_Ref = fgExtrato;
        glb_gridControlFill.grid2_Ref = fgExtConcilia;
        startDynamicGrids();
    }
}

/********************************************************************
Funcao do programador
Ida ao servidor para associar/dissociar lançamentos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function Dissocia_Lancamentos() {
    lockControlsInModalWin(true);

    var strPars = new String();
    var nExtLancamentoID_NC = fgExtrato.ValueMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'ExtLancamentoID'));

    strPars += '?nExtLancamentoID_NC=' + escape(nExtLancamentoID_NC);

    dsoDissociarLancamento.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/dissociarlancamento.aspx' + strPars;
    dsoDissociarLancamento.ondatasetcomplete = Dissocia_Lancamentos_DSC;
    dsoDissociarLancamento.refresh();
}

/********************************************************************
Funcao do programador
Retorno do servidor para associar/dissociar lançamentos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function Dissocia_Lancamentos_DSC() {
    lockControlsInModalWin(false);
    var nValorID, sResultado;

    if (!(dsoDissociarLancamento.recordset.BOF && dsoDissociarLancamento.recordset.EOF)) {
        sResultado = dsoDissociarLancamento.recordset['Resultado'].value;

        if ((sResultado != null) && (sResultado != '')) {
            if (window.top.overflyGen.Alert(sResultado) == 0)
                return null;
        }
        else {
            window.top.overflyGen.Alert('Lançamento dissociado');

            fgExtrato.Rows = 1;
            fgExtConcilia.Rows = 1;

        }

        lockControlsInModalWin(false);

        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 2;
        glb_gridControlFill.dso1_Ref = dsoGridExtrato;
        glb_gridControlFill.dso2_Ref = dsoGridExtConcilia;
        glb_gridControlFill.grid1_Ref = fgExtrato;
        glb_gridControlFill.grid2_Ref = fgExtConcilia;
        startDynamicGrids();
    }
}

/********************************************************************
Funcao do programador
Ida ao servidor para excluit lançamento de extrato concilia
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function Exclui_Lancamentos() {
    lockControlsInModalWin(true);

    var strPars = new String();
    var nExtLancamentoID_C;
    
    for (i = 1; i < fgExtConcilia.Rows; i++) {
        if (fgExtConcilia.ValueMatrix(i, getColIndexByColKey(fgExtConcilia, 'OK')) != 0) {
           nExtLancamentoID_C = fgExtConcilia.ValueMatrix(i, getColIndexByColKey(fgExtConcilia,'ExtLancamentoID'));
        }
    }

    if ((txtLog.value == null) || (txtLog.value == '')) {
        window.top.overflyGen.Alert('Insira o motivo de exclusão.');
        
        lockControlsInModalWin(false);
        return null;
    }
    else {
        strPars += '?nExtLancamentoID_C=' + escape(nExtLancamentoID_C);
        strPars += '&nUsuarioID=' + escape(getCurrUserID());
        strPars += '&sLogMotivo=' + txtLog.value;

        dsoExcluirLancamento.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/excluirlancamento.aspx' + strPars;
        dsoExcluirLancamento.ondatasetcomplete = Exclui_Lancamentos_DSC;
        dsoExcluirLancamento.refresh();
    }
}

/********************************************************************
Funcao do programador
Retorno do servidor para associar/dissociar lançamentos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function Exclui_Lancamentos_DSC() {

    lockControlsInModalWin(false);
    var nValorID, sResultado;

    if (!(dsoExcluirLancamento.recordset.BOF && dsoExcluirLancamento.recordset.EOF)) {
        sResultado = dsoExcluirLancamento.recordset['Resultado'].value;

        if ((sResultado != null) && (sResultado != '')) {
            if (window.top.overflyGen.Alert(sResultado) == 0)
                return null;
        }
        else {
            window.top.overflyGen.Alert('Lançamento excluído');

            fgExtrato.Rows = 1;
            fgExtConcilia.Rows = 1;
        }

        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 2;
        glb_gridControlFill.dso1_Ref = dsoGridExtrato;
        glb_gridControlFill.dso2_Ref = dsoGridExtConcilia;
        glb_gridControlFill.grid1_Ref = fgExtrato;
        glb_gridControlFill.grid2_Ref = fgExtConcilia;
        startDynamicGrids();
    }

}

/********************************************************************
Funcao do programador
Ida ao servidor para incluir lançamento 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function Inclui_Lancamentos() {
    lockControlsInModalWin(true);

    var strPars = new String();
    var nExtLancamentoID_NC = fgExtrato.ValueMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'ExtLancamentoID'));
    //var nExtrato_C = fgExtConcilia.ValueMatrix(fgExtConcilia.Row, getColIndexByColKey(fgExtConcilia, 'ExtratoID'))

    strPars += '?nExtLancamentoID_NC=' + escape(nExtLancamentoID_NC);
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    dsoIncluirLancamento.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/incluirlancamento.aspx' + strPars;
    dsoIncluirLancamento.ondatasetcomplete = Inclui_Lancamentos_DSC;
    dsoIncluirLancamento.refresh();
}

/********************************************************************
Funcao do programador
Retorno do servidor para associar/dissociar lançamentos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function Inclui_Lancamentos_DSC() {
    lockControlsInModalWin(false);
    var nValorID, sResultado;

    if (!(dsoIncluirLancamento.recordset.BOF && dsoIncluirLancamento.recordset.EOF)) {
        sResultado = dsoIncluirLancamento.recordset['Resultado'].value;

        if ((sResultado != null) && (sResultado != '')) {
            if (window.top.overflyGen.Alert(sResultado) == 0)
                return null;
        }
        else {
            window.top.overflyGen.Alert('Lançamento incluído');

            fgExtrato.Rows = 1;
            fgExtConcilia.Rows = 1;
        }

        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 2;
        glb_gridControlFill.dso1_Ref = dsoGridExtrato;
        glb_gridControlFill.dso2_Ref = dsoGridExtConcilia;
        glb_gridControlFill.grid1_Ref = fgExtrato;
        glb_gridControlFill.grid2_Ref = fgExtConcilia;
        startDynamicGrids();
    }
}

/********************************************************************
Funcao do programador
Ida ao servidor para associar lançamentos automaticamente
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function AssociacaoAutomatica() {
    lockControlsInModalWin(true);

    var strPars = new String();

    strPars += '?nExtratoID=' + escape(glb_nExtratoID);
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    dsoAssociacaoAutomatica.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/associarautomaticolancamentoconcilia.aspx' + strPars;
    dsoAssociacaoAutomatica.ondatasetcomplete = AssociacaoAutomatica_DSC;
    dsoAssociacaoAutomatica.refresh();
}

/********************************************************************
Funcao do programador
Retorno do servidor para associar lançamentos automaticamente
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function AssociacaoAutomatica_DSC() {
    lockControlsInModalWin(false);
    var nValorID, sResultado;

    if (!(dsoAssociacaoAutomatica.recordset.BOF && dsoAssociacaoAutomatica.recordset.EOF)) {
        sResultado = dsoAssociacaoAutomatica.recordset['Resultado'].value;

        fgExtrato.Rows = 1;
        fgExtConcilia.Rows = 1;

        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 2;
        glb_gridControlFill.dso1_Ref = dsoGridExtrato;
        glb_gridControlFill.dso2_Ref = dsoGridExtConcilia;
        glb_gridControlFill.grid1_Ref = fgExtrato;
        glb_gridControlFill.grid2_Ref = fgExtConcilia;
        startDynamicGrids();
    }
}

/********************************************************************
Funcao do programador
Retorno do servidor para associar/dissociar lançamentos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function controlInterfaceState() {
    var nExcluir = 0;
    var nAssociar_Dissociar = 0;
    var Associar_Dissociar;
    for (i = 1; i < fgExtConcilia.Rows; i++) {
        if (fgExtConcilia.ValueMatrix(i, getColIndexByColKey(fgExtConcilia, 'OK')) != 0) {
            nExcluir++;
        }
    }

    if (nExcluir == 1) {
        txtLog.disabled = false;
        btnExcluir.disabled = false;
    }
    else {
        txtLog.disabled = true;
        btnExcluir.disabled = true;
    }

    for (i = 1; i < fgExtConcilia.Rows; i++) {
        if (fgExtConcilia.ValueMatrix(i, getColIndexByColKey(fgExtConcilia, 'OK')) != 0) {
            nAssociar_Dissociar++;
            Associar_Dissociar = i;
        }
    }

    //Associar_Dissociar
    if (fgExtrato.Rows > 2 && fgExtConcilia.Rows > 2) {

        var nExtLancamentoConciliaID_NC = fgExtrato.ValueMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'ExtLancamentoConciliaID'));
        var nValor_NC = fgExtrato.ValueMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'Valor'));
        var nValor_C = fgExtConcilia.ValueMatrix(Associar_Dissociar, getColIndexByColKey(fgExtConcilia, 'Valor'));

        if (nExtLancamentoConciliaID_NC != null && nExtLancamentoConciliaID_NC != '' && nExtLancamentoConciliaID_NC != 0) {
                btnAssociar.value = 'Dissociar';
                btnAssociar.style.visibility = 'inherit';
        }
        else if (nAssociar_Dissociar == 1) {
            btnAssociar.value = 'Associar';

            if (nValor_C == nValor_NC)
                btnAssociar.style.visibility = 'inherit';
            else
                btnAssociar.style.visibility = 'hidden';
        }
        else
            btnAssociar.style.visibility = 'hidden';
    }
    else if (fgExtrato.Rows > 2) {
        var nExtLancamentoConciliaID_NC = fgExtrato.ValueMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'ExtLancamentoConciliaID'));
        if (nExtLancamentoConciliaID_NC != null && nExtLancamentoConciliaID_NC != '' && nExtLancamentoConciliaID_NC != 0) {
            btnAssociar.value = 'Dissociar';
            btnAssociar.style.visibility = 'inherit';
        }
        else
            btnAssociar.style.visibility = 'hidden';
    }
    else if (fgExtConcilia.Rows > 2)
        btnAssociar.style.visibility = 'hidden';

    txtLog.value = '';    
        
}




function fgValoresLocalizar_AfterEdit(fg, row, col) {
    txtLog.value = '';
    controlInterfaceState();
}

function GetDateSQL() {
    var dtData = putDateInMMDDYYYY(trimStr(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'V_dtInicial' + '\'' + '].value')));
    var dtData_dt = new Date(dtData);
    var iYear = dtData_dt.getYear();
    var iMonth = dtData_dt.getMonth() + 1;
    var iDate = dtData_dt.getDate();
    glb_FinalDate1 = iYear.toString() + '/' + iMonth.toString() + '/' + iDate.toString();

    dtData = putDateInMMDDYYYY(trimStr(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'V_dtFinal' + '\'' + '].value')));
    dtData_dt = new Date(dtData);
    iYear = dtData_dt.getYear();
    iMonth = dtData_dt.getMonth() + 1;
    iDate = dtData_dt.getDate();
    glb_FinalDate2 = iYear.toString() + '/' + iMonth.toString() + '/' + iDate.toString();
}