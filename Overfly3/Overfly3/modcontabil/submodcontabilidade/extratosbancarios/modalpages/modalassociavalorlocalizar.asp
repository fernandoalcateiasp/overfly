
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalassociavalorlocalizarHtml" name="modalassociavalorlocalizarHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
        
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/extratosbancarios/modalpages/modalassociavalorlocalizar.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais
Dim i, sCaller, nEmpresaID, nExtratoID, sBanco, nRelPesContaID, nTipoExtratoID, bConcilia

sCaller = ""
nEmpresaID = 0
nExtratoID = 0
sBanco = ""
nRelPesContaID = 0
nTipoExtratoID = 0
bConcilia = false

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nExtratoID").Count    
    nExtratoID = Request.QueryString("nExtratoID")(i)
Next

For i = 1 To Request.QueryString("sBanco").Count    
    sBanco = Request.QueryString("sBanco")(i)
Next

For i = 1 To Request.QueryString("nRelPesContaID").Count    
    nRelPesContaID = Request.QueryString("nRelPesContaID")(i)
Next

For i = 1 To Request.QueryString("nTipoExtratoID").Count    
    nTipoExtratoID = Request.QueryString("nTipoExtratoID")(i)
Next

For i = 1 To Request.QueryString("bConcilia").Count    
    bConcilia = Request.QueryString("bConcilia")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " &  nEmpresaID & ";"
Response.Write vbcrlf

Response.Write "var glb_nExtratoID = " &  nExtratoID & ";"
Response.Write vbcrlf

Response.Write "var glb_sBanco = " & Chr(39) & sBanco & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nRelPesContaID = " &  nRelPesContaID & ";"
Response.Write vbcrlf

Response.Write "var glb_nTipoExtratoID = " &  nTipoExtratoID & ";"
Response.Write vbcrlf

Response.Write "var glb_bConcilia = " &  bConcilia & ";"
Response.Write vbcrlf

'Necessario para compatibilidade da automacao
Response.Write "var glb_USERID = 0;"

Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

    //-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fgExtrato -->
<script LANGUAGE=javascript FOR=fgExtrato EVENT=BeforeSort>
<!--
    fg_ModConcExtrBeforeSort(fgExtrato, arguments[0]);
    //-->
</script>

<script LANGUAGE=javascript FOR=fgExtrato EVENT=AfterSelChange>
<!--
    fgExtrato_AfterSelChange(fgExtrato, arguments[0], arguments[1], arguments[2], arguments[3]);
    //-->
</script>

<script LANGUAGE=javascript FOR=fgExtrato EVENT=AfterSort>
<!--
    fg_ModConcExtrAfterSort(fgExtrato, arguments[0]);
    //-->
</script>

<SCRIPT LANGUAGE=javascript FOR=fgExtrato EVENT=ValidateEdit>
<!--
    fgExtrato_ValidateEdit(fgExtrato, arguments[0], arguments[1])
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgExtrato EVENT=AfterRowColChange>
<!--
    glb_HasGridButIsNotFormPage = true;
    //js_fg_AfterRowColChange fgExtrato, OldRow, OldCol, NewRow, NewCol
    js_fg_AfterRowColChange(fgExtrato, arguments[0], arguments[1], arguments[2], arguments[3]);
    treatCheckBoxReadOnly2(dsoGridExtrato, fgExtrato, arguments[0], arguments[1], arguments[2], arguments[3]);
    js_fg_ModConcExtrAfterRowColChangePesqList(fgExtrato, arguments[0], arguments[1], arguments[2], arguments[3]);
    //-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fgExtrato EVENT=BeforeRowColChange>
<!--
    glb_HasGridButIsNotFormPage = true;
    js_fg_BeforeRowColChange(fgExtrato, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
    js_fg_ModConcExtrBeforeRowColChange(fgExtrato, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgExtrato EVENT=DblClick>
<!--
    js_fg_ModConcExtrDblClick(fgExtrato, fgExtrato.Row, fgExtrato.Col);
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgExtrato EVENT=EnterCell>
<!--
    js_fg_EnterCell(fgExtrato);
    //-->
</SCRIPT>

<!-- fgValoresLocalizar -->
<script LANGUAGE=javascript FOR=fgValoresLocalizar EVENT=BeforeSort>
<!--
    fg_ModConcExtrBeforeSort(fgValoresLocalizar, arguments[0]);
    //-->
</script>

<script LANGUAGE=javascript FOR=fgValoresLocalizar EVENT=AfterSort>
<!--
    fg_ModConcExtrAfterSort(fgValoresLocalizar, arguments[0]);
    //-->
</script>
<SCRIPT LANGUAGE=javascript FOR=fgValoresLocalizar EVENT=AfterRowColChange>
<!--
    glb_HasGridButIsNotFormPage = true;
    //glb_FreezeRolColChangeEvents = False
    js_fg_AfterRowColChange(fgValoresLocalizar, arguments[0], arguments[1], arguments[2], arguments[3]);
    js_fg_ModConcExtrAfterRowColChangePesqList(fgValoresLocalizar, arguments[0], arguments[1], arguments[2], arguments[3]);
    //-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fgValoresLocalizar EVENT=BeforeRowColChange>
<!--
    glb_HasGridButIsNotFormPage = true;
    //glb_FreezeRolColChangeEvents = False
    js_fg_BeforeRowColChange(fgValoresLocalizar, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
    js_fg_ModConcExtrBeforeRowColChange(fgValoresLocalizar, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgValoresLocalizar EVENT=DblClick>
<!--
    js_fg_ModConcExtrDblClick(fgValoresLocalizar, fgValoresLocalizar.Row, fgValoresLocalizar.Col);
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgValoresLocalizar EVENT=AfterEdit>
<!--
    fgValoresLocalizar_AfterEdit(fgValoresLocalizar, fgValoresLocalizar.Row, fgValoresLocalizar.Col)
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgValoresLocalizar EVENT=EnterCell>
<!--
    js_fg_EnterCell(fgValoresLocalizar);
    //-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgExtrato EVENT=AfterEdit>
<!--
    fgExtrato_AfterEdit(fgExtrato, fgExtrato.Row, fgExtrato.Col)
    //-->
</SCRIPT>
</head>

<body id="modalassociavalorlocalizarBody" name="modalassociavalorlocalizarBody" LANGUAGE="javascript" onload="return window_onload()">
	
	<div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

	<div id="divGridExtrato" name="divGridExtrato" class="divGeneral">
		<p id="lblGridExtrato" name="lblGridExtrato" class="lblGeneral">Extrato Banc�rio</p>
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgExtrato" name="fgExtrato" class="fldGeneral" VIEWASTEXT>
        </object>
    </div>    
    
    <div id="divGridValoresLocalizar" name="divGridValoresLocalizar" class="divGeneral">
		<p id="lblGridValoresLocalizar" name="lblGridValoresLocalizar" class="lblGeneral">Valores a Localizar</p>
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgValoresLocalizar" name="fgValoresLocalizar" class="fldGeneral" VIEWASTEXT>
        </object>
    </div>    
    
    <div id="divControles" name="divControles" class="divGeneral">
		<p id="lblLancamentosPendentes" name="lblLancamentosPendentes" class="lblGeneral" LANGUAGE=javascript onclick="return invertChkBox(chkLancamentosPendentes)" title="S� lan�amentos pendentes?">LP</p>
		<input type="checkbox" id="chkLancamentosPendentes" name="chkLancamentosPendentes" class="fldGeneral" title="S� lan�amentos pendentes?"></input>
		<p id="lblLancamentosSemelhantes" name="lblLancamentosSemelhantes" class="lblGeneral" LANGUAGE=javascript onclick="return invertChkBox(chkLancamentosSemelhantes)" title="S� lan�amentos semelhantes?">LS</p>
		<input type="checkbox" id="chkLancamentosSemelhantes" name="chkLancamentosSemelhantes" class="fldGeneral" title="S� lan�amentos semelhantes?"></input>
		<p id="lblTipoLancamento" name="lblTipoLancamento" class="lblGeneral">Tipo Lan�amento</p>
		<select id="selTipoLancamento" name="selTipoLancamento" class="fldGeneral"  onchange="return cmbOnChange(this)">
<%

    Dim strSQL, rsData

    Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName " & _
			 "UNION ALL SELECT 1 AS Indice, ItemID AS fldID, ItemMasculino AS fldName " & _
			 "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
			 "WHERE TipoID = 831" & _
			 "ORDER BY fldID"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then
    
		While Not (rsData.EOF)
    
		    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )
  
		rsData.MoveNext()
  
		WEnd
 
 	End If

	rsData.Close
	Set rsData = Nothing
%>
		</select>
		<input type="button" id="btnAutomatico" name="btnAutomatico" value="Autom�tico" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Conciliar automaticamente os lan�amentos">
		<input type="button" id="btnAssociar" name="btnAssociar" value="Associar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Associar lan�amento"></input>
		<input type="button" id="btnLimpar" name="btnLimpar" value="Dissociar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Dissociar lan�amento"></input>
		<p  id="lblEhEstorno" name="lblEhEstorno" class="lblGeneral">Est</p>
		<input type="checkbox" id="chkEhEstorno" name="chkEhEstorno" class="fldGeneral" title="� Estorno?"></input>
		<p  id="lblUsoCotidiano" name="lblUsoCotidiano" class="lblGeneral">UC</p>
        <input type="checkbox" id="chkUsoCotidiano" name="chkUsoCotidiano" class="fldGeneral" title="Somente HP de uso cotidiano ou todos dispon�veis?" onclick="return chkOnClick(this)"></input>
		<p id="lblHistoricoPadraoID" name="lblHistoricoPadraoID" class="lblGeneral">Hist�rico Padr�o</p>
		<select id="selHistoricoPadraoID" name="selHistoricoPadraoID" class="fldGeneral"  onchange="return cmbOnChange(this)"></select>
		<p id="lblProcessoID" name="lblProcessoID" class="lblGeneral">Processo</p>
		<select id="selProcessoID" name="selProcessoID" class="fldGeneral" LANGUAGE="javascript" onchange="return cmbOnChange(this)">
<%

	Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName " & _
			 "UNION ALL SELECT 1 AS Indice, ItemID AS fldID, ItemMasculino AS fldName " & _
			 "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
			 "WHERE TipoID = 821 AND EstadoID = 2 " & _
			 "ORDER BY fldID"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
	Set rsData = Nothing
%>
		</select>
		<input type="button" id="btnGerarValorLocalizar" name="btnGerarValorLocalizar" value="Gerar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Gerar Valor a Localizar">
		<input type="button" id="btnLimparEx" name="btnLimparEx" value="Limpar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Limpar identificadores"></input>
        <input type="button" id="btnTransferir" name="btnTransferir" value="Transferir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Transferir da conta vinculada"></input>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral" LANGUAGE="javascript">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" class="fldGeneral" LANGUAGE="javascript"></input>
    	</div>	
	<input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
	<input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
</body>

</html>
