
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalassocialancamentoconciliaHtml" name="modalassocialancamentoconciliaHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/extratosbancarios/modalpages/modalassocialancamentoconcilia.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais
Dim i, sCaller, nEmpresaID, nExtratoID, sBanco, nRelPesContaID, nTipoExtratoID

sCaller = ""
nEmpresaID = 0
nExtratoID = 0
sBanco = ""
nRelPesContaID = 0
nTipoExtratoID = 0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nExtratoID").Count    
    nExtratoID = Request.QueryString("nExtratoID")(i)
Next

For i = 1 To Request.QueryString("sBanco").Count    
    sBanco = Request.QueryString("sBanco")(i)
Next

For i = 1 To Request.QueryString("nRelPesContaID").Count    
    nRelPesContaID = Request.QueryString("nRelPesContaID")(i)
Next

For i = 1 To Request.QueryString("nTipoExtratoID").Count    
    nTipoExtratoID = Request.QueryString("nTipoExtratoID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " &  nEmpresaID & ";"
Response.Write vbcrlf

Response.Write "var glb_nExtratoID = " &  nExtratoID & ";"
Response.Write vbcrlf

Response.Write "var glb_sBanco = " & Chr(39) & sBanco & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nRelPesContaID = " &  nRelPesContaID & ";"
Response.Write vbcrlf

Response.Write "var glb_nTipoExtratoID = " &  nTipoExtratoID & ";"
Response.Write vbcrlf

'Necessario para compatibilidade da automacao
Response.Write "var glb_USERID = 0;"

Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fgExtrato -->
<script LANGUAGE=javascript FOR=fgExtrato EVENT=BeforeSort>
<!--
fg_ModConcExtrBeforeSort(fgExtrato, arguments[0]);
//-->
</script>

<script LANGUAGE=javascript FOR=fgExtrato EVENT=AfterSelChange>
<!--
fgExtrato_AfterSelChange(fgExtrato, arguments[0],arguments[1],arguments[2],arguments[3]);
//-->
</script>

<script LANGUAGE=javascript FOR=fgExtrato EVENT=AfterSort>
<!--
fg_ModConcExtrAfterSort(fgExtrato, arguments[0]);
//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=fgExtrato EVENT=ValidateEdit>
<!--
 fgExtrato_ValidateEdit(fgExtrato,arguments[0], arguments[1])
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgExtrato EVENT=AfterRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
//js_fg_AfterRowColChange fgExtrato, OldRow, OldCol, NewRow, NewCol
js_fg_AfterRowColChange (fgExtrato, arguments[0], arguments[1], arguments[2], arguments[3]);
treatCheckBoxReadOnly2 (dsoGridExtrato, fgExtrato, arguments[0], arguments[1], arguments[2], arguments[3]);
js_fg_ModConcExtrAfterRowColChangePesqList (fgExtrato, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fgExtrato EVENT=BeforeRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
js_fg_BeforeRowColChange (fgExtrato, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
js_fg_ModConcExtrBeforeRowColChange (fgExtrato, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgExtrato EVENT=DblClick>
<!--
js_fg_ModConcExtrDblClick (fgExtrato, fgExtrato.Row, fgExtrato.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgExtrato EVENT=EnterCell>
<!--
js_fg_EnterCell (fgExtrato);
//-->
</SCRIPT>

<!-- fgValoresLocalizar -->
<script LANGUAGE=javascript FOR=fgExtConcilia EVENT=BeforeSort>
<!--
fg_ModConcExtrBeforeSort(fgExtConcilia, arguments[0]);
//-->
</script>

<script LANGUAGE=javascript FOR=fgExtConcilia EVENT=AfterSort>
<!--
fg_ModConcExtrAfterSort(fgExtConcilia, arguments[0]);
//-->
</script>
<SCRIPT LANGUAGE=javascript FOR=fgExtConcilia EVENT=AfterRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
//glb_FreezeRolColChangeEvents = False
js_fg_AfterRowColChange (fgExtConcilia, arguments[0], arguments[1], arguments[2], arguments[3]);
js_fg_ModConcExtrAfterRowColChangePesqList (fgExtConcilia, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fgExtConcilia EVENT=BeforeRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
//glb_FreezeRolColChangeEvents = False
js_fg_BeforeRowColChange (fgExtConcilia, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
js_fg_ModConcExtrBeforeRowColChange (fgExtConcilia, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgExtConcilia EVENT=DblClick>
<!--
js_fg_ModConcExtrDblClick (fgExtConcilia, fgExtConcilia.Row, fgExtConcilia.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgExtConcilia EVENT=AfterEdit>
<!--
fgValoresLocalizar_AfterEdit(fgExtConcilia, fgExtConcilia.Row, fgExtConcilia.Col)	
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgExtConcilia EVENT=EnterCell>
<!--
 js_fg_EnterCell (fgExtConcilia);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgExtrato EVENT=AfterEdit>
<!--
fgExtrato_AfterEdit(fgExtrato, fgExtrato.Row, fgExtrato.Col)	
//-->
</SCRIPT>

</head>

<body id="modalassocialancamentoconciliaBody" name="modalassocialancamentoconciliaBody" LANGUAGE="javascript" onload="return window_onload()">

	<div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

	<div id="divGridExtrato" name="divGridExtrato" class="divGeneral">
		<p id="lblGridExtrato" name="lblGridExtrato" class="lblGeneral">Extrato Banc�rio</p>
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgExtrato" name="fgExtrato" class="fldGeneral" VIEWASTEXT>
        </object>
    </div>    
    
    <div id="divGridExtConcilia" name="divGridExtConcilia" class="divGeneral">
		<p id="lblGridExtConcilia" name="lblGridExtConcilia" class="lblGeneral">Lan�amentos Extrato Concilia</p>
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgExtConcilia" name="fgExtConcilia" class="fldGeneral" VIEWASTEXT>
        </object>
    </div>    
    
    <div id="divControles" name="divControles" class="divGeneral">
		<p id="lblLancamentosPendentes" name="lblLancamentosPendentes" class="lblGeneral" LANGUAGE=javascript onclick="return invertChkBox(chkLancamentosPendentes)" title="S� lan�amentos pendentes?">LP</p>
		<input type="checkbox" id="chkLancamentosPendentes" name="chkLancamentosPendentes" class="fldGeneral" title="S� lan�amentos pendentes?"></input>
		<input type="button" id="btnAssociar" name="btnAssociar" value="Associar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Associar/Dissociar lan�amento"></input>
		<input type="button" id="btnExcluir" name="btnExcluir" value="Excluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Excluir lan�amento do extrato concilia"></input>
		<input type="button" id="btnIncluir" name="btnIncluir" value="Incluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Incluir lan�amento em extrato concilia"></input>
	    <p id="lblLog" name="lblLog" class="lblGeneral" LANGUAGE="javascript">Motivo Exclus�o</p>
        <input type="text" id="txtLog" name="txtLog" class="fldGeneral" LANGUAGE="javascript"></input>
    </div>	
	<input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
	<input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
</body>

</html>
