/********************************************************************
modalposicaoextratos.js

Library javascript para o modalposicaoextratos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_FirstTime = true;
var glb_bExtratoManual = false;

var dsoVerificacao = new CDatatransport('dsoVerificacao');
var dsoGridExtrato = new CDatatransport('dsoGridExtrato');
var dsoProcessamentoAutomatico = new CDatatransport('dsoProcessamentoAutomatico');
var dsoExtratoGerador = new CDatatransport('dsoExtratoGerador');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    
EVENTOS DOS OBJETOS DE IMPRESSAO:

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    ProcessamentoAutomatico();
    
    glb_aStaticCombos = null;
    
    // configuracao inicial do html
    setupPage();
    btnOK.value = 'Pr�ximo';   
    // final de carregamento e visibilidade de janela
    // foi movido daqui

	fillModalCmbFiles();
	btnCanc.style.visibility = 'hidden';
}

function fg_DblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if (fg.Row >= 1 )
        btn_onclick(btnOK);
}
/********************************************************************
Objeto controlador do carregamento dos dsos de grids e
preenchimento destes dsos 

Instanciado pela variavel glb_gridControlFill
********************************************************************/
function __gridControlFill() {
    this.bFollowOrder = false;
    this.nDsosToArrive = 0;
    this.dso1_Ref = null;
    this.grid1_Ref = null;
}

var glb_gridControlFill = new __gridControlFill();
/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if ((ctl.id == btnOK.id) && (btnCanc.disabled == false) && (btnOK.style.visibility != 'hidden'))
        btnOK.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);
    
    if (ctl.id == btnOK.id) {
        extratoManualGerador();
    }
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
	secText('Posi��o de Extratos Banc�rios', 1);

    // carrega dados na interface e configura travamento de seus elementos
	loadDataAndTreatInterface();

	// A janela esta carregando
	glb_gridControlFill.bFollowOrder = false;
	glb_gridControlFill.nDsosToArrive = 1;
	glb_gridControlFill.dso1_Ref = dsoGridExtrato;
	glb_gridControlFill.grid1_Ref = fg;

	lbldtExtrato.style.visibility = 'hidden';
	txtdtExtrato.style.visibility = 'hidden';

	startDynamicGrids();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;
    //var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
    //var btn_height = parseInt(btnOK.currentStyle.height, 10);

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;  
    
    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = widthFree - 2 * ELEM_GAP;    
        height = heightFree - ELEM_GAP;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height) - 16;
    }
    
    elem = document.getElementById('btnOK');
    with (elem.style) {
        left = modWidth - parseInt(btnOK.currentStyle.width, 10) - (ELEM_GAP * 2);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    
    // Por default o botao OK vem travado
    btnOK.disabled = false;
    
    // Bot�o Cancelar n�o ser� utilizado
    btnCanc.disabled = true;

    lbldtExtrato.style.top = (parseInt(btnOK.currentStyle.top) - 10);
    lbldtExtrato.style.left = (parseInt(fg.currentStyle.left) + 10);
    txtdtExtrato.style.top = (parseInt(btnOK.currentStyle.top) + 5);
    txtdtExtrato.style.left = (parseInt(fg.currentStyle.left) + 10);
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
	/*var sFileName = fg.TextMatrix(fg.Row, 3);
    sFileName = sFileName.substr(sFileName.lastIndexOf('\\') + 1, sFileName.length);
	
	if (sFileName.length > 255)
	{
		if ( window.top.overflyGen.Alert('O nome do arquivo deve ter no m�ximo 255 caracteres.') == 0 )
			return null;
	
		lockControlsInModalWin(false);
		return true;
    } */  
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, []);
}

/********************************************************************
Preenche o grid para o Retorno
********************************************************************/
function fillModalCmbFiles()
{
    var i;
    var aData = new Array();
    var strArquivos = '';
    var aArquivos = new Array();
	
    startGridInterface(fg);
    fg.FrozenCols = 0;
    headerGrid(fg, ['Conta',
                    'Data Concilia��o',
                    '�ltimo Processado',
                    'CorConciliacao', 
                    'CorDataUltimoProcessado'], []);
	fg.AutoSizeMode = 0;
	fg.AutoSize(0,fg.Cols-1);

    fg.Redraw = 2;
    
    showModalAfterDataLoad();
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modalfilesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    // coloca foco no grid
    if ( fg.runtimeStyle.visibility == 'visible' )
        fg.focus();
}


/********************************************************************
Evento onchange dos combos
********************************************************************/
function cmbOnChange(cmb) 
{
    ;
}


function adjustLabelsCombos(bPaging) 
{
    ;
}
/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos grids
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicGrids() {
    var dso;
    
    dso = glb_gridControlFill.dso1_Ref;

    setConnection(dso);

    if (!glb_FirstTime)
        lockControlsInModalWin(true);

    dso.SQL = 'SELECT dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 41) AS RelPesContaID, dbo.fn_ContaBancaria_Datas(a.RelPesContaID,1) AS dtConciliacao, ' +
              'dbo.fn_ContaBancaria_Datas(a.RelPesContaID,2) AS dtUltimo, ' + 
              'dbo.fn_ContaBancaria_Cor(a.RelPesContaID, 1) AS CorConciliacao , ' +
              'dbo.fn_ContaBancaria_Cor(a.RelPesContaID, 2) AS CorDataUltimoProcessado, ' +
              'a.Observacao, a.RelPesContaID AS RelPesConta2ID ' +
              'FROM RelacoesPessoas_Contas a WITH(NOLOCK) ' +
              'INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON a.RelacaoID = b.RelacaoID ' +
              'INNER JOIN Pessoas c WITH(NOLOCK) ON b.ObjetoID = c.PessoaID ' +
              'WHERE b.SujeitoID = ' + glb_nEmpresaID + ' AND a.EstadoID = 2 AND b.EstadoID = 2 AND c.EstadoID = 2 AND a.TipoContaID IN (1506, 1509) ' +
              'ORDER BY a.RelPesContaID ';
    dso.ondatasetcomplete = startDynamicGrids_DSC;
    dso.Refresh();
}

/********************************************************************
Preenche os grids e mostra a janela
********************************************************************/
function startDynamicGrids_DSC() {
    glb_gridControlFill.nDsosToArrive--;

    if (glb_gridControlFill.nDsosToArrive > 0)
        return null;

    var i;
    var dso, grid;
    var dTFormat = '';
    var nTipoID, sColor;
    var bPreencheuExtrato = false;
    var nColCorConciliacao;
    var nColCorCorDataUltimo;
    var theCol = 0;
    
    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';
    else
        dTFormat = 'dd/mm/yyyy';

        dso = glb_gridControlFill.dso1_Ref;
        grid = glb_gridControlFill.grid1_Ref;

        if (!(dso.recordset.BOF && dso.recordset.EOF)) {
            // move o cursor do dso para o primeiro registro
            dso.recordset.MoveFirst();
        }

        grid.ExplorerBar = 0;
        startGridInterface(grid, 1, null);
        grid.FontSize = '10';
        grid.Editable = false;
        grid.BorderStyle = 1;

        headerGrid(grid, ['Bco/Ag/Cta - Dv',
		               'Data Concilia��o',
		               '�ltimo Processado',
                       'Observacao',
                       'CorConciliacao',
		               'CorDataUltimoProcessado',
                       'RelPesConta2ID'], [4, 5, 6]);

        fillGridMask(grid, dso, ['RelPesContaID',
		                       'dtConciliacao',
		                       'dtUltimo',
                               'Observacao',
                               'CorConciliacao',
		                       'CorDataUltimoProcessado',
                               'RelPesConta2ID'],
		                       ['', '', '', '', '', '', ''],
		                       ['', dTFormat, dTFormat, '', '', '', '']);

        if (!(dso.recordset.BOF && dso.recordset.EOF)) {
            // move o cursor do dso para o primeiro registro
            dso.recordset.MoveFirst();
        }

        alignColsInGrid(grid, [1,2]);
        
        nColCorConciliacao = getColIndexByColKey(grid, 'CorConciliacao');
        nColCorCorDataUltimo = getColIndexByColKey(grid, 'CorDataUltimoProcessado');

        grid.FillStyle = 1;
        for (i = 1; i < grid.Rows; i++) {
            // dtConciliacao
            if (grid.TextMatrix(i, getColIndexByColKey(grid, 'dtConciliacao')) != '')
                grid.Cell(6, i, getColIndexByColKey(grid, 'dtConciliacao'), i, getColIndexByColKey(grid, 'dtConciliacao')) = eval(grid.TextMatrix(i, nColCorConciliacao));

            // dtUltimo
            if (grid.TextMatrix(i, getColIndexByColKey(grid, 'dtUltimo')) != '')
                grid.Cell(6, i, getColIndexByColKey(grid, 'dtUltimo'), i, getColIndexByColKey(grid, 'dtUltimo')) = eval(grid.TextMatrix(i, nColCorCorDataUltimo));
        }
        FillStyle = 0;

        for (theCol = 0; theCol < grid.Cols - 1 ; theCol++)
                grid.ColAlignment(theCol) = 0;

        grid.CellAlignment = 0;
        grid.ExplorerBar = 5;
        grid.Col = getColIndexByColKey(grid, 'RelPesContaID');
        grid.Sort = 1;
        
        if ((grid.Rows > 0) && grid.Row > 0)
            grid.TopRow = grid.Row;

        grid.Editable = false;

        adjustWidthCellsInGrid(grid);
    

    // Reinicializa objeto que controla dsos e grids
    glb_gridControlFill.bFollowOrder = false;
    glb_gridControlFill.nDsosToArrive = 0;
    glb_gridControlFill.dso1_Ref = null;
    glb_gridControlFill.grid1_Ref = null;

    // Mostra janela se for o caso
    if (glb_FirstTime) {
        glb_FirstTime = false;
        // A janela esta carregando
        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 1;
        glb_gridControlFill.dso1_Ref = dsoGridExtrato;
        glb_gridControlFill.grid1_Ref = fg;
        //showModalAfterDataLoad();
    }
    else
        lockControlsInModalWin(false);

    if (fg.Rows > 2) 
        fg.focus();

    extratoManual();
}

/********************************************************************
Ajusta largura de colunas dos grid
********************************************************************/
function adjustWidthCellsInGrid(grid) {
    var nCurrRow = 0;
    var sLabel1;

    sLabel1 = grid.TextMatrix(0, getColIndexByColKey(grid, 'RelPesContaID'));

    if (grid.Row > 0)
        nCurrRow = grid.Row;

    grid.Redraw = 0;

    grid.TextMatrix(0, getColIndexByColKey(grid, 'RelPesContaID')) = '9999999';

    grid.AutoSizeMode = 0;
    grid.AutoSize(0, grid.Cols - 1);

    grid.TextMatrix(0, getColIndexByColKey(grid, 'RelPesContaID')) = sLabel1;

    grid.Redraw = 2;
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function js_fg_ModConcExtrDblClick(grid, row, col) {
    return null;
}

/********************************************************************
Funcao do programador
Ida ao servidor para processar extratos automaticamente
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function ProcessamentoAutomatico() {
    lockControlsInModalWin(true);

    var strPars = new String();

    strPars += '?nEmpresaID=' + escape(glb_nEmpresaID);
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    dsoProcessamentoAutomatico.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/processamentoautomatico.aspx' + strPars;
    dsoProcessamentoAutomatico.ondatasetcomplete = ProcessamentoAutomatico_DSC;
    dsoProcessamentoAutomatico.refresh();
}

/********************************************************************
Funcao do programador
Retorno do servidor para processar extratos automaticamente
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function ProcessamentoAutomatico_DSC() {
    lockControlsInModalWin(false);
    btnOK.disabled = false;
    // Bot�o Cancelar n�o ser� utilizado
    btnCanc.style.visibility = 'hidden';
}

function js_Extrato_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol)
{
    extratoManual();
}

function extratoManual()
{
    var sObservacao = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Observacao'));

    if (sObservacao.indexOf('<EM>') != -1)
    {
        glb_bExtratoManual = true;
        lbldtExtrato.style.visibility = 'inherit';
        txtdtExtrato.style.visibility = 'inherit';
        btnOK.style.visibility = 'inherit';
        
    }
    else
    {
        glb_bExtratoManual = false;
        lbldtExtrato.style.visibility = 'hidden';
        txtdtExtrato.style.visibility = 'hidden';
        btnOK.style.visibility = 'hidden';
    }
}

function extratoManualGerador()
{
    lockControlsInModalWin(true);

    if ((!chkDataEx(txtdtExtrato.value)) && (txtdtExtrato.value ))
    {
        if (window.top.overflyGen.Alert('Data de Extrato � Inv�lida') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    var nRelPesContaID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RelPesConta2ID'));
    var dtExtrato = dateFormatToSearch(txtdtExtrato.value);
    var strPars = new String();

    strPars = '?nRelPesContaID=' + escape(nRelPesContaID);
    strPars += '&dtExtrato=' + escape(dtExtrato);

    dsoExtratoGerador.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/ExtratoBancarioManual.aspx' + strPars;
    dsoExtratoGerador.ondatasetcomplete = extratoManualGerador_DSC;
    dsoExtratoGerador.refresh();
}

function extratoManualGerador_DSC()
{
    var Resultado = '';

    if (!((dsoExtratoGerador.recordset.BOF) && (dsoExtratoGerador.recordset.EOF)))
    {
        Resultado = dsoExtratoGerador.recordset['Resultado'].value;

        if ((Resultado != '') && (Resultado != null))
        {
            if (window.top.overflyGen.Alert(Resultado) == 0)
                return null;

            lockControlsInModalWin(false);
            return null;
        }
    }

    if (window.top.overflyGen.Alert('Extrato gerado com sucesso!') == 0)
        return null;

    lockControlsInModalWin(false);

    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
}