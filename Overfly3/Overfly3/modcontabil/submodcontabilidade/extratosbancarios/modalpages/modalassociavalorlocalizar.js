/********************************************************************
modalassociavalorlocalizar.js

Library javascript para o modalassociavalorlocalizar.asp
Pessoas 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_FirstTime = true;
var glb_LASTLINESELID = null;
var glb_PassedOneInDblClick = false;
var glb_nSaldoExtrato = 0;
var glb_bDissociar = null;
var glb_nCounter = 0;
var glb_bConcilia = false;
var glb_TimerCallRowColChange = null;
var glb_nTam = null;
var glb_SortConc = null;
var glb_nAssociar = 0; /*Utilizada para associar vls*/
var glb_nAssociar2 = 0; /*Utilizada para associar vls*/
var glb_sMensagemAssocia = ''; /*Utilizada para associar vls*/
var glb_nRowExtrato = 1;
var glb_nRowVL = 1;
var glb_nCounterVL = 0;
var glb_nCounterExtrato = 0;
var glb_strPars1 = '';
var glb_strPars2 = '';
var glb_ExtratoDissocia = 1;
var glb_sMensagemErroDissocia = '';
var glb_bFimAssocia = false;

var dsoHistoricoPadrao = new CDatatransport('dsoHistoricoPadrao');
var dsoGridExtrato = new CDatatransport('dsoGridExtrato');
var dsoGridValoresLocalizar = new CDatatransport('dsoGridValoresLocalizar');
var dsoAssociarValorLocalizar = new CDatatransport('dsoAssociarValorLocalizar');
var dsoLimparIdentificadores = new CDatatransport('dsoLimparIdentificadores');
var dsoLimparIdentificadoresEx = new CDatatransport('dsoLimparIdentificadoresEx');
var dsoGerarValorLocalizar = new CDatatransport('dsoGerarValorLocalizar');
var dsoConciliaExtrato = new CDatatransport('dsoConciliaExtrato');
var dsoTransferirVinculada = new CDatatransport('dsoTransferirVinculada');
/********************************************************************
Objeto controlador do carregamento dos dsos de grids e
preenchimento destes dsos 

Instanciado pela variavel glb_gridControlFill
********************************************************************/
function __gridControlFill() {
    this.bFollowOrder = false;
    this.nDsosToArrive = 0;
    this.dso1_Ref = null;
    this.dso2_Ref = null;
    this.grid1_Ref = null;
    this.grid2_Ref = null;
}

var glb_gridControlFill = new __gridControlFill();

/********************************************************************
Objeto controlador de execucao dos eventos dos grids e de grid com
linha de totais

Nota:
nColDataType = 0 (String), 1 (Numerico), 2 (Data), 3 (Boolean)
ncolUsedToSort = -1 nenhuma coluna
ncolOrderUsedToSort = 1 ascendente, -1 descendente

Instanciado pela variavel glb_gridsEvents
********************************************************************/

function __resetOrderParams() {
    this.ncolUsedToSort_fgExtrato = -1;
    this.ncolOrderUsedToSort_fgExtrato = 0;
    this.ncolUsedToSort_fgLacContas = -1;
    this.ncolOrderUsedToSort_fgValoresLocalizar = 0;
}

function __gridsEvents() {
    this.bTotalLine_fgExtrato = false;
    this.bTotalLine_fgValoresLocalizar = false;
    this.bBlockEvents_fgExtrato = false;
    this.bBlockEvents_fgValoresLocalizar = false;
    this.ncolUsedToSort_fgExtrato = -1;
    this.ncolOrderUsedToSort_fgExtrato = 0;
    this.ncolUsedToSort_fgLacContas = -1;
    this.ncolOrderUsedToSort_fgValoresLocalizar = 0;

    this.resetOrderParams = __resetOrderParams;
}

var glb_gridsEvents = new __gridsEvents();

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
   
window_onload()
btn_onclick(ctl)
setupPage()
adjustControlsInInterface(topFree, heightFree, widthFree, frameBorder, mode)
startDynamicGrids()
startDynamicGrids_DSC()
showModalAfterDataLoad()
interfaceControlsState()
adjustWidthCellsInGrid(grid)

execAssociarValorLocalizar()
execAssociarValorLocalizar_DSC
geraValorLocalizar()
execSPValorLocalizar_Gerador()
execSPValorLocalizar_Gerador_DSC()

cmbOnChange(cmb)
chkOnClick()

js_fg_ModConcExtrBeforeRowColChange(grid, oldRow, oldCol, newRow, newCol)
js_fg_ModConcExtrAfterRowColChangePesqList(grid, oldRow, oldCol, newRow, newCol)
fg_ModConcExtrBeforeSort(grid, col)
fg_ModConcExtrAfterSort(grid, col)
js_fg_ModConcExtrDblClick(grid, row, col)

lineChangedInGrid_Extrato(grid, nRow)
lineChangedInGrid_ValoresLocalizar(grid, nRow)

controlsInterfaceState()
gridSortParams(grid, col)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    (getFrameInHtmlTop(getExtFrameID(window))).style.top =
		(getFrameInHtmlTop('frameSup01')).offsetTop;

    startDynamicGrids();
    startDynamicCmbs();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    window.focus();

    if (ctl.id == btnOK.id)
        btnOK.focus();

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    else if (ctl.id == btnAutomatico.id)
        execAssociarValorLocalizar(true);
        // 3. O usuario clicou o botao Associar
    else if (ctl.id == btnAssociar.id)
        execAssociarValorLocalizar();
    else if (ctl.id == btnLimpar.id)
        limparIdentificadores();
    else if (ctl.id == btnGerarValorLocalizar.id)
        geraValorLocalizar();
    else if (ctl.id == btnLimparEx.id)
        limparIdentificadoresEx();
    else if (ctl.id == btnTransferir.id)
        TransferirVinculada();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // btnOK nao e usado nesta modal
    with (btnOK) {
        disabled = true;
        style.visibility = 'hidden';
    }

    glb_SortConc = false;

    // btnCanc nao e usado nesta modal
    with (btnCanc) {
        style.visibility = 'hidden';
    }

    glb_bConcilia = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'Concilia' + '\'' + '].value ');

    btnAutomatico.style.visibility = 'visible';

    chkUsoCotidiano.onclick = chkOnClick;
    chkUsoCotidiano.checked = true;
    chkLancamentosPendentes.onclick = chkOnClick;
    chkLancamentosPendentes.checked = true;
    chkLancamentosSemelhantes.onclick = chkOnClick;

    // texto da secao01
    secText('Associar Valor a Localizar - Extrato ' + glb_nExtratoID + ' (' + glb_sBanco + ')', 1);

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 5;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    // altura livre da janela modal (descontando apenas a barra azul)
    heightFree = modHeight - topFree - frameBorder;

    // largura livre
    widthFree = modWidth - (2 * frameBorder);

    adjustControlsInInterface(topFree, heightFree, widthFree, frameBorder);

    glb_gridControlFill.bFollowOrder = false;
    glb_gridControlFill.nDsosToArrive = 2;
    glb_gridControlFill.dso1_Ref = dsoGridExtrato;
    glb_gridControlFill.dso2_Ref = dsoGridValoresLocalizar;
    glb_gridControlFill.grid1_Ref = fgExtrato;
    glb_gridControlFill.grid2_Ref = fgValoresLocalizar;
}

/********************************************************************
Posiciona os campos na interface, define labels e hints,
controla digitacao em campos num�ricos, define eventos associados
a controles, etc

Parametros:
	topFree	- inicio da altura da janela (posicao onde termina o div do
	          titulo da janela )
	heightFree - altura livre da janela (do top free ate o botao OK,
	          supondo o btnOK em baixo, que e o padrao de modal)
	widthFree - largura livre da janela
	frameBorder - largura da borda da janela
	          
	mode (integer) - opcional -> null implica em mode 1:
		1 - dois grid em cima e botoes em baixo
		2 - dois grids em baixo e botoes em cima
	
	gridPos (integer) - opcional -> null implica em gridPos 1:
		1 - um grid ao lado do outro
		2 - um grid em cima do outro
		
Retorno:
	nenhum
********************************************************************/
function adjustControlsInInterface(topFree, heightFree, widthFree, frameBorder, mode, gridPos) {
    var elem;
    var x_leftWidth = 0;
    var y_topHeight = 0;

    var elemLbl;
    var elemGrid;
    var lbl_height = 16;
    var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
    var btn_height = parseInt(btnOK.currentStyle.height, 10);
    var nDivControlsHeight = 0;

    if (mode == null)
        mode = 1;

    if (gridPos == null)
        gridPos = 1;

    // Posiciona os controles do divControles
    adjustElementsInForm([['lblTipoLancamento', 'selTipoLancamento', 12, 1, -5],
						  ['', '', 1, 1, -5],
						  ['lblLancamentosPendentes', 'chkLancamentosPendentes', 3, 1, -5],
						  ['lblLancamentosSemelhantes', 'chkLancamentosSemelhantes', 3, 1, -5],
						  ['btnAutomatico', 'btn', btn_width, 1, ELEM_GAP - 4],
						  ['btnAssociar', 'btn', btn_width, 1, ELEM_GAP - 4],
						  ['btnLimpar', 'btn', btn_width, 1, ELEM_GAP - 4],
						  ['btnLimparEx', 'btn', btn_width, 1, ELEM_GAP - 4],
                          ['btnTransferir', 'btn', btn_width, 1, ELEM_GAP - 4],
                          ['lblEhEstorno', 'chkEhEstorno', 3, 2, -5],
                          ['lblProcessoID', 'selProcessoID', 17, 2, -5],
                          ['lblUsoCotidiano', 'chkUsoCotidiano', 3, 2, -5],
                          ['lblHistoricoPadraoID', 'selHistoricoPadraoID', 23, 2, -5],
                          ['lblObservacao', 'txtObservacao', 30, 2, -5],
                          ['btnGerarValorLocalizar', 'btn', btn_width, 2, ELEM_GAP - 4]], null, null, true);

    btnGerarValorLocalizar.style.height = btn_height;
    btnAssociar.style.height = btn_height;
    btnLimpar.style.height = btn_height;
    btnLimparEx.style.height = btn_height;
    btnCanc.style.height = btn_height;
    btnAutomatico.style.height = btn_height;
    btnAutomatico.disabled = false;
    txtObservacao.maxLength = 30;

    nDivControlsHeight = parseInt(btnAssociar.currentStyle.top, 10) +
	                     parseInt(btnAssociar.currentStyle.height, 10);
    // Dimensiona os divs em funcao dos parametros

    // o div de controles
    elem = divControles;
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        width = widthFree - (3 * ELEM_GAP / 2);
        height = nDivControlsHeight;
    }

    // os divs de grids
    if (gridPos == 1) {
        elem = divGridExtrato;
        with (elem.style) {
            border = 'none';
            backgroundColor = 'transparent';
            width = (widthFree - (3 * ELEM_GAP / 2)) / 2 - (ELEM_GAP / 2);
            height = heightFree - 6 * ELEM_GAP - nDivControlsHeight;
        }

        elem = divGridValoresLocalizar;
        with (elem.style) {
            border = 'none';
            backgroundColor = 'transparent';
            width = (widthFree - (3 * ELEM_GAP / 2)) / 2 - (ELEM_GAP / 2);
            height = heightFree - 6 * ELEM_GAP - nDivControlsHeight;
        }
    }
    else if (gridPos == 2) {
            ;
    }

    // Dimensiona e posiciona os grids e seus labels
    elemLbl = lblGridExtrato;
    elemGrid = divGridExtrato;
    with (elemLbl.style) {
        backgroundColor = 'transparent';
        left = 0;
        top = ELEM_GAP / 2;
        width = parseInt(elemGrid.currentStyle.width, 10) - 2;
        height = lbl_height;
    }

    grid = fgExtrato;
    elem = divGridExtrato;
    with (grid.style) {
        left = 0;
        top = parseInt(elemLbl.currentStyle.top, 10) +
		      parseInt(elemLbl.currentStyle.height, 10);
        width = parseInt(elem.currentStyle.width, 10);
        height = parseInt(elem.currentStyle.height, 10) -
		         (parseInt(elemLbl.currentStyle.top, 10) +
		         parseInt(elemLbl.currentStyle.height, 10));
    }

    startGridInterface(grid, 1, null);
    grid.FontSize = '7';
    grid.Cols = 1;
    grid.ColWidth(0) = parseInt(elem.currentStyle.width, 10) * 18;

    elemLbl = lblGridValoresLocalizar;
    elemGrid = divGridValoresLocalizar;
    with (elemLbl.style) {
        backgroundColor = 'transparent';
        left = 0;
        top = ELEM_GAP / 2;
        width = parseInt(elemGrid.currentStyle.width, 10) - 2;
        height = lbl_height;
    }

    grid = fgValoresLocalizar;
    elem = divGridValoresLocalizar;
    with (grid.style) {
        left = 0;
        top = parseInt(elemLbl.currentStyle.top, 10) +
		      parseInt(elemLbl.currentStyle.height, 10);
        width = parseInt(elem.currentStyle.width, 10);
        height = parseInt(elem.currentStyle.height, 10) -
		         (parseInt(elemLbl.currentStyle.top, 10) +
		         parseInt(elemLbl.currentStyle.height, 10));
    }

    startGridInterface(grid, 1, null);
    grid.FontSize = '7';
    grid.Cols = 1;
    grid.ColWidth(0) = parseInt(elem.currentStyle.width, 10) * 18;

    // Posiciona os divs na horizontal e vertical
    if (mode == 1) {
        if (gridPos == 1) {
            elem = divGridExtrato;
            elem.style.left = ELEM_GAP;
            elem.style.top = topFree;
            y_topHeight = parseInt(elem.currentStyle.top, 10) +
			                  parseInt(elem.currentStyle.height, 10);
            x_leftWidth = parseInt(elem.currentStyle.left, 10) +
			                  parseInt(elem.currentStyle.width, 10);

            elem = divGridValoresLocalizar;
            elem.style.left = ELEM_GAP + x_leftWidth;
            elem.style.top = topFree;
            y_topHeight = parseInt(elem.currentStyle.top, 10) +
			                  parseInt(elem.currentStyle.height, 10);

            elem = divControles;
            elem.style.left = ELEM_GAP;
            elem.style.top = y_topHeight;
        }
        else if (gridPos == 2) {
                ;
        }
    }
    else if (mode == 2) {
        if (gridPos == 1) {
            elem = divControles;
            elem.style.left = ELEM_GAP;
            elem.style.top = topFree;
            y_topHeight = parseInt(elem.currentStyle.top, 10) +
			                  parseInt(elem.currentStyle.height, 10);

            elem = divGridExtrato;
            elem.style.left = ELEM_GAP;
            elem.style.top = y_topHeight + ELEM_GAP;
            x_leftWidth = parseInt(elem.currentStyle.left, 10) +
			                  parseInt(elem.currentStyle.width, 10);

            elem = divGridValoresLocalizar;
            elem.style.left = ELEM_GAP + x_leftWidth;
            elem.style.top = y_topHeight + ELEM_GAP;
        }
        else if (gridPos == 2) {
                ;
        }
    }
}

/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos combos
           
Parametros: 
bAutomatico

Retorno:
nenhum
********************************************************************/
function execAssociarValorLocalizar(bAutomatico) {
    lockControlsInModalWin(true);

    var strPars = new String();
    var strPars1 = new String();
    var strPars2 = new String();
    var i = 0;
    var iCount = 0;

    glb_nAssociar = 0;
    glb_nAssociar2 = 0;
    glb_sMensagemAssocia = '';

    var nValorID = 0;

    if (bAutomatico == true) {
        if ((selTipoLancamento.value != 1431) && (selTipoLancamento.value != 1432)) {
            if (window.top.overflyGen.Alert('Escolha um tipo de lan�amento') == 0)
                return null;

            lockControlsInModalWin(false);
            return null;
        }
        else {
            strPars += '?nExtratoID=' + escape(glb_nExtratoID);

            if (selTipoLancamento.value == 1431)
                strPars += '&bCredito=1';
            else {
                if (selTipoLancamento.value == 1432)
                    strPars += '&bCredito=0';
            }
            glb_nAssociar2--; //Para poder mostrar mensagem no DSC
            dsoAssociarValorLocalizar.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/associarautomatico.aspx' + strPars;
            dsoAssociarValorLocalizar.ondatasetcomplete = execAssociarValorLocalizar_DSC;
            dsoAssociarValorLocalizar.refresh();
        }
    }
    else {
        var sErro = '';

        if (sErro != '') {
            if (window.top.overflyGen.Alert(sErro) == 0)
                return null;

            lockControlsInModalWin(false);
            return null;
        }

        glb_nRowExtrato = 1;
        glb_nRowVL = 1;
        glb_bFimAssocia = false;

        AssociarLote();

/*
        for (i = 1; i < fgExtrato.Rows; i++) {
            if ((fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'OK')) != 0) && (fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'NaoConcilia*')) == 0))
                strPars1 += '?nExtLancamentoID=' + escape(getCellValueByColKey(fgExtrato, 'ExtLancamentoID*', i));
        }

        for (i = 1; i < fgValoresLocalizar.Rows; i++) {
            if (fgValoresLocalizar.ValueMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'OK')) != 0)
                glb_nAssociar++;
        }

        //Associa um por vez
        for (iCount = 1; iCount < fgValoresLocalizar.Rows; iCount++) {
            if (fgValoresLocalizar.ValueMatrix(iCount, getColIndexByColKey(fgValoresLocalizar, 'OK')) != 0) {
                strPars2 = '&nValorID=' + escape(getCellValueByColKey(fgValoresLocalizar, 'ValorID*', iCount));

                Associar(strPars1 + strPars2);
            }
        }
*/
    }
}

function AssociarLote()
{
    if (glb_nCounterExtrato > 1)
        glb_strPars1 = '';

    if (glb_nCounterVL > 1)
        glb_strPars2 = '';

    if ((glb_nCounterExtrato == 1) && (glb_nCounterVL == 1))
    {
        glb_strPars1 = '';
        glb_strPars2 = '';
    }

    if ((glb_nRowExtrato == 1) || (glb_nCounterExtrato > 1))
    {
        for (i = glb_nRowExtrato; i < fgExtrato.Rows; i++)
        {
            if ((fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'OK')) != 0) && (fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'NaoConcilia*')) == 0))
            {
                glb_strPars1 += '?nExtLancamentoID=' + escape(getCellValueByColKey(fgExtrato, 'ExtLancamentoID*', i));

                glb_nRowExtrato = (i + 1);

                break;
            }
        }
    }


    if ((glb_nRowVL == 1) || (glb_nCounterVL > 1))
    {
        //Associa um por vez
        for (iCount = glb_nRowVL; iCount < fgValoresLocalizar.Rows; iCount++)
        {
            if (fgValoresLocalizar.ValueMatrix(iCount, getColIndexByColKey(fgValoresLocalizar, 'OK')) != 0)
            {
                glb_strPars2 = '&nValorID=' + escape(getCellValueByColKey(fgValoresLocalizar, 'ValorID*', iCount));

                glb_nRowVL = (iCount + 1);

                break;
            }
        }
    }

    if ((glb_strPars1 != '') && (glb_strPars2 != ''))
        Associar(glb_strPars1 + glb_strPars2);
    else
    {
        glb_bFimAssocia = true;
        execAssociarValorLocalizar_DSC();
    }
}

/********************************************************************
Funcao do programador
Fun��o utilizada para chamar o arquivo asp
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function Associar(str) {
    dsoAssociarValorLocalizar.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/associarvalorlocalizar.aspx' + str;
    dsoAssociarValorLocalizar.ondatasetcomplete = Associar_DSC;
    dsoAssociarValorLocalizar.refresh();

    //execAssociarValorLocalizar_DSC();
}
function Associar_DSC()
{
    AssociarLote();
}

/********************************************************************
Funcao do programador
Retorno do servidor apos executar procedure de conciliacao bancaria
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function execAssociarValorLocalizar_DSC() {
    if (glb_FirstTime) {
        // final de carregamento e visibilidade de janela
        startDynamicCmbs();
    }
    else {
        glb_nAssociar2++;
        lockControlsInModalWin(false);

        if (!(dsoAssociarValorLocalizar.recordset.BOF && dsoAssociarValorLocalizar.recordset.EOF)) {
            if ((dsoAssociarValorLocalizar.recordset['Resultado'].value != null) &&
                 (dsoAssociarValorLocalizar.recordset['Resultado'].value != '')) {
                glb_sMensagemAssocia += '\n ' + dsoAssociarValorLocalizar.recordset['Resultado'].value;
            }
        }

        if ((glb_nCounterVL == glb_nAssociar2) || (glb_bFimAssocia))
        {

            if (glb_sMensagemAssocia == null || glb_sMensagemAssocia == '')
                glb_sMensagemAssocia = 'Lan�amento e Vl(s) associado(s) com sucesso';

            if (window.top.overflyGen.Alert(glb_sMensagemAssocia) == 0)
                return null;

            // A janela esta carregando
            glb_gridControlFill.bFollowOrder = false;
            glb_gridControlFill.nDsosToArrive = 2;
            glb_gridControlFill.dso1_Ref = dsoGridExtrato;
            glb_gridControlFill.dso2_Ref = dsoGridValoresLocalizar;
            glb_gridControlFill.grid1_Ref = fgExtrato;
            glb_gridControlFill.grid2_Ref = fgValoresLocalizar;

            startDynamicGrids();
        }
    }
}

/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos combos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function conciliaExtrato() {
    var ExtLancamentoID = escape(getCellValueByColKey(fgExtrato, 'ExtLancamentoID*', fgExtrato.Row));

    if (ExtLancamentoID != null) {

        if (glb_bConcilia == true) {

            lockControlsInModalWin(true);

            var strPars = new String();
            strPars += '?nExtLancamentoID=' + ExtLancamentoID;

            dsoConciliaExtrato.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/conciliaextrato.aspx' + strPars;
            dsoConciliaExtrato.ondatasetcomplete = conciliaExtrato_DSC;
            dsoConciliaExtrato.refresh();
        }
        else {
            if (window.top.overflyGen.Alert('O NC s� pode ser marcado em extrato concilia') == 0)
                return null;
        }
    }

}

/********************************************************************
Funcao do programador
Retorno do servidor
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function conciliaExtrato_DSC() {
    lockControlsInModalWin(false);

    // A janela esta carregando
    glb_gridControlFill.bFollowOrder = false;
    glb_gridControlFill.nDsosToArrive = 2;
    glb_gridControlFill.dso1_Ref = dsoGridExtrato;
    glb_gridControlFill.dso2_Ref = dsoGridValoresLocalizar;
    glb_gridControlFill.grid1_Ref = fgExtrato;
    glb_gridControlFill.grid2_Ref = fgValoresLocalizar;

    startDynamicGrids();
}

/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos combos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function limparIdentificadores()
{
    lockControlsInModalWin(true);

    if (glb_ExtratoDissocia < fgExtrato.Rows)
    {
        if (fgExtrato.ValueMatrix(glb_ExtratoDissocia, getColIndexByColKey(fgExtrato, 'OK')) != 0)
        {
            var strPars = new String();
            var nCounter = 0;
            var strPars = new String();
            strPars += '?nExtLancamentoID=' + escape(getCellValueByColKey(fgExtrato, 'ExtLancamentoID*', glb_ExtratoDissocia));


            dsoLimparIdentificadores.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/dissociarvalorlocalizar.aspx' + strPars;
            dsoLimparIdentificadores.ondatasetcomplete = limparIdentificadoresLoop_DSC;
            dsoLimparIdentificadores.refresh();
        }
        else
        {
            glb_ExtratoDissocia++;

            limparIdentificadores();
        }
    }
    else
    {
        glb_ExtratoDissocia = 1;
        limparIdentificadores_DSC();
    }
}

function limparIdentificadoresLoop_DSC()
{
    if (!(dsoLimparIdentificadores.recordset.BOF && dsoLimparIdentificadores.recordset.EOF))
        if ((dsoLimparIdentificadores.recordset['Resultado'].value != null) && (dsoLimparIdentificadores.recordset['Resultado'].value != ''))
            glb_sMensagemErroDissocia = dsoLimparIdentificadores.recordset['Resultado'].value;

    glb_ExtratoDissocia++;

    limparIdentificadores();
}


/********************************************************************
Funcao do programador
Retorno do servidor
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function limparIdentificadores_DSC()
{
    if (glb_FirstTime) {
        // final de carregamento e visibilidade de janela
        startDynamicCmbs();
    }
    else
    {
        lockControlsInModalWin(false);

        if (glb_sMensagemErroDissocia != '')
        {
            if (window.top.overflyGen.Alert(glb_sMensagemErroDissocia) == 0)
                return null;
        }

        // A janela esta carregando
        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 2;
        glb_gridControlFill.dso1_Ref = dsoGridExtrato;
        glb_gridControlFill.dso2_Ref = dsoGridValoresLocalizar;
        glb_gridControlFill.grid1_Ref = fgExtrato;
        glb_gridControlFill.grid2_Ref = fgValoresLocalizar;
        startDynamicGrids();
    }
}

/********************************************************************
Funcao do programador
Limpa Identificadores
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function limparIdentificadoresEx() {
    lockControlsInModalWin(true);

    var strPars = new String();
    var nCounter = 0;

    strPars = '';

    var nValorExtrato = 0;
    var nTipoLancamentoID = 0;
    var bEhEstorno = false;
    var nExtLancamentoID = 0;
    var nFormaPagamentoID = 0;
    var sMotivoDevolucao = '';
    var nUsuarioID = 0;
    var nProcessoID = 0;
    var nHistoricoPadraoID = 0;
    var _nValorID = 0;


    nProcessoID = selProcessoID.value;
    nHistoricoPadraoID = selHistoricoPadraoID.value;
    bEhEstorno = (chkEhEstorno.checked ? 1 : 0);
    nUsuarioID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');

    for (i = 1; i < fgValoresLocalizar.Rows; i++) {
        if ((fgValoresLocalizar.ValueMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'OK')) != 0) &&
			(fgValoresLocalizar.TextMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'Identificador*')) != '')) {
            if (nCounter == 0)
                strPars += '?';
            else
                strPars += '&';

            strPars += 'nValorID=' + escape(getCellValueByColKey(fgValoresLocalizar, 'ValorID*', i));
            nCounter++;
        }
    }

    if (nCounter == 0) {
        if (window.top.overflyGen.Alert('Selecione algum Valor a Localizar com identificador') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    dsoLimparIdentificadoresEx.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/limparExidentificadores.aspx' + strPars;
    dsoLimparIdentificadoresEx.ondatasetcomplete = limparIdentificadoresEx_DSC;
    dsoLimparIdentificadoresEx.refresh();
}

/********************************************************************
Funcao do programador
Retorno do servidor
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function limparIdentificadoresEx_DSC() {
    lockControlsInModalWin(false);

    var sResultado;

    if (!(dsoLimparIdentificadoresEx.recordset.BOF && dsoLimparIdentificadoresEx.recordset.EOF)) {
        sResultado = dsoLimparIdentificadoresEx.recordset['Resultado'].value;

        if ((sResultado != null) && (sResultado != '')) {
            if (window.top.overflyGen.Alert(sResultado) == 0)
                return null;
        }
        else {
            window.top.overflyGen.Alert('Limpeza efetuada com sucesso');

            // Marco
            fgExtrato.Rows = 1;
            fgValoresLocalizar.Rows = 1;

        }

        // A janela esta carregando
        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 2;
        glb_gridControlFill.dso1_Ref = dsoGridExtrato;
        glb_gridControlFill.dso2_Ref = dsoGridValoresLocalizar;
        glb_gridControlFill.grid1_Ref = fgExtrato;
        glb_gridControlFill.grid2_Ref = fgValoresLocalizar;
        startDynamicGrids();
    }
}


/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos grids
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicGrids() {
    if (!glb_FirstTime)
        lockControlsInModalWin(true);

    var dso;
    var sFiltro = '';
    var sData = '';
    var nValor = '';

    glb_bDissociar = null;

    if (glb_gridControlFill.dso1_Ref != null) {
        if (selTipoLancamento.value == 1431)
            sFiltro += ' AND Valor >=0';
        else {
            if (selTipoLancamento.value == 1432)
                sFiltro += ' AND Valor <=0';
        }


        if (chkLancamentosPendentes.checked)
            sFiltro += ' AND ValorID IS NULL AND NaoConcilia = 0';

        dso = glb_gridControlFill.dso1_Ref;

        setConnection(dso);

        dso.SQL = 'SELECT 1 AS Indice, ExtLancamentoID, dtLancamento, Documento, Valor, CONVERT(BIT, 0) AS OK, NaoConcilia, Historico, Identificador, ' +
						'dbo.fn_ExtratoBancarioLancamento_Tipo(ExtLancamentoID) AS TipoID, ' +
						'dbo.fn_ExtratoBancario_Saldo(' + glb_nExtratoID + ', 0, null) AS SaldoFinal, ' +
						'(CASE WHEN ExtLancamentoMaeID IS NULL THEN ExtLancamentoID ELSE ExtLancamentoMaeID END) AS ExtLancamentoMaeID, ' +
						'ValorID AS ValorID ' +
						'FROM ExtratosBancarios_Lancamentos WITH(NOLOCK) ' +
						'WHERE (ExtratoID = ' + glb_nExtratoID + ' AND Valor <> 0 ' + sFiltro + ' ) ' +
						'ORDER BY Indice, dtLancamento, ExtLancamentoMaeID, ExtLancamentoID';

        dso.ondatasetcomplete = startDynamicGrids_DSC;
        dso.Refresh();
    }

    if (glb_gridControlFill.dso2_Ref != null) {
        sFiltro = '';

        dso = glb_gridControlFill.dso2_Ref;

        setConnection(dso);

        if (selTipoLancamento.value == 1431)
            sFiltro += ' AND TipoValorID = 1002';
        else {
            if (selTipoLancamento.value == 1432)
                sFiltro += ' AND TipoValorID = 1001';
        }

        if (fgExtrato.Row > 0) {
            var identificador = fgExtrato.TextMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'Identificador*'));

            if ((chkLancamentosPendentes.checked) && (!chkLancamentosSemelhantes.checked))
                sFiltro += ' AND ( (ISNULL(a.Identificador, SPACE(0)) = SPACE(0) ) OR ( \'' + identificador + '\' LIKE a.Identificador + ' + '\'' + '*%' + '\'' + ') OR (dbo.fn_ValorLocalizar_Posicao(a.ValorID, 8) > 0) )';


            if ((chkLancamentosPendentes.checked) && (chkLancamentosSemelhantes.checked)) {
                sFiltro += ' AND ( (ISNULL(a.Identificador, SPACE(0)) = SPACE(0) ) OR ( \'' + identificador + '\' LIKE a.Identificador + ' + '\'' + '*%' + '\'' + ') ' +
    	                   ' OR (\'' + identificador + '\' LIKE a.Identificador ) OR (dbo.fn_ValorLocalizar_Posicao(a.ValorID, 8) > 0) ) ';
            }
        }

        if ((chkLancamentosPendentes.checked) && !((chkLancamentosPendentes.checked) && (chkLancamentosSemelhantes.checked))) {
            sFiltro += ' AND ((ext.ExtratoID IS NULL) OR (dbo.fn_ValorLocalizar_Posicao(a.ValorID, 8) > 0)) ';
            //sFiltro += ' AND (ext.ExtratoID IS NULL OR ((a.FormaPagamentoID = 1035) AND (dbo.fn_ValorLocalizar_Posicao(a.ValorID, 8) > 0))) ';
        }


        if (chkLancamentosSemelhantes.checked) {
            // Marco
            if (fgExtrato.Row > 0) {
                sData = putDateInMMDDYYYY2(fgExtrato.TextMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'dtLancamento*')));
                nValor = fgExtrato.ValueMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'Valor*'));
                sFiltro += ' AND ((CASE a.FormaPagamentoID WHEN 1032 THEN a.dtApropriacao ELSE a.dtEmissao END) BETWEEN DATEADD(dd, -180, ' + '\'' + sData + '\'' + ') AND DATEADD(dd, 6, ' + '\'' + sData + '\'' + ') OR ' +
					'a.dtApropriacao BETWEEN DATEADD(dd, -30, d.dtInicial) AND DATEADD(dd, 4, d.dtFinal)) AND ' +
					'a.Valor = ABS(' + nValor + ') ';
            }
                // Marco
            else {
                startDynamicGrids_DSC();
                return null;
            }

            if (fgExtrato.Row > 0) {
                var nValorID = fgExtrato.TextMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'ValorID*'));
                if (nValorID != '')
                    sFiltro += ' AND a.ValorID = ' + nValorID + ' ';
            }

        }
        else
            sFiltro += ' AND a.dtApropriacao BETWEEN DATEADD(dd, -30, d.dtInicial) AND DATEADD(dd, 4, d.dtFinal) ';

        dso.SQL = 'SELECT DISTINCT a.dtApropriacao AS dtBalancete, a.NumeroDocumento AS NumeroDocumento, ' +
						'CONVERT(NUMERIC(11,2), (a.Valor * POWER(-1, a.TipoValorID))) AS Valor, ' +
                        'CONVERT(NUMERIC(11,2), (dbo.fn_ValorLocalizar_Posicao(a.ValorID, 8) * POWER(-1, a.TipoValorID))) AS Saldo, ' +
						'a.ValorID AS ValorID, CONVERT(BIT, 0) AS OK, c.RecursoAbreviado AS Estado, b.ItemAbreviado AS FormaPagamento, a.BancoAgencia, a.Identificador, CONVERT(BIT, 0) AS Controle, ISNULL(e.ItemMasculino,SPACE(0)) AS Processo ' +
						'FROM ValoresLocalizar a WITH(NOLOCK) ' +
						'LEFT JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (a.FormaPagamentoID = b.ItemId) ' +
						'LEFT JOIN Recursos c WITH(NOLOCK) ON (a.EstadoID = c.RecursoID) ' +
						'LEFT JOIN ExtratosBancarios d WITH(NOLOCK) ON (d.ExtratoID = ' + glb_nExtratoID + ') ' +
						'LEFT JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (e.ItemID = a.ProcessoID) ' +
						'LEFT JOIN ExtratosBancarios_Lancamentos ext WITH(NOLOCK) ON (ext.ValorID = a.ValorID AND ext.NaoConcilia = 0) ' +
						'WHERE (a.EmpresaID = ' + glb_nEmpresaID + ' AND a.EstadoID IN (1,41,53,48) AND ProcessoID <> 1421 AND ' +
						    '((a.FormaPagamentoID = 1033) OR (a.FormaPagamentoID = 1032 AND a.TipoValorID = 1001) OR ((a.FormaPagamentoID = 1035) AND (a.CobrancaID IS NOT NULL))) AND ' +
							'(a.RelPesContaID IS NULL OR a.RelPesContaID = ' + glb_nRelPesContaID + ') AND ' +
							'(a.RelPesContaID IS NOT NULL OR ' +
								'(ISNULL(a.BancoAgencia, SPACE(0)) = SPACE(0) OR ISNULL(a.BancoAgencia, SPACE(0)) LIKE dbo.fn_ContaBancaria_Nome(' + glb_nRelPesContaID + ', 1) + ' + '\'' + '%' + '\'' + ')) AND ' +
							'b.Filtro LIKE ' + '\'' + '%(10140)%' + '\'' + ' ' +
							sFiltro + ') ' +
						'ORDER BY dtBalancete, ValorID ';

        dso.ondatasetcomplete = startDynamicGrids_DSC;
        dso.Refresh();
    }
}

/********************************************************************
Preenche os grids e mostra a janela
********************************************************************/
function startDynamicGrids_DSC() {
    glb_gridControlFill.nDsosToArrive--;

    if (glb_gridControlFill.nDsosToArrive > 0)
        return null;

    var i;
    var dso, grid;
    var dTFormat = '';
    var nTipoID, sColor;
    var bPreencheuExtrato = false;

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd';

    // Eventos dos grids - bloqueia/desbloqueia
    glb_gridsEvents.bBlockEvents_fgExtrato = true;
    glb_gridsEvents.bBlockEvents_fgValoresLocalizar = true;

    if (glb_gridControlFill.dso1_Ref != null) {
        glb_nSaldoExtrato = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'SaldoFinal' + '\'' + '].value ');

        dso = glb_gridControlFill.dso1_Ref;
        grid = glb_gridControlFill.grid1_Ref;

        if (!(dso.recordset.BOF && dso.recordset.EOF)) {
            // move o cursor do dso para o primeiro registro
            dso.recordset.MoveFirst();

            dso.recordset.Filter = 'Indice = 1';
        }

        glb_gridsEvents.bTotalLine_fgExtrato = true;

        grid.ExplorerBar = 0;
        startGridInterface(grid, 1, null);
        grid.FontSize = '7';
        grid.Editable = false;
        grid.BorderStyle = 1;
        bPreencheuExtrato = true;

        headerGrid(grid, ['Data',
		               'Doc',
		               'Valor',
		               'OK',
		               'NC',
		               'Hist�rico',
		               'Identificador',
		               'ExtLancamentoID',
		               'TipoID',
		               'ValorID'], [7, 8]);


        fillGridMask(grid, dso, ['dtLancamento*',
		                       'Documento*',
		                       'Valor*',
		                       'OK',
		                       'NaoConcilia*',
		                       'Historico*',
		                       'Identificador*',
		                       'ExtLancamentoID*',
		                       'TipoID*',
		                       'ValorID*'],
		                       ['', '', '', '', '', '', '', '', '', ''],
		                       ['', '', '(###,###,##0.00)', '', '', '', '', '', '', '']);

        // Coloca tipo de dado nas colunas
        with (grid) {
            ColDataType(0) = 7;
            ColDataType(1) = 8;
            ColDataType(2) = 6;
            ColDataType(3) = 11;
            ColDataType(3) = 11;
            ColDataType(5) = 8;
            ColDataType(6) = 8;
            ColDataType(7) = 3;
            ColDataType(8) = 3;
        }

        if (!(dso.recordset.BOF && dso.recordset.EOF)) {
            dso.recordset.Filter = '';

            // move o cursor do dso para o primeiro registro
            dso.recordset.MoveFirst();
        }

        alignColsInGrid(grid, [2]);



        if ((glb_gridsEvents.ncolUsedToSort_fgExtrato != -1) && (grid.Rows >= 3))
            grid.Select(2, glb_gridsEvents.ncolUsedToSort_fgExtrato);

        if (glb_gridsEvents.ncolOrderUsedToSort_fgExtrato == 1)
            grid.Sort = 1;
        else if (glb_gridsEvents.ncolOrderUsedToSort_fgExtrato == -1)
            grid.Sort = 2;

        // Linha de Totais
        gridHasTotalLine(grid, 'Tot', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(grid, 'Documento*'), '######', 'C'],
                         [getColIndexByColKey(grid, 'Valor*'), '###,###,###,##0.00', 'S']]);

        if (grid.Rows >= 3) {
            if (!((glb_gridControlFill.dso1_Ref.recordset.BOF) && (glb_gridControlFill.dso1_Ref.recordset.EOF))) {
                glb_gridControlFill.dso1_Ref.recordset.MoveFirst();
                glb_nSaldoExtrato = glb_gridControlFill.dso1_Ref.recordset['SaldoFinal'].value;
                glb_gridControlFill.dso1_Ref.recordset.Filter = '';
                glb_gridControlFill.dso1_Ref.recordset.MoveFirst();

            }

            if (glb_nSaldoExtrato != null)
                grid.TextMatrix(1, getColIndexByColKey(grid, 'Valor*')) = glb_nSaldoExtrato;
            else
                grid.TextMatrix(1, getColIndexByColKey(grid, 'Valor*')) = 0;

            grid.FillStyle = 1;
            grid.TextMatrix(1, getColIndexByColKey(grid, 'Documento*')) = grid.Rows - 2;
            grid.Select(1, getColIndexByColKey(grid, 'Documento*'), 1, getColIndexByColKey(grid, 'Documento*'));

            // Alinhamento da celula a direita
            grid.CellAlignment = 7;

            for (i = 1; i < grid.Rows; i++) {
                if (grid == fgExtrato) {
                    nTipoID = parseInt(grid.ValueMatrix(i, getColIndexByColKey(grid, 'TipoID*')), 10);

                    sColor = null;

                    // Amarelo
                    if (nTipoID == 1)
                        sColor = 0X8CE6F0;
                        // Verde
                    else if (nTipoID == 2)
                        sColor = 0X90EE90;
                        // Rosa
                    else if (nTipoID == 3)
                        sColor = 0X7280FA;

                    if (sColor != null)
                        grid.Cell(6, i, getColIndexByColKey(grid, 'Valor*'), i, getColIndexByColKey(grid, 'Valor*')) = sColor;
                }

                grid.Select(i, getColIndexByColKey(grid, 'Valor*'), i, getColIndexByColKey(grid, 'Valor*'));
                if (grid.ValueMatrix(i, getColIndexByColKey(grid, 'Valor*')) < 0)
                    grid.CellForeColor = 0X0000FF;
                else
                    grid.CellForeColor = 0;
            }

        }

        grid.FrozenCols = 4;
        grid.MergeCells = 4;
        grid.MergeCol(0) = true;
        grid.MergeCol(1) = true;

        grid.ExplorerBar = 5;

        if (glb_gridsEvents.bTotalLine_fgExtrato) {
            if (grid.Rows > 2)
                grid.Row = 2;
        }
        else {
            if (grid.Rows > 1)
                grid.Row = 1;
        }

        if ((grid.Rows > 0) && grid.Row > 0) {
            grid.TopRow = grid.Row;

            grid.Col = getColIndexByColKey(grid, 'OK');
        }

        grid.Editable = true;

        adjustWidthCellsInGrid(grid);

        for (i = 3; i <= grid.Rows; i++) {
            glb_nTam = i;
        }


    }

    if (glb_gridControlFill.dso2_Ref != null) {
        dso = glb_gridControlFill.dso2_Ref;
        grid = glb_gridControlFill.grid2_Ref;

        glb_gridsEvents.bTotalLine_fgValoresLocalizar = true;

        grid.ExplorerBar = 0;
        startGridInterface(grid, 1, null);
        grid.FontSize = '7';
        grid.Editable = false;
        grid.BorderStyle = 1;

        headerGrid(grid, ['Data',
		                 'Doc',
		                 'Valor',
                         'Saldo',
		                 'OK',
		                 'ValorID',
		                 'E',
		                 'For',
		                 'Bco/Ag',
		                 'Identificador',
		                 'Controle',
		                 'Processo'], [10]);

        fillGridMask(grid, dso, ['dtBalancete*',
		                       'NumeroDocumento*',
		                       'Valor*',
                               'Saldo*',
		                       'OK',
		                       'ValorID*',
		                       'Estado*',
		                       'FormaPagamento*',
                   		       'BancoAgencia*',
		                       'Identificador*',
		                       'Controle',
		                       'Processo*'],
		                       ['', '', '', '', '', '', '', '', '', '', '', ''],
		                       [dTFormat, '', '(###,###,##0.00)', '(###,###,##0.00)', '', '', '', '', '', '', '', '']);

        if (!(dso.recordset.BOF && dso.recordset.EOF)) {
            dso.recordset.Filter = '';

            // move o cursor do dso para o primeiro registro
            dso.recordset.MoveFirst();
        }

        alignColsInGrid(grid, [2, 4]);

        // Coloca tipo de dado nas colunas
        with (grid) {
            ColDataType(0) = 7;
            ColDataType(1) = 8;
            ColDataType(2) = 6;
            ColDataType(3) = 6;
            ColDataType(4) = 11;
            ColDataType(5) = 3;
            ColDataType(6) = 8;
            ColDataType(7) = 8;
            ColDataType(9) = 8;
            ColDataType(10) = 8;
        }

        if ((glb_gridsEvents.ncolUsedToSort_fgValoresLocalizar != -1) && (grid.Rows >= 3))
            grid.Select(2, glb_gridsEvents.ncolUsedToSort_fgValoresLocalizar);

        if (glb_gridsEvents.ncolOrderUsedToSort_fgValoresLocalizar == 1)
            grid.Sort = 1;
        else if (glb_gridsEvents.ncolOrderUsedToSort_fgValoresLocalizar == -1)
            grid.Sort = 2;

        // Linha de Totais
        gridHasTotalLine(grid, 'Tot', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(grid, 'NumeroDocumento*'), '######', 'C'],
                         [getColIndexByColKey(grid, 'Valor*'), '###,###,###,##0.00', 'S']]);


        if (grid.Rows > 1) {
            grid.TextMatrix(1, getColIndexByColKey(grid, 'NumeroDocumento*')) = 0;
            grid.TextMatrix(1, getColIndexByColKey(grid, 'Valor*')) = 0;
        }

        if (grid.Rows >= 3) {
            grid.FillStyle = 1;
            grid.TextMatrix(1, getColIndexByColKey(grid, 'NumeroDocumento*')) = grid.Rows - 2;
            grid.Select(1, getColIndexByColKey(grid, 'NumeroDocumento*'), 1, getColIndexByColKey(grid, 'NumeroDocumento*'));

            // Alinhamento da celula a direita
            grid.CellAlignment = 7;

            for (i = 1; i < grid.Rows; i++) {
                grid.Select(i, getColIndexByColKey(grid, 'Valor*'), i, getColIndexByColKey(grid, 'Valor*'));
                if (grid.ValueMatrix(i, getColIndexByColKey(grid, 'Valor*')) < 0)
                    grid.CellForeColor = 0X0000FF;
                else
                    grid.CellForeColor = 0;

            }
        }

        grid.FrozenCols = 3;
        grid.MergeCells = 4;
        grid.MergeCol(0) = true;

        grid.ExplorerBar = 5;

        if (glb_gridsEvents.bTotalLine_fgValoresLocalizar) {
            if (grid.Rows > 2)
                grid.Row = 2;
        }
        else {
            if (grid.Rows > 1)
                grid.Row = 1;
        }

        if ((grid.Rows > 0) && grid.Row > 0) {
            grid.TopRow = grid.Row;
            grid.Col = getColIndexByColKey(grid, 'OK');
        }
        grid.Editable = true;

        adjustWidthCellsInGrid(grid);
    }

    // Reinicializa objeto que controla dsos e grids
    glb_gridControlFill.bFollowOrder = false;
    glb_gridControlFill.nDsosToArrive = 0;
    glb_gridControlFill.dso1_Ref = null;
    glb_gridControlFill.dso2_Ref = null;
    glb_gridControlFill.grid1_Ref = null;
    glb_gridControlFill.grid2_Ref = null;

    // Eventos dos grids - bloqueia/desbloqueia
    glb_gridsEvents.bBlockEvents_fgExtrato = false;
    glb_gridsEvents.bBlockEvents_fgValoresLocalizar = false;

    // Mostra janela se for o caso
    if (glb_FirstTime) {
        glb_FirstTime = false;
        // A janela esta carregando
        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 2;
        glb_gridControlFill.dso1_Ref = dsoGridExtrato;
        glb_gridControlFill.dso2_Ref = dsoGridValoresLocalizar;
        glb_gridControlFill.grid1_Ref = fgExtrato;
        glb_gridControlFill.grid2_Ref = fgValoresLocalizar;
        adjustLabelsCombos();
        showModalAfterDataLoad();
    }
    else
        lockControlsInModalWin(false);

    controlsInterfaceState();

    if (fgExtrato.Rows > 2) {
        fgExtrato.focus();

        if (bPreencheuExtrato)
            glb_TimerCallRowColChange = window.setInterval('callAfterRowColChange()', 500, 'JavaScript');
    }
}

function callAfterRowColChange() {
    if (glb_TimerCallRowColChange != null) {
        window.clearInterval(glb_TimerCallRowColChange);
        glb_TimerCallRowColChange = null;
    }

    glb_gridsEvents.bBlockEvents_fgExtrato = false;
    js_fg_ModConcExtrAfterRowColChangePesqList(fgExtrato, fgExtrato.Row - 1, fgExtrato.Col, fgExtrato.Row, fgExtrato.Col);
}

/********************************************************************
Ajusta largura de colunas dos grid
********************************************************************/
function adjustWidthCellsInGrid(grid) {
    var nCurrRow = 0;
    var sLabel1;

    sLabel1 = grid.TextMatrix(0, getColIndexByColKey(grid, 'Valor*'));

    if (grid.Row > 0)
        nCurrRow = grid.Row;

    grid.Redraw = 0;

    grid.TextMatrix(0, getColIndexByColKey(grid, 'Valor*')) = '9999999';

    grid.AutoSizeMode = 0;
    grid.AutoSize(0, grid.Cols - 1);

    grid.TextMatrix(0, getColIndexByColKey(grid, 'Valor*')) = sLabel1;

    grid.Redraw = 2;
}

/********************************************************************
Preenche os grids e mostra a janela
********************************************************************/
function geraValorLocalizar() {
    var sErro = '';

    var sHistoricoComplementar = '';
    var sErro = '';
    var bEhEstorno = chkEhEstorno.checked;
    var nTipoID = null;
    var nProcessoID = 0;
    var nHistoricoPadraoID = 0;


    nProcessoID = selProcessoID.value;
    nHistoricoPadraoID = selHistoricoPadraoID.value;

    if (nProcessoID == 0)
        sErro += 'Escolha o Processo do Valor a Localizar' + '\n';

    if (sErro != '') {
        if (window.top.overflyGen.Alert(sErro) == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
        return null;
    }
    else {
        var _retConf = window.top.overflyGen.Confirm('Gerar Valor a Localizar?');

        if (_retConf == 0)
            return null;
        else if (_retConf == 1)
            execSPValorLocalizar_Gerador();
        else {
            lockControlsInModalWin(false);
            return null;
        }
    }
}
/********************************************************************
Funcao do programador
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function execSPValorLocalizar_Gerador() {
    lockControlsInModalWin(true);

    var strPars = new String();
    var nCounter = 0;
    var sErro_ = '';
    var sErro__ = '';

    strPars = '';

    var nValorExtrato = 0;
    var nTipoLancamentoID = 0;
    var bEhEstorno = false;
    var nExtLancamentoID = 0;
    var nFormaPagamentoID = 0;
    var sMotivoDevolucao = '';
    var nUsuarioID = 0;
    var nProcessoID = 0;
    var nHistoricoPadraoID = 0;
    var _nValorID = 0;
    var sObservacao = '';


    nProcessoID = selProcessoID.value;
    nHistoricoPadraoID = selHistoricoPadraoID.value;
    bEhEstorno = (chkEhEstorno.checked ? 1 : 0);
    nUsuarioID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    sObservacao = txtObservacao.value;

    for (i = 1; i < fgExtrato.Rows; i++) {
        if ((fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'OK')) != 0) && (fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'NaoConcilia*')) == 0)) {
            nValorExtrato = fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'Valor*'));
            _nValorID = 0;
            _nValorID = fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'ValorID*'));

            if (nValorExtrato > 0)
                nTipoLancamentoID = 1572; //Rec
            else
                nTipoLancamentoID = 1571; //Pag

            if ((nValorExtrato > 0) && ((nTipoLancamentoID == 1571 && !bEhEstorno) || (nTipoLancamentoID == 1572 && bEhEstorno))) {
                sErro_ += 'Tipo do lan�amento ' + escape(getCellValueByColKey(fgExtrato, 'ExtLancamentoID*', i));
                sErro_ += ' incompat�vel com o valor do extrato.\n';
            } else if ((nValorExtrato < 0) && ((nTipoLancamentoID == 1572 && !bEhEstorno) || (nTipoLancamentoID == 1571 && bEhEstorno))) {
                sErro_ += 'Tipo do lan�amento ' + escape(getCellValueByColKey(fgExtrato, 'ExtLancamentoID*', i));
                sErro_ += ' incompat�vel com o valor do extrato.\n';
            }

            if (_nValorID != 0) {
                sErro_ += 'Lan�amento ' + escape(getCellValueByColKey(fgExtrato, 'ExtLancamentoID*', i));
                sErro_ += ' j� conciliado com VL.\n';
            }

            if (sErro_ == '') {
                if (nCounter == 0)
                    strPars += '?';
                else
                    strPars += '&';

                strPars += 'nExtLancamentoID=' + escape(getCellValueByColKey(fgExtrato, 'ExtLancamentoID*', i));
                nCounter++;
            }
            sErro__ += sErro_;
            sErro_ = '';
        }
    }

    if (nCounter == 0)
        sErro__ += 'Selecione algum Lan�amento que n�o esteja associado';

    /*if (nCounter >= 50)
	    sErro__ += 'S� � poss�vel criar 50 lan�amentos por vez';*/

    if (sErro__ != '') {
        window.top.overflyGen.Alert(sErro__);

        lockControlsInModalWin(false);
        return null;
    }

    strPars += '&nProcessoID=' + escape(nProcessoID);
    strPars += '&nHistoricoPadraoID=' + escape(nHistoricoPadraoID);
    strPars += '&bEhEstorno=' + escape(bEhEstorno);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);
    strPars += '&sObservacao=' + escape(sObservacao);


    dsoGerarValorLocalizar.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/geravalorlocalizar.aspx' + strPars;
    dsoGerarValorLocalizar.ondatasetcomplete = execSPValorLocalizar_Gerador_DSC;
    dsoGerarValorLocalizar.refresh();
}

/********************************************************************
Funcao do programador
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function execSPValorLocalizar_Gerador_DSC() {
    lockControlsInModalWin(false);
    var nValorID, sResultado;

    if (!(dsoGerarValorLocalizar.recordset.BOF && dsoGerarValorLocalizar.recordset.EOF)) {
        sResultado = dsoGerarValorLocalizar.recordset['Resultado'].value;

        if ((sResultado != null) && (sResultado != '')) {
            if (window.top.overflyGen.Alert(sResultado) == 0)
                return null;
        }
        else {
            nValorID = dsoGerarValorLocalizar.recordset['ValorID'].value;

            window.top.overflyGen.Alert('Valor(es) a Localizar gerado(s)');

            // Marco
            fgExtrato.Rows = 1;
            fgValoresLocalizar.Rows = 1;

        }

        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 2;
        glb_gridControlFill.dso1_Ref = dsoGridExtrato;
        glb_gridControlFill.dso2_Ref = dsoGridValoresLocalizar;
        glb_gridControlFill.grid1_Ref = fgExtrato;
        glb_gridControlFill.grid2_Ref = fgValoresLocalizar;
        chkEhEstorno.checked = false;
        startDynamicGrids();
    }
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad() {
    // ajusta o body do html
    with (modalassociavalorlocalizarBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    window.focus();

    interfaceControlsState();
}

/********************************************************************
Inverte o check de um checkbox associado a um label
********************************************************************/
function invertChkBox(ctl) {
    if (ctl.disabled == true)
        return;

    ctl.checked = !ctl.checked;

    return true;
}

/********************************************************************
Altera o estado dos controles da interface
********************************************************************/
function interfaceControlsState() {
    ;
}

/********************************************************************
Evento onchange dos combos
********************************************************************/
function cmbOnChange(cmb) {
    if ((cmb.id).toUpperCase() == 'SELPROCESSOID') {
        adjustLabelsCombos();
    }
    else if ((cmb.id).toUpperCase() == 'SELTIPOLANCAMENTO') {
        adjustLabelsCombos();
        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 2;
        glb_gridControlFill.dso1_Ref = dsoGridExtrato;
        glb_gridControlFill.dso2_Ref = dsoGridValoresLocalizar;
        glb_gridControlFill.grid1_Ref = fgExtrato;
        glb_gridControlFill.grid2_Ref = fgValoresLocalizar;
        startDynamicCmbs();
        startDynamicGrids();
    }
    else if ((cmb.id).toUpperCase() == 'SELHISTORICOPADRAOID') {
        adjustLabelsCombos();
    }
}

/********************************************************************
Onclick dos checkbox
********************************************************************/
function chkOnClick() {
    if (((this.id).toUpperCase() == 'CHKLANCAMENTOSPENDENTES')) {
        // A janela esta carregando
        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 2;
        glb_gridControlFill.dso1_Ref = dsoGridExtrato;
        glb_gridControlFill.dso2_Ref = dsoGridValoresLocalizar;
        glb_gridControlFill.grid1_Ref = fgExtrato;
        glb_gridControlFill.grid2_Ref = fgValoresLocalizar;

        startDynamicGrids();
    }
    else if ((this.id).toUpperCase() == 'CHKLANCAMENTOSSEMELHANTES') {
        if (fgExtrato.Rows > 1) {
            // A janela esta carregando
            glb_gridControlFill.bFollowOrder = false;
            glb_gridControlFill.nDsosToArrive = 1;
            glb_gridControlFill.dso1_Ref = null;
            glb_gridControlFill.dso2_Ref = dsoGridValoresLocalizar;
            glb_gridControlFill.grid1_Ref = null;
            glb_gridControlFill.grid2_Ref = fgValoresLocalizar;

            startDynamicGrids();
        }
        else
            fgValoresLocalizar.Rows = 1;
    }
    else if ((this.id).toUpperCase() == 'CHKUSOCOTIDIANO') {
        startDynamicCmbs();
    }
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function js_fg_ModConcExtrBeforeRowColChange(grid, oldRow, oldCol, newRow, newCol) {
    if (glb_gridsEvents.bBlockEvents_fgExtrato)
        return true;

    if (glb_gridsEvents.bBlockEvents_fgValoresLocalizar)
        return true;
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function js_fg_ModConcExtrAfterRowColChangePesqList(grid, oldRow, oldCol, newRow, newCol) {
    // grid executa evento
    if ((grid == fgExtrato) && (glb_gridsEvents.bBlockEvents_fgExtrato))
        return true;

    if ((grid == fgValoresLocalizar) && (glb_gridsEvents.bBlockEvents_fgValoresLocalizar))
        return true;

    // grid tem primeira linha de totalizacao
    // impede a mesma de ser selecionada 
    if ((grid == fgExtrato) && (glb_gridsEvents.bTotalLine_fgExtrato)) {
        if (newRow == 1) {
            if (grid.Rows > 2)
                grid.Row = 2;
        }
    }

    // grid tem primeira linha de totalizacao
    // impede a mesma de ser selecionada 
    if ((grid == fgValoresLocalizar) && (glb_gridsEvents.bTotalLine_fgValoresLocalizar)) {
        if (newRow == 1) {
            if (grid.Rows > 2)
                grid.Row = 2;
        }
    }

    // grid e o de extrato e o de lancamentos tem linhas
    if (grid == fgExtrato) {
        if (oldRow != newRow) {
            if (glb_gridsEvents.bTotalLine_fgValoresLocalizar)
                lineChangedInGrid_Extrato(grid, newRow);
        }
    }

    // grid e o de lancamentos e o de extratos tem linhas
    if (grid == fgValoresLocalizar) {
        if (oldRow != newRow) {
            if (glb_gridsEvents.bTotalLine_fgExtrato)
                lineChangedInGrid_ValoresLocalizar(grid, newRow);
        }
    }

    controlsInterfaceState();

    if (grid == fgExtrato && !glb_SortConc)
    {
        if (newCol == getColIndexByColKey(fgExtrato, 'NaoConcilia*'))
        {
            if (newCol > 1)
                fgExtrato.TextMatrix(newRow, newCol) = 0;

            // conciliaExtrato();
        }
    }
    else
        glb_SortConc = false;

}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function fg_ModConcExtrBeforeSort(grid, col) {
    glb_LASTLINESELID = '';

    if (grid.Row > 0) {
        if (grid == fgExtrato)
            glb_LASTLINESELID = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'ExtLancamentoID*'));
        else if (grid == fgValoresLocalizar)
            glb_LASTLINESELID = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'ValorID*'));
    }
    else
        glb_LASTLINESELID = '';

    glb_SortConc = true;
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function fg_ModConcExtrAfterSort(grid, col) {
    var i, keyToSelectRow, bBlockEvents;

    keyToSelectRow = '';
    gridSortParams(grid, col);
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function js_fg_ModConcExtrDblClick(grid, row, col) {
    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    glb_PassedOneInDblClick = true;

    if (row > 1) {
        if ((grid == fgValoresLocalizar) && (!btnAssociar.disabled))
            btn_onclick(btnAssociar);
    }

    //Seleciona todos os OK's
    if (grid == fgExtrato) {
        // trap (grid esta vazio)
        if (grid.Rows <= 1)
            return true;

        // trap so colunas editaveis
        if (getColIndexByColKey(grid, 'OK') != col)
            return true;

        // trap so header
        if (row != 2)
            return true;

        var i;
        var bFill = true;

        grid.Editable = false;
        grid.Redraw = 0;
        lockControlsInModalWin(true);

        glb_PassedOneInDblClick = true;

        // limpa coluna se tem um check box checado
        for (i = 2; i < grid.Rows; i++) {
            if (grid.ValueMatrix(i, col) != 0) {
                bFill = false;
                break;
            }
        }

        for (i = 2; i < grid.Rows; i++) {
            if (bFill)
                grid.TextMatrix(i, col) = 1;
            else
                grid.TextMatrix(i, col) = 0;
        }

        lockControlsInModalWin(false);
        grid.Editable = true;
        grid.Redraw = 2;
        controlsInterfaceState();
        window.focus();
        grid.focus();
    }

}
/********************************************************************
Examina sort order dos grids e guarda parametros
********************************************************************/
function gridSortParams(grid, col) {
    // nColDataType = 0 (String), 1 (Numerico), 2 (Data), 3 (Boolean)

    var i_init;

    var dataFirstRow = 0;
    var dataLastRow = 0;
    var dataDif = 0;
    var dataType = 0;
    var ncolOrderUsedToSort = -1;

    if (grid == fgExtrato) {
        if (glb_gridsEvents.bTotalLine_fgExtrato)
            i_init = 2;
        else
            i_init = 1;

        glb_gridsEvents.ncolUsedToSort_fgExtrato = col;

        // inicialmente considera sempre sort ascendente
        glb_gridsEvents.ncolOrderUsedToSort_fgExtrato = 1;

    }

    if (grid == fgValoresLocalizar) {
        if (glb_gridsEvents.bTotalLine_fgValoresLocalizar)
            i_init = 2;
        else
            i_init = 1;

        glb_gridsEvents.ncolUsedToSort_fgValoresLocalizar = col;

        // inicialmente considera sempre sort ascendente
        glb_gridsEvents.ncolOrderUsedToSort_fgValoresLocalizar = 1;
    }

    dataType = grid.ColDataType(col);

    if (grid.Rows > i_init) {
        // data ou string
        if ((dataType == 7) ||
		     (dataType == 8) ||
		     (dataType == 30) ||
		     (dataType == 31)) {
            dataFirstRow = '';
            dataLastRow = '';
            dataFirstRow = grid.TextMatrix(i_init, col);
            dataLastRow = grid.TextMatrix((grid.Rows - 1), col);
        }
            // numericos
        else {
            dataFirstRow = 0;
            dataLastRow = 0;
            dataFirstRow = grid.ValueMatrix(i_init, col);
            dataLastRow = grid.ValueMatrix((grid.Rows - 1), col);
        }
    }

    if (grid.Rows > (i_init + 1)) {
        // String 8, 30, 31
        if ((dataType == 8) || (dataType == 30) || (dataType == 31)) {
            if (dataFirstRow == null)
                dataFirstRow = '';
            if (dataLastRow == null)
                dataLastRow = '';

            if ((dataFirstRow == '') && (dataLastRow == ''))
                ncolOrderUsedToSort = 1;
            else if ((dataFirstRow != '') && (dataLastRow == ''))
                ncolOrderUsedToSort = -1;
            else if ((dataFirstRow == '') && (dataLastRow != ''))
                ncolOrderUsedToSort = 1;
            else {
                if (dataFirstRow < dataLastRow)
                    ncolOrderUsedToSort = 1;
                else
                    ncolOrderUsedToSort = -1;
            }
        }
            // Numero 2, 3, 4, 5, 6, 14, 20
        else if ((dataType == 2) || (dataType == 3) ||
		          (dataType == 4) || (dataType == 5) ||
		          (dataType == 6) || (dataType == 14) ||
		          (dataType == 20)) {
            if (dataFirstRow == null)
                dataFirstRow = 0;
            if (dataLastRow == null)
                dataLastRow = 0;

            dataDif = dataLastRow - dataFirstRow;

            if (dataDif >= 0)
                ncolOrderUsedToSort = 1;
            else
                ncolOrderUsedToSort = -1;
        }
            // Data 7
        else if (dataType == 7) {
            if (dataFirstRow == null)
                dataFirstRow = '';
            if (dataLastRow == null)
                dataLastRow = '';

            if ((dataFirstRow == '') && (dataLastRow == ''))
                ncolOrderUsedToSort = 1;
            else if ((dataFirstRow != '') && (dataLastRow == ''))
                ncolOrderUsedToSort = -1;
            else if ((dataFirstRow == '') && (dataLastRow != ''))
                ncolOrderUsedToSort = 1;
            else {
                dataDif = window.top.daysBetween(dataFirstRow, dataLastRow);

                if (dataDif >= 0)
                    ncolOrderUsedToSort = 1;
                else
                    ncolOrderUsedToSort = -1;
            }
        }
    }

    if (grid == fgExtrato)
        glb_gridsEvents.ncolOrderUsedToSort_fgExtrato = ncolOrderUsedToSort;
    else if (grid == fgValoresLocalizar)
        glb_gridsEvents.ncolOrderUsedToSort_fgValoresLocalizar = ncolOrderUsedToSort;

}

/********************************************************************
Usuario mudou a linha do grid de extratos
********************************************************************/
function lineChangedInGrid_Extrato(grid, nRow) {
    var i, i_init;

    var nValor = fgExtrato.ValueMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'Valor*'));
    var nValorID = 0;
    var sYellowColor = 0X8CE6F0;
    var sReadOnlyColor = 0XDCDCDC;
    var sColor = '';
    var nRow = -1;
    var nTopRow = 0;
    var bFirst = false;
    var _nValorID = trimStr(fgExtrato.TextMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'ValorID*')));

    if (chkLancamentosSemelhantes.checked) {
        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 1;
        glb_gridControlFill.dso1_Ref = null;
        glb_gridControlFill.dso2_Ref = dsoGridValoresLocalizar;
        glb_gridControlFill.grid1_Ref = null;
        glb_gridControlFill.grid2_Ref = fgValoresLocalizar;
        startDynamicGrids();
    }
    else {
        if (glb_gridsEvents.bTotalLine_fgValoresLocalizar) {
            i_init = 2;
            if (fgValoresLocalizar.Rows <= 2)
                return true;
        }
        else
            i_init = 1;

        glb_gridsEvents.bBlockEvents_fgValoresLocalizar = true;

        for (i = i_init; i < fgValoresLocalizar.Rows; i++) {
            nValorID = trimStr(fgValoresLocalizar.TextMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'ValorID*')));
            if (nValorID == _nValorID) {
                sColor = sYellowColor;
                fgValoresLocalizar.TextMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'Controle')) = true;
                if (!bFirst) {
                    nRow = i;
                    nTopRow = i;
                }

                bFirst = true;
            }
            else {
                sColor = sReadOnlyColor;
                fgValoresLocalizar.TextMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'Controle')) = false;
            }

            fgValoresLocalizar.Cell(6, i, getColIndexByColKey(fgValoresLocalizar, 'Identificador*'), i, getColIndexByColKey(fgValoresLocalizar, 'Identificador*')) = sColor;
        }

        if (nRow > 0)
            fgValoresLocalizar.Row = nRow;

        if (nTopRow > 0)
            fgValoresLocalizar.TopRow = nTopRow;

        fgValoresLocalizar.Col = getColIndexByColKey(fgValoresLocalizar, 'OK');

        glb_gridsEvents.bBlockEvents_fgValoresLocalizar = false;
    }
}

/********************************************************************
Usuario mudou a linha do grid de lancamento de contas
********************************************************************/
function lineChangedInGrid_ValoresLocalizar(grid, nRow) {
    ;
}

/********************************************************************
Controla botoes, checkbox, combos inputs etc
********************************************************************/
function controlsInterfaceState() {
    var i_init = 0;
    var i_init2 = 0;
    var nTipoID = 0;
    var nValorExtrato = 0;
    var nCounter = 0;
    var nValor = 0;
    var nSaldo = 0;
    var nValorUnico = 0;
    var nCounter3 = 0;
    var nValorTotalExtrato = 0;

    if (fgValoresLocalizar.Rows > 1)
        setVarDissociar(fgValoresLocalizar, fgValoresLocalizar.Row, fgValoresLocalizar.Col);

    for (i = 1; i < fgValoresLocalizar.Rows; i++) {
        if ((fgValoresLocalizar.ValueMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'OK')) != 0) &&
	             (fgValoresLocalizar.TextMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'Identificador*')) != '')) {
            nCounter3++;
        }
    }
    if (nCounter3 >= 1)
        btnLimparEx.disabled = false;
    else
        btnLimparEx.disabled = true;


    if (glb_gridsEvents.bTotalLine_fgExtrato)
        i_init = 2;
    else
        i_init = 1;

    for (i = i_init; i < fgValoresLocalizar.Rows; i++) {
        if (fgValoresLocalizar.ValueMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'Controle')) != 0) {
            if ((fgValoresLocalizar.ValueMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'OK')) == 0) && (fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'NaoConcilia*')) == 0)) {
                nCounter++;
                nValor += fgValoresLocalizar.ValueMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'Valor*'));
                nSaldo += fgValoresLocalizar.ValueMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'Saldo*'));
            }
        }
        else if (fgValoresLocalizar.ValueMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'OK')) != 0) {
            nCounter++;
            nValor += fgValoresLocalizar.ValueMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'Valor*'));
            nSaldo += fgValoresLocalizar.ValueMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'Saldo*'));
        }
    }

    if (fgValoresLocalizar.Rows > 1) {
        fgValoresLocalizar.TextMatrix(1, getColIndexByColKey(fgValoresLocalizar, 'NumeroDocumento*')) = nCounter;
        fgValoresLocalizar.TextMatrix(1, getColIndexByColKey(fgValoresLocalizar, 'Valor*')) = nValor;
        fgValoresLocalizar.TextMatrix(1, getColIndexByColKey(fgValoresLocalizar, 'Saldo*')) = nSaldo;
    }

    if (fgExtrato.Row >= i_init) {
        nTipoID = fgExtrato.ValueMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'TipoID*'));
        nValorExtrato = fgExtrato.ValueMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'Valor*'));
    }

    for (i = i_init; i < fgExtrato.Rows; i++)
    {
        if (fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'OK')) != 0)
        {
            nValorTotalExtrato += fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'Valor*'));
        }
    }

    if (fgExtrato.Rows > 1)
    {
        fgExtrato.TextMatrix(1, getColIndexByColKey(fgExtrato, 'Valor*')) = nValorTotalExtrato;
    }

    //Esconder e Mostrar bot�es associar e gerar
    i_init2 = 1;
    var nCounter2 = 0;
    var nCounter3 = 0;
    for (i = i_init2; i < fgExtrato.Rows; i++) {
        if (fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'OK')) != 0) {
            nCounter3++;
            nValorUnico = fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'ExtLancamentoID*'));
        }
    }
    nValorUnico = 0;
    nCounter2 = 0;
    for (i = i_init2; i < fgExtrato.Rows; i++) {
        if ((fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'OK')) != 0) && (fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'NaoConcilia*')) == 0)) {
            nCounter2++;
            nValorUnico = fgExtrato.ValueMatrix(i, getColIndexByColKey(fgExtrato, 'ValorID*'));
        }

    }

    if (nCounter3 == 1) {
        //Btn Dissociar
        if ((nCounter2 == 1) && (nValorUnico != 0))
            btnLimpar.disabled = false;
        else
            btnLimpar.disabled = true;

        //Btn Associar
        if ((nCounter2 == 1) && (nValorUnico == 0)) {
            if (nCounter >= 1)
                btnAssociar.disabled = false;
            else
                btnAssociar.disabled = true;
        }
        else
            btnAssociar.disabled = true;
    }
    else if (nCounter3 == 0)
    {
        btnLimpar.disabled = true;
    }
    else
    {
        btnAssociar.disabled = true;
    }

    glb_nCounterVL = nCounter;
    glb_nCounterExtrato = nCounter3;

    if (((nCounter3 >= 1) && (nCounter >= 1)) && (!((nCounter3 > 1) && (nCounter > 1))))
        btnAssociar.disabled = false;
    else
        btnAssociar.disabled = true;

    if ((nCounter2 >= 1) && ((nCounter == 0) || (nCounter == null))) {
        btnGerarValorLocalizar.style.visibility = 'inherit';
        btnGerarValorLocalizar.disabled = false;
    } else {
        btnGerarValorLocalizar.style.visibility = 'hidden';
        btnGerarValorLocalizar.disabled = true;
    }

    adjustLabelsCombos();
    txtObservacao.value = '';

    if (btnGerarValorLocalizar.disabled == true) {
        lblProcessoID.style.visibility = 'hidden';
        lblProcessoID.disabled = true;
        selProcessoID.style.visibility = 'hidden';
        selProcessoID.disabled = true;
        selProcessoID.value = 0;
        lblHistoricoPadraoID.style.visibility = 'hidden';
        lblHistoricoPadraoID.disabled = true;
        selHistoricoPadraoID.style.visibility = 'hidden';
        selHistoricoPadraoID.disabled = true;
        selHistoricoPadraoID.value = 0;
        lblObservacao.style.visibility = 'hidden';
        lblObservacao.disabled = true;
        txtObservacao.style.visibility = 'hidden';
        txtObservacao.disabled = true;
        chkUsoCotidiano.style.visibility = 'hidden';
        chkUsoCotidiano.disabled = true;
        chkUsoCotidiano.checked = true;
        lblUsoCotidiano.style.visibility = 'hidden';
        lblUsoCotidiano.disabled = true;
        chkEhEstorno.style.visibility = 'hidden';
        chkEhEstorno.disabled = true;
        chkEhEstorno.checked = false;
        lblEhEstorno.style.visibility = 'hidden';
        lblEhEstorno.disabled = true;
    }
    else {
        lblProcessoID.style.visibility = 'inherit';
        lblProcessoID.disabled = false;
        selProcessoID.style.visibility = 'inherit';
        selProcessoID.disabled = false;
        lblHistoricoPadraoID.style.visibility = 'inherit';
        lblHistoricoPadraoID.disabled = false;
        selHistoricoPadraoID.style.visibility = 'inherit';
        selHistoricoPadraoID.disabled = false;
        lblObservacao.style.visibility = 'inherit';
        lblObservacao.disabled = false;
        txtObservacao.style.visibility = 'inherit';
        txtObservacao.disabled = false;
        chkUsoCotidiano.style.visibility = 'inherit';
        chkUsoCotidiano.disabled = false;
        lblUsoCotidiano.style.visibility = 'inherit';
        lblUsoCotidiano.disabled = false;
        chkEhEstorno.style.visibility = 'inherit';
        chkEhEstorno.disabled = false;
        lblEhEstorno.style.visibility = 'inherit';
        lblEhEstorno.disabled = false;
    }
}

function adjustLabelsCombos(bPaging) {
    setLabelOfControl(lblProcessoID, selProcessoID.value);
    setLabelOfControl(lblHistoricoPadraoID, selHistoricoPadraoID.value);
    //setLabelOfControl(lblTipoLancamento, selTipoLancamento.value);
}

function fgValoresLocalizar_AfterEdit(grid, row, col) {
    controlsInterfaceState();
}

function setVarDissociar(grid, row, col) {
    var i;
    var bOK = false;
    var bControle = false;
    var sValor = '';

    if (fgExtrato.Rows > 1 && fgValoresLocalizar.Rows > 1) {
        if (col == getColIndexByColKey(fgValoresLocalizar, 'OK')) {
            if (fgValoresLocalizar.ValueMatrix(row, getColIndexByColKey(fgValoresLocalizar, 'OK')) != 0)
                bOK = true;

            if (fgValoresLocalizar.ValueMatrix(row, getColIndexByColKey(fgValoresLocalizar, 'Controle')) != 0)
                bControle = true;
            sValor = fgExtrato.TextMatrix(fgExtrato.Row, getColIndexByColKey(fgExtrato, 'ValorID*'));

            if (bOK) {
                if (!bControle) {
                    if ((glb_bDissociar == null) && (sValor == ''))
                        glb_bDissociar = false;
                    else if (((glb_bDissociar == null) && (sValor != '')) || (glb_bDissociar == true)) {
                        glb_bDissociar = true;
                    }
                }
                else {
                    if (glb_bDissociar == null)
                        glb_bDissociar = true;
                    else if (glb_bDissociar == false) {
                        glb_bDissociar = true;
                    }
                }
            }
            else {
                glb_bDissociar = null;

                for (i = 1; i < fgValoresLocalizar.Rows; i++) {
                    if (fgValoresLocalizar.ValueMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'OK')) != 0) {
                        if (fgValoresLocalizar.ValueMatrix(i, getColIndexByColKey(fgValoresLocalizar, 'Controle')) != 0)
                            glb_bDissociar = true;
                        else
                            glb_bDissociar = false;
                        break;
                    }
                }
            }

            //controlsInterfaceState();
        }
    }
}

/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos combos
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs() {
    var sFiltro = '';

    if (!glb_FirstTime)
        lockControlsInModalWin(true);

    if (chkUsoCotidiano.checked)
        sFiltro = ' AND a.UsoCotidiano = 1 ';

    if (selTipoLancamento.value == 1431)
        sFiltro += ' AND a.TipoLancamentoID = 1572 ';
    else if (selTipoLancamento.value == 1432)
        sFiltro += ' AND a.TipoLancamentoID = 1571 ';
    else
        sFiltro += ' ';

    glb_CounterCmbsDynamics = 1;

    var dsoCmbDynamic01 = dsoHistoricoPadrao;

    setConnection(dsoCmbDynamic01);

    dsoCmbDynamic01.SQL = 'SELECT DISTINCT 0 as fldIndex, 0 as fldID, SPACE(0) as fldName, SPACE(0) AS HistoricoComplementar ' +
                          'UNION ALL ' +
                          'SELECT DISTINCT 1 as fldIndex, a.HistoricoPadraoID as fldID, a.HistoricoPadrao as fldName, ' +
	                      'ISNULL(a.HistoricoComplementar, SPACE(0)) AS HistoricoComplementar ' +
	                      'FROM HistoricosPadrao a WITH(NOLOCK), HistoricosPadrao_Contas b WITH(NOLOCK) ' +
	                      'WHERE (a.EstadoID = 2 AND a.HistoricoPadraoID = b.HistoricoPadraoID AND ' +
							'((dbo.fn_HistoricoPadrao_ConciliacaoBancaria(a.HistoricoPadraoID, 1571, 1) = 1) OR ' +
							'(dbo.fn_HistoricoPadrao_ConciliacaoBancaria(a.HistoricoPadraoID, 1572, 1) = 1)) ' +
							sFiltro +
							') ' +
						  'ORDER BY fldName';

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();
}

/********************************************************************
Funcao disparada pelo programado.
Preenchimento dos combos
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    var optionStr, optionValue;
    var aCmbsDynamics = [selHistoricoPadraoID];
    var aDSOsDynamics = [dsoHistoricoPadrao];
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var i;
    var lFirstRecord;
    var nQtdCmbs = 1;

    // Inicia o carregamento de combos dinamicos (selHistoricoPadraoID)
    clearComboEx(['selHistoricoPadraoID']);

    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics == 0) {
        for (i = 0; i < nQtdCmbs; i++) {
            oldDataSrc = aCmbsDynamics[i].dataSrc;
            oldDataFld = aCmbsDynamics[i].dataFld;
            aCmbsDynamics[i].dataSrc = '';
            aCmbsDynamics[i].dataFld = '';
            lFirstRecord = true;
            while (!aDSOsDynamics[i].recordset.EOF) {
                optionStr = aDSOsDynamics[i].recordset['fldName'].value;
                optionValue = aDSOsDynamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[i].add(oOption);
                aDSOsDynamics[i].recordset.MoveNext();
                lFirstRecord = false;
            }
            aCmbsDynamics[i].dataSrc = oldDataSrc;
            aCmbsDynamics[i].dataFld = oldDataFld;
        }

        if (!glb_FirstTime)
            lockControlsInModalWin(false);

        if (glb_FirstTime) {
            // A janela esta carregando
            glb_gridControlFill.bFollowOrder = false;
            glb_gridControlFill.nDsosToArrive = 2;
            glb_gridControlFill.dso1_Ref = dsoGridExtrato;
            glb_gridControlFill.dso2_Ref = dsoGridValoresLocalizar;
            glb_gridControlFill.grid1_Ref = fgExtrato;
            glb_gridControlFill.grid2_Ref = fgValoresLocalizar;

            startDynamicGrids();
        }
        else {
            ;
        }
    }

    adjustLabelsCombos();

    controlsInterfaceState();
    return null;
}

function fgExtrato_AfterEdit(grid, row, col)
{
    if ((grid == fgExtrato) && (row > 1))
    {
        if (col == getColIndexByColKey(fgExtrato, 'NaoConcilia*'))
        {
            fgExtrato.TextMatrix(row, col) = 0;
        }
    }

    controlsInterfaceState();
}


function fgExtrato_ValidateEdit(grid, Row, Col) {
    ;
}


function fgExtrato_AfterSelChange(grid, oldRow, oldCol, newRow, newCol) {


    ;

}

function js_fg_EnterCell(grid) {
    ;

}

function TransferirVinculada()
{
    var str = '?nExtratoID=' + escape(glb_nExtratoID);

    dsoTransferirVinculada.URL = SYS_ASPURLROOT + '/modcontabil/submodcontabilidade/extratosbancarios/serverside/transferenciaVinculada.aspx' + str;
    dsoTransferirVinculada.ondatasetcomplete = TransferirVinculada_DSC;
    dsoTransferirVinculada.refresh();
}

function TransferirVinculada_DSC()
{
    lockInterface(false);
    lockControlsInModalWin(false);

    var _retMsg = '';

    if (!(dsoTransferirVinculada.recordset.BOF && dsoTransferirVinculada.recordset.EOF)) {
        if ((dsoTransferirVinculada.recordset['Resultado'].value != null) && (dsoTransferirVinculada.recordset['Resultado'].value != '')) {
            if (window.top.overflyGen.Alert(dsoTransferirVinculada.recordset['Resultado'].value) == 0) {
                return null;
            }
        }
        else if (dsoTransferirVinculada.recordset['FinanceiroDeID'].value > 0) {
            _retMsg = window.top.overflyGen.Confirm('Transfer�ncia Banc�ria criada com sucesso: ' +
                      '\n  Financeiro Origem: ' + dsoTransferirVinculada.recordset['FinanceiroDeID'].value +
                      '\n  Financeiro Destino: ' + dsoTransferirVinculada.recordset['FinanceiroParaID'].value +
                      '\n\nDetalhar Financeiro Origem?');

            var detalhar = dsoTransferirVinculada.recordset['FinanceiroDeID'].value;

            if (_retMsg != 1) {
                return null;
            }
            else if (_retMsg == 1)
            {
                lockInterface(true);

                sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWDETAIL',
                                [detalhar, null, null]);
            }
        }
        else {
            if (window.top.overflyGen.Alert('N�o h� transfer�ncias') == 0) {
                return null;
            }
        }

    }
}