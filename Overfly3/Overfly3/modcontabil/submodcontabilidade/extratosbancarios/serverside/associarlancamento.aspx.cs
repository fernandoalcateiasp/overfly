﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class associarlancamento : System.Web.UI.OverflyPage
    {
        private string Resultado = "";
        private Integer[] ExtLancamentoID_NC;

        public Integer[] nExtLancamentoID_NC
        {
            get { return ExtLancamentoID_NC; }
            set { ExtLancamentoID_NC = value; }
        }
        private Integer[] ExtLancamentoID_C;

        public Integer[] nExtLancamentoID_C
        {
            get { return ExtLancamentoID_C; }
            set { ExtLancamentoID_C = value; }
        }

        protected DataSet AssociaLancamentoConcilia()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[3];

            for (int i = 0; i < ExtLancamentoID_NC.Length; i++)
            {
                procParams[0] = new ProcedureParameters(
                    "@ExtLancamentoID",
                    System.Data.SqlDbType.Int,
                    (ExtLancamentoID_NC.Length > 0) ? (Object)ExtLancamentoID_NC[i].ToString() : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                    "@ExtLancamentoConciliaID",
                    System.Data.SqlDbType.Int,
                    (ExtLancamentoID_C.Length > 0) ? (Object)ExtLancamentoID_C[i].ToString() : DBNull.Value);

                procParams[2] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[2].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_ExtratoBancarioLancamento_AssociaLancamentoConcilia",
                    procParams);

                Resultado += procParams[2].Data.ToString();
            }

            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (Resultado != null ? "'" + Resultado + "' " : "NULL") +
                 " as Resultado ");
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(AssociaLancamentoConcilia());


        }
    }
}