using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class Transferir : System.Web.UI.OverflyPage
    {
        private Integer relPesContaDeID;

        public Integer nRelPesContaDeID
        {
            get { return relPesContaDeID; }
            set { relPesContaDeID = value; }
        }

        private Integer relPesContaParaID;

        public Integer nRelPesContaParaID
        {
            get { return relPesContaParaID; }
            set { relPesContaParaID = value; }
        }

        private Integer usuarioID;

        public Integer nUsuarioID
        {
            get { return usuarioID; }
            set { usuarioID = value; }
        }

        private String valor;

        public String nValor
        {
            get { return valor; }
            set { valor = value; }
        }

        private Integer valorID;

        public Integer nValorID
        {
            get { return valorID; }
            set { valorID = value; }
        }

        string Resultado = string.Empty;
        int FinanceiroDeID = 0;
        int FinanceiroParaID = 0;
        int ValorDeID = 0;
        int ValorParaID = 0;

        protected override void PageLoad(object sender, EventArgs e)
        {
            int rowsAffected = 0;
            string sql = string.Empty;
            
            //Cria Transaferencia
            realizaTranferencia();

            //Fecha VLs/Finaneiros de Transferencia
            if (Resultado.Length == 0)
                fechaVLs();

            //Ajusta saldo das contas
            if (Resultado.Length == 0)
            {
                sql = "INSERT INTO ValoresLocalizar_Transaferencias " +
                        "SELECT " + valorID.ToString() + ", " + relPesContaDeID.ToString() + ", " + relPesContaParaID.ToString() + ", " + valor.ToString();

                rowsAffected = DataInterfaceObj.ExecuteSQLCommand(sql);
            }


            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select '" + Resultado + "' as Resultado, '" +
                                 FinanceiroDeID + "' as FinanceiroDeID, '" +
                                 FinanceiroParaID + "' as FinanceiroParaID, '" +
                                 ValorDeID + "' as ValorDeID, '" +
                                 ValorParaID + "' as ValorParaID"));
        }

        // Roda a procedure sp_ServicoBancario_Transferencia
        protected void realizaTranferencia()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[9];
            procParams[0] = new ProcedureParameters(
                "@RelPesContaDeID",
                System.Data.SqlDbType.Int,
                relPesContaDeID);

            procParams[1] = new ProcedureParameters(
                "@RelPesContaParaID",
                System.Data.SqlDbType.Int,
                relPesContaParaID);

            procParams[2] = new ProcedureParameters(
                "@ValorTransferencia",
                System.Data.SqlDbType.Money,
                valor);

            procParams[3] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                usuarioID);

            procParams[4] = new ProcedureParameters(
                "@FinanceiroDeID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value,
                ParameterDirection.InputOutput);

            procParams[5] = new ProcedureParameters(
                "@FinanceiroParaID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value,
                ParameterDirection.InputOutput);

            procParams[6] = new ProcedureParameters(
                "@ValorDeID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value,
                ParameterDirection.InputOutput);

            procParams[7] = new ProcedureParameters(
                "@ValorParaID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value,
                ParameterDirection.InputOutput);

            procParams[8] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                System.DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[8].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_ServicoBancario_Transferencia",
                procParams);

            if (procParams[4].Data != DBNull.Value)
                FinanceiroDeID = int.Parse(procParams[4].Data.ToString());

            if (procParams[5].Data != DBNull.Value)
                FinanceiroParaID = int.Parse(procParams[5].Data.ToString());

            if (procParams[6].Data != DBNull.Value)
                ValorDeID = int.Parse(procParams[6].Data.ToString());

            if (procParams[7].Data != DBNull.Value)
                ValorParaID = int.Parse(procParams[7].Data.ToString());

            if (procParams[8].Data != DBNull.Value)
                Resultado = procParams[8].Data.ToString();
        }

        // Roda a procedure sp_ValorLocalizar_Fechamento
        protected void fechaVLs()
        {
            
            //EXEC sp_ValorLocalizar_Fechamento @ValorDeID, 999, @Resultado OUTPUT
            ProcedureParameters[] procParams = new ProcedureParameters[3];
            procParams[0] = new ProcedureParameters(
                "@ValorID",
                System.Data.SqlDbType.Int,
                ValorDeID);

            procParams[1] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                usuarioID);

            procParams[2] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                System.DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[2].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_ValorLocalizar_Fechamento",
                procParams);

            if (procParams[2].Data != DBNull.Value)
                Resultado = procParams[2].Data.ToString();

            if (Resultado.Length == 0)
            {
                procParams[0] = new ProcedureParameters(
                    "@ValorID",
                    System.Data.SqlDbType.Int,
                    ValorParaID);

                procParams[1] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                    usuarioID);

                procParams[2] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    System.DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[2].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_ValorLocalizar_Fechamento",
                    procParams);

                if (procParams[2].Data != DBNull.Value)
                    Resultado = procParams[2].Data.ToString();
            }
        }
    }
}
