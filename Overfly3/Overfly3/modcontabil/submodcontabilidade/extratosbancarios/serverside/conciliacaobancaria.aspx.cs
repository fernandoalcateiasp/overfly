﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class conciliacaobancaria : System.Web.UI.OverflyPage
    {
        private string Resultado = "";

        private Integer[] ExtLancamentoID;

        public Integer[] nExtLancamentoID
        {
            get { return ExtLancamentoID; }
            set { ExtLancamentoID = value; }
        }
        private Integer[] LanContaID;

        public Integer[] nLanContaID
        {
            get { return LanContaID; }
            set { LanContaID = value; }
        }
        private Integer Dissociar;

        public Integer nDissociar
        {
            get { return Dissociar; }
            set { Dissociar = value; }
        }


        protected DataSet ExtratoBancario_Associar()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[4];

            for (int i = 0; i < ExtLancamentoID.Length; i++)
            {
                procParams[0] = new ProcedureParameters(
                    "@ExtLancamentoID",
                    System.Data.SqlDbType.Int,
                    (ExtLancamentoID.Length > 0) ? (Object)ExtLancamentoID[i].ToString() : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                    "@LanContaID",
                    System.Data.SqlDbType.Bit,
                    (LanContaID.Length > 0) ? (Object)LanContaID[i].ToString() : DBNull.Value);


                procParams[2] = new ProcedureParameters(
                    "@Dissociar",
                    System.Data.SqlDbType.Bit,
                    (Dissociar != null) ? (Object)Dissociar.ToString() : DBNull.Value);

                procParams[3] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[3].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_ExtratoBancario_Associar",
                    procParams);

                Resultado += procParams[3].Data.ToString();
            }

            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (Resultado != null ? "'" + Resultado + "' " : "NULL") +
                 " as Resultado ");
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(ExtratoBancario_Associar());


        }
    }
}