﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class verificacaoextrato : System.Web.UI.OverflyPage
    {

        private Integer ExtratoID;

        public Integer nExtratoID
        {
            get { return ExtratoID; }
            set { ExtratoID = value; }
        }

        private Integer CurrEstadoID;

        public Integer nCurrEstadoID
        {
            get { return CurrEstadoID; }
            set { CurrEstadoID = value; }
        }
        private Integer NewEstadoID;

        public Integer nNewEstadoID
        {
            get { return NewEstadoID; }
            set { NewEstadoID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            string strSql = "";
            int Verificacao = 0;

            strSql = "SELECT dbo.fn_ExtratoBancario_Verifica( " + ExtratoID + ", " + CurrEstadoID + ", " + NewEstadoID + ") AS Verificacao ";


            WriteResultXML(DataInterfaceObj.getRemoteData(strSql));


        }
    }
}