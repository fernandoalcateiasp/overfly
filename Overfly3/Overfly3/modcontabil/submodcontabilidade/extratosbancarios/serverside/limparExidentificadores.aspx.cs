﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class limparExidentificadores : System.Web.UI.OverflyPage
    {
        private string Resultado = "";

        private string[] ValorID;

        public string[] nValorID
        {
            get { return ValorID; }
            set { ValorID = value; }
        }

        protected DataSet ExtratoBancarioLancamento()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[2];

            for (int i = 0; i < ValorID.Length; i++)
            {
                procParams[0] = new ProcedureParameters(
                    "@ValorID",
                    System.Data.SqlDbType.Int,
                    (ValorID.Length > 0) ? (Object)ValorID[i].ToString() : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[1].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_ExtratoBancarioLancamento_Limpar",
                    procParams);

                Resultado += procParams[1].Data.ToString();
            }

            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (Resultado != null ? "'" + Resultado + "' " : "NULL") +
                 " as Resultado ");
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(ExtratoBancarioLancamento());

        }
    }
}