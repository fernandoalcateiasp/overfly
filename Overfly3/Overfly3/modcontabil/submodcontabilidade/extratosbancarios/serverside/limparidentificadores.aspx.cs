﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class limparidentificadores : System.Web.UI.OverflyPage
    {

        private string[] ValorID;

        public string[] nValorID
        {
            get { return ValorID; }
            set { ValorID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            for (int i = 0; i < ValorID.Length; i++)
            {
                string strSql = "";
                int Verificacao = 0;

                strSql = "UPDATE ValoresLocalizar SET Identificador = NULL WHERE (ValorID = " + ValorID[i].ToString() + ")";
                Verificacao = DataInterfaceObj.ExecuteSQLCommand(strSql);

            }
            WriteResultXML(DataInterfaceObj.getRemoteData(
                  "select '' as Resultado "));


        }
    }
}