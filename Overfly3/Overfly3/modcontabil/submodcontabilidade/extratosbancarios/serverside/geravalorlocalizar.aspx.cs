﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class geravalorlocalizar : System.Web.UI.OverflyPage
    {
        private string Resultado = "";
        private string Resultado2 = "";
        private string ValorID = "";
        private string ValorID2 = "";

        private Integer ProcessoID;

        public Integer nProcessoID
        {
            get { return ProcessoID; }
            set { ProcessoID = value; }
        }
        private Integer HistoricoPadraoID;

        public Integer nHistoricoPadraoID
        {
            get { return HistoricoPadraoID; }
            set { HistoricoPadraoID = value; }
        }
        private string EhEstorno;

        public string bEhEstorno
        {
            get { return EhEstorno; }
            set { EhEstorno = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private string Observacao;

        public string sObservacao
        {
            get { return Observacao; }
            set { Observacao = value; }
        }
        private Integer[] ExtLancamentoID;

        public Integer[] nExtLancamentoID
        {
            get { return ExtLancamentoID; }
            set { ExtLancamentoID = value; }
        }


        protected DataSet ExtratoDissociar()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[8];


            for (int i = 0; i < ExtLancamentoID.Length; i++)
            {

                procParams[0] = new ProcedureParameters(
                    "@ExtLancamentoID",
                    System.Data.SqlDbType.Int,
                    (ExtLancamentoID.Length > 0) ? (Object)Convert.ToInt32(ExtLancamentoID[i].ToString()) : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                   "@ProcessoID",
                   System.Data.SqlDbType.Int,
                   (ProcessoID != null ? (Object)Convert.ToInt32(ProcessoID.ToString()) : DBNull.Value));

                procParams[2] = new ProcedureParameters(
                   "@HistoricoPadraoID",
                   System.Data.SqlDbType.Int,
                   (HistoricoPadraoID != null ? (Object)Convert.ToInt32(HistoricoPadraoID.ToString()) : DBNull.Value));

                procParams[3] = new ProcedureParameters(
                   "@EhEstorno",
                   System.Data.SqlDbType.Bit,
                   (EhEstorno == "0" ? false : true));

                procParams[4] = new ProcedureParameters(
                   "@UsuarioID",
                   System.Data.SqlDbType.Int,
                   (UsuarioID != null ? (Object)Convert.ToInt32(UsuarioID.ToString()) : DBNull.Value));

                procParams[5] = new ProcedureParameters(
                   "@ObservacaoFin",
                   System.Data.SqlDbType.VarChar,
                   (Observacao != null ? (Object)Observacao.ToString() : DBNull.Value));

                procParams[6] = new ProcedureParameters(
                     "@Resultado",
                     System.Data.SqlDbType.VarChar,
                     DBNull.Value,
                     ParameterDirection.InputOutput);
                procParams[6].Length = 8000;

                procParams[7] = new ProcedureParameters(
                   "@ValorID",
                   System.Data.SqlDbType.Int,
                   DBNull.Value,
                   ParameterDirection.InputOutput);
                procParams[7].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_ExtratoBancarioLancamento_GerarValorLocalizar",
                    procParams);

                ValorID = procParams[7].Data.ToString();
                Resultado = procParams[6].Data.ToString();


                if (ValorID2 == "")
                {
                    ValorID2 = ValorID;
                }
                else
                {
                    ValorID2 = ValorID2 + ", " + ValorID;
                }

                if (Resultado != "")
                {
                    Resultado2 = Resultado2 + " " + Resultado;
                }

            }// fim do for

            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (ValorID2 != null ? "'" + ValorID2 + "' " : "NULL") +
                 " as ValorID, " +
                 (Resultado2 != null ? "'" + Resultado2 + "' " : "NULL") +
                 " as Resultado ");
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(ExtratoDissociar());


        }
    }
}