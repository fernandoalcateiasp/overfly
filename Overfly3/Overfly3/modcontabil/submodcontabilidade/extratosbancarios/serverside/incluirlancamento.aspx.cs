﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class incluirlancamento : System.Web.UI.OverflyPage
    {

        private string Resultado = "";
        private string ExtLancamentoID = "";

        private Integer[] ExtLancamentoID_NC;

        public Integer[] nExtLancamentoID_NC
        {
            get { return ExtLancamentoID_NC; }
            set { ExtLancamentoID_NC = value; }
        }
        private Integer[] UsuarioID;

        public Integer[] nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        protected DataSet GeraLancamentoConcilia()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[5];           

            for (int i = 0; i < ExtLancamentoID_NC.Length; i++)
            {
                procParams[0] = new ProcedureParameters(
                    "@ExtLancamentoID",
                    System.Data.SqlDbType.Int,
                    (ExtLancamentoID_NC.Length > 0) ? (Object)ExtLancamentoID_NC[i].ToString() : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                    "@ExtratoConciliaID",
                    System.Data.SqlDbType.Bit,
                    DBNull.Value);

                procParams[2] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                    (UsuarioID.Length > 0) ? (Object)UsuarioID[i].ToString() : DBNull.Value);

                procParams[3] = new ProcedureParameters(
                    "@ExtLancamentoConciliaID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value,
                    ParameterDirection.InputOutput);               

                procParams[4] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[4].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_ExtratoBancarioLancamento_GeraLancamentoConcilia",
                    procParams);

                ExtLancamentoID += procParams[3].Data.ToString() ;

                Resultado += procParams[4].Data.ToString();
            }


            return DataInterfaceObj.getRemoteData(
                   "select " +
                   (ExtLancamentoID != null ? "'" + ExtLancamentoID + "' " : "NULL") +
                   " as ExtLancamentoConciliaID , " +
                   (Resultado != null ? "'" + Resultado + "' " : "NULL") +
                   " as Resultado ");
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(GeraLancamentoConcilia());


        }
    }
}