﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class processamentoautomatico : System.Web.UI.OverflyPage
    {

        private string Resultado = "";

        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private string UsuarioID;

        public string nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        protected DataSet Processamento()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[2];

                procParams[0] = new ProcedureParameters(
                    "@EmpresaID",
                    System.Data.SqlDbType.Int,
                    (EmpresaID != null) ? (Object)EmpresaID.ToString() : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                    (UsuarioID != null) ? (Object)UsuarioID.ToString() : DBNull.Value);               

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_ExtratoBancario_Processamento",
                    procParams);

                Resultado += procParams[1].Data.ToString();

            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (Resultado != null ? "'" + Resultado + "' " : "NULL") +
                 " as Resultado ");
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(Processamento());

        }
    }
}