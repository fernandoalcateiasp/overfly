﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class associarvalorlocalizar : System.Web.UI.OverflyPage
    {
        private string Resultado = "";
        
        private Integer ExtLancamentoID;

        public Integer nExtLancamentoID
        {
            get { return ExtLancamentoID; }
            set { ExtLancamentoID = value; }
        }
        private string[] ValorID;

        public string[] nValorID
        {
            get { return ValorID; }
            set { ValorID = value; }
        }
        protected DataSet AssociarValorLocalizar()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[3];

            for (int i = 0; i < ValorID.Length; i++)
            {
                procParams[0] = new ProcedureParameters(
                    "@ExtLancamentoID",
                    System.Data.SqlDbType.Int,
                    (ExtLancamentoID != null) ? (Object)ExtLancamentoID.ToString() : DBNull.Value);

                procParams[1] = new ProcedureParameters(
                    "@ValorID",
                    System.Data.SqlDbType.Int,
                    (nValorID.Length > 0) ? (Object)nValorID[i].ToString() : DBNull.Value);

                procParams[2] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
                procParams[2].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_ExtratoBancarioLancamento_AssociarValorLocalizar",
                    procParams);

                Resultado += procParams[2].Data.ToString();
            }

            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (Resultado != null ? "'" + Resultado + "' " : "NULL") +
                 " as Resultado ");
        }
        protected override  void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(AssociarValorLocalizar());

        }
    }
}