﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class associarautomaticolancamentoconcilia : System.Web.UI.OverflyPage
    {
        private string Resultado = "";

        private Integer ExtratoID;

        public Integer nExtratoID
        {
            get { return ExtratoID; }
            set { ExtratoID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        protected DataSet AssociacaoAutomatica()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[3];

            procParams[0] = new ProcedureParameters(
                "@ExtratoID",
                System.Data.SqlDbType.Int,
                (ExtratoID != null) ? (Object)ExtratoID.ToString() : DBNull.Value);

            procParams[1] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                (UsuarioID != null) ? (Object)UsuarioID.ToString() : DBNull.Value);

            procParams[2] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[2].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_ExtratoBancario_AssociacaoAutomaticaLancamentosConcilia",
                procParams);

            Resultado += procParams[2].Data.ToString();

            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (Resultado != "" ? "'" + Resultado + "' " : "NULL") +
                 " as Resultado ");
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(AssociacaoAutomatica());


        }
    }
}