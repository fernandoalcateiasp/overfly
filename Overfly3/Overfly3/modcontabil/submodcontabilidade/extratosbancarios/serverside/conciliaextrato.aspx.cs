﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class conciliaextrato : System.Web.UI.OverflyPage
    {

        private Integer ExtLancamentoID;

        public Integer nExtLancamentoID
        {
            get { return ExtLancamentoID; }
            set { ExtLancamentoID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            string strSql = "";
            int Verificacao = 0;

            if (ExtLancamentoID != null)
            {
                strSql = "UPDATE ExtratosBancarios_Lancamentos SET NaoConcilia = CONVERT(BIT, ISNULL(NaoConcilia, 0)) ^ 1 WHERE (ExtLancamentoID = " + ExtLancamentoID + ")";
                Verificacao = DataInterfaceObj.ExecuteSQLCommand(strSql);
            }

            WriteResultXML(DataInterfaceObj.getRemoteData(
                  "select '' as Resultado "));


        }
    }
}