﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class transferenciaVinculada : System.Web.UI.OverflyPage
    {
        private int FinanceiroDeID = 0;
        private int FinanceiroParaID = 0;
        private int ValorDeID = 0;
        private int ValorParaID = 0;
        private string Resultado = "";

        private Integer ExtratoID;

        public Integer nExtratoID
        {
            get { return ExtratoID; }
            set { ExtratoID = value; }
        }

        protected DataSet transferenciaVinculadaCriar()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[6];
            
            procParams[0] = new ProcedureParameters(
                "@ExtratoID",
                System.Data.SqlDbType.Int,
                (ExtratoID.intValue() > 0) ? (Object)ExtratoID.intValue() : DBNull.Value);

            procParams[1] = new ProcedureParameters(
                    "@FinanceiroDeID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value,
                    ParameterDirection.InputOutput);

            procParams[2] = new ProcedureParameters(
                    "@FinanceiroParaID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value,
                    ParameterDirection.InputOutput);

            procParams[3] = new ProcedureParameters(
                    "@ValorDeID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value,
                    ParameterDirection.InputOutput);

            procParams[4] = new ProcedureParameters(
                    "@ValorParaID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value,
                    ParameterDirection.InputOutput);

            procParams[5] = new ProcedureParameters(
                    "@Resultado",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
            procParams[5].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_ExtratoBancario_TransferenciaVinculada",
                procParams);

            if (procParams[1].Data.ToString() != "")
                FinanceiroDeID = int.Parse(procParams[1].Data.ToString());
            if (procParams[2].Data.ToString() != "")
                FinanceiroParaID = int.Parse(procParams[2].Data.ToString());
            if (procParams[3].Data.ToString() != "")
                ValorDeID = int.Parse(procParams[3].Data.ToString());
            if (procParams[4].Data.ToString() != "")
                ValorParaID = int.Parse(procParams[4].Data.ToString());
            
            Resultado = procParams[5].Data.ToString();

            return DataInterfaceObj.getRemoteData(
                 "select " + FinanceiroDeID.ToString() + " AS FinanceiroDeID, " + FinanceiroParaID.ToString() + " AS FinanceiroParaID, " + ValorDeID.ToString() + " AS ValorDeID, " +
                    ValorParaID.ToString() + "AS ValorParaID, '" + Resultado + "' as Resultado ");
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(transferenciaVinculadaCriar());
        }
    }
}