﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class geralancamento : System.Web.UI.OverflyPage
    {
        private string Resultado = "";
        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private Integer ExtLancamentoID;

        public Integer nExtLancamentoID
        {
            get { return ExtLancamentoID; }
            set { ExtLancamentoID = value; }
        }
        private string EhEstorno;

        public string bEhEstorno
        {
            get { return EhEstorno; }
            set { EhEstorno = value; }
        }
        private Integer TipoLancamentoID;

        public Integer nTipoLancamentoID
        {
            get { return TipoLancamentoID; }
            set { TipoLancamentoID = value; }
        }
        private Integer HistoricoPadraoID;

        public Integer nHistoricoPadraoID
        {
            get { return HistoricoPadraoID; }
            set { HistoricoPadraoID = value; }
        }
        private string HistoricoComplementar;

        public string nHistoricoComplementar
        {
            get { return HistoricoComplementar; }
            set { HistoricoComplementar = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }

        protected DataSet sp_Lancamento_Gerador()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[10];

            procParams[0] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                (EmpresaID != null) ? (Object)EmpresaID.ToString() : DBNull.Value);

            procParams[1] = new ProcedureParameters(
                "@dtBalancete",
                System.Data.SqlDbType.Int,
                DBNull.Value);

            procParams[2] = new ProcedureParameters(
                 "@EhEstorno",
                 System.Data.SqlDbType.Bit,
                 (EhEstorno == "0") ? false : true);
            
            procParams[3] = new ProcedureParameters(
                "@TipoLancamentoID",
                System.Data.SqlDbType.Int,
                (TipoLancamentoID != null) ? (Object)TipoLancamentoID.ToString() : DBNull.Value);

            procParams[4] = new ProcedureParameters(
                "@HistoricoPadraoID",
                System.Data.SqlDbType.Int,
                (HistoricoPadraoID != null) ? (Object)HistoricoPadraoID.ToString() : DBNull.Value);

            procParams[5] = new ProcedureParameters(
                "@Gerador",
                System.Data.SqlDbType.Int,
                3);

            procParams[6] = new ProcedureParameters(
                "@ExtLancamentoID",
                System.Data.SqlDbType.Int,
                (ExtLancamentoID != null) ? (Object)ExtLancamentoID.ToString() : DBNull.Value);

            procParams[6] = new ProcedureParameters(
               "@ProprietarioID",  
               System.Data.SqlDbType.Int,
               (UsuarioID != null) ? (Object)UsuarioID.ToString() : DBNull.Value);

            procParams[7] = new ProcedureParameters(
                "@LancamentoID",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[7].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Lancamento_Gerador",
                procParams);

            Resultado += procParams[7].Data.ToString();

            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (Resultado != null ? "'" + Resultado + "' " : "NULL") +
                 " as LancamentoID ");
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(sp_Lancamento_Gerador());


        }
    }
}