﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class excluirlancamento : System.Web.UI.OverflyPage
    {
        private string Resultado = "";

        private Integer ExtLancamentoID_C;

        public Integer nExtLancamentoID_C
        {
            get { return ExtLancamentoID_C; }
            set { ExtLancamentoID_C = value; }
        }

        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private string LogMotivo;

        public string sLogMotivo
        {
            get { return LogMotivo; }
            set { LogMotivo = value; }
        }

        protected DataSet ExcluiLancamentoConcilia()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[4];

            procParams[0] = new ProcedureParameters(
                "@ExtLancamentoID",
                System.Data.SqlDbType.Int,
                (ExtLancamentoID_C != null) ? (Object)ExtLancamentoID_C.ToString() : DBNull.Value);

            procParams[1] = new ProcedureParameters(
              "@UsuarioID",
              System.Data.SqlDbType.Int,
              (UsuarioID != null) ? (Object)UsuarioID.ToString() : DBNull.Value);

            procParams[2] = new ProcedureParameters(
              "@LogMotivo",
              System.Data.SqlDbType.VarChar,
              (LogMotivo != null) ? (Object)LogMotivo.ToString() : DBNull.Value);
            
            procParams[3] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[3].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_ExtratoBancarioLancamento_ExcluiLancamentoConcilia",
                procParams);

            Resultado += procParams[3].Data.ToString();

            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (Resultado != null ? "'" + Resultado + "' " : "NULL") +
                 " as Resultado ");
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(ExcluiLancamentoConcilia());

        }
    }
}