﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class dissociarvalorlocalizar : System.Web.UI.OverflyPage
    {
        private string Resultado = "";

        private Integer ExtLancamentoID_NC;

        public Integer nExtLancamentoID
        {
            get { return ExtLancamentoID_NC; }
            set { ExtLancamentoID_NC = value; }
        }


        protected DataSet ExtratoDissociar()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[2];

            procParams[0] = new ProcedureParameters(
                "@ExtLancamentoID",
                System.Data.SqlDbType.Int,
                (ExtLancamentoID_NC !=  null ) ? (Object)ExtLancamentoID_NC.ToString() : DBNull.Value);

            procParams[1] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[1].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_ExtratoBancarioLancamento_DissociarValorLocalizar",
                procParams);

            Resultado += procParams[1].Data.ToString();

            return DataInterfaceObj.getRemoteData(
                 "select " +
                 (Resultado != null ? "'" + Resultado + "' " : "NULL") +
                 " as Resultado ");
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(ExtratoDissociar());


        }
    }
}