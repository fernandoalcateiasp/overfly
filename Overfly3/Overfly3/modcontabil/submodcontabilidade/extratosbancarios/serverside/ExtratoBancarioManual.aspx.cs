﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modcontabil.submodcontabilidade.extratosbancarios.serverside
{
    public partial class ExtratoBancarioManual : System.Web.UI.OverflyPage
    {
        private string Resultado = "";

        private Integer RelPesContaID;

        public Integer nRelPesContaID
        {
            get { return RelPesContaID; }
            set { RelPesContaID = value; }
        }
        private string Extrato;

        public string dtExtrato
        {
            get { return Extrato; }
            set { Extrato  = value; }
        }

        protected DataSet ExtratoCriar()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[3];
            
            procParams[0] = new ProcedureParameters(
                "@RelPesContaID",
                System.Data.SqlDbType.Int,
                (RelPesContaID.intValue() > 0) ? (Object)RelPesContaID.intValue() : DBNull.Value);

            procParams[1] = new ProcedureParameters(
                "@dtExtrato",
                System.Data.SqlDbType.DateTime,
                (Extrato.Length > 0 ? (Object)Extrato : DBNull.Value));

            procParams[2] = new ProcedureParameters(
                    "@Mensagem",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value,
                    ParameterDirection.InputOutput);
            procParams[2].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_ExtratoBancarioManual_Gerador",
                procParams);
                
            Resultado = procParams[2].Data.ToString();

            return DataInterfaceObj.getRemoteData(
                 "select '" + Resultado + "' as Resultado ");
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(ExtratoCriar());
        }
    }
}