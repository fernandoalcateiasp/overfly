<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot,strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))    
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="extratosbancariossup01Html" name="extratosbancariossup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf    
    

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/extratosbancarios/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/extratosbancarios/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="extratosbancariossup01Body" name="extratosbancariossup01Body" LANGUAGE="javascript" onload="return window_onload()">

  
    <!-- //@@ Os dsos sao definidos de acordo com o form -->
        
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p  id="lblConcilia" name="lblConcilia" class="lblGeneral">Concilia</p>
        <input type="checkbox" id="chkConcilia" name="chkConcilia" DATASRC="#dsoSup01" DATAFLD="Concilia" class="fldGeneral" title="Extrato concili�vel?"></input>
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p id="lbldtInicial" name="lbldtInicial" class="lblGeneral">Data Inicial</p>
        <input type="text" id="txtdtInicial" name="txtdtInicial" DATASRC="#dsoSup01" DATAFLD="V_dtInicial" class="fldGeneral"></input>
        <p id="lbldtFinal" name="lbldtFinal" class="lblGeneral">Data Final</p>
        <input type="text" id="txtdtFinal" name="txtdtFinal" DATASRC="#dsoSup01" DATAFLD="V_dtFinal" class="fldGeneral"></input>
        <p id="lbldtEmissao" name="lbldtEmissao" class="lblGeneral">Data Emiss�o</p>
        <input type="text" id="txtdtEmissao" name="txtdtEmissao" DATASRC="#dsoSup01" DATAFLD="V_dtEmissao" class="fldGeneral" title="Data da emiss�o do extrato banc�rio" disabled="true"></input>
		<p id="lblRelPesContaID" name="lblRelPesContaID" class="lblGeneral">Bco/Ag/Cta</p>
		<select id="selRelPesContaID" name="selRelPesContaID" DATASRC="#dsoSup01" DATAFLD="RelPesContaID" class="fldGeneral"></select>
        <p id="lblBanco" name="lblBanco" class="lblGeneral">Banco</p>
        <input type="text" id="txtBanco" name="txtBanco" DATASRC="#dsoSup01" DATAFLD="Banco" class="fldGeneral"></input>
        <p id="lblContaCorrente" name="lblContaCorrente" class="lblGeneral">Conta</p>
        <input type="text" id="txtContaCorrente" name="txtContaCorrente" DATASRC="#dsoSup01" DATAFLD="ContaCorrente" class="fldGeneral"></input>
        <p id="lblTipoConta" name="lblTipoConta" class="lblGeneral">Tipo</p>
        <input type="text" id="txtTipoConta" name="txtTipoConta" DATASRC="#dsoSup01" DATAFLD="TipoConta" class="fldGeneral"></input>
        <p id="lblNumeroLancamentos" name="lblNumeroLancamentos" class="lblGeneral" title="N�mero de lan�amentos">Lan�</p>
        <input type="text" id="txtNumeroLancamentos" name="txtNumeroLancamentos" DATASRC="#dsoSup01" DATAFLD="NumeroLancamentos" class="fldGeneral" title="N�mero de lan�amentos"></input>
        <p id="lblValorTotalDebitos" name="lblValorTotalDebitos" class="lblGeneral">Total D�bitos</p>
        <input type="text" id="txtValorTotalDebitos" name="txtValorTotalDebitos" DATASRC="#dsoSup01" DATAFLD="ValorTotalDebitos" class="fldGeneral"></input>
        <p id="lblValorTotalCreditos" name="lblValorTotalCreditos" class="lblGeneral">Total Cr�ditos</p>
        <input type="text" id="txtValorTotalCreditos" name="txtValorTotalCreditos" DATASRC="#dsoSup01" DATAFLD="ValorTotalCreditos" class="fldGeneral"></input>
        <p id="lblSaldoFinal" name="lblSaldoFinal" class="lblGeneral">Saldo Final</p>
        <input type="text" id="txtSaldoFinal" name="txtSaldoFinal" DATASRC="#dsoSup01" DATAFLD="SaldoFinal" class="fldGeneral"></input>
        <p id="lblSaldoNaoConcilia" name="lblSaldoNaoConcilia" class="lblGeneral">Saldo N�o Concilia (Extrato</p>
        <input type="text" id="txtSaldoNaoConcilia" name="txtSaldoNaoConcilia" DATASRC="#dsoSup01" DATAFLD="SaldoNaoConcilia" class="fldGeneral"></input>

        <p id="lblSaldoConciliaAnterior" name="lblSaldoConciliaAnterior" class="lblGeneral">Saldo Anterior (Extrato</p>
        <input type="text" id="txtSaldoConciliaAnterior" name="txtSaldoConciliaAnterior" DATASRC="#dsoSup01" DATAFLD="SaldoConciliaAnterior" class="fldGeneral"></input>

        <p id="lblArquivo" name="lblArquivo" class="lblGeneral">Arquivo</p>
        <input type="text" id="txtArquivo" name="txtArquivo" DATASRC="#dsoSup01" DATAFLD="Arquivo" class="fldGeneral"></input>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
        
    
    </div>
    
</body>

</html>
