/********************************************************************
commoncontrolbar.js

Library javascript de funcoes para os controlbars
********************************************************************/

// As constantes abaixo sao de uso exclusivo desta library

// CONSTANTES *******************************************************

var NUM_GEN_BTNS = 11;
var NUM_ESPEC_BTNS = 16;

// Diretorio de imagens
//var imgPath = '../images/btnstoolbar1/';
var imgPath = SYS_PAGESURLROOT + '/images/btnstoolbar1/';

// Variaveis de imagens =============================================
var overImg = new Array();
overImg[0] = new Image();
overImg[1] = new Image();
overImg[2] = new Image();
overImg[3] = new Image();
overImg[4] = new Image();
overImg[5] = new Image();
overImg[6] = new Image();
overImg[7] = new Image();
overImg[8] = new Image();
overImg[9] = new Image();
overImg[10] = new Image();
overImg[11] = new Image();
overImg[12] = new Image();
overImg[13] = new Image();
overImg[14] = new Image();
overImg[15] = new Image();
overImg[16] = new Image();
overImg[17] = new Image();
overImg[18] = new Image();
// ovr1024
overImg[19] = new Image();
overImg[20] = new Image();
overImg[21] = new Image();
overImg[22] = new Image();
overImg[23] = new Image();
overImg[24] = new Image();
overImg[25] = new Image();
overImg[26] = new Image();


var downImg = new Array();
downImg[0] = new Image();
downImg[1] = new Image();
downImg[2] = new Image();
downImg[3] = new Image();
downImg[4] = new Image();
downImg[5] = new Image();
downImg[6] = new Image();
downImg[7] = new Image();
downImg[8] = new Image();
downImg[9] = new Image();
downImg[10] = new Image();
downImg[11] = new Image();
downImg[12] = new Image();
downImg[13] = new Image();
downImg[14] = new Image();
downImg[15] = new Image();
downImg[16] = new Image();
downImg[17] = new Image();
downImg[18] = new Image();
// ovr1024
downImg[19] = new Image();
downImg[20] = new Image();
downImg[21] = new Image();
downImg[22] = new Image();
downImg[23] = new Image();
downImg[24] = new Image();
downImg[25] = new Image();
downImg[26] = new Image();

var defaultImg = new Array();
defaultImg[0] = new Image();
defaultImg[1] = new Image();
defaultImg[2] = new Image();
defaultImg[3] = new Image();
defaultImg[4] = new Image();
defaultImg[5] = new Image();
defaultImg[6] = new Image();
defaultImg[7] = new Image();
defaultImg[8] = new Image();
defaultImg[9] = new Image();
defaultImg[10] = new Image();
defaultImg[11] = new Image();
defaultImg[12] = new Image();
defaultImg[13] = new Image();
defaultImg[14] = new Image();
defaultImg[15] = new Image();
defaultImg[16] = new Image();
defaultImg[17] = new Image();
defaultImg[18] = new Image();
// ovr1024
defaultImg[19] = new Image();
defaultImg[20] = new Image();
defaultImg[21] = new Image();
defaultImg[22] = new Image();
defaultImg[23] = new Image();
defaultImg[24] = new Image();
defaultImg[25] = new Image();
defaultImg[26] = new Image();

var colorImg = new Array();
colorImg[0] = new Image();
colorImg[1] = new Image();
colorImg[2] = new Image();
colorImg[3] = new Image();
colorImg[4] = new Image();
colorImg[5] = new Image();
colorImg[6] = new Image();
colorImg[7] = new Image();
colorImg[8] = new Image();
colorImg[9] = new Image();
colorImg[10] = new Image();
colorImg[11] = new Image();
colorImg[12] = new Image();
colorImg[13] = new Image();
colorImg[14] = new Image();
colorImg[15] = new Image();
colorImg[16] = new Image();
colorImg[17] = new Image();
colorImg[18] = new Image();
// ovr1024
colorImg[19] = new Image();
colorImg[20] = new Image();
colorImg[21] = new Image();
colorImg[22] = new Image();
colorImg[23] = new Image();
colorImg[24] = new Image();
colorImg[25] = new Image();
colorImg[26] = new Image();

// Final de vari�veis de imagens ====================================

// Prepara os eventos de carregamento das imagens ===================
// Para mostrar a barra 

var glb__collImgsLen = 0;
var glb_imgs_events_fires = 0;
var glb_QuantNotificacoesNaoLidas = 0;
var glb_QuantNotificacoesLidas = 0;
var glb_QuantNotificacoes = 0;
var glb_UserID = getCurrUserID();

function prepareImgsEvents() {
    var i;
    var coll = window.document.getElementsByTagName('IMG');

    glb__collImgsLen = coll.length;

    for (i = 0; i < glb__collImgsLen; i++) {
        coll.item(i).onload = img_onload_OK;
        coll.item(i).onerror = img_onload_ERROR;
    }
}

function img_onload_OK() {
    glb_imgs_events_fires++;

    if (glb_imgs_events_fires == glb__collImgsLen)
        btnsImagesFinishLoad();
}

function img_onload_ERROR() {
    glb_imgs_events_fires++;

    if (glb_imgs_events_fires == glb__collImgsLen)
        btnsImagesFinishLoad();
}

function btnsImagesFinishLoad() {
    // Body
    with (controlbarBody) {
        style.visibility = 'visible';
        style.backgroundColor = 'silver';
        scroll = 'no';
    }

    // O arquivo carregou
    if (getIfSupOrInf() == 'SUP')
        sendJSMessage(getHtmlId(), JS_PAGELOADED, new Array(window.top.name.toString(), 'USERCHILDBROWSER'), 0X2);
    else
        sendJSMessage(getHtmlId(), JS_PAGELOADED, new Array(window.top.name.toString(), 'USERCHILDBROWSER'), 0X4);
}

// Final de Prepara os eventos de carregamento das imagens ==========
// Para mostrar a barra 

// Preload imagens no array de imagens ==============================

// Imagens particulares da barra superior e da inferior
if (window.document.getElementsByTagName('HTML').item(0).id == 'controlbarsupHtml') {
    defaultImg[0].src = window.top.opener.defaultImg[0];
    defaultImg[1].src = window.top.opener.defaultImg[1];

    overImg[0].src = window.top.opener.overImg[0];
    overImg[1].src = window.top.opener.overImg[1];

    downImg[0].src = window.top.opener.downImg[0];
    downImg[1].src = window.top.opener.downImg[1];

    colorImg[0].src = window.top.opener.colorImg[0];
    colorImg[1].src = window.top.opener.colorImg[1];
}
else if (window.document.getElementsByTagName('HTML').item(0).id == 'controlbarinfHtml') {
    defaultImg[0].src = window.top.opener.defaultImg_I[0];
    defaultImg[1].src = window.top.opener.defaultImg_I[1];

    overImg[0].src = window.top.opener.overImg_I[0];
    overImg[1].src = window.top.opener.overImg_I[1];

    downImg[0].src = window.top.opener.downImg_I[0];
    downImg[1].src = window.top.opener.downImg_I[1];

    colorImg[0].src = window.top.opener.colorImg_I[0];
    colorImg[1].src = window.top.opener.colorImg_I[1];
}

// Imagens comuns as duas barras
var nIndex = 0;
// ovr1024
for (nIndex = 2; nIndex <= 26; nIndex++) {
    // botao especifico 1
    if ((nIndex >= 11) && (nIndex <= 13) && (window.document.getElementsByTagName('HTML').item(0).id == 'controlbarinfHtml'))
    {
        defaultImg[nIndex].src = window.top.opener.defaultImg_I[nIndex];
        overImg[nIndex].src = window.top.opener.overImg_I[nIndex];
        downImg[nIndex].src = window.top.opener.downImg_I[nIndex];
        colorImg[nIndex].src = window.top.opener.colorImg_I[nIndex];
    }
    else
    {
        defaultImg[nIndex].src = window.top.opener.defaultImg[nIndex];
        overImg[nIndex].src = window.top.opener.overImg[nIndex];
        downImg[nIndex].src = window.top.opener.downImg[nIndex];
        colorImg[nIndex].src = window.top.opener.colorImg[nIndex];
    }
}

// Variaveis de estado dos botoes ===================================
var btnState = new Array();
// ovr1024
for (nIndex = 0; nIndex <= 26; nIndex++) {
    btnState[nIndex] = 4;
}


// Variavel que retem o id do frame cujo html est� usando o formbar
var idOfFrameAttached = new String();

// Variavel que retem o estado lock/unlock do formbar
var fBarIsLocked = false;

// Variavel que salva o estado da barra
var stateSaved = new String();
stateSaved = '';

// Variavel que controla o ultimo combo selecionado
// 0 (nenhum), 1 ou 2
var lastCmbSel = 0;

// Variavel que controla o ultimo combo usado pelo usuario
// 0 (nenhum), 1 ou 2

// Variavel que controla o ultimo botao clicado pelo usuario
var btnID = null;

// Variaveis que controlam a string dos botoes
var __currentBtnsCtrlBarStr = null;

// Array que controla botoes travados permanentemente
var __btnLocked = new Array(false, false, false, false,
                            false, false, false, false,
                            false, false, false,
                            false, false, false, false, false, false, false, false,
                            false, false, false, false, false, false, false, false); // ovr1024

// FINAL DE CONSTANTES **********************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES GERAIS:
wndJSProc(idElement, msg, param1, param2)
windowOnLoad_UniquePart()
windowOnLoad_Complement()
getIfSupOrInf()
getHtmlId()
        
FUNCOES DE TROCA DE IMAGENS:    
rollImageOver()
rollImageOut()
rollImageDown()
rollImageUp()

FUNCOES DE CONFIGURACAO DOS BOTOES E COMBOS
setupFBar(cmb1Dis, cmb2Dis, btnsMode)
parseStrBtns(btnsMode, fBtn)
allBtnsGrayOrColorOrPressed()
    
FUNCOES DE COMUNICACAO COM FORMS
regHtml(htmlID)
fireFunction()
onChangeCmb()
getComboData(comboNumber)
getComboDataByOptionIndex(comboNumber, optIndexInSel)
addOptionInCombo(comboNumber, optionText, optionValue)
removeOptionInCombo(comboNumber, optionValue)
cleanupCombo(comboNumber)
selByOptValueInSel(comboNumber, optionValue)
unselOptInSel(comboNumber)
numOptionsInCmb(comboNumber)
setFocusInCmb(comboNumber)            
btnIsEnable(btnNumber)
strArrayValuesInCmb(comboNumber)

FUNCOES DOS BOTOES ESPECIFICOS
showBtnsEspec(cmdShow, arrayBtnsAffected)
hintsBtnsEspec(hintArray)
setupEspecFBar(btnsMode)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES GERAIS ***************************************************

/********************************************************************
Carregamento
********************************************************************/
function window_onload() {
    sep1_1.style.visibility = 'hidden';
    sep1_2.style.visibility = 'hidden';
    btnNotificacoes.style.visibility = 'hidden';
    btnNotificacoes.disable = true;
    prepareImgsEvents();
    
    windowOnLoad_UniquePart();

    if (getIfSupOrInf() == 'INF') 
    {
        btnNotificacoes.style.visibility = 'hidden'; 
    }
    
    // Translada interface pelo dicionario do sistema
    translateInterface(window, null);
}

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2) {
    switch (msg)
    {
        case JS_PAGELOADED:
        {
            ;
        }
        break;

        // Msg do carrier
        case JS_NOTIFICATIONQTY:
        {
            confBotaoNotificacao(param1);
            return null;
        }
        break;

        default:
            return null;
    }
}

/********************************************************************
Configura os controles do arquivo.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function windowOnLoad_UniquePart() {
    var eLeft = 2;
    var eTop = 2;
    // Until 23/06/2003 var eGap = 3;
    var eGap = 0;
    var extraSepGap = 2;
    var sepHeight = 22;
    var rQuote = 0;

    var coll;
    var i;

    // Prepara os anchors
    coll = window.document.getElementsByTagName('A');

    for (i = 0; i < coll.length; i++) {
        with (coll.item(i)) {
            onclick = fireFunction;
            onmouseover = rollImageOver;
            onmouseout = rollImageOut;
            onmousedown = rollImageDown;
            onmouseup = rollImageUp;
        }
    }

    // Prepara as imagens
    coll = window.document.getElementsByTagName('IMG');

    for (i = 0; i < coll.length; i++) {
        with (coll.item(i)) {
            border = 0;
            style.width = 24;
            style.height = 23;
            coll.item(i).src = defaultImg[i].src;
        }
    }

    // Ajusta os elementos

    //Combos1 e 2: sel1 e sel2
    with (sel1.style) {
        left = eLeft;
        top = eTop;
        width = 153;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (sel2.style) {
        left = rQuote + 2 * eGap + 1;
        top = eTop;
        width = 153;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    // Combo sel1_Clone
    if (getHtmlId() == 'controlbarinfHtml') {
        with (sel1_Clone.style) {
            left = sel1.offsetLeft;
            top = sel1.offsetTop;
            width = sel1.offsetWidth;
            visibility = 'hidden';
        }

        sel1_Clone.disabled = true;
    }

    // Separador 1
    with (sep1_1.style) {
        //left = rQuote + eGap + 2 * extraSepGap + 2;
        //top = eTop;
        //width = 1;
        //height = sepHeight;
        //color = 'gray';
        //rQuote = parseInt(left, 10) + parseInt(width, 10);

        // Substitue o comentado acima em 21/10/2003
        left = 0;
        top = 0;
        width = 0;
        height = 0;
        color = 'transparent';
        rQuote = rQuote;
    }
    with (sep1_2.style) {
        //left = rQuote;
        //top = eTop;
        //width = 1;
        //height = sepHeight;
        //color = 'white';
        //rQuote = parseInt(left, 10) + parseInt(width, 10);

        // Substitue o comentado acima em 21/10/2003
        left = 0;
        top = 0;
        width = 0;
        height = 0;
        color = 'transparent';
        //rQuote = rQuote + 4;

        rQuote = rQuote + 2;
    }

    // Grupo 1 - Listar, Detalhar
    with (img1.style) {
        left = rQuote + eGap /* + extraSepGap*/ + 1;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img2.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    // Separador 2
    with (sep2_1.style) {
        left = rQuote /*+ eGap + extraSepGap*/;
        top = eTop;
        width = 1;
        height = sepHeight;
        color = 'gray';
        rQuote = parseInt(left, 10) /*+ parseInt(width, 10)*/;

        // Substitue o comentado acima em 25/07/2004
        //left = 0;
        top = 0;
        width = 0;
        height = 0;
        color = 'transparent';
    }
    with (sep2_2.style) {
        left = rQuote;
        top = eTop;
        width = 1;
        height = sepHeight;
        color = 'white';

        // Substitue o comentado acima em 25/07/2004
        left = 0;
        top = 0;
        width = 0;
        height = 0;
        color = 'transparent';
    }

    // Grupo 2 - Incluir, Alterar, Estado, Excluir
    with (img3.style) {
        left = rQuote + eGap /*+ extraSepGap*/ + 1;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img4.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img5.style) {
        left = rQuote + eGap + extraSepGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img6.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    // Separador 3
    with (sep3_1.style) {
        left = rQuote /*+ eGap + extraSepGap*/;
        top = eTop;
        width = 1;
        height = sepHeight;
        color = 'gray';
        rQuote = parseInt(left, 10) /*+ parseInt(width, 10)*/;

        // Substitue o comentado acima em 25/07/2004
        left = 0;
        top = 0;
        width = 0;
        height = 0;
        color = 'transparent';
    }
    with (sep3_2.style) {
        left = rQuote;
        top = eTop;
        width = 1;
        height = sepHeight;
        color = 'white';
        rQuote = parseInt(left, 10) /*+ parseInt(width, 10)*/;

        // Substitue o comentado acima em 25/07/2004
        left = 0;
        top = 0;
        width = 0;
        height = 0;
        color = 'transparent';
    }

    // Grupo 3 - OK, Cancela, Refresh
    with (img7.style) {
        left = rQuote + eGap + extraSepGap + 1;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img8.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img9.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    // Separador 4
    with (sep4_1.style) {
        left = rQuote /*+ eGap + extraSepGap*/;
        top = eTop;
        width = 1;
        height = sepHeight;
        color = 'gray';
        rQuote = parseInt(left, 10) /*+ parseInt(width, 10)*/;

        // Substitue o comentado acima em 25/07/2004
        left = 0;
        top = 0;
        width = 0;
        height = 0;
        color = 'transparent';
    }
    with (sep4_2.style) {
        left = rQuote;
        top = eTop;
        width = 1;
        height = sepHeight;
        color = 'white';
        rQuote = parseInt(left, 10) /*+ parseInt(width, 10)*/;

        // Substitue o comentado acima em 25/07/2004
        left = 0;
        top = 0;
        width = 0;
        height = 0;
        color = 'transparent';
    }

    // Grupo 4 - Anterior, Seguinte
    with (img10.style) {
        left = rQuote + eGap + extraSepGap + 1;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img11.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    // Separador 5
    with (sep5_1.style) {
        left = rQuote + eGap + extraSepGap;
        top = eTop;
        width = 1;
        height = sepHeight;
        color = 'gray';
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (sep5_2.style) {
        left = rQuote;
        top = eTop;
        width = 1;
        height = sepHeight;
        color = 'white';
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    // Grupo 2 - Botoes especificos, atualmente 8
    with (img12.style) {
        left = rQuote + eGap + extraSepGap + 1;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img13.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img14.style) {
        left = rQuote + eGap + extraSepGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img15.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img16.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img17.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img18.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img19.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    // ovr1024
    with (img20.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (img21.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (img22.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (img23.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (img24.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (img25.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (img26.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (img27.style) {
        left = rQuote + eGap;
        top = eTop;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
        
    // Linha horizontal de arremate inferior
    with (bottonLine.style) {
        left = 0;
        top = 2 * eTop + sepHeight;
        height = 1;
        width = MAX_FRAMEWIDTH;
        color = 'gray';
    }

    // Inverte botoes da barra
    __setupBtnsOrder();

   btnNotificacoes.style.visibility = 'inherit';
   btnNotificacoes.disable = false;
   windowOnLoad_Complement();
}

/********************************************************************
Operacoes complementares do carregamento
********************************************************************/
function windowOnLoad_Complement() {
    var i;
    var coll;

    // Combos entram desabilitados
    // Direciona onchange dos combos
    coll = window.document.getElementsByTagName('SELECT');

    for (i = 0; i < coll.length; i++) {
        coll.item(i).disabled = true;
        coll.item(i).onchange = onChangeCmb;
    }

    // Esconde os botoes especificos
    // ovr1024
    showBtnsEspec(false, new Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1));

    /***************************
    // Body
    with ( controlbarBody )
    {
    style.visibility = 'visible';
    style.backgroundColor = 'silver';
    scroll = 'no';
    }
        
    // O arquivo carregou
    if ( getIfSupOrInf() == 'SUP' )
    sendJSMessage(getHtmlId(), JS_PAGELOADED, new Array(window.top.name.toString(), 'USERCHILDBROWSER'), 0X2);
    else
    sendJSMessage(getHtmlId(), JS_PAGELOADED, new Array(window.top.name.toString(), 'USERCHILDBROWSER'), 0X4);
    **************************/
}

/********************************************************************
Retorna se o arquivo que esta usando a biblioteca e o sup ou o inf
********************************************************************/
function getIfSupOrInf() {
    if ((window.document.getElementsByTagName('HTML').item(0).id.toUpperCase()).indexOf('SUP') >= 0)
        return 'SUP';
    else
        return 'INF';
}

/********************************************************************
Retorna id da tag HTML do arquivo
********************************************************************/
function getHtmlId() {
    return window.document.getElementsByTagName('HTML').item(0).id;
}

// FINAL DE FUNCOES GERAIS ******************************************

// FUNCOES DE TROCA DE IMAGENS **************************************

/********************************************************************
Troca a imagem do bot�o conforme o evento disparado pelo mouse
na imagem do anchor. Mouse over.
********************************************************************/
function rollImageOver() {
    var anchorIndex = parseInt(this.id, 10) - 1;

    if ((btnState[anchorIndex] == 3) || (btnState[anchorIndex] == 4))
        return;

    document.images[anchorIndex].src = overImg[anchorIndex].src;
}

/********************************************************************
Troca a imagem do bot�o conforme o evento disparado pelo mouse
na imagem do anchor. Mouse out.
********************************************************************/
function rollImageOut() {
    var anchorIndex = parseInt(this.id, 10) - 1;

    if ((btnState[anchorIndex] == 3) || (btnState[anchorIndex] == 4))
        return;

    document.images[anchorIndex].src = colorImg[anchorIndex].src;
}

/********************************************************************
Troca a imagem do bot�o conforme o evento disparado pelo mouse
na imagem do anchor. Mouse down.
********************************************************************/
function rollImageDown() {
    var anchorIndex = parseInt(this.id, 10) - 1;

    if ((btnState[anchorIndex] == 3) || (btnState[anchorIndex] == 4))
        return;

    document.images[anchorIndex].src = downImg[anchorIndex].src;
}

/********************************************************************
Troca a imagem do bot�o conforme o evento disparado pelo mouse
na imagem do anchor. Mouse up.
********************************************************************/
function rollImageUp() {
    var anchorIndex = parseInt(this.id, 10) - 1;

    if ((btnState[anchorIndex] == 3) || (btnState[anchorIndex] == 4))
        return;

    document.images[anchorIndex].src = overImg[anchorIndex].src;
}

// FINAL DE FUNCOES DE TROCA DE IMAGENS *****************************

// FUNCOES DE CONFIGURACAO DOS BOTOES E COMBOS **********************

/********************************************************************
Configura os combos e os botoes do formbar
********************************************************************/
function setupFBar(cmb1Dis, cmb2Dis, btnsMode) {
    sel1.disabled = cmb1Dis;
    sel2.disabled = cmb2Dis;

    // Separa a string de estado dos bot�es
    parseStrBtns(btnsMode, 0);
}

/********************************************************************
Configura os botoes especificos do formbar
********************************************************************/
function setupEspecFBar(btnsMode) {
    // Separa a string de estado dos bot�es
    parseStrBtns(btnsMode, 11);
}

/********************************************************************
Funcao auxiliar. Da parse na string de configuracao dos botoes

Parametros:
fBtn    - botao onde inicia o parse (0 ou 11 se especifico)
********************************************************************/
function parseStrBtns(btnsMode, fBtn) {
    var i, j = btnsMode.length;

    for (i = 0; i < j; i++)
        if (__btnLocked[i + fBtn] == true)
        btnState[i + fBtn] = 4;
    else
        btnState[i + fBtn] = eval(btnsMode.charAt(i));

    allBtnsGrayOrColorOrPressed();
}

/********************************************************************
Funcao auxiliar. Coloca os bot�es ou cinza ou coloridos chapados
ou coloridos afundados em fun��o do btnsMode
    
1 - chapado, colorido, habilitado
3 - pressionado, colorido, desabilitado
4 - chapado, cinza, desabilitado
********************************************************************/
function allBtnsGrayOrColorOrPressed() {
    // manter comentado -> testa se esta travado
    // so pode ser usado se comentar
    // if (htmlScript.fBarIsLocked)
    // return false;
    // em function setupControlBar(theBar, combo1, combo2, buttonsArray)
    // na lib js_controlsbar.js

    //if (fBarIsLocked)
    //{
    //    resaveStateBarLocked();
    //    return false;
    //}    

    var i;

    for (i = 0; i < btnState.length; i++) {
        if (btnState[i] == 1) {
            document.images[i].src = colorImg[i].src;
            continue;
        }
        if (btnState[i] == 3) {
            window.document.images[i].src = downImg[i].src;
            continue;
        }

        window.document.images[i].src = defaultImg[i].src;
    }
}

/********************************************************************
Salva o estado da barra e congela a mesma
********************************************************************/
function saveStateBarAndLockIt() {
    // Feito para dois combos
    stateSaved = '';
    stateSaved = sel1.disabled.toString();
    stateSaved += '|';
    stateSaved += sel2.disabled.toString();
    stateSaved += '|';

    var i, j = btnState.length;
    var tmpBtn = new String();

    tmpBtn = '';
    for (i = 0; i < j; i++) {
        stateSaved += btnState[i].toString();
        tmpBtn += '4';
    }

    // State saved, lock the bar
    setupFBar(true, true, tmpBtn);

    fBarIsLocked = true;
}

/********************************************************************
Ressalva o estado da barra.
Funcao interna usada apenas com a barra ja congelada.
********************************************************************/
function resaveStateBarLocked() {
    // Feito para dois combos
    var pPass = decompParPassed(stateSaved, 3);

    // Feito para dois combos
    stateSaved = '';
    stateSaved = pPass[0];
    stateSaved += '|';
    stateSaved += pPass[1];
    stateSaved += '|';

    // o primeiro botao ocorre na seguinte posicao
    // sel1.disabled = false;
    // sel2.disabled = false;   -> no caracter [11]
    // sel1.disabled = true;
    // sel2.disabled = false;   -> no caracter [10]
    // sel1.disabled = false;
    // sel2.disabled = true;    -> no caracter [10]
    // sel1.disabled = true;
    // sel2.disabled = true;    -> no caracter [9]

    var i, j = btnState.length;
    var tmpBtn = new String();

    tmpBtn = '';
    for (i = 0; i < j; i++) {
        stateSaved += btnState[i].toString();
        tmpBtn += '4';
    }
}

/********************************************************************
Restabelece o estado da barra e descongela a mesma
********************************************************************/
function restoreStateBarAndUnlockIt() {
    // Feito para dois combos
    var pPass = decompParPassed(stateSaved, 3);

    // Restore state saved, unlock the bar
    setupFBar(eval(pPass[0]), eval(pPass[1]), pPass[2]);

    fBarIsLocked = false;
}

/********************************************************************
Funcao auxiliar
********************************************************************/
function decompParPassed(funcPar, partsPar) {
    if (partsPar == 1)
        return funcPar;

    var regExpObj = /\|/g;

    return (funcPar.split(regExpObj));
}

// FINAL DE FUNCOES DE CONFIGURACAO DOS BOTOES E COMBOS *************

// FUNCOES DE COMUNICAO COM FORMS ***********************************

/********************************************************************
Registra um html/asp para receber eventos do controlBar
********************************************************************/
function regHtml(htmlID) {
    idOfFrameAttached = htmlID;
    return true;
}

/********************************************************************
Retorna ID do html/asp atachado receber eventos do controlBar
********************************************************************/
function getIDOfFrameAttached() {
    return idOfFrameAttached;
}

/********************************************************************
Disparado pelo anchor. Informa qual botao foi clicado,
ao html que est� usando a botoeira.
********************************************************************/
function fireFunction() {
    var anchorIndex = parseInt(this.id, 10) - 1;

    if ((btnState[anchorIndex] == 3) || (btnState[anchorIndex] == 4))
        return;

    // recupera o html do frame que est� acoplado    
    var frameID = getFrameIdByHtmlId(idOfFrameAttached);
    if (!frameID)
        return;

    var htmlScript = getPageFrameInHtmlTop(frameID);
    if (!htmlScript)
        return;

    var ctlBar = getIfSupOrInf();

    // SUP
    // 0    - Listar        LIST
    // 1    - Detalhar      DET
    // INF
    // 0    - Retroceder    RETRO
    // 1    - Avancar       AVANC
    // COMUM AOS DOIS CONTROL BARS
    // 2    - Incluir       INCL
    // 3    - Alterar       ALT
    // 4    - Estado        EST
    // 5    - Excluir       EXCL
    // 6    - Ok            OK
    // 7    - Cancelar      CANC
    // 8    - Refresh       REFR
    // 9    - Anterior      ANT
    // 10   - Seguinte      SEG
    // 11   - Um            UM
    // 12   - Dois          DOIS
    // 13   - Tres          TRES
    // 14   - Quatro        QUATRO
    // 15   - Cinco         CINCO
    // 16   - Seis          SEIS
    // 17   - Sete          SETE
    // 18   - Oito          OITO

    switch (anchorIndex) {
        case 0:
            {
                if (getIfSupOrInf() == 'SUP')
                    btnID = 'LIST';
                else
                    btnID = 'RETRO';
            }
            break;
        case 1:
            {
                if (getIfSupOrInf() == 'SUP')
                    btnID = 'DET';
                else
                    btnID = 'AVANC';
            }
            break;
        case 2:
            btnID = 'INCL';
            break;
        case 3:
            btnID = 'ALT';
            break;
        case 4:
            btnID = 'EST';
            break;
        case 5:
            btnID = 'EXCL';
            break;
        case 6:
            btnID = 'OK';
            break;
        case 7:
            btnID = 'CANC';
            break;
        case 8:
            btnID = 'REFR';
            break;
        case 9:
            btnID = 'ANT';
            break;
        case 10:
            btnID = 'SEG';
            break;
        case 11:
            btnID = 'UM';
            break;
        case 12:
            btnID = 'DOIS';
            break;
        case 13:
            btnID = 'TRES';
            break;
        case 14:
            btnID = 'QUATRO';
            break;
        case 15:
            btnID = 'CINCO';
            break;
        case 16:
            btnID = 'SEIS';
            break;
        case 17:
            btnID = 'SETE';
            break;
        case 18:
            btnID = 'OITO';
            break;
        case 19:
            btnID = 'NOVE';
            break;
        case 20:
            btnID = 'DEZ';
            break;
        case 21:
            btnID = 'ONZE';
            break;
        case 22:
            btnID = 'DOZE';
            break;
        case 23:
            btnID = 'TREZE';
            break;
        case 24:
            btnID = 'QUATORZE';
            break;
        case 25:
            btnID = 'QUINZE';
            break;
        case 26:
            btnID = 'DEZESSEIS';
            break;
    }

    htmlScript.emulEvent(new Array('BTN', btnID, ctlBar));
}

/********************************************************************
Mudou a selecao do option do combo.
Informa o html que est� usando a botoeira.
********************************************************************/
function onChangeCmb() {
    var cmb = this;
    var cmbNumber = parseInt((cmb.id).substring(3), 10);

    var coll = window.document.getElementsByTagName('SELECT');
    var i;

    // Combo nao tem options
    if (cmb.options.length == 0)
        return;

    // Recupera o html do frame que est� acoplado    
    var frameID = getFrameIdByHtmlId(idOfFrameAttached);
    if (!frameID)
        return;

    var htmlScript = getPageFrameInHtmlTop(frameID);
    if (!htmlScript)
        return;

    var ctlBar = getIfSupOrInf();

    lastCmbSel = cmbNumber;

    var optIndexInSel = cmb.selectedIndex;
    var optValue = cmb.value;
    var optText = cmb.options.item(optIndexInSel).text;

    htmlScript.emulEvent(new Array('CMB', cmbNumber, ctlBar, optText, optValue, optIndexInSel));
}

/********************************************************************
Retorna os dados do option corrente de um combo
********************************************************************/
function getComboData(comboNumber) {
    var cmb;

    if ((comboNumber == 1) || (comboNumber == 2))
        cmb = eval('sel' + comboNumber.toString());
    else
        return null;

    // Combo esta vazio
    if (cmb.options.length == 0)
        return null;

    var optIndexInSel = cmb.selectedIndex;
    var optValue = cmb.value;
    var optText = cmb.options.item(optIndexInSel).text;

    return new Array(optIndexInSel, optValue, optText);
}

/********************************************************************
Retorna os dados do combo a partir de seu indice
********************************************************************/
function getComboDataByOptionIndex(comboNumber, optIndexInSel) {
    var cmb;

    if ((comboNumber == 1) || (comboNumber == 2))
        cmb = eval('sel' + comboNumber.toString());
    else
        return null;

    // Combo esta vazio
    if (cmb.options.length == 0)
        return null;

    if (optIndexInSel > cmb.options.length - 1)
        return null;

    var optValue = cmb.options.item(optIndexInSel).value;
    var optText = cmb.options.item(optIndexInSel).innerText;

    return new Array(optValue, optText);
}

/********************************************************************
Habilitas cmbs da barra
********************************************************************/
function enableCmbsControlBars(cmbNumber, enable) {
    var cmb;

    if ((cmbNumber == 1) || (cmbNumber == 2))
        cmb = eval('sel' + cmbNumber.toString());
    else
        return null;

    cmb.disabled = !enable;

    return null;
}

/********************************************************************
Adiciona options a um combo
********************************************************************/
function addOptionInCombo(comboNumber, optionText, optionValue) {
    var retVal = false;
    var cmb;

    if ((comboNumber == 1) || (comboNumber == 2))
        cmb = eval('sel' + comboNumber.toString());
    else
        return retVal;

    var oOption = document.createElement("OPTION");

    oOption.text = optionText;
    oOption.value = optionValue;

    var i, coll = document.all.tags('SELECT');

    for (i = 0; i < coll.length; i++) {
        if (coll(i).id == cmb.id) {
            coll(i).add(oOption);
            retVal = true;
            break;
        }
    }

    return retVal;
}

/********************************************************************
Remove um option de um combo
********************************************************************/
function removeOptionInCombo(comboNumber, optionValue) {
    var retVal = false;
    var cmb;

    if ((comboNumber == 1) || (comboNumber == 2))
        cmb = eval('sel' + comboNumber.toString());
    else
        return retVal;

    var i, j, coll = document.all.tags('SELECT');

    for (i = 0; i < coll.length; i++) {
        if (coll(i).id == cmb.id) {
            for (j = coll(i).length - 1; j >= 0; j--) {
                if (parseFloat(coll(i).options[j].value) == optionValue) {
                    coll(i).remove(j);
                    retVal = true;
                    break;
                }
            }
            // desabilita o combo se nao tem options
            if (coll(i).length == 0)
                coll(i).disabled = true;
        }
    }

    return retVal;
}

/********************************************************************
Remove os options de um combo
********************************************************************/
function cleanupCombo(comboNumber) {
    var retVal = false;
    var cmb;

    if ((comboNumber == 1) || (comboNumber == 2))
        cmb = eval('sel' + comboNumber.toString());
    else
        return retVal;

    var i, j, coll = document.all.tags('SELECT');

    for (i = 0; i < coll.length; i++) {
        if (coll(i).id == cmb.id) {
            for (j = coll(i).length - 1; j >= 0; j--)
                coll(i).remove(j);
        }
    }

    return true;
}

/********************************************************************
Seleciona um option pelo seu value em um combo
********************************************************************/
function selByOptValueInSel(comboNumber, optionValue) {
    var retVal = false;
    var cmb;

    if ((comboNumber == 1) || (comboNumber == 2))
        cmb = eval('sel' + comboNumber.toString());
    else
        return retVal;

    var i, j, coll = document.all.tags('SELECT');

    for (i = 0; i < coll.length; i++) {
        if (coll(i).id == cmb.id) {
            for (j = 0; j < coll(i).options.length; j++) {
                if (coll(i).options.item(j).value == optionValue) {
                    coll(i).options.item(j).selected = true;
                    retVal = true;
                    break;
                }
            }
        }
    }

    return retVal;
}

/********************************************************************
Deseleciona os options de um combo
********************************************************************/
function unselOptInSel(comboNumber) {
    var retVal = false;
    var cmb;

    if ((comboNumber == 1) || (comboNumber == 2))
        cmb = eval('sel' + comboNumber.toString());
    else
        return retVal;

    var i, j, coll = document.all.tags('SELECT');

    for (i = 0; i < coll.length; i++) {
        if (coll(i).id == cmb.id) {
            coll(i).selectedIndex = -1;
            retVal = true;
            break;
        }
    }

    return retVal;
}

/********************************************************************
Retorna numero de options em um combo

Se encontra o combo retorna o numero de options, caso contrario
retorna -1
********************************************************************/
function numOptionsInCmb(comboNumber) {
    var retVal = -1;
    var cmb;

    if ((comboNumber == 1) || (comboNumber == 2))
        cmb = eval('sel' + comboNumber.toString());
    else
        return retVal;

    var i, j, coll = document.all.tags('SELECT');

    for (i = 0; i < coll.length; i++) {
        if (coll(i).id == cmb.id)
            return coll(i).length;
    }

    return retVal;
}

/********************************************************************
Coloca foco em um combo
********************************************************************/
function setFocusInCmb(comboNumber) {
    var retVal = false;
    var cmb;

    if ((comboNumber == 1) || (comboNumber == 2))
        cmb = eval('sel' + comboNumber.toString());
    else
        return retVal;

    if (cmb.disabled == false) {
        try {
            window.focus();
            cmb.focus();

            // guarda o combo focado
            lastCmbSel = comboNumber;
        }
        catch (e) {
            return false;
        }

        return true;
    }

    return false;
}

/********************************************************************
Informa se um dado botao esta habilitado ou nao
********************************************************************/
function btnIsEnable(btnNumber) {
    // trap, botao nao existe na barra
    if ((btnNumber < 1) || (btnNumber > btnState.length))
        return null;

    // botao esta desabilitado
    if ((btnState[btnNumber - 1] == 3) || (btnState[btnNumber - 1] == 4))
        return false;
    // botao esta habilitado
    return true;
}

/********************************************************************
Retorna string array ou null dos values de um combo
********************************************************************/
function strArrayValuesInCmb(comboNumber) {
    var retVal = '';
    var cmb;

    if ((comboNumber == 1) || (comboNumber == 2))
        cmb = eval('sel' + comboNumber.toString());
    else
        return retVal;

    var i, j, coll = document.all.tags('SELECT');

    for (i = 0; i < coll.length; i++) {
        if (coll(i).id == cmb.id) {
            for (j = coll(i).length - 1; j >= 0; j--) {
                if (j > 0)
                    retVal += coll(i).options(j).value + ',';
                else
                    retVal += coll(i).options(j).value;
            }
        }
    }

    if (retVal == '')
        retVal = null;

    return retVal;
}

/********************************************************************
Mostra ou esconde combo clone do combo de pastas do control bar
inferior
********************************************************************/
function showCmb_sel1_Clone(bShow) {
    sel1_Clone.disabled = true;

    if (bShow) {
        if ((sel1.options.length > 0) && (sel1.selectedIndex != -1)) {
            sel1_Clone.options.item(0).text =
				sel1.options(sel1.selectedIndex).text;

            sel1_Clone.style.visibility = 'inherit';
            sel1_Clone.style.zIndex = 9;
        }
    }
    else {
        sel1_Clone.options.item(0).text = '';

        sel1_Clone.style.visibility = 'hidden';
        sel1_Clone.style.zIndex = 0;

    }
}

// FINAL DE FUNCOES DE COMUNICAO COM FORMS **************************

// FUNCOES DOS BOTOES ESPECIFICOS ***********************************

/********************************************************************
Mostra ou esconde o separador dos botoes especificos e
cada um dos botoes especificos
********************************************************************/
function showBtnsEspec(cmdShow, arrayBtnsAffected) {
    if (arrayBtnsAffected.length > NUM_ESPEC_BTNS)
        return;

    var i;
    var aLen = arrayBtnsAffected.length;

    if (cmdShow == true) {
        sep5_1.style.visibility = 'visible';
        sep5_2.style.visibility = 'visible';

        for (i = 0; i < aLen; i++) {
            if (arrayBtnsAffected[i] == 1)
                window.document.getElementById('img' + (12 + i).toString()).style.visibility = 'visible';
        }
    }
    else if (cmdShow == false) {
        sep5_1.style.visibility = 'hidden';
        sep5_2.style.visibility = 'hidden';

        for (i = 0; i < aLen; i++) {
            if (arrayBtnsAffected[i] == 1)
                window.document.getElementById('img' + (12 + i).toString()).style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Retorna true se um botao especifico esta visivel, false se nao esta
visivel e null e o botao esta errado
********************************************************************/
function btnSpcIsVisible(btnNumber) {
    if (btnNumber == 12)
        return img12.currentStyle.visibility;
    else if (btnNumber == 13)
        return img13.currentStyle.visibility;
    else if (btnNumber == 14)
        return img14.currentStyle.visibility;
    else if (btnNumber == 15)
        return img15.currentStyle.visibility;
    else if (btnNumber == 16)
        return img16.currentStyle.visibility;
    else if (btnNumber == 17)
        return img17.currentStyle.visibility;
    else if (btnNumber == 18)
        return img18.currentStyle.visibility;
    else if (btnNumber == 19)
        return img19.currentStyle.visibility;
    else
        return null;
}

/********************************************************************
Troca os hints dos botoes especificos
********************************************************************/
function hintsBtnsEspec(hintArray) {
    if (hintArray == null)
        return;

    if (hintArray.length > NUM_ESPEC_BTNS)
        return;

    var i;
    var aLen = hintArray.length;

    for (i = 0; i < NUM_ESPEC_BTNS; i++) {
        window.document.getElementById('img' + (12 + i).toString()).title = '';
    }

    for (i = 0; i < aLen; i++) {
        window.document.getElementById('img' + (12 + i).toString()).title = hintArray[i];
    }
}

/********************************************************************
Seta um botao especifico para imagem de print
btnNumber == 0 -> Nenhum botao tem imagem de print
********************************************************************/
function printBtn(btnNumber) {
    // 1024x768, aumento do numero de botoes especificos
    return null;
    
    var i;

    if ((btnNumber == null) || (btnNumber < 0) || (btnNumber > NUM_ESPEC_BTNS))
        btnNumber == 0;

    for (i = 0; i < 15; i++) {
        // botao 1 tem imagem de print
        if (btnNumber == i + 1) {
            defaultImg[(11 + i)].src = window.top.opener.defaultImgP;
            overImg[(11 + i)].src = window.top.opener.overImgP;
            downImg[(11 + i)].src = window.top.opener.downImgP;
            colorImg[(11 + i)].src = window.top.opener.colorImgP;
            continue;
        }

        defaultImg[(11 + i)].src = window.top.opener.defaultImg[(11 + i)];
        overImg[(11 + i)].src = window.top.opener.overImg[(11 + i)];
        downImg[(11 + i)].src = window.top.opener.downImg[(11 + i)];
        colorImg[(11 + i)].src = window.top.opener.colorImg[(11 + i)];
    }
}


/********************************************************************
Clique botao de Notificacoes
********************************************************************/
function btn_onclick(ctl) {
    
    window.focus();
    if (ctl.id == btnNotificacoes.id)
        btnNotificacoes.focus();
   
    if (ctl.id == btnNotificacoes.id) {
        // 1. O usuario clicou o botao Notificacao
        sendJSMessage('controlbarsupHtml', JS_NOTIFICATIONCALL, null, null);
    }

}

// FINAL DE FUNCOES DOS BOTOES ESPECIFICOS **************************

function confBotaoNotificacao(quantidadeNotificacoes) {
   
    with (btnNotificacoes.style) {
        left = 970;
        top = 3;
        height = 20;
        width = 25;
        if (quantidadeNotificacoes > 0) {
            color = 'white';
            background = 'red';
            btnNotificacoes.value = quantidadeNotificacoes;
        }
        else {
            background = 'transparent';
            color = 'white';
            background = 'gray';
            btnNotificacoes.value = 0;
        }
    }

}


var glb_aBtnsOffSetLeft;

glb_aBtnsOffSetLeft = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

function __setupBtnsOrder() {
    var i;

    for (i = 1; i <= NUM_GEN_BTNS; i++) {
        glb_aBtnsOffSetLeft[i] = (eval('img' + i.toString())).offsetLeft;
    }


    for (i = 3; i <= NUM_GEN_BTNS; i++) {
        // Avancar os botoes 3 a 8 tres posicoes
        if ((i >= 3) && (i <= 8))
            (eval('img' + i.toString())).style.left = glb_aBtnsOffSetLeft[i + 3];
        // Colocar os tres botoes 9, 10 e 11, ap�s o botao 2	
        if ((i >= 9) && (i <= 11))
            (eval('img' + i.toString())).style.left = glb_aBtnsOffSetLeft[i - 6];

        (eval('img6')).style.left = glb_aBtnsOffSetLeft[7];
        (eval('img4')).style.left = glb_aBtnsOffSetLeft[8];
        (eval('img5')).style.left = glb_aBtnsOffSetLeft[9];
    }

}
