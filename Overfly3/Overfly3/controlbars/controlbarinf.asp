<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="controlbarinfHtml" name="controlbarinfHtml">

<head>
<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/controlbars/controlbar.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/controlbars/commoncontrolbar.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<script LANGUAGE="javascript" FOR="window" EVENT="onload">
<!--
 window_onload();
//-->
</script>

</head>

<body id="controlbarBody" name="controlbarBody" LANGUAGE="javascript" onload="return window_onload()">
    
    <!-- Combos 1 e 2 -->
    <select id="sel1" name="sel1" class = "fldGeneral" title="Pastas"></select>
    <select id="sel2" name="sel2" class = "fldGeneral" title="Filtros Padr�es"></select>
    <select id="sel1_Clone" name="sel1_Clone" class = "fldGeneral" title="Pastas">
		<option value=0></option>
    </select>
    
    <!-- Separador 1 -->
    <hr id="sep1_1" name = "sep1_1" class="sepGeneral"></hr>
    <hr id="sep1_2" name = "sep1_2" class="sepGeneral"></hr>

    <!-- Grupo 1 -->
    <!-- Listar -->
    <a id="1" name="anchorList"><img id="img1" name="img1" class="imgGeneral" title="Retroceder"></img></a>
    <!-- Detalhar -->
    <a id="2" name="anchorDet"><img id="img2" name="img2" class="imgGeneral" title="Avan�ar"></img></a>

    <!-- Separador 2 -->
    <hr id="sep2_1" name = "sep2_1" class="sepGeneral"></hr>
    <hr id="sep2_2" name = "sep2_2" class="sepGeneral"></hr>

    <!-- Grupo 2 -->
    <!-- Incluir -->
    <a id="3" name="anchorIncl"><img id="img3" name="img3" class="imgGeneral" title="Incluir"></img></a>
    <!-- Alterar -->
    <a id="4" name="anchorAlt"><img id="img4" name="img4" class="imgGeneral" title="Alterar"></img></a>
    <!-- Estado -->
    <a id="5" name="anchorEst"><img id="img5" name="img5" class="imgGeneral" title="Estado"></img></a>
    <!-- Excluir -->
    <a id="6" name="anchorExcl"><img id="img6" name="img6" class="imgGeneral" title="Excluir"></img></a>

    <!-- Separador 3 -->
    <hr id="sep3_1" name = "sep3_1" class="sepGeneral"></hr>
    <hr id="sep3_2" name = "sep3_2" class="sepGeneral"></hr>

    <!-- Grupo 3 -->
    <!-- OK -->
    <a id="7" name="anchorOK"><img id="img7" name="img7" class="imgGeneral" title="OK"></img></a>
    <!-- Cancelar -->
    <a id="8" name="anchorCanc"><img id="img8" name="img8" class="imgGeneral" title="Cancelar"></img></a>
    <!-- Refresh -->
    <a id="9" name="anchorRefr"><img id="img9" name="img9" class="imgGeneral" title="Refresh"></img></a>

    <!-- Separador 4 -->
    <hr id="sep4_1" name = "sep4_1" class="sepGeneral"></hr>
    <hr id="sep4_2" name = "sep4_2" class="sepGeneral"></hr>
    
    <!-- Grupo 4 -->
    <!-- Anterior -->
    <a id="10" name="anchorAnt"><img id="img10" name="img10" class="imgGeneral" title="Anterior"></img></a>
    <!-- Seguinte -->
    <a id="11" name="anchorSeg"><img id="img11" name="img11" class="imgGeneral" title="Seguinte"></img></a>

    <!-- Separador 5 -->
    <hr id="sep5_1" name = "sep5_1" class="sepGeneral"></hr>
    <hr id="sep5_2" name = "sep5_2" class="sepGeneral"></hr>

    <!-- Grupo 5 -->
    <!-- Especifico 1 -->
    <a id="12" name="anchorEsp1"><img id="img12" name="img12" class="imgGeneral" title="Um"></img></a>
    <!-- Especifico 2 -->
    <a id="13" name="anchorEsp2"><img id="img13" name="img13" class="imgGeneral" title="Dois"></img></a>
    <!-- Especifico 3 -->
    <a id="14" name="anchorEsp3"><img id="img14" name="img14" class="imgGeneral" title="Tres"></img></a>
    <!-- Especifico 4 -->
    <a id="15" name="anchorEsp4"><img id="img15" name="img15" class="imgGeneral" title="Quatro"></img></a>
	<!-- Especifico 5 -->
    <a id="16" name="anchorEsp5"><img id="img16" name="img16" class="imgGeneral" title="Cinco"></img></a>
    <!-- Especifico 6 -->
    <a id="17" name="anchorEsp6"><img id="img17" name="img17" class="imgGeneral" title="Seis"></img></a>
	<!-- Especifico 7 -->
    <a id="18" name="anchorEsp7"><img id="img18" name="img18" class="imgGeneral" title="Sete"></img></a>
	<!-- Especifico 8 -->
    <a id="19" name="anchorEsp8"><img id="img19" name="img19" class="imgGeneral" title="Oito"></img></a>    
	<!-- Especifico 9 -->
    <a id="20" name="anchorEsp9"><img id="img20" name="img20" class="imgGeneral" title="Nove"></img></a>    
	<!-- Especifico 10 -->
    <a id="21" name="anchorEsp10"><img id="img21" name="img21" class="imgGeneral" title="Dez"></img></a>    
	<!-- Especifico 11 -->
    <a id="22" name="anchorEsp11"><img id="img22" name="img22" class="imgGeneral" title="Onze"></img></a>    
	<!-- Especifico 12 -->
    <a id="23" name="anchorEsp12"><img id="img23" name="img23" class="imgGeneral" title="Doze"></img></a>    
	<!-- Especifico 13 -->
    <a id="24" name="anchorEsp13"><img id="img24" name="img24" class="imgGeneral" title="Treze"></img></a>    
	<!-- Especifico 14 -->
    <a id="25" name="anchorEsp14"><img id="img25" name="img25" class="imgGeneral" title="Quartorze"></img></a>    
	<!-- Especifico 15 -->
    <a id="26" name="anchorEsp15"><img id="img26" name="img26" class="imgGeneral" title="Quinze"></img></a>    
	<!-- Especifico 16 -->
    <a id="27" name="anchorEsp16"><img id="img27" name="img27" class="imgGeneral" title="Dezesseis"></img></a>    
    <!-- Bot�o de Notifica��es -->
    <input type="button" id="btnNotificacoes" name="btnNotificacoes" value="" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="botaoinf">
    <!-- Linha de arremate inferior -->
    <hr id="bottonLine" name="bottonLine" class="sepGeneral"></hr>

</body>

</html>
