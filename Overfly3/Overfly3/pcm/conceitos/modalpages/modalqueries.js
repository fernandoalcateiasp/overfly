﻿var glb_selIdiomaIndexOld = -1;
var glb_nB1I;

var dsoQueries = new CDatatransport("dsoQueries");
/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    var elem = document.getElementById('modalqueriesBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    selIdiomaID.onchange = selIdiomaID_onchange;
    
    if (selIdiomaID.selectedIndex >= 0)
    {
        glb_selIdiomaIndexOld = selIdiomaID.selectedIndex;
        txtQuery.value = selIdiomaID.options[selIdiomaID.selectedIndex].getAttribute('argumentosVenda', 1);
        chkOK.checked = (selIdiomaID.options[selIdiomaID.selectedIndex].getAttribute('OK', 1) == "True");
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    if (! txtQuery.readOnly )
        txtQuery.focus();
}

function selIdiomaID_onchange()
{
    if (glb_selIdiomaIndexOld >= 0) {
        selIdiomaID.options[glb_selIdiomaIndexOld].setAttribute('argumentosVenda', replaceStr(txtQuery.value, '\'', ''), 1);
        selIdiomaID.options[glb_selIdiomaIndexOld].setAttribute('OK', (chkOK.checked ? "True" : "False"), 1);
    }

    txtQuery.value = selIdiomaID.options[selIdiomaID.selectedIndex].getAttribute('argumentosVenda', 1);

    chkOK.checked = (selIdiomaID.options[selIdiomaID.selectedIndex].getAttribute('OK', 1) == "True");
    
    glb_selIdiomaIndexOld = selIdiomaID.selectedIndex;
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	var sQuery = '';
	
    // texto da secao01
    secText('Argumento de venda' + glb_sConta, 1);

    // reajusta dimensoes e reposiciona a janela
    (getFrameInHtmlTop( getExtFrameID(window))).style.top = (getFrameInHtmlTop( 'frameSup01')).offsetTop;

    // reajusta dimensoes e reposiciona a janela
	//redimAndReposicionModalWin(740, 490, false);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    var nMemoHeight = 375;
    
    // ajusta o divCampos
    elem = window.document.getElementById('divCampos');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = MAX_FRAMEWIDTH_OLD - (ELEM_GAP * 7);
        temp = parseInt(width);
        height = 50;
    }

    // ajusta o divObservacoes
    elem = window.document.getElementById('divObservacoes');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divCampos.offsetTop + divCampos.offsetHeight;
        width = MAX_FRAMEWIDTH_OLD - (ELEM_GAP * 7);
        temp = parseInt(width);
        //height = (nMemoHeight * 1.5);
        height = 320;
    }

    // ajusta o lblOK
    elem = window.document.getElementById('lblOK');
    with (elem.style) {
        top = 2;
        left = 180;
    }

    // ajusta o chkOK
    elem = window.document.getElementById('chkOK');
    with (elem.style)
    {
        left = 175;
        width = FONT_WIDTH * 3;
    }

    elem = document.getElementById('txtQuery');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = MAX_FRAMEWIDTH_OLD - (ELEM_GAP * 7);
        height = divObservacoes.offsetHeight - 3;
    }

    glb_nB1I = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrRightValue(\'INF\',\'B1I\')');

	sQuery = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL ,
                               glb_gridID + '.TextMatrix(' + glb_gridID + '.Row,' +
                               glb_colQuery + ')');

	if (sQuery == null)
		sQuery = '';

    //btnOK.disabled = (glb_btnOKState == 'D');
    chkOK.disabled = (!(glb_nB1I == 1));

    txtQuery.maxLength = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL ,
                              glb_dso + ".recordset['" + glb_fieldName + "'].definedSize");

	setConnection(dsoQueries);
	dsoQueries.SQL = 'SELECT * FROM Conceitos_ArgumentosVenda WITH(NOLOCK) WHERE ConceitoID=' + glb_nConceitoID + ' ORDER BY IdiomaID';
    dsoQueries.ondatasetcomplete = dsoQueries_DSC;
    dsoQueries.Refresh();
}

function dsoQueries_DSC()
{
    ;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
	var tempStr = '';
	var match = 0;
	
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        saveQueries();
		// 1. O usuario clicou o botao OK
        //sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , txtQuery.value);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    
}

function txtQuery_ondigit() {
    if ((chkOK.checked == true) && (glb_nB1I == 0)) {
        //chkOK.disabled = false;
        chkOK.checked = false;
        //chkOK.disabled = true;
    }
}

function saveQueries()
{
    var i=0;
    
    lockControlsInModalWin(true);

    var chkOKchecked = chkOK.checked;
    
    selIdiomaID_onchange();
    
    for (i=0; i<selIdiomaID.options.length; i++)
    {
        // Registro Novo
        if (selIdiomaID.options[i].getAttribute('ConArgumentoID', 1) == 0)
        {
            if (trimStr(selIdiomaID.options[i].getAttribute('argumentosVenda', 1)) != '')
            {
                dsoQueries.recordset.AddNew();
                dsoQueries.recordset['ConceitoID'].value = glb_nConceitoID;
                dsoQueries.recordset['IdiomaID'].value = selIdiomaID.options[i].value;
                dsoQueries.recordset['ArgumentosVenda'].value = selIdiomaID.options[i].getAttribute('argumentosVenda', 1);
                dsoQueries.recordset['OK'].value = selIdiomaID.options[i].getAttribute('OK', 1);
                dsoQueries.recordset['Replicado'].value = 0; //joao
            }
        }
        else if (!((dsoQueries.recordset.BOF) && (dsoQueries.recordset.EOF)))
        {
            dsoQueries.recordset.moveFirst();
            while (!dsoQueries.recordset.EOF)
            {
                if (dsoQueries.recordset['IdiomaID'].value == selIdiomaID.options[i].value)
                {
                    // Deleta o registro
                    if (trimStr(selIdiomaID.options[i].getAttribute('argumentosVenda', 1)) == '')
                        dsoQueries.recordset.Delete();
                    else {
                        dsoQueries.recordset['ArgumentosVenda'].value = selIdiomaID.options[i].getAttribute('argumentosVenda', 1);
                        dsoQueries.recordset['OK'].value = selIdiomaID.options[i].getAttribute('OK', 1);
                    }
                    
                    break;
                }
                
                dsoQueries.recordset.moveNext();
            }
        }
    }

	try
	{			
	    dsoQueries.SubmitChanges();   
		dsoQueries.ondatasetcomplete = gravacao_DSC;
		dsoQueries.Refresh();
	}
	catch(e)
	{
	    // Numero de erro qdo o registro foi alterado ou removido por outro usuario
	    // ou operacao de gravacao abortada no banco
	    if (e.number == -2147217887)
	    {
	        if ( window.top.overflyGen.Alert('Erro ao gravar.') == 0 )
	            return null;
	            
	        lockControlsInModalWin(false);
	    }
	}
}

function gravacao_DSC()
{
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , null);
}

