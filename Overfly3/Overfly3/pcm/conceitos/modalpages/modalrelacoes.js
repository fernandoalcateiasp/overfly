/********************************************************************
modalrelacoes.js

Library javascript para o modalrelacoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var modPesqrelacoes_Timer = null;

var dsoPesq = new CDatatransport("dsoPesq");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modalrelacoesBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtSujObj').disabled == false )
        txtSujObj.focus();

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar ' + glb_sHeader, 1);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    
    // ajusta o divCidade
    elem = window.document.getElementById('divCidade');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        temp = parseInt(width, 10);
        height = 40;
        
    }
    
    // txtSujObj
    elem = window.document.getElementById('txtSujObj');
    elem.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (elem.style)
    {
        left = 0;
        top = 16;
        width = (elem.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }
    elem.onkeypress = txtSujObj_onKeyPress;
    
    // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');
    elem.disabled = true;

    with (elem.style)
    {
        top = parseInt(document.getElementById('txtSujObj').style.top, 10);
        left = parseInt(document.getElementById('txtSujObj').style.left, 10) + parseInt(document.getElementById('txtSujObj').style.width, 10) + 2;
        width = 80;
        height = 24;
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divCidade').style.top, 10) + parseInt(document.getElementById('divCidade').style.height, 10) + ELEM_GAP;
        width = temp + 221;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width, 10);
        height = parseInt(document.getElementById('divFG').style.height, 10);
    }
    
    startGridInterface(fg);
    
    if (glb_nRelEntreID == 134)
    {
        headerGrid(fg,['Conceito',
                       'ID',
                       'Conceito'], [2]);
    }
    else if (glb_nRelEntreID==135)
    {    
        headerGrid(fg,['Nome',
                       'ID',
                       'Fantasia',
                       'Documento',
                       'Cidade',
                       'UF',
                       'Pa�s'], [2]);
    }
    
    fg.Redraw = 2;
    
    lblCidade.innerText = glb_sHeader;
}

function txtSujObj_onKeyPress()
{
    if ( event.keyCode == 13 )
    {
        lockControlsInModalWin(true);
        modPesqrelacoes_Timer = window.setInterval('btnFindPesquisa_onclick()', 30, 'JavaScript');
    }
}

function txtSujObj_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    if ( modPesqrelacoes_Timer != null )
    {
        window.clearInterval(modPesqrelacoes_Timer);
        modPesqrelacoes_Timer = null;
    }
    
    txtSujObj.value = trimStr(txtSujObj.value);
    
    changeBtnState(txtSujObj.value);

    if (btnFindPesquisa.disabled)
    {
        lockControlsInModalWin(false);
        return;
    }    
    
    startPesq(txtSujObj.value);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqrelacoesDblClick()
{
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqrelacoesKeyPress(KeyAscii)
{
    if ( fg.Row < 1 )
        return true;
 
    if ( (KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , new Array(
                      fg.TextMatrix(fg.Row, 2), fg.TextMatrix(fg.Row, 1), 
                      glb_nTipoRelID) );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

function startPesq(strPesquisa)
{
    lockControlsInModalWin(true);
    
    var strPas = '?';
    strPas += 'strToFind='+escape(strPesquisa);
    strPas += '&nTipoRelID='+escape(glb_nTipoRelID);
    strPas += '&sTipoSujObj='+escape(glb_sTipoSujObj);
    strPas += '&nSujObjID='+escape(glb_nSujObjID);
    strPas += '&nRelEntreID='+escape(glb_nRelEntreID);
    
    dsoPesq.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/pesqsujobj.aspx'+strPas;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC()
{
    startGridInterface(fg);
    fg.FrozenCols = 0;
    
    if (glb_nRelEntreID == 134)
    {
        headerGrid(fg,['Conceito',
                       'ID',
                       'Conceito'], [2]);
        
        fillGridMask(fg,dsoPesq,['fldName',
                                 'fldID',
                                 'Fantasia'],['','']);
    }   
    else if (glb_nRelEntreID==135)
    {    
        headerGrid(fg,['Nome',
                       'ID',
                       'Fantasia',
                       'Documento',
                       'Cidade',
                       'UF',
                       'Pa�s'], [2]);

        fillGridMask(fg,dsoPesq,['fldName',
                                 'fldID',
                                 'Fantasia',
                                 'Documento',
                                 'Cidade',
                                 'UF',
                                 'Pais'],['','','','','','','']);
        fg.FrozenCols = 1;
    }
    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
        btnOK.disabled = false;
    }    
    else
        btnOK.disabled = true;    
        
    // writeInStatusBar('child', 'cellMode', 'XXXXXXXX');    
    
}
