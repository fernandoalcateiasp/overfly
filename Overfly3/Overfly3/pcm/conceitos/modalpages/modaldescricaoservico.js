﻿var glb_selIdiomaIndexOld = -1;

var dsoQueries = new CDatatransport("dsoQueries");

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    var elem = document.getElementById('modaldescricaoservicoBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Descrições', 1);

    // reajusta dimensoes e reposiciona a janela
    (getFrameInHtmlTop( getExtFrameID(window))).style.top = (getFrameInHtmlTop( 'frameSup01')).offsetTop;

    // reajusta dimensoes e reposiciona a janela
	//redimAndReposicionModalWin(740, 490, false);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    var nMemoHeight = 375;
    
    // ajusta o divObservacoes
    elem = window.document.getElementById('divObservacoes');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = MAX_FRAMEWIDTH_OLD - (ELEM_GAP * 7);
        temp = parseInt(width);
        height = 320;
    }

    elem = document.getElementById('txtQuery');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = MAX_FRAMEWIDTH_OLD - (ELEM_GAP * 7);
        height = divObservacoes.offsetHeight - 3;
    }

    btnOK.disabled = (glb_btnOKState == 'D');



    if (glb_nConServicoID > 0) 
    {
        lockControlsInModalWin(true);

        setConnection(dsoQueries);
        dsoQueries.SQL = 'SELECT * FROM Conceitos_Servicos WITH(NOLOCK) WHERE ConServicoID = ' + glb_nConServicoID;
        dsoQueries.ondatasetcomplete = dsoQueries_DSC;
        dsoQueries.Refresh();
    }
}

function dsoQueries_DSC()
{
    dsoQueries.recordset.MoveFirst();
    
    if ((!dsoQueries.recordset.BOF) && (!dsoQueries.recordset.EOF))
    {
        txtQuery.maxLength = dsoQueries.recordset['DescricaoLocal'].definedSize;
        txtQuery.value = dsoQueries.recordset['DescricaoLocal'].value;
    }

    lockControlsInModalWin(false);

    txtQuery.readOnly = (glb_btnOKState == 'D');

    if (!txtQuery.readOnly)
        txtQuery.focus();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
	var tempStr = '';
	var match = 0;
	
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', txtQuery.value);
        
        //saveQueries();
		// 1. O usuario clicou o botao OK
        //sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , txtQuery.value);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    
}