/********************************************************************
modalprodutosesd.js

Library javascript para o modalprodutosesd.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoCatalogo = new CDatatransport("dsoCatalogo");
var dsoCadastro = new CDatatransport("dsoCadastro");

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_DescricaoAutomatica;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES

FUNCOES GERAIS:
window_onload()
setupPage()
btn_onclick(ctl)
startPesq()

Eventos de grid particulares desta janela:


********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    // Carrega onload da automa��o
    window_onload_1stPart();

    // Configuracao inicial do html
    setupPage();

    // ajusta o body do html
    var elem = document.getElementById('modalprodutosesdHtmlBody');

    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // Ajusta posi��o da modal
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 27;
    //modalFrame.style.left = 0;

    // Mostra a janela modal
    showExtFrame(window, true);

    startPesq("onload");

    // coloca foco no campo apropriado
    /*if (document.getElementById('txtProdutos').disabled == false)
    txtProdutos.focus();*/
}


/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // Define o t�tulo da modal
    secText('Produtos ESD', 1);

    var elem;

    // ajusta o divControles
    elem = window.document.getElementById('divControles');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = MAX_FRAMEWIDTH_OLD - (ELEM_GAP * 7);
        temp = parseInt(width);
        height = 50;
    }

    // ajusta o btnGerarCadastro
    elem = window.document.getElementById('btnGerarCadastro');
    with (elem.style) {
        left = parseInt(getFrameInHtmlTop('frameModal').style.width) - elem.offsetWidth - 23;
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divControles').style.top) + parseInt(document.getElementById('divControles').style.height) - ELEM_GAP * 2;
        width = 855;
        height = 442;
    }

    // ajusta o grid
    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }

    startGridInterface(fg);
    headerGrid(fg, ['Part Number',
                    'Descri��o',
                    'Fabricante',
                    'Conceito',
                    'Serie',
                    'OK'], []);

    fg.Redraw = 2;

    // Desabilita bot�o de cadastrar
    btnGerarCadastro.disabled = true;
    
    // esconde bot�es OK/Cancelar
    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';
}


/********************************************************************
Clique botao OK ou Cancela
ctl.id - retorna o id do botao clicado
********************************************************************/
function btn_onclick(ctl) {
    // Trava a modal
    lockControlsInModalWin(true);

    // Clicou no bot�o OK
    if (ctl.id == btnOK.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    // Clicou no bot�o Cancelar
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    // Clicou no bot�o Listar
    else if (ctl.id == btnListar.id)
        startPesq(null);
    // Clicou no bot�o Gerar Cadastro
    else if (ctl.id == btnGerarCadastro.id)
        saveDataInGrid(fg);
}

/********************************************************************
Faz pesquisa dos produtos no WebService da Atma
********************************************************************/
function startPesq(call) {
    // Trava controles da modal
    if (call != "onload")
        lockControlsInModalWin(true);

    // Inicia conex�o do Dso
    setConnection(dsoCatalogo);
    
    dsoCatalogo.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/getCatalog.aspx';
    dsoCatalogo.ondatasetcomplete = dsoCatalogo_DSC;
    dsoCatalogo.refresh();
}

/********************************************************************
Retorno da pesquisa dos produtos para exibir no grid
********************************************************************/
function dsoCatalogo_DSC() {
    startGridInterface(fg);
    fg.FrozenCols = 0;

    // Monta grid
    headerGrid(fg, ['Part Number',
                    'Descri��o',
                    'Fabricante',
                    'Conceito',
                    'Serie',
                    'OK'], []);

    fillGridMask(fg, dsoCatalogo, ['PartNumber*',
                               'Description*',
                               'Manufacturer*',
                               'Conceito',
                               'Serie',
                               'OK'],
                               ['', '', '', '', '', ''],
                               ['', '', '', '', '', '']);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.ColWidth(getColIndexByColKey(fg, 'Conceito')) = 1500;
    fg.Redraw = 2;
    //fg.ExplorerBar = 5;

    // Destrava controles da modal
    lockControlsInModalWin(false);

    // Se existe pelo menos uma linha, torna o grid edit�vel e habilita bot�o cadastrar
    if (fg.Rows > 1) {
        fg.Editable = true;
        btnGerarCadastro.disabled = false;
    }
}

/********************************************************************
Gera cadastro de Conceitos e PesCon dos produtos selecionados
********************************************************************/
function saveDataInGrid(fg) {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;
    var nUserID = getCurrUserID();

    // Vari�veis de controle para envio ao servidor
    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_sResultado = '';

    // Trava controles da modal
    lockControlsInModalWin(true);

    // Percorre linahs do grid e pega dados dos registros marcados com OK
    for (i = 1; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0) {
            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                    strPars = '';
                    nDataLen = 0;
                }

                strPars += '?nUsuarioID=' + escape(nUserID);
            }

            var sPartNumber = getCellValueByColKey(fg, 'PartNumber*', i);
            var sConceito = getCellValueByColKey(fg, 'Conceito', i);
            var sSerie = getCellValueByColKey(fg, 'Serie', i);

            nDataLen++;
            strPars += '&sPartNumber=' + escape(sPartNumber);
            strPars += '&sConceito=' + escape(sConceito);
            strPars += '&sSerie=' + escape(sSerie);
        }
    }

    // Passa quantidade de registros a serem processados
    if (nDataLen > 0) {
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
        sendDataToServer();
    }
    else {
        if (window.top.overflyGen.Alert('Nenhum produto selecionado') == 0)
            return null;

        lockControlsInModalWin(false);
        startPesq(null);
    }
}

/********************************************************************
Envia dados ao servidor
********************************************************************/
function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoCadastro.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/cadastroesd.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoCadastro.ondatasetcomplete = sendDataToServer_DSC;
            dsoCadastro.refresh();
        }
        else {
            if (glb_sResultado == '')
                if (window.top.overflyGen.Alert('Cadastro realizado! Consulte os forms:\n - Conceitos\n - Rela��es Pessoas/Conceitos') == 0)
                   return null;
               
            lockControlsInModalWin(false);
            startPesq(null);
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}

function sendDataToServer_DSC() {
    if (!(dsoCadastro.recordset.BOF && dsoCadastro.recordset.EOF)) {
        if (dsoCadastro.recordset['fldresp'].value != 1) {
            glb_sResultado = dsoCadastro.recordset['fldresp'].value;

            if (glb_sResultado != '')
                if (window.top.overflyGen.Alert(glb_sResultado) == 0)
                    return null;
        }
    }

    // Seta um intervalo de tempo para efetivar a grava��o antes de mandar a proxima
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}


/********************************************************************
Limita quantidade de registros por coluna
********************************************************************/
function js_fg_modalprodutosesd_BeforeEdit(grid, nRow, nCol) {

    // return null;
    var nomeColuna = grid.ColKey(nCol);

    var lastPos = nomeColuna.length - 1;

    // retira o asterisco do nome do campo
    if (nomeColuna.substr(lastPos, 1) == '*') {
        nomeColuna = nomeColuna.substr(0, lastPos);
    }

    if ((dsoCatalogo.recordset[nomeColuna].type == 200) || (dsoCatalogo.recordset[nomeColuna].type == 129))
        fieldMaxLength(fg, nCol, dsoCatalogo);
}

