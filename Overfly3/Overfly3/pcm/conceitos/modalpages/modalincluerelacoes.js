/********************************************************************
modalincluerelacoes.js

Library javascript para o modalincluerelacoes.asp
********************************************************************/

/* INDICE DE FUNCOES ************************************************

window_onload()
btn_onclick(ctl)
txtPesquisa_onkeydown()
fillCmbFamilia()
fillgridRelacoes()
fillgridProdutos()
setupPage()
loadDataAndTreatInterface()
tipoRelacao_onchange()

*********************************************************************/

/* FUNCOES ESPECIFICAS DO GRID **************************************

js_modalincluerelacoes_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
js_modalincluerelacoes_AfterEdit(Row, Col)
js_modalincluerelacoes_DblClick()

*********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_loadingwindow = true;
var glb_modEmailsProm_Timer = null;

// Dsos genericos para banco de dados
var dsoGen01 = new CDatatransport("dsoGen01");
var dsoFillCmbs = new CDatatransport("dsoFillCmbs");
var dsoIncluirRelacoes = new CDatatransport("dsoIncluirRelacoes");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES ESPECIFICAS DO GRID
/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalincluerelacoes_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 0;
    
    if ( glb_totalCols__ != false )
        firstLine = 1;
        
    if (NewRow > firstLine)
    {
        fg.Row = NewRow;
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalincluerelacoes_AfterEdit(Row, Col)
{
	btnIncluirState();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalincluerelacoes_DblClick()
{
	var empresa = getCurrEmpresaData();
	var nRelacaoID = 0;
	var sExecCarrier = '';
	
	if (fg.Row >= 1)
	{
		nRelacaoID = getCellValueByColKey(fg, 'RelacaoID', fg.Row);
		sExecCarrier = "sendJSCarrier(getHtmlId(), 'SHOWRELCONCEITOS', new Array(" + empresa[0] + "," +  nRelacaoID + "))";
		sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, sExecCarrier);
	}	
}
// FINAL DE FUNCOES ESPECIFICAS DO GRID

// FUNCOES ESPECIFICAS DA JANELA
/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
}

/********************************************************************
Mostra a janela pela primeira vez
********************************************************************/
function show_This_Modal()
{
	if ( glb_modEmailsProm_Timer != null )    
	{
		window.clearInterval(glb_modEmailsProm_Timer);
		
		glb_modEmailsProm_Timer = null;
	}
	
    // ajusta o body do html
    with (modalincluerelacoesBody)
    {
        style.backgroundColor = 'white';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
    txtPesquisa.focus();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    
    if (ctl.id == btnOK.id )
    {
		;
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );
	else if (ctl.id == btnLista.id)
		fillgridProdutos();
	else if (ctl.id == btnIncluir.id)
		incluirRelacoes();
}

/********************************************************************
Preenche o grid de Relacoes
********************************************************************/
function fillgridRelacoes()
{
    if (!glb_loadingwindow)
		lockControlsInModalWin(true);

	setConnection(dsoGen01);

	dsoGen01.SQL = 'SELECT (CASE WHEN (a.SujeitoID = ' + glb_nProdutoID + ') THEN b.SujeitoOutros ELSE b.ObjetoOutros END) AS Tipo, ' +
		'd.Conceito AS Familia, c.ConceitoID AS ProdutoID, c.Conceito AS Produto, c.Modelo AS Modelo, c.PartNumber AS PartNumber, ' +
		'a.RelacaoID AS RelacaoID ' +
	'FROM RelacoesConceitos a WITH(NOLOCK), TiposRelacoes b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Conceitos d WITH(NOLOCK) ' +
	'WHERE (a.EstadoID = 2 AND a.TipoRelacaoID = b.TipoRelacaoID AND ' +
		'((a.SujeitoID = ' + glb_nProdutoID + ' AND a.ObjetoID = c.ConceitoID) OR ' +
		 '(a.ObjetoID = ' + glb_nProdutoID + ' AND a.SujeitoID = c.ConceitoID)) AND ' +
		 'c.ProdutoID = d.ConceitoID) ' +
	'ORDER BY d.Conceito, c.ConceitoID, c.Conceito';

	dsoGen01.ondatasetcomplete = fillgridRelacoes_DSC;
	dsoGen01.Refresh();
}

/********************************************************************
Preenche o grid de produtos
********************************************************************/
function fillgridRelacoes_DSC()
{
    var i;
    
    fg.Redraw = 0;
    
    fg.Editable = false;

    headerGrid(fg,['Tipo',
                    'Fam�lia',
                    'ID',
                    'Produto',
                    'Modelo',
                    'Part Number',
                    'RelacaoID'],[]);
    
    fillGridMask(fg,dsoGen01,['Tipo',
							  'Familia',
                              'ProdutoID',
                              'Produto',
                              'Modelo',
                              'PartNumber',
                              'RelacaoID'],
                              ['','','','','','',''],
                              ['','','','','','','']);
       
    alignColsInGrid(fg,[2,6]);                           

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

	fg.MergeCells = 4;
	fg.MergeCol(-1) = true;
    
    fg.ExplorerBar = 5;

	fg.Editable = false;
	
    fg.Redraw = 2;
            
    if (glb_loadingwindow)
	{
		glb_loadingwindow = false;
		glb_modEmailsProm_Timer = window.setInterval('show_This_Modal()', 100, 'JavaScript');
	}
	else
	{
		lockControlsInModalWin(false);
		fg.focus();
	}
}

/********************************************************************
Transfere keydown para btnOK_Click
********************************************************************/
function txtPesquisa_onkeydown()
{
    if ( event.keyCode == 13 )
		fillgridProdutos();
}

/********************************************************************
Executa a pesquisa de produtos, o resultado da pesquisa eh inserido
no combo selProdutos
********************************************************************/
function fillgridProdutos()
{
	var sPesquisa = trimStr(txtPesquisa.value);
	var nTipoRelacaoID = selTipoRelacao.value;
	var sFiltroFamilia = '';
	var bfirst = true;
	var i;
	var sFiltroEmpresa = '';
	
	for (i=0; i<selFamilia.length; i++)
	{
		if (selFamilia.options[i].selected)
		{
			if (bfirst)
			{
				bfirst = false;
				sFiltroFamilia = ' AND b.ConceitoID IN (' + selFamilia.options[i].value;
			}
			else
				sFiltroFamilia += ',' + selFamilia.options[i].value;
		}
	}

	if (!bfirst)
		sFiltroFamilia = sFiltroFamilia + ')';
	
	if (chkEmpresa.checked)
	    sFiltroEmpresa = ' AND (SELECT COUNT(*) FROM RelacoesPesCon WITH(NOLOCK) ' +
			'WHERE (TipoRelacaoID = 61 AND EstadoID NOT IN(4,5) AND ' +
				'SujeitoID = ' + glb_nEmpresaID + ' AND ObjetoID = a.ConceitoID)) > 0';

	lockControlsInModalWin(true);
	setConnection(dsoFillCmbs);

	dsoFillCmbs.SQL = 'SELECT DISTINCT a.ConceitoID AS ProdutoID, a.Conceito AS Produto, ' +
			'a.Modelo, a.Descricao, a.PartNumber, CONVERT(BIT, 0) AS OK ' +
		'FROM Conceitos a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
		'WHERE (a.TipoConceitoID = 303 AND a.EstadoID = 2 AND ' +
			'a.ProdutoID = b.ConceitoID AND a.MarcaID = c.ConceitoID AND ' +
			'((a.Conceito LIKE ' + '\'' + '%' + sPesquisa + '%' + '\'' + ') OR ' +
			  '(b.Conceito LIKE ' + '\'' + '%' + sPesquisa + '%' + '\'' + ') OR ' +
			  '(c.Conceito LIKE ' + '\'' + '%' + sPesquisa + '%' + '\'' + ') OR ' +
			  '(a.Modelo LIKE ' + '\'' + '%' + sPesquisa + '%' + '\'' + ') OR ' +
			  '(a.Descricao LIKE ' + '\'' + '%' + sPesquisa + '%' + '\'' + ') OR ' + 
			  '(a.PartNumber LIKE ' + '\'' + '%' + sPesquisa + '%' + '\'' + ') OR ' +
			  '(a.ConceitoID LIKE ' + '\'' + '%' + sPesquisa + '%' + '\'' + ')) AND ' +
			'((SELECT COUNT(*) FROM RelacoesConceitos WITH(NOLOCK) ' +
				'WHERE (TipoRelacaoID = ' + nTipoRelacaoID + ' AND ' +
					'((SujeitoID = ' + glb_nProdutoID + ' AND ObjetoID = a.ConceitoID) OR ' +
					'(ObjetoID = ' + glb_nProdutoID + ' AND SujeitoID = a.ConceitoID)))) = 0) ' +
					sFiltroFamilia + sFiltroEmpresa + ') ' +
		'ORDER BY a.ConceitoID';

	dsoFillCmbs.ondatasetcomplete = fillgridProdutos_DSC;
	dsoFillCmbs.Refresh();
}

/********************************************************************
Preenche o grid de produtos
********************************************************************/
function fillgridProdutos_DSC()
{
    var i;
    
    fg2.Redraw = 0;
    
    headerGrid(fg2,['OK',
					'ID',
                    'Produto',
                    'Descricao',
                    'Modelo',
                    'Part Number'],[]);
    
    fillGridMask(fg2,dsoFillCmbs,['OK',
							  'ProdutoID*',
                              'Produto*',
                              'Descricao*',
                              'Modelo*',
                              'PartNumber*'],
                              ['','','','','',''],
                              ['','','','','','']);
       
    alignColsInGrid(fg2,[1]);                           

    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0,fg2.Cols-1);

    fg2.ExplorerBar = 5;

    fg2.FrozenCols = 2;

    fg2.Redraw = 2;
            
	lockControlsInModalWin(false);

    // coloca foco no grid
    fg2.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText(glb_nProdutoID + ' ' + glb_sProduto + ' (' + glb_sFamilia + ')', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;
    var nCmbsHeight = 150;
    
    // excepcionalidade desta janela por ter dois grids
    hr_L_FGBorder.style.visibility = 'hidden';
	hr_R_FGBorder.style.visibility = 'hidden';
	hr_B_FGBorder.style.visibility = 'hidden';
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

	// ajusta elementos no divFields
    adjustElementsInForm([['lblTipoRelacao','selTipoRelacao',25,1,-10,-10],
						  ['lblEmpresa','chkEmpresa',3,1],
						  ['lblPesquisa','txtPesquisa',27,1],
						  ['lblFamilia','selFamilia',25,2,-ELEM_GAP]],null,null,true);

    selTipoRelacao.style.height = nCmbsHeight;
    selFamilia.style.height = nCmbsHeight;
    btnLista.disabled = false;
    selTipoRelacao.onchange = tipoRelacao_onchange;
	txtPesquisa.onkeydown = txtPesquisa_onkeydown;

    // ajusta o divFields
    elem = window.document.getElementById('divFields');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - (2 * ELEM_GAP);
        // altura e fixa para tres linhas de campos
        height = parseInt(selFamilia.currentStyle.top, 10) + 
				 parseInt(selFamilia.currentStyle.height, 10) + ELEM_GAP;
        y_gap = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
    }
    
    // ajusta o divFG2
    elem = window.document.getElementById('divFG2');
    with (elem.style)
    {
        border = 'inset';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = selFamilia.offsetLeft + selFamilia.offsetWidth + ELEM_GAP;
        top = selFamilia.offsetTop;
        width = modWidth - frameBorder - (selFamilia.offsetLeft + selFamilia.offsetWidth) - (3 * ELEM_GAP);    
        height = selFamilia.offsetHeight;
    }
    
    elem = document.getElementById('fg2');
    with (elem.style)
    {
        visibility = 'visible';
        left = 1;
        top = 1;
        width = parseInt(document.getElementById('divFG2').currentStyle.width) - 3;
        height = parseInt(document.getElementById('divFG2').currentStyle.height) - 3;
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'inset';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = divFields.offsetLeft;
        top = divFields.offsetTop + divFields.offsetHeight + (ELEM_GAP * 2);
        width = modWidth - (3 * ELEM_GAP) + 2;    
        height = modHeight - divFG.offsetTop - ELEM_GAP - 6;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 1;
        top = 1;
        width = parseInt(document.getElementById('divFG').currentStyle.width) - 3;
        height = parseInt(document.getElementById('divFG').currentStyle.height) - 3;
    }

	with (lblRelacoes.style)
	{
		height = 16;
		width = lblRelacoes.innerText.length * FONT_WIDTH;
		left = divFG.offsetLeft;
		top = divFG.offsetTop - lblRelacoes.offsetHeight - (ELEM_GAP/2);
	}
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Editable = false;

    startGridInterface(fg2);
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;
    fg2.Editable = true;
    
    btnOK.disabled = true;
    
	btnLista.style.top = txtPesquisa.offsetTop;
    btnLista.style.left = parseInt(txtPesquisa.currentStyle.left, 10) +
		txtPesquisa.offsetWidth + ELEM_GAP - 4;
	btnLista.style.width = btnOK.offsetWidth;
	
	btnIncluir.style.top = btnLista.offsetTop;
    btnIncluir.style.left = parseInt(btnLista.currentStyle.left, 10) +
		btnLista.offsetWidth + (ELEM_GAP / 3);
	btnIncluir.style.width = btnOK.offsetWidth;
	btnIncluir.disabled = true;

    btnOK.style.top = btnLista.offsetTop;
    btnOK.style.left = parseInt(btnLista.currentStyle.left, 10) +
		btnLista.offsetWidth + (ELEM_GAP / 3);
		
	btnCanc.style.top = 0;
    btnCanc.style.left = 0;
    btnCanc.style.width = 0;
    btnCanc.style.height = 0;
    btnCanc.style.visibility = 'hidden';
    
	tipoRelacao_onchange();
}

/********************************************************************
Evento onchange do combo de Tipo Relacao
********************************************************************/
function tipoRelacao_onchange()
{
	if (selTipoRelacao.options.length > 0)
		setLabelOfControl(lblTipoRelacao, selTipoRelacao.value);
	else
		setLabelOfControl(lblTipoRelacao, '');

	fillCmbFamilia();
}

/********************************************************************
Vai ao banco buscar os dados que serao usado para preencher o combo
	de Fam�lia.
********************************************************************/
function fillCmbFamilia()
{
    if (!glb_loadingwindow)
		lockControlsInModalWin(true);

	setConnection(dsoFillCmbs);
	
	var sFiltro = selTipoRelacao.options.item(selTipoRelacao.selectedIndex).getAttribute('Filtro', 1);
	var sFiltroSelect = '';

	if (sFiltro.indexOf('{1}') >= 0)
	{
	    sFiltroSelect = ' AND ((SELECT COUNT(*) FROM RelacoesConceitos WITH(NOLOCK) ' +
			'WHERE (TipoRelacaoID = 51 AND EstadoID = 2 AND ((b.ConceitoID = SujeitoID AND ObjetoID = ' + glb_nFamiliaID + ') OR ' +
				'(b.ConceitoID = ObjetoID AND SujeitoID = ' + glb_nFamiliaID + ')))) > 0)';
	}
	
	if (sFiltro.indexOf('{2}') >= 0)
	{
	    sFiltroSelect = ' AND ((SELECT COUNT(*) FROM RelacoesConceitos WITH(NOLOCK) ' +
			'WHERE (TipoRelacaoID = 51 AND EstadoID = 2 AND ((b.ConceitoID = SujeitoID AND ObjetoID = ' + glb_nFamiliaID + ') OR ' +
				'(b.ConceitoID = ObjetoID AND SujeitoID = ' + glb_nFamiliaID + ')))) = 0)';
	
	}
	
	dsoFillCmbs.SQL = 'SELECT DISTINCT b.ConceitoID AS fldID, b.Conceito AS fldName ' +
			'FROM Conceitos a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
			'WHERE (a.TipoConceitoID = 303 AND a.EstadoID = 2 AND ' +
				'a.ProdutoID = b.ConceitoID AND b.EstadoID = 2 ' + sFiltroSelect + ') ' +
			'ORDER BY b.Conceito ';

	dsoFillCmbs.ondatasetcomplete = fillCmbFamilia_DSC;
	dsoFillCmbs.Refresh();
}

/********************************************************************
Preenche o combo de Fam�lia.
********************************************************************/
function fillCmbFamilia_DSC()
{
    var optionStr, optionValue;
    var oOption;
    var i;
	
	clearComboEx(['selFamilia']);

    while (! dsoFillCmbs.recordset.EOF )
    {
        optionStr = dsoFillCmbs.recordset['fldName'].Value;
		optionValue = dsoFillCmbs.recordset['fldID'].Value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selFamilia.add(oOption);
        dsoFillCmbs.recordset.MoveNext();
    }


    if (glb_loadingwindow)
	{
		fillgridRelacoes();
	}
	else
	{
		lockControlsInModalWin(false);
		window.focus();
	}

	selFamilia.disabled = (selFamilia.options.length == 0);
}

/********************************************************************
Incluir Relacao
********************************************************************/
function incluirRelacoes()
{
	var i;
	var strPars = '';

    var nContador = 0;
    var _retMsg;
    var sMsg = 'Gerar ';
            
	strPars = '?nTipoRelacaoID=' + escape(selTipoRelacao.value);
	strPars += '&nProdutoObjetoID=' + escape(glb_nProdutoID);
	strPars += '&nUsuarioID=' + escape(getCurrUserID());

    for (i=1;i<fg2.Rows;i++)
    {
		if (getCellValueByColKey(fg2, 'OK', i) != 0)
		{
			nContador++;
			strPars += '&nProdutoSujeitoID=' + escape(getCellValueByColKey(fg2, 'ProdutoID*', i));
		}
    }
    
	sMsg = sMsg + nContador.toString() + (nContador == 1 ? ' rela��o' : ' rela��es') + '?';
	_retMsg = window.top.overflyGen.Confirm(sMsg);
			        	    
	if ( (_retMsg == 0) || (_retMsg == 2) )
	    return null;

	lockControlsInModalWin(true);

    dsoIncluirRelacoes.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/incluirrelacoes.aspx' + strPars;
    dsoIncluirRelacoes.ondatasetcomplete = incluirRelacoes_DSC;
    dsoIncluirRelacoes.refresh();
}

/********************************************************************
Preenche o grid de produtos
********************************************************************/
function incluirRelacoes_DSC()
{
	if (!((dsoIncluirRelacoes.recordset.BOF)&&(dsoIncluirRelacoes.recordset.BOF)))
	{
		if (dsoIncluirRelacoes.recordset['Mensagem'].value != '')
			if ( window.top.overflyGen.Alert(dsoIncluirRelacoes.recordset['Mensagem'].value) == 0 )
				return null;
 	}

	fg2.Rows = 1;
	lockControlsInModalWin(false);
	btnIncluirState();
	fillgridRelacoes();
}

function btnIncluirState()
{
	btnIncluir.disabled = true;

	for (i=1;i<fg2.Rows;i++)
	{
		if (getCellValueByColKey(fg2, 'OK', i) != 0)
		{
			btnIncluir.disabled = false;
			break;
		}			
	}
}
